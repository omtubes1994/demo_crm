<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/


$hook['pre_controller'][] = array(
        'class'    => 'Login_hooks',
        'function' => 'set_graph_data',
        'filename' => 'login_hooks.php',
        'filepath' => 'hooks',
        'params'   => array()
        );

$hook['pre_controller'][] = array(
        'class'    => 'Login_hooks',
        'function' => 'set_alert_data',
        'filename' => 'login_hooks.php',
        'filepath' => 'hooks',
        'params'   => array()
          );

?>