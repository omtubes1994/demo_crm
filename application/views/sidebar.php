<?php
	// echo "<pre>";print_r($this->session->userdata());echo"</pre><hr>";
	$lead_count = $this->session->userdata('lead_count');
	$main_module = $this->session->userdata('main_module');
	$main_module_access = array_unique(array_column($this->session->userdata('main_module_access'), 'module_id'));
	$sub_module = $this->session->userdata('sub_module');
	$sub_module_access = array();
	foreach ($this->session->userdata('sub_module_access') as $value) {
		
		$sub_module_access[$value['module_id']][] = $value['sub_module_id'];	
	}
	// echo "<pre>";print_r($main_module_access);echo"</pre><hr>";
	// echo "<pre>";print_r($sub_module_access);echo"</pre><hr>";exit;
?>

		<div class="kt-grid kt-grid--hor kt-grid--root">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

				<!-- begin:: Aside -->

				<!-- Uncomment this to display the close button of the panel
<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
-->
				<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

					<!-- begin:: Aside -->
					<div class="kt-aside__brand kt-grid__item " id="kt_aside_brand" style="background: #fff;">
						<div class="kt-aside__brand-logo">
							<a href="index.html">
								<img alt="Logo" src="assets/media/logos/logo.png" width="150" height="50" />
							</a>
						</div>
						<div class="kt-aside__brand-tools">
							<button class="kt-aside__brand-aside-toggler" id="kt_aside_toggler">
								<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
										<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<polygon points="0 0 24 0 24 24 0 24" />
											<path d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z" fill="#000000" fill-rule="nonzero" transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999) " />
											<path d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999) " />
										</g>
									</svg></span>
								<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
										<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<polygon points="0 0 24 0 24 24 0 24" />
											<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
											<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
										</g>
									</svg></span>
							</button>
						</div>
					</div>
					<!-- end:: Aside -->

					<!-- begin:: Aside Menu -->
					<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
						<div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
							<ul class="kt-menu__nav ">
								<?php foreach($main_module as $main_module_details){  ?>
									<?php 
									 	if(in_array($main_module_details['module_id'], $main_module_access) && !empty($sub_module_access[$main_module_details['module_id']]) ) { 
										$active_sub_menu="";
										if($main_module_details['module_id'] == $this->session->userdata('active_module_id')) {
											$active_sub_menu="kt-menu__item--open";
										}
									?>
										<?php if(count($sub_module[$main_module_details['module_id']]) == 1){?>
											<li class="kt-menu__item sub_module <?php echo $active_sub_menu;?>" aria-haspopup="true" main_module_id="<?php echo $main_module_details['module_id']?>" sub_module_id="<?php echo $sub_module[$main_module_details['module_id']][0]['sub_module_id']; ?>">
												<a href="<?php echo site_url($sub_module[$main_module_details['module_id']][0]['url']);?>" class="kt-menu__link ">
													<span class="kt-menu__link-icon">
														<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
															<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																<?php echo $main_module_details['icon_svg'];?>
															</g>
														</svg>
													</span>
													<span class="kt-menu__link-text" style="font-size: 1rem; font-weight: 500; line-height: 1.5rem; -webkit-transition: color 0.3s ease; transition: color 0.3s ease;"><?php echo $main_module_details['module_name']; ?></span>
												</a>
											</li>
										<?php }else{ ?>
											<li class="kt-menu__item  kt-menu__item--submenu <?php echo $active_sub_menu;?>" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
												<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
													<span class="kt-menu__link-icon" style="color: #fff;">
														
														<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
																<!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch -->
																<title>Stockholm-icons / Communication / Clipboard-list</title>
																<desc>Created with Sketch.</desc>
																<defs></defs>
																<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																	<?php echo $main_module_details['icon_svg'];?>
															</g>
														</svg>
													</span>
													<span class="kt-menu__link-text" style="font-size: 1rem; font-weight: 500; line-height: 1.5rem; -webkit-transition: color 0.3s ease; transition: color 0.3s ease;"><?php echo $main_module_details['module_name']; ?>
													</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
												</a>
												<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
													<ul class="kt-menu__subnav">
														<?php foreach($sub_module[$main_module_details['module_id']] as $sub_module_details){ ?>
															<?php 
																if(in_array($sub_module_details['sub_module_id'], $sub_module_access[$main_module_details['module_id']])) { 
																	$active_sub_module="";
																	if($sub_module_details['sub_module_id'] == $this->session->userdata('active_sub_module_id')) {
																		$active_sub_module="kt-menu__item--active";
																	}	
																?>
																<li class="kt-menu__item sub_module <?php echo $active_sub_module;?>" aria-haspopup="true" main_module_id="<?php echo $main_module_details['module_id']?>" sub_module_id="<?php echo $sub_module_details['sub_module_id']; ?>">
																	<a href="<?php echo site_url($sub_module_details['url']);?>" class="kt-menu__link">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																			<span></span>
																		</i>
																		<span class="kt-menu__link-text" style="font-size: 1rem;font-weight: 500; line-height: 1.5rem; -webkit-transition: color 0.3s ease; transition: color 0.3s ease;"><?php echo $sub_module_details['sub_module_name']; ?></span>
																	</a>
																</li>
															<?php } ?>	
														<?php } 
														?>
													</ul>
												</div>
											</li>
										<?php } ?>
										<?php if(in_array($main_module_details['module_id'], array(1, 3, 17, 20))){?>
										    <li class="kt-menu__item" aria-haspopup="true">
												<a href="javascript:void(0);" class="kt-menu__link ">
													<span class="kt-menu__link-icon">
														<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
															<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"></g>
														</svg>
													</span>
													<span class="kt-menu__link-text"></span>
												</a>
											</li>
										<?php } ?>	
									<?php } ?>
								<?php } ?>
							</ul>
						</div>
					</div>
					<!-- end:: Aside Menu -->
				</div>

				<!-- end:: Aside -->

				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

					<!-- begin:: Header -->
					<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

						<!-- begin:: Header Menu -->

						<!-- Uncomment this to display the close button of the panel
<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
-->
						<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
							<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
								<ul class="kt-menu__nav ">
									<li class="kt-menu__item  kt-menu__item--open kt-menu__item--here kt-menu__item--submenu kt-menu__item--rel kt-menu__item--open kt-menu__item--here kt-menu__item--active" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
										<h3 class="kt-subheader__title"><?php echo $title; ?></h3>
									</li>
									
								</ul>
							</div>
						</div>

						<!-- end:: Header Menu -->

						<!-- begin:: Header Topbar -->
						<div class="kt-header__topbar">

							<!--begin: Search -->

							<!--begin: Search -->
							<!-- <div class="kt-header__topbar-item kt-header__topbar-item--search dropdown" id="kt_quick_search_toggle">
								<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
									<span class="kt-header__topbar-icon">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<rect x="0" y="0" width="24" height="24" />
												<path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
												<path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero" />
											</g>
										</svg> </span>
								</div>
								<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-lg">
									<div class="kt-quick-search kt-quick-search--dropdown kt-quick-search--result-compact" id="kt_quick_search_dropdown">
										<form method="get" class="kt-quick-search__form">
											<div class="input-group">
												<div class="input-group-prepend"><span class="input-group-text"><i class="flaticon2-search-1"></i></span></div>
												<input type="text" class="form-control kt-quick-search__input" placeholder="Search...">
												<div class="input-group-append"><span class="input-group-text"><i class="la la-close kt-quick-search__close"></i></span></div>
											</div>
										</form>
										<div class="kt-quick-search__wrapper kt-scroll" data-scroll="true" data-height="325" data-mobile-height="200">
										</div>
									</div>
								</div>
							</div> -->

							<!--end: Search -->

							<!--end: Search -->
							<!-- add to jump -->
							<div class="kt-header__topbar-item kt-header__topbar-item--search dropdown" id="kt_quick_search_toggle">
								<p>Current Login Time: <?php echo date('d M Y h:i A', strtotime($this->session->userdata('current_login')));?><br/>
								Current IP Address: <?php echo $this->session->userdata('current_ip');?></p>
							</div>
									<div class="dropdown dropdown-inline" id="add_jump" data-toggle-="kt-tooltip" title="Quick actions" data-placement="left">
											<a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--md">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24" />
														<path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
														<path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" fill="#000000" />
													</g>
												</svg>

												<!--<i class="flaticon2-plus"></i>-->
											</a>

											<?php //if($this->session->userdata('role') == 5){ ?>
											<div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">

												<!--begin::Nav-->
												<ul class="kt-nav">
													
													<li class="kt-nav__item">
														<!-- <a id="task_update_btn" data-target="#daily_task_modal" data-toggle="modal" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-calendar"></i>
															<span class="kt-nav__link-text">Daily Task Update</span>
														</a> -->
													</li>
												</ul>

												<!--end::Nav-->
											</div>
											<?php //} ?>
										</div>
							<!-- add to jump -->

							<?php if($this->session->userdata('role') == 5){?>
							<!--begin: Notifications -->
							<div class="kt-header__topbar-item dropdown">
								<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="30px,0px" aria-expanded="true">
									<span class="kt-header__topbar-icon kt-pulse kt-pulse--brand">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<rect x="0" y="0" width="24" height="24" />
												<path d="M2.56066017,10.6819805 L4.68198052,8.56066017 C5.26776695,7.97487373 6.21751442,7.97487373 6.80330086,8.56066017 L8.9246212,10.6819805 C9.51040764,11.267767 9.51040764,12.2175144 8.9246212,12.8033009 L6.80330086,14.9246212 C6.21751442,15.5104076 5.26776695,15.5104076 4.68198052,14.9246212 L2.56066017,12.8033009 C1.97487373,12.2175144 1.97487373,11.267767 2.56066017,10.6819805 Z M14.5606602,10.6819805 L16.6819805,8.56066017 C17.267767,7.97487373 18.2175144,7.97487373 18.8033009,8.56066017 L20.9246212,10.6819805 C21.5104076,11.267767 21.5104076,12.2175144 20.9246212,12.8033009 L18.8033009,14.9246212 C18.2175144,15.5104076 17.267767,15.5104076 16.6819805,14.9246212 L14.5606602,12.8033009 C13.9748737,12.2175144 13.9748737,11.267767 14.5606602,10.6819805 Z" fill="#000000" opacity="0.3" />
												<path d="M8.56066017,16.6819805 L10.6819805,14.5606602 C11.267767,13.9748737 12.2175144,13.9748737 12.8033009,14.5606602 L14.9246212,16.6819805 C15.5104076,17.267767 15.5104076,18.2175144 14.9246212,18.8033009 L12.8033009,20.9246212 C12.2175144,21.5104076 11.267767,21.5104076 10.6819805,20.9246212 L8.56066017,18.8033009 C7.97487373,18.2175144 7.97487373,17.267767 8.56066017,16.6819805 Z M8.56066017,4.68198052 L10.6819805,2.56066017 C11.267767,1.97487373 12.2175144,1.97487373 12.8033009,2.56066017 L14.9246212,4.68198052 C15.5104076,5.26776695 15.5104076,6.21751442 14.9246212,6.80330086 L12.8033009,8.9246212 C12.2175144,9.51040764 11.267767,9.51040764 10.6819805,8.9246212 L8.56066017,6.80330086 C7.97487373,6.21751442 7.97487373,5.26776695 8.56066017,4.68198052 Z" fill="#000000" />
											</g>
										</svg> <span class="kt-pulse__ring"></span>
									</span>

															<!--
										Use dot badge instead of animated pulse effect:
										<span class="kt-badge kt-badge--dot kt-badge--notify kt-badge--sm kt-badge--brand"></span>
									-->
								</div>

								<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-lg" style="width: 1000px !important;">
									<form action="<?php echo site_url('client/clientConnect'); ?>" method="post" id="qc_frm">
										<!--begin: Head -->
										<div class="kt-head kt-head--skin-dark kt-head--fit-x kt-head--fit-b" style="background-image: url(assets/media/misc/bg-1.jpg)">
											<h3 class="kt-head__title" style="font-size: 2rem;">
												Touch Points
											</h3>
											<ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-success kt-notification-item-padding-x" role="tablist" id="qc_tabs">
												<li class="nav-item">
													<a class="nav-link active show" data-toggle="tab" href="#connect_list" role="tab" aria-selected="true" style="font-size: 1.3rem;">List</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" data-toggle="tab" href="#add_connect" role="tab" aria-selected="false" style="font-size: 1.3rem;">Add New</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" data-toggle="tab" href="#qc_performance" role="tab" aria-selected="false" style="font-size: 1.3rem;">Performance</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" data-toggle="tab" href="#qc_no_points" role="tab" aria-selected="false" style="font-size: 1.3rem;">No Touch Points</a>
												</li>
											</ul>
										</div>

										<!--end: Head -->
										<div class="tab-content container">
											<div class="tab-pane active show" id="connect_list" role="tabpanel">
												<div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
													<!-- <div class="row">
														<div class="col-md-4 form-group">
															<label for="qc_start">Start Date</label>
															<input type="text" class="form-control input-sm hasdatepicker" id="qc_start">
														</div>
														<div class="col-md-4 form-group">
															<label for="qc_start">Start Date</label>
															<input type="text" class="form-control input-sm hasdatepicker" id="qc_start">
														</div>
														<div class="col-md-4 form-group">
															<button type="button" class="btn btn-sm btn-warning" id="qc_search">Filter</button>
														</div>
													</div> -->
													<style type="text/css">
														#qc_table th{
															text-align: center;
														}
													</style>
													<table class="table table-bordered" id="qc_table">
														<thead>
															<tr>
																<th width="5%">Sr #</th>
																<th width="8%">Contacted On</th>
																<th width="20%">Client Name</th>
																<th width="17%">Person Name</th>
																<th width="8%">Contacted Via</th>
																<th width="7%">Email Sent</th>
																<th width="22%">Comments</th>
																<th width="13%">Action</th>
															</tr>
														</thead>
														<tbody></tbody>
													</table>
												</div>
											</div>
											<div class="tab-pane" id="add_connect" role="tabpanel">
												<div class="kt-notification kt-margin-t-10 kt-margin-b-10" data-scroll="false" data-height="300" data-mobile-height="200" id="quick_connect_div">
													<div class="row">
														<div class="col-md-6 form-group">
															<label for="qc_contacted_on">Contacted On</label>
															<input type="text" class="form-control hasdatepicker validate[required]" id="qc_contacted_on" name="qc_contacted_on" value="<?php echo date('d-m-Y'); ?>">
														</div>

														<div class="col-md-6 form-group">
															<label for="qc_client">Client</label>
															<div class="input-group">
																<select class="form-control validate[required]" id="qc_client" name="qc_client">
																	<option value="">Select</option>
																</select>
																<div class="input-group-append">
																	<button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#qc_add_company">+</button>
																</div>
															</div>
														</div>

														<div class="col-md-6 form-group">
															<label for="qc_person">Person spoken to</label>
															<div class="input-group">
																<select class="form-control validate[required]" id="qc_member" name="qc_member">
																	<option value="">Select</option>
																</select>
																<div class="input-group-append">
																	<button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#qc_add_member">+</button>
																</div>
															</div>
														</div>

														<div class="col-md-6 form-group">
															<label for="qc_contact_via">Contacted Via</label>
															<select class="form-control validate[required]" name="qc_contact_via" id="qc_contact_via">
																<option value="">Select</option>
																<option value="whatsapp">Whatsapp</option>
																<option value="call">Call</option>
																<option value="linkedin">LinkedIn</option>
															</select>
														</div>

														<div class="col-md-6 form-group">
															<label for="qc_email_sent" id="qc_email_sent_lbl">Email Sent</label>
															<select class="form-control" name="qc_email_sent" id="qc_email_sent">
																<option value="No">No</option>
																<option value="Yes">Yes</option>
															</select> 
														</div>

														<div class="col-md-6 form-group">
															<label for="qc_comments">Comments</label>
															<textarea class="form-control validate[required]" id="qc_comments" name="qc_comments"></textarea>
														</div>
													</div>

													<div class="row">
														<div class="col-md-3 offset-3 form-group">
															<input type="hidden" name="qc_connect_id" id="qc_connect_id" >
															<button type="submit" id="qc_submit" class="form-control btn btn-success">Submit</button>
														</div>
														<div class="col-md-3 form-group">
															<button type="button" id="qc_cancel" class="form-control btn btn-warning">Cancel</button>
														</div>
													</div>
													<style>
														#quick_connect_div .select2-container{
															width: 90% !important;
														}
													</style>
												</div>
											</div>
											<div class="tab-pane" id="qc_performance" role="tabpanel">
												<div class="kt-notification kt-margin-t-10 kt-margin-b-10" data-scroll="false" data-height="300" data-mobile-height="200" id="">
													<div class="row">
														<div class="col-md-7">
															<figure class="highcharts-figure">
															    <div id="container-touch-points"></div>
															</figure>

															<style>
																.highcharts-figure, .highcharts-data-table table {
																    min-width: 360px; 
																    max-width: 800px;
																    margin: 1em auto;
																}

																.highcharts-data-table table {
																	font-family: Verdana, sans-serif;
																	border-collapse: collapse;
																	border: 1px solid #EBEBEB;
																	margin: 10px auto;
																	text-align: center;
																	width: 100%;
																	max-width: 500px;
																}
																.highcharts-data-table caption {
																    padding: 1em 0;
																    font-size: 1.2em;
																    color: #555;
																}
																.highcharts-data-table th {
																	font-weight: 600;
																    padding: 0.5em;
																}
																.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
																    padding: 0.5em;
																}
																.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
																    background: #f8f8f8;
																}
																.highcharts-data-table tr:hover {
																    background: #f1f7ff;
																}

															</style>
														</div>
														<div class="col-md-4">
															<div class="row">
																<div class="col-md-12">
																	<div class="card text-center">
																		<div class="card-body">
																			<div class="card-title">Your Average Touch-points</div>
																			<div class="row">
																				<div class="col-md-6">For <?php echo date('M'); ?> <h2 id="user_monthly_avg"></h2></div>
																				<div class="col-md-6">From Start <h2 id="user_total_avg"></h2></div>
																			</div>
																		</div>
																	</div>
																</div>

																<div class="col-md-12">
																	<div class="card text-center">
																		<div class="card-body">
																			<div class="card-title">Sales Team Average Touch-points</div>
																			<div class="row">
																				<div class="col-md-6">
																					For <?php echo date('M'); ?> <h2 id="team_monthly_avg"></h2></div>
																				<div class="col-md-6">
																					From Start <h2 id="team_monthly_connects"></h2></div>
																			</div>
																		</div>
																	</div>
																</div>

																<div class="col-md-12">
																	<div class="card text-center">
																		<div class="card-body">
																			<div class="card-title">Your Total Touch-points</div>
																			<div class="row">
																				<div class="col-md-6">For <?php echo date('M'); ?> <h2 id="user_monthly_connects"></h2></div>
																				<div class="col-md-6">From Start <h2 id="user_total_connects"></h2></div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="tab-pane" id="qc_no_points" role="tabpanel">
												<div class="kt-notification kt-margin-t-10 kt-margin-b-10" data-scroll="false" data-height="300" data-mobile-height="200" id="">
													<div class="row">
														<div class="col-md-3 form-group">
															<label for="no_date">Date</label>
															<input type="text" id="qc_no_date" class="form-control hasdatepicker" value="<?php echo date('d-m-Y'); ?>">
														</div>
														<div class="col-md-4 form-group">
															<label for="no_reason">Reason</label>
															<textarea id="qc_no_reason" class="form-control"></textarea>
														</div>
														<div class="col-md-3">
															<button style="margin-top: 50px;" type="button" class="btn btn-sm btn-warning" id="qc_updateReason">Update</button>
														</div>
													</div>
													<div class="row">
														<div class="offset-1 col-md-4">
															<table class="table table-bordered" id="pendingUpdate" style="text-align: center;">
																<thead>
																	<tr>
																		<td>Sr #</td>
																		<td>Touchpoints update pending</td>
																	</tr>
																</thead>
																<tbody></tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
											<!-- <div class="tab-pane" id="topbar_notifications_logs" role="tabpanel">
												<div class="kt-grid kt-grid--ver" style="min-height: 200px;">
													<div class="kt-grid kt-grid--hor kt-grid__item kt-grid__item--fluid kt-grid__item--middle">
														<div class="kt-grid__item kt-grid__item--middle kt-align-center">
															All caught up!
															<br>No new notifications.
														</div>
													</div>
												</div>
											</div> -->
										</div>
									</form>
								</div>
							</div>
							<!--end: Notifications -->

							<?php } ?>

							<div class="kt-header__topbar-item dropdown">
								<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="30px,0px" aria-expanded="true" id="user_noti">
									<span class="kt-header__topbar-icon kt-pulse kt-pulse--brand">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
										    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										        <path d="M17,12 L18.5,12 C19.3284271,12 20,12.6715729 20,13.5 C20,14.3284271 19.3284271,15 18.5,15 L5.5,15 C4.67157288,15 4,14.3284271 4,13.5 C4,12.6715729 4.67157288,12 5.5,12 L7,12 L7.5582739,6.97553494 C7.80974924,4.71225688 9.72279394,3 12,3 C14.2772061,3 16.1902508,4.71225688 16.4417261,6.97553494 L17,12 Z" fill="#000000"/>
										        <rect fill="#000000" opacity="0.3" x="10" y="16" width="4" height="4" rx="2"/>
										    </g>
										</svg> 
										<span class="kt-pulse__ring"></span>
									</span>
								</div>
								<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-lg">
									<form>

										<!--begin: Head -->
										<div class="kt-head kt-head--skin-dark kt-head--fit-x kt-head--fit-b" style="background-image: url(assets/media/misc/bg-1.jpg)">
											<h3 class="kt-head__title">
												User Notifications
												&nbsp;
												<span class="btn btn-success btn-sm btn-bold btn-font-md" id="notify_count">0</span>
											</h3>
											<ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-success kt-notification-item-padding-x" role="tablist">
												<li class="nav-item" style="display: none;">
													<a class="nav-link active show" data-toggle="tab" href="#topbar_notifications_notifications" role="tab" aria-selected="true">Alerts</a>
												</li>
											</ul>
										</div>

										<!--end: Head -->
										<div class="tab-content">
											<div class="tab-pane active show" id="topbar_notifications_notifications" role="tabpanel">
												<div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200" id="notify_div">
													
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>

							<!--begin: My Cart --><!--end: My Cart -->

							<!--begin: Quick panel toggler -->
							
							

							<!--end: Quick panel toggler -->

							<!--begin: Language bar -->	<!--end: Language bar -->

							<!--begin: User Bar -->
							<div class="kt-header__topbar-item kt-header__topbar-item--user">
								<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
									<div class="kt-header__topbar-user">
										<span class="kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
										<span class="kt-header__topbar-username kt-hidden-mobile"><?php echo ucwords($this->session->userdata('username')); ?></span>
										<img class="kt-hidden" alt="Pic" src="assets/media/users/300_25.jpg" />
										<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold"><?php echo strtoupper(substr($this->session->userdata('name'), 0, 1)); ?></span>
									</div>
								</div>
								<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">

									<!--begin: Head -->
									<div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url(assets/media/misc/bg-1.jpg)">
										<div class="kt-user-card__avatar">
											<img class="kt-hidden" alt="Pic" src="assets/media/users/300_25.jpg" />
											<span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success"><?php echo strtoupper(substr($this->session->userdata('name'), 0, 1)); ?></span>
										</div>
										<div class="kt-user-card__name">
											<?php echo ucwords($this->session->userdata('name')); ?>
										</div>
										<div class="kt-user-card__badge">
											<a href="<?php echo site_url('login/logout'); ?>" class="btn btn-success btn-sm btn-bold btn-font-md">Sign Out</a>
										</div>
									</div>

									<!--end: Head -->
									<?php if($this->session->userdata('production_access')['production_mobile_client_type']){?>
									<!--begin: Navigation -->
									<div class="kt-notification">
										
										<a href="javascript:void(0);" class="kt-notification__item">
											<div class="kt-notification__item-icon">
												<i class="flaticon2-calendar-3 kt-font-success"></i>
											</div>
											<div class="kt-notification__item-details">
												<div class="kt-notification__item-title kt-font-bold">
													Production
												</div>
												<div class="kt-notification__item-time kt-font-bold">
													<div class="kt-radio-inline">
														<label class="kt-radio">
															<input type="radio" name="radio_company_type" value="om"> Om
															<span></span>
														</label>
														<label class="kt-radio">
															<input type="radio" name="radio_company_type" value="zen"> Zengineer
															<span></span>
														</label>
														<label class="kt-radio">
															<input type="radio" name="radio_company_type" value="in"> Instinox
															<span></span>
														</label>
														<label class="kt-radio">
															<input type="radio" name="radio_company_type" value=""> All
															<span></span>
														</label>
														
													</div>
												</div>
											</div>
										</a>
										<a href="custom/apps/user/profile-3.html" class="kt-notification__item kt-hidden">
											<div class="kt-notification__item-icon">
												<i class="flaticon2-mail kt-font-warning"></i>
											</div>
											<div class="kt-notification__item-details">
												<div class="kt-notification__item-title kt-font-bold">
													My Messages
												</div>
												<div class="kt-notification__item-time">
													Inbox and tasks
												</div>
											</div>
										</a>
										<a href="custom/apps/user/profile-2.html" class="kt-notification__item kt-hidden">
											<div class="kt-notification__item-icon">
												<i class="flaticon2-rocket-1 kt-font-danger"></i>
											</div>
											<div class="kt-notification__item-details">
												<div class="kt-notification__item-title kt-font-bold">
													My Activities
												</div>
												<div class="kt-notification__item-time">
													Logs and notifications
												</div>
											</div>
										</a>
										<a href="custom/apps/user/profile-3.html" class="kt-notification__item kt-hidden">
											<div class="kt-notification__item-icon">
												<i class="flaticon2-hourglass kt-font-brand"></i>
											</div>
											<div class="kt-notification__item-details">
												<div class="kt-notification__item-title kt-font-bold">
													My Tasks
												</div>
												<div class="kt-notification__item-time">
													latest tasks and projects
												</div>
											</div>
										</a>
										<a href="custom/apps/user/profile-1/overview.html" class="kt-notification__item kt-hidden">
											<div class="kt-notification__item-icon">
												<i class="flaticon2-cardiogram kt-font-warning"></i>
											</div>
											<div class="kt-notification__item-details">
												<div class="kt-notification__item-title kt-font-bold">
													Billing
												</div>
												<div class="kt-notification__item-time">
													billing & statements <span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill kt-badge--rounded">2 pending</span>
												</div>
											</div>
										</a>
										<div class="kt-notification__custom kt-space-between kt-hidden">
											<a href="custom/user/login-v2.html" target="_blank" class="btn btn-label btn-label-brand btn-sm btn-bold">Sign Out</a>
											<a href="custom/user/login-v2.html" target="_blank" class="btn btn-clean btn-sm btn-bold">Upgrade Plan</a>
										</div>
									</div>

									<!--end: Navigation -->
									<?php } ?>
								</div>
							</div>

							<!--end: User Bar -->
						</div>

						<!-- end:: Header Topbar -->
					</div>

					<!-- end:: Header -->

<!--begin::Modal-->
<div class="modal fade" id="qc_add_company" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<?php echo form_open('', array('id' => 'addCompany', 'class' => 'kt-form kt-form--label-right'));?>
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add Client</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="recipient-name" class="form-control-label">Company Name:</label>
								<input type="text" class="form-control validate[required]" id="company_name" name="company_name">
							</div>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="message-text" class="form-control-label">Country:</label>
								<select class="form-control validate[required]" id="country" name="country">
									<option value="" disbaled>Select</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="message-text" class="form-control-label">Region:</label>
								<select class="form-control validate[required]" id="region" name="region">
									<option value="" disbaled>Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="message-text" class="form-control-label">Website:</label>
								<input type="text" class="form-control validate[custom[url]]" id="website" name="website">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Add Client</button>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>

<div class="modal fade" id="qc_add_member" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<?php echo form_open('', array('id' => 'addMember', 'class' => 'kt-form kt-form--label-right'));?>
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add Company Member</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-4 col-sm-12">
							<label for="message-text" class="form-control-label">Name:</label>
							<input type="text" class="form-control validate[required]" id="name" name="name">
						</div>

						<div class="col-md-4 col-sm-12">
							<label for="message-text" class="form-control-label">Email:</label>
							<input type="email" class="form-control validate[required,custom[email]]" id="email" name="email">
						</div>

						<div class="col-md-4 col-sm-12">
							<label for="message-text" class="form-control-label">Mobile #:</label>
							<input type="number" class="form-control validate[custom[onlyNumberSp]]" id="mobile" name="mobile">
						</div>

						<div class="col-md-4 col-sm-12">
							<label for="message-text" class="form-control-label">Whatsapp #:</label>
							<input type="number" class="form-control validate[custom[onlyNumberSp]]" id="whatsapp" name="whatsapp">
						</div>

						<div class="col-md-4 col-sm-12">
							<label for="message-text" class="form-control-label">Skype ID:</label>
							<input type="text" class="form-control" id="skype" name="skype">
						</div>

						<div class="col-md-4 col-sm-12">
							<label for="message-text" class="form-control-label">Telephone:</label>
							<input type="number" class="form-control validate[custom[onlyNumberSp]]" id="telephone" name="telephone">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" id="addMemberBtn" class="btn btn-primary">Add Member</button>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>

<div class="modal fade" id="daily_task_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
		<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Daily Task Update</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-success kt-notification-item-padding-x" role="tablist" id="qc_tabs">
					<li class="nav-item">
						<a class="nav-link active show" data-toggle="tab" href="#add_new" role="tab" aria-selected="true">Add Task Update</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#view_previous" role="tab" aria-selected="false" style="font-size: 1.3rem;">View Previous Updates</a>
					</li>
				</ul>
				<div class="tab-content container">
					<div class="tab-pane active show" id="add_new" role="tabpanel">
						<?php echo form_open('', array('id' => 'taskUpdate', 'class' => 'kt-form kt-form--label-right'));?>
							<div class="row">
								<div class="col-md-4 col-sm-12">
									<div class="form-group">
										<label class="form-control-label">Date:</label>
										<input type="text" class="form-control hasdatepicker validate[required]" value="<?php echo date('d-m-Y'); ?>" id="date" name="date">
									</div>
								</div>
								<div class="col-md-4 col-sm-12">
									<div class="form-group">
										<label for="task_accomplished" class="form-control-label">Task Accomplished:</label>
										<textarea class="form-control validate[required]" id="task_accomplished" name="task_accomplished"></textarea>
									</div>
								</div>
								<!-- <div class="col-md-4 col-sm-12">
									<div class="form-group">
										<label for="work_in_progress" class="form-control-label">Work in Progress:</label>
										<textarea class="form-control validate[required]" id="work_in_progress" name="work_in_progress"></textarea>
									</div>
								</div> -->
								<div class="col-md-4 col-sm-12">
									<div class="form-group">
										<label for="plan_for_tomorrow" class="form-control-label">Plan Tomorrow:</label>
										<textarea class="form-control validate[required]" id="plan_for_tomorrow" name="plan_for_tomorrow"></textarea>
									</div>
								</div>
								<?php if($this->session->userdata('role') == 5){ ?>
								<div class="col-md-8 col-sm-12">
									<div class="form-group">
										<label for="touch_points" class="form-control-label">Touch Points:</label>
										<div id="touch_points_count"></div>
									</div>
								</div>
								<?php } ?>
							<?php if($this->session->userdata('role') == 5){ ?>
								<!-- <div class="col-md-4 col-sm-12">
									<label for="ca" class="form-control-label">No of calls attempted (CA)</label>
									<input type="text" name="ca" id="ca" class="form-control validate[required,custom[onlyNumberSp]]">
								</div> -->

								<div class="col-md-4 col-sm-12">
									<label for="cc" class="form-control-label">No of calls connected (CC)</label>
									<input type="text" name="cc" id="cc" class="form-control validate[required,custom[onlyNumberSp]]">
								</div>

								<!--<div class="col-md-4 col-sm-12">
									<label for="rc" class="form-control-label">No of email replies received from unique clients (RC)</label>
									<input type="text" name="rc" id="rc" class="form-control validate[required,custom[onlyNumberSp]]">
								</div> -->

								<div class="col-md-4 col-sm-12">
									<label for="wa" class="form-control-label">No of conversations with clients on whatsapp (WA)</label>
									<input type="text" name="wa" id="wa" class="form-control validate[required,custom[onlyNumberSp]]">
								</div>

								<!-- <div class="col-md-4 col-sm-12">
									<label for="qs" class="form-control-label">No of quotation submitted (QS)</label>
									<input type="text" name="qs" id="qs" class="form-control validate[required,custom[onlyNumberSp]]">
								</div> -->

								<div class="col-md-4 col-sm-12">
									<label for="wa" class="form-control-label">No of Email Sent (Email_sent)</label>
									<input type="text" name="email_sent" id="email_sent" class="form-control validate[required,custom[onlyNumberSp]]">
								</div>

								<div class="col-md-4 col-sm-12">
									<label for="qs" class="form-control-label">No of Email Recieved (Email_recieved)</label>
									<input type="text" name="email_received" id="email_received" class="form-control validate[required,custom[onlyNumberSp]]">
								</div>
							<?php } else if($this->session->userdata('role') == 6 || $this->session->userdata('role') == 8){ ?>
								<div class="col-md-4 col-sm-12">
									<label for="rd" class="form-control-label">No of rfq done today (RD)</label>
									<input type="text" name="rd" id="rd" class="form-control validate[required,custom[onlyNumberSp]]">
								</div>

								<div class="col-md-4 col-sm-12">
									<label for="rp" class="form-control-label">No of rfq pending as of today (RP)</label>
									<input type="text" name="rp" id="rp" class="form-control validate[required,custom[onlyNumberSp]]">
								</div>

								<div class="col-md-4 col-sm-12">
									<label for="qp" class="form-control-label">Queries pending as of today (QP)</label>
									<input type="text" name="qp" id="qp" class="form-control validate[required,custom[onlyNumberSp]]">
								</div>
							<?php } ?>
								<div class="col-md-2 offset-10">
									<input type="hidden" id="master_id" name="master_id">
									<button type="submit" class="btn btn-primary">Update</button>
									<button type="button" class="btn btn-default" id="reset">Reset</button>
								</div>
							</div>
						<?php echo form_close(); ?>
					</div>
					<div class="tab-pane" id="view_previous" role="tabpanel">
						<table class="table table-bordered">
							<thead>
								<tr>
									<td>Sr #</td>
									<td>Date</td>
									<td>Task Accomplished</td>
									<td>Work in Progress</td>
									<td>Plan for Next Day</td>
									<td>Action</td>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!--end::Modal-->	
