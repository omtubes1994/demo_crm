<?php 
Class Home_model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->db2 = $this->load->database('marketing', true);
		// $this->crm_db = $this->load->database('crm_sales', true);//not used on live server
		// $this->db2 = $this->load->database('crm_marketing', true);
	}
	
	function getData($table, $where = ''){
		if($where != ''){
			$this->db->where($where);
		}

		if($table == 'daily_task_update'){
			$this->db->order_by('modified_on', 'desc');
		}
		return $this->db->get($table)->result_array();
	}

	function updateData($table, $data, $where){
		$this->db->update($table, $data, $where);
	}

	function insertData($table, $data){
		$this->db->insert($table, $data);
	}

	function deleteData($table, $where){
		$this->db->delete($table, $where);
	}
	
	function getMachineTime($user_id){
		$this->db->join('machine_time mt', 'mt.desktrack_userid = u.desktrack_user_id', 'inner');
		$res = $this->db->get_where('users u', array('u.user_id' => $user_id))->result_array();
		$ret = array();
		foreach ($res as $key => $value) {
			$ret[$value['date']] = $value['desktopTime'];
		}
		return $ret;
	}

	public function get_quotation_won_data($date, $assigned_to) {

		$this->db->select('quotation_mst.client_id, quotation_mst.quotation_mst_id, quotation_mst.quote_no, quotation_mst.confirmed_on, quotation_mst.reference, clients.client_name');
		$this->db->join('clients', 'clients.client_id = quotation_mst.client_id', 'inner');
		$this->db->where('quotation_mst.status', 'Won');
		if(!empty($date)) {
			$this->db->where('quotation_mst.confirmed_on >', $date);
		}
		// $this->db->where('quotation_mst.assigned_to', $assigned_to);
		$this->db->order_by('quotation_mst.confirmed_on', 'DESC');
		$result = $this->db->get('quotation_mst')->result_array();
		return $result;
	}

	public function get_quotation_follow_up_data($date, $assigned_to){

		$this->db->where('followup_date >', $date);
		$this->db->where('followup_date <=', date('Y-m-d'));
		$this->db->where("quote_no != '' and quote_no is not null");
		$this->db->where("status", "open");
		// $this->db->where('m.assigned_to', $assigned_to);
		$this->db->order_by('followup_date', 'DESC');
		return $this->db->get('quotation_mst')->result_array();
	}

	public function get_todays_birthdate_employee() {

		// $res = $this->db->query('SELECT first_name, last_name FROM `people_information` WHERE status = "Active" AND MONTH(birth_date) = MONTH(NOW()) AND DAY(birth_date) = DAY(NOW()- interval 5 DAY)')->result_array();
		$res = $this->db->query('SELECT first_name, last_name FROM `people_information` WHERE status = "Active" AND MONTH(birth_date) = MONTH(NOW()) AND DAY(birth_date) = DAY(NOW())')->result_array();
		// echo $this->db->last_query(),"<hr>";
		return $res;
	}

	public function get_revenue($revenue_type, $year_value) {

		$select_string = "YEAR(invoice_date) as year_or_month, grand_total, currency_id";
		$where_string = "is_deleted IS NULL AND currency_id != 0 AND YEAR(invoice_date) != '1970'";
		if($revenue_type == 'month') {

			$select_string = "MONTHNAME(invoice_date) as year_or_month, grand_total, currency_id";
			$where_string = "is_deleted IS NULL AND currency_id != 0 AND YEAR(invoice_date) = '".$year_value."'";
		}
		$res = $this->db->query("SELECT {$select_string} FROM invoice_mst WHERE {$where_string} order by invoice_date asc")->result_array();
		return $res;
	}

	public function get_quotation_list($year_value, $sales_person) {

		$where_string = "stage IN ('publish','proforma')";
		if($year_value != 'All') {

			$where_string .= " AND YEAR(entered_on) = '".$year_value."'";
		}
		if($sales_person != 'All') {

			$where_string .= " AND assigned_to = ".$sales_person;
		}
		$res = $this->db->query("
							SELECT MONTHNAME(entered_on) month, 
							count('*') count, 
							count(IF(status = 'won', 1, NULL)) won,
							count(IF(status = 'open', 1, NULL)) open,
							count(IF(status = 'closed', 1, NULL)) closed
							FROM quotation_mst 
							WHERE {$where_string} 
							group by month 
							order by entered_on ASC 
						")->result_array();
		// echo $this->db->last_query(),"<hr>";die;
		return $res;
	}

	// public function get_world_map_data() {

		// 	$this->db->select('clients.country, country.country_name, country.country_global_map_value, sum(grand_total) as total');
		// 	$this->db->join('clients', 'clients.client_id = invoice_mst.company_id', 'LEFT');
		// 	$this->db->join('country', 'country.country_id = clients.country', 'LEFT');
		// 	$this->db->where(array('invoice_mst.is_deleted' => NULL, 'clients.country !=' => '', 'country.country_name !=' => '', 'grand_total >' => 0));
		// 	$this->db->group_by('clients.country');
		// 	$res =  $this->db->get('invoice_mst')->result_array();
		// 	return $res;

	// }

	public function get_world_map_data() {

		$this->db->select('customer_mst.country_id, country_mst.name, country_mst.country_global_map_value, sum(grand_total) as total');
		$this->db->join('customer_mst', 'customer_mst.id = invoice_mst.company_id', 'LEFT');
		$this->db->join('country_mst', 'country_mst.id = customer_mst.country_id', 'LEFT');
		$this->db->where(array('invoice_mst.is_deleted' => NULL, 'customer_mst.country_id !=' => '', 'country_mst.name !=' => '', 'grand_total >' => 0));
		$this->db->group_by('customer_mst.country_id');
		$res =  $this->db->get('invoice_mst')->result_array();
		return $res;

	}

	public function get_desktrack_highchart_data($where_array, $user_id_array) {

		$this->db->select('sum(hour(machine_time.desktopTime)) as total_hour, minute(machine_time.desktopTime) as total_minute, machine_time.fullname, users.user_id');
		$this->db->join('machine_time', 'machine_time.desktrack_userid = users.desktrack_user_id', 'left');
		$this->db->where_in('users.user_id', $user_id_array);

		// $this->db->where(array('machine_time.date >=' => $start_date));
		// if(!empty($end_date)) {

		// 	$this->db->where(array('machine_time.date <' => $end_date));
		// }
		$this->db->where($where_array);
		$this->db->order_by('total_hour', 'DESC');
		$this->db->group_by('machine_time.master_id');
		$res = $this->db->get('users')->result_array();
		// echo $this->db->last_query(),"<hr>";
		// echo "<pre>";print_r($res);echo"</pre><hr>";exit;
		return $res;
	}

	public function get_sales_user_name($admin = false) {

		$where = "status = 1 and role = 5 and user_id Not IN (61, 125)";
		if($admin) {

			$where = "status = 1 and user_id Not IN (61, 125) and (role = 5  OR role = 16)";
		}
		return $this->db->get_where('users', $where)->result_array();
	}

	public function get_daily_report_highchart_data($where_array, $user_id_array, $daily_report_type) {

		$select_string = 'users.name, users.user_id, sum(daily_task_update.ca) ca, sum(daily_task_update.cc) cc, sum(daily_task_update.wa) wa, sum(daily_task_update.email_sent) email_sent, sum(daily_task_update.email_received) email_received';
		if($daily_report_type != 'all') {

			switch ($daily_report_type) {
				case 'ca':
					$select_string = 'users.name, users.user_id, sum(daily_task_update.ca) ca';
					break;

				case 'cc':
					$select_string = 'users.name, users.user_id, sum(daily_task_update.cc) cc';
					break;
					
				case 'wa':
					$select_string = 'users.name, users.user_id, sum(daily_task_update.wa) wa';
					break;
				
				case 'email_sent':
					$select_string = 'users.name, users.user_id, sum(daily_task_update.email_sent) email_sent';
					break;

				case 'email_received':
					$select_string = 'users.name, users.user_id, sum(daily_task_update.email_received) email_received';
					break;		

				default:
					break;
			}
		}
		$this->db->select($select_string);
		$this->db->join('daily_task_update', 'daily_task_update.user_id=users.user_id', 'LEFT');
		$this->db->where_in('users.user_id', $user_id_array);
		$this->db->where($where_array);
		// $this->db->where(array('daily_task_update.date >= ' => $start_date, 'daily_task_update.date < ' => $end_date));
		$this->db->group_by('daily_task_update.user_id');
		$res = $this->db->get('users')->result_array();
		// echo $this->db->last_query(),"<hr>";
		// echo "<pre>";print_r($res);echo"</pre><hr>";exit;
		return $res;
	}

	public function get_follow_list_pending_data_for_highchart($where_array) {

		$this->db->where($where_array);
		$this->db->where("quote_no != '' and quote_no is not null");
		$this->db->where("status", "open");
		// $this->db->where('m.assigned_to', $assigned_to);
		$this->db->order_by('followup_date', 'DESC');
		return $this->db->get('quotation_mst')->result_array();
	}

	public function get_year_list($call_type= 'invoice') {

		if($call_type == 'invoice') {

			$res = $this->db->query("SELECT YEAR(invoice_date) year FROM invoice_mst WHERE YEAR(invoice_date) != '1970' group by year order by invoice_date DESC")->result_array();
		} elseif ($call_type == 'quotation') {

			$res = $this->db->query("SELECT YEAR(entered_on) year FROM quotation_mst WHERE YEAR(entered_on) != '1970' group by year order by entered_on DESC")->result_array();
		}
		// echo $this->db->last_query(),"<hr>";
		return $res;
	}


	public function get_month_wise_proforma_list() {

		$res = $this->db->query("SELECT YEAR(confirmed_on) year FROM quotation_mst WHERE YEAR(confirmed_on) != '1970' group by year order by confirmed_on DESC")->result_array();
		// echo $this->db->last_query(),"<hr>";exit;
		return $res;
	}

	public function get_quotation_list_data($where) {
		$this->db->select('
				assigned_to sales_person_id, count(assigned_to) count,
				count(IF(status = "closed", 1, NULL)) closed,
				count(IF(status = "open", 1, NULL)) open,
			    count(IF(status = "won", 1, NULL)) won');
		// $this->db->where(implode(' AND ', $client_where), null, false);
		$this->db->where($where, null, false);
		$this->db->order_by('count','desc');
		$this->db->group_by('assigned_to');
		$result =  $this->db->get('quotation_mst')->result_array();
		// echo $this->db->last_query(),"<hr>";
		// echo "<pre>";print_r($result);echo"</pre><hr>";exit;
		return $result;
	}

	public function get_all_data($select, $where_array, $table_name, $result_type = 'result_array') {

		$this->db->select($select);
		$this->db->where($where_array);
		return $this->db->get($table_name)->$result_type();		
	}

	public function get_task_data($task_status, $date, $assigned_to){

		$this->db->where('entered_on >', $date);
		$this->db->where('entered_on <=', date('Y-m-d 23:59:59'));
		$this->db->where("status", $task_status);
		// $this->db->where('m.assigned_to', $assigned_to);
		$this->db->order_by('deadline', 'DESC');
		$res = $this->db->get('tasks')->result_array();
		// echo $this->db->last_query(),"<hr>";
		return $res;
	}	

	// public function get_sales_person_lead_assign_data($client_where, $lead_where) {

		// 	// echo "<pre>";print_r($client_where);echo"</pre><hr>";exit;
		// 	$this->db->select('
		// 			clients.assigned_to sales_person_id, count(clients.assigned_to) count,
		// 			count(IF(clients.lead_stage = 6, 1, NULL)) stage_6,
		// 			count(IF(clients.lead_stage = 5, 1, NULL)) stage_5,
		// 			count(IF(clients.lead_stage = 4, 1, NULL)) stage_4,
		// 		    count(IF(clients.lead_stage = 3, 1, NULL)) stage_3,
		// 		    count(IF(clients.lead_stage = 2, 1, NULL)) stage_2,
		// 		    count(IF(clients.lead_stage = 1, 1, NULL)) stage_1,
		// 		    count(IF(clients.lead_stage = 0, 1, NULL)) stage_0,
		// 		    count(IF(clients.lead_stage is NULL, 1, NULL)) blank_stage');
		// 	$this->db->where(implode(' AND ', $client_where), null, false);
		// 	$this->db->group_by('clients.assigned_to');
		// 	$client_details =  $this->db->get('clients')->result_array();
		// 	// echo $this->db->last_query(),"<hr>";
		// 	$this->db2->select('lead_mst.assigned_to sales_person_id, count(lead_mst.assigned_to) count,
		// 			count(IF(lead_mst.lead_stage = 6, 1, NULL)) stage_6,
		// 			count(IF(lead_mst.lead_stage = 5, 1, NULL)) stage_5,
		// 			count(IF(lead_mst.lead_stage = 4, 1, NULL)) stage_4,
		// 		    count(IF(lead_mst.lead_stage = 3, 1, NULL)) stage_3,
		// 		    count(IF(lead_mst.lead_stage = 2, 1, NULL)) stage_2,
		// 		    count(IF(lead_mst.lead_stage = 1, 1, NULL)) stage_1,
		// 		    count(IF(lead_mst.lead_stage = 0, 1, NULL)) stage_0,
		// 		    count(IF(lead_mst.lead_stage is NULL, 1, NULL)) blank_stage');
		// 	$this->db2->where(implode(' AND ', $lead_where), null, false);
		// 	$this->db2->group_by('lead_mst.assigned_to');
		// 	$lead_mst_details =  $this->db2->get('lead_mst')->result_array();
		// 	// echo $this->db2->last_query(),"<hr>";
		// 	// echo "<pre>";print_r(array('client_leads'=>$client_details, 'lead_mst_leads'=>$lead_mst_details));echo"</pre><hr>";exit;
		// 	return array('client_leads'=>$client_details, 'lead_mst_leads'=>$lead_mst_details);
	// }

	public function get_sales_person_lead_assign_data($client_where) {

		// echo "<pre>";print_r($client_where);echo"</pre><hr>";exit;
		$this->db->select('
				customer_mst.assigned_to sales_person_id, count(customer_mst.assigned_to) count,
				count(IF(customer_mst.lead_stage = 6, 1, NULL)) stage_6,
				count(IF(customer_mst.lead_stage = 5, 1, NULL)) stage_5,
				count(IF(customer_mst.lead_stage = 4, 1, NULL)) stage_4,
			    count(IF(customer_mst.lead_stage = 3, 1, NULL)) stage_3,
			    count(IF(customer_mst.lead_stage = 2, 1, NULL)) stage_2,
			    count(IF(customer_mst.lead_stage = 1, 1, NULL)) stage_1,
			    count(IF(customer_mst.lead_stage = 0, 1, NULL)) stage_0,
			    count(IF(customer_mst.lead_stage is NULL, 1, NULL)) blank_stage');
		$this->db->where(implode(' AND ', $client_where), null, false);
		$this->db->group_by('customer_mst.assigned_to');
		$client_details =  $this->db->get('customer_mst')->result_array();
		// echo $this->db->last_query(),"<hr>";
		// $this->db2->select('lead_mst.assigned_to sales_person_id, count(lead_mst.assigned_to) count,
		// 		count(IF(lead_mst.lead_stage = 6, 1, NULL)) stage_6,
		// 		count(IF(lead_mst.lead_stage = 5, 1, NULL)) stage_5,
		// 		count(IF(lead_mst.lead_stage = 4, 1, NULL)) stage_4,
		// 	    count(IF(lead_mst.lead_stage = 3, 1, NULL)) stage_3,
		// 	    count(IF(lead_mst.lead_stage = 2, 1, NULL)) stage_2,
		// 	    count(IF(lead_mst.lead_stage = 1, 1, NULL)) stage_1,
		// 	    count(IF(lead_mst.lead_stage = 0, 1, NULL)) stage_0,
		// 	    count(IF(lead_mst.lead_stage is NULL, 1, NULL)) blank_stage');
		// $this->db2->where(implode(' AND ', $lead_where), null, false);
		// $this->db2->group_by('lead_mst.assigned_to');
		// $lead_mst_details =  $this->db2->get('lead_mst')->result_array();
		// echo $this->db2->last_query(),"<hr>";
		// echo "<pre>";print_r(array('client_leads'=>$client_details, 'lead_mst_leads'=>$lead_mst_details));echo"</pre><hr>";exit;
		return array('client_leads'=>$client_details);
	}

	public function get_quotation_proforma_highchart_list_data($year_value, $sales_person) {

		$where_string = "confirmed_on IS NOT NULL AND proforma_no IS NOT NULL AND status = 'won' AND  assigned_to != ''";
		if($year_value != 'All') {

			$where_string .= " AND YEAR(confirmed_on) = '".$year_value."'";
		}
		if($sales_person != 'All') {

			$where_string .= " AND assigned_to = ".$sales_person;
		}
		$this->db->select('quotation_mst_id, assigned_to, grand_total sales_amount, currency, MONTH(confirmed_on) month, YEAR(confirmed_on) year');
		$this->db->where($where_string, null, false);
		$result =  $this->db->get('quotation_mst')->result_array();
		return $result;
	}

	public function get_quotation_proforma_highchart_data($where) {
		$this->db->select('quotation_mst_id, assigned_to, grand_total sales_amount, currency, month(confirmed_on) month, confirmed_on');
		$this->db->where($where, null, false);
		$result =  $this->db->get('quotation_mst')->result_array();
		// echo $this->db->last_query(),"<hr>";
		// echo "<pre>";print_r($result);echo"</pre><hr>";exit;
		return $result;
	}

	public function get_organization_chart_info() {

		$this->db->select('*');
		$this->db->where_not_in('reporting_level', array(0, 1));
		$this->db->where(array('status'=> 'Active'));
		$this->db->order_by('reporting_level', 'asc');
		$res = $this->db->get('reporting_manager')->result_array();
		return $res;
	}

	public function get_reporting_manager_data($where){
		$this->db->select('reporting_manager.reporting_id,reporting_manager.people_id,users.name,users.role,role.role_name,users.username,users.password');
		$this->db->join('users', 'users.user_id=reporting_manager.people_id', 'LEFT');
		$this->db->join('role', 'role.role_id=users.role', 'LEFT');
		$this->db->where($where);
		$res = $this->db->get('reporting_manager')->result_array();
		return $res;
	}

	public function get_level_2_org_charts_details() {


		$this->db->select('organization_chart.id, organization_chart.user_id, organization_chart.reporting_manager, organization_chart.user_reporting_level, organization_chart.user_reporting_level_2_order, organization_chart.user_reporting_level_3_order, organization_chart.user_reporting_level_4_order, organization_chart.user_designation, users.name, people_information.profile_pic_file_path, organization_chart.user_department, users.username, users.password');
		$this->db->where(array('organization_chart.user_reporting_level'=>2, 'users.status'=> 1, 'organization_chart.status'=>'Active'));
		// $this->db->where(array('organization_chart.user_reporting_level'=>2, 'organization_chart.user_department'=>'Procurement', 'users.status'=> 1, 'people_information.status'=>'Active'));
		$this->db->join('users', 'users.user_id = organization_chart.user_id', 'left');
		$this->db->join('people_information', 'people_information.user_id = organization_chart.user_id', 'right');
		$this->db->order_by('user_reporting_level_2_order', 'asc');
		$res = $this->db->get('organization_chart')->result_array();
			
		return $res;
	}

	public function get_org_charts_details($level, $reporting_id) {

		$this->db->select('organization_chart.id, organization_chart.user_id, organization_chart.reporting_manager, organization_chart.user_reporting_level, organization_chart.user_reporting_level_2_order, organization_chart.user_reporting_level_3_order, organization_chart.user_reporting_level_4_order, organization_chart.user_designation, users.name, people_information.profile_pic_file_path, organization_chart.user_department, users.username, users.password');
		$this->db->where(array('organization_chart.user_reporting_level'=>$level, 'organization_chart.reporting_manager'=>$reporting_id, 'users.status'=> 1, 'organization_chart.status'=>'Active'));
		$this->db->order_by('user_reporting_level_'.$level.'_order', 'asc');
		$this->db->join('users', 'users.user_id = organization_chart.user_id', 'left');
		$this->db->join('people_information', 'people_information.user_id = organization_chart.user_id', 'left');
		$res = $this->db->get('organization_chart')->result_array();
		// echo $this->db->last_query(),"<hr>";die('debug');
		return $res;
	}

	public function get_dynamic_data_sales_db($select_string = '*', $where_array, $table_name, $return_type = 'result_array', $limit = 0, $offset = 0) {

		$this->db->select($select_string);
		$this->db->where($where_array);
		return $this->db->get($table_name, $limit, $offset)->$return_type();
	}

	public function update_data_sales_db($table_name, $update_data, $where_array, $update_type = 'single_update'){
		
		if($update_type == 'single_update') {

			return $this->db->update($table_name, $update_data, $where_array);
		} else if ($update_type == 'batch') {

			$this->db->update_batch($table_name, $update_data, $where_array);	
		}
	}

	public function next_order_data($reporting_level, $level_order_start_number){

		$this->db->select('*');
		$this->db->where(array('status'=>'Active', 'user_reporting_level'=>3, 'user_reporting_level_'.$reporting_level.'_order >'=>6));
		$this->db->order_by('user_reporting_level','asc');
		return $this->db->get('organization_chart')->result_array();
	}

	public function get_assign_person_list() {

		$return_array = $return =  array();
		if($this->session->userdata('role') == 1) {
			$return_array = $this->db->get_where('organization_chart', array('status'=>'Active'))->result_array();
		} else {
			$this->db->where(array('user_id'=>$this->session->userdata('user_id')));	
			$this->db->or_where(array('reporting_manager'=>$this->session->userdata('user_id')));	
			// $this->db->where(array('user_id'=>33));	
			// $this->db->or_where(array('reporting_manager'=>33));	
			$return_array = $this->db->get_where('organization_chart', array('status'=>'Active'))->result_array();
			// echo "<pre>";print_r($return_array);echo"</pre><hr>";
			foreach ($return_array as $value) {
				
				$level_2_data = $this->db->get_where('organization_chart', array('status'=>'Active', 'reporting_manager'=> $value['user_id']))->result_array();
				foreach ($level_2_data as $single_level_2_data) {
					$return_array[] = $single_level_2_data;
				}				
			}
			// echo "<pre>";print_r(array_merge($return_array, $return));echo"</pre><hr>";exit;
		}
		return $return_array;
	}

	public function rfq_status_highchart_data($rfq_where_string, $quotation_where_string){

		$this->db->select('rfq_date as date, rfq_status as status, DATE_FORMAT(rfq_date,"%U") as week');
		$this->db->where($rfq_where_string, null, false);
		$this->db->order_by('rfq_date', 'asc');
		$data = $this->db->get('rfq_mst')->result_array();
		
		$this->db->select('quotation_mst.confirmed_on as date, quotation_mst.stage as status, DATE_FORMAT(quotation_mst.confirmed_on,"%U") as week');
		$this->db->where($quotation_where_string, null, false);
		$this->db->join('rfq_mst', 'rfq_mst.rfq_mst_id = quotation_mst.rfq_id', 'left');
		$this->db->order_by('quotation_mst.confirmed_on', 'asc');
		$data2 = $this->db->get('quotation_mst')->result_array();
	 	return array_merge($data, $data2);
	}

	public function get_pending_count($quotation_mst_id){

		$res = $this->db->query("SELECT sum(quotation_dtl.unit_price * (quotation_dtl.quantity - quotation_dtl.production_quantity_done)) as total 
		FROM quotation_dtl 
		WHERE quotation_mst_id = {$quotation_mst_id}
		group by quotation_mst_id")->row_array();

		return (!empty($res)) ? $res['total'] : 0;
	}

	public function get_other_count($quotation_mst_id, $status){

		$res = $this->db->query("SELECT sum(if(production_quantity_done = 0 ,quotation_dtl.row_price, (unit_price* production_quantity_done))) as total 
		FROM quotation_dtl 
		WHERE quotation_mst_id = {$quotation_mst_id} AND production_status = '{$status}'
		group by quotation_mst_id")->row_array();

		return (!empty($res)) ? $res['total'] : 0;
	}
	
	public function get_all_count($quotation_mst_id, $column_name){

		$res = $this->db->query("SELECT sum((unit_price * {$column_name})) as total 
		FROM quotation_dtl 
		WHERE quotation_mst_id = {$quotation_mst_id} AND {$column_name} > 0
		group by quotation_mst_id")->row_array();

		return (!empty($res)) ? $res['total'] : 0;
	}
	public function get_other_count_old($quotation_mst_id, $status){

		$res = $this->db->query("SELECT sum(if(production_quantity_done = 0 ,quotation_dtl.row_price, (unit_price* production_quantity_done))) as total 
		FROM quotation_dtl 
		WHERE quotation_mst_id = {$quotation_mst_id} AND production_status = '{$status}'
		group by quotation_mst_id")->row_array();

		return (!empty($res)) ? $res['total'] : 0;
	}

	public function get_all_count_old($quotation_mst_id, $status){

		$res = $this->db->query("SELECT sum(if(production_quantity_done = 0 ,quotation_dtl.row_price, (unit_price* production_quantity_done))) as total 
		FROM quotation_dtl 
		WHERE quotation_mst_id = {$quotation_mst_id} AND {$status}_count > 0
		group by quotation_mst_id")->row_array();

		return (!empty($res)) ? $res['total'] : 0;
	}

	public function get_production_listingz_data($where_array, $order_by_array, $limit, $offset) {

		$this->db->select('
		SQL_CALC_FOUND_ROWS
		quotation_mst.quotation_mst_id,
		quotation_mst.net_total,
		quotation_mst.grand_total,
		quotation_mst.currency,
		quotation_mst.freight,
		quotation_mst.bank_charges,
		quotation_mst.gst,
		production_process_information.semi_ready_count,
		production_process_information.ready_for_dispatch_count, 
		production_process_information.dispatch_count, 
		production_process_information.on_hold_count,production_process_information.query_count, 
		production_process_information.mtt_rfd_count, 
		production_process_information.mtt_count, 
		production_process_information.cancel_count,
		(IF(quotation_mst.discount > 0 && quotation_mst.discount_type = "value" , quotation_mst.discount, (quotation_mst.net_total*quotation_mst.discount)/ 100)) as discount', FALSE);
		$this->db->join('production_process_information', 'production_process_information.quotation_mst_id = quotation_mst.quotation_mst_id', 'left');
		$this->db->where($where_array, null, false);
		$this->db->order_by('production_process_information.id', 'desc');
		$return_array['production_list'] = $this->db->get('quotation_mst', $limit, $offset)->result_array();
		// echo $this->db->last_query(),"<hr>";die('debug');
		$return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows;")->row_array();
		return $return_array;
	}

	public function get_org_form_name(){

		$get_org_user_ids = $this->db->select('user_id')->where(array('status'=>'Active'))->get('user_organization_chart')->result_array();
		$this->db->select('user_id as value, name, "" as selected');
		$this->db->where(array('status'=>'1'));
		if(!empty($get_org_user_ids)){

			$this->db->where_not_in('user_id', array_column($get_org_user_ids, 'user_id'));
		}
		return $this->db->get('users')->result_array();
	}

	public function get_level_2_detail_of_org_graph($reporting_manager_id){

		$this->db->select('user_organization_chart.user_id as tab_name, user_organization_chart.name, user_organization_chart.designation,  "background-color: rgba(40, 42, 60, 0.1);" as style, users.username, users.password, users.role, user_organization_chart.reporting_manager_id, user_organization_chart.order_id, "" as url, "" as count');
		$this->db->join('users', 'users.user_id = user_organization_chart.user_id', 'left');
		if($this->session->userdata('role') == 1){

		}else{

			// $current_session_id = $this->session->userdata('user_id');
			// if($reporting_manager_id == 2){

			// 	$this->db->where("user_organization_chart.status = 'Active' AND (user_organization_chart.user_id = {$current_session_id})", null , false);
			// }elseif($reporting_manager_id == 3){

			// 	$this->db->where("user_organization_chart.status = 'Active' AND (user_organization_chart.reporting_manager_id = {$current_session_id})", null , false);
			// }
		}
		$this->db->where(array('user_organization_chart.status'=> 'Active', 'user_organization_chart.reporting_manager_id'=>$reporting_manager_id));
		$res = $this->db->get('user_organization_chart')->result_array();

		return $res;
	}

	public function get_count_production_status($type, $where_string){

		$this->db->select("
		quotation_mst.quotation_mst_id,
		quotation_mst.net_total,
		quotation_mst.currency,
		quotation_dtl.unit_price,
		sum(((quotation_dtl.quantity - (quotation_dtl.semi_ready_count + quotation_dtl.ready_to_dispatch_order_count + quotation_dtl.dispatch_order_count + quotation_dtl.on_hold_order_count + quotation_dtl.mtt_count + quotation_dtl.import_count + quotation_dtl.query_count + quotation_dtl.mtt_rfd_count + quotation_dtl.order_cancelled_count))) * quotation_dtl.unit_price) as pending_count,
		sum(quotation_dtl.semi_ready_count * quotation_dtl.unit_price) as semi_ready_count,
		sum(if(production_process_information.production_status = 'semi_ready', quotation_dtl.row_price , (quotation_dtl.semi_ready_count * quotation_dtl.unit_price))) as semi_ready_count_2,
		sum(quotation_dtl.ready_to_dispatch_order_count * quotation_dtl.unit_price) as rfd_count,
		sum(if(production_process_information.production_status = 'ready_for_dispatch', quotation_dtl.row_price , (quotation_dtl.ready_to_dispatch_order_count * quotation_dtl.unit_price))) as rfd_count_2,
		sum(quotation_dtl.dispatch_order_count * quotation_dtl.unit_price) as dispatch_count,
		sum(if(production_process_information.production_status = 'dispatched', quotation_dtl.row_price , (quotation_dtl.dispatch_order_count * quotation_dtl.unit_price))) as dispatch_count_2,
		sum(quotation_dtl.on_hold_order_count * quotation_dtl.unit_price) as hold_count,
		sum(if(production_process_information.production_status = 'on_hold', quotation_dtl.row_price , (quotation_dtl.on_hold_order_count * quotation_dtl.unit_price))) as hold_count_2,
		sum(quotation_dtl.mtt_count * quotation_dtl.unit_price) as mtt_count,
		sum(if(production_process_information.production_status IN ('merchant_trade','mtt'), quotation_dtl.row_price , (quotation_dtl.mtt_count * quotation_dtl.unit_price))) as mtt_count_2,
		sum(quotation_dtl.import_count * quotation_dtl.unit_price) as import_count,
		sum(if(production_process_information.production_status = 'import', quotation_dtl.row_price , (quotation_dtl.import_count * quotation_dtl.unit_price))) as import_count_2,
		sum(quotation_dtl.query_count * quotation_dtl.unit_price) as query_count,
		sum(if(production_process_information.production_status = 'query', quotation_dtl.row_price , (quotation_dtl.query_count * quotation_dtl.unit_price))) as query_count_2,
		sum(quotation_dtl.mtt_rfd_count * quotation_dtl.unit_price) as mtt_rfd_count,
		sum(if(production_process_information.production_status IN ('mtt_rfd', 'in_transit_import', 'in_transit_mtt'), quotation_dtl.row_price , (quotation_dtl.mtt_rfd_count * quotation_dtl.unit_price))) as mtt_rfd_count_2,
		sum(quotation_dtl.order_cancelled_count * quotation_dtl.unit_price) as cancel_count,
		sum(if(production_process_information.production_status = 'order_cancelled', quotation_dtl.row_price , (quotation_dtl.order_cancelled_count * quotation_dtl.unit_price))) as cancel_count_2,
		");
		$this->db->where("quotation_mst.stage = 'proforma' AND quotation_mst.status = 'Won' AND quotation_mst.work_order_no != 0 AND (production_process_information.status is NULL or production_process_information.status = 'Active') {$where_string}");
		if(!empty($type)){
			
			$this->db->where("production_process_information.type = '{$type}'");
		}
		// $this->db->where_in("production_process_information.work_order_no", array(2696,2580,2350,2309,2236,2142,2041,1780,1544,1704,1705));
		$this->db->join('production_process_information', 'production_process_information.quotation_mst_id = quotation_mst.quotation_mst_id', 'inner');
		$this->db->join('quotation_dtl', 'quotation_dtl.quotation_mst_id = quotation_mst.quotation_mst_id', 'inner');
		$this->db->group_by('quotation_mst.quotation_mst_id');
		$data = $this->db->get('quotation_mst')->result_array();
		// echo $this->db->last_query(),"<hr>";die;
		return $data;
	}

	public function get_sales_person($select= '*', $sales_person_type= 'all') {

		$this->db->select($select);
		$role_array = array(5, 16);
		if($sales_person_type == 'sales_admin') {

			$role_array = array(16);
		} else if ($sales_person_type == 'sales_people'){

			$role_array = array(5);
		}
		$this->db->where_in('role', $role_array);
		return $this->db->get_where('users', array('status'=>1, 'user_id !='=>'125'))->result_array();
	}
}?>