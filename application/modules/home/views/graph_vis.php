<!DOCTYPE html>
<html>
  <head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vis/4.21.0/vis.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/vis/4.21.0/vis.min.css" />
    <style>
      #chart {
        width: 1000px;
        height: 1000px;
        border: 1px solid lightgray;
      }
    </style>
  </head>
  <body>
    <div id="chart"></div>
    <script>
      var nodes = new vis.DataSet([
        { id: 1, label: 'CEO' },
        { id: 2, label: 'COO' },
        { id: 3, label: 'HR' },
        { id: 4, label: 'Operations' },
        { id: 5, label: 'CFO' },
        { id: 6, label: 'Finance' },
        { id: 7, label: 'Accounting' },
        { id: 8, label: 'CMO' },
        { id: 9, label: 'Marketing' },
        { id: 10, label: 'Sales' }
      ]);

      var edges = new vis.DataSet([
        { from: 1, to: 2 },
        { from: 1, to: 5 },
        { from: 1, to: 8 },
        { from: 2, to: 3 },
        { from: 2, to: 4 },
        { from: 5, to: 6 },
        { from: 5, to: 7 },
        { from: 8, to: 9 },
        { from: 8, to: 10 }
      ]);

      var container = document.getElementById('chart');
      var data = {
        nodes: nodes,
        edges: edges
      };
      var options = {
        layout: {
          hierarchical: {
            direction: "UD",
            sortMethod: "directed"
          }
        },
        edges: {
          arrows: {
            to: { enabled: true, scaleFactor: 0.5 }
          }
        }
      };

      var network = new vis.Network(container, data, options);
    </script>
  </body>
</html>
