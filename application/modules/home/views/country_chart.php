<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<!--Begin::Section-->
	<div class="row">
		<div class="col-xl-12">

			<!--Begin::Portlet-->
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Quotation Ready List
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						<ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
							<li class="nav-item">
								<a class="nav-link change_quotation_ready_date" filter-date="current" data-toggle="tab" href="#kt_widget11_tab1_content" role="tab">
									Today
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link change_quotation_ready_date" filter-date="week" data-toggle="tab" href="#kt_widget11_tab1_content" role="tab" >
									Week 
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link change_quotation_ready_date" filter-date="month" data-toggle="tab" href="#kt_widget11_tab1_content" role="tab">
									Month
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link change_quotation_ready_date active" filter-date="all" data-toggle="tab" href="#kt_widget11_tab2_content" role="tab">
									All Time
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="kt-portlet__body">
					<!--Begin::Timeline 3 -->	
					<div class="kt-scroll" data-scroll="true" style="height: 400px">
						<div id="world_map_highchart"></div>
					</div>

					<!--End::Timeline 3 -->
				</div>
			</div>

			<!--End::Portlet-->
		</div>
	</div>
</div>