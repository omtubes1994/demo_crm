<div class="kt-timeline-v2__items  kt-padding-top-25 kt-padding-bottom-30">
	<?php if(!empty($quotation_list)) {?>
	<?php foreach($quotation_list as $quotation_list_date => $quotation_details) {?>	
		<!-- <div class="kt-timeline-v2__item"> -->
			<span class="kt-timeline-v2__item-time kt-font-bolder" style="width: 100%"><?php echo date('F, j', strtotime($quotation_list_date));?></span>
			<!-- <div class="kt-timeline-v2__item-cricle"> -->
				<!-- <span class="kt-timeline-v2__item-time"><?php echo $quotation_list_date; ?></span> -->
			<!-- </div> -->
			<!-- <div class="kt-timeline-v2__item-text kt-padding-top-5">
			</div> -->
		<!-- </div> -->
		<?php foreach($quotation_details as $quotation_single_details) {?>
			<div class="kt-timeline-v2__item">
				<span class="kt-timeline-v2__item-time"><?php echo date('h:m', strtotime($quotation_single_details['confirmed_on']));?></span>
				<div class="kt-timeline-v2__item-cricle">
					<i class="fa fa-genderless <?php echo $image_not_found_array[$image_not_found_array_key%3]; ?>"></i>
				</div>
				<div class="kt-timeline-v2__item-text kt-padding-top-5  kt-font-bolder kt-font-dark">
					Quotation <?php echo $quotation_single_details['quote_no']; ?> for <a href="#" class="kt-link kt-link--brand kt-font-bolder"><?php echo $quotation_single_details['client_name']; ?></a> against <?php echo $quotation_single_details['reference']; ?> is ready for submission.
				</div>
			</div>		
		<?php $image_not_found_array_key++;}?>	
	<?php }?>	
	<?php } else {?>	
		<div class="kt-timeline-v2__item">
			<span class="kt-timeline-v2__item-time"></span>
			<div class="kt-timeline-v2__item-cricle">
				<i class="fa fa-genderless"></i>
			</div>
			<div class="kt-timeline-v2__item-text kt-padding-top-5  kt-font-bolder kt-font-dark">
				No Quotation Found !!!.
			</div>
		</div>
	<?php }?>	
</div>