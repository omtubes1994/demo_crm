<div class="kanban-item dark-light" style="cursor: pointer; width:100%; padding: 10px 5px; <?php echo $style; ?>">
    <div class="kt-kanban__badge" style="justify-content: center; height: 150px;">
        <div class="kt-kanban__content">
            <div class="kt-kanban__title kt-font-xl view_details"  data-container="body" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title='<?php echo $count;?>' style="height: 30px;" name = "<?php echo $tab_name; ?>" url="<?php echo $url; ?>" reporting_manager_id = "<?php echo $reporting_manager_id;?>"><?php echo $name; ?></div>
            <div class="kt-kanban__title kt-font-boldest" style="color:#343a40; height: 60px;"><?php echo $designation; ?></div>
            <hr style="margin:0px; border-top: 1px solid #343a40;">
            <div class="kt-section__content kt-section__content--solid" style="margin-top:0.5rem;">
                <?php if($user_login) {?>
                <a class="btn btn-outline-dark btn-elevate btn-icon login_as_another_user" data-toggle="kt-tooltip" data-placement="left" title="" data-original-title="Login" style="height:2rem; width:2rem;" user_name="<?php echo $username; ?>" password="<?php echo $password; ?>">
                    <i class="flaticon2-user"></i>
                </a>
                &nbsp;
                <?php } ?>
                <?php if($user_login && in_array($role, array(1, 5, 16, 6, 8))) {?>
                <a href="<?php echo base_url('common/view_user_data/'),$tab_name;?>" target="_blank" class="btn btn-outline-dark btn-elevate btn-icon"data-toggle="kt-tooltip" data-placement="left" title="" data-original-title="User Assign Data" style="height:2rem; width:2rem;">
                    <i class="flaticon2-list-1"></i>
                </a>
                &nbsp;
                <?php if(in_array($role, array(1, 5, 16))) {?>
                    <a href="<?php echo base_url('common/daily_report');?>" target="_blank" class="btn btn-outline-dark btn-elevate btn-icon"data-toggle="kt-tooltip" data-placement="left" title="" data-original-title="Daily Report" style="height:2rem; width:2rem;">
                        <i class="flaticon2-calendar"></i>
                    </a>
                    &nbsp;
                <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>