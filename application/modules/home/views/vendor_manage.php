<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row" data-sticky-container>
		<div class="col-lg-3 col-xl-2">
			<div class="kt-portlet sticky" data-sticky="true" data-margin-top="100px" data-sticky-for="1023" data-sticky-class="kt-sticky">
				<div class="kt-portlet__body kt-portlet__body--fit">
					<ul class="kt-nav kt-nav--bold kt-nav--md-space kt-nav--v3 kt-margin-t-20 kt-margin-b-20 nav nav-tabs" role="tablist">
						<li class="kt-nav__item change_manage_tab <?php echo ($call_type == 'product')?'active': ''; ?>" tab-name="product">
							<a class="kt-nav__link" data-toggle="tab" href="#kt_profile_tab_personal_information" role="tab">
								<span class="kt-nav__link-icon"><i class="flaticon-avatar"></i></span>
								<span class="kt-nav__link-text">Product</span>
							</a>
						</li>
						<li class="kt-nav__item change_manage_tab <?php echo ($call_type == 'material')?'active': ''; ?>" tab-name="material">
							<a class="kt-nav__link" data-toggle="tab" href="#kt_profile_tab_account_information" role="tab">
								<span class="kt-nav__link-icon"><i class="flaticon-lock"></i></span>
								<span class="kt-nav__link-text">Material</span>
							</a>
						</li>
						<li class="kt-nav__item change_manage_tab" tab-name="procurement_pdf_footer">
							<a class="kt-nav__link" data-toggle="tab" href="#kt_profile_tab_account_information" role="tab">
								<span class="kt-nav__link-icon"><i class="flaticon-settings"></i></span>
								<span class="kt-nav__link-text">Procurement Pdf Footer</span>
							</a>
						</li>
						<li class="kt-nav__item change_manage_tab <?php echo ($call_type == 'delivery')?'active': ''; ?>" tab-name="delivery">
							<a class="kt-nav__link" data-toggle="tab" href="#kt_profile_tab_account_information" role="tab">
								<span class="kt-nav__link-icon"><i class="flaticon-multimedia-2"></i></span>
								<span class="kt-nav__link-text">Delivered To</span>
							</a>
						</li>
						<li class="kt-nav__item change_manage_tab <?php echo ($call_type == 'delivery_time')?'active': ''; ?>" tab-name="delivery_time">
							<a class="kt-nav__link" data-toggle="tab" href="#kt_profile_tab_account_information" role="tab">
								<span class="kt-nav__link-icon"><i class="flaticon-coins"></i></span>
								<span class="kt-nav__link-text">Delivery Time</span>
							</a>
						</li>
						<li class="kt-nav__item change_manage_tab <?php echo ($call_type == 'payment_terms')?'active': ''; ?>" tab-name="payment_terms">
							<a class="kt-nav__link" data-toggle="tab" href="#kt_profile_tab_account_information" role="tab">
								<span class="kt-nav__link-icon"><i class="flaticon-customer"></i></span>
								<span class="kt-nav__link-text">Payment</span>
							</a>
						</li>
						<li class="kt-nav__item change_manage_tab <?php echo ($call_type == 'validity')?'active': ''; ?>" tab-name="validity">
							<a class="kt-nav__link" data-toggle="tab" href="#kt_profile_tab_account_information" role="tab">
								<span class="kt-nav__link-icon"><i class="flaticon-bag"></i></span>
								<span class="kt-nav__link-text">Validity</span>
							</a>
						</li>
						<li class="kt-nav__item change_manage_tab <?php echo ($call_type == 'currency')?'active': ''; ?>" tab-name="currency">
							<a class="kt-nav__link" data-toggle="tab" href="#kt_profile_tab_account_information" role="tab">
								<span class="kt-nav__link-icon"><i class="flaticon-tabs"></i></span>
								<span class="kt-nav__link-text">Currency</span>
							</a>
						</li>
						<li class="kt-nav__item change_manage_tab <?php echo ($call_type == 'origin_country')?'active': ''; ?>" tab-name="origin_country">
							<a class="kt-nav__link" data-toggle="tab" href="#kt_profile_tab_account_information" role="tab">
								<span class="kt-nav__link-icon"><i class="flaticon-share"></i></span>
								<span class="kt-nav__link-text">Country Of Origin</span>
							</a>
						</li>
						<li class="kt-nav__item change_manage_tab <?php echo ($call_type == 'mtc_type')?'active': ''; ?>" tab-name="mtc_type">
							<a class="kt-nav__link" data-toggle="tab" href="#kt_profile_tab_account_information" role="tab">
								<span class="kt-nav__link-icon"><i class="flaticon-interface-9"></i></span>
								<span class="kt-nav__link-text">MTC Type</span>
							</a>
						</li>
						<li class="kt-nav__item change_manage_tab <?php echo ($call_type == 'transport_mode')?'active': ''; ?>" tab-name="transport_mode">
							<a class="kt-nav__link" data-toggle="tab" href="#kt_profile_tab_account_information" role="tab">
								<span class="kt-nav__link-icon"><i class="flaticon2-paperplane"></i></span>
								<span class="kt-nav__link-text">Transport Mode</span>
							</a>
						</li>
						<li class="kt-nav__item change_manage_tab <?php echo ($call_type == 'lead_stage_reasons')?'active': ''; ?>" tab-name="lead_stage_reasons">
							<a class="kt-nav__link" data-toggle="tab" href="#kt_profile_tab_account_information" role="tab">
								<span class="kt-nav__link-icon"><i class="flaticon2-exclamation"></i></span>
								<span class="kt-nav__link-text">Lead Stage Reasons</span>
							</a>
						</li>
						<li class="kt-nav__item change_manage_tab <?php echo ($call_type == 'quotation_close_reasons')?'active': ''; ?>" tab-name="quotation_close_reasons">
							<a class="kt-nav__link" data-toggle="tab" href="#kt_profile_tab_account_information" role="tab">
								<span class="kt-nav__link-icon"><i class="flaticon-questions-circular-button"></i></span>
								<span class="kt-nav__link-text">Quotation Close Reasons</span>
							</a>
						</li>
						<li class="kt-nav__item change_manage_tab <?php echo ($call_type == 'rfq_close_reasons')?'active': ''; ?>" tab-name="rfq_close_reasons">
							<a class="kt-nav__link" data-toggle="tab" href="#kt_profile_tab_account_information" role="tab">
								<span class="kt-nav__link-icon"><i class="flaticon-questions-circular-button"></i></span>
								<span class="kt-nav__link-text">RFQ Close Reasons</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-xl-8">
			<div class="kt-portlet kt-portlet--mobile dynamic_manage_table" >
				<?php echo $this->load->view('home/dynamic_manage_table');?>
			</div>	
		</div>
	</div>	
</div>
<!--begin::Modal-->
<div class="modal fade" id="add_product" tabindex="-1" role="dialog"  aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add New Product</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form  id="product_add_form">
					<div class="form-group">
						<label for="recipient-name" class="form-control-label">Product Name:</label>
						<input type="text" class="form-control" name="name">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary add" add_name="Product" form_name="product_add_form">Add Product</button>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->
<!--begin::Modal-->
<div class="modal fade" id="add_material" tabindex="-1" role="dialog"  aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add New Material</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form id="material_add_form">
					<div class="form-group">
						<label for="recipient-name" class="form-control-label">Material Name:</label>
						<input type="text" class="form-control" name="name">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary add" add_name="Material" form_name="material_add_form">Add Material</button>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->
<!--begin::Modal-->
<div class="modal fade" id="edit_terms_and_conditions" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Update Terms And Condition</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<div class="col-lg-12 col-md-9 col-sm-12">
					<textarea class="form-control" id="terms_conditions" rows="3"></textarea>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary update_terms_conditions">Update</button>
			</div>
		</div>
	</div>
</div>
<!--end::Modal-->
<!--begin::Modal-->
<div class="modal fade" id="add_delivery_place" tabindex="-1" role="dialog"  aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add New Delivery Place</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form id="delivery_place_add_form">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="recipient-name" class="form-control-label">Delivery Name:</label>
								<input type="text" class="form-control" name="delivery_name">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="recipient-name" class="form-control-label">Transport Mode:</label>
								<select class="form-control" name="transport_id">
									<option value=""> Select Transport Mode</option>
									<?php foreach($transport_mode as $single_transport_details) {?>
										<option value="<?php echo $single_transport_details['mode_id']; ?>"><?php echo $single_transport_details['mode']; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary add" add_name="Delivery Place" form_name="delivery_place_add_form">Add</button>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->
<!--begin::Modal-->
<div class="modal fade" id="add_delivery_time" tabindex="-1" role="dialog"  aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add New Delivery Time</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form  id="delivery_time_add_form">
					<div class="form-group">
						<label for="recipient-name" class="form-control-label">Delivery Time:</label>
						<input type="text" class="form-control" name="dt_value">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary add" add_name="Delivery Time" form_name="delivery_time_add_form">Add</button>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->
<!--begin::Modal-->
<div class="modal fade" id="add_payment" tabindex="-1" role="dialog"  aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add New Payments</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form  id="payment_time_add_form">
					<div class="form-group">
						<label for="recipient-name" class="form-control-label">Payments:</label>
						<input type="text" class="form-control" name="term_value">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary add" add_name="Payments" form_name="payment_time_add_form">Add</button>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->
<!--begin::Modal-->
<div class="modal fade" id="add_validity" tabindex="-1" role="dialog"  aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add New Validity</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form  id="validity_add_form">
					<div class="form-group">
						<label for="recipient-name" class="form-control-label">Validity:</label>
						<input type="text" class="form-control" name="validity_value">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary add" add_name="Validity" form_name="validity_add_form">Add</button>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->
<!--begin::Modal-->
<div class="modal fade" id="add_currency" tabindex="-1" role="dialog"  aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add New Currency</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form  id="currency_add_form">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="recipient-name" class="form-control-label">Currency Name:</label>
								<input type="text" class="form-control" name="currency">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="recipient-name" class="form-control-label">Currency Rate:</label>
								<input type="text" class="form-control" name="currency_rate">
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary add" add_name="Currency" form_name="currency_add_form">Add</button>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->
<!--begin::Modal-->
<div class="modal fade" id="add_country_origin" tabindex="-1" role="dialog"  aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add New Country Origin</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form  id="country_origin_add_form">
					<div class="form-group">
						<label for="recipient-name" class="form-control-label">Country Origin:</label>
						<input type="text" class="form-control" name="country">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary add" add_name="Country Origin" form_name="country_origin_add_form">Add</button>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->
<!--begin::Modal-->
<div class="modal fade" id="add_mtc_type" tabindex="-1" role="dialog"  aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add New MTC Type</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form  id="mtc_type_add_form">
					<div class="form-group">
						<label for="recipient-name" class="form-control-label">MTC Type:</label>
						<input type="text" class="form-control" name="mtc_value">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary add" add_name="MTC Type" form_name="mtc_type_add_form">Add</button>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->
<!--begin::Modal-->
<div class="modal fade" id="add_transport_mode" tabindex="-1" role="dialog"  aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add New Transport Mode</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form id="transport_mode_add_form">
					<div class="form-group">
						<label for="recipient-name" class="form-control-label">Transport Mode Name:</label>
						<input type="text" class="form-control" name="mode">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary add" add_name="Transport Mode" form_name="transport_mode_add_form">Add Transport Mode</button>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->
<!--begin::Modal-->
<div class="modal fade" id="add_lead_stage_reasons" tabindex="-1" role="dialog"  aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add New Lead Stage Reasons</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form  id="lead_stage_reasons_add_form">
					<div class="form-group">
						<label for="recipient-name" class="form-control-label">Lead Stage Reasons:</label>
						<input type="text" class="form-control" name="reason">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary add" add_name="Lead Stage Reasons" form_name="lead_stage_reasons_add_form">Add</button>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->
<!--begin::Modal-->
<div class="modal fade" id="add_reason" tabindex="-1" role="dialog"  aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add Quotation close Reason</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form  id="quotation_close_reasons_add_form">
					<div class="form-group">
						<label for="recipient-name" class="form-control-label">Quotation close Reason:</label>
						<input type="text" class="form-control" name="reason">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary add" add_name="Quotation Close Reasons" form_name="quotation_close_reasons_add_form">Add</button>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->
<!--begin::Modal-->
<div class="modal fade" id="add_rfq_close_reasons" tabindex="-1" role="dialog"  aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add New Rfq close Reasons</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form  id="rfq_close_reasons_add_form">
					<div class="form-group">
						<label for="recipient-name" class="form-control-label">RFQ Close Reasons:</label>
						<input type="text" class="form-control" name="reason">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary add" add_name="RFQ Close Reasons" form_name="rfq_close_reasons_add_form">Add</button>
			</div>
		</div>
	</div>
</div>
<!--end::Modal-->