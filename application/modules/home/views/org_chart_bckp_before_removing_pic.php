<?php if(true){?>
	<?php 
		$image_not_found_array = array('success', 'danger', 'warning', 'info', 'dark', 'brand');
        $image_not_found_array_key = 0;
	?>
	<!--Begin::Section-->
	<div class="row">
		<div class="col-xl-12">

			<!--Begin::Portlet-->
			<div class="kt-portlet kt-portlet--height-fluid" style="background-color: #e4e8ee !important;">
				
				<div class="kt-portlet__body" style="padding: 0px 00px;">

					<div class="row" style="padding: 20px 0px;">
						<div class="col-xl-5"></div>
						<div class="col-xl-2" style="background-color: #fff !important;box-sizing: border-box;border-top: 0.5rem solid #fd7e14;">
							<!--begin::Widget -->
							<div class="kt-widget kt-widget--user-profile-2">
								<div class="kt-widget__head" style="margin-top: 0px;padding: 10px;">
									<div class="kt-widget__media">
										<img class="kt-widget__img kt-hidden" src="assets/media/users/100_3.jpg" alt="image">
										<div class="kt-widget__pic kt-widget__pic--brand kt-font-brand kt-font-boldest kt-hidden-">
											JM
										</div>
									</div>
									<?php  if($this->session->userdata('global_user_details')['user_name'] == 'jay') {?>
									<div class="kt-widget__info" style="width:215px;">
									<?php }else{?>
									<div class="kt-widget__info" style="width:240px;">
									<?php }?>
										<span href="javascript:void(0)" class="kt-widget__username">
											CEO
										</span>
										<span class="kt-widget__desc">
											Jay Mehta
										</span>
									</div>
									<?php  if($this->session->userdata('global_user_details')['user_name'] == 'jay') {?>
									<div class="kt-widget__info" style="width:25px; padding-left: 0px;">
										<span href="javascript:void(0)" class="kt-widget__username">
											<div class="dropdown dropdown-inline">
												<button type="button" class="btn btn-hover-brand btn-elevate-hover btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<i class="flaticon-more"></i>
												</button>
												<div class="dropdown-menu dropdown-menu-right">
													<a class="dropdown-item login_as_another_user" href="javascript:void(0)" user_name="<?php echo $this->session->userdata('global_user_details')['user_name']; ?>" password="<?php echo $this->session->userdata('global_user_details')['password']; ?>"><i class="la la-user"></i> Login</a>
													<a class="dropdown-item" href="<?php echo base_url('home/organization_chart');?>" target="_blank"><i class="la la-plus"></i>Manage Org Chart</a>
													<a class="dropdown-item" href="<?php echo base_url('common/user_assign_data');?>" target="_blank"><i class="la la-plus"></i>Manage Lead</a>
												</div>
											</div>
										</span>
										<span class="kt-widget__desc" style="padding-top: 50px;">
										</span>
									</div>
									<?php }?>
								</div>
							</div>

							<!--end::Widget -->
						</div>
						<div class="col-xl-5"></div>
					</div>
				</div>
			</div>

			<!--End::Portlet-->
		</div>
	</div>

	<!--End::Section-->
	<!--Begin::Section-->
	<div class="row" style="width: 100000px;">
		<div class="col-xl-12">

			<!--Begin::Portlet-->
			<div class="kt-portlet kt-portlet--height-fluid" style="background-color: #e4e8ee !important;">
				
				<div class="kt-portlet__body" style="padding: 0px 00px;">

					<?php if(!empty($org_chart_details['level_2'])) {?>
					<div class="row" style="padding: 20px 0px;">
						<?php foreach ($org_chart_details['level_2'] as $level_2_details) { ?>
						<div class="" style="width: <?php echo $level_2_details['width'],'px'; ?>;"></div>
						<div class="" style="width: 350px;background-color: #fff !important;box-sizing: border-box;border-top: 0.5rem solid <?php echo $level_2_details['color'];?>;">
							<!--begin::Widget -->
							<div class="kt-widget kt-widget--user-profile-2">
								<div class="kt-widget__head" style="margin-top: 0px;padding: 10px;">
									<div class="kt-widget__media" style="width: 90px;">
										<img class="kt-widget__img kt-hidden" src="assets/media/users/100_1.jpg" alt="image">
										<?php if(!empty($level_2_details['profile_pic_file_path'])) {?>

											<img class="kt-widget__img kt-hidden-" src="<?php echo "https://crm.omtubes.com/assets/hr_document/profile_pic/".$level_2_details['profile_pic_file_path'];?>" alt="image" style="max-height: 90px;">
										<?php } else {?>
										<div class="kt-widget__pic kt-widget__pic--<?php echo $image_not_found_array[$image_not_found_array_key%5];?> kt-font-<?php echo $image_not_found_array[$image_not_found_array_key%5];?> kt-font-boldest kt-hidden-">
											<?php 
												$explode_name = explode(' ', $level_2_details['name']);
												foreach ($explode_name as $single_word) {
													echo ucfirst($single_word[0]);
												}
											?>
										</div>
										<?php $image_not_found_array_key++;}?>
									</div>
									<?php  if(in_array($level_2_details['user_id'], $allow_other_person_login_array)  || ($level_2_details['username'] == $this->session->userdata('global_user_details')['user_name'] && $level_2_details['password'] == $this->session->userdata('global_user_details')['password'])) {?>
									<div class="kt-widget__info" style="width:215px;">
									<?php }else{?>
									<div class="kt-widget__info" style="width:240px;">
									<?php }?>
										<span href="javascript:void(0)" class="kt-widget__username">
											<?php echo $level_2_details['user_designation']; ?>
										</span>
										<span class="kt-widget__desc">
											<?php echo $level_2_details['name']; ?>
										</span>
									</div>
									<?php  if(in_array($level_2_details['user_id'], $allow_other_person_login_array)  || ($level_2_details['username'] == $this->session->userdata('global_user_details')['user_name'] && $level_2_details['password'] == $this->session->userdata('global_user_details')['password'])) {?>
										<div class="kt-widget__info" style="width:25px; padding-left: 0px;">
											<span href="javascript:void(0)" class="kt-widget__username">
												<div class="dropdown dropdown-inline">
													<button type="button" class="btn btn-hover-brand btn-elevate-hover btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<i class="flaticon-more"></i>
													</button>
													<div class="dropdown-menu dropdown-menu-right">
														<a class="dropdown-item login_as_another_user" href="javascript:void(0)" user_name="<?php echo $level_2_details['username']; ?>" password="<?php echo $level_2_details['password']; ?>"><i class="la la-user"></i> Login</a>
														<a class="dropdown-item" href="<?php echo base_url('home/calendar/'.$level_2_details['user_id']);?>" target="_blank"><i class="la la-plus"></i> Daily Report</a>
														<?php if(in_array($level_2_details['user_department'], array('Sales', 'Procurement'))) {?>
														<a class="dropdown-item" href="<?php echo base_url('common/view_user_data/'),$level_2_details['user_id'];?>" target="_blank"><i class="la la-plus"></i>Manage Lead</a>
														<?php } ?>
													</div>
												</div>
											</span>
											<span class="kt-widget__desc" style="padding-top: 50px;">
											</span>
										</div>
									<?php }?>
								</div>
							</div>

							<!--end::Widget -->
						</div>
						<?php } ?>
					</div>
					<?php }?>

					<?php if(!empty($org_chart_details['level_3'])) {?>
					<div class="row" style="padding: 20px 0px;">
					<?php foreach ($org_chart_details['level_3'] as $level_3_details) { ?>
						<div class="" style="width: <?php echo $level_3_details['width'],'px'; ?>;"></div>
						<?php if(!empty($level_3_details['user_designation'])) { ?>
						<div class="" style="width: 350px;background-color: #fff !important;box-sizing: border-box;border-top: 0.5rem solid <?php echo $level_3_details['color'];?>;">
							<!--begin::Widget -->
							<div class="kt-widget kt-widget--user-profile-2">
								<div class="kt-widget__head" style="margin-top: 0px;padding: 10px;">
									<div class="kt-widget__media" style="width: 90px;">
										<img class="kt-widget__img kt-hidden" src="assets/media/users/100_1.jpg" alt="image">
										<?php if(!empty($level_3_details['profile_pic_file_path'])) {?>

											<img class="kt-widget__img kt-hidden-" src="<?php echo "https://crm.omtubes.com/assets/hr_document/profile_pic/".$level_3_details['profile_pic_file_path'];?>" alt="image" style="max-height: 90px;">
										<?php } else {?>
										<div class="kt-widget__pic kt-widget__pic--<?php echo $image_not_found_array[$image_not_found_array_key%5];?> kt-font-<?php echo $image_not_found_array[$image_not_found_array_key%5];?> kt-font-boldest kt-hidden-">
											<?php 
												$explode_name = explode(' ', $level_3_details['name']);
												foreach ($explode_name as $single_word) {
													echo ucfirst($single_word[0]);
												}
											?>
										</div>
										<?php $image_not_found_array_key++;}?>
									</div>
									<?php  if(in_array($level_3_details['user_id'], $allow_other_person_login_array)  || ($level_3_details['username'] == $this->session->userdata('global_user_details')['user_name'] && $level_3_details['password'] == $this->session->userdata('global_user_details')['password'])) {?>
									<div class="kt-widget__info" style="width:215px;">
									<?php }else{?>
									<div class="kt-widget__info" style="width:240px;">
									<?php }?>
										<a href="javascript:void(0)" class="kt-widget__username">
											<?php echo $level_3_details['user_designation']; ?>
										</a>
										<span class="kt-widget__desc">
											<?php echo $level_3_details['name']; ?>
										</span>
									</div>
									<?php  if(in_array($level_3_details['user_id'], $allow_other_person_login_array) || ($level_3_details['username'] == $this->session->userdata('global_user_details')['user_name'] && $level_3_details['password'] == $this->session->userdata('global_user_details')['password'])) {?>
										<div class="kt-widget__info" style="width:25px; padding-left: 0px;">
											<span href="javascript:void(0)" class="kt-widget__username">
												<div class="dropdown dropdown-inline">
													<button type="button" class="btn btn-hover-brand btn-elevate-hover btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<i class="flaticon-more"></i>
													</button>
													<div class="dropdown-menu dropdown-menu-right">
														<a class="dropdown-item login_as_another_user" href="javascript:void(0)" user_name="<?php echo $level_3_details['username']; ?>" password="<?php echo $level_3_details['password']; ?>"><i class="la la-user"></i> Login</a>
														<a class="dropdown-item" href="<?php echo base_url('home/calendar/'.$level_3_details['user_id']);?>" target="_blank"><i class="la la-plus"></i> Daily Report</a>
														<?php if(in_array($level_3_details['user_department'], array('Sales', 'Procurement'))) {?>
														<a class="dropdown-item" href="<?php echo base_url('common/view_user_data/'),$level_3_details['user_id'];?>" target="_blank"><i class="la la-plus"></i>Manage Lead</a>
														<?php } ?>
													</div>
												</div>
											</span>
											<span class="kt-widget__desc" style="padding-top: 50px;">
											</span>
										</div>
									<?php }?>
								</div>
							</div>

							<!--end::Widget -->
						</div>
						<?php }else{ ?>
							<div class="" style="width: 350px;"></div>
						<?php } ?>
					<?php } ?>
					</div>
					<?php }?>

					<?php if(!empty($org_chart_details['level_4'])) {?>
					<div class="row" style="padding: 20px 0px;">
					<?php foreach ($org_chart_details['level_4'] as $level_4_details) { ?>
						<div class="" style="width: <?php echo $level_4_details['width'],'px'; ?>;"></div>

						<?php if(!empty($level_4_details['user_designation'])) { ?>
						<div class="" style="width: 350px;background-color: #fff !important;box-sizing: border-box;border-top: 0.5rem solid <?php echo $level_4_details['color'];?>;">
							<!--begin::Widget -->
							<div class="kt-widget kt-widget--user-profile-2">
								<div class="kt-widget__head" style="margin-top: 0px;padding: 10px;">
									<div class="kt-widget__media" style="width: 90px;">
										<img class="kt-widget__img kt-hidden" src="assets/media/users/100_1.jpg" alt="image">
										<?php if(!empty($level_4_details['profile_pic_file_path'])) {?>

											<img class="kt-widget__img kt-hidden-" src="<?php echo "https://crm.omtubes.com/assets/hr_document/profile_pic/".$level_4_details['profile_pic_file_path'];?>" alt="image" style="max-height: 90px;">
										<?php } else {?>
										<div class="kt-widget__pic kt-widget__pic--<?php echo $image_not_found_array[$image_not_found_array_key%5];?> kt-font-<?php echo $image_not_found_array[$image_not_found_array_key%5];?> kt-font-boldest kt-hidden-">
											<?php 
												$explode_name = explode(' ', $level_4_details['name']);
												foreach ($explode_name as $single_word) {
													echo ucfirst($single_word[0]);
												}
											?>
										</div>
										<?php $image_not_found_array_key++;}?>
									</div>
									<?php  if(in_array($level_4_details['user_id'], $allow_other_person_login_array)  || ($level_4_details['username'] == $this->session->userdata('global_user_details')['user_name'] && $level_4_details['password'] == $this->session->userdata('global_user_details')['password'])) {?>
									<div class="kt-widget__info" style="width:215px;">
									<?php }else{?>
									<div class="kt-widget__info" style="width:240px;">
									<?php }?>
										<a href="javascript:void(0)" class="kt-widget__username">
											<?php echo $level_4_details['user_designation']; ?>
										</a>
										<span class="kt-widget__desc">
											<?php echo $level_4_details['name']; ?>
										</span>
									</div>
									<?php  if(in_array($level_4_details['user_id'], $allow_other_person_login_array) || ($level_4_details['username'] == $this->session->userdata('global_user_details')['user_name'] && $level_4_details['password'] == $this->session->userdata('global_user_details')['password'])) {?>
										<div class="kt-widget__info" style="width:25px; padding-left: 0px;">
											<span href="javascript:void(0)" class="kt-widget__username">
												<div class="dropdown dropdown-inline">
													<button type="button" class="btn btn-hover-brand btn-elevate-hover btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<i class="flaticon-more"></i>
													</button>
													<div class="dropdown-menu dropdown-menu-right">
														<a class="dropdown-item login_as_another_user" href="javascript:void(0)" user_name="<?php echo $level_4_details['username']; ?>" password="<?php echo $level_4_details['password']; ?>"><i class="la la-user"></i> Login</a>
														<a class="dropdown-item" href="<?php echo base_url('home/calendar/'.$level_4_details['user_id']);?>" target="_blank"><i class="la la-plus"></i> Daily Report</a>
														<?php if(in_array($level_4_details['user_department'], array('Sales', 'Procurement'))) {?>
														<a class="dropdown-item" href="<?php echo base_url('common/view_user_data/'),$level_4_details['user_id'];?>" target="_blank"><i class="la la-plus"></i>Manage Lead</a>
														<?php } ?>
													</div>
												</div>
											</span>
											<span class="kt-widget__desc" style="padding-top: 50px;">
											</span>
										</div>
									<?php }?>
								</div>
							</div>

							<!--end::Widget -->
						</div>
						<?php }else{ ?>
							<div class="" style="width: 350px;"></div>
						<?php } ?>
					<?php } ?>
					</div>
					<?php }?>
				</div>
			</div>

			<!--End::Portlet-->
		</div>
	</div>

	<!--End::Section-->
<?php } ?>
<?php if(in_array(3,$this->session->userdata('graph_data_access'))) { ?>
	<!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--Begin::Portlet-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head" style="padding: 25px 25px 00px 25px;">
                    <div class="kt-portlet__head-label">
                    	<div class="row" style="padding: 0px 10px;">
							<div class="col-xl-12">
								<h3 class="kt-portlet__head-title">Lead Stage</h3>
								<select class="form-control kt-selectpicker" id="lead_stage" multiple>
									<option value="null">Blank Lead Stage</option>
									<option value="1">Stage-1</option>
									<option value="2">Stage-2</option>
									<option value="3">Stage-3</option>
									<option value="4">Stage-4</option>
									<option value="5">Stage-5</option>
									<option value="6">Stage-6</option>
									<option value="0">Stage-0</option>
								</select>
							</div>
						</div>
						<div class="row" style="padding: 0px 10px;">
							<div class="col-xl-12">
								<h3 class="kt-portlet__head-title">Lead Type:</h3>
								<select class="form-control kt-selectpicker" id="lead_type" multiple>
									<option value="null">Blank Lead Type</option>
									<option value="1">Trader</option>
									<option value="2">Epc Contractor</option>
									<option value="3">End User</option>
								</select>
							</div>
						</div>
						<div class="row" style="padding: 0px 10px;">
							<div class="col-xl-12">
								<h3 class="kt-portlet__head-title">Country:</h3>
								<select class="form-control kt-selectpicker" id="lead_country" multiple>
									<option value="null">Blank Country</option>
									<?php foreach ($country_list as $country_details) { ?>										
										<option value="<?php echo $country_details['lookup_id']; ?>"><?php echo $country_details['lookup_value']; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="row" style="padding: 0px 10px;">
							<div class="col-xl-12">
								<h3 class="kt-portlet__head-title">Region:</h3>
								<select class="form-control kt-selectpicker" id="lead_region" multiple>
									<option value="null">Blank Region</option>
									<?php foreach ($region_list as $region_details) { ?>										
										<option value="<?php echo $region_details['lookup_id']; ?>"><?php echo $region_details['lookup_value']; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="row" style="padding: 0px 10px;">
							<div class="col-xl-12">
								<h3 class="kt-portlet__head-title">Hetro / Primary Leads:</h3>
								<select class="form-control kt-selectpicker" id="source" multiple>
									<?php foreach ($sub_module_list as $single_sub_module_details) { ?>										
										<option value="<?php echo $single_sub_module_details['url']; ?>"><?php echo $single_sub_module_details['sub_module_name']; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
                    </div>
                    <div class="kt-portlet__head-toolbar">
						<button type="button" class="btn btn-info add_search_filter"style="border: 5px solid #e2e5ec;">Search</button>
						<button type="button" class="btn btn-dark"style="border: 5px solid #e2e5ec;">Reset</button>
					</div>
                </div>
                <div class="kt-portlet__body">
                    <!--Begin::Timeline 3 -->   
                    <div class="kt-scroll" data-scroll="true" style="height: 400px">
                        <div id="sales_lead_count_highchart"></div>
                    </div>

                    <!--End::Timeline 3 -->
                </div>
            </div>

            <!--End::Portlet-->
        </div>
    </div>

    <!--End::Section-->
<?php } ?>