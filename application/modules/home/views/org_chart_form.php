<div class="row">
	<div class="col-xl-12 form-group row">
		<label for="recipient-name" class="col-xl-2 form-control-label">Name:</label>
        <select class="col-xl-6 form-control lead_select_picker" name="name">
            <option value="">Select Name</option>
            <?php 
            foreach($user_list as $single_user_list){
                        
                echo '<option value="'.$single_user_list['value'].'_'.$single_user_list['name'].'" '.$single_user_list['selected'].'>'.$single_user_list['name'].'</option>';
            }
            ?>                
        </select>
	</div>
	<div class="col-xl-12 form-group row">
		<label for="recipient-name" class="col-xl-2 form-control-label">Designation:</label>
        <select class="col-xl-6 form-control lead_select_picker" name="designation">
            <option value="">Select Designation</option>
            <?php 
            foreach($user_designation_list as $single_user_designation_list){
                        
                echo '<option value="'.$single_user_designation_list['name'].'" '.$single_user_designation_list['selected'].'>'.$single_user_designation_list['name'].'</option>';
            }
            ?>                
        </select>
	</div>
	<div class="col-xl-12 form-group row">
		<label for="recipient-name" class="col-xl-2 form-control-label">Order:</label>
        <select class="col-xl-6 form-control lead_select_picker" name="order_id">
            <option value="">Select Order</option>
            <?php 
            foreach($org_chart_list as $single_org_chart_list){
                        
                echo '<option value="'.$single_org_chart_list['value'].'" '.$single_org_chart_list['selected'].'>'.$single_org_chart_list['name'].'</option>';
            }
            ?>
        </select>
        <input type="hidden" class="" name="order_id_old" value="<?php echo $order_id; ?>">
		<input type="hidden" class="" name="reporting_manager_id" value="<?php echo $reporting_manager_id; ?>">
	</div>
</div>