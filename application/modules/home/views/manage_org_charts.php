<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Kanban Interactivity Demo
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div id="kanban3"></div>
					<div class="kanban-toolbar">
						<button class="btn btn-brand" id="addDefault">Add "Default" board</button>
						<button class="btn btn-danger" id="addToDo">Add element in "To Do" Board</button>
						<button class="btn btn-success" id="removeBoard">Remove "Done" Board</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
