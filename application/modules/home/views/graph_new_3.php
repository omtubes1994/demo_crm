<link href="assets/plugins/custom/kanban/kanban.bundle.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    .kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
		padding-top: 2% !important;
	}
	:root {
        --level-1: #8dccad;
        --level-2: #f5cc7f;
        --level-3: #7b9fe0;
        --level-4: #f27c8d;
        --black: black;
	}
	* {
        padding: 0;
        margin: 0;
        box-sizing: border-box;
	}
	ol {
	    list-style: none;
	}
	body {
        margin: 50px 0 100px;
        text-align: center;
        font-family: "Inter", sans-serif;
	}
    .level-one {
        width: 50%;
        margin: 0 auto 40px;
        background: var(--level-1);
	}
	.level-one::before {
        content: "";
        position: absolute;
        top: 100%;
        left: 50%;
        transform: translateX(-50%);
        width: 2px;
        height: 20px;
        background: black;
	}
	.level-two-wrapper {
        position: relative;
        display: grid;
        grid-template-columns: repeat(10, 1fr);
    }
    .level-two-wrapper::before {
        content: "";
        position: absolute;
        top: -12%;
        left: 5%;
        width: 90%;
        height: 2px;
        background: black;
    }
    .level-two-wrapper >li:before {
        content: "";
        position: absolute;
        bottom: 98%;
        transform: translateX(-50%);
        width: 2px;
        height: 25px;
        background: black;
    }
    <?php if(!empty($level_three_employee_list)){ ?>
        .level-two-wrapper >li::after {
            content: "";
            position: absolute;
            bottom: -10%;
            left: <?php echo $department_left; ?>;
            transform: translateX(-50%);
            width: 2px;
            height: 1.5rem;
            background: black;
        }
        .level-three-wrapper {
            position: relative;
            display: grid;
            grid-template-columns: repeat(10, 1fr);
        }
        .level-three-wrapper:before {
            content: "";
            position: absolute;
            top: -20px;
            left: 5%;
            width: 90%;
            height: 2px;
            background: black;
        }
        .level-three-wrapper >li::before {
            content: "";
            position: absolute;
            bottom: 98%;
            transform: translateX(-50%);
            width: 2px;
            height: 25px;
            background: black;
        }
    <?php } ?>
</style>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kanban-container" style="width: 100%; box-shadow: rgba(14, 30, 37, 0.12) 0px 2px 4px 0px, rgba(14, 30, 37, 0.32) 0px 2px 16px 0px; width: 1370px !important;">
        <div data-id="_backlog" data-order="1" class="kanban-board"
            style="width: 100% !important; margin-left: 0px; margin-right: 0px;">
            <main class="kanban-drag">
                <div class="kanban-item dark-light level-one" style="width: 60%; margin: 0 auto 40px; position: relative;">
                    <div class="kt-kanban__badge" style="justify-content: center;">
                        <div class="kt-kanban__content">
                            <div class="kt-kanban__title kt-font-xl">Jay Mehta</div>
                            <div class="kt-kanban__title kt-font-boldest" style="color:#343a40; font-weight: 800 !important;">CEO</div>
                        </div>
                    </div>
                </div>
                <ol class="level-two-wrapper">
                    <?php 
                        foreach ($level_two_employee_list as $single_employee_details) {
                            
                            echo '<li class="" style="margin: 5px 5px 0px 0px;" name = "'.$single_employee_details['tab_name'].'">';
                                $this->load->view('home/graph_employee_information', $single_employee_details);
                            echo '</li>';
                        }
                    ?>
                </ol>
                <div class="" style="width: 60%; margin: 0 auto 40px; position: relative;"></div>
                <ol class="level-three-wrapper">
                    <?php 
                        foreach ($level_three_employee_list as $single_employee_details) {
                            
                            echo '<li class="" style="margin: 5px 5px 0px 0px;">';
                                $this->load->view('home/graph_employee_information', $single_employee_details);
                            echo '</li>';
                        }
                    ?>
                </ol>
            </main>

            <footer></footer>
        </div>
    </div>
</div>
<!-- end:: Content -->

<script src="assets/plugins/custom/kanban/kanban.bundle.js" type="text/javascript"></script>
<script src="assets/js/pages/components/extended/kanban-board.js" type="text/javascript"></script>