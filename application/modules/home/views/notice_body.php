<?php if(!empty($notice_details)){ ?>
	<?php foreach ($notice_details as $single_notice_details) { ?>
		<div class="col-xl-6">
			<!--begin:: Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__body">
					<div class="kt-widget kt-widget--user-profile-3">
						<div class="kt-widget__top">
							<div class="kt-widget__content">
								<div class="kt-widget__head">
									<a href="#" class="kt-widget__username">CRM Notification</a>
									<div class="kt-widget__action"></div>
								</div>
								<div class="kt-widget__info"></div>
							</div>
						</div>
						<div class="kt-widget__bottom">
						</div>
						<div class="kt-widget__top">
							<?php if(!empty($single_notice_details['notice_img'])) {?>
							<div class="kt-widget__media kt-hidden-">
								<img src="<?php echo base_url($single_notice_details['notice_img']);?>" alt="image" style="width:200px;height:200px;">
							</div>
							<?php } else {?>
							<div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden-">
								<?php echo $single_notice_details['notice_type']; ?>
							</div>
							<?php } ?>
							<div class="kt-widget__content">
								<div class="kt-widget__head">
									<a href="javascript:void(0)" class="kt-widget__username show_big_image" data-toggle="modal" data-target="#kt_modal_4" id="<?php echo $single_notice_details['id']; ?>">
										<?php echo $single_notice_details['notice_type']; ?>
									</a>
									<div class="kt-widget__action"></div>
								</div>
								<div class="kt-widget__subhead">
									<a href="#"><i class="flaticon-calendar-with-a-clock-time-tools"></i><?php echo date('F-Y-j', strtotime($single_notice_details['add_time'])); ?></a>
								</div>
								<div class="kt-widget__info">
									<div class="kt-widget__desc">
										<?php echo $single_notice_details['notice_message'], ' By clicking on crm, you can view image in wider screen'; ?>
									</div>
								</div>
							</div>
						</div>
						<div class="kt-widget__bottom">
						</div>
					</div>
				</div>
			</div>

			<!--end:: Portlet-->
		</div>		
	<?php } ?>

<?php } else { ?>
	<!-- <div class="col-xl-6"> -->
		<!--begin:: Portlet-->
		<!-- <div class="kt-portlet">
			<div class="kt-portlet__body">
				<div class="kt-widget kt-widget--user-profile-3">
					<div class="kt-widget__top">
						<div class="kt-widget__media kt-hidden-">
							<img src="<?php echo base_url('assets/media/users/default.jpg');?>" alt="image" style="width:200px;height:200px;">
						</div>
						<div class="kt-widget__content">
							<div class="kt-widget__head">
								<a href="#" class="kt-widget__username">
									No Data Found
								</a>
								<div class="kt-widget__action"></div>
							</div>
							<div class="kt-widget__subhead">
								<a href="#"><i class="flaticon-calendar-with-a-clock-time-tools"></i></a>
							</div>
							<div class="kt-widget__info">
								<div class="kt-widget__desc">
								</div>
							</div>
						</div>
					</div>
					<div class="kt-widget__bottom">
					</div>
				</div>
			</div>
		</div> -->

		<!--end:: Portlet-->
	<!-- </div> -->
<?php } ?>