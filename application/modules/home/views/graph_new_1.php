<link href="assets/plugins/custom/kanban/kanban.bundle.css" rel="stylesheet" type="text/css" />
<style type="text/css">
	.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
		padding-top: 2% !important;
	}
:root {
	--level-1: #8dccad;
	--level-2: #f5cc7f;
	--level-3: #7b9fe0;
	--level-4: #f27c8d;
	--black: black;
}
* {
	padding: 0;
	margin: 0;
	box-sizing: border-box;
}
ol {
	list-style: none;
}
body {
	margin: 50px 0 100px;
	text-align: center;
	font-family: "Inter", sans-serif;
}
.container {
	max-width: 1000px;
	padding: 0 10px;
	margin: 0 auto;
}
.rectangle {
	position: relative;
	padding: 20px;
	box-shadow: 0 5px 15px rgba(0, 0, 0, 0.15);
}
.level-1 {
	width: 50%;
	margin: 0 auto 40px;
	background: var(--level-1);
}
.level-1::before {
	content: "";
	position: absolute;
	top: 100%;
	left: 50%;
	transform: translateX(-50%);
	width: 2px;
	height: 20px;
	background: var(--black);
}
.level-2-wrapper {
  position: relative;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
}
.level-2-wrapper::before {
  content: "";
  position: absolute;
  top: -20px;
  left: 25%;
  width: 50%;
  height: 2px;
  background: var(--black);
}
.level-2-wrapper::after {
  display: none;
  content: "";
  position: absolute;
  left: -20px;
  bottom: -20px;
  width: calc(100% + 20px);
  height: 2px;
  background: var(--black);
}
.level-2-wrapper li {
  position: relative;
}
.level-2-wrapper > li::before {
  content: "";
  position: absolute;
  bottom: 100%;
  left: 50%;
  transform: translateX(-50%);
  width: 2px;
  height: 20px;
  background: var(--black);
}
.level-2 {
  width: 70%;
  margin: 0 auto 40px;
  background: var(--level-2);
}
.level-2::before {
  content: "";
  position: absolute;
  top: 100%;
  left: 50%;
  transform: translateX(-50%);
  width: 2px;
  height: 20px;
  background: var(--black);
}
.level-2::after {
  display: none;
  content: "";
  position: absolute;
  top: 50%;
  left: 0%;
  transform: translate(-100%, -50%);
  width: 20px;
  height: 2px;
  background: var(--black);
}
.level-3-wrapper {
  position: relative;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-column-gap: 20px;
  width: 90%;
  margin: 0 auto;
}
.level-3-wrapper::before {
  content: "";
  position: absolute;
  top: -20px;
  left: calc(25% - 5px);
  width: calc(50% + 10px);
  height: 2px;
  background: var(--black);
}
.level-3-wrapper > li::before {
  content: "";
  position: absolute;
  top: 0;
  left: 50%;
  transform: translate(-50%, -100%);
  width: 2px;
  height: 20px;
  background: var(--black);
}
.level-3 {
  margin-bottom: 20px;
  background: var(--level-3);
}
.level-4-wrapper {
  position: relative;
  width: 80%;
  margin-left: auto;
}
.level-4-wrapper::before {
  content: "";
  position: absolute;
  top: -20px;
  left: -20px;
  width: 2px;
  height: calc(100% + 20px);
  background: var(--black);
}
.level-4-wrapper li + li {
  margin-top: 20px;
}
.level-4 {
  font-weight: normal;
  background: var(--level-4);
}
.level-4::before {
  content: "";
  position: absolute;
  top: 50%;
  left: 0%;
  transform: translate(-100%, -50%);
  width: 20px;
  height: 2px;
  background: var(--black);
}


.level_rectangle {
	position: relative;
	padding: 20px;
}
.level {
	width: 15%;
	margin: 0 auto 40px !important;
}
.level::before, .level_2::before {
	content: "";
	position: absolute;
	top: 100%;
	left: 50%;
	transform: translateX(-50%);
	width: 2px;
	height: 20px;
	background: var(--black);
}
.level_2 {
	width: 25%;
	margin: 0 auto 40px !important;
}
.level-2-wrapper {
  position: relative;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
}
.level-2-wrapper::before {
  content: "";
  position: absolute;
  top: -20px;
  left: 25%;
  width: 50%;
  height: 2px;
  background: var(--black);
}
.level-2-wrapper::after {
  display: none;
  content: "";
  position: absolute;
  left: -20px;
  bottom: -20px;
  width: calc(100% + 20px);
  height: 2px;
  background: var(--black);
}
.level-2-wrapper li {
  position: relative;
}
.level-2-wrapper > li::before {
  content: "";
  position: absolute;
  bottom: 100%;
  left: 50%;
  transform: translateX(-50%);
  width: 2px;
  height: 20px;
  background: var(--black);
}
</style>
<ol class="level-2-wrapper">
			<li>
				<h2 class="level-2 rectangle">B</h2>
				<ol class="level-3-wrapper">
					<li>
						<h3 class="level-3 rectangle">D</h3>
						<ol class="level-4-wrapper">
							<li>
								<h4 class="level-4 rectangle">...</h4>
							</li>
							...
						</ol>
					</li>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="kanban-container" style="width: 100%;">
		<div data-id="_backlog" data-order="1" class="kanban-board"
			style="width: 100% !important; margin-left: 0px; margin-right: 0px;">
			<main class="kanban-drag dark-light">
				<div class="kanban-item dark-light level level_rectangle">
					<div class="kt-kanban__badge" style="justify-content: center;">
						<div class="kt-kanban__content">
							<div class="kt-kanban__title kt-font-xl">Jay Mehta</div>
							<span class="kt-badge kt-badge--dark kt-badge--inline kt-badge--pill kt-font-sm">CEO</span>
						</div>
					</div>
				</div>
				<ol class="level-2-wrapper">
					<li>
						<div class="kanban-item dark-light level_2 level_rectangle">
							<div class="kt-kanban__badge" style="justify-content: center;">
								<div class="kt-kanban__content">
									<div class="kt-kanban__title kt-font-xl">Om Mehta</div>
									<span class="kt-badge kt-badge--dark kt-badge--inline kt-badge--pill kt-font-sm">Business Development Manager</span>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="kanban-item dark-light level_2 level_rectangle">
							<div class="kt-kanban__badge" style="justify-content: center;">
								<div class="kt-kanban__content">
									<div class="kt-kanban__title kt-font-xl">Pratik Mehta</div>
									<span class="kt-badge kt-badge--dark kt-badge--inline kt-badge--pill kt-font-sm">Business Development Manager</span>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="kanban-item dark-light level_2 level_rectangle">
							<div class="kt-kanban__badge" style="justify-content: center;">
								<div class="kt-kanban__content">
									<div class="kt-kanban__title kt-font-xl">Kunal Deshpande</div>
									<span class="kt-badge kt-badge--dark kt-badge--inline kt-badge--pill kt-font-sm">Sales Head</span>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="kanban-item dark-light level_2 level_rectangle">
							<div class="kt-kanban__badge" style="justify-content: center;">
								<div class="kt-kanban__content">
									<div class="kt-kanban__title kt-font-xl">Abhishek Desai</div>
									<span class="kt-badge kt-badge--dark kt-badge--inline kt-badge--pill kt-font-sm">PHP Developer</span>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="kanban-item dark-light level_2 level_rectangle">
							<div class="kt-kanban__badge" style="justify-content: center;">
								<div class="kt-kanban__content">
									<div class="kt-kanban__title kt-font-xl">Sayli Sawant</div>
									<span class="kt-badge kt-badge--dark kt-badge--inline kt-badge--pill kt-font-sm">Business Analyst</span>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="kanban-item dark-light level_2 level_rectangle">
							<div class="kt-kanban__badge" style="justify-content: center;">
								<div class="kt-kanban__content">
									<div class="kt-kanban__title kt-font-xl">Balamurali Shetty</div>
									<span class="kt-badge kt-badge--dark kt-badge--inline kt-badge--pill kt-font-sm">HR Consultant</span>
								</div>
							</div>
						</div>
					</li>				
					<li>
						<div class="kanban-item dark-light level_2 level_rectangle">
							<div class="kt-kanban__badge" style="justify-content: center;">
								<div class="kt-kanban__content">
									<div class="kt-kanban__title kt-font-xl">Aftab Qureshi</div>
									<span class="kt-badge kt-badge--dark kt-badge--inline kt-badge--pill kt-font-sm">Procurement Head</span>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="kanban-item dark-light level_2 level_rectangle">
							<div class="kt-kanban__badge" style="justify-content: center;">
								<div class="kt-kanban__content">
									<div class="kt-kanban__title kt-font-xl">Yash Jain</div>
									<span class="kt-badge kt-badge--dark kt-badge--inline kt-badge--pill kt-font-sm">Procurement Head</span>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="kanban-item dark-light level_2 level_rectangle">
							<div class="kt-kanban__badge" style="justify-content: center;">
								<div class="kt-kanban__content">
									<div class="kt-kanban__title kt-font-xl">Riyaj Khan</div>
									<span class="kt-badge kt-badge--dark kt-badge--inline kt-badge--pill kt-font-sm">Sr Export Import Executive</span>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="kanban-item dark-light level_2 level_rectangle">
							<div class="kt-kanban__badge" style="justify-content: center;">
								<div class="kt-kanban__content">
									<div class="kt-kanban__title kt-font-xl">Assistant Procurement Manager</div>
									<span class="kt-badge kt-badge--dark kt-badge--inline kt-badge--pill kt-font-sm">Atul Sarode</span>
								</div>
							</div>
						</div>
					</li>
				</ol>
			</main>
		</div>
	</div>
</div>

<!-- end:: Content -->

<script src="assets/plugins/custom/kanban/kanban.bundle.js" type="text/javascript"></script>
<script src="assets/js/pages/components/extended/kanban-board.js" type="text/javascript"></script>