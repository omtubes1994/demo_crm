$(document).ready(function() {

	<?php if($this->uri->segment('2', '') == "dashboard") { ?> 
    <?php if($this->session->userdata('role') == 1) { ?>  
      ajax_call_function({call_type: 'world_map_highchart', filter_year: ''}, 'world_map_highchart');
      ajax_call_function({call_type: 'revenue_highchart', filter_year: ''}, 'revenue_highchart');
      ajax_call_function({call_type: 'quotation_follow_up_highchart', filter_date: ''}, 'quotation_follow_up_highchart');
      ajax_call_function({call_type: 'quotation_vs_proforma_highchart', filter_year: '', sales_person: ''}, 'quotation_vs_proforma_highchart');
       	ajax_call_function({call_type: 'highchart_rfq_status', procurement_person_id: 'All'},'highchart_rfq_status');
		ajax_call_function({call_type: 'quotation_proforma_month_wise_highchart'}, 'quotation_proforma_month_wise_highchart');
    <?php }if(is_array($this->session->userdata('graph_data_access')) && in_array(1,$this->session->userdata('graph_data_access'))) { ?> 
      ajax_call_function({call_type: 'world_map_highchart', filter_year: ''}, 'world_map_highchart');
    <?php } if(is_array($this->session->userdata('graph_data_access')) && in_array(2,$this->session->userdata('graph_data_access'))) { ?>
      ajax_call_function({call_type: 'revenue_highchart', filter_year: ''}, 'revenue_highchart');
    <?php } if(is_array($this->session->userdata('graph_data_access')) && in_array(7,$this->session->userdata('graph_data_access'))) { ?>
      ajax_call_function({call_type: 'quotation_follow_up_highchart', filter_date: ''}, 'quotation_follow_up_highchart');
    <?php } if(is_array($this->session->userdata('graph_data_access')) && in_array(8,$this->session->userdata('graph_data_access'))) { ?>
      ajax_call_function({call_type: 'quotation_vs_proforma_highchart', filter_year: '', sales_person: ''}, 'quotation_vs_proforma_highchart');
    <?php } if(is_array($this->session->userdata('graph_data_access')) && in_array(18,$this->session->userdata('graph_data_access'))) {  ?>
		ajax_call_function({ call_type: 'invoice_total_highchart' }, 'invoice_total_highchart', '<?php echo base_url('invoices/ajax_function');?>');
    <?php } ?>
    ajax_call_function({call_type: 'get_notice_body_data', notice_type: 'crm'}, 'get_notice_body_data');
  <?php } ?>
  <?php if($this->session->userdata('role') == 17) { ?>

       ajax_call_function({call_type: 'highchart_rfq_status', procurement_person_id: 'All'},'highchart_rfq_status');
       ajax_call_function({call_type: 'quotation_proforma_month_wise_highchart'}, 'quotation_proforma_month_wise_highchart');
  <?php } ?>
  <?php if($this->uri->segment('2', '') == "graph") { ?>
    <?php if($this->session->userdata('role') == 1) { ?>  
      ajax_call_function({call_type: 'sales_lead_count_highchart'}, 'sales_lead_count_highchart');
    <?php } if(in_array(3,$this->session->userdata('graph_data_access'))) { ?>
      ajax_call_function({call_type: 'sales_lead_count_highchart'}, 'sales_lead_count_highchart');
    <?php } ?>
  <?php } ?>
	$('.kt-selectpicker').selectpicker();
	$('a.change_quotation_ready_date').click(function(){

		ajax_call_function({call_type: 'quotation_filter_data', filter_date: $(this).attr('filter-date')}, 'quotation_filter_data');
	});

	$('a.change_quotation_pending_for_follow_up_date').click(function(){

		ajax_call_function({call_type: 'quotation_pending_for_follow_up_filter_data', filter_date: $(this).attr('filter-date')}, 'quotation_pending_for_follow_up_filter_data');
	});
	$('div.select_year_filter_div').on('click', 'a.select_year_for_revenue_highchart', function(){

		ajax_call_function({call_type: 'revenue_highchart', filter_year: $(this).attr('year_value')}, 'revenue_highchart');	
	});
	$('a.compare_monthly_revenue').click(function(){

		ajax_call_function({call_type: 'compare_revenue_highchart'}, 'compare_revenue_highchart');	
	});
	$('div.select_year_filter_div_for_quotation_vs_proforma').on('click', 'a.select_year_for_quotation_vs_proforma', function(){

		ajax_call_function({call_type: 'quotation_vs_proforma_highchart', filter_year: $(this).attr('year_value'), sales_person: $('input#input_quotation_vs_proforma_sales_person_name').val()}, 'quotation_vs_proforma_highchart');	
	});
	$('div.quotation_vs_proforma_sales_person').on('click', 'a.select_sales_person_bar_graph', function(){

		ajax_call_function({call_type: 'quotation_vs_proforma_highchart', filter_year: $('input#input_quotation_vs_proforma_current_year').val(), sales_person: $(this).attr('sales-person')}, 'quotation_vs_proforma_highchart');	
	});
	$('div.select_year_filter_div_for_quotation_proforma').on('click', 'a.select_year_for_quotation_proforma', function(){

		ajax_call_function({call_type: 'quotation_proforma_month_wise_highchart', filter_year: $(this).attr('year_value'), sales_person: $('input#input_quotation_proforma_sales_person_name').val()}, 'quotation_proforma_month_wise_highchart');
	});
	$('div.quotation_proforma_sales_person').on('click', 'a.select_sales_person_bar_graph', function(){

		ajax_call_function({call_type: 'quotation_proforma_month_wise_highchart', filter_year: $('input#input_quotation_proforma_current_year').val(), sales_person: $(this).attr('sales-person')}, 'quotation_proforma_month_wise_highchart');
	});
	$('a.daily_report_date').click(function(){

		ajax_call_function({call_type: 'daily_report_highchart', filter_date: $(this).attr('filter-date'), filter_report_type: $('input#filter_report').val()}, 'daily_report_highchart');
	});
	$('div.select_daily_report_dropdown').on('click', 'a.daily_report_type', function(){

		ajax_call_function({call_type: 'daily_report_highchart', filter_date: $('input#filter_date').val(), filter_report_type: $(this).attr('filter-report-type')}, 'daily_report_highchart');
	});
	$('a.change_quotation_pending_for_follow_up_date_for_highchart').click(function(){

		ajax_call_function({call_type: 'quotation_follow_up_highchart', filter_date: $(this).attr('filter-date')}, 'quotation_follow_up_highchart');
	});
	$('a.change_quotation_list_date_for_highchart').click(function(){

		ajax_call_function({call_type: 'quotation_list_highchart', filter_date: $(this).attr('filter-date'), filter_year: $('input#filter_year_value').val(), filter_sales_person: $('input#filter_sales_person_value').val()}, 'quotation_list_highchart');

	});
	$('div.select_year_dropdown').on('click', 'a.select_year', function(){

		ajax_call_function({call_type: 'quotation_list_highchart', filter_date: 'month', filter_year: $(this).attr('year-value'), filter_sales_person: $('input#filter_sales_person_value').val()}, 'quotation_list_highchart');
		$('a.change_quotation_list_date_for_highchart').removeClass('active');
		$('a.change_quotation_list_date_for_highchart[filter-date = month]').addClass('active');
	});

	$('div.select_sales_person_dropdown').on('click', 'a.select_sales_person', function(){

		ajax_call_function({call_type: 'quotation_list_highchart', filter_date: $('input#filter_date_value').val(), filter_year: $('input#filter_year_value').val(), filter_sales_person: $(this).attr('sales-person')}, 'quotation_list_highchart');
	});

	$('button.add').click(function(){
		if (!$('button.add').hasClass('kt-spinner')){
			set_reset_spinner($(this));
			var form_name = $(this).attr('form_name');
			ajax_call_function({call_type: 'add', add_form: $('form#'+form_name).serializeArray(), type: $(this).attr('add_name')}, 'add');
		}
	});

	$('div.dynamic_manage_table').on('click', 'a.delete', function(){
		
		ajax_call_function({call_type: 'delete', id: $(this).attr('id'), type: $(this).attr('delete_name')}, 'delete');
	});

	$('li.change_manage_tab').click(function(){
		if(!$(this).hasClass('active')){

			$('li.change_manage_tab').removeClass('active');
			$($(this)).addClass('active');
			ajax_call_function({call_type: 'get_management_tab_details', tab_name: $(this).attr('tab-name')}, 'get_management_tab_details');
		}
	});

	$('div.dynamic_manage_table').on('click', 'i.edit_terms_and_conditions', function(){
		var alert_text = $.trim($('div.terms_conditions_alert').html());
		$('textarea#terms_conditions').html('').html(alert_text);
	});
	$('button.update_terms_conditions').click(function(){
		var updated_term_condition = $.trim($('textarea#terms_conditions').val());
		$('div.terms_conditions_alert').html('').html(updated_term_condition);
		ajax_call_function({call_type: 'update_footer__procurement', terms_condition: updated_term_condition}, 'update_footer__procurement');
	});
	KTAutosize.init();
	$('button.add_search_filter').click(function(){
		ajax_call_function(
		{
			call_type: 'sales_lead_count_highchart', lead_stage: $('select#lead_stage').val(),
			lead_type: $('select#lead_type').val(), lead_source: $('select#source').val(),
			lead_country: $('select#lead_country').val(), lead_region: $('select#lead_region').val()
		}, 'sales_lead_count_highchart');
	});
	$('a.login_as_another_user').click(function(){
		if($(this).attr('user_name') != '' && $(this).attr('password') != '') {

	     	$.ajax({
		        type: 'POST',
		        data: {call_type: 'generate_other_person_login', user_name: $(this).attr('user_name'), password: $(this).attr('password')},
		        url: "<?php echo base_url('home/ajax_function');?>",
		        dataType: 'JSON',
		        success: function(res){
		           if(res.url != '') {
		              location.href = res.url;
		           } 
		        },
		        beforeSend: function(response){
		           
		        }
	     	});
	     } else {
	     	sweet_alert('Something went wrong!!!');
	     }
  	});
  	$('a.reset_user_login').click(function(){
     	$.ajax({
	        type: 'POST',
	        data: {call_type: 'reset_user_login'},
	        url: "<?php echo base_url('home/ajax_function');?>",
	        dataType: 'JSON',
	        success: function(res){
	           if(res.url != '') {
	              location.href = res.url;
	           } 
	        },
	        beforeSend: function(response){
	           
	        }
     	});
  	});
  	$('button.save_org_data').click(function(){

		$.ajax({
	        type: 'POST',
	        data: {call_type: 'save_org_data', id: $(this).attr('id'), form_data: $('form#org_chart_info_form').serializeArray()},
	        url: "<?php echo base_url('home/ajax_function');?>",
	        dataType: 'JSON',
	        success: function(res){
	        	// location.reload();
	           // if(res.url != '') {
	           //    location.href = res.url;
	           // } 
	        },
	        beforeSend: function(response){
	           swal('Data updated sucessfull');
	        }
     	});
  	});
  	$('div.level_org').on('click', 'select#user_reporting_level', function(){
		$('div.next_to').show();
	});
  	$('button.get_notice').click(function(){

  		ajax_call_function({call_type: 'get_notice_body_data', notice_type: $(this).attr('notice_type')}, 'get_notice_body_data');

  	});
  	$('div.notice_panel').on('click', 'a.show_big_image', function(){
  		ajax_call_function({call_type: 'get_image', id: $(this).attr('id')}, 'get_image');

  	});
  	$('select#daily_report_department').change(function(){
  		var role_id = [];
  		role_id = $(this).val();
  		var new_role_id_added = 0;
  		for (var i = 1; i < 20; i++) {
  			var role_id_exist_id = 0
  			for (var j = 0; j < role_id.length; j++) {
  				if(role_id[j] == i){
  					role_id_exist_id = role_id[j];
  					continue;
  				}
  			}
  			if(role_id_exist_id > 0){

  				if($('li.nav_item_'+role_id_exist_id).css('display') == 'none'){
  					$('a.nav-link').removeClass('active');
				  	$('li.nav_item_'+role_id_exist_id).show();
				  	$('li.nav_item_'+role_id_exist_id).addClass('active');
				  	$('li.nav_item_'+role_id_exist_id+' a.nav-link').addClass('active'); 
				  	$('div.tab-pane').removeClass('active');
				  	$('div#department_'+role_id_exist_id).addClass('active');
	  			}else{
	  				// $('li.nav_item_'+i).css('display','table-cell');
	  			}
  			}else{

  				$('li.nav_item_'+i).hide();
  				$('div#department_'+i).removeClass('active');
  			}
  		}
  	});
  	$('#daily_report_department').select2({
        placeholder: "Select a department",
    });

    $('div.status_wise_count').on('click', 'a.view_details', function(){
        production_status_graph([], '');
        if($(this).attr('action_value') == 'show') {
            $('div.production_order_wise_graph_div').slideDown();
            $(this).attr('action_value', 'hide');
            $(this).html('').html('<i class="kt-font-dark flaticon2-line-chart"></i> Hide Details');
            ajax_call_function({call_type: 'production_stage_wise_highchart_data', highchart_data_call_type: $(this).attr('order_type')},'production_stage_wise_highchart_data',"<?php echo base_url('production/ajax_function'); ?>");

        } else {
            $('div.production_order_wise_graph_div').slideUp();
            $('a.view_details').attr('action_value', 'show');
            $('a.view_details').html('').html('<i class="kt-font-dark flaticon2-line-chart"></i> Show Details');
        }
    });
	$('div.production_client_wise_count').on('click', 'a.view_all_details', function () {

		production_client_wise_graph([], '');
		if ($(this).attr('action_value') == 'show') {
			$('div.production_client_wise_graph_div').slideDown();
			$('div.production_status_and_product_family_wise_graph_div').slideDown();
			$(this).attr('action_value', 'hide');
			$(this).html('').html('<i class="kt-font-dark flaticon2-line-chart"></i> Hide Details');
			ajax_call_function({ call_type: 'production_client_wise_highchart_data', highchart_data_call_type: $(this).attr('order_type') }, 'production_client_wise_highchart_data', "<?php echo base_url('production/ajax_function'); ?>");
			ajax_call_function({ call_type: 'product_family_wise_highchart_data', highchart_data_call_type: $(this).attr('order_type') }, 'product_family_wise_highchart_data', "<?php echo base_url('production/ajax_function'); ?>");
			ajax_call_function({ call_type: 'production_status_wise_highchart_data', highchart_data_call_type: $(this).attr('order_type') }, 'production_status_wise_highchart_data', "<?php echo base_url('production/ajax_function'); ?>");
		} else {
			$('div.production_client_wise_graph_div').slideUp();
			$('div.production_status_and_product_family_wise_graph_div').slideUp();
			$('a.view_all_details').attr('action_value', 'show');
			$('a.view_all_details').html('').html('<i class="kt-font-dark flaticon2-line-chart"></i> Show Details');
		}
	});


    $('div.procurement_list').on('click', 'a.procurement_id', function(){
    	ajax_call_function({call_type: 'highchart_rfq_status', procurement_person_id: $(this).attr('value')},'highchart_rfq_status');
    });

	$('li div.view_details').click(function(){

		window.location.href = '<?php echo base_url("home/dashboard/"); ?>' + $(this).attr('url');
	});

	$('button.get_org_form_details').click(function(){

		$('form#update_org_chart_user_details').html('');
		var form_call_type = $(this).attr('form_call_type');
		if(form_call_type == 'add_main'){

			var next_order_id = $(this).attr('next_order_id');
			var reporting_manager_id = $(this).attr('reporting_manager_id');
			var org_chart_id = $(this).attr('org_chart_id');
			if(next_order_id == 0 || reporting_manager_id == 0){

				alert('some thing went wrong!');
			}else{

				ajax_call_function({call_type: 'get_org_form_details', form_call_type: form_call_type,  next_order_id: next_order_id, reporting_manager_id: reporting_manager_id,  org_chart_id: org_chart_id},'get_org_form_details');
			}
		}else if(form_call_type == 'edit_main'){
			
			$('tbody#user_list_detail').html('');
			var org_chart_id = $(this).attr('org_chart_id');
			var reporting_manager_id = $(this).attr('reporting_manager_id');
			if(org_chart_id == 0){

				alert('some thing went wrong!');
			}else{
				ajax_call_function({call_type: 'get_org_form_details', form_call_type: form_call_type,  org_chart_id: org_chart_id, reporting_manager_id: reporting_manager_id},'get_org_form_details');
			}
		}
		
	});

	$('button.get_org_form_details_junior').click(function(){

		var form_call_type = $(this).attr('form_call_type');
		if(form_call_type == 'add_sub'){
			
			var next_order_id = $(this).attr('next_order_id');
			var reporting_manager_id = $(this).attr('reporting_manager_id');
			var org_chart_id = $(this).attr('org_chart_id');
			if(next_order_id == 0 || reporting_manager_id == 0){

				alert('some thing went wrong!');
			}else{

				ajax_call_function({call_type: 'get_org_form_details', form_call_type: form_call_type,  next_order_id: next_order_id, reporting_manager_id: reporting_manager_id,  org_chart_id: org_chart_id},'get_org_form_details');
			}
		}
		
	});

	$('tbody#user_list_detail').on('click', 'button.save_org_details_junior', function(){

		var current_key = $(this).attr('current_key');
		ajax_call_function({call_type: 'save_org_details', form_data: [{name:'name', value:$('select[name="name_'+current_key+'"]').val()}, {name:'designation', value:$('select[name="designation_'+current_key+'"]').val()}, {name:'order_id', value:$('select[name="order_id_'+current_key+'"]').val()}, {name:'reporting_manager_id', value:$('input[name="reporting_manager_id_'+current_key+'"]').val()}, {name:'order_id_old', value:$('input[name="order_id_old_'+current_key+'"]').val()}], id: $(this).attr('org_chart_id')},'save_org_details');
	});

	$('button.save_org_details').click(function(){

		var form_name = $(this).attr('form_name');
		if(form_name != ''){

			ajax_call_function({call_type: 'save_org_details', form_data: $('form#' + form_name).serializeArray(), id: $(this).attr('org_chart_id')},'save_org_details');
		}
	});
	document.addEventListener('keydown', function(event) {

		if (event.shiftKey) {

			if(event.key === 'I'){

				ajax_call_function({call_type: 'change_type', type: 'in'},'change_type');
			}else if(event.key === 'Z'){

				ajax_call_function({call_type: 'change_type', type: 'zen'},'change_type'); 
			}else if(event.key === 'O'){

				ajax_call_function({call_type: 'change_type', type: 'om'},'change_type');
			}else if(event.key === 'A'){

				ajax_call_function({call_type: 'change_type', type: ''},'change_type'); 
			}    
		}
        
    });
	$('input[name="radio_company_type"]').click(function(){
		
		ajax_call_function({call_type: 'change_type', type: $("input[type='radio'][name='radio_company_type']:checked").val()},'change_type'); 
	});
	$('div.sales_routine_div').on('click', 'a.save_daily_work_on_sales_routine', function(){
		
		var sales_routine_id = $(this).attr('sales_routine_id');
		var work_completed = $(this).attr('work_completed');
		var work_comment = $(this).attr('work_comment');
		$("input[name='work_completed'][value='yes']").prop('checked', false);
		$("input[name='work_completed'][value='no']").prop('checked', false);
		$("textarea#work_comment").attr('readonly', '');
		console.log(sales_routine_id, work_completed, work_comment);
		if(work_completed != ''){

    		$("input[name='work_completed'][value='" + work_completed + "']").prop('checked', true);
			if(work_completed == 'no'){

				$("textarea#work_comment").removeAttr('readonly');
			}
		}
		$("#work_comment").val(work_comment);
		autosize($('#work_comment'));
		$('button.save_daily_work_on_sales_routine_modal').attr('sales_routine_id', sales_routine_id);
		$('div#work_comment_modal').modal('show');
	});
	$("input[name='work_completed']").click(function(){

		
		var work_completed = $('input[name="work_completed"]:checked').val();
		console.log(work_completed);
		$("textarea#work_comment").attr('readonly', '');
		if(work_completed == 'no'){

			$("textarea#work_comment").removeAttr('readonly');
		}
	});
	$('button.save_daily_work_on_sales_routine_modal').click(function(){
		
		var sales_routine_id = $(this).attr('sales_routine_id');
		var work_completed = $('input[name="work_completed"]:checked').val();
		var work_comment = $("#work_comment").val();
		// console.log(sales_routine_id, work_completed, work_comment);
		if(work_completed == 'no'){

			if(typeof work_comment === 'undefined' || work_comment == ''){

				swal('Comment Cannot be empty.');
				return false;
			}
		}
		
		if(sales_routine_id == 0 || typeof work_completed === 'undefined' || work_completed == ''){

			swal('something went wrong. Please contact Developer Team');
			return false;
		}
		swal({
			title: "Are you sure?",
			text: "",
			icon: "info",
			buttons:  ["Cancel", "Save"],
			dangerMode: false, 
		})
		.then((willDelete) => {
			console.log(willDelete);
			
			if (willDelete) {

				ajax_call_function({

					call_type: 'save_daily_work_on_sales_routine',
					sales_routine_id: sales_routine_id,
					work_completed: work_completed,
					work_comment: work_comment,
				},'save_daily_work_on_sales_routine');
			}
		});
		
	});
	ajax_call_function({call_type: 'get_sales_routine_list'},'get_sales_routine_list');

});

var KTAutosize = function () {
    
    // Private functions
    var demos = function () {
        // basic demo
        var demo1 = $('#terms_conditions');
        autosize(demo1);
        $('.vendor_name_select_picker').selectpicker();
    }
    var daterangepickerInit = function() {
        if ($('#kt_dashboard_daterangepicker').length == 0) {
            return;
        }

        var picker = $('#kt_dashboard_daterangepicker');
        var start = moment().subtract(1, 'days');
        var end = moment().subtract(1, 'days');

        function cb(start, end, label) {
            var title = '';
            var range = '';
            if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D Y');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D Y');
            } else {
                range = start.format('MMM D Y') + ' - ' + end.format('MMM D Y');
            }

            $('#kt_dashboard_daterangepicker_date').html(range);
            $('#kt_dashboard_daterangepicker_title').html(title);
			ajax_call_function({call_type: 'desktrack_highchart', filter_date: range}, 'desktrack_highchart');
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end, 'Yesterday');
    }
    var daily_report_date_range_picker_Init = function() {
        if ($('#daily_report_date_range_picker').length == 0) {
            return;
        }

        var picker = $('#daily_report_date_range_picker');
        var start = moment().subtract(1, 'days');
        var end = moment().subtract(1, 'days');

        function cb(start, end, label) {
            var title = '';
            var range = '';

            if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D Y');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D Y');
            } else {
                range = start.format('MMM D Y') + ' - ' + end.format('MMM D Y');
            }

            $('#daily_report_date_range_picker_date').html(range);
            $('#daily_report_date_range_picker_title').html(title);
            ajax_call_function({call_type: 'daily_report_highchart', filter_date: range}, 'daily_report_highchart');
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end, 'Yesterday');
    }

    var quotation_created_date_range_picker_Init = function() {
        if ($('#quotation_created_date_range_picker').length == 0) {
            return;
        }

        var picker = $('#quotation_created_date_range_picker');
        var start = moment().startOf('year');
        var end = moment().endOf('year');

        function cb(start, end, label) {
            var title = '';
            var range = '';

            if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D Y');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D Y');
            } else {
                range = start.format('MMM D Y') + ' - ' + end.format('MMM D Y');
            }

            $('#quotation_created_date_range_picker_date').html(range);
            $('#quotation_created_date_range_picker_title').html(title);
            ajax_call_function({call_type: 'quotation_list_highchart', filter_date: range}, 'quotation_list_highchart');
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'This Week': [moment().startOf('week'), moment().endOf('week')],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end, 'This Month');
    }

    var quotation_proforma_date_range_picker_Init = function() {
        if ($('#quotation_proforma_daterangepicker').length == 0) {
            return;
        }

        var picker = $('#quotation_proforma_daterangepicker');
        var start = moment().startOf('month');
        var end = moment().endOf('month');

        function cb(start, end, label) {
            var title = '';
            var range = '';
            if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D Y');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D Y');
            } else {
                range = start.format('MMM D Y') + ' - ' + end.format('MMM D Y');
            }

            $('#quotation_proforma_daterangepicker_date').html(range);
            $('#quotation_proforma_daterangepicker_title').html(title);
			ajax_call_function({call_type: 'quotation_proforma_highchart', filter_date: range}, 'quotation_proforma_highchart');
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end, 'This Month');
    }

    return {
        // public functions
        init: function() {
            demos(); 
            daterangepickerInit(); 
            daily_report_date_range_picker_Init();
            quotation_created_date_range_picker_Init();
            quotation_proforma_date_range_picker_Init();
        }
    };
}();

function ajax_call_function(data, callType, url = "<?php echo base_url('home/ajax_function'); ?>") {

	$.ajax({
		type: 'POST',
		data: data,
		url: url,
		dataType: 'JSON',
		success: function(res){
			if(res.status == 'successful') {
				if(callType == 'quotation_filter_data') {
					sweet_alert('Quotation Data Updated');
			    	$('div#quotation_list_div_body').html('').html(res.html_body);
				} else if(callType == 'quotation_pending_for_follow_up_filter_data') {
					sweet_alert('Follup Data Updated');
			    	$('div#quotation_pending_for_follow_up_div_body').html('').html(res.html_body);
				} else if(callType == 'revenue_highchart') {

					ajax_call_function({call_type: 'revenue_highchart', filter_year: '2022'}, 'revenue_highchart_2022');
					if(data.filter_year != ''){
						sweet_alert('Revenue Data Updated');
					}
			    	revenue_highchart(res.revenue_highchart);
			    	$('div.select_year_filter_div').html('').html(res.html_body.filter_year);
			    	$('span#current_year').html('').html(res.html_body.current_filter);
				}else if (callType == 'revenue_highchart_2022') {

					revenue_highchart_2022(res.revenue_highchart);
				} else if(callType == 'compare_revenue_highchart') {
					sweet_alert('Compare Revenue Data Updated');
			    	compare_revenue_highchart(res.compare_revenue_highchart);
				} else if(callType == 'quotation_vs_proforma_highchart') {
					if(data.filter_year != ''){
						sweet_alert('Quotation vs Proforma Data Updated');
					}
			    	quotation_vs_proforma_highchart(res.quotation_vs_proforma_highchart_category, res.quotation_vs_proforma_highchart_data);
			    	$('span#quotation_vs_proforma_current_year').html('').html(res.html_body.current_filter);
			    	$('input#input_quotation_vs_proforma_current_year').html('').html(res.html_body.filter_year);
			    	$('div.select_year_filter_div_for_quotation_vs_proforma').html('').html(res.html_body.filter_year);
			    	$('span#quotation_vs_proforma_sales_person_name').html('').html(res.html_body.filter_sales_person_name);
			    	$('input#input_quotation_vs_proforma_sales_person_name').html('').html(res.html_body.filter_sales_person);
			    	$('div.quotation_vs_proforma_sales_person').html('').html(res.html_body.filter_sales_person_html);
				} else if(callType == 'world_map_highchart') {
					world_map_highchart(res.world_map_highchart);
				} else if(callType == 'desktrack_highchart') {
					if($('div#desktrack_highchart').html() != ''){
						sweet_alert('Desktrack Data Updated');
					}
					desktrack_highchart(res.desktrack_highchart);
				} else if(callType == 'daily_report_highchart') {
					if($('div#daily_report_highchart').html() != ''){
						sweet_alert('Daily Report Data Updated');
					}
					daily_report_highchart(res.daily_report_highchart);
					$('div.select_daily_report_dropdown').html('').html(res.html_body.dropdown_body);
			    	$('span#filter_report_type').html('').html(res.html_body.filter_report_type);
			    	$('input#filter_date').val(res.html_body.filter_date);
			    	$('input#filter_report').val(res.html_body.filter_report);
				} else if(callType == 'quotation_follow_up_highchart') {
					if(data.filter_date != ''){
						sweet_alert('Follup Data Updated');
					}
			    	quotation_follow_up_highchart(res.highchart_data);
				} else if(callType == 'quotation_list_highchart') {
					if(data.filter_date != ''){
						sweet_alert('Data Updated');
					}
					quotation_list_highchart(res.highchart_data);
					// $('input#filter_date_value').val(res.html_body.filter_date_name);
					// $('input#filter_year_value').val(res.html_body.filter_year_name);
					// $('input#filter_sales_person_value').val(res.html_body.filter_sales_person_id);
					// $('span#sales_person_name').html('').html(res.html_body.filter_sales_person_name);
					// $('div.select_year_dropdown').html('').html(res.html_body.filter_year);
					// $('div.select_sales_person_dropdown').html('').html(res.html_body.filter_sales_person);
				} else if(callType == 'delete' || callType == 'add') {
					sweet_alert(res.message);
					setTimeout(function(){
						// location.reload();
						location.href = "<?php echo base_url('home/vendor_management/');?>"+data.type;
					}, 2000)	
				} else if(callType == 'get_management_tab_details') {
									
					$('div.dynamic_manage_table').html('').html(res.html_body);
				} else if(callType == 'update_footer__procurement') {
					sweet_alert('Term & Condition Updated!!!');					
					$('div#edit_terms_and_conditions').modal('hide');
				} else if(callType == 'sales_lead_count_highchart') {
					sales_lead_count_highchart(res.sales_lead_count_highchart);
					$('div.select_sales_person_dropdown').html('').html(res.html_body.filter_sales_person);
				} else if(callType == 'quotation_proforma_highchart') {

					quotation_proforma_highchart(res.quotation_proforma_highchart);
					$('h3.total_Revenue').html('').html('Revenue by Sales Person : ' + res.total_revenu + ' USD');
				} else if(callType == 'get_notice_body_data') {
					$('div.notice_panel').html('').html(res.notice_html_body);
				} else if(callType == 'get_image') {
					console.log(res);
					$('img.notice_big_image').attr('src',res.notice_img);
				} else if(callType == 'production_stage_wise_highchart_data') {
                    production_status_graph(res.production_stage_wise_highchart, res.order_type);
					$('.status_wise_count_loader').hide();
				} else if (callType == 'production_client_wise_highchart_data') {
					production_client_wise_graph(res.production_client_wise_highchart);
					$('.status_wise_count_loader').hide();
				} else if (callType == 'product_family_wise_highchart_data') {

					production_product_family_wise_graph(res.production_product_family_wise_highchart);
					$('.status_wise_count_loader').hide();
				} else if (callType == 'production_status_wise_highchart_data') {

					production_status_wise_graph(res.production_status_wise_highchart);
					$('.status_wise_count_loader').hide();
				} else if(callType == 'highchart_rfq_status'){

					$('div.procurement_list').html('').html(res.procurment_person_html);
					$('span#current_procurement_person').html('').html(res.procurement_person_id);
					rfq_status_weekly(res.highchart_data);
				} else if(callType == 'quotation_proforma_month_wise_highchart'){

					quotation_proforma_month_wise_highchart(res.quotation_proforma_highchart);

					$('span#quotation_proforma_current_year').html('').html(res.html_body.current_filter);
					$('input#input_quotation_proforma_current_year').val(res.html_body.current_filter);
					$('div.select_year_filter_div_for_quotation_proforma').html('').html(res.html_body.filter_year);
					$('span#quotation_proforma_sales_person_name').html('').html(res.html_body.filter_sales_person_name);
					$('input#input_quotation_proforma_sales_person_name').val(res.html_body.filter_sales_person);
					$('div.quotation_proforma_sales_person').html('').html(res.html_body.filter_sales_person_html);

				} else if (callType == 'invoice_total_highchart') {
					invoice_total_highchart(res.invoice_total_highchart_data);
				} else if (callType == 'save_org_details') {
					
					location.reload();
				} else if (callType == 'get_org_form_details') {
					
					$('button.get_org_form_details_junior').attr('next_order_id', res.junior_next_order_id);
					$('button.get_org_form_details_junior').attr('reporting_manager_id', res.junior_reporting_manager_id);
					$('button.save_org_details').attr('org_chart_id', data.org_chart_id);
					$('form#update_org_chart_user_details').html(res.update_form_body);
					$('tbody#user_list_detail').append(res.user_list_detail_body);
					$('div#kt_modal_4').modal('show');
				} else if (callType == 'change_type') {

					$('div.status_wise_count').html('').html(res.status_wise_count);
					$('em.all_total').html('').html(res.production_title_value);
					$('.status_wise_count_loader').hide();
					$('div.production_order_wise_graph_div').slideUp();
					$('div.production_client_wise_graph_div').slideUp();
					$('div.production_status_and_product_family_wise_graph_div').slideUp();
				} else if (callType == 'get_sales_routine_list') {

					$('div.sales_routine_div').html('').html(res.sales_routine_div);
				} else if (callType == 'save_daily_work_on_sales_routine') {
					
					swal('Daily Routine Updated Succefully!');
					setTimeout(function () {
						
						ajax_call_function({call_type: 'get_sales_routine_list'},'get_sales_routine_list');
						$('div#work_comment_modal').modal('hide');
					}, 2000);
				}
				
			}
		},
		beforeSend: function(response){
			
			switch (data.call_type) {
				case 'production_client_wise_highchart_data':
				case 'product_family_wise_highchart_data':
				case 'production_status_wise_highchart_data':
				case 'production_stage_wise_highchart_data':
                case 'change_type':
                            
                    $('.status_wise_count_loader').show();
                break;
            }
		}
	});
};

function set_reset_spinner(obj, set_unset_flag = true) {

	if(set_unset_flag) {

		$(obj).addClass('kt-spinner');
    	$(obj).addClass('kt-spinner--right');
    	$(obj).addClass('kt-spinner--sm');
    	$(obj).addClass('kt-spinner--light');	
	} else{

		$(obj).removeClass('kt-spinner');
    	$(obj).removeClass('kt-spinner--right');
    	$(obj).removeClass('kt-spinner--sm');
    	$(obj).removeClass('kt-spinner--light');
	}
}

function sweet_alert(title, icon = 'success') {

	swal({
			title: title,
			icon: icon,
    	});
}

function rfq_status_weekly(highchart_data) {

	Highcharts.chart('rfq_status_highchart', {

	    chart: {
	        type: 'line'
	    },
	    title: {
	        text: ''
	    },
	    subtitle: {
	        text: ''
	    },
	    xAxis: {
	        categories: highchart_data.category
	    },
	    yAxis: {
	        title: {
	            text: ''
	        }
	    },
	    plotOptions: {
	        line: {
	            dataLabels: {
	                enabled: true
	            },
	            enableMouseTracking: false
	        }
	    },
	    series: highchart_data.series
	});
}
// create revenue highchart
function revenue_highchart(highchart_data) {

	Highcharts.chart('revenue_highchart', {
		// Create the chart
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: 'Om Tubes Annual Revenue'
	    },
	    subtitle: {
	        text: ''
	    },
	    accessibility: {
	        announceNewData: {
	            enabled: true
	        }
	    },
	    xAxis: {
	        type: 'category'
	    },
	    yAxis: {
	        title: {
	            text: 'TOTAL REVENUE'
	        }

	    },
	    legend: {
	        enabled: false
	    },
	    plotOptions: {
	        series: {
	            borderWidth: 0,
	            dataLabels: {
	                enabled: true,
	                format: '{point.y}'
	            }
	        }
	    },

	    tooltip: {
	        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	        pointFormat: '<span style="color:{point.color}">{point.name}</span>: Total Revenue is <b>{point.y}</b><br/>'
	    },

	    series: [
	        {
	            name: "Browsers",
	            colorByPoint: true,
	            data: highchart_data
	        }
	    ]
	});
}

function revenue_highchart_2022(highchart_data) {

	Highcharts.chart('revenue_highchart_2022', {
		// Create the chart
		chart: {
			type: 'column'
		},
		title: {
			text: 'Om Tubes Annual Revenue'
		},
		subtitle: {
			text: ''
		},
		accessibility: {
			announceNewData: {
				enabled: true
			}
		},
		xAxis: {
			type: 'category'
		},
		yAxis: {
			title: {
				text: 'TOTAL REVENUE'
			}

		},
		legend: {
			enabled: false
		},
		plotOptions: {
			series: {
				borderWidth: 0,
				dataLabels: {
					enabled: true,
					format: '{point.y}'
				}
			}
		},

		tooltip: {
			headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: Total Revenue is <b>{point.y}</b><br/>'
		},

		series: [
			{
				name: "Browsers",
				colorByPoint: true,
				data: highchart_data
			}
		]
	});
}
// create compare revenue highchart
function compare_revenue_highchart(highchart_data) {

	Highcharts.chart('revenue_highchart', {
		chart: {
	        type: 'column'
	    },
	    title: {
	        text: 'Yearly Revenue'
	    },
	    subtitle: {
	        text: ''
	    },
	    xAxis: {
	        categories: [
	            'Jan',
	            'Feb',
	            'Mar',
	            'Apr',
	            'May',
	            'Jun',
	            'Jul',
	            'Aug',
	            'Sep',
	            'Oct',
	            'Nov',
	            'Dec'
	        ],
	        crosshair: true
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Revenue'
	        }
	    },
	    tooltip: {
	        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	            '<td style="padding:0"><b>${point.y} </b></td></tr>',
	        footerFormat: '</table>',
	        shared: true,
	        useHTML: true
	    },
	    plotOptions: {
	        column: {
	            borderWidth: 0
	        }
	    },
	    series: highchart_data
	});  
}

// create revenue highchart
function quotation_vs_proforma_highchart(category, highchart_data) {

	Highcharts.chart('quotation_vs_proforma_highchart', {
		// Create the chart
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: ''
	    },
	    xAxis: {
	        // categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
	        categories: category
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Total Quotation'
	        },
	        stackLabels: {
	            enabled: true,
	            style: {
	                fontWeight: 'bold',
	                color: ( // theme
	                    Highcharts.defaultOptions.title.style &&
	                    Highcharts.defaultOptions.title.style.color
	                ) || 'gray'
	            }
	        }
	    },
	    legend: {
	        align: 'right',
	        x: -30,
	        verticalAlign: 'top',
	        y: 25,
	        floating: true,
	        backgroundColor:
	            Highcharts.defaultOptions.legend.backgroundColor || 'white',
	        borderColor: '#CCC',
	        borderWidth: 1,
	        shadow: false
	    },
	    tooltip: {
	        headerFormat: '<b>{point.x}</b><br/>',
	        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
	    },
	    plotOptions: {
	        column: {
	            stacking: 'normal',
	            dataLabels: {
	                enabled: true
	            }
	        }
	    },
	    series: highchart_data
	});
}

function world_map_highchart(highchart_data) {

	// Initiate the chart
	Highcharts.mapChart('world_map_highchart', {
	    chart: {
	        map: 'custom/world',
	        borderWidth: 0
	    },

	    colors: ['rgba(19,64,117,0.05)', 'rgba(19,64,117,0.2)', 'rgba(19,64,117,0.4)',
	        'rgba(19,64,117,0.5)', 'rgba(19,64,117,0.6)', 'rgba(19,64,117,0.8)', 'rgba(19,64,117,1)'],

	    title: {
	        text: 'Revenue Earned By Country'
	    },

	    mapNavigation: {
	        enabled: true
	    },

	    legend: {
	        title: {
	            text: 'Revenue Earned by Country',
	            style: {
	                color: ( // theme
	                    Highcharts.defaultOptions &&
	                    Highcharts.defaultOptions.legend &&
	                    Highcharts.defaultOptions.legend.title &&
	                    Highcharts.defaultOptions.legend.title.style &&
	                    Highcharts.defaultOptions.legend.title.style.color
	                ) || 'black'
	            }
	        },
	        align: 'left',
	        verticalAlign: 'bottom',
	        floating: true,
	        layout: 'vertical',
	        valueDecimals: 0,
	        backgroundColor: ( // theme
	            Highcharts.defaultOptions &&
	            Highcharts.defaultOptions.legend &&
	            Highcharts.defaultOptions.legend.backgroundColor
	        ) || 'rgba(255, 255, 255, 0.85)',
	        symbolRadius: 0,
	        symbolHeight: 14
	    },

	    colorAxis: {
	        dataClasses: [{
	            to: 10000
	        }, {
	            from: 10000,
	            to: 100000
	        }, {
	            from: 100000,
	            to: 200000
	        }, {
	            from: 200000,
	            to: 500000
	        }, {
	            from: 500000,
	            to: 1000000
	        }, {
	            from: 1000000,
	            to: 2000000
	        }, {
	            from: 2000000
	        }]
	    },
	    
	    plotOptions: {
	        series: {
	            point: {
	                events: {
	                    click: function () {
	                        // location.href = ''
	                    }
	                }
	            }
	        }
	    },
	    series: [{
	     	// data: datap,
	    	data: highchart_data,
	        animation: true,
	        name: 'Total Sale Done in', 
	        states: {
	            hover: {
	                color: '#a4edba'
	            }
	        },
	        shadow: false
	    }]
	});
}

function desktrack_highchart(highchart_data) {

	Highcharts.chart('desktrack_highchart', {
		// Create the chart
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: ''
	    },
	    subtitle: {
	        text: ''
	    },
	    accessibility: {
	        announceNewData: {
	            enabled: true
	        }
	    },
	    xAxis: {
	        type: 'category'
	    },
	    yAxis: {
	        title: {
	            text: 'Total Hours'
	        }

	    },
	    legend: {
	        enabled: false
	    },
	    plotOptions: {
	        series: {
	            borderWidth: 0,
	            dataLabels: {
	                enabled: true,
	                format: '{point.y}'
	            }
	        }
	    },

	    tooltip: {
	        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	        pointFormat: '<span style="color:{point.color}">{point.name}</span>: Worked <b>{point.y}hours</b> total<br/>'
	    },
	    series: [
	        {
	            name: "OM Tubes",
	            colorByPoint: true,
	            data: highchart_data
	        }
	    ]
	});	
}

function daily_report_highchart(highchart_data) {

	Highcharts.chart('daily_report_highchart', {
		// Create the chart
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: ''
	    },
	    subtitle: {
	        text: ''
	    },
	    xAxis: {
	        categories: highchart_data.category,
	        crosshair: true
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Total count'
	        }
	    },
	    tooltip: {
	        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	            '<td style="padding:0"><b>{point.y}</b></td></tr>',
	        footerFormat: '</table>',
	        shared: true,
	        useHTML: true
	    },
	    plotOptions: {
	        column: {
	            pointPadding: 0.2,
	            borderWidth: 0
	        }
	    },
	    series: highchart_data.series
	});	
}

// create user wise quotation follup list pending highchart
function quotation_follow_up_highchart(highchart_data) {

	Highcharts.chart('quotation_pending_highchart', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: 'Employee Wise Quotation Follow Up Pending Count'
	    },
	    subtitle: {
	        text: ''
	    },
	    accessibility: {
	        announceNewData: {
	            enabled: true
	        }
	    },
	    xAxis: {
	        type: 'category'
	    },
	    yAxis: {
	        title: {
	            text: 'Total Number of Follow Up Pending'
	        }

	    },
	    legend: {
	        enabled: false
	    },
	    plotOptions: {
	        series: {
	            borderWidth: 0,
	            dataLabels: {
	                enabled: true,
	                format: '{point.y:f}'
	            }
	        }
	    },

	    tooltip: {
	        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> Follow Up Pending<br/>'
	    },

	    series: [
	        {
	            name: "OM Tubes",
	            colorByPoint: true,
	            data: highchart_data
	        }
	    ]

	});
}

// create user wise quotation list pending highchart
function quotation_list_highchart(highchart_data) {

	Highcharts.chart('quotation_list_highchart', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: ''
	    },
	    xAxis: {
	        categories: highchart_data.category
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Total fruit consumption'
	        },
	        stackLabels: {
	            enabled: true,
	            style: {
	                fontWeight: 'bold',
	                color: ( // theme
	                    Highcharts.defaultOptions.title.style &&
	                    Highcharts.defaultOptions.title.style.color
	                ) || 'gray'
	            }
	        }
	    },
	    legend: {
	        align: 'right',
	        x: -30,
	        verticalAlign: 'top',
	        y: 25,
	        floating: true,
	        backgroundColor:
	            Highcharts.defaultOptions.legend.backgroundColor || 'white',
	        borderColor: '#CCC',
	        borderWidth: 1,
	        shadow: false
	    },
	    tooltip: {
	        headerFormat: '<b>{point.x}</b><br/>',
	        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
	    },
	    plotOptions: {
	        column: {
	            stacking: 'normal',
	            dataLabels: {
	                enabled: true
	            }
	        }
	    },
	    series: [{
	        name: 'won',
	        data: highchart_data.won,
	        color: '#28a745'
	    },{
	        name: 'Open',
	        data: highchart_data.open,
	        color: '#007bff'
	    },{
	        name: 'Closed',
	        data: highchart_data.closed,
	        color: '#dc3545'
	    }]
	});
}

function sales_lead_count_highchart(highchart_data) {

	Highcharts.chart('sales_lead_count_highchart', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: 'Lead distribution'
	    },
	    xAxis: {
	        categories: highchart_data.category
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Total number of leads'
	        },
	        stackLabels: {
	            enabled: true,
	            style: {
	                fontWeight: 'bold',
	                color: ( // theme
	                    Highcharts.defaultOptions.title.style &&
	                    Highcharts.defaultOptions.title.style.color
	                ) || 'gray'
	            }
	        }
	    },
	    legend: {
	        align: 'right',
	        x: -30,
	        verticalAlign: 'top',
	        y: 25,
	        floating: true,
	        backgroundColor:
	            Highcharts.defaultOptions.legend.backgroundColor || 'white',
	        borderColor: '#CCC',
	        borderWidth: 1,
	        shadow: false
	    },
	    tooltip: {
	        headerFormat: '<b>{point.x}</b><br/>',
	        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
	    },
	    plotOptions: {
	        column: {
	            stacking: 'normal',
	            dataLabels: {
	                enabled: true
	            }
	        }
	    },
	    series: [{
	        name: 'Stage - 6',
	        data: highchart_data.stage_6,
	        color: '#33ccff'
	    },{
	        name: 'Stage - 5',
	        data: highchart_data.stage_5,
	        color: '#28a745'
	    },{
	        name: 'Stage - 4',
	        data: highchart_data.stage_4,
	        color: '#007bff'
	    },{
	        name: 'Stage - 3',
	        data: highchart_data.stage_3,
	        color: '#fd7e14'
	    },{
	        name: 'Stage - 2',
	        data: highchart_data.stage_2,
	        color: '#ffc107'
	    },{
	        name: 'Stage - 1',
	        data: highchart_data.stage_1,
	        color: '#969ca0'
	    },{
	        name: 'Stage - 0',
	        data: highchart_data.stage_0,
	        color: '#ff0000'
	    },{
	        name: 'Blank Stage',
	        data: highchart_data.blank_stage,
	        color: '#17a2b8'
	    }]
	});
}

function quotation_proforma_highchart(highchart_data) {

	Highcharts.chart('quotation_proforma_highchart', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: ''
	    },
	    subtitle: {
	        text: ''
	    },
	    accessibility: {
	        announceNewData: {
	            enabled: true
	        }
	    },
	    xAxis: {
	        type: 'category'
	    },
	    yAxis: {
	        title: {
	            text: 'Sales done in USD'
	        }

	    },
	    legend: {
	        enabled: false
	    },
	    plotOptions: {
	        series: {
	            borderWidth: 0,
	            dataLabels: {
	                enabled: true,
	                format: '{point.y:f}'
	            }
	        }
	    },

	    tooltip: {
	        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> USD<br/>'
	    },

	    series: [
	        {
	            name: "OM Tubes",
	            colorByPoint: true,
	            data: highchart_data
	        }
	    ]

	});
}

function production_status_graph(highchart_data, order_type) {

    Highcharts.chart('production_status_graph', {
        chart: {
            type: 'column'
        },
        title: {
            text: order_type+' Details'
        },
        subtitle: {
            text: ''
        },
        accessibility: {
            announceNewData: {
                enabled: true
            }
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Grand Total in $'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '${point.y}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>${point.y}</b><br/>'
        },

        series: [
            {
                name: order_type,
                colorByPoint: true,
                data: highchart_data
            }
        ]
    });
}

function invoice_total_highchart(highchart_data) {
	console.log(highchart_data);
	Highcharts.chart('invoice_total_highchart', {
		chart: {
			type: 'column'
		},
		title: {
			text: 'Total Invoice'
		},
		subtitle: {
			text: ''
		},
		xAxis: {
			categories: [
				'Jan',
				'Feb',
				'Mar',
				'Apr',
				'May',
				'Jun',
				'Jul',
				'Aug',
				'Sep',
				'Oct',
				'Nov',
				'Dec'
			],
			crosshair: true
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Total Invoice is US Dollar'
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				'<td style="padding:0"><b>${point.y:.1f}</b></td></tr>',
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
		plotOptions: {
			column: {
				pointPadding: 0.2,
				borderWidth: 0
			}
		},
		// series: [{"name":2018,"data":[95421,81447,264768,153695,131686,189921,330266,516395,80345,185983,89970,282280]}]
		series: highchart_data
	});
	// Highcharts.chart('invoice_total_highchart', {
	// 	// Create the chart
	//     chart: {
	//         type: 'column'
	//     },
	//     title: {
	//         text: 'Total Invoice for Year: <b>2021</b>'
	//     },
	//     subtitle: {
	//         text: ''
	//     },
	//     accessibility: {
	//         announceNewData: {
	//             enabled: true
	//         }
	//     },
	//     xAxis: {
	//         type: 'category'
	//     },
	//     yAxis: {
	//         title: {
	//             text: 'Total Invoice is Rupees'
	//         }

	//     },
	//     legend: {
	//         enabled: false
	//     },
	//     plotOptions: {
	//         series: {
	//             borderWidth: 0,
	//             dataLabels: {
	//                 enabled: true,
	//                 format: '{point.y}'
	//             }
	//         }
	//     },

	//     tooltip: {
	//         headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	//         pointFormat: '<span style="color:{point.color}">{point.name}</span>: Total Revenue is <b>{point.y}</b><br/>'
	//     },

	//     series: [
	//         {
	//             name: "Total Invoice",
	//             colorByPoint: true,
	//             data: highchart_data
	//         }
	//     ]
	// });
}

//seema khade
function production_client_wise_graph(highchart_data) {

	Highcharts.chart('production_client_graph', {
		chart: {
			type: 'column'
		},
		title: {
			text: 'All Status Details'
		},
		subtitle: {
			text: ''
		},
		accessibility: {
			announceNewData: {
				enabled: true
			}
		},
		xAxis: {
			type: 'category'
		},
		yAxis: {
			title: {
				text: 'Currency Wise Grand Total'
			}

		},
		legend: {
			enabled: false
		},
		plotOptions: {
			series: {
				borderWidth: 0,
				dataLabels: {
					enabled: true,
					format: '{point.currency}{point.y}'
				}
			}
		},

		tooltip: {
			headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.currency}{point.y}</b><br/>'
		},

		series: [
			{
				colorByPoint: true,
				data: highchart_data
			}
		]
	});
}

function quotation_proforma_month_wise_highchart(highchart_data) {
	// console.log(highchart_data);
	Highcharts.chart('quotation_proforma_month_wise_highchart', {
		chart: {
			type: 'column'
		},
		title: {
			text: ''
		},
		subtitle: {
			text: ''
		},
		xAxis: {
			categories: [
				'Jan',
				'Feb',
				'Mar',
				'Apr',
				'May',
				'Jun',
				'Jul',
				'Aug',
				'Sep',
				'Oct',
				'Nov',
				'Dec'
			],
			crosshair: true
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Sales done in USD'
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				'<td style="padding:0"><b>${point.y:.1f}</b></td></tr>',
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
		plotOptions: {
			column: {
				pointPadding: 0.2,
				borderWidth: 0
			}
		},
		series: highchart_data
	});
}

function production_product_family_wise_graph(highchart_data) {

	Highcharts.chart('production_product_family_graph', {
		chart: {
			type: 'column'
		},
		title: {
			text: 'Product Family Details'
		},
		subtitle: {
			text: ''
		},
		accessibility: {
			announceNewData: {
				enabled: true
			}
		},
		xAxis: {
			type: 'category'
		},
		yAxis: {
			title: {
				text: 'Currency Wise Grand Total'
			}

		},
		legend: {
			enabled: false
		},
		plotOptions: {
			series: {
				borderWidth: 0,
				dataLabels: {
					enabled: true,
					format: '{point.currency}{point.y}'
				}
			}
		},

		tooltip: {
			headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.currency}{point.y}</b><br/>'
		},

		series: [
			{
				colorByPoint: true,
				data: highchart_data
			}
		]
	});
}

function production_status_wise_graph(highchart_data) {

	Highcharts.chart('production_product_status_graph', {
		chart: {
			type: 'column'
		},
		title: {
			text: 'Production Status Details'
		},
		subtitle: {
			text: ''
		},
		accessibility: {
			announceNewData: {
				enabled: true
			}
		},
		xAxis: {
			type: 'category'
		},
		yAxis: {
			title: {
				text: 'Currency Wise Grand Total'
			}

		},
		legend: {
			enabled: false
		},
		plotOptions: {
			series: {
				borderWidth: 0,
				dataLabels: {
					enabled: true,
					format: '{point.currency}{point.y}'
				}
			}
		},

		tooltip: {
			headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.currency}{point.y}</b><br/>'
		},

		series: [
			{
				colorByPoint: true,
				data: highchart_data
			}
		]
	});
}