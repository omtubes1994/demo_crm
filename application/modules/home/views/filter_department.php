<h6 class="dropdown-header">Select Department</h6>
<a class="dropdown-item daily_report_department  <?php echo ($department_value == 'All')? 'disabled':' '?>" department_value="All">All</a>
<?php foreach($department_list as $department_details) {?>
	<a class="dropdown-item <?php echo $select_year;?> <?php echo ($department_value == $department_details['department'])? 'disabled':' '?>" department_value="<?php echo $department_details['department'];?>"><?php echo $department_details['department'];?></a>
<?php }?>