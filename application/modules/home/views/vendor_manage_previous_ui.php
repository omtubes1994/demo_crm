<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand flaticon2-line-chart"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Product List
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						<div class="kt-portlet__head-wrapper">
							<a href="javascript:void();" class="btn btn-default" data-toggle="modal" data-target="#add_product">
									<i class="la la-cart-plus"></i> Add Product
								</a>
							<div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-xl-none"></div>
						</div>
					</div>
				</div>
				<div class="kt-portlet__body kt-portlet__body--fit">

					<!--begin: Datatable -->
					<div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded">
						<table class="kt-datatable__table" style="display: block;">
							<thead class="kt-datatable__head">
								<tr class="kt-datatable__row" style="left: 0px;">
									<th class="kt-datatable__cell kt-datatable__cell--sort" style="width: 20%;">
										<span style="padding: 0% 15%;">Number</span>
									</th>
									<th class="kt-datatable__cell kt-datatable__cell--sort" style="padding: 0% 0% 0% 5%">
										<span>Product Name</span>
									</th>
									<th data-autohide-disabled="false" class="kt-datatable__cell kt-datatable__cell--sort" style="width: 25%;">
										<span>Added Date</span>
									</th>
									<th data-autohide-disabled="false" class="kt-datatable__cell kt-datatable__cell--sort" style="width: 20%;">
										<span>Actions</span>
									</th>
								</tr>
							</thead>
							<tbody class="kt-datatable__body kt-scroll" data-scroll="true" style="height: 630px">
								<?php foreach($product_list as $product_key => $product_details) {?>
								<tr data-row="0" class="kt-datatable__row kt-datatable__row--active" style="left: 0px;">
									<td class="kt-datatable__cell" style="width: 20%;">
										<span style="padding: 0% 15%;"><?php echo $product_key+1; ?></span>
									</td>
									<td class="kt-datatable__cell" style="padding: 0% 0% 0% 5%">
										<span><?php echo $product_details['lookup_value']; ?></span>
									</td>
									<td class="kt-datatable__cell" style="width: 25%;">
										<span>
											<span class="kt-badge kt-badge--primary kt-badge--dot"></span>&nbsp;
											<span class="kt-font-bold kt-font-primary"><?php echo date('F, j Y', strtotime($product_details['entered_on'])); ?></span>
										</span>
									</td>
									<td class="kt-datatable__cell" style="width: 20%;">
										<span style="overflow: visible; position: relative;">
											<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_vendor_product" title="Delete" product-id="<?php echo $product_details['lookup_id']?>">
												<i class="la la-trash"></i>
											</a>
										</span>
									</td>
								</tr>
								<?php } ?>
						  	</tbody>
					  	</table>
				  	</div>

					<!--end: Datatable -->
				</div>