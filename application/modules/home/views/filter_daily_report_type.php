<h6 class="dropdown-header">Select Report Type</h6>
<?php foreach($daily_report_list as $daily_report_key => $daily_report_value) {?>
	<a 
	class="dropdown-item daily_report_type  <?php echo ($daily_report_type == $daily_report_key)? 'disabled':' '?>" 
	filter-report-type="<?php echo $daily_report_key?>">
		<?php echo $daily_report_value?>
	</a>
<?php }?>