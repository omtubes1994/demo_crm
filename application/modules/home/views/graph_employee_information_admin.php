<div class="kanban-item dark-light" style="cursor: pointer; width:100%; padding: 10px 5px; <?php echo $style; ?>">
    <div class="kt-kanban__badge" style="justify-content: center; height: 150px;">
        <div class="kt-kanban__content">
            <div class="kt-kanban__title kt-font-xl" style="height: 30px;"><?php echo $name; ?></div>
            <div class="kt-kanban__title kt-font-boldest" style="color:#343a40; height: 60px;"><?php echo $designation; ?></div>
            <hr style="margin:0px; border-top: 1px solid #343a40;">
            <div class="kt-section__content kt-section__content--solid" style="margin-top:0.5rem;">
                &nbsp;
                <button type="button" class="btn btn-outline-dark btn-elevate btn-icon get_org_form_details" data-toggle="kt-tooltip" data-placement="left" title="" data-original-title="Update User" style="height:2rem; width:2rem;" form_call_type= "edit_main" org_chart_id="<?php echo $id; ?>" reporting_manager_id="<?php echo $reporting_manager_id; ?>">
                    <i class="flaticon2-edit"></i>
                </button>
                &nbsp;
            </div>
        </div>
    </div>
</div>