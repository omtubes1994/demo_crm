<h6 class="dropdown-header">Select Year</h6>
<a class="dropdown-item <?php echo $select_year;?>  <?php echo ($year_value == 'All')? 'disabled':' '?>" year_value="All">All</a>
<?php foreach($years_list as $year_details) {?>
	<a class="dropdown-item <?php echo $select_year;?> <?php echo ($year_value == $year_details['year'])? 'disabled':' '?>" year_value="<?php echo $year_details['year'];?>"><?php echo $year_details['year'];?></a>
<?php }?>