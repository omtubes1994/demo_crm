<?php foreach ($user_junior_list as $user_junior_list_key => $single_user_junior_list) { ?>
    <?php $current_key = $user_junior_list_key+1; ?>
    <tr>
        <td><?php echo $current_key; ?></td>
        <td>
            <select class="col-xl-12 form-control lead_select_picker" name="<?php echo 'name_'.$current_key; ?>">
                <option value="">Select Name</option>
                <?php foreach($user_list as $single_user_list){ ?>
                            
                    <option 
                    value="<?php echo $single_user_list['value'].'_'.$single_user_list['name']?>"
                    <?php echo ($single_user_junior_list['user_id'] == $single_user_list['value']) ? 'selected' : ''; ?>
                    >
                        <?php echo $single_user_list['name']?>
                    </option>
                <?php } ?>         
            </select>
        </td>
        <td>
            <select class="col-xl-12 form-control lead_select_picker" name="<?php echo 'designation_'.$current_key; ?>">
                <option value="">Select Designation</option>
                <?php foreach($user_designation_list as $single_user_designation_list){ ?>
                            
                    <option 
                    value="<?php echo $single_user_designation_list['name']?>"
                    <?php echo ($single_user_junior_list['designation'] == $single_user_designation_list['name']) ? 'selected' : ''; ?>
                    >
                        <?php echo $single_user_designation_list['name']?>
                    </option>
                <?php } ?>
            </select>
        </td>
        <td>
            <select class="col-xl-12 form-control lead_select_picker" name="<?php echo 'order_id_'.$current_key; ?>">
                <option value="">Select Order</option>
                <?php foreach($org_chart_list_junior as $single_org_chart_list){ ?>
                            
                    <option 
                    value="<?php echo $single_org_chart_list['value']?>"
                    <?php echo ($single_user_junior_list['order_id'] == $single_org_chart_list['value']) ? 'selected' : ''; ?>
                    >
                        <?php echo $single_org_chart_list['name']?>
                    </option>
                <?php } ?>
            </select>
            <input type="hidden" class="" name="<?php echo 'order_id_old_'.$current_key; ?>" value="<?php echo $single_user_junior_list['order_id']; ?>">
            <input type="hidden" class="" name="<?php echo 'reporting_manager_id_'.$current_key; ?>" value="<?php echo $single_user_junior_list['reporting_manager_id']; ?>">
        </td>
        <td>
            <button type="button" class="btn btn-primary save_org_details_junior" current_key="<?php echo $current_key; ?>" org_chart_id="<?php echo $single_user_junior_list['id']; ?>">Save</button>
        </td>';
    </tr>
<?php } ?>
