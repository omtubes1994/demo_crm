<html>
  <head>
    <style>
      .org-chart {
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
      }
      .org-chart .node {
        width: 200px;
        height: 50px;
        border: 1px solid black;
        text-align: center;
        line-height: 50px;
      }
      .org-chart .node.superior {
        width: 100%;
        height: 100px;
        border: 1px solid black;
        text-align: center;
        line-height: 100px;
      }
    </style>
  </head>
  <body>
    <div class="org-chart">
      <div class="node superior">CEO</div>
      <div class="node">
        Director 1
        <div class="node">Manager 1</div>
        <div class="node">Manager 2</div>
      </div>
      <div class="node">
        Director 2
        <div class="node">Manager 3</div>
        <div class="node">Manager 4</div>
      </div>
    </div>
  </body>
</html>
