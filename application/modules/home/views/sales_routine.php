<?php  foreach ($routine_list as $routine_list_name => $routine_list_array) { ?>
	<div class="col-lg-6">
		<!--begin:: Widgets/Tasks -->
		<div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?php
							$explode_routine_name = explode("_", $routine_list_name);
							foreach ($explode_routine_name as $single_routine_name) {
								
								echo ucfirst($single_routine_name)," ";
							}
						?>
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar"></div>
			</div>
			<div class="kt-portlet__body" style="padding: 25px 0px 0px 10px;">
				<div class="tab-content">
					<div class="tab-pane active" id="kt_widget2_tab1_content">
						<div class="kt-widget2">
							<?php  foreach ($routine_list_array as $single_routine_details) { ?>
							<div class="kt-widget2__item <?php echo $single_routine_details['color']; ?>">
								<div class="kt-widget2__checkbox"></div>
								<div class="kt-widget2__info kt-align-left" style="width:70%">
									<a href="javascript:void(0);" class="kt-widget2__title">
										<?php echo $single_routine_details['sales_routine_name']; ?>
									</a>
									<a href="javascript:void(0);" class="kt-widget2__username">.</a>
								</div>
								<div class="kt-widget2__actions">
									
									<div class="dropdown dropdown-inline" data-toggle="kt-tooltip-" title="Update Routine Status" data-placement="left">
										<a href="javascript:void(0);" class="btn btn-icon save_daily_work_on_sales_routine" sales_routine_id="<?php echo $single_routine_details['sales_routine_id']; ?>" work_completed="<?php echo $single_routine_details['work_completed']; ?>" work_comment="<?php echo $single_routine_details['work_comment']; ?>">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--primary kt-svg-icon--md">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<polygon points="0 0 24 0 24 24 0 24" />
													<path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
													<path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" fill="#000000" />
												</g>
											</svg>
										</a>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!--end:: Widgets/Tasks -->
	</div>
<?php } ?>