<!DOCTYPE html>
<html>
  <head>
    <script src="https://d3js.org/d3.v6.min.js"></script>
    <style>
      .node circle {
        fill: #fff;
        stroke: steelblue;
        stroke-width: 3px;
      }

      .node text {
        font-size: 16px;
      }

      .link {
        fill: none;
        stroke: #ccc;
        stroke-width: 2px;
      }
    </style>
  </head>
  <body>
    <div id="chart"></div>
    <script>
      var data = {
        name: "CEO",
        children: [
          {
            name: "COO",
            children: [
              { name: "HR" },
              { name: "Operations" }
            ]
          },
          {
            name: "CFO",
            children: [
              { name: "Finance" },
              { name: "Accounting" }
            ]
          },
          {
            name: "CMO",
            children: [
              { name: "Marketing" },
              { name: "Sales" }
            ]
          }
        ]
      };

      var svg = d3.select("#chart")
        .append("svg")
        .attr("width", 1000)
        .attr("height", 1000),
        width = +svg.attr("width"),
        height = +svg.attr("height");

      var g = svg.append("g")
        .attr("transform", "translate(40,0)");

      var tree = d3.tree()
        .size([height, width - 160]);

      var root = d3.hierarchy(data);
      tree(root);

      var link = g.selectAll(".link")
        .data(root.descendants().slice(1))
        .enter().append("path")
        .attr("class", "link")
        .attr("d", function(d) {
          return "M" + d.y + "," + d.x
            + "C" + (d.parent.y + 100) + "," + d.x
            + " " + (d.parent.y + 100) + "," + d.parent.x
            + " " + d.parent.y + "," + d.parent.x;
        });

      var node = g.selectAll(".node")
        .data(root.descendants())
        .enter().append("g")
        .attr("class", function(d) {
          return "node" + (d.children ? " node--internal" : " node--leaf");
        })
        .attr("transform", function(d) {
          return "translate(" + d.y + "," + d.x + ")";
        });

      node.append("circle")
        .attr("r", 10);

      node.append("text")
        .attr("dy", ".35em")
        .attr("x",