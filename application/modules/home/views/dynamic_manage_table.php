<div class="kt-portlet__head kt-portlet__head--lg">
	<div class="kt-portlet__head-label">
		<span class="kt-portlet__head-icon">
			<i class="kt-font-brand flaticon2-line-chart"></i>
		</span>
		<h3 class="kt-portlet__head-title">
			<?php echo $view_details['view_name']; ?>
		</h3>
	</div>
	<?php if($view_details['add_new_flag']) {?>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<a href="javascript:void();" class="btn btn-default" data-toggle="modal" data-target="<?php echo $view_details['add_new_modal_name']; ?>">
					<i class="la la-cart-plus"></i>
					<?php echo $view_details['add_new_name']; ?>
				</a>
				<div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-xl-none"></div>
			</div>
		</div>
	<?php }?>	
</div>
<div class="kt-portlet__body kt-portlet__body--fit">

	<?php if($view_details['table_listing_flag']) {?>
		<!--begin: Datatable -->
		<div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded">
			<table class="kt-datatable__table" style="display: block;">
				<thead class="kt-datatable__head">
					<tr class="kt-datatable__row" style="left: 0px;">
						<?php foreach($view_details['table_header_column_array'] as $th) {?>
							<th class="kt-datatable__cell kt-datatable__cell--sort" style="<?php echo $th['th_style'];?>">
								<span style="<?php echo $th['th_span_style'];?>"><?php echo $th['th_value'];?></span>
							</th>
						<?php }?>
					</tr>
				</thead>
				<!-- <tbody class="kt-datatable__body kt-scroll" data-scroll="true" style="height: 630px"> -->
				<tbody class="kt-datatable__body">
					<?php if(!empty($view_details['table_body'])) {?>
						<?php foreach($view_details['table_body'] as $single_details) {?>
							<?php if(in_array($current_tab, array('product', 'material', 'transport_mode', 'delivery_time', 'payment_terms', 'validity','origin_country', 'mtc_type', 'lead_stage_reasons','quotation_close_reasons','rfq_close_reasons'))) {?>
								<tr data-row="0" class="kt-datatable__row kt-datatable__row--active" style="left: 0px;">
									<td class="kt-datatable__cell" style="width: 20%;">
										<span style="padding: 0% 15%;"><?php echo $single_details['id']; ?></span>
									</td>
									<td class="kt-datatable__cell" style="width: 30%;">
										<span><?php echo $single_details['name']; ?></span>
									</td>
									<td class="kt-datatable__cell" style="width: 30%;">
										<span>
											<span class="kt-badge kt-badge--primary kt-badge--dot"></span>&nbsp;
											<span class="kt-font-bold kt-font-primary"><?php echo $single_details['date']; ?></span>
										</span>
									</td>
									<td class="kt-datatable__cell" style="width: 20%;">
										<span style="overflow: visible; position: relative;">
											<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md delete" delete_name="<?php echo $view_details['delete_name']; ?>" title="Delete" id="<?php echo $single_details['delete_id']?>">
												<i class="la la-trash"></i>
											</a>
										</span>
									</td>
								</tr>
							<?php } else if (in_array($current_tab, array('delivery', 'currency'))) { ?>
								<tr data-row="0" class="kt-datatable__row kt-datatable__row--active" style="left: 0px;">
									<td class="kt-datatable__cell" style="width: 20%;">
										<span style="padding: 0% 15%;"><?php echo $single_details['id']; ?></span>
									</td>
									<td class="kt-datatable__cell" style="width: 20%;">
										<span><?php echo $single_details['name']; ?></span>
									</td>
									<td class="kt-datatable__cell" style="width: 20%;">
										<span>
											<span class="kt-font-bold kt-font-success" style="font-weight: 900 !important;"><?php echo $single_details['name_2']; ?></span>
										</span>
									</td>
									<td class="kt-datatable__cell" style="width: 20%;">
										<span>
											<span class="kt-badge kt-badge--primary kt-badge--dot"></span>&nbsp;
											<span class="kt-font-bold kt-font-primary"><?php echo $single_details['date']; ?></span>
										</span>
									</td>
									<td class="kt-datatable__cell" style="width: 20%;">
										<span style="overflow: visible; position: relative;">
											<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md delete" delete_name="<?php echo $view_details['delete_name']; ?>" title="Delete" id="<?php echo $single_details['delete_id']?>">
												<i class="la la-trash"></i>
											</a>
										</span>
									</td>
								</tr>
							<?php } ?>
						<?php } ?>
					<?php } ?>
			  	</tbody>
		  	</table>
	  	</div>

		<!--end: Datatable -->
	<?php } else {?>
		<div class="alert alert-light alert-elevate fade show" role="alert">
			<div class="alert-text terms_conditions_alert">
				<?php echo $view_details['table_body'];?>
			</div>
			<div class="alert-icon"  data-toggle="modal" data-target="#edit_terms_and_conditions"><i class="flaticon-edit-1 kt-font-brand edit_terms_and_conditions"></i></div>
		</div>
	<?php }?>	
</div>