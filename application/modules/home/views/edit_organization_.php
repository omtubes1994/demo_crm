<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12 col-xl-12 order-lg-1 order-xl-1">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Edit Form
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right" id="org_chart_info_form" >
                        <div class="kt-portlet__body">
                            <div class="form-group row">
                                <div class="col-lg-4"> 
                                    <label class="">user</label>
                                    <?php if(!empty($this->uri->segment(3,0))){ ?>
                                    <input type="text" class="form-control" name="user_id" value="<?php echo $organization_chart_data['user_id'];?>" hidden>
                                    <input type="text" class="form-control" value="<?php echo $organization_chart_data['user_name'];?>" readonly>
                                        <?php } else if(empty($this->uri->segment(3,0))){
                                            ?>
                                            <select class="form-control" name="user_id">
                                          <option value="">Select User</option>
                                         <?php foreach($users as $key=> $single_user_details){?> <option value="<?php echo $key?>"
                                        data-tokens="<?php echo $single_user_details;?>">
                                        <?php echo $single_user_details; ?>
                                        </option>
                                        <?php } }
                                        ?>                                 
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label class="">reporting manager</label>
                                    <select class="form-control" name="reporting_manager">
                                        <option value="">Select User</option>
                                         <?php if(!empty($this->uri->segment(3,0))){ foreach($organization_user as $key=> $single_user_details){?>
                                          <option value="<?php echo $single_user_details ?>"
                                        <?php echo($organization_chart_data['reporting_manager'] == $users[$single_user_details])?'selected':'';?>
                                        data-tokens="<?php $users[$single_user_details];?>">
                                        <?php echo $users[$single_user_details]; ?>
                                        </option>
                                        <?php } }else if(empty($this->uri->segment(3,0))){
                                            ?>
                                          <option value="">Select User</option>
                                         <?php foreach($users as $key=> $single_user_details){?> <option value="<?php echo $key?>"
                                        data-tokens="<?php echo $single_user_details;?>">
                                        <?php echo $single_user_details; ?>
                                        </option>
                                        <?php } }
                                        ?>                                 
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label class="">Designation</label>
                                    <?php if(!empty($this->uri->segment(3,0))){?>
                                    <input type="text" class="form-control" value="<?php echo $organization_chart_data['user_designation'];?>" readonly>
                                    <?php }else if(empty($this->uri->segment(3,0))){?>
                                     <select class="form-control" name="user_designation" readonly>
                                        <option value="">Select Designation</option>
                                         <?php foreach($organization_designation as  $single_organization_designation){?> <option value="<?php echo $single_organization_designation?>"
                                        <?php echo($organization_chart_data['user_designation'] == $single_organization_designation)?'selected':'';?>
                                        data-tokens="<?php echo $single_organization_designation;?>">
                                        <?php echo $single_organization_designation; ?>
                                        </option>
                                        <?php } }?>                                 
                                    </select> 
                                </div>  
                            </div>
                            <div class="form-group row">
                                
                                <div class="col-lg-4">
                                    <label class="">Department</label>
                                    <?php if(!empty($this->uri->segment(3,0))){?>
                                    <input type="text" class="form-control" value="<?php echo $organization_chart_data['user_department'];?>" readonly>
                                    <?php }else if(empty($this->uri->segment(3,0))){?>
                                     <select class="form-control" name="user_department" readonly>
                                        <option value="">Select Department</option>
                                         <?php foreach($organization_user_department as  $single_organization_user_department){?> <option value="<?php echo $single_organization_user_department?>"
                                        <?php echo($organization_chart_data['user_department'] == $single_organization_user_department)?'selected':'';?>
                                        data-tokens="<?php echo $single_organization_user_department;?>">
                                        <?php echo $single_organization_user_department; ?>
                                        </option>
                                        <?php } }?>                                 
                                    </select> 
                                </div>
                                <div class="col-lg-4 level_org">
                                    <label class="">level</label>
                                    <select class="form-control" name="user_reporting_level" id="user_reporting_level">
                                        <option value="">Select level</option>
                                         <?php  foreach($organization_user_reporting_level as $single_organization_user_reporting_level){?> <option value="<?php echo $single_organization_user_reporting_level?>"
                                        <?php echo($organization_chart_data['user_reporting_level'] == $single_organization_user_reporting_level)?'selected':'';?>
                                        data-tokens="<?php echo $single_organization_user_reporting_level;?>"> level
                                        <?php echo $single_organization_user_reporting_level; ?>
                                        </option>
                                        <?php }?>                                             
                                    </select>
                                </div>
                                <div class="col-lg-4 next_to" style="display:none;">
                                    <label class="">Next to</label>
                                    <select class="form-control" name="next_to" >
                                        <option value="">Select User</option>
                                         <?php foreach($users as $key=> $single_user_details){?> <option value="<?php echo $key?>"
                                        data-tokens="<?php echo $single_user_details;?>">
                                        <?php echo $single_user_details; ?>
                                        </option>
                                        <?php }?>                                 
                                    </select>
                                </div>
                            </div>           
                        </div>
                    </form>
                    <div class="kt-portlet__foot add_user">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary save_org_data" id="<?php echo $organization_chart_data['id']; ?>">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>