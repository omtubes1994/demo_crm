<div class="row" style="display: block;">
		<div class="col-xl-6">
			<!--Begin::Portlet-->
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Follup Up Pending
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						<ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
							<li class="nav-item">
								<a class="nav-link change_quotation_pending_for_follow_up_date active" filter-date="current" data-toggle="tab" href="#kt_widget11_tab1_content" role="tab">
									Today
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link change_quotation_pending_for_follow_up_date" filter-date="week" data-toggle="tab" href="#kt_widget11_tab1_content" role="tab" >
									Week 
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link change_quotation_pending_for_follow_up_date" filter-date="month" data-toggle="tab" href="#kt_widget11_tab1_content" role="tab">
									Month
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link change_quotation_pending_for_follow_up_date" filter-date="all" data-toggle="tab" href="#kt_widget11_tab2_content" role="tab">
									All Time
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="kt-portlet__body">
					<!--Begin::Timeline 3 -->	
					<div class="kt-timeline-v2 kt-scroll" id="quotation_pending_for_follow_up_div_body"  data-scroll="true" style="height: 582px">
						<?php $this->load->view('home/quotation_pending_for_follow_up_div_body');?>
					</div>

					<!--End::Timeline 3 -->
				</div>
			</div>

			<!--End::Portlet-->
		</div>
		<div class="col-xl-6">

			<!--Begin::Portlet-->
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Quotation Ready List
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						<ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
							<li class="nav-item">
								<a class="nav-link change_quotation_ready_date active" filter-date="current" data-toggle="tab" href="#kt_widget11_tab1_content" role="tab">
									Today
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link change_quotation_ready_date" filter-date="week" data-toggle="tab" href="#kt_widget11_tab1_content" role="tab" >
									Week 
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link change_quotation_ready_date" filter-date="month" data-toggle="tab" href="#kt_widget11_tab1_content" role="tab">
									Month
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link change_quotation_ready_date" filter-date="all" data-toggle="tab" href="#kt_widget11_tab2_content" role="tab">
									All Time
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="kt-portlet__body">
					<!--Begin::Timeline 3 -->	
					<div class="kt-timeline-v2 kt-scroll" id="quotation_list_div_body"  data-scroll="true" style="height: 582px">
						<?php $this->load->view('home/quotation_list_div_body');?>
					</div>

					<!--End::Timeline 3 -->
				</div>
			</div>

			<!--End::Portlet-->
		</div>
	</div>

<div class="col-6" style="display:none;">
	<!--Begin::Portlet-->

	<div class="kt-portlet kt-portlet--height-fluid">
		<div class="kt-portlet__head kt-portlet__head--right kt-portlet__head--noborder  kt-ribbon kt-ribbon--clip kt-ribbon--left kt-ribbon--info">
			<?php if(!empty($todays_birthday_information)) { $top_increment = 12?>
				<?php foreach($todays_birthday_information as $todays_birthday_information_details) {?>
					<div class="kt-ribbon__target" style="top: <?php echo $top_increment;?>px;">
						<span class="kt-ribbon__inner"></span>Today is <?php echo $todays_birthday_information_details['first_name'], ' ',$todays_birthday_information_details['last_name'];?> Birthday.
					</div>
				<?php $top_increment = $top_increment+40;}?>
			<?php }?>
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					Last Login History
				</h3>
			</div>
		</div>
		<div class="kt-portlet__body">
			<!--Begin::Timeline 3 -->	
			<div class="row d-flex justify-content-center">
				<img src="<?php echo base_url('assets/media/logos/OM-Logo.png'); ?>" width="400" height="120" alt="OTF Logo"/>
			</div>
			<br/><br/>
			<h4>Welcome: <?php echo $this->session->userdata('name');?></h4>
			<table class="table table-bordered">
				<tr>
					<td>Current Login Time: <?php echo date('d M Y H:i', strtotime($this->session->userdata('current_login')));?></td>
					<td>Current IP Address: <?php echo $this->session->userdata('current_ip');?></td>
				</tr>
				<tr>
					<td>Last Login Time: <?php echo date('d M Y H:i', strtotime($this->session->userdata('last_login')));?></td>
					<td>Last IP Address: <?php echo $this->session->userdata('last_ip');?></td>
				</tr>
			</table>

			<!--End::Timeline 3 -->
		</div>
	</div>
	<!--End::Portlet-->
</div>
	