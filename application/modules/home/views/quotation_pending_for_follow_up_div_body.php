<div class="kt-timeline-v2__items  kt-padding-top-25 kt-padding-bottom-30">
	<?php if(!empty($follow_up_list)) {?>
	<?php foreach($follow_up_list as $follow_up_date => $follow_up_details) {?>	
		<!-- <div class="kt-timeline-v2__item"> -->
			<span class="kt-timeline-v2__item-time kt-font-bolder" style="width: 100%"><?php echo date('F, j', strtotime($follow_up_date));?></span>
			<!-- <div class="kt-timeline-v2__item-cricle"> -->
				<!-- <span class="kt-timeline-v2__item-time"><?php echo $follow_up_date; ?></span> -->
			<!-- </div> -->
			<!-- <div class="kt-timeline-v2__item-text kt-padding-top-5">
			</div> -->
		<!-- </div> -->
		<?php foreach($follow_up_details as $follow_up_single_details) {?>
			<div class="kt-timeline-v2__item">
				<!-- <span class="kt-timeline-v2__item-time"><?php echo date('F, j', strtotime($follow_up_date));?></span> -->
				<div class="kt-timeline-v2__item-cricle">
					<i class="fa fa-genderless <?php echo $image_not_found_array[$image_not_found_array_key%3]; ?>"></i>
				</div>
				<div class="kt-timeline-v2__item-text kt-padding-top-5  kt-font-bolder kt-font-dark">
					Quotation Number <a href="#" class="kt-link kt-link--brand kt-font-bolder"><?php echo $follow_up_single_details['quote_no']; ?></a> is pending for follup up.
				</div>
			</div>		
		<?php $image_not_found_array_key++;}?>	
	<?php }?>	
	<?php } else {?>	
		<div class="kt-timeline-v2__item">
			<span class="kt-timeline-v2__item-time"></span>
			<div class="kt-timeline-v2__item-cricle">
				<i class="fa fa-genderless"></i>
			</div>
			<div class="kt-timeline-v2__item-text kt-padding-top-5  kt-font-bolder kt-font-dark">
				No Quotation Pending for Follow Up Found. Enjoy your day !!!.
			</div>
		</div>
	<?php }?>	
</div>