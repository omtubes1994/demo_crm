<style type="text/css">
	.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
		padding-top: 4% !important;
	}
	.layer-white{
	    display: none;
	    position: absolute;
	    top: 0em !important;
	    left: 0em !important;
	    width: 100%;
	    height: 100%;
	    text-align: center;
	    vertical-align: middle;
	    background-color: rgba(255, 255, 255, 0.55);
	    opacity: 1;
	    line-height: 1;
	    -webkit-animation-fill-mode: both;
	    animation-fill-mode: both;
	    -webkit-animation-duration: 0.5s;
	    animation-duration: 0.5s;
	    -webkit-transition: background-color 0.5s linear;
	    transition: background-color 0.5s linear;
	    -webkit-user-select: none;
	    -moz-user-select: none;
	    -ms-user-select: none;
	    user-select: none;
	    will-change: opacity;
	    z-index: 9;
	}
	.div-loader{
	    position: absolute;
		top: 50%;
		left: 50%;
		margin: 0px;
		text-align: center;
		z-index: 1000;
		-webkit-transform: translateX(-50%) translateY(-50%);
		transform: translateX(-50%) translateY(-50%);
	}
	.kt-spinner:before {
	    width: 50px;
	    height: 50px;
	    margin-top: -10px;
	}
	tbody#production_listing tr:hover {
		background-color: gainsboro;
	}
	tbody#production_listing tr:hover .first_div {
	    border-left: 0.25rem solid #5578eb !important;
	}
	tbody#production_listing td {
		font-style: normal;
		font-size: 15px;
	    border: 0.05rem solid gainsboro;
	}
	tbody#production_listing td span{
		/*width:200px !important;*/
		display: inline-flex;
	    width: 100%;
	}
	tbody#production_listing td span i{
		cursor: pointer;
	}
	tbody#production_listing td span abbr{
	    width: 100%;
	}    
	tbody#production_listing td span abbr em{
		display: block;
		color: #898989;
		margin: 3px 0 3px 0;
		font-style: normal;
		font-family: Helvetica Neue,Helvetica,Arial,sans-serif
	}
	tbody#production_listing td span abbr em i{
		font-weight: 500;
	    color: #898989;
	    margin: 3px 0 3px 0;
	    font-style: normal;
	    font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
	}
	thead#production_header tr th{
	    background: gainsboro;
	    color: #767676;
	    font-size: 14px;
	    font-family: 'latomedium';
	    padding: 10px 20px;
	    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
	}
	ul.menu-tab {
		margin: 0px;
	    padding: 0px;
	    border: 1px solid #E7E7E7;
	    /*border-radius: 50px;*/
	    overflow: hidden;
	    font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
	    font-size: 14px;
	    line-height: 20px;
	    color: #333;
	}
	ul.menu-tab li{
		width: calc(100%/6) !important;
	    display: inline;
	    text-align: center;
	    float: left;
	}
	ul.menu-tab li.active_list{
	    border-bottom: 0.25rem solid #767676 !important;
	}
	ul.menu-tab li a{
		cursor: pointer;
	    display: inline-block;
	    outline: none;
	    text-align: center;
	    width: 100%;
	    background: #F5F5F5;
	    /*border-right: 2px solid #fff;*/
	    color: #767676;
	    font-size: 15px;
	    font-family: 'latomedium';
	    background-color: gainsboro;
	    padding: 3px 3px 12px 3px;
	}
	ul.menu-tab li a i{
		display: inline-block;
	    width: 35px;
	    height: 28px;
	    font-style: normal;
	    background-size: 100%;
	    position: relative;
	    top: 6px;
	}
	.form_name{
	    color: #767676 !important;
	    font-size: 14px !important;
	    font-family: 'latomedium' !important;
	    padding: 10px 10px !important; 
	    font-weight: bold !important;
	}
	.kt-badge--gainsboro, .kt-font-gainsboro{
		background-color: gainsboro;
	}
	.production_title_name{
		font-weight: 700 !important;
		font-size: 1.5rem;
		display: block;
		color: #898989;
		margin: 3px 0 3px 0;
		font-style: normal;
		font-family: Helvetica Neue,Helvetica,Arial,sans-serif
	}
	.production_title_value{
		font-weight: 500;
		font-size: 1.75rem;
	    color: #898989;
	    margin: 3px 0 3px 0;
	    font-style: normal;
	    font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
	}
	.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
		padding-top: 2% !important;
	}
	:root {
        --level-1: #8dccad;
        --level-2: #f5cc7f;
        --level-3: #7b9fe0;
        --level-4: #f27c8d;
        --black: black;
	}
	* {
        padding: 0;
        margin: 0;
        box-sizing: border-box;
	}
	ol {
	    list-style: none;
	}
	body {
        margin: 50px 0 100px;
        text-align: center;
        font-family: "Inter", sans-serif;
	}
    .level-one {
        width: 50%;
        margin: 0 auto 40px;
        background: var(--level-1);
	}
	.level-one::before {
        content: "";
        position: absolute;
        top: 100%;
        left: 50%;
        transform: translateX(-50%);
        width: 2px;
        height: 20px;
        background: black;
	}
	.level-two-wrapper {
        position: relative;
        display: grid;
        grid-template-columns: repeat(10, 1fr);
    }
    .level-two-wrapper::before {
        content: "";
        position: absolute;
        top: -12%;
        left: 5%;
        width: 90%;
        height: 2px;
        background: black;
    }
    .level-two-wrapper >li:before {
        content: "";
        position: absolute;
        bottom: 98%;
        transform: translateX(-50%);
        width: 2px;
        height: 25px;
        background: black;
    }
    <?php if(!empty($level_three_employee_list)){ ?>
        .level-two-wrapper >li::after {
            content: "";
            position: absolute;
            bottom: -10%;
            left: <?php echo $department_left; ?>;
            transform: translateX(-50%);
            width: 2px;
            height: 1.5rem;
            background: black;
        }
        .level-three-wrapper {
            position: relative;
            display: grid;
            grid-template-columns: repeat(10, 1fr);
        }
        .level-three-wrapper:before {
            content: "";
            position: absolute;
            top: -20px;
            left: 5%;
            width: 90%;
            height: 2px;
            background: black;
        }
        .level-three-wrapper >li::before {
            content: "";
            position: absolute;
            bottom: 98%;
            transform: translateX(-50%);
            width: 2px;
            height: 25px;
            background: black;
        }
    <?php } ?>
	<?php if(!empty($level_four_employee_list)){ ?>
			.level-three-wrapper >li::after {
				content: "";
				position: absolute;
				bottom: -10%;
				left: <?php echo $department_left_1;?>;
				transform: translateX(-50%);
				width: 2px;
				height: 1.5rem;
				background: black;
			}
			.level-four-wrapper {
				position: relative;
				display: grid;
				grid-template-columns: repeat(10, 1fr);
			}
			.level-four-wrapper:before {
				content: "";
				position: absolute;
				top: -20px;
				left: 5%;
				width: 90%;
				height: 2px;
				background: black;
			}
			.level-four-wrapper >li::before {
				content: "";
				position: absolute;
				bottom: 98%;
				transform: translateX(-50%);
				width: 2px;
				height: 25px;
				background: black;
			}
	<?php } ?>
	<?php if(!empty($level_five_employee_list)){ ?>
			.level-four-wrapper >li::after {
				content: "";
				position: absolute;
				bottom: -10%;
				left: <?php echo $department_left_2; ?>;
				transform: translateX(-50%);
				width: 2px;
				height: 1.5rem;
				background: black;
			}
			.level-five-wrapper {
				position: relative;
				display: grid;
				grid-template-columns: repeat(10, 1fr) 
			}
			.level-five-wrapper:before {
				content: "";
				position: absolute;
				top: -20px;
				left: 5%;
				width: 90%;
				height: 2px;
				background: black;
			}
			.level-five-wrapper >li::before {
				content: "";
				position: absolute;
				bottom: 98%;
				transform: translateX(-50%);
				width: 2px;
				height: 25px;
				background: black;
			}
	<?php } ?>
	<?php if(!empty($level_six_employee_list)){ ?>
			.level-five-wrapper >li::after {
				content: "";
				position: absolute;
				bottom: -10%;
				left: <?php echo $department_left_3;?>;
				transform: translateX(-50%);
				width: 2px;
				height: 1.5rem;
				background: black;
			}
			.level-six-wrapper {
				position: relative;
				display: grid;
				grid-template-columns: repeat(10, 1fr) 
			}
			.level-six-wrapper:before {
				content: "";
				position: absolute;
				top: -20px;
				left: 5%;
				width: 90%;
				height: 2px;
				background: black;
			}
			.level-six-wrapper >li::before {
				content: "";
				position: absolute;
				bottom: 98%;
				transform: translateX(-50%);
				width: 2px;
				height: 25px;
				background: black;
			}
	<?php } ?>
</style>
<link href="assets/plugins/custom/kanban/kanban.bundle.css" rel="stylesheet" type="text/css" />
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		
	<!--Begin::Section-->
	<div class="row">
		<div class="col-12">
			<div class="alert alert-solid-info alert-bold kt-ribbon kt-ribbon--clip kt-ribbon--right kt-ribbon--info" role="alert">
				<?php if(!empty($todays_birthday_information)) { $top_increment = 0?>
					<?php foreach($todays_birthday_information as $todays_birthday_information_details) {?>
						<div class="kt-ribbon__target" style="top: <?php echo $top_increment;?>px;">
							<span class="kt-ribbon__inner"></span>Today is <?php echo $todays_birthday_information_details['first_name'], ' ',$todays_birthday_information_details['last_name'];?> Birthday.
						</div>
					<?php $top_increment = $top_increment+40;}?>
				<?php }?>
				<div class="alert-text"><h4>Welcome: <?php echo $this->session->userdata('name');?></h4></div>
			</div>
		</div>
	</div>

	<!--End::Section-->
	<?php if(!empty($notice_type)){?>
		<!--Begin::Section-->
		<div class="row">
			<div class="col-xl-12">
				<!--begin:: Portlet-->
				<div class="kt-portlet">
					<div class="kt-portlet__body" style="padding: 10px 25px;">
						<div class="kt-widget kt-widget--user-profile-3">
							<div class="kt-widget__top">
								<div class="kt-widget__content">
									<div class="kt-widget__head">
										<a href="#" class="kt-widget__username"></a>
										<div class="kt-widget__action">
											
												<?php foreach ($notice_type as $notice_name => $notice_count) { ?>
													<button type="button" class="btn btn-label-brand btn-sm btn-upper get_notice" notice_type="<?php echo $notice_name; ?>">
														<?php echo $notice_name; ?>
														<span class="kt-badge kt-badge--unified-warning kt-badge--sm kt-badge--rounded kt-badge--bolder">
														<?php echo $notice_count; ?>
														</span>
													</button>&nbsp;
												<?php } ?>
											
										</div>
									</div>
									<div class="kt-widget__info"></div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!--end:: Portlet-->
			</div>
		</div>

		<!--End::Section-->
	<?php } ?>
	<!--Begin::Section-->
	<div class="row notice_panel"></div>

	<!--End::Section-->
	
	<?php if(in_array($this->session->userdata('role'), array(1, 3, 16, 17))){?>
	<div class="row">
		<div class="col-xl-12">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<div class="kt-portlet__head-toolbar" style="padding: 10px 50px;">
							<a href="https://app.powerbi.com/groups/me/reports/b456555a-6f9f-4f96-9bfd-cfc96a332c74/ReportSection?experience=power-bi" class="btn btn-label-brand btn-sm btn-bold" target="_blank">
								Quotation Analysis 
							</a>
						</div>
						<div class="kt-portlet__head-toolbar" style="padding: 10px 50px;">
							<a href="https://app.powerbi.com/groups/me/reports/f0551526-5f33-4029-a91a-9fc3dfde73e3/ReportSection?experience=power-bi" class="btn btn-label-brand btn-sm btn-bold" target="_blank">
								Competitor Analysis
							</a>
						</div>
						<div class="kt-portlet__head-toolbar" style="padding: 10px 50px;">
							<a href="https://app.powerbi.com/groups/me/reports/20c478d1-76e5-4ecf-b3d2-0622abcae047/ReportSectiona509fdf9189c8f04ddd6?experience=power-bi" class="btn btn-label-brand btn-sm btn-bold" target="_blank">
								Internal Data
							</a>
						</div>
						<div class="kt-portlet__head-toolbar" style="padding: 10px 50px;">
							<a href="https://app.powerbi.com/groups/me/reports/0005c2c4-b2fd-4c32-8727-b9058de9d152/ReportSection768603ac172ab1594c40?experience=power-bi" class="btn btn-label-brand btn-sm btn-bold" target="_blank">
								Production Status
							</a>
						</div>
						<div class="kt-portlet__head-toolbar" style="padding: 10px 50px;">
							<a href="https://omdeveloper.atlassian.net/jira/software/c/projects/CRM/boards/1" class="btn btn-label-brand btn-sm btn-bold" target="_blank">
								Jira
							</a>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>
	<?php } ?>
	<?php if($this->session->userdata('production_access')['production_status_wise_count']){?>
		<div>
			<div class="layer-white status_wise_count_loader">
				<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
			</div>
			<div>
				<div class="col-xl-12">
					<div class="kt-portlet kt-portlet--mobile">
						<div class="kt-portlet__head kt-portlet__head--lg">
							<?php if(!in_array($this->session->userdata('user_id'), array(191))){ ?>
							<div class="kt-portlet__head-label">
								<h3 class="production_title_name" style="font-size: 14px;">
									Total :
									<span class="production_title_name">
										<?php echo $currency_list[3];?> :
										<em class="production_title_value all_total">
											<?php echo $total_of_3_order,'(',$total_of_3_order_number,')';?>
										</em>
									</span>
								</h3>
								<div class="production_client_wise_count">
									<div class="kt-section kt-section--last">
										<a href="javascript:;" class="btn btn-label-brand btn-sm btn-bold view_all_details" order_type="all_order" action_value="show" style="background-color: gainsboro; border-color: gainsboro; color: #767676; font-weight: 800;"><i class="kt-font-dark flaticon2-line-chart"></i>View Details</a>
									</div>
								</div>
							</div>
							<?php }else{ ?>
							<div class="kt-portlet__head-label">
								<h3 class="production_title_name" style="font-size: 14px;">
									
									<span class="production_title_name">
									Work Order Count :
										<em class="production_title_value all_total">
											<?php echo $total_of_3_order_number;?>
										</em>
									</span>
								</h3>
							</div>
							<?php }?>
						</div>
					</div>	
				</div>
			</div>
			<div class="row status_wise_count" >
				<?php $this->load->view('production/status'); ?>
			</div>
			<div class="row production_order_wise_graph_div" style="display:none;">
				<div class="col-xl-12">
					<div class="kt-portlet kt-portlet--mobile">
						<div class="kt-portlet__body" style="padding: 1% 1% 0% 1%;">
							<div >
								<div class="kt-scroll ps ps--active-x ps--active-y" data-scroll="true" data-scroll-x="true" style="height: 400px; overflow: hidden;" id="production_status_graph"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row production_client_wise_graph_div" style="display:none;">
				<div class="col-xl-12">
					<div class="kt-portlet kt-portlet--mobile">
						<div class="kt-portlet__body" style="padding: 1% 1% 0% 1%;">
							<div >
								<div class="kt-scroll ps ps--active-x ps--active-y" data-scroll="true" data-scroll-x="true" style="height: 400px; overflow: hidden;" id="production_client_graph"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row production_status_and_product_family_wise_graph_div" style="display:none;">
				<div class="col-xl-6">
					<div class="kt-portlet kt-portlet--mobile">
						<div class="kt-portlet__body" style="padding: 1% 1% 0% 1%;">
							<div>
								<div class="kt-scroll ps ps--active-x ps--active-y" data-scroll="true" data-scroll-x="true" style="height: 400px; overflow: hidden;" id="production_product_family_graph"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-6">
					<div class="kt-portlet kt-portlet--mobile">
						<div class="kt-portlet__body" style="padding: 1% 1% 0% 1%;">
							<div>
								<div class="kt-scroll ps ps--active-x ps--active-y" data-scroll="true" data-scroll-x="true" style="height: 400px; overflow: hidden;" id="production_product_status_graph"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	<?php }?>
	<div class="row">
		
		<?php if(in_array(2,$this->session->userdata('graph_data_access'))) { ?>
		<div class="col-xl-6">
			<!--Begin::Portlet-->
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							ANNUAL SALES REVENUE
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						<div class="col">
							<div class="btn-group">
								<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="border: 0px solid #e2e5ec;">
									YEAR : <span style="font-weight: 800; color: #5d78ff; padding: 1px 1px 1px 10px;" id="current_year"><?php echo '2021'; ?></span>
								</button>
								<div class="dropdown-menu kt-scroll select_year_filter_div" data-scroll="true" style="height: 250px" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -215px, 0px);">
									
									<?php $this->load->view('home/filter_year');?>
								</div>
							</div>
						</div>
						<ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
							<li class="nav-item">
								<a class="nav-link compare_monthly_revenue" data-toggle="tab" href="#kt_widget11_tab1_content" role="tab">
									Compare Monthly
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="kt-portlet__body">
					<!--Begin::Timeline 3 -->	
					<div class="kt-scroll" data-scroll="true" style="height: 400px">
						<div id="revenue_highchart"></div>
					</div>

					<!--End::Timeline 3 -->
				</div>
			</div>

			<!--End::Portlet-->
		</div>
		<!-- <div class="col-xl-6"> -->
			<!--Begin::Portlet-->
			<!-- <div class="kt-portlet kt-portlet--height-fluid"> -->
				<!-- <div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							ANNUAL SALES REVENUE
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar"></div>
				</div> -->
				<!-- <div class="kt-portlet__body"> -->
					<!--Begin::Timeline 3 -->	
					<!-- <div class="kt-scroll" data-scroll="true" style="height: 400px"> -->
						<!-- <div id="revenue_highchart_2022"></div> -->
					<!-- </div> -->

					<!--End::Timeline 3 -->
				<!-- </div> -->
			<!-- </div> -->

			<!--End::Portlet-->
		<!-- </div> -->
		<?php } ?>

		<?php if(in_array(1,$this->session->userdata('graph_data_access'))) { ?>
		<div class="col-xl-6">

			<!--Begin::Portlet-->
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Revenue Generated Country Wise
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">
					<!--Begin::Timeline 3 -->	
					<div class="kt-scroll" data-scroll="true" style="height: 400px">
						<div id="world_map_highchart"></div>
					</div>

					<!--End::Timeline 3 -->
				</div>
			</div>

			<!--End::Portlet-->
		</div>
		<?php } ?>
	</div>

	

	<?php if(in_array(18,$this->session->userdata('graph_data_access'))) { ?>
	<div class="row">
		<div class="col-xl-12">
			<!--Begin::Portlet-->
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Invoice 
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
					</div>
				</div>
				<div class="kt-portlet__body">
					<!--Begin::Timeline 3 -->	
					<div class="kt-scroll" data-scroll="true" style="height: 400px">
						<div id="invoice_total_highchart"></div>
					</div>

					<!--End::Timeline 3 -->
				</div>
			</div>

			<!--End::Portlet-->
		</div>
	</div>
	<?php }?>

    <?php if(in_array(4,$this->session->userdata('graph_data_access'))) { ?>
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--Begin::Portlet-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Destrack Time
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
							<a href="javascript:void(0);" class="btn kt-subheader__btn-daterange" id="kt_dashboard_daterangepicker" data-toggle="kt-tooltip" title="Select Destrack Date" data-placement="left" style="background: rgb(0 0 0 / 10%); display: inline-flex; height: 32px !important; padding-top: 0; padding-bottom: 0; margin-top: 0.25rem; margin-bottom: 0.25rem; margin-left: 0.25rem;">
								<span class="kt-subheader__btn-daterange-title" id="kt_dashboard_daterangepicker_title" style="display: inline-block;color: #959cb6;font-weight: 500;">Today</span>&nbsp;
								<span class="kt-subheader__btn-daterange-date" id="kt_dashboard_daterangepicker_date" style="display: inline-block;color: #5d78ff;margin-right: 0.75rem;font-weight: 500;">Aug 16</span>
								<i class="flaticon2-calendar-1" style="color: #5d78ff; font-size: 1rem !important;"></i>
							</a>
                        </ul>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--Begin::Timeline 3 -->   
                    <div class="kt-scroll" data-scroll="true" style="height: 400px">
                        <div id="desktrack_highchart"></div>
                    </div>
                    <!--End::Timeline 3 -->
                </div>
            </div>

            <!--End::Portlet-->
        </div>
    </div>

    <!--End::Section-->
    <?php } ?>
    <?php if(in_array(5,$this->session->userdata('graph_data_access'))) { ?>
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12 kt-hidden">
            <!--Begin::Portlet-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            SALES PERSON DAILY CLIENT CONTACT REPORT
                            <input type="text" id="filter_date" value="" hidden>
                            <input type="text" id="filter_report" value="" hidden>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="col">
                            <div class="btn-group">
                                <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="border: 0px solid #e2e5ec;">
                                    Report Type : <span style="font-weight: 800; color: #5d78ff; padding: 1px 1px 1px 10px;" id="filter_report_type">ALl</span>
                                </button>
                                <div class="dropdown-menu kt-scroll select_daily_report_dropdown" data-scroll="true" style="height: 300px" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -215px, 0px);"></div>
                                
                            </div>
                        </div>
                        <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
                        	<a href="javascript:void(0);" class="btn kt-subheader__btn-daterange" id="daily_report_date_range_picker" data-toggle="kt-tooltip" title="Select Daily Report Date" data-placement="left" style="background: rgb(0 0 0 / 10%); display: inline-flex; height: 32px !important; padding-top: 0; padding-bottom: 0; margin-top: 0.25rem; margin-bottom: 0.25rem; margin-left: 0.25rem;">
								<span class="kt-subheader__btn-daterange-title" id="daily_report_date_range_picker_title" style="display: inline-block;color: #959cb6;font-weight: 500;">Today</span>&nbsp;
								<span class="kt-subheader__btn-daterange-date" id="daily_report_date_range_picker_date" style="display: inline-block;color: #5d78ff;margin-right: 0.75rem;font-weight: 500;">Aug 16</span>
								<i class="flaticon2-calendar-1" style="color: #5d78ff; font-size: 1rem !important;"></i>
							</a>
                        </ul>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--Begin::Timeline 3 -->   
                    <div class="kt-scroll" data-scroll="true" style="height: 400px">
                        <div id="daily_report_highchart"></div>
                    </div>

                    <!--End::Timeline 3 -->
                </div>
            </div>

            <!--End::Portlet-->
        </div>
    </div>

    <!--End::Section-->
    <?php } ?>
	<?php if(in_array(6,$this->session->userdata('graph_data_access'))) { ?>
    <!--Begin::Section-->
	<div class="row">
		<div class="col-xl-12">
			<!--Begin::Portlet-->
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Follup Up Pending
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						<ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
							<li class="nav-item">
								<a class="nav-link change_quotation_pending_for_follow_up_date_for_highchart active" filter-date="current" data-toggle="tab" href="#kt_widget11_tab1_content" role="tab">
									Today
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link change_quotation_pending_for_follow_up_date_for_highchart" filter-date="yesterday" data-toggle="tab" href="#kt_widget11_tab1_content" role="tab">
									Yesterday
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link change_quotation_pending_for_follow_up_date_for_highchart" filter-date="week" data-toggle="tab" href="#kt_widget11_tab1_content" role="tab" >
									Week 
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link change_quotation_pending_for_follow_up_date_for_highchart" filter-date="month" data-toggle="tab" href="#kt_widget11_tab1_content" role="tab">
									Month
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link change_quotation_pending_for_follow_up_date_for_highchart" filter-date="all" data-toggle="tab" href="#kt_widget11_tab2_content" role="tab">
									All Time
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="kt-portlet__body">
					<!--Begin::Timeline 3 -->	
					<div class="kt-scroll" data-scroll="true" style="height: 400px">
						<div id="quotation_pending_highchart"></div>
					</div>

					<!--End::Timeline 3 -->
				</div>
			</div>

			<!--End::Portlet-->
		</div>
	</div>

    <!--End::Section-->
    <?php } ?>
	<?php if(in_array(7,$this->session->userdata('graph_data_access'))) { ?>
    <!--Begin::Section-->
	<div class="row">
		<div class="col-xl-12">
			<!--Begin::Portlet-->
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Number of Quotations
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						<div class="col">
							<div class="btn-group select_year_dropdown">
								
							</div>
						</div>
						<ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
                        	<a href="javascript:void(0);" class="btn kt-subheader__btn-daterange" id="quotation_created_date_range_picker" data-toggle="kt-tooltip" title="Select Daily Report Date" data-placement="left" style="background: rgb(0 0 0 / 10%); display: inline-flex; height: 32px !important; padding-top: 0; padding-bottom: 0; margin-top: 0.25rem; margin-bottom: 0.25rem; margin-left: 0.25rem;">
								<span class="kt-subheader__btn-daterange-title" id="quotation_created_date_range_picker_title" style="display: inline-block;color: #959cb6;font-weight: 500;">Today</span>&nbsp;
								<span class="kt-subheader__btn-daterange-date" id="quotation_created_date_range_picker_date" style="display: inline-block;color: #5d78ff;margin-right: 0.75rem;font-weight: 500;">Aug 16</span>
								<i class="flaticon2-calendar-1" style="color: #5d78ff; font-size: 1rem !important;"></i>
							</a>
                        </ul>
					</div>
				</div>
				<div class="kt-portlet__body">
					<!--Begin::Timeline 3 -->	
					<div class="kt-scroll" data-scroll="true" style="height: 400px">
						<div id="quotation_list_highchart"></div>
					</div>

					<!--End::Timeline 3 -->
				</div>
			</div>

			<!--End::Portlet-->
		</div>
	</div>

    <!--End::Section-->
    <?php } ?>
	<?php if(in_array(8,$this->session->userdata('graph_data_access'))) { ?>
    <!--Begin::Section-->
	<div class="row">
		<div class="col-xl-12">
			<!--Begin::Portlet-->
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Quotation Vs Proforma Won
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						<input type="text" id="input_quotation_vs_proforma_sales_person_name" value="" hidden>
						<input type="text" id="input_quotation_vs_proforma_current_year" value="" hidden>
						<div class="col">
							<div class="btn-group">
								<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="border: 0px solid #e2e5ec;">
									Sales Person : <span style="font-weight: 800; color: #5d78ff; padding: 1px 1px 1px 10px;" id="quotation_vs_proforma_sales_person_name"></span>
								</button>
								<div class="dropdown-menu kt-scroll quotation_vs_proforma_sales_person" data-scroll="true" style="height: 250px" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -215px, 0px);">
								</div>
							</div>
						</div>
						<div class="col">
							<div class="btn-group">
								<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="border: 0px solid #e2e5ec;">
									YEAR : <span style="font-weight: 800; color: #5d78ff; padding: 1px 1px 1px 10px;" id="quotation_vs_proforma_current_year"><?php echo '2021'; ?></span>
								</button>
								<div class="dropdown-menu kt-scroll select_year_filter_div_for_quotation_vs_proforma" data-scroll="true" style="height: 250px" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -215px, 0px);">
									
									<?php $this->load->view('home/filter_year');?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="kt-portlet__body">
					<!--Begin::Timeline 3 -->	
					<div class="kt-scroll" data-scroll="true" style="height: 400px">
						<div id="quotation_vs_proforma_highchart"></div>
					</div>

					<!--End::Timeline 3 -->
				</div>
			</div>

			<!--End::Portlet-->
		</div>
	</div>

    <!--End::Section-->
    <?php } ?>
    <?php if(in_array(9,$this->session->userdata('graph_data_access'))) { ?>
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--Begin::Portlet-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title total_Revenue"></h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
							<a href="javascript:void(0);" class="btn kt-subheader__btn-daterange" id="quotation_proforma_daterangepicker" data-toggle="kt-tooltip" title="Select Destrack Date" data-placement="left" style="background: rgb(0 0 0 / 10%); display: inline-flex; height: 32px !important; padding-top: 0; padding-bottom: 0; margin-top: 0.25rem; margin-bottom: 0.25rem; margin-left: 0.25rem;">
								<span class="kt-subheader__btn-daterange-title" id="quotation_proforma_daterangepicker_title" style="display: inline-block;color: #959cb6;font-weight: 500;">Today</span>&nbsp;
								<span class="kt-subheader__btn-daterange-date" id="quotation_proforma_daterangepicker_date" style="display: inline-block;color: #5d78ff;margin-right: 0.75rem;font-weight: 500;">Aug 16</span>
								<i class="flaticon2-calendar-1" style="color: #5d78ff; font-size: 1rem !important;"></i>
							</a>
                        </ul>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--Begin::Timeline 3 -->   
                    <div class="kt-scroll" data-scroll="true" style="height: 400px">
                        <div id="quotation_proforma_highchart"></div>
                    </div>

                    <!--End::Timeline 3 -->
                </div>
            </div>

            <!--End::Portlet-->
        </div>
    </div>

    <!--End::Section-->
	<?php }?>

	<?php if(in_array($this->session->userdata('role'),array(1, 17))) { ?>
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--Begin::Portlet-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                           	Proforma Month Wise
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">

						<input type="text" id="input_quotation_proforma_sales_person_name" value="" hidden>
						<input type="text" id="input_quotation_proforma_current_year" value="" hidden>
						<div class="col">
							<div class="btn-group">
								<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="border: 0px solid #e2e5ec;">
									Sales Person : <span style="font-weight: 800; color: #5d78ff; padding: 1px 1px 1px 10px;" id="quotation_proforma_sales_person_name"></span>
								</button>
								<div class="dropdown-menu kt-scroll quotation_proforma_sales_person" data-scroll="true" style="height: 250px" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -215px, 0px);">
								</div>
							</div>
						</div>
						<div class="col">
							<div class="btn-group">
								<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="border: 0px solid #e2e5ec;">
									YEAR : <span style="font-weight: 800; color: #5d78ff; padding: 1px 1px 1px 10px;" id="quotation_proforma_current_year"><?php echo '2021'; ?></span>
								</button>
								<div class="dropdown-menu kt-scroll select_year_filter_div_for_quotation_proforma" data-scroll="true" style="height: 250px" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -215px, 0px);">

									<?php $this->load->view('home/filter_year');?>
								</div>
							</div>
						</div>
					</div>
                </div>
                <div class="kt-portlet__body">
                    <!--Begin::Timeline 3 -->   
                    <div class="kt-scroll" data-scroll="true" style="height: 400px">
                        <div id="quotation_proforma_month_wise_highchart"></div>
                    </div>

                    <!--End::Timeline 3 -->
                </div>
            </div>

            <!--End::Portlet-->
        </div>
    </div>

    <!--End::Section-->
	<?php }?>

	<?php if(in_array($this->session->userdata('role'), array(1, 16, 5))) { ?>
	<!--Begin::Section-->
    <!-- <div class="row">
        <div class="col-xl-12"> -->
            <!--Begin::Portlet-->
            <!-- <div class="kt-portlet kt-portlet--height-fluid">
                <img src="<?php echo base_url('assets/no_champ_dec.jpeg');?>"height = "700px" >
            </div> -->

            <!--End::Portlet-->
	        <!-- </div>
	    </div> -->

    <!--End::Section-->
	<?php }?>
	<?php if(in_array(19,$this->session->userdata('graph_data_access'))) { ?>
	<!--Begin::Section-->
	
	

	<!--End::Section-->
	<?php }?>
	<?php if(in_array($this->session->userdata('role'), array(1, 17))) { ?>
	<!--Begin::Section-->
		<div class="row">
			<div class="col-xl-12">
				<!--Begin::Portlet-->
				<div class="kt-portlet kt-portlet--height-fluid">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">
								RFQ Weekly Status
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="col">
								<div class="btn-group">
									<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="border: 0px solid #e2e5ec;">
										Procurement Person : <span style="font-weight: 800; color: #5d78ff; padding: 1px 1px 1px 10px;" id="current_procurement_person"></span>
									</button>
									<div class="dropdown-menu kt-scroll procurement_list" data-scroll="true" style="height: 250px" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -215px, 0px);">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="kt-portlet__body">
						<!--Begin::Timeline 3 -->	
						<div class="kt-scroll" data-scroll="true" style="height: 400px">
							<div id="rfq_status_highchart"></div>
						</div>

						<!--End::Timeline 3 -->
					</div>
				</div>

				<!--End::Portlet-->
			</div>
		</div>

    <!--End::Section-->
    <?php }?>

	<?php if(in_array($this->session->userdata('role'), array(1, 5, 16, 17))){?>	
	<div class="row kt-hidden">
		<div class="col-xl-4 col-lg-6 order-lg-3 order-xl-1">

			<!--begin:: Widgets/Tasks -->
			<div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Daily Routine
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar"></div>
				</div>
				<div class="kt-portlet__body">
					<div class="tab-content">
						<div class="tab-pane active" id="kt_widget2_tab1_content">
							<div class="kt-widget2">
								<div class="kt-widget2__item kt-widget2__item--primary">
									<div class="kt-widget2__checkbox">
										
									</div>
									<div class="kt-widget2__info">
										<a href="javascript:void(0);" class="kt-widget2__title">
											CALL 25 PAX EVERYDAY
										</a>
										<a href="javascript:void(0);" class="kt-widget2__username">.</a>
									</div>
									<div class="kt-widget2__actions">
										<div class="btn-group nav btn-group btn-group-pill btn-group-air" role="group" style=" text-align: center; display: inline-block; margin: 0 0 0 0;">
											<button type="button" class="btn btn-w btn-bolder" data-toggle="tab" href="#kt-pricing-2_content1" role="tab" aria-expanded="true" style="margin: 0 0 0 0; padding: 0 0 0 0; color: #0abb87;">
												<i class="fa fa-check-circle"></i>
											</button>
											<button type="button" class="btn btn-wide btn-uppercase btn-bolder" data-toggle="tab" href="#kt-pricing-2_content2" role="tab" aria-expanded="false" style="margin: 0 0 0 0; padding: 0 0 0 0; color: #ff0000;">
												<i class="fa fa-times-circle"></i>
											</button>
										</div>
									</div>
								</div>
								<div class="kt-widget2__item kt-widget2__item--warning">
									<div class="kt-widget2__checkbox">
										
									</div>
									<div class="kt-widget2__info">
										<a href="javascript:void(0);" class="kt-widget2__title">
											EMAIL POST THE CALL
										</a>
										<a href="javascript:void(0);" class="kt-widget2__username">.</a>
									</div>
									<div class="kt-widget2__actions"><div class="btn-group nav btn-group btn-group-pill btn-group-air" role="group" style=" text-align: center; display: inline-block; margin: 0 0 0 0;">
											<button type="button" class="btn btn-w btn-bolder" data-toggle="tab" href="#kt-pricing-2_content1" role="tab" aria-expanded="true" style="margin: 0 0 0 0; padding: 0 0 0 0; color: #0abb87;">
												<i class="fa fa-check-circle"></i>
											</button>
											<button type="button" class="btn btn-wide btn-uppercase btn-bolder" data-toggle="tab" href="#kt-pricing-2_content2" role="tab" aria-expanded="false" style="margin: 0 0 0 0; padding: 0 0 0 0; color: #ff0000;">
												<i class="fa fa-times-circle"></i>
											</button>
										</div></div>
								</div>
								<div class="kt-widget2__item kt-widget2__item--brand">
									<div class="kt-widget2__checkbox">
										
									</div>
									<div class="kt-widget2__info">
										<a href="javascript:void(0);" class="kt-widget2__title">
											LINKEDIN + WHATSAPP
										</a>
										<a href="javascript:void(0);" class="kt-widget2__username">.</a>
									</div>
									<div class="kt-widget2__actions"><div class="btn-group nav btn-group btn-group-pill btn-group-air" role="group" style=" text-align: center; display: inline-block; margin: 0 0 0 0;">
											<button type="button" class="btn btn-w btn-bolder" data-toggle="tab" href="#kt-pricing-2_content1" role="tab" aria-expanded="true" style="margin: 0 0 0 0; padding: 0 0 0 0; color: #0abb87;">
												<i class="fa fa-check-circle"></i>
											</button>
											<button type="button" class="btn btn-wide btn-uppercase btn-bolder" data-toggle="tab" href="#kt-pricing-2_content2" role="tab" aria-expanded="false" style="margin: 0 0 0 0; padding: 0 0 0 0; color: #ff0000;">
												<i class="fa fa-times-circle"></i>
											</button>
										</div></div>
								</div>
								<div class="kt-widget2__item kt-widget2__item--success">
									<div class="kt-widget2__checkbox">
										
									</div>
									<div class="kt-widget2__info">
										<a href="javascript:void(0);" class="kt-widget2__title">
											UPDATE CRM
										</a>
										<a class="kt-widget2__username">.</a>
									</div>
									<div class="kt-widget2__actions"><div class="btn-group nav btn-group btn-group-pill btn-group-air" role="group" style=" text-align: center; display: inline-block; margin: 0 0 0 0;">
											<button type="button" class="btn btn-w btn-bolder" data-toggle="tab" href="#kt-pricing-2_content1" role="tab" aria-expanded="true" style="margin: 0 0 0 0; padding: 0 0 0 0; color: #0abb87;">
												<i class="fa fa-check-circle"></i>
											</button>
											<button type="button" class="btn btn-wide btn-uppercase btn-bolder" data-toggle="tab" href="#kt-pricing-2_content2" role="tab" aria-expanded="false" style="margin: 0 0 0 0; padding: 0 0 0 0; color: #ff0000;">
												<i class="fa fa-times-circle"></i>
											</button>
										</div></div>
								</div>
								<div class="kt-widget2__item kt-widget2__item--danger">
									<div class="kt-widget2__checkbox">
										
									</div>
									<div class="kt-widget2__info">
										<a href="javascript:void(0);" class="kt-widget2__title">
											MAKE SURE QUOTATION FOLLOW UPS ARE DONE AND UPDATED DAILY
										</a>
										<a href="javascript:void(0);" class="kt-widget2__username">.</a>
									</div>
									<div class="kt-widget2__actions"><div class="btn-group nav btn-group btn-group-pill btn-group-air" role="group" style=" text-align: center; display: inline-block; margin: 0 0 0 0;">
											<button type="button" class="btn btn-w btn-bolder" data-toggle="tab" href="#kt-pricing-2_content1" role="tab" aria-expanded="true" style="margin: 0 0 0 0; padding: 0 0 0 0; color: #0abb87;">
												<i class="fa fa-check-circle"></i>
											</button>
											<button type="button" class="btn btn-wide btn-uppercase btn-bolder" data-toggle="tab" href="#kt-pricing-2_content2" role="tab" aria-expanded="false" style="margin: 0 0 0 0; padding: 0 0 0 0; color: #ff0000;">
												<i class="fa fa-times-circle"></i>
											</button>
										</div></div>
								</div>
								<div class="kt-widget2__item kt-widget2__item--info">
									<div class="kt-widget2__checkbox">
										
									</div>
									<div class="kt-widget2__info">
										<a href="javascript:void(0);" class="kt-widget2__title">
											ALL NEW QUOTATIONS ARE SUBMITTED AND CLIENT IS CALLED FOR FU
										</a>
										<a href="javascript:void(0);" class="kt-widget2__username">.</a>
									</div>
									<div class="kt-widget2__actions"><div class="btn-group nav btn-group btn-group-pill btn-group-air" role="group" style=" text-align: center; display: inline-block; margin: 0 0 0 0;">
											<button type="button" class="btn btn-w btn-bolder" data-toggle="tab" href="#kt-pricing-2_content1" role="tab" aria-expanded="true" style="margin: 0 0 0 0; padding: 0 0 0 0; color: #0abb87;">
												<i class="fa fa-check-circle"></i>
											</button>
											<button type="button" class="btn btn-wide btn-uppercase btn-bolder" data-toggle="tab" href="#kt-pricing-2_content2" role="tab" aria-expanded="false" style="margin: 0 0 0 0; padding: 0 0 0 0; color: #ff0000;">
												<i class="fa fa-times-circle"></i>
											</button>
										</div></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!--end:: Widgets/Tasks -->
		</div>
		<div class="col-xl-4 col-lg-6 order-lg-3 order-xl-1">

			<!--begin:: Widgets/Tasks -->
			<div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
				<div class="kt-portlet__head"></div>
				<div class="kt-portlet__body">
					<div class="tab-content">
						<div class="tab-pane active" id="kt_widget2_tab1_content">
							<div class="kt-widget2">
								<div class="kt-widget2__item kt-widget2__item--primary">
									<div class="kt-widget2__checkbox">
										
									</div>
									<div class="kt-widget2__info">
										<a href="javascript:void(0);" class="kt-widget2__title">
											PITCH SPECIFIC PRODUCTS TO LEADS WHICH THEY BUY
										</a>
										<a href="javascript:void(0);" class="kt-widget2__username">.</a>
									</div>
									<div class="kt-widget2__actions"></div>
								</div>
								<div class="kt-widget2__item kt-widget2__item--warning">
									<div class="kt-widget2__checkbox">
										
									</div>
									<div class="kt-widget2__info">
										<a href="javascript:void(0);" class="kt-widget2__title">
											ALL RFQS TO BE FORWARDED TO RFQ TEAM AFTER A CHECK
										</a>
										<a href="javascript:void(0);" class="kt-widget2__username">.</a>
									</div>
									<div class="kt-widget2__actions"></div>
								</div>
								<div class="kt-widget2__item kt-widget2__item--brand">
									<div class="kt-widget2__checkbox">
										
									</div>
									<div class="kt-widget2__info">
										<a href="javascript:void(0);" class="kt-widget2__title">
											ALL QUERIES TO BE RAISED AND IF REPLIED CLOSED
										</a>
										<a href="javascript:void(0);" class="kt-widget2__username">.</a>
									</div>
									<div class="kt-widget2__actions"></div>
								</div>
								<div class="kt-widget2__item kt-widget2__item--success">
									<div class="kt-widget2__checkbox">
										
									</div>
									<div class="kt-widget2__info">
										<a href="javascript:void(0);" class="kt-widget2__title">
											INBOX TO BE CLEAN
										</a>
										<a class="kt-widget2__username">.</a>
									</div>
									<div class="kt-widget2__actions"></div>
								</div>
								<div class="kt-widget2__item kt-widget2__item--danger">
									<div class="kt-widget2__checkbox">
										
									</div>
									<div class="kt-widget2__info">
										<a href="javascript:void(0);" class="kt-widget2__title">
											NO LEADS TO HAVE MORE THAN 1 MONTH BETWEEN TWO FOLLOW UPS
										</a>
										<a href="javascript:void(0);" class="kt-widget2__username">.</a>
									</div>
									<div class="kt-widget2__actions"></div>
								</div>
								<div class="kt-widget2__item kt-widget2__item--info">
									<div class="kt-widget2__checkbox">
										
									</div>
									<div class="kt-widget2__info">
										<a href="javascript:void(0);" class="kt-widget2__title">
											TRY TO HAVE ATLEAST 1 ZOOM MEET WITH AN IMP CLIENT EACH WEEK
										</a>
										<a href="javascript:void(0);" class="kt-widget2__username">.</a>
									</div>
									<div class="kt-widget2__actions"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!--end:: Widgets/Tasks -->
		</div>
		<div class="col-xl-4 col-lg-6 order-lg-3 order-xl-1">

			<!--begin:: Widgets/Tasks -->
			<div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
				<div class="kt-portlet__head"></div>
				<div class="kt-portlet__body">
					<div class="tab-content">
						<div class="tab-pane active" id="kt_widget2_tab1_content">
							<div class="kt-widget2">
								<div class="kt-widget2__item kt-widget2__item--primary">
									<div class="kt-widget2__checkbox">
										
									</div>
									<div class="kt-widget2__info">
										<a href="javascript:void(0);" class="kt-widget2__title">
											 STATEMENT OF ACCOUNTS FOLLOW UP
										</a>
										<a href="javascript:void(0);" class="kt-widget2__username">.</a>
									</div>
									<div class="kt-widget2__actions"></div>
								</div>
								<div class="kt-widget2__item kt-widget2__item--warning">
									<div class="kt-widget2__checkbox">
										
									</div>
									<div class="kt-widget2__info">
										<a href="javascript:void(0);" class="kt-widget2__title">
											PRODUCTION STAGE + LOGISTICS FOLLOW UP
										</a>
										<a href="javascript:void(0);" class="kt-widget2__username">.</a>
									</div>
									<div class="kt-widget2__actions"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!--end:: Widgets/Tasks -->
		</div>
	</div>
	<div>
		<div class="layer-white status_wise_count_loader">
			<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
		</div>
		<div class="row sales_routine_div"></div>
	</div>	
	
	<?php } ?>
	<div class="row" style="padding-bottom: 24px;">
		<div class="kanban-container" style="width: 100%; box-shadow: rgba(14, 30, 37, 0.12) 0px 2px 4px 0px, rgba(14, 30, 37, 0.32) 0px 2px 16px 0px; width: 1370px !important;">
			<div data-id="_backlog" data-order="1" class="kanban-board"
				style="width: 100% !important; margin-left: 0px; margin-right: 0px;">
				<main class="kanban-drag">
					<div class="kanban-item dark-light level-one" style="width: 60%; margin: 0 auto 40px; position: relative;">
						<div class="kt-kanban__badge" style="justify-content: center;">
							<div class="kt-kanban__content">
								<div class="kt-kanban__title kt-font-xl">Jay Mehta</div>
								<div class="kt-kanban__title kt-font-boldest" style="color:#343a40; font-weight: 800 !important;">CEO</div>
								<hr style="margin:0px; border-top: 1px solid #343a40;">
								<div class="kt-section__content kt-section__content--solid" style="margin-top:0.5rem;">
									<?php if($this->session->userdata('global_user_details')['user_name'] == 'jay') {?>
										<a class="btn btn-outline-dark btn-elevate btn-icon login_as_another_user" data-toggle="kt-tooltip" data-placement="left" title="" data-original-title="Login" style="height:2rem; width:2rem;" user_name="<?php echo $this->session->userdata('global_user_details')['user_name']; ?>" password="<?php echo $this->session->userdata('global_user_details')['password']; ?>">
											<i class="flaticon2-user"></i>
										</a>
										&nbsp;
										<a href="<?php echo base_url('common/user_assign_data/');?>" target="_blank" class="btn btn-outline-dark btn-elevate btn-icon"data-toggle="kt-tooltip" data-placement="left" title="" data-original-title="User Assign Data" style="height:2rem; width:2rem;">
											<i class="flaticon2-list-1"></i>
										</a>
										&nbsp;
										<a href="<?php echo base_url('common/daily_report');?>" target="_blank" class="btn btn-outline-dark btn-elevate btn-icon"data-toggle="kt-tooltip" data-placement="left" title="" data-original-title="Daily Report" style="height:2rem; width:2rem;">
											<i class="flaticon2-calendar"></i>
										</a>
										&nbsp;
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
					<ol class="level-two-wrapper">
						<?php 
							foreach ($level_two_employee_list as $single_employee_details) {
								
								$single_employee_details['user_login'] = false;
								if(
									in_array($this->session->userdata('role'), array(1)) || 
									in_array($single_employee_details['tab_name'], $allowed_ids) ||
									(
										$this->session->userdata('global_user_details')['user_name'] == $single_employee_details['username'] 
										&& 
										$this->session->userdata('global_user_details')['password'] == $single_employee_details['password']
									)
								) {
									$single_employee_details['user_login'] = true;
								}
								echo '<li class="" style="margin: 5px 5px 0px 0px;" name = "'.$single_employee_details['tab_name'].'">';
									$this->load->view('home/graph_employee_information', $single_employee_details);
								echo '</li>';
							}
						?>
					</ol>
					<div class="" style="width: 60%; margin: 0 auto 40px; position: relative;"></div>
					<ol class="level-three-wrapper">
						<?php 
							foreach ($level_three_employee_list as $single_employee_details) {
								
								$single_employee_details['user_login'] = false;
								if(
									in_array($this->session->userdata('role'), array(1)) || 
									in_array($single_employee_details['tab_name'], $allowed_ids) ||
									(
										$this->session->userdata('global_user_details')['user_name'] == $single_employee_details['username']
										&&
										$this->session->userdata('global_user_details')['password'] == $single_employee_details['password']
									)
								) {
									$single_employee_details['user_login'] = true;
								}
								echo '<li class="" style="margin: 5px 5px 0px 0px;">';
									$this->load->view('home/graph_employee_information', $single_employee_details);
								echo '</li>';
							}
						?>
					</ol>

					<div class="" style="width: 60%; margin: 0 auto 40px; position: relative;"></div>
					<ol class="level-four-wrapper">
						<?php 
							foreach ($level_four_employee_list as $single_employee_details) {

								$single_employee_details['user_login'] = false;
								if(
									in_array($this->session->userdata('role'), array(1)) || 
									in_array($single_employee_details['tab_name'], $allowed_ids) ||
									(
										$this->session->userdata('global_user_details')['user_name'] == $single_employee_details['username']
										&&
										$this->session->userdata('global_user_details')['password'] == $single_employee_details['password']
									)
									
								) {
									$single_employee_details['user_login'] = true;
								}
								echo '<li class="" style="margin: 5px 5px 0px 0px;">';
									$this->load->view('home/graph_employee_information', $single_employee_details);
								echo '</li>';
							}
						?>
					</ol>

					<div class="" style="width: 60%; margin: 0 auto 40px; position: relative;"></div>
					<ol class="level-five-wrapper">
						<?php 
							foreach ($level_five_employee_list as $single_employee_details) {

								$single_employee_details['user_login'] = false;
								if(
									in_array($this->session->userdata('role'), array(1)) || 
									in_array($single_employee_details['tab_name'], $allowed_ids) ||
									(
										$this->session->userdata('global_user_details')['user_name'] == $single_employee_details['username']
										&&
										$this->session->userdata('global_user_details')['password'] == $single_employee_details['password']
									)
								) {
									$single_employee_details['user_login'] = true;
								}
								echo '<li class="" style="margin: 5px 5px 0px 0px;">';
									$this->load->view('home/graph_employee_information', $single_employee_details);
								echo '</li>';
							}
						?>
					</ol>

					<div class="" style="width: 60%; margin: 0 auto 40px; position: relative;"></div>
					<ol class="level-six-wrapper">
						<?php 
							foreach ($level_six_employee_list as $single_employee_details) {

								$single_employee_details['user_login'] = false;
								if(
									in_array($this->session->userdata('role'), array(1)) || 
									in_array($single_employee_details['tab_name'], $allowed_ids)
								) {
									$single_employee_details['user_login'] = true;
								}
								echo '<li class="" style="margin: 5px 5px 0px 0px;">';
									$this->load->view('home/graph_employee_information', $single_employee_details);
								echo '</li>';
							}
						?>
					</ol>
				</main>

				<footer></footer>
			</div>
		</div>
	</div>
	<div class="row kt-hidden">
		<div class="col-xl-12">

			<!--begin:: Widgets/Notifications-->
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							OM TUBES AND FITTINGS INDUSTRIES PREMIER LEAGUE
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						<ul class="nav nav-pills nav-pills-sm nav-pills-label nav-pills-bold" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" data-toggle="tab" href="#kt_widget6_tab1_content" role="tab">
									Fixtures
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#kt_widget6_tab2_content" role="tab">
									Teams
								</a>
							</li>
							<!-- <li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#kt_widget6_tab3_content" role="tab">
									Month
								</a>
							</li> -->
						</ul>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="tab-content">
						<div class="tab-pane active" id="kt_widget6_tab1_content" aria-expanded="true">
							<div class="kt-notification">
								<div class="row" style="display:block;">
									<div class="col-xl-12">

										<!--Begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid" style="background-color: #e4e8ee !important;">
											
											<div class="kt-portlet__body" style="padding: 0px 00px;">

												<div class="row" style="padding: 20px 0px;">
													<div class="" style="width: 3%;"></div>
													<div class="" style="width:45%;background-color: #fff !important;box-sizing: border-box; border-radius: 1rem;">
														<!--begin::Widget -->
														<div class="kt-widget kt-widget--user-profile-2">
															<div class="kt-widget__head row  kt-align-center" style="margin: 14px 14px;">
																<div class="col-xl-3">
																</div>
																<div class="col-xl-6">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		Match 01 | 2:00 - 2:50
																	</span>
																</div>
																<div class="col-xl-3">
																</div>
																<div class="col-xl-12">
																	<hr>
																</div>
																<div class="col-xl-5">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		Prasad Parker
																	</span>
																</div>
																<div class="col-xl-2">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		VS
																	</span>
																</div>
																<div class="col-xl-5">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		Rupesh Mhaske
																	</span>
																</div>
															</div>
														</div>

														<!--end::Widget -->
													</div>
													<div class="" style="width: 4%;"></div>
													<div class="" style="width:45%;background-color: #fff !important;box-sizing: border-box; border-radius: 1rem;">
														<!--begin::Widget -->
														<div class="kt-widget kt-widget--user-profile-2">
															<div class="kt-widget__head row  kt-align-center" style="margin: 14px 14px;">
																<div class="col-xl-3">
																</div>
																<div class="col-xl-6">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		Match 02 | 2:00 - 2:50
																	</span>
																</div>
																<div class="col-xl-3">
																</div>
																<div class="col-xl-12">
																	<hr>
																</div>
																<div class="col-xl-5">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		Hitesh Rathod
																	</span>
																</div>
																<div class="col-xl-2">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		VS
																	</span>
																</div>
																<div class="col-xl-5">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		Pranay Shetty
																	</span>
																</div>
															</div>
														</div>

														<!--end::Widget -->
													</div>
													<div class="" style="width: 3%;"></div>
												</div>
												<div class="row" style="padding: 20px 0px;">
													<div class="" style="width: 3%;"></div>
													<div class="" style="width:45%;background-color: #fff !important;box-sizing: border-box; border-radius: 1rem;">
														<!--begin::Widget -->
														<div class="kt-widget kt-widget--user-profile-2">
															<div class="kt-widget__head row  kt-align-center" style="margin: 14px 14px;">
																<div class="col-xl-3">
																</div>
																<div class="col-xl-6">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		Match 03 | 3:00 - 3:50
																	</span>
																</div>
																<div class="col-xl-3">
																</div>
																<div class="col-xl-12">
																	<hr>
																</div>
																<div class="col-xl-5">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		Abhishek Desai
																	</span>
																</div>
																<div class="col-xl-2">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		VS
																	</span>
																</div>
																<div class="col-xl-5">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		Riyaz Khan
																	</span>
																</div>
															</div>
														</div>

														<!--end::Widget -->
													</div>
													<div class="" style="width: 4%;"></div>
													<div class="" style="width:45%;background-color: #fff !important;box-sizing: border-box; border-radius: 1rem;">
														<!--begin::Widget -->
														<div class="kt-widget kt-widget--user-profile-2">
															<div class="kt-widget__head row  kt-align-center" style="margin: 14px 14px;">
																<div class="col-xl-3">
																</div>
																<div class="col-xl-6">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		Match 04 | 3:00 - 3:50
																	</span>
																</div>
																<div class="col-xl-3">
																</div>
																<div class="col-xl-12">
																	<hr>
																</div>
																<div class="col-xl-5">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		Sayyed Aaves
																	</span>
																</div>
																<div class="col-xl-2">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		VS
																	</span>
																</div>
																<div class="col-xl-5">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		Abhishek Gundu
																	</span>
																</div>
															</div>
														</div>

														<!--end::Widget -->
													</div>
													<div class="" style="width: 3%;"></div>
												</div>
												<div class="row" style="padding: 20px 0px;">
													<div class="" style="width: 3%;"></div>
													<div class="" style="width:45%;background-color: #fff !important;box-sizing: border-box; border-radius: 1rem;">
														<!--begin::Widget -->
														<div class="kt-widget kt-widget--user-profile-2">
															<div class="kt-widget__head row  kt-align-center" style="margin: 14px 14px;">
																<div class="col-xl-3">
																</div>
																<div class="col-xl-6">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		Semi Final 01 | 4:00 - 4:50
																	</span>
																</div>
																<div class="col-xl-3">
																</div>
																<div class="col-xl-12">
																	<hr>
																</div>
																<div class="col-xl-5">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		Winner Of Match 1
																	</span>
																</div>
																<div class="col-xl-2">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		VS
																	</span>
																</div>
																<div class="col-xl-5">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		Winner Of Match 3
																	</span>
																</div>
															</div>
														</div>

														<!--end::Widget -->
													</div>
													<div class="" style="width: 4%;"></div>
													<div class="" style="width:45%;background-color: #fff !important;box-sizing: border-box; border-radius: 1rem;">
														<!--begin::Widget -->
														<div class="kt-widget kt-widget--user-profile-2">
															<div class="kt-widget__head row  kt-align-center" style="margin: 14px 14px;">
																<div class="col-xl-3">
																</div>
																<div class="col-xl-6">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		Semi Final 02 | 4:00 - 4:50
																	</span>
																</div>
																<div class="col-xl-3">
																</div>
																<div class="col-xl-12">
																	<hr>
																</div>
																<div class="col-xl-5">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		Winner Of Match 2
																	</span>
																</div>
																<div class="col-xl-2">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		VS
																	</span>
																</div>
																<div class="col-xl-5">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		Winner Of Match 4
																	</span>
																</div>
															</div>
														</div>

														<!--end::Widget -->
													</div>
													<div class="" style="width: 3%;"></div>
												</div>
												<div class="row" style="padding: 20px 0px;">
													<div class="" style="width: 25%;"></div>
													<div class="" style="width:50%;background-color: #fff !important;box-sizing: border-box; border-radius: 1rem;">
														<!--begin::Widget -->
														<div class="kt-widget kt-widget--user-profile-2">
															<div class="kt-widget__head row  kt-align-center" style="margin: 14px 14px;">
																<div class="col-xl-3">
																</div>
																<div class="col-xl-6">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		Grand Final | 5:00 - 5:50
																	</span>
																</div>
																<div class="col-xl-3">
																</div>
																<div class="col-xl-12">
																	<hr>
																</div>
																<div class="col-xl-5">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		Winner Of Semi Final 1
																	</span>
																</div>
																<div class="col-xl-2">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		VS
																	</span>
																</div>
																<div class="col-xl-5">
																	<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg" style="background: darkslateblue;">
																		Winner Of Semi Final 2
																	</span>
																</div>
															</div>
														</div>

														<!--end::Widget -->
													</div>
												</div>
											</div>
										</div>

										<!--End::Portlet-->
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="kt_widget6_tab2_content" aria-expanded="false">
							<div class="kt-notification">
								<div class="row">
									<div class="col-xl-12">

										<!--Begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid" style="background-color: #e4e8ee !important;">
											
											<div class="kt-portlet__body" style="padding: 0px 00px;">

												<div class="row" style="padding: 20px 0px;">
													<div class="" style="width:3% !important;"></div>
													<div class="" style="width:45% !important;background-color: #fff !important;box-sizing: border-box; border-radius: 1rem;">
														<!--begin::Widget -->
														<div class="kt-widget kt-widget--user-profile-2">
															<div class="kt-widget__head row kt-align-left" style="margin: 14px 14px;">
																<div class="col-xl-4">
																	<a href="#" class="kt-media">
																		<img src="https://crm.omtubes.com/assets/hr_document/profile_pic/profile_pic_65.jpeg" alt="image" style="height:100px; max-width:100px;">
																	</a>
																</div>
																<div class="col-xl-8">
																	<div class="kt-widget4__item">
																		<div class="kt-widget4__info">
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Abhishek Desai
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Faraz Siddique
																				</div>
																			</a>
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Prasad Kerkar
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Ashish Madhik
																				</div>
																			</a>
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Jitu Solanki
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Rajkumar Kadam
																				</div>
																			</a>
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Krinjal Mandaliya
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Chetana Shevale
																				</div>
																			</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<!--end::Widget -->
													</div>
													<div class="" style="width:4% !important;"></div>
													<div class="" style="width:45% !important;background-color: #fff !important;box-sizing: border-box; border-radius: 1rem;">
														<!--begin::Widget -->
														<div class="kt-widget kt-widget--user-profile-2">
															<div class="kt-widget__head row kt-align-left" style="margin: 14px 14px;">
																<div class="col-xl-4">
																	<a href="#" class="kt-media">
																		<img src="https://crm.omtubes.com/assets/hr_document/profile_pic/profile_pic_39.jpeg" alt="image" style="height:100px; max-width:100px;">
																	</a>
																</div>
																<div class="col-xl-8">
																	<div class="kt-widget4__item">
																		<div class="kt-widget4__info">
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Riyaz Khan
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Dhiraj Viswakarma
																				</div>
																			</a>
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Vaibhav Joil
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Vinay  Apraj
																				</div>
																			</a>
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Jackson Simson
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Siddhesh Babu
																				</div>
																			</a>
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Rashmi Putta
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Akanksha Dwivedi
																				</div>
																			</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<!--end::Widget -->
													</div>
													<div class="" style="width:3% !important;"></div>
												</div>
												<div class="row" style="padding: 20px 0px;">
													<div class="" style="width:3% !important;"></div>
													<div class="" style="width:45% !important;background-color: #fff !important;box-sizing: border-box; border-radius: 1rem;">
														<!--begin::Widget -->
														<div class="kt-widget kt-widget--user-profile-2">
															<div class="kt-widget__head row  kt-align-left" style="margin: 14px 14px;">
																<div class="col-xl-4">
																	<a href="#" class="kt-media kt-widget__media">
																		<div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-hidden-">P P</div>
																	</a>
																</div>
																<div class="col-xl-8">
																	<div class="kt-widget4__item">
																		<div class="kt-widget4__info">
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Prasad Parker
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Asadullah Khan
																				</div>
																			</a>
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Aftab Quresi
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Sooraj Chaurasya
																				</div>
																			</a>
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Abdullah Bade
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Swapnil Patil
																				</div>
																			</a>
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Sejal Talekar
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Aisha Shaikh
																				</div>
																			</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<!--end::Widget -->
													</div>
													<div class="" style="width:4% !important;"></div>
													<div class="" style="width:45% !important;background-color: #fff !important;box-sizing: border-box; border-radius: 1rem;">
														<!--begin::Widget -->
														<div class="kt-widget kt-widget--user-profile-2">
															<div class="kt-widget__head row  kt-align-left" style="margin: 14px 14px;">
																<div class="col-xl-4">
																	<a href="#" class="kt-media">
																		<img src="https://crm.omtubes.com/assets/hr_document/profile_pic/profile_pic_205.jpg" alt="image" style="height:100px; max-width:100px;">
																	</a>
																</div>
																<div class="col-xl-8">
																	<div class="kt-widget4__item">
																		<div class="kt-widget4__info">
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Rupesh Mhaske
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Atul Sarode
																				</div>
																			</a>
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Manish L
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Mayank Tanna
																				</div>
																			</a>
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Vivek Amar Singh
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Akash  Pawar
																				</div>
																			</a>
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Kesari Panchal
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Urja Jadhav
																				</div>
																			</a>
																			
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<!--end::Widget -->
													</div>
													<div class="" style="width:3% !important;"></div>
												</div>
												<div class="row" style="padding: 20px 0px;">
													<div class="" style="width:3% !important;"></div>
													<div class="" style="width:45% !important;background-color: #fff !important;box-sizing: border-box; border-radius: 1rem;">
														<!--begin::Widget -->
														<div class="kt-widget kt-widget--user-profile-2">
															<div class="kt-widget__head row  kt-align-left" style="margin: 14px 14px;">
																<div class="col-xl-4">
																	<a href="#" class="kt-media">
																		<img src="https://crm.omtubes.com/assets/hr_document/profile_pic/profile_pic_79.jpeg" alt="image" style="height:100px; max-width:100px;">
																	</a>
																</div>
																<div class="col-xl-8">
																	<div class="kt-widget4__item">
																		<div class="kt-widget4__info">
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Hitesh Rathod
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Om Mehta
																				</div>
																			</a>
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Niraj Gupta
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Pawan MIshra
																				</div>
																			</a>
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Saquib Bade
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Atish Kadam
																				</div>
																			</a>
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Rajni Padaya
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Punam Jaiswal
																				</div>
																			</a>
																			
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<!--end::Widget -->
													</div>
													<div class="" style="width:4% !important;"></div>
													<div class="" style="width:45% !important;background-color: #fff !important;box-sizing: border-box; border-radius: 1rem;">
														<!--begin::Widget -->
														<div class="kt-widget kt-widget--user-profile-2">
															<div class="kt-widget__head row  kt-align-left" style="margin: 14px 14px;">
																<div class="col-xl-4">
																	<a href="#" class="kt-media kt-widget__media">
																		<div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-hidden-">P S</div>
																	</a>
																</div>
																<div class="col-xl-8">
																	<div class="kt-widget4__item">
																		<div class="kt-widget4__info">
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Pranay Shetty
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Ahmed Jamadar
																				</div>
																			</a>
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Swapnil Amburle
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Lokesh Dhani
																				</div>
																			</a>
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Gopal Shettiyar
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Rahul Solanki
																				</div>
																			</a>
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Disha Patel
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Sayli Sawant
																				</div>
																			</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<!--end::Widget -->
													</div>
													<div class="" style="width:3% !important;"></div>
												</div>
												<div class="row" style="padding: 20px 0px;">
													<div class="" style="width:3% !important;"></div>
													<div class="" style="width:45% !important;background-color: #fff !important;box-sizing: border-box; border-radius: 1rem;">
														<!--begin::Widget -->
														<div class="kt-widget kt-widget--user-profile-2">
															<div class="kt-widget__head row  kt-align-left" style="margin: 14px 14px;">
																<div class="col-xl-4">
																	<a href="#" class="kt-media kt-widget__media">
																		<div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-hidden-">A S</div>
																	</a>
																</div>
																<div class="col-xl-8">
																	<div class="kt-widget4__item">
																		<div class="kt-widget4__info">
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Aaves Sayed
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Pratik Mehta
																				</div>
																			</a>
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Malu Gandmala
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Mahesh Kumbhar
																				</div>
																			</a>
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Jay Mehta
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Samnan Nachan
																				</div>
																			</a>
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Rajasi Agre
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					seema khade
																				</div>
																			</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<!--end::Widget -->
													</div>
													<div class="" style="width:4% !important;"></div>
													<div class="" style="width:45% !important;background-color: #fff !important;box-sizing: border-box; border-radius: 1rem;">
														<!--begin::Widget -->
														<div class="kt-widget kt-widget--user-profile-2">
															<div class="kt-widget__head row  kt-align-left" style="margin: 14px 14px;">
																<div class="col-xl-4">
																	<a href="#" class="kt-media">
																		<img src="https://crm.omtubes.com/assets/hr_document/profile_pic/profile_pic_76.jpeg" alt="image" style="height:100px; max-width:100px;">
																	</a>
																</div>
																<div class="col-xl-8">
																	<div class="kt-widget4__item">
																		<div class="kt-widget4__info">
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Abhishek Gundu
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Ankit Mishra
																				</div>
																			</a>
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Balamurali Shetty
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Aakash Jadhav
																				</div>
																			</a>
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Syed Shadaab
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Jawaid Islam
																				</div>
																			</a>
																			<a href="#" class="kt-widget4__username kt-font-bold row">
																				<div class="col-xl-6 kt-font-dark">
																					Rushita Salunke
																				</div>
																				<div class="col-xl-6 kt-font-dark">
																					Jayashree Patil
																				</div>
																			</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<!--end::Widget -->
													</div>
													<div class="" style="width:3% !important;"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="kt_widget6_tab3_content" aria-expanded="false">
							<div class="kt-notification"></div>
						</div>
					</div>
				</div>
			</div>

			<!--end:: Widgets/Notifications-->
		</div>
	</div>
</div>
<!--begin::Modal-->
<div class="modal fade" id="kt_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">New message</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<!--begin:: Portlet-->
				<div class="kt-portlet">
					<div class="kt-portlet__body">
						<div class="kt-widget kt-widget--user-profile-3">
							<div class="kt-widget__top">
								<div class="kt-widget__media kt-hidden-">
									<img class="notice_big_image" src="" alt="image" style="width:1200px;height:600px;">
								</div>
								<div class="kt-widget__content">
									<div class="kt-widget__head">
										<div class="kt-widget__action"></div>
									</div>
									<div class="kt-widget__subhead">
									</div>
									<div class="kt-widget__info">
										<div class="kt-widget__desc">
										</div>
									</div>
								</div>
							</div>
							<div class="kt-widget__bottom">
							</div>
						</div>
					</div>
				</div>

				<!--end:: Portlet-->
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->
<!--begin::Modal-->
<div class="modal fade" id="work_comment_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Update task information</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group row">
						<label class="form-control-label col-lg-2" style="margin-top: 0.75rem;">Status</label>
						<div class="col-lg-3 kt-radio-inline">
							<label class="kt-radio">
								<input type="radio" name="work_completed" value="yes"> Completed
								<span></span>
							</label>
							<label class="kt-radio">
								<input type="radio" name="work_completed" value="no"> Pending
								<span></span>
							</label>
						</div>
					</div>
					<div class="form-group row">
						<label class="form-control-label col-lg-2">Comment:</label>
						<textarea class="form-control col-lg-10" id="work_comment" placeholder="You can add comment only for pending task" rows="3"></textarea>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary save_daily_work_on_sales_routine_modal" sales_routine_id="0">Save</button>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->