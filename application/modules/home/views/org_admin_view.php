<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                  
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions exclude__login">
                        <a href="<?php echo base_url('home/edit_organization_chart');?>" class="btn btn-brand btn-elevate btn-icon-sm "  >
                            Add New 
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" style="width:1536px">
                            <thead>
                                <th>No</th>
                                <th>Name</th>
                                <th>Reporting Manager</th>
                                <th>Department</th>
                                <th>Action</th>
                            </thead>
                            <tbody id='org_data' >
                                <?php  foreach($organization_chart_data as $key=>$single_organization_chart_data){
                                    ?>
                                    <tr>
                                        <td>
                                          <?php echo $key+1 ;?>
                                        </td>
                                        <td>
                                         <?php echo $single_organization_chart_data['user_id'];?>
                                        </td>
                                        <td>
                                         <?php echo $single_organization_chart_data['reporting_manager'] ;?>
                                        </td>
                                         <td>
                                         <?php echo $single_organization_chart_data['user_department'] ;?>
                                        </td>
                                        <td>
                                            <!-- <a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_org_data" delete_id="<?php echo $single_organization_chart_data['id']; ?>" title="Delete">
                                                <i class="la la-trash"></i>
                                            </a> -->
                                             <a href="<?php echo base_url('home/edit_organization_chart/'.$single_organization_chart_data['id']);?>" class="btn btn-sm btn-clean btn-icon btn-icon-md edit_organization_chart_data" 
                                            title="edit">
                                                <i class="la la-edit"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php }?>
                            </tbody>
                        </table>
                        <div id="kt_table_1_processing" class="dataTables_processing card" style="display: none; top: 10% !important;">Processing...</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- end:: Content -->

