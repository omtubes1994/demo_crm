<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('user_id')){
			//redirect('login', 'refresh');
			exit;
		}else{
			$access_count = 0;
			if(!empty($this->session->userdata('main_module_access')) && in_array(1, array_column($this->session->userdata('main_module_access'), 'module_id'))) {

				foreach ($this->session->userdata('sub_module_access') as $key => $value) {
					
					if($value['module_id'] == 1 && $value['status'] == 'Active') {
						$access_count++;
					}
				}
			}
			if($access_count == 0){
				redirect($this->session->userdata('home/dashboard'));
				exit;
			}
		}
		error_reporting(0);
		$this->load->model('home_model');
		$this->load->model('common/common_model');
		$this->load->model('login/login_model');
		$this->load->model('production/production_model');
	}

	function index(){
		
		$user_id = $this->session->userdata('user_id');
		$user_organization_chart_details = $this->common_model->get_dynamic_data_sales_db('id, user_id, reporting_manager_id',array('status'=>'Active', 'user_id'=> $user_id), 'user_organization_chart', 'row_array');
		$url = 'home/dashboard';
		// echo "<pre>";print_r($user_organization_chart_details);echo"</pre><hr>";exit;
		if(!empty($user_organization_chart_details)){

			if(($user_organization_chart_details['reporting_manager_id'] == 2)){

				$url .= '/'.$user_organization_chart_details['user_id'];
			}else{

				$user_organization_chart_details_2 = $this->common_model->get_dynamic_data_sales_db('id, user_id, reporting_manager_id',array('status'=>'Active', 'user_id'=> $user_organization_chart_details['reporting_manager_id']), 'user_organization_chart', 'row_array');
				if(($user_organization_chart_details_2['reporting_manager_id'] == 2)){

					$url .= '/'.$user_organization_chart_details_2['user_id'].'/'.$user_organization_chart_details['user_id'];
				}else{

					$user_organization_chart_details_3 = $this->common_model->get_dynamic_data_sales_db('id, user_id, reporting_manager_id',array('status'=>'Active', 'user_id'=> $user_organization_chart_details_2['reporting_manager_id']), 'user_organization_chart', 'row_array');
					if(($user_organization_chart_details_3['reporting_manager_id'] == 2)){
	
						$url .= '/'.$user_organization_chart_details_3['user_id'].'/'.$user_organization_chart_details_2['user_id'].'/'.$user_organization_chart_details['user_id'];
					}
				}
			}
		}
		// echo $url.'    ';die("debug");
		redirect($url);
	}
	public function dashboard(){

		$data = array();
		$this->session->set_userdata(array('type'=> 'OM'));
		if(in_array($this->session->userdata('role'), array(1))) {

			$data = $this->static_image_color();
			$data['select_year'] = $data['year_value'] = '';
			$data['years_list'] = array();
			$sub_module_list = $this->common_model->get_lead_module_name('no', 'yes');
			$data['sub_module_list'] = $sub_module_list['sub_module_details'];
			$data['country_list'] = $this->common_model->get_country_list();
			$data['region_list'] = $this->common_model->get_region_list();
		}
 		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
	 	// END
		$data['todays_birthday_information'] = $this->home_model->get_todays_birthdate_employee();
		$data['notice_type'] = $this->create_notice_details(0);
		
		if($this->session->userdata('production_access')['production_status_wise_count']){
			//production count
			$order_status_wise_total = $this->get_order_wise_total_new($this->session->userdata('type'));
			$data['order_status_wise_total'] = $order_status_wise_total['order_status_wise_total'];
			$data['total_of_3_order'] = $order_status_wise_total['total_of_3_order'];
			$data['total_of_3_order_number'] = $order_status_wise_total['total_of_3_order_number'];
		}
		$data['currency_list'] = array_column($this->common_model->get_dynamic_data_sales_db('currency_id, decimal_number',array('status'=>'Active'), 'currency'),'decimal_number','currency_id');

		
		// echo "<pre>";print_r($data);echo"</pre><hr>";die;
		$segment_array = array();
		$data['level_two_employee_list'] = $this->change_employee_list_data($this->home_model->get_level_2_detail_of_org_graph(2), $segment_array);
		
		$data['allowed_ids'][] = $this->session->userdata('user_id');
		$segment_3 = $this->uri->segment(3, '');
		$data['department_left'] = '';
		$data['level_three_employee_list'] = array();
		if($segment_3 != ''){
			
			$segment_array[] = $segment_3;
			$data['level_three_employee_list'] = $this->change_employee_list_data($this->home_model->get_level_2_detail_of_org_graph($segment_3), $segment_array);
			$data['allowed_ids'] = array_merge($data['allowed_ids'], $this->get_allowed_id($data['allowed_ids'], $data['level_three_employee_list']));
			$data['department_left'] = (($data['level_two_employee_list'][$segment_3]['order_id'] * 10)-5).'%';
			$data['level_two_employee_list'][$segment_3]['style'] = 'background-color: darkgray;';
		}
		
		$segment_4 = $this->uri->segment(4, '');
		$data['department_left'] = '';
		$data['level_four_employee_list'] = array();
		if($segment_4 != ''){

			$segment_array[] = $segment_4;
			$data['level_four_employee_list'] = $this->change_employee_list_data($this->home_model->get_level_2_detail_of_org_graph($segment_4), $segment_array);
			$data['allowed_ids'] = array_merge($data['allowed_ids'], $this->get_allowed_id($data['allowed_ids'], $data['level_four_employee_list']));
			$data['department_left'] = (($data['level_three_employee_list'][$segment_4]['order_id'] * 10)-5).'%';
			$data['level_three_employee_list'][$segment_4]['style'] = 'background-color: darkgray;';
		}

		$segment_5 = $this->uri->segment(5, '');
		$data['department_left'] = '';
		$data['level_five_employee_list'] = array();
		if($segment_5 != ''){

			$segment_array[] = $segment_5;
			$data['level_five_employee_list'] = $this->change_employee_list_data($this->home_model->get_level_2_detail_of_org_graph($segment_5), $segment_array);
			$data['allowed_ids'] = array_merge($data['allowed_ids'], $this->get_allowed_id($data['allowed_ids'], $data['level_five_employee_list']));
			$data['department_left'] = (($data['level_four_employee_list'][$segment_5]['order_id'] * 10)-5).'%';
			$data['level_four_employee_list'][$segment_5]['style'] = 'background-color: darkgray;';
		}

		$segment_6 = $this->uri->segment(6, '');
		$data['department_left'] = '';
		$data['level_six_employee_list'] = array();
		if($segment_6 != ''){

			$segment_array[] = $segment_6;
			$data['level_six_employee_list'] = $this->change_employee_list_data($this->home_model->get_level_2_detail_of_org_graph($segment_6), $segment_array);
			$data['allowed_ids'] = array_merge($data['allowed_ids'], $this->get_allowed_id($data['allowed_ids'], $data['level_six_employee_list']));
			$data['department_left'] = (($data['level_five_employee_list'][$segment_6]['order_id'] * 10)-5).'%';
			$data['level_five_employee_list'][$segment_6]['style'] = 'background-color: darkgray;';
		}

		// echo "<pre>";print_r($data);echo"</pre><hr>";die;
		$this->load->view('header', array('title' => 'Dashboard'));
		$this->load->view('sidebar', array('title' => 'Dashboard'));
		$this->load->view('sales_dashboard',$data);
		$this->load->view('footer');
	}
	private function get_allowed_id($allowed_id_array, $current_level_array){

		// echo "<pre>";print_r($current_level_array);echo"</pre><hr>";
		$return_array = array();
		foreach ($current_level_array as $single_data) {

			if(in_array($single_data['reporting_manager_id'], $allowed_id_array)){

				$return_array[] = $single_data['tab_name'];
			}
		}
		return $return_array;
	}
	private function change_employee_list_data($data, $segment_array){

		// echo "<pre>";print_r($data);echo"</pre><hr>";die;
		$return_array = array();
		$segment = '';
		if(!empty($segment_array)){

			$segment = implode("/", $segment_array);
			$segment .= "/";
		}
		foreach ($data as $key => $level_details) {
			
			$return_array[$level_details['tab_name']] = $level_details;
			$return_array[$level_details['tab_name']]['url'] = $segment."".$level_details['tab_name'];
			$return_array[$level_details['tab_name']]['count'] = count($this->common_model->get_dynamic_data_sales_db('id', array('status'=>'Active', 'reporting_manager_id'=>$level_details['tab_name']), 'user_organization_chart'));
		}

		return $return_array;
	}
	public function create_database_data(){
        // die('hii');
        echo PHP_EOL."Process Started".PHP_EOL;
        $data_transfer_object = $this->load->database('sales_db', true);
		// echo "<pre>";var_dump($data_transfer_object);echo"</pre><hr>";exit;
        echo PHP_EOL."db_got_connect".PHP_EOL;
        $db_util_object = $this->load->dbutil($data_transfer_object, true);
        echo PHP_EOL."db_util_got_connect".PHP_EOL;
        /* ---------------------------------------------------------------------- */
                                    /* Db utility use case */
        /* ---------------------------------------------------------------------- */
        //list all database exists
        $database_exist = $db_util_object->database_exists('qmznzeyjub');
        // echo "<pre>";var_dump($database_exist);echo"</pre><hr>";exit;
        if($database_exist){

            echo PHP_EOL."Database Exist!!!!".PHP_EOL;
            $table_list  = $data_transfer_object->list_tables();
            // echo "<pre>";print_r($table_list);echo"</pre><hr>";exit;
            if(!empty($table_list)){

                echo PHP_EOL."Total Number of table is :".count($table_list);
                foreach ($table_list as $table_no => $table_name) {
                    
                    // if($table_no >= 150 && $table_no <= 200){
                    if(true){

                        echo PHP_EOL.PHP_EOL."Table Name : ".$table_name." Process Started.";
                        echo PHP_EOL."Checking Table Exist Or Not";
                        if($this->db->table_exists($table_name)){
                            
                            echo PHP_EOL."Table Exist";
                            if($this->db->count_all_results($table_name) == 0){
    
                                echo PHP_EOL."Checking Table Data Exist Or Not";
                                $table_result = $data_transfer_object->get($table_name)->result_array();
                                // echo "<pre>";print_r($table_result);echo"</pre><hr>";exit;
                                if(!empty($table_result)){
        
                                    echo PHP_EOL."Table Data Exist";
                                    echo PHP_EOL."Inserting Data Into Table Started";
                                    $this->db->insert_batch($table_name, $table_result);
                                    echo PHP_EOL."Inserting Data Into Table Ended";
                                }else{
        
                                    echo PHP_EOL."Table Data Does Not Exist";
                                }
                            }else{
    
                                echo PHP_EOL."Table Data Is already inserted";
                            }
                        }else{
    
                            echo PHP_EOL."Table Does Not Exist";
                        }                    
                        echo PHP_EOL."Table Name : ".$table_name." Process Ended.";
                        echo PHP_EOL."Table Left For operation is : ".(count($table_list)-($table_no+1));
                        // if(!empty($table_result)){
    
                        //     die('done one process');
                        // }
                    }
                }
                echo PHP_EOL."All Table Done";
            }
        }
        
        /* ---------------------------------------------------------------------- */
        echo PHP_EOL."Process Ended".PHP_EOL;
		
	}
	public function show_session(){

		echo "<pre>"; print_r($this->session->userdata()); "</prer>"; exit;
	}
	public function create_database_structure_marketing(){

        $data_transfer_object = $this->load->database('data_transfer_marketing', true);
        $db_util_object = $this->load->dbutil($data_transfer_object, true);
	            
      	// Backup your entire database and assign it to a variable
      	$prefs = array(
    	  	'tables'        => array(),   // Array of tables to backup.
    	  	'ignore'        => array('ac_blocked', 'ac_contacts', 'ac_groupchat', 'ac_guests', 'ac_guests_messages', 'ac_messages', 'ac_profiles','ac_settings', 'ac_users_messages','clients_country_view', 'clients_country_view2', 'company_touch_point', 'confimred_on_view', 'member_touch_point', 'quotation2_mid', 'quotation_final', 'quotation_gds_report', 'quotation_gds_report_bp', 'quotation_gds_report_bp_test', 'quotation_gds_report_test', 'quotation_mid_confirmed_on', 'qutation_view1', 'rfq_quota', 'rfq_view', 'touch_points_view', 'user_view1', 'view1', 'people_request_info'),   // List of tables to omit from the backup
          	'format'        => 'zip',     // gzip, zip, txt
          	'filename'      => 'po_sales.sql',  // File name - NEEDED ONLY WITH ZIP FILES
          	'add_drop'      => TRUE,                        // Whether to add DROP TABLE statements to backup file
          	'add_insert'    => false,                        // Whether to add INSERT data to backup file
    	  	'newline'       => "\n"                         // Newline character used in backup file
      	);

      	$backup = $db_util_object->backup($prefs);
      	// echo "<pre>";print_r($backup);echo"</pre><hr>";exit;
      	$this->load->helper('download');
      	force_download('po_sales.zip', $backup);
      	die('done one process');
    }
	public function graph(){

		$data = array();
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Graph Dashboard'));
		$this->load->view('sidebar', array('title' => 'Graph Dashboard'));
		$this->load->view('home/graph_dashboard', $data);
		$this->load->view('footer');
	}
	public function graph_old() {

		$data['org_chart_details'] = array('level_1'=>array(), 'level_2'=>array(), 'level_3'=>array(), 'level_4'=>array()); 
		$level_2_data = $this->home_model->get_level_2_org_charts_details();
		// echo "<pre>";print_r($level_2_data);echo"</pre><hr>";die;
		$data['org_chart_details']['level_2'] = $level_2_data;
		foreach ($level_2_data as $key => $value) {
			$data['org_chart_details']['level_2'][$key]['width'] = 50;
			$color = "#007bff";	
			if($value['user_department'] == 'Quality') {
				$color = "#fd7e14";	
			}else if(in_array($value['user_department'],array('Developer'))) {
				$color = "#ff0000";	
			}else if($value['user_department'] == 'Sales') {
				$color = "#ffb822";	
			}else if($value['user_department'] == 'Hr') {
				$color = "#007bff";	
			}else if($value['user_department'] == 'Documentation Logistics') {
				$color = "#fd7e14";	
			}
			$data['org_chart_details']['level_2'][$key]['color'] = $color;
			$level_3_data = $this->home_model->get_org_charts_details(3, $value['user_id']);
			// echo "<pre>";print_r($level_3_data);echo"</pre><hr>";exit;
			if(!empty($level_3_data)){

				foreach ($level_3_data as $level_3_key => $level_3_value) {
					
					$data['org_chart_details']['level_3'][] = array(
																'id'=>$level_3_value['id'],
																'user_id'=>$level_3_value['user_id'],
																'reporting_manager'=>$level_3_value['reporting_manager'],
																'user_designation'=>$level_3_value['user_designation'],
																'name'=>$level_3_value['name'],
																'profile_pic_file_path'=>$level_3_value['profile_pic_file_path'],
																'user_department'=>$level_3_value['user_department'],
																'username'=>$level_3_value['username'],
																'password'=>$level_3_value['password'],
																'width'=>50,
															);
					$level_4_data = $this->home_model->get_org_charts_details(4, $level_3_value['user_id']);
					if(!empty($level_4_data)) {

						foreach ($level_4_data as $level_4_key => $level_4_value) {
						
							$data['org_chart_details']['level_4'][] = array(
																	'id'=>$level_4_value['id'],
																	'user_id'=>$level_4_value['user_id'],
																	'reporting_manager'=>$level_4_value['reporting_manager'],
																	'user_designation'=>$level_4_value['user_designation'],
																	'name'=>$level_4_value['name'],
																	'profile_pic_file_path'=>$level_4_value['profile_pic_file_path'],
																	'user_department'=>$level_4_value['user_department'],
																	'username'=>$level_4_value['username'],
																	'password'=>$level_4_value['password'],
																	'width'=>50,
																);
						}
					}else{
						$data['org_chart_details']['level_4'][] = array(
																	'width'=>50,
																);
					}
				}
			}else{
				$data['org_chart_details']['level_3'][] = array(
															'width'=>50,
														);
				$data['org_chart_details']['level_4'][] = array(
																'width'=>50,
															);
			}
		}
		$level_3_reporting_manager_wise_count = array();
		// echo "<pre>";print_r($data['org_chart_details']['level_3']);echo"</pre><hr>";exit;
		foreach ($data['org_chart_details']['level_3'] as $value) {

			if(array_key_exists($value['reporting_manager'], $level_3_reporting_manager_wise_count)){

				$level_3_reporting_manager_wise_count[$value['reporting_manager']] += 1;
			}else{

				$level_3_reporting_manager_wise_count[$value['reporting_manager']] = 0;
			}
		}

		foreach ($data['org_chart_details']['level_2'] as $key => $value) {
			
			if(array_key_exists($value['user_id'], $level_3_reporting_manager_wise_count)){

				if($level_3_reporting_manager_wise_count > 0){

					$data['org_chart_details']['level_2'][$key]['width'] += (200*($level_3_reporting_manager_wise_count[$value['user_id']]));
					$data['org_chart_details']['level_2'][$key+1]['width'] += (200*($level_3_reporting_manager_wise_count[$value['user_id']]));
				}
			}
		}

		$level_4_reporting_manager_wise_count = array();
		// echo "<pre>";print_r($data['org_chart_details']['level_3']);echo"</pre><hr>";exit;
		foreach ($data['org_chart_details']['level_4'] as $value) {

			if(array_key_exists($value['reporting_manager'], $level_4_reporting_manager_wise_count)){

				$level_4_reporting_manager_wise_count[$value['reporting_manager']] += 1;
			}else{

				$level_4_reporting_manager_wise_count[$value['reporting_manager']] = 0;
			}
		}
		foreach ($data['org_chart_details']['level_3'] as $level_3_key => $level_3_value) {

			if(array_key_exists($level_3_value['user_id'], $level_4_reporting_manager_wise_count)){

				if($level_4_reporting_manager_wise_count > 0){


					$data['org_chart_details']['level_3'][$level_3_key]['width'] += (200*($level_4_reporting_manager_wise_count[$level_3_value['user_id']]));
					$data['org_chart_details']['level_3'][$level_3_key+1]['width'] += (200*($level_4_reporting_manager_wise_count[$level_3_value['user_id']]));
					foreach ($data['org_chart_details']['level_2'] as $level_2_key => $level_2_value) {

						if($level_3_value['reporting_manager'] == $level_2_value['user_id']){

							$data['org_chart_details']['level_2'][$level_2_key]['width'] += (200*($level_4_reporting_manager_wise_count[$level_3_value['user_id']]));
							$data['org_chart_details']['level_2'][$level_2_key+1]['width'] += (200*($level_4_reporting_manager_wise_count[$level_3_value['user_id']]));	
						}
					}
				}
			}
		}
		foreach ($data['org_chart_details']['level_3'] as $level_3_key => $level_3_value) {

			if(!array_key_exists('id', $data['org_chart_details']['level_3'][$level_3_key]) ){

				$data['org_chart_details']['level_3'][$level_3_key+1]['width'] = ($data['org_chart_details']['level_3'][$level_3_key+1]['width']-($data['org_chart_details']['level_3'][$level_3_key]['width']-50));
				$data['org_chart_details']['level_3'][$level_3_key]['width'] = 50;
			}
		}
		for($level=4; $level>1; $level--){

			for($i = count($data['org_chart_details']['level_'.$level])-1; $i>0; $i--){

				if(!array_key_exists('id', $data['org_chart_details']['level_'.$level][$i]) ){
					
					unset($data['org_chart_details']['level_'.$level][$i]);
				}else{
					break;
				}
			}
		}
		// echo "<pre>";print_r($level_4_reporting_manager_wise_count);echo"</pre><hr>";exit;
		$data['allow_other_person_login_array'] = array_column($this->home_model->get_assign_person_list(), 'user_id');
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Graph Dashboard'));
		$this->load->view('sidebar', array('title' => 'Graph Dashboard'));
		$this->load->view('home/graph_dashboard', $data);
		$this->load->view('footer');
	}
	public function graph_new(){

		$data = array();
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Graph Dashboard'));
		$this->load->view('sidebar', array('title' => 'Graph Dashboard'));
		$this->load->view('home/graph_new', $data);
		$this->load->view('footer');
	}
	public function graph_new_(){

		$data = array();
		$data['level_two_employee_list'] = $this->common_model->get_dynamic_data_sales_db('user_id as tab_name, name, designation,  "background-color: rgba(40, 42, 60, 0.1);" as style', array('status'=> 'Active', 'reporting_manager_id'=>2), 'user_organization_chart');

		$segment_4 = $this->uri->segment(4, '');
		$data['department_left'] = '';
		$data['level_three_employee_list'] = array();
		if($segment_4 != ''){
			$static_array = array(
				'15'=>array('department_left'=> '15%', 'key_number'=> 1),
				'10'=>array('department_left'=> '25%', 'key_number'=> 2),
				'65'=>array('department_left'=> '35%', 'key_number'=> 3),
				'102'=>array('department_left'=> '45%', 'key_number'=> 4),
				'118'=>array('department_left'=> '55%', 'key_number'=> 5),
				'33'=>array('department_left'=> '65%', 'key_number'=> 6),
				'23'=>array('department_left'=> '75%', 'key_number'=> 7),
				'39'=>array('department_left'=> '85%', 'key_number'=> 8),
				'193'=>array('department_left'=> '95%', 'key_number'=> 9),
			);
			if(!empty($static_array[$segment_4])){

				$data['level_three_employee_list'] = $this->common_model->get_dynamic_data_sales_db('user_id as tab_name, name, designation,  "background-color: rgba(40, 42, 60, 0.1);" as style', array('status'=> 'Active', 'reporting_manager_id'=>$segment_4), 'user_organization_chart');
				$data['department_left'] = $static_array[$segment_4]['department_left'];
				$data['level_two_employee_list'][$static_array[$segment_4]['key_number']]['style'] = 'background-color: darkgray;';
			}
		}
		
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Graph Dashboard'));
		$this->load->view('sidebar', array('title' => 'Graph Dashboard'));
		$this->load->view('home/graph_new_'.$this->uri->segment(3, 1), $data);
		$this->load->view('footer');
	}
	public function graph_management(){

		$data = array();
		$data['level_two_employee_list'] = $this->common_model->get_all_conditional_data_sales_db('id, user_id as tab_name, name, reporting_manager_id, designation,  "background-color: rgba(40, 42, 60, 0.1);" as style', array('status'=> 'Active', 'reporting_manager_id'=>2), 'user_organization_chart', 'result_array', array(), array('column_name'=> 'order_id', 'column_value'=>'asc'));
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;

		$segment_4 = $this->uri->segment(4, '');
		$data['department_left'] = '';
		$data['level_three_employee_list'] = array();
		if($segment_4 != ''){
			$static_array = array(
				'15'=>array('department_left'=> '15%', 'key_number'=> 1),
				'10'=>array('department_left'=> '25%', 'key_number'=> 2),
				'65'=>array('department_left'=> '35%', 'key_number'=> 3),
				'102'=>array('department_left'=> '45%', 'key_number'=> 4),
				'118'=>array('department_left'=> '55%', 'key_number'=> 5),
				'33'=>array('department_left'=> '65%', 'key_number'=> 6),
				'23'=>array('department_left'=> '75%', 'key_number'=> 7),
				'39'=>array('department_left'=> '85%', 'key_number'=> 8),
				'193'=>array('department_left'=> '95%', 'key_number'=> 9),
			);
			if(!empty($static_array[$segment_4])){

				$data['level_three_employee_list'] = $this->common_model->get_dynamic_data_sales_db('id, user_id as tab_name, name, designation,  "background-color: rgba(40, 42, 60, 0.1);" as style', array('status'=> 'Active', 'reporting_manager_id'=>$segment_4), 'user_organization_chart');
				$data['department_left'] = $static_array[$segment_4]['department_left'];
				$data['level_two_employee_list'][$static_array[$segment_4]['key_number']]['style'] = 'background-color: darkgray;';
			}
		}

		$data['next_order_id'] = (count($data['level_two_employee_list']) + 1);
		$data['reporting_manager_id'] = 2;

		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;

		$this->load->view('header', array('title' => 'Graph Dashboard'));
		$this->load->view('sidebar', array('title' => 'Graph Dashboard'));
		$this->load->view('home/graph_management', $data);
		$this->load->view('footer');
	}
	public function graph_d3(){

		$data = array();
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		// $this->load->view('header', array('title' => 'Graph Dashboard'));
		// $this->load->view('sidebar', array('title' => 'Graph Dashboard'));
		$this->load->view('home/graph_new_5', $data);
		// $this->load->view('footer');
	}
	public function graph_vis(){

		$data = array();
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		// $this->load->view('header', array('title' => 'Graph Dashboard'));
		// $this->load->view('sidebar', array('title' => 'Graph Dashboard'));
		$this->load->view('home/graph_vis', $data);
		// $this->load->view('footer');
	}
	public function matches(){

		$data = array();
		$this->load->view('header', array('title' => 'Graph Dashboard'));
		$this->load->view('sidebar', array('title' => 'Graph Dashboard'));
		$this->load->view('home/matches', $data);
		$this->load->view('footer');
	}
	public function graph_bckp() {

		$data['org_chart_details'] = array('level_1'=>array(), 'level_2'=>array(), 'level_3'=>array(), 'level_4'=>array()); 
		$level_2_data = $this->home_model->get_level_2_org_charts_details();
		$department_start_count = array('Procurement'=>0,'Quality'=>0,'Developer'=>0,'Business Analyst'=>0,'Research Analyst'=>0,'Sales'=>0,'Hr'=>0,'Documentation Logistics'=>0);
		$department_start_count_level_4 = array('Procurement'=>0,'Quality'=>0,'Developer'=>0,'Business Analyst'=>0,'Research Analyst'=>0,'Sales'=>0,'Hr'=>0,'Documentation Logistics'=>0);
		$level_3_increment_value = 0;
		$level_4_increment_value = 0;
		$data['org_chart_details']['level_2'] = $level_2_data;
		foreach ($level_2_data as $key => $value) {
			$data['org_chart_details']['level_2'][$key]['width'] += 50;
			$color = '';
			if($value['user_department'] == 'Procurement') {
				$width_level_2 = 800;

			}else if($value['user_department'] == 'Quality') {
				$width_level_2 = 1900;
			}else if(in_array($value['user_department'],array('Developer'))) {

				$width_level_2 = 1500;
			}else if(in_array($value['user_department'],array('Business Analyst', 'Research Analyst'))) {

				$width_level_2 = 100;
			}else if($value['user_department'] == 'Sales') {
				$width_level_2 = 1900;
			}else if($value['user_department'] == 'Hr') {
				$width_level_2 = 3525;
			}else if($value['user_department'] == 'Documentation Logistics') {
				$width_level_2 = 1100;
			}
			if($value['user_department'] == 'Procurement') {
				$color = "#007bff";	
			}else if($value['user_department'] == 'Quality') {
				$color = "#fd7e14";	
			}else if(in_array($value['user_department'],array('Developer', 'Business Analyst', 'Research Analyst'))) {
				$color = "#ff0000";	
			}else if($value['user_department'] == 'Sales') {
				$color = "#ffb822";	
			}else if($value['user_department'] == 'Hr') {
				$color = "#007bff";	
			}else if($value['user_department'] == 'Documentation Logistics') {
				$color = "#fd7e14";	
			}
			$data['org_chart_details']['level_2'][$key]['color'] = $color;
			// create level 3 data
			$level_3_data = $this->home_model->get_org_charts_details(3, $value['user_id']);
			if(empty($level_3_data)) {
				if($value['user_department'] == 'Procurement') {
					$width = 50;
				}else {
					$width = 100;
				}
				$data['org_chart_details']['level_3'][$level_3_increment_value]['width'] += $width;
				// echo "<pre>";print_r($get_user_under_this_person_details);echo"</pre><hr>";exit;
				$department_start_count[$get_user_under_this_person_details['user_department']] += 1;
				if($value['user_department'] == 'Procurement') {
					$width_level_4 = 50;
				}else {
					$width_level_4 = 100;
				}
				$data['org_chart_details']['level_4'][$level_4_increment_value]['width'] += $width_level_4;
				$department_start_count_level_4[$get_user_under_this_person_details['user_department']] += 1;
				$level_4_increment_value++;
				$level_3_increment_value++;
			}else{

				foreach($level_3_data as $get_user_under_this_person_details) {

					if($department_start_count[$get_user_under_this_person_details['user_department']] == 0){
						if($value['user_department'] == 'Procurement') {
							$width = 50;
						}else {
							// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
							$width = 100;
						}
					}else {
						$width = 50;
					}
					$temp_width = 0;
					if(!empty($data['org_chart_details']['level_3'][$level_3_increment_value]['width'])) {
						$temp_width = $data['org_chart_details']['level_3'][$level_3_increment_value]['width'];
					}
					$data['org_chart_details']['level_3'][$level_3_increment_value] = $get_user_under_this_person_details;
					$data['org_chart_details']['level_3'][$level_3_increment_value]['width'] += $width+$temp_width;
					$data['org_chart_details']['level_3'][$level_3_increment_value]['color'] = $color;


					$department_start_count[$get_user_under_this_person_details['user_department']] += 1;
					$get_user_under_this_person = $this->home_model->get_org_charts_details(4, $get_user_under_this_person_details['user_id']);
					if(empty($get_user_under_this_person)) {
						if($department_start_count_level_4[$get_user_under_this_person_details['user_department']] == 0){
							if($get_user_under_this_person_details['user_department'] == 'Procurement') {
								$width_level_4 = 50;
							}else {
								$width_level_4 = 100;
							}
						}else {
							$width_level_4 = 50;
						}
						$data['org_chart_details']['level_4'][$level_4_increment_value]['width'] += $width_level_4;
						$department_start_count_level_4[$get_user_under_this_person_details['user_department']] += 1;
						$level_4_increment_value++;
					}else{
						foreach($get_user_under_this_person as $get_user_under_this_person_details_level_4) {
							if($department_start_count_level_4[$get_user_under_this_person_details_level_4['user_department']] == 0){
								if($get_user_under_this_person_details_level_4['user_department'] == 'Procurement') {
									$width_level_4 = 50;
								}else {
									$width_level_4 = 100;
								}
							}else {
								$width_level_4 = 50;	
							}
							$data['org_chart_details']['level_4'][$level_4_increment_value] = $get_user_under_this_person_details_level_4;
							$data['org_chart_details']['level_4'][$level_4_increment_value]['width'] = $width_level_4;
							$data['org_chart_details']['level_4'][$level_4_increment_value]['color'] = $color;
							$department_start_count_level_4[$get_user_under_this_person_details_level_4['user_department']] += 1;
							$level_4_increment_value++;
						}
						if(count($get_user_under_this_person) > 1){

							$current_width_sum = (((400*count($get_user_under_this_person))/2)-200);
							$data['org_chart_details']['level_3'][$level_3_increment_value]['width'] += $current_width_sum;
							$data['org_chart_details']['level_3'][$level_3_increment_value+1]['width'] += $current_width_sum;
							$data['org_chart_details']['level_2'][$key]['width'] += $current_width_sum;
							if(!empty($data['org_chart_details']['level_2'][$key+1])) {

								$data['org_chart_details']['level_2'][$key+1]['width'] += $current_width_sum;
							}
						}
					}
					$level_3_increment_value++;
				}
			}
			$total_width = 0;
			foreach ($data['org_chart_details']['level_3'] as $chart_level_3_details) {
				if($chart_level_3_details['user_department'] == $value['user_department']) {

					$total_width += 400;
				}
			}
			if($total_width != 0) {

				$data['org_chart_details']['level_2'][$key]['width'] += ($total_width/2)-200;
				if(!empty($data['org_chart_details']['level_2'][$key+1])) {

					$data['org_chart_details']['level_2'][$key+1]['width'] += ($total_width/2)-150;
				}
			}
		}
		$data['org_chart_details']['level_2'][0]['width'] = 1650;
		$data['org_chart_details']['level_2'][1]['width'] = 2700;
		$data['org_chart_details']['level_2'][6]['width'] = 3250;
		$data['allow_other_person_login_array'] = array_column($this->home_model->get_assign_person_list(), 'user_id');
		$this->load->view('header', array('title' => 'Graph Dashboard'));
		$this->load->view('sidebar', array('title' => 'Graph Dashboard'));
		$this->load->view('home/graph_dashboard', $data);
		$this->load->view('footer');
	}
	public function test_country() {

		$data = array();
		$this->load->view('header', array('title' => 'Sales Dashboard'));
		$this->load->view('sidebar', array('title' => 'Sales Dashboard'));
		$this->load->view('country_chart', $data);
		$this->load->view('footer');

	}
	public function currency(){
		if($this->session->userdata('role') == 1){
			if(!empty($this->input->post())){
				foreach ($this->input->post() as $key => $value) {
					$this->home_model->updateData('currency', array('currency_rate' => $value), array('currency_id' => trim($key, 'curr_')));
				}
				redirect('home/currency');
			}else{
				$data['currency'] = $this->home_model->getData('currency');
				$this->load->view('header', array('title' => 'Currency Rates'));
				$this->load->view('sidebar', array('title' => 'Currency Rates'));
				$this->load->view('currency_view', $data);
				$this->load->view('footer');
			}
		}else{
			redirect($this->session->userdata('module')[0]);
		}
	}
	function updateTask(){

		$already_inserted = $this->home_model->getData('daily_task_update', "user_id = ".$this->session->userdata('user_id')." and date = '".date('Y-m-d', strtotime($this->input->post('date')))."'");
		if(!empty($already_inserted) && $this->input->post('master_id') == ''){
			$msg = 'Details already updated for date - '.$this->input->post('date');
			$status = 'danger';
		}else{
			$insert_arr = array(
				'task_accomplished' => $this->input->post('task_accomplished'),
				'work_in_progress' => $this->input->post('work_in_progress'),
				'plan_for_tomorrow' => $this->input->post('plan_for_tomorrow'),
				'ca' => $this->input->post('ca'),
				'cc' => $this->input->post('cc'),
				'rc' => $this->input->post('rc'),
				'wa' => $this->input->post('wa'),
				'qs' => $this->input->post('qs'),
				'rd' => $this->input->post('rd'),
				'rp' => $this->input->post('rp'),
				'qp' => $this->input->post('qp'),
				'email_sent' => $this->input->post('email_sent'),
				'email_received' => $this->input->post('email_received'),
				'user_id' => $this->session->userdata('user_id'),
				'date' => date('Y-m-d', strtotime($this->input->post('date'))),
				'modified_on' => date('Y-m-d H:i:s')
			);

			if($this->input->post('master_id') > 0){
				$this->home_model->updateData('daily_task_update', $insert_arr, array('master_id' => $this->input->post('master_id')));
			}else{
				$insert_arr['entered_on'] = date('Y-m-d H:i:s');
				$this->home_model->insertData('daily_task_update', $insert_arr);
			}
			$msg = 'Details updated successfully.';
			$status = 'success';
		}

		$list = $this->home_model->getData('daily_task_update', 'user_id = '.$this->session->userdata('user_id'));
		echo json_encode(array('list' => $list, 'msg' => $msg, 'status' => $status));
	}
	function getDailyUpdates(){
		$list = $this->home_model->getData('daily_task_update', 'user_id = '.$this->session->userdata('user_id'));
		echo json_encode(array('list' => $list));
	}
	function deleteTask(){
		$master_id = $this->input->post('master_id');
		$this->home_model->deleteData('daily_task_update', array('master_id' => $master_id));

		$list = $this->home_model->getData('daily_task_update', 'user_id = '.$this->session->userdata('user_id'));
		echo json_encode(array('list' => $list));
	}
	function getTaskDetails(){
		$master_id = $this->input->post('master_id');
		$res = $this->home_model->getData('daily_task_update', 'master_id = '.$master_id);
		$res['date'] = date('d-m-Y', strtotime($res['date']));
		echo json_encode($res[0]);
	}
	function calendar($user_id = 0){
		
		$data = array();
		$user_id = $this->uri->segment('3',$this->session->userdata('user_id'));
		$data['users'] = array();
		if($this->session->userdata('role') == 1 || $this->session->userdata('role') == 13) {

			$data['users'] = $this->home_model->getData('users', 'status = 1 and role != 1');
		} else if ($this->session->userdata('role') == 16) {

			$data['users'] = $this->home_model->getData('users', 'status = 1 and role = 5');
		} else if ($this->session->userdata('role') == 6) { 

			$data['users'] = $this->home_model->getData('users', 'status = 1 and role = 8');
		}else if ($this->session->userdata('role') == 11) { 

			$data['users'] = $this->home_model->getData('users', 'status = 1 and role =10');
		}
		else if ($this->session->userdata('user_id')==29) { 

			$data['users'] = $this->home_model->getData('users', 'status = 1 and role =7');
		}
		$data['user_dtl'] = $this->home_model->getData('users', 'user_id = '.$user_id);
		$data['daily_tasks'] = $this->home_model->getData('daily_task_update', 'user_id = '.$user_id);
		$data['desktopTime'] = $this->home_model->getMachineTime($user_id);
		$this->load->view('header', array('title' => 'DAILY TASK REPORT CALENDER VIEW'));
		$this->load->view('sidebar', array('title' => 'DAILY TASK REPORT CALENDER VIEW'));
		$this->load->view('calendar_view', $data);
		$this->load->view('footer');
	}
	function action_event()
    {
        //set validation rules
        $this->form_validation->set_rules('title', 'title', 'trim|required');
        $this->form_validation->set_rules('start_date', 'start_date', 'required');
        $this->form_validation->set_rules('start_time', 'start_time', 'required');
        $this->form_validation->set_rules('end_date', 'end_date', 'required');
        $this->form_validation->set_rules('end_time', 'end_time', 'required');
        $this->form_validation->set_rules('description', 'description', 'trim|required');
        
        //run validation check
        if ($this->form_validation->run() == FALSE)
        {   //validation fails
            echo validation_errors();
        }
        else
        {
             $insert_arr = array(
            'event_title' => $this->input->post('title'),
            'start_date' => $this->input->post('start_date'),
            'start_time' => $this->input->post('start_time'),
            'end_date' => $this->input->post('end_date'),
            'end_time' => $this->input->post('end_time'),
            'description' => $this->input->post('description'),
            'user_id' => $this->session->userdata('user_id'),
            'modified_on' => date('Y-m-d H:i:s')
            );
            
				$insert_arr['entered_on'] = date('Y-m-d H:i:s');
				$this->home_model->insertData('calendar_events', $insert_arr);
				echo 'YES';
			
            
        }
    }
    function getNotifications(){
    	$list = $this->home_model->getData('notifications', 'for_id = '.$this->session->userdata('user_id'));
    	if(!empty($list)){
    		$ret = array();
    		foreach ($list as $key => $value) {
    			$return[$key]['text'] = urldecode($value['notify_str']);
    			$return[$key]['time'] = $this->time_elapsed_string($value['notify_date']);
    		}
    		echo json_encode(array( 'list' => $return, 'count' => sizeof($return)));
    	}
    }
    function time_elapsed_string($datetime, $full = false) {
	    $now = new DateTime;
	    $ago = new DateTime($datetime);
	    $diff = $now->diff($ago);

	    $diff->w = floor($diff->d / 7);
	    $diff->d -= $diff->w * 7;

	    $string = array(
	        'y' => 'year',
	        'm' => 'month',
	        'w' => 'week',
	        'd' => 'day',
	        'h' => 'hour',
	        'i' => 'minute',
	        's' => 'second',
	    );
	    foreach ($string as $k => &$v) {
	        if ($diff->$k) {
	            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
	        } else {
	            unset($string[$k]);
	        }
	    }

	    if (!$full) $string = array_slice($string, 0, 1);
	    return $string ? implode(', ', $string) . ' ago' : 'just now';
	}
	function vendor_management() {
		redirect('home/master_settings/'.$this->uri->segment('3'), 'refresh');
	}
	function master_settings() {

		$call_type = 'product';
		if(!empty($this->uri->segment('3'))) {

			$static_array = array(
								'Product'=> 'product', 'Material'=> 'material', 
								'Delivery%20Place' => 'delivery', 'Delivery%20Time' => 'delivery_time',
								'Payments'=> 'payment_terms', 'Validity'=> 'validity', 
								'Currency' => 'currency', 'Country%20Origin' => 'origin_country',
								'MTC%20Type' => 'mtc_type', 'Transport%20Mode' => 'transport_mode',
								'Lead%20Stage%20Reasons' => 'lead_stage_reasons','Quotation%20Close%20Reasons'=>'quotation_close_reasons', 'RFQ%20Close%20Reasons'=>'rfq_close_reasons'
							);                                                    
			// echo $this->uri->segment('3');die;
			$call_type = $static_array[$this->uri->segment('3')];
		}
		// echo $call_type, "<hr>";die('debug');
		$data = $this->prepare_all_view_details($call_type);
		$data['transport_mode'] = $this->home_model->get_all_data('mode, mode_id',array('status' => 'Active'),'transport_mode');
		$data['call_type'] = $call_type;
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Manage Product and Material'));
		$this->load->view('sidebar', array('title' => 'Manage Product and Material'));
		$this->load->view('vendor_manage', $data);
		$this->load->view('footer');
	}
	public function organization_chart(){
		$data['organization_chart_data'] =array();
		$org_data=$this->home_model->get_all_data('*',array('status' => 'Active', 'user_department !='=>'Admin'),'organization_chart');
		$users=array_column($this->home_model->get_all_data('*',array('status'=>1),'users'),'name', 'user_id');
		foreach($org_data as $key=>$single_org_data){
			$org_data[$key]['user_id']=$users[$single_org_data['user_id']];
			$org_data[$key]['reporting_manager']=$users[$single_org_data['reporting_manager']];
			$data['organization_chart_data']=$org_data;
			$data['users']=$users;
		}

	    // echo "<pre>";print_r($data);echo"</pre><hr>";exit;
	    $this->load->view('header', array('title' => 'organization_chart'));
		$this->load->view('sidebar', array('title' => 'organization chart'));
		$this->load->view('home/org_admin_view', $data);
		$this->load->view('footer');
		
	}
	public function edit_organization_chart($organization_chart_id=''){

		$data['organization_chart_data'] =array();
		$org_data=$this->home_model->get_all_data('*',array('status' => 'Active','id'=>$organization_chart_id),'organization_chart' );
		//echo "<pre>";print_r($org_data);echo"</pre><hr>";
		$users=array_column($this->home_model->get_all_data('*',array('status'=>1,),'users'),'name', 'user_id');
		$reporting_id=array_column($this->home_model->getData('organization_chart',array('status'=>'Active'),'reporting_manager'),'user_designation');
       //echo "<pre>";print_r($reporting_id);echo"</pre><hr>";exit;
		$organization_designation=array_column($this->common_model->get_all_conditional_data_sales_db('user_designation',array('status'=>'Active'),'organization_chart','result_array','','','user_designation'),'user_designation');

		if(!empty($organization_chart_id)){
		$organization_user_list=array_column($this->home_model->get_all_data('user_id,id',array('status'=>'Active','user_department'=>$org_data[0]['user_department'],'user_reporting_level <'=>$org_data[0]['user_reporting_level']),'organization_chart'),'user_id','id');}
		//echo "<pre>";print_r($organization_user_list);echo"</pre><hr>";
		$organization_user_department=array_column($this->common_model->get_all_conditional_data_sales_db('user_department',array('status'=>'Active'),'organization_chart','result_array','','','user_department'),'user_department');

		foreach($org_data as $key=>$single_org_data){
			$org_data[$key]['user_name']=$users[$single_org_data['user_id']];
			$org_data[$key]['reporting_manager']=$users[$single_org_data['reporting_manager']];
		}
		$data['organization_chart_data']=$org_data[0];
		$data['users']=$users;
		$data['organization_designation']=$organization_designation;
		$data['organization_user']=$organization_user_list;
		$data['organization_user'][1]=2;
		$data['organization_user_department']=$organization_user_department;
		$data['organization_user_reporting_level']=array(2,3,4);
	
		//echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'organization_chart'));
		$this->load->view('sidebar', array('title' => 'Edit organization chart'));
		$this->load->view('home/edit_organization', $data);
		$this->load->view('footer');
	}
	public function ajax_function() {

		if($this->input->is_ajax_request()) {
			$call_type = $this->input->post('call_type');
			$response['status'] = 'successful';	
			switch ($call_type) {

				case 'quotation_filter_data':
					
					$filter_type = $this->input->post('filter_date');
					// day filter
					$date = date('Y-m-d', strtotime('-1 days'));
					if( $filter_type == 'week') {
						$date = date('Y-m-d', strtotime('-7 days'));
					}
					if( $filter_type == 'month') {

						$date = date('Y-m-d', strtotime('-31 days'));
					} else if( $filter_type == 'all') {
						
						$date = '';
					}
					$data = $this->static_image_color();
					$data['quotation_list'] = $this->generate_quotation_details($date);
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
					$response['html_body'] = $this->load->view('home/quotation_list_div_body', $data, TRUE);
				break;
				case 'quotation_pending_for_follow_up_filter_data':
					
					$filter_type = $this->input->post('filter_date');
					// day filter
					$date = date('Y-m-d', strtotime('-1 days'));
					if( $filter_type == 'week') {
						$date = date('Y-m-d', strtotime('-7 days'));
					}if( $filter_type == 'month') {

						$date = date('Y-m-d', strtotime('-31 days'));
					} else if( $filter_type == 'all') {
						
						$date = '';
					}
					$data = $this->static_image_color();
					$data['follow_up_list'] = $this->generate_follow_up_details($date);
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
					$response['html_body'] = $this->load->view('home/quotation_pending_for_follow_up_div_body', $data, TRUE);
					break;	

				case 'revenue_highchart':

					$data['year_value'] = (!empty($this->input->post('filter_year')))?$this->input->post('filter_year'):'All';
					$revenue_call_type = ($data['year_value'] != 'All') ? 'month':'year';
					$data['years_list'] = $this->home_model->get_year_list();
					$data['select_year'] = 'select_year_for_revenue_highchart';
					$highchart_data = $this->prepare_revenue_highchart_data($revenue_call_type, $data['year_value']);
					foreach ($highchart_data as $year_or_month => $total) {

						$response['revenue_highchart'][] = array(
																'name' => $year_or_month,
																'y' => (int)$total['grand_total'],
																'drilldown' => $year_or_month
															);
					}
					$response['html_body']['filter_year'] = $this->load->view('home/filter_year', $data, TRUE);
					$response['html_body']['current_filter'] = $data['year_value'];

					// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
					break;	

				case 'compare_revenue_highchart':
					
					$response['compare_revenue_highchart'] = array();
					$default_value = array(0,0,0,0,0,0,0,0,0,0,0,0);
					$data['2021'] = $this->prepare_revenue_highchart_data('month', '2021');
					$data['2020'] = $this->prepare_revenue_highchart_data('month', '2020');
					$data['2019'] = $this->prepare_revenue_highchart_data('month', '2019');
					$data['2018'] = $this->prepare_revenue_highchart_data('month', '2018');
					$i=0;
					foreach ($data as $year_name => $year_details) {
						
						$response['compare_revenue_highchart'][$i]['name'] = (string)$year_name;
						$response['compare_revenue_highchart'][$i]['data'] = $default_value;
						$j = 0;
						foreach ($year_details as $year_details_value) {
							
							$response['compare_revenue_highchart'][$i]['data'][$j] = (int)$year_details_value['grand_total'];
							$j++;
						}
						$i++;
					}
					// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
				break;
				case 'quotation_vs_proforma_highchart':

					$year_value = (!empty($this->input->post('filter_year')))?$this->input->post('filter_year'):date('Y');
					$filter_sales_person = (!empty($this->input->post('sales_person'))) ? $this->input->post('sales_person'):'All';
					$years_list = $this->home_model->get_year_list();
					$quotation_list_details = $this->home_model->get_quotation_list($year_value, $filter_sales_person);
					// echo "<pre>";print_r($quotation_list_details);echo"</pre><hr>";
					$i=0;
					$response['quotation_vs_proforma_highchart_data'][0] = array();
					$response['quotation_vs_proforma_highchart_data'][0]['name'] = 'WON';
					$response['quotation_vs_proforma_highchart_data'][0]['color'] = '#28a745';
					$response['quotation_vs_proforma_highchart_data'][1] = array();
					$response['quotation_vs_proforma_highchart_data'][1]['name'] = 'OPEN';
					$response['quotation_vs_proforma_highchart_data'][1]['color'] = '#007bff';
					$response['quotation_vs_proforma_highchart_data'][2] = array();
					$response['quotation_vs_proforma_highchart_data'][2]['name'] = 'CLOSED';
					$response['quotation_vs_proforma_highchart_data'][2]['color'] = '#dc3545';
					foreach ($quotation_list_details as $key => $quotation_details) {						

						$won_percentage = 0;
						if(!empty($quotation_details['won']) || $quotation_details['won'] > 0){
							$won_percentage = number_format((($quotation_details['won'] / $quotation_details['count']) * 100),2);
						}
						$response['quotation_vs_proforma_highchart_category'][] = $quotation_details['month']."<br/>(".$won_percentage."%)";
						$response['quotation_vs_proforma_highchart_data'][0]['data'][] = (int)$quotation_details['won'];
						$response['quotation_vs_proforma_highchart_data'][1]['data'][] = (int)$quotation_details['open'];
						$response['quotation_vs_proforma_highchart_data'][2]['data'][] = (int)$quotation_details['closed'];
					}
					// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
					$response['html_body']['filter_year'] = $this->load->view('home/filter_year', array('years_list' => $years_list, 'year_value'=> $year_value, 'select_year' => 'select_year_for_quotation_vs_proforma'), TRUE);
					$response['html_body']['current_filter'] = $year_value;
					if($this->session->userdata('role') == '1'){

						$sales_person = array_column($this->common_model->get_sales_person('','all'),'name', 'user_id');
					} else if ($this->session->userdata('role') == '6') {

						$sales_person = array_column($this->common_model->get_sales_person('','sales_people'),'name', 'user_id');
					}
					$sales_person['All'] = 'All';
					$response['html_body']['filter_sales_person'] = $filter_sales_person;
					$response['html_body']['filter_sales_person_name'] = $sales_person[$filter_sales_person];
					$response['html_body']['filter_sales_person_html'] = $this->load->view('procurement/filter_sales_person',array(
																					'sales_person' => $sales_person,
																					'filter_sales_person' => $filter_sales_person,
																					'sales_filter_used_for_type' => 'bar_graph'
																				),
																				TRUE);
					// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
					break;

				case 'world_map_highchart':
						
					$country_details = $this->home_model->get_world_map_data();
					$response['world_map_highchart'] = array();
					foreach ($country_details as $country_details_key => $country_details_value) {
						
						$response['world_map_highchart'][$country_details_key]['hc-key'] = (string)$country_details_value['country_global_map_value'];
						$response['world_map_highchart'][$country_details_key]['value'] = (int)$country_details_value['total'];
					}
					// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
					break;	

				case 'desktrack_highchart':

					$filter_date = (!empty($this->input->post('filter_date')))?$this->input->post('filter_date'):'yesterday';
					// $filter_department = (!empty($this->input->post('filter_department')))?$this->input->post('filter_department'):5;
					$sales_person_list = array();
					$sales_person_list_keys = array();
					$i=0;
					foreach ($this->home_model->get_sales_user_name() as $sales_user_details) {
						
						$sales_person_list[$i] = $sales_user_details['name'];
						$sales_person_list_keys[$i] = $sales_user_details['user_id'];
						$static_data[$i]['name'] = $sales_user_details['name'];
						$static_data[$i]['y'] = 0;
						$static_data[$i]['drilldown'] = $sales_user_details['name'];
						$i++;
					}
					$where_array = array();
					$explode_filter_date = explode(' - ', $filter_date);
					if(count($explode_filter_date) == 1){

						$where_array['machine_time.date'] = date('Y-m-d', strtotime($explode_filter_date[0]));
					} else {

						if($explode_filter_date[0] != $explode_filter_date[1]) {

							$where_array['machine_time.date >='] = date('Y-m-d', strtotime($explode_filter_date[0]));
							$where_array['machine_time.date <='] = date('Y-m-d', strtotime($explode_filter_date[1]));
						}else{

							$where_array['machine_time.date'] = date('Y-m-d', strtotime($explode_filter_date[0]));
						}
					}
					$desktrack_highchart_data = $this->home_model->get_desktrack_highchart_data($where_array, $sales_person_list_keys);
					foreach ($desktrack_highchart_data as $desktrack_highchart_data_key => $desktrack_highchart_data_value) {
						$response['desktrack_highchart'][$desktrack_highchart_data_key]['name'] = $desktrack_highchart_data_value['fullname'];
						$response['desktrack_highchart'][$desktrack_highchart_data_key]['y'] = (int)$desktrack_highchart_data_value['total_hour'];
						$response['desktrack_highchart'][$desktrack_highchart_data_key]['drilldown'] = $desktrack_highchart_data_value['fullname'];
						unset($static_data[array_search($desktrack_highchart_data_value['user_id'], $sales_person_list_keys)]);
					}
					foreach ($static_data as $static_data_value) {

						$response['desktrack_highchart'][] = $static_data_value;
					}
					// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
					break;	

				case 'daily_report_highchart':
					
					$filter_date = (!empty($this->input->post('filter_date')))?$this->input->post('filter_date'):'yesterday';
					$daily_report_type = (!empty($this->input->post('filter_report_type')))?$this->input->post('filter_report_type'):'all';
					$daily_report_list = array('all'=>'All', 'ca'=>'Call Attempted', 'cc'=>'Call Closed', 'wa'=>'Whatsapp', 'email_sent'=>'Email Sent', 'email_received'=>'Email Received');
					$sales_person_list = array();
					$sales_person_list_keys = array();
					$static_series_data_for_highchart = array();
					foreach ($this->home_model->get_sales_user_name() as $sales_user_details) {
						
						$sales_person_list[] = $sales_user_details['name'];
						$sales_person_list_keys[] = $sales_user_details['user_id'];
						$static_series_data_for_highchart[] = 0;
					}
					$where_array = array();
					$explode_filter_date = explode(' - ', $filter_date);
					if(count($explode_filter_date) == 1){

						$where_array['daily_task_update.date'] = date('Y-m-d', strtotime($explode_filter_date[0]));
					} else {

						if($explode_filter_date[0] != $explode_filter_date[1]) {

							$where_array['daily_task_update.date >= '] = date('Y-m-d', strtotime($explode_filter_date[0]));
							$where_array['daily_task_update.date <='] = date('Y-m-d', strtotime($explode_filter_date[1]));
						}else{

							$where_array['daily_task_update.date'] = date('Y-m-d', strtotime($explode_filter_date[0]));
						}
					}
					// $where_array['users.user_id'] = 8;
					$daily_report_highchart_data = $this->home_model->get_daily_report_highchart_data($where_array, $sales_person_list_keys, $daily_report_type);
					// echo "<pre>";print_r($daily_report_highchart_data);echo"</pre><hr>";

					$response['daily_report_highchart']['category'] = $sales_person_list;
					// if(in_array($daily_report_type, array('all','ca'))){

					// 	$response['daily_report_highchart']['series'][] = array('name' => 'CA', 'data' => $static_series_data_for_highchart);
					// }
					if(in_array($daily_report_type, array('all','cc'))){

						$response['daily_report_highchart']['series'][] = array('name' => 'CC', 'data' => $static_series_data_for_highchart);
					}
					if(in_array($daily_report_type, array('all','wa'))){
					
						$response['daily_report_highchart']['series'][] = array('name' => 'WA', 'data' => $static_series_data_for_highchart);
					}
					if(in_array($daily_report_type, array('all','email_sent'))){

						$response['daily_report_highchart']['series'][] = array('name' => 'Email Sent', 'data' => $static_series_data_for_highchart);
					}
					if(in_array($daily_report_type, array('all','email_received'))){
					
						$response['daily_report_highchart']['series'][] = array('name' => 'Email Received', 'data' => $static_series_data_for_highchart);
					}
					// echo "<pre>";print_r($response);echo"</pre><hr>";
					foreach ($daily_report_highchart_data as $daily_report_highchart_data_value) {

						// if(in_array($daily_report_type, array('all','ca'))){
						// 	$key = 0;	
						// 	if($daily_report_type != 'all') {

						// 		$key = 0;
						// 	}
						// 	$response['daily_report_highchart']['series'][$key]['data'][array_search($daily_report_highchart_data_value['user_id'], $sales_person_list_keys)] = (int)$daily_report_highchart_data_value['ca'];
						// }
						if(in_array($daily_report_type, array('all','cc'))){
							$key = 0;	
							if($daily_report_type != 'all') {

								$key = 0;
							}
							$response['daily_report_highchart']['series'][$key]['data'][array_search($daily_report_highchart_data_value['user_id'], $sales_person_list_keys)] = (int)$daily_report_highchart_data_value['cc'];
						}
						if(in_array($daily_report_type, array('all','wa'))){
							$key = 1;	
							if($daily_report_type != 'all') {

								$key = 0;
							}
							$response['daily_report_highchart']['series'][$key]['data'][array_search($daily_report_highchart_data_value['user_id'], $sales_person_list_keys)] = (int)$daily_report_highchart_data_value['wa'];
						}
						if(in_array($daily_report_type, array('all','email_sent'))){
							$key = 2;	
							if($daily_report_type != 'all') {

								$key = 0;
							}
							$response['daily_report_highchart']['series'][$key]['data'][array_search($daily_report_highchart_data_value['user_id'], $sales_person_list_keys)] = (int)$daily_report_highchart_data_value['email_sent'];
						}
						if(in_array($daily_report_type, array('all','email_received'))){
							$key = 3;	
							if($daily_report_type != 'all') {

								$key = 0;
							}
							$response['daily_report_highchart']['series'][$key]['data'][array_search($daily_report_highchart_data_value['user_id'], $sales_person_list_keys)] = (int)$daily_report_highchart_data_value['email_received'];
						}
					}
					// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
					$response['html_body']['dropdown_body'] = $this->load->view('home/filter_daily_report_type', array('daily_report_type'=> $daily_report_type, 'daily_report_list'=> $daily_report_list),true);
					$response['html_body']['filter_report_type'] = $daily_report_list[$daily_report_type];
					$response['html_body']['filter_date'] = $filter_date;
					$response['html_body']['filter_report'] = $daily_report_type;
					// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
					break;

				case 'quotation_follow_up_highchart':
					
					$users = $this->home_model->getData('users', '(role = 5  OR role = 16)');
					// echo "<pre>";print_r($users);echo"</pre><hr>";exit;
					$user_name_array = array_column($users, 'name', 'user_id');
					$filter_type = $this->input->post('filter_date');
					// current time filter
					if(empty($filter_type) || $filter_type == 'current') {

						$where_array['followup_date'] = date('Y-m-d');

					} else if($filter_type == 'yesterday') {

						$where_array['followup_date'] = date('Y-m-d', strtotime('-1 days'));

					} else if( $filter_type == 'month') {

						$where_array['followup_date >='] = date('Y-m-1');
						$where_array['followup_date <='] = date('Y-m-31');

					} else if( $filter_type == 'week') {

						//check the current day
						if(date('D')!='Mon')
						{    
						 	//take the last monday
						  	$where_array['followup_date >='] = date('Y-m-d',strtotime('last Monday'));    

						}else{
						    $where_array['followup_date >='] = date('Y-m-d');   
						}

						//always next saturday
						if(date('D')!='Sat')
						{
						    $where_array['followup_date <='] = date('Y-m-d',strtotime('next Saturday'));
						}else{

						        $where_array['followup_date <='] = date('Y-m-d');
						}

					} else if( $filter_type == 'all') {
				        $where_array['followup_date <='] = date('Y-m-d');

					}
					// echo "<pre>";print_r($where_array);echo"</pre><hr>";exit;
					$data['follow_up_highchart_data'] = array();
					$follow_up_highchart_data = $this->home_model->get_follow_list_pending_data_for_highchart($where_array);
					// echo "<pre>";print_r($follow_up_highchart_data);echo"</pre><hr>";
					// echo $this->home_model->crm_db->last_query(),"<hr>";
					foreach ($follow_up_highchart_data as $highchart_data) {
						$data['follow_up_highchart_data'][$highchart_data['assigned_to']][] = $highchart_data;
					}
					$i = 0;
					foreach ($data['follow_up_highchart_data'] as $user_id => $highchart_data) {

						if(!empty($user_name_array[$user_id])) {

							$data[$i]['name'] = $user_name_array[$user_id];
							$data[$i]['y'] = (int)count($highchart_data);
							$data[$i]['drilldown'] = $user_name_array[$user_id];
							$i++;
						}
					}
					// echo "<pre>";print_r($data);echo"</pre><hr>";
					$sort_array_column = array_column($data, 'y');
					arsort($sort_array_column);
					foreach ($sort_array_column as $sort_array_column_key => $sort_array_column_value) {
						
						$response['highchart_data'][] = $data[$sort_array_column_key]; 						
					}
					// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
					break;

				case 'quotation_list_highchart':
		
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					$where_string = '';
					$where_string = "assigned_to != ''";
					$filter_date = explode(' - ', $this->input->post('filter_date'));
					if(count($filter_date) > 1) {

						$where_string .= " AND entered_on >= '".date('Y-m-d', strtotime($filter_date[0]))."' AND entered_on <= '".date('Y-m-d', strtotime($filter_date[1]))."'";
					}else {
						$where_string .= " AND entered_on = '".date('Y-m-d', strtotime($filter_date[0]))."'";
					}
					if(!in_array($this->session->userdata('role'), array(1, 16, 17))){

						$where_string .= " AND assigned_to = '".$this->session->userdata('user_id')."'";
					}
					// echo "<pre>";print_r($where_string);echo"</pre><hr>";exit;
					$all_user_details = array_column($this->common_model->get_dynamic_data_sales_db('*',array(), 'users'),'name', 'user_id');
					$quotation_count_details = $this->home_model->get_quotation_list_data($where_string);
					// echo "<pre>";print_r($quotation_count_details);echo"</pre><hr>";exit;
					$response['highchart_data'] = array();
					foreach($quotation_count_details as $quotation_key => $single_quotation_count_details) {

						$response['highchart_data']['category'][$quotation_key] = $all_user_details[$single_quotation_count_details['sales_person_id']];
						$response['highchart_data']['won'][$quotation_key] = (int)$single_quotation_count_details['won'];
						$response['highchart_data']['open'][$quotation_key] = (int)$single_quotation_count_details['open'];
						$response['highchart_data']['closed'][$quotation_key] = (int)$single_quotation_count_details['closed'];
					}
					// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
				break;
				case 'delete':
					
					$response['message'] = $this->input->post('type')." Deleted Successfully !!!";
					$id = $this->input->post('id');
					if(!empty($id)){

						$this->reset_field($this->input->post('type'), $id);
						switch ($this->input->post('type')) {
							case 'Product':
															
								$this->home_model->updateData('product_mst', array('status'=> 'Inactive'), array('id'=>$id));
							break;

							case 'Material':
								
								$this->home_model->updateData('material_mst', array('status'=> 'Inactive'), array('id'=>$id));
							break;

							case 'Delivery Place':
							
								$this->home_model->updateData('delivery', array('status'=> 'Inactive'), array('delivery_id'=>$id));
							break;

							case 'Delivery Time':
							
								$this->home_model->updateData('delivery_time', array('status'=> 'Inactive'), array('dt_id'=>$id));
							break;

							case 'Payments':
								
								$this->home_model->updateData('payment_terms', array('status'=> 'Inactive'), array('term_id'=>$id));
							break;

							case 'Validity':
							
								$this->home_model->updateData('validity', array('status'=> 'Inactive'), array('validity_id'=>$id));
							break;

							case 'Currency':
							
								$this->home_model->updateData('currency', array('status'=> 'Inactive'), array('currency_id'=>$id));
							break;

							case 'Country Origin':
							
								$this->home_model->updateData('origin_country', array('status'=> 'Inactive'), array('country_id'=>$id));
							break;

							case 'MTC Type':
							
								$this->home_model->updateData('mtc_type', array('status'=> 'Inactive'), array('mtc_id'=>$id));
							break;

							case 'Transport Mode':
							
								$this->home_model->updateData('transport_mode', array('status'=> 'Inactive'), array('mode_id'=>$id));
							break;

							case 'Lead Stage Reasons':
							
								$this->home_model->updateData('lead_stage_reasons', array('status'=> 'Inactive'), array('lead_reason_id'=>$id));
							break;

							case 'Quotation Close Reasons':
							
								$this->home_model->updateData('close_reason', array('status'=> 'Inactive'), array('reason_id '=>$id));
							break;

							case 'RFQ Close Reasons':

								$this->home_model->updateData('rfq_close_reason', array('status'=> 'Inactive'), array('id'=>$id));
							break;

							default:
								
								$response['message'] = $this->input->post('type')." is not deleted";
							break;
						}
					} else {

						$response['message'] = $this->input->post('type')." is not deleted";
					}
				break;
				case 'add':
					
					$response['message'] = $this->input->post('type')." is Added Successfully !!!";
					$form_data = array_column($this->input->post('add_form'), 'value', 'name');
						//echo "<pre>";print_r($response['message']);echo"</pre><hr>";exit;
					if(!empty($form_data)){

						$table_name = '';
						$insert_array = array('status' => 'Active');
						switch ($this->input->post('type')) {
							case 'Product':
								
								$table_name = 'product_mst';
								$insert_array['name'] = $form_data['name'];
							break;

							case 'Material':
								
								$table_name = 'material_mst';
								$insert_array['name'] = $form_data['name'];
							break;

							case 'Delivery Place':
								
								$table_name = 'delivery';
								$insert_array['delivery_name'] = $form_data['delivery_name'];
								$insert_array['transport_id'] = $form_data['transport_id'];
								// echo "<pre>";print_r($insert_array);echo"</pre><hr>";exit;
							break;	

							case 'Delivery Time':
								
								$table_name = 'delivery_time';
								$insert_array['dt_value'] = $form_data['dt_value'];
							break;

							case 'Payments':
							
								$table_name = 'payment_terms';
								$insert_array['term_value'] = $form_data['term_value'];
							break;	

							case 'Validity':
							
								$table_name = 'validity';
								$insert_array['validity_value'] = $form_data['validity_value'];
							break;

							case 'Currency':
							
								$table_name = 'currency';
								$insert_array['currency'] = $form_data['currency'];
								$insert_array['currency_rate'] = $form_data['currency_rate'];
								$insert_array['currency_icon'] = '';
							break;

							case 'Country Origin':
							
								$table_name = 'origin_country';
								$insert_array['country'] = $form_data['country'];
							break;

							case 'MTC Type':
							
								$table_name = 'mtc_type';
								$insert_array['mtc_value'] = $form_data['mtc_value'];
							break;

							case 'Transport Mode':
								
								$table_name = 'transport_mode';
								$insert_array['mode'] = $form_data['mode'];
							break;

							case 'Lead Stage Reasons':
								
								$table_name = 'lead_stage_reasons';
								$insert_array['reason'] = $form_data['reason'];
								 //echo "<pre>";print_r($insert_array);echo"</pre><hr>";exit;
							break;
							case 'Quotation Close Reasons':
								
								$table_name = 'close_reason';
								$insert_array['reason_text'] = $form_data['reason'];
							break;	

							case 'RFQ Close Reasons':

								$table_name = 'rfq_close_reason';
								$insert_array['name'] = $form_data['reason'];
							break;

							default:
								
							break;
						}
						if(!empty($table_name)) {
								//echo"<pre>";print_r($table_name);echo"</pre><hr>";exit;

							$this->home_model->insertData($table_name, $insert_array);
						}
					} else {

						$response['message'] = $this->input->post('type')." is not added";
					}
					break;			

				case 'get_management_tab_details':
					
					$tab_name = $this->input->post('tab_name');
					if(!empty($tab_name)) {

						$data = $this->prepare_all_view_details($tab_name);
						// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
						$response['html_body'] = $this->load->view('home/dynamic_manage_table', $data, true);
					}
					break;

				case 'update_footer__procurement':
					
					$terms_condition = $this->input->post('terms_condition');
					$this->home_model->updateData('pdf_footer',array('footer_text' => $terms_condition), array('status' => 'Active', 'pdf_type'=> 'procurement'));
				break;

				// case 'sales_lead_count_highchart':

					// 	// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					// 	$lead_stage = (!empty($this->input->post('lead_stage'))) ? $this->input->post('lead_stage'):'';
					// 	$lead_type = (!empty($this->input->post('lead_type'))) ? $this->input->post('lead_type'):'';
					// 	$lead_source = (!empty($this->input->post('lead_source'))) ? $this->input->post('lead_source'):'';
					// 	$lead_country = (!empty($this->input->post('lead_country'))) ? $this->input->post('lead_country'):'';
					// 	$lead_region = (!empty($this->input->post('lead_region'))) ? $this->input->post('lead_region'):'';
					// 	$all_user_details = array_column($this->common_model->get_dynamic_data_sales_db('*',array(), 'users'),'name', 'user_id');
					// 	$all_user_details[0] = 'Not Assigned';
					// 	$sales_person_details = array_column($this->common_model->get_sales_person(),'name', 'user_id');
					// 	$sales_person_details[0] = 'Blank Lead';
					// 	$clients_where[0] = 'clients.deleted is NULL';
					// 	$leads_where[0] = 'lead_mst.deleted is NULL';
					// 	// echo "<pre>";print_r($all_user_details);echo"</pre><hr>";
					// 	if(!empty($lead_stage)) {

					// 		if(count($lead_stage) == 1){

					// 			$clients_where[] = 'clients.lead_stage = "'.implode(' ', $lead_stage).'"';
					// 			$leads_where[] = 'lead_mst.lead_stage = "'.implode(' ', $lead_stage).'"';
					// 		} else {
					// 			$clients_where[] = 'clients.lead_stage in ("'.implode('","', $lead_stage).'")';
					// 			$leads_where[] = 'lead_mst.lead_stage in ("'.implode('","', $lead_stage).'")';
					// 		}
					// 	}
					// 	if(!empty($lead_type)) {

					// 		if(count($lead_type) == 1){

					// 			$clients_where[] = 'clients.lead_type = "'.implode(' ', $lead_type).'"';
					// 			$leads_where[] = 'lead_mst.lead_type = "'.implode(' ', $lead_type).'"';
					// 		} else {
					// 			$clients_where[] = 'clients.lead_type in ("'.implode('","', $lead_type).'")';
					// 			$leads_where[] = 'lead_mst.lead_type in ("'.implode('","', $lead_type).'")';
					// 		}
					// 	}
					// 	if(!empty($lead_source)) {

					// 		if(count($lead_source) == 1){

					// 			$explode_lead_name = explode('/', implode(' ', $lead_source));
					// 			if(count($explode_lead_name) == 3) {
					// 				$clients_where[] = 'clients.source = "'.$explode_lead_name[2].'"';
					// 				$leads_where[] = 'lead_mst.data_category = "'.$explode_lead_name[2].'"';
					// 			}
					// 		} else {
					// 			$all_lead_source_name = array();
					// 			foreach($lead_source as $single_lead_name) {

					// 				$explode_lead_name = explode('/', $single_lead_name);
					// 				if(count($explode_lead_name) == 3) {
					// 					$all_lead_source_name[] = $explode_lead_name[2];
					// 				}
					// 			}
					// 			// echo "<pre>";print_r($all_lead_source_name);echo"</pre><hr>";exit;
					// 			$clients_where[] = 'clients.source in ("'.implode('","', $all_lead_source_name).'")';
					// 			$leads_where[] = 'lead_mst.data_category in ("'.implode('","', $all_lead_source_name).'")';
					// 		}
					// 	}
					// 	if(!empty($lead_country)) {

					// 		if(count($lead_country) == 1){

					// 			$clients_where[] = 'clients.country = "'.implode(' ', $lead_country).'"';
					// 			$leads_where[] = 'lead_mst.country = "'.implode(' ', $lead_country).'"';
					// 		} else {
					// 			$clients_where[] = 'clients.country in ("'.implode('","', $lead_country).'")';
					// 			$leads_where[] = 'lead_mst.country in ("'.implode('","', $lead_country).'")';
					// 		}
					// 	}
					// 	if(!empty($lead_region)) {

					// 		if(count($lead_region) == 1){

					// 			$clients_where[] = 'clients.region = "'.implode(' ', $lead_region).'"';
					// 			$leads_where[] = 'lead_mst.region = "'.implode(' ', $lead_region).'"';
					// 		} else {
					// 			$clients_where[] = 'clients.region in ("'.implode('","', $lead_region).'")';
					// 			$leads_where[] = 'lead_mst.region in ("'.implode('","', $lead_region).'")';
					// 		}
					// 	}
					// 	$lead_assign_data = $this->home_model->get_sales_person_lead_assign_data($clients_where, $leads_where);
					// 	// echo "<pre>";print_r($lead_assign_data);echo"</pre><hr>";
					// 	$all_lead_data = array();
					// 	foreach($lead_assign_data['lead_mst_leads'] as $single_lead_data) {

					// 		// if(!empty())
					// 		$all_lead_data[$single_lead_data['sales_person_id']]['count'] = $single_lead_data['count'];
					// 		$all_lead_data[$single_lead_data['sales_person_id']]['stage_6'] = $single_lead_data['stage_6'];
					// 		$all_lead_data[$single_lead_data['sales_person_id']]['stage_5'] = $single_lead_data['stage_5'];
					// 		$all_lead_data[$single_lead_data['sales_person_id']]['stage_4'] = $single_lead_data['stage_4'];
					// 		$all_lead_data[$single_lead_data['sales_person_id']]['stage_3'] = $single_lead_data['stage_3'];
					// 		$all_lead_data[$single_lead_data['sales_person_id']]['stage_2'] = $single_lead_data['stage_2'];
					// 		$all_lead_data[$single_lead_data['sales_person_id']]['stage_1'] = $single_lead_data['stage_1'];
					// 		$all_lead_data[$single_lead_data['sales_person_id']]['stage_0'] = $single_lead_data['stage_0'];
					// 		$all_lead_data[$single_lead_data['sales_person_id']]['blank_stage'] = $single_lead_data['blank_stage'];
					// 	}
					// 	foreach($lead_assign_data['client_leads'] as $single_lead_data) {

					// 		$all_lead_data[$single_lead_data['sales_person_id']]['count'] += $single_lead_data['count'];
					// 		$all_lead_data[$single_lead_data['sales_person_id']]['stage_6'] += $single_lead_data['stage_6'];
					// 		$all_lead_data[$single_lead_data['sales_person_id']]['stage_5'] += $single_lead_data['stage_5'];
					// 		$all_lead_data[$single_lead_data['sales_person_id']]['stage_4'] += $single_lead_data['stage_4'];
					// 		$all_lead_data[$single_lead_data['sales_person_id']]['stage_3'] += $single_lead_data['stage_3'];
					// 		$all_lead_data[$single_lead_data['sales_person_id']]['stage_2'] += $single_lead_data['stage_2'];
					// 		$all_lead_data[$single_lead_data['sales_person_id']]['stage_1'] += $single_lead_data['stage_1'];
					// 		$all_lead_data[$single_lead_data['sales_person_id']]['stage_0'] += $single_lead_data['stage_0'];
					// 		$all_lead_data[$single_lead_data['sales_person_id']]['blank_stage'] += $single_lead_data['blank_stage'];
					// 	}
					// 	// echo "<pre>";print_r($all_lead_data);echo"</pre><hr>";die('debug');
					// 	// arsort($all_lead_data);
					// 	$i=0;
					// 	foreach($all_lead_data as $sales_person_id => $lead_details) {

					// 		if(!empty($sales_person_id)) {
					// 			$response['sales_lead_count_highchart']['category'][$i] = $all_user_details[$sales_person_id];
					// 			$response['sales_lead_count_highchart']['stage_6'][$i] = (int)$lead_details['stage_6'];
					// 			$response['sales_lead_count_highchart']['stage_5'][$i] = (int)$lead_details['stage_5'];
					// 			$response['sales_lead_count_highchart']['stage_4'][$i] = (int)$lead_details['stage_4'];
					// 			$response['sales_lead_count_highchart']['stage_3'][$i] = (int)$lead_details['stage_3'];
					// 			$response['sales_lead_count_highchart']['stage_2'][$i] = (int)$lead_details['stage_2'];
					// 			$response['sales_lead_count_highchart']['stage_1'][$i] = (int)$lead_details['stage_1'];
					// 			$response['sales_lead_count_highchart']['stage_0'][$i] = (int)$lead_details['stage_0'];
					// 			$response['sales_lead_count_highchart']['blank_stage'][$i] = (int)$lead_details['blank_stage'];
					// 			$i++;
					// 		}
					// 	}

					// 	// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
					// 	$response['html_body']['filter_sales_person'] = $this->load->view(
					// 																'quotations/filter_sales_person',
					// 																array(
					// 																	'sales_person' => $sales_person_details,
					// 																	'filter_sales_person' => 'All'
					// 																),
					// 																TRUE);
				// break;
				case 'sales_lead_count_highchart':

					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					$lead_stage = (!empty($this->input->post('lead_stage'))) ? $this->input->post('lead_stage'):'';
					$lead_type = (!empty($this->input->post('lead_type'))) ? $this->input->post('lead_type'):'';
					$lead_source = (!empty($this->input->post('lead_source'))) ? $this->input->post('lead_source'):'';
					$lead_country = (!empty($this->input->post('lead_country'))) ? $this->input->post('lead_country'):'';
					$lead_region = (!empty($this->input->post('lead_region'))) ? $this->input->post('lead_region'):'';
					$all_user_details = array_column($this->common_model->get_dynamic_data_sales_db('*',array(), 'users'),'name', 'user_id');
					$all_user_details[0] = 'Not Assigned';
					$sales_person_details = array_column($this->common_model->get_sales_person(),'name', 'user_id');
					$sales_person_details[0] = 'Blank Lead';
					$clients_where[0] = 'customer_mst.deleted is NULL';
					// echo "<pre>";print_r($all_user_details);echo"</pre><hr>";
					if(!empty($lead_stage)) {

						if(count($lead_stage) == 1){

							$clients_where[] = 'customer_mst.lead_stage = "'.implode(' ', $lead_stage).'"';
						} else {
							$clients_where[] = 'customer_mst.lead_stage in ("'.implode('","', $lead_stage).'")';
						}
					}
					if(!empty($lead_type)) {

						if(count($lead_type) == 1){

							$clients_where[] = 'customer_mst.lead_type = "'.implode(' ', $lead_type).'"';
							
						} else {
							$clients_where[] = 'customer_mst.lead_type in ("'.implode('","', $lead_type).'")';
							
						}
					}
					if(!empty($lead_country)) {

						if(count($lead_country) == 1){

							$clients_where[] = 'customer_mst.country_id = "'.implode(' ', $lead_country).'"';
						} else {
							$clients_where[] = 'customer_mst.country_id in ("'.implode('","', $lead_country).'")';
							
						}
					}
					if(!empty($lead_region)) {

						if(count($lead_region) == 1){

							$clients_where[] = 'customer_mst.region_id = "'.implode(' ', $lead_region).'"';
						
						} else {
							$clients_where[] = 'customer_mst.region_id in ("'.implode('","', $lead_region).'")';
							
						}
					}
					$lead_assign_data = $this->home_model->get_sales_person_lead_assign_data($clients_where);
					// echo "<pre>";print_r($lead_assign_data);echo"</pre><hr>";exit;


					$all_lead_data = array();
					foreach($lead_assign_data['client_leads'] as $single_lead_data) {

						$all_lead_data[$single_lead_data['sales_person_id']]['count'] += $single_lead_data['count'];
						$all_lead_data[$single_lead_data['sales_person_id']]['stage_6'] += $single_lead_data['stage_6'];
						$all_lead_data[$single_lead_data['sales_person_id']]['stage_5'] += $single_lead_data['stage_5'];
						$all_lead_data[$single_lead_data['sales_person_id']]['stage_4'] += $single_lead_data['stage_4'];
						$all_lead_data[$single_lead_data['sales_person_id']]['stage_3'] += $single_lead_data['stage_3'];
						$all_lead_data[$single_lead_data['sales_person_id']]['stage_2'] += $single_lead_data['stage_2'];
						$all_lead_data[$single_lead_data['sales_person_id']]['stage_1'] += $single_lead_data['stage_1'];
						$all_lead_data[$single_lead_data['sales_person_id']]['stage_0'] += $single_lead_data['stage_0'];
						$all_lead_data[$single_lead_data['sales_person_id']]['blank_stage'] += $single_lead_data['blank_stage'];
					}
					// echo "<pre>";print_r($all_lead_data);echo"</pre><hr>";die('debug');
					// arsort($all_lead_data);
					$i=0;
					foreach($all_lead_data as $sales_person_id => $lead_details) {

						if(!empty($sales_person_id)) {
							$response['sales_lead_count_highchart']['category'][$i] = $all_user_details[$sales_person_id];
							$response['sales_lead_count_highchart']['stage_6'][$i] = (int)$lead_details['stage_6'];
							$response['sales_lead_count_highchart']['stage_5'][$i] = (int)$lead_details['stage_5'];
							$response['sales_lead_count_highchart']['stage_4'][$i] = (int)$lead_details['stage_4'];
							$response['sales_lead_count_highchart']['stage_3'][$i] = (int)$lead_details['stage_3'];
							$response['sales_lead_count_highchart']['stage_2'][$i] = (int)$lead_details['stage_2'];
							$response['sales_lead_count_highchart']['stage_1'][$i] = (int)$lead_details['stage_1'];
							$response['sales_lead_count_highchart']['stage_0'][$i] = (int)$lead_details['stage_0'];
							$response['sales_lead_count_highchart']['blank_stage'][$i] = (int)$lead_details['blank_stage'];
							$i++;
						}
					}

					// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
					$response['html_body']['filter_sales_person'] = $this->load->view(
																				'quotations/filter_sales_person',
																				array(
																					'sales_person' => $sales_person_details,
																					'filter_sales_person' => 'All'
																				),
																				TRUE);
				break;
				case 'quotation_proforma_highchart':
					
					$where_string = '';
					$where_string = "assigned_to != '' AND status = 'won'";
					$filter_date = explode(' - ', $this->input->post('filter_date'));
					$where_string .= " AND confirmed_on >= '".date('Y-m-d 00:00:00', strtotime($filter_date[0]))."'";
					if(count($filter_date) > 1) {

						$where_string .= " AND confirmed_on <= '".date('Y-m-d 23:59:59', strtotime($filter_date[1]))."'";
					}else {

						$where_string .= " AND confirmed_on <= '".date('Y-m-d 23:59:59', strtotime($filter_date[0]))."'";
					}
					$all_user_details = array_column($this->common_model->get_dynamic_data_sales_db('*',array(), 'users'),'name', 'user_id');
					$currency_details = array_column($this->common_model->get_dynamic_data_sales_db('*',array(), 'currency'),'currency_rate', 'currency_id');
					$quotation_proforma_details = $this->home_model->get_quotation_proforma_highchart_data($where_string);
					// echo "<pre>";print_r($quotation_proforma_details);echo"</pre><hr>";die;
					$sales_person_array = array();
					$response['total_revenu'] = 0;
					$currency_details =  array_column($this->common_model->get_dynamic_data_sales_db('currency_id, currency_rate', array('status'=>'Active'), 'currency'), 'currency_rate', 'currency_id');
					$currency_value = $currency_details[1];
					foreach($quotation_proforma_details as $proforma_details) {
						if(empty($sales_person_array[$proforma_details['assigned_to']])) {

							$sales_person_array[$proforma_details['assigned_to']] = round((((int)$proforma_details['sales_amount']) * ($currency_details[$proforma_details['currency']] / $currency_value)));

						}else {

							$sales_person_array[$proforma_details['assigned_to']] += round((((int)$proforma_details['sales_amount']) * ($currency_details[$proforma_details['currency']] / $currency_value)));
						}
					}
					arsort($sales_person_array);
					foreach ($sales_person_array as $sales_person_id => $sales_amount) {

						$response['quotation_proforma_highchart'][] = array(
																		'name' => $all_user_details[$sales_person_id],
																		'y' => (int)$sales_amount,
																		'drilldown' => $all_user_details[$sales_person_id]);
						$total_revenu += (int)$sales_amount;
					}
					$response['total_revenu'] = number_format($total_revenu);
					// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
				break;
				case 'generate_other_person_login':
						
					$response['url'] = '';
					if(!empty($this->input->post('user_name')) && !empty($this->input->post('password'))){
						$data = $this->home_model->get_dynamic_data_sales_db('*', array('username'=> $this->input->post('user_name'), 'password'=> $this->input->post('password')), 'users', 'row_array');
						$this->session->set_userdata($this->login_model->get_user_details($data));
						$response['url'] = 'home/dashboard';
					}
				break;
				case 'reset_user_login':
					
					$user_details = $this->session->userdata('global_user_details');	
					$data = $this->common_model->get_dynamic_data_sales_db(
										'*',
										array(
											'username'=> $user_details['user_name'],
											'password'=> $user_details['password']
										), 
										'users',
										'row_array'
									);
					$this->session->set_userdata(
						$this->login_model->get_user_details(
							$data
						)
					);
					$response['url'] = 'home/dashboard';
				break;
				case 'save_org_data':

					$form_data=array_column($this->input->post('form_data'),'value','name');
					if(empty($this->input->post('id'))){
				        if(!empty($form_data['next_to'])){
			               $organization_data = $this->home_model->get_all_data('*', array('user_id'=>$form_data['next_to']), 'organization_chart', 'row_array');
				            //echo "<pre>";print_r($organization_data);echo"</pre><hr>";exit;
							//get order of all higher in this series
							$organization_data_order= $this->home_model->get_all_data('id,user_id,user_reporting_level,user_reporting_level_'.$organization_data['user_reporting_level'].'_order', array('user_reporting_level_'.$organization_data['user_reporting_level'].'_order >'=>$organization_data['user_reporting_level_'.$organization_data['user_reporting_level'].'_order']), 'organization_chart');
							$update_data=array(
											'user_id'=>$form_data['user_id'],
												'reporting_manager' => $form_data['reporting_manager'],
												'user_reporting_level'=> $form_data['user_reporting_level'],
												'user_reporting_level_'.$form_data['user_reporting_level'].'_order' =>$organization_data['user_reporting_level_'.$organization_data['user_reporting_level'].'_order']+1,
												'user_designation'=>$form_data['user_designation'],
												'user_department'=>$form_data['user_department']
										);
							//echo "<pre>";print_r($update_data);echo"</pre><hr>";exit();
							$this->home_model->insertData(
									'organization_chart', 
									$update_data
								);
							
						 	foreach($organization_data_order as $single_data){
							 	$update_datas['user_reporting_level_'.$single_data['user_reporting_level'].'_order']=$single_data['user_reporting_level_'.$single_data['user_reporting_level'].'_order']+1;
								// echo "<pre>";print_r($update_datas);echo"</pre><hr>";
							 	$this->home_model->update_data_sales_db(
										'organization_chart', 
										$update_datas,
										array('id'=>$single_data['id'])
									);
						 	}
						}
					}else if(!empty($this->input->post('id'))){

						//getting default info of user in org chart
						$organization_chart_data = $this->home_model->get_all_data('*', array('id'=>$this->input->post('id')), 'organization_chart', 'row_array');
						$organization_chart_greater_then_order_data= $this->home_model->get_all_data('id,user_id,user_reporting_level,user_reporting_level_'.$organization_chart_data['user_reporting_level'].'_order', array('user_reporting_level_'.$organization_chart_data['user_reporting_level'].'_order >'=>$organization_chart_data['user_reporting_level_'.$organization_chart_data['user_reporting_level'].'_order']), 'organization_chart');
						foreach($organization_chart_greater_then_order_data as $single_data){
						 	$update_dataa['user_reporting_level_'.$single_data['user_reporting_level'].'_order']=$single_data['user_reporting_level_'.$single_data['user_reporting_level'].'_order']-1;
						 	$this->home_model->update_data_sales_db(
											'organization_chart', 
											$update_dataa,
											array('id'=>$single_data['id'])
										);
				 		}

						$this->home_model->update_data_sales_db( 
											'organization_chart', 
											array('user_reporting_level_'.$organization_chart_data['user_reporting_level'].'_order'=>0),
											array('id'=>$organization_chart_data['id'])
										);

	           			//echo "<pre>";print_r($form_data);echo"</pre><hr>";
					 	if($organization_chart_data['user_reporting_level'] != $form_data['user_reporting_level'] && $organization_chart_data['reporting_manager'] != $form_data['reporting_manager']) {
							if(!empty($form_data['next_to'])) {

								$organization_data = $this->home_model->get_all_data('*', array('user_id'=>$form_data['next_to']), 'organization_chart', 'row_array');
							 	//echo "<pre>";print_r($organization_data);echo"</pre><hr>";exit;
								//get order of all higher in this series
								$organization_data_order= $this->home_model->get_all_data('id,user_id,user_reporting_level,user_reporting_level_'.$organization_data['user_reporting_level'].'_order', array('user_reporting_level_'.$organization_data['user_reporting_level'].'_order >'=>$organization_data['user_reporting_level_'.$organization_data['user_reporting_level'].'_order']), 'organization_chart');
								$update_data=array(
											'reporting_manager' => $form_data['reporting_manager'],
											'user_reporting_level'=> $form_data['user_reporting_level'],
											'user_reporting_level_'.$form_data['user_reporting_level'].'_order' =>$organization_data['user_reporting_level_'.$organization_data['user_reporting_level'].'_order']+1
										);
								//echo "<pre>";print_r($update_data);echo"</pre><hr>";
								$this->home_model->update_data_sales_db(
										'organization_chart', 
										$update_data,
										array('id'=>$this->input->post('id'))
								);
							 	foreach($organization_data_order as $single_data){
								 	$update_datas['user_reporting_level_'.$single_data['user_reporting_level'].'_order']=$single_data['user_reporting_level_'.$single_data['user_reporting_level'].'_order']+1;
									//echo "<pre>";print_r($update_datas);echo"</pre><hr>";die;
								 	$this->home_model->update_data_sales_db(
											'organization_chart', 
											$update_datas,
											array('id'=>$single_data['id'])
										);
							 	}
							}
						}else if($organization_chart_data['reporting_manager'] != $form_data['reporting_manager']) {
							if(!empty($form_data['next_to']))
							{

								$organization_data = $this->home_model->get_all_data('*', array('user_id'=>$form_data['next_to']), 'organization_chart', 'row_array');
							 	//echo "<pre>";print_r($organization_data);echo"</pre><hr>";exit;
								//get order of all higher in this series
								$organization_data_order= $this->home_model->get_all_data('id,user_id,user_reporting_level,user_reporting_level_'.$organization_data['user_reporting_level'].'_order', array('user_reporting_level_'.$organization_data['user_reporting_level'].'_order >'=>$organization_data['user_reporting_level_'.$organization_data['user_reporting_level'].'_order']), 'organization_chart');
								$update_data=array(
											'reporting_manager' => $form_data['reporting_manager'],
											'user_reporting_level_'.$form_data['user_reporting_level'].'_order' =>$organization_data['user_reporting_level_'.$organization_data['user_reporting_level'].'_order']+1
										);
								//echo "<pre>";print_r($update_data);echo"</pre><hr>";
								$this->home_model->update_data_sales_db(
										'organization_chart', 
										$update_data,
										array('id'=>$this->input->post('id'))
								);
							 	foreach($organization_data_order as $single_data){
								 	$update_datas['user_reporting_level_'.$single_data['user_reporting_level'].'_order']=$single_data['user_reporting_level_'.$single_data['user_reporting_level'].'_order']+1;
									//echo "<pre>";print_r($update_datas);echo"</pre><hr>";die;
								 	$this->home_model->update_data_sales_db(
											'organization_chart', 
											$update_datas,
											array('id'=>$single_data['id'])
										);
							 	}
			                }	        
							else if(empty($form_data['next_to'])) {
								$form_data['next_to']=0;
								$organization_data_order= $this->home_model->get_all_data('id,user_id,user_reporting_level,user_reporting_level_'.$organization_chart_data['user_reporting_level'].'_order', array('user_reporting_level_'.$organization_chart_data['user_reporting_level'].'_order >'=>0),'organization_chart');
									foreach($organization_data_order as $single_data){
								 	$update_datas['user_reporting_level_'.$single_data['user_reporting_level'].'_order']=$single_data['user_reporting_level_'.$single_data['user_reporting_level'].'_order']+1;
									//echo "<pre>";print_r($update_datas);echo"</pre><hr>";die;
								 	$this->home_model->update_data_sales_db(
											'organization_chart', 
											$update_datas,
											array('id'=>$single_data['id'])
										);
							 	}
								$this->home_model->update_data_sales_db(
										'organization_chart', 
										array('reporting_manager' => $form_data['reporting_manager'],
											'user_reporting_level_'.$organization_chart_data['user_reporting_level'].'_order '=>1),
										array('id'=>$this->input->post('id'))
								);

							}
		                }
					}	
				break;
				case 'get_notice_body_data':
					
					if(!empty($this->input->post('notice_type'))) {

						$data['notice_details'] = $this->create_notice_details($this->input->post('notice_type'));
						// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
						$response['notice_html_body'] = $this->load->view('home/notice_body', $data, true);
					}

				break;
				case 'get_image':
					
					if(!empty($this->input->post('id'))) {

						$notice_details = $this->common_model->get_dynamic_data_sales_db('notice_img',array('id'=> $this->input->post('id')), 'user_notice','row_array');
						// echo "<pre>";print_r($notice_details);echo"</pre><hr>";exit;
						$response['notice_img'] = base_url($notice_details['notice_img']);
					}

				break;
				case 'highchart_rfq_status':

					$response['procurement_person_id'] = $procurement_person_id = $this->input->post('procurement_person_id');
					$rfq_where_string = $quotation_where_string = '';
					$rfq_where_string .= "year(rfq_date) = '".date('Y')."'";
					$quotation_where_string .= "year(quotation_mst.confirmed_on) = '".date('Y')."'";
					if($procurement_person_id != "All") {

						$rfq_where_string .= " AND (rfq_mst.assigned_to =".$procurement_person_id." OR rfq_mst.assigned_to_1 =".$procurement_person_id." OR rfq_mst.assigned_to_2 =".$procurement_person_id." OR rfq_mst.assigned_to_3 =".$procurement_person_id.")";
						$quotation_where_string .= " AND (rfq_mst.assigned_to =".$procurement_person_id." OR rfq_mst.assigned_to_1 =".$procurement_person_id." OR rfq_mst.assigned_to_2 =".$procurement_person_id." OR rfq_mst.assigned_to_3 =".$procurement_person_id.")";
					}
					$rfq_status_details = $this->home_model->rfq_status_highchart_data($rfq_where_string, $quotation_where_string);
					// echo "<pre>";print_r($rfq_status_details);echo"</pre><hr>";exit;
					$response['highchart_data']['series'] = array(
													array('name'=> 'RFQ-Waiting', 'color'=> '#fd7e14', 'data'=> array()),
													array('name'=> 'RFQ-Pending', 'color'=> '#5578eb', 'data'=> array()),
													array('name'=> 'RFQ-Query', 'color'=> '#343a40', 'data'=> array()),
													array('name'=> 'RFQ-Regret', 'color'=> '#ff0000', 'data'=> array()),
													array('name'=> 'RFQ-Done', 'color'=> '#17a2b8', 'data'=> array()),
													array('name'=> 'Quotation-Won', 'color'=> '#0abb87', 'data'=> array())
												);
					$response['highchart_data']['category'] = array();
					$i = 0;
					$current_key = 0;
					foreach ($rfq_status_details as $single_rfq_status_details) {
						
						if(!in_array($single_rfq_status_details['week'], $response['highchart_data']['category'])){

							$current_key = $i;
							$response['highchart_data']['category'][$current_key] = $single_rfq_status_details['week'];
							$response['highchart_data']['series'][0]['data'][$current_key] = 0;
							$response['highchart_data']['series'][1]['data'][$current_key] = 0;
							$response['highchart_data']['series'][2]['data'][$current_key] = 0;
							$response['highchart_data']['series'][3]['data'][$current_key] = 0;
							$response['highchart_data']['series'][4]['data'][$current_key] = 0;
							$response['highchart_data']['series'][5]['data'][$current_key] = 0;
							$i++;
						}else{
							$current_key = array_search($single_rfq_status_details['week'],$response['highchart_data']['category']);
						}
						if($single_rfq_status_details['status'] == 'waiting'){

							$response['highchart_data']['series'][0]['data'][$current_key] += 1;
						}else if($single_rfq_status_details['status'] == 'pending'){

							$response['highchart_data']['series'][1]['data'][$current_key] += 1;
						}else if($single_rfq_status_details['status'] == 'query'){

							$response['highchart_data']['series'][2]['data'][$current_key] += 1;
						}else if($single_rfq_status_details['status'] == 'regret'){

							$response['highchart_data']['series'][3]['data'][$current_key] += 1;
						}else if($single_rfq_status_details['status'] == 'done'){

							$response['highchart_data']['series'][4]['data'][$current_key] += 1;
						}else if($single_rfq_status_details['status'] == 'proforma'){

							$response['highchart_data']['series'][5]['data'][$current_key] += 1;
						}
					}

					$response['procurment_person_html'] = '<h6 class="dropdown-header">Select Procurement Person</h6>';
					$response['procurment_person_html'] .= '<a class="dropdown-item procurement_id';
					if($procurement_person_id == 'All'){
						$response['procurment_person_html'] .= ' disabled';
					}
					$response['procurment_person_html'] .= ' " value="All">All</a>';
					foreach($this->common_model->get_dynamic_data_sales_db_null_false('*', "status=1 AND role IN(8)", 'users') as $single_details) {
						$response['procurment_person_html'] .= '<a class="dropdown-item procurement_id';
						if($single_details['user_id'] == $procurement_person_id){
							$response['procurment_person_html'] .= ' disabled';
							$response['procurement_person_id'] = $single_details['name'];
						}
						$response['procurment_person_html'] .= '" value="'.$single_details['user_id'].'">'.$single_details['name'].'</a>';
					}
					foreach ($response['highchart_data']['category'] as $category_key => $week_number) {
						
						$response['highchart_data']['category'][$category_key] = date('M j', strtotime("2024W".$week_number.""))."--".date('M j', strtotime("2024W".$week_number." +6 days"));
					}
					// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
				break;
				case 'quotation_proforma_month_wise_highchart':

					$year_value = (!empty($this->input->post('filter_year')))?$this->input->post('filter_year'):'All';
					$years_list = $this->home_model->get_month_wise_proforma_list();
					$filter_sales_person = (!empty($this->input->post('sales_person'))) ? $this->input->post('sales_person'):'All';

					$quotation_proforma_month_wise_details = $this->home_model->get_quotation_proforma_highchart_list_data($year_value, $filter_sales_person);
					// echo "<pre>";print_r($quotation_proforma_month_wise_details);echo"</pre><hr>";exit;

					$unique_year = array_unique(array_column($quotation_proforma_month_wise_details, 'year'));
					$data = array();
					foreach ($unique_year as $year_name) {
						for ($i=1; $i < 13; $i++) {

							$data[$year_name][$i] = 0;
						}
					}

					$currency_details =  array_column($this->common_model->get_dynamic_data_sales_db('currency_id, currency_rate', array('status'=>'Active'), 'currency'), 'currency_rate', 'currency_id');
					$currency_value = $currency_details[1];
					foreach ($quotation_proforma_month_wise_details as $month_wise_data) {

						$month = $month_wise_data['month'];

						$data[$month_wise_data['year']][$month] += round((((int)$month_wise_data['sales_amount']) * ($currency_details[$month_wise_data['currency']] / $currency_value)));
					}
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;

					$response['quotation_proforma_highchart'] = array();
					foreach ($data as $year_name => $year_proforma) {

						ksort($year_proforma);
						$highchart_data = array();
						foreach ($year_proforma as $single_total) {

							$highchart_data[] = (int)$single_total;
						}
						// echo "<pre>";print_r($highchart_data);echo"</pre><hr>";exit;
						$response['quotation_proforma_highchart'][] = array(
																	'name' => $year_name,
																	'data' => $highchart_data
																);
					}

					$response['html_body']['filter_year'] = $this->load->view('home/filter_year', array('years_list' => $years_list, 'year_value'=> $year_value, 'select_year' => 'select_year_for_quotation_proforma'), TRUE);
					$response['html_body']['current_filter'] = $year_value;

					if(in_array($this->session->userdata('role'),array(1, 17))){

						$sales_person = array_column($this->home_model->get_sales_person('','all'),'name', 'user_id');
					} else if ($this->session->userdata('role') == '16') {

						$sales_person = array_column($this->home_model->get_sales_person('','sales_people'),'name', 'user_id');
					}
					$sales_person['All'] = 'All';
					$response['html_body']['filter_sales_person'] = $filter_sales_person;
					$response['html_body']['filter_sales_person_name'] = $sales_person[$filter_sales_person];
					// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
					$response['html_body']['filter_sales_person_html'] = $this->load->view('procurement/filter_sales_person',array(
																					'sales_person' => $sales_person,
																					'filter_sales_person' => $filter_sales_person,
																					'sales_filter_used_for_type' => 'bar_graph'
																				), TRUE);
					// echo "<pre>";print_r($response['html_body']['filter_sales_person_html']);echo"</pre><hr>";exit;
				break;
				case 'get_org_form_details':

					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";die;
					$form_call_type = $this->input->post('form_call_type');
					if($form_call_type == 'add_main'){

						$org_chart_id = $this->input->post('org_chart_id');
						$next_order_id = $this->input->post('next_order_id');
						$reporting_manager_id = $this->input->post('reporting_manager_id');
						if(!empty($next_order_id) && !empty($reporting_manager_id)){

							$data['user_list'] = $this->get_user_list();

							$data['user_designation_list'] = $this->common_model->get_dynamic_data_sales_db('id as value, name, "" as selected', array('status'=>'Active'), 'user_designation');

							$data['org_chart_list'] = array(array('name'=> $next_order_id, 'value'=> $next_order_id,'selected'=> 'selected'));
							$data['org_chart_id'] = $org_chart_id;
							$data['reporting_manager_id'] = $reporting_manager_id;
							$data['order_id'] = 0;
							$response['update_form_body'] = $this->load->view('home/org_chart_form', $data, true);
						}

					}else if($form_call_type == 'edit_main'){

						$org_chart_id = $this->input->post('org_chart_id');
						$reporting_manager_id = $this->input->post('reporting_manager_id');
						if(!empty($this->input->post('org_chart_id'))){

							$data['user_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'id'=> $org_chart_id), 'user_organization_chart', 'row_array');
	
							$data['user_list'] = $this->change_key_as_per_value($this->common_model->get_dynamic_data_sales_db('user_id as value, name, "" as selected', array('status'=>'1'), 'users'));
	
							$data['user_designation_list'] = $this->change_key_as_per_value($this->common_model->get_dynamic_data_sales_db('id as value, name, "" as selected', array('status'=>'Active'), 'user_designation'), 'name');
	
							$data['org_chart_list'] = $this->change_key_as_per_value($this->common_model->get_all_conditional_data_sales_db('order_id as value, order_id as name, "" as selected', array('status'=> 'Active', 'reporting_manager_id'=> 2), 'user_organization_chart', 'result_array', array(), array('column_name'=> 'order_id', 'column_value'=> 'asc')));

							$data['org_chart_list_junior'] = $this->change_key_as_per_value($this->common_model->get_all_conditional_data_sales_db('order_id as value, order_id as name, "" as selected', array('status'=> 'Active', 'reporting_manager_id'=> $data['user_details']['user_id']), 'user_organization_chart', 'result_array', array(), array('column_name'=> 'order_id', 'column_value'=> 'asc')));

							$data['user_list'][$data['user_details']['user_id']]['selected'] = 'selected';
							$data['user_designation_list'][$data['user_details']['designation']]['selected'] = 'selected';
							$data['org_chart_list'][$data['user_details']['order_id']]['selected'] = 'selected';
							$data['org_chart_id'] = $org_chart_id;
							$data['reporting_manager_id'] = $reporting_manager_id;
							$data['order_id'] = $data['user_details']['order_id'];
							$data['user_junior_list'] = $this->common_model->get_all_conditional_data_sales_db('*', array('status'=>'Active', 'reporting_manager_id'=> $data['user_details']['user_id']), 'user_organization_chart', 'result_array', array(), array('column_name'=> 'order_id', 'column_value'=>'asc'));
							$response['junior_next_order_id'] = count($data['user_junior_list'])+1;
							$response['junior_reporting_manager_id'] = $data['user_details']['user_id'];

							// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
							$response['update_form_body'] = $this->load->view('home/org_chart_form', $data, true);
							$response['user_list_detail_body'] = $this->load->view('home/org_chart_form_2', $data, true);
						}
					}else if($form_call_type == 'add_sub'){

						$org_chart_id = $this->input->post('org_chart_id');
						$next_order_id = $this->input->post('next_order_id');
						$reporting_manager_id = $this->input->post('reporting_manager_id');
						if(!empty($next_order_id) && !empty($reporting_manager_id)){

							$data['user_list'] = $this->get_user_list();

							$data['user_designation_list'] = $this->common_model->get_dynamic_data_sales_db('id as value, name, "" as selected', array('status'=>'Active'), 'user_designation');

							$data['org_chart_list_junior'] = array(array('name'=> $next_order_id, 'value'=> $next_order_id,'selected'=> 'selected'));
							$data['org_chart_id'] = $org_chart_id;
							$data['reporting_manager_id'] = $reporting_manager_id;
							$data['current_key'] = $next_order_id;
							$data['user_junior_list'] = array(($next_order_id-1)=>array('user_id'=>0, 'designation'=>'', 'order_id'=> 0, 'reporting_manager_id'=> $reporting_manager_id, 'id'=> 0));
							$response['junior_next_order_id'] = $next_order_id+1;
							$response['user_list_detail_body'] = $this->load->view('home/org_chart_form_2', $data, true);
						}
					}
				break;
				case 'save_org_details':

					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";die;
					$insert_update_array = array();
					$form_data = array_column($this->input->post('form_data'), 'value', 'name');
					$explode_name = explode("_", $form_data['name']);
					$insert_update_array['user_id'] = $explode_name[0];
					$explode_first_name = explode(" ", $explode_name[1]);
					$insert_update_array['name'] = $explode_first_name[0].' '.$explode_first_name[1][0];
					$insert_update_array['designation'] = $form_data['designation'];
					$insert_update_array['reporting_manager_id'] = $form_data['reporting_manager_id'];
					$insert_update_array['order_id'] = $form_data['order_id'];

					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";die;
					if($form_data['order_id_old'] != 0 && $form_data['order_id'] != $form_data['order_id_old']){
						$this->update_order_of_all($form_data['reporting_manager_id'], $form_data['order_id'], $form_data['order_id_old']);
					}
					if(!empty($insert_update_array)){

						if(!empty($this->input->post('id'))){

							// echo "<pre>";print_r($insert_update_array);echo"</pre><hr>";die('came in update');
							$this->common_model->update_data_sales_db('user_organization_chart', $insert_update_array, array('id'=> $this->input->post('id')));
	
						}else{
	
							// echo "<pre>";print_r($insert_update_array);echo"</pre><hr>";die('came in insert');
							$this->common_model->insert_data_sales_db('user_organization_chart', $insert_update_array);
						}
					}

					
				break;
				case 'change_type':

					$this->session->set_userdata(array('type'=> $this->input->post('type')));
					//production count
					$order_status_wise_total = $this->get_order_wise_total_new($this->session->userdata('type'));
					$data['order_status_wise_total'] = $order_status_wise_total['order_status_wise_total'];
					$data['total_of_3_order'] = $order_status_wise_total['total_of_3_order'];
					$data['total_of_3_order_number'] = $order_status_wise_total['total_of_3_order_number'];
					$data['currency_list'] = array_column($this->common_model->get_dynamic_data_sales_db('currency_id, decimal_number',array('status'=>'Active'), 'currency'),'decimal_number','currency_id');
					
					$response['production_title_value'] = $data['total_of_3_order'].'('.$data['total_of_3_order_number'].')';
					$response['status_wise_count'] = $this->load->view('production/status', $data, true);
				break;
				case 'get_sales_routine_list':
					
					$data =  array();
					$data['routine_list'] = array('completed_routine'=> array(), 'pending_routine'=> array());
					$routine_list = array_column($this->common_model->get_dynamic_data_sales_db('id, name', array('status'=>'Active'), 'sales_routine'), 'name', 'id');
					$routine_assign_list = $this->common_model->get_dynamic_data_sales_db('sales_routine_id', array('status'=>'Active', 'user_id'=> $this->session->userdata('user_id')), 'user_to_sales_routine');
					$all_color = array(
						0 => 'kt-widget2__item--primary',
						1 => 'kt-widget2__item--warning',
						2 => 'kt-widget2__item--brand',
						3 => 'kt-widget2__item--success',
						4 => 'kt-widget2__item--danger',
					);

					if(!empty($routine_assign_list)){

						foreach ($routine_assign_list as $routine_assign_list_key => $routine_assign_list_value) {
							
							$routine_details_today = $this->common_model->get_dynamic_data_sales_db('work_completed, work_comment', array('status'=>'Active', 'user_id'=> $this->session->userdata('user_id'), 'sales_routine_id'=> $routine_assign_list_value['sales_routine_id'], 'date(work_date)'=> $this->get_indian_time('y-m-d')), 'daily_work_on_sales_routine');

							if(!empty($routine_details_today)){
								
								if($routine_details_today[count($routine_details_today)-1]['work_completed'] == 'yes'){

									$count = count($data['routine_list']['completed_routine']);
									$data['routine_list']['completed_routine'][$count]['sales_routine_id'] = $routine_assign_list_value['sales_routine_id'];
									$data['routine_list']['completed_routine'][$count]['sales_routine_name'] = $routine_list[$routine_assign_list_value['sales_routine_id']];
									$data['routine_list']['completed_routine'][$count]['work_completed'] = $routine_details_today[count($routine_details_today)-1]['work_completed'];
									$data['routine_list']['completed_routine'][$count]['work_comment'] = $routine_details_today[count($routine_details_today)-1]['work_comment'];
									$data['routine_list']['completed_routine'][$count]['color'] = $all_color[$count%5];
								}else if($routine_details_today[count($routine_details_today)-1]['work_completed'] == 'no'){

									$count = count($data['routine_list']['pending_routine']);
									$data['routine_list']['pending_routine'][$count]['sales_routine_id'] = $routine_assign_list_value['sales_routine_id'];
									$data['routine_list']['pending_routine'][$count]['sales_routine_name'] = $routine_list[$routine_assign_list_value['sales_routine_id']];
									$data['routine_list']['pending_routine'][$count]['work_completed'] = $routine_details_today[count($routine_details_today)-1]['work_completed'];
									$data['routine_list']['pending_routine'][$count]['work_comment'] = $routine_details_today[count($routine_details_today)-1]['work_comment'];
									$data['routine_list']['pending_routine'][$count]['color'] = $all_color[$count%5];
								}
								
							}else{

								$count = count($data['routine_list']['pending_routine']);
								$data['routine_list']['pending_routine'][$count]['sales_routine_id'] = $routine_assign_list_value['sales_routine_id'];
								$data['routine_list']['pending_routine'][$count]['sales_routine_name'] = $routine_list[$routine_assign_list_value['sales_routine_id']];
								$data['routine_list']['pending_routine'][$count]['work_completed'] = '';
								$data['routine_list']['pending_routine'][$count]['work_comment'] = '';
								$data['routine_list']['pending_routine'][$count]['color'] = $all_color[$count%5];
							}
							
						}
					}
					// echo "<pre>";print_r($data);echo"</pre><hr>";die('debug');
					$response['sales_routine_div'] = $this->load->view('home/sales_routine', $data, true);
				break;
				case 'save_daily_work_on_sales_routine':
					
					$insert_array = array();
					$insert_array['user_id'] = $this->session->userdata('user_id');
					$insert_array['sales_routine_id'] = $this->input->post('sales_routine_id');
					$insert_array['work_completed'] = $this->input->post('work_completed');
					$insert_array['work_comment'] = $this->input->post('work_comment');
					$insert_array['work_date'] = $this->get_indian_time();
					$this->common_model->insert_data_sales_db('daily_work_on_sales_routine', $insert_array);
				break;
				default:
					$response['status'] = 'failed';	
					$response['message'] = "call type not found";
				break;
			}
			echo json_encode($response);

		} else{
			
			die('access is not allowed to this function');
		}
	}
	private function update_order_of_all($reporting_manager_id, $order_id, $order_id_old){

		$res = $this->common_model->get_all_conditional_data_sales_db('*', array('status'=> 'Active', 'reporting_manager_id'=> $reporting_manager_id), 'user_organization_chart', 'result_array', array(), array('column_name'=> 'order_id', 'column_value'=> 'asc'));
		$res_2 = array();
		foreach ($res as $single_res) {

			$res_2[$single_res['order_id']] = $single_res;
		}
		$batch_update = array();
		if($order_id > $order_id_old){

			for($i = $order_id_old; $i<=$order_id; $i++){

				if($i == $order_id_old){

					$batch_update[] = array('id'=>$res_2[$i]['id'], 'order_id'=>$order_id);
				}else{

					$batch_update[] = array('id'=>$res_2[$i]['id'], 'order_id'=>$res_2[$i]['order_id']-1);
				}
			}
		}else{

			for($i = $order_id_old; $i>=$order_id; $i--){

				if($i == $order_id_old){

					$batch_update[] = array('id'=>$res_2[$i]['id'], 'order_id'=>$order_id);
				}else{

					$batch_update[] = array('id'=>$res_2[$i]['id'], 'order_id'=>$res_2[$i]['order_id']+1);
				}
			}
		}
		// echo "<pre>";print_r($batch_update);echo"</pre><hr>";die('debug');
		if(!empty($batch_update)){

			$this->common_model->update_data_sales_db('user_organization_chart', $batch_update, 'id', 'batch');
		}
	}
	private function get_user_list(){

		$return_array = array();
		$return_array = $this->home_model->get_org_form_name();
		return $return_array;
	}
	private function change_key_as_per_value($data, $changing_key_column_name = 'value'){

		$return_data = array();
		foreach ($data as $single_data) {
			
			$return_data[$single_data[$changing_key_column_name]] = $single_data;
		}

		return $return_data;
	}
	private function prepare_revenue_highchart_data($month_year_value, $year_value) {

		$highchart_data =  array();
		$currency_details =  array_column($this->common_model->get_dynamic_data_sales_db('currency_id, currency_rate', array('status'=>'Active'), 'currency'), 'currency_rate', 'currency_id');
		$currency_value = $currency_details[1];
		$data = $this->home_model->get_revenue($month_year_value, $year_value);
		if(!empty($data)) {

			foreach ($data as $data_value) {
				
				$highchart_data[$data_value['year_or_month']]['grand_total'] += round((((int)$data_value['grand_total']) * ($currency_details[$data_value['currency_id']] / $currency_value)));	
			}
		}
		return $highchart_data;
	}
	private function prepare_all_view_details($tab_name) {

		$return_array = array();
		$return_array['current_tab'] = $tab_name;
		$view_name = 'Product List';
		$add_new_flag = true;
		$add_new_name = 'Add Product';
		$add_new_modal_name = '#add_product';
		$table_listing_flag = true;
		$table_header_column_array[0] = array('th_style'=>'width: 20%;', 'th_span_style'=>'padding: 0% 15%;', 'th_value'=>'Number');
		$table_header_column_array[1] = array('th_style'=>'width: 30%;', 'th_span_style'=>'', 'th_value'=>'');
		$table_header_column_array[2] = array('th_style'=>'width: 30%;', 'th_span_style'=>'', 'th_value'=>'Added Date');
		$table_header_column_array[3] = array('th_style'=>'width: 20%;', 'th_span_style'=>'', 'th_value'=>'Actions');
		$delete_name = 'Product';


		switch ($tab_name) {
			case 'product':
				$table_header_column_array[1]['th_value'] = 'Product Name';
				$table_body_data = $this->prepare_table_body_for_management(
													$this->home_model->get_all_data(
																			'name, add_time as date, id as delete_id',
																			array(
																				'status' => 'Active'
																			),
																			'product_mst'
																		),
																		'product'	
																	);
			break;

			case 'material':
				$view_name = 'Material List';
				$add_new_flag = true;
				$add_new_name = 'Add Material';
				$add_new_modal_name = '#add_material';
				$table_listing_flag = true;
				$table_header_column_array[1]['th_value'] = 'Material Name';
				$delete_name = 'Material';
				$table_body_data = $this->prepare_table_body_for_management(
													$this->home_model->get_all_data(
																			'name, add_time as date, id as delete_id',
																			array(
																				'status' => 'Active'
																			),
																			'material_mst'
																		),
																		'material'
																	);
			break;
			
			case 'procurement_pdf_footer':
				$view_name = 'Procurement Footer List';
				$add_new_flag = false;
				$add_new_name = '';
				$add_new_modal_name = '#add_material';
				$table_listing_flag = false;
				$table_header_name = '';
				$delete_name = '';
				$footer_text = $this->home_model->get_all_data('footer_text', array('status'=> 'Active', 'pdf_type'=> 'procurement'), 'pdf_footer', 'row_array');
				$table_body_data = $footer_text['footer_text'];
			break;	

			case 'delivery':
				$view_name = 'Delivered List';
				$add_new_flag = true;
				$add_new_name = 'Add Delivery Place';
				$add_new_modal_name = '#add_delivery_place';
				$table_listing_flag = true;
				$table_header_column_array[1]['th_value'] = 'Delivery Name';
				$table_header_column_array[1]['th_style'] = 'width: 20%;';
				$table_header_column_array[4] = $table_header_column_array[3];
				$table_header_column_array[3] = $table_header_column_array[2];
				$table_header_column_array[3]['th_style'] = 'width: 20%;';
				$table_header_column_array[2] = array('th_style'=>'width: 20%;', 'th_span_style'=>'', 'th_value'=>'Transport Mode');;
				$delete_name = 'Delivery Place';
				$table_body_data = $this->prepare_table_body_for_management(
													$this->home_model->get_all_data(
																			'delivery_name as name, transport_id, add_time as date, delivery_id as delete_id',
																			array(
																				'status' => 'Active'
																			),
																			'delivery'
																		),
																		'delivery'
																	);
			break;

			case 'delivery_time':
				$view_name = 'Delivery Time List';
				$add_new_flag = true;
				$add_new_name = 'Add Delivery Time';
				$add_new_modal_name = '#add_delivery_time';
				$table_listing_flag = true;
				$table_header_column_array[1]['th_value'] = 'Delivery Time';
				$delete_name = 'Delivery Time';
				$table_body_data = $this->prepare_table_body_for_management(
													$this->home_model->get_all_data(
																			'dt_value as name, add_time as date, dt_id as delete_id',
																			array(
																				'status' => 'Active'
																			),
																			'delivery_time'
																		),
																		'delivery_time'	
																	);
			break;

			case 'payment_terms':
				$view_name = 'Payments List';
				$add_new_flag = true;
				$add_new_name = 'Add Payment';
				$add_new_modal_name = '#add_payment';
				$table_listing_flag = true;
				$table_header_column_array[1]['th_value'] = 'Payments';
				$delete_name = 'Payments';
				$table_body_data = $this->prepare_table_body_for_management(
													$this->home_model->get_all_data(
																			'term_value as name, add_time as date, term_id as delete_id',
																			array(
																				'status' => 'Active'
																			),
																			'payment_terms'
																		),
																		'payment_terms'	
																	);
			break;	

			case 'validity':
				$view_name = 'Validity List';
				$add_new_flag = true;
				$add_new_name = 'Add Validity';
				$add_new_modal_name = '#add_validity';
				$table_listing_flag = true;
				$table_header_column_array[1]['th_value'] = 'Validity';
				$delete_name = 'Validity';
				$table_body_data = $this->prepare_table_body_for_management(
													$this->home_model->get_all_data(
																			'validity_value as name, add_time as date, validity_id as delete_id',
																			array(
																				'status' => 'Active'
																			),
																			'validity'
																		),
																		'validity'	
																	);
			break;

			case 'currency':
				$view_name = 'Currency List';
				$add_new_flag = true;
				$add_new_name = 'Add Currency';
				$add_new_modal_name = '#add_currency';
				$table_listing_flag = true;
				$table_header_column_array[1]['th_value'] = 'Currency';
				$table_header_column_array[1]['th_style'] = 'width: 20%;';
				$table_header_column_array[4] = $table_header_column_array[3];
				$table_header_column_array[3] = $table_header_column_array[2];
				$table_header_column_array[3]['th_style'] = 'width: 20%;';
				$table_header_column_array[2] = array('th_style'=>'width: 20%;', 'th_span_style'=>'', 'th_value'=>'Currency Rate');;
				$delete_name = 'Currency';
				$table_body_data = $this->prepare_table_body_for_management(
													$this->home_model->get_all_data(
																			'currency as name, currency_icon, currency_rate, add_time as date, currency_id as delete_id',
																			array(
																				'status' => 'Active'
																			),
																			'currency'
																		),
																		'currency'
																	);
			break;

			case 'origin_country':
				$view_name = 'Country Origin List';
				$add_new_flag = true;
				$add_new_name = 'Add Country Origin';
				$add_new_modal_name = '#add_country_origin';
				$table_listing_flag = true;
				$table_header_column_array[1]['th_value'] = 'Country Origin';
				$delete_name = 'Country Origin';
				$table_body_data = $this->prepare_table_body_for_management(
													$this->home_model->get_all_data(
																			'country as name, add_time as date, country_id as delete_id',
																			array(
																				'status' => 'Active'
																			),
																			'origin_country'
																		),
																		'origin_country'	
																	);
			break;
			
			case 'mtc_type':
				$view_name = 'MTC Type';
				$add_new_flag = true;
				$add_new_name = 'Add Mtc Type';
				$add_new_modal_name = '#add_mtc_type';
				$table_listing_flag = true;
				$table_header_column_array[1]['th_value'] = 'MTC Type';
				$delete_name = 'MTC Type';
				$table_body_data = $this->prepare_table_body_for_management(
													$this->home_model->get_all_data(
																			'mtc_value as name, add_time as date, mtc_id as delete_id',
																			array(
																				'status' => 'Active'
																			),
																			'mtc_type'
																		),
																		'mtc_type'	
																	);
			break;																													

			case 'transport_mode':
				$view_name = 'Transport Mode';
				$add_new_flag = true;
				$add_new_name = 'Add Transport Mode';
				$add_new_modal_name = '#add_transport_mode';
				$table_listing_flag = true;
				$table_header_column_array[1]['th_value'] = 'Transport Mode';
				$delete_name = 'Transport Mode';
				$table_body_data = $this->prepare_table_body_for_management(
													$this->home_model->get_all_data(
																			'mode as name, add_time as date, mode_id as delete_id',
																			array(
																				'status' => 'Active'
																			),
																			'transport_mode'
																		),
																		'transport_mode'	
																	);
			break;

			case 'lead_stage_reasons':
				$view_name = 'Lead Stage Reasons';
				$add_new_flag = true;
				$add_new_name = 'Add Lead Stage Reasons';
				$add_new_modal_name = '#add_lead_stage_reasons';
				$table_listing_flag = true;
				$table_header_column_array[1]['th_value'] = 'Lead Stage Reasons';
				$delete_name = 'Lead Stage Reasons';
				$table_body_data = $this->prepare_table_body_for_management(
													$this->home_model->get_all_data(
																			'reason as name, add_time as date, lead_reason_id as delete_id',
																			array(
																				'status' => 'Active'
																			),
																			'lead_stage_reasons'
																		),
																		'lead_stage_reasons'	
																	);
				//echo "<pre>";print_r($table_header_column_array);echo"</pre><hr>";exit;	
			break;
			case 'quotation_close_reasons':
				$view_name = 'Quotation Close Reasons';
				$add_new_flag = true;
				$add_new_name = 'Add Reason';
				$add_new_modal_name = '#add_reason';
				$table_listing_flag = true;
				$table_header_column_array[1]['th_value'] = 'Quotation Close Reasons';
				$delete_name = 'Quotation Close Reasons';
				$table_body_data = $this->prepare_table_body_for_management(
													$this->home_model->get_all_data(
																			'reason_text as name, add_time as date, reason_id as delete_id',
																			array(
																				'status' => 'Active'
																			),
																			'close_reason'
																		),
																		'quotation_close_reasons');	
				
				//echo "<pre>";print_r($table_header_column_array);echo"</pre><hr>";exit;													
			break;
			case 'rfq_close_reasons':
				$view_name = 'RFQ Close Reasons';
				$add_new_flag = true;
				$add_new_name = 'Add Reason';
				$add_new_modal_name = '#add_rfq_close_reasons';
				$table_listing_flag = true;
				$table_header_column_array[1]['th_value'] = 'RFQ Close Reasons';
				$delete_name = 'RFQ Close Reasons';
				$table_body_data = $this->prepare_table_body_for_management(
													$this->home_model->get_all_data(
																			'name, add_time as date, id as delete_id',
																			array(
																				'status' => 'Active'
																			),
																			'rfq_close_reason'
																		),
																		'rfq_close_reasons');
			break;

			default:
				
			break;
		}
		$return_array['view_details']['view_name'] = $view_name; 
		$return_array['view_details']['add_new_flag'] = $add_new_flag;
		$return_array['view_details']['add_new_name'] = $add_new_name;
		$return_array['view_details']['add_new_modal_name'] = $add_new_modal_name;
		$return_array['view_details']['table_listing_flag'] = $table_listing_flag;
		$return_array['view_details']['table_header_column_array'] = $table_header_column_array;
		$return_array['view_details']['table_body'] = $table_body_data;
		$return_array['view_details']['delete_name'] = $delete_name;
		 //echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;

		return $return_array;
	}
	private function generate_quotation_details($date) {

		$return_array = array();
		$quotation_won_details = $this->home_model->get_quotation_won_data($date, $this->session->userdata('user_id'));
		foreach ($quotation_won_details as $key => $value) {
			
			$return_array[date('Y-m-d', strtotime($value['confirmed_on']))][] = $value;  
		}
    	return $return_array;
	}
	private function generate_follow_up_details($date) {

		$return_array = array();
		$follow_up_details = $this->home_model->get_quotation_follow_up_data($date, $this->session->userdata('user_id'));
		// echo "<pre>";print_r($follow_up_details);echo"</pre><hr>";exit;
		foreach ($follow_up_details as $key => $value) {
			
			$return_array[date('Y-m-d', strtotime($value['followup_date']))][] = $value;  
		}
    	return $return_array;	
	}
	private function generate_task_details($date) {

		$return_array = array();
		$task_open_details = $this->home_model->get_task_data('Open', $date, $this->session->userdata('user_id'));
		// echo "<pre>";print_r($task_open_details);echo"</pre><hr>";exit;
		foreach ($task_open_details as $key => $value) {
			
			$return_array[date('Y-m-d', strtotime($value['followup_date']))][] = $value;  
		}
    	return $return_array;	
	}
	private function static_image_color() {

		$return_array = array();
		$return_array['image_not_found_array'] = array('kt-font-success', 'kt-font-danger', 'kt-font-warning');
    	$return_array['image_not_found_array_key'] = 0;
    	return $return_array;	
	}
	private function prepare_table_body_for_management($data, $call_type) {

		// echo "<pre>";print_r($data);echo"</pre><hr>";die;
		$return_array = array();
		if(in_array($call_type, array('product', 'material', 'transport_mode', 'delivery_time', 'payment_terms', 'validity', 'origin_country', 'mtc_type', 'lead_stage_reasons','quotation_close_reasons', 'rfq_close_reasons'))) {

			foreach ($data as $key => $value) {
				
				$return_array[$key]['id'] = $key+1;
				$return_array[$key]['name'] = $value['name'];
				$return_array[$key]['date'] = date('F j, Y', strtotime($value['date']));
				$return_array[$key]['delete_id'] = $value['delete_id'];
			}
		} else if (in_array($call_type, array('delivery'))) {

			$transport_mode = array_column($this->home_model->get_all_data('mode, mode_id',array('status' => 'Active'),'transport_mode'), 'mode', 'mode_id');
			foreach ($data as $key => $value) {
				
				$return_array[$key]['id'] = $key+1;
				$return_array[$key]['name'] = $value['name'];
				$return_array[$key]['name_2'] = $transport_mode[$value['transport_id']];
				$return_array[$key]['date'] = date('F j, Y', strtotime($value['date']));
				$return_array[$key]['delete_id'] = $value['delete_id'];
			}
		} else if (in_array($call_type, array('currency'))) {

			foreach ($data as $key => $value) {
			
				$return_array[$key]['id'] = $key+1;
				$return_array[$key]['name'] = $value['name'];
				$return_array[$key]['name_2'] = $value['currency_icon'].$value['currency_rate'];
				$return_array[$key]['date'] = date('F j, Y', strtotime($value['date']));
				$return_array[$key]['delete_id'] = $value['delete_id'];
			}
		}

		// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		return $return_array;
	}
	private function reset_field($type, $id) {

		switch ($type) {
			case 'Product':
			case 'Material':
					
				$this->home_model->updateData('lookup', array('status'=> 'Inactive'), array('lookup_id'=>$id));
			break;

			case 'Delivery Place':
				
				$data = $this->common_model->get_dynamic_data_sales_db('*',array('delivery_id'=>$id), 'delivery', 'row_array');
				$this->home_model->updateData('procurement', array('delivery_to'=> NULL), array('delivery_to'=>$data['delivery_name']));
				$this->home_model->updateData('quotation_mst', array('delivered_through'=> NULL), array('delivered_through'=>$id));
			break;

			case 'Delivery Time':
				
				$data = $this->common_model->get_dynamic_data_sales_db('*',array('dt_id'=>$id), 'delivery_time', 'row_array');
				$this->home_model->updateData('procurement', array('delivery_time'=> NULL), array('delivery_time'=>$data['dt_value']));
				$this->home_model->updateData('quotation_mst', array('delivered_through'=> NULL), array('delivered_through'=>$id));
			break;

			case 'Payments':
				
				$data = $this->common_model->get_dynamic_data_sales_db('*',array('term_id'=>$id), 'payment_terms', 'row_array');
				$this->home_model->updateData('procurement', array('payment'=> NULL), array('payment'=>$data['term_value']));
				$this->home_model->updateData('quotation_mst', array('payment_term'=> NULL), array('payment_term'=>$id));
			break;

			case 'Validity':
				
				$data = $this->common_model->get_dynamic_data_sales_db('*',array('validity_id'=>$id), 'validity', 'row_array');
				$this->home_model->updateData('procurement', array('validity'=> NULL), array('validity'=>$data['validity_value']));
				$this->home_model->updateData('quotation_mst', array('validity'=> NULL), array('validity'=>$id));
			break;

			case 'Currency':
				
				$data = $this->common_model->get_dynamic_data_sales_db('*',array('currency_id'=>$id), 'currency', 'row_array');
				$this->home_model->updateData('procurement', array('currency'=> NULL), array('currency'=>$data['currency']));
				$this->home_model->updateData('quotation_mst', array('currency'=> NULL), array('currency'=>$id));
			break;

			case 'Country Origin':
				
				$data = $this->common_model->get_dynamic_data_sales_db('*',array('country_id'=>$id), 'origin_country', 'row_array');
				$this->home_model->updateData('procurement', array('country_of_origin'=> NULL), array('country_of_origin'=>$data['country']));
				$this->home_model->updateData('quotation_mst', array('origin_country'=> NULL), array('origin_country'=>$id));
			break;

			case 'MTC Type':
			
				$data = $this->common_model->get_dynamic_data_sales_db('*',array('mtc_id'=>$id), 'mtc_type', 'row_array');
				$this->home_model->updateData('procurement', array('mtc_type'=> NULL), array('mtc_type'=>$data['mtc_value']));
				$this->home_model->updateData('quotation_mst', array('mtc_type'=> NULL), array('mtc_type'=>$id));
			break;

			case 'Transport Mode':
				
				$data = $this->common_model->get_dynamic_data_sales_db('*',array('mode_id'=>$id), 'transport_mode', 'row_array');
				$this->home_model->updateData('procurement', array('packing_type'=> NULL), array('packing_type'=>$data['mode']));
				$this->home_model->updateData('quotation_mst', array('transport_mode'=> NULL), array('transport_mode'=>$id));
			break;
			case 'Quotation Close Reasons':
				
				$data = $this->common_model->get_dynamic_data_sales_db('*',array('reason_id'=>$id), 'close_reason', 'row_array');
				$this->home_model->updateData('quotation_mst', array('close_reason'=> NULL), array('close_reason'=>$id));
			break;

			default:
				
			break;
		}
	}
	private function create_notice_details($notice_type = '') {

		$data = array();
		$where_array = array('status'=> 'Active', 'expiry_time >='=> date('Y-m-d'));
		if(!empty($notice_type)) {

			$where_array['notice_type'] = $notice_type;

		}

		if(!empty($notice_type)) {

			$notice_details = $this->common_model->get_dynamic_data_sales_db('*',$where_array, 'user_notice');
			// echo "<pre>";print_r($notice_details);echo"</pre><hr>";exit;
			if(!empty($notice_details)) {
				foreach ($notice_details as $single_notice_details) {
					if(empty($single_notice_details['notice_access_role_id'])) {

						$data[] = $single_notice_details;
					}else{
						$explode_role_id = explode(',', $single_notice_details['notice_access_role_id']);
						if(in_array($this->session->userdata('role'), $explode_role_id)) {

							$data[] = $single_notice_details;

						}

					}
				}
			}
		}else{

			if(!empty($notice_details)) {
				foreach ($notice_details as $single_notice_details) {
					if(empty($single_notice_details['notice_access_role_id'])) {

						$data[$single_notice_details['notice_type']] += 1;
					}else{
						$explode_role_id = explode(',', $single_notice_details['notice_access_role_id']);
						if(in_array($this->session->userdata('role'), $explode_role_id)) {

							$data[$single_notice_details['notice_type']] += 1;

						}

					}
				}
			}

		}
		return $data;
	}
	//production module code start
	private function prepare_listing_data($where, $order_by, $limit, $offset, $search_filter_array = array()) {

		$data = $this->production_model->get_production_listingz_data($where, $order_by,$limit, $offset);
		// echo "<pre>";print_r($data);echo"</pre><hr>";die;
		$data['total_revenue_list'] = array();
		foreach ($data['production_list'] as $production_list_key => $production_list_value) {
			
			// echo "<pre>";print_r($production_list_value);echo"</pre><hr>";
			//getting product details
			$product_details = $this->common_model->get_dynamic_data_sales_db('*',array('quotation_mst_id'=>$production_list_value['quotation_mst_id']), 'quotation_dtl');
			$temp_total_value = 0;
			if(!empty($product_details)) {
					
				foreach ($product_details as $product_key => $single_product_detail) {

					// echo "<pre>";print_r($single_product_detail);echo"</pre><hr>";
					// echo "<pre>";print_r($this->session->userdata('production_tab_name'));echo"</pre><hr>";
					if($this->session->userdata('production_tab_name') == 'pending_order') {
						
						if($single_product_detail['production_status'] == 'pending_order') {

							if($single_product_detail['production_quantity_done'] == 0){

								$temp_total_value += $single_product_detail['row_price'];
							}else {
								if(($single_product_detail['quantity'] - $single_product_detail['production_quantity_done']) > 0) {

									$temp_total_value += $single_product_detail['unit_price'] * ($single_product_detail['quantity'] - $single_product_detail['production_quantity_done']);
								}
							}
						}else {
							if(($single_product_detail['quantity'] - $single_product_detail['production_quantity_done']) > 0) {

								$temp_total_value += $single_product_detail['unit_price'] * ($single_product_detail['quantity'] - $single_product_detail['production_quantity_done']);
							}
						}
					}else {

						if($single_product_detail['production_status'] == $this->session->userdata('production_tab_name')) {

							if($single_product_detail['production_quantity_done'] == 0){

								$temp_total_value += $single_product_detail['row_price'];
							}else {
								
								$temp_total_value += $single_product_detail['unit_price']*$single_product_detail['production_quantity_done'];
							}
						}
					}
					// echo "<pre>";print_r($single_product_detail);echo"</pre><hr>";
					// echo "<pre>";print_r($temp_total_value);echo"</pre><hr>";

				}
			}
			// echo "<pre>";print_r($temp_total_value);echo"</pre><hr>";
			// echo "<pre>";print_r(($production_list_value['semi_ready_count'] + $production_list_value['ready_for_dispatch_count'] + $production_list_value['dispatch_count'] + $production_list_value['on_hold_count'] + $production_list_value['cancel_count'] + $production_list_value['mtt_count'] + $production_list_value['query_count'] + $production_list_value['mtt_rfd_count']));echo"</pre><hr>";



			if(($production_list_value['semi_ready_count'] + $production_list_value['ready_for_dispatch_count'] + $production_list_value['dispatch_count'] + $production_list_value['on_hold_count'] + $production_list_value['cancel_count'] + $production_list_value['mtt_count'] + $production_list_value['query_count'] + $production_list_value['mtt_rfd_count']) > 0){

				if(!empty($temp_total_value)){
					$data['production_list'][$production_list_key]['grand_total'] = $temp_total_value;
					if(empty($data['total_revenue_list'][$production_list_value['currency']])) {

						$data['total_revenue_list'][$production_list_value['currency']]['currency'] = $production_list_value['currency'];
						$data['total_revenue_list'][$production_list_value['currency']]['total'] = $temp_total_value;
					} else {

						$data['total_revenue_list'][$production_list_value['currency']]['total'] += $temp_total_value;
					}
				}else{

					unset($data['production_list'][$production_list_key]);
					$data['paggination_data']['total_rows']  = $data['paggination_data']['total_rows'] - 1;
				}
			}else{
				// die('came in');
				if(empty($data['total_revenue_list'][$production_list_value['currency']])) {

					$data['total_revenue_list'][$production_list_value['currency']]['currency'] = $production_list_value['currency'];
					$data['total_revenue_list'][$production_list_value['currency']]['total'] = $production_list_value['grand_total'];
				} else {

					$data['total_revenue_list'][$production_list_value['currency']]['total'] += $production_list_value['grand_total'];
				}
			}
			// echo "<pre>";print_r($data['total_revenue_list']);echo"</pre><hr>";
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";die('came in');
		return $data;
	}
	private function prepare_listing_data_new($where, $order_by, $limit, $offset, $search_filter_array = array()) {

		// echo "<pre>";print_r($where);echo"</pre><hr>";exit;
		$data = $this->home_model->get_production_listingz_data($where, $order_by,$limit, $offset);
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$return_data['total_revenue_list'] = array();
		$return_data['paggination_data'] = $data['paggination_data'];
		foreach ($data['production_list'] as $production_list_key => $production_list_value) {
			
			$temp_grand_total = 0;
			// echo "<pre>";print_r($production_list_value);echo"</pre><hr>";
			if(!isset($return_data['total_revenue_list'][$production_list_value['currency']])){

				$return_data['total_revenue_list'][$production_list_value['currency']]['currency'] = $production_list_value['currency'];
				$return_data['total_revenue_list'][$production_list_value['currency']]['total'] = 0;
			}

			if(($production_list_value['semi_ready_count'] + $production_list_value['ready_for_dispatch_count'] + $production_list_value['dispatch_count'] + $production_list_value['on_hold_count'] + $production_list_value['query_count'] + $production_list_value['mtt_rfd_count'] + $production_list_value['mtt_count'] + $production_list_value['cancel_count']) > 0){
				
				if($this->session->userdata('production_tab_name') == 'pending_order') {

					$temp_grand_total = $this->home_model->get_pending_count($production_list_value['quotation_mst_id']);
				}else{

					$temp_grand_total = $this->home_model->get_other_count($production_list_value['quotation_mst_id'], $this->session->userdata('production_tab_name'));
					
				}
				if($temp_grand_total > 0){

					$return_data['total_revenue_list'][$production_list_value['currency']]['total'] += $temp_grand_total;
					// $return_data['total_revenue_list'][$production_list_value['currency']]['total'] += ($production_list_value['freight'] + $production_list_value['bank_charges'] + $production_list_value['gst']);
					// $return_data['total_revenue_list'][$production_list_value['currency']]['total'] -= $production_list_value['discount'];
				}else{

					// $return_data['paggination_data']['total_rows'] = $return_data['paggination_data']['total_rows']-1;
					// if($return_data['paggination_data']['total_rows'] == 0){

					// 	$return_data['total_revenue_list'] = array();
					// }
				}
				
			}else{

				$return_data['total_revenue_list'][$production_list_value['currency']]['total'] += $production_list_value['net_total'];
			}
			// echo "<pre>";print_r($return_data['total_revenue_list']);echo"</pre><hr>";
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";die('came in');
		return $return_data;
	}
	private function default_where_for_all_tab($tab_name){

		$this->session->set_userdata('production_tab_name',$tab_name);
		$return_where = array(
							0=> "quotation_mst.stage = 'proforma'",
							1=> "quotation_mst.status = 'Won'",
							2=> "quotation_mst.work_order_no != 0",
							3=> "(production_process_information.production_status is NULL OR production_process_information.production_status != 'dispatched' AND production_process_information.production_status != 'on_hold' AND production_process_information.production_status != 'ready_for_dispatch' AND production_process_information.production_status != 'semi_ready' AND
							production_process_information.production_status != 'merchant_trade' AND 
							production_process_information.production_status != 'query' AND
							production_process_information.production_status != 'mtt_rfd' AND
							production_process_information.production_status != 'order_cancelled')",
							4=> "(production_process_information.status is NULL or production_process_information.status = 'Active')");
		if($tab_name == 'dispatch_order') {
			$return_where[3] = "(production_process_information.production_status = 'dispatched' OR production_process_information.dispatch_count > 0)";
		
		}elseif ($tab_name == 'on_hold_order') {
			$return_where[3] = "(production_process_information.production_status = 'on_hold' OR production_process_information.on_hold_count > 0)";
		
		}elseif ($tab_name == 'ready_to_dispatch_order') {
			$return_where[3] = "(production_process_information.production_status = 'ready_for_dispatch' OR production_process_information.ready_for_dispatch_count > 0)";
		
		}elseif ($tab_name == 'semi_ready') {
			$return_where[3] = "(production_process_information.production_status = 'semi_ready' OR production_process_information.semi_ready_count > 0)";
		
		}elseif ($tab_name == 'order_cancelled') {
			$return_where[3] = "(production_process_information.production_status = 'order_cancelled' OR production_process_information.cancel_count > 0)";
		
		}elseif ($tab_name == 'mtt') {
			$return_where[3] = "(production_process_information.production_status = 'merchant_trade' OR production_process_information.mtt_count > 0)";
		
		}elseif ($tab_name == 'query') {
			$return_where[3] = "(production_process_information.production_status = 'query' OR production_process_information.query_count > 0)";
		
		}elseif ($tab_name == 'mtt_rfd') {
			$return_where[3] = "(production_process_information.production_status = 'mtt_rfd' OR production_process_information.mtt_rfd_count > 0)";
		
		}
		if(in_array($this->session->userdata('role'), array(5, 8))) {
			$return_where[5] = "quotation_mst.assigned_to = ".$this->session->userdata('user_id');
			if($this->session->userdata('role') == 8) {
				$procurement_user_name = $this->common_model->get_dynamic_data_sales_db('name',array('user_id'=>$this->session->userdata('user_id')), 'users', 'row_array');
				$user_explode = explode(' ',$procurement_user_name['name']);
				$return_where[5] = "production_process_information.handled_by LIKE '%".$user_explode['0']."%'";
			}

		}else if(in_array($this->session->userdata('role'), array(18))) {
			
			$procurement_user_name = $this->common_model->get_dynamic_data_sales_db('name',array('user_id'=>$this->session->userdata('user_id')), 'users', 'row_array');
			$user_explode = explode(' ',$procurement_user_name['name']);
			$return_where[5] = "production_process_information.handled_by LIKE '%".$user_explode['0']."%'";
		}
		// $return_where[] = "quotation_mst.quotation_mst_id = 14948 ";
		// echo "<pre>";print_r($return_where);echo"</pre><hr>";exit;
		return $return_where;
	}
	private function get_total_in_indian_currency($total_details) {

		// getting currency details
		$currency_details = array_column($this->common_model->get_dynamic_data_sales_db('*',array('status'=>'Active'), 'currency'), 'currency_rate', 'currency_id');
		$num = 0;
		foreach ($total_details as $single_details) {
			$num +=  $single_details['total'] * $currency_details[$single_details['currency']];
		}
		return $this->convert_number_into_indian_currency($num);
	}
	private function convert_number_into_indian_currency($num){

		$num = number_format($num,0,'','');
		$new_num = '';
		if(strlen($num) < 4) {
			$new_num = $num;
		}else{
			if(strlen($num) % 2 == 0) {
				for ($i=0; $i < strlen($num)-3; $i++) { 
					
					if(($i % 2) == 0) {

						$new_num .= $num[$i];
						$new_num .= ',';
					}else {

						$new_num .= $num[$i];
					}
				}
				$new_num .= $num[strlen($num)-3];
				$new_num .= $num[strlen($num)-2];
				$new_num .= $num[strlen($num)-1];
			}else {
				for ($i=0; $i < strlen($num)-3; $i++) { 
					
					if(($i % 2) != 0) {

						$new_num .= $num[$i];
						$new_num .= ',';
					}else {

						$new_num .= $num[$i];
					}
				}
				$new_num .= $num[strlen($num)-3];
				$new_num .= $num[strlen($num)-2];
				$new_num .= $num[strlen($num)-1];
			}
		}
		return $new_num;
	}
	private function get_order_wise_total(){

		$pending_order_total = $this->prepare_listing_data(implode(' AND ', $this->default_where_for_all_tab('pending_order')),array(),0,0);

		$semi_ready_total = $this->prepare_listing_data(implode(' AND ', $this->default_where_for_all_tab('semi_ready')),array(),0,0);

		$mtt_total = $this->prepare_listing_data(implode(' AND ', $this->default_where_for_all_tab('mtt')),array(),0,0);

		$query_total = $this->prepare_listing_data(implode(' AND ', $this->default_where_for_all_tab('query')),array(),0,0);

		$mtt_rfd_total = $this->prepare_listing_data(implode(' AND ', $this->default_where_for_all_tab('mtt_rfd')),array(),0,0);

		$ready_to_dispatch_total = $this->prepare_listing_data(implode(' AND ', $this->default_where_for_all_tab('ready_to_dispatch_order')),array(),0,0);

		$return_array[] = array(
							'title_name'=> 'Pending Order',
							'order_type'=> 'pending_order',
							'total' => $this->get_total_in_indian_currency($pending_order_total['total_revenue_list']),
							'currency_wise_total'=> $pending_order_total['total_revenue_list'],
							'total_work_order' => $pending_order_total['paggination_data']['total_rows']
						);
		$return_array[] = array(
							'title_name'=> 'Semi Ready',
							'order_type'=> 'semi_ready',
							'total' => $this->get_total_in_indian_currency($semi_ready_total['total_revenue_list']),
							'currency_wise_total'=> $semi_ready_total['total_revenue_list'],
							'total_work_order' => $semi_ready_total['paggination_data']['total_rows']
						);
		$return_array[] = array(
							'title_name'=> 'MTT',
							'order_type'=> 'mtt',
							'total' => $this->get_total_in_indian_currency($mtt_total['total_revenue_list']),
							'currency_wise_total'=> $mtt_total['total_revenue_list'],
							'total_work_order' => $mtt_total['paggination_data']['total_rows']
						);
		$return_array[] = array(
							'title_name'=> 'Query',
							'order_type'=> 'query',
							'total' => $this->get_total_in_indian_currency($query_total['total_revenue_list']),
							'currency_wise_total'=> $query_total['total_revenue_list'],
							'total_work_order' => $query_total['paggination_data']['total_rows']
						);
		$return_array[] = array(
							'title_name'=> 'MTT/RFD',
							'order_type'=> 'mtt_rfd',
							'total' => $this->get_total_in_indian_currency($mtt_rfd_total['total_revenue_list']),
							'currency_wise_total'=> $mtt_rfd_total['total_revenue_list'],
							'total_work_order' => $mtt_rfd_total['paggination_data']['total_rows']
						);
		$return_array[] = array(
							'title_name'=> 'Ready To Dispatch',
							'order_type'=> 'ready_to_dispatch_order',
							'total' => $this->get_total_in_indian_currency($ready_to_dispatch_total['total_revenue_list']),
							'currency_wise_total'=> $ready_to_dispatch_total['total_revenue_list'],
							'total_work_order' => $ready_to_dispatch_total['paggination_data']['total_rows']
						);
		
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";die('done');				
		return $return_array;
	}	
	private function default_where_for_all_tab_new($tab_name, $type){

		$this->session->set_userdata('production_tab_name',$tab_name);
		$return_where = array(
							0=> "quotation_mst.stage = 'proforma'",
							1=> "quotation_mst.status = 'Won'",
							2=> "quotation_mst.work_order_no != 0",
							3=> "(production_process_information.production_status is NULL OR production_process_information.production_status != 'dispatched' AND production_process_information.production_status != 'on_hold' AND production_process_information.production_status != 'ready_for_dispatch' AND production_process_information.production_status != 'semi_ready' AND
							production_process_information.production_status != 'merchant_trade' AND
							production_process_information.production_status != 'query' AND
							production_process_information.production_status != 'mtt_rfd' AND
							production_process_information.production_status != 'order_cancelled')",
							4=> "(production_process_information.status is NULL or production_process_information.status = 'Active')");
		if($tab_name == 'dispatch_order') {
			$return_where[3] = "(production_process_information.production_status = 'dispatched' OR production_process_information.dispatch_count > 0)";
		
		}elseif ($tab_name == 'on_hold_order') {
			$return_where[3] = "(production_process_information.production_status = 'on_hold' OR production_process_information.on_hold_count > 0)";
		
		}elseif ($tab_name == 'ready_to_dispatch_order') {
			$return_where[3] = "(production_process_information.production_status = 'ready_for_dispatch' OR production_process_information.ready_for_dispatch_count > 0)";
		
		}elseif ($tab_name == 'semi_ready') {
			$return_where[3] = "(production_process_information.production_status = 'semi_ready' OR production_process_information.semi_ready_count > 0)";
		
		}elseif ($tab_name == 'order_cancelled') {
			$return_where[3] = "(production_process_information.production_status = 'order_cancelled' OR production_process_information.cancel_count > 0)";
		
		}elseif ($tab_name == 'mtt') {
			$return_where[3] = "(production_process_information.production_status = 'merchant_trade' OR production_process_information.mtt_count > 0)";
		
		}elseif ($tab_name == 'query') {
			$return_where[3] = "(production_process_information.production_status = 'query' OR production_process_information.query_count > 0)";
		
		}elseif ($tab_name == 'mtt_rfd') {
			$return_where[3] = "(production_process_information.production_status = 'mtt_rfd' OR production_process_information.mtt_rfd_count > 0)";
		
		}
		if(!empty($type)){

			$return_where[] = "production_process_information.type = '{$type}'";
		}
		// echo "<pre>";print_r($return_where);echo"</pre><hr>";exit;
		return $return_where;
	}
	private function get_order_wise_total_new_2($type){

		$return_array = array(

			'pending_order'=> array(
								'title_name'=> 'Pending Order',
								'order_type'=> 'pending_order',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'pending'
							),
			'semi_ready'=> 	array(
								'title_name'=> 'Semi Ready',
								'order_type'=> 'semi_ready',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'semi_ready',
							),
			'mtt'=> 		array(
								'title_name'=> 'ICE',
								'order_type'=> 'mtt',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'mtt',
								
							),
			'query'=> 		array(
								'title_name'=> 'Query',
								'order_type'=> 'query',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'query',
							),
			'mtt_rfd'=> 	array(
								'title_name'=> 'In transit ICE',
								'order_type'=> 'mtt_rfd',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'mtt_rfd',
							),
			'ready_to_dispatch_order'=> array(
								'title_name'=> 'Ready To Dispatch',
								'order_type'=> 'ready_to_dispatch_order',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'rfd',
							)
		);
		$currency_details = array_column($this->common_model->get_dynamic_data_sales_db('*',array('status'=>'Active'), 'currency'), 'currency_rate', 'currency_id');
		$all_data = $this->home_model->get_count_production_status($type);
		foreach ($all_data as $single_data) {		

			$work_order_count = 1;
			if(($single_data['production_status'] == NULL || $single_data['production_status'] == 'yet_to_start_production' || $single_data['production_status'] == 'in_production' || $single_data['production_status'] == 'delay_in_delivery') && $single_data['pending'] > 0){

				$return_array['pending_order']['total'] +=  $single_data['pending'] * $currency_details[$single_data['currency']];
				if(!isset($return_array['pending_order']['currency_wise_total'][$single_data['currency']])){

					$return_array['pending_order']['currency_wise_total'][$single_data['currency']] = array(
						'currency'=> $single_data['currency'],
						'total'=> 0
					);
				}
				$return_array['pending_order']['currency_wise_total'][$single_data['currency']]['total'] +=  $single_data['pending'];
				$return_array['pending_order']['total_work_order'] +=  $work_order_count;
				$work_order_count = 0;
			}
			if(($single_data['production_status'] == 'semi_ready' || $single_data['semi_ready_count'] > 0) && $single_data['semi_ready'] > 0){

				$return_array['semi_ready']['total'] +=  $single_data['pending'] * $currency_details[$single_data['currency']];
				if(!isset($return_array['semi_ready']['currency_wise_total'][$single_data['currency']])){

					$return_array['semi_ready']['currency_wise_total'][$single_data['currency']] = array(
						'currency'=> $single_data['currency'],
						'total'=> 0
					);
				}
				$return_array['semi_ready']['currency_wise_total'][$single_data['currency']]['total'] +=  $single_data['pending'];
				$return_array['semi_ready']['total_work_order'] +=  $work_order_count;
				$work_order_count = 0;
			}
			if(($single_data['production_status'] == 'merchant_trade' || $single_data['mtt_count'] > 0) && $single_data['mtt'] > 0){

				$return_array['mtt']['total'] +=  $single_data['pending'] * $currency_details[$single_data['currency']];
				if(!isset($return_array['mtt']['currency_wise_total'][$single_data['currency']])){

					$return_array['mtt']['currency_wise_total'][$single_data['currency']] = array(
						'currency'=> $single_data['currency'],
						'total'=> 0
					);
				}
				$return_array['mtt']['currency_wise_total'][$single_data['currency']]['total'] +=  $single_data['pending'];
				$return_array['mtt']['total_work_order'] +=  $work_order_count;
				$work_order_count = 0;
			}
			if(($single_data['production_status'] == 'query' || $single_data['query_count'] > 0) && $single_data['query'] > 0){

				$return_array['query']['total'] +=  $single_data['pending'] * $currency_details[$single_data['currency']];
				if(!isset($return_array['query']['currency_wise_total'][$single_data['currency']])){

					$return_array['query']['currency_wise_total'][$single_data['currency']] = array(
						'currency'=> $single_data['currency'],
						'total'=> 0
					);
				}
				$return_array['query']['currency_wise_total'][$single_data['currency']]['total'] +=  $single_data['pending'];
				$return_array['query']['total_work_order'] +=  $work_order_count;
				$work_order_count = 0;
			}
			if(($single_data['production_status'] == 'mtt_rfd' || $single_data['mtt_rfd_count'] > 0) && $single_data['mtt_rfd'] > 0){

				$return_array['mtt_rfd']['total'] +=  $single_data['pending'] * $currency_details[$single_data['currency']];
				if(!isset($return_array['mtt_rfd']['currency_wise_total'][$single_data['currency']])){

					$return_array['mtt_rfd']['currency_wise_total'][$single_data['currency']] = array(
						'currency'=> $single_data['currency'],
						'total'=> 0
					);
				}
				$return_array['mtt_rfd']['currency_wise_total'][$single_data['currency']]['total'] +=  $single_data['pending'];
				$return_array['mtt_rfd']['total_work_order'] +=  $work_order_count;
				$work_order_count = 0;
			}
			if(($single_data['production_status'] == 'ready_for_dispatch' || $single_data['ready_for_dispatch_count'] > 0) && $single_data['pending'] > 0){

				$return_array['ready_to_dispatch_order']['total'] +=  $single_data['pending'] * $currency_details[$single_data['currency']];
				if(!isset($return_array['ready_to_dispatch_order']['currency_wise_total'][$single_data['currency']])){

					$return_array['ready_to_dispatch_order']['currency_wise_total'][$single_data['currency']] = array(
						'currency'=> $single_data['currency'],
						'total'=> 0
					);
				}
				$return_array['ready_to_dispatch_order']['currency_wise_total'][$single_data['currency']]['total'] +=  $single_data['pending'];
				$return_array['ready_to_dispatch_order']['total_work_order'] +=  $work_order_count;
				$work_order_count = 0;
			}
		}
		foreach ($return_array as $key => $value) {
			
			$return_array[$key]['total'] = $this->convert_number_into_indian_currency($return_array[$key]['total']);
		}					
		return $return_array;
	}
	private function get_order_wise_total_new($type){

		$return_array = array(
			'pending_order'=> array(
								'title_name'=> 'Pending Order',
								'order_type'=> 'pending_order',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'pending'
							),
			'semi_ready'=> 	array(
								'title_name'=> 'Semi Ready',
								'order_type'=> 'semi_ready',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'semi_ready',
							),
			'mtt'=> 		array(
								'title_name'=> 'ICE',
								'order_type'=> 'mtt',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'mtt',
								
							),
			'import'=> 		array(
								'title_name'=> 'Import',
								'order_type'=> 'import',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'import',
							),
			'query'=> 		array(
								'title_name'=> 'Query',
								'order_type'=> 'query',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'query',
							),
			'mtt_rfd'=> 	array(
								'title_name'=> 'In transit ICE',
								'order_type'=> 'mtt_rfd',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'mtt_rfd',
							),
			'ready_to_dispatch_order'=> array(
								'title_name'=> 'Ready To Dispatch',
								'order_type'=> 'ready_to_dispatch_order',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'rfd',
			)
		);
		$currency_details = array_column($this->common_model->get_dynamic_data_sales_db('*',array('status'=>'Active'), 'currency'), 'currency_rate', 'currency_id');
		foreach (array(
					array(
						'type'=> 'pending',
						'where_string'=> " AND (production_process_information.production_status = 'yet_to_start_production' OR production_process_information.production_status = 'in_production' OR production_process_information.production_status = 'pending_order' OR production_process_information.production_status = 'yet_to_start_production' OR (production_process_information.production_status = 'semi_ready' OR quotation_dtl.semi_ready_count > 0) OR (production_process_information.production_status IN ('merchant_trade', 'mtt') OR quotation_dtl.mtt_count > 0) OR (production_process_information.production_status = 'import' OR quotation_dtl.import_count > 0) OR (production_process_information.production_status = 'query' OR quotation_dtl.query_count > 0) OR (production_process_information.production_status IN ('mtt_rfd', 'in_transit_import', 'in_transit_mtt') OR quotation_dtl.mtt_rfd_count > 0) OR (production_process_information.production_status = 'ready_for_dispatch' OR quotation_dtl.ready_to_dispatch_order_count > 0))"
					),
					array(
						'type'=> 'semi_ready',
						'where_string'=> " AND (production_process_information.production_status = 'semi_ready' OR quotation_dtl.semi_ready_count > 0)"
					),
					array(
						'type'=> 'mtt',
						'where_string'=> " AND (production_process_information.production_status IN ('merchant_trade', 'mtt') OR quotation_dtl.mtt_count > 0)"
					),
					array(
						'type'=> 'import',
						'where_string'=> " AND (production_process_information.production_status = 'import' OR quotation_dtl.import_count > 0)"
					),
					array(
						'type'=> 'query',
						'where_string'=> " AND (production_process_information.production_status = 'query' OR quotation_dtl.query_count > 0)"
					),
					array(
						'type'=> 'mtt_rfd',
						'where_string'=> " AND (production_process_information.production_status IN ('mtt_rfd', 'in_transit_import', 'in_transit_mtt') OR quotation_dtl.mtt_rfd_count > 0)"
					),
					array(
						'type'=> 'ready_to_dispatch_order',
						'where_string'=> " AND (production_process_information.production_status = 'ready_for_dispatch' OR quotation_dtl.ready_to_dispatch_order_count > 0)"
					),
				) as $single_details) {
			
			$all_data = $this->home_model->get_count_production_status($type, $single_details['where_string']);
			// echo "<pre>";print_r($all_data);echo"</pre><hr>";	

			foreach ($all_data as $single_data) {		

				$total_status_wise = $this->create_total_status_wise($single_data);
				if($single_details['type'] == 'pending' && $total_status_wise['pending_order']['value'] > 0){

					if(!isset($return_array['pending_order']['currency_wise_total'][$single_data['currency']])){

						$return_array['pending_order']['currency_wise_total'][$single_data['currency']] = array(
							'currency'=> $single_data['currency'],
							'total'=> 0
						);
					}
					$return_array['pending_order']['currency_wise_total'][$single_data['currency']]['total'] += $total_status_wise['pending_order']['value'];
					if($this->check_work_order_increase_or_not($single_details['type'], $single_data)){

						$return_array['pending_order']['total_work_order']++;
					}
				}
				if($single_details['type'] == 'semi_ready' && $total_status_wise['semi_ready']['value'] > 0){

					if(!isset($return_array['semi_ready']['currency_wise_total'][$single_data['currency']])){

						$return_array['semi_ready']['currency_wise_total'][$single_data['currency']] = array(
							'currency'=> $single_data['currency'],
							'total'=> 0
						);
					}
					$return_array['semi_ready']['currency_wise_total'][$single_data['currency']]['total'] += $total_status_wise['semi_ready']['value'];
					if($this->check_work_order_increase_or_not($single_details['type'], $single_data)){

						$return_array['semi_ready']['total_work_order']++;
					}
				}
				if($single_details['type'] == 'mtt' && $total_status_wise['mtt']['value'] > 0){

					if(!isset($return_array['mtt']['currency_wise_total'][$single_data['currency']])){

						$return_array['mtt']['currency_wise_total'][$single_data['currency']] = array(
							'currency'=> $single_data['currency'],
							'total'=> 0
						);
					}
					$return_array['mtt']['currency_wise_total'][$single_data['currency']]['total'] += $total_status_wise['mtt']['value'];
					if($this->check_work_order_increase_or_not($single_details['type'], $single_data)){

						$return_array['mtt']['total_work_order']++;
					}
				}
				if($single_details['type'] == 'import' && $total_status_wise['import']['value'] > 0){

					if(!isset($return_array['import']['currency_wise_total'][$single_data['currency']])){

						$return_array['import']['currency_wise_total'][$single_data['currency']] = array(
							'currency'=> $single_data['currency'],
							'total'=> 0
						);
					}
					$return_array['import']['currency_wise_total'][$single_data['currency']]['total'] += $total_status_wise['import']['value'];
					if($this->check_work_order_increase_or_not($single_details['type'], $single_data)){

						$return_array['import']['total_work_order']++;
					}
				}
				if($single_details['type'] == 'query' && $total_status_wise['query']['value'] > 0){


					if(!isset($return_array['query']['currency_wise_total'][$single_data['currency']])){

						$return_array['query']['currency_wise_total'][$single_data['currency']] = array(
							'currency'=> $single_data['currency'],
							'total'=> 0
						);
					}
					$return_array['query']['currency_wise_total'][$single_data['currency']]['total'] += $total_status_wise['query']['value'];
					if($this->check_work_order_increase_or_not($single_details['type'], $single_data)){

						$return_array['query']['total_work_order']++;
					}
				}
				if($single_details['type'] == 'mtt_rfd' && $total_status_wise['mtt_rfd']['value'] > 0){

					if(!isset($return_array['mtt_rfd']['currency_wise_total'][$single_data['currency']])){

						$return_array['mtt_rfd']['currency_wise_total'][$single_data['currency']] = array(
							'currency'=> $single_data['currency'],
							'total'=> 0
						);
					}
					$return_array['mtt_rfd']['currency_wise_total'][$single_data['currency']]['total'] += $total_status_wise['mtt_rfd']['value'];
					if($this->check_work_order_increase_or_not($single_details['type'], $single_data)){

						$return_array['mtt_rfd']['total_work_order']++;
					}
				}
				if($single_details['type'] == 'ready_to_dispatch_order' && $total_status_wise['ready_to_dispatch_order']['value'] > 0){

					if(!isset($return_array['ready_to_dispatch_order']['currency_wise_total'][$single_data['currency']])){

						$return_array['ready_to_dispatch_order']['currency_wise_total'][$single_data['currency']] = array(
							'currency'=> $single_data['currency'],
							'total'=> 0
						);
					}
					$return_array['ready_to_dispatch_order']['currency_wise_total'][$single_data['currency']]['total'] += $total_status_wise['ready_to_dispatch_order']['value'];
					if($this->check_work_order_increase_or_not($single_details['type'], $single_data)){

						$return_array['ready_to_dispatch_order']['total_work_order']++;
					}
				}
			}
			// echo "<pre>";print_r($return_array);echo"</pre><hr>";die('done');
		}
		
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";die('done');	
		$total_of_3_order = 0;
		$total_of_3_order_number = 0;	
		foreach ($return_array as $status_name => $status_details) {
			
			foreach ($status_details['currency_wise_total'] as $currency_key => $currency_wise_details) {

				$return_array[$status_name]['total'] += $currency_wise_details['total'] *$currency_details[$currency_wise_details['currency']];
				$total_of_3_order += $currency_wise_details['total'] *$currency_details[$currency_wise_details['currency']];
				
				$return_array[$status_name]['currency_wise_total'][$currency_key]['total'] = $this->convert_number_into_indian_currency($return_array[$status_name]['currency_wise_total'][$currency_key]['total']);
			}
			$total_of_3_order_number += (int)$status_details['total_work_order'];
			$return_array[$status_name]['total'] = $this->convert_number_into_indian_currency($return_array[$status_name]['total']);
		}
		$total_of_3_order = $this->convert_number_into_indian_currency($total_of_3_order);
		
		return array('order_status_wise_total'=> $return_array, 'total_of_3_order'=> $total_of_3_order, 'total_of_3_order_number'=> $total_of_3_order_number);
	}
	private function get_order_wise_total_new_bckp($type){

		$return_array = array(
			'pending_order'=> array(
								'title_name'=> 'Pending Order',
								'order_type'=> 'pending_order',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'pending'
							),
			'semi_ready'=> 	array(
								'title_name'=> 'Semi Ready',
								'order_type'=> 'semi_ready',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'semi_ready',
							),
			'mtt'=> 		array(
								'title_name'=> 'ICE',
								'order_type'=> 'mtt',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'mtt',
								
							),
			'query'=> 		array(
								'title_name'=> 'Query',
								'order_type'=> 'query',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'query',
							),
			'mtt_rfd'=> 	array(
								'title_name'=> 'In transit ICE',
								'order_type'=> 'mtt_rfd',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'mtt_rfd',
							),
			'ready_to_dispatch_order'=> array(
								'title_name'=> 'Ready To Dispatch',
								'order_type'=> 'ready_to_dispatch_order',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'rfd',
			)
		);
		$currency_details = array_column($this->common_model->get_dynamic_data_sales_db('*',array('status'=>'Active'), 'currency'), 'currency_rate', 'currency_id');
		foreach (array(
					array(
						'type'=> 'semi_ready',
						'where_string'=> " AND (production_process_information.production_status = 'semi_ready' OR production_process_information.semi_ready_count > 0)"
					),
					array(
						'type'=> 'mtt',
						'where_string'=> " AND (production_process_information.production_status = 'merchant_trade' OR production_process_information.mtt_count > 0)"
					),
					array(
						'type'=> 'query',
						'where_string'=> " AND (production_process_information.production_status = 'query' OR production_process_information.query_count > 0)"
					),
					array(
						'type'=> 'mtt_rfd',
						'where_string'=> " AND (production_process_information.production_status = 'mtt_rfd' OR production_process_information.mtt_rfd_count > 0)"
					),
					array(
						'type'=> 'ready_to_dispatch_order',
						'where_string'=> " AND (production_process_information.production_status = 'ready_for_dispatch' OR production_process_information.ready_for_dispatch_count > 0)"
					),
				) as $single_details) {
			
			$all_data = $this->home_model->get_count_production_status($type, $single_details['where_string']);
			// echo "<pre>";print_r($all_data);echo"</pre><hr>";die;	

			foreach ($all_data as $single_data) {		

				$total_status_wise = $this->create_total_status_wise($single_data);
				if($total_status_wise['pending_order']['value'] > 0){

					if(!isset($return_array['pending_order']['currency_wise_total'][$single_data['currency']])){

						$return_array['pending_order']['currency_wise_total'][$single_data['currency']] = array(
							'currency'=> $single_data['currency'],
							'total'=> 0
						);
					}
					$return_array['pending_order']['currency_wise_total'][$single_data['currency']]['total'] += $total_status_wise['pending_order']['value'];
					$return_array['pending_order']['total_work_order']++;
				}
				if($single_details['type'] == 'semi_ready' && $total_status_wise['semi_ready']['value'] > 0){

					if(!isset($return_array['semi_ready']['currency_wise_total'][$single_data['currency']])){

						$return_array['semi_ready']['currency_wise_total'][$single_data['currency']] = array(
							'currency'=> $single_data['currency'],
							'total'=> 0
						);
					}
					$return_array['semi_ready']['currency_wise_total'][$single_data['currency']]['total'] += $total_status_wise['semi_ready']['value'];
					$return_array['semi_ready']['total_work_order']++;
				}
				if($single_details['type'] == 'mtt' && $total_status_wise['mtt']['value'] > 0){

					if(!isset($return_array['mtt']['currency_wise_total'][$single_data['currency']])){

						$return_array['mtt']['currency_wise_total'][$single_data['currency']] = array(
							'currency'=> $single_data['currency'],
							'total'=> 0
						);
					}
					$return_array['mtt']['currency_wise_total'][$single_data['currency']]['total'] += $total_status_wise['mtt']['value'];
					$return_array['mtt']['total_work_order']++;
				}
				if($single_details['type'] == 'query' && $total_status_wise['query']['value'] > 0){

					if(!isset($return_array['query']['currency_wise_total'][$single_data['currency']])){

						$return_array['query']['currency_wise_total'][$single_data['currency']] = array(
							'currency'=> $single_data['currency'],
							'total'=> 0
						);
					}
					$return_array['query']['currency_wise_total'][$single_data['currency']]['total'] += $total_status_wise['query']['value'];
					$return_array['query']['total_work_order']++;
				}
				if($single_details['type'] == 'mtt_rfd' && $total_status_wise['mtt_rfd']['value'] > 0){

					if(!isset($return_array['mtt_rfd']['currency_wise_total'][$single_data['currency']])){

						$return_array['mtt_rfd']['currency_wise_total'][$single_data['currency']] = array(
							'currency'=> $single_data['currency'],
							'total'=> 0
						);
					}
					$return_array['mtt_rfd']['currency_wise_total'][$single_data['currency']]['total'] += $total_status_wise['mtt_rfd']['value'];
					$return_array['mtt_rfd']['total_work_order']++;
				}
				if($single_details['type'] == 'ready_to_dispatch_order' && $total_status_wise['ready_to_dispatch_order']['value'] > 0){

					if(!isset($return_array['ready_to_dispatch_order']['currency_wise_total'][$single_data['currency']])){

						$return_array['ready_to_dispatch_order']['currency_wise_total'][$single_data['currency']] = array(
							'currency'=> $single_data['currency'],
							'total'=> 0
						);
					}
					$return_array['ready_to_dispatch_order']['currency_wise_total'][$single_data['currency']]['total'] += $total_status_wise['ready_to_dispatch_order']['value'];
					$return_array['ready_to_dispatch_order']['total_work_order']++;
				}
			}
		}
		
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";die('done');	
		$total_of_3_order = 0;
		$total_of_3_order_number = 0;	
		foreach ($return_array as $status_name => $status_details) {
			
			foreach ($status_details['currency_wise_total'] as $currency_key => $currency_wise_details) {

				$return_array[$status_name]['total'] += $currency_wise_details['total'] *$currency_details[$currency_wise_details['currency']];
				$total_of_3_order += $currency_wise_details['total'] *$currency_details[$currency_wise_details['currency']];
				
				$return_array[$status_name]['currency_wise_total'][$currency_key]['total'] = $this->convert_number_into_indian_currency($return_array[$status_name]['currency_wise_total'][$currency_key]['total']);
			}
			$total_of_3_order_number += (int)$status_details['total_work_order'];
			$return_array[$status_name]['total'] = $this->convert_number_into_indian_currency($return_array[$status_name]['total']);
		}
		$total_of_3_order = $this->convert_number_into_indian_currency($total_of_3_order);
		
		return array('order_status_wise_total'=> $return_array, 'total_of_3_order'=> $total_of_3_order, 'total_of_3_order_number'=> $total_of_3_order_number);
	}
	private function create_total_status_wise($product_details){

		$return_array = array(
			'pending_order' => array(
				'name'=> 'Pending',
				'value'=> 0
			),
			'semi_ready' => array(
				'name'=> 'Semi Ready',
				'value'=> 0
			),
			'mtt' => array(
				'name'=> 'Import cum Export(ICE)',
				'value'=> 0,
			),
			'import' => array(
				'name'=> 'Import',
				'value'=> 0,
			),
			'query' => array(
				'name'=> 'Query',
				'value'=> 0,
			),
			'mtt_rfd' => array(
				'name'=> 'In transit ICE',
				'value'=> 0,
			),
			'ready_to_dispatch_order' => array(
				'name'=> 'RFD',
				'value'=> 0,
			),
			'dispatch_order' => array(
				'name'=> 'Dispatch',
				'value'=> 0,
			),
			'on_hold_order' => array(
				'name'=> 'Hold',
				'value'=> 0,
			),
			'order_cancelled' => array(
				'name'=> 'Cancelled',
				'value'=> 0,
			),
		);
		// echo "<pre>";print_r($product_details);echo"</pre><hr>";exit;
	
		$return_array['pending_order']['value'] += $product_details['pending_count'];
		$return_array['semi_ready']['value'] += $product_details['semi_ready_count'];
		$return_array['ready_to_dispatch_order']['value'] += $product_details['rfd_count'];
		$return_array['dispatch_order']['value'] += $product_details['dispatch_count'];
		$return_array['on_hold_order']['value'] += $product_details['hold_count'];
		$return_array['mtt']['value'] += $product_details['mtt_count'];
		$return_array['import']['value'] += $product_details['import_count'];
		$return_array['query']['value'] += $product_details['query_count'];
		$return_array['mtt_rfd']['value'] += $product_details['mtt_rfd_count'];
		$return_array['order_cancelled']['value'] += $product_details['cancel_count'];

		return $return_array;
	}
	private function get_order_wise_total_new_old($type){

		$pending_order_total = $this->prepare_listing_data_new(implode(' AND ', $this->default_where_for_all_tab_new('pending_order', $type)),array(),0,0);
		// echo "<pre>";print_r($pending_order_total);echo"</pre><hr>";die('done');
		$semi_ready_total = $this->prepare_listing_data_new(implode(' AND ', $this->default_where_for_all_tab_new('semi_ready', $type)),array(),0,0);
		
		$mtt_total = $this->prepare_listing_data_new(implode(' AND ', $this->default_where_for_all_tab_new('mtt', $type)),array(),0,0);
		// echo "<pre>";print_r($mtt_total);echo"</pre><hr>";die('done');
		$query_total = $this->prepare_listing_data_new(implode(' AND ', $this->default_where_for_all_tab_new('query', $type)),array(),0,0);

		$mtt_rfd_total = $this->prepare_listing_data_new(implode(' AND ', $this->default_where_for_all_tab_new('mtt_rfd', $type)),array(),0,0);

		$ready_to_dispatch_total = $this->prepare_listing_data_new(implode(' AND ', $this->default_where_for_all_tab_new('ready_to_dispatch_order', $type)),array(),0,0);
		
		$return_array[] = array(
							'title_name'=> 'Pending Order',
							'order_type'=> 'pending_order',
							'total' => $this->get_total_in_indian_currency($pending_order_total['total_revenue_list']),
							'currency_wise_total'=> $pending_order_total['total_revenue_list'],
							'total_work_order' => $pending_order_total['paggination_data']['total_rows']
						);
		$return_array[] = array(
							'title_name'=> 'Semi Ready',
							'order_type'=> 'semi_ready',
							'total' => $this->get_total_in_indian_currency($semi_ready_total['total_revenue_list']),
							'currency_wise_total'=> $semi_ready_total['total_revenue_list'],
							'total_work_order' => $semi_ready_total['paggination_data']['total_rows']
						);
		$return_array[] = array(
							'title_name'=> 'ICE',
							'order_type'=> 'mtt',
							'total' => $this->get_total_in_indian_currency($mtt_total['total_revenue_list']),
							'currency_wise_total'=> $mtt_total['total_revenue_list'],
							'total_work_order' => $mtt_total['paggination_data']['total_rows']
						);
		$return_array[] = array(
							'title_name'=> 'Query',
							'order_type'=> 'query',
							'total' => $this->get_total_in_indian_currency($query_total['total_revenue_list']),
							'currency_wise_total'=> $query_total['total_revenue_list'],
							'total_work_order' => $query_total['paggination_data']['total_rows']
						);
		$return_array[] = array(
							'title_name'=> 'In transit ICE',
							'order_type'=> 'mtt_rfd',
							'total' => $this->get_total_in_indian_currency($mtt_rfd_total['total_revenue_list']),
							'currency_wise_total'=> $mtt_rfd_total['total_revenue_list'],
							'total_work_order' => $mtt_rfd_total['paggination_data']['total_rows']
						);
		$return_array[] = array(
							'title_name'=> 'Ready To Dispatch',
							'order_type'=> 'ready_to_dispatch_order',
							'total' => $this->get_total_in_indian_currency($ready_to_dispatch_total['total_revenue_list']),
							'currency_wise_total'=> $ready_to_dispatch_total['total_revenue_list'],
							'total_work_order' => $ready_to_dispatch_total['paggination_data']['total_rows']
						);
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";die('done');					
		return $return_array;
	}
	//production module code end
	private function create_select_drop_down_data($master_array, $master_array_key_name, $condition_value, $default_value = 'selected'){

		if(! is_array($condition_value)){
			$condition_value = array($condition_value);
		}
		$return_array = array();
		foreach ($master_array as $master_key => $master_details) {
				
			$return_array[$master_key] = $master_details;
			$return_array[$master_key]['selected'] = '';
			if(!empty($condition_value) && in_array($master_details[$master_array_key_name], $condition_value)){

				$return_array[$master_key]['selected'] = $default_value;
			}
		}
		return $return_array;
	}
	private function get_indian_time($date_format = 'Y-m-d H:i:s'){

		date_default_timezone_set('Asia/Kolkata');
		return date($date_format);
	}
	private function check_work_order_increase_or_not($type, $data){

		switch ($type) {
			case 'pending':
				
				if( $data['pending_count'] > ($data['semi_ready_count'] + $data['rfd_count'] + $data['dispatch_count'] + $data['hold_count'] + $data['mtt_count'] + $data['import_count'] + $data['query_count'] + $data['mtt_rfd_count'] + $data['cancel_count']) ){

					return true;
				}
			break;
			case 'semi_ready':
				
				if( $data['semi_ready_count'] > ($data['pending_count'] + $data['rfd_count'] + $data['dispatch_count'] + $data['hold_count'] + $data['mtt_count'] + $data['import_count'] + $data['query_count'] + $data['mtt_rfd_count'] + $data['cancel_count']) ){

					return true;
				}
			break;
			case 'mtt':
				
				if( $data['mtt_count'] > ($data['pending_count'] + $data['rfd_count'] + $data['dispatch_count'] + $data['hold_count'] + $data['semi_ready_count'] + $data['import_count'] + $data['query_count'] + $data['mtt_rfd_count'] + $data['cancel_count']) ){

					return true;
				}
			break;
			case 'import':

				if( $data['import_count'] > ($data['pending_count'] + $data['rfd_count'] + $data['dispatch_count'] + $data['hold_count'] + $data['semi_ready_count'] + $data['mtt_count'] + $data['query_count'] + $data['mtt_rfd_count'] + $data['cancel_count']) ){

					return true;
				}
			break;
			case 'query':
				
				if( $data['query_count'] > ($data['pending_count'] + $data['rfd_count'] + $data['dispatch_count'] + $data['hold_count'] + $data['mtt_count'] + $data['import_count'] + $data['semi_ready_count'] + $data['mtt_rfd_count'] + $data['cancel_count']) ){

					return true;
				}
			break;
			case 'mtt_rfd':
				
				if( $data['mtt_rfd_count'] > ($data['pending_count'] + $data['rfd_count'] + $data['dispatch_count'] + $data['hold_count'] + $data['mtt_count'] + $data['import_count'] + $data['query_count'] + $data['semi_ready_count'] + $data['cancel_count']) ){

					return true;
				}
			break;
			case 'ready_to_dispatch_order':
				
				if( $data['rfd_count'] > ($data['pending_count'] + $data['semi_ready_count'] + $data['dispatch_count'] + $data['hold_count'] + $data['mtt_count'] + $data['import_count'] + $data['query_count'] + $data['mtt_rfd_count'] + $data['cancel_count']) ){

					return true;
				}
			break;
		}
		return false;
	}

	// private function selected_sales_person($selected_sales_person) {
	// 	if ($selected_sales_person != 'All') {
	// 		return 	$this->db->where('assigned_to', $selected_sales_person);
	// 	}
	// 	return false;
	// }
}
?>