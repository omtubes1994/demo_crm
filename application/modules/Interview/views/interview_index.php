<!DOCTYPE html>

<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 8
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">

	<!-- begin::Head -->
	<head>
		<base href="../../../">
		<meta charset="utf-8" />
		<title>Metronic | List Default</title>
		<meta name="description" content="User default listing">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!--begin::Fonts -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

		<!--end::Fonts -->

		<!--begin::Global Theme Styles(used by all pages) -->
		<link href="<?php echo base_url('/assets/plugins/global/plugins.bundle.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url('/assets/css/style.bundle.css'); ?>" rel="stylesheet" type="text/css" />

		<!--end::Global Theme Styles -->

		<!--begin::Layout Skins(used by all pages) -->
		<link href="<?php echo base_url('/assets/css/skins/header/base/light.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url('/assets/css/skins/header/menu/light.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url('/assets/css/skins/brand/dark.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url('/assets/css/skins/aside/dark.css'); ?>" rel="stylesheet" type="text/css" />

		<!--end::Layout Skins -->
		<link rel="shortcut icon" href="<?php echo base_url('/assets/media/logos/favicon.ico'); ?>" />
        
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

		<!-- begin:: Page -->

		<div class="kt-grid kt-grid--hor kt-grid--root">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

				<!-- end:: Aside -->
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper" style="padding-left: 0px;">

					<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

						<!-- begin:: Content Head -->
						<div class="kt-subheader   kt-grid__item" id="kt_subheader" style="top: 25px; left: 25px; right: 25px; height:auto;">
							<div class="kt-container  kt-container--fluid ">
                                <h3 class="kt-subheader__title">
                                    <img class="kt-hidden-" alt="Pic" src="https://www.omtubes.com/wp-content/uploads/2023/04/LOGO-GPW.png" />
                                    <img class="kt-hidden-" alt="Pic" src="https://www.omtubes.com/wp-content/uploads/2020/01/OM-Logo-for-website.png" width="200" />
                                    &nbsp&nbsp&nbsp&nbsp
                                    Om Tubes Interview Test
                                </h3>
							</div>
						</div>

						<!-- end:: Content Head -->
                        <?php if(!empty($questions)){?>
						<!-- begin:: Content -->
						<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                            <form class="kt-form kt-form--label-right" id="interview_form" style="padding-top: 25px;">
                                <?php foreach ($questions as $single_question) {?>
                                <!--begin:: Portlet-->
                                <div class="kt-portlet">
                                    <div class="kt-portlet__body">
                                        <div class="kt-widget kt-widget--user-profile-3">
                                            <div class="kt-widget__top">
                                                <div class="kt-widget__content">
                                                    <div class="kt-widget__head">
                                                        <a href="#" class="kt-widget__username">
                                                            1. <?php echo $single_question['question_name']; ?>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="kt-widget__bottom" style="margin-top:1rem; display:block;">
                                                <div class="row" style="padding: 2rem 1.5rem;">
                                                    <?php foreach ($single_question['options'] as $single_option) {?>
                                                    <div class="col-xl-6">
                                                        <span class="kt-widget__value" style="color: #48465b; font-weight: 600; font-size: 1.2rem;">
                                                            <input 
                                                            type="radio" 
                                                            name="question_<?php echo $single_option['interview_question_id']; ?>"
                                                            value="<?php echo $single_option['id']; ?>"> <?php echo $single_option['option_text']; ?>
                                                        </span>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--end:: Portlet-->
                                <?php } ?>
                                <!--begin:: Portlet-->
                                <div class="kt-portlet">
                                    <div class="kt-portlet__body">
                                        <div class="kt-widget kt-widget--user-profile-3">
                                            <div class="kt-widget__top">
                                                <div class="kt-widget__content">
                                                    <div class="kt-widget__head">
                                                        <a href="javascript:void(0);" class="btn btn-label-brand btn-bold save_interview_details" user_id ="<?php echo $user_id; ?>">Save Test </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--end:: Portlet-->
                            </form>
						</div>

						<!-- end:: Content -->
                        <?php } ?>
					</div>
				</div>
			</div>
		</div>

		<!-- end:: Page -->

		<!-- begin::Scrolltop -->
		<div id="kt_scrolltop" class="kt-scrolltop">
			<i class="fa fa-arrow-up"></i>
		</div>

		<!-- end::Scrolltop -->

		<!-- begin::Global Config(global config for global JS sciprts) -->
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": [
							"#c5cbe3",
							"#a1a8c3",
							"#3d4465",
							"#3e4466"
						],
						"shape": [
							"#f0f3ff",
							"#d9dffa",
							"#afb4d4",
							"#646c9a"
						]
					}
				}
			};
		</script>
        
		<!-- end::Global Config -->

		<!--begin::Global Theme Bundle(used by all pages) -->
		<script src="<?php echo base_url('/assets/plugins/global/plugins.bundle.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo base_url('/assets/js/scripts.bundle.js'); ?>" type="text/javascript"></script>
            
		<!--end::Global Theme Bundle -->
        <script>
            $(document).ready(function(){

                $('a.save_interview_details').click(function(){

                    $.ajax({
                        type: 'POST',
                        data: {call_type: 'save_interview_details', form_data: $('form#interview_form').serializeArray(), user_id: $(this).attr('user_id')},
                        url: '<?php echo base_url('Interview/ajax_function'); ?>',
                        dataType: 'JSON',
                        success: function (res) {
                        },
                        beforeSend: function (response) {

                        }
                    });
                });
            });
        </script>  
	</body>

	<!-- end::Body -->
</html>