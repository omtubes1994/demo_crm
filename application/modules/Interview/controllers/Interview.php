<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Interview extends MX_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('common_model');
		// ini_set('max_execution_time', 1200);
		ini_set('memory_limit', -1);
		error_reporting(E_ALL);
	}

	public function index(){

		$id = $this->uri->segment(3, 0);
		if(!empty($id)){
			
			$data = array();

			$id = base64_decode($id);
			$user_info = $this->common_model->get_dynamic_data_sales_db('*', array('status'=> 'Active', 'id'=> $id), 'interview_user', 'row_array');
			// echo "<pre>";print_r($user_info);echo"</pre><hr>";exit;
			if(!empty($user_info)){

				$question_list = $this->common_model->get_dynamic_data_sales_db('*', array('status'=> 'Active', 'department'=> $user_info['department'], 'experience_level'=> $user_info['experience_level']), 'interview_question', 'result_array');
				// echo "<pre>";print_r($question_list);echo"</pre><hr>";exit;
				foreach ($question_list as $single_question) {
					
					$data['questions'][] = array(
						'question_name'=> $single_question['question_text'],
						'options' => $this->common_model->get_dynamic_data_sales_db('*', array('status'=> 'Active', 'interview_question_id'=> $single_question['id']), 'interview_option')
					);
				}
			}
			$data['user_id'] = $id;
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
			$this->load->view('Interview/interview_index', $data);
		}
	}
	public function result(){

		$data = array();
		$interview_info = $this->common_model->get_dynamic_data_sales_db('*', array('status'=> 'Active'), 'interview_answer');
		// echo "<pre>";print_r($interview_info);echo"</pre><hr>";

		$question_with_right_answer_id = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status'=> 'Active'), 'interview_question'), 'correct_option_id', 'id');
		$data['user_list'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status'=> 'Active'), 'interview_user'), 'user_name', 'id');
		// echo "<pre>";print_r($question_with_right_answer_id);echo"</pre><hr>";
		$data['user_result_list'] = array();
		foreach ($interview_info as $single_interview_info) {
			
			if(!isset($data['user_result_list'][$single_interview_info['interview_user_id']])){

				$data['user_result_list'][$single_interview_info['interview_user_id']]['total_answer'] = 0;
				$data['user_result_list'][$single_interview_info['interview_user_id']]['correct_answer'] = 0;
			}
			$data['user_result_list'][$single_interview_info['interview_user_id']]['total_answer'] += 1;
			if($single_interview_info['answer_option_id'] == $question_with_right_answer_id[$single_interview_info['interview_question_id']]){

				$data['user_result_list'][$single_interview_info['interview_user_id']]['correct_answer'] += 1; 
			}
		}

		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('Interview/interview_result', $data);
	}
	public function ajax_function() {

		if($this->input->is_ajax_request()){
			$post_details = $this->input->post();
			$response['status'] = 'successful';	
			switch ($post_details['call_type']) {

				case 'save_interview_details':
					
					$insert_array = array();
					// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;
					if(!empty($post_details['form_data'])){

						foreach ($post_details['form_data'] as $single_form_data) {
							
							// echo "<pre>";print_r($single_form_data['name']);echo"</pre><hr>";exit;
							
							$explode_name = explode("_", $single_form_data['name']);
							// echo "<pre>";print_r($explode_name);echo"</pre><hr>";exit;

							if(!empty($explode_name)){

								// $insert_array = array(
								// 	'interview_user_id' => $post_details['user_id'],
								// 	'interview_question_id' => $explode_name[1],
								// 	'answer_option_id' => $single_form_data['value'],
								// );
								// $this->common_model->db->insert('interview_answer', $insert_array);
								// $this->common_model->db->on_duplicate('interview_answer', $insert_array);
								$sql = "INSERT INTO interview_answer (interview_user_id, interview_question_id, answer_option_id)VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE answer_option_id = VALUES(answer_option_id)";

								$this->db->query($sql, array($post_details['user_id'], $explode_name[1], $single_form_data['value']));
							}
						}
					}
					// if(!empty($insert_array)){
						
					// 	// for
					// 	$this->common_model->insert_data_sales_db('interview_answer', $insert_array, 'batch');
					// }
					// echo "<pre>";print_r($insert_array);echo"</pre><hr>";exit;
				break;

				default:
					$response['status'] = 'failed';	
					$response['message'] = "call type not found";
				break;
			}
			echo json_encode($response);

		}else{
			die('access is not allowed to this function');
		}
	}
}
?>