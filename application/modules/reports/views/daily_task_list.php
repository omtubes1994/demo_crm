<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="kt-portlet">
		<?php if($this->session->flashdata('success')){ ?>
			<div class="alert alert-success" id="success-alert">
				<strong><?php echo $this->session->flashdata('success'); ?></strong> 
			</div>
		<?php } ?>
		<div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Daily Report
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
            	<label for="sales_person">Sales Person: 
		        	<select class="form-control" id="sales_person">
		  				<option value="">All</option>
		  				<?php 
		  				foreach($sales_person as $sp){
		  					echo '<option value="'.$sp['user_id'].'">'.$sp['name'].'</option>';
		  				}?>
					</select>
				</label>
                <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
					<a href="javascript:void(0);" class="btn kt-subheader__btn-daterange" id="daily_report_daterangepicker" data-toggle="kt-tooltip" title="Select Destrack Date" data-placement="left" style="background: rgb(0 0 0 / 10%); display: inline-flex; height: 32px !important; padding-top: 0; padding-bottom: 0; margin-top: 0.25rem; margin-bottom: 0.25rem; margin-left: 0.25rem;">
						<span class="kt-subheader__btn-daterange-title" id="daily_report_daterangepicker_title" style="display: inline-block;color: #959cb6;font-weight: 500;">Today</span>&nbsp;
						<span class="kt-subheader__btn-daterange-date" id="daily_report_daterangepicker_date" style="display: inline-block;color: #5d78ff;margin-right: 0.75rem;font-weight: 500;">Aug 16</span>
						<i class="flaticon2-calendar-1" style="color: #5d78ff; font-size: 1rem !important;"></i>
					</a>
                </ul>
            </div>
        </div>
		<div class="kt-portlet__body">
			<!--begin: Datatable -->
			<table class="table table-striped- table-bordered table-hover table-checkable" id="points_table">
				<thead>
					<tr>
						<th>Record ID</th>
						<th>Date</th>
						<th>Sales Person</th>
						<th>Task Accomplished</th>
						<th>Work in Progress</th>
						<th>Next Day Plan</th>
						<th>Touch Points</th>
						<th>Desktop Time</th>
						<th>Idle Time</th>
						<th>Productive Time</th>
						<th>Productivity</th>
						<th>Actions</th>
					</tr>
				</thead>
			</table>

			<!--end: Datatable -->
		</div>
	</div>
</div>

<div class="modal fade" id="tp_performance" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
		</div>
	</div>
</div>