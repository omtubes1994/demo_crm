<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MX_Controller {

	function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('user_id')){
			redirect('login', 'refresh');
			exit;
		}else{
			$access_count = 0;
			if(!empty($this->session->userdata('main_module_access')) && in_array(13, array_column($this->session->userdata('main_module_access'), 'module_id'))) {

				foreach ($this->session->userdata('sub_module_access') as $key => $value) {
					
					if($value['module_id'] == 13 && $value['status'] == 'Active') {
						$access_count++;
					}
				}
			}
			if($access_count == 0){
				redirect($this->session->userdata('home/dashboard'));
				exit;
			}
		}
		$this->load->model('reports_model');
	}

	function index(){
		redirect('reports/daily_task_list');
	}

	function daily_task_list(){

		$data = array();
		$where_string = '';
		if($this->session->userdata('role') == 1 || $this->session->userdata('role') == 13){

			$where_string = array('status' => 1,'role !='=>19);
		}else if($this->session->userdata('role') == 6) {

			$where_string = 'status = 1 AND role = 8';
		}else if($this->session->userdata('role') == 11) {

			$where_string = 'status = 1 AND role = 10';
		}else if($this->session->userdata('role') == 16) {

			$where_string = 'status = 1 AND (role = 5)';
		}
		if($where_string) {

			$data['sales_person'] = $this->reports_model->get_dynamic_data('*', $where_string, 'users');
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => ''));
		$this->load->view('sidebar', array('title' => ''));
		$this->load->view('daily_task_list',$data);
		$this->load->view('footer');
	}

	function daily_task_list_data(){
		$order_by = $this->input->get('columns')[$this->input->get('order')[0]['column']]['data'];
		$dir = $this->input->get('order')[0]['dir'];
		if($order_by == 'record_id'){
			$order_by = 'dt.date';
			$dir = 'desc';
		}
		$searchBySalesPerson = $this->input->get('searchBySalesPerson');
		$records = $this->reports_model->getDailyTaskList($this->input->get('start'), $this->input->get('length'), $this->input->get('search')['value'], $order_by, $dir, $searchBySalesPerson, $this->input->get('date'));
		$data['iTotalDisplayRecords'] = $data['iTotalRecords'] = $this->reports_model->getDailyTaskListCount($this->input->get('search')['value'], $searchBySalesPerson, $this->input->get('date'));
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$data['aaData'] = $records;
		echo json_encode($data);
	}
}