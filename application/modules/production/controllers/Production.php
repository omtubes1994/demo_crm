<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Production extends MX_Controller {

	public function __construct(){

		parent::__construct();
		$this->load->model('common/common_model');
		$this->load->model('production_model');
		$this->load->model('home/home_model');
		error_reporting(0);
		ini_set('memory_limit', '500G');
	}
	
	public function index(){
		redirect('production/production_listingz');		
	}	
	public function production_listingz() {

		$this->session->unset_userdata('production_id_for_mtc_upload');
		$this->session->unset_userdata('production_id_for_osdr_document_upload');
		if(in_array($this->session->userdata('role'), array(1, 4, 5, 6, 8, 10, 11, 14, 16, 17, 18))) {

			$data = array();
			// if($this->session->userdata('production_access')['yta_tab_access']){

			// 	$data = $this->create_yta_data();
			// 	$data['ul_menu_tab_li_width'] = 'calc(100%/6) !important';
			// }else{

			// 	$where_response = $this->create_where_response_on_search_filter(
			// 							array(
			// 								'tab_name' => 'all_order',
			// 								'production_search_form' => array(
			// 									0 => array(
			// 										'name' => 'type',
			// 										'value' => 'OM'
			// 									)
			// 								)
			// 							)
			// 					);
			// 	$data = $this->prepare_listing_data(implode(' AND ', $where_response['where_string_array']),array(), 10, 0, $where_response['where_array']);
			// 	$data['ul_menu_tab_li_width'] = 'calc(100%/5) !important';
			// }
			if(in_array('yta', $this->session->userdata('production_access')['production_list_tab_access'])){
				$data = $this->create_yta_data();
				$data['yta_tab_count'] = count($data['production_list']);

			}else{

				$tab_list = $this->session->userdata('production_access')['production_list_tab_access'];
				if($tab_list[0] == 'all'){

					$tab_name = 'all_order';
				}else if($tab_list[0] == 'pending'){

					$tab_name = 'pending_order';
				}

				$this->session->set_userdata(array('production_tab_name'=> $tab_name));
				$where_response = $this->create_where_response_on_search_filter(
										array(
											'tab_name' => $tab_name,
											'production_search_form' => array(
												0 => array(
													'name' => 'type',
													'value' => 'OM'
												)
											)
										)
								);
				$data = $this->prepare_listing_data(implode(' AND ', $where_response['where_string_array']),array(), 10, 0, $where_response['where_array']);


				if(in_array('all', $this->session->userdata('production_access')['production_list_tab_access']) == $tab_list[0]){

					$data['all_tab_active'] = 'active_list';

				}else if(in_array('pending', $this->session->userdata('production_access')['production_list_tab_access']) == $tab_list[0]){

					$data['pending_tab_active'] = 'active_list';
				}
			}
			$data['ul_menu_tab_li_width'] = 'calc(100%/6) !important';
			$where_array = array('production_process_information.type'=> 'OM');
			$data['tab_count'] = $this->prepare_tab_count_data($where_array);

			// if(in_array($this->session->userdata('user_id'), array(79, 85, 133, 141, 179, 180, 189, 193, 150, 151, 170, 190, 205, 210)))

			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
			$this->load->view('header', array('title' => 'Production Listing'));
			$this->load->view('sidebar', array('title' => 'Production Module'));
			$this->load->view('production/product_listing', $data);
			$this->load->view('footer');

		} else {
			die("Soory!!!. You Dont have Access to this.");
		}
	}

	public function update_production_listing() {

		$quotation_id = $this->uri->segment('3', 0);
		if(empty($quotation_id)) {
			redirect('production/production_listingz');
		}
		$data = array();
		$data['quotation_details'] = $this->common_model->get_dynamic_data_sales_db('*',array('quotation_mst_id'=>$quotation_id), 'quotation_mst','row_array');
		$data['users_details'] = $this->get_common_data('users_details');
		if(!empty($data['quotation_details'])) {
			
			$data['rfq_details'] = $this->common_model->get_dynamic_data_sales_db('rfq_mst_id, rfq_no',array('rfq_mst_id'=>$data['quotation_details']['rfq_id']), 'rfq_mst', 'row_array');
			$data['client_details'] = $this->common_model->get_dynamic_data_sales_db('*',array('id'=>$data['quotation_details']['client_id']), 'customer_mst', 'row_array');
			$data['currency_details'] = $this->common_model->get_dynamic_data_sales_db('*',array('status'=>'Active', 'currency_id'=>$data['quotation_details']['currency']), 'currency', 'row_array');
			$data['payment_term_details'] = $this->common_model->get_dynamic_data_sales_db('*',array('status'=>'Active', 'term_id'=>$data['quotation_details']['payment_term']), 'payment_terms', 'row_array');
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";
		$product_details = $this->common_model->get_dynamic_data_sales_db('*',array('quotation_mst_id'=>$quotation_id), 'quotation_dtl');

		$static_quotation_status =  array(
				'pending_order'=> array('name'=>'Pending'),
				'semi_ready'=>array('name'=>'Semi_Ready'),
				'ready_to_dispatch_order'=>array('name'=>'Ready_For_Dispatch'),
				'dispatch_order'=>array('name'=>'Dispatched'),
				'on_hold_order'=>array('name'=>'Order_On_Hold'),
				'mtt'=>array('name'=>'ICE'),
				'import'=>array('name'=>'Import'),
				'query'=>array('name'=>'Query'),
				'mtt_rfd'=>array('name'=>'In_transit_ICE'),
				'order_cancelled'=>array('name'=>'order_cancelled')
		);

		// echo "<pre>";print_r($product_details);echo"</pre><hr>";exit;
		$data['handle_by_users'] = $this->get_common_data('handle_by');$this->common_model->get_procurement_person('user_id, name');
		$data['product_list'] = array();
		$data['material_list'] = array();
		$data['product_production_status'] = array();
		// $products = array_column($this->common_model->get_dynamic_data_sales_db('*', array('lookup_group'=>259), 'lookup'), 'lookup_value', 'lookup_id');	
		$products = array_column($this->common_model->get_dynamic_data_sales_db('*', array(), 'product_mst'), 'name', 'id');	
		// $materials = array_column($this->common_model->get_dynamic_data_sales_db('*', array('lookup_group'=>272), 'lookup'), 'lookup_value', 'lookup_id');
		$materials = array_column($this->common_model->get_dynamic_data_sales_db('*', array(), 'material_mst'), 'name', 'id');
		
		if(!empty($product_details)) {

			foreach ($product_details as $product_key => $single_product_detail) {
				if(!empty($products[$single_product_detail['product_id']])) {

					if(!in_array($products[$single_product_detail['product_id']], $data['product_list'])) {

						$data['product_list'][] = $products[$single_product_detail['product_id']]; 		
					}
				}
				if(!empty($materials[$single_product_detail['material_id']])) {

					if(!in_array($materials[$single_product_detail['material_id']], $data['material_list'])) {

						$data['material_list'][] = $materials[$single_product_detail['material_id']];
					}
				}
				
				// if($single_product_detail['production_status'] != 'pending_order') {

				// 	foreach ($static_quotation_status as $status_key => $single_status_details) {
				// 		if($single_product_detail['production_status'] == $status_key){
					
				// 			$data['product_production_status'][$single_status_details['name']][] =
				// 						array(
				// 							'id'=> $product_key+1,
				// 							'quotation_dtl_id'=> $single_product_detail['quotation_dtl_id'],
				// 							'product'=>
				// 								(!empty($single_product_detail['product_id']))
				// 								?$products[$single_product_detail['product_id']]
				// 								:'',
				// 							'material'=>
				// 								(!empty($single_product_detail['material_id']))
				// 								?$materials[$single_product_detail['material_id']]
				// 								:'',
				// 							'description'=> $single_product_detail['description'],
				// 							'quantity'=> $single_product_detail['quantity'],
				// 							'production_status'=> $single_product_detail['production_status'],
				// 							'total'=> ($single_product_detail['unit_price'] * $single_product_detail['production_quantity_done']),
				// 							'production_quantity_done'=> $single_product_detail['production_quantity_done']
				// 						);
				// 		}
				// 	}
				// }
				// if((int)$single_product_detail['quantity'] > (int)$single_product_detail['production_quantity_done']) {

				// 	$data['product_production_status']['pending_order'][] = 
				// 							array(
				// 								'id'=> $product_key+1,
				// 								'quotation_dtl_id'=> $single_product_detail['quotation_dtl_id'],
				// 								'product'=> 
				// 									(!empty($single_product_detail['product_id']))
				// 									?$products[$single_product_detail['product_id']]
				// 									:'',
				// 								'material'=> 
				// 									(!empty($single_product_detail['material_id']))
				// 									?$materials[$single_product_detail['material_id']]
				// 									:'',
				// 								'description'=> $single_product_detail['description'],
				// 								'quantity'=> $single_product_detail['quantity'],
				// 								'production_status'=> $single_product_detail['production_status'],
				// 								'total'=> ($single_product_detail['unit_price'] * ($single_product_detail['quantity'] - $single_product_detail['production_quantity_done'])),
				// 								'production_quantity_done'=> $single_product_detail['production_quantity_done']
				// 							);
				// }
			}
			$data['product_production_status'] = $this->create_quantity_status_wise_data($product_details, $products, $materials);
		}
		$data['production_information'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active','quotation_mst_id'=>$quotation_id), 'production_process_information', 'row_array');
		if(empty($data['production_information'])) {

			$data['production_information'] = array('id'=>'', 'work_order_no'=>'', 'product_family'=> '', 'delivery_date'=>date('Y-m-d'), 'vendor_po'=>'', 'handled_by'=>'', 'production_status'=>'', 'payment_status'=>'', 'qc_clearance'=>'', 'latest_update'=>'', 'special_comment'=>'', 'type'=>'', 'actual_delivery_date'=>date('Y-m-d')); 
		}else{

			$latest_update = json_decode($data['production_information']['latest_update'], true);
			$data['production_information']['latest_update'] = '';
			if(count($latest_update) > 0){

				$data['production_information']['latest_update'] = $latest_update[count($latest_update)-1]['message'];
			}
		}
		$data['production_status'] =  $this->create_production_status_data();
		$data['production_type'] =  $this->create_production_type_data();

		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Production Update'));
		$this->load->view('sidebar', array('title' => 'Production Update'));
		$this->load->view('production/product_update', $data);
		$this->load->view('footer');	
	}

	public function create_work_order_sheet() {

		$work_order_no = $this->uri->segment('3', 0);
		if(empty($work_order_no)) {
			redirect('production/production_listingz');
		}
		if(true) {
		// if(in_array($this->session->userdata('role'), array(1, 17))) {

			$data = array();
			$data['work_order_sheet_details']['work_order_no'] = $work_order_no;
			$data['work_order_sheet_details']['completion_date'] = date('Y-m-d');
			$data['work_order_sheet_details']['create_date'] = date('Y-m-d');
			$data['work_order_sheet_details']['delivery_date_master'] = $this->common_model->get_all_conditional_data_sales_db('delivery_id, delivery_name', array('status'=>'Active'), 'delivery');
			$data['work_order_sheet_details']['procurement_person'] = $this->common_model->get_all_conditional_data_sales_db('user_id, name', array('status'=>'1', 'role'=>8), 'users');
			$data['work_order_sheet_details']['production_person'] = $this->common_model->get_all_conditional_data_sales_db('user_id, name', array('status'=>'1', 'role'=>18), 'users');
			$data['work_order_sheet_details']['checked_by_person'] = array(array('user_id'=> 59, 'name'=> 'Mayank Tanna'), array('user_id'=> 100, 'name'=> 'Mahesh Sahani'));

			$data['work_order_sheet_details']['approved_by_person'] = array(array('user_id'=> 2, 'name'=> 'Jay Mehta'), array('user_id'=> 16, 'name'=> 'Pratik Mehta'), array('user_id'=> 23, 'name'=> 'Yash Jain'), array('user_id'=> 189, 'name'=> 'Faraz Siddiqui'));

			$data['users_details'] = array_column($this->common_model->get_all_conditional_data_sales_db('*', array('status'=>1), 'users'), 'name', 'user_id');
			$delivery = $procurement_person = $production_person = $made_by = $checked_by = $approved_by = $work_order_sheet_id = '';
			$data['work_order_sheet_revision_comment'] = $data['work_order_sheet_packaging_comment'] = $data['work_order_sheet_marking_comment'] = $data['work_order_sheet_quality_note_master'] = $work_order_sheet_quality_note = $data['work_order_sheet_special_note'] = $data['work_order_sheet_item_details'] = array();
			$data['work_order_sheet_quality_note_master'] = $this->common_model->get_all_conditional_data_sales_db('*', array('status'=>'Active'), 'work_order_sheet_quality_note_master');

			$data['production_details'] = $this->common_model->get_all_conditional_data_sales_db('type, technical_document_file_and_path', array('status'=>'Active', 'work_order_no'=> $work_order_no), 'production_process_information', 'row_array');

			$work_order_sheet_details = $this->common_model->get_all_conditional_data_sales_db('*', array('status'=>'Active', 'work_order_no'=>$work_order_no), 'work_order_sheet', 'row_array');
			
			if(!empty($work_order_sheet_details)) {

				$data['work_order_sheet_details']['completion_date'] = $work_order_sheet_details['completion_date'];
				$data['work_order_sheet_details']['create_date'] = $work_order_sheet_details['create_date'];
				$delivery = $work_order_sheet_details['delivery'];
				$procurement_person = $work_order_sheet_details['procurement_person'];
				$production_person = $work_order_sheet_details['production_person'];
				$made_by = $work_order_sheet_details['made_by'];
				$checked_by = $work_order_sheet_details['checked_by'];
				$approved_by = $work_order_sheet_details['approved_by'];
				$work_order_sheet_id = $work_order_sheet_details['id'];

				$data['work_order_sheet_item_details'] = $this->common_model->get_all_conditional_data_sales_db('*', array('status'=>'Active', 'work_order_sheet_id'=> $work_order_sheet_id), 'work_order_sheet_item_details');
				
				$data['work_order_sheet_special_note'] = $this->common_model->get_all_conditional_data_sales_db('*', array('status'=>'Active', 'work_order_sheet_id'=> $work_order_sheet_id), 'work_order_sheet_special_note');
				
				$work_order_sheet_quality_note = array_column($this->common_model->get_all_conditional_data_sales_db('*', array('status'=>'Active', 'work_order_sheet_id'=> $work_order_sheet_id), 'work_order_sheet_quality_note'), 'quality_id');

				$data['work_order_sheet_marking_comment'] = $this->common_model->get_all_conditional_data_sales_db('*', array('status'=>'Active', 'work_order_sheet_id'=> $work_order_sheet_id), 'work_order_sheet_marking_comment');
				
				$data['work_order_sheet_packaging_comment'] = $this->common_model->get_all_conditional_data_sales_db('*', array('status'=>'Active', 'work_order_sheet_id'=> $work_order_sheet_id), 'work_order_sheet_packaging_comment');

				$data['work_order_sheet_revision_comment'] = $this->common_model->get_all_conditional_data_sales_db('*', array('status'=>'Active', 'work_order_sheet_id'=> $work_order_sheet_id), 'work_order_sheet_revision_comment');
			}

			$data['work_order_sheet_details']['delivery_date_master'] = $this->create_select_drop_down_data($data['work_order_sheet_details']['delivery_date_master'], 'delivery_id', $delivery);

			$data['work_order_sheet_details']['procurement_person'] = $this->create_select_drop_down_data($data['work_order_sheet_details']['procurement_person'], 'user_id', $procurement_person);

			$data['work_order_sheet_details']['production_person'] = $this->create_select_drop_down_data($data['work_order_sheet_details']['production_person'], 'user_id', $production_person);

			$data['work_order_sheet_quality_note_master'] = $this->create_select_drop_down_data($data['work_order_sheet_quality_note_master'], 'id', $work_order_sheet_quality_note, 'checked');

			$data['work_order_sheet_details']['approved_by_person'] = $this->create_select_drop_down_data($data['work_order_sheet_details']['approved_by_person'], 'user_id', $approved_by, 'checked');
			$data['work_order_sheet_id'] = $work_order_sheet_id;

			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
			$this->load->view('header', array('title' => 'Production Listing'));
			$this->load->view('sidebar', array('title' => 'Production Module'));
			$this->load->view('production/create_work_order_sheet_index', $data);
			$this->load->view('footer');

		} else {
			die("Soory!!!. You Dont have Access to this.");
		}
	}

	function pdf(){
		$work_order_no = $this->uri->segment('3', 0);
		if(empty($work_order_no)) {
			redirect('production/production_listingz');
		}
		$this->load->library('Pdf');
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetCreator('');
		$pdf->SetAuthor('');
		$pdf->SetTitle('');
		$pdf->SetSubject('');
		$pdf->SetKeywords('');
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, NULL, '', array(0,0,0), array(255,255,255));
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		    require_once(dirname(__FILE__).'/lang/eng.php');
		    $pdf->setLanguageArray($l);
		}
		$pdf->SetFont('helvetica', '', 10);
		$pdf->AddPage();
		$data = $work_order_sheet_details = array();
		$data['work_order_sheet_details'] = array('work_order_no'=> '', 'completion_date'=> '', 'create_date'=> '', 'delivery'=> '', 'procurement_person'=> '', 'production_person'=> '', 'made_by_approved_by_combine'=> array(), 'approved_by'=> '');
		$data['work_order_sheet_item_details'] = array();
		$data['work_order_sheet_special_note'] = array();
		$data['work_order_sheet_quality_note'] = array();
		$data['work_order_sheet_marking_comment'] = array();
		$data['work_order_sheet_packaging_comment'] = array();
		$data['users_details'] = array_column($this->common_model->get_all_conditional_data_sales_db('*', array('status'=>1), 'users'), 'name', 'user_id');
		$data['users_details'][''] = '';
		$data['users_details'][0] = '';

		$data['delivery_details'] = array_column($this->common_model->get_all_conditional_data_sales_db('*', array('status'=>'Active'), 'delivery'), 'delivery_name', 'delivery_id');
		$data['delivery_details'][''] = '';
	
		$data['quality_details'] = array_column($this->common_model->get_all_conditional_data_sales_db('*', array('status'=>'Active'), 'work_order_sheet_quality_note_master'), 'notes', 'id');
		$data['quality_details'][''] = '';

		$data['signature_details'] = array_column($this->common_model->get_all_conditional_data_sales_db('*', array('status'=>'Active'), 'work_order_sheet_pdf_information'), 'sign_path', 'user_id');
		$data['signature_details'][''] = '';


		$work_order_sheet_details = $this->common_model->get_all_conditional_data_sales_db('*', array('status'=>'Active', 'work_order_no'=> $work_order_no), 'work_order_sheet', 'row_array');
		if(!empty($work_order_sheet_details)){

			$data['work_order_sheet_details'] = $work_order_sheet_details;
			$data['work_order_sheet_details']['made_by_approved_by_combine'][0] = array('made_by_name'=>'<strong>Made By</strong>', 'approved_by_name'=>'<strong>Approved By</strong>');
			if(!empty($work_order_sheet_details['made_by'])){

				foreach (json_decode($work_order_sheet_details['made_by'], true) as $made_by_details) {
					
					$data['work_order_sheet_details']['made_by_approved_by_combine'][] = array('made_by_name'=>$made_by_details['name'].' '.date('M d, Y', strtotime($made_by_details['time'])),'approved_by_name'=>'');
				}
			}
			if(!empty($work_order_sheet_details['approved_by'])){

				$data['work_order_sheet_details']['made_by_approved_by_combine'][1]['approved_by_sign'] = $data['signature_details'][$work_order_sheet_details['approved_by']];
				$data['work_order_sheet_details']['made_by_approved_by_combine'][2]['approved_by_name'] = $data['users_details'][$work_order_sheet_details['approved_by']];
			}
			$data['work_order_sheet_item_details'] = $this->common_model->get_all_conditional_data_sales_db('*', array('status'=>'Active', 'work_order_sheet_id'=> $work_order_sheet_details['id']), 'work_order_sheet_item_details');

			$data['work_order_sheet_special_note'] = $this->common_model->get_all_conditional_data_sales_db('*', array('status'=>'Active', 'work_order_sheet_id'=> $work_order_sheet_details['id']), 'work_order_sheet_special_note');

			$data['work_order_sheet_quality_note'] = $this->common_model->get_all_conditional_data_sales_db('*', array('status'=>'Active', 'work_order_sheet_id'=> $work_order_sheet_details['id']), 'work_order_sheet_quality_note');

			$data['work_order_sheet_marking_comment'] = $this->common_model->get_all_conditional_data_sales_db('*', array('status'=>'Active', 'work_order_sheet_id'=> $work_order_sheet_details['id']), 'work_order_sheet_marking_comment');
			
			$data['work_order_sheet_packaging_comment'] = $this->common_model->get_all_conditional_data_sales_db('*', array('status'=>'Active', 'work_order_sheet_id'=> $work_order_sheet_details['id']), 'work_order_sheet_packaging_comment');
			
			$data['work_order_sheet_revision_comment'] = $this->common_model->get_all_conditional_data_sales_db('*', array('status'=>'Active', 'work_order_sheet_id'=> $work_order_sheet_details['id']), 'work_order_sheet_revision_comment');

			$data['production_details'] = $this->common_model->get_all_conditional_data_sales_db('type, technical_document_file_and_path', array('work_order_no'=> $work_order_no), 'production_process_information', 'row_array');

		}
		

		// echo "<pre>";print_r($data);echo"</pre><hr>";die;
		$html = $this->load->view('production/work_order_pdf', $data, true);
		$pdf->writeHTML($html, true, false, true, false, '');
		$pdf->lastPage();
		$pdf->Output('example_061.pdf', 'I');
	}

	public function ajax_function() {

		if($this->input->is_ajax_request()){
			$call_type = $this->input->post('call_type');
			$post_details = $this->input->post();
			$response['status'] = 'successful';	
			switch ($post_details['call_type']) {

				case 'save_production_details_form':

					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";die;
					$form_data = $post_details['production_details_form'];
					if(!empty($form_data)) {
						$insert_array = array();
						
						foreach ($form_data as $single_form_value) {
							
							if(empty($insert_array[$single_form_value['name']])) {

								$insert_array[$single_form_value['name']] = trim($single_form_value['value']);
							} else {

								$insert_array[$single_form_value['name']] = $insert_array[$single_form_value['name']].','.$single_form_value['value'];
							}
						}
						$id=$insert_array['id'];
						unset($insert_array['id']);
						// echo "<pre>";print_r($insert_array);echo"</pre><hr>";exit;
						$latest_update = array();
						if(empty($id)) {
							$last_quote_id = $this->common_model->get_dynamic_data_sales_db('last_quote_id', array('status'=> "Active", 'quote_name'=>'production'), 'last_quote_created_number', 'row_array');
							$next_quote_id = ((int)$last_quote_id['last_quote_id'])+1;
							$insert_array['work_order_no'] = $next_quote_id;
							$insert_array['latest_update'] = json_encode($latest_update);
							if(!empty($insert_array['delivery_date'])){

								$insert_array['delivery_date'] = date('Y-m-d', strtotime(str_replace(',', '', $insert_array['delivery_date'])));
							}
							if(!empty($insert_array['actual_delivery_date'])){

								$insert_array['actual_delivery_date'] = date('Y-m-d', strtotime(str_replace(',', '', $insert_array['actual_delivery_date'])));
							}
							// echo "<pre>";print_r($insert_array);echo"</pre><hr>";exit;
							$this->common_model->insert_data_sales_db('production_process_information', $insert_array);
							$this->common_model->update_data_sales_db('last_quote_created_number', array('last_quote_id'=>$next_quote_id), array('quote_name'=>'production'));
						} else {

							$data = $this->common_model->get_dynamic_data_sales_db('latest_update', array('id'=> $id), 'production_process_information', 'row_array');
							$latest_update = json_decode($data['latest_update'], true);
							$latest_update[] = array(
								'message'=> trim($insert_array['latest_update']),
								'user_id'=> $this->session->userdata('user_id'),
								'date_time'=> $this->get_indian_time()
							);
							$insert_array['latest_update'] = json_encode($latest_update);
							if(!empty($insert_array['delivery_date'])){

								$insert_array['delivery_date'] = date('Y-m-d', strtotime(str_replace(',', '', $insert_array['delivery_date'])));
							}else{
								$insert_array['delivery_date'] = null;
							}
							if(!empty($insert_array['actual_delivery_date'])){

								$insert_array['actual_delivery_date'] = date('Y-m-d', strtotime(str_replace(',', '', $insert_array['actual_delivery_date'])));
							}else{
								$insert_array['actual_delivery_date'] = null;
							}
							$this->common_model->update_data_sales_db('production_process_information', $insert_array, array('id'=>$id));
						}
					}
					// echo "<pre>";print_r($insert_array);echo"</pre><hr>";exit;
				break;

				case 'change_tab':
				case 'search_filter':
				case 'paggination_filter':
					
					// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;
					$this->session->set_userdata(array('production_tab_name'=> $post_details['tab_name']));
					$where_response = $this->create_where_response_on_search_filter($post_details);
					// echo "<pre>";print_r($where_response);echo"</pre><hr>";exit;
					$data = $this->prepare_listing_data(implode(' AND ', $where_response['where_string_array']),array(),$post_details['limit'],$post_details['offset'], $where_response['where_array']);
					// echo "<pre>";print_r($data);echo"</pre><hr>";die;

					$response['tab_count'] = $this->prepare_tab_count_data($post_details['production_search_form']);
					$response['search_filter_body'] = $this->load->view('production/product_list_search_filter_form', $data, true);
					$response['list_body'] = $this->load->view('production/production_list_body', $data, true);
					$response['paggination_filter_body'] = $this->load->view('production/product_list_paggination', $data, true);
				break;

				case 'change_type':
					
					$this->session->set_userdata(array('type'=> $post_details['type']));
					$where_response = $this->create_where_response_on_search_filter($post_details);
					// echo "<pre>";print_r($where_response);echo"</pre><hr>";exit;
					$data = $this->prepare_listing_data(implode(' AND ', $where_response['where_string_array']),array(),$post_details['limit'],$post_details['offset'], $where_response['where_array']);
					$order_status_wise_total = $this->get_order_wise_total_new($this->session->userdata('type'));
					$data['order_status_wise_total'] = $order_status_wise_total['order_status_wise_total'];
					$data['total_of_3_order'] = $order_status_wise_total['total_of_3_order'];
					$data['total_of_3_order_number'] = $order_status_wise_total['total_of_3_order_number'];

					$response['tab_count'] = $this->prepare_tab_count_data($post_details['production_search_form']);
					// echo "<pre>";print_r($data);echo"</pre><hr>";die;
					$response['production_title_value'] = $data['total_of_3_order'].'('.$data['total_of_3_order_number'].')';
					$response['status_wise_count'] = $this->load->view('production/status', $data, true);
					$response['search_filter_body'] = $this->load->view('production/product_list_search_filter_form', $data, true);
					$response['list_body'] = $this->load->view('production/production_list_body', $data, true);
					$response['paggination_filter_body'] = $this->load->view('production/product_list_paggination', $data, true);
				break;

				case 'delete_production_list':
					$production_id = $post_details['delete_id'];
					$response['status'] = 'failed';	
					if(!empty($production_id)) {
						$this->common_model->update_data_sales_db('production_process_information', array('status'=>'Inactive'), array('id'=>$production_id));
						$response['status'] = 'successful';
					}
				break;

				case 'get_query_history':
					$res = $this->production_model->get_query_history($this->input->post('quote_id'), $this->input->post('query_type'));
					$response['query_table_history'] = '<table class="table table-bordered table-stripped"><tr><th>Sr #</th><th>Query Message</th><th>Query Raised By</th><th>Query Answer By</th></tr>';
					if(!empty($res)) {
						$all_user = $this->get_common_data('users_details');
						// echo "<pre>";print_r($res);echo"</pre><hr>";
						// echo "<pre>";print_r($all_user);echo"</pre><hr>";exit;
						foreach ($res as $res_key => $res_value) {
							
							$response['query_table_history'] .= "<tr>";
								$response['query_table_history'] .= "<td>".++$res_key."</td>";
								$response['query_table_history'] .= "<td style='width:60%;'>".$res_value['query_text']."</td>";
								$response['query_table_history'] .= "<td>";
								$response['query_table_history'] .= (!empty($all_user[$res_value['raised_by']])) ? $all_user[$res_value['raised_by']] : 'No user found';
								$response['query_table_history'] .= "</td>";
								$response['query_table_history'] .= "<td>";
								$response['query_table_history'] .= (!empty($all_user[$res_value['query_recepient']]))? $all_user[$res_value['query_recepient']] : 'No User found';
								$response['query_table_history'] .= "</td";
							$response['query_table_history'] .= "</tr>";
						}
					}
					$response['query_table_history'] .= '</table>';
				break;

				case 'create_work_order':

					// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;
					$production_process_information = $this->common_model->get_dynamic_data_sales_db('*', array('status'=> "Active", 'quotation_mst_id'=>$this->input->post('quotation_mst_id')), 'production_process_information', 'row_array');
					if(empty($production_process_information)) {

						$production_insert_array = array('work_order_no'=> 0, 'quotation_mst_id'=> $this->input->post('quotation_mst_id'), 'handled_by_new'=> '');
						$rfq_details = $this->common_model->get_dynamic_data_sales_db('*', array('rfq_mst_id'=>$this->input->post('rfq_mst_id')), 'rfq_mst', 'row_array');
						// echo "<pre>";print_r($rfq_details);echo"</pre><hr>";
						if(!empty($rfq_details)){

							$rfq_assign_array = array();
							if(!empty($rfq_details['assigned_to'])){

								$rfq_assign_array[] = $rfq_details['assigned_to'];
							}
							if(!empty($rfq_details['assigned_to_1']) && !in_array($rfq_details['assigned_to_1'], $rfq_assign_array)){

								$rfq_assign_array[] = $rfq_details['assigned_to_1'];
							}
							if(!empty($rfq_details['assigned_to_2']) && !in_array($rfq_details['assigned_to_2'], $rfq_assign_array)){
							
								$rfq_assign_array[] = $rfq_details['assigned_to_2'];
							}
							if(!empty($rfq_details['assigned_to_3']) && !in_array($rfq_details['assigned_to_3'], $rfq_assign_array)){

								$rfq_assign_array[] = $rfq_details['assigned_to_3'];
							}

							// echo "<pre>";print_r($rfq_assign_array);echo"</pre><hr>";exit;
							if(!empty($rfq_assign_array)){

								$user_details = array_column($this->common_model->get_dynamic_data_sales_db('user_id, name', array(), 'users'), 'name', 'user_id');
								foreach ($rfq_assign_array as $user_id) {
									
									$explode_user_details = explode(' ', $user_details[$user_id]);
									if(!empty($production_insert_array['handled_by_new'])){

										$production_insert_array['handled_by_new'] .= ','.$explode_user_details[0];
									}else{

										$production_insert_array['handled_by_new'] .= $explode_user_details[0];
									}
								}
							}
							$production_insert_array['type'] = $rfq_details['type'];
							$production_insert_array['rating_value'] = $rfq_details['priority'];
							$production_insert_array['rating_comment'] = $rfq_details['priority_reason'];
						}
						$last_quote_id = $this->common_model->get_dynamic_data_sales_db('last_quote_id', array('status'=> "Active", 'quote_name'=>'production'), 'last_quote_created_number', 'row_array');
						$next_quote_id = ((int)$last_quote_id['last_quote_id'])+1;
						$production_insert_array['work_order_no'] = $next_quote_id;
						// echo "<pre>";print_r($production_insert_array);echo"</pre><hr>";exit;
						$this->common_model->insert_data_sales_db('production_process_information', $production_insert_array);
						$this->common_model->update_data_sales_db('quotation_mst', array('work_order_no'=>$next_quote_id), array('quotation_mst_id'=>$this->input->post('quotation_mst_id')));
						$this->common_model->update_data_sales_db('last_quote_created_number', array('last_quote_id'=>$next_quote_id), array('quote_name'=>'production'));
						$this->common_model->insert_data_sales_db('task_management', array('type'=> 'drawing', 'user_id'=> 325, 'name'=> 'Create Drawing', 'work_order_no'=> $next_quote_id));

						if($rfq_details['product_family'] == 'industrial'){

							$this->common_model->insert_data_sales_db('task_management', array('type'=> 'marking', 'user_id'=> 189, 'name'=> 'Create Marking', 'work_order_no'=> $next_quote_id));
							$this->common_model->insert_data_sales_db('task_management', array('type'=> 'mtc', 'user_id'=> 189, 'name'=> 'Create MTC', 'work_order_no'=> $next_quote_id));
						}else{

							$this->common_model->insert_data_sales_db('task_management', array('type'=> 'marking', 'user_id'=> 297, 'name'=> 'Create Marking', 'work_order_no'=> $next_quote_id));
							$this->common_model->insert_data_sales_db('task_management', array('type'=> 'mtc', 'user_id'=> 297, 'name'=> 'Create MTC', 'work_order_no'=> $next_quote_id));
						}
						$this->common_model->insert_data_sales_db('task_management', array('type'=> 'logistics_weight_and_dimension', 'user_id'=> 40, 'name'=> 'Logistics weight and dimension', 'work_order_no'=> $next_quote_id));
						$this->common_model->insert_data_sales_db('task_management', array('type'=> 'quality_inspection', 'user_id'=> 120, 'name'=> 'Quality Inspection', 'work_order_no'=> $next_quote_id));

					}else {

						$this->common_model->update_data_sales_db('quotation_mst', array('work_order_no'=>$production_process_information['work_order_no']), array('quotation_mst_id'=>$this->input->post('quotation_mst_id')));
					}
				break;

				case 'upload_mtc':

					$response['msg'] = 'File is uploaded successfully!!!';
					$return_response = $this->upload_doc_config();
					$response['status'] = $return_response['status'];
					if($return_response['status'] == 'successful') {

						$data = $this->common_model->get_dynamic_data_sales_db('*',array('id'=>$this->session->userdata('production_id_for_mtc_upload')), 'production_process_information', 'row_array');
						if(empty($data['mtc_pdf'])) {

							$this->common_model->update_data_sales_db('production_process_information', array('mtc_pdf'=>$return_response['file_name']), array('id'=>$this->session->userdata('production_id_for_mtc_upload')));
						} else {

							$this->common_model->update_data_sales_db('production_process_information', array('mtc_pdf'=> $data['mtc_pdf'].','.$return_response['file_name']), array('id'=>$this->session->userdata('production_id_for_mtc_upload')));
						}
						$quotation_details = $this->common_model->get_dynamic_data_sales_db('*',array('work_order_no'=> $data['work_order_no']), 'quotation_mst', 'row_array');
						if(!empty($quotation_details)) {

							// $client_details = $this->common_model->get_all_conditional_data_sales_db('client_name',array('client_id'=> $quotation_details['client_id']),'clients', 'row_array');
							$client_details = $this->common_model->get_all_conditional_data_sales_db('name',array('id'=> $quotation_details['client_id']),'customer_mst', 'row_array');
							
							$this->common_model->insert_data_sales_db('user_alert', array('module_name'=> 'quality', 'alert_showed_user_id'=> $quotation_details['assigned_to'], 'alert_given_user_id'=> $this->session->userdata('user_id'), 'alert_subject'=> "MTC PDF is uploaded for client ".$client_details['name']." against proforma # ".$quotation_details['proforma_no']."."));
						}
					}else {
						$response['msg'] = 'File is not uploaded.';
					}
				break;

				case 'upload_marking':

					$response['msg'] = 'File is uploaded successfully!!!';
					$return_response = $this->upload_doc_config();
					$response['status'] = $return_response['status'];
					if($return_response['status'] == 'successful') {

						$data = $this->common_model->get_dynamic_data_sales_db('marking_pdf',array('id'=>$this->session->userdata('production_id_for_mtc_upload')), 'production_process_information', 'row_array');
						if(empty($data['marking_pdf'])) {

							$this->common_model->update_data_sales_db('production_process_information', array('marking_pdf'=>$return_response['file_name']), array('id'=>$this->session->userdata('production_id_for_mtc_upload')));
						} else {

							$this->common_model->update_data_sales_db('production_process_information', array('marking_pdf'=> $data['marking_pdf'].','.$return_response['file_name']), array('id'=>$this->session->userdata('production_id_for_mtc_upload')));
						}
					}else {
						$response['msg'] = 'File is not uploaded.';
					}
				break;

				case 'get_mtc_upload_history':
				
					$production_id = (!empty($this->input->post('production_id'))) ? $this->input->post('production_id') : $this->session->userdata('production_id_for_mtc_upload'); 				
					if(!empty($production_id)) {

						$data = $this->common_model->get_dynamic_data_sales_db('mtc_pdf as pdf, mtc_email_name',array('id'=>$production_id), 'production_process_information', 'row_array');
						$response['mtc_upload_table'] = $this->load->view('production/mtc_pdf_table', $data, true);
						$response['mtc_email_name'] = $data['mtc_email_name'];
						$data = $this->common_model->get_dynamic_data_sales_db('marking_pdf as pdf, marking_email_name',array('id'=>$production_id), 'production_process_information', 'row_array');
						// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
						$response['marking_upload_table'] = $this->load->view('production/mtc_pdf_table', $data, true);
						$response['marking_email_name'] = $data['marking_email_name'];
						$response['id'] = $production_id;
						// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
						$this->session->set_userdata('production_id_for_mtc_upload', $production_id);
					}
				break;

				case 'unset_mtc_production_id':
					$this->session->unset_userdata('production_id_for_mtc_upload');
				break;

				case 'delete_mtc_pdf':
					$data = $this->common_model->get_dynamic_data_sales_db('mtc_pdf',array('id'=>$this->session->userdata('production_id_for_mtc_upload')), 'production_process_information', 'row_array');	
					if(!empty($data['mtc_pdf'])){
						$pdf_array =  explode(',', $data['mtc_pdf']);
						unset($pdf_array[$this->input->post('pdf_key')]);
						$this->common_model->update_data_sales_db('production_process_information', array('mtc_pdf'=>implode(',', $pdf_array)), array('id'=>$this->session->userdata('production_id_for_mtc_upload')));
					}
				break;

				case 'update_product_status':

					$update_array = array();

					if(!empty($this->input->post('quotation_dtl_id'))) {
						//getting quotation dtl data
						$quotation_details_data = $this->common_model->get_dynamic_data_sales_db('*', array('quotation_dtl_id'=>$this->input->post('quotation_dtl_id')), 'quotation_dtl', 'row_array');
						// echo "<pre>";print_r($quotation_details_data);echo"</pre><hr>";
						if(!empty($quotation_details_data)) {
							if($quotation_details_data['production_status'] != 'pending_order') {

								$this->add_remove_production_count($quotation_details_data['quotation_mst_id'], $quotation_details_data['production_status'], 'remove');	
							}
							$this->add_remove_production_count($quotation_details_data['quotation_mst_id'], $this->input->post('production_status'), 'add');
							$update_array['production_status'] = $this->input->post('production_status');
							$update_array['production_quantity_done'] = $this->input->post('production_quantity');
							$update_array[$post_details['production_status']."_count"] = $this->input->post('production_quantity');
						}
					}
					if(!empty($update_array)) {
						$this->common_model->update_data_sales_db('quotation_dtl', $update_array, array('quotation_dtl_id'=>$this->input->post('quotation_dtl_id')));
					}
				break;

				case 'production_stage_wise_highchart_data':
					
					$highchart_data_call_type = (empty($this->input->post('highchart_data_call_type'))) ? 'pending_order': $this->input->post('highchart_data_call_type');
					$where_response = $this->create_where_response_on_search_filter(
						array(
							'tab_name' => $highchart_data_call_type,
							'production_search_form' => array(
								0 => array(
									'name' => 'type',
									'value' => $this->session->userdata('type')
								)
							)
						)
					);
				
					$data = $this->prepare_listing_data(implode(' AND ', $where_response['where_string_array']),array(), 0, 0, $where_response['where_array']);
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
					$temp_data = array();
					$currency_details =  array_column($this->common_model->get_dynamic_data_sales_db('currency_id, currency_rate', array('status'=>'Active'), 'currency'), 'currency_rate', 'currency_id');
					$currency_value = $currency_details[1];
					foreach ($data['production_list'] as $single_production_list) {
						
						// echo "<pre>";print_r($single_production_list);echo"</pre><hr>";die;
						if($single_production_list['total_status_wise'][$highchart_data_call_type]['value'] > 0){

							// $total = $single_production_list['total_status_wise'][$highchart_data_call_type]['value'];
							$total = round((((int)$single_production_list['total_status_wise'][$highchart_data_call_type]['value']) * ($currency_details[$single_production_list['currency']] / $currency_value)));
							
							if(empty($temp_data[$single_production_list['client_id']])){
								
								$temp_data[$single_production_list['client_id']] = array('name'=> $single_production_list['client_details']['name'], 'grand_total'=> (int)$total, 'client_id'=> $single_production_list['client_id']);
							} else {
								
								$temp_data[$single_production_list['client_id']]['grand_total'] += (int)$total;
							}
						}
					}
					// echo "<pre>";print_r($temp_data);echo"</pre><hr>";exit;
					$sort_temp_data_total_wise= array_column($temp_data, 'grand_total', 'client_id');

					arsort($sort_temp_data_total_wise);
					$i= 0;
					$start_count= 0;
					// echo "<pre>";print_r($sort_temp_data_total_wise);echo"</pre><hr>";exit;
					foreach ($sort_temp_data_total_wise as $client_id => $grand_total) {
						
						if($start_count <= 20) {

							$response['production_stage_wise_highchart'][$start_count]['name'] = ucfirst($temp_data[$client_id]['name']);
							$response['production_stage_wise_highchart'][$start_count]['y'] = (int)$grand_total;
							$response['production_stage_wise_highchart'][$start_count]['drilldown'] = ucfirst($temp_data[$client_id]['name']);
							$start_count++;
						}
					}
					$response['order_type'] = '';
					foreach (explode('_', $highchart_data_call_type) as $single_word) {
						$response['order_type'] .= ucfirst($single_word).' ';
					}
					// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
				break;

				case 'submit_work_order_sheet_data':

					// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;
					$response['work_order_sheet_id_status'] = "failed";
					$work_order_sheet_id = 0;
					$insert_data_for_item_details = $insert_data_for_special_note = array();
					$insert_data_for_quality_note = $insert_data_for_marking_comment = array();
					$insert_data_for_packaging_comment = $insert_data_for_revision_comment = array();
					if(empty($post_details['work_order_sheet_id'])){

						$insert_data_for_work_order_sheet = array();
						$insert_data_for_work_order_sheet = array_column($post_details['work_order_sheet'], 'value', 'name');
						if(!empty($insert_data_for_work_order_sheet['approved_by'])){

							$insert_data_for_work_order_sheet['approved_by_status'] = 'approved';
						}
						$insert_data_for_work_order_sheet['add_time'] = date('Y-m-d h:i:s');
						$insert_data_for_work_order_sheet['update_time'] = date('Y-m-d h:i:s');
						$insert_data_for_work_order_sheet['made_by'] = json_encode(array(array('name'=>$insert_data_for_work_order_sheet['made_by'], 'time'=>date('Y-m-d h:i:s'))));
						// echo "<pre>";print_r($insert_data_for_work_order_sheet);echo"</pre><hr>";exit;
						if(!empty($insert_data_for_work_order_sheet)){

							$work_order_sheet_id = $this->common_model->insert_data_sales_db('work_order_sheet', $insert_data_for_work_order_sheet);

							if(!empty($insert_data_for_work_order_sheet['approved_by'])){

								$this->common_model->update_data_sales_db('production_process_information', array('work_order_sheet_approved_by' => $insert_data_for_work_order_sheet['approved_by'],'work_order_sheet_approved_by_status'=> $insert_data_for_work_order_sheet['approved_by_status']), array('work_order_no'=> $insert_data_for_work_order_sheet['work_order_no']));
							}
						}
					}else{

						$work_order_sheet_id = $post_details['work_order_sheet_id'];
						$update_data_for_work_order_sheet = array();
						$update_data_for_work_order_sheet = array_column($post_details['work_order_sheet'], 'value', 'name');
						$update_data_for_work_order_sheet['update_time'] = date('Y-m-d h:i:s');
						$work_order_sheet_details = $this->common_model->get_dynamic_data_sales_db('*', array('id'=>$work_order_sheet_id, 'status'=>'Active'), 'work_order_sheet', 'row_array');
						$made_by_array = array();
						$made_by_array[] = array('name'=>$update_data_for_work_order_sheet['made_by'], 'time'=>date('Y-m-d h:i:s'));
						// if(!empty($work_order_sheet_details) && !empty($work_order_sheet_details['made_by'])){

						// 	$made_by_list = json_decode($work_order_sheet_details['made_by'], true);
						// 	foreach ($made_by_list as $made_by_details) {
							  	
						// 		$made_by_array[] = $made_by_details;
						// 	}
						// }
						$update_data_for_work_order_sheet['made_by'] = json_encode($made_by_array);
						if(empty($update_data_for_work_order_sheet['approved_by']) || (!empty($update_data_for_work_order_sheet['approved_by']) && !empty($work_order_sheet_details['approved_by']))){

							$update_data_for_work_order_sheet['approved_by'] = 0;
							$update_data_for_work_order_sheet['approved_by_status'] = 'revised';

						}else if(!empty($update_data_for_work_order_sheet['approved_by'])){

							$update_data_for_work_order_sheet['approved_by_status'] = 'approved';
						}
						if(!empty($update_data_for_work_order_sheet)) {

							$this->common_model->update_data_sales_db('work_order_sheet', $update_data_for_work_order_sheet, array('id'=> $work_order_sheet_id));

							$this->common_model->update_data_sales_db('production_process_information', array('work_order_sheet_approved_by'=>$update_data_for_work_order_sheet['approved_by'],
							'work_order_sheet_approved_by_status'=>$update_data_for_work_order_sheet['approved_by_status']), array('work_order_no'=> $update_data_for_work_order_sheet['work_order_no']));

						}
						$this->common_model->update_data_sales_db('work_order_sheet_item_details', array('status'=>'Inactive', 'update_time'=>date('Y-m-d h:i:s')), array('work_order_sheet_id'=> $work_order_sheet_id));

						$this->common_model->update_data_sales_db('work_order_sheet_special_note', array('status'=>'Inactive', 'update_time'=>date('Y-m-d h:i:s')), array('work_order_sheet_id'=> $work_order_sheet_id));

						$this->common_model->update_data_sales_db('work_order_sheet_quality_note', array('status'=>'Inactive', 'update_time'=>date('Y-m-d h:i:s')), array('work_order_sheet_id'=> $work_order_sheet_id));

						$this->common_model->update_data_sales_db('work_order_sheet_marking_comment', array('status'=>'Inactive', 'update_time'=>date('Y-m-d h:i:s')), array('work_order_sheet_id'=> $work_order_sheet_id));

						$this->common_model->update_data_sales_db('work_order_sheet_packaging_comment', array('status'=>'Inactive', 'update_time'=>date('Y-m-d h:i:s')), array('work_order_sheet_id'=> $work_order_sheet_id));
					}

					if(!empty($work_order_sheet_id)){

						$response['work_order_sheet_id_status'] = "success";
						$line_item_data = array_column($post_details['work_order_item_details'], 'value', 'name');
						// echo "<pre>";print_r($line_item_data);echo"</pre><hr>";die;
						for ($i=1; $i < ((count($line_item_data)/4)+1); $i++) { 
							
							if(
								!empty($line_item_data['po_item_description_'.$i]) || 
								!empty($line_item_data['wo_item_description_'.$i]) || 
								!empty($line_item_data['po_quantity_'.$i]) || 
								!empty($line_item_data['wo_quantity_'.$i])
							){

								$insert_data_for_item_details[$i]['work_order_sheet_id'] = $work_order_sheet_id;
								$insert_data_for_item_details[$i]['po_item_description'] = $line_item_data['po_item_description_'.$i];
								$insert_data_for_item_details[$i]['wo_item_description'] = $line_item_data['wo_item_description_'.$i];
								$insert_data_for_item_details[$i]['po_quantity'] = $line_item_data['po_quantity_'.$i];
								$insert_data_for_item_details[$i]['wo_quantity'] = $line_item_data['wo_quantity_'.$i];
								$insert_data_for_item_details[$i]['add_time'] = date('Y-m-d h:i:s');
								$insert_data_for_item_details[$i]['update_time'] = date('Y-m-d h:i:s');
							}
						}
						if(!empty($insert_data_for_item_details)){

							$this->common_model->insert_data_sales_db('work_order_sheet_item_details', $insert_data_for_item_details, 'batch');
						}
						if(!empty($post_details['work_order_special_notes'])) {
							foreach ($post_details['work_order_special_notes'] as $special_notes_details) {
								if(!empty($special_notes_details['value'])){

									$insert_data_for_special_note[] = array(
																	'work_order_sheet_id' => $work_order_sheet_id,
																	'notes'=> $special_notes_details['value'],
																	'add_time' => date('Y-m-d h:i:s'),
																	'update_time' => date('Y-m-d h:i:s'),
																);
								}
							}
						}
						if(!empty($insert_data_for_special_note)){

							$this->common_model->insert_data_sales_db('work_order_sheet_special_note', $insert_data_for_special_note, 'batch');
						}
						if(!empty($post_details['work_order_quality_notes'])) {
							foreach ($post_details['work_order_quality_notes'] as $quality_notes_details) {
								if(!empty($quality_notes_details['value']) && $quality_notes_details['value'] =='on'){

									$insert_data_for_quality_note[] = array(
																	'work_order_sheet_id' => $work_order_sheet_id,
																	'quality_id'=> $quality_notes_details['name'],
																	'add_time' => date('Y-m-d h:i:s'),
																	'update_time' => date('Y-m-d h:i:s'),
																);
								}
							}
						}
						if(!empty($insert_data_for_quality_note)){

							$this->common_model->insert_data_sales_db('work_order_sheet_quality_note', $insert_data_for_quality_note, 'batch');
						}
						if(!empty($post_details['work_order_marking_comment'])) {
							foreach ($post_details['work_order_marking_comment'] as $marking_comment_details) {
								if(!empty($marking_comment_details['value'])){

									$insert_data_for_marking_comment[] = array(
																	'work_order_sheet_id' => $work_order_sheet_id,
																	'comments'=> $marking_comment_details['value'],
																	'add_time' => date('Y-m-d h:i:s'),
																	'update_time' => date('Y-m-d h:i:s'),
																);
								}
							}
						}
						if(!empty($insert_data_for_marking_comment)){

							$this->common_model->insert_data_sales_db('work_order_sheet_marking_comment', $insert_data_for_marking_comment, 'batch');
						}
						if(!empty($post_details['work_order_packaging_comment'])) {
							foreach ($post_details['work_order_packaging_comment'] as $packaging_comment_details) {
								if(!empty($packaging_comment_details['value'])){

									$insert_data_for_packaging_comment[] = array(
																	'work_order_sheet_id' => $work_order_sheet_id,
																	'comments'=> $packaging_comment_details['value'],
																	'add_time' => date('Y-m-d h:i:s'),
																	'update_time' => date('Y-m-d h:i:s'),
																);
								}
							}
						}
						if(!empty($insert_data_for_packaging_comment)){

							$this->common_model->insert_data_sales_db('work_order_sheet_packaging_comment', $insert_data_for_packaging_comment, 'batch');
						}
						if(!empty($post_details['work_order_revision_comment'])) {
							foreach ($post_details['work_order_revision_comment'] as $revision_comment_details) {
								if(!empty($revision_comment_details['value'])){

									$insert_data_for_revision_comment[] = array(
																		'work_order_sheet_id' => $work_order_sheet_id,
																		'comments'=> $revision_comment_details['value'],
																		'user_id' => $this->session->userdata('user_id'),
																		'add_time' => date('Y-m-d h:i:s'),
																		'update_time' => date('Y-m-d h:i:s'),
																	);
								}
							}
						}
						if(!empty($insert_data_for_revision_comment)){

							$this->common_model->insert_data_sales_db('work_order_sheet_revision_comment', $insert_data_for_revision_comment, 'batch');
						}

					}
					// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
				break;

				case 'work_order_sheet_add_item':
					
					$response['table_body_html'] = $response['next_increment_number'] = '';
					if(!empty($post_details['number'])){
						$response['table_body_html'] = '
							<tr>
								<td class="first_div" style="width: 100px; padding: 0px 0px 0px 15px;">'.$post_details['number'].'</td>
								<td style="width: 300px; padding: 0px 0px 0px 15px;">
									<span>
								        <abbr>
								            <em class="kt-font-bolder">
								            	<textarea class="form-control work_order_sheet_autosize" name="po_item_description_'.$post_details['number'].'" rows="3"></textarea>
								      		</em> 
								    	</abbr>
									</span>
								</td>
								<td style="width: 300px; padding: 0px 0px 0px 15px;">
									<span>
								        <abbr>
								            <em class="kt-font-bolder">
								            	<textarea class="form-control work_order_sheet_autosize" name="wo_item_description_'.$post_details['number'].'" rows="3"></textarea>
								      		</em> 
								    	</abbr>
									</span>
								</td>
								<td style="width: 300px; padding: 0px 0px 0px 15px;">
									<span>
								        <abbr>
								            <em class="kt-font-bolder">
								            	<textarea class="form-control work_order_sheet_autosize" name="po_quantity_'.$post_details['number'].'" rows="3"></textarea>
								      		</em> 
								    	</abbr>
									</span>
								</td>
								<td style="width: 300px; padding: 0px 0px 0px 15px;">
									<span>
								        <abbr>
								            <em class="kt-font-bolder">
								            	<textarea class="form-control work_order_sheet_autosize" name="wo_quantity_'.$post_details['number'].'" rows="3"></textarea>
								      		</em> 
								    	</abbr>
									</span>
								</td>
							</tr>';
						$response['next_increment_number'] = $post_details['number']+1;
					}
				break;

				case 'work_order_sheet_add_notes_or_comments':
					
					$response['table_body_html'] = $response['next_increment_number'] = '';
					if(!empty($post_details['name']) && !empty($post_details['number'])){

						$response['table_body_html'] = '<tr>
											<td class="first_div" style="width: 30px; padding: 0px 0px 0px 15px;">'.$post_details['number'].'</td>
											<td style="width: 300px; padding: 0px 0px 0px 15px;">
												<span>
											        <abbr>
											            <em class="kt-font-bolder">
											            	<textarea class="form-control work_order_sheet_autosize" name="'.$post_details['name'].'_'.$post_details['number'].'" rows="1"></textarea>
											      		</em> 
											    	</abbr>
												</span>
											</td>
										</tr>';
						$response['next_increment_number'] = $post_details['number']+1;				
					}
				break;

				case 'work_order_sheet_add_quality_notes':
					
					$response['table_body_html'] = $response['next_increment_number'] = '';
					if(!empty($post_details['note'])) {
						$id = $this->common_model->insert_data_sales_db('work_order_sheet_quality_note_master', array('notes'=> $post_details['note']));
						$response['table_body_html'] = '<tr>
											<th scope="row" style=" border: 0.1rem solid black;">'.$post_details['number'].'</th>
											<td style=" border: 0.1rem solid black;">
												<label class="kt-checkbox kt-font-bolder kt-font-dark">
													<input type="checkbox" name="'.$id.'" checked>'.$post_details['note'].'
													<span></span>
												</label>
											</td>
										</tr>';
						$response['next_increment_number'] = $post_details['number']+1;
					}	
				break;

				case 'acknowledge':
					
					if(!empty($post_details['quotation_mst_id'])){

						$this->common_model->update_data_sales_db('quotation_mst', array('yet_to_acknowledge'=>'done'), array('quotation_mst_id'=>$post_details['quotation_mst_id']));
					}
				break;

				case 'sales_acknowledge':
					
					if(!empty($post_details['quotation_mst_id'])){

						$this->common_model->update_data_sales_db('quotation_mst', array('acknowledge'=>'done'), array('quotation_mst_id'=>$post_details['quotation_mst_id']));
					}
				break;
				
				case 'get_star_rating':
					
					$rating_details = $this->common_model->get_dynamic_data_sales_db('rating_comment, rating_value', array('id'=>$post_details['production_id']), 'production_process_information', 'row_array');
					$response['rating_html'] = $this->load->view('production/rating_comment_div', $rating_details, true); 
					$response['id'] = $post_details['production_id'];
				break;	

				case 'save_star_rating':
					
					$this->common_model->update_data_sales_db('production_process_information', array('rating_comment' => $post_details['rating_comment'], 'rating_value' => $post_details['rating_value']), array('id' => $post_details['production_id']));
				break;

				case 'get_client_name_production':

					$search_client_name = $post_details['client_name'];
					$client_details = $this->production_model->get_production_listingz_client_list_data($search_client_name);
					$response['client_list_html'] = '<option value="">Select Name</option>';
					foreach($client_details as $single_details){

                        $response['client_list_html'] .= '<option value="'.$single_details['client_id'].'">'.$single_details['name'].'</option>';
                    }
				break;
				
				case 'get_file_upload_history_tab_wise':
					
					$upload_date_history = array(
						'type_name'=> $post_details['type_name'],
						'file_type'=> $post_details['file_type'],
						'call_type'=> 'filter',
						'date_list'=> array()
					);
					$upload_image_history = array(
						'call_type'=> 'upload_history',
						'file_type_array'=> array(),
						'upload_history'=> array()
					);
					
					if(!empty($post_details['type_name']) && !empty($post_details['file_type']) && !empty($post_details['work_order_no'])){

						switch ($post_details['type_name']) {
							case 'drawing':
								
								$upload_image_history['file_type_array'] = array(
									'vendor'=> array('name'=> 'Vendor', 'class'=> '', 'type_name'=> 'drawing', 'file_type'=> 'vendor', 'work_order_no'=> $post_details['work_order_no']),
									'client'=> array('name'=> 'Client', 'class'=> '', 'type_name'=> 'drawing', 'file_type'=> 'client', 'work_order_no'=> $post_details['work_order_no'])
								);
							break;

							case 'marking':
								
								$upload_image_history['file_type_array'] = array(
									'marking'=> array('name'=> 'Marking', 'class'=> '', 'type_name'=> 'marking', 'file_type'=> 'marking', 'work_order_no'=> $post_details['work_order_no'])
								);
							break;
							
							case 'mtc':
								
								$upload_image_history['file_type_array'] = array(
									'mtc'=> array('name'=> 'MTC', 'class'=> '', 'type_name'=> 'mtc', 'file_type'=> 'mtc', 'work_order_no'=> $post_details['work_order_no'])
								);
							break;
							
							case 'logistics_invoice_and_pl':

								$upload_image_history['file_type_array'] = array(
									'invoice'=> array('name'=> 'Invoice', 'class'=> '', 'type_name'=> 'logistics_invoice_and_pl', 'file_type'=> 'invoice', 'work_order_no'=> $post_details['work_order_no']),
									'proforma_invoice'=> array('name'=> 'Proforma Invoice', 'class'=> '', 'type_name'=> 'logistics_invoice_and_pl', 'file_type'=> 'proforma_invoice', 'work_order_no'=> $post_details['work_order_no'])
								);
							break;
							
							case 'quality_inspection':

								$upload_image_history['file_type_array'] = array(
									'pmi_photos'=> array('name'=> 'PMI Photos', 'class'=> '', 'type_name'=> 'quality_inspection', 'file_type'=> 'pmi_photos', 'work_order_no'=> $post_details['work_order_no']),
									'dimension_photos'=> array('name'=> 'Dimension Photos', 'class'=> '', 'type_name'=> 'quality_inspection', 'file_type'=> 'dimension_photos', 'work_order_no'=> $post_details['work_order_no']),
									'marking_photos'=> array('name'=> 'Marking Photos', 'class'=> '', 'type_name'=> 'quality_inspection', 'file_type'=> 'marking_photos', 'work_order_no'=> $post_details['work_order_no']),
									'finishing_photos'=> array('name'=> 'Finishing Photos', 'class'=> '', 'type_name'=> 'quality_inspection', 'file_type'=> 'finishing_photos', 'work_order_no'=> $post_details['work_order_no']),
									'packing_and_dispatch'=> array('name'=> 'Packing & Dispatch Photos', 'class'=> '', 'type_name'=> 'quality_inspection', 'file_type'=> 'packing_and_dispatch', 'work_order_no'=> $post_details['work_order_no']),
									'reports_pdf'=> array('name'=> 'Reports PDF', 'class'=> '', 'type_name'=> 'quality_inspection', 'file_type'=> 'reports_pdf', 'work_order_no'=> $post_details['work_order_no'])
								);
							break;
						}
						$upload_image_history['file_type_array'][$post_details['file_type']]['class'] = 'active';
						$upload_date_history['date_list'] = $this->common_model->get_all_conditional_data_sales_db('date(add_time) as date, "" as checked', array('status'=> "Active", 'upload_status'=> 'Done', 'task_type'=> $post_details['type_name'], 'file_type'=> $post_details['file_type'], 'work_order_no'=> $post_details['work_order_no']), 'google_cloud_upload_logs', 'result_array', array(), array('column_name'=> 'add_time', 'column_value'=> 'desc'), array('date(add_time)'));
						// echo "<pre>";print_r($upload_date_history);echo"</pre><hr>";die;

						if(!empty($upload_date_history['date_list'])){

							$date = $upload_date_history['date_list'][0]['date'];
							if(!empty($post_details['date'])){

								$date = $post_details['date'];
							}
							foreach ($upload_date_history['date_list'] as $date_list_key => $single_date_list) {
								
								if($single_date_list['date'] == $date){

									$upload_date_history['date_list'][$date_list_key]['checked'] = "checked";
								}
								$upload_date_history['date_list'][$date_list_key]['work_order_no'] = $post_details['work_order_no'];
							}
							$upload_history = $this->common_model->get_all_conditional_data_sales_db('file_name, add_time', array('status'=> "Active", 'upload_status'=> 'Done', 'task_type'=> $post_details['type_name'], 'file_type'=> $post_details['file_type'], 'work_order_no'=> $post_details['work_order_no'], 'date(add_time)'=> $date), 'google_cloud_upload_logs', 'result_array', array(), array('column_name'=> 'add_time', 'column_value'=> 'desc'));
							// echo $this->common_model->db->last_query(),"</hr>";die('debug');
							// if(!empty($upload_history['date_list'])){
								foreach ($upload_history as $upload_history_details) {
										
									$upload_image_history['upload_history'][date('Y-m-d', strtotime($upload_history_details['add_time']))][] = array(
										'file_html'=> $this->add_image_html_based_on_document_type($upload_history_details['file_name'])
									);
								}
							// }
						}
					}

					// echo "<pre>";print_r($upload_image_history);echo"</pre><hr>";exit;
					$response['upload_date_history'] = $this->load->view('production/file_upload_history_tab_wise', $upload_date_history, true);
					$response['file_upload_history_tab_wise'] = $this->load->view('production/file_upload_history_tab_wise', $upload_image_history, true);
				break;
				case 'get_all_file_upload_history':
					
					$data = array('upload_history' => array());
					if(!empty($post_details['work_order_no'])){

						$upload_history = $this->common_model->get_dynamic_data_sales_db('*', array('status'=> "Active", 'upload_status'=> 'Done', 'task_type'=> 'drawing', 'work_order_no'=> $post_details['work_order_no']), 'google_cloud_upload_logs');
						// echo "<pre>";print_r($upload_history);echo"</pre><hr>";exit;
						foreach ($upload_history as $upload_history_details) {
							
							$data['upload_history'][date('Y-m-d', strtotime($upload_history_details['add_time']))][] = array(
								'file_name'=> $upload_history_details['file_name']
							);
						}
					}
					
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
					$response['all_file_upload_history'] = $this->load->view('production/all_file_upload_history', $data, true);
				break;
				case 'get_all_file_upload_history_new':
					
					$main_tab = array(
						'call_type'=> 'main_tab',
						'main_tab'=> array(
							'all'=> array(
										'background_color'=> '#fff',
										'border'=> '1px solid #dadce0',
										'image_src'=> '',
										'name'=> 'All',
										'task_type'=> 'all',
										'sub_task_type'=> 'all',
										'work_order_no'=> $post_details['work_order_no']
									),
							'drawing'=>	array(
										'background_color'=> '#fff',
										'border'=> '1px solid #dadce0',
										'image_src'=> '',
										'name'=> 'Drawing',
										'task_type'=> 'drawing',
										'sub_task_type'=> 'all',
										'work_order_no'=> $post_details['work_order_no']
									),
							'marking'=> array(
										'background_color'=> '#fff',
										'border'=> '1px solid #dadce0',
										'image_src'=> '',
										'name'=> 'Marking',
										'task_type'=> 'marking',
										'sub_task_type'=> 'all',
										'work_order_no'=> $post_details['work_order_no']
									),
							'mtc'=> array(
										'background_color'=> '#fff',
										'border'=> '1px solid #dadce0',
										'image_src'=> '',
										'name'=> 'MTC',
										'task_type'=> 'mtc',
										'sub_task_type'=> 'all',
										'work_order_no'=> $post_details['work_order_no']
									),
							'logistics_invoice_and_pl'=> array(
										'background_color'=> '#fff',
										'border'=> '1px solid #dadce0',
										'image_src'=> '',
										'name'=> 'Logistics - Invoice & PL',
										'task_type'=> 'logistics_invoice_and_pl',
										'sub_task_type'=> 'all',
										'work_order_no'=> $post_details['work_order_no']
									),
							'quality_inspection'=> array(
										'background_color'=> '#fff',
										'border'=> '1px solid #dadce0',
										'image_src'=> '',
										'name'=> 'Quality Inspection',
										'task_type'=> 'quality_inspection',
										'sub_task_type'=> 'all',
										'work_order_no'=> $post_details['work_order_no']
									)
						)
					);
					$get_all_type_image = $this->common_model->get_all_conditional_data_sales_db('task_type, file_name', array('status'=> "Active", 'upload_status'=> 'Done', 'work_order_no'=> $post_details['work_order_no']), 'google_cloud_upload_logs', 'result_array', array(), array(), array('task_type'));
					foreach ($get_all_type_image as $get_all_type_image_details) {

						if(empty($main_tab['main_tab']['all']['image_src'])){

							$main_tab['main_tab']['all']['image_src'] = "https://storage.googleapis.com/documentation_bucket_name/".$get_all_type_image_details['file_name'];
						}
						switch ($get_all_type_image_details['task_type']) {
							case 'drawing':
								
								if(empty($main_tab['main_tab']['drawing']['image_src'])){

									$main_tab['main_tab']['drawing']['image_src'] = "https://storage.googleapis.com/documentation_bucket_name/".$get_all_type_image_details['file_name'];
								}
							break;
							case 'marking':
								
								if(empty($main_tab['main_tab']['marking']['image_src'])){

									$main_tab['main_tab']['marking']['image_src'] = "https://storage.googleapis.com/documentation_bucket_name/".$get_all_type_image_details['file_name'];
								}
							break;
							case 'mtc':
								
								if(empty($main_tab['main_tab']['mtc']['image_src'])){

									$main_tab['main_tab']['mtc']['image_src'] = "https://storage.googleapis.com/documentation_bucket_name/".$get_all_type_image_details['file_name'];
								}
							break;
							case 'logistics_invoice_and_pl':
								
								if(empty($main_tab['main_tab']['logistics_invoice_and_pl']['image_src'])){

									$main_tab['main_tab']['logistics_invoice_and_pl']['image_src'] = "https://storage.googleapis.com/documentation_bucket_name/".$get_all_type_image_details['file_name'];
								}
							break;
							case 'quality_inspection':
								
								if(empty($main_tab['main_tab']['quality_inspection']['image_src'])){

									$main_tab['main_tab']['quality_inspection']['image_src'] = "https://storage.googleapis.com/documentation_bucket_name/".$get_all_type_image_details['file_name'];
								}
							break;
						}
					}

					$sub_tab = array(
						'call_type'=> 'sub_tab',
						'sub_tab'=> array()
					);

					switch ($post_details['task_type']) {
						case 'drawing':
								
							$sub_tab['sub_tab']['vendor'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'Vendor',
								'task_type'=> 'vendor',
								'work_order_no'=> $post_details['work_order_no']
							);
							$sub_tab['sub_tab']['client'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'Client',
								'task_type'=> 'client',
								'work_order_no'=> $post_details['work_order_no']
							);
						break;
						case 'marking':
							
							$sub_tab['sub_tab']['marking'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'Marking',
								'task_type'=> 'marking',
								'work_order_no'=> $post_details['work_order_no']
							);
						break;
						case 'mtc':
							
							$sub_tab['sub_tab']['mtc'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'MTC',
								'task_type'=> 'mtc',
								'work_order_no'=> $post_details['work_order_no']
							);
						break;
						case 'logistics_invoice_and_pl':
							
							$sub_tab['sub_tab']['invoice'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'Invoice',
								'task_type'=> 'invoice',
								'work_order_no'=> $post_details['work_order_no']
							);
							$sub_tab['sub_tab']['proforma_invoice'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'Proforma Invoice',
								'task_type'=> 'proforma_invoice',
								'work_order_no'=> $post_details['work_order_no']
							);
						break;
						case 'quality_inspection':
							
							$sub_tab['sub_tab']['all'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'All',
								'task_type'=> $post_details['task_type'],
								'sub_task_type'=> 'all',
								'work_order_no'=> $post_details['work_order_no']
							);
							$sub_tab['sub_tab']['pmi_photos'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'PMI Photos',
								'task_type'=> $post_details['task_type'],
								'sub_task_type'=> 'pmi_photos',
								'work_order_no'=> $post_details['work_order_no']
							);
							$sub_tab['sub_tab']['dimension_photos'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'Dimension Photos',
								'task_type'=> $post_details['task_type'],
								'sub_task_type'=> 'dimension_photos',
								'work_order_no'=> $post_details['work_order_no']
							);
							$sub_tab['sub_tab']['marking_photos'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'Marking Photos',
								'task_type'=> $post_details['task_type'],
								'sub_task_type'=> 'marking_photos',
								'work_order_no'=> $post_details['work_order_no']
							);
							$sub_tab['sub_tab']['finishing_photos'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'Finishing Photos',
								'task_type'=> $post_details['task_type'],
								'sub_task_type'=> 'finishing_photos',
								'work_order_no'=> $post_details['work_order_no']
							);
							$sub_tab['sub_tab']['packing_and_dispatch'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'Packing & Dispatch',
								'task_type'=> $post_details['task_type'],
								'sub_task_type'=> 'packing_and_dispatch',
								'work_order_no'=> $post_details['work_order_no']
							);
							$sub_tab['sub_tab']['reports_pdf'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'Reports PDF',
								'task_type'=> $post_details['task_type'],
								'sub_task_type'=> 'reports_pdf',
								'work_order_no'=> $post_details['work_order_no']
							);
						break;
						default:
						break;
					}
					$sidebar = array(
						'call_type'=> 'sidebar',
						'sidebar_photos'=> array()
					);
					$main_photos = array(
						'call_type'=> 'main_photos',
						'main_photos_src'=> '',
						'main_photos_height'=> '',
						'main_photos_html'=> ''
					);

					$update_history_where = "status = 'Active' AND upload_status = 'Done' AND work_order_no IN(".$post_details['work_order_no'].")";
					if($post_details['task_type'] != 'all'){

						$update_history_where .= " AND task_type = '".$post_details['task_type']."'";
						$sub_tab['sub_tab'][$post_details['sub_task_type']]['background_color'] = 'bisque';
						$sub_tab['sub_tab'][$post_details['sub_task_type']]['border'] = '1px solid bisque';
					}
					if($post_details['sub_task_type'] != 'all'){
						
						$update_history_where .= " AND file_type = '".$post_details['sub_task_type']."'";
					}
					$main_tab['main_tab'][$post_details['task_type']]['background_color'] = 'bisque';
					$main_tab['main_tab'][$post_details['task_type']]['border'] = '1px solid bisque';
					
					$upload_history = $this->common_model->get_dynamic_data_sales_db_null_false('file_name, "" as selected_class', $update_history_where, 'google_cloud_upload_logs', 'result_array');
					$sidebar_photos = array();
					// echo "<pre>";print_r($upload_history);echo"</pre><hr>";exit;
					foreach ($upload_history as $upload_history_key => $upload_history_details) {
						
						if($main_photos['main_photos_src'] == ''){

							$main_photos['main_photos_src'] = $upload_history_details['file_name'];
							$upload_history[$upload_history_key]['selected_class'] = 'active';
						}
					}
					
					// echo "<pre>";print_r($sidebar_photos);echo"</pre><hr>";exit;
					// echo "<pre>";print_r($sidebar);echo"</pre><hr>";exit;
					// 
					$response['sub_tab_html_height'] = "0px";
					$main_photos['main_photos_height'] = "700px";
					if(count($sub_tab['sub_tab']) > 0){

						if(count($sub_tab['sub_tab']) < 5){

							$response['sub_tab_html_height'] = "50px";
							$main_photos['main_photos_height'] = "650px";
						}else{
	
							$response['sub_tab_html_height'] = "100px";
							$main_photos['main_photos_height'] = "600px";
						}
					}
					$sidebar['sidebar_photos'] = $this->add_image_html_based_on_document_type($upload_history, $main_photos['main_photos_height']);
					$main_photos['main_photos_html'] = $this->main_photos_html($main_photos['main_photos_src'], $main_photos['main_photos_height']);


					// echo "<pre>";print_r($main_photos);echo"</pre><hr>";exit;
					$response['work_order_no'] = $post_details['work_order_no'];
					$response['main_tab_html'] = $this->load->view('production/view_all_document_modal', $main_tab, true);
					$response['sidebar_photos_html'] = $this->load->view('production/view_all_document_modal', $sidebar, true);
					$response['sub_tab_html'] = $this->load->view('production/view_all_document_modal', $sub_tab, true);
					$response['main_photos_html'] = $this->load->view('production/view_all_document_modal', $main_photos, true);
				break;

				case 'set_main_photos':
					
					$response['main_photos_html'] = $this->main_photos_html($post_details['file_name'], $post_details['main_photos_height']);
				break;

				case 'get_quotation_quantity_status':
					
					// echo "<pre>";print_r($post_details);echo"</pre><hr>";die;
					if(!empty($post_details['quotation_dtl_id'])){

						$quotation_details = $this->common_model->get_dynamic_data_sales_db('*', array('quotation_dtl_id'=> $post_details['quotation_dtl_id']), 'quotation_dtl', 'row_array');
						$response['quantity'] = $quotation_details['quantity'];
						$response['form_quotation_quantity_status_html'] = '
						<div class="row">
							<div class="col-xl-2 form-group validated">
								<label class="quotation_details_title">Pending:</label>
								<input type="text" class="form-control quotation_details_title" value="'.($quotation_details['quantity']- ($quotation_details['semi_ready_count'] + $quotation_details['ready_to_dispatch_order_count'] + $quotation_details['dispatch_order_count'] + $quotation_details['on_hold_order_count'] + $quotation_details['mtt_count'] + $quotation_details['import_count'] + $quotation_details['query_count'] + $quotation_details['mtt_rfd_count'] + $quotation_details['order_cancelled_count'])).'" id="pending_count" readonly>
							</div>
							<div class="col-xl-2 form-group validated">
								<label class="quotation_details_title">Semi Ready:</label>
								<input type="text" class="form-control quotation_details_title input_quantity " name="semi_ready_count" value="'.$quotation_details['semi_ready_count'].'">
								<span class="form-text semi_ready_count_span"></span>
							</div>
							<div class="col-xl-2 form-group validated">
								<label class="quotation_details_title">ICE:</label>
								<input type="text" class="form-control quotation_details_title input_quantity" name="mtt_count" value="'.$quotation_details['mtt_count'].'">
								<span class="form-text mtt_count_span"></span>
							</div>
							<div class="col-xl-2 form-group validated">
								<label class="quotation_details_title">Import:</label>
								<input type="text" class="form-control quotation_details_title input_quantity" name="import_count" value="'.$quotation_details['import_count'].'">
								<span class="form-text import_count_span"></span>
							</div>
							<div class="col-xl-2 form-group validated">
								<label class="quotation_details_title">Query:</label>
								<input type="text" class="form-control quotation_details_title input_quantity" name="query_count" value="'.$quotation_details['query_count'].'">
								<span class="form-text query_count_span"></span>
							</div>
							<div class="col-xl-2 form-group validated">
								<label class="quotation_details_title">In transit ICE:</label>
								<input type="text" class="form-control quotation_details_title input_quantity" name="mtt_rfd_count" value="'.$quotation_details['mtt_rfd_count'].'">
								<span class="form-text mtt_rfd_count_span"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-2 form-group validated">
								<label class="quotation_details_title">Ready To Dispatch:</label>
								<input type="text" class="form-control quotation_details_title input_quantity" name="ready_to_dispatch_order_count" value="'.$quotation_details['ready_to_dispatch_order_count'].'">
								<span class="form-text ready_to_dispatch_order_count_span"></span>
							</div>
							<div class="col-xl-2 form-group validated">
								<label class="quotation_details_title">Dispatch:</label>
								<input type="text" class="form-control quotation_details_title input_quantity" name="dispatch_order_count" value="'.$quotation_details['dispatch_order_count'].'">
								<span class="form-text dispatch_order_count_span"></span>
							</div>
							<div class="col-xl-2 form-group validated">
								<label class="quotation_details_title">Hold:</label>
								<input type="text" class="form-control quotation_details_title input_quantity" name="on_hold_order_count" value="'.$quotation_details['on_hold_order_count'].'">
								<span class="form-text on_hold_order_count_span"></span>
							</div>
							<div class="col-xl-2 form-group validated">
								<label class="quotation_details_title">Cancel:</label>
								<input type="text" class="form-control quotation_details_title input_quantity" name="order_cancelled_count" value="'.$quotation_details['order_cancelled_count'].'">
								<span class="form-text order_cancelled_count_span"></span>
							</div>
						</div>';
					}
				break;
				case 'save_quotation_quantity_status':
					
					$update_array = array();

					// if(!empty($this->input->post('quotation_dtl_id'))) {
					// 	//getting quotation dtl data
					// 	$quotation_details_data = $this->common_model->get_dynamic_data_sales_db('*', array('quotation_dtl_id'=>$this->input->post('quotation_dtl_id')), 'quotation_dtl', 'row_array');
					// 	// echo "<pre>";print_r($quotation_details_data);echo"</pre><hr>";
					// 	if(!empty($quotation_details_data)) {
					// 		if($quotation_details_data['production_status'] != 'pending_order') {

					// 			$this->add_remove_production_count($quotation_details_data['quotation_mst_id'], $quotation_details_data['production_status'], 'remove');	
					// 		}
					// 		$this->add_remove_production_count($quotation_details_data['quotation_mst_id'], $this->input->post('production_status'), 'add');
					// 	}
					// }

					// echo "<pre>";print_r($post_details);echo"</pre><hr>";die;
					if(!empty($post_details['quotation_dtl_id']) && !empty($post_details['form_data'])){

						$update_array = array();
						foreach ($post_details['form_data'] as $single_form_data) {
							
							$update_array[$single_form_data['name']] = $single_form_data['value'];
						}
						$this->common_model->update_data_sales_db('quotation_dtl', $update_array, array('quotation_dtl_id' => $post_details['quotation_dtl_id']));
					}
				break;

				case 'get_technical_document_upload_history':

					$production_id = (!empty($this->input->post('production_id'))) ? $this->input->post('production_id') : $this->session->userdata('production_id_for_document_upload');
					if(!empty($production_id)) {

						$technical_documents_details = $this->common_model->get_dynamic_data_sales_db('id, technical_document_file_and_path',array('id'=>$production_id), 'production_process_information', 'row_array');

						if(!empty($technical_documents_details)){

							$response['production_technical_document_history'] = $this->load->view('production/document_pdf_table', array('document_details'=> $technical_documents_details), true);
						}
						$this->session->set_userdata('production_id_for_document_upload', $production_id);
					}
				break;

				case 'technical_document_upload':

					$response['msg'] = 'File is uploaded successfully!!!';
					$return_response['status'] = '';
					$production_id = $this->session->userdata('production_id_for_document_upload');
					if(!empty($production_id)){

						$return_response = $this->upload_technical_doc_config();
					}
					$response['status'] = $return_response['status'];
					if($return_response['status'] == 'successful') {

						$document_path = array();
						$technical_document_details = $this->common_model->get_dynamic_data_sales_db('technical_document_file_and_path',array('id'=> $production_id), 'production_process_information', 'row_array');

						if(!empty($technical_document_details)){

							$document_path = json_decode($technical_document_details['technical_document_file_and_path'], true);
							$document_path[] = array('file_type' => $return_response['file_type'], 'file_name' => $return_response['file_name'], 'file_add_date' => $return_response['file_add_date']);

							$this->common_model->update_data_sales_db('production_process_information', array('technical_document_file_and_path'=> json_encode($document_path)), array('id'=>$production_id));
						}
						// echo "<pre>";print_r($rfq_file_path);echo"</pre><hr>";exit;
					}else {

						$response['msg'] = 'File is not uploaded.';
					}
				break;

				case 'technical_document_delete':

					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					if(!empty($this->input->post('production_id')) && !empty($this->input->post('file_name'))){

						$file_details = $this->common_model->get_dynamic_data_sales_db('technical_document_file_and_path', array('id'=> $this->input->post('production_id')), 'production_process_information', 'row_array');

						if(!empty($file_details)){

							$arrayData = json_decode($file_details['technical_document_file_and_path'], true);
							foreach($arrayData as $single_array){

								if($single_array['file_name'] !== $this->input->post('file_name')){

									$filter_array[] = $single_array;
								}
							}

							if(empty($filter_array)){

								$this->common_model->update_data_sales_db('production_process_information', array('technical_document_file_and_path' => ''), array('id'=> $this->input->post('production_id')));
							}else{

								$this->common_model->update_data_sales_db('production_process_information', array('technical_document_file_and_path' => json_encode($filter_array)), array('id'=> $this->input->post('production_id')));
							}
						}
					}
				break;

				case 'get_chat_conversation':

					$response['chat_history'] = '';
					if(!empty($this->input->post('id'))){

						$chat_details = $this->common_model->get_dynamic_data_sales_db('work_order_no, latest_update', array('id'=> $this->input->post('id')), 'production_process_information', 'row_array');
						// echo "<pre>";print_r($chat_details);echo"</pre><hr>";exit;
						if(!empty($chat_details)){

							$data['chat_details'] = array();
							$user_details = $this->common_model->get_all_conditional_data_sales_db(
								'users.user_id, users.name, people_information.profile_pic_file_path',
								array(),
								'users',
								'result_array',
								array(
									'table_name'=> 'people_information',
									'condition'=> 'people_information.user_id = users.user_id',
									'type'=> 'LEFT'
								)
							);
							foreach ($user_details as $single_user_details) {

								$user_information[$single_user_details['user_id']]['name'] = $single_user_details['name'];
								$user_information[$single_user_details['user_id']]['profile_pic_file_path'] = $single_user_details['profile_pic_file_path'];
							}
							// echo "<pre>";print_r($user_information);echo"</pre><hr>";
							foreach (json_decode($chat_details['latest_update'], true) as $chat_key => $single_user_details) {

								// echo "<pre>";print_r($single_user_details);echo"</pre><hr>";exit;
								$data['chat_details'][$chat_key]['profile_path'] = "assets/media/users/default.jpg";
								if(!empty($user_information[$single_user_details['user_id']]['profile_pic_file_path'])){

									$data['chat_details'][$chat_key]['profile_path'] = "assets/hr_document/profile_pic/".$user_information[$single_user_details['user_id']]['profile_pic_file_path'];
								}
								$data['chat_details'][$chat_key]['user_name'] = $user_information[$single_user_details['user_id']]['name'];
								$data['chat_details'][$chat_key]['tat'] = $this->creat_tat($single_user_details['date_time']);
								$data['chat_details'][$chat_key]['message'] = $single_user_details['message'];
							}

							// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
							$response['chat_history'] = $this->load->view('chat_history_production_person', $data, true);
							$response['work_order_no'] = $chat_details['work_order_no'];
						}
					}
				break;

				case 'production_client_wise_highchart_data':
					if(!empty($this->input->post('highchart_data_call_type'))){

						$where_string = "quotation_mst.stage = 'proforma' AND quotation_mst.status = 'Won' AND
							quotation_mst.work_order_no != 0 AND quotation_mst.client_id > 0 AND
							quotation_mst.client_id is not null AND (quotation_mst.acknowledge = 'done' OR quotation_mst.yet_to_acknowledge = 'done') AND
							(production_process_information.production_status IS NULL
								OR production_process_information.production_status IN ('yet_to_start_production', 'in_production', 'pending_order', 'semi_ready', 'merchant_trade', 'mtt', 'import', 'query', 'mtt_rfd', 'in_transit_import', 'in_transit_mtt', 'ready_for_dispatch')
							) AND
							(production_process_information.status IS NULL OR production_process_information.status = 'Active')";

						if(!empty($this->session->userdata('type'))){
							$where_string .= " AND production_process_information.type IN ('".$this->session->userdata('type')."')";
						}

						$currency_icon = array_column($this->common_model->get_dynamic_data_sales_db('currency_id, currency_icon', array('status' => 'Active'), 'currency'), 'currency_icon', 'currency_id');
						$currency_icon['']='';
						$currency_icon[0]='';

						$data = $this->production_model->get_quotation_client_id($where_string);
						// echo "<pre>";print_r($data);echo"</pre><hr>";exit;

						$temp_data = array();
						$return_data = array();
						foreach($data['client_list'] as $client_id_key => $single_client_id){

							$return_data = $this->production_model->get_total_sum_for_client($single_client_id['client_id'], $this->session->userdata('type'));
							$currency_rate = array_column($this->common_model->get_dynamic_data_sales_db('currency_id, currency_rate', array('status' => 'Active'), 'currency'), 'currency_rate', 'currency_id');
							$currency_rate['']='';
							$currency_rate[0]='';

							$grand_total = $convert_total = 0;
							foreach($return_data as $single_return_data){

								if($single_return_data['currency'] == '3'){

									$grand_total = (int)($single_return_data['total_sum'] / $currency_rate[1]);
									$temp_data[$single_client_id['name']]['currency_icon'] = $currency_icon[1];

								}else if($single_return_data['currency'] == '1'){

									$grand_total  = $single_return_data['total_sum'];
									$temp_data[$single_client_id['name']]['currency_icon'] = $currency_icon[$single_return_data['currency']];
								}else{

									$convert_total = ($single_return_data['total_sum'] * $currency_rate[$single_return_data['currency']]);
									$grand_total = (int)($convert_total / $currency_rate[1]);
									$temp_data[$single_client_id['name']]['currency_icon'] = $currency_icon[1];

								}
								$temp_data[$single_client_id['name']]['grand_total'] += $grand_total;
							}
						}

						$sort_temp_data_total_wise = $temp_data;
						arsort($sort_temp_data_total_wise);
						$i= 0;
						$start_count= 0;

						foreach ($sort_temp_data_total_wise as $name => $total_with_currency) {
							if($start_count <= 20) {
								$response['production_client_wise_highchart'][$start_count]['name'] = ucfirst($name);
								$response['production_client_wise_highchart'][$start_count]['y'] = (int)$total_with_currency['grand_total'];
								$response['production_client_wise_highchart'][$start_count]['currency'] = $total_with_currency['currency_icon'];
								$response['production_client_wise_highchart'][$start_count]['drilldown'] = ucfirst($name);
								$start_count++;
							}
						}
					}
					// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
				break;

				case 'production_status_wise_highchart_data':
					if(!empty($this->input->post('highchart_data_call_type'))){

						$order_status_wise_total = $this->get_order_wise_total_new($this->session->userdata('type'));

						// echo "<pre>";print_r($order_status_wise_total);echo"</pre><hr>";exit;


						$currency_rate = array_column($this->common_model->get_dynamic_data_sales_db('currency_id, currency_rate', array('status' => 'Active'), 'currency'), 'currency_rate', 'currency_id');
						$currency_rate['']='';
						$currency_rate[0]='';

						$return_data = array();

						foreach($order_status_wise_total['order_status_wise_total'] as $single_status_key => $single_status_total){

							$currency_icon = array_column($this->common_model->get_dynamic_data_sales_db('currency_id, currency_icon', array('status' => 'Active'), 'currency'), 'currency_icon', 'currency_id');
							$currency_icon['']='';
							$currency_icon[0]='';

							$sum_of_total = $temp = 0;
							foreach($single_status_total['currency_wise_total'] as $single_data){

								$total = str_replace(',', '', $single_data['total']);

								if($single_data['currency'] == '3'){

									$sum_of_total_sum = (int)($total / $currency_rate[1]);
								}else if($single_data['currency'] == '1' ){

									$sum_of_total_sum = $total;
								}else{

									$temp = ($total * $currency_rate[$single_data['currency']]);
									$sum_of_total_sum = (int)($temp / $currency_rate[1]);
								}

								$return_data[$single_status_total['title_name']]['grand_total'] += (int)$sum_of_total_sum;
								$return_data[$single_status_total['title_name']]['currency_icon'] = $currency_icon[1];
							}
						}

						$sort_product_status_wise_data = $return_data;
						arsort($sort_product_status_wise_data);
						$start_count= 0;

						foreach ($sort_product_status_wise_data as $name => $total_with_currency) {
							if($start_count <= 20) {
								$response['production_status_wise_highchart'][$start_count]['name'] = ucfirst($name);
								$response['production_status_wise_highchart'][$start_count]['y'] = (int)$total_with_currency['grand_total'];
								$response['production_status_wise_highchart'][$start_count]['currency'] = $total_with_currency['currency_icon'];
								$response['production_status_wise_highchart'][$start_count]['drilldown'] = ucfirst($name);
								$start_count++;
							}
						}
					}
				break;

				case 'product_family_wise_highchart_data':
					if(!empty($this->input->post('highchart_data_call_type'))){

						$where_string = "(production_status IS NULL OR production_status IN ('yet_to_start_production', 'in_production', 'pending_order', 'semi_ready', 'merchant_trade', 'mtt', 'import', 'query', 'mtt_rfd', 'in_transit_import', 'in_transit_mtt', 'ready_for_dispatch')) AND product_family is not null AND (status IS NULL OR status = 'Active')";

						if(!empty($this->session->userdata('type'))){
							$where_string_1 .= " AND type IN ('".$this->session->userdata('type')."')";
						}

						$currency_icon = array_column($this->common_model->get_dynamic_data_sales_db('currency_id, currency_icon', array('status' => 'Active'), 'currency'), 'currency_icon', 'currency_id');
						$currency_icon['']='';
						$currency_icon[0]='';

						$product_family = $this->production_model->get_product_family($where_string);

						$return_data = array();
						foreach($product_family['product_family_list'] as $single_product_family){

							$product_family_data = $this->production_model->get_total_sum_for_product_family($single_product_family['product_family'], $this->session->userdata('type'));

							$currency_rate = array_column($this->common_model->get_dynamic_data_sales_db('currency_id, currency_rate', array('status' => 'Active'), 'currency'), 'currency_rate', 'currency_id');
							$currency_rate['']='';
							$currency_rate[0]='';

							// echo "<pre>";print_r($currency_rate);echo"</pre><hr>";exit;
							$sum_of_total_sum  = $temp= 0;
							foreach($product_family_data['product_family_details'] as $single_data){

								if($single_data['currency'] == '3'){

									$sum_of_total_sum = (int)($single_data['total_sum'] / $currency_rate[1]);
								}else if($single_data['currency'] == '1' ){

									$sum_of_total_sum = $single_data['total_sum'];
								}else{

									$temp = ($single_data['total_sum'] * $currency_rate[$single_data['currency']]);
									$sum_of_total_sum = (int)($temp / $currency_rate[1]);
								}

								$return_data[$single_product_family['product_family']]['grand_total'] += (int)$sum_of_total_sum;
								$return_data[$single_product_family['product_family']]['currency_icon'] = $currency_icon[1];
							}
						}

						$sort_product_family_wise_data = $return_data;
						arsort($sort_product_family_wise_data);
						$start_count= 0;

						foreach ($sort_product_family_wise_data as $name => $total_with_currency) {
							if($start_count <= 20) {
								$response['production_product_family_wise_highchart'][$start_count]['name'] = ucfirst($name);
								$response['production_product_family_wise_highchart'][$start_count]['y'] = (int)$total_with_currency['grand_total'];
								$response['production_product_family_wise_highchart'][$start_count]['currency'] = $total_with_currency['currency_icon'];
								$response['production_product_family_wise_highchart'][$start_count]['drilldown'] = ucfirst($name);
								$start_count++;
							}
						}
					}
				break;

				case 'osdr_document_upload':

					$response['msg'] = 'File is uploaded successfully!!!';
					$return_response['status'] = '';
					$production_id = $this->session->userdata('production_id_for_osdr_document_upload');
					if(!empty($production_id)){

						$return_response = $this->osdr_document_upload_config();
					}
					$response['status'] = $return_response['status'];
					if($return_response['status'] == 'successful') {

						$document_path = array();
						$technical_document_details = $this->common_model->get_dynamic_data_sales_db('osdr_document',array('id'=> $production_id), 'production_process_information', 'row_array');

						if(!empty($technical_document_details)){

							$document_path = json_decode($technical_document_details['osdr_document'], true);
							$document_path[] = array('file_type' => $return_response['file_type'], 'file_name' => $return_response['file_name'], 'file_add_date' => $return_response['file_add_date']);

							$this->common_model->update_data_sales_db('production_process_information', array('osdr_document'=> json_encode($document_path)), array('id'=>$production_id));
						}
						// echo "<pre>";print_r($rfq_file_path);echo"</pre><hr>";exit;
					}else {

						$response['msg'] = 'File is not uploaded.';
					}
				break;

				default:
					$response['status'] = 'failed';	
					$response['message'] = "call type not found";
				break;
			}
			echo json_encode($response);
		} else {
			die('access is not allowed to this function');
		}
	}
	private function create_quantity_status_wise_data($product_details, $products, $materials){

		$return_array = array(
			'Pending'=> array(),
			'Semi_Ready'=> array(),
			'ICE'=> array(),
			'Import'=> array(),
			'Query'=> array(),
			'In_transit_ICE'=> array(),
			'RFD'=> array(),
			'Dispatch'=> array(),
			'Hold'=> array(),
			'Cancelled'=> array(),
		);
		foreach ($product_details as $single_product_detail_key => $single_product_detail) {
			
			$single_product_detail['key'] = $single_product_detail_key+1;
			$total_of_count = ($single_product_detail['semi_ready_count'] + $single_product_detail['ready_to_dispatch_order_count'] + $single_product_detail['dispatch_order_count'] + $single_product_detail['on_hold_order_count'] + $single_product_detail['mtt_count'] + $single_product_detail['import_count'] + $single_product_detail['query_count'] + $single_product_detail['mtt_rfd_count'] + $single_product_detail['order_cancelled_count']);
			if($total_of_count == 0){

				$return_array['Pending'][] = $this->create_quantity_status_wise_data_2($single_product_detail, $products, $materials, $single_product_detail['quantity']);
			}else{

				$total_quantity = $single_product_detail['quantity'];
				if($single_product_detail['semi_ready_count'] > 0){

					$return_array['Semi_Ready'][] = $this->create_quantity_status_wise_data_2($single_product_detail, $products, $materials, $single_product_detail['semi_ready_count']);
					$total_quantity = $total_quantity - $single_product_detail['semi_ready_count'];
				}
				if($single_product_detail['ready_to_dispatch_order_count'] > 0){

					$return_array['RFD'][] = $this->create_quantity_status_wise_data_2($single_product_detail, $products, $materials, $single_product_detail['ready_to_dispatch_order_count']);
					$total_quantity = $total_quantity - $single_product_detail['ready_to_dispatch_order_count'];
				}
				if($single_product_detail['dispatch_order_count'] > 0){

					$return_array['Dispatch'][] = $this->create_quantity_status_wise_data_2($single_product_detail, $products, $materials, $single_product_detail['dispatch_order_count']);
					$total_quantity = $total_quantity - $single_product_detail['dispatch_order_count'];
				}
				if($single_product_detail['on_hold_order_count'] > 0){

					$return_array['Hold'][] = $this->create_quantity_status_wise_data_2($single_product_detail, $products, $materials, $single_product_detail['on_hold_order_count']);
					$total_quantity = $total_quantity - $single_product_detail['on_hold_order_count'];
				}
				if($single_product_detail['mtt_count'] > 0){

					$return_array['ICE'][] = $this->create_quantity_status_wise_data_2($single_product_detail, $products, $materials, $single_product_detail['mtt_count']);
					$total_quantity = $total_quantity - $single_product_detail['mtt_count'];
				}
				if($single_product_detail['import_count'] > 0){

					$return_array['Import'][] = $this->create_quantity_status_wise_data_2($single_product_detail, $products, $materials, $single_product_detail['import_count']);
					$total_quantity = $total_quantity - $single_product_detail['import_count'];
				}
				if($single_product_detail['query_count'] > 0){

					$return_array['Query'][] = $this->create_quantity_status_wise_data_2($single_product_detail, $products, $materials, $single_product_detail['query_count']);
					$total_quantity = $total_quantity - $single_product_detail['query_count'];
				}
				if($single_product_detail['mtt_rfd_count'] > 0){

					$return_array['In_transit_ICE'][] = $this->create_quantity_status_wise_data_2($single_product_detail, $products, $materials, $single_product_detail['mtt_rfd_count']);
					$total_quantity = $total_quantity - $single_product_detail['mtt_rfd_count'];
				}
				if($single_product_detail['order_cancelled_count'] > 0){

					$return_array['Cancelled'][] = $this->create_quantity_status_wise_data_2($single_product_detail, $products, $materials, $single_product_detail['order_cancelled_count']);
					$total_quantity = $total_quantity - $single_product_detail['order_cancelled_count'];
				}
				if($total_quantity > 0){

					$return_array['Pending'][] = $this->create_quantity_status_wise_data_2($single_product_detail, $products, $materials, $total_quantity);
				}
			}
		}
		return $return_array;
	}
	private function create_quantity_status_wise_data_2($single_product_detail, $products, $materials, $quantity){

		return array(
			'id'=> $single_product_detail['key'],
			'quotation_dtl_id'=> $single_product_detail['quotation_dtl_id'],
			'product'=>
				(!empty($single_product_detail['product_id']))
				?$products[$single_product_detail['product_id']]
				:'',
			'material'=>
				(!empty($single_product_detail['material_id']))
				?$materials[$single_product_detail['material_id']]
				:'',
			'description'=> $single_product_detail['description'],
			'quantity'=> $single_product_detail['quantity'],
			'production_status'=> $single_product_detail['production_status'],
			'total'=> ($quantity * $single_product_detail['unit_price']),
			'production_quantity_done'=> $quantity
		);
	}
	private function prepare_listing_data($where, $order_by, $limit, $offset, $search_filter_array = array()) {

		// echo "<pre>";print_r($where);echo"</pre><hr>";
		// echo "<pre>";print_r($order_by);echo"</pre><hr>";
		// echo "<pre>";print_r($search_filter_array);echo"</pre><hr>";exit;
		$data = $this->production_model->get_production_listingz_data($where, $order_by,$limit, $offset);
		// echo "<pre>";print_r($data);echo"</pre><hr>";die('debug');
		// all product list
		$data['product_list'] = $this->get_common_data('product_list');
		// all material list
		$data['material_list'] = $this->get_common_data('material_list');
		// all currency list
		$data['currency_list'] = $this->get_common_data('currency_details');
		$data['total_revenue_list'] = array();
		foreach ($data['production_list'] as $production_list_key => $production_list_value) {
			
			//getting client name
			$data['production_list'][$production_list_key]['client_details'] = $this->common_model->get_dynamic_data_sales_db('name',array('id'=>$production_list_value['client_id']), 'customer_mst', 'row_array');
			
			// echo "<pre>";print_r($data['production_list'][$production_list_key]['client_details']);echo"</pre><hr>";exit;
			// getting currency symbol
			$data['production_list'][$production_list_key]['currency_details']['currency_icon'] = $data['currency_list'][$production_list_value['currency']];

			// echo "<pre>";print_r($data);echo"</pre><hr>";die;exit;
			//getting product details
			$product_details = $this->common_model->get_dynamic_data_sales_db('*, (((quotation_dtl.quantity - (quotation_dtl.semi_ready_count + quotation_dtl.ready_to_dispatch_order_count + quotation_dtl.dispatch_order_count + quotation_dtl.on_hold_order_count + quotation_dtl.mtt_count + quotation_dtl.import_count + quotation_dtl.query_count + quotation_dtl.mtt_rfd_count + quotation_dtl.order_cancelled_count)))) as pending_count',array('quotation_mst_id'=>$production_list_value['quotation_mst_id']), 'quotation_dtl');
			// echo "<pre>";print_r($product_details);echo"</pre><hr>";die;exit;
			// production type name
			$data['production_list'][$production_list_key]['type'] = $this->create_production_type_data('single', $production_list_value['type']);

			// production status name
			$data['production_list'][$production_list_key]['production_status_name'] = $this->create_production_status_data('single', $production_list_value['production_status']);
			
			$single_product_status_wise_details = array('yet_to_start_production'=>0, 'dispatched'=>0,'on_hold'=>0,'ready_for_dispatch'=>0, 'semi_ready'=>0, 'order_cancelled'=>0);
			$temp_total_value = 0;
			$total_status_wise = array();
			if(!empty($product_details)) {
				
				$data['production_list'][$production_list_key]['product_details'] = array();
				$product_array = array();
				$material_array = array();
				
				$data['production_list'][$production_list_key]['total_status_wise'] = array();
				$total_status_wise = array();
				// echo "<pre>";print_r($data);echo"</pre><hr>";die;
				foreach ($product_details as $product_key => $single_product_detail) {
					
					if(!empty($data['product_list'][$single_product_detail['product_id']])) {

						if(!in_array($data['product_list'][$single_product_detail['product_id']], $product_array)) {

							$product_array[] = $data['product_list'][$single_product_detail['product_id']];
						}
					}
					if(!empty($data['material_list'][$single_product_detail['material_id']])) {

						if(!in_array($data['material_list'][$single_product_detail['material_id']], $material_array)) {

							$material_array[] = $data['material_list'][$single_product_detail['material_id']];
						}
					}
				}
				//finding maximum count
				$higher_count = (count($product_array)>count($material_array)) ? count($product_array) : count($material_array);
				for ($i=0; $i < $higher_count; $i++) { 
					$data['production_list'][$production_list_key]['product_details'][$i]['product_name'] = '';
					$data['production_list'][$production_list_key]['product_details'][$i]['material_name'] = '';
					if(!empty($product_array[$i])) {

						$data['production_list'][$production_list_key]['product_details'][$i]['product_name'] = $product_array[$i];
					}
					if(!empty($material_array[$i])) {

						$data['production_list'][$production_list_key]['product_details'][$i]['material_name'] = $material_array[$i];
					}
				}				
			}
			$data['production_list'][$production_list_key]['total_status_wise'] = $this->create_total_status_wise($production_list_value);
			// echo "<pre>";print_r($data['production_list'][$production_list_key]['total_status_wise']);echo"</pre><hr>";exit;
			$tab_name = $this->session->userdata('production_tab_name');
			$static_tab_name_to_status_key_array = array(
														'semi_ready'=> array('semi_ready', 'semi_ready_new'),
														'mtt'=> array('mtt', 'mtt_new'),
														'import'=> array('import', 'import_new'),
														'query'=> array('query', 'query_new'),
														'mtt_rfd'=> array('mtt_rfd', 'mtt_rfd_new'),
														'ready_to_dispatch_order'=> array('ready_to_dispatch_order', 'ready_to_dispatch_order_new'),
														'dispatch_order'=> array('dispatch_order', 'dispatch_order_new'),
														'on_hold_order'=> array('on_hold_order', 'on_hold_order_new'),
														'order_cancelled'=> array('order_cancelled', 'order_cancelled_new'),
													);

			if (
				$this->session->userdata('production_tab_name') != 'all_order' && 
				$this->session->userdata('production_tab_name') != 'pending_order' && 
				$data['production_list'][$production_list_key]['total_status_wise'][$static_tab_name_to_status_key_array[$this->session->userdata('production_tab_name')][0]]['value'] == 0 && $data['production_list'][$production_list_key]['total_status_wise'][$static_tab_name_to_status_key_array[$this->session->userdata('production_tab_name')][1]]['value'] == 0) {

				unset($data['production_list'][$production_list_key]);
				$data['paggination_data']['total_rows']--;
				continue;
			}else{
				
				if(isset($data['production_list'][$production_list_key]['total_status_wise']['semi_ready_new'])){

					unset($data['production_list'][$production_list_key]['total_status_wise']['semi_ready_new']);
				}
				if(isset($data['production_list'][$production_list_key]['total_status_wise']['mtt_new'])){

					unset($data['production_list'][$production_list_key]['total_status_wise']['mtt_new']);
				}
				if(isset($data['production_list'][$production_list_key]['total_status_wise']['import_new'])){

					unset($data['production_list'][$production_list_key]['total_status_wise']['import_new']);
				}
				if(isset($data['production_list'][$production_list_key]['total_status_wise']['query_new'])){

					unset($data['production_list'][$production_list_key]['total_status_wise']['query_new']);
				}
				if(isset($data['production_list'][$production_list_key]['total_status_wise']['mtt_rfd_new'])){

					unset($data['production_list'][$production_list_key]['total_status_wise']['mtt_rfd_new']);
				}
				if(isset($data['production_list'][$production_list_key]['total_status_wise']['ready_to_dispatch_order_new'])){

					unset($data['production_list'][$production_list_key]['total_status_wise']['ready_to_dispatch_order_new']);
				}
				if(isset($data['production_list'][$production_list_key]['total_status_wise']['dispatch_order_new'])){

					unset($data['production_list'][$production_list_key]['total_status_wise']['dispatch_order_new']);
				}
				if(isset($data['production_list'][$production_list_key]['total_status_wise']['on_hold_order_new'])){

					unset($data['production_list'][$production_list_key]['total_status_wise']['on_hold_order_new']);
				}
				if(isset($data['production_list'][$production_list_key]['total_status_wise']['order_cancelled_new'])){

					unset($data['production_list'][$production_list_key]['total_status_wise']['order_cancelled_new']);
				}
			}
			// echo "<pre>";print_r($data['production_list'][$production_list_key]['total_status_wise']);echo"</pre><hr>";exit;

			if(!empty($data['production_list'][$production_list_key]['total_status_wise'])){

				foreach($data['production_list'][$production_list_key]['total_status_wise'] as $total_status_wise_key => $total_status_wise_value) {

					if($total_status_wise_key == $tab_name && $total_status_wise_value['value'] > 0){

						if(empty($data['total_revenue_list'][$production_list_value['currency']])) {

							$data['total_revenue_list'][$production_list_value['currency']]['currency'] = $production_list_value['currency'];
							$data['total_revenue_list'][$production_list_value['currency']]['total'] = $total_status_wise_value['value'];
						} else {

							$data['total_revenue_list'][$production_list_value['currency']]['total'] += $total_status_wise_value['value'];
						}
					}
				}
			}

			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
			$work_order_sheet_details = $this->common_model->get_dynamic_data_sales_db('*',array('work_order_no'=>$production_list_value['work_order_no']), 'work_order_sheet', 'row_array');
			// echo "<pre>";print_r($work_order_sheet_details);echo"</pre><hr>";exit;
			if(!empty($work_order_sheet_details)) {

				$data['production_list'][$production_list_key]['add_button'] = false;
				$data['production_list'][$production_list_key]['edit_button'] = true;
				if(!empty($work_order_sheet_details['approved_by']) && $work_order_sheet_details['approved_by_status'] == 'approved'){

					$data['production_list'][$production_list_key]['pdf_button'] = true;
				}
			}else{

				$data['production_list'][$production_list_key]['add_button'] = true;
				$data['production_list'][$production_list_key]['edit_button'] = false;
				$data['production_list'][$production_list_key]['pdf_button'] = false;
			}
			// echo "<pre>";print_r($temp_total_value);echo"</pre><hr>";exit;
			if(!empty($data['production_list'][$production_list_key])){

				$po_details = $this->common_model->get_dynamic_data_sales_db('id, vendor_name, gross_total, delivery_time',array('work_order_no'=>$production_list_value['work_order_no']), 'procurement');
				$data['production_list'][$production_list_key]['po_details'] = array();
				if(!empty($po_details)){
	
					$data['production_list'][$production_list_key]['po_details'] = $po_details;
				}
				$data['production_list'][$production_list_key]['file_upload_color'] = 'kt-svg-icon--dark';
				$file_upload_count = $this->common_model->get_dynamic_data_sales_db('count(*) as count',array('work_order_no'=>$production_list_value['work_order_no']), 'google_cloud_upload_logs', 'row_array')['count'];
				if($file_upload_count > 0){

					$data['production_list'][$production_list_key]['file_upload_color'] = 'kt-svg-icon--success';
				}
			}
			// echo "<pre>";print_r($po_details);echo"</pre><hr>";exit;
			if(!empty($production_list_value['latest_update'])){

				$latest_update_array = json_decode($production_list_value['latest_update'], true);
				$last_update_data = $latest_update_array[count($latest_update_array)-1];
				$data['production_list'][$production_list_key]['latest_update_info'] = $last_update_data['message'];
			}

			if(!empty($production_list_value['technical_document_file_and_path'])){

				$data['production_list'][$production_list_key]['technical_document_color'] = 'kt-svg-icon--success';
			}else{
				$data['production_list'][$production_list_key]['technical_document_color'] = 'kt-svg-icon--dark';
			}
		}

		// all client name list
		$data['client_list'] = array();
		if(!empty($search_filter_array) && !empty($search_filter_array['search_production_client_name'])){

			$data['client_list'] = $this->production_model->get_production_listingz_client_list_data($search_filter_array['search_production_client_name']);
		}
		
		// echo "<pre>";print_r($data['client_list']);echo"</pre><hr>";exit;
		
		// all handle by list
		$data['handle_by_users'] = $this->get_common_data('handle_by');
		// all production status list
		$data['production_status'] = $this->create_production_status_data();
		// all production type list
		$data['production_type'] = $this->create_production_type_data();
		// all user list
		$data['users_details'] = $this->get_common_data('users_details');
		// all user Sales Person list
		$data['sales_person_details'] = $this->get_common_data('sales_person');
		// all user Approved Person list
		$data['approved_by_list'] = $this->get_common_data('approved_by');
		$data['payment_term_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*',array('status'=>'Active'), 'payment_terms'),'term_value', 'term_id');
		$data['paggination_data']['limit'] = $limit;
		$data['paggination_data']['offset'] = $offset;
		$data['search_filter'] = $this->create_search_filter_data($search_filter_array);
		// $data['total_revenue_list'] = $this->production_model->production_listingz_total_revenue($where);
		// $data['total_revenue_list'] = $this->get_total_revenue($where, $order_by);
		if(!empty($search_filter_array['sort'][0])){

			// die('came in');
			$total = array();
			foreach ($data['production_list'] as $production_list_key => $production_list_value) {

				$total[$production_list_key] = 0;
				foreach ($production_list_value['total_status_wise'] as $single_total_status_wise) {

					$total[$production_list_key] += $single_total_status_wise['value'];
				}
			}
			// echo "<pre>";print_r($total);echo"</pre><hr>";
			if($search_filter_array['sort'][0] == 'desc'){

				arsort($total);
			}else{

				asort($total);
			}
			// echo "<pre>";print_r($total);echo"</pre><hr>";
			foreach ($total as $production_list_key => $single_total) {

				$data['production_list_new'][] = $data['production_list'][$production_list_key];
			}
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		}else{

			$data['production_list_new'] = $data['production_list'];
		}

		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		return $data;
	}
	private function create_total_status_wise($product_details){

		$return_array = array(
			'pending_order' => array(
				'name'=> 'Pending',
				'value'=> 0
			),
			'semi_ready' => array(
				'name'=> 'Semi Ready',
				'value'=> 0
			),
			'semi_ready_new' => array(
				'name'=> 'Semi Ready NEW',
				'value'=> 0,
			),
			'mtt' => array(
				'name'=> 'Import cum Export(ICE)',
				'value'=> 0,
			),
			'mtt_new' => array(
				'name'=> 'Import cum Export(ICE) NEW',
				'value'=> 0,
			),
			'import' => array(
				'name'=> 'Import',
				'value'=> 0,
			),
			'import_new' => array(
				'name'=> 'Import NEW',
				'value'=> 0,
			),
			'query' => array(
				'name'=> 'Query',
				'value'=> 0,
			),
			'query_new' => array(
				'name'=> 'Query NEW',
				'value'=> 0,
			),
			'mtt_rfd' => array(
				'name'=> 'In transit ICE',
				'value'=> 0,
			),
			'mtt_rfd_new' => array(
				'name'=> 'In transit ICE NEW',
				'value'=> 0,
			),
			'ready_to_dispatch_order' => array(
				'name'=> 'RFD',
				'value'=> 0,
			),
			'ready_to_dispatch_order_new' => array(
				'name'=> 'RFD NEW',
				'value'=> 0,
			),
			'dispatch_order' => array(
				'name'=> 'Dispatch',
				'value'=> 0,
			),
			'dispatch_order_new' => array(
				'name'=> 'Dispatch NEW',
				'value'=> 0,
			),
			'on_hold_order' => array(
				'name'=> 'Hold',
				'value'=> 0,
			),
			'on_hold_order_new' => array(
				'name'=> 'Hold NEW',
				'value'=> 0,
			),
			'order_cancelled' => array(
				'name'=> 'Cancelled',
				'value'=> 0,
			),
			'order_cancelled_new' => array(
				'name'=> 'Cancelled NEW',
				'value'=> 0,
			),
		);
		// echo "<pre>";print_r($product_details);echo"</pre><hr>";exit;
	
		$return_array['pending_order']['value'] += $product_details['pending_count'];
		$return_array['semi_ready']['value'] += $product_details['semi_ready_count'];
		$return_array['semi_ready_new']['value'] += $product_details['semi_ready_count_2'];
		$return_array['ready_to_dispatch_order']['value'] += $product_details['rfd_count'];
		$return_array['ready_to_dispatch_order_new']['value'] += $product_details['rfd_count_2'];
		$return_array['dispatch_order']['value'] += $product_details['dispatch_count'];
		$return_array['dispatch_order_new']['value'] += $product_details['dispatch_count_2'];
		$return_array['on_hold_order']['value'] += $product_details['hold_count'];
		$return_array['on_hold_order_new']['value'] += $product_details['hold_count_2'];
		$return_array['mtt']['value'] += $product_details['mtt_count'];
		$return_array['mtt_new']['value'] += $product_details['mtt_count_2'];
		$return_array['import']['value'] += $product_details['import_count'];
		$return_array['import_new']['value'] += $product_details['import_count_2'];
		$return_array['query']['value'] += $product_details['query_count'];
		$return_array['query_new']['value'] += $product_details['query_count_2'];
		$return_array['mtt_rfd']['value'] += $product_details['mtt_rfd_count'];
		$return_array['mtt_rfd_new']['value'] += $product_details['mtt_rfd_count_2'];
		$return_array['order_cancelled']['value'] += $product_details['cancel_count'];
		$return_array['order_cancelled_new']['value'] += $product_details['cancel_count_2'];

		return $return_array;
	}
	private function prepare_listing_data_old($where, $order_by, $limit, $offset, $search_filter_array = array()) {

		// echo "<pre>";print_r($search_filter_array);echo"</pre><hr>";exit;
		$data = $this->production_model->get_production_listingz_data($where, $order_by,$limit, $offset);
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		// all product list
		$data['product_list'] = $this->get_common_data('product_list');
		// all material list
		$data['material_list'] = $this->get_common_data('material_list');
		// all currency list
		$data['currency_list'] = $this->get_common_data('currency_details');
		$data['total_revenue_list'] = array();
		foreach ($data['production_list'] as $production_list_key => $production_list_value) {
			
			//getting client name
			$data['production_list'][$production_list_key]['client_details'] = $this->common_model->get_dynamic_data_sales_db('name',array('id'=>$production_list_value['client_id']), 'customer_mst', 'row_array');
			
			// echo "<pre>";print_r($data['production_list'][$production_list_key]['client_details']);echo"</pre><hr>";exit;
			// getting currency symbol
			$data['production_list'][$production_list_key]['currency_details']['currency_icon'] = $data['currency_list'][$production_list_value['currency']];
			
			// echo "<pre>";print_r($data);echo"</pre><hr>";die;exit;
			//getting product details
			$product_details = $this->common_model->get_dynamic_data_sales_db('*',array('quotation_mst_id'=>$production_list_value['quotation_mst_id']), 'quotation_dtl');
			// echo "<pre>";print_r($product_details);echo"</pre><hr>";die('debug');
			
			// production type name
			$data['production_list'][$production_list_key]['type'] = $this->create_production_type_data('single', $production_list_value['type']);

			// production status name
			$data['production_list'][$production_list_key]['production_status_name'] = $this->create_production_status_data('single', $production_list_value['production_status']);
			
			$single_product_status_wise_details = array('yet_to_start_production'=>0, 'dispatched'=>0,'on_hold'=>0,'ready_for_dispatch'=>0, 'semi_ready'=>0, 'order_cancelled'=>0);
			$temp_total_value = 0;
			$total_status_wise = array();
			if(!empty($product_details)) {
				
				$data['production_list'][$production_list_key]['product_details'] = array();
				$product_array = array();
				$material_array = array();
				
				$data['production_list'][$production_list_key]['total_status_wise'] = array();
				$total_status_wise = array();
				// echo "<pre>";print_r($data);echo"</pre><hr>";die;
				foreach ($product_details as $product_key => $single_product_detail) {
					
					if(!empty($data['product_list'][$single_product_detail['product_id']])) {

						if(!in_array($data['product_list'][$single_product_detail['product_id']], $product_array)) {

							$product_array[] = $data['product_list'][$single_product_detail['product_id']];
						}
					}
					if(!empty($data['material_list'][$single_product_detail['material_id']])) {

						if(!in_array($data['material_list'][$single_product_detail['material_id']], $material_array)) {

							$material_array[] = $data['material_list'][$single_product_detail['material_id']];
						}
					}
					if(!isset($total_status_wise[$single_product_detail['production_status']])){

						$total_status_wise[$single_product_detail['production_status']] = 0;
					}
					if(!isset($total_status_wise['pending_order'])){

						$total_status_wise['pending_order'] = 0;
					}
					if($this->session->userdata('production_tab_name') == 'pending_order') {
						
						if($single_product_detail['production_status'] == 'pending_order') {

							if($single_product_detail['production_quantity_done'] == 0){

								$total_status_wise[$single_product_detail['production_status']] += $single_product_detail['row_price'];
								$temp_total_value += $single_product_detail['row_price'];
							}else {
								if(($single_product_detail['quantity'] - $single_product_detail['production_quantity_done']) > 0) {

									$total_status_wise[$single_product_detail['production_status']] += $single_product_detail['unit_price'] * ($single_product_detail['quantity'] - $single_product_detail['production_quantity_done']);
									$temp_total_value += $single_product_detail['unit_price'] * ($single_product_detail['quantity'] - $single_product_detail['production_quantity_done']);
								}
							}
						}else {

							$total_status_wise[$single_product_detail['production_status']] += $single_product_detail['unit_price'] * ($single_product_detail['production_quantity_done']);
							$total_status_wise['pending_order'] += $single_product_detail['unit_price'] * ($single_product_detail['quantity'] - $single_product_detail['production_quantity_done']);
							
							if(($single_product_detail['quantity'] - $single_product_detail['production_quantity_done']) > 0) {
								
								$temp_total_value += $single_product_detail['unit_price'] * ($single_product_detail['quantity'] - $single_product_detail['production_quantity_done']);
							}
						}
					}else {
						
						if($single_product_detail['production_quantity_done'] == 0){

							$total_status_wise[$single_product_detail['production_status']] += $single_product_detail['row_price'];
						}else {
							
							$total_status_wise['pending_order'] += $single_product_detail['unit_price'] * ($single_product_detail['quantity'] - $single_product_detail['production_quantity_done']);
							$total_status_wise[$single_product_detail['production_status']] += $single_product_detail['unit_price']*$single_product_detail['production_quantity_done'];
						}
						if($single_product_detail['production_status'] == $this->session->userdata('production_tab_name')) {

							if($single_product_detail['production_quantity_done'] == 0){

								$temp_total_value += $single_product_detail['row_price'];
							}else {
								
								$temp_total_value += $single_product_detail['unit_price']*$single_product_detail['production_quantity_done'];
							}
						}
					}
				}
				// echo "<pre>";print_r($data['production_list']);echo"</pre><hr>";die;
				//finding maximum count
				$higher_count = (count($product_array)>count($material_array)) ? count($product_array) : count($material_array);
				for ($i=0; $i < $higher_count; $i++) { 
					$data['production_list'][$production_list_key]['product_details'][$i]['product_name'] = '';
					$data['production_list'][$production_list_key]['product_details'][$i]['material_name'] = '';
					if(!empty($product_array[$i])) {

						$data['production_list'][$production_list_key]['product_details'][$i]['product_name'] = $product_array[$i];
					}
					if(!empty($material_array[$i])) {

						$data['production_list'][$production_list_key]['product_details'][$i]['material_name'] = $material_array[$i];
					}
				}
			}
			// echo "<pre>";print_r($total_status_wise);echo"</pre><hr>";exit;
			$static_production_status =  array(
				'pending_order'=> array('name'=>'Pending'),
				'semi_ready'=>array('name'=>'Semi Ready'),
				'ready_to_dispatch_order'=>array('name'=>'RFD'),
				'dispatch_order'=>array('name'=>'Dispatch'),
				'on_hold_order'=>array('name'=>'Hold'),
				'mtt'=>array('name'=>'Import cum Export(ICE)'),
				'query'=>array('name'=>'Query'),
				'mtt_rfd'=>array('name'=>'In transit ICE'),
				'order_cancelled'=>array('name'=>'Cancelled')
			);
			$i = 0;
			foreach ($total_status_wise as $status_name => $value) {
					
				$data['production_list'][$production_list_key]['total_status_wise'][$i] = $static_production_status[$status_name];
				$data['production_list'][$production_list_key]['total_status_wise'][$i]['value'] = $value;
				$i++;
			}
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
			$data['production_list'][$production_list_key]['add_button'] = false;
			if(!in_array($this->session->userdata('role'), array('8', '18'))){

				$data['production_list'][$production_list_key]['add_button'] = true;
			}
			$data['production_list'][$production_list_key]['edit_button'] = false;
			$data['production_list'][$production_list_key]['pdf_button'] = false;
			
			$work_order_sheet_details = $this->common_model->get_dynamic_data_sales_db('*',array('work_order_no'=>$production_list_value['work_order_no']), 'work_order_sheet');
			// echo "<pre>";print_r($work_order_sheet_details);echo"</pre><hr>";exit;
			if(!empty($work_order_sheet_details)) {

				if(!in_array($this->session->userdata('role'), array('8', '18'))){

					$data['production_list'][$production_list_key]['add_button'] = false;
					$data['production_list'][$production_list_key]['edit_button'] = true;
				}
				$data['production_list'][$production_list_key]['pdf_button'] = true;
			}
			// echo "<pre>";print_r($temp_total_value);echo"</pre><hr>";exit;
			if(($production_list_value['semi_ready_count'] + $production_list_value['ready_for_dispatch_count'] + $production_list_value['dispatch_count'] + $production_list_value['on_hold_count'] + $production_list_value['cancel_count'] + $production_list_value['mtt_count'] + $production_list_value['query_count'] + $production_list_value['mtt_rfd_count']) > 0){

				if(!empty($temp_total_value)){

					$data['production_list'][$production_list_key]['grand_total'] = $temp_total_value;
					if(empty($data['total_revenue_list'][$production_list_value['currency']])) {

						$data['total_revenue_list'][$production_list_value['currency']]['currency'] = $production_list_value['currency'];
						$data['total_revenue_list'][$production_list_value['currency']]['total'] = $temp_total_value;
					} else {

						$data['total_revenue_list'][$production_list_value['currency']]['total'] += $temp_total_value;
					}
				}else{

					unset($data['production_list'][$production_list_key]);
					$data['paggination_data']['total_rows']  = $data['paggination_data']['total_rows'] - 1;
				}
			}else{

				if(empty($data['total_revenue_list'][$production_list_value['currency']])) {

					$data['total_revenue_list'][$production_list_value['currency']]['currency'] = $production_list_value['currency'];
					$data['total_revenue_list'][$production_list_value['currency']]['total'] = $production_list_value['grand_total'];
				} else {

					$data['total_revenue_list'][$production_list_value['currency']]['total'] += $production_list_value['grand_total'];
				}
			}
			if(!empty($data['production_list'][$production_list_key])){

				$po_details = $this->common_model->get_dynamic_data_sales_db('id, vendor_name, gross_total, delivery_time',array('work_order_no'=>$production_list_value['work_order_no']), 'procurement');
				$data['production_list'][$production_list_key]['po_details'] = array();
				if(!empty($po_details)){
	
					$data['production_list'][$production_list_key]['po_details'] = $po_details;
				}
				$data['production_list'][$production_list_key]['file_upload_color'] = 'kt-svg-icon--dark';
				$file_upload_count = $this->common_model->get_dynamic_data_sales_db('count(*) as count',array('work_order_no'=>$production_list_value['work_order_no']), 'google_cloud_upload_logs', 'row_array')['count'];
				if($file_upload_count > 0){

					$data['production_list'][$production_list_key]['file_upload_color'] = 'kt-svg-icon--success';
				}
			}
			// echo "<pre>";print_r($po_details);echo"</pre><hr>";exit;
		}

		// all client name list
		$data['client_list'] = array();
		if(!empty($search_filter_array) && !empty($search_filter_array['search_production_client_name'])){

			$data['client_list'] = $this->production_model->get_production_listingz_client_list_data($search_filter_array['search_production_client_name']);
		}
		
		// echo "<pre>";print_r($data['client_list']);echo"</pre><hr>";exit;
		
		// all handle by list
		$data['handle_by_users'] = $this->get_common_data('handle_by');
		// all production status list
		$data['production_status'] = $this->create_production_status_data();
		// all production type list
		$data['production_type'] = $this->create_production_type_data();
		// all user list
		$data['users_details'] = $this->get_common_data('users_details');
		// all user Sales Person list
		$data['sales_person_details'] = $this->get_common_data('sales_person');
		$data['payment_term_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*',array('status'=>'Active'), 'payment_terms'),'term_value', 'term_id');
		$data['paggination_data']['limit'] = $limit;
		$data['paggination_data']['offset'] = $offset;
		$data['search_filter'] = $this->create_search_filter_data($search_filter_array);
		// $data['total_revenue_list'] = $this->production_model->production_listingz_total_revenue($where);
		// $data['total_revenue_list'] = $this->get_total_revenue($where, $order_by);
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		return $data;
	}
	private function create_production_status_data($call_type='all', $production_status_name = ''){

		$static_production_status =  array(
										'yet_to_start_production'=> array('name'=>'Yet To Start Production','color'=>'dark'),
										'in_production'=> array('name'=>'In Production', 'color'=>'gainsboro'),
										'pending_order'=> array('name'=>'Pending Order','color'=>'primary'),
										'semi_ready'=>array('name'=>'Semi Ready', 'color'=>'darkkhaki'),
										'merchant_trade'=>array('name'=>'Import cum Export (ICE)', 'color'=>'warning'),
										'mtt'=>array('name'=>'M T T', 'color'=>'success'),
										'import'=>array('name'=>'Import', 'color'=>'Salmon'),
										'query'=>array('name'=>'Query', 'color'=>'gainsboro'),
										'in_transit_import'=>array('name'=>'In Transit Import', 'color'=>'warning'),
										'mtt_rfd'=>array('name'=>'In Transit ICE', 'color'=>'success'),
										'in_transit_mtt'=>array('name'=>'In Transit MTT', 'color'=>'primary'),
										'ready_for_dispatch'=>array('name'=>'Ready For Dispatch', 'color'=>'darkkhaki'),
										'dispatched'=>array('name'=>'Dispatched', 'color'=>'Salmon'),
										'delay_in_delivery'=>array('name'=>'Delay in Delivery', 'color'=>'gainsboro'),
										'on_hold'=>array('name'=>'Order On Hold', 'color'=>'dark'),
										'order_cancelled'=>array('name'=>'Order Cancelled', 'color'=>'danger')
		);
		if($call_type == 'all') {

			return $static_production_status;
		} else {

			return (!empty($production_status_name)) ? $static_production_status[$production_status_name] : array('name'=>'','color'=>'');
		}
	}
	private function create_production_type_data($call_type='all', $production_type_name = ''){

		$static_production_type =  array(
										'zen'=> array('name'=>'Z','color'=>'primary'),
										'IN'=> array('name'=>'IN','color'=>'warning'),
										'OM'=> array('name'=>'OM', 'color'=>'success')
									);
		if($call_type == 'all') {

			return $static_production_type;
		} else {

			return (!empty($production_type_name)) ? $static_production_type[$production_type_name] : array('name'=>'','color'=>'');
		}
	}
	private function get_common_data($data_type = '') {

		$return_array = array();
		if(!empty($data_type)) {

			switch ($data_type) {
				
				case 'product_list':
					// $return_array = array_column($this->common_model->get_dynamic_data_sales_db('*', array('lookup_group'=>259, 'status' => 'Active'), 'lookup'), 'lookup_value', 'lookup_id');	
					$return_array = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status' => 'Active'), 'product_mst'), 'name', 'id');	
				break;
					
				case 'material_list':
						// $return_array = array_column($this->common_model->get_dynamic_data_sales_db('*', array('lookup_group'=>272, 'status' => 'Active'), 'lookup'), 'lookup_value', 'lookup_id');
					$return_array = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status' => 'Active'), 'material_mst'), 'name', 'id');	
				break;
				
				case 'handle_by':

					// $return_array = $this->common_model->get_procurement_person('user_id, name');
					$return_array = $this->common_model->get_dynamic_data_sales_db_null_false('user_id, name',"status = 1 AND role IN (6, 8, 18)", 'users');
				break;

				case 'users_details':
					$return_array = array_column($this->common_model->get_dynamic_data_sales_db('user_id, name',array(), 'users'),'name','user_id');
				break;

				case 'currency_details':
					$return_array = array_column($this->common_model->get_dynamic_data_sales_db('currency_id, decimal_number',array('status'=>'Active'), 'currency'),'decimal_number','currency_id');
				break;

				case 'sales_person':
					$return_array = array_column($this->common_model->get_sales_person(),'name','user_id');
				break;

				case 'approved_by':

					$return_array = $this->common_model->get_dynamic_data_sales_db_null_false('user_id, name',"status = 1 AND user_id IN (2, 16, 23, 189)", 'users');
				break;
				default:
					
					echo "Data Type does not matched!!!";die('debug');
				break;
			}
		} else {
			echo "Data Type is provided!!!";die('debug');
		}
		return $return_array;
	}
	private function create_search_filter_data($filter_data = '') {

		$return_array = array(
			'work_order_no' => array(0=>''), 'search_production_client_name' => '', 'client_id' => array(), 'assigned_to' => array(), 'product_family' => array(), 'proforma_no' => array(0=>''), 'order_no' => array(0=>''), 'proforma_date' => array(0=>''), 'payment_term' => array(), 'payment_status' => array(0=>''), 'vendor_po' => array(0=>''), 'delivery_date' => array(0=>''), 'qc_clearance' => array(),
			'mtc_sent' => array(), 'latest_update' => array(0=>''), 'special_comment' => array(0=>''), 'handled_by' => array(), 'production_status' => array(), 'type' => array(), 'rating_value' => array(0=>''), 'sort' => array(), 'work_order_sheet_approved_by' => array(), 'work_order_sheet_approved_by_status' => array(),
		);
		if(!empty($filter_data)) {
			foreach ($filter_data as $key => $value) {
				$return_array[$key] = $value;
			}
		}
		return $return_array; 
	}
	// private function create_where_response_on_search_filter($search_form_data, $tab_name) {
	private function create_where_response_on_search_filter($post_details) {

		// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;
		$return_response = array();
		$return_response['where_array'] = array();
		$return_response['where_string_array'] = array();
		if(!empty($post_details)) {
			if(!empty($post_details['tab_name'])) {

				$return_response['where_string_array'] = $this->default_where_for_all_tab($post_details['tab_name']);
			}
			if(!empty($post_details['production_search_form'])) {

				foreach ($post_details['production_search_form'] as $form_key => $form_data) {

					if(!empty($form_data['value'])){

						$return_response['where_array'][$form_data['name']][] = $form_data['value'];
					}
				}
			}
			
			// echo "<pre>";print_r($return_response['where_array']);echo"</pre><hr>";die;
			$type_search_filter = false;
			foreach ($return_response['where_array'] as $column_name => $column_details) {

				$current_count_number = count($return_response['where_string_array'])+1;
				if($column_name == 'work_order_no'){

					$return_response['where_string_array'][$current_count_number] = "production_process_information.work_order_no = '".$column_details[0]."'";
				} elseif($column_name == 'search_production_client_name') {

					$return_response['where_array'][$column_name] = $column_details[0];
				} elseif($column_name == 'client_id') {

					$return_response['where_string_array'][$current_count_number] = "(quotation_mst.client_id = ".$column_details[0];
					
					unset($column_details[0]);
					foreach ($column_details as $client_id) {
						$return_response['where_string_array'][$current_count_number] .= " OR quotation_mst.client_id = '".$client_id."'";
					}
					$return_response['where_string_array'][$current_count_number] .= ")";
				} elseif($column_name == 'assigned_to') {

					$return_response['where_string_array'][$current_count_number] = "(quotation_mst.assigned_to = '".$column_details[0]."'";
					unset($column_details[0]);
					foreach ($column_details as $sales_person_id) {
						$return_response['where_string_array'][$current_count_number] .= " OR quotation_mst.assigned_to = '".$sales_person_id."'";
					}
					$return_response['where_string_array'][$current_count_number] .= ")";
				} elseif($column_name == 'product_family') {

					$return_response['where_string_array'][$current_count_number] = "(production_process_information.product_family = '".$column_details[0]."'";
					unset($column_details[0]);
					foreach ($column_details as $product_family_name) {
						$return_response['where_string_array'][$current_count_number] .= " OR production_process_information.product_family = '".$product_family_name."'";
					}
					$return_response['where_string_array'][$current_count_number] .= ")";
				} elseif($column_name == 'proforma_no') {

					$return_response['where_string_array'][$current_count_number] = "quotation_mst.proforma_no LIKE '%".$column_details[0]."%'";
				} elseif($column_name == 'order_no') {

					$return_response['where_string_array'][$current_count_number] = "quotation_mst.order_no LIKE '%".$column_details[0]."%'";
				} elseif($column_name == 'proforma_date') {
						
					$date_explode = explode(' - ',$column_details[0]);
					if(count($date_explode) == 1) {
						$return_response['where_string_array'][$current_count_number] = "(quotation_mst.confirmed_on >= '".date('Y-m-d 00:00:00', strtotime($date_explode[0]))."' AND quotation_mst.confirmed_on <= '".date('Y-m-d 23:59:59',strtotime($date_explode[0]))."')";	
					} else {
						$return_response['where_string_array'][$current_count_number] = "(quotation_mst.confirmed_on >= '".date('Y-m-d 00:00:00', strtotime($date_explode[0]))."' AND quotation_mst.confirmed_on <= '".date('Y-m-d 23:59:59',strtotime($date_explode[1]))."')";	
					}
					$this->session->unset_userdata('search_filter_proforma_date');
					$this->session->set_userdata('search_filter_proforma_date', $column_details[0]);
				} else if($column_name == 'payment_term') {

					$return_response['where_string_array'][$current_count_number] = "(quotation_mst.payment_term = '".$column_details[0]."'";
					unset($column_details[0]);
					foreach ($column_details as $payment_terms_name) {
						$return_response['where_string_array'][$current_count_number] .= " OR quotation_mst.payment_term = '".$product_family_name."'";
					}
					$return_response['where_string_array'][$current_count_number] .= ")";
				} elseif($column_name == 'payment_status') {

					$return_response['where_string_array'][$current_count_number] = "production_process_information.payment_status LIKE '%".$column_details[0]."%'";
				} elseif($column_name == 'vendor_po') {

					$return_response['where_string_array'][$current_count_number] = "production_process_information.vendor_po LIKE '%".$column_details[0]."%'";
				} elseif($column_name == 'delivery_date') {
					
					$this->session->unset_userdata('search_filter_delivery_date');
					$this->session->set_userdata('search_filter_delivery_date', $column_details[0]);
					$date_explode = explode(' - ',$column_details[0]);
					if(count($date_explode) == 1) {
						$return_response['where_string_array'][$current_count_number] = "production_process_information.delivery_date = '".date('Y-m-d', strtotime($date_explode[0]))."'";
					} else {
						if($date_explode[0] == $date_explode[1]) {
							$return_response['where_string_array'][$current_count_number] = "production_process_information.delivery_date = '".date('Y-m-d', strtotime($date_explode[0]))."'";
						} else{ 
							$return_response['where_string_array'][$current_count_number] = "(production_process_information.delivery_date >= '".date('Y-m-d', strtotime($date_explode[0]))."' AND production_process_information.delivery_date <= '".date('Y-m-d',strtotime($date_explode[1]))."')";
						}
					}
				} else if($column_name == 'qc_clearance') {

					$return_response['where_string_array'][$current_count_number] = "(production_process_information.qc_clearance = '".$column_details[0]."'";
					unset($column_details[0]);
					foreach ($column_details as $payment_terms_name) {
						$return_response['where_string_array'][$current_count_number] .= " OR production_process_information.qc_clearance = '".$product_family_name."'";
					}
					$return_response['where_string_array'][$current_count_number] .= ")";
				} elseif($column_name == 'latest_update') {

					$return_response['where_string_array'][$current_count_number] = "production_process_information.latest_update LIKE '%".$column_details[0]."%'";
				} elseif($column_name == 'special_comment') {

					$return_response['where_string_array'][$current_count_number] = "production_process_information.special_comment LIKE '%".$column_details[0]."%'";
				} elseif($column_name == 'handled_by') {
					
					$return_response['where_string_array'][$current_count_number] = "(production_process_information.handled_by LIKE '%".$column_details[0]."%'";
					unset($column_details[0]);
					foreach ($column_details as $handle_user_name) {
						$return_response['where_string_array'][$current_count_number] .= " OR production_process_information.handled_by LIKE '%".$handle_user_name."%'";
					}
					$return_response['where_string_array'][$current_count_number] .= ")";
				} elseif($column_name == 'production_status') {
					unset($return_response['where_string_array'][2]);
					$return_response['where_string_array'][2] = "(production_process_information.production_status = '".$column_details[0]."'";
					unset($column_details[0]);
					foreach ($column_details as $production_status) {
						$return_response['where_string_array'][2] .= " OR production_process_information.production_status = '".$production_status."'";
					}
					$return_response['where_string_array'][2] .= ")";
				} elseif($column_name == 'type') {

					$return_response['where_string_array'][$current_count_number] = "(production_process_information.type = '".$column_details[0]."'";
					unset($column_details[0]);
					foreach ($column_details as $type_name) {

						$return_response['where_string_array'][$current_count_number] .= " OR production_process_information.type = '".$type_name."'";
					}
					$return_response['where_string_array'][$current_count_number] .= ")";
					$type_search_filter = true;
				} elseif($column_name == 'rating_value'){

					$return_response['where_string_array'][$current_count_number] = "production_process_information.rating_value = ".$column_details[0];
				} elseif($column_name == 'work_order_sheet_approved_by'){

					$return_response['where_string_array'][$current_count_number] = "(production_process_information.work_order_sheet_approved_by = '".$column_details[0]."'";
					unset($column_details[0]);
					foreach ($column_details as $approved_person_id) {
						$return_response['where_string_array'][$current_count_number] .= " OR production_process_information.work_order_sheet_approved_by = '".$approved_person_id."'";
					}
					$return_response['where_string_array'][$current_count_number] .= ")";
				} elseif($column_name == 'work_order_sheet_approved_by_status') {

					$return_response['where_string_array'][$current_count_number] = "(production_process_information.work_order_sheet_approved_by_status = '".$column_details[0]."'";
					unset($column_details[0]);
					foreach ($column_details as $work_order_sheet_approved_by_status) {
						$return_response['where_string_array'][$current_count_number] .= " OR production_process_information.work_order_sheet_approved_by_status = '".$work_order_sheet_approved_by_status."'";
					}
					$return_response['where_string_array'][$current_count_number] .= ")";
				}
				$current_count_number++;
			}
			if(!$type_search_filter){

				if(!empty($this->session->userdata('type'))){

					$return_response['where_string_array'][] = "production_process_information.type = '{$this->session->userdata('type')}'";
				}
				
			}
			// echo "<pre>";print_r($return_response);echo"</pre><hr>";exit;
		}
		if($this->session->userdata('production_access')['production_product_family_access']) {
			$return_response['where_string_array'][] = " production_process_information.product_family IN ('".implode("', '", $this->session->userdata('production_access')['production_product_family_access'])."')";
		}
		// $return_response['where_string_array'][] = " production_process_information.work_order_no = 2037";
		// echo "<pre>";print_r($return_response);echo"</pre><hr>";exit;
		return $return_response;
	}
	private function default_where_for_all_tab($tab_name){

		// $this->session->set_userdata('production_tab_name',$tab_name);
		$return_where = array(
							0=> "quotation_mst.stage = 'proforma'",
							1=> "quotation_mst.status = 'Won'",
							2=> "quotation_mst.work_order_no != 0",
							3=> "(production_process_information.production_status is NULL OR production_process_information.production_status = 'yet_to_start_production' OR production_process_information.production_status = 'in_production' OR production_process_information.production_status = 'pending_order' OR (production_process_information.production_status = 'semi_ready' OR quotation_dtl.semi_ready_count > 0) OR (production_process_information.production_status IN ('merchant_trade', 'mtt') OR quotation_dtl.mtt_count > 0) OR (production_process_information.production_status = 'import' OR quotation_dtl.import_count > 0) OR (production_process_information.production_status = 'query' OR quotation_dtl.query_count > 0) OR (production_process_information.production_status IN ('mtt_rfd', 'in_transit_import', 'in_transit_mtt') OR quotation_dtl.mtt_rfd_count > 0) OR (production_process_information.production_status = 'ready_for_dispatch' OR quotation_dtl.ready_to_dispatch_order_count > 0))",
							4=> "(production_process_information.status is NULL or production_process_information.status = 'Active')");
		if($tab_name == 'dispatch_order') {
			$return_where[3] = "(production_process_information.production_status = 'dispatched' OR quotation_dtl.dispatch_order_count > 0)";
		
		}elseif ($tab_name == 'on_hold_order') {
			$return_where[3] = "(production_process_information.production_status = 'on_hold' OR quotation_dtl.on_hold_order_count > 0)";
		
		}elseif ($tab_name == 'ready_to_dispatch_order') {
			$return_where[3] = "(production_process_information.production_status = 'ready_for_dispatch' OR quotation_dtl.ready_to_dispatch_order_count > 0)";
		
		}elseif ($tab_name == 'semi_ready') {
			$return_where[3] = "(production_process_information.production_status = 'semi_ready' OR quotation_dtl.semi_ready_count > 0)";
		
		}elseif ($tab_name == 'order_cancelled') {
			$return_where[3] = "(production_process_information.production_status = 'order_cancelled' OR quotation_dtl.order_cancelled_count > 0)";
		
		}elseif ($tab_name == 'mtt') {
			$return_where[3] = "(production_process_information.production_status IN ('merchant_trade', 'mtt') OR quotation_dtl.mtt_count > 0)";
		
		}elseif ($tab_name == 'import') {
			$return_where[3] = "(production_process_information.production_status = 'import' OR quotation_dtl.import_count > 0)";

		}elseif ($tab_name == 'query') {
			$return_where[3] = "(production_process_information.production_status = 'query' OR quotation_dtl.query_count > 0)";
		
		}elseif ($tab_name == 'mtt_rfd') {
			$return_where[3] = "(production_process_information.production_status IN ('mtt_rfd', 'in_transit_import', 'in_transit_mtt') OR quotation_dtl.mtt_rfd_count > 0)";
		}elseif ($tab_name == 'all_order') {

			unset($return_where[3]);
		}

		if($this->session->userdata('role') == 5) {

			$return_where[] = "quotation_mst.assigned_to IN (".implode(', ',$this->session->userdata('production_access')['production_sales_user_id']).")";
		}elseif($this->session->userdata('role') == 8) {
			
			$procurement_user_name = $this->common_model->get_dynamic_data_sales_db_null_false('name',"user_id IN ('".implode("', '", $this->session->userdata('production_access')['production_procurement_user_id'])."')", 'users');
			if(!empty($procurement_user_name)){

				$user_explode = explode(' ',$procurement_user_name[0]['name']);
				$proc_user_like = "( production_process_information.handled_by LIKE '%".$user_explode[0]."%'";
				unset($procurement_user_name[0]);
				foreach ($procurement_user_name as $procurement_user_details) {
					
					$user_explode = explode(' ',$procurement_user_details['name']);
					$proc_user_like .= " OR production_process_information.handled_by LIKE '%".$user_explode[0]."%'";
				}
				$proc_user_like .= " )";
				$user_explode = explode(' ',$procurement_user_name['name']);
				$return_where[] = $proc_user_like;
			}
		}
		if($this->session->userdata('production_access')['production_product_family_access']) {
			$return_where[] = "production_process_information.product_family IN ('".implode("', '", $this->session->userdata('production_access')['production_product_family_access'])."')";
		}
		// $return_where[] =" production_process_information.work_order_no = 2258";

		// echo "<pre>";print_r($return_where);echo"</pre><hr>";exit;
		return $return_where;
	}
	private function default_having_for_all_tab($tab_name){

		if($tab_name == 'pending_order'){
				$having_condition = 'pending_count > 0';

			}elseif($tab_name == 'semi_ready'){
				$having_condition = 'semi_ready_count > 0 OR semi_ready_count_2 > 0';

			}elseif($tab_name == 'mtt'){
				$having_condition = 'mtt_count > 0 OR mtt_count_2 > 0';

			}elseif($tab_name == 'import'){
				$having_condition = 'import_count > 0 OR import_count_2 > 0';

			}elseif($tab_name == 'query'){
				$having_condition = 'query_count > 0 OR query_count_2 > 0';

			}elseif($tab_name == 'mtt_rfd'){
				$having_condition = 'mtt_rfd_count > 0 OR mtt_rfd_count_2 > 0';

			}elseif($tab_name == 'ready_to_dispatch_order'){
				$having_condition = 'rfd_count > 0 OR rfd_count_2 > 0';

			}elseif($tab_name == 'dispatch_order'){
				$having_condition = 'dispatch_count > 0 OR dispatch_count_2 > 0';

			}elseif($tab_name == 'on_hold_order'){
				$having_condition = 'hold_count > 0 OR hold_count_2 > 0';

			}elseif($tab_name == 'order_cancelled'){
				$having_condition = 'cancel_count > 0 OR cancel_count_2 > 0';

			}
		return $having_condition;
	}
	private function prepare_tab_count_data($production_search_form) {
		$production_tabs = [
			'all_order' => 'all_order', 'pending_order' => 'pending_order', 'semi_ready' => 'semi_ready',
			'mtt' => 'mtt', 'import' => 'import', 'query' => 'query', 'mtt_rfd' => 'mtt_rfd',
			'ready_to_dispatch_order' => 'ready_to_dispatch_order', 'dispatch_order' => 'dispatch_order',
			'on_hold_order' => 'on_hold_order', 'order_cancelled' => 'order_cancelled'
		];

		$join_string = "INNER JOIN production_process_information ON production_process_information.quotation_mst_id = quotation_mst.quotation_mst_id
						LEFT JOIN customer_mst ON customer_mst.id = quotation_mst.client_id
						INNER JOIN quotation_dtl ON quotation_dtl.quotation_mst_id = quotation_mst.quotation_mst_id";

		$tab_count = [];
		foreach ($production_tabs as $key => $tab_name) {

			$where_condition['production_search_form'] = $production_search_form;
			$where_condition['tab_name'] = $tab_name;

			$where_array = $this->create_where_response_on_search_filter($where_condition);
			$where_string = implode(' AND ', $where_array['where_string_array']);
			$having_condition = $this->default_having_for_all_tab($tab_name);
			// echo "<pre>";print_r($where_string);echo"</pre><hr>";die;
			$data = $this->production_model->get_production_tab_count_data($where_string, $join_string, $having_condition);

			$tab_count[$key] = $data ?? 0;
		}
		return $tab_count;
	}
	private function upload_doc_config() {

		// echo "<pre>";print_r($_FILES);echo"</pre><hr>";exit;
		$config['upload_path']          = './assets/production_mtc_pdf/';
        $config['allowed_types']        = 'pdf';//'jpeg|gif|jpg|png';
        $config['file_name']            = 'mtc_pdf_'.$this->session->userdata('user_id').'_'.time();
        $config['overwrite']            = TRUE;

        $this->load->library('upload', $config);

		$data = array('status' => 'successful', 'msg' => '', 'file_name' => '');
        if ( ! $this->upload->do_upload('file'))
        {
			$error = array('error' => $this->upload->display_errors());
			$data['status'] = 'failed';
			$data['msg'] = $error['error'];
        }
        else
        {
        	$file_dtls = $this->upload->data();
			$data['file_name'] = $file_dtls['file_name'];
        }
        // echo "<pre>";print_r($data);echo"</pre><hr>";exit;
        return $data;
	}
	private function add_remove_production_count($quotation_id, $product_status, $call_type) {
		
		$update_array = array();
		//get details of production process information based on quotation id
		$production_details = $this->common_model->get_dynamic_data_sales_db('*', array('status' => 'Active', 'quotation_mst_id'=>$quotation_id), 'production_process_information', 'row_array');
		// echo "<pre>";print_r($production_details);echo"</pre><hr>";exit;
		$static_call_type_operation = array('remove'=>'-1', 'add'=>'+1');
		if(!empty($production_details)) {

			if($product_status == 'semi_ready') {
				
				if($call_type == 'remove') {
					if($production_details['semi_ready_count'] != 0) {
						$update_array = array('semi_ready_count'=> $production_details['semi_ready_count']-1);
					}
				}else if($call_type == 'add') {

					$update_array = array('semi_ready_count'=> $production_details['semi_ready_count']+1);
				}
				
			} elseif($product_status == 'ready_to_dispatch_order') {

				if($call_type == 'remove') {
					if($production_details['ready_for_dispatch_count'] != 0) {

						$update_array = array('ready_for_dispatch_count'=> $production_details['ready_for_dispatch_count']-1);
					}
				}else if($call_type == 'add') {

					$update_array = array('ready_for_dispatch_count'=> $production_details['ready_for_dispatch_count']+1);
				}
			} elseif($product_status == 'dispatch_order') {

				if($call_type == 'remove') {
					if($production_details['dispatch_count'] != 0) {
						$update_array = array('dispatch_count'=> $production_details['dispatch_count']-1);
					}
				}else if($call_type == 'add') {

					$update_array = array('dispatch_count'=> $production_details['dispatch_count']+1);
				}
			} elseif($product_status == 'on_hold_order') {

				if($call_type == 'remove') {
					if($production_details['on_hold_count'] != 0) {
						$update_array = array('on_hold_count'=> $production_details['on_hold_count']-1);
					}
				}else if($call_type == 'add') {

					$update_array = array('on_hold_count'=> $production_details['on_hold_count']+1);
				}
			} elseif($product_status == 'order_cancelled') {

				if($call_type == 'remove') {
					if($production_details['cancel_count'] != 0) {
						$update_array = array('cancel_count'=> $production_details['cancel_count']-1);
					}
				}else if($call_type == 'add') {

					$update_array = array('cancel_count'=> $production_details['cancel_count']+1);
				}
			} elseif($product_status == 'mtt') {

				if($call_type == 'remove') {
					if($production_details['mtt_count'] != 0) {
						$update_array = array('mtt_count'=> $production_details['mtt_count']-1);
					}
				}else if($call_type == 'add') {

					$update_array = array('mtt_count'=> $production_details['mtt_count']+1);
				}
			} elseif($product_status == 'query') {

				if($call_type == 'remove') {
					if($production_details['query_count'] != 0) {
						$update_array = array('query_count'=> $production_details['query_count']-1);
					}
				}else if($call_type == 'add') {

					$update_array = array('query_count'=> $production_details['query_count']+1);
				}
			} elseif($product_status == 'mtt_rfd') {

				if($call_type == 'remove') {
					if($production_details['mtt_rfd_count'] != 0) {
						$update_array = array('mtt_rfd_count'=> $production_details['mtt_rfd_count']-1);
					}
				}else if($call_type == 'add') {

					$update_array = array('mtt_rfd_count'=> $production_details['mtt_rfd_count']+1);
				}
			}
			// echo "<pre>";print_r($update_array);echo"</pre><hr>";exit;
			if(!empty($update_array)){

				$this->common_model->update_data_sales_db('production_process_information', $update_array, array('id'=>$production_details['id']));
			}
			return true;
		}
		return false;
	}
	private function get_total_revenue($where, $order_by) {

		$return_array = array();
		$total = array();
		$all_data = $this->production_model->get_production_listingz_data($where, $order_by, 00, 00);
		foreach ($all_data['production_list'] as $production_list_key => $production_list_value) {

			//getting product details
			$product_details = $this->common_model->get_dynamic_data_sales_db('*',array('quotation_mst_id'=>$production_list_value['quotation_mst_id']), 'quotation_dtl');

			$single_product_status_wise_details = array('yet_to_start_production'=>0, 'dispatched'=>0,'on_hold'=>0,'ready_for_dispatch'=>0, 'semi_ready'=>0, 'order_cancelled'=>0);
			if(!empty($product_details)) {
					
				foreach ($product_details as $product_key => $single_product_detail) {
					
					if($single_product_detail['production_status'] == 'yet_to_start_production') {

						$single_product_status_wise_details['yet_to_start_production'] += $single_product_detail['row_price'];
					}else {

						$single_product_status_wise_details[$single_product_detail['production_status']] += $single_product_detail['unit_price']*$single_product_detail['production_quantity_done'];
						$single_product_status_wise_details['yet_to_start_production'] += $single_product_detail['unit_price']*($single_product_detail['quantity']-$single_product_detail['production_quantity_done']);
					}
				}
			}
			if(empty($total[$production_list_value['currency']])){
				$total = array($production_list_value['currency']=>0);
			}
			if($this->session->userdata('production_tab_name') == 'pending_order') {

				$total[$production_list_value['currency']] += $single_product_status_wise_details['yet_to_start_production'];
			}elseif ($this->session->userdata('production_tab_name') == 'dispatch_order') {

				$total[$production_list_value['currency']] += $single_product_status_wise_details['dispatched'];				
			}elseif ($this->session->userdata('production_tab_name') == 'on_hold_order') {

				$total[$production_list_value['currency']] += $single_product_status_wise_details['on_hold'];
			}elseif ($this->session->userdata('production_tab_name') == 'ready_to_dispatch_order') {

				$total[$production_list_value['currency']] += $single_product_status_wise_details['ready_for_dispatch'];				
			}elseif ($this->session->userdata('production_tab_name') == 'semi_ready') {

				$total[$production_list_value['currency']] += $single_product_status_wise_details['semi_ready'];
			}elseif ($this->session->userdata('production_tab_name') == 'order_cancelled') {

				$total[$production_list_value['currency']] += $single_product_status_wise_details['order_cancelled'];
			}
		}
		$i = 0;
		foreach($total as $currency_id => $total_details) {

			$return_array[$i]['currency'] = $currency_id;
			$return_array[$i]['total'] = $total_details;
			$i++;
		}
		return $return_array;
	}
	private function get_total_in_indian_currency($total_details) {

		// getting currency details
		$currency_details = array_column($this->common_model->get_dynamic_data_sales_db('*',array('status'=>'Active'), 'currency'), 'currency_rate', 'currency_id');
		$num = 0;
		foreach ($total_details as $single_details) {
			$num +=  $single_details['total'] * $currency_details[$single_details['currency']];
		}
		return $this->convert_number_into_indian_currency($num);
	}
	private function convert_number_into_indian_currency($num){

		$num = number_format($num,0,'','');
		$new_num = '';
		if(strlen($num) < 4) {
			$new_num = $num;
		}else{
			if(strlen($num) % 2 == 0) {
				for ($i=0; $i < strlen($num)-3; $i++) { 
					
					if(($i % 2) == 0) {

						$new_num .= $num[$i];
						$new_num .= ',';
					}else {

						$new_num .= $num[$i];
					}
				}
				$new_num .= $num[strlen($num)-3];
				$new_num .= $num[strlen($num)-2];
				$new_num .= $num[strlen($num)-1];
			}else {
				for ($i=0; $i < strlen($num)-3; $i++) { 
					
					if(($i % 2) != 0) {

						$new_num .= $num[$i];
						$new_num .= ',';
					}else {

						$new_num .= $num[$i];
					}
				}
				$new_num .= $num[strlen($num)-3];
				$new_num .= $num[strlen($num)-2];
				$new_num .= $num[strlen($num)-1];
			}
		}
		return $new_num;
	}
	private function get_order_wise_total(){

		$pending_order_total = $this->prepare_listing_data(implode(' AND ', $this->default_where_for_all_tab('pending_order')),array(),0,0);
		$semi_ready_total = $this->prepare_listing_data(implode(' AND ', $this->default_where_for_all_tab('semi_ready')),array(),0,0);
		$mtt_total = $this->prepare_listing_data(implode(' AND ', $this->default_where_for_all_tab('mtt')),array(),0,0);
		$import_total = $this->prepare_listing_data(implode(' AND ', $this->default_where_for_all_tab('import')),array(),0,0);
		$query_total = $this->prepare_listing_data(implode(' AND ', $this->default_where_for_all_tab('query')),array(),0,0);
		$mtt_rfd_total = $this->prepare_listing_data(implode(' AND ', $this->default_where_for_all_tab('mtt_rfd')),array(),0,0);
		$ready_to_dispatch_total = $this->prepare_listing_data(implode(' AND ', $this->default_where_for_all_tab('ready_to_dispatch_order')),array(),0,0);
		$return_array[] = array(
							'title_name'=> 'Pending Order',
							'order_type'=> 'pending_order',
							'total' => $this->get_total_in_indian_currency($pending_order_total['total_revenue_list']),
							'currency_wise_total'=> $pending_order_total['total_revenue_list'],
							'total_work_order' => $pending_order_total['paggination_data']['total_rows']
						);
		$return_array[] = array(
							'title_name'=> 'Semi Ready',
							'order_type'=> 'semi_ready',
							'total' => $this->get_total_in_indian_currency($semi_ready_total['total_revenue_list']),
							'currency_wise_total'=> $semi_ready_total['total_revenue_list'],
							'total_work_order' => $semi_ready_total['paggination_data']['total_rows']
						);
		$return_array[] = array(
							'title_name'=> 'MTT',
							'order_type'=> 'mtt',
							'total' => $this->get_total_in_indian_currency($mtt_total['total_revenue_list']),
							'currency_wise_total'=> $mtt_total['total_revenue_list'],
							'total_work_order' => $mtt_total['paggination_data']['total_rows']
						);
		$return_array[] = array(
							'title_name'=> 'Import',
							'order_type'=> 'import',
							'total' => $this->get_total_in_indian_currency($import_total['total_revenue_list']),
							'currency_wise_total'=> $import_total['total_revenue_list'],
							'total_work_order' => $import_total['paggination_data']['total_rows']
						);
		$return_array[] = array(
							'title_name'=> 'Query',
							'order_type'=> 'query',
							'total' => $this->get_total_in_indian_currency($query_total['total_revenue_list']),
							'currency_wise_total'=> $query_total['total_revenue_list'],
							'total_work_order' => $query_total['paggination_data']['total_rows']
						);
		$return_array[] = array(
							'title_name'=> 'MTT/RFD',
							'order_type'=> 'mtt_rfd',
							'total' => $this->get_total_in_indian_currency($mtt_rfd_total['total_revenue_list']),
							'currency_wise_total'=> $mtt_rfd_total['total_revenue_list'],
							'total_work_order' => $mtt_rfd_total['paggination_data']['total_rows']
						);
		$return_array[] = array(
							'title_name'=> 'Ready To Dispatch',
							'order_type'=> 'ready_to_dispatch_order',
							'total' => $this->get_total_in_indian_currency($ready_to_dispatch_total['total_revenue_list']),
							'currency_wise_total'=> $ready_to_dispatch_total['total_revenue_list'],
							'total_work_order' => $ready_to_dispatch_total['paggination_data']['total_rows']
						);
		return $return_array;
	}
	private function default_where_for_all_tab_new($tab_name, $type){

		$this->session->set_userdata('production_tab_name',$tab_name);
		$return_where = array(
							0=> "quotation_mst.stage = 'proforma'",
							1=> "quotation_mst.status = 'Won'",
							2=> "quotation_mst.work_order_no != 0",
							3=> "(production_process_information.production_status is NULL OR production_process_information.production_status != 'dispatched' AND production_process_information.production_status != 'on_hold' AND production_process_information.production_status != 'ready_for_dispatch' AND production_process_information.production_status != 'semi_ready' AND
							production_process_information.production_status != 'merchant_trade' AND
							production_process_information.production_status != 'query' AND
							production_process_information.production_status != 'mtt_rfd' AND
							production_process_information.production_status != 'order_cancelled')",
							4=> "(production_process_information.status is NULL or production_process_information.status = 'Active')");
		if($tab_name == 'dispatch_order') {
			$return_where[3] = "(production_process_information.production_status = 'dispatched' OR production_process_information.dispatch_count > 0)";
		
		}elseif ($tab_name == 'on_hold_order') {
			$return_where[3] = "(production_process_information.production_status = 'on_hold' OR production_process_information.on_hold_count > 0)";
		
		}elseif ($tab_name == 'ready_to_dispatch_order') {
			$return_where[3] = "(production_process_information.production_status = 'ready_for_dispatch' OR production_process_information.ready_for_dispatch_count > 0)";
		
		}elseif ($tab_name == 'semi_ready') {
			$return_where[3] = "(production_process_information.production_status = 'semi_ready' OR production_process_information.semi_ready_count > 0)";
		
		}elseif ($tab_name == 'order_cancelled') {
			$return_where[3] = "(production_process_information.production_status = 'order_cancelled' OR production_process_information.cancel_count > 0)";
		
		}elseif ($tab_name == 'mtt') {
			$return_where[3] = "(production_process_information.production_status = 'merchant_trade' OR production_process_information.mtt_count > 0)";
		
		}elseif ($tab_name == 'query') {
			$return_where[3] = "(production_process_information.production_status = 'query' OR production_process_information.query_count > 0)";
		
		}elseif ($tab_name == 'mtt_rfd') {
			$return_where[3] = "(production_process_information.production_status = 'mtt_rfd' OR production_process_information.mtt_rfd_count > 0)";
		
		}
		if(!empty($type)){

			$return_where[] = "production_process_information.type = '{$type}'";
		}
		// $return_where[] =" production_process_information.work_order_no = 2258";
		
		// echo "<pre>";print_r($return_where);echo"</pre><hr>";exit;
		return $return_where;
	}
	private function get_order_wise_total_new($type){

		$return_array = array(
			'pending_order'=> array(
								'title_name'=> 'Pending Order',
								'order_type'=> 'pending_order',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'pending'
							),
			'semi_ready'=> 	array(
								'title_name'=> 'Semi Ready',
								'order_type'=> 'semi_ready',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'semi_ready',
							),
			'mtt'=> 		array(
								'title_name'=> 'ICE',
								'order_type'=> 'mtt',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'mtt',
								
							),
			'import'=> 		array(
								'title_name'=> 'Import',
								'order_type'=> 'import',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'import',

							),
			'query'=> 		array(
								'title_name'=> 'Query',
								'order_type'=> 'query',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'query',
							),
			'mtt_rfd'=> 	array(
								'title_name'=> 'In transit ICE',
								'order_type'=> 'mtt_rfd',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'mtt_rfd',
							),
			'ready_to_dispatch_order'=> array(
								'title_name'=> 'Ready To Dispatch',
								'order_type'=> 'ready_to_dispatch_order',
								'total' => 0,
								'currency_wise_total'=> array(),
								'total_work_order' => 0,
								'single_data_column'=> 'rfd',
			)
		);
		$currency_details = array_column($this->common_model->get_dynamic_data_sales_db('*',array('status'=>'Active'), 'currency'), 'currency_rate', 'currency_id');
		foreach (array(
					array(
						'type'=> 'pending',
						'where_string'=> " AND (production_process_information.production_status = 'yet_to_start_production' OR production_process_information.production_status = 'in_production' OR production_process_information.production_status = 'pending_order' OR production_process_information.production_status = 'yet_to_start_production' OR (production_process_information.production_status = 'semi_ready' OR quotation_dtl.semi_ready_count > 0) OR (production_process_information.production_status IN ('merchant_trade', 'mtt') OR quotation_dtl.mtt_count > 0) OR (production_process_information.production_status = 'import' OR quotation_dtl.import_count > 0) OR (production_process_information.production_status = 'query' OR quotation_dtl.query_count > 0) OR (production_process_information.production_status IN ('mtt_rfd', 'in_transit_import', 'in_transit_mtt')  OR quotation_dtl.mtt_rfd_count > 0) OR (production_process_information.production_status = 'ready_for_dispatch' OR quotation_dtl.ready_to_dispatch_order_count > 0))"
					),
					array(
						'type'=> 'semi_ready',
						'where_string'=> " AND (production_process_information.production_status = 'semi_ready' OR quotation_dtl.semi_ready_count > 0)"
					),
					array(
						'type'=> 'mtt',
						'where_string'=> " AND (production_process_information.production_status IN ('merchant_trade','mtt') OR quotation_dtl.mtt_count > 0)"
					),
					array(
						'type'=> 'import',
						'where_string'=> " AND (production_process_information.production_status = 'import' OR quotation_dtl.import_count > 0)"
					),
					array(
						'type'=> 'query',
						'where_string'=> " AND (production_process_information.production_status = 'query' OR quotation_dtl.query_count > 0)"
					),
					array(
						'type'=> 'mtt_rfd',
						'where_string'=> " AND (production_process_information.production_status IN ('mtt_rfd', 'in_transit_import', 'in_transit_mtt') OR quotation_dtl.mtt_rfd_count > 0)"
					),
					array(
						'type'=> 'ready_to_dispatch_order',
						'where_string'=> " AND (production_process_information.production_status = 'ready_for_dispatch' OR quotation_dtl.ready_to_dispatch_order_count > 0)"
					),
				) as $single_details) {
			
			$all_data = $this->home_model->get_count_production_status($type, $single_details['where_string']);
			// echo "<pre>";print_r($all_data);echo"</pre><hr>";	

			foreach ($all_data as $single_data) {		

				$total_status_wise = $this->create_total_status_wise($single_data);
				if($single_details['type'] == 'pending' && $total_status_wise['pending_order']['value'] > 0){

					if(!isset($return_array['pending_order']['currency_wise_total'][$single_data['currency']])){

						$return_array['pending_order']['currency_wise_total'][$single_data['currency']] = array(
							'currency'=> $single_data['currency'],
							'total'=> 0
						);
					}
					$return_array['pending_order']['currency_wise_total'][$single_data['currency']]['total'] += $total_status_wise['pending_order']['value'];
					if($this->check_work_order_increase_or_not($single_details['type'], $single_data)){

						$return_array['pending_order']['total_work_order']++;
					}
				}
				if($single_details['type'] == 'semi_ready' && $total_status_wise['semi_ready']['value'] > 0){

					if(!isset($return_array['semi_ready']['currency_wise_total'][$single_data['currency']])){

						$return_array['semi_ready']['currency_wise_total'][$single_data['currency']] = array(
							'currency'=> $single_data['currency'],
							'total'=> 0
						);
					}
					$return_array['semi_ready']['currency_wise_total'][$single_data['currency']]['total'] += $total_status_wise['semi_ready']['value'];
					if($this->check_work_order_increase_or_not($single_details['type'], $single_data)){

						$return_array['semi_ready']['total_work_order']++;
					}
				}
				if($single_details['type'] == 'mtt' && $total_status_wise['mtt']['value'] > 0){

					if(!isset($return_array['mtt']['currency_wise_total'][$single_data['currency']])){

						$return_array['mtt']['currency_wise_total'][$single_data['currency']] = array(
							'currency'=> $single_data['currency'],
							'total'=> 0
						);
					}
					$return_array['mtt']['currency_wise_total'][$single_data['currency']]['total'] += $total_status_wise['mtt']['value'];
					if($this->check_work_order_increase_or_not($single_details['type'], $single_data)){

						$return_array['mtt']['total_work_order']++;
					}
				}
				if($single_details['type'] == 'import' && $total_status_wise['import']['value'] > 0){

					if(!isset($return_array['import']['currency_wise_total'][$single_data['currency']])){

						$return_array['import']['currency_wise_total'][$single_data['currency']] = array(
							'currency'=> $single_data['currency'],
							'total'=> 0
						);
					}
					$return_array['import']['currency_wise_total'][$single_data['currency']]['total'] += $total_status_wise['import']['value'];
					if($this->check_work_order_increase_or_not($single_details['type'], $single_data)){

						$return_array['import']['total_work_order']++;
					}
				}
				if($single_details['type'] == 'query' && $total_status_wise['query']['value'] > 0){


					if(!isset($return_array['query']['currency_wise_total'][$single_data['currency']])){

						$return_array['query']['currency_wise_total'][$single_data['currency']] = array(
							'currency'=> $single_data['currency'],
							'total'=> 0
						);
					}
					$return_array['query']['currency_wise_total'][$single_data['currency']]['total'] += $total_status_wise['query']['value'];
					if($this->check_work_order_increase_or_not($single_details['type'], $single_data)){

						$return_array['query']['total_work_order']++;
					}
				}
				if($single_details['type'] == 'mtt_rfd' && $total_status_wise['mtt_rfd']['value'] > 0){

					if(!isset($return_array['mtt_rfd']['currency_wise_total'][$single_data['currency']])){

						$return_array['mtt_rfd']['currency_wise_total'][$single_data['currency']] = array(
							'currency'=> $single_data['currency'],
							'total'=> 0
						);
					}
					$return_array['mtt_rfd']['currency_wise_total'][$single_data['currency']]['total'] += $total_status_wise['mtt_rfd']['value'];
					if($this->check_work_order_increase_or_not($single_details['type'], $single_data)){

						$return_array['mtt_rfd']['total_work_order']++;
					}
				}
				if($single_details['type'] == 'ready_to_dispatch_order' && $total_status_wise['ready_to_dispatch_order']['value'] > 0){

					if(!isset($return_array['ready_to_dispatch_order']['currency_wise_total'][$single_data['currency']])){

						$return_array['ready_to_dispatch_order']['currency_wise_total'][$single_data['currency']] = array(
							'currency'=> $single_data['currency'],
							'total'=> 0
						);
					}
					$return_array['ready_to_dispatch_order']['currency_wise_total'][$single_data['currency']]['total'] += $total_status_wise['ready_to_dispatch_order']['value'];
					if($this->check_work_order_increase_or_not($single_details['type'], $single_data)){

						$return_array['ready_to_dispatch_order']['total_work_order']++;
					}
				}
			}
			// echo "<pre>";print_r($return_array);echo"</pre><hr>";die('done');
		}

		// echo "<pre>";print_r($return_array);echo"</pre><hr>";die('done');	
		$total_of_3_order = 0;
		$total_of_3_order_number = 0;	
		foreach ($return_array as $status_name => $status_details) {
			
			foreach ($status_details['currency_wise_total'] as $currency_key => $currency_wise_details) {

				$return_array[$status_name]['total'] += $currency_wise_details['total'] *$currency_details[$currency_wise_details['currency']];
				$total_of_3_order += $currency_wise_details['total'] *$currency_details[$currency_wise_details['currency']];
				
				$return_array[$status_name]['currency_wise_total'][$currency_key]['total'] = $this->convert_number_into_indian_currency($return_array[$status_name]['currency_wise_total'][$currency_key]['total']);
			}
			$total_of_3_order_number += (int)$status_details['total_work_order'];
			$return_array[$status_name]['total'] = $this->convert_number_into_indian_currency($return_array[$status_name]['total']);
		}
		$total_of_3_order = $this->convert_number_into_indian_currency($total_of_3_order);
		
		return array('order_status_wise_total'=> $return_array, 'total_of_3_order'=> $total_of_3_order, 'total_of_3_order_number'=> $total_of_3_order_number);
	}
	private function check_work_order_increase_or_not($type, $data){

		switch ($type) {
			case 'pending':
				
				if( $data['pending_count'] > ($data['semi_ready_count'] + $data['rfd_count'] + $data['dispatch_count'] + $data['hold_count'] + $data['mtt_count'] + $data['import_count'] + $data['query_count'] + $data['mtt_rfd_count'] + $data['cancel_count']) ){

					return true;
				}
			break;
			case 'semi_ready':
				
				if( $data['semi_ready_count'] > ($data['pending_count'] + $data['rfd_count'] + $data['dispatch_count'] + $data['hold_count'] + $data['mtt_count'] + $data['import_count'] + $data['query_count'] + $data['mtt_rfd_count'] + $data['cancel_count']) ){

					return true;
				}
			break;
			case 'mtt':
				
				if( $data['mtt_count'] > ($data['pending_count'] + $data['rfd_count'] + $data['dispatch_count'] + $data['hold_count'] + $data['semi_ready_count'] + $data['import_count'] + $data['query_count'] + $data['mtt_rfd_count'] + $data['cancel_count']) ){

					return true;
				}
			break;
			case 'import':

				if( $data['import_count'] > ($data['pending_count'] + $data['rfd_count'] + $data['dispatch_count'] + $data['hold_count'] + $data['semi_ready_count'] + $data['mtt_count'] + $data['query_count'] + $data['mtt_rfd_count'] + $data['cancel_count']) ){

					return true;
				}
			break;
			case 'query':
				
				if( $data['query_count'] > ($data['pending_count'] + $data['rfd_count'] + $data['dispatch_count'] + $data['hold_count'] + $data['mtt_count'] + $data['import_count'] + $data['semi_ready_count'] + $data['mtt_rfd_count'] + $data['cancel_count']) ){

					return true;
				}
			break;
			case 'mtt_rfd':
				
				if( $data['mtt_rfd_count'] > ($data['pending_count'] + $data['rfd_count'] + $data['dispatch_count'] + $data['hold_count'] + $data['mtt_count'] + $data['import_count'] + $data['query_count'] + $data['semi_ready_count'] + $data['cancel_count']) ){

					return true;
				}
			break;
			case 'ready_to_dispatch_order':
				
				if( $data['rfd_count'] > ($data['pending_count'] + $data['semi_ready_count'] + $data['dispatch_count'] + $data['hold_count'] + $data['mtt_count'] + $data['import_count'] + $data['query_count'] + $data['mtt_rfd_count'] + $data['cancel_count']) ){

					return true;
				}
			break;
		}
		return false;
	}
	private function get_order_wise_total_new_old($type){
		
		$pending_order_total = $this->prepare_listing_data_new(implode(' AND ', $this->default_where_for_all_tab_new('pending_order', $type)),array(),0,0);

		$semi_ready_total = $this->prepare_listing_data_new(implode(' AND ', $this->default_where_for_all_tab_new('semi_ready', $type)),array(),0,0);

		$mtt_total = $this->prepare_listing_data_new(implode(' AND ', $this->default_where_for_all_tab_new('mtt', $type)),array(),0,0);

		$query_total = $this->prepare_listing_data_new(implode(' AND ', $this->default_where_for_all_tab_new('query', $type)),array(),0,0);

		$mtt_rfd_total = $this->prepare_listing_data_new(implode(' AND ', $this->default_where_for_all_tab_new('mtt_rfd', $type)),array(),0,0);

		$ready_to_dispatch_total = $this->prepare_listing_data_new(implode(' AND ', $this->default_where_for_all_tab_new('ready_to_dispatch_order', $type)),array(),0,0);

		$return_array[] = array(
							'title_name'=> 'Pending Order',
							'order_type'=> 'pending_order',
							'total' => $this->get_total_in_indian_currency($pending_order_total['total_revenue_list']),
							'currency_wise_total'=> $pending_order_total['total_revenue_list'],
							'total_work_order' => $pending_order_total['paggination_data']['total_rows']
						);
		$return_array[] = array(
							'title_name'=> 'Semi Ready',
							'order_type'=> 'semi_ready',
							'total' => $this->get_total_in_indian_currency($semi_ready_total['total_revenue_list']),
							'currency_wise_total'=> $semi_ready_total['total_revenue_list'],
							'total_work_order' => $semi_ready_total['paggination_data']['total_rows']
						);
		$return_array[] = array(
							'title_name'=> 'ICE',
							'order_type'=> 'mtt',
							'total' => $this->get_total_in_indian_currency($mtt_total['total_revenue_list']),
							'currency_wise_total'=> $mtt_total['total_revenue_list'],
							'total_work_order' => $mtt_total['paggination_data']['total_rows']
						);
		$return_array[] = array(
							'title_name'=> 'Query',
							'order_type'=> 'query',
							'total' => $this->get_total_in_indian_currency($query_total['total_revenue_list']),
							'currency_wise_total'=> $query_total['total_revenue_list'],
							'total_work_order' => $query_total['paggination_data']['total_rows']
						);
		$return_array[] = array(
							'title_name'=> 'In transit ICE',
							'order_type'=> 'mtt_rfd',
							'total' => $this->get_total_in_indian_currency($mtt_rfd_total['total_revenue_list']),
							'currency_wise_total'=> $mtt_rfd_total['total_revenue_list'],
							'total_work_order' => $mtt_rfd_total['paggination_data']['total_rows']
						);
		$return_array[] = array(
							'title_name'=> 'Ready To Dispatch',
							'order_type'=> 'ready_to_dispatch_order',
							'total' => $this->get_total_in_indian_currency($ready_to_dispatch_total['total_revenue_list']),
							'currency_wise_total'=> $ready_to_dispatch_total['total_revenue_list'],
							'total_work_order' => $ready_to_dispatch_total['paggination_data']['total_rows']
						);				
		echo "<pre>";print_r($return_array);echo"</pre><hr>";die('done');					
		return $return_array;
	}
	private function prepare_listing_data_new($where, $order_by, $limit, $offset, $search_filter_array = array()) {

		// echo "<pre>";print_r($where);echo"</pre><hr>";die;
		$data = $this->home_model->get_production_listingz_data($where, $order_by,$limit, $offset);
		// echo "<pre>";print_r($data);echo"</pre><hr>";
		$return_data['total_revenue_list'] = array();
		$return_data['paggination_data'] = $data['paggination_data'];
		foreach ($data['production_list'] as $production_list_key => $production_list_value) {
			
			$temp_grand_total = 0;
			// echo "<pre>";print_r($production_list_value);echo"</pre><hr>";
			if(!isset($return_data['total_revenue_list'][$production_list_value['currency']])){

				$return_data['total_revenue_list'][$production_list_value['currency']]['currency'] = $production_list_value['currency'];
				$return_data['total_revenue_list'][$production_list_value['currency']]['total'] = 0;
			}
			$product_details = $this->common_model->get_dynamic_data_sales_db('*', array('quotation_mst_id'=> $production_list_value['quotation_mst_id']), 'quotation_dtl');
			foreach ($product_details as $key => $single_product_detail) {

				$total_of_count = ($single_product_detail['semi_ready_count'] + $single_product_detail['ready_to_dispatch_order_count'] + $single_product_detail['dispatch_order_count'] + $single_product_detail['on_hold_order_count'] + $single_product_detail['mtt_count'] + $single_product_detail['query_count'] + $single_product_detail['mtt_rfd_count'] + $single_product_detail['order_cancelled_count']);
				if($total_of_count == 0){
	
					$return_data['total_revenue_list'][$production_list_value['currency']]['total'] += $single_product_detail['row_price'];
				}else{
	
					$total_quantity = $single_product_detail['quantity'];
					if($single_product_detail['semi_ready_count'] > 0){

						$return_data['total_revenue_list'][$production_list_value['currency']]['total'] += $single_product_detail['semi_ready_count'] * $single_product_detail['unit_price'];
						$total_quantity = $total_quantity - $single_product_detail['semi_ready_count'];
					}
					if($single_product_detail['ready_to_dispatch_order_count'] > 0){

						$return_data['total_revenue_list'][$production_list_value['currency']]['total'] += $single_product_detail['ready_to_dispatch_order_count'] * $single_product_detail['unit_price'];
						$total_quantity = $total_quantity - $single_product_detail['ready_to_dispatch_order_count'];
					}
					if($single_product_detail['dispatch_order_count'] > 0){

						$return_data['total_revenue_list'][$production_list_value['currency']]['total'] += $single_product_detail['dispatch_order_count'] * $single_product_detail['unit_price'];
						$total_quantity = $total_quantity - $single_product_detail['dispatch_order_count'];
					}
					if($single_product_detail['on_hold_order_count'] > 0){

						$return_data['total_revenue_list'][$production_list_value['currency']]['total'] += $single_product_detail['on_hold_order_count'] * $single_product_detail['unit_price'];
						$total_quantity = $total_quantity - $single_product_detail['on_hold_order_count'];
					}
					if($single_product_detail['mtt_count'] > 0){

						$return_data['total_revenue_list'][$production_list_value['currency']]['total'] += $single_product_detail['mtt_count'] * $single_product_detail['unit_price'];
						$total_quantity = $total_quantity - $single_product_detail['mtt_count'];
					}
					if($single_product_detail['query_count'] > 0){

						$return_data['total_revenue_list'][$production_list_value['currency']]['total'] += $single_product_detail['query_count'] * $single_product_detail['unit_price'];
						$total_quantity = $total_quantity - $single_product_detail['query_count'];
					}
					if($single_product_detail['mtt_rfd_count'] > 0){

						$return_data['total_revenue_list'][$production_list_value['currency']]['total'] += $single_product_detail['mtt_rfd_count'] * $single_product_detail['unit_price'];
						$total_quantity = $total_quantity - $single_product_detail['mtt_rfd_count'];
					}
					if($single_product_detail['order_cancelled_count'] > 0){

						$return_data['total_revenue_list'][$production_list_value['currency']]['total'] += $single_product_detail['order_cancelled_count'] * $single_product_detail['unit_price'];
						$total_quantity = $total_quantity - $single_product_detail['order_cancelled_count'];
					}
					if($total_quantity > 0){

						$return_data['total_revenue_list'][$production_list_value['currency']]['total'] += $total_quantity * $single_product_detail['unit_price'];
					}
				}
			}
			// if(($production_list_value['semi_ready_count'] + $production_list_value['ready_for_dispatch_count'] + $production_list_value['dispatch_count'] + $production_list_value['on_hold_count'] + $production_list_value['query_count'] + $production_list_value['mtt_rfd_count'] + $production_list_value['mtt_count'] + $production_list_value['cancel_count']) > 0){
				
			// 	if($this->session->userdata('production_tab_name') == 'pending_order') {

			// 		$temp_grand_total = $this->home_model->get_pending_count($production_list_value['quotation_mst_id']);
			// 	}else{

			// 		$temp_grand_total = $this->home_model->get_other_count($production_list_value['quotation_mst_id'], $this->session->userdata('production_tab_name'));
			// 	}
			// 	// $temp_grand_total = $this->home_model->get_pending_count($production_list_value['quotation_mst_id']);
			// 	if($temp_grand_total > 0){

			// 		$return_data['total_revenue_list'][$production_list_value['currency']]['total'] += $temp_grand_total;
			// 		$return_data['total_revenue_list'][$production_list_value['currency']]['total'] += ($production_list_value['freight'] + $production_list_value['bank_charges'] + $production_list_value['gst']);
			// 		$return_data['total_revenue_list'][$production_list_value['currency']]['total'] -= $production_list_value['discount'];
			// 	}else{

			// 		$return_data['paggination_data']['total_rows'] = $return_data['paggination_data']['total_rows']-1;
			// 		if($return_data['paggination_data']['total_rows'] == 0){

			// 			$return_data['total_revenue_list'] = array();
			// 		}
			// 	}
			// }else{

			// 	$return_data['total_revenue_list'][$production_list_value['currency']]['total'] += $production_list_value['grand_total'];
			// }
			// echo "<pre>";print_r($return_data['total_revenue_list']);echo"</pre><hr>";
		}
		// echo "<pre>";print_r($return_data);echo"</pre><hr>";die('came in');
		return $return_data;
	}
	private function create_select_drop_down_data($master_array, $master_array_key_name, $condition_value, $default_value = 'selected'){

		if(! is_array($condition_value)){
			$condition_value = array($condition_value);
		}
		$return_array = array();
		foreach ($master_array as $master_key => $master_details) {
				
			$return_array[$master_key] = $master_details;
			$return_array[$master_key]['selected'] = '';
			if(!empty($condition_value) && in_array($master_details[$master_array_key_name], $condition_value)){

				$return_array[$master_key]['selected'] = $default_value;
			}
		}
		return $return_array;
	}
	public function create_production_handle_by_new(){

		$production_details = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'), 'production_process_information', 'result_array');
		foreach ($production_details as $production_details_key => $production_details_value) {
			
			$production_update_array['handled_by_new'] = '';
			// getting quotation mst details
			$quotation_details = $this->common_model->get_dynamic_data_sales_db('*', array('quotation_mst_id'=> $production_details_value['quotation_mst_id']), 'quotation_mst', 'row_array');
			if(!empty($quotation_details)){

				// getting rfq details
				$rfq_details = $this->common_model->get_dynamic_data_sales_db('*', array('rfq_mst_id'=> $quotation_details['rfq_id']), 'rfq_mst', 'row_array');
				if(!empty($rfq_details)){

					$rfq_assign_array = array();
					if(!empty($rfq_details['assigned_to'])){

						$rfq_assign_array[] = $rfq_details['assigned_to'];
					}
					if(!empty($rfq_details['assigned_to_1']) && !in_array($rfq_details['assigned_to_1'], $rfq_assign_array)){

						$rfq_assign_array[] = $rfq_details['assigned_to_1'];
					}
					if(!empty($rfq_details['assigned_to_2']) && !in_array($rfq_details['assigned_to_2'], $rfq_assign_array)){
					
						$rfq_assign_array[] = $rfq_details['assigned_to_2'];
					}
					if(!empty($rfq_details['assigned_to_3']) && !in_array($rfq_details['assigned_to_3'], $rfq_assign_array)){

						$rfq_assign_array[] = $rfq_details['assigned_to_3'];
					}
					// echo "<pre>";print_r($rfq_assign_array);echo"</pre><hr>";exit;
					if(!empty($rfq_assign_array)){

						$user_details = array_column($this->common_model->get_dynamic_data_sales_db('user_id, name', array(), 'users'), 'name', 'user_id');
						foreach ($rfq_assign_array as $user_id) {
							
							$explode_user_details = explode(' ', $user_details[$user_id]);
							if(!empty($production_update_array['handled_by_new'])){

								$production_update_array['handled_by_new'] .= ','.$explode_user_details[0];
							}else{

								$production_update_array['handled_by_new'] .= $explode_user_details[0];
							}
						}
						// echo "<pre>";print_r($production_update_array);echo"</pre><hr>";die('debug');
					}
				}
			}
			// echo "<pre>";print_r($production_update_array);echo"</pre><hr>";exit;
			$this->common_model->update_data_sales_db('production_process_information', $production_update_array, array('id'=>$production_details_value['id']));

		}
		echo "<pre>";print_r($production_details);echo"</pre><hr>";
	}
	private function create_yta_data(){

		$where_string = '';
		$where_string .= "quotation_mst.stage = 'proforma'";
		$where_string .= " AND quotation_mst.status = 'Won'";
		$where_string .= " AND quotation_mst.confirmed_on >= '".date('Y-m-d 00:00:00', strtotime('- 30 days'))."'";
		$where_string .= " AND quotation_mst.confirmed_on <= '".date('Y-m-d 23:59:59')."'";
		$where_string .= " AND (quotation_mst.work_order_no = 0 OR quotation_mst.yet_to_acknowledge = 'pending' OR quotation_mst.acknowledge = 'pending') ";
		if(in_array($this->session->userdata('role'), array(5, 8, 18))){

			if($this->session->userdata('role') == 5){

				$where_string .= " AND quotation_mst.assigned_to IN (".implode(', ',$this->session->userdata('production_access')['production_sales_user_id']).")";
			}else if($this->session->userdata('role') == 8){

				$where_string .= " AND ( rfq_mst.assigned_to_1 = '".$this->session->userdata('user_id')."' OR rfq_mst.assigned_to_2 = '".$this->session->userdata('user_id')."' OR rfq_mst.assigned_to_3 = '".$this->session->userdata('user_id')."' )";
			}
		}
		// echo "<pre>";print_r($where_string);echo"</pre><hr>";die('debug');
		$data = $this->production_model->get_production_listingz_data_for_first_tab($where_string, array('column_name'=> 'confirmed_on', 'column_value'=> 'desc'));
		// all currency list
		$data['currency_list'] = $this->get_common_data('currency_details');
		// all user list
		$data['users_details'] = $this->get_common_data('users_details');
		foreach ($data['production_list'] as $production_list_key => $production_list_value) {
		
			//getting client name
			$data['production_list'][$production_list_key]['client_details'] = $this->common_model->get_dynamic_data_sales_db('name',array('id'=>$production_list_value['client_id']), 'customer_mst', 'row_array');

			// getting currency symbol
			$data['production_list'][$production_list_key]['currency_details']['currency_icon'] = $data['currency_list'][$production_list_value['currency']];

			$data['production_list'][$production_list_key]['work_order_action_name'] = 'WO';
			if(!empty($production_list_value['work_order_no']) && $production_list_value['work_order_no'] > 0){

				$data['production_list'][$production_list_key]['work_order_class_name'] = 'btn btn-success btn-sm';
			}else{

				$data['production_list'][$production_list_key]['work_order_class_name'] = 'btn btn-warning btn-sm';
				if(in_array($this->session->userdata('role'), array('6', '17')) || in_array($this->session->userdata('user_id'), array('38'))){
					
					$data['production_list'][$production_list_key]['work_order_class_name'] .= ' create_work_order';
				}
			}

			$data['production_list'][$production_list_key]['acknowledge_order_action_name'] = 'Proc Acknowledge';
			if($production_list_value['yet_to_acknowledge'] == 'done'){
				
				$data['production_list'][$production_list_key]['acknowledge_order_class_name'] = 'btn btn-success btn-sm';
			}else{
				
				$data['production_list'][$production_list_key]['acknowledge_order_class_name'] = 'btn btn-warning btn-sm';
				if(in_array($this->session->userdata('role'), array('6', '17')) || in_array($this->session->userdata('user_id'), array('38'))){
					
					$data['production_list'][$production_list_key]['acknowledge_order_class_name'] .= ' acknowledge';
				}
			}

			$data['production_list'][$production_list_key]['sales_acknowledge_order_action_name'] = 'Sales Acknowledge';
			if($production_list_value['acknowledge'] == 'done'){
				
				$data['production_list'][$production_list_key]['sales_acknowledge_order_class_name'] = 'btn btn-success btn-sm';
			}else{

				$data['production_list'][$production_list_key]['sales_acknowledge_order_class_name'] = 'btn btn-warning btn-sm';
				if($production_list_value['yet_to_acknowledge'] == 'done' && in_array($this->session->userdata('role'), array('5', '16'))){
					
					$data['production_list'][$production_list_key]['sales_acknowledge_order_class_name'] .= ' sales_acknowledge';
				}

			}
		}
		
		$order_status_wise_total = $this->get_order_wise_total_new($this->session->userdata('type'));
		$data['order_status_wise_total'] = $order_status_wise_total['order_status_wise_total'];
		$data['total_of_3_order'] = $order_status_wise_total['total_of_3_order'];
		$data['total_of_3_order_number'] = $order_status_wise_total['total_of_3_order_number'];
		// echo "<pre>";print_r($data);echo"</pre><hr>"; die;
		return $data;
	}
	private function main_photos_html($file_name, $height){

		if(empty($file_name)){

			return "";
		}
		$return_html = "";
		$explode_file_path = explode('.', $file_name);
		if(!empty($explode_file_path)){

			if(in_array($explode_file_path[count($explode_file_path)-1], array('jpg', 'jpeg', 'gif', 'png'))){

				$return_html = '<img src="https://storage.googleapis.com/documentation_bucket_name/'.$file_name.'" class="set_main_photos" alt="Photo" style="touch-action: auto; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); background: #000; border-radius: 12px; margin-left: 25px; height:'.$height.'; width: 100%;">';
			}else{

				$return_html = '<iframe src="https://docs.google.com/gview?url=https://storage.googleapis.com/documentation_bucket_name/'.$file_name.'&embedded=true" class="sidebar_image change_main_photos" frameborder="0" style="height:'.$height.';"></iframe>';
			}
		}

		return $return_html;
	}
	private function add_image_html_based_on_document_type($file_name_array, $height){

		// echo "<pre>";print_r($file_name_array);echo"</pre><hr>";exit;
		$return_array = array();
		if(!empty($file_name_array)){

			foreach ($file_name_array as $file_key => $single_file_name) {
				
				$explode_file_path = explode('.', $single_file_name['file_name']);
				if(!empty($explode_file_path)){

					if(in_array($explode_file_path[count($explode_file_path)-1], array('jpg', 'jpeg', 'gif', 'png'))){

						$return_array[] = array(
							'image_html'=> '<img src="https://storage.googleapis.com/documentation_bucket_name/'.$single_file_name['file_name'].'" class="sidebar_image change_main_photos '.$single_file_name['selected_class'].'" alt="Photo" height="101" width="141" file_name="'.$single_file_name['file_name'].'" main_photos_height="'.$height.'">',
							'div_width'=> '48'
						);
						if($return_array[count($return_array)-1]['div_width'] == '48' && $return_array[count($return_array)-2]['div_width'] != '4'){

							$return_array[] = array(
								'image_html'=> '',
								'div_width'=> '4'
							);
						}
					}else{

						$return_array[] = array(
							'image_html'=> '<iframe src="https://docs.google.com/gview?url=https://storage.googleapis.com/documentation_bucket_name/'.$single_file_name['file_name'].'&embedded=true" class="sidebar_image change_main_photos '.$single_file_name['selected_class'].'" frameborder="0" file_name="'.$single_file_name['file_name'].'" main_photos_height="'.$height.'" style="height:300px;"></iframe>',
							'div_width'=> '100'
						);
					}
				}
			}	
		}
		
		return $return_array;
	}
	private function upload_technical_doc_config() {

		// echo "<pre>";print_r($_FILES);echo"</pre><hr>";exit;
		$config['upload_path']          = './assets/production_technical_document/';
        $config['allowed_types']        = 'pdf|doc|docx|csv|jpeg|gif|jpg|png';
        $config['overwrite']            = TRUE;

        $this->load->library('upload', $config);

		$data = array('status' => 'successful', 'msg' => '', 'file_name' => '');
        if ( ! $this->upload->do_upload('file'))
        {
			$error = array('error' => $this->upload->display_errors());
			$data['status'] = 'failed';
			$data['msg'] = $error['error'];
        }
        else
        {
			$file_dtls = $this->upload->data();
			$data['file_type'] = $file_dtls['file_type'];
			$data['file_name'] = $file_dtls['file_name'];
			$data['file_add_date'] = date('Y-m-d H:i:s');
        }
        // echo "<pre>";print_r($data);echo"</pre><hr>";exit;
        return $data;
	}
	private function get_indian_time($date_format = 'Y-m-d H:i:s'){

		date_default_timezone_set('Asia/Kolkata');
		return date($date_format);
	}
	private function creat_tat($date_time){

		$return_tat = '';
		// Get the timestamp of the chat message
		$chat_time = strtotime(str_replace("Z", "", $date_time)); // Replace with your chat message timestamp

		// Create DateTime objects for the chat message and current time
		$chat_date = new DateTime("@$chat_time");
		$current_date = new DateTime();
		// Get the difference between the two dates
		$date_diff = $current_date->diff($chat_date);

		// Output the result
		if ($date_diff->y > 0) {

			$return_tat = $date_diff->y . " year(s)";
		}else if ($date_diff->m > 0) {

			$return_tat = $date_diff->m . " month(s)";
		}else if ($date_diff->d > 0) {

			$return_tat = $date_diff->d . " day(s)";
		}else if ($date_diff->h > 0) {

			$return_tat = $date_diff->h . " hour(s)";
		}else if ($date_diff->i > 0) {

			$return_tat = $date_diff->i . " minute(s)";
		}else {

			$return_tat = $date_diff->s . " second(s)";
		}

		return $return_tat." ago";
	}
	private function osdr_document_upload_config() {

		// echo "<pre>";print_r($_FILES);echo"</pre><hr>";exit;
		$config['upload_path']          = './assets/production_osdr_document/';
        $config['allowed_types']        = 'pdf|doc|docx|csv|jpeg|gif|jpg|png';
        $config['file_name']            = 'osdr_'.$this->session->userdata('user_id').'_'.time();
        $config['overwrite']            = TRUE;

        $this->load->library('upload', $config);

		$data = array('status' => 'successful', 'msg' => '', 'file_name' => '');
        if ( ! $this->upload->do_upload('file'))
        {
			$error = array('error' => $this->upload->display_errors());
			$data['status'] = 'failed';
			$data['msg'] = $error['error'];
        }
        else
        {
			$file_dtls = $this->upload->data();
			$data['file_type'] = $file_dtls['file_type'];
			$data['file_name'] = $file_dtls['file_name'];
			$data['file_add_date'] = date('Y-m-d H:i:s');
        }
        // echo "<pre>";print_r($data);echo"</pre><hr>";exit;
        return $data;
	}
}