<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Production extends MX_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('common/common_model');
		$this->load->model('production_model');
	}

	public function index(){
		redirect('production/production_listingz');		
	}

	public function production_listingz() {

		$this->session->set_userdata('production_tab_name','');
		$this->session->set_userdata('search_filter_proforma_date', 'time pass');
		$this->session->set_userdata('search_filter_delivery_date', 'time pass');
		if(in_array($this->session->userdata('role'), array(1, 4, 5, 6, 8, 16, 17))) {
			$data = $this->prepare_listing_data(implode(' AND ', $this->default_where_for_all_tab('pending_order')),array(),0,0);
			$this->load->view('header', array('title' => 'Production Listing'));
			$this->load->view('sidebar', array('title' => 'Production Listing'));
			$this->load->view('production/product_listing', $data);
			$this->load->view('footer');
		} else {
			die("Soory!!!. You Dont have Access to this.");
		}
	}

	public function update_production_listing() {

		$quotation_id = $this->uri->segment('3', 0);
		if(empty($quotation_id)) {
			redirect('production/production_listingz');
		}
		$data = array();
		$data['quotation_details'] = $this->common_model->get_dynamic_data_sales_db('*',array('quotation_mst_id'=>$quotation_id), 'quotation_mst','row_array');
		$data['users_details'] = $this->get_common_data('users_details');
		if(!empty($data['quotation_details'])) {

			$data['client_details'] = $this->common_model->get_dynamic_data_sales_db('*',array('client_id'=>$data['quotation_details']['client_id']), 'clients', 'row_array');
			$data['currency_details'] = $this->common_model->get_dynamic_data_sales_db('*',array('status'=>'Active', 'currency_id'=>$data['quotation_details']['currency']), 'currency', 'row_array');
			$data['payment_term_details'] = $this->common_model->get_dynamic_data_sales_db('*',array('status'=>'Active', 'term_id'=>$data['quotation_details']['payment_term']), 'payment_terms', 'row_array');
		}
		$product_details = $this->common_model->get_dynamic_data_sales_db('*',array('quotation_mst_id'=>$quotation_id), 'quotation_dtl');
		$data['handle_by_users'] = $this->get_common_data('handle_by');$this->common_model->get_procurement_person('user_id, name');
		$data['product_list'] = array();
		$data['material_list'] = array();
		if(!empty($product_details)) {

			$products = array_column($this->common_model->get_dynamic_data_sales_db('*', array('lookup_group'=>259, 'status' => 'Active'), 'lookup'), 'lookup_value', 'lookup_id');	
			$materials = array_column($this->common_model->get_dynamic_data_sales_db('*', array('lookup_group'=>272, 'status' => 'Active'), 'lookup'), 'lookup_value', 'lookup_id');
			foreach ($product_details as $single_product_detail) {
				if(!empty($products[$single_product_detail['product_id']])) {

					if(!in_array($products[$single_product_detail['product_id']], $data['product_list'])) {

						$data['product_list'][] = $products[$single_product_detail['product_id']]; 		
					}
				}
				if(!empty($materials[$single_product_detail['material_id']])) {

					if(!in_array($materials[$single_product_detail['material_id']], $data['material_list'])) {

						$data['material_list'][] = $materials[$single_product_detail['material_id']];
					}
				}
			}
		}
		$data['production_information'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active','quotation_mst_id'=>$quotation_id), 'production_process_information', 'row_array');
		if(empty($data['production_information'])) {
			$data['production_information'] = array('id'=>'', 'work_order_no'=>'', 'product_family'=> '', 'delivery_date'=>date('Y-m-d'), 'vendor_po'=>'', 'handled_by'=>'', 'production_status'=>'', 'payment_status'=>'', 'qc_clearance'=>'', 'latest_update'=>'', 'special_comment'=>''); 
		}
		$data['production_status'] =  $this->create_production_status_data();

		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Production Update'));
		$this->load->view('sidebar', array('title' => 'Production Update'));
		$this->load->view('production/product_update', $data);
		$this->load->view('footer');	
	}

	public function ajax_function() {

		if($this->input->is_ajax_request()){
			$call_type = $this->input->post('call_type');
			$post_details = $this->input->post();
			$response['status'] = 'successful';	
			switch ($post_details['call_type']) {

				case 'save_production_details_form':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";die;
					$form_data = $post_details['production_details_form'];
					if(!empty($form_data)) {
						$insert_array = array();
						
						foreach ($form_data as $single_form_value) {
							
							if(empty($insert_array[$single_form_value['name']])) {

								$insert_array[$single_form_value['name']] = trim($single_form_value['value']);
							} else {

								$insert_array[$single_form_value['name']] = $insert_array[$single_form_value['name']].','.$single_form_value['value'];
							}
						}
						$id=$insert_array['id'];
						unset($insert_array['id']);
						if(empty($id)) {
							$last_quote_id = $this->common_model->get_dynamic_data_sales_db('last_quote_id', array('status'=> "Active", 'quote_name'=>'production'), 'last_quote_created_number', 'row_array');
							$next_quote_id = ((int)$last_quote_id['last_quote_id'])+1;
							$insert_array['work_order_no'] = $next_quote_id;
							// echo "<pre>";print_r($insert_array);echo"</pre><hr>";exit;
							$this->common_model->insert_data_sales_db('production_process_information', $insert_array);
							$this->common_model->update_data_sales_db('last_quote_created_number', array('last_quote_id'=>$next_quote_id), array('quote_name'=>'production'));
						} else {

							$this->common_model->update_data_sales_db('production_process_information', $insert_array, array('id'=>$id));
						}
					}
					// echo "<pre>";print_r($insert_array);echo"</pre><hr>";exit;
				break;

				case 'change_tab':
					
					$tab_name = $post_details['tab_name'];
					if($tab_name == 'dispatch_order') {

						$data = $this->prepare_listing_data(implode(' AND ', $this->default_where_for_all_tab('dispatch_order')),array(),10,0);
					}elseif ($tab_name == 'pending_order') {

						$data = $this->prepare_listing_data(implode(' AND ', $this->default_where_for_all_tab('pending_order')),array(),10,0);
					}elseif ($tab_name == 'on_hold_order') {

						$data = $this->prepare_listing_data(implode(' AND ', $this->default_where_for_all_tab('on_hold_order')),array(),10,0);
					}elseif ($tab_name == 'ready_to_dispatch_order') {

						$data = $this->prepare_listing_data(implode(' AND ', $this->default_where_for_all_tab('ready_to_dispatch_order')),array(),10,0);
					}
					$response['search_filter_body'] = $this->load->view('production/product_list_search_filter_form', $data, true);
					$response['list_body'] = $this->load->view('production/production_list_body', $data, true);
					$response['paggination_filter_body'] = $this->load->view('production/product_list_paggination', $data, true);
				break;

				case 'search_filter':
					
					$tab_name = $post_details['tab_name'];
					$where_response = $this->create_where_response_on_search_filter($post_details['production_search_form'], $tab_name);
					$data = $this->prepare_listing_data(implode(' AND ', $where_response['where_string_array']),array(),10,0, $where_response['where_array']);
					$response['search_filter_body'] = $this->load->view('production/product_list_search_filter_form', $data, true);
					$response['list_body'] = $this->load->view('production/production_list_body', $data, true);
					$response['paggination_filter_body'] = $this->load->view('production/product_list_paggination', $data, true);
				break;

				case 'paggination_filter':
					
					$tab_name = $post_details['tab_name'];
					$where_response = $this->create_where_response_on_search_filter($post_details['production_search_form'], $tab_name);
					$data = $this->prepare_listing_data(implode(' AND ', $where_response['where_string_array']),array(),$post_details['limit'],$post_details['offset'], $where_response['where_array']);
					$response['search_filter_body'] = $this->load->view('production/product_list_search_filter_form', $data, true);
					$response['list_body'] = $this->load->view('production/production_list_body', $data, true);
					$response['paggination_filter_body'] = $this->load->view('production/product_list_paggination', $data, true);
				break;

				case 'delete_production_list':
					$production_id = $post_details['delete_id'];
					$response['status'] = 'failed';	
					if(!empty($production_id)) {
						$this->common_model->update_data_sales_db('production_process_information', array('status'=>'Inactive'), array('id'=>$production_id));
						$response['status'] = 'successful';
					}
				break;

				case 'get_query_history':
					$res = $this->production_model->get_query_history($this->input->post('quote_id'), $this->input->post('query_type'));
					$response['query_table_history'] = '<table class="table table-bordered table-stripped"><tr><th>Sr #</th><th>Query Message</th><th>Query Raised By</th><th>Query Answer By</th></tr>';
					if(!empty($res)) {
						$all_user = $this->get_common_data('users_details');
						// echo "<pre>";print_r($res);echo"</pre><hr>";
						// echo "<pre>";print_r($all_user);echo"</pre><hr>";exit;
						foreach ($res as $res_key => $res_value) {
							
							$response['query_table_history'] .= "<tr>";
								$response['query_table_history'] .= "<td>".++$res_key."</td>";
								$response['query_table_history'] .= "<td style='width:60%;'>".$res_value['query_text']."</td>";
								$response['query_table_history'] .= "<td>";
								$response['query_table_history'] .= (!empty($all_user[$res_value['raised_by']])) ? $all_user[$res_value['raised_by']] : 'No user found';
								$response['query_table_history'] .= "</td>";
								$response['query_table_history'] .= "<td>";
								$response['query_table_history'] .= (!empty($all_user[$res_value['query_recepient']]))? $all_user[$res_value['query_recepient']] : 'No User found';
								$response['query_table_history'] .= "</td";
							$response['query_table_history'] .= "</tr>";
						}
					}
					$response['query_table_history'] .= '</table>';
				break;

				case 'create_work_order':
					$production_process_information = $this->common_model->get_dynamic_data_sales_db('*', array('status'=> "Active", 'quotation_mst_id'=>$this->input->post('quotation_mst_id')), 'production_process_information', 'row_array');
					if(empty($production_process_information)) {

						$last_quote_id = $this->common_model->get_dynamic_data_sales_db('last_quote_id', array('status'=> "Active", 'quote_name'=>'production'), 'last_quote_created_number', 'row_array');
						$next_quote_id = ((int)$last_quote_id['last_quote_id'])+1;
						$this->common_model->insert_data_sales_db('production_process_information', array('work_order_no'=>$next_quote_id, 'quotation_mst_id'=>$this->input->post('quotation_mst_id')));
						$this->common_model->update_data_sales_db('quotation_mst', array('work_order_no'=>$next_quote_id), array('quotation_mst_id'=>$this->input->post('quotation_mst_id')));
						$this->common_model->update_data_sales_db('last_quote_created_number', array('last_quote_id'=>$next_quote_id), array('quote_name'=>'production'));
					}else {

						$this->common_model->update_data_sales_db('quotation_mst', array('work_order_no'=>$production_process_information['work_order_no']), array('quotation_mst_id'=>$this->input->post('quotation_mst_id')));
					}
				break;

				default:
					$response['status'] = 'failed';	
					$response['message'] = "call type not found";
				break;
			}
			echo json_encode($response);

		} else{
			die('access is not allowed to this function');
		}
	}

	private function prepare_listing_data($where, $order_by, $limit, $offset, $search_filter_array = array()) {

		$data = $this->production_model->get_production_listingz_data($where, $order_by, $limit, $offset);
		// all product list
		$data['product_list'] = $this->get_common_data('product_list');
		// all material list
		$data['material_list'] = $this->get_common_data('material_list');
		// all currency list
		$data['currency_list'] = $this->get_common_data('currency_details');
		foreach ($data['production_list'] as $production_list_key => $production_list_value) {
			
			//getting client name
			$data['production_list'][$production_list_key]['client_details'] = $this->common_model->get_dynamic_data_sales_db('client_name',array('client_id'=>$production_list_value['client_id']), 'clients', 'row_array');

			// getting currency symbol
			$data['production_list'][$production_list_key]['currency_details']['currency_icon'] = $data['currency_list'][$production_list_value['currency']];

			//getting product details
			$product_details = $this->common_model->get_dynamic_data_sales_db('*',array('quotation_mst_id'=>$production_list_value['quotation_mst_id']), 'quotation_dtl');

			// production status name
			$data['production_list'][$production_list_key]['production_status_name'] = $this->create_production_status_data('single', $production_list_value['production_status']);
			// echo "<pre>";print_r($product_details);echo"</pre><hr>";
			if(!empty($product_details)) {
				
				$data['production_list'][$production_list_key]['product_details'] = array();
				$product_array = array();
				$material_array = array();
				foreach ($product_details as $product_key => $single_product_detail) {
					
					if(!empty($data['product_list'][$single_product_detail['product_id']])) {

						if(!in_array($data['product_list'][$single_product_detail['product_id']], $product_array)) {

							$product_array[] = $data['product_list'][$single_product_detail['product_id']];
						}
					}
					if(!empty($data['material_list'][$single_product_detail['material_id']])) {

						if(!in_array($data['material_list'][$single_product_detail['material_id']], $material_array)) {

							$material_array[] = $data['material_list'][$single_product_detail['material_id']];
						}
					}
				}
				//finding maximum count
				$higher_count = (count($product_array)>count($material_array)) ? count($product_array) : count($material_array);
				for ($i=0; $i < $higher_count; $i++) { 
					$data['production_list'][$production_list_key]['product_details'][$i]['product_name'] = '';
					$data['production_list'][$production_list_key]['product_details'][$i]['material_name'] = '';
					if(!empty($product_array[$i])) {

						$data['production_list'][$production_list_key]['product_details'][$i]['product_name'] = $product_array[$i];
					}
					if(!empty($material_array[$i])) {

						$data['production_list'][$production_list_key]['product_details'][$i]['material_name'] = $material_array[$i];
					}
				}
			}
		}

		// all client name list
		$data['client_list'] = $this->production_model->get_production_listingz_client_list_data();
		
		// all handle by list
		$data['handle_by_users'] = $this->get_common_data('handle_by');
		// all production status list
		$data['production_status'] = $this->create_production_status_data();
		// all user list
		$data['users_details'] = $this->get_common_data('users_details');
		// all user Sales Person list
		$data['sales_person_details'] = $this->get_common_data('sales_person');
		$data['payment_term_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*',array('status'=>'Active'), 'payment_terms'),'term_value', 'term_id');
		$data['paggination_data']['limit'] = $limit;
		$data['paggination_data']['offset'] = $offset;
		$data['search_filter'] = $this->create_search_filter_data($search_filter_array);
		$data['total_revenue_list'] = $this->production_model->production_listingz_total_revenue($where);
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		return $data;
	}

	private function create_production_status_data($call_type='all', $production_status_name = ''){

		$static_production_status =  array(
										'yet_to_start_production'=> array('name'=>'Yet To Start Production','color'=>'dark'),
										'in_production'=> array('name'=>'In Production', 'color'=>'warning'),
										'ready_for_dispatch'=>array('name'=>'Ready For Dispatch', 'color'=>'primary'),
										'delay_in_delivery'=>array('name'=>'Delay in Delivery', 'color'=>'danger'),
										'on_hold'=>array('name'=>'Order On Hold', 'color'=>'gainsboro'),
										'dispatched'=>array('name'=>'Dispatched', 'color'=>'success')
									);
		if($call_type == 'all') {

			return $static_production_status;
		} else {

			return (!empty($production_status_name)) ? $static_production_status[$production_status_name] : array('name'=>'','color'=>'');
		}
	}

	private function get_common_data($data_type = '') {

		$return_array = array();
		if(!empty($data_type)) {

			switch ($data_type) {
				case 'product_list':
					$return_array = array_column($this->common_model->get_dynamic_data_sales_db('*', array('lookup_group'=>259, 'status' => 'Active'), 'lookup'), 'lookup_value', 'lookup_id');	
				break;

				case 'material_list':
					$return_array = array_column($this->common_model->get_dynamic_data_sales_db('*', array('lookup_group'=>272, 'status' => 'Active'), 'lookup'), 'lookup_value', 'lookup_id');
				break;
				
				case 'handle_by':
					$return_array = $this->common_model->get_procurement_person('user_id, name');
				break;

				case 'users_details':
					$return_array = array_column($this->common_model->get_dynamic_data_sales_db('user_id, name',array(), 'users'),'name','user_id');
				break;

				case 'currency_details':
					$return_array = array_column($this->common_model->get_dynamic_data_sales_db('currency_id, decimal_number',array('status'=>'Active'), 'currency'),'decimal_number','currency_id');
				break;

				case 'sales_person':
					$return_array = array_column($this->common_model->get_sales_person(),'name','user_id');
				break;

				default:
					
					echo "Data Type does not matched!!!";die('debug');
				break;
			}
		} else {
			echo "Data Type is provided!!!";die('debug');
		}
		return $return_array;
	}

	private function create_search_filter_data($filter_data = '') {

		$return_array = array(
			'work_order_no' => array(0=>''), 'client_name' => array(), 'assigned_to' => array(), 'product_family' => array(), 
			'proforma_no' => array(0=>''), 'order_no' => array(0=>''), 'proforma_date' => array(0=>''), 'payment_term' => array(), 
			'payment_status' => array(0=>''), 'vendor_po' => array(0=>''), 'delivery_date' => array(0=>''), 'qc_clearance' => array(),
			'mtc_sent' => array(), 'latest_update' => array(0=>''), 'special_comment' => array(0=>''), 'handled_by' => array(),
			'production_status' => array()
		);
		if(!empty($filter_data)) {
			foreach ($filter_data as $key => $value) {
				$return_array[$key] = $value;
			}
		}
		return $return_array; 
	}

	private function create_where_response_on_search_filter($search_form_data, $tab_name) {

		// echo "<pre>";print_r($search_form_data);echo"</pre><hr>";
		$return_response = array();
		$return_response['where_array'] = array();
		$return_response['where_string_array'] = $this->default_where_for_all_tab($tab_name);
		if(!empty($search_form_data)) {

			foreach ($search_form_data as $form_key => $form_data) {

				if(!empty($form_data['value'])){

					$return_response['where_array'][$form_data['name']][] = $form_data['value'];
				}
			}
		}
		// echo "<pre>";print_r($return_response['where_array']);echo"</pre><hr>";die;
		foreach ($return_response['where_array'] as $column_name => $column_details) {

			$current_count_number = count($return_response['where_string_array']);
			if($column_name == 'work_order_no'){

				$return_response['where_string_array'][$current_count_number] = "production_process_information.work_order_no LIKE '%".$column_details[0]."%'";
			} elseif($column_name == 'client_name') {

				$return_response['where_string_array'][$current_count_number] = "(quotation_mst.client_id = '".$column_details[0]."'";
				unset($column_details[0]);
				foreach ($column_details as $client_id) {
					$return_response['where_string_array'][$current_count_number] .= " OR quotation_mst.client_id = '".$client_id."'";
				}
				$return_response['where_string_array'][$current_count_number] .= ")";
			} elseif($column_name == 'assigned_to') {

				$return_response['where_string_array'][$current_count_number] = "(quotation_mst.assigned_to = '".$column_details[0]."'";
				unset($column_details[0]);
				foreach ($column_details as $sales_person_id) {
					$return_response['where_string_array'][$current_count_number] .= " OR quotation_mst.assigned_to = '".$sales_person_id."'";
				}
				$return_response['where_string_array'][$current_count_number] .= ")";
			} elseif($column_name == 'product_family') {

				$return_response['where_string_array'][$current_count_number] = "(production_process_information.product_family = '".$column_details[0]."'";
				unset($column_details[0]);
				foreach ($column_details as $product_family_name) {
					$return_response['where_string_array'][$current_count_number] .= " OR production_process_information.product_family = '".$product_family_name."'";
				}
				$return_response['where_string_array'][$current_count_number] .= ")";
			} elseif($column_name == 'proforma_no') {

				$return_response['where_string_array'][$current_count_number] = "quotation_mst.proforma_no LIKE '%".$column_details[0]."%'";
			} elseif($column_name == 'order_no') {

				$return_response['where_string_array'][$current_count_number] = "quotation_mst.order_no LIKE '%".$column_details[0]."%'";
			} elseif($column_name == 'proforma_date') {
					
				$date_explode = explode(' - ',$column_details[0]);
				if(count($date_explode) == 1) {
					$return_response['where_string_array'][$current_count_number] = "(quotation_mst.confirmed_on >= '".date('Y-m-d 00:00:00', strtotime($date_explode[0]))."' AND quotation_mst.confirmed_on <= '".date('Y-m-d 23:59:59',strtotime($date_explode[0]))."')";	
				} else {
					$return_response['where_string_array'][$current_count_number] = "(quotation_mst.confirmed_on >= '".date('Y-m-d 00:00:00', strtotime($date_explode[0]))."' AND quotation_mst.confirmed_on <= '".date('Y-m-d 23:59:59',strtotime($date_explode[1]))."')";	
				}
				$this->session->unset_userdata('search_filter_proforma_date');
				$this->session->set_userdata('search_filter_proforma_date', $column_details[0]);
			} else if($column_name == 'payment_term') {

				$return_response['where_string_array'][$current_count_number] = "(quotation_mst.payment_term = '".$column_details[0]."'";
				unset($column_details[0]);
				foreach ($column_details as $payment_terms_name) {
					$return_response['where_string_array'][$current_count_number] .= " OR quotation_mst.payment_term = '".$product_family_name."'";
				}
				$return_response['where_string_array'][$current_count_number] .= ")";
			} elseif($column_name == 'payment_status') {

				$return_response['where_string_array'][$current_count_number] = "production_process_information.payment_status LIKE '%".$column_details[0]."%'";
			} elseif($column_name == 'vendor_po') {

				$return_response['where_string_array'][$current_count_number] = "production_process_information.vendor_po LIKE '%".$column_details[0]."%'";
			} elseif($column_name == 'delivery_date') {
				
				$this->session->unset_userdata('search_filter_delivery_date');
				$this->session->set_userdata('search_filter_delivery_date', $column_details[0]);
				$date_explode = explode(' - ',$column_details[0]);
				if(count($date_explode) == 1) {
					$return_response['where_string_array'][$current_count_number] = "production_process_information.delivery_date = '".date('Y-m-d', strtotime($date_explode[0]))."'";
				} else {
					if($date_explode[0] == $date_explode[1]) {
						$return_response['where_string_array'][$current_count_number] = "production_process_information.delivery_date = '".date('Y-m-d', strtotime($date_explode[0]))."'";
					} else{ 
						$return_response['where_string_array'][$current_count_number] = "(production_process_information.delivery_date >= '".date('Y-m-d', strtotime($date_explode[0]))."' AND production_process_information.delivery_date <= '".date('Y-m-d',strtotime($date_explode[1]))."')";
					}
				}
			} else if($column_name == 'qc_clearance') {

				$return_response['where_string_array'][$current_count_number] = "(production_process_information.qc_clearance = '".$column_details[0]."'";
				unset($column_details[0]);
				foreach ($column_details as $payment_terms_name) {
					$return_response['where_string_array'][$current_count_number] .= " OR production_process_information.qc_clearance = '".$product_family_name."'";
				}
				$return_response['where_string_array'][$current_count_number] .= ")";
			} elseif($column_name == 'latest_update') {

				$return_response['where_string_array'][$current_count_number] = "production_process_information.latest_update LIKE '%".$column_details[0]."%'";
			} elseif($column_name == 'special_comment') {

				$return_response['where_string_array'][$current_count_number] = "production_process_information.special_comment LIKE '%".$column_details[0]."%'";
			} elseif($column_name == 'handled_by') {
				
				$return_response['where_string_array'][$current_count_number] = "(production_process_information.handled_by LIKE '%".$column_details[0]."%'";
				unset($column_details[0]);
				foreach ($column_details as $handle_user_name) {
					$return_response['where_string_array'][$current_count_number] .= " OR production_process_information.handled_by LIKE '%".$handle_user_name."%'";
				}
				$return_response['where_string_array'][$current_count_number] .= ")";
			} elseif($column_name == 'production_status') {
				unset($return_response['where_string_array'][2]);
				$return_response['where_string_array'][2] = "(production_process_information.production_status = '".$column_details[0]."'";
				unset($column_details[0]);
				foreach ($column_details as $production_status) {
					$return_response['where_string_array'][2] .= " OR production_process_information.production_status = '".$production_status."'";
				}
				$return_response['where_string_array'][2] .= ")";
			}
		}
		// echo "<pre>";print_r($return_response);echo"</pre><hr>";exit;
		return $return_response;
	}

	private function default_where_for_all_tab($tab_name){

		$this->session->set_userdata('production_tab_name',$tab_name);
		$return_where = array(
							0=> "quotation_mst.stage = 'proforma'",
							1=> "quotation_mst.status = 'Won'",
							2=> "quotation_mst.work_order_no != 0",
							3=> "(production_process_information.production_status is NULL OR production_process_information.production_status != 'dispatched' AND production_process_information.production_status != 'on_hold' AND production_process_information.production_status != 'ready_for_dispatch')",
							4=> "(production_process_information.status is NULL or production_process_information.status = 'Active')");
		if($tab_name == 'dispatch_order') {
			$return_where[3] = "production_process_information.production_status = 'dispatched'";
		
		}elseif ($tab_name == 'on_hold_order') {
			$return_where[3] = "production_process_information.production_status = 'on_hold'";
		
		}elseif ($tab_name == 'ready_to_dispatch_order') {
			$return_where[3] = "production_process_information.production_status = 'ready_for_dispatch'";
		
		}
		if(in_array($this->session->userdata('role'), array(5, 8))) {
			$return_where[5] = "quotation_mst.assigned_to = ".$this->session->userdata('user_id');
			if($this->session->userdata('role') == 8) {
				$procurement_user_name = $this->common_model->get_dynamic_data_sales_db('name',array('user_id'=>$this->session->userdata('user_id')), 'users', 'row_array');
				$user_explode = explode(' ',$procurement_user_name['name']);
				$return_where[5] = "production_process_information.handled_by LIKE '%".$user_explode['0']."%'";
			}

		}
		// echo "<pre>";print_r($return_where);echo"</pre><hr>";exit;
		return $return_where;
	}
}