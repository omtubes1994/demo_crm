jQuery(document).ready(function() {
    var controller = "<?php echo $this->uri->segment('1',0)?>";
    var method = "<?php echo $this->uri->segment('2',0)?>";
    if(method == 'update_production_listing') {
        All_js.init();
    }else if(method == 'production_listingz') {
        Listing_js.init();
        KTDropzoneDemo.init();
    }else if(method == 'create_work_order_sheet'){
        Create_work_order_sheet.init();
    }
    $('.save_production_details_form').click(function(){
        if(!$(this).hasClass('kt-spinner')) {
            set_reset_spinner($(this));
            ajax_call_function({call_type: 'save_production_details_form',production_details_form: $('form#production_details_form').serializeArray()}, 'save_production_details_form');
        }
    });

    $('li.tab_name_click').click(function(){
        var tab_name = $(this).attr('tab-name');
        if(!$('li.'+tab_name).hasClass('active_list')){
            $('li.active_list').removeClass('active_list');
            $('li.'+tab_name).addClass('active_list');
            ajax_call_function({call_type: 'change_tab',tab_name: tab_name, production_search_form: $('form#production_listing_search_filter_form').serializeArray(), limit: 10, offset: 0},'change_tab');  
        }
    });
    $('.add_production_listing_search_filter_form').click(function(){
        if($(this).attr('action_value') == 'show') {

            $('.production_listing_search_filter').show();
            $('a.add_production_listing_search_filter_form').html('').html('Remove Search Filter');
            $(this).attr('action_value', 'hide');
        } else {

            $('.production_listing_search_filter').hide();
            $('a.add_production_listing_search_filter_form').html('').html('Add Search Filter');
            $(this).attr('action_value', 'show');
        }
    });
    $('div.production_listing_search_filter').on('click', 'button.production_listing_search_filter_form_submit', function(){
        ajax_call_function({call_type: 'search_filter', tab_name: get_tab_name(), production_search_form: $('form#production_listing_search_filter_form').serializeArray(), limit: 10, offset: 0},'search_filter');  
    });
    $('div.production_listing_search_filter').on('click', 'button.production_listing_search_filter_form_reset', function(){
        ajax_call_function({call_type: 'search_filter', tab_name: get_tab_name(), production_search_form: [], limit: $('input#production_list_paggination_limit').val(), offset: $('input#production_list_paggination_offset').val()},'search_filter');  
    });
    $('div#product_list_paggination').on('click', '.production_list_paggination_number', function(){
        ajax_call_function({call_type: 'paggination_filter', tab_name: get_tab_name(), production_search_form: $('form#production_listing_search_filter_form').serializeArray(), limit: $(this).attr('limit'), offset: $(this).attr('offset')},'paggination_filter'); 
    });
    $('div#product_list_paggination').on('change', 'select#set_limit', function(){
        ajax_call_function({call_type: 'paggination_filter', tab_name: get_tab_name(), production_search_form: $('form#production_listing_search_filter_form').serializeArray(), limit: $('select#set_limit').val(), offset: 0},'paggination_filter');
    });
    $('#production_listing').on('click', '.delete_production_list', function(){
        if(!$(this).hasClass('kt-spinner')) {
            set_reset_spinner($(this), true, 'dark', 'center');
            swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this Work Order ID!",
                  icon: "warning",
                  buttons:  ["Cancel", "Delete"],
                  dangerMode: true, 
                })
            .then((willDelete) => {
                if (willDelete) {
                    ajax_call_function({call_type: 'delete_production_list', delete_id: $(this).attr('production_id')},'delete_production_list');
                } else {
                    swal({
                        title: "Work Order ID is not deleted",
                        icon: "info",
                    });
                }
            });
        }
    });
    $('#production_listing').on('click', '.add_query', function(){
       
        ajax_call_function({call_type: 'get_production_query_history', production_query_quotation_id: $(this).attr('quotation_for')},'get_production_query_history',"<?php echo base_url('query/ajax_function'); ?>");

    });

    $('#production_listing').on('click', 'a.mtc_upload', function(){
        ajax_call_function({call_type: 'get_mtc_upload_history', production_id: $(this).attr('production_id')},'get_mtc_upload_history');        
    });
    $('button.mtc_upload_close').click(function(){

        ajax_call_function({call_type: 'unset_mtc_production_id'},'unset_mtc_production_id');        
    });
    // $('button.add_query_submit').click(function(){
        // ajax_call_function({quote_id: $('#add_query_quotation_id').val(), add_query_quotation_type: 'production'},'add_query_submit', 'quotations/addQuery');
    // });
    $('div#production_mtc_pdf_history').on('click', '.delete_mtc_pdf_production', function(){
        swal({
              title: "Are you sure?",
              text: "Once deleted, you will not be able to recover this MTC PDF!",
              icon: "warning",
              buttons:  ["Cancel", "Delete"],
              dangerMode: true, 
            })
        .then((willDelete) => {
            if (willDelete) {
                ajax_call_function({call_type: 'delete_mtc_pdf', pdf_key: $(this).attr('pdf_key')},'delete_mtc_pdf');
            } else {
                swal({
                    title: "MTC Pdf is not deleted",
                    icon: "info",
                });
            }
        });
    });
    $('tbody').on('click', 'button.update_product_status', function(){

        var quotation_dtl_id = $(this).attr('quotation_dtl_id');
        var production_quantity = parseInt($('input[name="production_quantity_'+quotation_dtl_id+'"]').val());
        var max_quantity = parseInt($(this).attr('max_value'));
        if(production_quantity > max_quantity){
            toastr.info("Please Add less Quantity!!!");
            return false;
        }
        var production_status_id_value = $(this).attr('production_status_id');
        var production_status = $('select#'+production_status_id_value+'').val();
        ajax_call_function({call_type: 'update_product_status', quotation_dtl_id: quotation_dtl_id, production_status: production_status, production_quantity: production_quantity},'update_product_status');
    });
    $('div.status_wise_count').on('click', 'a.view_details', function(){

        production_status_graph([], '');
        if($(this).attr('action_value') == 'show') {
            $('div.production_order_wise_graph_div').slideDown();
            $(this).attr('action_value', 'hide');
            $(this).html('').html('<i class="kt-font-dark flaticon2-line-chart"></i> Hide Details');
            ajax_call_function({call_type: 'production_stage_wise_highchart_data', highchart_data_call_type: $(this).attr('order_type')},'production_stage_wise_highchart_data');

        } else {
            $('div.production_order_wise_graph_div').slideUp();
            $('a.view_details').attr('action_value', 'show');
            $('a.view_details').html('').html('<i class="kt-font-dark flaticon2-line-chart"></i> Show Details');
        }
    });
    $('div.production_client_wise_count').on('click', 'a.view_all_details', function () {

        production_client_wise_graph([], '');
        if ($(this).attr('action_value') == 'show') {
            $('div.production_client_wise_graph_div').slideDown();
            $('div.production_status_and_product_family_wise_graph_div').slideDown();
            $(this).attr('action_value', 'hide');
            $(this).html('').html('<i class="kt-font-dark flaticon2-line-chart"></i> Hide Details');
            ajax_call_function({ call_type: 'production_client_wise_highchart_data', highchart_data_call_type: $(this).attr('order_type') }, 'production_client_wise_highchart_data');
            ajax_call_function({ call_type: 'product_family_wise_highchart_data', highchart_data_call_type: $(this).attr('order_type') }, 'product_family_wise_highchart_data');
            ajax_call_function({ call_type: 'production_status_wise_highchart_data', highchart_data_call_type: $(this).attr('order_type') }, 'production_status_wise_highchart_data');

        } else {
            $('div.production_client_wise_graph_div').slideUp();
            $('div.production_status_and_product_family_wise_graph_div').slideUp();
            $('a.view_all_details').attr('action_value', 'show');
            $('a.view_all_details').html('').html('<i class="kt-font-dark flaticon2-line-chart"></i> Show Details');
        }
    });
    $('button.add_production_query').click(function(){

        ajax_call_function({call_type: 'save_production_query', form_data: $('form#production_query_add_form').serializeArray()},'save_production_query',"<?php echo base_url('query/ajax_function'); ?>");

    });
    $('a.save_work_order_sheet').click(function(){

        ajax_call_function(
            {
                call_type: 'submit_work_order_sheet_data', 
                work_order_sheet: $('form#create_work_order_sheet_form').serializeArray(),
                work_order_item_details: $('form#create_work_order_item_details_form').serializeArray(),
                work_order_special_notes: $('form#create_work_order_special_notes_form').serializeArray(),
                work_order_quality_notes: $('form#create_work_order_quality_notes_form').serializeArray(),
                work_order_marking_comment: $('form#create_work_order_marking_comment_form').serializeArray(),
                work_order_packaging_comment: $('form#create_work_order_packaging_comment_form').serializeArray(),
                work_order_revision_comment: $('form#create_work_order_revision_comment_form').serializeArray(),
                work_order_sheet_id: $(this).attr('work_order_sheet_id'),
            },'submit_work_order_sheet_data'); 
    });
    $('a.add_item_details').click(function(){

        ajax_call_function(
            {
                call_type: 'work_order_sheet_add_item',
                number: $(this).attr('next_count_number'),
            },'work_order_sheet_add_item');
    });
    $('a.add_special_notes, a.add_marking_comment, a.add_packaging_comment').click(function(){

        ajax_call_function(
            {
                call_type: 'work_order_sheet_add_notes_or_comments',
                name: $(this).attr('textarea_name'),
                number: $(this).attr('next_count_number'),
            },'work_order_sheet_add_notes_or_comments'); 
    });
    $('button.submit_add_quality_notes_form').click(function(){

        ajax_call_function(
            {
                call_type: 'work_order_sheet_add_quality_notes',
                note: $('input[name="new_quality_note"]').val(),
                number: $('a.add_quality_notes').attr('next_count_number'),
            },'work_order_sheet_add_quality_notes');
    });
    $("#production_listing").on('click', '.create_work_order', function(){
        swal({
          title: "Are you sure?",
          text: "Once made, you won't be able to create again!",
          icon: "warning",
          buttons:  ["Cancel", "Create Work Order"],
          dangerMode: true, 
        })
        .then((willDelete) => {
            if (willDelete) {
                var url = "<?php echo base_url('production/ajax_function'); ?>";
                $.ajax({
                    type: 'POST',
                    data: {call_type: 'create_work_order', quotation_mst_id: $(this).attr('quotation_mst_id'), rfq_mst_id: $(this).attr('rfq_mst_id')},
                    url: url,
                    dataType: 'JSON',
                    success: function(res){
                        swal({
                            title: "Work Order is created",
                            icon: "success",
                        });
                    },
                    beforeSend: function(response){
                        
                    }
                });

            } else {
                swal({
                    title: "Work Order is not created",
                    icon: "info",
                });
            }
        });
    });
    $("tbody#production_listing").on('click', 'a.show_in_details', function(){

        var prod_id = $(this).attr('production_id');
        if ($(this).attr('action_value') == 'open'){

            $('a.show_in_details_'+prod_id).html('').html('<i class="la la-angle-up"></i>');
            $('tr.show_in_details_'+prod_id).show();
            $(this).attr('action_value', 'close');
        }else{
            
            $('a.show_in_details_' + prod_id).html('').html('<i class="la la-angle-down"></i>');
            $('tr.show_in_details_' + prod_id).hide();
            $(this).attr('action_value', 'open');
        }
    });
    $("#production_listing").on('click', '.acknowledge', function(){
        swal({
          title: "Are you sure?",
          text: "Once made, you won't be able to create again!",
          icon: "warning",
          buttons:  ["Cancel", "Acknowledge"],
          dangerMode: true, 
        })
        .then((willDelete) => {
            if (willDelete) {
                var url = "<?php echo base_url('production/ajax_function'); ?>";
                $.ajax({
                    type: 'POST',
                    data: {call_type: 'acknowledge', quotation_mst_id: $(this).attr('quotation_mst_id')},
                    url: url,
                    dataType: 'JSON',
                    success: function(res){
                        swal({
                            title: "Order is Acknowledge",
                            icon: "success",
                        });
                    },
                    beforeSend: function(response){
                        
                    }
                });

            } else {
                swal({
                    title: "Order is not Acknowledge",
                    icon: "info",
                });
            }
        });
    });
    $("#production_listing").on('click', '.sales_acknowledge', function () {
        swal({
            title: "Are you sure?",
            text: "Once made, you won't be able to create again!",
            icon: "warning",
            buttons: ["Cancel", "Acknowledge"],
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    var url = "<?php echo base_url('production/ajax_function'); ?>";
                    $.ajax({
                        type: 'POST',
                        data: { call_type: 'sales_acknowledge', quotation_mst_id: $(this).attr('quotation_mst_id') },
                        url: url,
                        dataType: 'JSON',
                        success: function (res) {
                            swal({
                                title: "Order is Acknowledged",
                                icon: "success",
                            });
                        },
                        beforeSend: function (response) {

                        }
                    });

                } else {
                    swal({
                        title: "Order is not Acknowledge",
                        icon: "info",
                    });
                }
            });
    });
    $("#production_listing").on('click', 'i.show_star_rating', function () {

        var production_id = $(this).attr('production_id');       
        ajax_call_function(
        {
            call_type       :  'get_star_rating',
            production_id   :  production_id,
        }, 
        'get_star_rating');
    });
    $("button.save_star_rating").click(function(){

        if ($('textarea.star_rating').val() == '' || $("input[name='rating_value']").val() == ''){

            swal({
                title: "Please enter all details!",
                icon: "warning",
            });
            return false;
        }
        ajax_call_function(
        {
            call_type       :  'save_star_rating',
            production_id   :  $(this).attr('id'),
            rating_comment  :  $('textarea.star_rating').val(),
            rating_value    :  $("input[name='rating_value']").val()
            }, 'save_star_rating');
    });
    $("div.rating_html").on('click', 'i.rating', function(){

        var rating_value = $(this).attr('rating_value');
        for (index = 1; index <= 5; index++) {

            $('i.rating_value_' + index).removeClass('la-star');
            $('i.rating_value_' + index).removeClass('la-star-o');
            if (index <= rating_value) {

                $('i.rating_value_' + index).addClass('la-star');
            } else {

                $('i.rating_value_' + index).addClass('la-star-o');
            }
        }
        $("input[name='rating_value']").val(rating_value);
    });
    document.addEventListener('keydown', function(event) {
        
        if($('form#production_listing_search_filter_form').serializeArray() != ''){

            if (event.shiftKey) {

                if(event.key === 'I'){
    
                    $("select[name='type']").val('in');
                    ajax_call_function({call_type: 'change_type',tab_name: get_tab_name(), production_search_form: $('form#production_listing_search_filter_form').serializeArray(), limit: 10, offset: 0, type: 'in'},'change_type');
                }else if(event.key === 'Z'){
    
                    $("select[name='type']").val('zen');
                    ajax_call_function({call_type: 'change_type',tab_name: get_tab_name(), production_search_form: $('form#production_listing_search_filter_form').serializeArray(), limit: 10, offset: 0, type: 'zen'},'change_type'); 
                }else if(event.key === 'O'){
    
                    $("select[name='type']").val('om');
                    ajax_call_function({call_type: 'change_type',tab_name: get_tab_name(), production_search_form: $('form#production_listing_search_filter_form').serializeArray(), limit: 10, offset: 0, type: 'om'},'change_type'); 
                }else if(event.key === 'A'){
    
                    $("select[name='type']").val('');
                    ajax_call_function({call_type: 'change_type',tab_name: get_tab_name(), production_search_form: $('form#production_listing_search_filter_form').serializeArray(), limit: 10, offset: 0, type: ''},'change_type'); 
                }    
            }
        }
        
    });

    $('input[name="radio_company_type"]').click(function () {

        ajax_call_function({ call_type: 'change_type', tab_name: get_tab_name(), production_search_form: $('form#production_listing_search_filter_form').serializeArray(), limit: 10, offset: 0, type: $("input[type='radio'][name='radio_company_type']:checked").val() }, 'change_type');
    });

    $('div.production_listing_search_filter').on('input', 'input#client_name', function () {

        var search_text = $('input#client_name').val();
        if (search_text.length <= 3) {
            // toastr.info('Please Enter more than 3 character to update company name!');
            return false;
        }
        $('select[name="client_id"]').html('');
        $('.layer-white').show();
        ajax_call_function({
            call_type: 'get_client_name_production',
            client_name: search_text,
        }, 'get_client_name_production');
    });
    $('div.production_listing_search_filter').on('change', 'select[name="client_id"]', function(){

        ajax_call_function({call_type: 'search_filter', tab_name: get_tab_name(), production_search_form: $('form#production_listing_search_filter_form').serializeArray(), limit: 10, offset: 0},'search_filter');
    });

    $('tbody#production_listing').on('click', 'a.get_all_file_upload_history', function(){

        $('div.upload_history_loader').show();
        ajax_call_function({call_type: 'get_all_file_upload_history', work_order_no: $(this).attr('work_order_no')},'get_all_file_upload_history');
    });
    $('a.get_file_upload_history_tab_wise').click(function(){
        
        $('a.get_file_upload_history_tab_wise').removeClass('kt-widget__item--active');
        $(this).addClass('kt-widget__item--active');
        $('div.upload_history_loader').show();
        ajax_call_function({call_type: 'get_file_upload_history_tab_wise', type_name: $(this).attr('type_name'), file_type: $(this).attr('file_type'), work_order_no: $(this).attr('work_order_no')},'get_file_upload_history_tab_wise');
    });
    $('div.file_upload_history_tab_wise').on('click', 'a.get_file_upload_history_tab', function(){

        $('div.upload_history_loader').show();
        ajax_call_function({call_type: 'get_file_upload_history_tab_wise', type_name: $(this).attr('type_name'), file_type: $(this).attr('file_type'), work_order_no: $(this).attr('work_order_no')},'get_file_upload_history_tab_wise');
    });
    $('div.upload_date_history').on('click', 'input.get_file_upload_date', function(){
        
        $('div.upload_history_loader').show();
        ajax_call_function({call_type: 'get_file_upload_history_tab_wise', type_name: $(this).attr('type_name'), file_type: $(this).attr('file_type'), work_order_no: $(this).attr('work_order_no'), date: $(this).attr('date')},'get_file_upload_history_tab_wise');
    });

    $('tbody#production_listing').on('click', 'a.get_all_file_upload_history_new', function(){

        $('#kt_modal_4_2_new').modal('show');
        $('div.upload_history_loader').show();
        ajax_call_function({call_type: 'get_all_file_upload_history_new', work_order_no: $(this).attr('work_order_no'), task_type: 'all', sub_task_type: 'all'},'get_all_file_upload_history_new');
    });
    $('div.main_tab_html, div.sub_tab_html').on('click', 'div.get_photos_history', function(){

        $('div.upload_history_loader').show();
        ajax_call_function({call_type: 'get_all_file_upload_history_new', work_order_no: $(this).attr('work_order_no'), task_type: $(this).attr('task_type'), sub_task_type: $(this).attr('sub_task_type')},'get_all_file_upload_history_new');
    });
    $('div.sidebar_photos').on('click', '.change_main_photos', function(){
        
        ajax_call_function({call_type: 'set_main_photos', file_name: $(this).attr('file_name'), main_photos_height: $(this).attr('main_photos_height')},'set_main_photos');
        $('.change_main_photos').removeClass('active');
        $(this).addClass('active');
    });
    $('tbody.quotation_quantity_body').on('click', 'button.get_quotation_quantity_status', function(){

        $('form.form_quotation_quantity_status').html('');
        ajax_call_function({call_type: 'get_quotation_quantity_status', quotation_dtl_id: $(this).attr('quotation_dtl_id')},'get_quotation_quantity_status');
    });
    $('button.save_quotation_quantity_status').click(function(){

        var sum_of_all_quantity_in_form = ( parseFloat($('input[name="semi_ready_count"]').val()) + parseFloat($('input[name="ready_to_dispatch_order_count"]').val()) + parseFloat($('input[name="dispatch_order_count"]').val()) + parseFloat($('input[name="on_hold_order_count"]').val()) + parseFloat($('input[name="mtt_count"]').val()) + parseFloat($('input[name="query_count"]').val()) + parseFloat($('input[name="mtt_rfd_count"]').val()) + parseFloat($('input[name="order_cancelled_count"]').val()) );

        if(sum_of_all_quantity_in_form > $(this).attr('total_quantity')){

            alert('quantity is higher');
            return false;
        }
        ajax_call_function({call_type: 'save_quotation_quantity_status', quotation_dtl_id: $(this).attr('quotation_dtl_id'), form_data: $('form.form_quotation_quantity_status').serializeArray()},'save_quotation_quantity_status');
    });

    $('form.form_quotation_quantity_status').on('input', 'input.input_quantity', function(){

        var sum_of_all_quantity_in_form = (parseFloat($('input[name="semi_ready_count"]').val()) + parseFloat($('input[name="ready_to_dispatch_order_count"]').val()) + parseFloat($('input[name="dispatch_order_count"]').val()) + parseFloat($('input[name="on_hold_order_count"]').val()) + parseFloat($('input[name="mtt_count"]').val()) + parseFloat($('input[name="import_count"]').val()) + parseFloat($('input[name="query_count"]').val()) + parseFloat($('input[name="mtt_rfd_count"]').val()) + parseFloat($('input[name="order_cancelled_count"]').val()) );

        var total_quantity = parseFloat($('button.save_quotation_quantity_status').attr('total_quantity'));

        $('input#pending_count').val(0);
        $(this).removeClass('invalid');
        var name = $(this).attr('name');
        $('span.'+name+'_span').removeClass('invalid-feedback').html('');
        var pending_count = (total_quantity - sum_of_all_quantity_in_form);
        if(total_quantity >= sum_of_all_quantity_in_form){

            $('input#pending_count').val(pending_count.toFixed('1'));
        }else{

            $('input#pending_count').val(pending_count.toFixed('1'));
            $('span.'+name+'_span').addClass('invalid-feedback').html('Quantity enter is more than total pending quantity');
            $(this).addClass('invalid');
        }
    });

    $('tbody#production_listing').on('click', 'a.document_upload', function () {
        ajax_call_function({ call_type: 'get_technical_document_upload_history', production_id: $(this).attr('production_id') }, 'get_technical_document_upload_history');
    });

    $("a.get_chat_conversation").click(function(){

		var production_list_id = parseInt($(this).attr('production_list_id'));
		if(production_list_id > 0){

			$('div.div_chat_history').html('');
			$('div.rfq_chat_loader').show();
			ajax_call_function({ call_type: 'get_chat_conversation', id: production_list_id }, 'get_chat_conversation');
		}
	});

    $("a.quality_notes_list").click(function (){

        var button_value = $(this).attr('button_value');
        if (button_value == 'show') {

            $('tbody.quality_notes').slideDown();
            $(this).attr('button_value', 'hide');
            $(this).html('Quality Notes List');
        } else {
            $('tbody.quality_notes').slideUp();
            $(this).attr('button_value', 'show');
            $(this).html('Quality Notes List');
        }
    });

    $("div#technical_document_history_list").on('click', "button.delete_technical_document", function () {

        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this Technical File!",
            icon: "warning",
            buttons: ["Cancel", "Delete"],
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {

                ajax_call_function({ call_type: 'technical_document_delete', production_id: $(this).attr('production_id'), file_name: $(this).attr('file_name') }, 'technical_document_delete');
            } else {
                swal({
                    title: "File is not deleted",
                    icon: "info",
                });
            }
        });
    });

    $('#production_listing').on('click', '.add_osdr_query', function() {

        ajax_call_function({ call_type: 'get_osdr_query_history', osdr_query_quotation_id: $(this).attr('quotation_for'), production_id: $(this).attr('production_id')}, 'get_osdr_query_history', "<?php echo base_url('query/ajax_function'); ?>");

    });
    $('button.add_osdr_query').click(function () {

        ajax_call_function({ call_type: 'save_osdr_query', form_data: $('form#osdr_query_add_form').serializeArray() }, 'save_osdr_query', "<?php echo base_url('query/ajax_function'); ?>");

    });

    $("form#production_query_add_form").on('change', 'select[name="sub_query_type"]', function () {
        var type = $(this).val();
        var assigned_id = $("input[name='query_assigned_id']").val();

        if (type == 'Physical Inspection' || type == 'Material Readiness' || type == 'Payment' || type == 'Dispatch') {

            $("input[name='query_assigned_id']").val(assigned_id);
        } else if (type == 'MTC' || type == 'QC') {

            $("input[name='query_assigned_id']").val(297);
        } else {

            $("input[name='query_assigned_id']").val(assigned_id);
        }
    });

});
function get_tab_name() {

    if ($('li.all_order').hasClass('active_list')) {
        return 'all_order';
    } else if($('li.pending_order').hasClass('active_list')) {
        return 'pending_order';
    } else if($('li.on_hold_order').hasClass('active_list')) {
        return 'on_hold_order';
    } else if($('li.dispatch_order').hasClass('active_list')) {
        return 'dispatch_order';
    } else if($('li.ready_to_dispatch_order').hasClass('active_list')) {
        return 'ready_to_dispatch_order';
    } else if($('li.semi_ready').hasClass('active_list')) {
        return 'semi_ready';
    } else if($('li.order_cancelled').hasClass('active_list')) {
        return 'order_cancelled';
    } else if ($('li.mtt').hasClass('active_list')) {
        return 'mtt';
    } else if($('li.query').hasClass('active_list')) {
        return 'query';
    } else if ($('li.import').hasClass('active_list')) {
        return 'import';
    } else if ($('li.mtt_rfd').hasClass('active_list')) {
        return 'mtt_rfd';
    }
    return '';
}
function set_reset_spinner(obj, set_unset_flag = true, spinner_color = 'light', spinner_align = 'right') {

    if(set_unset_flag) {

        $(obj).addClass('kt-spinner');
        $(obj).addClass('kt-spinner--'+spinner_align);
        $(obj).addClass('kt-spinner--sm');
        $(obj).addClass('kt-spinner--'+spinner_color);   
    } else{

        $(obj).removeClass('kt-spinner');
        $(obj).removeClass('kt-spinner--'+spinner_align);
        $(obj).removeClass('kt-spinner--sm');
        $(obj).removeClass('kt-spinner--'+spinner_color);
    }
}

function ajax_call_function(data, callType, url = "<?php echo base_url('production/ajax_function'); ?>") {

    $.ajax({
        type: 'POST',
        data: data,
        url: url,
        dataType: 'JSON',
        success: function(res){
            if(res.status == 'successful') {
                switch (data.call_type) {

                    case 'save_production_details_form':
                        setTimeout(function(){

                            toastr.info("Details Are Updated!!!"); 
                            setTimeout(function(){
                                location.reload();
                            }, 2000);
                        }, 1000);
                    break;
                    case 'change_tab':
                    case 'search_filter':
                    case 'paggination_filter':
                        $('div.production_listing_search_filter').html('').html(res.search_filter_body);
                        $('tbody#production_listing').html('').html(res.list_body);
                        $('div#product_list_paggination').html('').html(res.paggination_filter_body);
                        if (res.tab_count) {
                            $.each(res.tab_count, function (tabKey, count) {
                                $('li[tab-name="' + tabKey + '"] span').each(function () {
                                    var existingText = $(this).text();
                                    var updatedText = existingText.replace(/\(\d+\)/, "(" + count + ")");
                                    $(this).text(updatedText);
                                });
                            });
                        }
                        Listing_js.init();
                        autosize($('textarea.star_rating'));
                    break;
                    case 'change_type':
                        $('div.production_listing_search_filter').html('').html(res.search_filter_body);
                        $('tbody#production_listing').html('').html(res.list_body);
                        $('div#product_list_paggination').html('').html(res.paggination_filter_body);
                        $('div.status_wise_count').html('').html(res.status_wise_count);
                        $('em.all_total').html('').html(res.production_title_value);
                        if (res.tab_count) {
                            $.each(res.tab_count, function (tabKey, count) {
                                $('li[tab-name="' + tabKey + '"] span').each(function () {
                                    var existingText = $(this).text();
                                    var updatedText = existingText.replace(/\(\d+\)/, "(" + count + ")");
                                    $(this).text(updatedText);
                                });
                            });
                        }
                        $('div.production_order_wise_graph_div').slideUp();
                        $('div.production_client_wise_graph_div').slideUp();
                        $('div.production_status_and_product_family_wise_graph_div').slideUp();
                        Listing_js.init();
                        autosize($('textarea.star_rating'));
                    break;
                    case 'delete_production_list':
                        toastr.success("Details Are Deleted!!!");
                        ajax_call_function({call_type: 'search_filter', tab_name: get_tab_name(), production_search_form: $('form#production_listing_search_filter_form').serializeArray()},'change_tab');  
                    break;
                    case 'get_production_query_history':
                       
                        $('div#kt_chat_modal').modal('show');
                        $('em.wo_no_for_query').html('').html(res.work_order_no);
                        $('div.sales_query_history').html(res.query_history_html);
                        $('textarea[name="query_text"]').val('');
                        autosize($('textarea[name="query_text"]'));
                        $('input[name="query_reference"]').val(res.work_order_no);
                        $('input[name="query_reference_id"]').val(res.quotation_id);
                        $('input[name="query_creator_id"]').val(res.creator_id);
                        $('input[name="query_assigned_id"]').val(res.query_assigned_id);
                        $('div.query_type').html(res.query_type_option_tag);
                        $('.kt-selectpicker').selectpicker();
                        $('.kt-scroll').scrollTop($('.kt-scroll')[0].scrollHeight);
                        // $('div.rfq_chat_loader').hide();
                    break;
                    case 'get_mtc_upload_history':
                        $('div#production_mtc_pdf_history').html('').html(res.mtc_upload_table);
                    break;
                    case 'delete_mtc_pdf':
                        swal({
                            title: "MTC Pdf is Deleted successfully",
                            icon: "success",
                        });
                        ajax_call_function({call_type: 'get_mtc_upload_history'},'get_mtc_upload_history');
                    break;
                    case 'production_stage_wise_highchart_data':
                        production_status_graph(res.production_stage_wise_highchart, res.order_type);
                    break;
                    case 'production_client_wise_highchart_data':

                        production_client_wise_graph(res.production_client_wise_highchart);
                    break;
                    case 'product_family_wise_highchart_data':

                        production_product_family_wise_graph(res.production_product_family_wise_highchart);
                    break;
                    case 'production_status_wise_highchart_data':

                        production_status_wise_graph(res.production_status_wise_highchart);
                    break;
                    case 'save_production_query':
                        $('div#production_query').modal('show');
                        toastr.clear();
                        toastr.success(res.message);
                    break;
                    case 'submit_work_order_sheet_data':
                        if(res.work_order_sheet_id_status == 'success'){
                          toastr.success('Data is saved!!!');
                          location.reload();
                        }else{

                          toastr.error('Data is not saved!!!');  
                        }
                    break;
                    case 'work_order_sheet_add_item':
                        $('tbody.item_details').append(res.table_body_html);
                        autosize($('textarea.work_order_sheet_autosize'));
                        $('a.add_item_details').attr('next_count_number', res.next_increment_number);
                    break;
                    case 'work_order_sheet_add_notes_or_comments':
                        $('tbody.'+data.name).append(res.table_body_html);
                        autosize($('textarea.work_order_sheet_autosize'));
                        $('a.add_'+data.name).attr('next_count_number', res.next_increment_number);
                    break;
                    case 'work_order_sheet_add_quality_notes':
                        $('tbody.quality_notes').append(res.table_body_html);
                        $('div#kt_modal_7').modal('hide');
                        $('a.add_quality_notes').attr('next_count_number', res.next_increment_number);
                    break;
                    case 'get_star_rating':
                        
                        $('div.rating_html').html('').html(res.rating_html);
                        $('button.save_star_rating').attr('id', res.id);
                        $('#rating_comment_modal').modal('show');
                    break;        
                    case 'save_star_rating':
                        
                        swal({
                            title: "Comment Saved!!!",
                            icon: "success",
                        });
                        $('#rating_comment_modal').modal('hide');
                    break;
                    case 'update_product_status':
                        
                        swal({
                            title: "Information Saved!!!",
                            icon: "success",
                        });
                    break;
                    case 'get_client_name_production':

                        $('div.production_listing_search_filter select[name="client_id"]').html(res.client_list_html).selectpicker('refresh');
                        toastr.info('Company Name is Updated!');
                    break;
                    case 'get_all_file_upload_history':

                        $('div.all_file_upload_history').html('').html(res.all_file_upload_history);
                        $('a.get_file_upload_history_tab_wise').attr('work_order_no', data.work_order_no);
                        $('div.file_upload_history_tab_wise').html('');
                        $('div.upload_date_history').html('');
                        $('div.upload_history_loader').hide();
                        $('a.get_file_upload_history_tab_wise').removeClass('kt-widget__item--active');
                        $('#kt_modal_4_2').modal('show');
                    break;
                    case 'get_file_upload_history_tab_wise':
                        
                        $('div.file_upload_history_tab_wise').html('').html(res.file_upload_history_tab_wise);
                        $('div.upload_date_history').html('').html(res.upload_date_history);
                        setTimeout(function(){
                            $('div.upload_history_loader').hide();
                        }, 3000)
                    break;
                    case 'get_all_file_upload_history_new':

                        $('div.sub_tab_html').css('height', res.sub_tab_html_height);
                        $('span.work_order_no').html('').html(res.work_order_no);
                        $('div.main_tab_html').html('').html(res.main_tab_html);
                        $('div.sub_tab_html').html('').html(res.sub_tab_html);
                        $('div.sidebar_photos').html('').html(res.sidebar_photos_html);
                        console.log(res.main_photos_html);
                        $('div.main_photos').html('').html(res.main_photos_html);
                        $('div.upload_history_loader').hide();
                    break;
                    case 'set_main_photos':
                        
                        $('div.main_photos').html('').html(res.main_photos_html);
                    break;
                    case 'get_quotation_quantity_status':
                        
                        $('form.form_quotation_quantity_status').html(res.form_quotation_quantity_status_html);
                        $('button.save_quotation_quantity_status').attr('quotation_dtl_id', data.quotation_dtl_id);
                        $('button.save_quotation_quantity_status').attr('total_quantity', res.quantity);
                        $('span.total_quantity').html('').html(res.quantity);
                    break;
                    case 'save_quotation_quantity_status':
                        
                    setTimeout(() => {
                        
                        location.reload();
                    }, 1000);
                    break;
                    case 'get_technical_document_upload_history':

                        $('div#technical_document_history_list').html('').html(res.production_technical_document_history);

                    break;
                    case 'technical_document_delete':
                        swal({
                            title: "Technical Docuemnt is Deleted successfully",
                            icon: "success",
                        });
                        ajax_call_function({ call_type: 'get_technical_document_upload_history' }, 'get_technical_document_upload_history');
                    break;
                    case 'get_chat_conversation':

                        $('div#kt_chat_modal').modal('show');
                        $('em.header_1').html('').html(res.work_order_no);
                        $('div.div_chat_history').html('').html(res.chat_history);
                        $('.kt-scroll').scrollTop($('.kt-scroll')[0].scrollHeight);
                    break;
                    case 'get_osdr_query_history':

                        $('div#kt_modal_1_2').modal('show');
                        $('em.wo_no_for_query').html('').html(res.work_order_no);
                        $('div.osdr_query_history').html(res.query_history_html);
                        $('textarea[name="query_text"]').val('');
                        autosize($('textarea[name="query_text"]'));
                        $('input[name="query_reference"]').val(res.work_order_no);
                        $('input[name="query_reference_id"]').val(res.quotation_id);
                        $('input[name="query_creator_id"]').val(res.creator_id);
                        $('div.query_type').html(res.query_type_option_tag);
                        $('div#osdr_document_history_list').html('').html(res.production_osdr_document_history);
                        $('.kt-selectpicker').selectpicker();
                        $('.kt-scroll').scrollTop($('.kt-scroll')[0].scrollHeight);
                    break;
                    case 'save_osdr_query':

                        ajax_call_function({ call_type: 'get_osdr_query_history', osdr_query_quotation_id: $('form#osdr_query_add_form').serializeArray()[2]['value'] }, 'get_osdr_query_history', "<?php echo base_url('query/ajax_function'); ?>");
                        toastr.clear();
                        toastr.success(res.message);
                    break;
                }
                $('.layer-white').hide();
            } else {
                    
                switch (data.call_type) {

                    case 'delete_production_list':

                        toastr.error("Details Are Not Deleted!!!");
                    break;
                    case 'save_production_query':
                        toastr.clear();
                        toastr.error(res.message);
                    break;
                }
            }
        },
        beforeSend: function(response){
            switch (data.call_type) {
                case 'save_production_details_form':
                    toastr.warning("Details Are Getting Updated. Wait for 20 seconds."); 
                break;
                case 'delete_production_list':
                    toastr.warning("Details Are Getting Deleted. Wait for 10 seconds."); 
                break;
                case 'change_tab':
                case 'search_filter':
                case 'paggination_filter':
                case 'work_order_sheet_add_item':
                case 'work_order_sheet_add_notes_or_comments':
                case 'work_order_sheet_add_quality_notes':
                case 'work_order_sheet_add_quality_notes':
                case 'submit_work_order_sheet_data':
                case 'production_stage_wise_highchart_data':
                case 'production_client_wise_highchart_data':
                case 'product_family_wise_highchart_data':
                case 'production_status_wise_highchart_data':
                            
                    $('.layer-white').show();
                break;
                case 'add_query_submit':
                    toastr.warning("Query is getting submitted. Wait for 10 seconds."); 
                break;
                case 'save_production_query':
                    toastr.clear();
                    toastr.warning('Saving of the query has begun.');
                break;
                case 'change_type':
                            
                    $('.layer-white ').show();
                break;
          }
        }
    });
};
// Class definition

var All_js = function () {

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }
    
    // Private functions
    var demos = function () {   
        // enable clear button 
        $('#delivery_date, #delivery_date_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: true,
            templates: arrows,
            format: 'yyyy-mm-dd',
        });
        $('.handled_by_select_picker').selectpicker();
        autosize($('#kt_autosize_1'));
        autosize($('#kt_autosize_2'));
        autosize($('#kt_autosize_3'));

    };

    return {
        // public functions
        init: function() {
            demos(); 
        }
    };
}();

var Listing_js = function () {

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }
    
    // Private functions
    var demos = function () {   
        // enable clear button 
        $('#listing_date, #listing_delivery_date').datepicker({
            rtl: KTUtil.isRTL(),
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: true,
            templates: arrows,
            format: 'yyyy-mm-dd',
        });
        $('.listing_select_picker').selectpicker();

    };

    var daterangepickerInit = function() {
    
        if ($('#kt_dashboard_daterangepicker').length == 0) {
            return;
        }

        var picker = $('#kt_dashboard_daterangepicker');
        var start = moment().subtract(1, 'days');
        var end = moment().subtract(1, 'days');

        function cb(start, end, label) {
            var title = '';
            var range = '';
            if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D Y');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D Y');
            } else {                
                range = start.format('MMM D Y') + ' - ' + end.format('MMM D Y');
            }

            if(label == 'first_time') {
                $('#kt_dashboard_daterangepicker_title').html('Select');
                $('#kt_dashboard_daterangepicker_date').html('Date');
                $('input#delivery_date').val('');
            } else {

                $('#kt_dashboard_daterangepicker_title').html(title);
                $('#kt_dashboard_daterangepicker_date').html(range);
                $('input#delivery_date').val(range);
            }
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end, 'first_time');
    };

    var proformadaterangepickerInit = function() {
    
        if ($('#kt_proforma_daterangepicker').length == 0) {
            return;
        }

        var picker = $('#kt_proforma_daterangepicker');
        var start = moment().subtract(1, 'days');
        var end = moment().subtract(1, 'days');

        function cb(start, end, label) {
            var title = '';
            var range = '';
            if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D Y');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D Y');
            } else {                
                range = start.format('MMM D Y') + ' - ' + end.format('MMM D Y');
            }

            if(label == 'first_time') {

                $('#kt_proforma_daterangepicker_title').html('Select');
                $('#kt_proforma_daterangepicker_date').html('Date');
                $('input#proforma_date').val('');
            } else {

                $('#kt_proforma_daterangepicker_title').html(title);
                $('#kt_proforma_daterangepicker_date').html(range);
                $('input#proforma_date').val(range);
            }
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end, 'first_time');
    };

    var priority_value = function () {

        $('#rating_value').ionRangeSlider({
            min: 0,
            max: 5
        });
    }

    return {
        // public functions
        init: function() {
            demos(); 
            daterangepickerInit(); 
            proformadaterangepickerInit(); 
            priority_value();
        }
    };
}();
toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};
"use strict";
// Class definition

var KTDropzoneDemo = function () {
    // Private functions
    var demo2 = function () {
        // set the dropzone container id
        var id = '#kt_dropzone_4';

        // set the preview element template
        var previewNode = $(id + " .dropzone-item");
        previewNode.id = "";
        var previewTemplate = previewNode.parent('.dropzone-items').html();
        previewNode.remove();

        var myDropzone4 = new Dropzone(id, { // Make the whole body a dropzone
            url: "<?php echo base_url('production/ajax_function');?>", // Set the url for your upload script location
            params: {'call_type': 'upload_mtc'},
            parallelUploads: 3,
            previewTemplate: previewTemplate,
            maxFilesize: 1, // Max filesize in MB
            autoQueue: false, // Make sure the files aren't queued until manually added
            previewsContainer: id + " .dropzone-items", // Define the container to display the previews
            clickable: id + " .dropzone-select" // Define the element that should be used as click trigger to select files.
        });

        myDropzone4.on("addedfile", function(file) {
            // Hookup the start button
            file.previewElement.querySelector(id + " .dropzone-start").onclick = function() { myDropzone4.enqueueFile(file); };
            $(document).find( id + ' .dropzone-item').css('display', '');
            $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'inline-block');
        });

        // Update the total progress bar
        myDropzone4.on("totaluploadprogress", function(progress) {
            $(this).find( id + " .progress-bar").css('width', progress + "%");
        });

        myDropzone4.on("sending", function(file) {
            // Show the total progress bar when upload starts
            $( id + " .progress-bar").css('opacity', '1');
            // And disable the start button
            file.previewElement.querySelector(id + " .dropzone-start").setAttribute("disabled", "disabled");
        });

        // Hide the total progress bar when nothing's uploading anymore
        myDropzone4.on("complete", function(progress) {
            var thisProgressBar = id + " .dz-complete";
            setTimeout(function(){
                $( thisProgressBar + " .progress-bar, " + thisProgressBar + " .progress, " + thisProgressBar + " .dropzone-start").css('opacity', '0');
            }, 300)
            toastr.success("MTC Pdf is Uploaded!!!.");
            ajax_call_function({call_type: 'get_mtc_upload_history'},'get_mtc_upload_history');
        });

        // Setup the buttons for all transfers
        document.querySelector( id + " .dropzone-upload").onclick = function() {
            myDropzone4.enqueueFiles(myDropzone4.getFilesWithStatus(Dropzone.ADDED));
        };

        // Setup the button for remove all files
        document.querySelector(id + " .dropzone-remove-all").onclick = function() {
            $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
            myDropzone4.removeAllFiles(true);
        };

        // On all files completed upload
        myDropzone4.on("queuecomplete", function(progress){
            $( id + " .dropzone-upload").css('display', 'none');
        });

        // On all files removed
        myDropzone4.on("removedfile", function(file){
            if(myDropzone4.files.length < 1){
                $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
            }
        });
    }
    var demo3 = function () {
        // set the dropzone container id
        var id = '#kt_dropzone_document_upload';

        // set the preview element template
        var previewNode = $(id + " .dropzone-item");
        previewNode.id = "";
        var previewTemplate = previewNode.parent('.dropzone-items').html();
        previewNode.remove();

        var myDropzone4 = new Dropzone(id, { // Make the whole body a dropzone
            url: "<?php echo base_url('production/ajax_function');?>", // Set the url for your upload script location
            params: { 'call_type': 'technical_document_upload' },
            parallelUploads: 3,
            previewTemplate: previewTemplate,
            maxFilesize: 5, // Max filesize in MB
            autoQueue: false, // Make sure the files aren't queued until manually added
            previewsContainer: id + " .dropzone-items", // Define the container to display the previews
            clickable: id + " .dropzone-select" // Define the element that should be used as click trigger to select files.
        });

        myDropzone4.on("addedfile", function (file) {
            // Hookup the start button
            file.previewElement.querySelector(id + " .dropzone-start").onclick = function () { myDropzone4.enqueueFile(file); };
            $(document).find(id + ' .dropzone-item').css('display', '');
            $(id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'inline-block');
        });

        // Update the total progress bar
        myDropzone4.on("totaluploadprogress", function (progress) {
            $(this).find(id + " .progress-bar").css('width', progress + "%");
        });

        myDropzone4.on("sending", function (file) {
            // Show the total progress bar when upload starts
            $(id + " .progress-bar").css('opacity', '1');
            // And disable the start button
            file.previewElement.querySelector(id + " .dropzone-start").setAttribute("disabled", "disabled");
        });

        // Hide the total progress bar when nothing's uploading anymore
        myDropzone4.on("complete", function (progress) {
            var thisProgressBar = id + " .dz-complete";
            setTimeout(function () {
                $(thisProgressBar + " .progress-bar, " + thisProgressBar + " .progress, " + thisProgressBar + " .dropzone-start").css('opacity', '0');
            }, 300)
            toastr.success("Technical Document is Uploaded!!!.");
            ajax_call_function({ call_type: 'get_technical_document_upload_history' }, 'get_technical_document_upload_history');
        });

        // Setup the buttons for all transfers
        document.querySelector(id + " .dropzone-upload").onclick = function () {
            myDropzone4.enqueueFiles(myDropzone4.getFilesWithStatus(Dropzone.ADDED));
        };

        // Setup the button for remove all files
        document.querySelector(id + " .dropzone-remove-all").onclick = function () {
            $(id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
            myDropzone4.removeAllFiles(true);
        };

        // On all files completed upload
        myDropzone4.on("queuecomplete", function (progress) {
            $(id + " .dropzone-upload").css('display', 'none');
        });

        // On all files removed
        myDropzone4.on("removedfile", function (file) {
            if (myDropzone4.files.length < 1) {
                $(id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
            }
        });
    }
    var demo4 = function () {
        // set the dropzone container id
        var id = '#kt_dropzone_osdr_document';

        // set the preview element template
        var previewNode = $(id + " .dropzone-item");
        previewNode.id = "";
        var previewTemplate = previewNode.parent('.dropzone-items').html();
        previewNode.remove();

        var myDropzone4 = new Dropzone(id, { // Make the whole body a dropzone
            url: "<?php echo base_url('production/ajax_function');?>", // Set the url for your upload script location
            params: { 'call_type': 'osdr_document_upload' },
            parallelUploads: 3,
            previewTemplate: previewTemplate,
            maxFilesize: 5, // Max filesize in MB
            autoQueue: false, // Make sure the files aren't queued until manually added
            previewsContainer: id + " .dropzone-items", // Define the container to display the previews
            clickable: id + " .dropzone-select" // Define the element that should be used as click trigger to select files.
        });

        myDropzone4.on("addedfile", function (file) {
            // Hookup the start button
            file.previewElement.querySelector(id + " .dropzone-start").onclick = function () { myDropzone4.enqueueFile(file); };
            $(document).find(id + ' .dropzone-item').css('display', '');
            $(id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'inline-block');
        });

        // Update the total progress bar
        myDropzone4.on("totaluploadprogress", function (progress) {
            $(this).find(id + " .progress-bar").css('width', progress + "%");
        });

        myDropzone4.on("sending", function (file) {
            // Show the total progress bar when upload starts
            $(id + " .progress-bar").css('opacity', '1');
            // And disable the start button
            file.previewElement.querySelector(id + " .dropzone-start").setAttribute("disabled", "disabled");
        });

        // Hide the total progress bar when nothing's uploading anymore
        myDropzone4.on("complete", function (progress) {
            var thisProgressBar = id + " .dz-complete";
            setTimeout(function () {
                $(thisProgressBar + " .progress-bar, " + thisProgressBar + " .progress, " + thisProgressBar + " .dropzone-start").css('opacity', '0');
            }, 300)
            toastr.success("OSDR Document is Uploaded!!!.");
            ajax_call_function({ call_type: 'get_osdr_document_history', work_order_no: $('input[name="query_reference"]').val() }, 'get_osdr_document_history', "<?php echo base_url('query/ajax_function'); ?>");
        });

        // Setup the buttons for all transfers
        document.querySelector(id + " .dropzone-upload").onclick = function () {
            myDropzone4.enqueueFiles(myDropzone4.getFilesWithStatus(Dropzone.ADDED));
        };

        // Setup the button for remove all files
        document.querySelector(id + " .dropzone-remove-all").onclick = function () {
            $(id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
            myDropzone4.removeAllFiles(true);
        };

        // On all files completed upload
        myDropzone4.on("queuecomplete", function (progress) {
            $(id + " .dropzone-upload").css('display', 'none');
        });

        // On all files removed
        myDropzone4.on("removedfile", function (file) {
            if (myDropzone4.files.length < 1) {
                $(id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
            }
        });
    }

    return {
        // public functions
        init: function() {
            demo2();
            demo3();
            demo4();
        }
    };
}();

function production_status_graph(highchart_data, order_type) {

    Highcharts.chart('production_status_graph', {
        chart: {
            type: 'column'
        },
        title: {
            text: order_type+' Details'
        },
        subtitle: {
            text: ''
        },
        accessibility: {
            announceNewData: {
                enabled: true
            }
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Grand Total in $'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '${point.y}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>${point.y}</b><br/>'
        },

        series: [
            {
                name: order_type,
                colorByPoint: true,
                data: highchart_data
            }
        ]
    });
}
var Create_work_order_sheet = function () {

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }
    
    // Private functions
    var demos = function () {   
        // enable clear button 
        $('.work_order_sheet_datepicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: true,
            templates: arrows,
            format: 'yyyy-mm-dd',
        });
        $('.work_order_sheet_select_picker').selectpicker();
        autosize($('textarea.work_order_sheet_autosize'));
    };

    return {
        // public functions
        init: function() {
            demos(); 
        }
    };
}();

//seema khade
function production_client_wise_graph(highchart_data) {

    Highcharts.chart('production_client_graph', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'All Status Details'
        },
        subtitle: {
            text: ''
        },
        accessibility: {
            announceNewData: {
                enabled: true
            }
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Currency Wise Grand Total'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.currency}{point.y}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.currency}{point.y}</b><br/>'
        },

        series: [
            {
                colorByPoint: true,
                data: highchart_data
            }
        ]
    });
}
function production_product_family_wise_graph(highchart_data) {

    Highcharts.chart('production_product_family_graph', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Product Family Details'
        },
        subtitle: {
            text: ''
        },
        accessibility: {
            announceNewData: {
                enabled: true
            }
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Currency Wise Grand Total'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.currency}{point.y}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.currency}{point.y}</b><br/>'
        },

        series: [
            {
                colorByPoint: true,
                data: highchart_data
            }
        ]
    });
}
function production_status_wise_graph(highchart_data) {

    Highcharts.chart('production_product_status_graph', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Production Status Details'
        },
        subtitle: {
            text: ''
        },
        accessibility: {
            announceNewData: {
                enabled: true
            }
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Currency Wise Grand Total'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.currency}{point.y}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.currency}{point.y}</b><br/>'
        },

        series: [
            {
                colorByPoint: true,
                data: highchart_data
            }
        ]
    });
}
