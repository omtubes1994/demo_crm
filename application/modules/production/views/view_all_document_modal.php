<?php if($call_type == 'main_tab'){ ?>
    <?php foreach($main_tab as $single_main_tab_details){?>
        <div class="kt-widget__item get_photos_history" work_order_no="<?php echo $single_main_tab_details['work_order_no']; ?>" task_type="<?php echo $single_main_tab_details['task_type']; ?>" sub_task_type="<?php echo $single_main_tab_details['sub_task_type']; ?>" style="background-color: <?php echo $single_main_tab_details['background_color']; ?>; border: <?php echo $single_main_tab_details['border']; ?>; border-radius: 34px; height: 40px; margin-right: 4px; position: relative; border-radius: 34px; cursor: pointer;">
            <img alt="" src="<?php echo (!empty($single_main_tab_details['image_src'])) ? $single_main_tab_details['image_src']:base_url('assets/media/users/default.jpg'); ?>" height="34" width="34" style="border-radius: 50%; margin: 2px 1px;">
            <span class="kt-widget__desc task_management_font" style="padding: 0 10px;">
                <?php echo $single_main_tab_details['name']; ?>
            </span>				
        </div>
    <?php } ?>
<?php }elseif($call_type == 'sidebar'){ ?>
    <?php if(!empty($sidebar_photos)) {?>
        <?php foreach ($sidebar_photos as $single_sidebar_photos) { ?>
        <div class="" style="width:<?php echo $single_sidebar_photos['div_width']; ?>%;">
            <?php echo $single_sidebar_photos['image_html']; ?>
        </div>
        <?php } ?>
    <?php } ?>
<?php }elseif($call_type == 'sub_tab'){ ?>
    <?php foreach($sub_tab as $single_sub_tab_details){?>
        <div class="kt-widget__item get_photos_history" work_order_no="<?php echo $single_sub_tab_details['work_order_no']; ?>" task_type="<?php echo $single_sub_tab_details['task_type']; ?>" sub_task_type="<?php echo $single_sub_tab_details['sub_task_type']; ?>" style="background-color: <?php echo $single_sub_tab_details['background_color']; ?>; border: <?php echo $single_sub_tab_details['border']; ?>; border-radius: 34px; height: 40px; margin-right: 4px; position: relative; border-radius: 34px; cursor: pointer;">
            <img alt="" src="<?php echo (!empty($single_sub_tab_details['image_src'])) ? $single_sub_tab_details['image_src']:base_url('assets/media/users/default.jpg'); ?>" height="34" width="34" style="border-radius: 50%; margin: 2px 1px;">
            <span class="kt-widget__desc task_management_font" style="padding: 0 10px;">
                <?php echo $single_sub_tab_details['name']; ?>
            </span>				
        </div>
    <?php } ?>
<?php }elseif($call_type == 'main_photos'){ ?>
    <?php echo $main_photos_html; ?>
<?php } ?>
