<?php if($call_type == 'filter'){ ?>
    <div class="kt-notes__items" style="padding: 0px 15px;">
        <ul class="nav nav-pills nav-fill mont_font_use" role="tablist" style="margin: 0px 0px 20px 0px;">
            <li class="nav-item">
                <a class="nav-link task_management_font tab" href="javascript:void(0);" style="">Select Date</a>
            </li>
        </ul>
        <?php foreach ($date_list as $single_date_list) { ?>
            <div class="kt-notes__item">
                <div class="kt-notes__media">
                    <span class="kt-notes__icon kt-font-boldest kt-hidden-">
                        <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand" style="padding-left: 25px;margin-bottom: 50%;">
                            <input class="get_file_upload_date" type="checkbox" date="<?php echo $single_date_list['date']; ?>" type_name="<?php echo $type_name; ?>" file_type="<?php echo $file_type; ?>" work_order_no="<?php echo $single_date_list['work_order_no']; ?>" <?php echo $single_date_list['checked']; ?>>
                            <span style="border-radius: 50% !important; height: 25px; width: 25px;"></span>
                        </label>
                    </span>
                </div>
                <div class="kt-notes__content">
                    <div class="kt-notes__section">
                        <div class="kt-notes__info">
                            <a href="#" class="kt-notes__title">
                                <?php echo $single_date_list['date']; ?>
                            </a>
                        </div>
                    </div>
                    <span class="kt-notes__body"></span>
                </div>
            </div>
        <?php } ?>
    </div>
<?php }else if($call_type == 'upload_history'){ ?>
    <div class="kt-notes__items" style="padding: 0px 15px;">
        <ul class="nav nav-pills nav-fill mont_font_use" role="tablist" style="margin: 0px 0px 20px 0px;">
            <?php foreach ($file_type_array as $single_file_type_array) { ?>
            <li class="nav-item">
                <a class="nav-link task_management_font get_file_upload_history_tab tab <?php echo $single_file_type_array['class']; ?>" href="javascript:void(0);" type_name="<?php echo $single_file_type_array['type_name']; ?>" file_type="<?php echo $single_file_type_array['file_type']; ?>" work_order_no="<?php echo $single_file_type_array['work_order_no']; ?>" style=""><?php echo $single_file_type_array['name']; ?></a>
            </li>    
            <?php } ?>
        </ul>
        <?php foreach ($upload_history as $upload_date => $upload_data_as_per_date) { ?>
            <div class="kt-notes__item">
                <div class="kt-notes__media">
                    <span class="kt-notes__circle kt-hidden-"></span>
                </div>
                <div class="kt-notes__content">
                    <div class="kt-notes__section">
                        <div class="kt-notes__info">
                            <a href="#" class="kt-notes__title">
                                <?php echo $upload_date; ?>
                            </a>
                        </div>
                        
                    </div>
                    <span class="kt-notes__body"></span>
                </div>
            </div>
            <?php foreach ($upload_data_as_per_date as $single_upload_details) { ?>
                <div class="kt-notes__item kt-notes__item--clean row" style="height: 1000px;">
                    <div class="kt-notes__content" style="width:100%; padding: 0px; margin-right: 5%;">
                        <span class="kt-notes__body" style="padding: 0px; height: 100%;"><?php echo $single_upload_details['file_html']; ?></span>
                    </div>
                </div>
                <div class="kt-notes__item kt-notes__item--clean row">
                    <div class="kt-notes__content"></div>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
<?php } ?>