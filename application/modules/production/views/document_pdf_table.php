<table class="table table-striped" style="margin: 0% 0% 0% 0%;">
	<thead>
		<tr>
			<th scope="col" class="kt-align-center" style="width:10%;">#</th>
			<th scope="col" class="kt-align-center" style="width:50%;">File Name</th>
			<th scope="col" class="kt-align-center" style="width:30%;">Date</th>
			<th scope="col" class="kt-align-center" style="width:20%;">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php if(!empty($document_details['technical_document_file_and_path'])) {?>
                <?php foreach(json_decode($document_details['technical_document_file_and_path'], true) as $single_document_key => $single_document_details) {?>
				<tr>
					<th scope="row" class="kt-align-center"><?php echo $single_document_key+1;?></th>
					<td class="kt-font-bold kt-align-center"><?php echo $single_document_details['file_name']; ?></td>
					<td class="kt-font-bold kt-align-center"><?php echo date('j F, Y', strtotime($single_document_details['file_add_date'])); ?></td>
					<td class="kt-font-bold kt-align-center">
						<div class="row">
							<div class="col-md-12">

								<?php if($single_document_details['file_type'] == 'application/pdf'){ ?>

									<a class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View <?php echo $single_document_details['file_name']; ?>" href="<?php echo base_url('assets/production_technical_document/'.$single_document_details['file_name']);?>" style="color: #212529;" target="_blank">
						    			<i class="la la-eye kt-font-bolder" style="color: #212529;"></i>
							    	</a>									
								<?php } else{ ?>

									<a class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View <?php echo $single_document_details['file_name']; ?>" href="<?php echo base_url('assets/production_technical_document/'.$single_document_details['file_name']);?>" style="color: #212529;" target="_blank" download>
										<i class="fa fa-download kt-font-bolder" style="color: #212529;"></i>
									</a>
								<?php } ?>

								<?php if($this->session->userdata('production_access')['production_technical_document_delete_access']){ ?>

									<button class="btn btn-sm btn-clean btn-icon btn-icon-md delete_technical_document" title="Delete" production_id="<?php echo $document_details['id'];?>" file_name="<?php echo $single_document_details['file_name'];?>" style="color: #212529;">
										<i class="la la-trash kt-font-bolder" style="color: #212529;"></i>
									</button>
								<?php } ?>

							</div>
						</div>
					</td>
				</tr>
                <?php } ?>

			<?php } else if(!empty($document_details['osdr_document'])){ ?>

				<?php foreach(json_decode($document_details['osdr_document'], true) as $single_document_key => $single_document_details) {?>
				<tr>
					<th scope="row" class="kt-align-center"><?php echo $single_document_key+1;?></th>
					<td class="kt-font-bold kt-align-center"><?php echo $single_document_details['file_name']; ?></td>
					<td class="kt-font-bold kt-align-center"><?php echo date('j F, Y', strtotime($single_document_details['file_add_date'])); ?></td>
					<td class="kt-font-bold kt-align-center">
						<div class="row">
							<div class="col-md-12">

								<?php if($single_document_details['file_type'] == 'application/pdf'){ ?>

									<a class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View <?php echo $single_document_details['file_name']; ?>" href="<?php echo base_url('assets/production_osdr_document/'.$single_document_details['file_name']);?>" style="color: #212529;" target="_blank">
										<i class="la la-eye kt-font-bolder" style="color: #212529;"></i>
									</a>
								<?php } else{ ?>

									<a class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View <?php echo $single_document_details['file_name']; ?>" href="<?php echo base_url('assets/production_osdr_document/'.$single_document_details['file_name']);?>" style="color: #212529;" target="_blank" download>
										<i class="fa fa-download kt-font-bolder" style="color: #212529;"></i>
									</a>
								<?php } ?>
							</div>
						</div>
					</td>
				</tr>
                <?php } ?>
			<?php } else{ ?>

				<th class="row"></th>
				<td class="kt-font-bold kt-align-right">Data Not Found !!</td>
				<td></td>
				<td></td>
			<?php }?>
	</tbody>
</table>