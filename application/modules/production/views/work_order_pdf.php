<style>
    tbody#production_listing tr:hover {
        background-color: gainsboro;
    }
    tbody#production_listing tr:hover .first_div {
        border-left: 0.25rem solid #5578eb !important;
    }
    tbody#production_listing td {
        font-style: normal;
        font-size: 15px;
        border: 0.05rem solid black;
    }
    tbody#production_listing td span{
        /*width:200px !important;*/
        display: inline-flex;
        width: 100%;
    }
    tbody#production_listing td span i{
        cursor: pointer;
    }
    tbody#production_listing td span abbr{
        width: 100%;
    }    
    tbody#production_listing td span abbr em{
        display: block;
        color: #898989;
        margin: 3px 0 3px 0;
        font-style: normal;
        font-family: Helvetica Neue,Helvetica,Arial,sans-serif
      }
    tbody#production_listing td span abbr em i{
        font-weight: 500;
        color: #898989;
        margin: 3px 0 3px 0;
        font-style: normal;
        font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
    }
    thead#production_header tr th{
        background: white;
        color: black;
        font-size: 14px;
        font-family: 'latomedium';
        padding: 10px 20px;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        border: 0.05rem solid black;
    }
    .form_name{
        color: #767676 !important;
        font-size: 14px !important;
        font-family: 'latomedium' !important;
        padding: 10px 10px !important; 
        font-weight: bold !important;
    }
    .kt-badge--gainsboro, .kt-font-gainsboro{
        background-color: gainsboro;
    }
    .production_title_name{
        font-weight: 700 !important;
        font-size: 1.5rem;
        display: block;
        color: #898989;
        margin: 3px 0 3px 0;
        font-style: normal;
        font-family: Helvetica Neue,Helvetica,Arial,sans-serif
    }
    .production_title_value{
        font-weight: 500;
        font-size: 1.75rem;
        color: #898989;
        margin: 3px 0 3px 0;
        font-style: normal;
        font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
    }
    .kt-portlet {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-flex: 1;
        -ms-flex-positive: 1;
        flex-grow: 1;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        -webkit-box-shadow: 0px 0px 13px 0px rgb(82 63 105 / 5%);
        box-shadow: 0px 0px 13px 0px rgb(82 63 105 / 5%);
        background-color: #ffffff;
        margin-bottom: 20px;
        border-radius: 4px;
    }
    .kt-portlet .kt-portlet__body {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        padding: 25px;
        border-radius: 4px;
    }
    .row {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        margin-right: -10px;
        margin-left: -10px;
    }
    .form-group {
        margin-bottom: 1rem;
    }
    .col-lg-4 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 33.33333%;
        flex: 0 0 33.33333%;
        max-width: 33.33333%;
    }
    .form-group label {
        font-size: 1rem;
        font-weight: 400;
    }
    .kt-font-dark {
        color: #282a3c !important;
    }
    .kt-font-bolder {
        font-weight: 600 !important;
    }
    .col-lg-6 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 50%;
        flex: 0 0 50%;
        max-width: 50%;
    }
</style>

<table>
    <tr>
        <td style="text-align: left; font-size: 16px;">
            <strong>Work Order Sheet</strong>
        </td>
        <td style="text-align:right; width: 45%; font-size: 16px;">
            <?php if($production_details['type'] == 'OM'){?>

                <strong>OM TUBES</strong>
            <?php }else if($production_details['type'] == 'zen'){?>

                <strong>ZENGINEER MFG</strong>
            <?php }else if($production_details['type'] == 'IN'){?>

                <strong>INSTINOX</strong>
            <?php }?>
        </td>
    </tr>
</table>
<div></div>
<div></div>
<table style="border: 0.2rem solid black;">
    <tr>
        <td style="border: 0.2rem solid black; text-align: left; width: 30%;">
            <strong>Work order:</strong>
            <em style="text-align: center;">
                <?php echo $work_order_sheet_details['work_order_no']; ?>
            </em>
        </td>
        <td style="border: 0.2rem solid black; text-align: left; width: 40%;">
            <strong>Completion Date:</strong>
            <em style="text-align: center;">
                <?php echo $work_order_sheet_details['completion_date']; ?>
            </em>
        </td>
        <td style="border: 0.2rem solid black; text-align: left; width: 30%;">
            <strong>Purchaser:</strong>
            <em style="text-align: center;">
                <?php echo (!empty($users_details[$work_order_sheet_details['procurement_person']]))?$users_details[$work_order_sheet_details['procurement_person']]:''; ?>
            </em>
        </td>
    </tr>
    <tr>
        <td style="border: 0.2rem solid black; text-align: left; width: 30%;">
            <strong>Created on:</strong>   
            <em style="text-align: center;">
                <?php echo $work_order_sheet_details['create_date']; ?>
            </em>
        </td>        
        <td style="border: 0.2rem solid black; width: 40%;">
            <strong>Delivered on:</strong>   
            <em>
                <?php echo (!empty($delivery_details[$work_order_sheet_details['delivery']]))?$delivery_details[$work_order_sheet_details['delivery']]:''; ?>
            </em>
        </td>
        <td style="border: 0.2rem solid black; text-align: left; width: 30%;">
            <strong>Purchaser:</strong>
            <em style="text-align: center;">
                <?php echo (!empty($users_details[$work_order_sheet_details['production_person']]))?$users_details[$work_order_sheet_details['production_person']]:''; ?>
            </em>
        </td>
    </tr>
</table>
<table style="border: 0.2rem solid black;">
    <tr>
        <td style="text-align: center; width: 5%"></td>
        <td style="text-align: center; width: 40%;"></td>
        <td style="text-align: center; width: 35%;"></td>
        <td style="text-align: center; width: 10%;"></td>
        <td style="text-align: center; width: 10%;"></td>
    </tr>
    <tr style="background-color: gainsboro;">
        <td style="border: 0.2rem solid black; text-align: center; width: 5%"><strong>#</strong></td>
        <td style="border: 0.2rem solid black; text-align: center; width: 40%;"><strong>PO Item Description</strong></td>
        <td style="border: 0.2rem solid black; text-align: center; width: 35%;"><strong>WO Item Description</strong></td>
        <td style="border: 0.2rem solid black; text-align: center; width: 10%;"><strong>PO Qty</strong></td>
        <td style="border: 0.2rem solid black; text-align: center; width: 10%;"><strong>WO Qty</strong></td>
    </tr>
    <?php foreach($work_order_sheet_item_details as $item_key => $single_item_details){?>
        <tr style="<?php echo (($item_key+1)%2 != 0) ?'':'background-color: gainsboro;'; ?> ">
            <td style="border: 0.2rem solid black; text-align: center; width: 5%">
                <strong><?php echo $item_key+1; ?></strong>
            </td>
            <td style="border: 0.2rem solid black; text-align: left; width: 40%;">
                <?php echo htmlspecialchars($single_item_details['po_item_description']); ?>
            </td>
            <td style="border: 0.2rem solid black; text-align: left; width: 35%;">
                <?php echo htmlspecialchars($single_item_details['wo_item_description']); ?>
            </td>
            <td style="border: 0.2rem solid black; text-align: center; width: 10%;">
                <?php echo $single_item_details['po_quantity']; ?>
            </td>
            <td style="border: 0.2rem solid black; text-align: center; width: 10%;">
                <?php echo $single_item_details['wo_quantity']; ?>
            </td>
        </tr>
    <?php } ?>
    <tr>
        <td style=""></td>
        <td style=""></td>
        <td style=""></td>
        <td style=""></td>
        <td style=""></td>
    </tr>
</table>
<table style="border: 0.2rem solid black;">
    <tr>
        <td style="text-align: left;"><strong>Drawings and Technical Requirements</strong></td>
    </tr>
    <?php if(!empty($production_details['technical_document_file_and_path'])){?>
    <tr>
        <td>
            <span style="border: 0.2rem solid black;">Please refer the below files uploaded in CRM</span><br>
        </td>
    </tr>
    <?php foreach(json_decode($production_details['technical_document_file_and_path'], true) as $single_document_key => $single_document_details){ ?>
        <tr>
            <td width="75%" style="text-align: left;">
                <span >
                    <?php echo $single_document_key+1;?>: <?php echo $single_document_details['file_name'];?>
                </span>
            </td>
            <td width="25%" style="text-align: right;">
                <span>
                    <?php echo date('M d, Y', strtotime($single_document_details['file_add_date']));?>
                </span>
            </td>
        </tr>
    <?php }
    } ?>
    <tr><td></td></tr>
</table>
<table style="border: 0.2rem solid black;">
    <tr>
        <td style="text-align: left;"><strong>Special Notes</strong></td>
    </tr>
    <?php foreach($work_order_sheet_special_note as $special_note_key => $special_note_details){ ?>
        <tr>
            <td style="text-align: left;">
                <strong><?php echo $special_note_key+1; ?>:</strong>   
                <em style="text-align: center;">
                    <?php echo $special_note_details['notes']; ?>
                </em>
            </td>
        </tr>
    <?php } ?>
    <tr><td></td></tr>
</table>
<table style="border: 0.2rem solid black;">
    <tr>
        <td style="text-align: left;"><strong>Quality Notes</strong></td>
    </tr>
    <?php foreach($work_order_sheet_quality_note as $quality_note_key => $quality_note_details){ ?>
        <tr>
            <td style="text-align: left;">
                <strong><?php echo $quality_note_key+1; ?>:</strong>   
                <em style="text-align: center;">
                    <?php echo $quality_details[$quality_note_details['quality_id']]; ?>
                </em>
            </td>
        </tr>
    <?php } ?>
    <tr><td></td></tr>
</table>
<table style="border: 0.2rem solid black;">
    <tr>
        <td style="text-align: left;"><strong>Marking Comment</strong></td>
    </tr>
    <?php foreach($work_order_sheet_marking_comment as $marking_comment_key => $marking_comment_details){ ?>
        <tr>
            <td style="text-align: left;">
                <strong><?php echo $marking_comment_key+1; ?>:</strong>   
                <em style="text-align: center;">
                    <?php echo $marking_comment_details['comments']; ?>
                </em>
            </td>
        </tr>
    <?php } ?>
    <tr><td></td></tr>
</table>
<table style="border: 0.2rem solid black;">
    <tr>
        <td style="text-align: left;"><strong>Packaging Comment</strong></td>
    </tr>
    <?php foreach($work_order_sheet_packaging_comment as $packaging_comment_key => $packaging_comment_details){ ?>
        <tr>
            <td style="text-align: left;">
                <strong><?php echo $packaging_comment_key+1; ?>:</strong>   
                <em style="text-align: center;">
                    <?php echo $packaging_comment_details['comments']; ?>
                </em>
            </td>
        </tr>
    <?php } ?>
    <tr><td></td></tr>
</table>
<table style="border: 0.2rem solid black;">
   <tr>
        <td style="border: 0.2rem solid black; text-align: center; width: 5%;"><strong>#</strong></td>
        <td style="border: 0.2rem solid black; text-align: center; width: 40%;"><strong>Revision Comment</strong></td>
        <td style="border: 0.2rem solid black; text-align: center; width: 25%;"><strong>Revise By</strong></td>
        <td style="border: 0.2rem solid black; text-align: center; width: 30%;"><strong>Date</strong></td>
   </tr>
   <?php foreach($work_order_sheet_revision_comment as $revision_comment_key => $revision_comment_details){?>
        <tr style="<?php echo (($revision_comment_key+1)%2 == 0) ?'':'background-color: gainsboro'; ?>">
            <td style="border: 0.2rem solid black; text-align: center;">
                <strong><?php echo $revision_comment_key+1; ?></strong>
            </td>
            <td style="border: 0.2rem solid black; text-align: center;">
                <?php echo $revision_comment_details['comments']; ?>
            </td>
            <td style="border: 0.2rem solid black; text-align: center;">
                <?php echo  (!empty($users_details[$revision_comment_details['user_id']]))?$users_details[$revision_comment_details['user_id']]:'Name Not Found!'; ?>
            </td>
            <td style="border: 0.2rem solid black; text-align: center;">
                <?php echo date("M d, Y", strtotime($revision_comment_details['add_time'])); ?>
            </td>
        </tr>
    <?php } ?>
    <tr><td></td><td></td><td></td><td></td></tr>
</table>
<table style="border: 0.2rem solid black;">
    <?php foreach($work_order_sheet_details['made_by_approved_by_combine'] as $single_details){ ?>
        <tr>
            <td style="text-align: left;"><?php echo $single_details['made_by_name']; ?></td>
            <td style="text-align: right;">
                <img src="<?php echo $single_details['approved_by_sign']; ?>" style="height:50px; weight:20px;">&nbsp;
                <?php echo $single_details['approved_by_name']; ?>
            </td>
        </tr>
    <?php } ?>
    <tr><td></td></tr>
</table>
