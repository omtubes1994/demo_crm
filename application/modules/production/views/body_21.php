<?php foreach ($production_list as $production_list_key => $single_production_details) {?>
<tr>
	<td class="first_div" style="width: 30px; padding: 0px 0px 0px 15px;"><?php echo $production_list_key+1;?></td>
	<td style="width: 30px; padding: 0px 0px 0px 15px;"><?php echo $single_production_details['work_order_no'];?></td>
	<td style="width: 300px; padding: 0px 0px 0px 15px;">
		<span>
			<!-- <i style="padding: 1% 10% 1% 1%; display: block; color: #898989; margin: 3px 0 3px 0; font-style: normal; font-family: Helvetica Neue,Helvetica,Arial,sans-serif"><?php echo $production_list_key+1; ?></i> -->
	        <abbr>
	            <em class="kt-font-bolder"> Client Name: 
	              	<i><?php echo $single_production_details['client_details']['client_name'];?></i>
	      		</em>
	            <em class="kt-font-bolder"> Sales Person: 
	            	<i class="">
	            		<?php echo $users_details[$single_production_details['assigned_to']];?>
	        		</i>
	        	</em>
	        	<em class="kt-font-bolder"> Product Family: 
	              	<i class=""><?php echo $single_production_details['product_family'];?></i>
	      		</em> 
	    	</abbr>
		</span>
	</td>
	<td style="width: 230px; padding:0px 0px 0px 15px;">
		<span>    
	        <abbr>
	            <em class="kt-font-bolder"> Proforma #: 
	              	<i><?php echo $single_production_details['proforma_no'];?></i>
	      		</em>
	      		<em class="kt-font-bolder"> PO #: 
	              	<i><?php echo $single_production_details['order_no'];?></i>
	      		</em>
	            <em class="kt-font-bolder"> Proforma Date: 
	            	<i>
	            		<?php 
	            			if(!empty($single_production_details['confirmed_on'])) {
	            				echo date('F j, Y',strtotime($single_production_details['confirmed_on']));
	            			} else {
	            				echo "Proforma Date Not Found";
	            			}
	            		?>
	        		</i>
	        	</em> 
	    	</abbr>
		</span>
	</td>
	<td class="kt-align-center" style="    width: 294px;
    padding: 0px 0px 0px 15px;">
		<span>
	        <abbr>
	            <em class="kt-font-bolder"> 
	            	<span style="width: 47%;padding: 0% 10% 0% 0%;">
	            		Product
	            	</span> 
	            	|  
	            	<span style="width: 47%;padding: 0% 0% 0% 10%";>
	            		Material
	            	</span>
	        	</em>
	        	<?php if(!empty($single_production_details['product_details'])) {?>
	    		<?php foreach ($single_production_details['product_details'] as $single_product_detail) { ?>

		            <em> 
		            	<span style="width: 47%;padding: 0% 10% 0% 0%;">
		            		<?php echo $single_product_detail['product_name']; ?>
	            		</span> 
		            	|  
		            	<span style="width: 47%;padding: 0% 0% 0% 10%;">
		            		<?php echo $single_product_detail['material_name']; ?>
	            		</span> 
		            </em>
	    		<?php }?>
	        	<?php }?>
	    	</abbr>
		</span>
	</td>
	<td style="    width: 200px;
    padding: 0px 0px 0px 15px;">
		<span>
	        <abbr>
	            <em class="kt-font-bolder"> Payment Terms: 
	              	<i><?php echo $payment_term_details[$single_production_details['payment_term']];?></i>
	      		</em>
	      		<em class="kt-font-bolder"> Payment Status: 
	              	<i><?php echo $single_production_details['payment_status'];?></i>
	      		</em>
	    	</abbr>
		</span>
	</td>
	<td style="    width: 250px;
    padding: 0px 0px 0px 15px;">
		<span>
	        <abbr>
	            <em class="kt-font-bolder"> Vendor PO: 
	              	<i><?php echo $single_production_details['vendor_po'];?></i>
	      		</em>
	            <em class="kt-font-bolder"> Delivery Date: 
	            	<i>
	            		<?php 
	            			if(!empty($single_production_details['delivery_date'])) {
	            				echo date('F j, Y',strtotime($single_production_details['delivery_date']));
	            			} else {
	            				echo "Delivery Date Not Found";
	            			}
	            		?>
	        		</i>
	        	</em>
	        	<em class="kt-font-bolder"> Product Family: 
	              	<i class="kt-font-success"><?php echo $single_production_details['product_family'];?></i>
	      		</em> 
	    	</abbr>
		</span>
	</td>
	<td style="    width: 250px;
    padding: 0px 0px 0px 15px;">
		<span>
	        <abbr>
	            <em class="kt-font-bolder"> QC Clearance: 
	              	<i><?php echo $single_production_details['qc_clearance'];?></i>
	      		</em>
	      		<em class="kt-font-bolder"> Latest Update: 
	              	<i><?php echo $single_production_details['latest_update'];?></i>
	      		</em>
	            <em class="kt-font-bolder"> Special Comments: 
	            	<i>
	            		<?php echo $single_production_details['special_comment'];?>
	        		</i>
	        	</em> 
	    	</abbr>
		</span>
	</td>
	<td class="kt-align-center" style="    width: 130px;
    padding: 0px 0px 0px 15px;">
		<span>
	        <abbr>
	    	<?php foreach (explode(',', $single_production_details['handled_by']) as $handled_by_name) { ?>
	        	<em> <?php echo $handled_by_name;?> </em>
	    	<?php }?>
	    	</abbr>
		</span>
	</td>
	<td class="kt-align-right kt-font-bolder kt-font-info" style="width: 100px;
    padding: 0px 10px 0px 0px;"><?php echo $single_production_details['currency_details']['currency_icon'],$single_production_details['grand_total'];?></td>
	<td class="kt-align-center kt-font-bolder kt-font-<?php echo $single_production_details['production_status_name']['color'];?>" style="    width: 200px;
    padding: 0px 0px 0px 0px;"><?php echo $single_production_details['production_status_name']['name'];?></td>
	<td class="kt-align-right" style="    width: 100px;
    padding: 0px 10px 0px 10px;">
		<a href="<?php echo base_url('proforma/pdf/'.$single_production_details['quotation_mst_id']);?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View Proforma" target="_blank">
	        <i class="la la-eye kt-font-bolder" style="color: #ACACAC;"></i>
	    </a>
	    <a href="<?php echo base_url('assets/purchase_orders/'.$single_production_details['purchase_order']);?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View Purchase Order" target="_blank">
	        <i class="fa fa-file-pdf-o kt-font-bolder" style="color: #ACACAC;"></i>
	    </a>
	    <a href="<?php echo base_url('production/update_production_listing/'.$single_production_details['quotation_mst_id']);?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit" target="_blank">
	        <i class="la la-edit kt-font-bolder" style="color: #ACACAC;"></i>
	    </a>
	    <?php if(!empty($single_production_details['id'])) {?>
	    <button class="btn btn-sm btn-clean btn-icon btn-icon-md delete_production_list" title="Delete" production_id="<?php echo $single_production_details['id'];?>">
	    	<i class="la la-trash kt-font-bolder" style="color: #ACACAC;"></i>
	    </button>
	    <?php } ?>
	    <a class="btn btn-sm btn-clean btn-icon btn-icon-md add_query" title="Add Query" data-toggle="modal" data-target="#production_query" quotation_for="<?php echo $single_production_details['quotation_mst_id'];?>"> 
	        <i class="fa fa-question kt-font-bolder" style="color: #ACACAC;"></i>
	    </a>    
	</td>  
</tr>
<?php }?>