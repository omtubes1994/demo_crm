<?php $key = 1; ?>
<?php foreach ($production_list_new as $single_production_details) {
		// echo "<pre>";print_r($single_production_details);echo"</pre><hr>";exit;
	?>
<tr>
	<td class="first_div" style="width:30px; padding:0px 0px 0px 15px; text-align:center;">
		<?php echo $key;$key++;?>
		<hr style="margin: 2px;">
		<span class="kt-font-bolder" style="width: auto;"><?php echo $single_production_details['type']['name'];?></span>
		<hr style="margin: 2px;">&nbsp;
		<?php if($single_production_details['work_order_sheet_approved_by_status'] == 'revised'){ ?>
			<sup>
				<span class="badge badge-sm badge-danger" style="width: auto;">New</span>
			</sup>
		<?php }?>
	</td>
	<td style="width:30px; padding:0px 0px 0px 15px;">
		<?php echo $single_production_details['work_order_no'];?>
		<?php if($this->session->userdata('production_access')['production_list_work_order_sheet_add_access'] && $single_production_details['add_button']){?>
			<a href="<?php echo base_url('production/create_work_order_sheet/'.$single_production_details['work_order_no']);?>" target="_blank">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--dark kt-svg-icon--md">
				    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				        <polygon points="0 0 24 0 24 24 0 24"/>
				        <path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
				        <path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" fill="#000000"/>
				    </g>
				</svg>
			</a>
		<?php } ?>
		<?php if($this->session->userdata('production_access')['production_list_work_order_sheet_edit_access'] && $single_production_details['edit_button']){?>
			<a href="<?php echo base_url('production/create_work_order_sheet/'.$single_production_details['work_order_no']);?>" target="_blank">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--dark kt-svg-icon--md">
				    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				        <rect x="0" y="0" width="24" height="24"/>
				        <path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953) "/>
				        <path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
				    </g>
				</svg>
			</a>
		<?php } ?>
		<?php if($this->session->userdata('production_access')['production_list_work_order_sheet_view_access'] && $single_production_details['pdf_button']){?>
			<a href="<?php echo base_url('production/pdf/'.$single_production_details['work_order_no']);?>" title="Work Order Sheet" target="_blank">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--md">
				    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				        <rect x="0" y="0" width="24" height="24"/>
				        <path d="M3,12 C3,12 5.45454545,6 12,6 C16.9090909,6 21,12 21,12 C21,12 16.9090909,18 12,18 C5.45454545,18 3,12 3,12 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
				        <path d="M12,15 C10.3431458,15 9,13.6568542 9,12 C9,10.3431458 10.3431458,9 12,9 C13.6568542,9 15,10.3431458 15,12 C15,13.6568542 13.6568542,15 12,15 Z" fill="#000000" opacity="0.3"/>
				    </g>
				</svg>
			</a>
		<?php } ?>
		<?php if($this->session->userdata('production_access')['production_list_action_get_task_module_document_access']){?>
			<a href="javascript:void(0)" class="get_all_file_upload_history kt-hidden" work_order_no="<?php echo $single_production_details['work_order_no']; ?>">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--dark kt-svg-icon--md">
					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<rect x="0" y="0" width="24" height="24"/>
						<path d="M3.5,21 L20.5,21 C21.3284271,21 22,20.3284271 22,19.5 L22,8.5 C22,7.67157288 21.3284271,7 20.5,7 L10,7 L7.43933983,4.43933983 C7.15803526,4.15803526 6.77650439,4 6.37867966,4 L3.5,4 C2.67157288,4 2,4.67157288 2,5.5 L2,19.5 C2,20.3284271 2.67157288,21 3.5,21 Z" fill="#000000" opacity="0.3"/>
						<polygon fill="#000000" opacity="0.3" points="4 19 10 11 16 19"/>
						<polygon fill="#000000" points="11 19 15 14 19 19"/>
						<path d="M18,12 C18.8284271,12 19.5,11.3284271 19.5,10.5 C19.5,9.67157288 18.8284271,9 18,9 C17.1715729,9 16.5,9.67157288 16.5,10.5 C16.5,11.3284271 17.1715729,12 18,12 Z" fill="#000000" opacity="0.3"/>
					</g>
				</svg>
			</a>
			<a href="javascript:void(0)" class="get_all_file_upload_history_new" work_order_no="<?php echo $single_production_details['work_order_no']; ?>">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon <?php echo $single_production_details['file_upload_color']; ?> kt-svg-icon--md">
					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<rect x="0" y="0" width="24" height="24"/>
						<path d="M3.5,21 L20.5,21 C21.3284271,21 22,20.3284271 22,19.5 L22,8.5 C22,7.67157288 21.3284271,7 20.5,7 L10,7 L7.43933983,4.43933983 C7.15803526,4.15803526 6.77650439,4 6.37867966,4 L3.5,4 C2.67157288,4 2,4.67157288 2,5.5 L2,19.5 C2,20.3284271 2.67157288,21 3.5,21 Z" fill="#000000" opacity="0.3"/>
						<polygon fill="#000000" opacity="0.3" points="4 19 10 11 16 19"/>
						<polygon fill="#000000" points="11 19 15 14 19 19"/>
						<path d="M18,12 C18.8284271,12 19.5,11.3284271 19.5,10.5 C19.5,9.67157288 18.8284271,9 18,9 C17.1715729,9 16.5,9.67157288 16.5,10.5 C16.5,11.3284271 17.1715729,12 18,12 Z" fill="#000000" opacity="0.3"/>
					</g>
				</svg>
			</a>
		<?php } ?>
	</td>
	<td class="kt-align-center" style="width:150px; padding:0px 10px 0px 10px;">
		
		<em class="kt-align-center kt-font-bolder kt-font-<?php echo $single_production_details['production_status_name']['color'];?>" style="width: 200px;padding: 0px 0px 0px 0px;">
    		<?php echo $single_production_details['production_status_name']['name'];?>
		</em>
		<hr style="margin: 5px 0px !important;">
		<?php if($this->session->userdata('quotation_access')['quotation_list_action_view_proforma_pdf_access']){ ?>
			<a class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View Proforma" href="<?php echo site_url('pdf_management/proforma_pdf/'.$single_production_details['quotation_mst_id']); ?>" style="color: #ACACAC;" target="_blank">
				<i class="la la-eye kt-font-bolder" style="color: #ACACAC;"></i>
			</a>
		<?php } ?>
		<?php if($this->session->userdata('production_access')['production_list_action_edit_access']){ ?>
			<a href="<?php echo base_url('production/update_production_listing/'.$single_production_details['quotation_mst_id']);?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit" target="_blank">
				<i class="la la-edit kt-font-bolder" style="color: #ACACAC;"></i>
			</a>
		<?php } ?>
	    <?php if(!empty($single_production_details['id']) && $this->session->userdata('production_access')['production_list_delete']) {?>
			<button class="btn btn-sm btn-clean btn-icon btn-icon-md delete_production_list" title="Delete" production_id="<?php echo $single_production_details['id'];?>">
				<i class="la la-trash kt-font-bolder" style="color: #ACACAC;"></i>
			</button>
	    <?php } ?>
		<?php if(!empty($single_production_details['purchase_order']) && $this->session->userdata('quotation_access')['quotation_list_action_purchase_order_view_access']){ ?>
			<a class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View Purchase Order" href="<?php echo base_url('assets/purchase_orders/'.$single_production_details['purchase_order']);?>"  style="color: #ACACAC;" target="_blank">

				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="225pt" height="225pt" viewBox="0 0 225 225" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--md">
					<g transform="translate(0,225) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
						<path d="M520 1120 l0 -800 610 0 610 0 0 603 0 602 -198 198 -197 197 -413 0 -412 0 0 -800z m730 495 l0 -185 185 0 185 0 0 -495 0 -495 -490 0 -490 0 0 680 0 680 305 0 305 0 0 -185z m280 -57 c0 -5 -34 -8 -75 -8 l-75 0 0 82 0 82 75 -74 c41 -41 75 -78 75 -82z" fill="#000000"/>
						<path d="M1212 1240 c-43 -10 -88 -54 -103 -98 -16 -49 -6 -158 18 -191 28 -38 80 -61 138 -61 111 0 170 70 162 193 -5 72 -20 104 -68 138 -29 21 -101 31 -147 19z m112 -80 c39 -39 37 -159 -4 -192 -28 -23 -82 -23 -109 0 -40 32 -48 127 -14 180 27 44 88 50 127 12z" fill="#000000"/>
						<path d="M840 1065 l0 -175 35 0 34 0 3 63 3 62 50 5 c79 9 115 43 115 110 0 82 -41 110 -160 110 l-80 0 0 -175z m154 99 c36 -35 7 -84 -50 -84 l-34 0 0 50 0 50 34 0 c19 0 42 -7 50 -16z" fill="#000000"/>
					</g>
				</svg>
			</a>
		<?php } ?>
		<?php if($this->session->userdata('production_access')['production_list_action_query_access']){ ?>
			<a class="btn btn-sm btn-clean btn-icon btn-icon-md add_query" title="Add Query" href="javascript:;" data-toggle="modal" data-target="#production_query" quotation_for="<?php echo $single_production_details['quotation_mst_id'];?>" style="color: #ACACAC;">
				<i class="fa fa-question kt-font-bolder" style="color: #ACACAC;"></i>
			</a>
		<?php } ?>
		<?php if($this->session->userdata('production_access')['production_list_action_mtc_view_access']){ ?>
			<?php if(!empty($single_production_details['mtc_pdf'])){ ?>
				<a href="javascript:void(0)" class="btn btn-sm btn-icon btn-clean btn-icon-md mtc_upload" title="MTC View" data-toggle="modal" data-target="#mtc_upload" production_id="<?php echo $single_production_details['id'];?>">

					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20pt" height="20pt" viewBox="0 0 20 20" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--md">

						<g transform="translate(0,20) scale(0.003906,-0.003906)" fill="#000000" stroke="none">
						<path d="M1070 4821 c-49 -15 -87 -40 -124 -81 -78 -86 -71 97 -71 -1940 0 -1726 1 -1827 18 -1863 24 -52 83 -114 131 -140 38 -21 57 -22 284 -25 l243 -3 34 34 c47 47 49 104 6 148 l-29 29 -214 0 c-177 0 -217 3 -236 16 l-22 15 0 1788 c0 1651 1 1790 17 1802 13 12 202 14 1120 17 l1103 2 0 -308 c0 -349 5 -385 67 -453 20 -22 57 -52 82 -67 l46 -27 278 -3 277 -3 0 -1370 c0 -1356 0 -1369 -20 -1389 -19 -19 -33 -20 -236 -20 l-216 0 -29 -29 c-23 -24 -29 -38 -29 -73 1 -46 10 -64 50 -91 21 -15 54 -17 245 -17 250 0 284 6 355 67 24 21 53 60 66 87 l24 51 0 1481 0 1480 -22 30 c-40 54 -757 818 -790 842 l-31 22 -1176 -1 c-647 0 -1187 -4 -1201 -8z m2710 -641 l181 -195 -192 -3 c-133 -2 -196 1 -205 9 -11 9 -14 55 -14 222 l0 212 24 -25 c13 -14 106 -113 206 -220z" fill="#000000"/>
						<path d="M3337 3124 c-66 -20 -128 -84 -157 -160 -19 -51 -21 -73 -18 -164 4 -81 9 -114 26 -146 28 -55 92 -111 144 -124 60 -17 174 -9 223 15 35 18 40 25 43 59 6 65 0 69 -60 41 -93 -43 -177 -27 -220 44 -20 31 -23 49 -23 136 0 82 4 106 20 134 47 79 119 99 210 57 31 -14 61 -23 66 -20 17 10 10 80 -10 98 -49 44 -155 57 -244 30z" fill="#000000"/>
						<path d="M1452 3118 c-9 -9 -12 -89 -12 -299 0 -311 -2 -299 55 -299 57 0 55 -8 55 251 0 131 3 239 8 239 4 0 43 -106 87 -236 68 -199 84 -237 103 -245 33 -13 65 -11 86 4 10 7 53 114 100 245 46 128 86 236 90 239 3 4 6 -104 6 -239 0 -266 -2 -258 55 -258 57 0 55 -12 55 299 0 210 -3 290 -12 299 -19 19 -142 15 -166 -5 -13 -11 -50 -96 -93 -211 -40 -106 -75 -190 -80 -185 -4 4 -38 91 -74 193 -39 109 -75 192 -87 203 -24 21 -156 25 -176 5z" fill="#000000"/>
						<path d="M2434 3105 c-3 -14 -3 -36 0 -50 6 -24 9 -25 86 -25 l80 0 0 -245 c0 -231 1 -245 19 -255 23 -12 71 -13 88 -2 9 6 13 72 15 252 l3 245 80 5 80 5 3 48 3 47 -225 0 -226 0 -6 -25z" fill="#000000"/>
						<path d="M2451 2020 c-356 -49 -653 -324 -726 -670 -19 -89 -19 -264 -1 -352 83 -391 437 -678 837 -678 464 0 849 384 849 847 0 252 -88 454 -276 632 -141 134 -327 213 -530 224 -49 3 -118 2 -153 -3z m259 -226 c177 -41 339 -171 419 -334 75 -154 89 -317 41 -475 -65 -210 -237 -374 -451 -431 -85 -22 -235 -22 -318 0 -354 95 -558 463 -451 810 33 105 75 173 164 261 89 88 183 143 291 169 80 19 225 19 305 0z" fill="#000000"/>
						<path d="M2545 1656 c-48 -22 -335 -306 -344 -341 -18 -70 33 -135 107 -135 26 0 46 12 100 63 l67 62 5 -255 c3 -140 9 -263 14 -273 19 -39 91 -58 139 -36 49 22 52 37 57 306 l5 253 62 -56 c72 -64 114 -76 161 -48 36 21 52 52 52 102 0 35 -9 46 -157 190 -138 134 -200 183 -230 182 -4 -1 -21 -7 -38 -14z" fill="#000000"/>
						</g>
					</svg>
				</a>
			<?php }?>
		<?php }?>
		<?php if($this->session->userdata('production_access')['production_list_action_technical_document_file_and_path_access']){ ?>
			<a href="javascript:void(0)" class="document_upload" title="Technical Document" data-toggle="modal" data-target="#document_upload" production_id="<?php echo $single_production_details['id'];?>">

				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="32px" viewBox="0 0 32 32" version="1.1" width="32px" class="kt-svg-icon <?php echo $single_production_details['technical_document_color']; ?> kt-svg-icon--md">
					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<rect x="0" y="0" width="24" height="24"/>
						<path d="m21 13v-3l-6-7h-10.99723987c-1.10609388 0-2.00276013.89833832-2.00276013 2.00732994v22.98534016c0 1.1086177.89092539 2.0073299 1.99742191 2.0073299h15.00515619c1.1031457 0 1.9974219-.8982124 1.9974219-1.9907951v-2.0092049h7.9931517c1.6606364 0 3.0068483-1.3422643 3.0068483-2.9987856v-7.0024288c0-1.6561835-1.3359915-2.9987856-3.0068483-2.9987856zm-1 13v2.0066023c0 .5484514-.4476974.9933977-.9999602.9933977h-15.0000796c-.54525127 0-.9999602-.4456813-.9999602-.995457v-23.00908597c0-.54019415.44573523-.99545703.9955775-.99545703h10.0044225v4.99408095c0 1.11936425.8944962 2.00591905 1.9979131 2.00591905h4.0020869v2h-7.9931517c-1.6606364 0-3.0068483 1.3422643-3.0068483 2.9987856v7.0024288c0 1.6561835 1.3359915 2.9987856 3.0068483 2.9987856zm-5-21.5v4.49121523c0 .55713644.4506511 1.00878477.9967388 1.00878477h3.7032124zm-3.0054385 9.5c-1.1015659 0-1.9945615.9001762-1.9945615 1.992017v7.015966c0 1.1001606.9023438 1.992017 1.9945615 1.992017h17.010877c1.1015659 0 1.9945615-.9001762 1.9945615-1.992017v-7.015966c0-1.1001606-.9023438-1.992017-1.9945615-1.992017zm10.5054385 9h-.5l-1.5-3-1.5 3h-.5-.5v-7h1v5l1-2h.5.5l1 2v-5h1v7zm-10.5-7v7h2.9951185c1.1072655 0 2.0048815-.8865548 2.0048815-2.0059191v-2.9881618c0-1.1078385-.8938998-2.0059191-2.0048815-2.0059191zm1 1v5h2.0010434c.5517085 0 .9989566-.4437166.9989566-.9998075v-3.000385c0-.5521784-.4426603-.9998075-.9989566-.9998075zm15 3v2h-2.0001925c-.5560909 0-.9998075-.4476291-.9998075-.9998075v-3.000385c0-.5560909.4476291-.9998075.9998075-.9998075h3.0001925v-1h-2.9951185c-1.1072655 0-2.0048815.8865548-2.0048815 2.0059191v2.9881618c0 1.1078385.8938998 2.0059191 2.0048815 2.0059191h2.9951185v-.75-2.25-1h-3v1z" fill="#000000"/>
					</g>
				</svg>
			</a>
		<?php } ?>
		<?php if($this->session->userdata('production_access')['production_list_action_quotation_edit_access']){ ?>
			<a href="<?php echo base_url('quotations/add/'.$single_production_details['quotation_mst_id']);?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit" target="_blank">
				<i class="la la-edit kt-font-bolder kt-font-info" style="color: #ACACAC;"></i>
			</a>
		<?php } ?>
		<?php if($this->session->userdata('production_access')['production_list_action_show_in_details_access']){ ?>	
	    	<a href="javascript:void(0);" data-ktportlet-tool="toggle" class="btn btn-sm btn-icon btn-clean btn-icon-md show_in_details show_in_details_<?php echo $single_production_details['id'];?>" production_id="<?php echo $single_production_details['id'];?>" action_value="open" aria-describedby="tooltip_lg8p698cuf"><i class="la la-angle-down"></i></a>
		<?php } ?>
		<?php if($this->session->userdata('production_access')['production_list_action_rfq_edit_access']){ ?>
			<a href="<?php echo base_url('procurement/addRFQ/'.$single_production_details['rfq_id']);?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="RFQ Edit" target="_blank">
				<i class="la la-edit kt-font-bolder kt-font-success" style="color: #ACACAC;"></i>
			</a>
		<?php } ?>
		<?php if($this->session->userdata('production_access')['production_list_action_rating_access']){ ?>
			<a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md kt-hidden" title="RFQ Edit">
				<i class="la la-star kt-font-bolder show_star_rating" style="color: #ACACAC;" production_id="<?php echo $single_production_details['id'];?>"></i>
			</a>
			<?php if(!empty($single_production_details['rating_value'])){?>
				<hr style="margin: 5px 0px !important;">
				<a class="kt-font-bolder kt-font-warning" style="padding: 0% 5% !important; font-size: 2.0rem !important;" title="<?php echo $single_production_details['rating_comment']; ?>">
				<?php 
					for ($i = 1; $i <= 5; $i++) { 
						
						$class_name = 'la-star-o';
						if($i <= $single_production_details['rating_value']){
							
							$class_name = 'la-star';
						}
						echo '<i class="la '.$class_name.'"></i>';
					}
				?>
				</a>
			<?php } ?>
		<?php } ?>
		<?php if($this->session->userdata('production_tab_name') == 'dispatch_order' || $this->session->userdata('production_tab_name') == 'on_hold_order' || $this->session->userdata('production_tab_name') == 'order_cancelled'){ ?>
			<?php if(!empty($single_production_details['osdr_document'])){ ?>
				<button class="btn btn-sm btn-danger btn-clean btn-icon btn-icon-md add_osdr_query" title="OSDR Query" data-toggle="modal" data-target="#kt_modal_1_2" quotation_for="<?php echo $single_production_details['quotation_mst_id'];?>" production_id="<?php echo $single_production_details['id'];?>">
					<i class="fa fa-exclamation text-danger"></i>
				</button>
			<?php }else{ ?>

				<button class="btn btn-sm btn-clean btn-icon btn-icon-md add_osdr_query" title="OSDR Query" data-toggle="modal" data-target="#kt_modal_1_2" quotation_for="<?php echo $single_production_details['quotation_mst_id'];?>" production_id="<?php echo $single_production_details['id'];?>">
					<i class="fa fa-exclamation kt-font-bolder" style="color: #ACACAC;"></i>
				</button>
			<?php } ?>
		<?php } ?>
	</td>
	<td style="width:300px; padding:0px 0px 0px 15px;">
		<span>
	        <abbr>
				<?php if($this->session->userdata('production_access')['production_listing_filter_client_name']){?>
					<em class="kt-font-bolder"> Client Name:
						<i><?php echo $single_production_details['client_details']['name']; ?></i>
					</em>
				<?php } ?>
	            <em class="kt-font-bolder"> Sales Person: 
	            	<i class="">
	            		<?php echo $users_details[$single_production_details['assigned_to']];?>
	        		</i>
	        	</em>
	        	<em class="kt-font-bolder"> Product Family: 
					<i class=""><?php echo ucfirst($single_production_details['product_family']);?></i>
	      		</em> 
	    	</abbr>
		</span>
	</td>
	<td style="width:230px; padding:0px 0px 0px 15px;">
		<span>    
	        <abbr>
	            <em class="kt-font-bolder"> Proforma #: 
	              	<i><?php echo $single_production_details['proforma_no'];?></i>
	      		</em>
	      		<em class="kt-font-bolder"> PO #: 
	              	<i><?php echo $single_production_details['order_no'];?></i>
	      		</em>
	            <em class="kt-font-bolder"> Proforma Date: 
	            	<i>
	            		<?php 
	            			if(!empty($single_production_details['confirmed_on'])) {
	            				echo date('F j, Y',strtotime($single_production_details['confirmed_on']));
	            			} else {
	            				echo "Proforma Date Not Found";
	            			}
	            		?>
	        		</i>
	        	</em> 
	    	</abbr>
		</span>
	</td>
	<?php if(!in_array($this->session->userdata('role'), array('8', '18')) || $this->session->userdata('production_access')['production_form_data_view_access']){?>
		<?php if(!in_array($this->session->userdata('user_id'), array('354'))){?>
	<td style="width:150px; padding:0px 0px 0px 15px;">
		<span>    
	        <abbr>
				<?php 
					foreach ($single_production_details['total_status_wise'] as $status_key => $status_details){
						if(!empty($status_details['value']) && $status_details['value'] > 0){
							echo '<em class="kt-font-bolder"> '.$status_details['name'].':  ';
								echo '<i class="kt-font-info">'.$single_production_details['currency_details']['currency_icon'],number_format($status_details['value'],2,'.',',').'</i>';
							echo '</em>';
						}

						if(!empty($status_details['value']) && ($this->session->userdata('production_tab_name') == 'all_order' || $status_key == $this->session->userdata('production_tab_name'))){

							if($status_details['value'] == $single_production_details['grand_total']){

								$hidden_class = 'kt-hidden';
							}
						}
					}
				?>
				<?php if($this->session->userdata('production_access')['production_list_charges_and_grand_total_access']){?>
					<?php if(!empty($single_production_details['gst']) && $single_production_details['gst'] > 0){
						echo '<em class="kt-font-bolder">GST:  ';
							echo '<i class="kt-font-info">'.$single_production_details['currency_details']['currency_icon'],number_format($single_production_details['gst'],2,'.',',').'</i>';
						echo '</em>';
					} ?>
					<?php if(!empty($single_production_details['discount']) && $single_production_details['discount'] > 0){
						echo '<em class="kt-font-bolder">Discount:  ';
							echo '<i class="kt-font-info">'.$single_production_details['currency_details']['currency_icon'],number_format($single_production_details['discount'],2,'.',',').'</i>';
						echo '</em>';
					} ?>
					<?php if(!empty($single_production_details['freight']) && $single_production_details['freight'] > 0){
						echo '<em class="kt-font-bolder">Freight Charge:  ';
							echo '<i class="kt-font-info">'.$single_production_details['currency_details']['currency_icon'],number_format($single_production_details['freight'],2,'.',',').'</i>';
						echo '</em>';
					} ?>
					<?php if(!empty($single_production_details['bank_charges']) && $single_production_details['bank_charges'] > 0){
						echo '<em class="kt-font-bolder">Bank Charge:  ';
							echo '<i class="kt-font-info">'.$single_production_details['currency_details']['currency_icon'],number_format($single_production_details['bank_charges'],2,'.',',').'</i>';
						echo '</em>';
					} ?>
					<?php if(!empty($single_production_details['other_charges']) && $single_production_details['other_charges'] > 0){
						echo '<em class="kt-font-bolder">Other Charge:  ';
							echo '<i class="kt-font-info">'.$single_production_details['currency_details']['currency_icon'],number_format($single_production_details['other_charges'],2,'.',',').'</i>';
						echo '</em>';
					} ?>
					<?php if(!empty($single_production_details['grand_total']) && $single_production_details['grand_total'] > 0){
						echo '<em class="kt-font-bolder '.$hidden_class.'">Grand Total:  ';
							echo '<i class="kt-font-info '.$hidden_class.'">'.$single_production_details['currency_details']['currency_icon'],number_format($single_production_details['grand_total'],2,'.',',').'</i>';
						echo '</em>';
					} ?>
				<?php }?>
	    	</abbr>
		</span>
    </td>
	<td style="width:200px; padding: 0px 0px 0px 15px;">
		<span>
	        <abbr>
	            <em class="kt-font-bolder"> Payment Terms: 
	              	<i><?php echo (!empty($payment_term_details[$single_production_details['payment_term']])) ? $payment_term_details[$single_production_details['payment_term']]: '';?></i>
	      		</em>
	      		<em class="kt-font-bolder"> Payment Status: 
	              	<i><?php echo $single_production_details['payment_status'];?></i>
	      		</em>
	    	</abbr>
		</span>
	</td>
	<?php } } ?>
	<td style="width:320px; padding:0px 0px 0px 15px;">
		<span>
	        <abbr>
				<em class="kt-font-bolder">PO Delivery Date:
	            	<i>
	            		<?php 
	            			if(!empty($single_production_details['delivery_date'])) {
	            				echo date('F j, Y',strtotime($single_production_details['delivery_date']));
	            			} else {
	            				echo "Delivery Date Not Found";
	            			}
	            		?>
	        		</i>
				</em>
				<em class="kt-font-bolder">Actual Delivery Date:
					<i>
						<?php if(!empty($single_production_details['actual_delivery_date'])) {
							echo date('F j, Y',strtotime($single_production_details['actual_delivery_date']));
						} else {
							echo "Date Not Found";
						} ?>
					</i>
	        	</em>
	        	<em class="kt-font-bolder"> Latest Update: 
	              	<i>
	              		<?php
							if(strlen($single_production_details['latest_update_info']) > 100) {

								echo substr($single_production_details['latest_update_info'], 0, 100), '...';

	            			} else {
		
								echo $single_production_details['latest_update_info'];

	            			}
	            		?>
	              	</i>
	      		</em>
	      		<?php if(!in_array($this->session->userdata('role'), array('8', '18'))){?>
	      		<em class="kt-font-bolder"> Special Comments: 
	            	<i>
	            		<?php
	            			if(strlen($single_production_details['special_comment_2']) > 100) {


	            				echo substr($single_production_details['special_comment_2'], 0, 100), '...';

	            			} else {
		
	            				echo $single_production_details['special_comment_2'];	

	            			}
	            		?>
	        		</i>
	        	</em>
	        	<?php } ?>
	    	</abbr>
		</span>
	</td>
	<?php if(!in_array($this->session->userdata('role'), array('8'))){?>
	<td style="width:180px; padding:0px 0px 0px 15px;">
		<span>
	        <abbr>
	            <em class="kt-font-bolder"> QC Clearance: 
	              	<i><?php echo $single_production_details['qc_clearance'];?></i>
	      		</em>
      			<em class="kt-font-bolder"> QC Comments: 
	            	<i>
	            		<?php
	            			if(strlen($single_production_details['special_comment']) > 50) {

	            				echo substr($single_production_details['special_comment'], 0, 50), '...';

	            			} else {
		
	            				echo $single_production_details['special_comment'];	

	            			}
	            		?>
	        		</i>
	        	</em>
	        	<?php if(!in_array($this->session->userdata('role'), array('18'))){?>
	        	<em class="kt-font-bolder"> MTC Sent: 
	              	<i class=""><?php echo $single_production_details['mtc_sent'];?></i>
	      		</em>
	      		<?php } ?>
	    	</abbr>
		</span>
	</td>
	<?php } ?>
	<td class="kt-align-center" style="width:130px; padding:0px 0px 0px 15px;">
		<span>
	        <abbr>
	    	<?php foreach (explode(',', $single_production_details['handled_by']) as $handled_by_name) { ?>
	        	<em> <?php echo $handled_by_name;?> </em>
	    	<?php }?>
	    	</abbr>
		</span>
	</td>
	<td class="kt-align-center" style="width:130px; padding:0px 0px 0px 15px;">
		<span>
	        <abbr>
	    	<?php foreach (explode(',', $single_production_details['handled_by_new']) as $handled_by_name) { ?>
	        	<em> <?php echo $handled_by_name;?> </em>
	    	<?php }?>
	    	</abbr>
		</span>
	</td>
	<?php if(!in_array($this->session->userdata('role'), array('8', '18'))){?>
	<td class="kt-align-center" style="width:294px;padding:0px 0px 0px 15px;">
		<span>
	        <abbr>
	            <em class="kt-font-bolder"> 
	            	<span style="width: 47%;padding: 0% 10% 0% 0%;">
	            		Product
	            	</span> 
	            	|
	            	<span style="width: 47%;padding: 0% 0% 0% 10%";>
	            		Material
	            	</span>
	        	</em>
	        	<?php if(!empty($single_production_details['product_details'])) {?>
	    		<?php foreach ($single_production_details['product_details'] as $single_product_detail) { ?>

		            <em> 
		            	<span style="width: 47%;padding: 0% 10% 0% 0%;">
		            		<?php echo $single_product_detail['product_name']; ?>
	            		</span> 
		            	|
		            	<span style="width: 47%;padding: 0% 0% 0% 10%;">
		            		<?php echo $single_product_detail['material_name']; ?>
	            		</span> 
		            </em>
	    		<?php }?>
	        	<?php }?>
	    	</abbr>
		</span>
	</td>
	<?php } ?>
	<?php if(!in_array($this->session->userdata('role'), array('5'))){?>
	<td class="kt-align-center kt-hidden" style="width:500px;padding:0px 0px 0px 15px;">
		<span>
			<abbr>
				<em class="kt-font-bolder row"> 
					<span style="width: 2%;padding: 0% 0% 0% 0%;">
			    		#
			    	</span> 
			    	|
			    	<span style="width: 45%;padding: 0% 0% 0% 0%;">
			    		Vendor Name
			    	</span> 
			    	|
			    	<span style="width: 15%;padding: 0% 0% 0% 0%";>
			    		PO Value
			    	</span>
			    	|
	            	<span style="width: 20%;padding: 0% 0% 0% 0%";>
	            		Delivery Time
	            	</span>
	            	|
	            	<span style="width: 8%;padding: 0% 0% 0% 0%";>
	            		Action
	            	</span>
			    </em>
			    <hr>
			    <?php 
			    if(!empty($single_production_details['po_details'])){
			    	foreach ($single_production_details['po_details'] as $single_po_key => $single_po_details) {
	    		?>
					    <em class="kt-font-bolder row"> 
							<span style="width: 2%;padding: 0% 0% 0% 0%;">
					    		<?php echo $single_po_key+1;?>
					    	</span> 
					    	|
					    	<span style="width: 45%;padding: 0% 0% 0% 0%;">
					    		<?php echo $single_po_details['vendor_name'];?>
					    	</span> 
					    	|
					    	<span style="width: 15%;padding: 0% 0% 0% 0%";>
					    		<?php echo $single_po_details['gross_total'];?>
					    	</span>
					    	|
			            	<span style="width: 20%;padding: 0% 0% 0% 0%";>
								<?php echo $single_po_details['delivery_time'];?>
			            	</span>
			            	|
			            	<span style="width: 8%;padding: 0% 0% 0% 0%";>
								<a href="<?php echo base_url('procurement/pdf/'.$single_po_details['id']);?>" target="_blank">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--dark kt-svg-icon--md">
									    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									        <rect x="0" y="0" width="24" height="24"/>
									        <path d="M3,12 C3,12 5.45454545,6 12,6 C16.9090909,6 21,12 21,12 C21,12 16.9090909,18 12,18 C5.45454545,18 3,12 3,12 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
									        <path d="M12,15 C10.3431458,15 9,13.6568542 9,12 C9,10.3431458 10.3431458,9 12,9 C13.6568542,9 15,10.3431458 15,12 C15,13.6568542 13.6568542,15 12,15 Z" fill="#000000" opacity="0.3"/>
									    </g>
									</svg>
								</a>
			            	</span>
					    </em>
					    <hr>
				<?php 
					}
				}
				?>
			</abbr>
		</span>
	</td>
	<?php } ?>
</tr>
<tr class="show_in_details_<?php echo $single_production_details['id'];?>" style="display:none;">
	<td colspan="12">
		<div class="kt-portlet kt-portlet--collapse" data-ktportlet="true" id="kt_portlet_tools_1" style="width: 50%;"> 
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<div class="kt-portlet__head-toolbar">
						
					</div>
				</div>
			</div>
			<div class="kt-portlet__body" kt-hidden-height="245" style="display: flex; overflow: hidden; padding-top: 0px; padding-bottom: 0px;">
				<div class="kt-portlet__content row">
					<span class="col-xl-12 kt-font-dark kt-font-xl kt-font-bold kt-align-left">
						<em style="width:50%;">Client Name:</em>
						<em><?php $single_production_details['client_details']['name'];?></em>
					</span>
					<span class="col-xl-12 kt-font-dark kt-font-xl kt-font-bold kt-align-left" style="background-color: antiquewhite;">
						<em style="width:50%;">Sales Person:</em>
						<em><?php echo $users_details[$single_production_details['assigned_to']];?></em>
					</span>
					<span class="col-xl-12 kt-font-dark kt-font-xl kt-font-bold kt-align-left">
						<em style="width:50%;">Product Family:</em>
						<em><?php echo $single_production_details['product_family'];?></em>
					</span>
					<span class="col-xl-12 kt-font-dark kt-font-xl kt-font-bold kt-align-left" style="background-color: antiquewhite;">
						<em style="width:50%;">Proforma NO:</em>
						<em><?php echo $single_production_details['proforma_no'];?></em>
					</span>
					<span class="col-xl-12 kt-font-dark kt-font-xl kt-font-bold kt-align-left">
						<em style="width:50%;">PO #:</em>
						<em><?php echo $single_production_details['order_no'];?></em>
					</span>
					<span class="col-xl-12 kt-font-dark kt-font-xl kt-font-bold kt-align-left" style="background-color: antiquewhite;">
						<em style="width:50%;">Proforma Date:</em>
						<em>
							<?php 
		            			if(!empty($single_production_details['confirmed_on'])) {
		            				echo date('F j, Y',strtotime($single_production_details['confirmed_on']));
		            			} else {
		            				echo "Proforma Date Not Found";
		            			}
		            		?>
						</em>
					</span>
					<span class="col-xl-12 kt-font-dark kt-font-xl kt-font-bold kt-align-left">
						<em style="width:50%;">Status:</em>
						<em class="kt-align-center kt-font-bolder kt-font-<?php echo $single_production_details['production_status_name']['color'];?>" style="width: 200px;padding: 0px 0px 0px 0px;">
				    		<?php echo $single_production_details['production_status_name']['name'];?>
						</em>
					</span>
					<span class="col-xl-12 kt-font-dark kt-font-xl kt-font-bold kt-align-left" style="background-color: antiquewhite;">
						<em style="width:50%;">PO VALUE:</em>
						<em><?php echo $single_production_details['currency_details']['currency_icon'],number_format($single_production_details['grand_total'],2,'.',',');?></em>
					</span>
					<span class="col-xl-12 kt-font-dark kt-font-xl kt-font-bold kt-align-left">
						<em style="width:50%;">Payment Terms:</em>
						<em><?php echo (!empty($payment_term_details[$single_production_details['payment_term']])) ? $payment_term_details[$single_production_details['payment_term']]: '';?></em>
					</span>
					<span class="col-xl-12 kt-font-dark kt-font-xl kt-font-bold kt-align-left" style="background-color: antiquewhite;">
						<em style="width:50%;">Payment Status:</em>
						<em><?php echo $single_production_details['payment_status'];?></em>
					</span>
					<span class="col-xl-12 kt-font-dark kt-font-xl kt-font-bold kt-align-left">
						<em style="width:50%;">Delivery Date:</em>
						<em>
							<?php 
		            			if(!empty($single_production_details['delivery_date'])) {
		            				echo date('F j, Y',strtotime($single_production_details['delivery_date']));
		            			} else {
		            				echo "Delivery Date Not Found";
		            			}
		            		?>
	            		</em>
					</span>
					<span class="col-xl-12 kt-font-dark kt-font-xl kt-font-bold kt-align-left" style="background-color: antiquewhite;">
						<em style="width:50%;">Latest Update:</em>
						<em>
						<?php
							if(strlen($single_production_details['latest_update_info']) > 50) {

								echo substr($single_production_details['latest_update_info'], 0, 50), '...';

	            			} else {
		
								echo $single_production_details['latest_update_info'];

	            			}
	            		?>
						</em>
					</span>
					<span class="col-xl-12 kt-font-dark kt-font-xl kt-font-bold kt-align-left">
						<em style="width:50%;">Special Comments:</em>
						<em>
							<?php
		            			if(strlen($single_production_details['special_comment_2']) > 100) {


		            				echo substr($single_production_details['special_comment_2'], 0, 100), '...';

		            			} else {
			
		            				echo $single_production_details['special_comment_2'];	

		            			}
		            		?>
						</em>
					</span>
					<span class="col-xl-12 kt-font-dark kt-font-xl kt-font-bold kt-align-left" style="background-color: antiquewhite;">
						<em style="width:50%;">QC Clearance:</em>
						<em><?php echo $single_production_details['qc_clearance'];?></em>
					</span>
					<span class="col-xl-12 kt-font-dark kt-font-xl kt-font-bold kt-align-left">
						<em style="width:50%;">QC Comments:</em>
						<em>
							<?php
		            			if(strlen($single_production_details['special_comment']) > 50) {

		            				echo substr($single_production_details['special_comment'], 0, 50), '...';

		            			} else {
			
		            				echo $single_production_details['special_comment'];	

		            			}
		            		?>
						</em>
					</span>
					<span class="col-xl-12 kt-font-dark kt-font-xl kt-font-bold kt-align-left" style="background-color: antiquewhite;">
						<em style="width:50%;">MTC SENT:</em>
						<em><?php echo $single_production_details['mtc_sent'];?></em>
					</span>
					<span class="col-xl-12 kt-font-dark kt-font-xl kt-font-bold kt-align-left">
						<em style="width:50%;">Handled By:</em>
			        	<em> <?php echo $single_production_details['handled_by'];?> </em>
					</span>
					<span class="col-xl-12 kt-font-dark kt-font-xl kt-font-bold kt-align-left" style="background-color: antiquewhite;">
						<em style="width:50%;">Procurement Person:</em>
			        	<em> <?php echo $single_production_details['handled_by_new'];?> </em>
					</span>
					<span class="col-xl-12 kt-font-dark kt-font-xl kt-font-bold kt-align-left">
						<em style="width:50%;">Product:</em>
						<em>
							<?php 
							if(!empty($single_production_details['product_details'])) {
					    		foreach ($single_production_details['product_details'] as $single_product_detail) { 
					            	echo $single_product_detail['product_name'],", ";
					    		}
				        	}
				        	?>
						</em>
					</span>
					<span class="col-xl-12 kt-font-dark kt-font-xl kt-font-bold kt-align-left" style="background-color: antiquewhite;">
						<em style="width:50%;">Material:</em>
						<em>
							<?php 
							if(!empty($single_production_details['product_details'])) {
					    		foreach ($single_production_details['product_details'] as $single_product_detail) { 
					            	echo $single_product_detail['material_name'],", ";
					    		}
				        	}
				        	?>
						</em>
					</span>	
				</div>
			</div>
		</div>
	</td>
</tr>
<?php }?>