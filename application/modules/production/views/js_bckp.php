jQuery(document).ready(function() {
    var controller = "<?php echo $this->uri->segment('1',0)?>";
    var method = "<?php echo $this->uri->segment('2',0)?>";
    if(method == 'update_production_listing') {
        All_js.init();
    }
    if(method == 'production_listingz') {
        Listing_js.init();
        KTDropzoneDemo.init();
    }
    $('.save_production_details_form').click(function(){
        if(!$(this).hasClass('kt-spinner')) {
            set_reset_spinner($(this));
            ajax_call_function({call_type: 'save_production_details_form',production_details_form: $('form#production_details_form').serializeArray()}, 'save_production_details_form');
        }
    });

    $('li.tab_name_click').click(function(){
        var tab_name = $(this).attr('tab-name');
        if(!$('li.'+tab_name).hasClass('active_list')){
            $('li.active_list').removeClass('active_list');
            $('li.'+tab_name).addClass('active_list');
            ajax_call_function({call_type: 'change_tab',tab_name: tab_name, production_search_form: $('form#production_listing_search_filter_form').serializeArray()},'change_tab');  
        }
    });
    $('.add_production_listing_search_filter_form').click(function(){
        if($(this).attr('action_value') == 'show') {

            $('.production_listing_search_filter').show();
            $('a.add_production_listing_search_filter_form').html('').html('Remove Search Filter');
            $(this).attr('action_value', 'hide');
        } else {

            $('.production_listing_search_filter').hide();
            $('a.add_production_listing_search_filter_form').html('').html('Add Search Filter');
            $(this).attr('action_value', 'show');
        }
    });
    $('div.production_listing_search_filter').on('click', 'button.production_listing_search_filter_form_submit', function(){
        ajax_call_function({call_type: 'search_filter', tab_name: get_tab_name(), production_search_form: $('form#production_listing_search_filter_form').serializeArray()},'search_filter');  
    });
    $('div.production_listing_search_filter').on('click', 'button.production_listing_search_filter_form_reset', function(){
        ajax_call_function({call_type: 'search_filter', tab_name: get_tab_name(), production_search_form: []},'search_filter');  
    });
    $('div#product_list_paggination').on('click', '.production_list_paggination_number', function(){
        ajax_call_function({call_type: 'paggination_filter', tab_name: get_tab_name(), production_search_form: $('form#production_listing_search_filter_form').serializeArray(), limit: $(this).attr('limit'), offset: $(this).attr('offset')},'paggination_filter'); 
    });
    $('div#product_list_paggination').on('change', 'select#set_limit', function(){
        ajax_call_function({call_type: 'paggination_filter', tab_name: get_tab_name(), production_search_form: $('form#production_listing_search_filter_form').serializeArray(), limit: $('select#set_limit').val(), offset: 0},'paggination_filter');
    });
    $('#production_listing').on('click', '.delete_production_list', function(){
        if(!$(this).hasClass('kt-spinner')) {
            set_reset_spinner($(this), true, 'dark', 'center');
            swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this Work Order ID!",
                  icon: "warning",
                  buttons:  ["Cancel", "Delete"],
                  dangerMode: true, 
                })
            .then((willDelete) => {
                if (willDelete) {
                    ajax_call_function({call_type: 'delete_production_list', delete_id: $(this).attr('production_id')},'delete_production_list');
                } else {
                    swal({
                        title: "Work Order ID is not deleted",
                        icon: "info",
                    });
                }
            });
        }
    });
    $('#production_listing').on('click', '.add_query', function(){
        $('input#add_query_quotation_id').val($(this).attr('quotation_for'));
        ajax_call_function({call_type: 'get_query_history', quote_id: $(this).attr('quotation_for'), 'query_type': 'production'},'get_query_history');
    });
    $('#production_listing').on('click', 'a.mtc_upload', function(){
        ajax_call_function({call_type: 'get_mtc_upload_history', production_id: $(this).attr('production_id')},'get_mtc_upload_history');        
    });
    $('button.mtc_upload_close').click(function(){

        ajax_call_function({call_type: 'unset_mtc_production_id'},'unset_mtc_production_id');        
    });
    // $('button.add_query_submit').click(function(){
        // ajax_call_function({quote_id: $('#add_query_quotation_id').val(), add_query_quotation_type: 'production'},'add_query_submit', 'quotations/addQuery');
    // });
    $('div#production_mtc_pdf_history').on('click', '.delete_mtc_pdf_production', function(){
        swal({
              title: "Are you sure?",
              text: "Once deleted, you will not be able to recover this MTC PDF!",
              icon: "warning",
              buttons:  ["Cancel", "Delete"],
              dangerMode: true, 
            })
        .then((willDelete) => {
            if (willDelete) {
                ajax_call_function({call_type: 'delete_mtc_pdf', pdf_key: $(this).attr('pdf_key')},'delete_mtc_pdf');
            } else {
                swal({
                    title: "MTC Pdf is not deleted",
                    icon: "info",
                });
            }
        });
    });

    $('tbody').on('click', 'button.update_product_status', function(){

        var quotation_dtl_id = $(this).attr('quotation_dtl_id');
        var production_quantity = parseInt($('input[name="production_quantity_'+quotation_dtl_id+'"]').val());
        var max_quantity = parseInt($(this).attr('max_value'));
        if(production_quantity > max_quantity){
            toastr.info("Please Add less Quantity!!!");
            return false;
        }
        var production_status = $('select[name="production_status_'+quotation_dtl_id+'"]').val();
        ajax_call_function({call_type: 'update_product_status', quotation_dtl_id: quotation_dtl_id, production_status: production_status, production_quantity: production_quantity},'update_product_status');
    });

    $('a.view_details').click(function(){
        production_status_graph([], '');
        if($(this).attr('action_value') == 'show') {
            $('div.production_order_wise_graph_div').slideDown();
            $(this).attr('action_value', 'hide');
            $(this).html('').html('<i class="kt-font-dark flaticon2-line-chart"></i> Hide Details');
            ajax_call_function({call_type: 'production_stage_wise_highchart_data', highchart_data_call_type: $(this).attr('order_type')},'production_stage_wise_highchart_data');

        } else {
            $('div.production_order_wise_graph_div').slideUp();
            $('a.view_details').attr('action_value', 'show');
            $('a.view_details').html('').html('<i class="kt-font-dark flaticon2-line-chart"></i> Show Details');
        }
    });
});
function get_tab_name() {
    if($('li.pending_order').hasClass('active_list')) {
        return 'pending_order';
    } else if($('li.on_hold_order').hasClass('active_list')) {
        return 'on_hold_order';
    } else if($('li.dispatch_order').hasClass('active_list')) {
        return 'dispatch_order';
    } else if($('li.ready_to_dispatch_order').hasClass('active_list')) {
        return 'ready_to_dispatch_order';
    } else if($('li.semi_ready').hasClass('active_list')) {
        return 'semi_ready';
    } else if($('li.order_cancelled').hasClass('active_list')) {
        return 'order_cancelled';
    }
    return '';
}
function set_reset_spinner(obj, set_unset_flag = true, spinner_color = 'light', spinner_align = 'right') {

    if(set_unset_flag) {

        $(obj).addClass('kt-spinner');
        $(obj).addClass('kt-spinner--'+spinner_align);
        $(obj).addClass('kt-spinner--sm');
        $(obj).addClass('kt-spinner--'+spinner_color);   
    } else{

        $(obj).removeClass('kt-spinner');
        $(obj).removeClass('kt-spinner--'+spinner_align);
        $(obj).removeClass('kt-spinner--sm');
        $(obj).removeClass('kt-spinner--'+spinner_color);
    }
}

function ajax_call_function(data, callType, url = "<?php echo base_url('production/ajax_function'); ?>") {

    $.ajax({
        type: 'POST',
        data: data,
        url: url,
        dataType: 'JSON',
        success: function(res){
            if(res.status == 'successful') {
                switch (data.call_type) {

                    case 'save_production_details_form':
                        setTimeout(function(){

                            toastr.info("Details Are Updated!!!"); 
                            setTimeout(function(){
                                location.reload();
                            }, 2000);
                        }, 1000);
                    break;
                    case 'change_tab':
                    case 'search_filter':
                    case 'paggination_filter':
                        
                        $('div.production_listing_search_filter').html('').html(res.search_filter_body);
                        $('tbody#production_listing').html('').html(res.list_body);
                        $('div#product_list_paggination').html('').html(res.paggination_filter_body);
                        Listing_js.init();
                        //$('input#delivery_date').val("<?php echo $this->session->userdata('search_filter_delivery_date');?>");
                        
                    break;
                    case 'delete_production_list':
                        toastr.success("Details Are Deleted!!!");
                        ajax_call_function({call_type: 'search_filter', tab_name: get_tab_name(), production_search_form: $('form#production_listing_search_filter_form').serializeArray()},'change_tab');  
                    break;
                    case 'get_query_history':
                        $('div#production_query_history').html('').html(res.query_table_history);
                    break;
                    case 'get_mtc_upload_history':
                        $('div#production_mtc_pdf_history').html('').html(res.mtc_upload_table);
                    break;
                    case 'delete_mtc_pdf':
                        swal({
                            title: "MTC Pdf is Deleted successfully",
                            icon: "success",
                        });
                        ajax_call_function({call_type: 'get_mtc_upload_history'},'get_mtc_upload_history');
                    break;
                    case 'production_stage_wise_highchart_data':
                        production_status_graph(res.production_stage_wise_highchart, res.order_type);
                    break;
                }
                $('.layer-white').hide();
            } else {
                    
                switch (data.call_type) {

                    case 'delete_production_list':

                        toastr.error("Details Are Not Deleted!!!");
                    break;
                }
            }
        },
        beforeSend: function(response){
            switch (data.call_type) {
                case 'save_production_details_form':
                    toastr.warning("Details Are Getting Updated. Wait for 20 seconds."); 
                break;
                case 'delete_production_list':
                    toastr.warning("Details Are Getting Deleted. Wait for 10 seconds."); 
                break;
                case 'change_tab':
                case 'search_filter':
                case 'paggination_filter':
                    
                    $('.layer-white').show();
                break;
                case 'add_query_submit':
                    toastr.warning("Query is getting submitted. Wait for 10 seconds."); 
                break;
          }
        }
    });
};
// Class definition

var All_js = function () {

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }
    
    // Private functions
    var demos = function () {   
        // enable clear button 
        $('#delivery_date, #delivery_date_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: true,
            templates: arrows,
            format: 'yyyy-mm-dd',
        });
        $('.handled_by_select_picker').selectpicker();
        autosize($('#kt_autosize_1'));
        autosize($('#kt_autosize_2'));

    };

    return {
        // public functions
        init: function() {
            demos(); 
        }
    };
}();

var Listing_js = function () {

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }
    
    // Private functions
    var demos = function () {   
        // enable clear button 
        $('#listing_date, #listing_delivery_date').datepicker({
            rtl: KTUtil.isRTL(),
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: true,
            templates: arrows,
            format: 'yyyy-mm-dd',
        });
        $('.listing_select_picker').selectpicker();

    };

    var daterangepickerInit = function() {
    
        if ($('#kt_dashboard_daterangepicker').length == 0) {
            return;
        }

        var picker = $('#kt_dashboard_daterangepicker');
        var start = moment().subtract(1, 'days');
        var end = moment().subtract(1, 'days');

        function cb(start, end, label) {
            var title = '';
            var range = '';
            if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D Y');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D Y');
            } else {                
                range = start.format('MMM D Y') + ' - ' + end.format('MMM D Y');
            }

            if(label == 'first_time') {
                $('#kt_dashboard_daterangepicker_title').html('Select');
                $('#kt_dashboard_daterangepicker_date').html('Date');
                $('input#delivery_date').val('');
            } else {

                $('#kt_dashboard_daterangepicker_title').html(title);
                $('#kt_dashboard_daterangepicker_date').html(range);
                $('input#delivery_date').val(range);
            }
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end, 'first_time');
    };

    var proformadaterangepickerInit = function() {
    
        if ($('#kt_proforma_daterangepicker').length == 0) {
            return;
        }

        var picker = $('#kt_proforma_daterangepicker');
        var start = moment().subtract(1, 'days');
        var end = moment().subtract(1, 'days');

        function cb(start, end, label) {
            var title = '';
            var range = '';
            if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D Y');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D Y');
            } else {                
                range = start.format('MMM D Y') + ' - ' + end.format('MMM D Y');
            }

            if(label == 'first_time') {

                $('#kt_proforma_daterangepicker_title').html('Select');
                $('#kt_proforma_daterangepicker_date').html('Date');
                $('input#proforma_date').val('');
            } else {

                $('#kt_proforma_daterangepicker_title').html(title);
                $('#kt_proforma_daterangepicker_date').html(range);
                $('input#proforma_date').val(range);
            }
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end, 'first_time');
    };


    return {
        // public functions
        init: function() {
            demos(); 
            daterangepickerInit(); 
            proformadaterangepickerInit(); 
        }
    };
}();
toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};
"use strict";
// Class definition

var KTDropzoneDemo = function () {
    // Private functions
    var demo2 = function () {
        // set the dropzone container id
        var id = '#kt_dropzone_4';

        // set the preview element template
        var previewNode = $(id + " .dropzone-item");
        previewNode.id = "";
        var previewTemplate = previewNode.parent('.dropzone-items').html();
        previewNode.remove();

        var myDropzone4 = new Dropzone(id, { // Make the whole body a dropzone
            url: "<?php echo base_url('production/ajax_function');?>", // Set the url for your upload script location
            params: {'call_type': 'upload_mtc'},
            parallelUploads: 3,
            previewTemplate: previewTemplate,
            maxFilesize: 1, // Max filesize in MB
            autoQueue: false, // Make sure the files aren't queued until manually added
            previewsContainer: id + " .dropzone-items", // Define the container to display the previews
            clickable: id + " .dropzone-select" // Define the element that should be used as click trigger to select files.
        });

        myDropzone4.on("addedfile", function(file) {
            // Hookup the start button
            file.previewElement.querySelector(id + " .dropzone-start").onclick = function() { myDropzone4.enqueueFile(file); };
            $(document).find( id + ' .dropzone-item').css('display', '');
            $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'inline-block');
        });

        // Update the total progress bar
        myDropzone4.on("totaluploadprogress", function(progress) {
            $(this).find( id + " .progress-bar").css('width', progress + "%");
        });

        myDropzone4.on("sending", function(file) {
            // Show the total progress bar when upload starts
            $( id + " .progress-bar").css('opacity', '1');
            // And disable the start button
            file.previewElement.querySelector(id + " .dropzone-start").setAttribute("disabled", "disabled");
        });

        // Hide the total progress bar when nothing's uploading anymore
        myDropzone4.on("complete", function(progress) {
            var thisProgressBar = id + " .dz-complete";
            setTimeout(function(){
                $( thisProgressBar + " .progress-bar, " + thisProgressBar + " .progress, " + thisProgressBar + " .dropzone-start").css('opacity', '0');
            }, 300)
            toastr.success("MTC Pdf is Uploaded!!!.");
            ajax_call_function({call_type: 'get_mtc_upload_history'},'get_mtc_upload_history');
        });

        // Setup the buttons for all transfers
        document.querySelector( id + " .dropzone-upload").onclick = function() {
            myDropzone4.enqueueFiles(myDropzone4.getFilesWithStatus(Dropzone.ADDED));
        };

        // Setup the button for remove all files
        document.querySelector(id + " .dropzone-remove-all").onclick = function() {
            $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
            myDropzone4.removeAllFiles(true);
        };

        // On all files completed upload
        myDropzone4.on("queuecomplete", function(progress){
            $( id + " .dropzone-upload").css('display', 'none');
        });

        // On all files removed
        myDropzone4.on("removedfile", function(file){
            if(myDropzone4.files.length < 1){
                $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
            }
        });
    }
    return {
        // public functions
        init: function() {
            demo2();
        }
    };
}();

function production_status_graph(highchart_data, order_type) {

    Highcharts.chart('production_status_graph', {
        chart: {
            type: 'column'
        },
        title: {
            text: order_type+' Details'
        },
        subtitle: {
            text: ''
        },
        accessibility: {
            announceNewData: {
                enabled: true
            }
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Grand Total in $'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '${point.y}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>${point.y}</b><br/>'
        },

        series: [
            {
                name: order_type,
                colorByPoint: true,
                data: highchart_data
            }
        ]
    });
}