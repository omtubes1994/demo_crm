<div class="form-group row">
    <label class="col-form-label col-lg-3 col-sm-12">Rating</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="kt-checkbox-inline">
            <span class="kt-font-bolder kt-font-warning" style="font-size: 2.0rem !important;">
            <?php 
                for ($i = 1; $i <= 5; $i++) { 
                    
                    $class_name = 'la-star-o';
                    if($i <= $rating_value){
                        
                        $class_name = 'la-star';
                    }
                    echo '<i class="rating rating_value_'.$i.' la '.$class_name.'"  rating_value="'.$i.'"></i>';
                }
            ?>
            </span>
            <input type="text" value="<?php echo $rating_value; ?>" hidden name="rating_value">
        </div>
        <span class="form-text text-muted">Please tick the star</span>
    </div>
</div>
<div class="form-group row">
    <label class="col-form-label col-lg-3 col-sm-12">Comment</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <textarea class="form-control star_rating" placeholder="Enter a Comment" rows="8" style="overflow: hidden; overflow-wrap: break-word; resize: none; height: 77px;"><?php echo $rating_comment; ?></textarea>
        <span class="form-text text-muted">Please enter a comment within text length range 10 and 100.</span>
    </div>
</div>