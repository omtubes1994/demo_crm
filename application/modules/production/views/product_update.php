<style type="text/css">
	.form_name{
	    color: #767676 !important;
	    font-size: 14px !important;
	    font-family: 'latomedium' !important;
	    padding: 10px 10px !important; 
	    font-weight: bold !important;
	}
	.kt-badge--gainsboro{
		background-color: gainsboro !important;
	}
	.kt-badge--Salmon{
		background-color: Salmon !important;
	}
	.kt-badge--darkkhaki{
		background-color: darkkhaki !important;
	}
	.kt-widget27 .kt-widget27__container .nav .nav-item.pending_order > a {
      border-color: #282a3c !important;
      color: #282a3c !important;
   }
	.kt-widget27 .kt-widget27__container .nav .nav-item.pending_order > a.active {
		color: #ffffff !important;
      background: #282a3c !important;
	}

	.kt-widget27 .kt-widget27__container .nav .nav-item.in_production > a {
      border-color: #ffb822 !important;
      color: #ffb822 !important;
   }
	.kt-widget27 .kt-widget27__container .nav .nav-item.in_production > a.active {
      color: #111111 !important;
      background: #ffb822 !important;
	}

	.kt-widget27 .kt-widget27__container .nav .nav-item.ready_for_dispatch > a, .kt-widget27 .kt-widget27__container .nav .nav-item.semi_ready > a{
      border-color: #5867dd !important;
      color: #5867dd !important;
   }
	.kt-widget27 .kt-widget27__container .nav .nav-item.ready_for_dispatch > a.active, .kt-widget27 .kt-widget27__container .nav .nav-item.semi_ready > a.active {
      color: #ffffff !important;
      background: #5867dd !important;
	}

	.kt-widget27 .kt-widget27__container .nav .nav-item.delay_in_delivery > a, .kt-widget27 .kt-widget27__container .nav .nav-item.order_cancelled > a {
      border-color: #ff0000 !important;
      color: #ff0000 !important;
   }
	.kt-widget27 .kt-widget27__container .nav .nav-item.delay_in_delivery > a.active, .kt-widget27 .kt-widget27__container .nav .nav-item.order_cancelled > a.active {
		color: #ffffff !important;
      background: #ff0000 !important;
	}

	.kt-widget27 .kt-widget27__container .nav .nav-item.on_hold > a {
      border-color: gainsboro !important;
      color: gainsboro !important;
   }
	.kt-widget27 .kt-widget27__container .nav .nav-item.on_hold > a.active {
		color: #000 !important;
      background-color: gainsboro !important;
	}

	.kt-widget27 .kt-widget27__container .nav .nav-item.dispatched > a {
      border-color: #0abb87 !important;
      color: #0abb87 !important;
   }
	.kt-widget27 .kt-widget27__container .nav .nav-item.dispatched > a.active {
		color: #ffffff !important;
      	background: #0abb87 !important;
	}
	.quotation_details_title{

		font-size: 1.1rem;
		font-weight: 500;
		color: #000;
	}
	.form-control.invalid {
		border-color: #fd397a;
		padding-right: calc(1.5em + 1.3rem);
		background-image: url(data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' width='12' height='12' fill='none' stroke='%23fd397a' viewBox='0 0 12 12'%3e%3ccircle cx='6' cy='6' r='4.5'/%3e%3cpath stroke-linejoin='round' d='M5.8 3.6h.4L6 6.5z'/%3e%3ccircle cx='6' cy='8.2' r='.6' fill='%23fd397a' stroke='none'/%3e%3c/svg%3e);
		background-repeat: no-repeat;
		background-position: right calc(0.375em + 0.325rem) center;
		background-size: calc(0.75em + 0.65rem) calc(0.75em + 0.65rem);
	}
	.form-control.isvalid {
		border-color: #0abb87;
		padding-right: calc(1.5em + 1.3rem);
		background-image: url(data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8' viewBox='0 0 8 8'%3e%3cpath fill='%230abb87' d='M2.3 6.73L.6 4.53c-.4-1.04.46-1.4 1.1-.8l1.1 1.4 3.4-3.8c.6-.63 1.6-.27 1.2.7l-4 4.6c-.43.5-.8.4-1.1.1z'/%3e%3c/svg%3e);
		background-repeat: no-repeat;
		background-position: right calc(0.375em + 0.325rem) center;
		background-size: calc(0.75em + 0.65rem) calc(0.75em + 0.65rem);
	}
</style>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col">
			<div class="alert alert-light alert-elevate fade show" role="alert">
				<div class="alert-icon"><i class="flaticon-edit kt-font-brand"></i></div>
				<div class="alert-text">
					<h2 class="kt-font-info">Product Details</h2>
				</div>
				<div class="alert-text">
					<?php if($quotation_details['client_type'] == 'omtubes'){?>

						<h2 class="kt-font-info">OM TUBES</h2>
					<?php }else if($quotation_details['client_type'] == 'zengineer'){?>

						<h2 class="kt-font-info">ZENGINEER MFG</h2>
					<?php }else if($quotation_details['client_type'] == 'instinox'){?>

						<h2 class="kt-font-info">INSTINOX</h2>
					<?php }?>
				</div>
				<div style="padding: 0px 16px 0px 0px;">
					<a href="<?php echo base_url('procurement/update_purchase_order_via_production/'.$this->uri->segment('3')); ?>" target="_blank">
						<button type="button" class="btn btn-primary btn-wide">
							Create PO
						</button>
					</a>
				</div>
				<div>
					<button type="button" class="btn btn-primary btn-wide save_production_details_form">Save Details</button>
				</div>
			</div>
		</div>
	</div>
	<!--begin::Portlet-->
	<div class="kt-portlet" style="background: #F5F5F5;">
		<div class="kt-portlet__body">
			<div class="kt-form__section kt-form__section--first">
				<!--begin::Form-->
				<form class="kt-form" id="production_details_form">
					<input type="text" name="quotation_mst_id" value="<?php echo $this->uri->segment('3');?>" hidden>
					<?php
						$form_details_display = 'display:none;';
						if($this->session->userdata('production_access')['production_form_data_view_access']){

							$form_details_display = 'block;';
						}
					?>
					<div class="form-group row">
						<input type="text" value="<?php echo $production_information['id'];?>" name="id" hidden>
						<label class="col-lg-1 col-form-label form_name">Work Order No:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" value="<?php echo $production_information['work_order_no'];?>" readonly>
						</div>
						<label class="col-lg-1 col-form-label form_name">Sales Person:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" value="<?php echo $users_details[$quotation_details['assigned_to']];?>" readonly>
						</div>
						<?php if($this->session->userdata('production_access')['production_listing_filter_client_name']){?>
							<label class="col-lg-1 col-form-label form_name">Client Name:</label>
							<div class="col-lg-2 form-group-sub">
								<input type="text" class="form-control" value="<?php echo $client_details['name']; ?>" readonly>
							</div>
						<?php } ?>
						
						<label class="col-lg-1 col-form-label form_name">Proforma No:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" value="<?php echo $quotation_details['proforma_no'];?>" readonly>
						</div>		
					</div>
					<div class="form-group row">
						<label class="col-lg-1 col-form-label form_name">Proforma Date:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" value="<?php echo date('F, j Y', strtotime($quotation_details['confirmed_on']));?>" readonly>
						</div>
						<label class="col-lg-1 col-form-label form_name">Product Family:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control" name="product_family">
								<option value="">Select Product Family</option>
								<option value="piping" 
								<?php echo ($production_information['product_family'] == 'piping')?'selected':''?>>
									Piping
								</option>
								<option value="instrumentation"
								<?php echo ($production_information['product_family'] == 'instrumentation')?'selected':''?>>
									Instrumentation
								</option>
								<option value="precision"
								<?php echo ($production_information['product_family'] == 'precision')?'selected':''?>>
									Precision
								</option>
								<option value="tubing"
								<?php echo ($production_information['product_family'] == 'tubing')?'selected':''?>>
									Tubing
								</option>
								<option value="industrial"
								<?php echo ($production_information['product_family'] == 'industrial')?'selected':''?>>
									Industrial
								</option>
								<option value="fastener"
								<?php echo ($production_information['product_family'] == 'fastener')?'selected':''?>>
									Fastener
								</option>
								<option value="valve"
								<?php echo ($production_information['product_family'] == 'valve')?'selected':''?>>
									Valve
								</option>
							</select>
						</div>
						<label class="col-lg-1 col-form-label form_name">Item:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control">
								<optgroup label="Product">
									<?php foreach ($product_list as $single_product) { ?>
										<option><?php echo $single_product?></option>
									<?php } ?>
								</optgroup>
								<optgroup label="Material">
									<?php foreach ($material_list as $single_material) { ?>
										<option><?php echo $single_material?></option>
									<?php } ?>
								</optgroup>
							</select>
						</div>
						<label class="col-lg-1 col-form-label form_name">Handled By:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control handled_by_select_picker" name="handled_by" multiple>
								<?php $explode_handle_by = explode(',', $production_information['handled_by']); ?>
								<?php foreach ($handle_by_users as $single_user) { ?>
									<?php $explode_name = explode(' ', $single_user['name']); ?>
									<option value="<?php echo $explode_name[0];?>"
									<?php echo (in_array($explode_name[0], $explode_handle_by))?'selected':''?>>
										<?php echo $explode_name[0];?>
									</option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-1 col-form-label form_name">PO No:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" value="<?php echo $quotation_details['order_no'];?>" readonly>
						</div>
						<label class="col-lg-1 col-form-label form_name" style="<?php echo $form_details_display; ?>">PO Value:</label>
						<div class="col-lg-2 form-group-sub" style="<?php echo $form_details_display; ?>">
							<input type="text" class="form-control" value='<?php echo $currency_details['decimal_number'],$quotation_details['grand_total'];?>' readonly>
						</div>
						<label class="col-lg-1 col-form-label form_name" style="<?php echo $form_details_display; ?>">Payment terms:</label>
						<div class="col-lg-2 form-group-sub" style="<?php echo $form_details_display; ?>">
							<input type="text" class="form-control" value="<?php echo (!empty($payment_term_details['term_value']))?$payment_term_details['term_value']:'';?>" readonly>
						</div>
						<label class="col-lg-1 col-form-label form_name">MTC Sent:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control" name="mtc_sent">
								<option value="">Select MTC Sent</option>
								<option value="Yes"
								<?php echo ($production_information['mtc_sent'] == 'Yes')?'selected':''?>>
									Yes
								</option>
								<option value="No"
								<?php echo ($production_information['mtc_sent'] == 'No')?'selected':''?>>
									No
								</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-1 col-form-label form_name" style="<?php echo $form_details_display; ?>">Delivery Date:</label>
						<div class="col-lg-2 form-group-sub" style="<?php echo $form_details_display; ?>">
							<div class="input-group date">
								<input type="text" class="form-control" value="<?php echo (!empty($production_information['delivery_date'])) ? date('F j, Y', strtotime($production_information['delivery_date'])): "";?>" name="delivery_date" id="delivery_date" />
								<div class="input-group-append kt-hidden">
									<span class="input-group-text">
										<i class="la la-calendar"></i>
									</span>
								</div>
							</div>
						</div>
						<label class="col-lg-1 col-form-label form_name" style="<?php echo $form_details_display; ?>">Current Status:</label>
						<div class="col-lg-2 form-group-sub" style="<?php echo $form_details_display; ?>">
							<select class="form-control handled_by_select_picker" name="production_status">
								<option value="">Select Current Status</option>
								<?php foreach ($production_status as $production_key => $production_details) { ?>
									<option 
										value="<?php echo $production_key;?>"
										data-content="<span class='kt-badge kt-badge--<?php echo $production_details['color'];?> kt-badge--inline kt-badge--rounded kt-font-bolder'><?php echo $production_details['name'];?></span>"
										<?php echo ($production_information['production_status']== $production_key)?'selected':''?>>
											<?php echo $production_details['name'];?>
									</option>
								<?php }?>
							</select>
						</div>
						<label class="col-lg-1 col-form-label form_name">Vendor PO:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" name="vendor_po" value="<?php echo $production_information['vendor_po'];?>">
						</div>
						<label class="col-lg-1 col-form-label form_name">QC Clearance:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control" name="qc_clearance">
								<option value="">Select QC Clearance</option>
								<option value="Yes"
								<?php echo ($production_information['qc_clearance'] == 'Yes')?'selected':''?>>
									Yes
								</option>
								<option value="No"
								<?php echo ($production_information['qc_clearance'] == 'No')?'selected':''?>>
									No
								</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-1 col-form-label form_name" style="<?php echo $form_details_display; ?>">Actual Delivery Date:</label>
						<div class="col-lg-2 form-group-sub" style="<?php echo $form_details_display; ?>">
							<div class="input-group date">
								<input type="text" class="form-control" value="<?php echo (!empty($production_information['actual_delivery_date'])) ? date('F j, Y', strtotime($production_information['actual_delivery_date'])): "";?>" name="actual_delivery_date" id="delivery_date" />
								<div class="input-group-append kt-hidden">
									<span class="input-group-text">
										<i class="la la-calendar"></i>
									</span>
								</div>
							</div>
						</div>
						<label class="col-lg-1 col-form-label form_name" style="<?php echo $form_details_display; ?>">Payment Status:</label>
						<div class="col-lg-2 form-group-sub" style="<?php echo $form_details_display; ?>">
							<input type="text" class="form-control" name="payment_status" value="<?php echo $production_information['payment_status'];?>">
						</div>
						<label class="col-lg-1 col-form-label form_name" style="<?php echo $form_details_display; ?>">QC Comments:</label>
						<div class="col-lg-5 form-group-sub" style="<?php echo $form_details_display; ?>">
							<textarea class="form-control" id="kt_autosize_2" name="special_comment" rows="2" style="overflow: hidden; overflow-wrap: break-word; resize: none; height: 76px;"><?php echo trim($production_information['special_comment']);?></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-1 col-form-label form_name">Latest Update:</label>
						<div class="col-lg-4 form-group-sub">
							<textarea class="form-control" id="kt_autosize_1" name="latest_update" rows="2" style="overflow: hidden; overflow-wrap: break-word; resize: none; height: 76px;"><?php echo trim($production_information['latest_update']);?></textarea>
						</div>
						<a class="col-lg-1 btn btn-bold btn-label-brand btn-lg get_chat_conversation" production_list_id="<?php echo $production_information['id']; ?>" title="Add Comment" href="javascript:;" style="">
							<i class="fa fa-comments kt-font-bolder"></i>
						</a>
						<label class="col-lg-1 col-form-label form_name" style="<?php echo $form_details_display; ?>">Special Comments:</label>
						<div class="col-lg-3 form-group-sub" style="<?php echo $form_details_display; ?>">
							<textarea class="form-control" id="kt_autosize_3" name="special_comment_2" rows="2" style="overflow: hidden; overflow-wrap: break-word; resize: none; height: 76px;"><?php echo trim($production_information['special_comment_2']);?></textarea>
						</div>
						<div class="col-lg-2 form-group-sub">
							<span>
								<strong>RFQ No:  <?php echo $rfq_details['rfq_no'];?><strong>
							</span><br></br>
							<span>
								<strong>Quote No:  <?php echo $quotation_details['quote_no'];?></strong>
							</span>
						</div>
					</div>
				</form>
    
				<!--end::Form-->
			</div>	
		</div>
	</div>
	
	<!--end::Portlet-->
	<div class="row" style="padding-top:2rem;">
		<div class="col-xl-12">

			<!--begin:: Widgets/Personal Income-->
			<div class="kt-portlet kt-portlet--fit kt-portlet--head-lg kt-portlet--head-overlay kt-portlet--height-fluid">
				<div class="kt-portlet__body">
					<div class="kt-widget27">
						<div class="kt-widget27__container kt-portlet__space-x">
							<ul class="nav nav-pills nav-fill" role="tablist" style="padding-top:3rem;">
								<?php $nav_item=true; $tab_item=true;?>
								<?php foreach ($product_production_status as $status_name => $product_details){?>
									<?php if(!empty($product_details)){ ?>
									<li class="nav-item <?php echo $status_name; ?>">
										<a class="nav-link <?php echo ($nav_item) ? ' active':''?>" data-toggle="pill" href="<?php echo '#'.$status_name;?>"><?php echo $status_name; ?></a>
									</li>
									<?php $nav_item=false;?>
									<?php } ?>
								<?php } ?>
							</ul>
							<div class="tab-content">
							<?php foreach ($product_production_status as $status_name => $product_details){
								$status_wise_total = 0;
								?>
								<?php if(!empty($product_details)){ ?>
								<div id="<?php echo $status_name; ?>" class="tab-pane <?php echo ($tab_item) ? ' active':''?>">
									<div class="kt-widget11">
										<div class="table-responsive" style="min-height: 450px;">

											<!--begin::Table-->
											<table class="table">

												<!--begin::Thead-->
												<tbody>
													<tr>
														<td style="width: 5%;">
															<a href="javascript:void(0);" class="kt-widget11__title">Sr.No</a>
														</td>
														<td style="width: 50%;">
															<a href="javascript:void(0);" class="kt-widget11__title">Description</a>
														</td>
														<td style="width: 10%;">
															<a href="javascript:void(0);" class="kt-widget11__title">Product Name</a>
														</td>
														<td style="width: 10%;">
															<a href="javascript:void(0);" class="kt-widget11__title">Material Name</a>
														</td>
														<td style="width: 5%;">
															<a href="javascript:void(0);" class="kt-widget11__title">Quantity</a>
														</td>
														<!-- <td style="width: 10%;">
															<a href="javascript:void(0);" class="kt-widget11__title">Production Status</a>
														</td> -->
														<td style="width: 8%;">
															<a href="javascript:void(0);" class="kt-widget11__title">Quantity Done</a>
														</td>
														<?php if($this->session->userdata('production_access')['production_form_data_view_access']){ ?>
														<td style="width: 7%;">
															<a href="javascript:void(0);" class="kt-widget11__title">Total</a>
														</td>
														<td style="width: 5%;">
															<a href="javascript:void(0);" class="kt-widget11__title">Action</a>
														</td>
														<?php } ?>
													</tr>
												</tbody>

												<!--end::Thead-->

												<!--begin::Tbody-->
												<tbody class="quotation_quantity_body">
													<?php foreach ($product_details as $single_production_details) {?>
														<?php $status_wise_total += $single_production_details['total']; 
														$description = str_replace(['<p>', '</p>', '<br />'], ['<br>', '', ''], $single_production_details['description']);
														?>
													<tr>
														<td>
															<a class="kt-widget11__title"><?php echo $single_production_details['id']; ?></a>
														</td>
														<td>
															<a class="kt-widget11__title"><?php echo $description; ?></a>
														</td>
														<td>
															<a class="kt-widget11__title"><?php echo $single_production_details['product']; ?></a>
														</td>
														<td>
															<a class="kt-widget11__title"><?php echo $single_production_details['material']; ?></a>
														</td>
														<td>
															<a class="kt-widget11__title"><?php echo $single_production_details['quantity']; ?></a>
														</td>
														<!-- <td class="">
															<div class="row">
																<div class="col-lg-12 col-md-9 col-sm-12">
																	<select class="form-control handled_by_select_picker" name="production_status_<?php echo $single_production_details['quotation_dtl_id'];?>" id="<?php echo $single_production_details['production_status'],'_',$single_production_details['quotation_dtl_id']; ?>">
																			<option 
																				value="pending_order"
																				data-content="<span class='kt-badge kt-badge--dark kt-badge--inline kt-badge--rounded kt-font-bolder'>Pending Order</span>"
																				<?php echo ($single_production_details['production_status']== 'pending_order')?'selected':''?>>
																					Pending Order
																			</option>
																			<option 
																				value="semi_ready"
																				data-content="<span class='kt-badge kt-badge--primary kt-badge--inline kt-badge--rounded kt-font-bolder'>Semi Ready</span>"
																				<?php echo ($single_production_details['production_status']== 'semi_ready')?'selected':''?>>
																					Semi Ready
																			</option>
																			<option 
																				value="ready_to_dispatch_order"
																				data-content="<span class='kt-badge kt-badge--primary kt-badge--inline kt-badge--rounded kt-font-bolder'>Ready For Dispatch</span>"
																				<?php echo ($single_production_details['production_status']== 'ready_to_dispatch_order')?'selected':''?>>
																					Ready For Dispatch
																			</option>
																			<option 
																				value="dispatch_order"
																				data-content="<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--rounded kt-font-bolder'>Dispatch Order</span>"
																				<?php echo ($single_production_details['production_status']== 'dispatch_order')?'selected':''?>>
																					Dispatch Order
																			</option>
																			<option 
																				value="on_hold_order"
																				data-content="<span class='kt-badge kt-badge--gainsboro kt-badge--inline kt-badge--rounded kt-font-bolder'>Order On Hold</span>"
																				<?php echo ($single_production_details['production_status']== 'on_hold_order')?'selected':''?>>
																					Order On Hold
																			</option>
																			<option 
																				value="order_cancelled"
																				data-content="<span class='kt-badge kt-badge--danger kt-badge--inline kt-badge--rounded kt-font-bolder'>Order Cancelled</span>"
																				<?php echo ($single_production_details['production_status']== 'order_cancelled')?'selected':''?>>
																					Order Cancelled
																			</option>
																			<option 
																				value="mtt"
																				data-content="<span class='kt-badge kt-badge--danger kt-badge--inline kt-badge--rounded kt-font-bolder'>ICE</span>"
																				<?php echo ($single_production_details['production_status']== 'mtt')?'selected':''?>>
																					ICE
																			</option>
																			<option 
																				value="query"
																				data-content="<span class='kt-badge kt-badge--danger kt-badge--inline kt-badge--rounded kt-font-bolder'>Query</span>"
																				<?php echo ($single_production_details['production_status']== 'query')?'selected':''?>>
																					Query
																			</option>
																			<option 
																				value="mtt_rfd"
																				data-content="<span class='kt-badge kt-badge--danger kt-badge--inline kt-badge--rounded kt-font-bolder'>In transit ICE</span>"
																				<?php echo ($single_production_details['production_status']== 'mtt_rfd')?'selected':''?>>
																					In transit ICE
																			</option>
																	</select>
																</div>
															</div>
														</td>-->
														<td>
															<span class='kt-badge kt-badge--brand kt-badge--inline kt-badge--rounded kt-font-bolder'>
															<?php echo $single_production_details['production_quantity_done'];?> Quantity
															</span>
															<!-- <input type="number" class="kt-widget11__title form-control" value="<?php echo trim($single_production_details['production_quantity_done']);?>" name="production_quantity_<?php echo $single_production_details['quotation_dtl_id'];?>">
															<?php if((int)$single_production_details['quantity'] > (int)$single_production_details['production_quantity_done']) {?>
																<div><hr></div>
																<span class='kt-badge kt-badge--warning kt-badge--inline kt-badge--rounded kt-font-bolder'>
																	<?php echo (((int)$single_production_details['quantity'])-((int)$single_production_details['production_quantity_done']));?>
																		Quantity Pending
																</span>
															<?php } ?> -->
														</td> 
														<?php if($this->session->userdata('production_access')['production_form_data_view_access']){ ?>
														<td>
															<a href="javascript:void(0);" class="kt-widget11__title"><?php echo $currency_details['decimal_number'].$single_production_details['total']; ?></a>
														</td>
														<td>
															<button type="button" class="btn btn-primary btn-icon update_product_status kt-hidden" max_value="<?php echo $single_production_details['quantity'];?>" production_status_id = "<?php echo $single_production_details['production_status'],'_',$single_production_details['quotation_dtl_id']; ?>" quotation_dtl_id="<?php echo $single_production_details['quotation_dtl_id'];?>"><i class="fa fa-check"></i></button>
															<button type="button" class="btn btn-primary btn-icon get_quotation_quantity_status btn-sm" data-toggle="modal" data-target="#update_quantity_status" quotation_dtl_id="<?php echo $single_production_details['quotation_dtl_id'];?>"><i class="fa fa-eye"></i></button>
														</td>
														<?php } ?>
													</tr>
													<?php } ?>
													<?php if($this->session->userdata('production_access')['production_form_data_view_access']){ ?>
													<tr>
														<td></td>
														<td>
															<a href="javascript:void(0);" class="kt-widget11__title">TOTAL</a>
														</td>
														<td></td>
														<td></td>
														<td></td>
														<td class=""></td>
														<td></td>
														<td>
															<a href="javascript:void(0);" class="kt-widget11__title"><?php echo $currency_details['decimal_number'].$status_wise_total; ?></a>
														</td>
														<td></td>
													</tr>
													<?php } ?>
												</tbody>

												<!--end::Tbody-->
											</table>

											<!--end::Table-->
										</div>
									</div>
								</div>
								<?php $tab_item=false;?>
								<?php } ?>
							<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!--end:: Widgets/Personal Income-->
		</div>
	</div>
</div>

<!-- end:: Content -->

<!--begin::Modal-->
<div class="modal fade" id="update_quantity_status" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="padding-top: 200px;">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Update Quanity</h5>
				<h5 class="modal-title" id="exampleModalLabel">Total Quantity <span class="total_quantity"></span></h5>
			</div>
			<div class="modal-body">
				<form class="form_quotation_quantity_status"></form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary save_quotation_quantity_status" quotation_dtl_id =0 total_quantity=0>Save</button>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->

<!--Begin:: Chat-->
<div class="modal fade- modal-sticky-bottom-right" id="kt_chat_modal" role="dialog" data-backdrop="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="kt-chat">
				<div class="kt-portlet kt-portlet--last">
					<div class="kt-portlet__head">
						<div class="kt-chat__head ">
							<div class="kt-chat__left">
								<div class="kt-chat__label">
									<a href="#" class="kt-chat__title">
										<em class= "header_1"></em>
									</a>
									<span class="kt-chat__status header_2">Work Order No</span>
								</div>
							</div>
							<div class="kt-chat__right"></div>
						</div>
					</div>
					<div class="kt-portlet__body">
						<div class="kt-scroll kt-scroll--pull chat_scroll" data-height="410" data-mobile-height="225">
							<div class="layer-white rfq_chat_loader">
								<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
							</div>
							<div class="kt-chat__messages kt-chat__messages--solid div_chat_history"></div>
						</div>
					</div>
					<div class="kt-portlet__foot">
						<form  id="" style="display:block;">
							<div class="kt-chat__input">
								<div class="kt-chat__editor"></div>
								<div class="kt-chat__toolbar">
									<div class="kt_chat__tools">
										<div class="query_type"></div>
									</div>
									<div class="kt_chat__actions">
										<button type="button" class="btn btn-brand btn-md btn-font-sm btn-upper btn-bold" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--ENd:: Chat-->