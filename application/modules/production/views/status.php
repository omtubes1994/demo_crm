<?php foreach ($order_status_wise_total as $single_order_details) { ?>
	<?php if(!in_array($this->session->userdata('user_id'), array(191))){ ?>
	<div class="col-xl-3">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="production_title_name" style="font-size: 14px;">
						Total of <?php echo $single_order_details['title_name'];?> (<?php echo $single_order_details['total_work_order']; ?>).
						<span class="production_title_name">
							<?php echo $currency_list[3];?> :
							<em class="production_title_value">
								<?php echo $single_order_details['total'];?>
							</em>
						</span>
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-section kt-section--last">
						<a href="javascript:;" class="btn btn-label-brand btn-sm btn-bold view_details" order_type="<?php echo $single_order_details['order_type'];?>" action_value="show" style="background-color: gainsboro; border-color: gainsboro; color: #767676; font-weight: 800;"><i class="kt-font-dark flaticon2-line-chart"></i>View Details</a>
					</div>
				</div>
			</div>
			<div class="kt-portlet__body" style="padding: 10px 10px 10px 10px;">
				<div class="kt-scroll ps ps--active-y" data-scroll="true" style="height: 75px; overflow: hidden;">
					<div class="kt-section kt-section--space-md" style="margin-bottom: 10px;">
						<div class="kt-widget24 kt-widget24--solid">
							<div class="kt-widget24__details">
								<div class="row" style="width: 100%;">
									<?php foreach ($single_order_details['currency_wise_total'] as $single_currency_details) { ?>
										<div class="col-lg-6">
											<span class="production_title_value">
												<a href="#" class="production_title_name kt-align-left" title="Click to edit">
													<?php echo $currency_list[$single_currency_details['currency']];?> :
													<em class="kt-align-right">
														<?php echo $single_currency_details['total'];?>
													</em>
												</a>
											</span>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php }else{ ?>
	<div class="col-xl-4">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="production_title_name" style="font-size: 14px;">
						<span class="production_title_name">
							Total of <?php echo $single_order_details['title_name'];?> Work Order :
							<em class="production_title_value">
								<?php echo $single_order_details['total_work_order'];?>
							</em>
						</span>
					</h3>
				</div>
			</div>
		</div>
	</div>
	<?php }?>
<?php } ?>