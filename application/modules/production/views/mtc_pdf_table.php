<table class="table table-striped" style="margin: 0% 0% 0% 0%;">
	<thead>
		<tr>
			<th scope="col" style="width:10%;">#</th>
			<th scope="col">Pdf Name</th>
			<th scope="col">Date</th>
			<th scope="col" style="width:10%;">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php if(!empty($pdf)) {?>
			<?php foreach(explode(',', $pdf) as $pdf_key => $single_pdf_name) {?>
				<tr>
					<th scope="row"><?php echo $pdf_key+1;?></th>
					<td class="kt-font-bold"><?php echo $single_pdf_name; ?></td>
					<td class="kt-font-bold">
					<?php 
						$explode = explode("_", $single_pdf_name);

						echo date('g:i a F j ', $explode[3]); 
					?>
					</td>
					<td class="kt-font-bold">
						<div class="row" style="padding: 0px 20px 0px 0px;">
							<div class="col-md-6">
		        				<a class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View Proforma" href="<?php echo base_url('assets/production_mtc_pdf/'.$single_pdf_name);?>" style="color: #212529;" target="_blank">
						    		<i class="la la-eye kt-font-bolder" style="color: #212529;"></i>
							    </a>
							</div>
							<div class="col-md-6">
						    	<button class="btn btn-sm btn-clean btn-icon btn-icon-md delete_mtc_pdf_production" title="Delete" pdf_key="<?php echo $pdf_key;?>">
							    	<i class="la la-trash kt-font-bolder" style="color: #212529;"></i>
							    </button>
							</div>
						</div>
					</td>
				</tr>
			<?php } ?>
		<?php } else {?>
		<tr>
			<th scope="row"></th>
			<td>NO DATA FOUND</td>
			<td></td>
		</tr>
		<?php } ?>
	</tbody>
</table>