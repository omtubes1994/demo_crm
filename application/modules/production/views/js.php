jQuery(document).ready(function() {
	var controller = "<?php echo $this->uri->segment('1',0)?>";
	var method = "<?php echo $this->uri->segment('2',0)?>";
	if(method == 'update_production_listing') {
		All_js.init();
	}
    if(method == 'production_listingz') {
        Listing_js.init();
    }
	$('.save_production_details_form').click(function(){
        if(!$(this).hasClass('kt-spinner')) {
            set_reset_spinner($(this));
	        ajax_call_function({call_type: 'save_production_details_form',production_details_form: $('form#production_details_form').serializeArray()}, 'save_production_details_form');
        }
	});

    $('li.tab_name_click').click(function(){
        var tab_name = $(this).attr('tab-name');
        if(!$('li.'+tab_name).hasClass('active_list')){
            $('li.active_list').removeClass('active_list');
            $('li.'+tab_name).addClass('active_list');
            ajax_call_function({call_type: 'change_tab',tab_name: tab_name},'change_tab');  
        }
    });
    $('.add_production_listing_search_filter_form').click(function(){
        $('.production_listing_search_filter').show();
    });
    $('div.production_listing_search_filter').on('click', 'button.production_listing_search_filter_form_submit', function(){
        ajax_call_function({call_type: 'search_filter', tab_name: get_tab_name(), production_search_form: $('form#production_listing_search_filter_form').serializeArray()},'search_filter');  
    });
    $('div#product_list_paggination').on('click', '.production_list_paggination_number', function(){
        ajax_call_function({call_type: 'paggination_filter', tab_name: get_tab_name(), production_search_form: $('form#production_listing_search_filter_form').serializeArray(), limit: $(this).attr('limit'), offset: $(this).attr('offset')},'paggination_filter'); 
    });
    $('div#product_list_paggination').on('change', 'select#set_limit', function(){
        ajax_call_function({call_type: 'paggination_filter', tab_name: get_tab_name(), production_search_form: $('form#production_listing_search_filter_form').serializeArray(), limit: $('select#set_limit').val(), offset: 0},'paggination_filter');
    });
    $('#production_listing').on('click', '.delete_production_list', function(){
        if(!$(this).hasClass('kt-spinner')) {
            set_reset_spinner($(this), true, 'dark', 'center');
            ajax_call_function({call_type: 'delete_production_list', delete_id: $(this).attr('production_id')},'delete_production_list');
        }
    });
    $('#production_listing').on('click', '.add_query', function(){
        $('input#add_query_quotation_id').val($(this).attr('quotation_for'));
        ajax_call_function({call_type: 'get_query_history', quote_id: $(this).attr('quotation_for'), 'query_type': 'production'},'get_query_history');
    });
    $('button.add_query_submit').click(function(){
        alert('came in');
        ajax_call_function({quote_id: $('#add_query_quotation_id').val(), add_query_quotation_type: 'production'},'add_query_submit', 'quotations/addQuery');
    });
});
function get_tab_name() {
    if($('li.pending_order').hasClass('active_list')) {
        return 'pending_order';
    } else if($('li.on_hold_order').hasClass('active_list')) {
        return 'on_hold_order';
    } else if($('li.dispatch_order').hasClass('active_list')) {
        return 'dispatch_order';
    } else if($('li.ready_to_dispatch_order').hasClass('active_list')) {
        return 'ready_to_dispatch_order';
    }
    return '';
}
function set_reset_spinner(obj, set_unset_flag = true, spinner_color = 'light', spinner_align = 'right') {

    if(set_unset_flag) {

        $(obj).addClass('kt-spinner');
        $(obj).addClass('kt-spinner--'+spinner_align);
        $(obj).addClass('kt-spinner--sm');
        $(obj).addClass('kt-spinner--'+spinner_color);   
    } else{

        $(obj).removeClass('kt-spinner');
        $(obj).removeClass('kt-spinner--'+spinner_align);
        $(obj).removeClass('kt-spinner--sm');
        $(obj).removeClass('kt-spinner--'+spinner_color);
    }
}

function ajax_call_function(data, callType, url = "<?php echo base_url('production/ajax_function'); ?>") {

	$.ajax({
		type: 'POST',
		data: data,
		url: url,
		dataType: 'JSON',
		success: function(res){
			if(res.status == 'successful') {
                switch (data.call_type) {

                    case 'save_production_details_form':
                        setTimeout(function(){

                            toastr.info("Details Are Updated!!!"); 
                            setTimeout(function(){
                                location.reload();
                            }, 2000);
                        }, 1000);
                    break;
                    case 'change_tab':
                    case 'search_filter':
                    case 'paggination_filter':
                        
                        $('div.production_listing_search_filter').html('').html(res.search_filter_body);
                        $('tbody#production_listing').html('').html(res.list_body);
                        $('div#product_list_paggination').html('').html(res.paggination_filter_body);
                        Listing_js.init();
                        //$('input#delivery_date').val("<?php echo $this->session->userdata('search_filter_delivery_date');?>");
                        
                    break;
                    case 'delete_production_list':
                        toastr.success("Details Are Deleted!!!");
                        ajax_call_function({call_type: 'search_filter', tab_name: get_tab_name(), production_search_form: $('form#production_listing_search_filter_form').serializeArray()},'change_tab');  
                    break;
                    case 'get_query_history':
                        $('div#production_query_history').html('').html(res.query_table_history);
                    break;
    			}
                $('.layer-white').hide();
            } else {
                    
                switch (data.call_type) {

                    case 'delete_production_list':

                        toastr.error("Details Are Not Deleted!!!");
                    break;
                }
            }
		},
		beforeSend: function(response){
            switch (data.call_type) {
                case 'save_production_details_form':
                    toastr.warning("Details Are Getting Updated. Wait for 20 seconds."); 
                break;
                case 'delete_production_list':
                    toastr.warning("Details Are Getting Deleted. Wait for 10 seconds."); 
                break;
                case 'change_tab':
                case 'search_filter':
                case 'paggination_filter':
                    
                    $('.layer-white').show();
                break;
                case 'add_query_submit':
                    toastr.warning("Query is getting submitted. Wait for 10 seconds."); 
                break;
          }
		}
	});
};
// Class definition

var All_js = function () {

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }
    
    // Private functions
    var demos = function () {   
        // enable clear button 
        $('#delivery_date, #delivery_date_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: true,
            templates: arrows,
            format: 'yyyy-mm-dd',
        });
        $('.handled_by_select_picker').selectpicker();
        autosize($('#kt_autosize_1'));
        autosize($('#kt_autosize_2'));

    };

    return {
        // public functions
        init: function() {
            demos(); 
        }
    };
}();

var Listing_js = function () {

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }
    
    // Private functions
    var demos = function () {   
        // enable clear button 
        $('#listing_date, #listing_delivery_date').datepicker({
            rtl: KTUtil.isRTL(),
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: true,
            templates: arrows,
            format: 'yyyy-mm-dd',
        });
        $('.listing_select_picker').selectpicker();

    };

    var daterangepickerInit = function() {
    
        if ($('#kt_dashboard_daterangepicker').length == 0) {
            return;
        }

        var picker = $('#kt_dashboard_daterangepicker');
        var start = moment().subtract(1, 'days');
        var end = moment().subtract(1, 'days');

        function cb(start, end, label) {
            var title = '';
            var range = '';
            if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D Y');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D Y');
            } else {                
                range = start.format('MMM D Y') + ' - ' + end.format('MMM D Y');
            }

            if(label == 'first_time') {
                $('#kt_dashboard_daterangepicker_title').html('Select');
                $('#kt_dashboard_daterangepicker_date').html('Date');
                $('input#delivery_date').val('');
            } else {

                $('#kt_dashboard_daterangepicker_title').html(title);
                $('#kt_dashboard_daterangepicker_date').html(range);
                $('input#delivery_date').val(range);
            }
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end, 'first_time');
    };

    var proformadaterangepickerInit = function() {
    
        if ($('#kt_proforma_daterangepicker').length == 0) {
            return;
        }

        var picker = $('#kt_proforma_daterangepicker');
        var start = moment().subtract(1, 'days');
        var end = moment().subtract(1, 'days');

        function cb(start, end, label) {
            var title = '';
            var range = '';
            if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D Y');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D Y');
            } else {                
                range = start.format('MMM D Y') + ' - ' + end.format('MMM D Y');
            }

            if(label == 'first_time') {

                $('#kt_proforma_daterangepicker_title').html('Select');
                $('#kt_proforma_daterangepicker_date').html('Date');
                $('input#proforma_date').val('');
            } else {

                $('#kt_proforma_daterangepicker_title').html(title);
                $('#kt_proforma_daterangepicker_date').html(range);
                $('input#proforma_date').val(range);
            }
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end, 'first_time');
    };


    return {
        // public functions
        init: function() {
            demos(); 
            daterangepickerInit(); 
            proformadaterangepickerInit(); 
        }
    };
}();
toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-top-right",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

