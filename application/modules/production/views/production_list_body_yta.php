<?php 
if(!empty($production_list)){
	foreach( $production_list as $production_list_key => $production_list_value){

			// echo "<pre>";print_r($production_list_value);echo"</pre><hr>";exit;
?>
<tr>
	<td class="first_div" style="width: 30px; padding: 0px 0px 0px 15px;"><?php echo $production_list_key+1; ?></td>
	<td style="width: 30px; padding: 0px 0px 0px 15px;"></td>
	<td class="kt-align-center" style="width: 200px;padding: 0px 10px 0px 10px;">
		<div class="kt-widget__action">
			<button type="button" class="<?php echo $production_list_value['work_order_class_name'];?>" quotation_mst_id="<?php echo $production_list_value['quotation_mst_id']; ?>" rfq_mst_id="<?php echo $production_list_value['rfq_id']; ?>" style="margin: 5px 0px 5px 0px;">
				<?php echo $production_list_value['work_order_action_name'];?>
			</button>
			<hr style="margin: 0px 0px 0px 0px;">
			<button type="button" class="<?php echo $production_list_value['acknowledge_order_class_name'];?>" quotation_mst_id="<?php echo $production_list_value['quotation_mst_id']; ?>" rfq_mst_id="<?php echo $production_list_value['rfq_id']; ?>" style="    margin: 5px 0px 5px 0px;">
				<?php echo $production_list_value['acknowledge_order_action_name'];?>
			</button>
			<hr style="margin: 0px 0px 0px 0px;">
			<button type="button" class="<?php echo $production_list_value['sales_acknowledge_order_class_name'];?>" quotation_mst_id="<?php echo $production_list_value['quotation_mst_id']; ?>" rfq_mst_id="<?php echo $production_list_value['rfq_id']; ?>" style="    margin: 5px 0px 5px 0px;">
				<?php echo $production_list_value['sales_acknowledge_order_action_name'];?>
			</button>
		</div>
		<hr style="margin: 0px 0px 0px 0px;">
		<?php if($this->session->userdata('quotation_access')['quotation_list_action_purchase_order_view_access']){ ?>
			<?php if($production_list_value['purchase_order'] != '' && $production_list_value['purchase_order'] != NULL){ ?>
				<a href="<?php echo site_url('assets/purchase_orders/'.$production_list_value['purchase_order']); ?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View Purchase Order" target="_blank" >
					<i class="fa fa-file-pdf-o"></i>
				</a>
			<?php } ?>
		<?php } ?>
		<?php if($this->session->userdata('quotation_access')['quotation_list_action_view_proforma_pdf_access']){ ?>
			<a href="<?php echo base_url('pdf_management/proforma_pdf/'.$production_list_value['quotation_mst_id']);?>" target="_blank" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Pdf" >
				<i class="la la-eye"></i>
			</a>
		<?php } ?>
		<?php if($this->session->userdata('rfq_access')['rfq_list_action_edit_access']){ ?>
			<a href="<?php echo base_url('procurement/addRFQ/'.$production_list_value['rfq_id']);?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" target="_blank" title="View RFQ">
				<i class="la la-info-circle"></i>
			</a>
		<?php } ?>
		<?php if($this->session->userdata('quotation_access')['quotation_list_action_edit_access']){ ?>
			<a href="<?php echo base_url('quotations/add/'.$production_list_value['quotation_mst_id'].'/'.$production_list_value['rfq_id']);?>" target="_blank" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">
				<i class="la la-edit"></i>
			</a>
		<?php } ?>
	</td>
	<td style="width: 300px; padding: 0px 0px 0px 15px;">
		<span>
			<abbr>
				<?php if($this->session->userdata('production_access')['production_listing_filter_client_name']){?>
					<em class="kt-font-bolder"> Client Name:
						<i><?php echo $production_list_value['client_details']['name']; ?></i>
					</em>
				<?php } ?>
				<em class="kt-font-bolder"> Sales Person: 
					<i class=""><?php echo $users_details[$production_list_value['sales_person_id']]; ?></i>
				</em>
			</abbr>
		</span>
	</td>
	<td style="width: 230px; padding:0px 0px 0px 15px;">
		<span>    
			<abbr>
				<em class="kt-font-bolder"> Proforma #: 
					<i><?php echo $production_list_value['proforma_no']; ?></i>
				</em>
				<em class="kt-font-bolder"> PO #: 
					<i class=""><?php echo $production_list_value['order_no']; ?></i>
				</em>
				<em class="kt-font-bolder"> Proforma Date: 
					<i>
					<?php 
						if(!empty($production_list_value['confirmed_on'])) {
							echo date('F j, Y',strtotime($production_list_value['confirmed_on']));
						} else {
							echo "Proforma Date Not Found";
						}
					?>	
					</i>
				</em> 
			</abbr>
		</span>
	</td>
	<td class="kt-align-right kt-font-bolder kt-font-info" style="width: 100px;
	padding: 0px 10px 0px 0px;">
		<?php if(in_array($this->session->userdata('role'), array(1, 5, 6, 14, 16, 17))){ ?>
		<?php echo $production_list_value['currency_details']['currency_icon'],number_format($production_list_value['grand_total'],2,'.',',');?>
		<?php } ?>
		<span>    
			<abbr>
				<hr>
				<?php 
					foreach (array(
								array('name'=>'assigned_to'),
								array('name'=>'assigned_to_2'),
								array('name'=>'assigned_to_3')
							)
							as 
							$assign_details) { 
				?>		
					<em class="kt-font-bolder"> 
						<?php 
							if(isset($users_details[$assign_details['name']])){
								
								echo $users_details[$assign_details['name']]; 
							}
						?>
					</em>
					<hr>	
				<?php
					}
				?>
			</abbr>
		</span>		
	</td>
	<td style="width: 200px; padding: 0px 0px 0px 15px;"></td>
	<td style="width: 250px; padding: 0px 0px 0px 15px;"></td>
	<td style="width: 250px; padding: 0px 0px 0px 15px;"></td>
	<td class="kt-align-center" style="width: 130px;padding: 0px 0px 0px 15px;"></td>
	<td class="kt-align-center" style="width: 130px; padding: 0px 0px 0px 15px;"></td>
	<td class="kt-align-center" style="width: 294px;padding: 0px 0px 0px 15px;"></td>
</tr>
<?php 
	}
}
?>