<div class="kt-notes__item kt-notes__item--clean" style="padding: 0 0 5px 45px;">
    <div class="kt-notes__media">
        <span class="kt-notes__circle kt-hidden-"></span>
    </div>
    <div class="kt-notes__content">
        <div class="kt-notes__section">
            <div class="kt-notes__info">
                <a href="#" class="kt-notes__title">
                <?php echo date('d M, Y'); ?>
                </a>
                <span class="kt-notes__desc"></span>
            </div>
        </div>
        <span class="kt-notes__body"></span>
    </div>
    <div class="kt-notes__media">
        <span class="kt-notes__circle kt-hidden-"></span>
    </div>
    <div class="kt-notes__content">
        <div class="kt-notes__section">
            <div class="kt-notes__info">
                <a href="#" class="kt-notes__title">
                <?php echo date('d M, Y'); ?>
                </a>
                <span class="kt-notes__desc"></span>
            </div>
        </div>
        <span class="kt-notes__body"></span>
    </div>
    <div class="kt-notes__media">
        <span class="kt-notes__circle kt-hidden-"></span>
    </div>
    <div class="kt-notes__content">
        <div class="kt-notes__section">
            <div class="kt-notes__info">
                <a href="#" class="kt-notes__title">
                <?php echo date('d M, Y'); ?>
                </a>
                <span class="kt-notes__desc"></span>
            </div>
        </div>
        <span class="kt-notes__body"></span>
    </div>
</div>
<div class="row all_file_upload_history" style="margin-right: 0px; margin-left: 0px;">
    <div class="col-lg-4 kt-notes__item">
        <div class="kt-notes__content" style="padding: 0px;">
            <span class="kt-notes__body" style="height: 400px; padding: 0px;">
                <?php echo ""; ?>
            </span>
        </div>
    </div> 
</div>