<style type="text/css">
	.form_name{
	    color: #767676 !important;
	    font-size: 14px !important;
	    font-family: 'latomedium' !important;
	    padding: 10px 10px !important; 
	    font-weight: bold !important;
	}
	.kt-badge--gainsboro{
		background-color: gainsboro !important;
	}
	.kt-widget27 .kt-widget27__container .nav .nav-item.pending_order > a {
      border-color: #282a3c !important;
      color: #282a3c !important;
   }
	.kt-widget27 .kt-widget27__container .nav .nav-item.pending_order > a.active {
		color: #ffffff !important;
      background: #282a3c !important;
	}

	.kt-widget27 .kt-widget27__container .nav .nav-item.in_production > a {
      border-color: #ffb822 !important;
      color: #ffb822 !important;
   }
	.kt-widget27 .kt-widget27__container .nav .nav-item.in_production > a.active {
      color: #111111 !important;
      background: #ffb822 !important;
	}

	.kt-widget27 .kt-widget27__container .nav .nav-item.ready_for_dispatch > a, .kt-widget27 .kt-widget27__container .nav .nav-item.semi_ready > a{
      border-color: #5867dd !important;
      color: #5867dd !important;
   }
	.kt-widget27 .kt-widget27__container .nav .nav-item.ready_for_dispatch > a.active, .kt-widget27 .kt-widget27__container .nav .nav-item.semi_ready > a.active {
      color: #ffffff !important;
      background: #5867dd !important;
	}

	.kt-widget27 .kt-widget27__container .nav .nav-item.delay_in_delivery > a, .kt-widget27 .kt-widget27__container .nav .nav-item.order_cancelled > a {
      border-color: #ff0000 !important;
      color: #ff0000 !important;
   }
	.kt-widget27 .kt-widget27__container .nav .nav-item.delay_in_delivery > a.active, .kt-widget27 .kt-widget27__container .nav .nav-item.order_cancelled > a.active {
		color: #ffffff !important;
      background: #ff0000 !important;
	}

	.kt-widget27 .kt-widget27__container .nav .nav-item.on_hold > a {
      border-color: gainsboro !important;
      color: gainsboro !important;
   }
	.kt-widget27 .kt-widget27__container .nav .nav-item.on_hold > a.active {
		color: #000 !important;
      background-color: gainsboro !important;
	}

	.kt-widget27 .kt-widget27__container .nav .nav-item.dispatched > a {
      border-color: #0abb87 !important;
      color: #0abb87 !important;
   }
	.kt-widget27 .kt-widget27__container .nav .nav-item.dispatched > a.active {
		color: #ffffff !important;
      background: #0abb87 !important;
	}
</style>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col">
			<div class="alert alert-light alert-elevate fade show" role="alert">
				<div class="alert-icon"><i class="flaticon-edit kt-font-brand"></i></div>
				<div class="alert-text">
					<h2 class="kt-font-info">Product Details</h2>
				</div>
				<div>
					<button type="button" class="btn btn-primary btn-wide save_production_details_form">Save Details</button>
				</div>
			</div>
		</div>
	</div>
	<!--begin::Portlet-->
	<div class="kt-portlet" style="background: #F5F5F5;">
		<div class="kt-portlet__body">
			<div class="kt-form__section kt-form__section--first">
				<!--begin::Form-->
				<form class="kt-form" id="production_details_form">
					<input type="text" name="quotation_mst_id" value="<?php echo $this->uri->segment('3');?>" hidden>
					<div class="form-group row">
						<input type="text" value="<?php echo $production_information['id'];?>" name="id" hidden>
						<label class="col-lg-1 col-form-label form_name">Work Order No:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" value="<?php echo $production_information['work_order_no'];?>" readonly>
						</div>
						<label class="col-lg-1 col-form-label form_name">Sales Person:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" value="<?php echo $users_details[$quotation_details['assigned_to']];?>" readonly>
						</div>
						<label class="col-lg-1 col-form-label form_name">Client Name:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" value="<?php echo $client_details['client_name']; ?>" readonly>
						</div>
						
						<label class="col-lg-1 col-form-label form_name">Proforma No:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" value="<?php echo $quotation_details['proforma_no'];?>" readonly>
						</div>		
					</div>
					<div class="form-group row">
						<label class="col-lg-1 col-form-label form_name">Proforma Date:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" value="<?php echo date('F, j Y', strtotime($quotation_details['confirmed_on']));?>" readonly>
						</div>
						<label class="col-lg-1 col-form-label form_name">Item:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control">
								<optgroup label="Product">
									<?php foreach ($product_list as $single_product) { ?>
										<option><?php echo $single_product?></option>
									<?php } ?>
								</optgroup>
								<optgroup label="Material">
									<?php foreach ($material_list as $single_material) { ?>
										<option><?php echo $single_material?></option>
									<?php } ?>
								</optgroup>
							</select>
						</div>
						<label class="col-lg-1 col-form-label form_name">PO Value:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" value='<?php echo $currency_details['decimal_number'],$quotation_details['grand_total'];?>' readonly>
						</div>
						<label class="col-lg-1 col-form-label form_name">Payment terms:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" value="<?php echo $payment_term_details['term_value'];?>" readonly>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-1 col-form-label form_name">Product Family:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control" name="product_family">
								<option value="">Select Product Family</option>
								<option value="piping" 
								<?php echo ($production_information['product_family'] == 'piping')?'selected':''?>>
									Piping
								</option>
								<option value="instrumentation"
								<?php echo ($production_information['product_family'] == 'instrumentation')?'selected':''?>>
									Instrumentation
								</option>
								<option value="precision"
								<?php echo ($production_information['product_family'] == 'precision')?'selected':''?>>
									Precision
								</option>
							</select>
						</div>
						<label class="col-lg-1 col-form-label form_name">Delivery Date:</label>
						<div class="col-lg-2 form-group-sub">
							<div class="input-group date">
								<input type="text" class="form-control" value="<?php echo (!empty($production_information['delivery_date'])) ? date('Y-m-d', strtotime($production_information['delivery_date'])): "";?>" name="delivery_date" id="delivery_date" />
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="la la-calendar"></i>
									</span>
								</div>
							</div>
						</div>
						<label class="col-lg-1 col-form-label form_name">Vendor PO:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" name="vendor_po" value="<?php echo $production_information['vendor_po'];?>">
						</div>
						<label class="col-lg-1 col-form-label form_name">Handled By:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control handled_by_select_picker" name="handled_by" multiple>
								<?php $explode_handle_by = explode(',', $production_information['handled_by']); ?>
								<?php foreach ($handle_by_users as $single_user) { ?>
									<?php $explode_name = explode(' ', $single_user['name']); ?>
									<option value="<?php echo $explode_name[0];?>"
									<?php echo (in_array($explode_name[0], $explode_handle_by))?'selected':''?>>
										<?php echo $explode_name[0];?>
									</option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-1 col-form-label form_name">Current Status:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control handled_by_select_picker" name="production_status">
								<option value="">Select Current Status</option>
								<?php foreach ($production_status as $production_key => $production_details) { ?>
									<option 
										value="<?php echo $production_key;?>"
										data-content="<span class='kt-badge kt-badge--<?php echo $production_details['color'];?> kt-badge--inline kt-badge--rounded kt-font-bolder'><?php echo $production_details['name'];?></span>"
										<?php echo ($production_information['production_status']== $production_key)?'selected':''?>>
											<?php echo $production_details['name'];?>
									</option>
								<?php }?>
							</select>
						</div>
						<label class="col-lg-1 col-form-label form_name">Payment Status:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" name="payment_status" value="<?php echo $production_information['payment_status'];?>">
						</div>
						<label class="col-lg-1 col-form-label form_name">QC Clearance:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control" name="qc_clearance">
								<option value="">Select QC Clearance</option>
								<option value="Yes"
								<?php echo ($production_information['qc_clearance'] == 'Yes')?'selected':''?>>
									Yes
								</option>
								<option value="No"
								<?php echo ($production_information['qc_clearance'] == 'No')?'selected':''?>>
									No
								</option>
							</select>
						</div>
						<label class="col-lg-1 col-form-label form_name">MTC Sent:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control" name="mtc_sent">
								<option value="">Select MTC Sent</option>
								<option value="Yes"
								<?php echo ($production_information['mtc_sent'] == 'Yes')?'selected':''?>>
									Yes
								</option>
								<option value="No"
								<?php echo ($production_information['mtc_sent'] == 'No')?'selected':''?>>
									No
								</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-1 col-form-label form_name">Latest Update:</label>
						<div class="col-lg-5 form-group-sub">
							<textarea class="form-control" id="kt_autosize_1" name="latest_update" rows="2" style="overflow: hidden; overflow-wrap: break-word; resize: none; height: 76px;"><?php echo trim($production_information['latest_update']);?></textarea>
						</div>
						<label class="col-lg-1 col-form-label form_name">QC Comments:</label>
						<div class="col-lg-5 form-group-sub">
							<textarea class="form-control" id="kt_autosize_2" name="special_comment" rows="2" style="overflow: hidden; overflow-wrap: break-word; resize: none; height: 76px;"><?php echo trim($production_information['special_comment']);?></textarea>
						</div>		
					</div>
					<div class="form-group row">
						<label class="col-lg-1 col-form-label form_name">Special Comments:</label>
						<div class="col-lg-5 form-group-sub">
							<textarea class="form-control" id="kt_autosize_3" name="special_comment_2" rows="2" style="overflow: hidden; overflow-wrap: break-word; resize: none; height: 76px;"><?php echo trim($production_information['special_comment_2']);?></textarea>
						</div>		
					</div>
				</form>
    
				<!--end::Form-->
			</div>	
		</div>
	</div>
	
	<!--end::Portlet-->
	<div class="row" style="padding-top:2rem;">
		<div class="col-xl-12">

			<!--begin:: Widgets/Personal Income-->
			<div class="kt-portlet kt-portlet--fit kt-portlet--head-lg kt-portlet--head-overlay kt-portlet--height-fluid">
				<div class="kt-portlet__body">
					<div class="kt-widget27">
						<div class="kt-widget27__container kt-portlet__space-x">
							<ul class="nav nav-pills nav-fill" role="tablist" style="padding-top:3rem;">
								<?php $nav_item=true; $tab_item=true;?>
								<?php foreach ($product_production_status as $status_name => $product_details){?>
								<li class="nav-item <?php echo $status_name; ?>">
									<a class="nav-link <?php echo ($nav_item) ? ' active':''?>" data-toggle="pill" href="<?php echo '#'.$status_name;?>"><?php echo $status_name; ?></a>
								</li>
								<?php $nav_item=false;?>
								<?php } ?>
							</ul>
							<div class="tab-content">
							<?php foreach ($product_production_status as $status_name => $product_details){?>
								<div id="<?php echo $status_name; ?>" class="tab-pane <?php echo ($tab_item) ? ' active':''?>">
									<div class="kt-widget11">
										<div class="table-responsive" style="min-height: 450px;">

											<!--begin::Table-->
											<table class="table">

												<!--begin::Thead-->
												<tbody>
													<tr>
														<td style="width: 45%;">
															<a href="javascript:void(0);" class="kt-widget11__title">Description</a>
														</td>
														<td style="width: 10%;">
															<a href="javascript:void(0);" class="kt-widget11__title">Product Name</a>
														</td>
														<td style="width: 10%;">
															<a href="javascript:void(0);" class="kt-widget11__title">Material Name</a>
														</td>
														<td style="width: 5%;">
															<a href="javascript:void(0);" class="kt-widget11__title">Quantity</a>
														</td>
														<td style="width: 10%;">
															<a href="javascript:void(0);" class="kt-widget11__title">Production Status</a>
														</td>
														<td style="width: 15%;">
															<a href="javascript:void(0);" class="kt-widget11__title">Quantity Done</a>
														</td>
														<td style="width: 5%;">
															<a href="javascript:void(0);" class="kt-widget11__title">Action</a>
														</td>
													</tr>
												</tbody>

												<!--end::Thead-->

												<!--begin::Tbody-->
												<tbody>
													<?php foreach ($product_details as $single_production_details) {?>
													<tr>
														<td>
															<a href="javascript:void(0);" class="kt-widget11__title"><?php echo $single_production_details['description']; ?></a>
														</td>
														<td>
															<a href="javascript:void(0);" class="kt-widget11__title"><?php echo $single_production_details['product']; ?></a>
														</td>
														<td>
															<a href="javascript:void(0);" class="kt-widget11__title"><?php echo $single_production_details['material']; ?></a>
														</td>
														<td>
															<a href="javascript:void(0);" class="kt-widget11__title"><?php echo $single_production_details['quantity']; ?></a>
														</td>
														<td class="">
															<div class="row">
																<div class="col-lg-12 col-md-9 col-sm-12">
																	<select class="form-control handled_by_select_picker" name="production_status_<?php echo $single_production_details['quotation_dtl_id'];?>" id="<?php echo $single_production_details['production_status'],'_',$single_production_details['quotation_dtl_id']; ?>">
																			<option 
																				value="pending_order"
																				data-content="<span class='kt-badge kt-badge--dark kt-badge--inline kt-badge--rounded kt-font-bolder'>Pending Order</span>"
																				<?php echo ($single_production_details['production_status']== 'pending_order')?'selected':''?>>
																					Pending Order
																			</option>
																			<option 
																				value="semi_ready"
																				data-content="<span class='kt-badge kt-badge--primary kt-badge--inline kt-badge--rounded kt-font-bolder'>Semi Ready</span>"
																				<?php echo ($single_production_details['production_status']== 'semi_ready')?'selected':''?>>
																					Semi Ready
																			</option>
																			<option 
																				value="ready_to_dispatch_order"
																				data-content="<span class='kt-badge kt-badge--primary kt-badge--inline kt-badge--rounded kt-font-bolder'>Ready For Dispatch</span>"
																				<?php echo ($single_production_details['production_status']== 'ready_to_dispatch_order')?'selected':''?>>
																					Ready For Dispatch
																			</option>
																			<option 
																				value="dispatch_order"
																				data-content="<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--rounded kt-font-bolder'>Dispatch Order</span>"
																				<?php echo ($single_production_details['production_status']== 'dispatch_order')?'selected':''?>>
																					Dispatch Order
																			</option>
																			<option 
																				value="on_hold_order"
																				data-content="<span class='kt-badge kt-badge--gainsboro kt-badge--inline kt-badge--rounded kt-font-bolder'>Order On Hold</span>"
																				<?php echo ($single_production_details['production_status']== 'on_hold_order')?'selected':''?>>
																					Order On Hold
																			</option>
																			<option 
																				value="order_cancelled"
																				data-content="<span class='kt-badge kt-badge--danger kt-badge--inline kt-badge--rounded kt-font-bolder'>Order Cancelled</span>"
																				<?php echo ($single_production_details['production_status']== 'order_cancelled')?'selected':''?>>
																					Order Cancelled
																			</option>
																	</select>
																</div>
															</div>
														</td>
														<td>
															<input type="number" class="kt-widget11__title form-control" value="<?php echo trim($single_production_details['production_quantity_done']);?>" name="production_quantity_<?php echo $single_production_details['quotation_dtl_id'];?>">
															<?php if((int)$single_production_details['quantity'] > (int)$single_production_details['production_quantity_done']) {?>
																<div><hr></div>
																<span class='kt-badge kt-badge--warning kt-badge--inline kt-badge--rounded kt-font-bolder'>
																	<?php echo (((int)$single_production_details['quantity'])-((int)$single_production_details['production_quantity_done']));?>
																		Quantity Pending
																</span>
															<?php } ?>
														</td>
														<td>
															<button type="button" class="btn btn-primary btn-icon update_product_status" max_value="<?php echo $single_production_details['quantity'];?>" production_status_id = "<?php echo $single_production_details['production_status'],'_',$single_production_details['quotation_dtl_id']; ?>" quotation_dtl_id="<?php echo $single_production_details['quotation_dtl_id'];?>"><i class="fa fa-check"></i></button>
														</td>
													</tr>
													<?php } ?>
												</tbody>

												<!--end::Tbody-->
											</table>

											<!--end::Table-->
										</div>
									</div>
								</div>
							<?php $tab_item=false;?>
							<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!--end:: Widgets/Personal Income-->
		</div>
	</div>
</div>

<!-- end:: Content -->