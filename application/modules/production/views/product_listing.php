<style type="text/css">
	.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
		padding-top: 4% !important;
	}
	.layer-white{
	    display: none;
	    position: fixed;
	    top: 0em !important;
	    left: 0em !important;
	    width: 100%;
	    height: 100%;
	    text-align: center;
	    vertical-align: middle;
	    background-color: rgba(255, 255, 255, 0.55);
	    opacity: 1;
	    line-height: 1;
	    -webkit-animation-fill-mode: both;
	    animation-fill-mode: both;
	    -webkit-animation-duration: 0.5s;
	    animation-duration: 0.5s;
	    -webkit-transition: background-color 0.5s linear;
	    transition: background-color 0.5s linear;
	    -webkit-user-select: none;
	    -moz-user-select: none;
	    -ms-user-select: none;
	    user-select: none;
	    will-change: opacity;
	    z-index: 9;
	}
	.div-loader{
	    position: absolute;
		top: 50%;
		left: 50%;
		margin: 0px;
		text-align: center;
		z-index: 1000;
		-webkit-transform: translateX(-50%) translateY(-50%);
		transform: translateX(-50%) translateY(-50%);
	}
	.kt-spinner:before {
	    width: 50px;
	    height: 50px;
	    margin-top: -10px;
	}
	tbody#production_listing tr:hover {
		background-color: gainsboro;
	}
	tbody#production_listing tr:hover .first_div {
	    /* border-left: 0.25rem solid #5578eb !important; */
	}
	tbody#production_listing td {
		font-style: normal;
		font-size: 15px;
	    border: 0.05rem solid gainsboro;
	}
	tbody#production_listing td span{
		/*width:200px !important;*/
		display: inline-flex;
	    width: 100%;
	}
	tbody#production_listing td span i{
		cursor: pointer;
	}
	tbody#production_listing td span abbr{
	    width: 100%;
	}    
	tbody#production_listing td span abbr em{
		display: block;
		color: #898989;
		margin: 3px 0 3px 0;
		font-style: normal;
		font-family: Helvetica Neue,Helvetica,Arial,sans-serif
	}
	tbody#production_listing td span abbr em i{
		font-weight: 500;
	    color: #898989;
	    margin: 3px 0 3px 0;
	    font-style: normal;
	    font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
	}
	thead#production_header tr th{
	    background: gainsboro;
	    color: #767676;
	    font-size: 14px;
	    font-family: 'latomedium';
	    padding: 10px 20px;
	    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
	}
	ul.menu-tab {
		margin: 0px;
	    padding: 0px;
	    border: 1px solid #E7E7E7;
	    /*border-radius: 50px;*/
	    overflow: hidden;
	    font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
	    font-size: 14px;
	    line-height: 20px;
	    color: #333;
	}
	ul.menu-tab li{
		width: <?php echo $ul_menu_tab_li_width; ?>;
	    display: inline;
	    text-align: center;
	    float: left;
	}
	ul.menu-tab li.active_list{
		
		background-color: dimgray;
		color: antiquewhite;
	    border: 0.25rem solid #767676 !important;
	}
	ul.menu-tab li.active_list:hover{

		color: #333;
	}
	ul.menu-tab li a{
		cursor: pointer;
	    display: inline-block;
	    outline: none;
	    text-align: center;
	    width: 100%;
	    background: #F5F5F5;
	    /*border-right: 2px solid #fff;*/
	    color: #767676;
	    font-size: 15px;
	    font-family: 'latomedium';
	    background-color: gainsboro;
	    padding: 4px 0px;
	}
	ul.menu-tab li a i{
		display: inline-block;
	    width: 35px;
	    height: 28px;
	    font-style: normal;
	    background-size: 100%;
	    position: relative;
	    top: 6px;
	}
	.form_name{
	    color: #767676 !important;
	    font-size: 14px !important;
	    font-family: 'latomedium' !important;
	    padding: 10px 10px !important; 
	    font-weight: bold !important;
	}
	.kt-badge--gainsboro, .kt-font-gainsboro{
		color: SlateGrey;
	}
	.kt-badge--Salmon, .kt-font-Salmon{
		color: Salmon;
	}
	.kt-badge--darkkhaki, .kt-font-darkkhaki{
		color: darkkhaki;
	}
	.production_title_name{
		font-weight: 700 !important;
		font-size: 1.5rem;
		display: block;
		color: #898989;
		margin: 3px 0 3px 0;
		font-style: normal;
		font-family: Helvetica Neue,Helvetica,Arial,sans-serif
	}
	.production_title_value{
		font-weight: 500;
		font-size: 1.75rem;
	    color: #898989;
	    margin: 3px 0 3px 0;
	    font-style: normal;
	    font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
	}
	.task_management_font {

		font-family: Poppins, Helvetica, sans-serif !important;
		-webkit-font-smoothing: antialiased !important;
		font-size: 1.2rem !important;
		font-weight: 500 !important;
		line-height: 1.5rem !important;
		transition: color 0.3s ease !important;
		color: #000 !important;
	}
	.tab{

		border-bottom: 0.25rem solid rgba(0, 0, 0, 0.1) !important;
		color: #646c9a !important;
		font-family: Montserrat !important;
		font-weight: bold !important;
	}
	.tab.active{

		border-bottom: 0.25rem solid #5d78ff !important;
		color: #1a73e8 !important;
		background-color: #fff !important;
		font-weight: bold !important;
	}
	.sidebar_image{
		position: relative;
		background-color: #ccc;
		display: block;
		margin-bottom: 8px;
		width: 100%;
		border-radius: 12px;
		cursor: pointer;
	}
	.sidebar_image.active{

		border: 4px solid #6c757d;
	}
</style>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div id="invoice_table_loader" class="layer-white">
		<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
	</div>
	<div class="row">	
		<div class="col-xl-12">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
					<?php if($this->session->userdata('production_access')['production_status_wise_count']){?>
						<h3 class="production_title_name" style="font-size: 14px;">
							Total :
							<span class="production_title_name">
								<?php echo $currency_list[3];?> :
								<em class="production_title_value all_total">
									<?php echo $total_of_3_order,'(',$total_of_3_order_number,')';?>
								</em>
							</span>
						</h3>
						<div class="production_client_wise_count">
							<div class="kt-section kt-section--last">
								<a href="javascript:;" class="btn btn-label-brand btn-sm btn-bold view_all_details" order_type="all_order" action_value="show" style="background-color: gainsboro; border-color: gainsboro; color: #767676; font-weight: 800;"><i class="kt-font-dark flaticon2-line-chart"></i>View Details</a>
							</div>
						</div>
					<?php } ?>
					</div>
					<div class="kt-portlet__head-toolbar">
						<div class="kt-portlet__head-wrapper">
							<div class="kt-portlet__head-actions">
								<?php if($this->session->userdata('production_access')['production_list_search_filter_access']){?>
									<a href="javascript:;" class="btn btn-brand btn-elevate btn-icon-sm add_production_listing_search_filter_form" action_value="show" style="background-color: gainsboro; border-color: gainsboro; color: #767676; font-weight: 800;">
										Add Search Filter
									</a>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>
	<?php if($this->session->userdata('production_access')['production_status_wise_count']){?>
	<div class="row status_wise_count" >
		<?php $this->load->view('production/status'); ?>
	</div>
	<?php }?>
	<div class="row production_order_wise_graph_div" style="display:none;">
		<div class="col-xl-12">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__body" style="padding: 1% 1% 0% 1%;">
	        		<div >
						<div class="kt-scroll ps ps--active-x ps--active-y" data-scroll="true" data-scroll-x="true" style="height: 400px; overflow: hidden;" id="production_status_graph"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row production_client_wise_graph_div" style="display:none;">
		<div class="col-xl-12">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__body" style="padding: 1% 1% 0% 1%;">
					<div>
						<div class="kt-scroll ps ps--active-x ps--active-y" data-scroll="true" data-scroll-x="true" style="height: 400px; overflow: hidden;" id="production_client_graph"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row production_status_and_product_family_wise_graph_div" style="display:none;">
		<div class="col-xl-6">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__body" style="padding: 1% 1% 0% 1%;">
					<div>
						<div class="kt-scroll ps ps--active-x ps--active-y" data-scroll="true" data-scroll-x="true" style="height: 400px; overflow: hidden;" id="production_product_family_graph"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-6">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__body" style="padding: 1% 1% 0% 1%;">
					<div>
						<div class="kt-scroll ps ps--active-x ps--active-y" data-scroll="true" data-scroll-x="true" style="height: 400px; overflow: hidden;" id="production_product_status_graph"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xl-12">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__body production_listing_search_filter" style="display: none; padding: 1% 1% 0% 1%;">
					<?php 
						$this->load->view('production/product_list_search_filter_form');
					?>
				</div>
			</div>
		</div>
	</div>
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__body" style="padding: 15px 25px 0px 25px;">
			<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
				<div class="row">
					<div class="col-sm-12">
						<ul class="menu-tab kt-font-bolder" style="">
							<?php if(in_array('yta', $this->session->userdata('production_access')['production_list_tab_access'])){ ?>
								<li class="active_list">
									<a>
										<i class="custom_heading_icon active_issues_tabs_icon">
											<img src="assets/media/icons/svg/Tools/Angle Grinder.svg"/>
										</i>
										<span style="top: 6px;position: relative;">
											Y T A (<?php echo $yta_tab_count ?? 0; ?>)
										</span>
									</a>
								</li>
							<?php } ?>
							<?php if(in_array('all', $this->session->userdata('production_access')['production_list_tab_access'])){ ?>
								<li class="tab_name_click all_order <?php echo $all_tab_active;?>" tab-name="all_order">
									<a>
										<i class="custom_heading_icon active_issues_tabs_icon">
											<img src="assets/media/icons/svg/Tools/Angle Grinder.svg"/>
										</i>
										<span style="top: 6px;position: relative;">
											All (<?php echo $tab_count['all_order'] ?? 0; ?>)
										</span>
									</a>
								</li>
							<?php } ?>
							<?php if(in_array('pending', $this->session->userdata('production_access')['production_list_tab_access'])){ ?>
								<li class="tab_name_click pending_order <?php echo $pending_tab_active;?>" tab-name="pending_order">
									<a>
										<i class="custom_heading_icon active_issues_tabs_icon">
											<img src="assets/media/icons/svg/Tools/Angle Grinder.svg"/>
										</i>
										<span style="top: 6px;position: relative;">
											Pending Order (<?php echo $tab_count['pending_order'] ?? 0; ?>)
										</span>
									</a>
								</li>
							<?php } ?>
							<?php if(in_array('semi_ready', $this->session->userdata('production_access')['production_list_tab_access'])){ ?>
								<li class="tab_name_click semi_ready" tab-name="semi_ready">
									<a>
										<i class="custom_heading_icon active_issues_tabs_icon">
											<img src="assets/media/icons/svg/Tools/Tools.svg"/>
										</i>
										<span style="top: 6px;position: relative;">
											Semi Ready (<?php echo $tab_count['semi_ready'] ?? 0; ?>)
										</span>
									</a>
								</li>
							<?php } ?>
							<?php if(in_array('mtt', $this->session->userdata('production_access')['production_list_tab_access'])){ ?>
								<li class="tab_name_click mtt" tab-name="mtt">
									<a>
										<i class="custom_heading_icon active_issues_tabs_icon">
											<img src="assets/media/icons/svg/Tools/Tools.svg"/>
										</i>
										<span style="top: 6px;position: relative;">
											ICE (<?php echo $tab_count['mtt'] ?? 0; ?>)
										</span>
									</a>
								</li>
							<?php } ?>
							<?php if(in_array('import', $this->session->userdata('production_access')['production_list_tab_access'])){ ?>
								<li class="tab_name_click import" tab-name="import">
									<a>
										<i class="custom_heading_icon active_issues_tabs_icon">
											<img src="assets/media/icons/svg/Tools/Tools.svg"/>
										</i>
										<span style="top: 6px;position: relative;">
											Import (<?php echo $tab_count['import'] ?? 0; ?>)
										</span>
									</a>
								</li>
							<?php } ?>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<ul class="menu-tab kt-font-bolder" style="">
							<?php if(in_array('query', $this->session->userdata('production_access')['production_list_tab_access'])){ ?>
								<li class="tab_name_click query" tab-name="query">
									<a>
										<i class="custom_heading_icon active_issues_tabs_icon">
											<img src="assets/media/icons/svg/Tools/Tools.svg"/>
										</i>
										<span style="top: 6px;position: relative;">
											Query (<?php echo $tab_count['query'] ?? 0; ?>)
										</span>
									</a>
								</li>
							<?php } ?>
							<?php if(in_array('mtt_rfd', $this->session->userdata('production_access')['production_list_tab_access'])){ ?>
								<li class="tab_name_click mtt_rfd" tab-name="mtt_rfd">
									<a>
										<i class="custom_heading_icon active_issues_tabs_icon">
											<img src="assets/media/icons/svg/Tools/Tools.svg"/>
										</i>
										<span style="top: 6px;position: relative;">
											In Transit (<?php echo $tab_count['mtt_rfd'] ?? 0; ?>)
										</span>
									</a>
								</li>
							<?php } ?>
							<?php if(in_array('ready_to_dispatch', $this->session->userdata('production_access')['production_list_tab_access'])){ ?>
								<li class="tab_name_click ready_to_dispatch_order" tab-name="ready_to_dispatch_order">
									<a>
										<i class="custom_heading_icon active_issues_tabs_icon">
											<img src="assets/media/icons/svg/Tools/Tools.svg"/>
										</i>
										<span style="top: 6px;position: relative;">
											R F D  (<?php echo $tab_count['ready_to_dispatch_order'] ?? 0; ?>)
										</span>
									</a>
								</li>
							<?php } ?>
							<?php if(in_array('dispatch', $this->session->userdata('production_access')['production_list_tab_access'])){ ?>
								<li class="tab_name_click dispatch_order" tab-name="dispatch_order">
									<a>
										<i class="custom_heading_icon active_issues_tabs_icon">
											<img src="assets/media/icons/svg/Tools/Tools.svg"/>
										</i>
										<span style="top: 6px;position: relative;">
											Dispatch Order (<?php echo $tab_count['dispatch_order'] ?? 0; ?>)
										</span>
									</a>
								</li>
							<?php } ?>
							<?php if(in_array('on_hold', $this->session->userdata('production_access')['production_list_tab_access'])){ ?>
								<li class="tab_name_click on_hold_order" tab-name="on_hold_order">
									<a>
										<i class="custom_heading_icon active_issues_tabs_icon">
											<img src="assets/media/icons/svg/Tools/Tools.svg"/>
										</i>
										<span style="top: 6px;position: relative;">
											On Hold Order (<?php echo $tab_count['on_hold_order'] ?? 0; ?>)
										</span>
									</a>
								</li>
							<?php } ?>
							<?php if(in_array('cancel', $this->session->userdata('production_access')['production_list_tab_access'])){ ?>
								<li class="tab_name_click order_cancelled" tab-name="order_cancelled">
									<a>
										<i class="custom_heading_icon active_issues_tabs_icon">
											<img src="assets/media/icons/svg/Tools/Tools.svg"/>
										</i>
										<span style="top: 6px;position: relative;">
											Order Cancelled (<?php echo $tab_count['order_cancelled'] ?? 0; ?>)
										</span>
									</a>
								</li>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
			<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
				<div class="row">
					<div class="col-sm-12">
						<div class="kt-scroll" data-scroll="true" data-scroll-x="true" style="height: auto;">
							<div style="width: <?php echo (!in_array($this->session->userdata('role'), array('8', '18')))?'3400px':'2300px';?>;">
								<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 100%; border: 0.25rem solid gainsboro;">
									<thead id="production_header">
		          						<tr role="row">
		          							<th 
		          								class="kt-font-bolder"
		          								style=" padding: 0px 0px 0px 0px;
												    	text-align: center;"
									    	>Sr. No.</th>
		          							<th 
		          								class="kt-font-bolder"
		          								style=" padding: 0px 0px 0px 0px;
											    		text-align: center;"
								    		>Wo. No.</th>
											<th class="sorting_disabled kt-font-bolder kt-align-center" style="">Actions</th>
											<th 
												class=" sorting_search kt-font-bolder kt-align-center"
												sorting_name="invoice_mst.invoice_date"
												sorting_value=""
												colspan="2" 
												style=" padding: 0% 0% 0% 15px;">
												Proforma Details<span class="kt-font-bolder kt-align-right"></span>
											</th>
											<?php if(!in_array($this->session->userdata('role'), array('8', '18')) || $this->session->userdata('production_access')['production_form_data_view_access']){?>
												<?php if(!in_array($this->session->userdata('user_id'), array('354'))){?>
											<th 
												class=" sorting_search kt-font-bolder kt-align-center" 
												sorting_name="invoice_mst.invoice_date"
												sorting_value=""
												style="">
												PO Value
											</th>
											<th 
												class=" sorting_search kt-font-bolder kt-align-center" 
												sorting_name="invoice_mst.invoice_date"
												sorting_value=""
												style="">
												Payment Information
											</th>
											<?php } } ?>
											<th 
												class=" sorting_search kt-font-bolder kt-align-center" 
												sorting_name="invoice_mst.invoice_date"
												sorting_value=""
												colspan="<?php echo (!in_array($this->session->userdata('role'), array('8'))) ? 2:1; ?>" 
												style="">
												Other Details
											</th>
											<th 
												class=" sorting_search kt-font-bolder kt-align-center" 
												sorting_name="invoice_mst.invoice_date"
												sorting_value=""
												style="">
												Handled By
											</th>
											<th 
												class=" sorting_search kt-font-bolder kt-align-center" 
												sorting_name="invoice_mst.invoice_date"
												sorting_value=""
												style="">
												Procurement Person
											</th>
											<?php if(!in_array($this->session->userdata('role'), array('8', '18'))){?>
											<th 
												class=" sorting_search kt-font-bolder kt-align-center" 
												sorting_name="invoice_mst.invoice_date"
												sorting_value=""
												style="">
												Item
											</th>
											<?php } ?>
											<?php if(!in_array($this->session->userdata('role'), array('5'))){?>
											<th 
												class="sorting_disabled kt-font-bolder kt-align-center kt-hidden"
												style="">
												Vendor PO
											</th>
											<?php } ?>
										</tr>
									</thead>
									<tbody id="production_listing">
										<?php 
										// if(!$this->session->userdata('production_access')['yta_tab_access']){

										// 	$this->load->view('production/production_list_body');
										// }else{
											
										// 	$this->load->view('production/production_list_body_yta');
										// } 
											if(in_array('yta', $this->session->userdata('production_access')['production_list_tab_access'])){

												$this->load->view('production/production_list_body_yta');
											}else{

												$this->load->view('production/production_list_body');
											}
										?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- <div id="invoice_table_loader" class="layer-white">
							<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
						</div> -->
					</div>
				</div>
				<div class="row" id="product_list_paggination" style="padding: 25px 1px 1px 1px;">
					<?php $this->load->view('production/product_list_paggination'); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- end:: Content -->
<!--Begin:: Chat-->
<div class="modal fade- modal-sticky-bottom-right" id="kt_chat_modal" role="dialog" data-backdrop="false" style="max-width: 500px !important;">
	<div class="modal-dialog" role="document" style="max-width: 500px !important;">
		<div class="modal-content">
			<div class="kt-chat">
				<div class="kt-portlet kt-portlet--last">
					<div class="kt-portlet__head">
						<div class="kt-chat__head ">
							<div class="kt-chat__left">
								<div class="kt-chat__label">
									<a href="#" class="kt-chat__title">
										<em class= "wo_no_for_query"></em>
									</a>
									<span class="kt-chat__status">
										Work Order No
									</span>
								</div>
							</div>
							<div class="kt-chat__right"></div>
						</div>
					</div>
					<div class="kt-portlet__body">
						<div class="kt-scroll kt-scroll--pull" data-height="410" data-mobile-height="225">
							<div class="layer-white chat_loader">
								<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
							</div>
							<div class="kt-chat__messages kt-chat__messages--solid sales_query_history"></div>
						</div>
					</div>
					<div class="kt-portlet__foot">
						<form id="production_query_add_form">
							<div class="kt-chat__input">
								<div class="kt-chat__editor">
									<input type="text" class="form-control" name="query_type" value="production_query" hidden />
									<input type="text" class="form-control" name="query_reference" value="" hidden />
									<input type="text" class="form-control" name="query_reference_id" value="" hidden />
									<textarea class="kt-font-dark" style="height: 50px" placeholder="Type here..." name="query_text"></textarea>
									<input type="text" class="form-control" name="query_creator_id" value="" hidden />
									<input type="text" class="form-control" name="query_assigned_id" value="" hidden />
									<input type="text" class="form-control" name="query_status" value="open" hidden />
								</div>
								<div class="kt-chat__toolbar">
									<div class="kt_chat__tools">
										<div class="query_type"></div>
									</div>
									<div class="kt_chat__actions">
										<button type="button" class="btn btn-brand btn-md btn-font-sm btn-upper btn-bold add_production_query">reply</button>
										<button type="button" class="btn btn-brand btn-md btn-font-sm btn-upper btn-bold" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--ENd:: Chat-->
<!--begin::Modal-->
<div class="modal fade" id="mtc_upload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">MTC PDF</h5>
				</div>
				<div class="modal-body">
					<div class="form-group form-group-last row kt-hidden">
						<label class="col-lg-3 col-form-label">Upload Files:</label>
						<div class="col-lg-9">
							<div class="dropzone dropzone-multi" id="kt_dropzone_4">
								<div class="dropzone-panel">
									<a class="dropzone-select btn btn-label-brand btn-bold btn-sm">Attach files</a>
									<a class="dropzone-upload btn btn-label-brand btn-bold btn-sm">Upload All</a>
									<a class="dropzone-remove-all btn btn-label-brand btn-bold btn-sm">Remove All</a>
								</div>
								<div class="dropzone-items">
									<div class="dropzone-item" style="display:none">
										<div class="dropzone-file">
											<div class="dropzone-filename" title="some_image_file_name.jpg"><span data-dz-name>some_image_file_name.jpg</span> <strong>(<span  data-dz-size>340kb</span>)</strong></div>
											<div class="dropzone-error" data-dz-errormessage></div>
										</div>
										<div class="dropzone-progress">
											<div class="progress">
												<div class="progress-bar kt-bg-brand" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress></div>
											</div>
										</div>
										<div class="dropzone-toolbar">
											<span class="dropzone-start"><i class="flaticon2-arrow"></i></span>
											<span class="dropzone-cancel" data-dz-remove style="display: none;"><i class="flaticon2-cross"></i></span>
											<span class="dropzone-delete" data-dz-remove><i class="flaticon2-cross"></i></span>
										</div>
									</div>
								</div>
							</div>
							<span class="form-text text-muted">Max file size is 1MB and max number of files is 5.</span>
						</div>
					</div>
					<h4>MTC PDF</h4>
					<div id="production_mtc_pdf_history" style="padding: 0% 10% 0% 10%;">

					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger mtc_upload_close" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<!--end::Modal-->
</div>

<!--begin::Modal-->
<div class="modal fade" id="rating_comment_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add Rating & Comment</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<div class="kt-portlet__body rating_html" >
					
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary save_star_rating" id="">Save</button>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="kt_modal_4_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content" style="background-color: #f4f4f7;">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body row" style="padding: 25px; width: 100%; margin: 0px;">
				<div class="col-lg-12" style="padding: 0px;">	
					<div class="kt-portlet ">
						<div class="kt-portlet__body kt-portlet__body--fit-y" style="padding:0px;">
							<!--begin::Widget -->
							<div class="kt-widget kt-widget--user-profile-1" style="padding-bottom: 0px;">
								<div class="kt-widget__body">
									<div class="kt-widget__items row" style="padding: 15px 15px 5px 15px;">
										<a href="javascript:void(0);" class="kt-widget__item get_file_upload_history_tab_wise" type_name="drawing" work_order_no="0" file_type="vendor" style="padding: 10px 14px;">
											<span class="kt-widget__section">
												<span class="kt-widget__icon">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<polygon points="0 0 24 0 24 24 0 24" />
															<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero" />
															<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3" />
														</g>
													</svg>
												</span>
												<span class="kt-widget__desc">
													Drawing
												</span>
											</span>
										</a>
										<a href="javascript:void(0);" class="kt-widget__item get_file_upload_history_tab_wise" type_name="marking" work_order_no="0" file_type="marking" style="padding: 10px 14px;">
											<span class="kt-widget__section">
												<span class="kt-widget__icon">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<polygon points="0 0 24 0 24 24 0 24" />
															<path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
															<path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
														</g>
													</svg>
												</span>
												<span class="kt-widget__desc">
													Marking
												</span>
											</span>
										</a>
										<a href="javascript:void(0);" class="kt-widget__item get_file_upload_history_tab_wise" type_name="mtc" work_order_no="0" file_type="mtc" style="padding: 10px 14px;">
											<span class="kt-widget__section">
												<span class="kt-widget__icon">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<rect x="0" y="0" width="24" height="24" />
															<path d="M2.56066017,10.6819805 L4.68198052,8.56066017 C5.26776695,7.97487373 6.21751442,7.97487373 6.80330086,8.56066017 L8.9246212,10.6819805 C9.51040764,11.267767 9.51040764,12.2175144 8.9246212,12.8033009 L6.80330086,14.9246212 C6.21751442,15.5104076 5.26776695,15.5104076 4.68198052,14.9246212 L2.56066017,12.8033009 C1.97487373,12.2175144 1.97487373,11.267767 2.56066017,10.6819805 Z M14.5606602,10.6819805 L16.6819805,8.56066017 C17.267767,7.97487373 18.2175144,7.97487373 18.8033009,8.56066017 L20.9246212,10.6819805 C21.5104076,11.267767 21.5104076,12.2175144 20.9246212,12.8033009 L18.8033009,14.9246212 C18.2175144,15.5104076 17.267767,15.5104076 16.6819805,14.9246212 L14.5606602,12.8033009 C13.9748737,12.2175144 13.9748737,11.267767 14.5606602,10.6819805 Z" fill="#000000" opacity="0.3" />
															<path d="M8.56066017,16.6819805 L10.6819805,14.5606602 C11.267767,13.9748737 12.2175144,13.9748737 12.8033009,14.5606602 L14.9246212,16.6819805 C15.5104076,17.267767 15.5104076,18.2175144 14.9246212,18.8033009 L12.8033009,20.9246212 C12.2175144,21.5104076 11.267767,21.5104076 10.6819805,20.9246212 L8.56066017,18.8033009 C7.97487373,18.2175144 7.97487373,17.267767 8.56066017,16.6819805 Z M8.56066017,4.68198052 L10.6819805,2.56066017 C11.267767,1.97487373 12.2175144,1.97487373 12.8033009,2.56066017 L14.9246212,4.68198052 C15.5104076,5.26776695 15.5104076,6.21751442 14.9246212,6.80330086 L12.8033009,8.9246212 C12.2175144,9.51040764 11.267767,9.51040764 10.6819805,8.9246212 L8.56066017,6.80330086 C7.97487373,6.21751442 7.97487373,5.26776695 8.56066017,4.68198052 Z" fill="#000000" />
														</g>
													</svg>
												</span>
												<span class="kt-widget__desc">
													MTC
												</span>
											</span>
										</a>
										<a href="javascript:void(0);" class="kt-widget__item get_file_upload_history_tab_wise" type_name="logistics_weight_and_dimension" work_order_no="0" file_type="" style="padding: 10px 14px;">
											<span class="kt-widget__section">
												<span class="kt-widget__icon">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<rect x="0" y="0" width="24" height="24" />
															<path d="M4,4 L11.6314229,2.5691082 C11.8750185,2.52343403 12.1249815,2.52343403 12.3685771,2.5691082 L20,4 L20,13.2830094 C20,16.2173861 18.4883464,18.9447835 16,20.5 L12.5299989,22.6687507 C12.2057287,22.8714196 11.7942713,22.8714196 11.4700011,22.6687507 L8,20.5 C5.51165358,18.9447835 4,16.2173861 4,13.2830094 L4,4 Z" fill="#000000" opacity="0.3" />
															<path d="M12,11 C10.8954305,11 10,10.1045695 10,9 C10,7.8954305 10.8954305,7 12,7 C13.1045695,7 14,7.8954305 14,9 C14,10.1045695 13.1045695,11 12,11 Z" fill="#000000" opacity="0.3" />
															<path d="M7.00036205,16.4995035 C7.21569918,13.5165724 9.36772908,12 11.9907452,12 C14.6506758,12 16.8360465,13.4332455 16.9988413,16.5 C17.0053266,16.6221713 16.9988413,17 16.5815,17 C14.5228466,17 11.463736,17 7.4041679,17 C7.26484009,17 6.98863236,16.6619875 7.00036205,16.4995035 Z" fill="#000000" opacity="0.3" />
														</g>
													</svg>
												</span>
												<span class="kt-widget__desc">
													Logistics - Weight & Dimensions
												</span>
											</span>
										</a>
										<a href="javascript:void(0);" class="kt-widget__item get_file_upload_history_tab_wise" type_name="logistics_invoice_and_pl" work_order_no="0" file_type="invoice" style="padding: 10px 14px;">
											<span class="kt-widget__section">
												<span class="kt-widget__icon">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<rect x="0" y="0" width="24" height="24" />
															<path d="M6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,12 C19,12.5522847 18.5522847,13 18,13 L6,13 C5.44771525,13 5,12.5522847 5,12 L5,3 C5,2.44771525 5.44771525,2 6,2 Z M7.5,5 C7.22385763,5 7,5.22385763 7,5.5 C7,5.77614237 7.22385763,6 7.5,6 L13.5,6 C13.7761424,6 14,5.77614237 14,5.5 C14,5.22385763 13.7761424,5 13.5,5 L7.5,5 Z M7.5,7 C7.22385763,7 7,7.22385763 7,7.5 C7,7.77614237 7.22385763,8 7.5,8 L10.5,8 C10.7761424,8 11,7.77614237 11,7.5 C11,7.22385763 10.7761424,7 10.5,7 L7.5,7 Z" fill="#000000" opacity="0.3" />
															<path d="M3.79274528,6.57253826 L12,12.5 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 Z" fill="#000000" />
														</g>
													</svg>
												</span>
												<span class="kt-widget__desc">
													Logistics - Invoice & PL
												</span>
											</span>
										</a>
										<a href="javascript:void(0);" class="kt-widget__item get_file_upload_history_tab_wise" type_name="quality_inspection" work_order_no="0" file_type="pmi_photos" style="padding: 10px 14px;">
											<span class="kt-widget__section">
												<span class="kt-widget__icon">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<rect x="0" y="0" width="24" height="24" />
															<rect fill="#000000" x="2" y="5" width="19" height="4" rx="1" />
															<rect fill="#000000" opacity="0.3" x="2" y="11" width="19" height="10" rx="1" />
														</g>
													</svg> 
												</span>
												<span class="kt-widget__desc">
													Quality Inspection
												</span>
											</span>
										</a>
									</div>
								</div>
							</div>

							<!--end::Widget -->
						</div>
					</div>
				</div>
				<div class="col-lg-12 row">
					<div class="layer-white upload_history_loader">
						<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
					</div>
					<div class="" style="background-color: #fff !important; padding: 0px; width: 30% !important; margin-right: 2%;">
						<div class="kt-notes kt-scroll kt-scroll--pull ps ps--active-y upload_date_history" data-scroll="true" style="height: 700px; overflow: hidden;"></div>
					</div>
					<div class="" style="background-color: #fff !important; padding: 0px; width: 68% !important;">
						<div class="kt-notes kt-scroll kt-scroll--pull ps ps--active-y file_upload_history_tab_wise" data-scroll="true" style="height: 700px; overflow: hidden;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->
<!--begin::Modal-->
<div class="modal fade" id="kt_modal_4_2_new" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content" style="padding:0px 25px; border-radius: 25px;">
			<div class="modal-header" style="padding: 25px 0px 0px 10px;border-bottom: 0px solid #ebedf2;">
				<h5 class="modal-title task_management_font" id="exampleModalLabel">Work Order # <span class="work_order_no"></span></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body row">
				<div class="layer-white upload_history_loader">
					<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
				</div>
				<div class="col-xl-12 row main_tab_html"></div>
				<div class="col-xl-4" style="padding-top: 24px;">
					<div class="kt-notes kt-scroll kt-scroll--pull ps ps--active-y row sidebar_photos" data-scroll="true" style="height: 700px; overflow: hidden;"></div>					
				</div>
				<div class="col-xl-8 row"style="padding-top: 24px;">
					<div class="col-xl-12 row sub_tab_html" style="margin-left: 25px;"></div>
					<div class="col-xl-12 main_photos"style=""></div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="document_upload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Technical Documents:</h5>
			</div>
			<div class="modal-body row">
				<div class="form-group form-group-last col-lg-12 row document_upload_div" style="padding: 10px 25px;">
					<label class="col-lg-3 col-form-label kt-font-bolder kt-font-lg">Upload Document</label>
					<div class="col-lg-9">
						<div class="dropzone dropzone-multi" id="kt_dropzone_document_upload">
							<div class="dropzone-panel">
								<a class="dropzone-select btn btn-label-brand btn-bold btn-sm">Attach files</a>
								<a class="dropzone-upload btn btn-label-brand btn-bold btn-sm">Upload All</a>
								<a class="dropzone-remove-all btn btn-label-brand btn-bold btn-sm">Remove All</a>
							</div>
							<div class="dropzone-items">
								<div class="dropzone-item" style="display:none">
									<div class="dropzone-file">
										<div class="dropzone-filename" title="some_image_file_name.jpg"><span data-dz-name>some_image_file_name.jpg</span> <strong>(<span  data-dz-size>340kb</span>)</strong></div>
										<div class="dropzone-error" data-dz-errormessage></div>
									</div>
									<div class="dropzone-progress">
										<div class="progress">
											<div class="progress-bar kt-bg-brand" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress></div>
										</div>
									</div>
									<div class="dropzone-toolbar">
										<span class="dropzone-start"><i class="flaticon2-arrow"></i></span>
										<span class="dropzone-cancel" data-dz-remove style="display: none;"><i class="flaticon2-cross"></i></span>
										<span class="dropzone-delete" data-dz-remove><i class="flaticon2-cross"></i></span>
									</div>
								</div>
							</div>
						</div>
						<span class="form-text text-muted kt-font-bold">Max file size is 5MB and max number of files is 5.</span>
					</div>
				</div>

				<div class="col-lg-12" id="technical_document_history_list" style="padding: 10px 20px 10px 20px;"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger document_upload_close" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!--end::Modal-->

<!--Begin:: modal-->
<div class="modal fade modal-sticky-bottom-right kt-hidden" tabindex="-1" id="kt_chat_modal_1" role="dialog" style="max-width: 500px !important;" aria-hidden="true">
	<div class="modal-dialog" role="document" style="max-width: 500px !important;">
		<div class="modal-content">
			<div class="kt-chat">
				<div class="kt-portlet kt-portlet--last">
					<div class="kt-portlet__head">
						<div class="kt-chat__head ">
							<div class="kt-chat__left">
								<div class="kt-chat__label">
									<a href="#" class="kt-chat__title">
										<em class= "wo_no_for_query"></em>
									</a>
									<span class="kt-chat__status">
										Work Order No
									</span>
								</div>
							</div>
							<div class="kt-chat__right"></div>
						</div>
					</div>
					<div class="kt-portlet__body">
						<div class="kt-scroll kt-scroll--pull" data-height="410" data-mobile-height="225">
							<div class="layer-white chat_loader">
								<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
							</div>
							<div class="kt-chat__messages kt-chat__messages--solid osdr_query_history"></div>
						</div>
					</div>
					<div class="kt-portlet__foot">
						<form id="osdr_query_add_form">
							<div class="kt-chat__input">
								<div class="kt-chat__editor">
									<input type="text" class="form-control" name="query_type" value="osdr_query" hidden />
									<input type="text" class="form-control" name="query_reference" value="" hidden />
									<input type="text" class="form-control" name="query_reference_id" value="" hidden />
									<textarea class="kt-font-dark" style="height: 80px" placeholder="Type here..." name="query_text"></textarea>
									<input type="text" class="form-control" name="query_creator_id" value="" hidden />
									<input type="text" class="form-control" name="query_status" value="open" hidden />
								</div>
								<div class="kt-chat__toolbar">
									<div class="kt_chat__tools">
										<div class="query_type"></div>
									</div>
									<div class="kt_chat__actions">
										<button type="button" class="btn btn-brand btn-md btn-font-sm btn-upper btn-bold add_osdr_query">Send</button>
										<button type="button" class="btn btn-brand btn-md btn-font-sm btn-upper btn-bold" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--ENd:: model-->


<div class="modal fade" id="kt_modal_1_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Work Order No : <em class= "wo_no_for_query"></em></h5>
			</div>
			<div class="modal-body row">
				<div class="form-group form-group-last col-lg-5 row">
					<label class="col-lg-3 col-form-label kt-font-bolder kt-font-lg">Document</label>
					<div class="col-lg-9">
						<div class="dropzone dropzone-multi" id="kt_dropzone_osdr_document">
							<div class="dropzone-panel">
								<a class="dropzone-select btn btn-label-brand btn-bold btn-sm">Attach files</a>
								<a class="dropzone-upload btn btn-label-brand btn-bold btn-sm">Upload All</a>
								<a class="dropzone-remove-all btn btn-label-brand btn-bold btn-sm">Remove All</a>
							</div>
							<div class="dropzone-items">
								<div class="dropzone-item" style="display:none">
									<div class="dropzone-file">
										<div class="dropzone-filename" title="some_image_file_name.jpg"><span data-dz-name>some_image_file_name.jpg</span> <strong>(<span  data-dz-size>340kb</span>)</strong></div>
										<div class="dropzone-error" data-dz-errormessage></div>
									</div>
									<div class="dropzone-progress">
										<div class="progress">
											<div class="progress-bar kt-bg-brand" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress></div>
										</div>
									</div>
									<div class="dropzone-toolbar">
										<span class="dropzone-start"><i class="flaticon2-arrow"></i></span>
										<span class="dropzone-cancel" data-dz-remove style="display: none;"><i class="flaticon2-cross"></i></span>
										<span class="dropzone-delete" data-dz-remove><i class="flaticon2-cross"></i></span>
									</div>
								</div>
							</div>
						</div>
						<span class="form-text text-muted kt-font-bold">Max file size is 5MB and max number of files is 5.</span>
					</div>
					<div class="col-lg-12" id="osdr_document_history_list" style="padding: 10px 20px 10px 20px;"></div>

				</div>
				<div class="line-divider col-lg-1 row">
					<div style="border-right: 2px solid #e0e0e0; height: 100%;"></div>
				</div>
				<div class="form-group form-group-last col-lg-6 row">
					<div class="kt-chat w-100 h-100">
						<div class="kt-portlet kt-portlet--last">
							<div class="kt-portlet__body">
								<div class="kt-scroll" data-scroll="true" data-height="200">
									<div class="kt-chat__messages kt-chat__messages--solid osdr_query_history"></div>
								</div>
							</div>
							<div class="kt-portlet__foot">
								<form id="osdr_query_add_form">
									<div class="kt-chat__input">
										<div class="kt-chat__editor">
											<input type="text" class="form-control" name="query_type" value="osdr_query" hidden />
											<input type="text" class="form-control" name="query_reference" value="" hidden />
											<input type="text" class="form-control" name="query_reference_id" value="" hidden />
											<textarea class="kt-font-dark" style="height: 80px" placeholder="Type here..." name="query_text"></textarea>
											<input type="text" class="form-control" name="query_creator_id" value="" hidden />
											<input type="text" class="form-control" name="query_status" value="open" hidden />
										</div>
										<div class="kt-chat__toolbar">
											<div class="kt_chat__tools">
												<div class="query_type"></div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">

				<button type="button" class="btn btn-brand btn-md btn-font-sm btn-upper btn-bold add_osdr_query">Send</button>
				<button type="button" class="btn btn-brand btn-md btn-font-sm btn-upper btn-bold" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>