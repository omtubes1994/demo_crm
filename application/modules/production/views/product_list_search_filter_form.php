<!--begin: Search Form -->
<form class="kt-form kt-form--fit kt-margin-b-20" id="production_listing_search_filter_form">
	<div style="position: relative; width: 100%; padding-right: 10px; padding-left: 10px;">
		<div class="row kt-margin-b-20">
			<label class="col-lg-1 col-form-label form_name">WO #:</label>
			<div class="col-lg-2 form-group-sub">
				<input type="text" class="form-control" name="work_order_no" value="<?php echo $search_filter['work_order_no'][0];?>">
			</div>
			<?php 
				if($this->session->userdata('production_access')['production_listing_filter_client_name']){
			?>
			<label class="col-lg-1 col-form-label form_name">Client Name:</label>
			<div class="col-lg-2 form-group-sub">
				<div class="input-group">
                    <input type="text" class="form-control" id="client_name" name="search_production_client_name" value="<?php echo $search_filter['search_production_client_name']; ?>" placeholder="min 4 character">
                </div>
				<select class="form-control listing_select_picker" name="client_id" multiple data-live-search = "true">
                    <option value="">Select Name</option>
					<?php foreach ($client_list as $client_details) { ?>
						<option value="<?php echo $client_details['client_id'];?>"
						<?php echo (in_array($client_details['client_id'], $search_filter['client_id']))?'selected':''?>>
							<?php echo $client_details['name'];?>
						</option>
					<?php } ?>
                </select>
			</div>
			<?php } ?>
			<label class="col-lg-1 col-form-label form_name">Sales Person:</label>
			<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="assigned_to" multiple data-live-search="true">
					<?php foreach ($sales_person_details as $sales_person_id => $sales_person_name) { ?>
						<option value="<?php echo $sales_person_id;?>"
						<?php echo (in_array($sales_person_id, $search_filter['assigned_to']))?'selected':''?>>
							<?php echo $sales_person_name;?>
						</option>
					<?php } ?>
				</select>
			</div>
			<label class="col-lg-1 col-form-label form_name">Product Family:</label>
			<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="product_family" multiple data-live-search="true">
					<option value="piping" 
					<?php echo (in_array('piping', $search_filter['product_family']))?'selected':''?>>
						Piping
					</option>
					<option value="instrumentation"
					<?php echo (in_array('instrumentation', $search_filter['product_family']))?'selected':''?>>
						Instrumentation
					</option>
					<option value="precision"
					<?php echo (in_array('precision', $search_filter['product_family']))?'selected':''?>>
						Precision
					</option>
					<option value="tubing"
					<?php echo (in_array('tubing', $search_filter['product_family']))?'selected':''?>>
						Tubing
					</option>
					<option value="industrial"
					<?php echo (in_array('industrial', $search_filter['product_family']))?'selected':''?>>
						Industrial
					</option>
					<option value="fastener"
					<?php echo (in_array('fastener', $search_filter['product_family']))?'selected':''?>>
						Fastener
					</option>
					<option value="valve"
					<?php echo (in_array('valve', $search_filter['product_family']))?'selected':''?>>
						Valve
					</option>
				</select>
			</div>
		</div>
		<div class="row kt-margin-b-20">
			<label class="col-lg-1 col-form-label form_name">Proforma #:</label>
			<div class="col-lg-2 form-group-sub">
				<input type="text" class="form-control" name="proforma_no" value="<?php echo $search_filter['proforma_no'][0];?>">
			</div>
			<label class="col-lg-1 col-form-label form_name">PO #:</label>
			<div class="col-lg-2 form-group-sub">
				<input type="text" class="form-control" name="order_no" value="<?php echo $search_filter['order_no'][0];?>">
			</div>
			<label class="col-lg-1 col-form-label form_name">proforma Date:</label>
			<div class="col-lg-2 form-group-sub">
				<div class="kt-portlet__head-toolbar">
					<input type="text" class="form-control" value="<?php echo $search_filter['proforma_date'][0];?>" name="proforma_date" id="proforma_date" hidden/>
                    <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
						<a href="javascript:void(0);" class="btn kt-subheader__btn-daterange" id="kt_proforma_daterangepicker" data-toggle="kt-tooltip" title="Select Destrack Date" data-placement="left" style="background: rgb(0 0 0 / 10%); display: inline-flex; height: 32px !important; padding-top: 0; padding-bottom: 0; margin-top: 0.25rem; margin-bottom: 0.25rem; margin-left: 0.25rem;">
							<span class="kt-subheader__btn-daterange-title" id="kt_proforma_daterangepicker_title" style="display: inline-block;color: #959cb6;font-weight: 500;">Today</span>&nbsp;
							<span class="kt-subheader__btn-daterange-date" id="kt_proforma_daterangepicker_date" style="display: inline-block;color: #5d78ff;margin-right: 0.75rem;font-weight: 500;">Aug 16</span>
							<i class="flaticon2-calendar-1" style="color: #5d78ff; font-size: 1rem !important;"></i>
						</a>
                    </ul>
                </div>
			</div>
			<label class="col-lg-1 col-form-label form_name">Payment Terms:</label>
			<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="payment_term" multiple data-live-search="true">
					<?php foreach ($payment_term_details as $term_id => $term_value) { ?>
						<option value="<?php echo $term_id;?>"
						<?php echo (in_array($term_id, $search_filter['payment_term']))?'selected':''?>>
							<?php echo $term_value;?>
						</option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="row kt-margin-b-20">
			<label class="col-lg-1 col-form-label form_name">Payment Status #:</label>
			<div class="col-lg-2 form-group-sub">
				<input type="text" class="form-control" name="payment_status" value="<?php echo $search_filter['payment_status'][0];?>">
			</div>
			<label class="col-lg-1 col-form-label form_name">Vendor PO #:</label>
			<div class="col-lg-2 form-group-sub">
				<input type="text" class="form-control" name="vendor_po" value="<?php echo $search_filter['vendor_po'][0];?>">
			</div>
			<label class="col-lg-1 col-form-label form_name">Delivery Date:</label>
			<div class="col-lg-2 form-group-sub">
				<div class="kt-portlet__head-toolbar">
					<input type="text" class="form-control" value="<?php  echo $search_filter['delivery_date'][0];?>" name="delivery_date" id="delivery_date" hidden/>
                    <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
						<a href="javascript:void(0);" class="btn kt-subheader__btn-daterange" id="kt_dashboard_daterangepicker" data-toggle="kt-tooltip" title="Select Destrack Date" data-placement="left" style="background: rgb(0 0 0 / 10%); display: inline-flex; height: 32px !important; padding-top: 0; padding-bottom: 0; margin-top: 0.25rem; margin-bottom: 0.25rem; margin-left: 0.25rem;">
							<span class="kt-subheader__btn-daterange-title" id="kt_dashboard_daterangepicker_title" style="display: inline-block;color: #959cb6;font-weight: 500;">Today</span>&nbsp;
							<span class="kt-subheader__btn-daterange-date" id="kt_dashboard_daterangepicker_date" style="display: inline-block;color: #5d78ff;margin-right: 0.75rem;font-weight: 500;">Aug 16</span>
							<i class="flaticon2-calendar-1" style="color: #5d78ff; font-size: 1rem !important;"></i>
						</a>
                    </ul>
                </div>
			</div>
			<label class="col-lg-1 col-form-label form_name">QC Clearance:</label>
			<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="qc_clearance" multiple data-live-search="true">
					<option value="Yes"
					<?php echo (in_array('Yes', $search_filter['qc_clearance']))?'selected':''?>>Yes</option>
					<option value="No"
					<?php echo (in_array('No', $search_filter['qc_clearance']))?'selected':''?>>No</option>
				</select>
			</div>
		</div>
		<div class="row kt-margin-b-20">
			<!-- <label class="col-lg-1 col-form-label form_name">Latest Update #:</label>
			<div class="col-lg-2 form-group-sub">
				<input type="text" class="form-control" name="latest_update" value="<?php echo $search_filter['latest_update'][0];?>">
			</div>
			<label class="col-lg-1 col-form-label form_name">Special Comments #:</label>
			<div class="col-lg-2 form-group-sub">
				<input type="text" class="form-control" name="special_comment" value="<?php echo $search_filter['special_comment'][0];?>">
			</div> -->
			<label class="col-lg-1 col-form-label form_name kt-hidden">Type:</label>
			<div class="col-lg-2 form-group-sub kt-hidden">
				<select class="form-control listing_select_picker" name="type" multiple data-live-search="true">
					<option value="" 
					<?php echo (in_array('', $search_filter['type']))?'selected':''?>>ALL</option>
					<option value="om"
					<?php echo (in_array('om', $search_filter['type']))?'selected':''?>>OM</option>
					<option value="in"
					<?php echo (in_array('in', $search_filter['type']))?'selected':''?>>IN</option>
					<option value="zen"
					<?php echo (in_array('zen', $search_filter['type']))?'selected':''?>>Z</option>
				</select>
			</div>
			<label class="col-lg-1 col-form-label form_name">Handled By:</label>
			<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="handled_by" multiple>
					<?php foreach ($handle_by_users as $handle_by_users_details) { ?>
						<?php $explode_name = explode(' ', $handle_by_users_details['name']); ?>
						<option value="<?php echo $explode_name[0];?>"
						<?php echo (in_array($explode_name[0], $search_filter['handled_by']))?'selected':''?>>
							<?php echo $explode_name[0];?>
						</option>
					<?php } ?>
				</select>
			</div>
			<?php if($this->session->userdata('production_tab_name') == 'pending_order') {?>
			<label class="col-lg-1 col-form-label form_name">Status:</label>
			<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="production_status" multiple>
					<?php foreach ($production_status as $production_key => $production_details) { ?>
						<option 
							value="<?php echo $production_key;?>"
							data-content="<span class='kt-badge kt-badge--<?php echo $production_details['color'];?> kt-badge--inline kt-badge--rounded kt-font-bolder'><?php echo $production_details['name'];?></span>"
							<?php echo (in_array($production_key, $search_filter['production_status']))?'selected':''?>>
								<?php echo $production_details['name'];?>
						</option>
					<?php } ?>
				</select>
			</div>
			<?php }?>

			<label class="col-lg-1 col-form-label form_name">Sort:</label>
			<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="sort">
					<option value=""></option>
					<option value="asc" <?php echo ($search_filter['sort'][0] == 'asc')?'selected':''?>>Low To High</option>
						<option value="desc" <?php echo ($search_filter['sort'][0] == 'desc')?'selected':''?>>High To Low</option>
				</select>
			</div>

			<label class="col-lg-1 col-form-label form_name">Priority range</label>
			<div class="kt-font-info col-lg-2 col-md-3 col-sm-8">
				<div class="kt-ion-range-slider">
					<input type="hidden" class="form-control" id="rating_value" name="rating_value" value="<?php echo $search_filter['rating_value'][0];?>">
				</div>
			</div>
		</div>
		<div class="row kt-margin-b-20">			
			<label class="col-lg-1 col-form-label form_name">Approved By:</label>
			<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="work_order_sheet_approved_by" multiple>
					<?php foreach ($approved_by_list as $single_approved_by) { ?>
						<option value="<?php echo $single_approved_by['user_id'];?>"
						<?php echo (in_array($single_approved_by['user_id'], $search_filter['work_order_sheet_approved_by']))?'selected':''?>>
							<?php echo $single_approved_by['name'];?>
						</option>
					<?php } ?>
				</select>
			</div>
			<label class="col-lg-1 col-form-label form_name">Approved By Status:</label>
			<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="work_order_sheet_approved_by_status" multiple>
					<option value="revised"
					<?php echo (in_array('revised', $search_filter['work_order_sheet_approved_by_status']))?'selected':''?>>
						Revised
					</option>
					<option value="approved"
					<?php echo (in_array('approved', $search_filter['work_order_sheet_approved_by_status']))?'selected':''?>>
						Approved
					</option>
					<option value="not_approved"
					<?php echo (in_array('not_approved', $search_filter['work_order_sheet_approved_by_status']))?'selected':''?>>
						Not Approved
					</option>
				</select>
			</div>
			<div class="col-lg-4">
				
			</div>
			<div class="col-lg-2">
		        <button class="btn btn-primary btn-brand--icon production_listing_search_filter_form_submit" type="reset">
		            <span>
		                <i class="la la-search"></i>
		                <span>Search</span>
		            </span>
		        </button>
		        &nbsp;&nbsp;
		        <button class="btn btn-secondary btn-secondary--icon production_listing_search_filter_form_reset" type="reset">
		            <span>
		                <i class="la la-close"></i>
		                <span>Reset</span>
		            </span>
		        </button>
		    </div>	
		</div>
	</div>	
</form>