<style type="text/css">
	.form_name{
	    color: #767676 !important;
	    font-size: 14px !important;
	    font-family: 'latomedium' !important;
	    padding: 10px 10px !important; 
	    font-weight: bold !important;
	}
	.kt-badge--gainsboro{
		background-color: gainsboro;
	}
</style>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col">
			<div class="alert alert-light alert-elevate fade show" role="alert">
				<div class="alert-icon"><i class="flaticon-edit kt-font-brand"></i></div>
				<div class="alert-text">
					<h2 class="kt-font-info">Product Details</h2>
				</div>
				<div>
					<button type="button" class="btn btn-primary btn-wide save_production_details_form">Save Details</button>
				</div>
			</div>
		</div>
	</div>
	<!--begin::Portlet-->
	<div class="kt-portlet" style="background: #F5F5F5;">
		<div class="kt-portlet__body">
			<div class="kt-form__section kt-form__section--first">
				<!--begin::Form-->
				<form class="kt-form" id="production_details_form">
					<input type="text" name="quotation_mst_id" value="<?php echo $this->uri->segment('3');?>" hidden>
					<div class="form-group row">
						<input type="text" value="<?php echo $production_information['id'];?>" name="id" hidden>
						<label class="col-lg-1 col-form-label form_name">Work Order No:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" value="<?php echo $production_information['work_order_no'];?>" readonly>
						</div>
						<label class="col-lg-1 col-form-label form_name">Sales Person:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" value="<?php echo $users_details[$quotation_details['assigned_to']];?>" readonly>
						</div>
						<label class="col-lg-1 col-form-label form_name">Client Name:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" value="<?php echo $client_details['client_name']; ?>" readonly>
						</div>
						
						<label class="col-lg-1 col-form-label form_name">Proforma No:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" value="<?php echo $quotation_details['proforma_no'];?>" readonly>
						</div>		
					</div>
					<div class="form-group row">
						<label class="col-lg-1 col-form-label form_name">Proforma Date:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" value="<?php echo date('F, j Y', strtotime($quotation_details['confirmed_on']));?>" readonly>
						</div>
						<label class="col-lg-1 col-form-label form_name">Item:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control">
								<optgroup label="Product">
									<?php foreach ($product_list as $single_product) { ?>
										<option><?php echo $single_product?></option>
									<?php } ?>
								</optgroup>
								<optgroup label="Material">
									<?php foreach ($material_list as $single_material) { ?>
										<option><?php echo $single_material?></option>
									<?php } ?>
								</optgroup>
							</select>
						</div>
						<label class="col-lg-1 col-form-label form_name">PO Value:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" value='<?php echo $currency_details['decimal_number'],$quotation_details['grand_total'];?>' readonly>
						</div>
						<label class="col-lg-1 col-form-label form_name">Payment terms:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" value="<?php echo $payment_term_details['term_value'];?>" readonly>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-1 col-form-label form_name">Product Family:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control" name="product_family">
								<option value="">Select Product Family</option>
								<option value="piping" 
								<?php echo ($production_information['product_family'] == 'piping')?'selected':''?>>
									Piping
								</option>
								<option value="instrumentation"
								<?php echo ($production_information['product_family'] == 'instrumentation')?'selected':''?>>
									Instrumentation
								</option>
								<option value="precision"
								<?php echo ($production_information['product_family'] == 'precision')?'selected':''?>>
									Precision
								</option>
							</select>
						</div>
						<label class="col-lg-1 col-form-label form_name">Delivery Date:</label>
						<div class="col-lg-2 form-group-sub">
							<div class="input-group date">
								<input type="text" class="form-control" value="<?php echo (!empty($single_production_details['delivery_date'])) ? date('Y-m-d', strtotime($production_information['delivery_date'])): "";?>" name="delivery_date" id="delivery_date" />
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="la la-calendar"></i>
									</span>
								</div>
							</div>
						</div>
						<label class="col-lg-1 col-form-label form_name">Vendor PO:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" name="vendor_po" value="<?php echo $production_information['vendor_po'];?>">
						</div>
						<label class="col-lg-1 col-form-label form_name">Handled By:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control handled_by_select_picker" name="handled_by" multiple>
								<?php $explode_handle_by = explode(',', $production_information['handled_by']); ?>
								<?php foreach ($handle_by_users as $single_user) { ?>
									<?php $explode_name = explode(' ', $single_user['name']); ?>
									<option value="<?php echo $explode_name[0];?>"
									<?php echo (in_array($explode_name[0], $explode_handle_by))?'selected':''?>>
										<?php echo $explode_name[0];?>
									</option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-1 col-form-label form_name">Current Status:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control handled_by_select_picker" name="production_status">
								<option value="">Select Current Status</option>
								<?php foreach ($production_status as $production_key => $production_details) { ?>
									<option 
										value="<?php echo $production_key;?>"
										data-content="<span class='kt-badge kt-badge--<?php echo $production_details['color'];?> kt-badge--inline kt-badge--rounded kt-font-bolder'><?php echo $production_details['name'];?></span>"
										<?php echo ($production_information['production_status']== $production_key)?'selected':''?>>
											<?php echo $production_details['name'];?>
									</option>
								<?php }?>
							</select>
						</div>
						<label class="col-lg-1 col-form-label form_name">Payment Status:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" name="payment_status" value="<?php echo $production_information['payment_status'];?>">
						</div>
						<label class="col-lg-1 col-form-label form_name">QC Clearance:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control" name="qc_clearance">
								<option value="">Select QC Clearance</option>
								<option value="Yes"
								<?php echo ($production_information['qc_clearance'] == 'Yes')?'selected':''?>>
									Yes
								</option>
								<option value="No"
								<?php echo ($production_information['qc_clearance'] == 'No')?'selected':''?>>
									No
								</option>
							</select>
						</div>
						<label class="col-lg-1 col-form-label form_name">MTC Sent:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control" name="mtc_sent">
								<option value="">Select MTC Sent</option>
								<option value="Yes"
								<?php echo ($production_information['mtc_sent'] == 'Yes')?'selected':''?>>
									Yes
								</option>
								<option value="No"
								<?php echo ($production_information['mtc_sent'] == 'No')?'selected':''?>>
									No
								</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-1 col-form-label form_name">Latest Update:</label>
						<div class="col-lg-5 form-group-sub">
							<textarea class="form-control" id="kt_autosize_1" name="latest_update" rows="2" style="overflow: hidden; overflow-wrap: break-word; resize: none; height: 76px;"><?php echo trim($production_information['latest_update']);?></textarea>
						</div>		
						<label class="col-lg-1 col-form-label form_name">Special Comments:</label>
						<div class="col-lg-5 form-group-sub">
							<textarea class="form-control" id="kt_autosize_2" name="special_comment" rows="2" style="overflow: hidden; overflow-wrap: break-word; resize: none; height: 76px;"><?php echo trim($production_information['special_comment']);?></textarea>
						</div>		
					</div>
				</form>
    
				<!--end::Form-->
			</div>	
		</div>
	</div>
	
	<!--end::Portlet-->
</div>

<!-- end:: Content -->