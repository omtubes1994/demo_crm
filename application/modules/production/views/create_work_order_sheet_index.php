<style type="text/css">
	.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
		padding-top: 4% !important;
	}
	.layer-white{
	    display: none;
	    position: absolute;
	    top: 0em !important;
	    left: 0em !important;
	    width: 100%;
	    height: 100%;
	    text-align: center;
	    vertical-align: middle;
	    background-color: rgba(255, 255, 255, 0.55);
	    opacity: 1;
	    line-height: 1;
	    -webkit-animation-fill-mode: both;
	    animation-fill-mode: both;
	    -webkit-animation-duration: 0.5s;
	    animation-duration: 0.5s;
	    -webkit-transition: background-color 0.5s linear;
	    transition: background-color 0.5s linear;
	    -webkit-user-select: none;
	    -moz-user-select: none;
	    -ms-user-select: none;
	    user-select: none;
	    will-change: opacity;
	    z-index: 9;
	}
	.div-loader{
	    position: absolute;
		top: 50%;
		left: 50%;
		margin: 0px;
		text-align: center;
		z-index: 1000;
		-webkit-transform: translateX(-50%) translateY(-50%);
		transform: translateX(-50%) translateY(-50%);
	}
	.kt-spinner:before {
	    width: 50px;
	    height: 50px;
	    margin-top: -10px;
	}
	tbody#production_listing tr:hover {
		background-color: gainsboro;
	}
	tbody#production_listing tr:hover .first_div {
	    border-left: 0.25rem solid #5578eb !important;
	}
	tbody#production_listing td {
		font-style: normal;
		font-size: 15px;
	    border: 0.05rem solid black;
	}
	tbody#production_listing td span{
		/*width:200px !important;*/
		display: inline-flex;
	    width: 100%;
	}
	tbody#production_listing td span i{
		cursor: pointer;
	}
	tbody#production_listing td span abbr{
	    width: 100%;
	}    
	tbody#production_listing td span abbr em{
		display: block;
		color: #898989;
		margin: 3px 0 3px 0;
		font-style: normal;
		font-family: Helvetica Neue,Helvetica,Arial,sans-serif
	}
	tbody#production_listing td span abbr em i{
		font-weight: 500;
	    color: #898989;
	    margin: 3px 0 3px 0;
	    font-style: normal;
	    font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
	}
	thead#production_header tr th{
	    background: white;
	    color: black;
	    font-size: 14px;
	    font-family: 'latomedium';
	    padding: 10px 20px;
	    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
	    border: 0.05rem solid black;
	}
	ul.menu-tab {
		margin: 0px;
	    padding: 0px;
	    border: 1px solid #E7E7E7;
	    /*border-radius: 50px;*/
	    overflow: hidden;
	    font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
	    font-size: 14px;
	    line-height: 20px;
	    color: #333;
	}
	ul.menu-tab li{
		width: calc(100%/6) !important;
	    display: inline;
	    text-align: center;
	    float: left;
	}
	ul.menu-tab li.active_list{
	    border-bottom: 0.25rem solid #767676 !important;
	}
	ul.menu-tab li a{
		cursor: pointer;
	    display: inline-block;
	    outline: none;
	    text-align: center;
	    width: 100%;
	    background: #F5F5F5;
	    /*border-right: 2px solid #fff;*/
	    color: #767676;
	    font-size: 15px;
	    font-family: 'latomedium';
	    background-color: gainsboro;
	    padding: 3px 3px 12px 3px;
	}
	ul.menu-tab li a i{
		display: inline-block;
	    width: 35px;
	    height: 28px;
	    font-style: normal;
	    background-size: 100%;
	    position: relative;
	    top: 6px;
	}
	.form_name{
	    color: #767676 !important;
	    font-size: 14px !important;
	    font-family: 'latomedium' !important;
	    padding: 10px 10px !important; 
	    font-weight: bold !important;
	}
	.kt-badge--gainsboro, .kt-font-gainsboro{
		background-color: gainsboro;
	}
	.production_title_name{
		font-weight: 700 !important;
		font-size: 1.5rem;
		display: block;
		color: #898989;
		margin: 3px 0 3px 0;
		font-style: normal;
		font-family: Helvetica Neue,Helvetica,Arial,sans-serif
	}
	.production_title_value{
		font-weight: 500;
		font-size: 1.75rem;
	    color: #898989;
	    margin: 3px 0 3px 0;
	    font-style: normal;
	    font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
	}
</style>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" style="padding: 16px 16px 16px 16px;">
	<!--begin:: Portlet-->
	<div class="kt-portlet">
		<div class="layer-white">
			<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
		</div>
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title kt-font-bolder kt-font-dark">
					Create Work Order Sheet
				</h3>
			</div>
			<div class="kt-portlet__head-label">
				<div class="kt-font-bolder kt-font-dark" style="font-size: 18px;">
					<?php if($production_details['type'] == 'OM'){?>
						OM TUBES
					<?php }else if($production_details['type'] == 'zen'){?>
						ZENGINEER MFG
					<?php }else if($production_details['type'] == 'IN'){?>
						INSTINOX
					<?php }?>
				</div>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-actions">
					<a href="javascript:void(0);" class="btn-sm btn btn-label-dark btn-bold save_work_order_sheet" work_order_sheet_id = "<?php echo $work_order_sheet_id; ?>">
						Save sheet
					</a>
				</div>
			</div>
		</div>
		<div class="kt-portlet__body" style="padding:0px 5px;">
			<div class="kt-widget kt-widget--user-profile-3">
				<!--begin::Form-->
				<form class="kt-form kt-form--label-right" id="create_work_order_sheet_form">
					<div style="padding: 10px; border: 0.2rem solid black;">
						<div class="form-group form-group-last kt-hide">
							<div class="alert alert-danger" role="alert" id="kt_form_1_msg">
								<div class="alert-icon"><i class="flaticon-warning"></i></div>
								<div class="alert-text">
									Oh snap! Change a few things up and try submitting again.
								</div>
								<div class="alert-close">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="la la-close"></i></span>
									</button>
								</div>
							</div>
						</div>
						<div class="form-group row" style="margin-bottom: 16px;">
							<div class="col-lg-4 row">
								<div class="col-lg-4">
									<label class="form-control-label kt-font-bolder kt-font-dark">Work Order No</label>
								</div>
								<div class="col-lg-6">
									<input type="text" class="form-control kt-font-bolder kt-font-dark" readonly value="<?php echo $work_order_sheet_details['work_order_no']; ?>" name="work_order_no">
								</div>
							</div>
							<div class="col-lg-4 row">
								<div class="col-lg-6">
									<label class="form-control-label kt-font-bolder kt-font-dark">Completion Date</label>
								</div>
								<div class="col-lg-6">
									<input type="text" class="form-control kt-font-bolder kt-font-dark work_order_sheet_datepicker" value="<?php echo $work_order_sheet_details['completion_date']; ?>" name="completion_date"/>
								</div>
							</div>
							<div class="col-lg-4 row">
								<div class="col-lg-6">
									<label class="form-control-label kt-font-bolder kt-font-dark">Create Date</label>
								</div>
								<div class="col-lg-6">
									<input type="text" class="form-control kt-font-bolder kt-font-dark work_order_sheet_datepicker" value="<?php echo $work_order_sheet_details['create_date']; ?>" name="create_date"/>
								</div>
							</div>
						</div>
						<div class="form-group row" style="margin-bottom: 16px;">
							<div class="col-lg-4 row">
								<div class="col-lg-4">
									<label class="form-control-label kt-font-bolder kt-font-dark">Delivery</label>
								</div>
								<div class="col-lg-6">
									<select class="form-control work_order_sheet_select_picker kt-font-bolder kt-font-dark" data-size="7" data-live-search="true" name="delivery">
										<option class="kt-font-bolder kt-font-dark" value="">Select Delivery</option>
										<?php foreach($work_order_sheet_details['delivery_date_master'] as $single_delivery_details){?>
											<option class="kt-font-bolder kt-font-dark" value="<?php echo $single_delivery_details['delivery_id']; ?>" <?php echo $single_delivery_details['selected']; ?>>
												<?php echo $single_delivery_details['delivery_name']; ?>
											</option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-lg-4 row">
								<div class="col-lg-6">
									<label class="form-control-label kt-font-bolder kt-font-dark">Procurement Person</label>
								</div>
								<div class="col-lg-6">
									<select class="form-control work_order_sheet_select_picker kt-font-bolder kt-font-dark" data-size="7" data-live-search="true" name="procurement_person">
										<option class="kt-font-bolder kt-font-dark" value="">Procurement Person</option>
										<?php foreach($work_order_sheet_details['procurement_person'] as $single_procurement_details){?>
											<option  class="kt-font-bolder kt-font-dark" value="<?php echo $single_procurement_details['user_id']; ?>" <?php echo $single_procurement_details['selected']; ?>>
												<?php echo $single_procurement_details['name']; ?>
											</option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-lg-4 row">
								<div class="col-lg-6">
									<label class="form-control-label kt-font-bolder kt-font-dark">Production Person *</label>
								</div>
								<div class="col-lg-6">
									<select class="form-control work_order_sheet_select_picker" data-size="7" data-live-search="true" name="production_person">
										<option class="kt-font-bolder kt-font-dark" value="">Production Person</option>
										<?php foreach($work_order_sheet_details['production_person'] as $single_production_details){?>
											<option class="kt-font-bolder kt-font-dark" value="<?php echo $single_production_details['user_id']; ?>" <?php echo $single_production_details['selected']; ?>>
												<?php echo $single_production_details['name']; ?>
											</option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
					</div>
				</form>

				<!--end::Form-->
				<!--begin::Form-->
				<form class="kt-form kt-form--label-right" id="create_work_order_item_details_form">
					<div class="kt-scroll" data-scroll="true" data-scroll-x="true" style="height: auto;">
						<div style="width: auto;">
							<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 100%; border-left: 0.05rem solid black; border-top: 0.05rem solid black; border-right: 0.05rem solid black; border-bottom: 0.025rem solid black; margin-top: 0px !important; margin-bottom: 0px !important;">
								<thead id="production_header">
	          						<tr role="row">
	          							<th class="sorting_disabled kt-font-bolder kt-align-center" style="padding: 5px 0px 5px 0px;">
	          								<a href="javascript:;" class="btn-sm btn btn-label-dark btn-bold add_item_details" next_count_number="<?php echo (empty($work_order_sheet_item_details))?3:count($work_order_sheet_item_details)+1; ?>">
												<i class="la la-plus"></i>
											</a>
	          								Sr. No.
	          							</th>
										<th class="sorting_disabled kt-font-bolder kt-align-center" style="">PO Item Description</th>
										<th class="sorting_disabled kt-font-bolder kt-align-center" style="">WO Item Description</th>
										<th class="sorting_disabled kt-font-bolder kt-align-center" style="">PO Quantity + Unit</th>
										<th class="sorting_disabled kt-font-bolder kt-align-center" style="">WO Quantity + Unit</th>
									</tr>
								</thead>
								<tbody class="item_details" id="production_listing">
								<?php if(empty($work_order_sheet_item_details)){?>
									<tr>
										<td  style="width: 100px; padding: 0px 0px 0px 15px;">1</td>
										<td style="width: 300px; padding: 0px 0px 0px 15px;">
											<span>
										        <abbr>
										            <em class="kt-font-bolder">
										            	<textarea class="form-control work_order_sheet_autosize" name="po_item_description_1" rows="3"></textarea>
										      		</em> 
										    	</abbr>
											</span>
										</td>
										<td style="width: 300px; padding: 0px 0px 0px 15px;">
											<span>
										        <abbr>
										            <em class="kt-font-bolder">
										            	<textarea class="form-control work_order_sheet_autosize" name="wo_item_description_1" rows="3"></textarea>
										      		</em> 
										    	</abbr>
											</span>
										</td>
										<td style="width: 300px; padding: 0px 0px 0px 15px;">
											<span>
										        <abbr>
										            <em class="kt-font-bolder">
										            	<textarea class="form-control work_order_sheet_autosize" name="po_quantity_1" rows="3"></textarea>
										      		</em> 
										    	</abbr>
											</span>
										</td>
										<td style="width: 300px; padding: 0px 0px 0px 15px;">
											<span>
										        <abbr>
										            <em class="kt-font-bolder">
										            	<textarea class="form-control work_order_sheet_autosize" name="wo_quantity_1" rows="3"></textarea>
										      		</em> 
										    	</abbr>
											</span>
										</td>
									</tr>
									<tr>
										<td  style="width: 100px; padding: 0px 0px 0px 15px;">2</td>
										<td style="width: 300px; padding: 0px 0px 0px 15px;">
											<span>
										        <abbr>
										            <em class="kt-font-bolder">
										            	<textarea class="form-control work_order_sheet_autosize" name="po_item_description_2" id="" rows="3"></textarea>
										      		</em> 
										    	</abbr>
											</span>
										</td>
										<td style="width: 300px; padding: 0px 0px 0px 15px;">
											<span>
										        <abbr>
										            <em class="kt-font-bolder">
										            	<textarea class="form-control work_order_sheet_autosize" name="wo_item_description_2" id="" rows="3"></textarea>
										      		</em> 
										    	</abbr>
											</span>
										</td>
										<td style="width: 300px; padding: 0px 0px 0px 15px;">
											<span>
										        <abbr>
										            <em class="kt-font-bolder">
										            	<textarea class="form-control work_order_sheet_autosize" name="po_quantity_2" id="" rows="3"></textarea>
										      		</em> 
										    	</abbr>
											</span>
										</td>
										<td style="width: 300px; padding: 0px 0px 0px 15px;">
											<span>
										        <abbr>
										            <em class="kt-font-bolder">
										            	<textarea class="form-control work_order_sheet_autosize" name="wo_quantity_2" id="" rows="3"></textarea>
										      		</em> 
										    	</abbr>
											</span>
										</td>
									</tr>
								<?php }else {?>
									<?php foreach($work_order_sheet_item_details as $item_keys => $item_details){ ?>
										<tr>
											<td  style="width: 100px; padding: 0px 0px 0px 15px;">
												<?php echo $item_keys+1; ?>
											</td>
											<td style="width: 300px; padding: 0px 0px 0px 15px;">
												<span>
											        <abbr>
											            <em class="kt-font-bolder">
											            	<textarea class="form-control work_order_sheet_autosize" name="<?php echo 'po_item_description_',($item_keys+1); ?>" rows="3"><?php echo $item_details['po_item_description']; ?></textarea>
											      		</em> 
											    	</abbr>
												</span>
											</td>
											<td style="width: 300px; padding: 0px 0px 0px 15px;">
												<span>
											        <abbr>
											            <em class="kt-font-bolder">
											            	<textarea class="form-control work_order_sheet_autosize" name="<?php echo 'wo_item_description_',($item_keys+1); ?>" rows="3"><?php echo $item_details['wo_item_description']; ?></textarea>
											      		</em> 
											    	</abbr>
												</span>
											</td>
											<td style="width: 300px; padding: 0px 0px 0px 15px;">
												<span>
											        <abbr>
											            <em class="kt-font-bolder">
											            	<textarea class="form-control work_order_sheet_autosize" name="<?php echo 'po_quantity_',($item_keys+1); ?>" rows="3"><?php echo $item_details['po_quantity']; ?></textarea>
											      		</em> 
											    	</abbr>
												</span>
											</td>
											<td style="width: 300px; padding: 0px 0px 0px 15px;">
												<span>
											        <abbr>
											            <em class="kt-font-bolder">
											            	<textarea class="form-control work_order_sheet_autosize" name="<?php echo 'wo_quantity_',($item_keys+1); ?>" rows="3"><?php echo $item_details['wo_quantity']; ?></textarea>
											      		</em> 
											    	</abbr>
												</span>
											</td>
										</tr>
									<?php } 
								}?>
								</tbody>
							</table>
						</div>
					</div>
				</form>

				<!--end::Form-->
				<!--begin::Form-->
				<?php if(!empty($production_details['technical_document_file_and_path'])){ ?>
				<form class="kt-form kt-form--label-right" id="create_work_order_technical_document_form">
					<div class="kt-scroll" data-scroll="true" data-scroll-x="true" style="height: auto;">
						<div style="width: auto;">
							<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 100%; border-top: 0.005rem; border-left: 0.05rem solid black; border-right: 0.05rem solid black; border-bottom: 0.005rem; margin-top: 0px !important; margin-bottom: 0px !important;">
								<thead id="production_header">
									<tr role="row">
										<th class="sorting_disabled kt-font-bolder"colspan="3" style="">
											Drawings and Technical Requirements :
										</th>
									</tr>
								</thead>
								<tbody class="technical_document" id="production_listing">
									<?php foreach(json_decode($production_details['technical_document_file_and_path'], true) as $single_document_key => $single_document_details){ ?>
										<tr>
											<td width="30px; padding:0px 0px 0px 15px;">
												<span>
													<?php echo $single_document_key+1;?>
												</span>
											</td>
											<td class="kt-font-bolder" width="300px; padding:0px 0px 0px 15px;">
												<span>
													<?php echo $single_document_details['file_name'];?>
												</span>
											</td>
											<td class="kt-font-bolder" width="30px; padding:0px 0px 0px 15px;">
												<span>
													<?php echo date('M d, Y', strtotime($single_document_details['file_add_date']));?>
												</span>
											</td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</form>
				<?php } ?>

				<!--end::Form-->
				<!--begin::Form-->
				<form class="kt-form kt-form--label-right" id="create_work_order_special_notes_form">
					<div class="kt-scroll" data-scroll="true" data-scroll-x="true" style="height: auto;">
						<div style="width: auto;">
							<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 100%; border-top: 0.025rem solid black; border-left: 0.05rem solid black; border-right: 0.05rem solid black; border-bottom: 0.025rem solid black; margin-top: 0px !important; margin-bottom: 0px !important;">
								<thead id="production_header">
	          						<tr role="row">
										<th class="sorting_disabled kt-font-bolder" colspan="2" style="">
											<a href="javascript:;" class="btn-sm btn btn-label-dark btn-bold add_special_notes" next_count_number="<?php echo (empty($work_order_sheet_special_note))?4:count($work_order_sheet_special_note)+1; ?>" textarea_name="special_notes">
												<i class="la la-plus"></i>
											</a>
											Special Notes 
										</th>
									</tr>
								</thead>
								<tbody class="special_notes" id="production_listing">
								<?php if(empty($work_order_sheet_special_note)) {?>
									<tr>
										<td  style="width: 30px; padding: 0px 0px 0px 15px;">1</td>
										<td style="width: 300px; padding: 0px 0px 0px 15px;">
											<span>
										        <abbr>
										            <em class="kt-font-bolder">
										            	<textarea class="form-control work_order_sheet_autosize" name="special_notes_1" rows="1"></textarea>
										      		</em> 
										    	</abbr>
											</span>
										</td>
									</tr>
									<tr>
										<td  style="width: 30px; padding: 0px 0px 0px 15px;">2</td>
										<td style="width: 300px; padding: 0px 0px 0px 15px;">
											<span>
										        <abbr>
										            <em class="kt-font-bolder">
										            	<textarea class="form-control work_order_sheet_autosize" name="special_notes_2" rows="1"></textarea>
										      		</em> 
										    	</abbr>
											</span>
										</td>
									</tr>
									<tr>
										<td  style="width: 30px; padding: 0px 0px 0px 15px;">3</td>
										<td style="width: 300px; padding: 0px 0px 0px 15px;">
											<span>
										        <abbr>
										            <em class="kt-font-bolder">
										            	<textarea class="form-control work_order_sheet_autosize" name="special_notes_3" rows="1"></textarea>
										      		</em> 
										    	</abbr>
											</span>
										</td>
									</tr>
								<?php } else{ ?>
									<?php foreach($work_order_sheet_special_note as $special_notes_key => $special_notes_details){?>
									<tr>
										<td  style="width: 30px; padding: 0px 0px 0px 15px;">
											<?php echo $special_notes_key+1; ?>
										</td>
										<td style="width: 300px; padding: 0px 0px 0px 15px;">
											<span>
										        <abbr>
										            <em class="kt-font-bolder">
										            	<textarea class="form-control work_order_sheet_autosize" name="<?php echo 'special_notes_',$special_notes_key+1; ?>" rows="1"><?php echo $special_notes_details['notes']; ?></textarea>
										      		</em> 
										    	</abbr>
											</span>
										</td>
									</tr>
									<?php } ?>
								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</form>

				<!--end::Form-->
				<!--begin::Form-->
				<form class="kt-form kt-form--label-right" id="create_work_order_quality_notes_form">
					<div class="kt-scroll" data-scroll="true" data-scroll-x="true" style="height: auto;">
						<div style="width: auto;">
							<table class="table" style="border-top: 0.005rem solid black; border-left: 0.05rem solid black; border-right: 0.05rem solid black; border-bottom: 0.025rem solid black; margin-top: 0px !important; margin-bottom: 0px !important;">
								<thead style="">
									<tr  style="">
										<th class="kt-font-bolder kt-align-left" colspan="2"  style=" border: 0.1rem solid black;">
											<a href="javascript:;" class="btn-sm btn btn-label-dark btn-bold add_quality_notes" data-toggle="modal" data-target="#kt_modal_7" next_count_number="<?php echo count($work_order_sheet_quality_note_master)+1; ?>">
												<i class="la la-plus"></i>
											</a>
											<a href="javascript:;" class="btn-sm btn btn-label-dark btn-bold quality_notes_list" button_value = "show">
												Quality Notes List
											</a>
										</th>
									</tr>
								</thead>
								<tbody class="quality_notes" style="display: none;">
									<?php foreach($work_order_sheet_quality_note_master as $quality_key => $quality_notes_details){?>
										<tr>
											<th scope="row" style=" border: 0.1rem solid black;"><?php echo $quality_key+1; ?></th>
											<td style=" border: 0.1rem solid black;">
												<label class="kt-checkbox kt-font-bolder kt-font-dark">
													<input type="checkbox" name="<?php echo $quality_notes_details['id']?>" <?php echo $quality_notes_details['selected']?>> <?php echo $quality_notes_details['notes']?>
													<span></span>
												</label>
											</td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</form>

				<!--end::Form-->
				<!--begin::Form-->
				<form class="kt-form kt-form--label-right" id="create_work_order_marking_comment_form">
					<div class="kt-scroll" data-scroll="true" data-scroll-x="true" style="height: auto;">
						<div style="width: auto;">
							<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 100%; border-top: 0.025rem solid black; border-left: 0.05rem solid black; border-right: 0.05rem solid black; border-bottom: 0.025rem solid black; margin-top: 0px !important; margin-bottom: 0px !important;">
								<thead id="production_header">
	          						<tr role="row">
										<th class="sorting_disabled kt-font-bolder kt-align-left" colspan="2" style="">
											<a href="javascript:;" class="btn-sm btn btn-label-dark btn-bold add_marking_comment" next_count_number="<?php echo (empty($work_order_sheet_marking_comment))?4:count($work_order_sheet_marking_comment)+1; ?>" textarea_name="marking_comment">
												<i class="la la-plus"></i>
											</a>
											Marking Comment
										</th>
									</tr>
								</thead>
								<tbody class="marking_comment" id="production_listing">
									<?php if(empty($work_order_sheet_marking_comment)) {?>
									<tr>
										<td  style="width: 30px; padding: 0px 0px 0px 15px;">1</td>
										<td style="width: 300px; padding: 0px 0px 0px 15px;">
											<span>
										        <abbr>
										            <em class="kt-font-bolder">
										            	<textarea class="form-control work_order_sheet_autosize" name="marking_comment_1" rows="1"></textarea>
										      		</em> 
										    	</abbr>
											</span>
										</td>
									</tr>
									<tr>
										<td  style="width: 30px; padding: 0px 0px 0px 15px;">2</td>
										<td style="width: 300px; padding: 0px 0px 0px 15px;">
											<span>
										        <abbr>
										            <em class="kt-font-bolder">
										            	<textarea class="form-control work_order_sheet_autosize" name="marking_comment_2" rows="1"></textarea>
										      		</em> 
										    	</abbr>
											</span>
										</td>
									</tr>
									<tr>
										<td  style="width: 30px; padding: 0px 0px 0px 15px;">3</td>
										<td style="width: 300px; padding: 0px 0px 0px 15px;">
											<span>
										        <abbr>
										            <em class="kt-font-bolder">
										            	<textarea class="form-control work_order_sheet_autosize" name="marking_comment_3" rows="1"></textarea>
										      		</em> 
										    	</abbr>
											</span>
										</td>
									</tr>
									<?php } else{ ?>
									<?php foreach($work_order_sheet_marking_comment as $marking_comment_key => $marking_comment_details){?>
										<tr>
											<td  style="width: 30px; padding: 0px 0px 0px 15px;"><?php echo $marking_comment_key+1; ?></td>
											<td style="width: 300px; padding: 0px 0px 0px 15px;">
												<span>
											        <abbr>
											            <em class="kt-font-bolder">
											            	<textarea class="form-control work_order_sheet_autosize" name="<?php echo 'marking_comment_',$marking_comment_key; ?>" rows="1"><?php echo $marking_comment_details['comments'];?></textarea>
											      		</em> 
											    	</abbr>
												</span>
											</td>
										</tr>
									<?php } ?>
								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</form>

				<!--end::Form-->
				<!--begin::Form-->
				<form class="kt-form kt-form--label-right" id="create_work_order_packaging_comment_form">
					<div class="kt-scroll" data-scroll="true" data-scroll-x="true" style="height: auto;">
						<div style="width: auto;">
							<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 100%; border-top: 0.025rem solid black; border-left: 0.05rem solid black; border-right: 0.05rem solid black; border-bottom: 0.025rem solid black; margin-top: 0px !important; margin-bottom: 0px !important;">
								<thead id="production_header">
	          						<tr role="row">
										<th class="sorting_disabled kt-font-bolder kt-align-left" colspan="2" style="">
											<a href="javascript:;" class="btn-sm btn btn-label-dark btn-bold add_packaging_comment" next_count_number="<?php echo (empty($work_order_sheet_packaging_comment))?4:count($work_order_sheet_packaging_comment)+1; ?>" textarea_name="packaging_comment">
												<i class="la la-plus"></i>
											</a>
											Packaging Comment
										</th>
									</tr>
								</thead>
								<tbody class="packaging_comment" id="production_listing">
								<?php if(empty($work_order_sheet_packaging_comment)) {?>
									<tr>
										<td  style="width: 30px; padding: 0px 0px 0px 15px;">1</td>
										<td style="width: 300px; padding: 0px 0px 0px 15px;">
											<span>
										        <abbr>
										            <em class="kt-font-bolder">
										            	<textarea class="form-control work_order_sheet_autosize" name="packaging_comment_1" rows="1"></textarea>
										      		</em> 
										    	</abbr>
											</span>
										</td>
									</tr>
									<tr>
										<td  style="width: 30px; padding: 0px 0px 0px 15px;">2</td>
										<td style="width: 300px; padding: 0px 0px 0px 15px;">
											<span>
										        <abbr>
										            <em class="kt-font-bolder">
										            	<textarea class="form-control work_order_sheet_autosize" name="packaging_comment_2" rows="1"></textarea>
										      		</em> 
										    	</abbr>
											</span>
										</td>
									</tr>
									<tr>
										<td  style="width: 30px; padding: 0px 0px 0px 15px;">3</td>
										<td style="width: 300px; padding: 0px 0px 0px 15px;">
											<span>
										        <abbr>
										            <em class="kt-font-bolder">
										            	<textarea class="form-control work_order_sheet_autosize" name="packaging_comment_3" rows="1"></textarea>
										      		</em> 
										    	</abbr>
											</span>
										</td>
									</tr>
								<?php } else{ ?>
								<?php foreach($work_order_sheet_packaging_comment as $packaging_comment_key => $packaging_comment_details){?>
									<tr>
										<td  style="width: 30px; padding: 0px 0px 0px 15px;"><?php echo $packaging_comment_key+1; ?></td>
										<td style="width: 300px; padding: 0px 0px 0px 15px;">
											<span>
										        <abbr>
										            <em class="kt-font-bolder">
										            	<textarea class="form-control work_order_sheet_autosize" name="<?php echo 'packaging_comment_',$packaging_comment_key; ?>" rows="1"><?php echo $packaging_comment_details['comments'];?></textarea>
										      		</em> 
										    	</abbr>
											</span>
										</td>
									</tr>
									<?php } ?>
								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</form>

				<!--end::Form-->
				<!--begin::Form-->
				<form class="kt-form kt-form--label-right" id="create_work_order_revision_comment_form">
					<div class="kt-scroll" data-scroll="true" data-scroll-x="true" style="height: auto;">
						<div style="width: auto;">
							<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 100%; border-top: 0.025rem solid black; border-left: 0.05rem solid black; border-right: 0.05rem solid black; border-bottom: 0.025rem solid black; margin-top: 0px !important; margin-bottom: 0px !important;">
								<thead id="production_header">
	          						<tr role="row">
										<th class="sorting_disabled kt-font-bolder kt-align-left" colspan="4" style="">
											Revision Comment
										</th>
									</tr>
								</thead>
								<tbody class="revision_comment" id="production_listing">
								<?php foreach ($work_order_sheet_revision_comment as $revision_comment_key => $revision_comment_details) { ?>
									<tr>
										<td  style="width: 30px; padding: 0px 0px 0px 15px;"><?php echo $revision_comment_key+1; ?></td>
										<td style="width: 300px; padding: 0px 0px 0px 15px;">
											<span>
										        <abbr>
										            <em class="kt-font-bolder">
										            	<textarea class="form-control work_order_sheet_autosize" rows="1"><?php echo $revision_comment_details['comments'];?></textarea>
										      		</em> 
										    	</abbr>
											</span>
										</td>
										<td  style="width: 30px; padding: 0px 0px 0px 15px;" class="kt-font-bolder">
											<input type="text" value="<?php echo $revision_comment_details['user_id']; ?>" hidden>
											<?php echo (!empty($users_details[$revision_comment_details['user_id']]))?$users_details[$revision_comment_details['user_id']]:'Name Not Found!'; ?>
										</td>
										<td  style="width: 30px; padding: 0px 0px 0px 15px;" class="kt-font-bolder">
											<input type="text" value="<?php echo date('M d, Y', strtotime($revision_comment_details['add_time'])); ?>" hidden>
											<?php echo date('M d, Y', strtotime($revision_comment_details['add_time'])); ?>
										</td>
									</tr>
								<?php } ?>
								<?php
									$revision_key = (!empty($work_order_sheet_revision_comment))? count($work_order_sheet_revision_comment)+1 : 1;
								?>
									<tr>
										<td  style="width: 30px; padding: 0px 0px 0px 15px;"><?php echo $revision_key; ?></td>
										<td style="width: 300px; padding: 0px 0px 0px 15px;">
											<span>
										        <abbr>
										            <em class="kt-font-bolder">
										            	<textarea class="form-control work_order_sheet_autosize" name="revision_comment_<?php echo $revision_key; ?>" rows="1"></textarea>
										      		</em> 
										    	</abbr>
											</span>
										</td>
										<td  style="width: 30px; padding: 0px 0px 0px 15px;"class="kt-font-bolder"><?php echo $this->session->userdata('name'); ?></td>
										<td  style="width: 30px; padding: 0px 0px 0px 15px;"class="kt-font-bolder"><?php echo date('M d, Y'); ?></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</form>

				<!--end::Form-->
				<!--begin::Form-->
				<form class="kt-form kt-form--label-right" id="create_work_order_sheet_form">
					<div style="width: auto; position: relative; overflow: hidden;">
						<div class="row" style=" border-top: 0.025rem solid black; border-left: 0.9rem solid black; border-right: 1rem solid black; border-bottom: 0.20rem solid black; padding: 16px 16px;">
							<label class="col-lg-6 kt-font-bolder kt-font-dark kt-align-left">Made By:</label>
							<!-- <label class="col-lg-4 kt-font-bolder kt-font-dark kt-align-left">Checked By:</label> -->
							<label class="col-lg-6 kt-font-bolder kt-font-dark kt-align-right">Approved By:</label>
							<label class="col-lg-6 kt-font-bolder kt-font-dark kt-align-left">
								<input type="text" name="made_by" value="<?php echo $this->session->userdata('name');?>" hidden>
								<?php echo ucfirst($this->session->userdata('name')); ?>:
							</label>
							<!-- <div class="col-lg-4 kt-align-left">
								<div class="form-group">
									<div class="kt-radio-inline">
									<?php foreach($work_order_sheet_details['checked_by_person'] as $single_checked_by_details){?>
										<label class="kt-radio kt-font-bolder kt-font-dark">
											<input type="radio" name="checked_by" value="<?php echo $single_checked_by_details['user_id']; ?>"> <?php echo $single_checked_by_details['name']; ?>
											<span></span>
										</label>
									<?php } ?>
									</div>
								</div>
							</div> -->
							<div class="col-lg-6 kt-align-right">
								<div class="form-group">
									<div class="kt-radio-inline">
									<?php foreach($work_order_sheet_details['approved_by_person'] as $single_approved_by_details){?>
										<?php if($single_approved_by_details['user_id'] == $this->session->userdata('user_id')){ ?>
											<label class="kt-radio kt-font-bolder kt-font-dark">
												<input type="radio" name="approved_by" value="<?php echo $single_approved_by_details['user_id']; ?>" <?php echo $single_approved_by_details['selected'];?>><?php echo $single_approved_by_details['name'];?>
												<span></span>
											</label>
										<?php }else if($this->session->userdata('user_id') == 2){?>
											<label class="kt-radio kt-font-bolder kt-font-dark">
												<input type="radio" name="approved_by" value="<?php echo $single_approved_by_details['user_id']; ?>" <?php echo $single_approved_by_details['selected'];?>><?php echo $single_approved_by_details['name'];?>
												<span></span>
											</label>
										<?php }?>
									<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>

				<!--end::Form-->
			</div>
		</div>
		<div class="kt-portlet__foot">
			<div class="kt-portlet__foot-toolbar">
				<div class="kt-portlet__foot-actions">
					<a href="javascript:void(0);" class="btn-sm btn btn-label-dark btn-bold save_work_order_sheet" style="float:right" work_order_sheet_id = "<?php echo $work_order_sheet_id; ?>">
					Save sheet
					</a>
				</div>
			</div>
		</div>
	</div>

	<!--end:: Portlet-->

</div>
<!--begin::Modal-->
<div class="modal modal-stick-to-bottom fade" id="kt_modal_7" role="dialog" data-backdrop="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add Quality Note</h5>
				<button type="button" class="close kt-font-dark" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<!--begin::Portlet-->
				<div class="kt-portlet">
					<!--begin::Form-->
					<form class="kt-form">
						<div class="kt-portlet__body">
							<div class="kt-section kt-section--first">
								<div class="kt-section__body">
									<div class="form-group row">
										<label class="col-lg-3 col-form-label kt-font-bolder kt-font-dark">Quality Note:</label>
										<div class="col-lg-6">
											<input type="text" class="form-control kt-font-bolder kt-font-dark" name="new_quality_note">
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>

					<!--end::Form-->
				</div>

				<!--end::Portlet-->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn-sm btn btn-label-dark btn-bold" data-dismiss="modal">Close</button>
				<button type="button" class="btn-sm btn btn-label-dark btn-bold submit_add_quality_notes_form">Save changes</button>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->