<?php 
Class Production_model extends CI_Model{

	function __construct(){
		
		parent::__construct();
		// $this->db =  $this->load->database('crm_sales', true);
	}

	// public function get_production_listingz_data($where_array, $order_by_array, $limit, $offset) {

	// 	$this->db->select('
	// 		SQL_CALC_FOUND_ROWS quotation_mst.client_id, quotation_mst.quotation_mst_id,
	// 		quotation_mst.proforma_no, quotation_mst.confirmed_on,
	// 		quotation_mst.currency,	quotation_mst.grand_total,
	// 		quotation_mst.assigned_to , quotation_mst.order_no,
	// 		quotation_mst.payment_term, quotation_mst.purchase_order, quotation_mst.client_type,
	// 		production_process_information.handled_by, production_process_information.vendor_po,
	// 		production_process_information.production_status, production_process_information.product_family,
	// 		production_process_information.payment_status, production_process_information.delivery_date,
	// 		production_process_information.qc_clearance, production_process_information.latest_update,
	// 		production_process_information.special_comment, production_process_information.work_order_no,
	// 		production_process_information.id, production_process_information.mtc_sent,
	// 		production_process_information.semi_ready_count, production_process_information.ready_for_dispatch_count,
	// 		production_process_information.dispatch_count, production_process_information.on_hold_count,
	// 		production_process_information.query_count, production_process_information.mtt_rfd_count, production_process_information.mtt_count,
	// 		production_process_information.cancel_count, production_process_information.special_comment_2,
	// 		production_process_information.handled_by_new, quotation_mst.rfq_id,
	// 		production_process_information.rating_value, production_process_information.rating_comment
	// 		', FALSE);
	// 	$this->db->join('production_process_information', 'production_process_information.quotation_mst_id = quotation_mst.quotation_mst_id', 'left');
	// 	$this->db->where($where_array, null, false);
	// 	$this->db->order_by('production_process_information.id', 'desc');
	// 	$return_array['production_list'] = $this->db->get('quotation_mst', $limit, $offset)->result_array();
	// 	// echo $this->db->last_query(),"<hr>";die;
	//     $return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows;")->row_array();
	// 	return $return_array;
	// }

	public function get_production_listingz_data($where_array, $order_by_array, $limit, $offset) {

		$this->db->select('
			SQL_CALC_FOUND_ROWS quotation_mst.client_id, quotation_mst.quotation_mst_id,
			quotation_mst.proforma_no, quotation_mst.confirmed_on,
			quotation_mst.currency,	quotation_mst.grand_total, quotation_mst.assigned_to,
			quotation_mst.order_no, quotation_mst.payment_term, quotation_mst.purchase_order,
			quotation_mst.client_type, quotation_mst.freight, quotation_mst.bank_charges,
			quotation_mst.gst, quotation_mst.other_charges, quotation_mst.discount,
			production_process_information.handled_by, production_process_information.vendor_po,
			production_process_information.production_status, production_process_information.product_family,
			production_process_information.payment_status, production_process_information.delivery_date,
			production_process_information.qc_clearance, production_process_information.latest_update,
			production_process_information.special_comment, production_process_information.work_order_no,
			production_process_information.id, production_process_information.mtc_sent,
			production_process_information.osdr_document, production_process_information.actual_delivery_date,
			quotation_dtl.semi_ready_count,
			quotation_dtl.ready_to_dispatch_order_count,
			quotation_dtl.dispatch_order_count,
			quotation_dtl.on_hold_order_count,
			quotation_dtl.query_count,
			quotation_dtl.mtt_rfd_count,
			quotation_dtl.mtt_count,
			quotation_dtl.import_count,
			quotation_dtl.order_cancelled_count,
			production_process_information.special_comment_2,
			production_process_information.handled_by_new, quotation_mst.rfq_id,
			production_process_information.rating_value, production_process_information.rating_comment, production_process_information.type, production_process_information.work_order_sheet_approved_by,
			customer_mst.name, production_process_information.technical_document_file_and_path,
			production_process_information.mtc_pdf, production_process_information.work_order_sheet_approved_by_status,

			sum(((quotation_dtl.quantity - (quotation_dtl.semi_ready_count + quotation_dtl.ready_to_dispatch_order_count + quotation_dtl.dispatch_order_count + quotation_dtl.on_hold_order_count + quotation_dtl.mtt_count + quotation_dtl.import_count + quotation_dtl.query_count + quotation_dtl.mtt_rfd_count + quotation_dtl.order_cancelled_count))) * quotation_dtl.unit_price) as pending_count,

			sum(quotation_dtl.semi_ready_count * quotation_dtl.unit_price) as semi_ready_count,
			sum(if(production_process_information.production_status = "semi_ready", quotation_dtl.row_price , (quotation_dtl.semi_ready_count * quotation_dtl.unit_price))) as semi_ready_count_2,

			sum(quotation_dtl.ready_to_dispatch_order_count * quotation_dtl.unit_price) as rfd_count,
			sum(if(production_process_information.production_status = "ready_for_dispatch", quotation_dtl.row_price , (quotation_dtl.ready_to_dispatch_order_count * quotation_dtl.unit_price))) as rfd_count_2,
			
			sum(quotation_dtl.dispatch_order_count * quotation_dtl.unit_price) as dispatch_count,
			sum(if(production_process_information.production_status = "dispatched", quotation_dtl.row_price , (quotation_dtl.dispatch_order_count * quotation_dtl.unit_price))) as dispatch_count_2,

			sum(quotation_dtl.on_hold_order_count * quotation_dtl.unit_price) as hold_count,
			sum(if(production_process_information.production_status = "on_hold", quotation_dtl.row_price , (quotation_dtl.on_hold_order_count * quotation_dtl.unit_price))) as hold_count_2,

			sum(quotation_dtl.mtt_count * quotation_dtl.unit_price) as mtt_count,
			sum(if(production_process_information.production_status IN ("merchant_trade", "mtt"), quotation_dtl.row_price , (quotation_dtl.mtt_count * quotation_dtl.unit_price))) as mtt_count_2,

			sum(quotation_dtl.import_count * quotation_dtl.unit_price) as import_count,
			sum(if(production_process_information.production_status = "import", quotation_dtl.row_price , (quotation_dtl.import_count * quotation_dtl.unit_price))) as import_count_2,

			sum(quotation_dtl.query_count * quotation_dtl.unit_price) as query_count,
			sum(if(production_process_information.production_status = "query", quotation_dtl.row_price , (quotation_dtl.query_count * quotation_dtl.unit_price))) as query_count_2,

			sum(quotation_dtl.mtt_rfd_count * quotation_dtl.unit_price) as mtt_rfd_count,
			sum(if(production_process_information.production_status IN ("mtt_rfd", "in_transit_import", "in_transit_mtt"), quotation_dtl.row_price , (quotation_dtl.mtt_rfd_count * quotation_dtl.unit_price))) as mtt_rfd_count_2,

			sum(quotation_dtl.order_cancelled_count * quotation_dtl.unit_price) as cancel_count,
			sum(if(production_process_information.production_status = "order_cancelled", quotation_dtl.row_price , (quotation_dtl.order_cancelled_count * quotation_dtl.unit_price))) as cancel_count_2,
			', FALSE);
		$this->db->join('production_process_information', 'production_process_information.quotation_mst_id = quotation_mst.quotation_mst_id', 'inner');
		$this->db->join('customer_mst', 'customer_mst.id = quotation_mst.client_id', 'left');
		$this->db->join('quotation_dtl', 'quotation_dtl.quotation_mst_id = quotation_mst.quotation_mst_id', 'inner');
		$this->db->where($where_array, null, false);
		// $this->db->where_in("production_process_information.work_order_no", array(2696,2580,2350,2309,2236,2142,2041,1780,1544,1704,1705));
		$this->db->group_by('quotation_mst.quotation_mst_id');
		$static_tab_name_to_having_array = array(
			'pending_order'=> 'pending_count > 0',
			'semi_ready'=> 'semi_ready_count > 0 OR semi_ready_count_2 > 0',
			'mtt'=> 'mtt_count > 0 OR mtt_count_2 > 0',
			'import'=> 'import_count > 0 OR import_count_2 > 0',
			'query'=> 'query_count > 0 OR query_count_2 > 0',
			'mtt_rfd'=> 'mtt_rfd_count > 0 OR mtt_rfd_count_2 > 0',
			'ready_to_dispatch_order'=> 'rfd_count > 0 OR rfd_count_2 > 0',
			'dispatch_order'=> 'dispatch_count > 0 OR dispatch_count_2 > 0',
			'on_hold_order'=> 'hold_count > 0 OR hold_count_2 > 0',
			'order_cancelled'=> 'cancel_count > 0 OR cancel_count_2 > 0',
		);
		if(isset($static_tab_name_to_having_array[$this->session->userdata('production_tab_name')])){

			$this->db->having($static_tab_name_to_having_array[$this->session->userdata('production_tab_name')]);
		}
		
		$this->db->order_by('production_process_information.id', 'desc');
		$return_array['production_list'] = $this->db->get('quotation_mst', $limit, $offset)->result_array();
		
		// echo $this->db->last_query(),"<hr>";die;
	    $return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows;")->row_array();
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		return $return_array;
	}

	public function get_production_listingz_data_for_first_tab($where_string, $order_by_array) {

		$this->db->select('quotation_mst.client_id, quotation_mst.quotation_mst_id,
			quotation_mst.proforma_no, quotation_mst.confirmed_on, quotation_mst.currency,
			quotation_mst.grand_total, quotation_mst.assigned_to sales_person_id, quotation_mst.order_no,
			quotation_mst.payment_term, quotation_mst.purchase_order, quotation_mst.rfq_id, quotation_mst.work_order_no, quotation_mst.yet_to_acknowledge, quotation_mst.purchase_order, quotation_mst.acknowledge, rfq_mst.assigned_to, rfq_mst.assigned_to_1, rfq_mst.assigned_to_2, rfq_mst.assigned_to_3, quotation_mst.client_type');
		$this->db->join('rfq_mst', 'rfq_mst.rfq_mst_id = quotation_mst.rfq_id', 'left');
		$this->db->where($where_string, null, false);
		if(!empty($order_by_array)) {
			$this->db->order_by($order_by_array['column_name'], $order_by_array['column_value']);
		}
		$return_array['production_list'] = $this->db->get('quotation_mst')->result_array();
		// echo $this->db->last_query(),"<hr>";die;
		return $return_array;
	}

	// public function get_production_listingz_client_list_data() {

	// 	$this->db->select('quotation_mst.client_id, clients.client_name');
	// 	$this->db->join('clients', 'clients.client_id = quotation_mst.client_id', 'left');
	// 	$this->db->where(array('quotation_mst.stage'=>'Proforma', 'quotation_mst.status'=>'Won'));
	// 	$this->db->group_by('quotation_mst.client_id');
	// 	$res = $this->db->get('quotation_mst')->result_array();
	// 	return $res;
	// }

	public function get_production_listingz_client_list_data($search_production_client_name) {

		$this->db->select('quotation_mst.client_id, customer_mst.name');
		$this->db->join('customer_mst', 'customer_mst.id = quotation_mst.client_id', 'left');
		$this->db->where("quotation_mst.stage = 'Proforma' AND quotation_mst.status = 'Won' AND customer_mst.name LIKE '%".$search_production_client_name."%'", null, false);
		$this->db->group_by('quotation_mst.client_id');
		$res = $this->db->get('quotation_mst')->result_array();
		return $res;
	}

	public function production_listingz_total_revenue($where_string) {

		$this->db->select('quotation_mst.currency, sum(quotation_mst.grand_total) as total');
		$this->db->join('production_process_information', 'production_process_information.quotation_mst_id = quotation_mst.quotation_mst_id', 'left');
		$this->db->where($where_string, null, false);
		$this->db->group_by('quotation_mst.currency');
		$result = $this->db->get('quotation_mst')->result_array();
		return $result;
	}

	function get_query_history($quote_id, $query_type){
		$this->db->join('query_texts qt', 'qt.query_id = q.query_id', 'inner');
		return $this->db->get_where('query_mst q', array('q.query_for_id' => $quote_id, 'q.query_type' => $query_type))->result_array();
	}

	public function production_listingz_total_work_order_count($where_string) {

		$this->db->select('*');
		$this->db->join('production_process_information', 'production_process_information.quotation_mst_id = quotation_mst.quotation_mst_id', 'left');
		$this->db->where($where_string, null, false);
		$this->db->group_by('production_process_information.work_order_no');
		$result = $this->db->get('quotation_mst')->result_array();
		// echo "<pre>";print_r($result);echo"</pre><hr>";exit;
		return count($result);
	}	

	public function get_quotation_client_id($where_string) {

		$this->db->select('SQL_CALC_FOUND_ROWS quotation_mst.client_id, customer_mst.name', FALSE);
		$this->db->join('production_process_information', 'production_process_information.quotation_mst_id = quotation_mst.quotation_mst_id', 'INNER');
		$this->db->join('customer_mst', 'customer_mst.id = quotation_mst.client_id', 'left');
		$this->db->where($where_string, null, false);
		$this->db->group_by('client_id');
		$return['client_list'] = $this->db->get('quotation_mst')->result_array();
		// echo "<pre>";print_r($return);echo"</pre><hr>";exit;
		return $return;
	}

    public function get_total_sum_for_client($client_id, $type) {
		if (!empty($type)) {
			$typeCondition = "AND production_process_information.type = '".$type."'";
		}

		$query = ("

					SELECT
						client_id,
						name,
						currency,
						production_status,
						pending_count,
						semi_ready_count,
						rfd_count,
						mtt_count,
						import_count,
						query_count,
						mtt_rfd_count,
						(pending_count + mtt_rfd_count + import_count + query_count + mtt_count + rfd_count + semi_ready_count) as total_sum
					FROM (
						SELECT
							quotation_mst.client_id,
							quotation_mst.currency,
							customer_mst.name,
							production_process_information.production_status,

							SUM(((quotation_dtl.quantity - (quotation_dtl.semi_ready_count + quotation_dtl.ready_to_dispatch_order_count + quotation_dtl.dispatch_order_count + quotation_dtl.on_hold_order_count + quotation_dtl.mtt_count + quotation_dtl.import_count + quotation_dtl.query_count + quotation_dtl.mtt_rfd_count + quotation_dtl.order_cancelled_count))) * quotation_dtl.unit_price) as pending_count,

							sum(quotation_dtl.semi_ready_count * quotation_dtl.unit_price) as semi_ready_count,

							sum(quotation_dtl.ready_to_dispatch_order_count * quotation_dtl.unit_price) as rfd_count,

							sum(quotation_dtl.mtt_count * quotation_dtl.unit_price) as mtt_count,
							sum(quotation_dtl.import_count * quotation_dtl.unit_price) as import_count,
							sum(quotation_dtl.query_count * quotation_dtl.unit_price) as query_count,
							sum(quotation_dtl.mtt_rfd_count * quotation_dtl.unit_price) as mtt_rfd_count
						FROM
							quotation_mst
						INNER JOIN
							production_process_information ON production_process_information.quotation_mst_id = quotation_mst.quotation_mst_id
						LEFT JOIN
							`customer_mst` ON `customer_mst`.`id` = `quotation_mst`.`client_id` 
						INNER JOIN
							`quotation_dtl` ON `quotation_dtl`.`quotation_mst_id` = `quotation_mst`.`quotation_mst_id` 
						WHERE
							quotation_mst.stage = 'proforma'
							AND quotation_mst.status = 'Won'
							AND quotation_mst.work_order_no != 0
							AND (
								production_process_information.production_status IS NULL
								OR production_process_information.production_status IN ('yet_to_start_production', 'in_production', 'pending_order', 'semi_ready', 'merchant_trade', 'mtt', 'import', 'query', 'mtt_rfd', 'in_transit_import', 'in_transit_mtt', 'ready_for_dispatch')
							)
							AND (production_process_information.status IS NULL OR production_process_information.status = 'Active')
							$typeCondition
							AND quotation_mst.client_id = $client_id
						GROUP BY
							quotation_mst.currency, quotation_mst.client_id = {$client_id}
					) AS subquery

			");

		$result = $this->db->query($query)->result_array();
		// echo $this->db->last_query(),"<hr>";die;
		return (!empty($result)) ? $result : null;
    }

	public function get_product_family($where_string) {

		$this->db->select('SQL_CALC_FOUND_ROWS product_family', FALSE);
		$this->db->where($where_string, null, false);
		$this->db->group_by('product_family');
		$return['product_family_list'] = $this->db->get('production_process_information')->result_array();
		// echo "<pre>";print_r($return);echo"</pre><hr>";exit;
		return $return;
	}

	public function get_total_sum_for_product_family($product_family, $type) {
		if (!empty($type)) {
			$typeCondition = "AND production_process_information.type = '".$type."'";
		}

		$query = ("
					SELECT
						currency,
						product_family,
						pending_count,
						semi_ready_count,
						rfd_count,
						mtt_count,
						import_count,
						query_count,
						mtt_rfd_count,
						(pending_count + mtt_rfd_count + import_count + query_count + mtt_count + rfd_count + semi_ready_count) as total_sum
					FROM (
						SELECT
							quotation_mst.client_id,
							quotation_mst.currency,
							production_process_information.product_family,

							SUM(((quotation_dtl.quantity - (quotation_dtl.semi_ready_count + quotation_dtl.ready_to_dispatch_order_count + quotation_dtl.dispatch_order_count + quotation_dtl.on_hold_order_count + quotation_dtl.mtt_count + quotation_dtl.import_count + quotation_dtl.query_count + quotation_dtl.mtt_rfd_count + quotation_dtl.order_cancelled_count))) * quotation_dtl.unit_price) as pending_count,

							sum(quotation_dtl.semi_ready_count * quotation_dtl.unit_price) as semi_ready_count,

							sum(quotation_dtl.ready_to_dispatch_order_count * quotation_dtl.unit_price) as rfd_count,

							sum(quotation_dtl.mtt_count * quotation_dtl.unit_price) as mtt_count,
							sum(quotation_dtl.import_count * quotation_dtl.unit_price) as import_count,
							sum(quotation_dtl.query_count * quotation_dtl.unit_price) as query_count,
							sum(quotation_dtl.mtt_rfd_count * quotation_dtl.unit_price) as mtt_rfd_count
						FROM
							quotation_mst
						INNER JOIN
							production_process_information ON production_process_information.quotation_mst_id = quotation_mst.quotation_mst_id
						INNER JOIN
							`quotation_dtl` ON `quotation_dtl`.`quotation_mst_id` = `quotation_mst`.`quotation_mst_id`
						WHERE
							quotation_mst.stage = 'proforma'
							AND quotation_mst.status = 'Won'
							AND quotation_mst.work_order_no != 0
							AND production_process_information.product_family is not null
							AND (
								production_process_information.production_status IS NULL
								OR production_process_information.production_status IN ('yet_to_start_production', 'in_production', 'pending_order', 'semi_ready', 'merchant_trade', 'mtt', 'import', 'query', 'mtt_rfd', 'in_transit_import', 'in_transit_mtt', 'ready_for_dispatch')
							)
							AND (production_process_information.status IS NULL OR production_process_information.status = 'Active')
							$typeCondition
							AND production_process_information.product_family IN ('".$product_family."')
						GROUP BY
							quotation_mst.currency
					) AS total_sum_all
			");

		$result['product_family_details'] = $this->db->query($query)->result_array();
		// echo $this->db->last_query(),"<hr>";die;
		return $result;
    }

	public function get_production_tab_count_data($where_string, $join_string, $having_condition) {
		$query = "
			SELECT COUNT(*) as total_count
			FROM (
				SELECT quotation_mst.quotation_mst_id,

					SUM((quotation_dtl.quantity - (quotation_dtl.semi_ready_count + quotation_dtl.ready_to_dispatch_order_count + quotation_dtl.dispatch_order_count + quotation_dtl.on_hold_order_count + quotation_dtl.mtt_count + quotation_dtl.import_count + quotation_dtl.query_count + quotation_dtl.mtt_rfd_count + quotation_dtl.order_cancelled_count)) * quotation_dtl.unit_price) AS pending_count,

					SUM(quotation_dtl.semi_ready_count * quotation_dtl.unit_price) AS semi_ready_count,
					SUM(IF(production_process_information.production_status = 'semi_ready', quotation_dtl.row_price, (quotation_dtl.semi_ready_count * quotation_dtl.unit_price))) AS semi_ready_count_2,

					SUM(quotation_dtl.ready_to_dispatch_order_count * quotation_dtl.unit_price) AS rfd_count,
					SUM(IF(production_process_information.production_status = 'ready_for_dispatch', quotation_dtl.row_price, (quotation_dtl.ready_to_dispatch_order_count * quotation_dtl.unit_price))) AS rfd_count_2,

					SUM(quotation_dtl.dispatch_order_count * quotation_dtl.unit_price) AS dispatch_count,
					SUM(IF(production_process_information.production_status = 'dispatched', quotation_dtl.row_price, (quotation_dtl.dispatch_order_count * quotation_dtl.unit_price))) AS dispatch_count_2,

					SUM(quotation_dtl.on_hold_order_count * quotation_dtl.unit_price) AS hold_count,
					SUM(IF(production_process_information.production_status = 'on_hold', quotation_dtl.row_price, (quotation_dtl.on_hold_order_count * quotation_dtl.unit_price))) AS hold_count_2,

					SUM(quotation_dtl.mtt_count * quotation_dtl.unit_price) AS mtt_count,
					SUM(IF(production_process_information.production_status IN ('merchant_trade', 'mtt'), quotation_dtl.row_price, (quotation_dtl.mtt_count * quotation_dtl.unit_price))) AS mtt_count_2,

					SUM(quotation_dtl.import_count * quotation_dtl.unit_price) AS import_count,
					SUM(IF(production_process_information.production_status = 'import', quotation_dtl.row_price, (quotation_dtl.import_count * quotation_dtl.unit_price))) AS import_count_2,

					SUM(quotation_dtl.query_count * quotation_dtl.unit_price) AS query_count,
					SUM(IF(production_process_information.production_status = 'query', quotation_dtl.row_price, (quotation_dtl.query_count * quotation_dtl.unit_price))) AS query_count_2,

					SUM(quotation_dtl.mtt_rfd_count * quotation_dtl.unit_price) AS mtt_rfd_count,
					SUM(IF(production_process_information.production_status IN ('mtt_rfd', 'in_transit_import', 'in_transit_mtt'), quotation_dtl.row_price, (quotation_dtl.mtt_rfd_count * quotation_dtl.unit_price))) AS mtt_rfd_count_2,

					SUM(quotation_dtl.order_cancelled_count * quotation_dtl.unit_price) AS cancel_count,
					SUM(IF(production_process_information.production_status = 'order_cancelled', quotation_dtl.row_price, (quotation_dtl.order_cancelled_count * quotation_dtl.unit_price))) AS cancel_count_2

				FROM `quotation_mst`
				{$join_string}
				WHERE {$where_string}
				GROUP BY quotation_mst.quotation_mst_id";

		if (!empty($having_condition)) {
			$query .= " HAVING {$having_condition}";
		}
		$query .= ") AS filtered_production";

		$result = $this->db->query($query)->row_array();
		// echo "<pre>"; print_r($this->db->last_query()); echo "</pre><hr>"; exit;
		return $result['total_count'] ?? 0;
	}
}