<?php 
Class Production_model extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	public function get_production_listingz_data($where_array, $order_by_array, $limit, $offset) {

		$this->db->select('
			SQL_CALC_FOUND_ROWS quotation_mst.client_id, quotation_mst.quotation_mst_id, quotation_mst.proforma_no, 
			quotation_mst.confirmed_on, quotation_mst.currency, quotation_mst.grand_total,
			quotation_mst.assigned_to, quotation_mst.order_no, quotation_mst.payment_term, quotation_mst.purchase_order,
			production_process_information.handled_by, production_process_information.vendor_po, production_process_information.handled_by,
			production_process_information.production_status, production_process_information.product_family,
			production_process_information.payment_status, production_process_information.delivery_date,
			production_process_information.qc_clearance, production_process_information.latest_update,
			production_process_information.special_comment, production_process_information.work_order_no, production_process_information.id,
			production_process_information.mtc_sent
			', FALSE);
		$this->db->join('production_process_information', 'production_process_information.quotation_mst_id = quotation_mst.quotation_mst_id', 'left');
		$this->db->where($where_array, null, false);
		$return_array['production_list'] = $this->db->get('quotation_mst', $limit, $offset)->result_array();
	    $return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows;")->row_array();
		return $return_array;
	}
	public function get_production_listingz_client_list_data() {

		$this->db->select('quotation_mst.client_id, clients.client_name');
		$this->db->join('clients', 'clients.client_id = quotation_mst.client_id', 'left');
		$this->db->where(array('quotation_mst.stage'=>'Proforma', 'quotation_mst.status'=>'Won'));
		$this->db->group_by('quotation_mst.client_id');
		$res = $this->db->get('quotation_mst')->result_array();
		return $res;
	}

	public function production_listingz_total_revenue($where_string) {

		$this->db->select('quotation_mst.currency, sum(quotation_mst.grand_total) as total');
		$this->db->join('production_process_information', 'production_process_information.quotation_mst_id = quotation_mst.quotation_mst_id', 'left');
		$this->db->where($where_string, null, false);
		$this->db->group_by('quotation_mst.currency');
		$result = $this->db->get('quotation_mst')->result_array();
		return $result;
	}

	function get_query_history($quote_id, $query_type){
		$this->db->join('query_texts qt', 'qt.query_id = q.query_id', 'inner');
		return $this->db->get_where('query_mst q', array('q.query_for_id' => $quote_id, 'q.query_type' => $query_type))->result_array();
	}
}