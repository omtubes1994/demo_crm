<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Leads extends MX_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('user_id')){
			redirect('login', 'refresh');
			exit;
		}else{
			$access_count = $main_module_count = 0;
			if(!empty($this->session->userdata('main_module_access'))) {

				foreach ($this->session->userdata('main_module_access') as $key => $value) {
					
					if(in_array($value['module_id'], array(5, 6, 7)) && $value['status'] == 'Active') {
						$main_module_count++;
					}
				}
				if($main_module_count > 0) {

					if(!empty($this->session->userdata('lead_count'))) {
						
						foreach ($this->session->userdata('lead_count') as $key => $value) {
							
							if($value > 0) {
								$access_count++;
							}
						}
					}
				}
			}
			if($access_count == 0  && $this->session->userdata('role') != 1){
				redirect($this->session->userdata('home/dashboard'));
				exit;
			}
		}
		error_reporting(0);
		$this->load->model('leads_model');
		$this->load->model('common/common_model');
	}

	function index(){
		$this->list();
	}

	function list(){
		$data['country_str'] = $data['port_str'] = $data['exporter_name'] = $data['importer_name'] = $data['new_importer_name'] = '';
		$countries = $this->leads_model->getUniqueData('COUNTRY_OF_DESTINATION');
		foreach($countries as $country){ 
			$data['country_str'] .= '<option value="'.$country['COUNTRY_OF_DESTINATION'].'">'.ucwords(strtolower($country['COUNTRY_OF_DESTINATION'])).'</option>';
		}

		$exporter_name = $this->leads_model->getUniqueData('EXPORTER_NAME');
		foreach($exporter_name as $ename){ 
			$data['exporter_name'] .= '<option value="'.addslashes($ename['EXPORTER_NAME']).'">'.ucwords(strtolower(addslashes($ename['EXPORTER_NAME']))).'</option>';
		}

		$importer_name = $this->leads_model->getUniqueData('IMPORTER_NAME');
		foreach($importer_name as $iname){ 
			$data['importer_name'] .= '<option value="'.addslashes($iname['IMPORTER_NAME']).'">'.ucwords(strtolower(addslashes($iname['IMPORTER_NAME']))).'</option>';
		}

		$new_importer_name = $this->leads_model->getUniqueData('NEW_IMPORTER_NAME');
		foreach($new_importer_name as $niname){ 
			$data['new_importer_name'] .= '<option value="'.addslashes($niname['NEW_IMPORTER_NAME']).'">'.ucwords(strtolower(addslashes($niname['NEW_IMPORTER_NAME']))).'</option>';
		}
		$this->load->view('header', array('title' => 'Leads List'));
		$this->load->view('sidebar', array('title' => 'Leads List'));
		$this->load->view('lead_list_view', $data);
		$this->load->view('footer');
	}

	function list_data(){
		ini_set('memory_limit', -1);
		$search = array();
		foreach ($this->input->post('columns') as $key => $value) {
			$search[$key] = $this->input->post('columns')[$key]['search']['value'];
		}
		$order_by = $this->input->post('columns')[$this->input->post('order')[0]['column']]['data'];
		if($order_by == 'record_id' || $order_by == 'exporter_name'){
			$order_by = 'exporter_name';
		}
		$dir = $this->input->post('order')[0]['dir'];
		$records = $this->leads_model->getLeadsList($this->input->post('start'), $this->input->post('length'), $search, $order_by, $dir);
		$data['iTotalDisplayRecords'] = $data['iTotalRecords'] = $this->leads_model->getLeadsListCount($search);
		$data['aaData'] = $records;

		$countries = $this->leads_model->getUniqueData('COUNTRY_OF_DESTINATION', $search);
		foreach($countries as $country){ 
			$data['country_str'] .= '<option value="'.$country['COUNTRY_OF_DESTINATION'].'">'.ucwords(strtolower($country['COUNTRY_OF_DESTINATION'])).'</option>';
		}

		$exporter_name = $this->leads_model->getUniqueData('EXPORTER_NAME', $search);
		foreach($exporter_name as $ename){ 
			$data['exporter_name'] .= '<option value="'.addslashes($ename['EXPORTER_NAME']).'">'.ucwords(strtolower(addslashes($ename['EXPORTER_NAME']))).'</option>';
		}

		$importer_name = $this->leads_model->getUniqueData('IMPORTER_NAME', $search);
		foreach($importer_name as $iname){ 
			$data['importer_name'] .= '<option value="'.addslashes($iname['IMPORTER_NAME']).'">'.ucwords(strtolower(addslashes($iname['IMPORTER_NAME']))).'</option>';
		}

		$new_importer_name = $this->leads_model->getUniqueData('NEW_IMPORTER_NAME', $search);
		foreach($new_importer_name as $niname){ 
			$data['new_importer_name'] .= '<option value="'.addslashes($niname['NEW_IMPORTER_NAME']).'">'.ucwords(strtolower(addslashes($niname['NEW_IMPORTER_NAME']))).'</option>';
		}

		echo json_encode($data);
	}

	function fuzzy_exp_imp_list(){
		$data['country_str'] = $data['port_str'] = $data['exporter_name'] = $data['importer_name'] = $data['new_importer_name'] = '';
		$countries = $this->leads_model->getUniqueData('COUNTRY_OF_DESTINATION');
		foreach($countries as $country){ 
			$data['country_str'] .= '<option value="'.$country['COUNTRY_OF_DESTINATION'].'">'.ucwords(strtolower($country['COUNTRY_OF_DESTINATION'])).'</option>';
		}

		$exporter_name = $this->leads_model->getUniqueData('EXPORTER_NAME');
		foreach($exporter_name as $ename){ 
			$data['exporter_name'] .= '<option value="'.addslashes($ename['EXPORTER_NAME']).'">'.ucwords(strtolower(addslashes($ename['EXPORTER_NAME']))).'</option>';
		}

		$new_importer_name = $this->leads_model->getUniqueData('NEW_IMPORTER_NAME');
		foreach($new_importer_name as $niname){ 
			$data['new_importer_name'] .= '<option value="'.addslashes($niname['NEW_IMPORTER_NAME']).'">'.ucwords(strtolower(addslashes($niname['NEW_IMPORTER_NAME']))).'</option>';
		}
		$this->load->view('header', array('title' => 'Leads List'));
		$this->load->view('sidebar', array('title' => 'Leads List'));
		$this->load->view('fuzzy_exp_imp_view', $data);
		$this->load->view('footer');
	}

	function fuzzy_exp_imp_list_data(){
		$search = array();
		foreach ($this->input->post('columns') as $key => $value) {
			$search[$key] = $this->input->post('columns')[$key]['search']['value'];
		}
		$order_by = $this->input->post('columns')[$this->input->post('order')[0]['column']]['data'];
		if($order_by == 'record_id' || $order_by == 'exporter_name'){
			$order_by = 'exporter_name';
		}
		$dir = $this->input->post('order')[0]['dir'];
		$records = $this->leads_model->getExpImpLeadsList($this->input->post('start'), $this->input->post('length'), $search, $order_by, $dir);
		$data['iTotalDisplayRecords'] = $data['iTotalRecords'] = $this->leads_model->getExpImpLeadsListCount($search);
		$data['aaData'] = $records;

		$countries = $this->leads_model->getUniqueData('COUNTRY_OF_DESTINATION', $search);
		foreach($countries as $country){ 
			$data['country_str'] .= '<option value="'.$country['COUNTRY_OF_DESTINATION'].'">'.ucwords(strtolower($country['COUNTRY_OF_DESTINATION'])).'</option>';
		}

		$exporter_name = $this->leads_model->getUniqueData('EXPORTER_NAME', $search);
		foreach($exporter_name as $ename){ 
			$data['exporter_name'] .= '<option value="'.addslashes($ename['EXPORTER_NAME']).'">'.ucwords(strtolower(addslashes($ename['EXPORTER_NAME']))).'</option>';
		}

		$new_importer_name = $this->leads_model->getUniqueData('NEW_IMPORTER_NAME', $search);
		foreach($new_importer_name as $niname){ 
			$data['new_importer_name'] .= '<option value="'.addslashes($niname['NEW_IMPORTER_NAME']).'">'.ucwords(strtolower(addslashes($niname['NEW_IMPORTER_NAME']))).'</option>';
		}
		echo json_encode($data);
	}

	function fuzzy_imp_list(){
		$data['country_str'] = $data['port_str'] = $data['exporter_name'] = $data['importer_name'] = $data['new_importer_name'] = '';
		$countries = $this->leads_model->getUniqueData('COUNTRY_OF_DESTINATION');
		foreach($countries as $country){ 
			$data['country_str'] .= '<option value="'.$country['COUNTRY_OF_DESTINATION'].'">'.ucwords(strtolower($country['COUNTRY_OF_DESTINATION'])).'</option>';
		}

		$new_importer_name = $this->leads_model->getUniqueData('NEW_IMPORTER_NAME');
		foreach($new_importer_name as $niname){ 
			$data['new_importer_name'] .= '<option value="'.addslashes($niname['NEW_IMPORTER_NAME']).'">'.ucwords(strtolower(addslashes($niname['NEW_IMPORTER_NAME']))).'</option>';
		}
		$this->load->view('header', array('title' => 'Leads List'));
		$this->load->view('sidebar', array('title' => 'Leads List'));
		$this->load->view('fuzzy_imp_view', $data);
		$this->load->view('footer');
	}

	function fuzzy_imp_list_data(){
		$search = array();
		foreach ($this->input->post('columns') as $key => $value) {
			$search[$key] = $this->input->post('columns')[$key]['search']['value'];
		}
		$order_by = $this->input->post('columns')[$this->input->post('order')[0]['column']]['data'];
		if($order_by == 'record_id' || $order_by == 'exporter_name'){
			$order_by = 'exporter_name';
		}
		$dir = $this->input->post('order')[0]['dir'];
		$records = $this->leads_model->getImpLeadsList($this->input->post('start'), $this->input->post('length'), $search, $order_by, $dir);
		$data['iTotalDisplayRecords'] = $data['iTotalRecords'] = $this->leads_model->getImpLeadsListCount($search);
		$data['aaData'] = $records;

		$countries = $this->leads_model->getUniqueData('COUNTRY_OF_DESTINATION', $search);
		foreach($countries as $country){ 
			$data['country_str'] .= '<option value="'.$country['COUNTRY_OF_DESTINATION'].'">'.ucwords(strtolower($country['COUNTRY_OF_DESTINATION'])).'</option>';
		}

		$new_importer_name = $this->leads_model->getUniqueData('NEW_IMPORTER_NAME', $search);
		foreach($new_importer_name as $niname){ 
			$data['new_importer_name'] .= '<option value="'.addslashes($niname['NEW_IMPORTER_NAME']).'">'.ucwords(strtolower(addslashes($niname['NEW_IMPORTER_NAME']))).'</option>';
		}
		echo json_encode($data);
	}

	function getImporterNames($type){
		switch($type){
			case 1:
				$ids = explode(",", trim($this->input->post('record_ids'), ","));
				$data = $this->leads_model->getRecordData(1, $ids);
				break;

			case 2:
				$exp_imp = explode(",", trim($this->input->post('record_ids'), ","));
				$data = array();
				foreach ($exp_imp as $value) {
					$exp_imp_arr = explode("/", $value);
					$data[] = $this->leads_model->getRecordData(2, $exp_imp_arr[0], $exp_imp_arr[1]);
				}
				break;

			case 3:
				$new_imp_name = explode(",", trim($this->input->post('record_ids'), ","));
				$data = $this->leads_model->getRecordData(3, $new_imp_name);
				break;
		}
		echo json_encode($data);
	}

	function updateImporterName($type){
		$ids = explode(",", trim($this->input->post('ids'), ","));
		$this->leads_model->updateImporterName($type, $this->input->post('new_imp_name'), $ids);
	}

	function getDetails(){
		$res = $this->leads_model->getDetails($this->input->post('nimp_name'));
		echo json_encode($res);
	}

	function updateDetails(){
		$insert_arr = array(
			'new_importer_name' => $this->input->post('new_importer_name'),
			'no_of_employees' => $this->input->post('no_of_employees'),
			'buyer_name' => $this->input->post('buyer_name'),
			'designation' => $this->input->post('designation'),
			'email' => $this->input->post('email'),
			'telephone' => $this->input->post('telephone'),
			'mobile' => $this->input->post('mobile'),
			'is_whatsapp' => $this->input->post('is_whatsapp'),
			'skype' => $this->input->post('skype'),
			'entered_on' => date('Y-m-d H:i:s'),
			'entered_by' => $this->session->userdata('user_id')
		);
		$this->leads_model->insertData('lead_details', $insert_arr);
		redirect('leads/list');
	}

	function addLeadDetails($lead_id=0){
		$this->session->set_userdata('lead_priority', 0);
		if(!empty($this->input->post())){
			$main_array = array(
				//'company_name' => $this->input->post('lead_name'),
				'client_name' => $this->input->post('lead_name'),
				'country' => $this->input->post('lead_country'),
				'region' => $this->input->post('lead_region'),
				'brand' => $this->input->post('brand'),
				'source' => $this->input->post('source'),
				'website' => $this->input->post('website'),
				'no_of_employees' => $this->input->post('no_of_employees'),
				'lead_type' => $this->input->post('lead_type'),
				'lead_industry' => $this->input->post('lead_industry'),
				'lead_stage' => $this->input->post('lead_stage'),
				'stage_reason' => $this->input->post('stage_reason'),
				'box' => (!empty($this->input->post('box'))) ? $this->input->post('box'): 'No',
				'box_comment' => ($this->input->post('box') == 'Yes') ? $this->input->post('box_comment') : '',
				'sample' => (!empty($this->input->post('sample'))) ? $this->input->post('sample'): 'No',
				'sample_comment' => ($this->input->post('sample') == 'Yes') ? $this->input->post('sample_comment') : '',
				'modified_on' => date('Y-m-d H:i:s'),
				'status' => 'Y' 
			);

			if($this->input->post('assigned_to') > 0){
				$main_array['assigned_to'] = $this->input->post('assigned_to');
			}else if($this->input->post('source') == 'Miscellaneous Leads') {
				$main_array['assigned_to'] = $this->session->userdata('user_id');
			}
			$main_array['lead_priority'] = (int)$this->input->post('lead_priority_hetro');
			if($this->input->post('lead_id') > 0){
				$lead_id = $this->input->post('lead_id');
				$this->leads_model->updateData('clients', $main_array, array('client_id' => $lead_id));
				//$this->leads_model->deleteData('hetro_lead_detail', array('lead_id' => $lead_id));
			}else{
				$main_array['entered_on'] = date('Y-m-d H:i:s');
				$lead_id = $this->leads_model->insertData('clients', $main_array);
			}

			if(!empty($this->input->post('name'))){
				foreach ($this->input->post('name') as $key => $value) {
					$member_array = array(
						'lead_id' => $lead_id,
						'member_name' => $this->input->post('name')[$key],
						'designation' => $this->input->post('designation')[$key],
						'email' => $this->input->post('email')[$key],
						'mobile' => $this->input->post('mobile')[$key],
						'is_whatsapp' => $this->input->post('is_whatsapp')[$key],
						'skype' => $this->input->post('skype')[$key],
						'telephone' => $this->input->post('telephone')[$key],
						'main_buyer' => $this->input->post('main_buyer')[$key],
						'other_member' => $this->input->post('other_member')[$key],
					);
					if($this->input->post('member_id')[$key] > 0){
						$this->leads_model->updateData('hetro_lead_detail', $member_array, array('lead_dtl_id' => $this->input->post('member_id')[$key]));
					}else{
						$this->leads_model->insertData('hetro_lead_detail', $member_array);
					}
				}
			}
			$this->session->set_flashdata('lead_success', 'Lead updated successfully.');
			redirect('leads/hetregenous_leads/'.$this->input->post('source'), 'refresh');
		}else{
			if($lead_id != 0){
				$data['lead_id'] = $lead_id;
				$data['client_details'] = $this->leads_model->getLeadDetails($lead_id);
				if(!empty($data['client_details'])){
					$this->session->set_userdata('lead_priority', $data['client_details'][0]['lead_priority']);
				}
				$data['client_connects'] = $this->leads_model->getData('lead_connects', 'lead_id = '.$lead_id);
			}
			$data['source'] = $this->leads_model->getData('leads_source', 'status="Active" and source_type="hetro"');
			$data['users'] = $this->leads_model->getData('users', 'role=5 OR role=16');
			$data['region'] = $this->leads_model->getLookup(1);
			$data['country'] = $this->leads_model->getLookup(2);
			$data['lead_type'] = $this->leads_model->getData('lead_type');
			$data['lead_industry'] = $this->leads_model->getData('lead_industry');
			$data['lead_stages'] = $this->leads_model->getData('lead_stages');
			$data['lead_stage_reasons'] = $this->leads_model->getData('lead_stage_reasons', array('status'=> 'Active'));
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
			$this->load->view('header', array('title' => 'Add / Edit Lead'));
			$this->load->view('sidebar', array('title' => 'Add / Edit Lead'));
			$this->load->view('lead_details', $data);
			$this->load->view('footer');
		}
	}

	function hetregenous_leads_old($type = ''){
		$country = $this->leads_model->getDistinctHetroData('country', $type);
		$data['lead_country_str'] = '';
		foreach ($country as $key => $value) {
			$data['lead_country_str'] .= '<option value="'.$value['id'].'">'.$value['value'].'</option>';
		}

		$region = $this->leads_model->getDistinctHetroData('region', $type);
		$data['lead_region_str'] = '';
		foreach ($region as $key => $value) {
			$data['lead_region_str'] .= '<option value="'.$value['id'].'">'.$value['value'].'</option>';
		}

		$lead_type = $this->leads_model->getDistinctHetroData('type', $type);
		$data['lead_type_str'] = '';
		foreach ($lead_type as $key => $value) {
			$data['lead_type_str'] .= '<option value="'.$value['id'].'">'.$value['value'].'</option>';
		}

		$stage = $this->leads_model->getDistinctHetroData('stage', $type);
		$data['lead_stage_str'] = '';
		foreach ($stage as $key => $value) {
			$data['lead_stage_str'] .= '<option value="'.$value['id'].'">'.substr($value['value'], 0, 9).'</option>';
		}

		$data['type'] = $type;
		$this->load->view('header', array('title' => 'Leads List'));
		$this->load->view('sidebar', array('title' => 'Leads List'));
		$this->load->view('hetro_list_view', $data);
		$this->load->view('footer');
	} 

	function hetro_lead_data($type = ''){
		$search = array();
		foreach ($this->input->post('columns') as $key => $value) {
			$search_key = '';
			if($key == 1){
				$search_key = 'company_name';
			}else if($key == 2){
				$search_key = 'country';
			}else if($key == 3){
				$search_key = 'member_name';
			}else if($key == 4){
				$search_key = 'email';
			}else if($key == 5){
				$search_key = 'brand';
			}else if($key == 6){
				$search_key = 'lead_type';
			}else if($key == 7){
				$search_key = 'lead_stage';
			}else if($key == 8){
				$search_key = 'assigned_to';
			}else if($key == 9){
				$search_key = 'box_sample';
			}else if($key == 10){
				$search_key = 'region';
			}

			$search[$search_key] = $this->input->post('columns')[$key]['search']['value'];
		}
		if(!empty($this->input->post('last_contacted'))) {

			$search['last_contact']	= $this->input->post('last_contacted');
		}
		if(!empty($this->input->post('box_hetro'))) {

			$search['box_hetro'] = $this->input->post('box_hetro');
		}
		if(!empty($this->input->post('lead_type_hetro'))) {

			$search['lead_type_hetro'] = $this->input->post('lead_type_hetro');
		}
		if(!empty($this->input->post('lead_stage_hetro'))) {

			$search['lead_stage_hetro'] = $this->input->post('lead_stage_hetro');
		}
		if(!empty($this->input->post('country_hetro'))) {

			$search['country_hetro'] = $this->input->post('country_hetro');
		}
		if(!empty($this->input->post('region_hetro'))) {

			$search['region_hetro'] = $this->input->post('region_hetro');
		}
		if(!empty($this->input->post('sales_person_hetro'))) {

			$search['sales_person_hetro'] = $this->input->post('sales_person_hetro');
		}
		if(!empty($this->input->post('brand_hetro'))) {

			$search['brand_hetro'] = $this->input->post('brand_hetro');
		}
		if(!empty($this->input->post('lead_priority_hetro'))) {

			$search['lead_priority_hetro'] = $this->input->post('lead_priority_hetro');
		}
		// echo "<pre>";print_r($search);echo"</pre><hr>";exit;
		//print_r($search);
		$order_by = $this->input->post('columns')[$this->input->post('order')[0]['column']]['data'];
		$dir = $this->input->post('order')[0]['dir'];
		if($order_by == 'record_id' || $order_by = 'lead_stage'){
			if($order_by != 'lead_stage'){

				$dir = 'desc';
			}
			$order_by = 'lead_stage';
		}
		// echo "<pre>";print_r($order_by);echo"</pre><hr>";exit;
		$records = $this->leads_model->getHetroLeadsList($this->input->post('start'), $this->input->post('length'), $search, $order_by, $dir, $type);
		$data['iTotalDisplayRecords'] = $data['iTotalRecords'] = $this->leads_model->getHetroLeadsListCount($search, $type);
		$data['aaData'] = $records;
		echo json_encode($data);
	}

	function hetregenous_leads($type = ''){
		$country = $this->leads_model->getDistinctHetroData('country', $type);
		$data['lead_country_str'] = '';
		foreach ($country as $key => $value) {
			$data['lead_country_str'] .= '<option value="'.$value['id'].'">'.$value['value'].'</option>';
		}

		$region = $this->leads_model->getDistinctHetroData('region', $type);
		$data['lead_region_str'] = '';
		foreach ($region as $key => $value) {
			$data['lead_region_str'] .= '<option value="'.$value['id'].'">'.$value['value'].'</option>';
		}

		$lead_type = $this->leads_model->getDistinctHetroData('type', $type);
		$data['lead_type_str'] = '';
		foreach ($lead_type as $key => $value) {
			$data['lead_type_str'] .= '<option value="'.$value['id'].'">'.$value['value'].'</option>';
		}

		$stage = $this->leads_model->getDistinctHetroData('stage', $type);
		$data['lead_stage_str'] = '';
		foreach ($stage as $key => $value) {
			$data['lead_stage_str'] .= '<option value="'.$value['id'].'">'.substr($value['value'], 0, 9).'</option>';
		}

		$brands = $this->leads_model->getDistinctHetroData('brand', $type);
		$data['lead_brand_str'] = '';
		foreach ($brands as $key => $value) {
			$data['lead_brand_str'] .= '<option value="'.$value['id'].'">'.$value['value'].'</option>';
		}

		$users = $this->leads_model->getData('users', 'role = 5 OR role=16');
		$data['user_str'] = '';
		foreach ($users as $key => $value) {
			$data['user_str'] .= '<option value="'.$value['user_id'].'">'.$value['name'].'</option>';
		}

		$region = $this->leads_model->getData('lookup', 'lookup_group = 1');
		$data['region_str'] = '';
		foreach ($region as $key => $value) {
			$data['region_str'] .= '<option value="'.$value['lookup_id'].'">'.$value['lookup_value'].'</option>';
		}

		$data['type'] = $type;
		$this->load->view('header', array('title' => 'Leads List'));
		$this->load->view('sidebar', array('title' => 'Leads List'));
		$this->load->view('hetro_leads_list', $data);
		$this->load->view('footer');
	}

	function addComments(){

		// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";
		$arr = array(
			'lead_id' => $this->input->post('lead_id'),
			'member_id' => $this->input->post('member_id'),
			'connected_on' => date('Y-m-d', strtotime($this->input->post('contact_date'))),
			'comments' => $this->input->post('contact_details'),
			'connect_mode' => $this->input->post('connect_mode'),
			'email_sent' => $this->input->post('email_sent'),
			'entered_on' => date('Y-m-d H:i:s')
		);
		$this->insert_into_daily_work_sales_on_lead_data('hetro');
		$this->leads_model->insertData('lead_connects', $arr);
		redirect('leads/addLeadDetails/'.$this->input->post('lead_id'));
	}

	function addCommentsPrimary(){

		// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
		$arr = array(
			'lead_id' => $this->input->post('lead_id'),
			'member_id' => $this->input->post('member_id'),
			'connected_on' => date('Y-m-d', strtotime($this->input->post('contact_date'))),
			'comments' => $this->input->post('contact_details'),
			'connect_mode' => $this->input->post('connect_mode'),
			'email_sent' => $this->input->post('email_sent'),
			'entered_on' => date('Y-m-d H:i:s')
		);
		$this->insert_into_daily_work_sales_on_lead_data('primary');
		$this->leads_model->insertDataDB2('lead_connects', $arr);
		redirect('leads/addPrimaryLeadDetails/'.$this->input->post('lead_id').'/'.$this->input->post('category'));	
	}

	private function insert_into_daily_work_sales_on_lead_data($call_type){

		// echo "<pre>";print_r($data);echo"</pre><hr>";
		$insert_array = array();
		if($call_type == 'primary') {

			$insert_array['user_id'] = $this->session->userdata('user_id');
			$insert_array['client_id'] = $this->input->post('lead_id');
			$lead_details = $this->common_model->get_dynamic_data_marketing_db('IMPORTER_NAME, data_category, COUNTRY_OF_DESTINATION', array('lead_mst_id' => $this->input->post('lead_id')), 'lead_mst','row_array');
			$insert_array['client_name'] = '';
			$insert_array['lead_name'] = '';
			$insert_array['country'] = '';
			if(!empty($lead_details)){

				$insert_array['client_name'] = $lead_details['IMPORTER_NAME'];
				$insert_array['lead_name'] = $lead_details['data_category'];
				$insert_array['country'] = $lead_details['COUNTRY_OF_DESTINATION'];
			}
			$insert_array['member_id'] = $this->input->post('member_id');
			$insert_array['member_name'] = '';
			$member_details = $this->common_model->get_dynamic_data_marketing_db('member_name', array('lead_mst_id' => $this->input->post('lead_id'), 'lead_dtl_id' => $this->input->post('member_id')), 'lead_detail','row_array');
			if(!empty($member_details)){

				$insert_array['member_name'] = $member_details['member_name'];
			}
			
			$insert_array['contact_date'] = date('Y-m-d', strtotime($this->input->post('contact_date'))).' '.date('h:i:s');
			$insert_array['contact_mode'] = $this->input->post('connect_mode');
			$insert_array['comments'] = $this->input->post('contact_details');
			$insert_array['email_sent'] = $this->input->post('email_sent');
		}else if($call_type == 'hetro'){

			$insert_array['user_id'] = $this->session->userdata('user_id');
			$insert_array['client_id'] = $this->input->post('lead_id');
			$lead_details = $this->common_model->get_dynamic_data_sales_db('client_name, source, country', array('client_id' => $this->input->post('lead_id')), 'clients','row_array');
			$insert_array['client_name'] = '';
			$insert_array['lead_name'] = '';
			$insert_array['country'] = '';
			if(!empty($lead_details)){

				$insert_array['client_name'] = $lead_details['client_name'];
				$insert_array['lead_name'] = $lead_details['source'];
				if(!empty($lead_details['country'])){

					$lookup_details = $this->common_model->get_dynamic_data_sales_db('lookup_value', array('lookup_id' => $lead_details['country']), 'lookup','row_array');
					if(!empty($lookup_details)){

						$insert_array['country'] = $lookup_details['lookup_value'];
					}
				}
			}
			$insert_array['member_id'] = $this->input->post('member_id');
			$member_details = $this->common_model->get_dynamic_data_sales_db('member_name', array('lead_id' => $this->input->post('lead_id'), 'lead_dtl_id' => $this->input->post('member_id')), 'hetro_lead_detail','row_array');
			$insert_array['member_name'] = '';
			if(!empty($member_details)){

				$insert_array['member_name'] = $member_details['member_name'];
			}
			$insert_array['contact_date'] = date('Y-m-d', strtotime($this->input->post('contact_date'))).' '.date('h:i:s');
			$insert_array['contact_mode'] = $this->input->post('connect_mode');
			$insert_array['comments'] = $this->input->post('contact_details');
			$insert_array['email_sent'] = $this->input->post('email_sent');
		}
	
		// echo "<pre>";print_r($insert_array);echo"</pre><hr>";exit;
		if(!empty($insert_array)){

			$this->common_model->insert_data_sales_db('daily_work_sales_on_lead_data', $insert_array);
		}
	}

	function deleteMember(){
		$this->leads_model->deleteData('hetro_lead_detail', array('lead_dtl_id' => $this->input->post('member_id')));
	}

	function deleteMemberDB2(){
		$this->leads_model->deleteDataDB2('lead_detail', array('lead_dtl_id' => $this->input->post('member_id')));
	}

	function primary_leads($category=''){
		$data['lead_category'] = $category;
		$this->load->view('header', array('title' => 'Leads List'));
		$this->load->view('sidebar', array('title' => 'Leads List'));
		$this->load->view('primary_list_view', $data);
		$this->load->view('footer');
	}

	function primary_leads_data($category=''){
		$search = array();
		foreach ($this->input->post('columns') as $key => $value) {
			$search[$this->input->post('columns')[$key]['data']] = $this->input->post('columns')[$key]['search']['value'];
		}
		//print_r($search);
		$order_by = $this->input->post('columns')[$this->input->post('order')[0]['column']]['data'];
		if($order_by == 'record_id'){
			$order_by = 'RANK';
		}
		$dir = $this->input->post('order')[0]['dir'];
		$records = $this->leads_model->getPrimaryLeadsList($this->input->post('start'), $this->input->post('length'), $search, $order_by, $dir, $category);
		$data['iTotalDisplayRecords'] = $data['iTotalRecords'] = $this->leads_model->getPrimaryLeadsListCount($search, $category);
		$data['aaData'] = $records;

		echo json_encode($data);
	}

	function addPrimaryLeadDetails($lead_id=0, $category){
		$category = urldecode($category);
		$this->session->set_userdata('lead_priority', 0);
		if(!empty($this->input->post())){
			// echo "<pre>";print_r($this->input->post());exit;
			$main_array = array(
				'imp_id' => $this->input->post('imp_id'),
				'source' => $this->input->post('source'),
				'data_category' => $this->input->post('data_category'),
				'website' => $this->input->post('website'),
				'no_of_employees' => $this->input->post('no_of_employees'),
				'lead_type' => $this->input->post('lead_type'),
				'lead_industry' => $this->input->post('lead_industry'),
				'lead_stage' => $this->input->post('lead_stage'),
				'stage_reason' => $this->input->post('stage_reason'),
				'purchase_factor_1' => $this->input->post('purchase_factor_1'),
				'purchase_factor_2' => $this->input->post('purchase_factor_2'),
				'purchase_factor_3' => $this->input->post('purchase_factor_3'),
				'sales_notes' => $this->input->post('sales_notes'),
				'box' => (!empty($this->input->post('box'))) ? $this->input->post('box'): 'No',
				'box_comment' => ($this->input->post('box') == 'Yes') ? $this->input->post('box_comment') : '',
				'sample' => (!empty($this->input->post('sample'))) ? $this->input->post('sample'): 'No',
				'sample_comment' => ($this->input->post('sample') == 'Yes') ? $this->input->post('sample_comment') : '',
				'modified_on' => date('Y-m-d H:i:s') 
			);

			if($this->input->post('product_pitch')){
				$main_array = array(
					'product_pitch' => $this->input->post('product_pitch'),
					'purchase_comments' => $this->input->post('purchase_comments'),
					'margins' => $this->input->post('margins'),
					'no_of_employees' => $this->input->post('no_of_employees'),
					'box' => (!empty($this->input->post('box'))) ? $this->input->post('box'): 'No',
					'box_comment' => ($this->input->post('box') == 'Yes') ? $this->input->post('box_comment') : '',
					'sample' => (!empty($this->input->post('sample'))) ? $this->input->post('sample'): 'No',
					'sample_comment' => ($this->input->post('sample') == 'Yes') ? $this->input->post('sample_comment') : ''
				);
			}
			$main_array['lead_priority'] = (int)$this->input->post('primary_lead_priority');
			if($this->input->post('assigned_to')){
				$main_array['assigned_to'] = $this->input->post('assigned_to');
			}

			if($this->input->post('lead_mst_id') > 0){
				// echo "<pre>";print_r($main_array);echo"</pre><hr>";exit;
				$lead_id = $this->input->post('lead_mst_id');
				$this->leads_model->updateDataDB2('lead_mst', $main_array, array('lead_mst_id' => $lead_id));
				//$this->leads_model->deleteDataDB2('lead_detail', array('lead_mst_id' => $lead_id));
			}else{
				$main_array['entered_on'] = date('Y-m-d H:i:s');
				$lead_id = $this->leads_model->insertDataDB2('lead_mst', $main_array);
			}

			if(!empty($this->input->post('name'))){
				foreach ($this->input->post('name') as $key => $value) {
					$member_array = array(
						'lead_mst_id' => $lead_id,
						'member_name' => $this->input->post('name')[$key],
						'designation' => $this->input->post('designation')[$key],
						'email' => $this->input->post('email')[$key],
						'mobile' => $this->input->post('mobile')[$key],
						'is_whatsapp' => $this->input->post('is_whatsapp')[$key],
						'skype' => $this->input->post('skype')[$key],
						'telephone' => $this->input->post('telephone')[$key],
						'main_buyer' => $this->input->post('main_buyer')[$key],
						'other_member' => $this->input->post('other_member')[$key],
						'decision_maker' => $this->input->post('decision_maker')[$key],
					);

					if($this->input->post('lead_dtl_id')[$key] > 0){
						$this->leads_model->updateDataDB2('lead_detail', $member_array, array('lead_dtl_id' => $this->input->post('lead_dtl_id')[$key]));
					}else{
						$this->leads_model->insertDataDB2('lead_detail', $member_array);
					}
				}
			}
			$this->session->set_flashdata('lead_success', 'Lead updated successfully.');
			redirect('leads/primary_leads_list/'.$category, 'refresh');
		}else{
			if($lead_id != 0){
				$data['lead_id'] = $lead_id;
				$data['lead_category'] = $category;
				$data['client_details'] = $this->leads_model->getPrimaryLeadDetails($lead_id, $category);
				if(!empty($data['client_details'])){
					$this->session->set_userdata('lead_priority', $data['client_details'][0]['lead_priority']);
				}
				$lead_id = $data['client_details'][0]['lead_mst_id'];
				if($lead_id > 0){
					$data['client_connects'] = $this->leads_model->getConnectDetailsDB2($lead_id);
					krsort($data['client_connects']);
				}
			}
			$data['users'] = $this->leads_model->getData('users', 'role=5 OR role=16');
			$data['region'] = $this->leads_model->getLookup(1);
			$data['country'] = $this->leads_model->getLookup(2);
			$data['lead_type'] = $this->leads_model->getData('lead_type');
			$data['lead_industry'] = $this->leads_model->getData('lead_industry');
			$data['lead_stages'] = $this->leads_model->getData('lead_stages');
			$data['lead_stage_reasons'] = $this->leads_model->getData('lead_stage_reasons', array('status'=> 'Active'));
			$data['purchase_factors'] = $this->leads_model->getData('purchase_factors');
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;

			$this->load->view('header', array('title' => 'Add / Edit Lead'));
			$this->load->view('sidebar', array('title' => 'Add / Edit Lead'));
			$this->load->view('primary_lead_details', $data);
			$this->load->view('footer');
		}
	}

	function getConnectDetailsDB2(){
		/*$res = $this->leads_model->getDataDB2('lead_connects', 'member_id = '.$this->input->post('member_id'));
		$arr = array();
		foreach ($res as $key => $value) {
			$arr[$key] = $value;
			$arr[$key]['connected_on'] = date('d M', strtotime($value['connected_on']));
		}
		echo json_encode($arr);*/

		$res = $this->leads_model->getLeadConnectsDB2($this->input->post('member_id'));
		$arr = array();
		foreach ($res as $key => $value) {
			$arr[$key] = $value;
			$arr[$key]['connected_on'] = date('d M', strtotime($value['connected_on']));
		}
		echo json_encode($arr);
	}

	function getConnectDetails(){
		$res = $this->leads_model->getData('hetro_lead_detail', 'lead_dtl_id = '.$this->input->post('member_id'));
		$res = $this->leads_model->getData('lead_connects', 'lead_id = '.$res[0]['lead_id']);
		$arr = array();
		foreach ($res as $key => $value) {
			$arr[$key] = $value;
			$arr[$key]['connected_on'] = date('d M', strtotime($value['connected_on']));
		}
		echo json_encode($arr);
	}

	function updatePrimaryLeadStage(){
		$this->leads_model->updateDataDB2('lead_mst', array('lead_stage' => $this->input->post('stage')), array('lead_mst_id' => $this->input->post('lead_mst_id')));
	}

	function primary_leads_list($category){
		$category = urldecode($category);
		$lead_type = $this->leads_model->getData('lead_type');
		$data['lead_type_str'] = '<option value="blank">Blank Lead Type</option>';
		foreach ($lead_type as $key => $value) {
			$data['lead_type_str'] .= '<option value="'.$value['lead_type_id'].'">'.$value['type_name'].'</option>';
		}

		$stage = $this->leads_model->getData('lead_stages');
		$data['lead_stage_str'] = '<option value="blank">Blank Lead Stage</option>';
		foreach ($stage as $key => $value) {
			$data['lead_stage_str'] .= '<option value="'.$value['lead_stage_id'].'">'.substr($value['stage_name'], 0, 9).'</option>';
		}

		$users = $this->leads_model->getData('users', 'role = 5 OR role=16');
		$data['user_str'] = '<option value="0">Blank User</option>';
		foreach ($users as $key => $value) {
			$data['user_str'] .= '<option value="'.$value['user_id'].'">'.$value['name'].'</option>';
		}

		$country = $this->leads_model->getLeadCountries($category);
		$data['lead_country'] = '';
		foreach ($country as $key => $value) {
			$data['lead_country'] .= '<option value="'.$value['COUNTRY_OF_DESTINATION'].'">'.substr($value['COUNTRY_OF_DESTINATION'], 0, 9).'</option>';
		}

		$region = $this->leads_model->getData('lookup', 'lookup_group = 1');
		$data['region_str'] = '';
		foreach ($region as $key => $value) {
			$data['region_str'] .= '<option value="'.$value['lookup_id'].'">'.$value['lookup_value'].'</option>';
		}

		$data['lead_category'] = $category;

		$data['year_list'] = $this->leads_model->get_all_year_name();
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Leads List'));
		$this->load->view('sidebar', array('title' => 'Leads List'));
		$this->load->view('primary_leads_list', $data);
		$this->load->view('footer');
	} 

	function primary_list_data($category){
		$category = urldecode($category);
		$search = array();
		foreach ($this->input->post('columns') as $key => $value) {
			$search_key = '';
			if($key == 1){
				$search_key = 'IMPORTER_NAME';
			}else if($key == 2){
				$search_key = 'lead_type';
			}else if($key == 3){
				$search_key = 'lead_stage';
			}else if($key == 4){
				$search_key = 'COUNTRY_OF_DESTINATION';
			}else if($key == 5){
				$search_key = 'box_sample';
			}else if($key == 6){
				$search_key = 'assigned_to';
			}else if($key == 7){
				$search_key = 'region';
			}

			$search[$search_key] = $this->input->post('columns')[$key]['search']['value'];
		}
		if(!empty($this->input->post('last_contacted'))) {

			$search['last_contact']	= $this->input->post('last_contacted');
		}
		if(!empty($this->input->post('last_purchase'))) {

			$search['last_purchase'] = $this->input->post('last_purchase');
		}
		if(!empty($this->input->post('box_primary'))) {

			$search['box_primary'] = $this->input->post('box_primary');
		}
		if(!empty($this->input->post('lead_type_primary'))) {

			$search['lead_type_primary'] = $this->input->post('lead_type_primary');
		}
		if(!empty($this->input->post('lead_stage_primary'))) {

			$search['lead_stage_primary'] = $this->input->post('lead_stage_primary');
		}
		if(!empty($this->input->post('country_primary'))) {

			$search['country_primary'] = $this->input->post('country_primary');
		}
		if(!empty($this->input->post('region_primary'))) {

			$search['region_primary'] = $this->input->post('region_primary');
		}
		if(!empty($this->input->post('sales_person_primary'))) {

			$search['sales_person_primary'] = $this->input->post('sales_person_primary');
		}
		if(!empty($this->input->post('connect_mode_primary'))) {

			$search['connect_mode_primary'] = $this->input->post('connect_mode_primary');
		}
		if(!empty($this->input->post('lead_priority_primary'))) {

			$search['lead_priority_primary'] = $this->input->post('lead_priority_primary');
		}
		$order_by = $this->input->post('columns')[$this->input->post('order')[0]['column']]['data'];
		if($order_by == 'record_id'){
			$order_by = 't1.IMPORTER_NAME';
		}
		// echo "<pre>";print_r($search);echo"</pre><hr>";exit;
		$dir = $this->input->post('order')[0]['dir'];
		$records = $this->leads_model->getPrimaryListData($this->input->post('start'), $this->input->post('length'), $search, $order_by, $dir, $category);
		$data['iTotalDisplayRecords'] = $data['iTotalRecords'] = $this->leads_model->getPrimaryListCount($search, $category);
		$data['aaData'] = $records;
		echo json_encode($data);
	}

	function getYearWiseImport(){
		$imp_name = $this->input->post('imp_name');
		$category = $this->input->post('category');
		$res = $this->leads_model->getYearWiseImport($imp_name, $category);
		echo json_encode($res);
	}

	function getMembers(){
		$res = $this->leads_model->getMembers($this->input->post('lead_id'), $this->input->post('member_type'));
		echo json_encode($res);
	}

	function getHetroMembers(){
		$res = $this->leads_model->getHetroMembers($this->input->post('lead_id'), $this->input->post('member_type'));
		echo json_encode($res);
	}

	public function ajax_function() {

		if($this->input->is_ajax_request()){
			$call_type = $this->input->post('call_type');
			$response['status'] = 'successful';	
			switch ($call_type) {

				case 'update_rating_primary_lead':

					$form_data = array_column($this->input->post('rating_value'), 'value', 'name');
					foreach ($form_data as $form_key => $form_value) {
						
						$explode = explode('_', $form_key);
						// echo "<pre>";print_r($explode);echo"</pre><hr>";exit;
						if(count($explode) == 5) {

							if(empty($form_data['update_primary_lead_priority_reason_'.$explode[4]])){

								$response['status'] = 'failed';
							} else {
								
								$this->common_model->update_data_marketing_db('lead_mst', array('lead_priority' => (int)$form_value), array('lead_mst_id'=>$explode[4]));
							}
						} elseif(count($explode) == 6) {

							$this->common_model->update_data_marketing_db('lead_mst', array('priority_reason' => $form_value), array('lead_mst_id'=>$explode[5]));
						}
					}
				break;

				case 'update_rating_hetro_lead':

					$form_data = array_column($this->input->post('rating_value'), 'value', 'name');
					foreach ($form_data as $form_key => $form_value) {
						
						$explode = explode('_', $form_key);
						if(count($explode) == 5) {

							if(empty($form_data['update_hetro_lead_priority_reason_'.$explode[4]])){

								$response['status'] = 'failed';
							} else {
								
								$this->common_model->update_data_sales_db('clients', array('lead_priority' => (int)$form_value), array('client_id'=>$explode[4]));
							}
						} elseif(count($explode) == 6) {

							$this->common_model->update_data_sales_db('clients', array('priority_reason' => $form_value), array('client_id'=>$explode[5]));
						}
					}
				break;

				case 'delete_lead':
					$response['status'] = 'failed';
					if(!empty($this->input->post('lead_id'))) {
						$this->common_model->update_data_sales_db('clients', array('deleted'=>1), array('client_id'=>$this->input->post('lead_id')));
						$response['status'] = 'successful';
					}	
				break;

				default:
					$response['status'] = 'failed';	
					$response['message'] = "call type not found";
					break;
			}
			echo json_encode($response);

		} else{
			die('access is not allowed to this function');
		}
	}
}
