<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="kt-portlet ">
		<?php if($this->session->flashdata('lead_success')){ ?>
			<div class="alert alert-success" id="success-alert">
				<strong><?php echo $this->session->flashdata('lead_success'); ?></strong> 
			</div>
		<?php } ?>
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					<?php 
						if($this->uri->segment(3) == 'Forged%20Fittings') {
							echo 'PRIMARY LEADS - ',strtoupper(str_replace('%20', '  ', $this->uri->segment(3)));
						}else if($this->uri->segment(3) == 'distributors') {
							echo 'TUBE FITTINGS DISTRIBUTORS';
						}else if($this->uri->segment(3) == 'PVF%20companies') {
							echo 'PVF LEADS';
						}else if($this->uri->segment(3) == 'Heteregenous%20Tubes%20India') {
							echo 'TUBE FITTINGS + VALVE LEADS- INDIA';
						}else if($this->uri->segment(3) == 'sugar%20companies') {
							echo 'SUGAR PRODUCER LEADS';
						}else if($this->uri->segment(3) == 'EPC%20companies') {
							echo 'EPC LEADS';
						}else if($this->uri->segment(3) == 'Water%20Companies') {
							echo 'WATER TREATMENT COMPANY LEADS';
						}else if($this->uri->segment(3) == 'Chemical%20Companies') {
							echo 'CHEMICAL PRODUCER LEADS';
						}else if($this->uri->segment(3) == 'Shipyards') {
							echo 'SHIPYARDS LEADS';
						}else if($this->uri->segment(3) == 'Miscellaneous%20Leads') {
							echo 'Miscellaneous Leads';
						}
					?>
				</h3>
			</div>
		</div>
		<div class="kt-portlet__body">

			<div class="row">
				<div class="col-md-2 kt-margin-b-10-tablet-and-mobile">
			        <label>Last Contact:</label>
			        <select class="form-control" id="last_contact">
			            <option value="">Last Contacted</option>
						<option value="0-7">Less Than 1 Week</option>
						<option value="7-28">1 Week To 4 Week</option>
						<option value="28-0">1 Month +</option>
						<option value="0-0">blank</option>     
			        </select>
				</div>	
				<div class="col-md-2 kt-margin-b-10-tablet-and-mobile">
			        <label>Box/Sample :</label>
					<select class="form-control kt-selectpicker" id="box_hetro" multiple>
						<option value="box-Yes">Box-Yes</option>
						<option value="box-No">Box-No</option>
						<option value="sample-Yes">Sample-Yes</option>
						<option value="sample-No">Sample-No</option>
					</select>
				</div>
				<div class="col-md-2 kt-margin-b-10-tablet-and-mobile">
			        <label>Lead Type :</label>
					<select class="form-control kt-selectpicker" id="lead_type_hetro" multiple>
						<?php echo $lead_type_str;?>
					</select>
				</div>
				<div class="col-md-2 kt-margin-b-10-tablet-and-mobile">
			        <label>Lead Stage :</label>
					<select class="form-control kt-selectpicker" id="lead_stage_hetro" multiple>
						<?php echo $lead_stage_str;?>
					</select>
				</div>
				<div class="col-md-2 kt-margin-b-10-tablet-and-mobile">
			        <label>Country :</label>
					<select class="form-control kt-selectpicker" id="country_hetro" multiple>
						<?php echo $lead_country_str;?>
					</select>
				</div>
				<div class="col-md-2 pull-right">
					<a href="<?php echo site_url('leads/addLeadDetails'); ?>" class="btn btn-primary pull-right" target="_blank">Add New</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2 kt-margin-b-10-tablet-and-mobile">
			        <label>Region :</label>
					<select class="form-control kt-selectpicker" id="region_hetro" multiple>
						<?php echo $region_str;?>
					</select>
				</div>
				<?php if($type == '' || $type == 'distributors'){?>
				<div class="col-md-2 kt-margin-b-10-tablet-and-mobile">
			        <label>Brand :</label>
					<select class="form-control kt-selectpicker" id="brand_hetro" multiple>
						<?php echo $lead_brand_str;?>
					</select>
				</div>
				<?php }?>
				<div class="col-md-2 kt-margin-b-10-tablet-and-mobile">
			        <label>Lead Priority :</label>
					<div class="kt-font-info">
						<input type="hidden" id="lead_priority_hetro" value="0" />
					</div>
				</div>
				<?php if(in_array($this->session->userdata('role'), array(1, 16))){ ?>
				<div class="col-md-2 kt-margin-b-10-tablet-and-mobile">
			        <label>Sales Person :</label>
					<select class="form-control kt-selectpicker" id="sales_person_hetro" multiple>
						<?php echo $user_str;?>
					</select>
				</div>
				<?php }?>
			</div>
			<!--begin: Datatable -->
			<table class="table table-striped- table-bordered table-hover table-checkable" id="leads_table">
				<thead>
					<tr>
						<th width="3%">No.</th>
						<th width="17%">Lead Details</th>
						<th width="10%">Lead Stage</th>
						<th width="15%">Name</th>
						<th width="15%">Contact</th>
						<th width="5%"></th>
						<?php if($type == '' || $type == 'distributors'){?>
						<th width="5%">Brand</th>
						<?php }?>
						<th width="5%">Assigned To</th>
						<th width="15%">Comments</th>
						<th width="5%">Connect Mode</th>
						<th width="5%">Actions</th>
					</tr>
				</thead>
			</table>

			<!--end: Datatable -->
		</div>
	</div>
</div>


<div class="modal fade" id="member-contact-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Follow Up</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
                <form action="<?php echo site_url('leads/addComments'); ?>" method="post" id="lead_connect">
               		<div class="row">
               			<div class="col-md-4 form-group">
               				<label for="contact_date">Connect Date</label>
               				<input type="text" id="contact_date" name="contact_date" class="form-control validate[required] hasdatepicker" value="<?php echo date('d-m-Y'); ?>">
               			</div>

               			<div class="col-md-4 form-group">
               				<label for="contact_date">Connect Mode</label>
               				<select class="form-control validate[required]" name="connect_mode" id="connect_mode">
               					<option value=""></option>
               					<option value="whatsapp">Whatsapp</option>
               					<option value="call">Call Connected</option>
               					<option value="call_attempted">Call Attempted</option>
               					<option value="linkedin">LinkedIn</option>
               					<option value="email">Email</option>
               				</select>
               			</div>

               			<div class="col-md-4 form-group">
               				<label for="contact_date">Email Sent</label>
               				<select class="form-control validate[required]" name="email_sent" id="email_sent">
               					<option value=""></option>
               					<option value="Yes">Yes</option>
               					<option value="No">No</option>
               				</select>
               			</div>

               			<div class="col-md-12 form-group">
               				<label for="contact_details">Comments</label>
               				<textarea id="contact_details" name="contact_details" class="form-control validate[required]"></textarea>
               			</div>
               		</div>
               		<div class="clearfix"></div>
               		<div class="row">
               			<div class="col-md-6 align-self-center">
               				<input type="hidden" id="member_id" name="member_id">
							<input type="hidden" id="imp_id" name="imp_id" value="<?php if(isset($client_details)){echo $client_details[0]['imp_id'];}?>">
               				<input type="hidden" id="lead_id" name="lead_id" value="<?php if(isset($client_details)){echo $client_details[0]['lead_mst_id'];} ?>">
               				<button class="btn btn-success" type="submit">Submit</button>
               			</div>
               		</div>
               	</form>
               	<hr/>
               	<h4>Connect History</h4>
                <div id="tab_history">
                	<table class="table table-bordered" id="connect_table">
                		<thead>
	                		<tr>
	                			<th>Contacted On</th>
	                			<th>Contact Mode</th>
	                			<th>Email Sent</th>
	                			<th>Comments</th>
	                		</tr>
	                	</thead>
	                	<tbody>
                		<?php if(!empty($client_connects)) {?>
                		<?php 
                			foreach ($client_connects as $key => $value) {
                				echo '<tr member_id = "'.$value['member_id'].'">
                					<td>'.date('d-m-Y', strtotime($value['connected_on'])).'</td>
                					<td>'.ucfirst($value['connect_mode']).'</td>
                					<td>'.$value['email_sent'].'</td>
                					<td>'.$value['comments'].'</td>
                				</tr>';
                			}
                		?>
                		<?php }?>
                		</tbody>
                	</table>
                </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="box-comment-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Comments</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<style>
					#box_comments_table.table-bordered th, #box_comments_table.table-bordered td, #member_table.table-bordered th, #member_table.table-bordered td{
						border: 1px solid #ebedf2 !important;
					}
				</style>
				<table class="table table-bordered" id="box_comments_table">
					<thead>
						<tr>
							<th style="width: 50% !important;">Box Comments</th>
							<th style="width: 50% !important;">Sample Box Comments</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="member-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Member Details</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<style>
					#buyer_table.table-bordered th, #buyer_table.table-bordered td, #member_table.table-bordered th, #member_table.table-bordered td{
						border: 1px solid #ebedf2 !important;
					}
				</style>
				<table class="table table-bordered" id="buyer_table">
					<thead>
						<tr>
							<th colspan="3" style="text-align:center">Buyers</th>
						</tr>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Whatsapp</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>

				<table class="table table-bordered" id="member_table">
					<thead>
						<tr>
							<th colspan="3" style="text-align:center">Other Members</th>
						</tr>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Whatsapp</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="taskModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<form id="addTask" name="addTask">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">New Task</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
               			<div class="col-12 align-self-center form-group">
               				<textarea class="form-control validate[required]" placeholder="Task Details" id="taskDetail" name="task_detail"></textarea>
               			</div>
               			<div class="col-12 align-self-center form-group">
               				<input type="text" class="form-control validate[required]" id="deadline" name="deadline" placeholder="Task Deadline">
               			</div>
               		</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" id="task_lead_id" name="task_lead_id" >
					<input type="hidden" id="task_member_id" name="task_member_id" value="">
					<input type="hidden" name="lead_source" value="hetro leads">
					<button type="submit" class="btn btn-success" id="saveTask">Save Task</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close" onclick="addTask.reset();">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!--begin::Modal-->
<div class="modal fade" id="update_rating_hetro_lead" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Update Rating</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form id="hetro_lead_priority_form">
					<div class="row">
						<div class="col-md-12 form-group row">
							<label class="col-form-label col-lg-4 col-sm-12">Lead Priority</label>
							<div class="kt-font-info col-lg-6 col-md-9 col-sm-12" id="modal_body_update_rating"></div>
						</div>
						<div class="col-md-12 form-group row">
							<label class="col-form-label col-lg-4 col-sm-12">Priority Reason</label>
							<div class="kt-font-info col-lg-6 col-md-9 col-sm-12" id="modal_body_update_rating_reason"></div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary save_rating_primary_lead">Save changes</button>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->