jQuery(document).ready(function(){

    // alert("hii");
    Pq_Js.init();

    $('a.add_search_filter').click(function () {

        var button_value = $(this).attr('button_value');
        if (button_value == 'show') {
            ajax_call_function({
                call_type: 'get_search_filter_div' }, 'get_search_filter_div');
            $('div.search_filter_data').show();
            $(this).attr('button_value', 'hide');
            $(this).html('Hide Search Filter');
        } else {
            $('div.search_filter_data').hide();
            $(this).attr('button_value', 'show');
            $(this).html('Add Search Filter');
        }
    });

    $('div.search_filter_data').on('click', 'button.search_filter_submit', function () {
        
        set_reset_spinner($('button.search_filter_submit'));
        var tab_name = $('li.tab_name_click.active_list').attr('tab-name');
        if (tab_name == '') {

            tab_name = 'pending_list';
        }
        ajax_call_function({
            call_type: 'search_filter',
            tab_name: tab_name,
            search_form_data: $('form#search_filter_form').serializeArray(),
            limit: $('input#limit').val(),
            offset: $('input#offset').val(),
        }, 'search_filter');
    });

    $('div.search_filter_data').on('click', 'button.search_filter_reset', function () {

        var tab_name = $('li.tab_name_click.active_list').attr('tab-name');
        if (tab_name == '') {

            tab_name = 'pending_list';
        }
        ajax_call_function({
                call_type: 'search_filter',
                tab_name: tab_name,
                search_form_data: [],
                limit: $('input#limit').val(),
                offset: $('input#offset').val(),
            }, 'search_filter');
       
    });

    $('div.pq_paggination').on('click', 'li.paggination_number', function () {

        paggination_filter($(this).attr('limit'), $(this).attr('offset'));
    });

    $('div.pq_paggination').on('change', 'select.limit_change', function () {

        paggination_filter($('select.limit_change').val());
    });

    $('li.tab_name_click').click(function () {

        var tab_name = $(this).attr('tab-name');
        if (!$('li.' + tab_name).hasClass('active_list')) {
            $('li.active_list').removeClass('active_list');
            $(this).addClass('active_list');
            ajax_call_function({ 

                    call_type: 'change_tab', 
                    tab_name: tab_name,
                    search_form_data: $('form#search_filter_form').serializeArray(), 
                    limit: 10, 
                    offset: 0 
                }, 'change_tab');

            ajax_call_function({ call_type: 'pq_status_highchart', tab_name: get_tab_name() }, 'pq_status_highchart');              
        }
    });	

    $('tbody#pq_list_body').on('change', 'select.pq_dropdown', function () {

        ajax_call_function({
            call_type: 'update_pq_lead_stage',
            comp_mst_id: $(this).attr('comp_mst_id'),
            lead_stage: $(this).val(),
        }, 'update_pq_lead_stage');
    });

    $('a.add_member').click(function () {
        var next_count_number = $(this).attr('next_count_number');
        ajax_call_function({
            call_type: 'get_member_body',
            next_count_number: next_count_number,
        }, 'get_member_body');
        $(this).attr('next_count_number', ++next_count_number);
    });

    $('a.add_other_member').click(function () {
        var next_number = $(this).attr('next_number');
        ajax_call_function({
            call_type: 'get_other_member_body',
            next_number: next_number,
        }, 'get_other_member_body');
        $(this).attr('next_number', ++next_number);
    });

    $('tbody#add_member_body').on('click', 'a.delete_member_details', function () {
        $(this).closest('tr').remove();
    });

    $('tbody#add_other_member_body').on('click', 'a.delete_other_member_details', function () {
        $(this).closest('tr').remove();
    });

    $('tbody#add_member_body').on('click', 'a.member_followup_modal', function () {
        ajax_call_function({
            call_type: 'member_followup_modal',
            comp_dtl_id: $(this).attr('comp_dtl_id'),
            comp_mst_id: $(this).attr('comp_mst_id'),
        }, 'member_followup_modal');
    });

    $('tbody#add_other_member_body').on('click', 'a.other_member_followup_modal', function () {
        ajax_call_function({
            call_type: 'other_member_followup_modal',
            comp_dtl_id: $(this).attr('comp_dtl_id'),
            comp_mst_id: $(this).attr('comp_mst_id'),
        }, 'other_member_followup_modal');
    });

    $('div#followup_model').on('click', 'button.save_followup', function () {

        ajax_call_function({
            call_type: 'save_followup',
            comp_dtl_id: $(this).val(),
            add_followup_data_form: $('form#followup_form').serializeArray()
        }, 'save_followup');
    });

    $('tbody#add_member_body').on('click', 'a.delete_member', function () {
        // $('a.delete_member').click( function () {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover Member Details",
            icon: "warning",
            buttons: ["Cancel", "Delete"],
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    ajax_call_function({
                        call_type: 'delete_member',
                        comp_dtl_id: $(this).attr('comp_dtl_id'),
                        comp_mst_id: $(this).attr('comp_mst_id'),
                    }, 'delete_member');
                    swal({
                        title: "Member Deleted",
                        icon: "success",
                    });
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                } else {
                    swal({
                        title: "Member Not Delete",
                        icon: "info",
                    });
                }
            });
    });

    $('tbody#add_other_member_body').on('click', 'a.delete_other_member', function () {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover Member Details",
            icon: "warning",
            buttons: ["Cancel", "Delete"],
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    ajax_call_function({
                        call_type: 'delete_other_member',
                        comp_dtl_id: $(this).attr('comp_dtl_id'),
                        comp_mst_id: $(this).attr('comp_mst_id'),
                    }, 'delete_other_member');
                    swal({
                        title: "Member Deleted",
                        icon: "success",
                    });
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                } else {
                    swal({
                        title: "Member Not Delete",
                        icon: "info",
                    });
                }
            });
    });

    // save/update pq
    $('button.save_pq_form').click(function () {

        var comp_mst_id = $(this).attr('comp_mst_id');
        ajax_call_function({
            call_type: 'update_pq_details',
            comp_mst_id: comp_mst_id,
            client_details: $('form#add_pq_details').serializeArray(),
            client_member_details: $('form#member_details').serializeArray(),
            client_other_member_details: $('form#other_member_details').serializeArray(),
        }, 'update_pq_details');
    });

    //new add pq
    $('form#add_new_pq_details').on('change', 'select.country_id', function () {

        $('div.selected_region').hide();
        var val = $(this).val();
        if (val != '') {
            $('div.selected_region').show();
            $("#region_id").val($("#country_id option:selected").attr('region_id'));
        }
    });

    $(".customer_name").keyup(function () {
        var search = $(this).val();
        // alert(search);
        if (search != '') {
            $.ajax({
                type: 'POST',
                data: { search: search },
                url: "<?php echo base_url('client/searchClients');?>",
                success: function (res) {
                    var resp = $.parseJSON(res);
                    var leads = '';
                    Object.keys(resp).forEach(function (key) {

                        // leads += '<div style="padding: 5px; width: 90%; cursor: pointer"><a href="<?php echo base_url("lead_management/add_lead/"); ?>' + resp[key].id + '" target="_blank">' + resp[key].name + '</div></a>';                       

                        leads += '<div style="width: 90%;"><a href="<?php echo base_url("pre_qualification/update_pre_qualification/");?>' + resp[key].id + '" target="_blank">' + resp[key].name + '</a><div>';

                    });

                    if (leads != '') {

                        $("#customer_result_client").html(leads).show();
                    } else {

                        $("#customer_result_client").html(leads).hide();
                    }
                }
            });
        }
    });

    $('button.save_new_pq_form').click(function () {
        
        swal({
            title: "Please Check! client name invalid",
            text: "You can not add new client data in PQ",
            icon: "warning"
        })
        
    });

    //graph
    ajax_call_function({
        call_type: 'pq_status_highchart',
        tab_name: get_tab_name(),
    }, 'pq_status_highchart');

    //activity Module
    $('tbody#pq_list_body').on('click', 'a.pre_qualification_activity', function () {
        ajax_call_function({
            call_type: 'client_activity',
            comp_mst_id: $(this).attr('comp_mst_id'),
        }, 'client_activity');
    });

    $('div#activity_modal').on('click', 'button.save_activity_data', function () {

        ajax_call_function({
            call_type: 'save_client_activity',
            comp_mst_id: $(this).val(),
            add_activity_form: $('form#client_activity_form').serializeArray()
        }, 'save_client_activity');
    });

    $('div#activity_modal').on('click', 'button.delete_client_activity', function () {

        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover client activity Details",
            icon: "warning",
            buttons: ["Cancel", "Delete"],
            dangerMode: true,
        })        
        .then((willDelete) => {
            if (willDelete) {
                ajax_call_function({
                    call_type: 'delete_client_activity',
                    comp_mst_id: $(this).attr('comp_mst_id'),
                    activity_id: $(this).attr('activity_id'),
                }, 'delete_client_activity');
                swal({
                    title: "Activity Deleted",
                    icon: "success",
                });
                setTimeout(function () {
                    location.reload();
                }, 1000);
            } else {
                swal({
                    title: "Activity Not Delete",
                    icon: "info",
                });
            }
        });
    });
        
});

function ajax_call_function(data, call_type, url = "<?php echo base_url('pre_qualification/ajax_function'); ?>") {
    $.ajax({
        type: 'POST',
        data: data,
        url: url,
        dataType: 'JSON',
        success: function (res) {
            if (res.status == 'successful') {
                switch (call_type) {

                    case 'get_search_filter_div':
                        $('div.search_filter_data').html('').html(res.search_filter_data);
                        Pq_Js.init();
                    break; 

                    case 'change_tab':
                    case 'search_filter':
                    case 'paggination_filter':
                        $('tbody#pq_list_body').html('').html(res.pq_list_body);
                        $('div#search_filter_div').html('').html(res.pq_search_filter);
                        $('div.pq_paggination').html('').html(res.pq_paggination);
                        Pq_Js.init();
                        $('div#pq_list_process').hide();
                    break;

                    case 'update_pq_lead_data':
                        swal({
                            title: "Update added",
                            icon: "success",
                        });
                    break;

                    case 'get_member_body':
                        $('tbody#add_member_body').append(res.member_body_detail);
                    break;

                    case 'get_other_member_body':
                        $('tbody#add_other_member_body').append(res.other_member_body_detail);
                    break;

                    case 'member_followup_modal':
                    case 'other_member_followup_modal':
                        $('form#followup_form').html('').html(res.follow_up_html);
                        $('button[id=comp_dtl_id]').val(res.comp_dtl_id);
                        Pq_Js.init();
                    break;

                    case 'save_followup':
                        swal({
                            title: "Followup added",
                            icon: "success",
                        });
                        setTimeout(function () {
                            location.reload();
                        }, 1000);
                    break;

                    case 'update_pq_details':
                        swal({
                            title: "PQ Client Data are Updated",
                            icon: "success",
                        }); 
                        setTimeout(function () {

                            // location.reload();
                            location.href = "<?php echo base_url('pre_qualification/approve_pending_list');?>";
                        }, 1000);
                    break;

                    case 'pq_status_highchart':

                        pq_status_highchart(res.pq_status_highchart_data);
                    break;

                    case 'client_activity':
                        $('form#client_activity_form').html('').html(res.client_activity_html);
                        $('button[id=comp_mst_id]').val(res.comp_mst_id);
                        Pq_Js.init();
                    break;

                    case 'save_client_activity':
                        swal({
                            title: "Activity added",
                            icon: "success",
                        });
                    break;

                }
            }
        },
        beforesend: function (response) {
            switch (callType) {
                default:
                break;
            }
        }
    })
}

function paggination_filter(limit = $('input#limit').val(), offset = $('input#offset').val()) {

    // console.log($('li.tab_name_click.active_list').attr('tab-name'));
    $('div#pq_list_process').show();

    var search_filter = $('form#search_filter_form').serializeArray();
    if (search_filter == '') {

        search_filter = '';
    }
    var tab_name = $('li.tab_name_click.active_list').attr('tab-name');
    if (tab_name == '') {

        tab_name = 'pending_list';
    }
    ajax_call_function({
            call_type: 'paggination_filter',
            tab_name: tab_name,
            search_form_data: search_filter,
            limit: limit,
            offset: offset
    }, 'paggination_filter' );
}

function set_reset_spinner(obj, set_unset_flag = true) {
    if (set_unset_flag) {

        $(obj).addClass('kt-spinner');
        $(obj).addClass('kt-spinner--right');
        $(obj).addClass('kt-spinner--sm');
        $(obj).addClass('kt-spinner--light');
    } else {

        $(obj).removeClass('kt-spinner');
        $(obj).removeClass('kt-spinner--right');
        $(obj).removeClass('kt-spinner--sm');
        $(obj).removeClass('kt-spinner--light');
    }
}

var Pq_Js = function () {

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }

    // Private functions
    var datepicker = function () {
        // enable clear button 
        $('.pq_date_picker').datepicker({
            rtl: KTUtil.isRTL(),
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: true,
            templates: arrows,
            format: 'yyyy-mm-dd',
        });
    };

    return {
        // public functions
        init: function () {
            datepicker();
            $('.pq_select_picker').selectpicker();
            $('.pq_search_filter_form').selectpicker();

        }
    };
}();


function pq_status_highchart(highchart_data) {
   
    var tab_name= $('li.tab_name_click.active_list').attr('tab-name');
    var status_name;
    if(tab_name == 'pending_list'){
        status_name = "Pending";
    }else if (tab_name == 'approved_list'){
         status_name = "Approved";
    }
    Highcharts.chart('pq_status_highchart', {
        // Create the chart
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        accessibility: {
            announceNewData: {
                enabled: true
            }
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: status_name + ' Count'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>:<b>{point.y}</b><br/>'
        },

        series: [
            {
                name: status_name,
                colorByPoint: true,
                data: highchart_data
            }
        ]
    });
}

function get_tab_name() {

    if ($('li.pending_list').hasClass('active_list')) {
        return 'pending_list';
    } else if ($('li.approved_list').hasClass('active_list')) {
        return 'approved_list';
    } 
    return '';
}




			
