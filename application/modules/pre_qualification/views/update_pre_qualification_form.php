<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12 col-xl-12 order-lg-1 order-xl-1">
                <div class="kt-portlet">

                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="kt-font-brand flaticon2-line-chart"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                UPDATE PRE QUALIFICATION
                            </h3>
                        </div> 
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-wrapper">
                                <div class="kt-portlet__head-actions">
                                    <button type="submit" class="btn btn-primary save_pq_form" comp_mst_id="<?php echo $customer_data['id'];?>" >Update PQ</button>
                                </div>
                            </div>
                        </div>              
                    </div>
                    <div class="kt-portlet__body">
                        <form class="kt-form" id="add_pq_details">
                            <div class="form-group row">
								<div class="col-lg-4 form-group-sub">
								    <label class="form-control-label">Company Name:</label>
                                    <select class="form-control pq_select_picker" name="name" disabled="disabled">
                                    <option value="">Select</option>
                                        <?php foreach ($client_list as $single_client_detail) { ?>
                                            <option value="<?php echo $single_client_detail['id']; ?>"
                                            <?php 
                                                echo ($customer_data['id'] == $single_client_detail['id']) ? 'selected': ''; ?>>
                                                <?php echo $single_client_detail['name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-lg-4 form-group-sub">
                                    <label class="form-control-label">Country:</label>
                                    <select class="form-control pq_select_picker" name="country_id" disabled="disabled">
                                        <?php foreach ($country_list as $single_country) { ?>
                                            <option value="<?php echo $single_country['id']; ?>"
                                            <?php 
                                                echo ($customer_data['country_id'] == $single_country['id']) ? 'selected': ''; ?>>
                                                <?php echo $single_country['name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
								</div>
                                <div class="col-lg-4 form-group-sub">
                                    <label class="form-control-label">Region:</label>
                                    <select class="form-control pq_select_picker" name="region_id" disabled="disabled">
                                        <?php foreach ($region_list as $single_region) { ?>
                                            <option value="<?php echo $single_region['id']; ?>"
                                            <?php 
                                                echo ($customer_data['region_id'] == $single_region['id']) ? 'selected': ''; ?>>
                                                <?php echo $single_region['name']; ?>
                                            </option>;
                                        <?php } ?>
                                    </select>
								</div>                                
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-4 form-group-sub">
								    <label class="form-control-label">Handled By:</label>
									<select class="form-control pq_select_picker" name="assigned_to" disabled="disabled">
                                        <?php foreach ($assigned_to as $single_person) { ?>
                                            <option value="<?php echo $single_person['user_id']; ?>"
                                                <?php 
                                                echo ($customer_data['assigned_to'] == $single_person['user_id']) ? 'selected': '';
                                                ?>>
                                                <?php echo $single_person['name']; ?>
                                            </option>;
                                        <?php } ?>
                                    </select>
								</div>
                                <div class="col-lg-4 form-group-sub">
                                    <label class="form-control-label">Website</label>
                                    <input type="text" class="form-control" name="website" value="<?php echo  $customer_data['website'];?>" disabled="disabled">
                                </div>
                                <div class="col-lg-4 form-group-sub">
								    <label class="form-control-label">Lead Stage</label>
									<select class="form-control pq_select_picker" name="lead_stage" disabled="disabled">
                                    <option value="">Select</option>
                                        <?php foreach ($lead_stage as $lead_stage_detail) { ?>
                                            <option value="<?php echo $lead_stage_detail['lead_stage_id']; ?>"
                                            <?php 
                                                echo ($customer_data['lead_stage'] == $lead_stage_detail['lead_stage_id']) ? 'selected': ''; ?>>
                                                <?php echo $lead_stage_detail['stage_name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
								</div>                                
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-4 form-group-sub">
								    <label class="form-control-label">Lead Type</label>
									<select class="form-control pq_select_picker" name="lead_type" disabled="disabled">
                                        <option value="">Select</option>
                                        <?php foreach ($lead_type as $lead_type_detail) { ?>
                                            <option value="<?php echo $lead_type_detail['lead_type_id']; ?>"
                                            <?php 
                                                echo ($customer_data['lead_type'] == $lead_type_detail['lead_type_id']) ? 'selected': ''; ?>>
                                                <?php echo $lead_type_detail['type_name']; ?>
                                            </option>;
                                        <?php } ?>
                                    </select>
								</div>
								<div class="col-lg-4 form-group-sub">
								    <label class="form-control-label">Lead Status</label>
                                    <select class="form-control pq_select_picker" name="pq_client_status">
                                        <option value="Pending"<?php echo ($customer_data['pq_client_status'] == 'Pending') ? 'selected' : ''; ?>>Pending</option>

                                        <option value="Approved"<?php echo ($customer_data['pq_client_status'] == 'Approved') ? 'selected' : ''; ?>>Approved</option> 
                                    </select>
                                    </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-9 form-group-sub">
                                    <label class="form-control-label">Sales Notes</label>
                                    <textarea name="sales_notes" class="form-control"><?php echo $customer_data['sales_notes']; ?></textarea> 
                                </div> 
                            </div>
                        </form>

                        <form id="member_details">
                            <div class="form-group row">
                                <div class="col-lg-12" style="padding: 0px 0px 0px 0px !important;">
                                    <table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info">
                                        <thead>
                                            <tr role="row">
                                                <th colspan="12" style="text-align: center;">Member</th>     
                                                <th style="text-align: center;">
                                                    <a href="javascript:;" class="btn-sm btn btn-label-success btn-bold add_member" next_count_number="<?php echo $next_count_number;?>">
                                                        <i class="la la-plus">Add</i>
                                                    </a>
                                                </th>
                                            </tr>       
                                            <tr role="row">
                                                <th style="width: 3%;">Sr</th>
                                                <th style="width: 10%;">Name</th>
												<th style="width: 6%;">Designation</th>
												<th style="width: 6%;">Email</th>
								    			<th style="width: 6%;">Mobile</th>
												<th style="width: 6%;">Whatsapp</th>
												<th style="width: 3%;">Skype</th>
												<th style="width: 6%;">Telephone</th>
												<th style="width: 6%;">MainBuyer</th>
												<th style="width: 6%;">Last Contact Date</th>
												<th style="width: 6%;">Mode</th>
												<th style="width: 3%;">Decision Maker (%)</th>
												<th style="width: 4%;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="add_member_body">
                                           <?php if(!empty($member_detail)){ ?>
                                                <?php $count_no = 1;?>
                                                <?php foreach($member_detail as $member_detail_key => $single_member_detail){ ?>

                                                    <tr class="<?php echo $count_no;?>">

                                                        <td><?php echo ++$member_detail_key; ?><br>
                                                            <input type="text" class="form-control" name="comp_dtl_id_<?php echo $count_no;?>" value="<?php echo $single_member_detail['comp_dtl_id'];?>" hidden>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="member_name_<?php echo $count_no;?>"  value="<?php echo $single_member_detail['member_name'];?>">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="designation_<?php echo $count_no;?>" value="<?php echo $single_member_detail['designation'];?>">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="email_<?php echo $count_no;?>" value="<?php echo $single_member_detail['email'];?>">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="mobile_<?php echo $count_no;?>" value="<?php echo $single_member_detail['mobile'];?>">
                                                        </td>
                                                        <td>
                                                            <select class="form-control" name="is_whatsapp_<?php echo $count_no;?>">
                                                                <option value="No"<?php echo ($single_member_detail['is_whatsapp'] =='No') ? 'selected' : '';?>>No</option>
                                                                <option value="Yes"<?php echo ($single_member_detail['is_whatsapp'] =='Yes') ? 'selected' : '';?>>Yes</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="skype_<?php echo $count_no;?>" value="<?php echo $single_member_detail['skype'];?>">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="telephone_<?php echo $count_no;?>" value="<?php echo $single_member_detail['telephone'];?>">
                                                        </td>
                                                        <td>
                                                            <select class="form-control" name="main_buyer_<?php echo $count_no;?>">
                                                                <option value="No"<?php echo ($single_member_detail['main_buyer'] =='No') ? 'selected' : ''; ?>>No</option>
                                                                <option value="Yes"<?php echo ($single_member_detail['main_buyer'] =='Yes') ? 'selected' : ''; ?>>Yes</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" value="<?php echo $single_member_detail['connected_on'];?>" readonly>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" value="<?php echo $single_member_detail['connect_mode'];?>" readonly>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="decision_maker_<?php echo $count_no;?>" value="<?php echo $single_member_detail['decision_maker'];?>">
                                                        </td>
                                                        <td>
                                                        
                                                            <a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon btn-icon-md member_followup_modal" data-toggle="modal" data-target="#followup_model" comp_dtl_id="<?php echo $single_member_detail['comp_dtl_id'];?>" comp_mst_id="<?php echo $single_member_detail['comp_mst_id'];?>" title="Follow Up">
                                                                <i class="la la-comment"></i>
                                                            </a>

                                                            <a href="javascript:;" data-repeater-delete="" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_member" comp_dtl_id="<?php echo $single_member_detail['comp_dtl_id']; ?>" comp_mst_id="<?php echo $single_member_detail['comp_mst_id']; ?>" count_no="<?php echo $count_no;?>">
                                                                <i class="la la-trash"></i>
                                                            </a>
                                                        </td>             
                                                    </tr>
                                                <?php $count_no++;}?>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                        
                        <form id="other_member_details">
                            <div class="form-group row">
                                <div class="col-lg-12" style="padding: 0px 0px 0px 0px !important;">
                                    <table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info">
                                        <thead>
                                            <tr role="row">
                                                <th colspan="10" style="text-align: center;">Other Member</th>     
                                                <th style="text-align: center;">
                                                    <a href="javascript:;" class="btn-sm btn btn-label-success btn-bold add_other_member" next_number="<?php echo $next_number;?>">
                                                        <i class="la la-plus">Add</i>
                                                    </a>
                                                </th>
                                            </tr>       
                                            <tr role="row">
                                                <th width="3%">Sr</th>
                                                <th width="15%">Name</th>
												<th width="15%">Designation</th>
												<th width="15%">Email</th>
												<th width="15%">Mobile</th>
												<th width="6%">Whatsapp</th>
												<th width="7%">Skype</th>
												<th width="7%">Telephone</th>
												<th width="15%">Last Contact Date</th>
			    								<th width="15%">Mode</th>
												<th width="8%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="add_other_member_body">
                                            <?php if(!empty($other_member_detail)){ ?>
                                                <?php $next_count = 1;?>
                                                <?php foreach($other_member_detail  as $other_member_detail_key => $single_other_member_detail){ ?>

                                                    <tr class="<?php echo $next_count;?>">
                                                        <td><?php echo ++$other_member_detail_key;?><br>
                                                            <input type="text" class="form-control" name="comp_dtl_id_<?php echo $next_count;?>" value="<?php echo $single_other_member_detail['comp_dtl_id'];?>" hidden>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="member_name_<?php echo $next_count;?>" value="<?php echo $single_other_member_detail['member_name'];?>">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="designation_<?php echo $next_count;?>" value="<?php echo $single_other_member_detail['designation'];?>">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="email_<?php echo $next_count;?>" value="<?php echo $single_other_member_detail['email'];?>">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="mobile_<?php echo $next_count;?>" value="<?php echo $single_other_member_detail['mobile'];?>">
                                                        </td>
                                                        <td>
                                                            <select class="form-control" name="is_whatsapp_<?php echo $next_count;?>">
                                                                <option value="No" <?php echo ($single_other_member_detail['is_whatsapp'] =='No') ? 'selected' : ''; ?>>No</option>
                                                                <option value="Yes" <?php echo ($single_other_member_detail['is_whatsapp'] =='Yes') ? 'selected' : ''; ?>>Yes</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="skype_<?php echo $next_count;?>" value="<?php echo $single_other_member_detail['skype'];?>">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="telephone_<?php echo $next_count;?>" value="<?php echo $single_other_member_detail['telephone'];?>">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" value="<?php echo $single_other_member_detail['connected_on'];?>" readonly>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" value="<?php echo $single_other_member_detail['connect_mode'];?>" readonly>
                                                        </td>
                                                        <td>                                                    
                                                            <a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon btn-icon-md other_member_followup_modal" data-toggle="modal"data-target="#followup_model" comp_dtl_id="<?php echo $single_other_member_detail['comp_dtl_id'];?>" comp_mst_id="<?php echo $single_other_member_detail['comp_mst_id'];?>" title="Follow Up">
                                                                <i class="la la-comment"></i>
                                                            </a>
                                                       
                                                            <a href="javascript:;" data-repeater-delete="" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_other_member" comp_dtl_id="<?php echo $single_other_member_detail['comp_dtl_id'];?>" comp_mst_id="<?php echo $single_other_member_detail['comp_mst_id'];?>" next_count="<?php echo $next_count;?>">
                                                                <i class="la la-trash"></i>
                                                            </a>

                                                        </td>             
                                                    </tr>
                                                <?php $next_count++; }?>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="followup_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
         	<div class="modal-content">
	            <div class="modal-header">
	               <h5 class="modal-title" id="exampleModalLabel">Follow Up</h5>
	               <button type="reset" class="close" data-dismiss="modal" aria-label="Close">
	               </button>
	            </div>
	            <div class="modal-body">
		            <form id="followup_form">	            	

						
		            </form>
	            </div>
	            <div class="modal-footer">
	               	<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
	            	<!-- <button class="btn btn-success save_primary_followup" type="reset" id="comp_dtl_id" style="float: right;">Submit</button> -->
	            </div>
        	</div>
    	</div>
   	</div>

</div>
<!-- end:: Content -->