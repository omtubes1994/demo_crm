<h6>Client Contact Details:</h6>
<div>
	<table class="table table-bordered">
	   <thead>
		   <tr>
			   	<th>Email ID</th>
			   	<th>Mobile Number</th>
		   </tr>
		</thead>
		<tbody>
		<?php if(!empty($client_details)){ ?>
			<?php foreach ($client_details as $client_details_key => $single_client_details) { ?>
			  	<tr>
                    <?php if(!empty($single_client_details['email'] && $single_client_details['mobile'])){ ?>
                        <td><?php echo $single_client_details['email']; ?></td>
                        <td><?php echo $single_client_details['mobile']; ?></td>
                    <?php }?>
			   </tr>
			<?php } ?>
		<?php } ?>
		</tbody>
	</table>
</div>

<hr/>
<h5>Add New Activity</h5>
<hr/>
<form>
    <div class="row">
        <input type="text" class="form-control" name="comp_mst_id" value="<?php echo $comp_mst_id; ?>" hidden/>
        <div class="col-md-4 form-group">
            <label for="contact_date">Activity Type</label>
            <input type="text" class="form-control" name="activity_type" value="">
        </div>
        <div class="col-md-4 form-group">
            <label for="contact_date">Activity Date</label>
            <input type="date" class="form-control pq_date_picker" name="activity_date" value="<?php echo date('Y-m-d', strtotime($activity_date));?>">
        </div>
        <div class="col-md-4 form-group">
            <label for="contact_date">Activity Description</label>
            <textarea type="text" class="form-control" name="activity_description"></textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 form-group">
            <label for="contact_date">Client Comments Date</label>
            <input type="date" class="form-control pq_date_picker" name="client_comments_date" value="<?php echo date('Y-m-d', strtotime($client_comments_date))?>">
        </div>
        <div class="col-md-4 form-group">
            <label for="contact_date">Client Comments</label>
            <textarea type="text" class="form-control" name="client_comments"></textarea>
        </div>
        <div class="col-md-4 form-group">
            <label for="contact_date">Notes / Strategies</label>
            <textarea type="text" class="form-control" name="activity_notes"></textarea>
        </div>  		               
    </div>
    <div class="row">
        <div class="col-md-12 forms-group" style="justify-content: right;">
		    <button class="btn btn-success save_activity_data" type="reset" id="comp_mst_id" style="float: right;">Submit</button>
	    </div>
    </div>
</form>
<hr/>
<h5>Activity List</h5>
<hr/>
<table class="table table-bordered">
    <thead>
        <tr>
            <th>Sr. #</th>
            <th>Acitivity Type</th>
            <th>Activity Date</th>
            <th>Actitivy Description</th>
            <th>Comments Date</th>
            <th>Client Comments</th>
            <th>Notes / Strategis</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody id="client_activity_body">
        <?php if(!empty($activity_details)){ ?>
			<?php foreach ($activity_details as $activity_details_key => $single_activity_details) { ?>
			  	<tr>
                    <td><?php echo $activity_details_key+1; ?></td>
				   	<td><?php echo $single_activity_details['activity_type'];?></td>
				   	<td><?php echo date('d-m-Y', strtotime($single_activity_details['activity_date']));?></td>
				   	<td><?php echo $single_activity_details['activity_description'];?></td>
				   	<td><?php echo date('d-m-Y', strtotime($single_activity_details['client_comments_date']));?></td>
				   	<td><?php echo $single_activity_details['client_comments'];?></td>
				   	<td><?php echo $single_activity_details['activity_notes'];?></td>
				   	<td>
                        <button type="reset" class="btn btn-danger delete_client_activity" activity_id="<?php echo $single_activity_details['activity_id'];?>" comp_mst_id="<?php echo $single_activity_details['comp_mst_id'];?>">Delete</button>
                    </td>
			   </tr>
			<?php } ?>
		<?php } ?>
    </tbody>
</table>