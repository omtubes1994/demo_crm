<?php if(!empty($pq_list)){ ?>
    <?php foreach($pq_list as $pq_list_key => $single_pq_list_value){ ?>
       
        <tr role="row" class="<?php echo (($pq_list_key+1)/2 == 00)?'even':'odd'?>">
            <td><?php echo $pq_list_key+1;?></td>
            <td class="kt-align">
				<img class="kt-widget3__img rounded-circle" src="assets/media/flags/<?php echo $single_pq_list_value['flag_name'];?>" title="<?php echo $single_pq_list_value['country_name']; ?>" style="width: 30px" alt="">
                <strong style="margin-left: 4px; font-size: 13px;"> <?php echo $single_pq_list_value['customer_name'];?></strong></br>
                <span>
				    <abbr>
                        <?php if(!empty($single_pq_list_value['region_name'])) { ?>
                            <?php echo $single_pq_list_value['region_name'];?>
                        <?php } ?>			    
				    </abbr>
				</span>              
			</td>
            <td>  
                <select class="pq_dropdown" name="lead_stage" comp_mst_id=<?php echo $single_pq_list_value['id']; ?>>

                    <option value="1" <?php echo ($single_pq_list_value['lead_stage'] == '1') ? 'selected': '';?>> Stage 1</option>

                    <option value="2" <?php echo ($single_pq_list_value['lead_stage'] == '2') ? 'selected': '';?>> Stage 2</option>
                    
                    <option value="3" <?php echo ($single_pq_list_value['lead_stage'] == '3') ? 'selected': '';?>> Stage 3</option>
                    
                    <option value="4" <?php echo ($single_pq_list_value['lead_stage'] == '4') ? 'selected': '';?>> Stage 4</option>
                    
                    <option value="5" <?php echo ($single_pq_list_value['lead_stage'] == '5') ? 'selected': '';?>> Stage 5</option>
                    
                    <option value="6" <?php echo ($single_pq_list_value['lead_stage'] == '6') ? 'selected': '';?>> Stage 6</option>
                    
                    <option value="0" <?php echo ($single_pq_list_value['lead_stage'] == '0') ? 'selected': '';?> disabled> Stage 0</option>
                </select> 
                <br/>                    
                <span>
				    <abbr>
                        <?php if(!empty($single_pq_list_value['lead_name'])) { ?>
                            
                            <?php echo $single_pq_list_value['lead_name'];?>
                        <?php } ?>			    
				    </abbr>
				</span>
            </td>
            <td>
                <span style="font-size: 14px;">
                    <?php echo $single_pq_list_value['user_name'];?>
                </span>
			</td>
            <td>  
                <p> 
                    <?php if(!empty($single_pq_list_value['website'])){ ?>
                        <span class="contact_span">
                            <span class="email">
                                <i class="fas fa-envelope"></i>
                                <?php echo $single_pq_list_value['website'];?>
                            </span>
                        </span><br/>
                    <?php } ?>
                </p>
            </td>                      
            <td>  
                <span style="font-size: 14px;">
                    <?php echo $single_pq_list_value['comments'];?>
                </span>           
                <br/><br/>
                <?php if(!empty($single_pq_list_value['last_contacted'])){ ?>
                    <span style="font-size: 10px;">
                        <?php echo $single_pq_list_value['last_contacted'];?>
                    </span>
                <?php } ?>
            </td>
            <td>
                <a href="<?php echo base_url('pre_qualification/update_pre_qualification/'.$single_pq_list_value['id']);?>" target="_blank" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">
					<i class="la la-info-circle"></i>
				</a>

                <a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon btn-icon-md pre_qualification_activity" data-toggle="modal" data-target="#activity_modal" comp_mst_id="<?php echo $single_pq_list_value['id'];?>" title="CLient Details & Activity List">
                    <i class="la la-list-alt"></i>
                </a>
                                                       
            </td>
        <tr>
    <?php } ?>
<?php } ?>