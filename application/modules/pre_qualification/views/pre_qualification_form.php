<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12 col-xl-12 order-lg-1 order-xl-1">
                <div class="kt-portlet">

                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="kt-font-brand flaticon2-line-chart"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                ADD PRE QUALIFICATION
                            </h3>
                        </div> 
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-wrapper">
                                <div class="kt-portlet__head-actions">
                                    <button type="submit" class="btn btn-primary save_new_pq_form" comp_mst_id="">Save PQ</button>
                                </div>
                            </div>
                        </div>              
                    </div>
                    <div class="kt-portlet__body">
                        <form class="kt-form" id="add_new_pq_details">
                            <div class="form-group row">
                                <div class="col-lg-4 form-group-sub">
								    <label class="form-control-label">Company</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control customer_name" id="customer_name"  name="name" value="<?php echo $customer_data['name'];?>">
                                    </div>
                                    <div class="row">
										<div class="col-12">
                                            <div id="customer_result_client" style="background-color: #fff; z-index: 100; position: absolute; border: 1px solid; width: 80%; max-height: 100px; height: 100px; overflow-y: scroll; display: none;">
											</div>
										</div>
									</div>                                    
								</div>
                                <div class="col-lg-4 form-group-sub">
                                    <label class="form-control-label">Country:</label>
                                    <select class="form-control pq_select_picker country_id" id="country_id" name="country_id">
                                        <option value="">Select</option>
                                        <?php foreach ($country_list as $single_country) { ?>
                                            <option value="<?php echo $single_country['id']; ?>" 
                                                    region_id="<?php echo $single_country['region_id'];?>"
                                            <?php echo ($customer_data['country_id'] == $single_country['id']) ? 'selected': ''; ?>>
                                                <?php echo $single_country['name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
								</div>
                                <div class="col-lg-4 form-group-sub  selected_region" style="display:<?php echo ($customer_data['country_id'] == 'selected') ? 'block' : 'none';?>">
                                    <label class="form-control-label">Region</label>
                                    <select class="form-control" id="region_id" name="region_id">
                                        <?php foreach ($region_list as $single_region) { ?>
                                            <option value="<?php echo $single_region['id'];?>">
                                                <?php echo $single_region['name'];?>
                                            </option>
                                        <?php } ?>
								    </select>
								</div>                               
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-4 form-group-sub">
								    <label class="form-control-label">Handled By:</label>
									<select class="form-control pq_select_picker" name="assigned_to">
                                        <option value="">Select</option>
                                        <?php foreach ($assigned_to as $single_person) { ?>
                                            <option value="<?php echo $single_person['user_id']; ?>"
                                                <?php echo ($customer_data['assigned_to'] == $single_person['user_id']) ? 'selected': ''; ?>>
                                                <?php echo $single_person['name']; ?>
                                            </option>;
                                        <?php } ?>
                                    </select>
								</div>
                                <div class="col-lg-4 form-group-sub">
                                    <label class="form-control-label">Website</label>
                                    <input type="text" class="form-control" name="website" value="<?php echo $customer_data['website'];?>">
                                </div>
                                <div class="col-lg-4 form-group-sub">
								    <label class="form-control-label">Lead Stage</label>
									<select class="form-control pq_select_picker" name="lead_stage">
                                    <option value="">Select</option>
                                        <?php foreach ($lead_stage as $lead_stage_detail) { ?>
                                            <option value="<?php echo $lead_stage_detail['lead_stage_id']; ?>">
                                                <?php echo $lead_stage_detail['stage_name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
								</div>                                
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-4 form-group-sub">
								    <label class="form-control-label">Lead Type</label>
									<select class="form-control pq_select_picker" name="lead_type">
                                        <option value="">Select</option>
                                        <?php foreach ($lead_type as $lead_type_detail) { ?>
                                            <option value="<?php echo $lead_type_detail['lead_type_id']; ?>"
                                                <?php echo ($customer_data['lead_type'] == $lead_type_detail['lead_type']) ? 'selected': ''; ?>>
                                                <?php echo $lead_type_detail['type_name']; ?>
                                            </option>;
                                        <?php } ?>
                                    </select>
								</div>
								<div class="col-lg-4 form-group-sub">
								    <label class="form-control-label">Lead Status</label>
                                    <select class="form-control pq_select_picker" name="pq_client_status">
                                        <option value="Pending">Pending</option>
                                        <option value="Approved">Approved</option> 
                                    </select>
                                    </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-9 form-group-sub">
                                    <label class="form-control-label">Sales Notes</label>
                                    <textarea class="form-control" name="sales_notes"><?php echo $customer_data['sales_notes'];?></textarea> 
                                </div> 
                            </div>
                        </form>

                        <form id="member_details">
                            <div class="form-group row">
                                <div class="col-lg-12" style="padding: 0px 0px 0px 0px !important;">
                                    <table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info">
                                        <thead>
                                            <tr role="row">
                                                <th colspan="12" style="text-align: center;">Member</th>     
                                                <th style="text-align: center;">
                                                    <a href="javascript:;" class="btn-sm btn btn-label-success btn-bold add_member" next_count_number="<?php echo $next_count_number;?>">
                                                        <i class="la la-plus">Add</i>
                                                    </a>
                                                </th>
                                            </tr>       
                                            <tr role="row">
                                                <th style="width: 3%;">Sr</th>
                                                <th style="width: 10%;">Name</th>
												<th style="width: 6%;">Designation</th>
												<th style="width: 6%;">Email</th>
								    			<th style="width: 6%;">Mobile</th>
												<th style="width: 6%;">Whatsapp</th>
												<th style="width: 3%;">Skype</th>
												<th style="width: 6%;">Telephone</th>
												<th style="width: 6%;">MainBuyer</th>
												<th style="width: 6%;">Last Contact Date</th>
												<th style="width: 6%;">Mode</th>
												<th style="width: 3%;">Decision Maker (%)</th>
												<th style="width: 4%;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="add_member_body">
                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                        
                        <form id="other_member_details">
                            <div class="form-group row">
                                <div class="col-lg-12" style="padding: 0px 0px 0px 0px !important;">
                                    <table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info">
                                        <thead>
                                            <tr role="row">
                                                <th colspan="10" style="text-align: center;">Other Member</th>     
                                                <th style="text-align: center;">
                                                    <a href="javascript:;" class="btn-sm btn btn-label-success btn-bold add_other_member" next_number="<?php echo $next_number;?>">
                                                        <i class="la la-plus">Add</i>
                                                    </a>
                                                </th>
                                            </tr>       
                                            <tr role="row">
                                                <th width="3%">Sr</th>
                                                <th width="15%">Name</th>
												<th width="15%">Designation</th>
												<th width="15%">Email</th>
												<th width="15%">Mobile</th>
												<th width="6%">Whatsapp</th>
												<th width="7%">Skype</th>
												<th width="7%">Telephone</th>
												<th width="15%">Last Contact Date</th>
			    								<th width="15%">Mode</th>
												<th width="8%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="add_other_member_body">
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                                             
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- end:: Content -->