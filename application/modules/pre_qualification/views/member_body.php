<tr class="<?php echo $next_count_number;?>">
    <td><?php echo $next_count_number;?><br>
        <input type="text" class="form-control" name="comp_dtl_id_<?php echo $next_count_number;?>" hidden>
    </td>
    <td>
        <input type="text" class="form-control" name="member_name_<?php echo $next_count_number;?>">
    </td>
    <td>
        <input type="text" class="form-control" name="designation_<?php echo $next_count_number;?>">
    </td>
    <td>
        <input type="text" class="form-control" name="email_<?php echo $next_count_number;?>">
    </td>
    <td>
        <input type="text" class="form-control" name="mobile_<?php echo $next_count_number;?>">
    </td>
    <td>
        <select class="form-control" name="is_whatsapp_<?php echo $next_count_number;?>">
            <option value="">Select</option>
            <option value="No">No</option>
            <option value="Yes">Yes</option>                                                      
        </select>
    </td>
    <td>
        <input type="text" class="form-control" name="skype_<?php echo $next_count_number;?>" >
    </td>
    <td>
        <input type="text" class="form-control" name="telephone_<?php echo $next_count_number;?>">
    </td>
    <td>
        <select class="form-control" name="main_buyer_<?php echo $next_count_number;?>">
            <option value="">Select</option>
            <option value="No">No</option>
            <option value="Yes">Yes</option>                                     
        </select>
    </td>
    <td>
        <input type="text" class="form-control" value="<?php echo $connected_on;?>" readonly>
    </td>
    <td>
        <input type="text" class="form-control" value="<?php echo $connect_mode;?>" readonly>
    </td>
    <td>
        <input type="text" class="form-control" name="decision_maker_<?php echo $next_count_number;?>">
    </td>
    <td>       
        <a href="javascript:;" data-repeater-delete="" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_member_details" title="Delete" count_no="<?php echo $next_count_number;?>">
            <i class="la la-trash"></i>
        </a>
    </td>             
</tr>
