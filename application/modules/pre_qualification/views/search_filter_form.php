<form class="kt-form kt-form--fit kt-margin-b-20" id="search_filter_form">
    <div id="search_filter_div">
        <div class="row kt-margin-b-20">
            <div class="col-lg-4 kt-margin-b-10-tablet-and-mobile">
                <label>Company Name :</label>
                <select class="form-control pq_select_picker" name="company_name" data-live-search = "true">
                    <option value="">Select</option>
                        <?php foreach($company_list as $single_company_name) { ?>
                        <option value="<?php echo $single_company_name['name']; ?>" 
                            <?php 
                                echo ($search_filter['company_name'] == $single_company_name['name']) ? 'selected': ''; ?>>
                            <?php echo $single_company_name['name']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-lg-4 kt-margin-b-10-tablet-and-mobile">
                <label>Country :</label>
                <select class="form-control pq_select_picker" name="country_id" data-live-search = "true"  multiple>
                    <option value="">Select Country</option>
                    <?php foreach($country_list as $single_country) { ?>
                        <option value="<?php echo $single_country['id']; ?>"
                            <?php 
                                echo ($search_filter['country_name'] == $single_country['name']) ? 'selected': ''; ?>>
                            <?php echo $single_country['name']; ?>
                        </option>
                    <?php } ?>                              
                </select>
            </div>
            <div class="col-lg-4 kt-margin-b-10-tablet-and-mobile">
                <label>Region :</label>
                <select class="form-control pq_select_picker" name="region_id" data-live-search = "true" multiple>
                    <option value="">Select region</option>
                    <?php foreach($region_list as $single_region) { ?>
                        <option value="<?php echo $single_region['id']; ?>"
                            <?php 
                                echo ($search_filter['region_name'] == $single_region['name']) ? 'selected': ''; ?>>
                            <?php echo $single_region['name']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>           
        </div>
        <div class="row kt-margin-b-20">
            <div class="col-md-4 kt-margin-b-10-tablet-and-mobile">
                <label>Sales Person :</label>
                <select class="form-control pq_select_picker" name="assigned_to" data-live-search = "true" multiple>
                    <option value="">Select</option>
                    <?php foreach($user_list as $single_user) { ?>
                        <option value="<?php echo $single_user['user_id']; ?>" 
                            <?php 
                                echo ($search_filter['assigned_to'] == $single_user['user_id']) ? 'selected': ''; ?>>
                            <?php echo $single_user['name']; ?>
                        </option>
                    <?php } ?> 
                </select>
            </div> 
            <div class="col-lg-4 kt-margin-b-10-tablet-and-mobile">
                <label>Lead Type :</label>
                <select class="form-control pq_select_picker" name="lead_type" multiple>
                    <option value="">Select Type</option>
                    <?php foreach($lead_type as $single_lead_type) {?>
                        <option value="<?php echo $single_lead_type['lead_type_id']; ?>" 
                            <?php 
                                echo ($search_filter['lead_type'] == $single_lead_type['lead_type_id']) ? 'selected': ''; ?>>
                            <?php echo $single_lead_type['type_name']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-lg-4 kt-margin-b-10-tablet-and-mobile">
                <label>Lead Stage :</label>
                <select class="form-control pq_select_picker" name="lead_stage" multiple>
                    <option value="">Select Stage</option>                    
                    <?php foreach($lead_stage as $single_lead_stage) { ?>
                        <option value="<?php echo $single_lead_stage['lead_stage_id']; ?>" 
                            <?php 
                                echo ($search_filter['lead_stage'] == $single_lead_stage['lead_stage_id']) ? 'selected': ''; ?>>
                            <?php echo $single_lead_stage['stage_name']; ?>
                        </option>
                    <?php } ?>          
                </select>
            </div>
        </div>                  
        <div class="kt-separator kt-separator--md kt-separator--dashed"></div>
            <div class="row">
                <div class="col-lg-12">
                    <button class="btn btn-primary btn-brand--icon search_filter_submit" type="reset">
                        <span>
                            <i class="la la-search"></i>
                            <span>Search</span>
                        </span>
                    </button>
                    &nbsp;&nbsp;
                    <button class="btn btn-secondary btn-secondary--icon search_filter_reset" type="reset">
                        <span>
                            <i class="la la-close"></i>
                            <span>Reset</span>
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>