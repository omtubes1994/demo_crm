<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

    <style type="text/css">
        .kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
            padding-top: 4% !important;
        }
        .layer-white{
            display: none;
            position: absolute;
            top: 0em !important;
            left: 0em !important;
            width: 100%;
            height: 100%;
            text-align: center;
            vertical-align: middle;
            background-color: rgba(255, 255, 255, 0.55);
            opacity: 1;
            line-height: 1;
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both;
            -webkit-animation-duration: 0.5s;
            animation-duration: 0.5s;
            -webkit-transition: background-color 0.5s linear;
            transition: background-color 0.5s linear;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            will-change: opacity;
            z-index: 9;
        }
        .div-loader{
            position: absolute;
            top: 50%;
            left: 50%;
            margin: 0px;
            text-align: center;
            z-index: 1000;
            -webkit-transform: translateX(-50%) translateY(-50%);
            transform: translateX(-50%) translateY(-50%);
        }
        .kt-spinner:before {
            width: 50px;
            height: 50px;
            margin-top: -10px;
        }
        tbody#quality_listing tr:hover {
            background-color: gainsboro;
        }
        tbody#quality_listing tr:hover .first_div {
            border-left: 0.25rem solid #767676 !important;
        }
        tbody#quality_listing td {
            font-style: normal;
            font-size: 15px;
            border: 0.05rem solid gainsboro;
        }
        tbody#quality_listing td span{
            /*width:200px !important;*/
            display: inline-flex;
            width: 100%;
        }
        tbody#quality_listing td span i{
            cursor: pointer;
        }
        tbody#quality_listing td span abbr{
            width: 100%;
        }    
        tbody#quality_listing td span abbr em{
            display: block;
            color: #898989;
            margin: 3px 0 3px 0;
            font-style: normal;
            font-family: Helvetica Neue,Helvetica,Arial,sans-serif
        }
        tbody#quality_listing td span abbr em i{
            font-weight: 500;
            color: #898989;
            margin: 3px 0 3px 0;
            font-style: normal;
            font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
        }
        thead#production_header tr th{
            background: gainsboro;
            color: #767676;
            font-size: 14px;
            font-family: 'latomedium';
            padding: 10px 20px;
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        }
        ul.menu-tab {
            margin: 0px;
            padding: 0px;
            border: 1px solid #E7E7E7;
            /*border-radius: 50px;*/
            overflow: hidden;
            font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
            font-size: 14px;
            line-height: 20px;
            color: #333;
        }
        ul.menu-tab li{
            width: calc(100%/2) !important;
            display: inline;
            text-align: center;
            float: left;
        }
        ul.menu-tab li.active_list{
            border-bottom: 0.25rem solid #767676 !important;
        }
        ul.menu-tab li a{
            cursor: pointer;
            display: inline-block;
            outline: none;
            text-align: center;
            width: 100%;
            background: #F5F5F5;
            /*border-right: 2px solid #fff;*/
            color: #767676;
            font-size: 15px;
            font-family: 'latomedium';
            background-color: gainsboro;
            padding: 3px 3px 12px 3px;
        }
        ul.menu-tab li a i{
            display: inline-block;
            width: 35px;
            height: 28px;
            font-style: normal;
            background-size: 100%;
            position: relative;
            top: 6px;
        }
     
    </style>

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        PRE - QUALIFICATION LIST
                    </h3>
                </div> 
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">
                            <a href="javascript:;" class="btn btn-brand btn-elevate btn-icon-sm add_search_filter" button_value = "show">
                                Add Search Filter
                            </a>
                        </div>
                    </div>
                </div>              
            </div>           
            <div class="kt-portlet__body search_filter_data" style="display: none;">
               
            </div>
        </div>

        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body">
                <!--Begin::Timeline 3 -->   
                <div class="kt-scroll" data-scroll="true" style="height: 400px">
                    <div id="pq_status_highchart"></div>
                </div>
                <!--End::Timeline 3 -->
            </div>
        </div>

        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body">
                <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="menu-tab kt-font-bolder" style="">
                                <li class="tab_name_click pending_list active_list" tab-name="pending_list">
                                    <a>
                                        <i class="custom_heading_icon active_issues_tabs_icon">
                                            <img src="assets/media/icons/svg/Communication/Clipboard-list.svg"/>
                                        </i>
                                        <span style="top: 6px;position: relative;">Pending List</span>
                                    </a>                                            
                                </li>
                                <li class="tab_name_click approved_list" tab-name="approved_list" style="padding: 0px 0px 0px 10px;">
                                    <a>
                                        <i class="custom_heading_icon active_issues_tabs_icon">
                                            <img src="assets/media/icons/svg/Communication/Clipboard-check.svg"/>
                                        </i>
                                        <span style="top: 6px;position: relative;">Approved List</span>
                                    </a>
                                </li>                                     
                            </ul>							
                        </div>

                        <div class="col-sm-12">
                            <table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th width="20%">Lead Details</th>
                                        <th width="10%">Lead Stage</th>
                                        <th width="15%">Lead Team</th>
                                        <!-- <th width="10%">ID & Password</th> -->
                                        <th width="25%">Website</th>
                                        <th width="15%">Comments</th>
                                        <th width="5%">Actions</th>
                                    </tr>
                                </thead>
                                <tbody id="pq_list_body">
                                    <?php $this->load->view('pre_qualification/pre_qualification_list_body');?>
                                </tbody>
                            </table> 
                            <div id="pq_list_process" class="layer-white">
                                <div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
                            </div>                            
                        </div>
                    </div>
                    <div class="row pq_paggination">
                        <?php $this->load->view('pre_qualification/paggination'); ?>
                    </div>
                </div>
            </div>        
        </div>
    </div>
             
    <div class="modal fade" id="activity_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">CLient Details & Activity List</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                   <form id="client_activity_form">

                   
                   </form>
                </div>
                <div class="modal-footer">
	               	<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
	            	<!-- <button class="btn btn-success save_primary_followup" type="reset" id="comp_dtl_id" style="float: right;">Submit</button> -->
	            </div>
            </div>
        </div>
    </div>

</div>
<!-- end:: Content -->
