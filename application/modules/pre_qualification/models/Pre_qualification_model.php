<?php 
Class Pre_qualification_model extends CI_Model{
	
	function __construct(){
		parent::__construct();
		$CI = &get_instance();
	}


	public function	get_pq_listing_data($where_array, $order_by_array, $limit, $offset){
		
		$return_array = array();
		$this->db->select('SQL_CALC_FOUND_ROWS customer_mst.*, customer_mst.name customer_name, 
		region_mst.name region_name, country_mst.name country_name, country_flags.flag_name', FALSE);
		
		$this->db->join('country_mst', 'country_mst.id = customer_mst.country_id', 'left');
		$this->db->join('region_mst', 'region_mst.id = customer_mst.region_id', 'left');
		$this->db->join('country_flags', 'country_flags.country = country_mst.name', 'left');
					
		$this->db->where($where_array, null, false);
		$this->db->order_by('customer_mst.lead_stage desc, customer_mst.name asc');
		$return_array['pq_list'] = $this->db->get('customer_mst', $limit, $offset)->result_array();
		// echo "<pre>";print_r($this->db->last_query());echo"</pre><hr>";exit;
		$return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows")->row_array();
		return $return_array;		
	}

	public function get_all_data($select, $where_array, $table_name, $result_type = 'result_array') {

		$this->db->select($select);
		$this->db->where($where_array);
		return $this->db->get($table_name)->$result_type();		
	}

	public function get_all_conditional_data_sales_db($select_string = '*', $where_array, $table_name, $return_type = 'result_array', $join_array = array(), $order_by_array = array(), $group_by_array = array(), $limit = 0, $offset = 0) {

		$this->db->select($select_string);
		$this->db->where($where_array);
		if(!empty($group_by_array)) {
			$this->db->group_by($group_by_array);
		}
		if(!empty($order_by_array)) {
			$this->db->order_by($order_by_array['column_name'], $order_by_array['column_value']);
		}
		return $this->db->get($table_name, $limit, $offset)->$return_type();
	}

	public function get_dynamic_data_sales_db_null_false($select_string = '*', $where_array, $table_name, $return_type = 'result_array', $limit = 0, $offset = 0) {

		$this->db->select($select_string);
		$this->db->where($where_array, null, false);
		return $this->db->get($table_name, $limit, $offset)->$return_type();
	}

	public function get_dynamic_data_sales_db($select_string = '*', $where_array, $table_name, $return_type = 'result_array', $limit = 0, $offset = 0) {
		
		$this->db->select($select_string);
		$this->db->where($where_array);
		return $this->db->get($table_name, $limit, $offset)->$return_type();
	}

	public function update_data($table_name, $update_data, $where_array, $update_type = 'single_update'){
		
		if($update_type == 'single_update') {

			return $this->db->update($table_name, $update_data, $where_array);
		} else if ($update_type == 'batch') {

			$this->db->update_batch($table_name, $update_data, $where_array);
		}
	}

	public function delete_Data($table, $where){
		$this->db->delete($table, $where);
	}

	public function insert_data($table_name, $insert_data, $insert_type = 'single_insert'){

		if($insert_type == 'single_insert') {

			$this->db->insert($table_name, $insert_data);
			return $this->db->insert_id();
		} else if ($insert_type == 'batch') {

			$this->db->insert_batch($table_name, $insert_data);
		}
	}

	public function get_graph_data($status){

		$result = $this->db->query("
							SELECT (assigned_to), 
							count('*') total_count, 
							count(IF(pq_client_status= '".$status."', 1, NULL)) count
							FROM customer_mst 
							group by (assigned_to) 
							order by count DESC
						")->result_array();
						// echo "<pre>";print_r($this->db->last_query());echo"</pre><hr>";exit;
		return $result;
	}

}