<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pre_qualification extends MX_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('user_id')){
			redirect('login', 'refresh');
			exit;
		}
		else{
			$access_count = 0;
			if(!empty($this->session->userdata('main_module_access')) && in_array(27, array_column($this->session->userdata('main_module_access'), 'module_id'))) {

				foreach ($this->session->userdata('sub_module_access') as $key => $value) {
					
					if($value['module_id'] == 27 && $value['status'] == 'Active') {
						$access_count++;
					}
				}
			}
			if($access_count == 0){
				redirect($this->session->userdata('home/dashboard'));
				exit;
			}
		}
		error_reporting(0);
		$this->load->model('pre_qualification_model');
	}

	public function show_session(){

		echo "<pre>"; print_r($this->session->userdata()); "</prer>"; exit;
	}

	public function add_pre_qualification(){
	
		$data = array();
		$data['customer_data'] = array('id'=>'', 'name'=>'', 'country_id'=>'', 'region_id'=>'', 'assigned_to'=>'', 'website'=>'', 'lead_type'=>'', 'lead_stage'=>'',  'sales_notes'=>'');		
		
		$data['next_count_number'] = 1;
		$data['next_number'] = 1;

		$data['client_list'] = $this->pre_qualification_model->get_dynamic_data_sales_db('id, name', array('id !='=>0,),'customer_mst');
		
		$data['assigned_to'] = $this->pre_qualification_model->get_dynamic_data_sales_db_null_false('user_id,name',"status=1 AND role IN (5, 16)", 'users');

		$data['region_list'] = $this->pre_qualification_model->get_dynamic_data_sales_db('*', array('status'=>'Active'), 'region_mst');

		$data['country_list'] = $this->pre_qualification_model->get_dynamic_data_sales_db('*', array('status'=>'Active'), 'country_mst');

		$data['lead_type'] = $this->pre_qualification_model->get_dynamic_data_sales_db('lead_type_id, type_name', array(), 'lead_type');

		$data['lead_stage'] = $this->pre_qualification_model->get_dynamic_data_sales_db('lead_stage_id, stage_name', array(), 'lead_stages');
		
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Pre Qualification'));
		$this->load->view('sidebar', array('title' => 'Pre Qualification'));
		$this->load->view('pre_qualification/pre_qualification_form', $data);
		$this->load->view('footer');
	}

	public function approve_pending_list(){
	
		$data = $this->create_pq_list_data(array('customer_mst.id !='=>0, 'customer_mst.is_pq ='=>'"Yes"', 'customer_mst.pq_client_status ='=>'"Pending"'), 10, 0, 'pending_list');

		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Pre Qualification'));
		$this->load->view('sidebar', array('title' => 'Pre Qualification'));
		$this->load->view('pre_qualification/pre_qualification_list', $data);
		$this->load->view('footer');
	}

	public function update_pre_qualification(){
	
		$data = array();
		$data['customer_data'] = array('id'=>'', 'name'=>'', 'country_id'=>'', 'region_id'=>'', 'assigned_to'=>'', 'website'=>'', 'lead_type'=>'', 'lead_stage'=>'',  'sales_notes'=>'');		

		$data['next_count_number'] = 1;
		$data['next_number'] = 1;

		$comp_mst_id = $this->uri->segment(3, 0);
		// echo "<pre>";print_r($comp_mst_id);echo"</pre><hr>";exit;
		if(!empty($comp_mst_id)){

			$data['customer_data'] = $this->pre_qualification_model->get_all_data('*', array('id'=> $comp_mst_id), 'customer_mst','row_array');

			if(!empty($data['customer_data'])){
				
				$data['member_last_contact'] = $this->pre_qualification_model->get_all_conditional_data_sales_db('*', array('comp_mst_id'=>$comp_mst_id), 'customer_connect','row_array', array(),array('column_name'=>'connected_on', 'column_value'=>'desc'),array(),0,0);			
			}
			
			$data['member_detail'] = $this->get_member_detail($comp_mst_id);
			$data['other_member_detail'] = $this->get_other_member_detail($comp_mst_id);			
			
			$data['next_count_number'] = count($data['member_detail'])+1;
			$data['next_number'] = count($data['other_member_detail'])+1;
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		}

		$data['assigned_to'] = $this->pre_qualification_model->get_dynamic_data_sales_db_null_false('user_id,name',"status=1 AND role IN (5, 16)",'users');

		$data['region_list'] = $this->pre_qualification_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'region_mst');

		$data['country_list'] = $this->pre_qualification_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'country_mst');

		$data['lead_type'] = $this->pre_qualification_model->get_dynamic_data_sales_db('lead_type_id, type_name', array(),'lead_type');

		$data['lead_stage'] = $this->pre_qualification_model->get_dynamic_data_sales_db('lead_stage_id, stage_name', array(),'lead_stages');
		
		$data['stage_reasons'] = $this->pre_qualification_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'lead_stage_reasons');
		
		$data['client_list'] = $this->pre_qualification_model->get_dynamic_data_sales_db('id, name', array('id !='=>0,),'customer_mst');

		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Pre Qualification'));
		$this->load->view('sidebar', array('title' => 'Pre Qualification'));
		$this->load->view('pre_qualification/update_pre_qualification_form', $data);
		$this->load->view('footer');
	}


	public function ajax_function() {

		if($this->input->is_ajax_request()){
			$post_details = $this->input->post();
			$call_type = $this->input->post('call_type');
			$response['status'] = 'successful';	
			switch ($call_type) {

				case 'get_search_filter_div':
					// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;					
					$data['lead_type'] = $this->pre_qualification_model->get_all_conditional_data_sales_db('lead_type_id, type_name',array(),'lead_type');
					$data['lead_stage'] = $this->pre_qualification_model->get_all_conditional_data_sales_db('lead_stage_id, stage_name' ,array(),'lead_stages');
					$data['region_list'] = $this->pre_qualification_model->get_all_conditional_data_sales_db('id, name',array('status'=>'active'),'region_mst');
					$data['country_list'] = $this->pre_qualification_model->get_all_conditional_data_sales_db('id, name',array('status'=>'active'),'country_mst');
					$data['company_list'] = $this->pre_qualification_model->get_dynamic_data_sales_db('id, name', array('status'=>'Active'), 'customer_mst');						
					$data['user_list'] = $this->pre_qualification_model->get_dynamic_data_sales_db_null_false('user_id,name',"status=1 AND role IN (5, 16)",'users');
						
					$data['search_filter'] = array('lead_type' => '', 'lead_stage' => '', 'company_name' => '', 'region_id' => '', 'country_id' => '', 'assigned_to' => '');
						
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
					$response['search_filter_data'] = $this->load->view('pre_qualification/search_filter_form', $data, true);				
				break;

				case 'change_tab':
				case 'search_filter':
				case 'paggination_filter':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;					
					$search_filter_data = $this->create_search_filter_data($post_details['search_form_data'], $post_details['tab_name']);	
					
					$data = $this->create_pq_list_data($search_filter_data['where'], $this->input->post('limit'), $this->input->post('offset'), $post_details['tab_name']);
					
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;					
					$data['search_filter'] = $search_filter_data['search_filter_data'];			
					$data['lead_type'] = $this->pre_qualification_model->get_all_conditional_data_sales_db('lead_type_id, type_name',array(),'lead_type');
					$data['lead_stage'] = $this->pre_qualification_model->get_all_conditional_data_sales_db('lead_stage_id, stage_name',array(),'lead_stages');
					$data['region_list'] = $this->pre_qualification_model->get_all_conditional_data_sales_db('id, name',array('status'=>'active'),'region_mst');
					$data['country_list'] = $this->pre_qualification_model->get_all_conditional_data_sales_db('id, name',array('status'=>'active'),'country_mst');						
					$data['company_list'] = $this->pre_qualification_model->get_dynamic_data_sales_db('id, name', array('status'=>'Active'), 'customer_mst');						
					$data['user_list'] = $this->pre_qualification_model->get_dynamic_data_sales_db_null_false('user_id,name',"status=1 AND role IN (5, 16)",'users');
					
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
					$response['pq_list_body'] = $this->load->view('pre_qualification/pre_qualification_list_body', $data, true);
					$response['pq_search_filter'] = $this->load->view('pre_qualification/search_filter_form', $data, true);
					$response['pq_paggination'] = $this->load->view('pre_qualification/paggination', $data, true);
				break;

				case 'update_pq_lead_stage':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					if(!empty($post_details['comp_mst_id'])){

						$update_data['lead_stage'] = $post_details['lead_stage'];
						$update_data['modified_on'] = date('Y-m-d H:i:s');
						$update_data['modified_by'] = $this->session->userdata('user_id');

						$this->pre_qualification_model->update_data('customer_mst', $update_data, array('id' =>$post_details['comp_mst_id']));
					}
				break;

				case 'get_member_body':
					// echo "<pre>"; print_r($this->input->post()); "</pre>";exit;
					if(!empty($this->input->post('next_count_number'))){
						
						$data['next_count_number'] = $this->input->post('next_count_number');
						$data['connected_on'] = '-';
						$data['connect_mode'] = '-';
						// echo "<pre>"; print_r($data); "</pre>";exit;
						$response['member_body_detail']	= $this->load->view('pre_qualification/member_body', $data, true);
					}
				break;

				case 'get_other_member_body':
					// echo "<pre>"; print_r($this->input->post()); "</pre>";exit;
					if(!empty($this->input->post('next_number'))){
						
						$data['next_number'] = $this->input->post('next_number');
						$data['connected_on'] = '-';
						$data['connect_mode'] = '-';
						// echo "<pre>"; print_r($data); "</pre>";exit;
						$response['other_member_body_detail']	= $this->load->view('pre_qualification/other_member_body', $data, true);
					}
				break;	

				case 'member_followup_modal':
				case 'other_member_followup_modal':
					
					// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;
					$response['follow_up_html'] = "";
					$response['comp_dtl_id'] = $post_details['comp_dtl_id'];
					if(!empty($post_details['comp_dtl_id'])){
							
						$data['connected_on'] = date('d-m-Y');
						$data['comp_mst_id'] = $post_details['comp_mst_id'];
							
						//getting follow up history
						$data['follow_up_details'] = $this->pre_qualification_model->get_all_conditional_data_sales_db('*',array('comp_detail_id'=>$post_details['comp_dtl_id'], 'comp_mst_id'=>$post_details['comp_mst_id'], ),'customer_connect','result_array',array(), array('column_name'=>'connected_on', 'column_value'=>'desc'), array(), 0,0);
						
						$user_list = array_column($this->pre_qualification_model->get_all_data('user_id, name', array(), 'users'), 'name', 'user_id');
						$user_list['']='';
						$user_list[0]='';

						foreach ($data['follow_up_details'] as $follow_up_details_key => $single_follow_up) {

							$data['follow_up_details'][$follow_up_details_key]['user_name'] = $user_list[$single_follow_up['entered_by']]; 
						}

						// echo "<pre>";print_r($data);echo"</pre><hr>";
						$response['follow_up_html']= $this->load->view('pre_qualification/follow_up_model_body', $data, true);
						$response['comp_dtl_id'] = $post_details['comp_dtl_id'];
					}
				break;

				case 'save_followup':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					if(!empty($post_details['comp_dtl_id'])){

						$form_data = array_column($post_details['add_followup_data_form'], 'value', 'name');
						$comp_dtl_id = $post_details['comp_dtl_id'];
						
						$insert_array = array(
												'comp_detail_id' => $comp_dtl_id,
												'comp_mst_id' => $form_data['comp_mst_id'],
												'connect_mode' => $form_data['connect_mode'],
												'email_sent' => $form_data['email'],
												'comments' => $form_data['comments'],
												'connected_on' => $form_data['connected_on'],
												'entered_on' => date('Y-m-d H:i:s'),
												'entered_by' => $this->session->userdata('user_id'),
						); 
						// echo "<pre>";print_r($insert_array);echo"</pre><hr>";
						if(!empty($insert_array)){
							$this->pre_qualification_model->insert_data('customer_connect', $insert_array);
						}
					}
				break;

				case 'delete_member':
				case 'delete_other_member':	
					// echo "<pre>";print_r($post_details['comp_dtl_id']);echo"</pre><hr>";exit;
					if(!empty($post_details['comp_dtl_id'])){

						$this->pre_qualification_model->delete_Data('customer_dtl', array('comp_mst_id' => $post_details['comp_mst_id'], 'comp_dtl_id'=> $post_details['comp_dtl_id']));
					}
				break;

				case 'update_pq_details':

					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					$response['message'] = "PQ Client Data are Updated";					
					$update_array = array();
					$update_array = array_column($this->input->post('client_details'), 'value', 'name');
					$update_member_array = array_column($this->input->post('client_member_details'), 'value', 'name');
					$update_other_member_array = array_column($this->input->post('client_other_member_details'), 'value', 'name');
					
					$update_array['modified_on'] = date('Y-m-d H:i:s');
					$update_array['modified_by'] = $this->session->userdata('user_id');
					$update_array['is_pq'] = 'Yes';
					$comp_mst_id = $this->input->post('comp_mst_id');
					
					$this->pre_qualification_model->update_data('customer_mst', $update_array, array('id'=>$comp_mst_id));
					$this->pre_qualification_model->delete_Data('customer_dtl', array('comp_mst_id'=>$comp_mst_id));
					
					// echo "<pre>";print_r($this->input->post('client_member_details'));echo"</pre><hr>";exit;
					for($i=1; $i <= (count($this->input->post('client_member_details'))/10); $i++){
						
						$member_array[] = array(
							
							'comp_mst_id' => $comp_mst_id,
							'comp_dtl_id' => $update_member_array['comp_dtl_id_'.$i],
							'member_name'=>$update_member_array['member_name_'.$i],
							'designation'=>$update_member_array['designation_'.$i],
							'email'=>$update_member_array['email_'.$i],
							'mobile'=>$update_member_array['mobile_'.$i],
							'is_whatsapp'=>$update_member_array['is_whatsapp_'.$i],
							'skype'=>$update_member_array['skype_'.$i],
							'telephone'=>$update_member_array['telephone_'.$i],
							'main_buyer'=>$update_member_array['main_buyer_'.$i],
							'decision_maker'=>$update_member_array['decision_maker_'.$i],
							'other_member' => $update_member_array['other_member'.$i]='No',
						);
						
					}
					if (!empty($member_array)) {
						$this->pre_qualification_model->insert_data('customer_dtl', $member_array, 'batch');
					}

					// update other member detail
					for ($i=1; $i <= (count($this->input->post('client_other_member_details'))/8); $i++) {
						
						$other_member_array[] = array(
							
							'comp_mst_id' => $comp_mst_id,
							'comp_dtl_id' => $update_other_member_array['comp_dtl_id_'.$i],
							'member_name' => $update_other_member_array['member_name_'.$i],
							'designation' => $update_other_member_array['designation_'.$i],
							'email' => $update_other_member_array['email_'.$i],
							'mobile' => $update_other_member_array['mobile_'.$i],
							'is_whatsapp' => $update_other_member_array['is_whatsapp_'.$i],
							'skype' => $update_other_member_array['skype_'.$i],
							'telephone' => $update_other_member_array['telephone_'.$i],
							'other_member' => $update_other_member_array['other_member'.$i]='Yes',
						);						
					}
					if (!empty($other_member_array)) {
						$this->pre_qualification_model->insert_data('customer_dtl', $other_member_array, 'batch');
					}
				break;
		
				case 'pq_status_highchart':

					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					$response['pq_status_highchart_data'] = array();
					if(!empty($this->input->post('tab_name'))) {
						
						if($this->input->post('tab_name') == 'pending_list'){
							
							$status = 'pending';
						}else if($this->input->post('tab_name') == 'approved_list'){
							
							$status = 'approved';
						}

						$count_details=$this->pre_qualification_model->get_graph_data($status);
						
						$sales_user=array_column($this->pre_qualification_model->get_dynamic_data_sales_db('name, user_id','status=1','users'),'name','user_id');

						// echo "<pre>";print_r($count_details);echo"</pre><hr>";exit;
						foreach ($count_details as $count_details_key => $single_count_details) {

							foreach ($sales_user as $sales_user_id => $sales_user_details){

								if($single_count_details['assigned_to'] == $sales_user_id){

									$single_count_details['assigned_to'] = $sales_user_details;		
								}   
							}							
							$response['pq_status_highchart_data'][$count_details_key]['name'] = $single_count_details['assigned_to'];

							$response['pq_status_highchart_data'][$count_details_key]['y'] = (int)$single_count_details['count'];

							$response['pq_status_highchart_data'][$count_details_key]['drilldown'] = $single_count_details['assigned_to'];
						}
					}
				break;

				case 'client_activity':

					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					$response['client_activity_html'] = "";
					$response['comp_mst_id'] = $post_details['comp_mst_id'];
					if(!empty($post_details['comp_mst_id'])){
							
						$data['activity_date'] = date('d-m-Y');
						$data['client_comments_date'] = date('d-m-Y');
						$data['comp_mst_id'] = $post_details['comp_mst_id'];

						$data['client_details'] = $this->pre_qualification_model->get_all_conditional_data_sales_db('*',array( 'comp_mst_id'=>$post_details['comp_mst_id']),'customer_dtl','result_array',array(), array(), array(), 0,0);

						$data['activity_details'] = $this->pre_qualification_model->get_all_conditional_data_sales_db('*',array( 'comp_mst_id'=>$post_details['comp_mst_id']),'customer_activity','result_array',array(), array(), array(), 0,0);

						// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
						$response['client_activity_html']= $this->load->view('pre_qualification/client_activity_model_body', $data, true);
						$response['comp_mst_id'] = $post_details['comp_mst_id'];
					}
				break;

				case 'save_client_activity':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					if(!empty($post_details['comp_mst_id'])){

						$form_data = array_column($post_details['add_activity_form'], 'value', 'name');
						$comp_mst_id = $post_details['comp_mst_id'];
						
						$insert_array = array(
												'comp_mst_id' => $comp_mst_id,
												'activity_type' => $form_data['activity_type'],
												'activity_date' => $form_data['activity_date'],
												'activity_description' => $form_data['activity_description'],
												'client_comments_date' => $form_data['client_comments_date'],
												'client_comments' => $form_data['client_comments'],
												'activity_notes' => $form_data['activity_notes'],
												'entered_on' => date('Y-m-d H:i:s'),
												'entered_by' => $this->session->userdata('user_id'),
						); 
						// echo "<pre>";print_r($insert_array);echo"</pre><hr>";
						if(!empty($insert_array)){
							$this->pre_qualification_model->insert_data('customer_activity', $insert_array);
						}
					}
				break;

				case 'delete_client_activity':
					// echo "<pre>";print_r($post_details['activity_id']);echo"</pre><hr>";exit;
					if(!empty($post_details['activity_id'])){

						$this->pre_qualification_model->delete_Data('customer_activity', array('comp_mst_id' => $post_details['comp_mst_id'], 'activity_id'=> $post_details['activity_id']));
					}
				break;

				default:
					$response['status'] = 'failed';	
					$response['message'] = "call type not found";
				break;
			}
			echo json_encode($response);
		} else{
			die('access is not allowed to this function');
		}
	}

	private function create_pq_list_data($where_array, $limit, $offset, $tab_name){

		$data = $this->pre_qualification_model->get_pq_listing_data($where_array, $order_by, $limit, $offset);
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;

		$lead_type_details = array_column($this->pre_qualification_model->get_all_data('lead_type_id, type_name', array(), 'lead_type'), 'type_name', 'lead_type_id');
		$lead_type_details['']='';
		$lead_type_details[0]='';

		$user_list = array_column($this->pre_qualification_model->get_all_data('user_id, name', array(), 'users'), 'name', 'user_id');
		$user_list['']='';
		$user_list[0]='';

		foreach($data['pq_list'] as $pq_list_key => $pq_list_details){			
			// echo "<pre>";print_r($pq_list_details);echo"</pre><hr>";exit;
			
			$data['pq_list'][$pq_list_key]['lead_name'] = $lead_type_details[$pq_list_details['lead_type']];
			$data['pq_list'][$pq_list_key]['user_name'] = $user_list[$pq_list_details['assigned_to']];
			
			$lead_member_details = $this->pre_qualification_model->get_all_conditional_data_sales_db('*', array('comp_mst_id'=>$pq_list_details['id'],'status'=>'Active'),'customer_dtl','result_array', array(),array(),array(),0,0);			
			// echo "<pre>";print_r($lead_member_details);echo"</pre><hr>";exit;
			
			$data['pq_list'][$pq_list_key]['comp_dtl_id'] = null;
			$data['pq_list'][$pq_list_key]['member_name'] = null;
			$data['pq_list'][$pq_list_key]['designation'] = null;
			$data['pq_list'][$pq_list_key]['email'] = null; 
			$data['pq_list'][$pq_list_key]['mobile'] = null;
			$member_count = $non_member_count = 0;
			
			if(!empty($lead_member_details)){
				
				foreach($lead_member_details as $single_member_details){
					// echo "<pre>";print_r($single_member_details);echo"</pre><hr>";exit;
					
					if(strtolower($single_member_details['main_buyer']) == 'yes'){
						
						$data['pq_list'][$pq_list_key]['comp_dtl_id'] = $single_member_details['comp_dtl_id'];
						$data['pq_list'][$pq_list_key]['member_name'] = $single_member_details['member_name'];
						$data['pq_list'][$pq_list_key]['designation'] = $single_member_details['designation'];
						$data['pq_list'][$pq_list_key]['email'] = $single_member_details['email']; 
						$data['pq_list'][$pq_list_key]['mobile'] = $single_member_details['mobile'];
						
					}else{
						if($single_member_details['member_name'] = '' && $single_member_details['email'] = '' && $single_member_details['mobile'] = ''){}else{
								
							if(strtolower($single_member_details['other_member']) == 'yes'){
								$non_member_count++;
							}else{
								$member_count++;
							}
						}
							
					}
				}
			}

			$data['pq_list'][$pq_list_key]['member_count'] = $member_count;
			$data['pq_list'][$pq_list_key]['non_member_count'] = $non_member_count;
				
			$lead_last_comment_details = $this->pre_qualification_model->get_all_conditional_data_sales_db('connected_on, connect_mode, comments', array('comp_mst_id'=>$pq_list_details['id'], ),'customer_connect','row_array', array(),array('column_name'=>'connected_on', 'column_value'=>'desc'),array(),0,0);			
				// echo "<pre>";print_r($lead_last_comment_details);echo"</pre><hr>";exit;	
				
			$last_contacted_data = $this->get_last_contacted_data($lead_last_comment_details);

			$data['pq_list'][$pq_list_key]['last_contacted'] = $last_contacted_data['last_contacted'];
			$data['pq_list'][$pq_list_key]['comments'] = $last_contacted_data['comments'];
			$data['pq_list'][$pq_list_key]['connect_mode'] = $last_contacted_data['connect_mode'];
		}
		
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$data['paggination_data']['limit'] = $limit;
		$data['paggination_data']['offset'] = $offset;
		$data['tab_name'] = $tab_name;
		return $data;
	}

	private function create_search_filter_data($search_filter_form_data, $tab_name){
			
		// $return_array['where'] = array();
		$return_array['where'] = "customer_mst.id != 0 AND customer_mst.is_pq ='Yes' AND customer_mst.pq_client_status ='Pending'";			
		$return_array['search_filter_data'] = array('lead_type' => '', 'lead_stage' => '', 'company_name' => '', 'country_id' => '', 'region_id' => '', 'assigned_to' => '');

		if($tab_name == 'approved_list'){

			$return_array['where'] = "customer_mst.id != 0 AND customer_mst.is_pq ='Yes' AND customer_mst.pq_client_status ='Approved'";
		}
	
		if(!empty($search_filter_form_data)){
							
			foreach($search_filter_form_data as $form_data){
				
				if(!empty($form_data['value']) || $form_data['value'] === '0'){
						
					switch($form_data['name']){

						case 'lead_type':
							// $return_array['where']['lead_type'] = $form_data['value'];
							$return_array['where'] .= " AND customer_mst.lead_type = '".$form_data['value']."'";$return_array['search_filter_data']['lead_type'] = $form_data['value'];
						break;

						case 'lead_stage':
							// $return_array['where']['lead_stage'] = $form_data['value'];
							$return_array['where'] .= " AND customer_mst.lead_stage = '".$form_data['value']."'";
							$return_array['search_filter_data']['lead_stage'] = $form_data['value'];
						break;

						case 'company_name':
							// $return_array['where']['client_mst_id'] = $form_data['value'];
							$return_array['where'] .= " AND customer_mst.name LIKE '%".$form_data['value']."%'";
							$return_array['search_filter_data']['company_name'] = $form_data['value'];
						break;

						case 'region_id':
							// $return_array['where']['region_id'] = $form_data['value'];
							$return_array['where'] .= " AND customer_mst.region_id = '".$form_data['value']."'";
							$return_array['search_filter_data']['region_id'] = $form_data['value'];
						break;
								
						case 'country_id':
							// $return_array['where']['country_id'] = $form_data['value'];
							$return_array['where'] .= " AND customer_mst.country_id = '".$form_data['value']."'";
							$return_array['search_filter_data']['country_id'] = $form_data['value'];
						break;
									
						case 'assigned_to':
							// $return_array['where']['assigned_to'] = $form_data['value'];
							$return_array['where'] .= " AND customer_mst.assigned_to = '".$form_data['value']."'";
							$return_array['search_filter_data']['assigned_to'] = $form_data['value'];
						break;
								
						default:
								
						break;		
					}						
				}					
			}				
		}
		// echo "<pre>";print_r($return_array['where']);echo"</pre><hr>";exit;
		return $return_array;
	}

	private function get_last_contacted_data($data) {
		
		$return_array = array('last_contacted' => '', 'comments' => '', 'connect_mode' => '');
		if(!empty($data)){
			
			$date1 = date_create($data['connected_on']);
			$date2 = date_create(date('Y-m-d'));
			$diff_obj = date_diff($date1, $date2);
			$diff = $diff_obj->format("%a");
			if($diff < 8){
				$return_array['last_contacted'] = $diff.' days ago';
			}else if($diff < 30){
				$weeks = round($diff / 7);
				$return_array['last_contacted'] = $weeks.' weeks ago';
			}else if($diff < 365){
				$months = round($diff / 30);
				$return_array['last_contacted'] = $months.' months ago';
			}else if($diff > 365){
				$years = round($diff / 365);
				$return_array['last_contacted'] = $years.' years ago';
			}
			$return_array['comments'] = $data['comments'];
			$return_array['connect_mode'] = $data['connect_mode'];
		}
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		return $return_array;
	}

	private function get_member_detail($comp_mst_id){
		
		$member_data= $this->pre_qualification_model->get_all_conditional_data_sales_db('*', array('comp_mst_id'=> $comp_mst_id, 'other_member'=>'No', 'main_buyer'=>'Yes', 'status'=>'Active'), 'customer_dtl','result_array', array(),array('column_name'=>'comp_dtl_id', 'column_value'=>'asc'),array(),0,0);
		// echo "<pre>";print_r($member_data);echo"</pre><hr>";exit;

		if(!empty($member_data)){
				
			foreach($member_data as $member_key => $single_member_detail){				
					
				$member_lead_connect = $this->pre_qualification_model->get_all_conditional_data_sales_db('*', array('comp_detail_id'=>$single_member_detail['comp_dtl_id']), 'customer_connect','row_array', array(),array('column_name'=>'connected_on', 'column_value'=>'desc'),array(),0,0);
					
				if(!empty($member_lead_connect)){
						
					$member_data[$member_key]['connected_on'] = date('d-m-Y', strtotime($member_lead_connect['connected_on']));
						
					$member_data[$member_key]['connect_mode'] = $member_lead_connect['connect_mode'];				
				}else{
						
					$member_data[$member_key]['connected_on'] = '-';
					$member_data[$member_key]['connect_mode'] = '-';
				}					
			}
		}			
		// echo "<pre>";print_r($member_data);echo"</pre><hr>";exit;
		return $member_data;
	}

	private function get_other_member_detail($comp_mst_id){

		$other_member = $this->pre_qualification_model->get_all_conditional_data_sales_db('*', array('comp_mst_id'=> $comp_mst_id, 'other_member'=>'Yes', 'main_buyer'=>'No', 'status'=>'Active'), 'customer_dtl','result_array', array(),array('column_name'=>'comp_dtl_id', 'column_value'=>'asc'),array(),0,0);
		
		if(!empty($other_member)){
			
			foreach($other_member as $other_member_key => $other_member_detail){	
				// echo "<pre>";print_r($other_member_detail);echo"</pre><hr>";exit;

				$other_member_lead_connect = $this->pre_qualification_model->get_all_conditional_data_sales_db('*', array('comp_mst_id'=> $comp_mst_id, 'comp_detail_id'=>$other_member_detail['comp_dtl_id']), 'customer_connect','row_array', array(),array('column_name'=>'connected_on', 'column_value'=>'desc'),array(),0,0);
					// echo "<pre>";print_r($other_member_lead_connect);echo"</pre><hr>";exit;

					if(!empty($other_member_lead_connect)){

						$other_member[$other_member_key]['connected_on'] = date('d-m-y', strtotime($other_member_lead_connect['connected_on']));

						$other_member[$other_member_key]['connect_mode'] = $other_member_lead_connect['connect_mode'];
					}else{

						$other_member[$other_member_key]['connected_on'] = '-';	
						$other_member[$other_member_key]['connect_mode'] = '-';
					}
					
				}
			}
		// echo "<pre>";print_r($other_member);echo"</pre><hr>";exit;
		return $other_member;
	}
	
}