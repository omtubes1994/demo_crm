<?php 
class Proforma_model extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	function getData($table, $where = ''){
		if($where != ''){
			$this->db->where($where);
		}
		return $this->db->get($table)->result_array();
	}
	
	// function getQuotationDetails($quote_id){
	// 	$this->db->select('m.*, d.*, c.name client_name, lc.name country, lr.name region, 
	// 	mb.member_name, mb.email, mb.mobile, mb.telephone, mb.skype, mb.is_whatsapp,
	// 	dl.delivery_name, dt.dt_value, pt.term_value, oc.country origin, mt.mtc_value, v.validity_value, cr.currency, cr.currency_icon, t.mode, u.unit_value, us.name uname, us.email uemail, us.mobile umobile');
	// 	$this->db->join('quotation_dtl d', 'm.quotation_mst_id = d.quotation_mst_id', 'inner');
	// 	$this->db->join('customer_mst c', 'c.id = m.client_id', 'inner');
	// 	$this->db->join('customer_mst mb', 'mb.comp_dtl_id = m.member_id', 'inner');
	// 	// $this->db->join('members mb', 'mb.member_id = m.member_id', 'inner');
	// 	$this->db->join('transport_mode t', 'm.transport_mode = t.mode_id', 'inner');
	// 	$this->db->join('payment_terms pt', 'm.payment_term = pt.term_id', 'inner');
	// 	$this->db->join('delivery dl', 'm.delivered_through = dl.delivery_id', 'inner');
	// 	$this->db->join('delivery_time dt', 'm.delivery_time = dt.dt_id', 'inner');
	// 	$this->db->join('origin_country oc', 'm.origin_country = oc.country_id', 'inner');
	// 	$this->db->join('currency cr', 'm.currency = cr.currency_id', 'inner');
	// 	$this->db->join('validity v', 'm.validity = v.validity_id', 'inner');
	// 	$this->db->join('mtc_type mt', 'm.mtc_type = mt.mtc_id', 'inner');
	// 	$this->db->join('units u', 'd.unit = u.unit_id', 'inner');
	// 	$this->db->join('users us', 'm.assigned_to = us.user_id', 'inner');
	// 	$this->db->join('country_mst lc', 'lc.id = c.country_id', 'left');
	// 	$this->db->join('region_mst lr', 'lr.id = c.region_id', 'left');
	// 	return $this->db->get_where('quotation_mst m', array('m.quotation_mst_id' => $quote_id))->result_array();
	// 	// echo $this->db->last_query();
	// }

	function getQuotationDetails($quote_id){

		// echo "<pre>";print_r($quote_id);echo"</pre><hr>";
		
		$this->db->select('m.client_id, m.quotation_mst_id, m.quote_no, m.work_order_no, m.is_new, m.order_no, m.proforma_no, m.confirmed_on, m.made_by, m.assigned_to, m.stage, m.quotation_priority, m.priority_reason, m.member_id, m.reference, m.net_total, m.freight, m.bank_charges, m.gst, m.discount_type, m.discount, m.other_charges, m.grand_total, m.delivered_through, m.delivery_time, m.payment_term, m.validity, m.currency, m.origin_country, m.mtc_type, m.transport_mode, m.status, m.yet_to_acknowledge, m.importance, m.close_reason, m.followup_date, m.entered_on quote_date, m.modified_by, m.modified_on, m.currency_rate, m.rfq_id, m.additional_comment, m.purchase_order, m.po_add_time, m.client_id_old, m.client_id_crm, m.acknowledge, m.client_type, 
		d.*,
		c.name client_name, lc.name country, lr.name region,
		mb.member_name, mb.email, mb.mobile, mb.telephone, mb.skype, mb.is_whatsapp, mb.main_buyer, mb.other_member,
		dl.delivery_name, dt.dt_value, pt.term_value, oc.country origin, t.mode, cr.currency, cr.currency_icon, v.validity_value, mt.mtc_value, u.unit_value, us.name uname, us.email uemail, us.mobile umobile');
		$this->db->join('quotation_dtl d', 'm.quotation_mst_id = d.quotation_mst_id', 'inner');
		$this->db->join('customer_mst c', 'c.id = m.client_id', 'left');
		$this->db->join('country_mst lc', 'lc.id = c.country_id', 'left');
		$this->db->join('region_mst lr', 'lr.id = c.region_id', 'left');
		$this->db->join('customer_dtl mb', 'mb.comp_dtl_id = m.member_id', 'left');
		$this->db->join('delivery dl', 'm.delivered_through = dl.delivery_id', 'left');
		$this->db->join('delivery_time dt', 'm.delivery_time = dt.dt_id', 'left');
		$this->db->join('payment_terms pt', 'm.payment_term = pt.term_id', 'left');
		$this->db->join('origin_country oc', 'm.origin_country = oc.country_id', 'left');
		$this->db->join('transport_mode t', 'm.transport_mode = t.mode_id', 'left');
		 $this->db->join('currency cr', 'm.currency = cr.currency_id', 'left');
		$this->db->join('validity v', 'm.validity = v.validity_id', 'left');
		$this->db->join('mtc_type mt', 'm.mtc_type = mt.mtc_id', 'left');
		$this->db->join('units u', 'd.unit = u.unit_id', 'left');
		$this->db->join('users us', 'm.assigned_to = us.user_id', 'left');
		return $this->db->get_where('quotation_mst m', array('m.quotation_mst_id' => $quote_id))->result_array();		
	}

	function getPortName($port_type, $delivery_type, $country){
		$res = $this->db->get_where('ports', array('port_type' => $port_type, 'delivery_type' => $delivery_type, 'country' => $country))->row_array();
		return $res['port_name'];
	}

	function getProformaList($start, $length, $where, $order, $dir, $filter_year){
		$this->db->select('m.*, c.name client_name, lc.name country, lr.name region, MONTH(m.confirmed_on) month, WEEK(m.confirmed_on) week, DATE_FORMAT(m.confirmed_on, "%d-%b") date, mb.mobile, mb.is_whatsapp, ua.name username, up.name purchased_by, cr.currency_icon');
		$this->db->join('customer_mst c', 'c.id = m.client_id', 'left');
		$this->db->join('customer_dtl mb', 'mb.comp_dtl_id = m.member_id', 'left');
		// $this->db->join('members mb', 'mb.member_id = m.member_id', 'left');
		$this->db->join('country_mst lc', 'lc.id = c.country_id', 'left');
		$this->db->join('region_mst lr', 'lr.id = c.region_id', 'left');
		$this->db->join('users ua', 'ua.user_id = m.assigned_to', 'left');
		$this->db->join('rfq_mst r', 'r.rfq_mst_id = m.rfq_id', 'left');
		$this->db->join('users up', 'up.user_id = r.assigned_to', 'left');
		$this->db->join('currency cr', 'cr.currency_id = m.currency', 'left');
		$this->db->where('m.status', 'Won');
		/*if($where != ''){
			$this->db->group_start();
			$this->db->where('m.quote_no like ', '%'.$where.'%');
			$this->db->or_where('m.confirmed_on like ', '%'.$where.'%');
			$this->db->or_where('c.client_name like ', '%'.$where.'%');
			$this->db->or_where('mb.name like ', '%'.$where.'%');
			$this->db->or_where('m.grand_total like ', '%'.$where.'%');
			$this->db->or_where('lc.lookup_value like ', '%'.$where.'%');
			$this->db->or_where('lr.lookup_value like ', '%'.$where.'%');
			$this->db->group_end();
		}*/

		if(!empty($where)){
			
			foreach ($where as $key => $value) {
				// $this->db->group_start();
				if($key == 'proforma_no' && $value != ''){
					$this->db->where("m.proforma_no like '%".$value."%'");
				}
				else if($key == 'assigned_to' && $value != ''){
					$this->db->where("m.assigned_to = ".$value);
				}
				else if($key == 'purchase_person' && $value != ''){
					$this->db->where("r.assigned_to = ".$value);
				}
				else if($key == 'confirmed_on' && $value != ''){
					$this->db->where("m.confirmed_on like '%".$value."%'");
				}
				else if($key == 'client_name' && $value != ''){
					$this->db->where("c.name like '%".$value."%'");
				}
				else if($key == 'grand_total' && $value != ''){
					$this->db->where("m.grand_total like '%".$value."%'");
				} 
				else if($key == 'country' && $value != ''){
					$this->db->where("lc.name = ".$value);
				} 
				else if($key == 'region' && $value != ''){
					$this->db->where("lr.name = ".$value);
				}
				else if($key == 'importance' && $value != ''){
					$this->db->where("m.importance like '%".$value."%'");
				} 
				else if($key == 'status' && $value != ''){
					$this->db->where("m.status like '%".$value."%'");
				} 
			}
		}
		if($this->session->userdata('role') == 5){

			$this->db->where_in('m.assigned_to', $this->session->userdata('quotation_access')['sales_user_id']);
		}else if($this->session->userdata('role') == 8){
			
			$this->db->where("( r.assigned_to IN (".implode(', ', $this->session->userdata('quotation_access')['procurement_user_id']).") || r.assigned_to_1 IN (".implode(', ', $this->session->userdata('quotation_access')['procurement_user_id']).") || r.assigned_to_2 IN (".implode(', ', $this->session->userdata('quotation_access')['procurement_user_id']).") || r.assigned_to_3 IN (".implode(', ', $this->session->userdata('quotation_access')['procurement_user_id']).") )", null, false);
		}
		if($filter_year != 'all'){
			$years = explode('-', $filter_year);
			$this->db->where('m.confirmed_on >= ', date($years[0].'-04-01 00:00:00'));
			$this->db->where('m.confirmed_on <= ', date($years[1].'-03-31 23:59:59'));
		}
		$this->db->limit($length, $start);
		// $this->db->order_by('proforma_no', 'desc');
		// $this->db->order_by("CAST(REPLACE(m.proforma_no, 'OM/', '') as INT)", "desc", FALSE);
		if(!empty($order)){
			
			$this->db->order_by($order, $dir);
		}else{
			
			$this->db->order_by("CAST(REPLACE(m.proforma_no, 'OM/', '') as INT)", "desc", FALSE);
		}
		$res = $this->db->get('quotation_mst m')->result_array();
		// echo $this->db->last_query()."<hr>";die;
		$k=0;
		$result = array();
		foreach ($res as $key => $value) {
			$result[$key] = $value;
			$result[$key]['record_id'] = ++$k;
		}
		// echo "<pre>";print_r($result);echo"</pre><hr>";exit;	
		return $result;
	}

	function getProformaListCount($where, $filter_year){
		
		$this->db->select('m.*, c.name client_name, lc.name country, lr.name region, MONTH(m.confirmed_on) month, WEEK(m.confirmed_on) week, DATE_FORMAT(m.confirmed_on, "%d-%b") date, mb.mobile, mb.is_whatsapp, ua.name username, up.name purchased_by, cr.currency_icon');
		$this->db->join('customer_mst c', 'c.id = m.client_id', 'inner');
		// $this->db->join('members mb', 'mb.member_id = m.member_id', 'left');
		$this->db->join('customer_dtl mb', 'mb.comp_dtl_id = m.member_id', 'left');
		$this->db->join('country_mst lc', 'lc.id = c.country_id', 'left');
		$this->db->join('region_mst lr', 'lr.id = c.region_id', 'left');
		$this->db->join('users ua', 'ua.user_id = m.assigned_to', 'left');
		$this->db->join('rfq_mst r', 'r.rfq_mst_id = m.rfq_id', 'left');
		$this->db->join('users up', 'up.user_id = r.assigned_to', 'left');
		$this->db->join('currency cr', 'cr.currency_id = m.currency', 'left');
		$this->db->where('m.status', 'Won');
		/*if($where != ''){
			$this->db->group_start();
			$this->db->where('m.quote_no like ', '%'.$where.'%');
			$this->db->or_where('m.confirmed_on like ', '%'.$where.'%');
			$this->db->or_where('c.client_name like ', '%'.$where.'%');
			$this->db->or_where('mb.name like ', '%'.$where.'%');
			$this->db->or_where('m.grand_total like ', '%'.$where.'%');
			$this->db->or_where('lc.lookup_value like ', '%'.$where.'%');
			$this->db->or_where('lr.lookup_value like ', '%'.$where.'%');
			$this->db->group_end();
		}*/

		if(!empty($where)){
			
			foreach ($where as $key => $value) {
				// $this->db->group_start();
				if($key == 'proforma_no' && $value != ''){
					$this->db->where("m.proforma_no like '%".$value."%'");
				}
				else if($key == 'assigned_to' && $value != ''){
					$this->db->where("m.assigned_to = ".$value);
				}
				else if($key == 'purchase_person' && $value != ''){
					$this->db->where("r.assigned_to = ".$value);
				}
				else if($key == 'confirmed_on' && $value != ''){
					$this->db->where("m.confirmed_on like '%".$value."%'");
				}
				else if($key == 'client_name' && $value != ''){
					$this->db->where("c.name like '%".$value."%'");
				}
				else if($key == 'grand_total' && $value != ''){
					$this->db->where("m.grand_total like '%".$value."%'");
				} 
				else if($key == 'country' && $value != ''){
					$this->db->where("lc.name = ".$value);
				} 
				else if($key == 'region' && $value != ''){
					$this->db->where("lr.name = ".$value);
				}
				else if($key == 'importance' && $value != ''){
					$this->db->where("m.importance like '%".$value."%'");
				} 
				else if($key == 'status' && $value != ''){
					$this->db->where("m.status like '%".$value."%'");
				} 
			}
		}
		if($this->session->userdata('role') == 5){

			$this->db->where_in('m.assigned_to', $this->session->userdata('quotation_access')['sales_user_id']);
		}else if($this->session->userdata('role') == 8){

			$this->db->where("( r.assigned_to IN (".implode(', ', $this->session->userdata('quotation_access')['procurement_user_id']).") || r.assigned_to_1 IN (".implode(', ', $this->session->userdata('quotation_access')['procurement_user_id']).") || r.assigned_to_2 IN (".implode(', ', $this->session->userdata('quotation_access')['procurement_user_id']).") || r.assigned_to_3 IN (".implode(', ', $this->session->userdata('quotation_access')['procurement_user_id']).") )", null, false);
		}
		if($filter_year != 'all'){
			$years = explode('-', $filter_year);
			$this->db->where('m.entered_on >= ', date($years[0].'-04-01 00:00:00'));
			$this->db->where('m.entered_on <= ', date($years[1].'-03-31 23:59:59'));
		}
		$res = $this->db->get('quotation_mst m')->result_array();
		return sizeof($res);
	}

	function updateData($table, $data, $where){
		$this->db->update($table, $data, $where);
	}

	function getFinancialYears(){

		$res = $this->db->query("
						SELECT
						   CASE WHEN MONTH(entered_on)>=3 THEN
						          concat(YEAR(entered_on), '-',YEAR(entered_on)+1)
						   ELSE concat(YEAR(entered_on)-1,'-', YEAR(entered_on)) END AS years
						FROM quotation_mst
						WHERE Year(entered_on) != 1970
						GROUP BY years
						order by years desc;
					")->result_array();
		// echo "<pre>";print_r($res);echo"</pre><hr>";exit;
		return $res;

		// $this->db->select('Year(entered_on) as year');
		// $this->db->where(array('Year(entered_on) !=' => '1970'));
		// $this->db->group_by('year');
		// $this->db->order_by('year', 'desc');
		// $res = $this->db->get('quotation_mst')->result_array();
		// return $this->create_year_list($res);
	}

	private function create_year_list($all_years){

		$return_array = array();

		if(!empty($all_years)) {

			foreach ($all_years as $key => $single_year) {
				
				$return_array[]['years'] = $single_year['year'].'-'.( ((int)$single_year['year']) +1);				
			}
			$return_array[]['years'] = ( ((int)$all_years[count($all_years)-1]['year']) -1).'-'.$all_years[count($all_years)-1]['year'];				
		}

		return $return_array;
	}
} 
?>