jQuery(document).ready(function() {

	KTAutosize.init();
});

var KTAutosize = function () {
    
    // Private functions
    var quotation_proforma_date_range_picker_Init = function() {
        if ($('#quotation_proforma_daterangepicker').length == 0) {
            return;
        }
        var picker = $('#quotation_proforma_daterangepicker');
        var start = moment().startOf('month');
        var end = moment().endOf('month');

        function cb(start, end, label) {
            var title = '';
            var range = '';
            if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D');
            } else {
                range = start.format('MMM D') + ' - ' + end.format('MMM D');
            }

            $('#quotation_proforma_daterangepicker_date').html(range);
            $('#quotation_proforma_daterangepicker_title').html(title);
			ajax_call_function({call_type: 'quotation_proforma_highchart', filter_date: range}, 'quotation_proforma_highchart', "<?php echo base_url('home/ajax_function'); ?>");
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end, 'This Month');
    }

    return {
        // public functions
        init: function() {
            quotation_proforma_date_range_picker_Init();
        }
    };
}();

function ajax_call_function(data, callType, url = "<?php echo base_url('home/ajax_function'); ?>") {

	$.ajax({
		type: 'POST',
		data: data,
		url: url,
		dataType: 'JSON',
		success: function(res){
			if(res.status == 'successful') {
				if(callType == 'quotation_proforma_highchart') {

					quotation_proforma_highchart(res.quotation_proforma_highchart);
				}
			}
		},
		beforeSend: function(response){
			
		}
	});
};

function set_reset_spinner(obj, set_unset_flag = true) {

	if(set_unset_flag) {

		$(obj).addClass('kt-spinner');
    	$(obj).addClass('kt-spinner--right');
    	$(obj).addClass('kt-spinner--sm');
    	$(obj).addClass('kt-spinner--light');	
	} else{

		$(obj).removeClass('kt-spinner');
    	$(obj).removeClass('kt-spinner--right');
    	$(obj).removeClass('kt-spinner--sm');
    	$(obj).removeClass('kt-spinner--light');
	}
}

function sweet_alert(title, icon = 'success') {

	swal({
			title: title,
			icon: icon,
    	});
}

function quotation_proforma_highchart(highchart_data) {

	Highcharts.chart('quotation_proforma_highchart', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: ''
	    },
	    subtitle: {
	        text: ''
	    },
	    accessibility: {
	        announceNewData: {
	            enabled: true
	        }
	    },
	    xAxis: {
	        type: 'category'
	    },
	    yAxis: {
	        title: {
	            text: 'Sales done in INR'
	        }

	    },
	    legend: {
	        enabled: false
	    },
	    plotOptions: {
	        series: {
	            borderWidth: 0,
	            dataLabels: {
	                enabled: true,
	                format: '{point.y:f}'
	            }
	        }
	    },

	    tooltip: {
	        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> INR<br/>'
	    },

	    series: [
	        {
	            name: "OM Tubes",
	            colorByPoint: true,
	            data: highchart_data
	        }
	    ]

	});
}