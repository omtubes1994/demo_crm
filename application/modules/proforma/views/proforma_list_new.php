<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<?php if(in_array(13,$this->session->userdata('graph_data_access'))) { ?>
	<!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--Begin::Portlet-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Quotation Won
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
							<a href="javascript:void(0);" class="btn kt-subheader__btn-daterange" id="quotation_proforma_daterangepicker" data-toggle="kt-tooltip" title="Select Destrack Date" data-placement="left" style="background: rgb(0 0 0 / 10%); display: inline-flex; height: 32px !important; padding-top: 0; padding-bottom: 0; margin-top: 0.25rem; margin-bottom: 0.25rem; margin-left: 0.25rem;">
								<span class="kt-subheader__btn-daterange-title" id="quotation_proforma_daterangepicker_title" style="display: inline-block;color: #959cb6;font-weight: 500;">Today</span>&nbsp;
								<span class="kt-subheader__btn-daterange-date" id="quotation_proforma_daterangepicker_date" style="display: inline-block;color: #5d78ff;margin-right: 0.75rem;font-weight: 500;">Aug 16</span>
								<i class="flaticon2-calendar-1" style="color: #5d78ff; font-size: 1rem !important;"></i>
							</a>
                        </ul>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--Begin::Timeline 3 -->   
                    <div class="kt-scroll" data-scroll="true" style="height: 400px">
                        <div id="quotation_proforma_highchart"></div>
                    </div>

                    <!--End::Timeline 3 -->
                </div>
            </div>

            <!--End::Portlet-->
        </div>
    </div>

    <!--End::Section-->
    <?php } ?>
	<div class="kt-portlet ">
		<?php if($this->session->flashdata('success')){ ?>
			<div class="alert alert-success" id="success-alert">
				<strong><?php echo $this->session->flashdata('success'); ?></strong> 
			</div>
		<?php } ?>
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					Proforma List
				</h3>
			</div>
		</div>
		<div class="kt-portlet__body">
			<div class="pull-left">
	 			<label for="fin_year">Financial Year: 
		        	<select class="form-control" id="fin_year">
		  				<option value="2023-2024">All</option>
		  				<?php 
		  				foreach($finYears as $fin){
		  					echo '<option value="'.$fin['years'].'">'.$fin['years'].'</option>';
		  				}?>
					</select>
				</label>
	        </div>
			<!--begin: Datatable -->
			<table class="table table-striped- table-bordered table-hover table-checkable" id="proforma_table">
				<thead>
					<tr>
						<th width="3%">Sr #</th>
						<th width="7%">Proforma #</th>
						<?php if(in_array($this->session->userdata('role'), array(1, 5, 16))) { ?><th width="5%">Assigned To</th><?php } ?>
						<th width="5%">Purchase Person</th>
						<th width="2%">Date</th>
						<th width="25%">Client</th>
						<th width="5%">Value</th>
						<th width="5%">Country</th>
						<th width="5%">Reg</th>
						<th width="5%">Imp</th>
						<th width="5%">Status</th>
						<th width="5%">Actions</th>
					</tr>
				</thead>
			</table>

			<!--end: Datatable -->
		</div>
	</div>
</div>

<div class="modal fade" id="po-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">upload Purchase Order</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
                <form action="<?php echo site_url('quotations/uploadPO'); ?>" method="post" id="po_form" name="po_form" enctype="multipart/form-data" target="_blank">
               		<div class="row">
               			<div class="col-md-6">
               				<label for="po_file">Purchase Order</label>
               				<input type="file" class="form-control validate[required]" name="po_file" id="po_file" accept="application/pdf">
               			</div>
               			<div class="col-md-6 align-self-center">
               				<input type="hidden" id="quote_id" name="quote_id">
               				<button class="btn btn-success" type="submit">Upload</button>
               			</div>
               		</div>
               	</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="query-popup_bckp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Query</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
                <form action="<?php echo site_url('quotations/addQuery'); ?>" method="post" id="query_form" name="query_form">
               		<div class="row">
               			<div class="col-md-6">
               				<label for="query_text">Query Details</label>
               				<textarea id="query_text" name="query_text" class="form-control validate[required]"></textarea>
               			</div>
               			<div class="col-md-6" id="close_query" style="display: none;">
               				<label for="close_query">Close Query</label>
               				<select class="form-control" name="query_status" id="query_status">
               					<option value="open">No</option>
               					<option value="closed">Yes</option>
               				</select>
               			</div>
               		</div>
               		<div class="clearfix"></div>
               		<div class="row">
               			<div class="col-md-6 align-self-center">
               				<input type="hidden" id="quote_id" name="quote_id">
               				<input type="hidden" id="query_id" name="query_id">
               				<input type="hidden" name="query_type" value="proforma">
               				<button class="btn btn-success" type="submit">Add Query</button>
               			</div>
               		</div>
               	</form>
               	<hr/>
               	<h4>Query History</h4>
                <div id="tab_history"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="pquery-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Query</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
                <form action="<?php echo site_url('quotations/addQuery'); ?>" method="post" id="pquery_form" name="pquery_form">
               		<div class="row">
               			<div class="col-md-6">
               				<label for="query_text">Query Details</label>
               				<textarea id="query_text" name="query_text" class="form-control validate[required]"></textarea>
               			</div>
               			<div class="col-md-6" id="close_query" style="display: none;">
               				<label for="close_query">Close Query</label>
               				<select class="form-control" name="query_status" id="query_status">
               					<option value="open">No</option>
               					<option value="closed">Yes</option>
               				</select>
               			</div>
               		</div>
               		<div class="clearfix"></div>
               		<div class="row">
               			<div class="col-md-6 align-self-center">
               				<input type="hidden" id="quote_id" name="quote_id">
               				<input type="hidden" id="query_id" name="query_id">
               				<input type="hidden" name="query_type" value="purchase">
               				<button class="btn btn-success" type="submit">Add Query</button>
               			</div>
               		</div>
               	</form>
               	<hr/>
               	<h4>Query History</h4>
                <div id="tab_history"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
			</div>
		</div>
	</div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="query-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<form id="proforma_query_add_form">
					<div class="row">
						
						<!--Begin:: App Content-->
						<div class="kt-grid__item kt-grid__item--fluid kt-app__content" id="kt_chat_content" style="width:100%; padding: 0px 24px 0px 24px;">
							<div class="kt-chat">
								<div class="kt-portlet kt-portlet--head-lg kt-portlet--last">
									<div class="kt-portlet__head">
										<div class="kt-chat__head ">
											<div class="kt-chat__left">

												<!--begin:: Aside Mobile Toggle -->
												<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md kt-hidden-desktop" id="kt_chat_aside_mobile_toggle">
													<i class="flaticon2-open-text-book"></i>
												</button>

												<!--end:: Aside Mobile Toggle-->
												<div class="dropdown dropdown-inline">
													<div class="kt-chat__label">
														<a href="#" class="kt-chat__title">Proforma Query Informations</a>
													</div>
												</div>
											</div>
											<div class="kt-chat__center">
												<div class="kt-chat__label">
													<!-- <a href="#" class="kt-chat__title">Jason Muller</a>
													<span class="kt-chat__status">
														<span class="kt-badge kt-badge--dot kt-badge--success"></span> Active
													</span> -->
												</div>
												<div class="kt-chat__pic kt-hidden">
													<span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Jason Muller">
														<img src="assets/media/users/300_12.jpg" alt="image">
													</span>
													<span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Nick Bold">
														<img src="assets/media/users/300_11.jpg" alt="image">
													</span>
													<span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Milano Esco">
														<img src="assets/media/users/100_14.jpg" alt="image">
													</span>
													<span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Teresa Fox">
														<img src="assets/media/users/100_4.jpg" alt="image">
													</span>
												</div>
											</div>
											<div class="kt-chat__right">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
											</div>
										</div>
									</div>
									<div class="kt-portlet__body">
										<div class="kt-scroll sales_query_history" data-scroll="true" data-height="400">
											
										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-chat__input">
											<div class="kt-chat__editor">
												<input type="text" class="form-control" name="query_type" value="proforma_query" hidden />
												<input type="text" class="form-control" name="query_reference" value="" hidden />
												<input type="text" class="form-control" name="query_reference_id" value="" hidden />
												<textarea style="height: 50px" placeholder="Type here..." name="query_text"></textarea>
												<input type="text" class="form-control" name="query_creator_id" value="" hidden />
												<input type="text" class="form-control" name="query_status" value="open" hidden />
											</div>
											<div class="kt-chat__toolbar">
												<div class="kt_chat__tools">
													<div class="query_type">
														
													</div>
												</div>
												<div class="kt_chat__actions">
													<button type="button" class="btn btn-brand btn-md btn-upper btn-bold kt-chat__reply add_proforma_query">reply</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<!--End:: App Content-->
					</div>
				</form>
			</div>
			<div class="modal-footer">
				
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->