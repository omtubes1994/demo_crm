<?php 
class Vendors_model extends CI_Model{

	function insertData($table, $data){
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	function updateData($table, $data, $where){
		$this->db->update($table, $data, $where);
	}

	function deleteData($table, $where){
		$this->db->delete($table, $where);
	}

	function getData($table, $where=''){
		if($where != ''){
			$this->db->where($where);
		}
		return $this->db->get($table)->result_array();
	}

	function getVendorDetails($vendor_id){
		$this->db->select('v.*, vd.*, v.vendor_id');
		$this->db->join('vendor_dtl vd', 'vd.vendor_id = v.vendor_id', 'left' );
		return $this->db->get_where('vendors v', array('v.vendor_id' => $vendor_id))->result_array();
	}

	function getVendorProducts($vendor_id){
		return $this->db->get_where('vendor_products', array('vendor_id' => $vendor_id))->result_array();
	}

	// function getVendorListData($start, $length, $search, $order, $dir){
	// 	$vendors = array();
	// 	// echo "<pre>";print_r($search);echo"</pre><hr>";exit;
	// 	if($search['product'] != ''){
	// 		$vendor_arr = $this->db->get_where('vendor_products', array('product_id' => $search['product']))->result_array();
	// 		foreach ($vendor_arr as $key => $value) {
	// 			$vendors[] = $value['vendor_id'];
	// 		}		
	// 	}
	// 	if($search['material'] != ''){
	// 		$vendor_arr = $this->db->get_where('vendor_products', array('material_id' => $search['material']))->result_array();
	// 		foreach ($vendor_arr as $key => $value) {
	// 			$vendors[] = $value['vendor_id'];
	// 		}
	// 	}
	// 	if($search['product'] != '' || $search['material'] != '') {
	// 		$vendors[0] = 0;
	// 		$where_string = "";
	// 		if(!empty($search['product'])) {

	// 			$where_string .= "product_id = ".$search['product'];
	// 		}
	// 		if(!empty($search['material'])) {

	// 			if(!empty($where_string)){

	// 				$where_string .= " AND ";
	// 			}
	// 			$where_string .= "material_id =".$search['material'];
	// 		}
	// 		$vendor_arr = $this->db->get_where('vendor_products', $where_string)->result_array();
	// 		foreach ($vendor_arr as $key => $value) {
	// 			$vendors[$key] = $value['vendor_id'];
	// 		}
	// 	}
	// 	$vendors[0] = 0;
	// 	if(!empty($search['product'])) {

	// 		$this->db->where_in('product_id', $search['product']);
	// 	}
	// 	if(!empty($search['material'])) {

	// 		$this->db->where_in('material_id', $search['material']);
	// 	}
	// 	// $vendor_arr = $this->db->get('vendor_products')->result_array();
	// 	$this->db->select('vendors.vendor_id');
	// 	$this->db->join('vendors', 'vendors.vendor_name = procurement.vendor_name', 'left');
	// 	$this->db->join('vendor_products', 'vendor_products.vendor_id = vendors.vendor_id', 'left');
	// 	$this->db->join('procurement_product_information', 'procurement_product_information.procurement_id = procurement.id', 'left');
	// 	$this->db->where(array('procurement.status'=> "Active"));
	// 	if(!empty($search['product'])) {

	// 		$this->db->where_in('vendor_products.product_id', $search['product']);
	// 	}
	// 	if(!empty($search['material'])) {

	// 		$this->db->where_in('vendor_products.material_id', $search['material']);
	// 	}
	// 	if(!empty($search['description'])) {

	// 		$this->db->like('procurement_product_information.description_name', $search['description']);
	// 	}
	// 	$this->db->group_by(array('vendors.vendor_id'));
	// 	$vendor_arr = $this->db->get('procurement')->result_array();
		
	// 	// SELECT procurement.vendor_name, procurement.procurement_no, vendors.vendor_id, procurement_product_information.description_name
	// 	// FROM `procurement`
	// 	// LEFT JOIN vendors ON vendors.vendor_name = procurement.vendor_name
	// 	// LEFT JOIN vendor_products ON vendor_products.vendor_id = vendors.vendor_id
	// 	// LEFT JOIN procurement_product_information ON procurement_product_information.procurement_id = procurement.id
	// 	// WHERE procurement.status = "Active"
	// 	// AND procurement_product_information.description_name LIKE '%male connector%';
	// 	foreach ($vendor_arr as $key => $value) {
	// 		$vendors[$key] = $value['vendor_id'];
	// 	}
	// 	$this->db->select('v.*, vd.*, v.vendor_id, cf.flag_name, users.name procurement_person_name');
	// 	$this->db->join('(select *, min(vendor_dtl_id) from vendor_dtl WHERE main_seller = "Yes" group by vendor_id ) vd', 'vd.vendor_id = v.vendor_id', 'left' );
	// 	$this->db->join('country_mst l', 'l.id = v.country', 'left');
	// 	// $this->db->join('lookup l', 'l.lookup_id = v.country', 'inner');
	// 	$this->db->join('country_flags cf', 'cf.country = l.name', 'left');
	// 	$this->db->join('users', 'users.user_id = v.entered_by', 'left');
	// 	$this->db->limit($length, $start);
	// 	$this->db->order_by($order, $dir);
	// 	// echo "<pre>";print_r($search);echo"</pre><hr>";exit;
	// 	if(!empty($search)){
	// 		foreach ($search as $key => $value) {
	// 			if($value != ''){
	// 				if($key == 'vendor_name'){
	// 					$this->db->where($key." like '%".$value."%'");
	// 				}else if($key == 'product' || $key == 'material' || $key == 'country' || $key == 'vendor_stage' || $key == 'vendor_source' || $key == 'procurement_person_id' || $key == 'description'){
	// 				}else{
	// 					// echo "<pre>";print_r($key);echo"</pre><hr>";exit;
	// 					$this->db->where($key, $value);
	// 				}
	// 			}
	// 		}
	// 	}
	// 	$this->db->where(array('v.status' => 'Active'));
	// 	if(!empty($vendors)){
	// 		$this->db->where_in('v.vendor_id', $vendors);
	// 	}
	// 	if(!empty($search['country'])) {

	// 		$this->db->where_in('v.country', $search['country']);
	// 	}
	// 	if(!empty($search['vendor_stage'])) {

	// 		$this->db->where_in('v.stage', $search['vendor_stage']);
	// 	}
	// 	if(!empty($search['vendor_source'])) {

	// 		$this->db->where_in('v.source', $search['vendor_source']);
	// 	}
	// 	if(!empty($search['procurement_person_id'])) {

	// 		$this->db->where_in('v.entered_by', $search['procurement_person_id']);
	// 	}
		
	// 	$result = $this->db->get('vendors v')->result_array();
	// 	// echo $this->db->last_query(),"<hr>";
	// 	// echo "<pre>";print_r($res);echo"</pre><hr>";exit;
	// 	$k=0;
	// 	$result = array();
	// 	foreach ($res as $key => $value) {
	// 		$result[$key] = $value;
	// 		$result[$key]['record_id'] = ++$k;
	// 		$result[$key]['all_product_name'] = '';
	// 		$result[$key]['all_material_name'] = '';
	// 		$r = $this->db->get_where('vendor_products', array('vendor_id' => $value['vendor_id']))->result_array();
	// 		if(!empty($r)){

	// 			$all_product_id = array_column($r, 'product_id');
	// 			$all_material_id = array_column($r, 'material_id');
	// 			if(!empty($all_product_id)) {

	// 				$result[$key]['all_product_name'] = $this->get_vendor_product_name($all_product_id);
	// 			}
	// 			if(!empty($all_material_id)) {

	// 				$result[$key]['all_material_name'] = $this->get_vendor_material_name($all_material_id);
	// 			}
	// 		}
	// 	}
	// 	// echo "<pre>";print_r($result);echo"</pre><hr>";exit;
	// 	return $result;
	// }

	// function getVendorListCount($search){
			// //$vendors = array();
			// if($search['product'] != ''){
			// 	$vendor_arr = $this->db->get_where('vendor_products', array('product_id' => $search['product']))->result_array();
			// 	foreach ($vendor_arr as $key => $value) {
			// 		$vendors[] = $value['vendor_id'];
			// 	}
			// }

		// 	// if($search['material'] != ''){
		// 	// 	$vendor_arr = $this->db->get_where('vendor_products', array('material_id' => $search['material']))->result_array();
		// 	// 	foreach ($vendor_arr as $key => $value) {
		// 	// 		$vendors[] = $value['vendor_id'];
		// 	// 	}
		// 	// }
		// 	if(!empty($search['product']) || !empty($search['material'])) {
		// 		$vendors[0] = 0;
		// 		if(!empty($search['product'])) {

		// 			$this->db->where_in('product_id', $search['product']);
		// 		}
		// 		if(!empty($search['material'])) {

		// 			$this->db->where_in('material_id', $search['material']);
		// 		}
		// 		$vendor_arr = $this->db->get('vendor_products')->result_array();
		// 		foreach ($vendor_arr as $key => $value) {
		// 			$vendors[$key] = $value['vendor_id'];
		// 		}
		// 	}
		// 	$this->db->select('v.*, vd.*, v.vendor_id, cf.flag_name');
		// 	$this->db->join('vendor_dtl vd', 'vd.vendor_id = v.vendor_id', 'left' );
		// 	// $this->db->join('lookup l', 'l.lookup_id = v.country', 'inner');
		// 	$this->db->join('country_mst l', 'l.id = v.country', 'inner');
		// 	$this->db->join('country_flags cf', 'cf.country = l.name', 'left');
		// 	if(!empty($search)){
		// 		foreach ($search as $key => $value) {
		// 			if($value != ''){
		// 				if($key == 'vendor_name'){
		// 					$this->db->where($key." like '%".$value."%'");
		// 				}else if($key == 'product' || $key == 'material' || $key == 'country' || $key == 'vendor_stage' || $key == 'vendor_source' || $key == 'procurement_person_id' || $key == 'description'){}else{
		// 					$this->db->where($key, $value);
		// 				}
		// 			}
		// 		}
		// 	}
		// 	if(!empty($vendors)){
		// 		$this->db->where_in('v.vendor_id', $vendors);
		// 	}
		// 	$res = $this->db->get('vendors v')->result_array();
		// 	return sizeof($res);
	// }

	// private function get_vendor_product_and_material_name($lookup_id) {

		// 	$this->db->select('lookup_value');
		// 	$this->db->where_in('lookup_id',$lookup_id);
		// 	$result = $this->db->get('lookup')->result_array();
		// 	return implode(' , ', array_column($result, 'lookup_value'));
	// }

	function getVendorListData($start, $length, $search, $order, $dir){
		
		$vendors = array();
		$vendors[0] = 0;		
		$this->db->select('vendors.vendor_id');
		$this->db->join('vendors', 'vendors.vendor_name = procurement.vendor_name', 'left');
		$this->db->join('vendor_products', 'vendor_products.vendor_id = vendors.vendor_id', 'left');
		$this->db->join('procurement_product_information', 'procurement_product_information.procurement_id = procurement.id', 'left');
		$this->db->where(array('procurement.status'=> "Active"));
		if(!empty($search['product'])) {

			$this->db->where_in('vendor_products.product_id', $search['product']);
		}
		if(!empty($search['material'])) {

			$this->db->where_in('vendor_products.material_id', $search['material']);
		}
		if(!empty($search['description'])) {

			$this->db->like('procurement_product_information.description_name', $search['description']);
		}
		$this->db->group_by(array('vendors.vendor_id'));
		$vendor_arr = $this->db->get('procurement')->result_array();

		// echo "<pre>";print_r($vendor_arr);echo"</pre><hr>";exit;

		foreach ($vendor_arr as $key => $value) {
			$vendors[$key] = $value['vendor_id'];
		}
		$this->db->select('v.*, vd.*, v.vendor_id, cf.flag_name, users.name procurement_person_name');
		$this->db->join('(select *, min(vendor_dtl_id) from vendor_dtl WHERE main_seller = "Yes" group by vendor_id ) vd', 'vd.vendor_id = v.vendor_id', 'left' );
		$this->db->join('country_mst l', 'l.id = v.country', 'left');
		$this->db->join('country_flags cf', 'cf.country = l.name', 'left');
		$this->db->join('users', 'users.user_id = v.entered_by', 'left');
		$this->db->limit($length, $start);
		$this->db->order_by($order, $dir);
		// echo "<pre>";print_r($search);echo"</pre><hr>";exit;
		if(!empty($search)){
			foreach ($search as $key => $value) {
				if($value != ''){
					if($key == 'vendor_name'){
						$this->db->where($key." like '%".$value."%'");
					}else if($key == 'product' || $key == 'material' || $key == 'country' || $key == 'vendor_stage' || $key == 'vendor_source' || $key == 'procurement_person_id' || $key == 'description'){
					}else{
						// echo "<pre>";print_r($key);echo"</pre><hr>";exit;
						$this->db->where($key, $value);
					}
				}
			}
		}
		$this->db->where(array('v.status' => 'Active'));
		if(!empty($vendors)){
			$this->db->where_in('v.vendor_id', $vendors);
		}
		if(!empty($search['country'])) {

			$this->db->where_in('v.country', $search['country']);
		}
		if(!empty($search['vendor_stage'])) {

			$this->db->where_in('v.stage', $search['vendor_stage']);
		}
		if(!empty($search['vendor_source'])) {

			$this->db->where_in('v.source', $search['vendor_source']);
		}
		if(!empty($search['procurement_person_id'])) {

			$this->db->where_in('v.entered_by', $search['procurement_person_id']);
		}
		
		$res = $this->db->get('vendors v')->result_array();
		// echo $this->db->last_query(),"<hr>";
		// echo "<pre>";print_r($res);echo"</pre><hr>";exit;
		$k=0;
		$result = array();
		foreach ($res as $key => $value) {
			$result[$key] = $value;
			$result[$key]['record_id'] = ++$k;
			$result[$key]['all_product_name'] = '';
			$result[$key]['all_material_name'] = '';
			$r = $this->db->get_where('vendor_products', array('vendor_id' => $value['vendor_id']))->result_array();
			if(!empty($r)){

				$all_product_id = array_column($r, 'product_id');
				$all_material_id = array_column($r, 'material_id');
				if(!empty($all_product_id)) {

					$result[$key]['all_product_name'] = $this->get_vendor_product_name($all_product_id);
				}
				if(!empty($all_material_id)) {

					$result[$key]['all_material_name'] = $this->get_vendor_material_name($all_material_id);
				}
			}
		}
		// echo "<pre>";print_r($result);echo"</pre><hr>";exit;
		return $result;
	}

	private function get_vendor_product_name($product_id) {

		$this->db->select('name');
		$this->db->where_in('id',$product_id);
		$result = $this->db->get('product_mst')->result_array();
		return implode(' , ', array_column($result, 'name'));
	}

	private function get_vendor_material_name($material_id) {

		$this->db->select('name');
		$this->db->where_in('id',$material_id);
		$result = $this->db->get('material_mst')->result_array();
		return implode(' , ', array_column($result, 'name'));
	}

	function getVendorListCount($search){
		$vendors = array();
		
		if(!empty($search['product']) || !empty($search['material'])) {
			$vendors[0] = 0;
			if(!empty($search['product'])) {

				$this->db->where_in('product_id', $search['product']);
			}
			if(!empty($search['material'])) {

				$this->db->where_in('material_id', $search['material']);
			}
			$vendor_arr = $this->db->get('vendor_products')->result_array();
			foreach ($vendor_arr as $key => $value) {
				$vendors[$key] = $value['vendor_id'];
			}
		}
		$this->db->select('v.*, vd.*, v.vendor_id, cf.flag_name');
		$this->db->join('vendor_dtl vd', 'vd.vendor_id = v.vendor_id', 'left' );
		$this->db->join('country_mst l', 'l.id = v.country', 'inner');
		$this->db->join('country_flags cf', 'cf.country = l.name', 'left');
		
		if(!empty($search)){
			foreach ($search as $key => $value) {
				if($value != ''){
					if($key == 'vendor_name'){
						$this->db->where($key." like '%".$value."%'");
					}else if($key == 'product' || $key == 'material' || $key == 'country' || $key == 'vendor_stage' || $key == 'vendor_source' || $key == 'procurement_person_id' || $key == 'description'){}else{
 						$this->db->where($key, $value);
					}
				}
			}
		}
		if(!empty($vendors)){
			$this->db->where_in('v.vendor_id', $vendors);
		}
		$res = $this->db->get('vendors v')->result_array();
		return sizeof($res);
	}

	public function get_procurement_person() {

		$this->db->select('name, user_id');
		$this->db->where('status=1 AND role=6 OR role=8', null, false);
		return $this->db->get('users')->result_array();
	}

	public function get_vendor_management_data(){

		$return_array = array();
		$this->db->select('SQL_CALC_FOUND_ROWS *,vendors.vendor_id as vendor_primary_key, vendors.vendor_name, vendors.country, vendors.entered_by, procurement.id as procurement_primary_key, procurement.procurement_no, procurement_product_information.id as procurement_product_information_primary_key, procurement_product_information.product_name,  procurement_product_information.material_name, procurement_product_information.description_name, procurement_product_information.quantity, procurement_product_information.units', false);
		$this->db->where(array('vendors.status'=>'Active', 'vendors.vendor_name'=>'Maitri Enggtech'));
		$this->db->join('procurement', 'procurement.vendor_name = vendors.vendor_name', 'left');
		$this->db->join('procurement_product_information', 'procurement_product_information.procurement_id = procurement.id', 'left');
		$return_array['vendor_list'] = $this->db->get('vendors')->result_array();
		// echo "<pre>";print_r($res);echo"</pre><hr>";exit;
		$return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows;")->row_array();
		return $return_array;
	}

	public function get_rfq_data($vendor_to_rfq_id){

		$this->db->select('rfq_mst.rfq_mst_id, rfq_mst.rfq_no, rfq_mst.rfq_status, quotation_mst.quotation_mst_id
		');
		$this->db->where("rfq_mst_id IN ('".implode("','",array_column($vendor_to_rfq_id, 'rfq_id'))."')");
		$this->db->join('quotation_mst', 'quotation_mst.rfq_id = rfq_mst.rfq_mst_id', 'left');
		return $this->db->get('rfq_mst')->result_array();
	}

	public function get_po_data($vendor_id){

		$this->db->select('procurement.id, procurement.procurement_no');
		$this->db->where(array('vendors.vendor_id'=>$vendor_id));
		$this->db->join('vendors', 'vendors.vendor_name = procurement.vendor_name', 'left');
		return $this->db->get('procurement')->result_array();
	}
}
?>