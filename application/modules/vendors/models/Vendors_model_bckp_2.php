<?php 
class Vendors_model extends CI_Model{

	function insertData($table, $data){
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	function updateData($table, $data, $where){
		$this->db->update($table, $data, $where);
	}

	function deleteData($table, $where){
		$this->db->delete($table, $where);
	}

	function getData($table, $where=''){
		if($where != ''){
			$this->db->where($where);
		}
		return $this->db->get($table)->result_array();
	}

	function getVendorDetails($vendor_id){
		$this->db->select('v.*, vd.*, v.vendor_id');
		$this->db->join('vendor_dtl vd', 'vd.vendor_id = v.vendor_id', 'left' );
		return $this->db->get_where('vendors v', array('v.vendor_id' => $vendor_id))->result_array();
	}

	function getVendorProducts($vendor_id){
		return $this->db->get_where('vendor_products', array('vendor_id' => $vendor_id))->result_array();
	}

	function getVendorListData($start, $length, $search, $order, $dir){
		$vendors = array();
		// if($search['product'] != ''){
		// 	$vendor_arr = $this->db->get_where('vendor_products', array('product_id' => $search['product']))->result_array();
		// 	foreach ($vendor_arr as $key => $value) {
		// 		$vendors[] = $value['vendor_id'];
		// 	}
		// }

		// if($search['material'] != ''){
		// 	$vendor_arr = $this->db->get_where('vendor_products', array('material_id' => $search['material']))->result_array();
		// 	foreach ($vendor_arr as $key => $value) {
		// 		$vendors[] = $value['vendor_id'];
		// 	}
		// }

		if($search['product'] != '' || $search['material'] != '') {
			$vendors[0] = 0;
			$where_string = "";
			if(!empty($search['product'])) {

				$where_string .= "product_id = ".$search['product'];
			}
			if(!empty($search['material'])) {

				if(!empty($where_string)){

					$where_string .= " AND ";
				}
				$where_string .= "material_id =".$search['material'];
			}
			$vendor_arr = $this->db->get_where('vendor_products', $where_string)->result_array();
			foreach ($vendor_arr as $key => $value) {
				$vendors[$key] = $value['vendor_id'];
			}
		}

		$this->db->select('v.*, vd.*, v.vendor_id, cf.flag_name, users.name procurement_person_name');
		$this->db->join('(select *, min(vendor_dtl_id) from vendor_dtl group by vendor_id ) vd', 'vd.vendor_id = v.vendor_id', 'left' );
		$this->db->join('lookup l', 'l.lookup_id = v.country', 'inner');
		$this->db->join('country_flags cf', 'cf.country = l.lookup_value', 'left');
		$this->db->join('users', 'users.user_id = v.entered_by', 'left');
		$this->db->limit($length, $start);
		$this->db->order_by($order, $dir);
		if(!empty($search)){
			foreach ($search as $key => $value) {
				if($value != ''){
					if($key == 'vendor_name'){
						$this->db->where($key." like '%".$value."%'");
					}else if($key == 'product' || $key == 'material'){}else{
						$this->db->where($key, $value);
					}
				}
			}
		}
		$this->db->where(array('v.status' => 'Active'));
		if(!empty($vendors)){
			$this->db->where_in('v.vendor_id', $vendors);
		}
		$res = $this->db->get('vendors v')->result_array();
		$k=0;
		$result = array();
		foreach ($res as $key => $value) {
			$result[$key] = $value;
			$result[$key]['record_id'] = ++$k;
			$result[$key]['all_product_name'] = '';
			$result[$key]['all_material_name'] = '';
			$r = $this->db->get_where('vendor_products', array('vendor_id' => $value['vendor_id']))->result_array();
			if(!empty($r)){

				$all_product_id = array_column($r, 'product_id');
				$all_material_id = array_column($r, 'material_id');
				if(!empty($all_product_id)) {

					$result[$key]['all_product_name'] = $this->get_vendor_product_and_material_name($all_product_id);
				}
				if(!empty($all_material_id)) {

					$result[$key]['all_material_name'] = $this->get_vendor_product_and_material_name($all_material_id);
				}
			}
		}
		return $result;
	}

	private function get_vendor_product_and_material_name($lookup_id) {

		$this->db->select('lookup_value');
		$this->db->where_in('lookup_id',$lookup_id);
		$result = $this->db->get('lookup')->result_array();
		return implode(' , ', array_column($result, 'lookup_value'));
	}
	function getVendorListCount($search){
		$vendors = array();
		if($search['product'] != ''){
			$vendor_arr = $this->db->get_where('vendor_products', array('product_id' => $search['product']))->result_array();
			foreach ($vendor_arr as $key => $value) {
				$vendors[] = $value['vendor_id'];
			}
		}

		if($search['material'] != ''){
			$vendor_arr = $this->db->get_where('vendor_products', array('material_id' => $search['material']))->result_array();
			foreach ($vendor_arr as $key => $value) {
				$vendors[] = $value['vendor_id'];
			}
		}
		$this->db->select('v.*, vd.*, v.vendor_id, cf.flag_name');
		$this->db->join('vendor_dtl vd', 'vd.vendor_id = v.vendor_id', 'left' );
		$this->db->join('lookup l', 'l.lookup_id = v.country', 'inner');
		$this->db->join('country_flags cf', 'cf.country = l.lookup_value', 'left');
		if(!empty($search)){
			foreach ($search as $key => $value) {
				if($value != ''){
					if($key == 'vendor_name'){
						$this->db->where($key." like '%".$value."%'");
					}else if($key == 'product' || $key == 'material'){}else{
						$this->db->where($key, $value);
					}
				}
			}
		}
		if(!empty($vendors)){
			$this->db->where_in('v.vendor_id', $vendors);
		}
		$res = $this->db->get('vendors v')->result_array();
		return sizeof($res);
	}

	public function get_procurement_person() {

		$this->db->select('name, user_id');
		$this->db->where('status=1 AND role=6 OR role=8', null, false);
		return $this->db->get('users')->result_array();
	}
}
?>