<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendors extends MX_Controller {
	public function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('user_id')){
			redirect('login', 'refresh');
			exit;
		}else{
			// $access_count = 0;
			// if(!empty($this->session->userdata('main_module_access')) && in_array(12, array_column($this->session->userdata('main_module_access'), 'module_id'))) {

			// 	foreach ($this->session->userdata('sub_module_access') as $key => $value) {
					
			// 		if($value['module_id'] == 12 && $value['status'] == 'Active') {
			// 			$access_count++;
			// 		}
			// 	}
			// }
			// if($access_count == 0){
			// 	redirect($this->session->userdata('home/dashboard'));
			// 	exit;
			// }
		}
		$this->load->model('vendors_model');
		$this->load->model('common_model');
	}

	function index(){
		$this->add_vendor();
	}

	function add_vendor($vendor_id = 0){

		if(!empty($this->input->post())){
			$insert_arr = array(
				'vendor_name' => $this->input->post('vendor_name'),
				'country' => $this->input->post('country'),
				'source' => $this->input->post('source'),
				'website' => $this->input->post('website'),
				'stage' => $this->input->post('stage'),
				'entered_by' => $this->input->post('procurement_user_id')
			);

			if($this->input->post('vendor_id') > 0){
				$vendor_id = $this->input->post('vendor_id');
				$this->vendors_model->updateData('vendors', $insert_arr, array('vendor_id' => $vendor_id));
				$this->vendors_model->deleteData('vendor_products', array('vendor_id' => $vendor_id));
				$this->vendors_model->deleteData('vendor_dtl', array('vendor_id' => $vendor_id));
			}else{
				$insert_arr['entered_on'] = date('Y-m-d H:i:s');
				$vendor_id = $this->vendors_model->insertData('vendors', $insert_arr);
			}

			if(!empty($this->input->post('product_id'))){
				foreach($this->input->post('product_id') as $key => $value){
					$insert_arr = array(
						'vendor_id' => $vendor_id,
						'product_id' => $this->input->post('product_id')[$key],
						'material_id' => $this->input->post('material_id')[$key],
						'vendor_type' => $this->input->post('vendor_type')[$key],
					);
					$this->vendors_model->insertData('vendor_products', $insert_arr);
				}
			}
			if(!empty($this->input->post('name'))){
				foreach($this->input->post('name') as $key => $value){
					$insert_arr = array(
						'vendor_id' => $vendor_id,
						'name' => $this->input->post('name')[$key],
						'designation' => $this->input->post('designation')[$key],
						'email' => $this->input->post('email')[$key],
						'mobile' => $this->input->post('mobile')[$key],
						'is_whatsapp' => $this->input->post('is_whatsapp')[$key],
						'skype' => $this->input->post('vendor_type')[$key],
						'telephone' => $this->input->post('telephone')[$key],
						'main_seller' => $this->input->post('main_seller')[$key],
						'pmoc' => $this->input->post('pmoc')[$key],
					);
					$this->vendors_model->insertData('vendor_dtl', $insert_arr);
				}
			}
			redirect('vendors/add_vendor/'.$vendor_id);
			
		}else{
			if($vendor_id > 0){
				$data['vendor_id'] = $vendor_id;

				$data['vendor_details'] = $this->vendors_model->getVendorDetails($vendor_id);
				$data['vendor_products'] = $this->vendors_model->getVendorProducts($vendor_id);
				// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
			}
			$data['prd_str'] = $data['mat_str'] = '';
			$data['country'] = $this->vendors_model->getData('country_mst', 'status = "Active"');
			$data['product'] = $product = $this->vendors_model->getData('product_mst', 'status = "Active"');
			foreach($product as $prod){ 
				$data['prd_str'] .= '<option value="'.$prod['id'].'">'.ucwords(strtolower($prod['name'])).'</option>';
			}

			$data['material'] = $material = $this->vendors_model->getData('material_mst', 'status = "Active"');
			foreach($material as $mat){ 
				$data['mat_str'] .= '<option value="'.$mat['id'].'">'.ucwords(strtolower($mat['name'])).'</option>';
			}
			$data['procurement_users'] = $this->vendors_model->get_procurement_person();

			$data['vendor_rfq_list'] = array();
			$vendor_to_rfq_id = $this->common_model->get_dynamic_data_sales_db('rfq_id', array('vendor_id'=> $vendor_id), 'rfq_to_vendor');
			if(!empty($vendor_to_rfq_id)){
				
				
				$vendor_to_rfq_data = $this->vendors_model->get_rfq_data($vendor_to_rfq_id);
				
				if(!empty($vendor_to_rfq_data)){
					
					foreach ($vendor_to_rfq_data as $vendor_to_rfq_data_single) {
						
						$data['vendor_rfq_list'][$vendor_to_rfq_data_single['rfq_status']][] = $vendor_to_rfq_data_single;
					}
				}
			}
			$data['vendor_po_list'] = array();
			$data['vendor_po_list'] = $this->vendors_model->get_po_data($vendor_id);
			
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
			$this->load->view('header', array('title' => 'Add Vendor'));
			$this->load->view('sidebar', array('title' => 'Add Vendor'));
			$this->load->view('add_vendor', $data);
			$this->load->view('footer');
		}
	}

	function list(){

		$data['prd_str'] = $data['mat_str'] = $data['country_str'] = '';
		$country = $this->vendors_model->getData('country_mst', 'status = "Active"');
		foreach($country as $cnt){ 
			$data['country_str'] .= '<option value="'.$cnt['id'].'">'.ucwords(strtolower($cnt['name'])).'</option>';
		}
		
		$product = $this->vendors_model->getData('product_mst', 'status = "Active"');
		foreach($product as $prod){ 
			$data['prd_str'] .= '<option value="'.$prod['id'].'">'.ucwords(strtolower($prod['name'])).'</option>';
		}

		$material = $this->vendors_model->getData('material_mst', 'status = "Active"');
		foreach($material as $mat){ 
			$data['mat_str'] .= '<option value="'.$mat['id'].'">'.ucwords(strtolower($mat['name'])).'</option>';
		}
		$data['procurement_person'] = $this->vendors_model->getData('users', 'status="1" AND role in(6,8)');

		$this->load->view('header', array('title' => 'Vendors List'));
		$this->load->view('sidebar', array('title' => 'Vendors List'));
		$this->load->view('vendors_list', $data);
		$this->load->view('footer');
	}

	function list_data(){

		// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
		$search = array();
		foreach ($this->input->post('columns') as $key => $value) {
			$search_key = '';
			if($key == 1){
				$search_key = 'vendor_name';
			}else if($key == 2){
				$search_key = 'v.country';
			}else if($key == 3){
				$search_key = 'stage';
			}else if($key == 4){
				$search_key = 'source';
			}

			$search[$search_key] = $this->input->post('columns')[$key]['search']['value'];
		}
		$search['product'] = $this->input->post('searchByProduct');
		$search['material'] = $this->input->post('searchByMaterial');
		$search['country'] = $this->input->post('country');
		$search['vendor_stage'] = $this->input->post('vendor_stage');
		$search['vendor_source'] = $this->input->post('vendor_source');
		$search['procurement_person_id'] = $this->input->post('procurement_person_id');
		$search['description'] = $this->input->post('description');
		// echo "<pre>";print_r($search);echo"</pre><hr>";exit;
		$order_by = $this->input->post('columns')[$this->input->post('order')[0]['column']]['data'];
		if($order_by == 'record_id'){
			$order_by = 'vendor_name';
		}
		$dir = $this->input->post('order')[0]['dir'];
		$records = $this->vendors_model->getVendorListData($this->input->post('start'), $this->input->post('length'), $search, $order_by, $dir);

		$data['iTotalDisplayRecords'] = $data['iTotalRecords'] = $this->vendors_model->getVendorListCount($search);
		$data['aaData'] = $records;
		// echo "<pre>";print_r($data);die;
		echo json_encode($data);
	}

	public function vendor_management() {

		$data = array();

		$this->load->view('header', array('title' => 'Vendor Management'));
		$this->load->view('sidebar', array('title' => 'Vendor Management'));
		$this->load->view('vendors/vendor_management_index', $data);
		$this->load->view('footer');
	}

	public function ajax_function() {

		if($this->input->is_ajax_request()) {
			$call_type = $this->input->post('call_type');
			$response['status'] = 'successful';	
			switch ($call_type) {

				case 'delete_vendor':
					
					$response['message'] = "Vendor is Deleted Successfully !!!";
					$vendor_id = $this->input->post('vendor_id');
					if(!empty($vendor_id)) {
						$this->vendors_model->updateData('vendors',array('status'=>'Inactive'), array('vendor_id'=>$vendor_id));	
					}else{
						$response['message'] = "Vendor is not deleted";
					}
				break;	

				case 'vendor_management_list_data':
					
					$data = $this->prepare_vendor_management_data();
					$response['table_body'] = $this->load->view('vendors/vendor_management_index_body', $data, true);
					$response['table_paggination'] = $this->load->view('vendors/vendor_management_index_body', $data, true);
				break;

				default:
					$response['status'] = 'failed';	
					$response['message'] = "call type not found";
				break;
			}
			echo json_encode($response);

		} else{
			die('access is not allowed to this function');
		}
	}

	private function prepare_vendor_management_data() {

		$data = $this->vendors_model->get_vendor_management_data();
		$data['lookup_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*',array(),'lookup'), 'lookup_value', 'lookup_id');
		$data['user_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*',array(),'users'), 'name', 'user_id');

		return $data;
	}
}
?>