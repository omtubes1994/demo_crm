<style type="text/css">
	.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
		padding-top: 5% !important;
	}
	.layer-white{
	    display: none;
	    position: absolute;
	    top: 0em !important;
	    left: 0em !important;
	    width: 100%;
	    height: 100%;
	    text-align: center;
	    vertical-align: middle;
	    background-color: rgba(255, 255, 255, 0.55);
	    opacity: 1;
	    line-height: 1;
	    -webkit-animation-fill-mode: both;
	    animation-fill-mode: both;
	    -webkit-animation-duration: 0.5s;
	    animation-duration: 0.5s;
	    -webkit-transition: background-color 0.5s linear;
	    transition: background-color 0.5s linear;
	    -webkit-user-select: none;
	    -moz-user-select: none;
	    -ms-user-select: none;
	    user-select: none;
	    will-change: opacity;
	    z-index: 9;
	}
	.div-loader{
	    position: absolute;
		top: 50%;
		left: 50%;
		margin: 0px;
		text-align: center;
		z-index: 1000;
		-webkit-transform: translateX(-50%) translateY(-50%);
		transform: translateX(-50%) translateY(-50%);
	}
	.kt-spinner:before {
	    width: 50px;
	    height: 50px;
	    margin-top: -10px;
	}
</style>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title kt-font-bolder kt-font-bolder" style="color:#646c9a;">
					Vendor Management
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
		        <div class="kt-portlet__head-wrapper">
		            <div class="kt-portlet__head-actions">
		                <a href="javascript:;" class="btn btn-brand btn-elevate btn-icon-sm rfq_search_filter_button" button_value = "show">
		                    Add Search Filter
		                </a>
		            </div>
		        </div>
	    	</div>
		</div>
	</div>
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__body">
			<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
				<div class="row">
					<div class="col-sm-12">
						<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 100%; min-height: 400px;">
							<thead id="rfq_list_header">
	      						<tr>
									<th class="sorting_disabled kt-align-center kt-font-bolder" style="color:#646c9a;width: 5%; color:#646c9a;">Sr.No</th>
									<th 
										class="sorting_search kt-align-left kt-font-bolder" 
										sorting_name=""
										sorting_value=""
										style="padding-left: 20px; width: 20%; color:#646c9a;">
										Vendor Details
									</th>
									<th 
										class="sorting_search kt-align-left kt-font-bolder" 
										sorting_name=""
										sorting_value=""
										style="padding-left: 20px; color:#646c9a;">
										Product/Material Details
									</th>
									<th 
										class="sorting_search kt-align-left kt-font-bolder" 
										sorting_name=""
										sorting_value=""
										style="padding-left: 20px; width: 10%; color:#646c9a;">
										Quanity Details
									</th>
								</tr>
							</thead>
							<tbody id="vendor_management_list_body"></tbody>
						</table>
					</div>
				</div>
				<div class="row" id="vendor_management_list_paggination"></div>
			</div>
		</div>
	</div>
	<div id="rfq_table_loader" class="layer-white">
		<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
	</div>
</div>


<!-- end:: Content -->