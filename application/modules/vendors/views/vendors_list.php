<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="kt-portlet ">
		<?php if($this->session->flashdata('lead_success')){ ?>
			<div class="alert alert-success" id="success-alert">
				<strong><?php echo $this->session->flashdata('lead_success'); ?></strong> 
			</div>
		<?php } ?>
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					Vendors List
				</h3>
			</div>
		</div>
		<div class="kt-portlet__body">
			<div class="row">
				<div class="col-md-2 kt-margin-b-10-tablet-and-mobile">
		 			<label for="product">Product :</label>
					<div class="dropdown bootstrap-select show-tick form-control kt-">
			        	<select class="form-control kt-selectpicker" id="product" multiple>
			        		<option value="">Select</option>
			  				<?php echo $prd_str; ?>
						</select>
					</div>
				</div>
				<div class="col-md-2 kt-margin-b-10-tablet-and-mobile">
			        <label>Material :</label>
					<div class="dropdown bootstrap-select show-tick form-control kt-">
						<select class="form-control kt-selectpicker" id="material" multiple>
							<option value="">Select Material</option>
			  				<?php echo $mat_str; ?>
						</select>
					</div>
				</div>
				<div class="col-md-2 kt-margin-b-10-tablet-and-mobile">
			        <label>Country :</label>
					<div class="dropdown bootstrap-select show-tick form-control kt-">
						<select class="form-control kt-selectpicker" id="vendor_list_country" multiple>
							<option value="">Select Country</option>
			  				<?php echo $country_str; ?>
						</select>
					</div>
				</div>
				<div class="col-md-2 kt-margin-b-10-tablet-and-mobile">
			        <label>Vendor Stage :</label>
					<div class="dropdown bootstrap-select show-tick form-control kt-">
						<select class="form-control kt-selectpicker" id="vendor_list_stage" multiple>
			  				<option value="">Select Vendor Stage</option>
							<option value="1">Stage 1</option>
							<option value="2">Stage 2</option>
							<option value="3">Stage 3</option>
							<option value="4">Stage 4</option>
							<option value="5">Stage 0</option>
						</select>
					</div>
				</div>
				<div class="col-md-2 kt-margin-b-10-tablet-and-mobile">
			        <label>Vendor Source :</label>
					<div class="dropdown bootstrap-select show-tick form-control kt-">
						<select class="form-control kt-selectpicker" id="vendor_list_source" multiple>
							<option value="">Select Vendor Source</option>
			  				<option value="primary leads">Primary Leads</option>
							<option value="hetregenous leads">Hetregenous Leads</option>
						</select>
					</div>
				</div>
				<div class="col-md-2 kt-margin-b-10-tablet-and-mobile">
			        <label>Procurement Person :</label>
					<div class="dropdown bootstrap-select show-tick form-control kt-">
						<select class="form-control kt-selectpicker" id="procurement_person_id" multiple>
							<option value="">Select Procurement Person</option>
			  				<?php foreach($procurement_person as $single_person_name) {?>
								<option value="<?php echo $single_person_name['user_id']; ?>"><?php echo $single_person_name['name']; ?></option>
			  				<?php }?>
						</select>
					</div>
				</div>
				<div class="col-md-8 kt-margin-b-10-tablet-and-mobile">
			        <label>Description :</label>
					<input type="text" class="form-control kt-input" name="description">
				</div>
			</div>
			<!--begin: Datatable -->
			<table class="table table-striped- table-bordered table-hover table-checkable" id="vendors_list">
				<thead>
					<tr>
						<th width="3%">Sr #</th>
						<th width="20%">Vendor Name</th>
						<th width="15%">Stage</th>
						<th width="15%">Person</th>
						<th width="12%">Contact</th>
						<th width="8%">Contact Mode</th>
						<th width="17%">Comments</th>
						<th width="10%">Actions</th>
					</tr>
				</thead>
			</table>

			<!--end: Datatable -->
		</div>
	</div>
</div>
<style type="text/css">
	.select2-container{
		width: 120px !important;
	}

	#vendors_list tbody td{
		border: none;
	}
</style>