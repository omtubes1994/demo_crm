jQuery(document).ready(function() {
	KTBootstrapSelect.init();
	$('table#vendors_list').on('click', 'a.delete_vendor', function(){
		set_reset_spinner($(this));
		swal({
			title: "Are you sure?",
			text: "Once deleted, you won't be able to recover this vendor!",
			icon: "warning",
			buttons:  ["Cancel", "Delete"],
			dangerMode: true,	
		})
		.then((willDelete) => {
		  	if (willDelete) {
				ajax_call_function({call_type: 'delete_vendor', vendor_id: $(this).attr('vendor-id')}, 'delete_vendor');
	  		} else {
			    swal({
		    		title: "Vendor is not deleted",
		      		icon: "info",
		    	});
		    	set_reset_spinner($(this), false);
		  	}
		});
	});
});	

function ajax_call_function(data, callType, url = "<?php echo base_url('vendors/ajax_function'); ?>") {

	$.ajax({
		type: 'POST',
		data: data,
		url: url,
		dataType: 'JSON',
		success: function(res){
			if(res.status == 'successful') {
				if(callType == 'delete_vendor') {
					sweet_alert(res.message);
					setTimeout(function(){
						location.reload();
					}, 2000)
				}
			}
		},
		beforeSend: function(response){
			
		}
	});
};

function set_reset_spinner(obj, set_unset_flag = true) {

	if(set_unset_flag) {

		$(obj).addClass('kt-spinner');
    	$(obj).addClass('kt-spinner--right');
    	$(obj).addClass('kt-spinner--sm');
    	$(obj).addClass('kt-spinner--light');	
	} else{

		$(obj).removeClass('kt-spinner');
    	$(obj).removeClass('kt-spinner--right');
    	$(obj).removeClass('kt-spinner--sm');
    	$(obj).removeClass('kt-spinner--light');
	}
}

function sweet_alert(title, icon = 'success') {

	swal({
			title: title,
			icon: icon,
    	});
}