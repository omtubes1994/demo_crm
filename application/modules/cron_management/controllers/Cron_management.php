<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron_management extends MX_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('common/common_model');
		$this->load->model('hr/hr_model');
		$this->load->model('cron_management/cron_model');
	}

	public function index() {

		$static_segment_3 = array('remove_margin_after_quotation_won', 'close_query_cron', 'pending_query_alert', 'create_default_activity_system_admin', 'set_ticket_as_overdue', 'create_people_information_user_id', 'send_daily_report_for_sales_on_sales_routine');
		$call_type = $this->uri->segment('3','');
		switch ($call_type) {
			case 'remove_margin_after_quotation_won':
				
				$this->remove_margin_after_quotation_won();	
			break;
			
			case 'close_query_cron':
				
				$this->close_query_cron();	
			break;

			case 'pending_query_alert':
				
				$this->pending_query_alert();	
			break;

			case 'create_default_activity_system_admin':
				
				$this->create_default_activity_system_admin();	
			break;

			case 'set_ticket_as_overdue':
				
				$this->set_ticket_as_overdue();	
			break;

			case 'create_people_information_user_id':
				
				$this->create_people_information_user_id();	
			break;
			
			###############PARAMETER############################
			# start date : 10-july-2023to13-july-2023 : end date
			####################################################
			case 'send_daily_report_for_sales_on_sales_routine':
				
				$data = array(
					'sales_routine_data'=> array(),
					'report_title'=> '',
					'report_date'=> ''
				);
				$date = $this->uri->segment('4','');
				$date_explode = explode("to", $date);
				$sales_routine_data_user_wise_where = array();
				if(count($date_explode) == 1){

					$sales_routine_data_user_wise_where['date(work_date)'] = date('Y-m-d', strtotime($date_explode[0]));
					$data['report_title'] = 'Sales Person Daily Report';
					$data['report_date'] = date('j F, Y', strtotime($date_explode[0]));
				}else if(count($date_explode) == 2){

					$sales_routine_data_user_wise_where['date(work_date) >='] = date('Y-m-d 00:00:00', strtotime($date_explode[0]));
					$sales_routine_data_user_wise_where['date(work_date) <='] = date('Y-m-d 23:59:59', strtotime($date_explode[1]));
					$data['report_title'] = 'Sales Person Weekly Report';
					$data['report_date'] = date('j F, Y', strtotime($date_explode[0]))." to ". date('j F, Y', strtotime($date_explode[1]));
				}
				// echo "<pre>";print_r($data);echo"</pre><hr>";
				$user_list =  $this->common_model->get_dynamic_data_sales_db('user_id, name', array('status'=>'1', 'role'=> 5), 'users');
				$routine_list =  array_column($this->common_model->get_dynamic_data_sales_db('id, name', array('status'=>'Active'), 'sales_routine'), 'name', 'id');
				foreach ($user_list as $single_user_details) {
					
					$data['sales_routine_data'][$single_user_details['name']] = array();

					$sales_routine_data_user_wise_where['status'] = 'Active';
					$sales_routine_data_user_wise_where['user_id'] = $single_user_details['user_id'];
					
					$sales_routine_data_user_wise =  $this->common_model->get_dynamic_data_sales_db('sales_routine_id, work_completed, work_comment, work_date', $sales_routine_data_user_wise_where, 'daily_work_on_sales_routine');
					// echo "<pre>";print_r($sales_routine_data_user_wise);echo"</pre><hr>";die('debug');

					if(!empty($sales_routine_data_user_wise)){

						foreach ($sales_routine_data_user_wise as $sales_routine_data_user_wise_value) {
						
							$count = count($data['sales_routine_data'][$single_user_details['name']]);
							$data['sales_routine_data'][$single_user_details['name']][$count]['name'] = $routine_list[$sales_routine_data_user_wise_value['sales_routine_id']];
							$data['sales_routine_data'][$single_user_details['name']][$count]['status'] = $sales_routine_data_user_wise_value['work_completed'];
							$data['sales_routine_data'][$single_user_details['name']][$count]['comment'] = $sales_routine_data_user_wise_value['work_comment'];
							$data['sales_routine_data'][$single_user_details['name']][$count]['date'] = date('j F, Y', strtotime($sales_routine_data_user_wise_value['work_date']));

						}
					}
				}
				// echo "<pre>";print_r($data);echo"</pre><hr>";die('debug');
				$mail_message = $this->load->view('common/pdf', $data, true);
				$this->sendMail(
								"Daily Report",
								"crm@omtubes.com",
								"Daily Report of Sales",
								$mail_message
							);
			break;
			
			default:

				echo "Please Enter one of the following parameter as segment!!!","<hr>","<br>";
				foreach ($static_segment_3 as $key => $value) {
					echo $key+1, ")   ",$value,"<br>";
				}
			break;
		}		
	}

	private function remove_margin_after_quotation_won() {

		// check cron status
		$cron_status = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'process_name'=>'margin_remove_on_quotation_won', 'process_date'=>date('Y-m-d', mktime(0, 0, 0, date('n'), date('d'), date('Y')))), 'cron_process_details');
		if(empty($cron_status)) {

			echo "<br>", "Cron Process Started!!!","<hr>";
			//inserting in cron details with pending status
			$cron_process_id = $this->common_model->insert_data_sales_db('cron_process_details', array('process_status'=>'pending', 'process_name'=>'margin_remove_on_quotation_won', 'process_date'=>date('Y-m-d', mktime(0, 0, 0, date('n'), date('d'), date('Y')))));
			
			if(!empty($cron_process_id)) {

				//getting quotation details
				$quotation_details = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Won', 'entered_on > ' => date('Y-m-d', strtotime('-2 months'))), 'quotation_mst', 'result_array');
				if(!empty($quotation_details)) {

					$this->common_model->update_data_sales_db('cron_process_details', array('process_status'=>'in_progress'), array('id'=>$cron_process_id));
					foreach ($quotation_details as $key => $value) {
						// $this->common_model->update_data_sales_db('quotation_dtl', array('unit_rate' => null, 'margin' => null, 'packing_charge' => null), array('quotation_mst_id' => $value['quotation_mst_id']));
					}
					$this->common_model->update_data_sales_db('cron_process_details', array('process_status'=>'completed'), array('id'=>$cron_process_id));
				}else{
				
					echo "<br>", "Quotation details added!!!","<hr>";
				}
			}else{
				
				echo "<br>", "Cron Process id not available!!!","<hr>";
			}

			echo "<br>", "Cron Process Ended!!!";
		}else{
			echo "<br>", "Process Done For today!!!","<hr>";
		}
	}

	private function close_query_cron(){

		// check cron status
		$cron_status = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'process_name'=>'autoclose_query_close_after_2days', 'process_date'=>date('Y-m-d', mktime(0, 0, 0, date('n'), date('d'), date('Y')))), 'cron_process_details');
		if(empty($cron_status)) {

			echo "<br>", "Cron Process Started!!!","<hr>";
			//inserting in cron details with pending status
			$cron_process_id = $this->common_model->insert_data_sales_db('cron_process_details', array('process_status'=>'pending', 'process_name'=>'autoclose_query_close_after_2days', 'process_date'=>date('Y-m-d', mktime(0, 0, 0, date('n'), date('d'), date('Y')))));
			if(!empty($cron_process_id)) {

				//getting rfq regret query details
				$regret_query_details = $this->common_model->get_dynamic_data_sales_db_null_false('*', "rfq_status = 'regret' AND rfq_regret_reason > 0", 'rfq_mst');

				if(!empty($regret_query_details)){

					foreach($regret_query_details as $single_data){

						//getting query details
						$query_details = $this->common_model->get_dynamic_data_sales_db_null_false('*', "status = 'Active' AND query_status = 'open' AND reply_add_time IS NOT NULL AND 'reply_add_time' > 'query_add_time' OR query_reference_id='".$single_data['rfq_mst_id']."' AND  query_reference LIKE '%".$single_data['rfq_no']."%' ", 'query_master');

						if(!empty($query_details)){

							foreach($query_details as $single_data){

								$this->common_model->update_data_sales_db('query_master', array('query_status'=>'closed'), array('id'=>$single_data['id']));
							}
						}
					}
					$this->common_model->update_data_sales_db('cron_process_details', array('process_status'=>'completed'), array('id'=>$cron_process_id));
					echo "<br>", "Query is Closed!!!","<hr>";
				}else{

					echo "<br>", "Query is not found!!!","<hr>";
				}
			}else{
				
				echo "<br>", "Cron Process id not available!!!","<hr>";
			}

			echo "<br>", "Cron Process Ended!!!";
		}else{
			echo "<br>", "Process Done For today!!!","<hr>";
		}
	}

    private function pending_query_alert() {

		// check cron status
		$cron_status = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'process_name'=>'pending_query_alert', 'process_date'=>date('Y-m-d', mktime(0, 0, 0, date('n'), date('d'), date('Y')))), 'cron_process_details');

		if(empty($cron_status)) {

			echo "<br>", "Cron Process Started!!!","<hr>";
			//inserting in cron details with pending status
			$cron_process_id = $this->common_model->insert_data_sales_db('cron_process_details', array('process_status'=>'pending', 'process_name'=>'pending_query_alert', 'process_date'=>date('Y-m-d', mktime(0, 0, 0, date('n'), date('d'), date('Y')))));
			
			if(!empty($cron_process_id)) {

				//getting quotation details
				$pending_query_details = $this->common_model->get_all_conditional_data_sales_db('count(*) as total_pending_query, query_assigned_id',array('status'=>'Active', 'query_status'=> 'open'), 'query_master', 'result_array', array(), array(), 'query_assigned_id');
				if(!empty($pending_query_details)) {

                  	foreach ($pending_query_details as  $assigned_person_details) {
			            $user_details = array();
			            $user_details = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>1, 'user_id'=> $assigned_person_details['query_assigned_id']), 'users', 'row_array');
			            if(!empty($user_details)) {

			                $sms_txt = 'Dear '.$user_details['name'].', Good Morning. You have '.$assigned_person_details['total_pending_query'].' query pending. - Om Tubes';
			                // $this->sendSms(9082159156, str_replace(' ', '%20', $sms_txt), '1307164551238202161');
			                $this->sendSms($user_details['mobile'], str_replace(' ', '%20', $sms_txt), '1307164551238202161');
			                echo "<br>", "Sms alert sent!!!","<hr>";
			            }
			        }
			        $this->common_model->update_data_sales_db('cron_process_details', array('process_status'=>'completed'), array('id'=>$cron_process_id));
				}else{
				
					echo "<br>", "Query details added!!!","<hr>";
				}
			}else{
				
				echo "<br>", "Cron Process id not available!!!","<hr>";
			}

			echo "<br>", "Cron Process Ended!!!";
		}else{
			echo "<br>", "Process Done For today!!!","<hr>";
		}
    }

    private function create_default_activity_system_admin() {

    	$cron_status = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'process_name'=>'create_default_activity_system_admin', 'process_date'=>date('Y-m-d', mktime(0, 0, 0, date('n'), date('d'), date('Y')))), 'cron_process_details');

		if(empty($cron_status)) {

			echo "<br>", "Cron Process Started!!!","<hr>";
			//inserting in cron details with pending status
			$cron_process_id = $this->common_model->insert_data_sales_db('cron_process_details', array('process_status'=>'pending', 'process_name'=>'create_default_activity_system_admin', 'process_date'=>date('Y-m-d', mktime(0, 0, 0, date('n'), date('d'), date('Y')))));
			
			if(!empty($cron_process_id)) {

				//getting quotation details
				//getting starting day of month and ending day
				$start_day = date('j', mktime(0, 0, 0, date('n'), 1, date('Y')));
				$end_day = date('j', mktime(0, 0, 0, date('n')+1, 0, date('Y')));
				// echo $start_day, "<hr>", $end_day, "<hr>";
				$monday_array = array();
				$tuesday_array = array();
				$wednesday_array = array();
				$thursday_array = array();
				$friday_array = array();
				$saturday_array = array();
				$sunday_array = array();
				for($i = $start_day; $i <= $end_day; $i++) {

					$day_name = date('l', mktime(0, 0, 0, date('n'), $i, date('Y')));
					switch ($day_name) {
						case 'Monday':
						
							$monday_array[] = date('Y-m-d 23:59:59', mktime(0, 0, 0, date('n'), $i, date('Y')));
						break;
						case 'Tuesday':
						
							$tuesday_array[] = date('Y-m-d 23:59:59', mktime(0, 0, 0, date('n'), $i, date('Y')));
						break;
						case 'Wednesday':
						
							$wednesday_array[] = date('Y-m-d 23:59:59', mktime(0, 0, 0, date('n'), $i, date('Y')));
						break;
						case 'Thursday':
						
							$thursday_array[] = date('Y-m-d 23:59:59', mktime(0, 0, 0, date('n'), $i, date('Y')));
						break;
						case 'Friday':
						
							$friday_array[] = date('Y-m-d 23:59:59', mktime(0, 0, 0, date('n'), $i, date('Y')));
						break;
						case 'Saturday':
						
							$saturday_array[] = date('Y-m-d 23:59:59', mktime(0, 0, 0, date('n'), $i, date('Y')));
						break;
						case 'Sunday':
						
							$sunday_array[] = date('Y-m-d 23:59:59', mktime(0, 0, 0, date('n'), $i, date('Y')));
						break;
					}
				}
				
				$static_array_every_month_work = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'type' => 'system_admin', 'ticket_cron_status'=> 'pending'), 'ticket');
				$day_array = array();
				$date = '';
				foreach ($static_array_every_month_work as $ticket_details) {
					
					$this->common_model->update_data_sales_db('ticket', array('ticket_cron_status'=> 'in_progress'), array('id'=>$ticket_details['id']));
					$insert_array = array();
		        	$ticket_repeat_day=explode(" ",$ticket_details['ticket_repeat_day']);
			        switch ($ticket_repeat_day[1]) {
						case 'Monday':
		                	$day_array = $monday_array;
		                break;
		                case 'Tuesday':
		                	$day_array = $tuesday_array;
		                break;
		                case 'Wednesday':
		                	$day_array = $wednesday_array;
		                break;
		                case 'Thursday':
		                	$day_array = $thursday_array;
		                break;
		                case 'Friday':
		                	$day_array = $friday_array;
		                break;
		                case 'Saturday':
		                	$day_array = $saturday_array;
		                break;
		                case 'Sunday':
		                	$day_array = $sunday_array;
		                break;
		                case 'Day':
		                	$date = $start_day;
		                	if($ticket_details['ticket_repeat_day'] == 'Last Day') {

		                		$date = $end_day;
		                	}
		                break;
		            }
		            if($ticket_repeat_day[0] == 'Every'){

		            	if(!empty($day_array)) {

							foreach ($day_array as $single_date) {
								$insert_array[] = array(
														'ticket_id' => $ticket_details['id'],
														'user_id' => 109,
														'user_department_id' => 1,
														'user_comment'=>$ticket_details['name'],
														'ticket_status'=>'Pending',
														'ticket_close_time'=>$single_date,
														'add_time'=>date('Y-m-d 00:00:00', strtotime($single_date)),
														'update_time'=>date('Y-m-d 00:00:00', strtotime($single_date))
													);
							}
		            	}
					
					}else if(in_array($ticket_details['ticket_repeat_day'], array('First Day', 'Last Day'))){
							
						if(!empty($date)) {

							$insert_array[] = array(
													'ticket_id' => $ticket_details['id'],
													'user_id' => 109,
													'user_department_id' => 1,
													'user_comment'=>$ticket_details['name'],
													'ticket_status'=>'Pending',
													'ticket_close_time'=> date('Y-m-d 23:59:59', mktime(0, 0, 0, date('n'), $date, date('Y'))),
													'add_time'=>date('Y-m-d 00:00:00', mktime(0, 0, 0, date('n'), $date, date('Y'))),
													'update_time'=>date('Y-m-d 00:00:00', mktime(0, 0, 0, date('n'), $date, date('Y')))
												);
						}	
						
					}else if(in_array($ticket_repeat_day[0], array('First', 'Second', 'Third', 'Fourth', 'Fifth'))){

						$static_day_key_name = array('First'=>0, 'Second'=>1, 'Third'=>2, 'Fourth'=>3, 'Fifth'=>4);
						if(!empty($static_day_key_name[$ticket_repeat_day[0]])) {

							if(!empty($day_array[$static_day_key_name[$ticket_repeat_day[0]]])) {
						
								$insert_array[] = array(
														'ticket_id' => $ticket_details['id'],
														'user_id' => 109,
														'user_department_id' => 1,
														'user_comment'=>$ticket_details['name'],
														'ticket_status'=>'Pending',
														'ticket_close_time'=>$day_array[$static_day_key_name[$ticket_repeat_day[0]]],
														'add_time'=>date('Y-m-d 00:00:00', strtotime($day_array[$static_day_key_name[$ticket_repeat_day[0]]])),
														'update_time'=>date('Y-m-d 00:00:00', strtotime($day_array[$static_day_key_name[$ticket_repeat_day[0]]]))
													);
							}
						}

					}

					if(!empty($insert_array)) {

						// echo "<pre>";print_r($insert_array);echo"</pre><hr>";
						$this->common_model->insert_data_sales_db('ticket_management_details', $insert_array, 'batch');
						$this->common_model->update_data_sales_db('ticket', array('ticket_cron_status'=> 'completed'), array('id'=>$ticket_details['id']));
					}
				}
				$this->common_model->update_data_sales_db('cron_process_details', array('process_status'=>'completed'), array('id'=>$cron_process_id));
				
			}else{
				
				echo "<br>", "Cron Process id not available!!!","<hr>";
			}

			echo "<br>", "Cron Process Ended!!!";
		}else{
			echo "<br>", "Process Done For today!!!","<hr>";
		}
	}

	private function set_ticket_as_overdue() {

		// check cron status
		$cron_status = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'process_name'=>'set_ticket_as_overdue', 'process_date'=>date('Y-m-d', mktime(0, 0, 0, date('n'), date('d'), date('Y')))), 'cron_process_details');

		if(empty($cron_status)) {

			echo "<br>", "Cron Process Started!!!","<hr>";
			//inserting in cron details with pending status
			$cron_process_id = $this->common_model->insert_data_sales_db('cron_process_details', array('process_status'=>'pending', 'process_name'=>'set_ticket_as_overdue', 'process_date'=>date('Y-m-d', mktime(0, 0, 0, date('n'), date('d'), date('Y')))));
			
			if(!empty($cron_process_id)) {

				//getting quotation details
					$ticket_details =  $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'ticket_status'=>'Pending'), 'ticket_management_details');
						foreach ($ticket_details as $single_ticket_details) {
							
							if(date('Y') == date('Y', strtotime($single_ticket_details['ticket_close_time']))) {

								if(date('n') == date('n', strtotime($single_ticket_details['ticket_close_time']))) {

									if(date('d') > date('d', strtotime($single_ticket_details['ticket_close_time']))) {

										$this->common_model->update_data_sales_db('ticket_management_details', array('ticket_status'=>'Overdue'), array('id'=>$single_ticket_details['id']));
									}
								}
							}

						}
					$this->common_model->update_data_sales_db('cron_process_details', array('process_status'=>'completed'), array('id'=>$cron_process_id));
			}else{
				
				echo "<br>", "Cron Process id not available!!!","<hr>";
			}

			echo "<br>", "Cron Process Ended!!!";
		}else{
			echo "<br>", "Process Done For today!!!","<hr>";
		}
	}

	private function create_people_information_user_id() {

		// check cron status
		$cron_status = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'process_name'=>'create_people_information_user_id', 'process_date'=>date('Y-m-d', mktime(0, 0, 0, date('n'), date('d'), date('Y')))), 'cron_process_details');

		if(empty($cron_status)) {

			echo "<br>", "Cron Process Started!!!","<hr>";
			//inserting in cron details with pending status
			$cron_process_id = $this->common_model->insert_data_sales_db('cron_process_details', array('process_status'=>'pending', 'process_name'=>'create_people_information_user_id', 'process_date'=>date('Y-m-d', mktime(0, 0, 0, date('n'), date('d'), date('Y')))));
			
			if(!empty($cron_process_id)) {

				$this->common_model->update_data_sales_db('cron_process_details', array('process_status'=>'completed'), array('id'=>$cron_process_id));
				//getting  details
				$this->hr_model->create_people_information_user_id();
			}else{
				
				echo "<br>", "Cron Process id not available!!!","<hr>";
			}

			echo "<br>", "Cron Process Ended!!!";
		}else{
			echo "<br>", "Process Done For today!!!","<hr>";
		}
    }

    private function sendSms($mobile,$sms_txt, $dlt_id){ 

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.msg91.com/api/sendhttp.php?authkey=345359A1zGdmFe5f930cf3P1&mobiles=91".$mobile."&message=".$sms_txt."&sender=OMTUBE&route=4&DLT_TE_ID=".$dlt_id,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        
        if ($err) {
          // echo "cURL Error #:" . $err;
        } else {
          // echo $response,"<hr>";
        }
    }

	public function sendMail(
							$from_email_message = "OTP Verification",
							$to_email = "crm@omtubes.com",
							$subject = "Om tubes OTP Verification",
							$message = ""){

		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'crm@omtubes.com',
			'smtp_pass' => 'ehvwcmgmzdduyhaj',
			'smtp_crypto' => 'ssl',
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'smtp_timeout' => '4', //in seconds
			'wordwrap' => TRUE
		);
        $res = $this->load->library('email', $config);
      	$this->email->set_newline("\r\n");
      	$this->email->from("crm@omtubes.com", $from_email_message); // change it to yours
      	$this->email->to($to_email);// change it to yours
      	$this->email->subject($subject);
      	$this->email->message($message);
      	$response = $this->email->send();
      	echo "<pre>";var_dump($response);echo"</pre><hr>";die('debug');
      	echo $response;
	}
}