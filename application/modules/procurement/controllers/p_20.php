<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Procurement extends MX_Controller {
	public function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('user_id')){
			redirect('login', 'refresh');
			exit;
		}else{
			// $access_count = 0;
			// if(!empty($this->session->userdata('main_module_access')) && in_array(9, array_column($this->session->userdata('main_module_access'), 'module_id'))) {

			// 	foreach ($this->session->userdata('sub_module_access') as $key => $value) {
					
			// 		if($value['module_id'] == 9 && $value['status'] == 'Active') {
			// 			$access_count++;
			// 		}
			// 	}
			// }
			// if($access_count == 0){
			// 	redirect($this->session->userdata('home/dashboard'));
			// 	exit;
			// }
		}
		$this->load->model('procurement_model');
	}

	public function index(){
		$this->add();
	}

	public function procurement_form(){
		$this->load->view('header', array('title' => 'Procurement Form'));
		$this->load->view('sidebar', array('title' => 'Procurement Form'));
		$this->load->view('procurement_form');
		$this->load->view('footer');
	}

	public function procurement_list(){
		$this->load->view('header', array('title' => 'Procurement List'));
		$this->load->view('sidebar', array('title' => 'Procurement List'));
		$this->load->view('procurement_list');
		$this->load->view('footer');
	}

	public function addRFQ($rfq_id=0){
		if(!empty($this->input->post())){
			$insert_arr = array(
				'rfq_sentby' => $this->input->post('rfq_sentby'),
				'rfq_company' => $this->input->post('rfq_company'),
				'rfq_buyer' => $this->input->post('rfq_buyer'),
				'client_source' => $this->input->post('client_source'),
				'rfq_rank' => $this->input->post('rfq_rank'),
				'rfq_lastbuy' => $this->input->post('rfq_lastbuy'),
				'rfq_subject' => $this->input->post('rfq_subject'),
				'rfq_importance' => $this->input->post('rfq_importance'),
				'assigned_to' => $this->input->post('assigned_to'),
				'reference' => $this->input->post('reference'),
				'rfq_status' => $this->input->post('rfq_status'),
				'modified_on' => date('Y-m-d H:i:s')
			);

			if($this->input->post('rfq_closedate') != ''){
				$insert_arr['rfq_closedate'] = date('Y-m-d', strtotime($this->input->post('rfq_closedate')));
			}else{
				$insert_arr['rfq_closedate'] = null;
			}

			if($this->input->post('rfq_mst_id') > 0){
				$rfq_mst_id = $this->input->post('rfq_mst_id');
				$this->procurement_model->updateData('rfq_mst', $insert_arr, array('rfq_mst_id' => $rfq_mst_id));
				$this->procurement_model->deleteData('rfq_dtl', array('rfq_mst_id' => $rfq_mst_id));
			}
			else{
				$insert_arr['rfq_date'] = date('Y-m-d');
				$insert_arr['entered_on'] = date('Y-m-d H:i:s');
				$insert_arr['rfq_no'] = 'OM/'.$this->procurement_model->getRFQNo().'/20-21';
				$rfq_mst_id = $this->procurement_model->insertData('rfq_mst', $insert_arr);
			}
			if(!empty($this->input->post('product_id'))) {
				foreach ($this->input->post('product_id') as $key => $value) {
					$dtl_array  = array(
						'rfq_mst_id' => $rfq_mst_id,
						'product_id' => $this->input->post('product_id')[$key],
						'material_id' => $this->input->post('material_id')[$key],
						'description' => $this->input->post('description')[$key],
						'unit' => $this->input->post('unit')[$key],
						'quantity' => $this->input->post('quantity')[$key],
					);
					$this->procurement_model->insertData('rfq_dtl', $dtl_array);
				}
			}
			//echo $this->db->last_query();
			// redirect('procurement/addRFQ/'.$rfq_mst_id, 'refresh');
			if(!empty($rfq_id)) {

				redirect('procurement/addRFQ/'.$rfq_id, 'refresh');
			}else {

				redirect('procurement/addRFQ/', 'refresh');
			}
		}else{
			$data['prd_str'] = $data['mat_str'] = $data['unit_str'] = $data['vendor_str'] = '';
			if($rfq_id > 0){
				$data['rfq_id'] = $rfq_id;
				$data['rfq_details'] = $this->procurement_model->getRfq($rfq_id);
				$data['rfq_details'][0]['rfq_company_name'] = $this->procurement_model->getLeadName($data['rfq_details'][0]['rfq_company'], $data['rfq_details'][0]['client_source']);
				$data['vendors'] = $vendors = $this->procurement_model->getData('vendors');
				foreach($vendors as $ven){ 
					$data['vendor_str'] .= '<option value="'.$ven['vendor_id'].'">'.ucwords(strtolower($ven['vendor_name'])).'</option>';
				}
				$data['rfq_to_vendor'] = $this->procurement_model->getData('rfq_to_vendor', 'rfq_id = '.$rfq_id);
				$data['rfq_notes'] = $this->procurement_model->getData('rfq_note_query', "type = 'notes' and rfq_id = ".$rfq_id);
				$data['rfq_query'] = $this->procurement_model->getData('rfq_note_query', "type = 'query' and rfq_id = ".$rfq_id);
			}
			$data['clients'] = $this->procurement_model->getClients();
			$data['sales_person'] = $this->procurement_model->getData('users', '(role = 5  OR role = 16) and status = 1');
			$data['purchase_person'] = $this->procurement_model->getData('users', '(role = 6 or role = 8) and status = 1');
			$data['product'] = $product = $this->procurement_model->getData('lookup', 'status="Active" AND lookup_group = 259');
			foreach($product as $prod){ 
				$data['prd_str'] .= '<option value="'.$prod['lookup_id'].'">'.ucwords(strtolower($prod['lookup_value'])).'</option>';
			}

			$data['material'] = $material = $this->procurement_model->getData('lookup', 'status="Active" AND lookup_group = 272');
			foreach($material as $mat){ 
				$data['mat_str'] .= '<option value="'.$mat['lookup_id'].'">'.ucwords(strtolower($mat['lookup_value'])).'</option>';
			}

			$data['units'] = $units = $this->procurement_model->getData('units');
			foreach($units as $un){
				$data['unit_str'] .= '<option value="'.$un['unit_id'].'">'.ucwords(strtolower($un['unit_value'])).'</option>';
			}

			$data['country'] = $product = $this->procurement_model->getData('lookup', 'lookup_group = 2');
			$data['region'] = $product = $this->procurement_model->getData('lookup', 'lookup_group = 1');
			
			$this->load->view('header', array('title' => 'Add RFQ'));
			$this->load->view('sidebar', array('title' => 'Add RFQ'));
			$this->load->view('addrfq_form', $data);
			$this->load->view('footer');
		}
	}

	function getMembers(){
		$members = $this->procurement_model->getMembers($this->input->post());
		echo json_encode($members);
	}

	function rfq_list(){
		$data['sales_person'] = $data['purchase_person'] = '';
		$sales_person = $this->procurement_model->getData('users', '(role = 5  OR role = 16) and status = 1');
		foreach($sales_person as $sp){
			$data['sales_person'] .= '<option value="'.$sp['user_id'].'">'.ucwords(strtolower($sp['name'])).'</option>';
		}
		
		$purchase_person = $this->procurement_model->getData('users', '(role = 6 or role = 8) and status = 1');
		foreach($purchase_person as $pp){
			$data['purchase_person'] .= '<option value="'.$pp['user_id'].'">'.ucwords(strtolower($pp['name'])).'</option>';
		}
		$this->load->view('header', array('title' => 'RFQ List'));
		$this->load->view('sidebar', array('title' => 'RFQ List'));
		$this->load->view('rfq_list', $data);
		$this->load->view('footer');
	}

	function rfq_list_data(){
		// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
		$search = array();
		foreach ($this->input->post('columns') as $key => $value) {
			$search_key = '';
			if($this->session->userdata('role') == 5){
				if($key == 1){
					$search_key = 'rfq_no';
				}else if($key == 2){
					$search_key = 'rfq_company';
				}else if($key == 7){
					$search_key = 'r.assigned_to';
				}else if($key == 8){
					$search_key = 'rfq_status';
				}else if($key == 5){
					$search_key = 'rfq_date';
				}else if($key == 6){
					$search_key = 'rfq_importance';
				}else if($key == 9){
					$search_key = 'quote_status';
				}
			}else{
				if($key == 1){
					$search_key = 'rfq_no';
				}else if($key == 2){
					$search_key = 'rfq_company';
				}else if($key == 8){
					$search_key = 'r.assigned_to';
				}else if($key == 9){
					$search_key = 'rfq_status';
				}else if($key == 7){
					$search_key = 'rfq_sentby';
				}else if($key == 5){
					$search_key = 'rfq_date';
				}else if($key == 6){
					$search_key = 'rfq_importance';
				}else if($key == 10){
					$search_key = 'quote_status';
				}
			}

			if($search_key == 'rfq_date'){
				if($this->input->post('columns')[$key]['search']['value'] != ''){
					$search[$search_key] = date('Y-m-d', strtotime($this->input->post('columns')[$key]['search']['value']));	
				}else{
					$search[$search_key] = '';
				}
			}else{
				$search[$search_key] = $this->input->post('columns')[$key]['search']['value'];
			}
		}
		if(!empty($this->input->post('rfq_priority'))) {

			$search['priority'] = $this->input->post('rfq_priority');
		}
		//print_r($this->input->post('length'));
		$order_by = $this->input->post('columns')[$this->input->post('order')[0]['column']]['data'];
		$dir = $this->input->post('order')[0]['dir'];
		if($order_by == 'record_id'){
			$order_by = 'rfq_no';
			$dir = 'desc';
		}else if($order_by == 'company_name'){
			$order_by = 'rfq_company';
		}
		$records = $this->procurement_model->getRFQListData($this->input->post('start'), $this->input->post('length'), $search, $order_by, $dir);
		$data['iTotalDisplayRecords'] = $data['iTotalRecords'] = $this->procurement_model->getRFQListCount($search);
		$data['aaData'] = $records;
		//echo "<pre>";print_r($data);
		echo json_encode($data);
	}

	function assignVendor(){
		$insert_arr = array(
			'rfq_id' => $this->input->post('rfq_id'),
			'vendor_id' => $this->input->post('vendor_id'),
			'vendor_status' => $this->input->post('vendor_status'),
			'evaluate_price' => $this->input->post('evaluate_price'),
			'evaluate_delivery' => $this->input->post('evaluate_delivery'),
			'modified_on' => date('Y-m-d H:i:s'),
			'modified_by' => $this->session->userdata('user_id')
		);

		if($this->input->post('connect_id') > 0){
			$this->procurement_model->updateData('rfq_to_vendor', $insert_arr);	
		}else{
			$insert_arr['entered_on'] = date('Y-m-d H:i:s');
			$insert_arr['entered_by'] = $this->session->userdata('user_id');
			$this->procurement_model->insertData('rfq_to_vendor', $insert_arr);	
		}

	}

	function viewPdf($connect_id){
		$data['rfq_details'] = $this->procurement_model->getRFQDetails($connect_id);
		$data['vendor_details'] = $this->procurement_model->getVendorDetails($connect_id);
		$vendor_sales_person = "Sir/Madam,";
		if(!empty($data['vendor_details']) && $data['vendor_details']['name']) {

			$vendor_sales_person = $data['vendor_details']['name'].',';
		}
		$this->load->library('Pdf');
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		

		// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, NULL, '', array(0,0,0), array(255,255,255));
		$pdf->SetFooterData(array(0,0,0), array(255,255,255));

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(10, PDF_MARGIN_TOP-10, 10);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		    require_once(dirname(__FILE__).'/lang/eng.php');
		    $pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('helvetica', '', 10);

		// add a page
		$pdf->AddPage();

		

		// define some HTML content with style


		$table_str = '';

		$i=0;
        foreach ($data['rfq_details'] as $key => $value) { 
			$bg_color = '';
			if($i%2 != 0){
				$bg_color = '#e4e1e1;';
			}

			if(strpos($value['quantity'], '.') === false){
				$value['quantity'] = $value['quantity'].".00";
			}

			$table_str .= '<tr style="background-color: '.$bg_color.'; font-size: 11px; font-family: courier;">
				<td style="text-align: right;">'.++$i.'</td>
				<td>'.$value['description'].'</td>
				<td style="text-align: right;"> '.$value['quantity'].'</td>
				<td>'.$value['unit_value'].'.</td>
			</tr>';
        }

        $date = date('d-m-Y', strtotime($data['rfq_details'][0]['rfq_date']));
		$client_name = trim($data['vendor_details']['vendor_name']);

		$html = '<!-- EXAMPLE OF CSS STYLE -->
		<table cellpadding="5" cellspacing="0">
			<tr style="background-color: #fff;">
				<td width="50%" style="padding:5px;vertical-align: text-top; ">
					<img src="/assets/media/client-logos/logo.png" width="180" height="50" style="padding-left: 10px;"><br/>
					<strong style="font-size: 17px;">OM TUBES & FITTINGS INDUSTRIES</strong>
					<div class="left_addrs" style="font-size: 14px;color: #484545; line-height: 20px;">
						10 Bordi Bunglow, 1st Panjarapole Lane, CP Tank, Mumbai, Maharashtra, India <br/>
						GSTIN 27AFRPM5323E1ZC
					</div>
					<table style="margin-top: -15px;">
						<tr style="background-color: #fff;">
							<td style="font-size: 14px; color: #484545; line-height: 20px;" width="50%">+91 (22) 6743 7634</td>
						</tr>
						<tr style="background-color: #fff;">
							<td style="font-size: 14px; color: #484545; line-height: 20px;" align="left">www.omtubes.com</td>
						</tr>
					</table>
				</td>
			<td width="50%">
				<strong style="font-size: 25px; text-align: right; line-height: 35px;">Request For Quotation</strong><br/>
				<table style="line-height: 22px;">
					<tr style="background-color: #e4e1e1; border-bottom: 1px solid;">
						<td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;"> RFQ # : </strong></td>
						<td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;">Date : </strong></td>
					</tr>
					<tr>
						<td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;">'.$data['rfq_details'][0]['rfq_no'].'</td>
						<td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;">'.$date.'</td>
					</tr>
					<tr style="background-color: #e4e1e1;">
						<td colspan="2" style="padding:5px;vertical-align: text-top;"><strong style="font-size: 12px;"> Seller:</strong></td>
					</tr>
					<tr>
						<td colspan="2" style="padding:5px;vertical-align: text-top; font-family: courier;">
							<strong>'.$client_name.'</strong>
							<div class="left_addrs" style="font-size: 11px;color: #484545; line-height: 17px; font-family: courier;">'.$data['vendor_details']['country'].'<br/>'.$data['vendor_details']['name'].'
							</div>
						</td>
					</tr>
					<tr style="background-color: #e4e1e1;">
						<td colspan="2" style="padding:5px;vertical-align: text-top;"><strong style="font-size: 12px;">Our reference :</strong></td>
					</tr>
					<tr>
						<td colspan="2" style="font-family: courier; font-size: 11px;color: #484545;">'.$data['rfq_details'][0]['reference'].'</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr style="background-color: #fff;">
			<td colspan="2" style="padding:5px;vertical-align: text-top; font-family: courier;"><strong style="font-size: 16px;">URGENT</strong><br/><br/>
Dear '.$vendor_sales_person.'<br/><br/>
please submit your quotation according to the following specification<br/>
- price terms: as specified, including standard packing<br/>
- validity of quotation: 90 days minimum<br/>
- delivery time<br/>
- resales discount<br/>
- estimated total weight/ -volume<br/><br/>
Thank you very much.
</td>
		</tr>
		<tr>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td colspan="2">
				<table cellspacing="0" cellpadding="10" border="0">
					<thead>
						<tr style="background-color: #e4e1e1;">
							<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="7%;">#</td>
							<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="63%">Item Description</td>
							<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="15%">Quantity</td>
							<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="15%">Unit</td>
						</tr>
					</thead>
					<tbody>'.$table_str.'</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td></td>
		</tr>
		<tr>
			<td style="font-size: 11px; font-family: courier;">
				<b style="font-size: 16px;">Additional Notes</b><br/>
				Please provide reason/explanation for possible deviation/discrepancy to our enquiry-specification and support<br/><br/>OM Tubes & Fittings Industries has conducted its business for decades with integrity. 
Therefore, we also expect our suppliers to ensure our values. 
For additional details, please contact us.
			</td>
			<td align="center">
				Thank you for your business<br/>
				For Om Tubes & Fittings Industries<br/><br/>
				<img src="/assets/media/stamp.png"/><br/><br/>
				<table>
					<tr>
						<td width="26%" rowspan="3"></td>
					 	<td align="left" width="74%" >Name : <span style="font-family: courier; ">'.$data['rfq_details'][0]['uname'].'</span></td>
					</tr>
					<tr>
						<td align="left">Email : <span style="font-family: courier;">'.$data['rfq_details'][0]['uemail'].'</span></td>
					</tr>
					<tr>
						<td align="left">Mobile : <span style="font-family: courier;">'.$data['rfq_details'][0]['umobile'].'</span></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>';

		// output the HTML content
		$pdf->writeHTML($html, true, false, true, false, '');

		//Close and output PDF document
		$pdf->Output(str_replace('/', '-', $data['rfq_details'][0]['rfq_no']).'.pdf', 'I');
	
	}

	function addNotes(){
		$note = $this->input->post('notes');
		$rfq_id = $this->input->post('rfq_id');

		$this->procurement_model->insertData('rfq_note_query', array('rfq_id' => $rfq_id, 'note' => $note, 'entered_on' => date('Y-m-d H:i:s'), 'entered_by' => $this->session->userdata('user_id'), 'type' => 'notes'));
		$res = $this->procurement_model->getData('rfq_note_query', "type='notes' and rfq_id = ".$rfq_id);
		foreach ($res as $key => $value) {
			$res[$key]['entered_on'] = date('d M h:i a', strtotime($value['entered_on']));
		}
		echo json_encode($res);
	}

	function addQuery(){
		$note = $this->input->post('notes');
		$rfq_id = $this->input->post('rfq_id');

		$this->procurement_model->insertData('rfq_note_query', array('rfq_id' => $rfq_id, 'note' => $note, 'entered_on' => date('Y-m-d H:i:s'), 'entered_by' => $this->session->userdata('user_id'), 'type' => 'query'));
		$res = $this->procurement_model->getData('rfq_note_query', "type='query' and rfq_id = ".$rfq_id);
		foreach ($res as $key => $value) {
			$res[$key]['entered_on'] = date('d M h:i a', strtotime($value['entered_on']));
		}
		echo json_encode($res);	
	}

	function searchLead(){
		$search = $this->input->post('search');
		$leads = $this->procurement_model->getClients($search);
		echo json_encode($leads); 
	}

	function deleteRfq(){
		$rfq_id = $this->input->post('rfq_id');
		$this->procurement_model->deleteData('rfq_note_query', array('rfq_id' => $rfq_id));
		$this->procurement_model->deleteData('rfq_to_vendor', array('rfq_id' => $rfq_id));
		$this->procurement_model->deleteData('rfq_dtl', array('rfq_mst_id' => $rfq_id));
		$this->procurement_model->deleteData('rfq_mst', array('rfq_mst_id' => $rfq_id));
	}

	public function procurement_listingz() {

		$data = $this->create_procurement_data(array('status'=> "Active"), array('column_name'=>'id', 'column_value'=>'asc'), 10, 0);
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;  
		$this->load->view('header', array('title' => 'Procurement Listing'));
		$this->load->view('sidebar', array('title' => 'Procurement Listing'));
		$this->load->view('procurement/procurement_listingz', $data);
		$this->load->view('footer');
	}

	public function update_purchase_order() {

		$this->unset_session();
		$data = array();
		$data['procurement_data'] = array('id'=>'', 'subject'=>'', 'procurement_person'=> '','sales_person'=> '','vendor_name'=> '','vendor_country'=> '','vendor_sales_person_name'=> '','procurement_status'=> '','comments'=> '','delivery_to'=> '','currency'=> '','delivery_time'=> '','country_of_origin'=> '','payment'=> '','mtc_type'=> '','validity'=> '','packing_type'=>'');
		$data['next_count_number'] = 1;
		$procurement_id = $this->uri->segment(3, 0);
		if(!empty($procurement_id)) {
			$data['procurement_data'] = $this->procurement_model->get_all_data('*', array('status'=> "Active", 'id'=> $procurement_id), 'procurement','row_array');
			$data['product_details'] = $this->procurement_model->get_all_data('*', array('status'=> "Active", 'procurement_id'=> $procurement_id), 'procurement_product_information');
			$data['charges_details'] = $this->procurement_model->get_all_data('*', array('status'=> "Active", 'procurement_id'=> $procurement_id), 'procurement_charges_information');
			$this->generate_session($data['procurement_data'], $data['product_details'], $data['charges_details']);
			$data['next_count_number'] = count($data['product_details']) +1;
		}
		$data['sales_person'] = $this->procurement_model->get_sales_person('name, user_id');
		$data['procurement_person'] = $this->procurement_model->get_procurement_person();
		$data['vendor_list'] = $this->procurement_model->get_vendor_list();
		$data['vendor_sales_person'] = $this->procurement_model->get_vendor_sales_person();
		$data['product_list'] = $this->procurement_model->get_lookup_data(259);
		$data['material_list'] = $this->procurement_model->get_lookup_data(272);
		$data['units_list'] = $this->procurement_model->get_units_data();
		$data['delivery'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'delivery');
		$data['currency'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'currency');
		$data['delivery_time'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'delivery_time');
		$data['origin_country'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'origin_country');
		$data['payment_terms'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'payment_terms');
		$data['mtc_type'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'mtc_type');
		$data['validity'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'validity');
		$data['transport_mode'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'transport_mode');
		$footer_text = $this->procurement_model->get_all_data('footer_text', array('status'=> 'Active', 'pdf_type'=> 'procurement'), 'pdf_footer', 'row_array');
		$footer = '';
 		$footer = $footer_text['footer_text'];
		if($this->session->userdata('user_id') == 23) {

			$footer = str_replace('procurement@omtubes.com', 'yash@omtubes.com', $footer);
		} else if($this->session->userdata('user_id') == 45) {

			$footer = str_replace('procurement@omtubes.com', 'rfq@omtubes.com', $footer);
		} else if($this->session->userdata('user_id') == 77) {

			$footer = str_replace('procurement@omtubes.com', 'sourcing@omtubes.com', $footer);
		} else if($this->session->userdata('user_id') == 42) {

			$footer = str_replace('procurement@omtubes.com', 'purchasing@omtubes.com', $footer);
		} else if($this->session->userdata('user_id') == 33) {

			$footer = str_replace('procurement@omtubes.com', 'technical@omtubes.com', $footer);
		}
		$data['footer_text'] = $footer;
		//echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Purchase Order'));
		$this->load->view('sidebar', array('title' => 'Purchase Order'));
		$this->load->view('procurement/update_purchase_order', $data);
		$this->load->view('footer');
	}

	private function generate_session($procurement_data, $product_details, $charges_details) {

		$charge_name_array = array('Added'=>'+', 'Deducted'=>'-');
		$order_details['net_total'] = $procurement_data['net_total'];
		$order_details['gross_total'] = $procurement_data['gross_total'];
		foreach ($product_details as $product_details_key => $product_details_value) {
			$order_details['all_net_total'][$product_details_key+1] = $product_details_value['total'];
		}	
		foreach ($charges_details as $charges_details_key => $charges_details_value) {
			
			$order_details['different_charges'][$charges_details_key]['charges_name'] = $charges_details_value['charge_name'];
			$order_details['different_charges'][$charges_details_key]['add_minus_charges'] = $charge_name_array[$charges_details_value['charge_deducted_or_added']];
			$order_details['different_charges'][$charges_details_key]['charge_value'] = $charges_details_value['charge_value'];
			$order_details['different_charges'][$charges_details_key]['add_minus_charges_value'] = $charges_details_value['charge_deducted_or_added'];
			$order_details['different_charges'][$charges_details_key]['gross_total'] = $charges_details_value['gross_total_after_charge'];
		}
		$this->session->set_userdata('order_details', $order_details);
	}

	public function unset_session() {
		$this->session->unset_userdata('order_details');
	}
	
	public function show_session() {

		echo "<pre>";print_r($this->session->userdata());echo"</pre><hr>";exit;
	}

	public function pdf(){

		$procurement_id = $this->uri->segment('3',0);
		$data['procurement_data'] = $this->procurement_model->get_all_data('*', array('status'=> "Active", 'id'=> $procurement_id), 'procurement','row_array');
		$data['product_details'] = $this->procurement_model->get_all_data('*', array('status'=> "Active", 'procurement_id'=> $procurement_id), 'procurement_product_information');
		$data['charges_details'] = $this->procurement_model->get_all_data('*', array('status'=> "Active", 'procurement_id'=> $procurement_id), 'procurement_charges_information');
		$data['grand_total_words'] = 'INR : '.$this->numberTowords($data['procurement_data']['gross_total'], 'paise');
		$data['purchase_person_details'] = $this->procurement_model->get_all_data('*', array('name'=> $data['procurement_data']['procurement_person']), 'users','row_array');
		$data['country_list'] = array_column($this->procurement_model->get_all_data('country_id, country_name', array('status'=> "Active"), 'country'), 'country_name', 'country_id');
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		// $this->load->view('procurement/procurement_pdf', $data);	
		$this->load->library('Pdf');
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

			// set document information
			

			// set default header data
			$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, NULL, '', array(0,0,0), array(255,255,255));
			$pdf->SetFooterData(array(0,0,0), array(255,255,255));

			// set header and footer fonts
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			// set margins
			$pdf->SetMargins(10, PDF_MARGIN_TOP-10, 10);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER-5);

			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM-15);

			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			// set some language-dependent strings (optional)
			if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			    require_once(dirname(__FILE__).'/lang/eng.php');
			    $pdf->setLanguageArray($l);
			}

			// ---------------------------------------------------------

			// set font
			$pdf->SetFont('helvetica', '', 10);

		// add a page
		$pdf->AddPage();
		$html= $this->load->view('procurement/procurement_pdf', $data, true);	
		// output the HTML content
		$pdf->writeHTML($html, true, false, true, false, '');

		//Close and output PDF document
		$pdf->Output(str_replace('/', '-', $data['procurement_data']['procurement_no']).'.pdf', 'I');
	}

	function numberTowords($num, $dec_text)
	{

		$ones = array(
		0 =>"ZERO",
		1 => "ONE",
		2 => "TWO",
		3 => "THREE",
		4 => "FOUR",
		5 => "FIVE",
		6 => "SIX",
		7 => "SEVEN",
		8 => "EIGHT",
		9 => "NINE",
		10 => "TEN",
		11 => "ELEVEN",
		12 => "TWELVE",
		13 => "THIRTEEN",
		14 => "FOURTEEN",
		15 => "FIFTEEN",
		16 => "SIXTEEN",
		17 => "SEVENTEEN",
		18 => "EIGHTEEN",
		19 => "NINETEEN",
		"014" => "FOURTEEN"
		);
		$tens = array( 
		0 => "ZERO",
		1 => "TEN",
		2 => "TWENTY",
		3 => "THIRTY", 
		4 => "FORTY", 
		5 => "FIFTY", 
		6 => "SIXTY", 
		7 => "SEVENTY", 
		8 => "EIGHTY", 
		9 => "NINETY" 
		); 
		$hundreds = array( 
		"HUNDRED", 
		"THOUSAND", 
		"MILLION", 
		"BILLION", 
		"TRILLION", 
		"QUARDRILLION" 
		); /*limit t quadrillion */
		$num = number_format($num,2,".",","); 
		$num_arr = explode(".",$num); 
		$wholenum = $num_arr[0]; 
		$decnum = $num_arr[1]; 
		$whole_arr = array_reverse(explode(",",$wholenum)); 
		krsort($whole_arr,1); 
		$rettxt = ""; 
		foreach($whole_arr as $key => $i){
			
			if(!empty($i)){
				
				while(substr($i,0,1)=="0")
					$i=substr($i,1,5);
					if($i < 20){ 
					/* echo "getting:".$i; */
						if(!empty($ones[$i])) {

							$rettxt .= $ones[$i]; 
						}
					}elseif($i < 100){ 
					if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)]; 
					if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)]; 
					}else{ 
					if(substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
					if(substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)]; 
					if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)]; 
					} 
					if($key > 0){ 
					$rettxt .= " ".$hundreds[$key]." "; 
					}
			}
		} 
		if($decnum > 0){
			$rettxt .= " and ";
			if($decnum < 20){
				if(!empty($ones[$decnum])) {
					$rettxt .= $ones[$decnum];
				}
			}elseif($decnum < 100){
				$rettxt .= $tens[substr($decnum,0,1)];
				$rettxt .= " ".$ones[substr($decnum,1,1)];
			}

			$rettxt .= " ".$dec_text;
		}
		return $rettxt;
	}

	public function ajax_function() {

		if($this->input->is_ajax_request()){
			$call_type = $this->input->post('call_type');
			$response['status'] = 'successful';	
			switch ($call_type) {

				case 'rfq_pending_list':
					
					$filter_type = (!empty($this->input->post('filter_date'))) ? $this->input->post('filter_date'):'current';
					$filter_sales_person = (!empty($this->input->post('filter_sales_person'))) ? $this->input->post('filter_sales_person'):'All';

					// all time filter
					$date = '';
					if( $filter_type == 'month') {
						$date = date('Y-m-d', strtotime('-31 days'));

					} else if( $filter_type == 'week') {
						$date = date('Y-m-d', strtotime('-7 days'));

					} else if( $filter_type == 'current') {
						$date = date('Y-m-d', strtotime('-1 days'));

					}
					$pending_rfq_data = $this->procurement_model->get_pending_rfq_list($date, $filter_sales_person);
					foreach ($pending_rfq_data as $pending_rfq_data_key => $pending_rfq_data_value) {

						$response['highchart_data'][$pending_rfq_data_key]['name'] = $pending_rfq_data_value['name'];
						$response['highchart_data'][$pending_rfq_data_key]['y'] = (int)$pending_rfq_data_value['count'];
						$response['highchart_data'][$pending_rfq_data_key]['drilldown'] = $pending_rfq_data_value['name'];
					}
					if($this->session->userdata('role') == '1'){

						$sales_person = array_column($this->procurement_model->getData('users', '(role = 5  OR role = 16) and status = 1'), 'name', 'user_id');
					} else if ($this->session->userdata('role') == '6') {

						$sales_person = array_column($this->procurement_model->getData('users', 'role = 5 and status = 1'), 'name', 'user_id');
					}
					$response['html_body']['filter_sales_person'] = $this->load->view(
																				'procurement/filter_sales_person',
																				array(
																					'sales_person' => $sales_person,
																					'filter_sales_person' => $filter_sales_person,
																					'sales_filter_used_for_type' => 'bar_graph'
																				),
																				TRUE);
					$response['html_body']['filter_sales_person_id'] = $filter_sales_person;
					$response['html_body']['filter_sales_person_name'] = ($filter_sales_person != 'All') ? $sales_person[$filter_sales_person] : $filter_sales_person;
					$response['html_body']['filter_date_name'] = $filter_type;
					break;

				case 'rfq_done_list':
					
					$filter_type = (!empty($this->input->post('filter_date'))) ? $this->input->post('filter_date'):'day';
					$filter_sales_person = (!empty($this->input->post('filter_sales_person'))) ? $this->input->post('filter_sales_person'):'All';
					$filter_rfq_status = (!empty($this->input->post('filter_rfq_status'))) ? $this->input->post('filter_rfq_status'):'done';
					$response['highchart_data']['category'] = array();
					$response['highchart_data']['series'] = array();
					$start_day = '';
					$end_day = date('Y-m-d 23:59:59');

					if( $filter_type == 'day') {

						$start_day = date('Y-m-d 00:00:00', strtotime('-7 days'));
					}	
					if( $filter_type == 'week') {

						$start_day = date('Y-m-d 00:00:00', strtotime('-77 days'));
					}	
					if( $filter_type == 'month') {

						$start_day = date('Y-m-d 00:00:00', strtotime('-12 months'));
					}	
					$rfq_done_data = $this->procurement_model->get_rfq_done_data($filter_type, $start_day, $end_day, $filter_sales_person, $filter_rfq_status);
					if($this->session->userdata('role') == '1'){

						$procurement_person = array_column($this->procurement_model->get_procurement_user_name(), 'name', 'user_id');
					} else if ($this->session->userdata('role') == '6') {

						$procurement_person = array_column($this->procurement_model->get_procurement_user_name('procurement_user'), 'name', 'user_id');
					}
					$rfq_done_highchart_data = array();
					foreach ($rfq_done_data as $single_data) {
						if(!empty($procurement_person[$single_data['assigned_to']])){

							$rfq_done_highchart_data[$procurement_person[$single_data['assigned_to']]][$single_data[$filter_type]][] = $single_data; 
						}
					}
					$week_increment=2;
					foreach (array_column($rfq_done_data, $filter_type, $filter_type) as $category_value) {
						
						$category = $category_value;
						if( $filter_type == 'week') {

							$week = ($category_value-$week_increment);
							$category = date('d, M', strtotime('-'.$week.' weeks'));
							$week_increment = $week_increment+2;
						}
						$response['highchart_data']['category'][] = $category;
						$response['highchart_data']['series'][0]['data'][] = 0;
					}
					$i=1;
					$response['highchart_data']['series'][0]['name'] = 'Total';
					foreach ($rfq_done_highchart_data as $procurement_person_name => $procurement_person_details) {
						
						$date_name_handling_in_increment = 0;
						$response['highchart_data']['series'][$i]['name'] = $procurement_person_name;
						foreach ($procurement_person_details as $date_name => $single_person_details) {
							
							$response['highchart_data']['series'][0]['data'][$date_name_handling_in_increment] += (int)count($single_person_details);
							$response['highchart_data']['series'][$i]['data'][] = (int)count($single_person_details);
							$date_name_handling_in_increment++;
						}
						$i++;
					}
					$response['highchart_data']['category'] = array_unique($response['highchart_data']['category']);
					$response['html_body']['filter_sales_person'] = $this->load->view(
																				'procurement/filter_procurement_person',
																				array(
																					'sales_person' => $procurement_person,
																					'filter_sales_person' => $filter_sales_person,
																					'sales_filter_used_for_type' => 'line_graph'
																				),
																				TRUE);
					$response['html_body']['filter_sales_person_id'] = $filter_sales_person;
					$response['html_body']['filter_sales_person_name'] = ($filter_sales_person != 'All') ? $procurement_person[$filter_sales_person] : $filter_sales_person;
					$response['html_body']['filter_date_name'] = $filter_type;
					break;
				
				case 'add_charges_in_purchase_order':
					
					$charge_name_array = array('+'=>'Added', '-'=>'Deducted');
					$form_data = array_column($this->input->post('add_charges_in_purchase_order_form'), 'value', 'name');
					$form_data['add_minus_charges_value'] = $charge_name_array[$form_data['add_minus_charges']]; 
					$find_percentage_in_string = strpos($form_data['charge_value'], '%');
					$order_details = $this->session->userdata('order_details');
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";
					// echo "<pre>";print_r($order_details);echo"</pre><hr>";die;
					$gross_total = $order_details['gross_total'];
					if($find_percentage_in_string) {
						$percentage_value = $gross_total*(((int) $form_data['charge_value'] ) / 100 );
						if($form_data['add_minus_charges'] == '+') {

							$form_data['gross_total'] = $gross_total + $percentage_value; 
						} else if ($form_data['add_minus_charges'] == '-'){

							$form_data['gross_total'] = $gross_total - $percentage_value; 
						}
					} else {

						if($form_data['add_minus_charges'] == '+') {

							$form_data['gross_total'] = $gross_total + $form_data['charge_value']; 
						} else if ($form_data['add_minus_charges'] == '-'){

							$form_data['gross_total'] = $gross_total - $form_data['charge_value']; 
						}
					}
					
					if(empty($order_details['different_charges'])){

						$order_details['different_charges'][0] = $form_data;
					}else {

						$order_details['different_charges'][count($order_details['different_charges'])] = $form_data;
					}


					$order_details['gross_total'] = $form_data['gross_total'];
					// echo "<pre>";print_r($order_details);echo"</pre><hr>";exit;
					$this->session->set_userdata('order_details', $order_details);
					// echo "<pre>";print_r($form_data);echo"</pre><hr>";exit;
					break;

				case 'get_gst_or_discount_history':

					$response['charge_history_body'] = $this->load->view('procurement/charges_history', array(), true);
					break;	

				case 'set_order_detail':

					if(!empty($this->input->post('net_total'))) {
						if(empty($this->session->userdata('order_details'))) {

							$net_total = (int)$this->input->post('net_total');
							$gross_total = (int)$this->input->post('net_total');
							$all_net_total[$this->input->post('count_number')] = $net_total;
						} else {

							$all_net_total = $this->session->userdata('order_details')['all_net_total'];
							if(!empty($all_net_total[$this->input->post('count_number')])) {

								$all_net_total[$this->input->post('count_number')] = (int)$this->input->post('net_total'); 
							} else {
								array_push($all_net_total, (int)$this->input->post('net_total'));
							}
							$net_total = array_sum($all_net_total);
						}

						$this->session->set_userdata(
												'order_details',
												array(
													'net_total' => $net_total,
													'gross_total' => $net_total,
													'all_net_total'=> $all_net_total
												)
											);
					}
					$response['net_total_gross_total_body'] = $this->load->view(
																'procurement/net_total_gross_total_body',
																array(),
																true);
				break;	
				
				case 'add_purchase_order':
					
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";
					$response['message'] = "Procurement Data are added";
					$form_data = array();
					$form_data['person_details'] = array_column($this->input->post('person_details'), 'value', 'name');
					$form_data['terms_conditon'] = array_column($this->input->post('terms_conditon'), 'value', 'name');
					$form_data['product_details'] = array_column($this->input->post('product_details'), 'value', 'name');
					// echo "<pre>";print_r($form_data);echo"</pre><hr>";
					// preparing procurement person information
						$insert_array = array_merge($form_data['person_details'], $form_data['terms_conditon']);
						$insert_array['term_condition_text'] = $this->input->post('term_condition_text');
						$last_quote_id = $this->procurement_model->get_all_data('last_quote_id', array('status'=> "Active", 'quote_name'=>'procurement'), 'last_quote_created_number', 'row_array');
						$next_quote_id = ((int)$last_quote_id['last_quote_id'])+1;
						$next_quote_id = (string)$next_quote_id;
						$next_quote_id = '00'.$next_quote_id;
						$insert_array['procurement_no'] = 'OM/'.$next_quote_id.'/21-22';
						// OM/001/21-22
						$insert_array['net_total'] = $this->session->userdata('order_details')['net_total'];
						$insert_array['gross_total'] = $this->session->userdata('order_details')['gross_total'];
						// echo "<pre>";print_r($insert_array);echo"</pre><hr>";
						$procurement_id = $this->procurement_model->insertData('procurement', $insert_array);
						// $procurement_id = 1;
						$last_quote_id = $this->procurement_model->updateData('last_quote_created_number', array('last_quote_id'=>$next_quote_id), array('status'=> "Active", 'quote_name'=>'procurement'));
					// preparing product information 
						for ($i=1; $i <= (count($this->input->post('product_details')) / 8) ; $i++) { 
							
							$product_insert_array[] = array(
														'procurement_id' => $procurement_id,
														'product_name' => $form_data['product_details']['product_name_'.$i], 
														'material_name' => $form_data['product_details']['material_name_'.$i], 
														'description_name' => $form_data['product_details']['description_name_'.$i], 
														'quantity' => $form_data['product_details']['quantity_'.$i], 
														'units' => $form_data['product_details']['units_'.$i], 
														'price' => $form_data['product_details']['price_'.$i], 
														'per_unit' => $form_data['product_details']['per_unit_'.$i], 
														'total' => $form_data['product_details']['total_'.$i], 
													);
						}
						// echo "<pre>";print_r($product_insert_array);echo"</pre><hr>";
						if(!empty($product_insert_array)) {

							$this->procurement_model->insert_data_batch('procurement_product_information', $product_insert_array);
						}
					// preparing charges information
						$charges_insert_array = array();
						if(!empty($this->session->userdata('order_details')['different_charges'])) {

							foreach ($this->session->userdata('order_details')['different_charges'] as $charge_key => $charges_details) {
								
								$charges_insert_array[$charge_key]['procurement_id'] = $procurement_id; 
								$charges_insert_array[$charge_key]['charge_name'] = $charges_details['charges_name']; 
								$charges_insert_array[$charge_key]['charge_value'] = $charges_details['charge_value']; 
								$charges_insert_array[$charge_key]['charge_deducted_or_added'] = $charges_details['add_minus_charges_value']; 
								$charges_insert_array[$charge_key]['gross_total_after_charge'] = $charges_details['gross_total']; 
							}
							// echo "<pre>";print_r($charges_insert_array);echo"</pre><hr>";exit;
							$this->procurement_model->insert_data_batch('procurement_charges_information', $charges_insert_array);
						}

					break;

				case 'update_purchase_order':
					
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					$response['message'] = "Procurement Data are updated";
					$form_data = array();
					$form_data['person_details'] = array_column($this->input->post('person_details'), 'value', 'name');
					$form_data['terms_conditon'] = array_column($this->input->post('terms_conditon'), 'value', 'name');
					$form_data['product_details'] = array_column($this->input->post('product_details'), 'value', 'name');
					$procurement_id = $this->input->post('procurement_id');
					// preparing procurement person information
						$update_array = array_merge($form_data['person_details'], $form_data['terms_conditon']);
						$update_array['term_condition_text'] = $this->input->post('term_condition_text');
						$update_array['net_total'] = $this->session->userdata('order_details')['net_total'];
						$update_array['gross_total'] = $this->session->userdata('order_details')['gross_total'];
						// echo "<pre>";print_r($update_array);echo"</pre><hr>";exit;
						$this->procurement_model->updateData('procurement', $update_array, array('id'=>$procurement_id));
					// preparing product information 
						$this->procurement_model->updateData('procurement_product_information', array('status'=>'Inactive'), array('procurement_id'=>$procurement_id));
						for ($i=1; $i <= (count($this->input->post('product_details')) / 8) ; $i++) { 
							
							$product_insert_array[] = array(
														'procurement_id' => $procurement_id,
														'product_name' => $form_data['product_details']['product_name_'.$i], 
														'material_name' => $form_data['product_details']['material_name_'.$i], 
														'description_name' => $form_data['product_details']['description_name_'.$i], 
														'quantity' => $form_data['product_details']['quantity_'.$i], 
														'units' => $form_data['product_details']['units_'.$i], 
														'price' => $form_data['product_details']['price_'.$i], 
														'per_unit' => $form_data['product_details']['per_unit_'.$i], 
														'total' => $form_data['product_details']['total_'.$i], 
													);
						}
						// echo "<pre>";print_r($product_insert_array);echo"</pre><hr>";
						if(!empty($product_insert_array)) {

							$this->procurement_model->insert_data_batch('procurement_product_information', $product_insert_array);
						}
					// preparing charges information
						$this->procurement_model->updateData('procurement_charges_information', array('status'=>'Inactive'), array('procurement_id'=>$procurement_id));
						$charges_insert_array = array();
						if(!empty($this->session->userdata('order_details')['different_charges'])) {

							foreach ($this->session->userdata('order_details')['different_charges'] as $charge_key => $charges_details) {
								
								$charges_insert_array[$charge_key]['procurement_id'] = $procurement_id; 
								$charges_insert_array[$charge_key]['charge_name'] = $charges_details['charges_name']; 
								$charges_insert_array[$charge_key]['charge_value'] = $charges_details['charge_value']; 
								$charges_insert_array[$charge_key]['charge_deducted_or_added'] = $charges_details['add_minus_charges_value']; 
								$charges_insert_array[$charge_key]['gross_total_after_charge'] = $charges_details['gross_total']; 
							}
							$this->procurement_model->insert_data_batch('procurement_charges_information', $charges_insert_array);
						}
						
					break;		

				case 'set_vendor_name_and_country':
					
					$response['message'] = "record not found";
					if(!empty($this->input->post('vendor_id'))){

						//getting vendor name and country name based on vendor id
						$vendor_details = $this->procurement_model->get_all_data('vendor_name, country', array('status'=> "Active", 'vendor_id'=> $this->input->post('vendor_id')), 'vendors', 'row_array');
						if(!empty($vendor_details)){

							$response['message'] = "record found";
							$response['vendor_details'] = $vendor_details;
							// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
						}
					}

					break;

				case 'get_product_body':
					
					if(!empty($this->input->post('next_count_number'))){

						$data['next_count_number'] = $this->input->post('next_count_number');
						$data['material_name'] = $this->input->post('material_name');
						$data['product_name'] = $this->input->post('product_name');
						$data['product_list'] = $this->procurement_model->get_lookup_data(259);
						$data['material_list'] = $this->procurement_model->get_lookup_data(272);
						$data['units_list'] = $this->procurement_model->get_units_data();
						$response['product_list_body']	= $this->load->view('procurement/new_product_body', $data, true);
					}
					break;

				case 'delete_procurement_list_data':
					
					if(!empty($this->input->post('procurement_id'))){
						$this->procurement_model->updateData('procurement', array('status'=>'Inactive'), array('id'=>$this->input->post('procurement_id')));
					}
					break;	

				case 'search_filter':
					
					$response_html = $this->all_filter_data($this->input->post('search_form_data'), array('column_name'=>'id', 'column_value'=>'asc'), 10, 0);
					$response['procurement_body'] = $response_html['procurement_body']; 
					$response['procurement_search_filter'] = $response_html['procurement_search_filter'];
					$response['procurement_sorting'] = $response_html['procurement_sorting'];
					$response['procurement_paggination'] = $response_html['procurement_paggination'];

					break;

				case 'sort_filter':

					$response_html = $this->all_filter_data(
											$this->input->post('search_form_data'), 
											array(
												'column_name'=> $this->input->post('procurement_sort_name'), 
												'column_value'=>$this->input->post('procurement_sort_value')
											), 10, 0
										);
					$response['procurement_body'] = $response_html['procurement_body']; 
					$response['procurement_search_filter'] = $response_html['procurement_search_filter'];
					$response['procurement_sorting'] = $response_html['procurement_sorting'];
					$response['procurement_paggination'] = $response_html['procurement_paggination'];
					break;

				case 'paggination_filter':

					$response_html = $this->all_filter_data($this->input->post('search_form_data'), array('column_name'=>'id', 'column_value'=>'asc'), $this->input->post('limit'), $this->input->post('offset'));
					$response['procurement_body'] = $response_html['procurement_body']; 
					$response['procurement_search_filter'] = $response_html['procurement_search_filter'];
					$response['procurement_sorting'] = $response_html['procurement_sorting'];
					$response['procurement_paggination'] = $response_html['procurement_paggination'];
					break;		

				case 'delete_product_details':
					
					$order_details = $this->session->userdata('order_details');
					$get_net_total = $order_details['all_net_total'][$this->input->post('count_no')];
					unset($order_details['all_net_total'][$this->input->post('count_no')]);
					$order_details['net_total'] = $order_details['net_total']-$get_net_total;
					$order_details['gross_total'] = $order_details['gross_total']-$get_net_total;
					foreach ($order_details['different_charges'] as $charge_key => $charges_details) {
						
						$order_details['different_charges'][$charge_key]['gross_total'] = $order_details['different_charges'][$charge_key]['gross_total']-$get_net_total;
					}
					// echo "<pre>";print_r($order_details);echo"</pre><hr>";exit;
					$this->session->userdata('order_details', $order_details);
					break;	

				case 'delete_charge':

					$charge_key = $this->input->post('charge_id');
					$session_data = $this->session->userdata('order_details');
					if(array_key_exists($charge_key, $session_data['different_charges'])) {

 						$charge_data_of_delete = $session_data['different_charges'][$charge_key];
 						$gross_total = 0;
						$current_charge_value = 0;
						$gross_total_changed_flag = false;
 						foreach ($session_data['different_charges'] as $different_charges_key => $different_charges_value) {
 							
 							if($different_charges_key == $charge_key) {
								if($charge_data_of_delete['add_minus_charges_value'] == 'Added') {

									$current_charge_value = ($charge_data_of_delete['gross_total']- ($gross_total + $session_data['net_total']));
									$session_data['gross_total'] = $session_data['gross_total'] - $current_charge_value;
								}else if($charge_data_of_delete['add_minus_charges_value'] == 'Deducted') {
									
									$current_charge_value = (($gross_total + $session_data['net_total']) - $charge_data_of_delete['gross_total']);
									$session_data['gross_total'] = $session_data['gross_total'] + $current_charge_value;
								}
 							}else{
 								if($current_charge_value == 0) {

									if($different_charges_value['add_minus_charges_value'] == 'Added') {

		 								$gross_total += ($different_charges_value['gross_total']- ($gross_total + $session_data['net_total']));
									}else if($different_charges_value['add_minus_charges_value'] == 'Deducted') {

										// echo $gross_total,"<hr>";
		 								$gross_total -= (($gross_total + $session_data['net_total']) -$different_charges_value['gross_total']);
										// echo $gross_total;die;
									}
 								} else {

 									if($charge_data_of_delete['add_minus_charges_value'] == 'Added') {

	 									$session_data['different_charges'][$different_charges_key]['gross_total'] -= $current_charge_value;		
									}else if($charge_data_of_delete['add_minus_charges_value'] == 'Deducted') {

	 									$session_data['different_charges'][$different_charges_key]['gross_total'] += $current_charge_value;	
									}
 								}
 							}
 						}
						unset($session_data['different_charges'][$charge_key]);
						$this->session->set_userdata('order_details',$session_data);
					}
	 			break;
	 			case 'update_rating':

 					$form_data = array_column($this->input->post('rating_value'), 'value', 'name');
					foreach ($form_data as $form_key => $form_value) {
						
						$explode = explode('_', $form_key);
						// echo "<pre>";print_r($explode);echo"</pre><hr>";exit;
						if(count($explode) == 4) {
							$this->procurement_model->updateData('rfq_mst', array('priority' => (int)$form_value), array('rfq_mst_id'=>$explode[3]));
						} else{
							$response['status'] = 'failed';	
						}
					}
				break;	

		 		default:
		 			$response['status'] = 'failed';	
		 			$response['message'] = "call type not found";
	 			break;
		 	}
		 	echo json_encode($response);

	       } else{
		 	die('access is not allowed to this function');
		 }

	}

	private function all_filter_data($search_form_data, $order_by_array, $limit, $offset) {

		$where_array = $return_response = array();
		$form_data = array_column($search_form_data, 'value', 'name');
		$where_array['status'] = 'Active';
		foreach ($form_data as $key => $value) {
			if(!empty($value)) {

				$where_array[$key] = $value;
			}
		}
		$data = $this->create_procurement_data($where_array, $order_by_array, $limit, $offset);
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$return_response['procurement_body'] = $this->load->view('procurement/procurement_body', $data, true);
		$return_response['procurement_search_filter'] = $this->load->view('procurement/procurement_search_filter_form', $data, true);
		$return_response['procurement_sorting'] = $this->load->view('procurement/procurement_head', $data, true);
		$return_response['procurement_paggination'] = $this->load->view('procurement/procurement_paggination', $data, true);
		return $return_response;
	}

	private function create_procurement_data($where_array, $order_by, $limit, $offset) {

		$data = $this->procurement_model->get_procurement_listing_data($where_array, $order_by, $limit, $offset);
		$data['country_list'] = array_column($this->procurement_model->get_all_data('country_id, country_name', array('status'=> "Active"), 'country'), 'country_name', 'country_id');
		$data['sales_person'] = $this->procurement_model->get_sales_person('name, user_id');
		$data['procurement_person'] = $this->procurement_model->get_procurement_person();
		$data['vendor_list'] = $this->procurement_model->get_vendor_list();
		$data['search_filter'] = $this->set_search_filter_value($where_array);
		$data['sorting'] = $this->set_sorting_value($order_by);
		$data['paggination_data']['limit'] = $limit;
		$data['paggination_data']['offset'] = $offset;
		return $data;
	} 

	private function set_search_filter_value($where_array) {

		$return_array['procuremet_person_name'] = (!empty($where_array['procurement_person'])) ? $where_array['procurement_person']: ''; 
		$return_array['sales_person_name'] = (!empty($where_array['sales_person'])) ? $where_array['sales_person']: ''; 
		$return_array['vendor_name'] = (!empty($where_array['vendor_name'])) ? $where_array['vendor_name']: ''; 
		$return_array['procurement_status'] = (!empty($where_array['procurement_status'])) ? $where_array['procurement_status']: ''; 
		return $return_array;
	}

	private function set_sorting_value($sort_array) {

		$sorting_static_array = array('id', 'procurement_no', 'procurement_person', 'sales_person', 'add_time', 'vendor_name', 'net_total');
		$sorting = array();
		foreach ($sorting_static_array as $sorting_static_value) {
			
			$sorting[strtolower($sorting_static_value)]['class_name'] = 'sorting';
			$sorting[strtolower($sorting_static_value)]['value'] = 'asc';
			if($sort_array['column_name'] == $sorting_static_value) {
				if($sort_array['column_value'] == 'asc') {

					$sorting[strtolower($sorting_static_value)]['class_name'] = 'sorting_asc';
					$sorting[strtolower($sorting_static_value)]['value'] = 'desc';
				} else {
					$sorting[strtolower($sorting_static_value)]['class_name'] = 'sorting_desc';
				}			
			}
		}
		return $sorting;
	}	
}