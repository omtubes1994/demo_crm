<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Procurement extends MX_Controller {
	public function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('user_id')){
			redirect('login', 'refresh');
			exit;
		}else{
			// $access_count = 0;
			// if(!empty($this->session->userdata('main_module_access')) && in_array(9, array_column($this->session->userdata('main_module_access'), 'module_id'))) {

			// 	foreach ($this->session->userdata('sub_module_access') as $key => $value) {
					
			// 		if($value['module_id'] == 9 && $value['status'] == 'Active') {
			// 			$access_count++;
			// 		}
			// 	}
			// }
			// if($access_count == 0){
			// 	redirect($this->session->userdata('home/dashboard'));
			// 	exit;
			// }
		}
		error_reporting(0);
		$this->load->model('procurement_model');
		$this->load->model('common/common_model');
	}

	public function index(){
		$this->add();
	}

	public function procurement_form(){
		$this->load->view('header', array('title' => 'Procurement Form'));
		$this->load->view('sidebar', array('title' => 'Procurement Form'));
		$this->load->view('procurement_form');
		$this->load->view('footer');
	}

	public function procurement_list(){
		$this->load->view('header', array('title' => 'Procurement List'));
		$this->load->view('sidebar', array('title' => 'Procurement List'));
		$this->load->view('procurement_list');
		$this->load->view('footer');
	}

	public function addRFQ($rfq_id=0){
		
		$sidebar_title = 'Add RFQ';
		$header_title = 'Add RFQ';
		if(!empty($this->input->post())){

			// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
			$insert_arr = array(
				'rfq_sentby' => $this->input->post('rfq_sentby'),
				'rfq_company' => $this->input->post('rfq_company'),
				'rfq_buyer' => $this->input->post('rfq_buyer'),
				// 'client_source' => $this->input->post('client_source'),
				'rfq_rank' => $this->input->post('rfq_rank'),
				'rfq_lastbuy' => $this->input->post('rfq_lastbuy'),
				'rfq_subject' => $this->input->post('rfq_subject'),
				'rfq_importance' => $this->input->post('rfq_importance'),
				'oem_number' => $this->input->post('oem_number'),
				'assigned_to' => $this->input->post('assigned_to_1'),
				'assigned_to_1' => $this->input->post('assigned_to_1'),
				'status_1' => $this->input->post('status_1'),
				'assigned_to_2' => $this->input->post('assigned_to_2'),
				'status_2' => $this->input->post('status_2'),
				'assigned_to_3' => $this->input->post('assigned_to_3'),
				'status_3' => $this->input->post('status_3'),
				'reference' => $this->input->post('reference'),
				'rfq_status' => $this->input->post('rfq_status'),
				'product_family' => $this->input->post('product_family'),
				'priority' => $this->input->post('priority'),
				'priority_reason' => $this->input->post('priority_reason'),
				'type' => $this->input->post('type'),
				'client_type' => $this->input->post('client_type'),
				'rfq_type' => $this->input->post('rfq_type'),
				'modified_on' => date('Y-m-d H:i:s')
			);
			$insert_arr['rfq_regret_reason'] = 0;
			if($insert_arr['rfq_status'] == 'regret'){

				$insert_arr['rfq_regret_reason'] = $this->input->post('rfq_regret_reason');
			}

			if($this->input->post('rfq_closedate') != ''){
				$insert_arr['rfq_closedate'] = $this->input->post('rfq_closedate');
			}else{
				$insert_arr['rfq_closedate'] = null;
			}

			if($this->input->post('assigned_to_date') != ''){
				$insert_arr['assigned_to_date'] = date('Y-m-d H:i:s', strtotime($this->input->post('assigned_to_date')));
			}

			if($this->input->post('rfq_mst_id') > 0){
				$rfq_mst_id = $this->input->post('rfq_mst_id');
				$this->procurement_model->updateData('rfq_mst', $insert_arr, array('rfq_mst_id' => $rfq_mst_id));
				$this->procurement_model->deleteData('rfq_dtl', array('rfq_mst_id' => $rfq_mst_id));
			}
			else{
				$insert_arr['rfq_date'] = date('Y-m-d');
				$insert_arr['entered_on'] = date('Y-m-d H:i:s');
				$rfq_type = 'om';
				$rfq_year = date('y').'-'.(date('y') + 1);
				if(date('m') <= 3){

					$rfq_year = (date('y')-1)."-".date('y');
				}
				if(!empty($this->input->post('type')) && $this->input->post('type') != 'om'){

					$rfq_type = $this->input->post('type');
				}
				$last_query_id = $this->common_model->get_dynamic_data_sales_db('last_quote_id', array('status'=> "Active", 'module_name'=> 'procurement', 'sub_module_name'=> 'rfq', 'sub_module_type'=> 'om'), 'last_quote_created_number', 'row_array');
				$rfq_no = (((int)$last_query_id['last_quote_id'])+1);

				$this->common_model->update_data_sales_db('last_quote_created_number', array('last_quote_id'=>(((int)$last_query_id['last_quote_id'])+1)), array('status'=> "Active", 'module_name'=> 'procurement', 'sub_module_name'=> 'rfq', 'sub_module_type'=> 'om'));

				$insert_arr['rfq_no'] = strtoupper($rfq_type).'/'.$rfq_no.'/'.$rfq_year;
				// echo "<pre>";print_r($insert_arr);echo"</pre><hr>";exit;
				// $insert_arr['rfq_no'] = 'OM/'.$this->procurement_model->getRFQNo().'/20-21';
				$rfq_mst_id = $this->procurement_model->insertData('rfq_mst', $insert_arr);
			}
			if(!empty($this->input->post('product_id'))) {
				foreach ($this->input->post('product_id') as $key => $value) {
					$dtl_array  = array(
						'rfq_mst_id' => $rfq_mst_id,
						'product_id' => $this->input->post('product_id')[$key],
						'material_id' => $this->input->post('material_id')[$key],
						'description' => $this->input->post('description')[$key],
						'unit' => $this->input->post('unit')[$key],
						'quantity' => $this->input->post('quantity')[$key],
					);
					$this->procurement_model->insertData('rfq_dtl', $dtl_array);
				}
			}
			//echo $this->db->last_query();
			// redirect('procurement/addRFQ/'.$rfq_mst_id, 'refresh');
			if(!empty($rfq_id)) {

				redirect('procurement/addRFQ/'.$rfq_id, 'refresh');
			}else {

				redirect('procurement/addRFQ/', 'refresh');
			}
		}else{
			$quotation_product_details = array();
			$data['prd_str'] = $data['mat_str'] = $data['unit_str'] = $data['vendor_str'] = '';
			if($rfq_id > 0){
				$data['rfq_id'] = $rfq_id;
				$data['rfq_details'] = $this->procurement_model->getRfq($rfq_id);
				if(!empty($data['rfq_details'])){

					$sidebar_title = $data['rfq_details'][0]['rfq_no'];
					$header_title = $data['rfq_details'][0]['rfq_no'];
				}
				// $data['rfq_details'][0]['rfq_company_name'] = $this->procurement_model->getLeadName($data['rfq_details'][0]['rfq_company'], $data['rfq_details'][0]['client_source']);
				$data['rfq_details'][0]['rfq_company_name'] = $this->procurement_model->getLeadName($data['rfq_details'][0]['rfq_company']);
				// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
				$data['vendors'] = $vendors = $this->procurement_model->getData('vendors');
				foreach($vendors as $ven){ 
					$data['vendor_str'] .= '<option value="'.$ven['vendor_id'].'">'.ucwords(strtolower($ven['vendor_name'])).'</option>';
				}
				// $data['rfq_to_vendor'] = $this->procurement_model->getData('rfq_to_vendor', 'rfq_id = '.$rfq_id);
				$data['rfq_notes'] = $this->procurement_model->getData('rfq_note_query', "type = 'notes' and rfq_id = ".$rfq_id);
				$data['rfq_query'] = $this->procurement_model->getData('rfq_note_query', "type = 'query' and rfq_id = ".$rfq_id);
				$quotation_details = $this->common_model->get_dynamic_data_sales_db('*', array('rfq_id'=> $rfq_id), 'quotation_mst', 'row_array');
				// echo "<pre>";print_r($quotation_details);echo"</pre><hr>";exit;
				if(!empty($quotation_details)){
					
					$quotation_product_details = $this->common_model->get_dynamic_data_sales_db('*', array('quotation_mst_id'=> $quotation_details['quotation_mst_id']), 'quotation_dtl');
				}
			}
			// $data['clients'] = $this->procurement_model->getClients();
			$data['clients'] = $this->procurement_model->get_client_name_on_id($data['rfq_details'][0]['rfq_company']);
			$data['members'] = $this->common_model->get_dynamic_data_sales_db('comp_dtl_id, member_name', array('status'=>'Active', 'comp_mst_id'=> $data['rfq_details'][0]['rfq_company']), 'customer_dtl');
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
			$data['sales_person'] = $this->procurement_model->getData('users', '(role = 5  OR role = 16) and status = 1');
			$data['purchase_person'] = $this->procurement_model->getData('users', '(role = 6 or role = 8) and status = 1');
			$data['close_reason'] = $this->procurement_model->getData('rfq_close_reason', 'status = "Active"');
			// $data['product'] = $product = $this->procurement_model->getData('lookup', 'status="Active" AND lookup_group = 259');
			$data['product'] = $product = $this->procurement_model->getData('product_mst', 'status="Active"');
			foreach($product as $prod){ 
				$data['prd_str'] .= '<option value="'.$prod['id'].'">'.ucwords(strtolower($prod['name'])).'</option>';
			}

			// $data['material'] = $material = $this->procurement_model->getData('lookup', 'status="Active" AND lookup_group = 272');
			$data['material'] = $material = $this->procurement_model->getData('material_mst', 'status="Active"');
			foreach($material as $mat){ 
				$data['mat_str'] .= '<option value="'.$mat['id'].'">'.ucwords(strtolower($mat['name'])).'</option>';
			}

			$data['units'] = $units = $this->procurement_model->getData('units', 'status="Active"');
			foreach($units as $un){
				$data['unit_str'] .= '<option value="'.$un['unit_id'].'">'.ucwords(strtolower($un['unit_value'])).'</option>';
			}

			// $data['country'] = $product = $this->procurement_model->getData('lookup', 'lookup_group = 2');
			// $data['region'] = $product = $this->procurement_model->getData('lookup', 'lookup_group = 1');
			$data['country'] = $product = $this->procurement_model->getData('country_mst', 'status = "Active"');
			$data['region'] = $product = $this->procurement_model->getData('region_mst', 'status = "Active"');
			
			// $data['lookup_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array(), 'lookup'), 'lookup_value','lookup_id');
			$data['product_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array(), 'product_mst'), 'name','id');

			$data['material_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array(), 'material_mst'), 'name','id');
			
			$data['units_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'), 'units'), 'unit_value','unit_id');
			$data['vendor_list'] = array();
			if(!empty($quotation_product_details)){

				foreach ($quotation_product_details as $quotation_product_key => $quotation_product_value) {
					
					$data['quotation_details'][$quotation_product_key]['product_name'] = $data['product_details'][$quotation_product_value['product_id']]; 
					$data['quotation_details'][$quotation_product_key]['material_name'] = $data['material_details'][$quotation_product_value['material_id']];
					$data['quotation_details'][$quotation_product_key]['description'] = $quotation_product_value['description'];
					$data['quotation_details'][$quotation_product_key]['quantity'] = $quotation_product_value['quantity'];
					$data['quotation_details'][$quotation_product_key]['id'] = $quotation_product_value['quotation_dtl_id'];
					$data['quotation_details'][$quotation_product_key]['vendor_id'] = $quotation_product_value['vendor_id'];
					$data['quotation_details'][$quotation_product_key]['units'] = $data['units_details'][$quotation_product_value['unit']];
					$product_name_list[] = $data['product_details'][$quotation_product_value['product_id']];
					$material_name_list[] = $data['material_details'][$quotation_product_value['material_id']];

					$product_id_list[] = $quotation_product_value['product_id'];
					$material_id_list[] = $quotation_product_value['material_id'];
				}
				$vendor_list_name_wise = $this->procurement_model->get_vendor_name_on_product_name_and_material_name(array_unique($product_name_list), array_unique($material_name_list));
				// echo "<pre>";print_r($vendor_list_name_wise);echo"</pre><hr>";
				$vendor_list_id_wise = $this->procurement_model->get_vendor_name_on_product_id_and_material_id(array_unique($product_id_list), array_unique($material_id_list));
				// echo "<pre>";print_r($vendor_list_id_wise);echo"</pre><hr>";
				$vendor_list_name_wise_count = count($vendor_list_name_wise);
				$vendor_list_id_wise_count = count($vendor_list_id_wise);
				if(($vendor_list_name_wise_count + $vendor_list_id_wise_count) > 0){

					foreach ($vendor_list_name_wise as $value) {
						
						$data['vendor_list'][$value['vendor_id']]['vendor_id'] = $value['vendor_id']; 
						$data['vendor_list'][$value['vendor_id']]['vendor_name'] = $value['vendor_name'];
						$data['vendor_list'][$value['vendor_id']]['vendor_sub_name'] = "PO Vendor";
					}

					foreach ($vendor_list_id_wise as $value) {
						
						if(!in_array($value['vendor_id'], array_keys($data['vendor_list']))){

							$data['vendor_list'][$value['vendor_id']]['vendor_id'] = $value['vendor_id']; 
							$data['vendor_list'][$value['vendor_id']]['vendor_name'] = $value['vendor_name'];
							$data['vendor_list'][$value['vendor_id']]['vendor_sub_name'] = "Main Vendor";
						} else {

							$data['vendor_list'][$value['vendor_id']]['vendor_sub_name'] = "PO Vendor & Main Vendor";
						}
					}
				}
				// echo "<pre>";print_r(count($data['vendor_list']));echo"</pre><hr>";
				// echo "<pre>";print_r($data['vendor_list']);echo"</pre><hr>";exit;
				// $data['vendor_list'] = array_unique(array_merge( $vendor_list_name_wise, $vendor_list_id_wise));
			}
			// echo "<pre>";print_r($data['quotation_details']);echo"</pre><hr>";exit;
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
			$this->load->view('header', array('title' => $header_title));
			$this->load->view('sidebar', array('title' => $sidebar_title));
			$this->load->view('addrfq_form', $data);
			$this->load->view('footer');
		}
	}

	function getMembers(){
		$members = $this->procurement_model->getMembers($this->input->post());
		echo json_encode($members);
	}

	public function rfq_list_new(){
		
		$data = array();
		$this->session->set_userdata(array('rfq_client_type'=> 'om'));
		$this->session->set_userdata(array('rfq_graph_type'=> 'procurement'));

		$this->load->view('header', array('title' => 'RFQ List'));
		$this->load->view('sidebar', array('title' => 'RFQ List'));
		$this->load->view('procurement/rfq_index', $data);
		$this->load->view('footer');
	}

	function rfq_list(){
		$data['sales_person'] = $data['purchase_person'] = '';
		$sales_person = $this->procurement_model->getData('users', '(role = 5  OR role = 16) and status = 1');
		foreach($sales_person as $sp){
			$data['sales_person'] .= '<option value="'.$sp['user_id'].'">'.ucwords(strtolower($sp['name'])).'</option>';
		}
		
		$purchase_person = $this->procurement_model->getData('users', '(role = 6 or role = 8) and status = 1');
		foreach($purchase_person as $pp){
			$data['purchase_person'] .= '<option value="'.$pp['user_id'].'">'.ucwords(strtolower($pp['name'])).'</option>';
		}
		//echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'RFQ List'));
		$this->load->view('sidebar', array('title' => 'RFQ List'));
		$this->load->view('rfq_list', $data);
		$this->load->view('footer');
	}

	function rfq_list_data(){
		// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
		$search = array();
		foreach ($this->input->post('columns') as $key => $value) {
			$search_key = '';
			if($this->session->userdata('role') == 5){
				if($key == 1){
					$search_key = 'rfq_no';
				}else if($key == 2){
					$search_key = 'rfq_company';
				}else if($key == 7){
					$search_key = 'r.assigned_to';
				}else if($key == 8){
					$search_key = 'rfq_status';
				}else if($key == 5){
					$search_key = 'rfq_date';
				}else if($key == 6){
					$search_key = 'rfq_importance';
				}else if($key == 9){
					$search_key = 'quote_status';
				}
			}else{
				if($key == 1){
					$search_key = 'rfq_no';
				}else if($key == 2){
					$search_key = 'rfq_company';
				}else if($key == 8){
					$search_key = 'r.assigned_to';
				}else if($key == 9){
					$search_key = 'rfq_status';
				}else if($key == 7){
					$search_key = 'rfq_sentby';
				}else if($key == 5){
					$search_key = 'rfq_date';
				}else if($key == 6){
					$search_key = 'rfq_importance';
				}else if($key == 10){
					$search_key = 'quote_status';
				}
			}

			if($search_key == 'rfq_date'){
				if($this->input->post('columns')[$key]['search']['value'] != ''){
					$search[$search_key] = date('Y-m-d', strtotime($this->input->post('columns')[$key]['search']['value']));	
				}else{
					$search[$search_key] = '';
				}
			}else{
				$search[$search_key] = $this->input->post('columns')[$key]['search']['value'];
			}
		}
		if(!empty($this->input->post('rfq_priority'))) {

			$search['priority'] = $this->input->post('rfq_priority');
		}
		if(!empty($this->input->post('rfq_close_date'))) {

			$search['rfq_closedate'] = $this->input->post('rfq_close_date');
		}
		//print_r($this->input->post('length'));
		$order_by = $this->input->post('columns')[$this->input->post('order')[0]['column']]['data'];
		$dir = $this->input->post('order')[0]['dir'];
		if($order_by == 'record_id'){
			$order_by = 'rfq_no';
			$dir = 'desc';
		}else if($order_by == 'company_name'){
			$order_by = 'rfq_company';
		}
		$records = $this->procurement_model->getRFQListData($this->input->post('start'), $this->input->post('length'), $search, $order_by, $dir);
		$data['iTotalDisplayRecords'] = $data['iTotalRecords'] = $this->procurement_model->getRFQListCount($search);
		$data['aaData'] = $records;
		//echo "<pre>";print_r($data);exit();
		echo json_encode($data);
	}

	function assignVendor(){
		$insert_arr = array(
			'rfq_id' => $this->input->post('rfq_id'),
			'vendor_id' => $this->input->post('vendor_id'),
			'vendor_status' => $this->input->post('vendor_status'),
			'evaluate_price' => $this->input->post('evaluate_price'),
			'evaluate_delivery' => $this->input->post('evaluate_delivery'),
			'modified_on' => date('Y-m-d H:i:s'),
			'modified_by' => $this->session->userdata('user_id')
		);

		if($this->input->post('connect_id') > 0){
			$this->procurement_model->updateData('rfq_to_vendor', $insert_arr);	
		}else{
			$insert_arr['entered_on'] = date('Y-m-d H:i:s');
			$insert_arr['entered_by'] = $this->session->userdata('user_id');
			$this->procurement_model->insertData('rfq_to_vendor', $insert_arr);	
		}

	}

	function pdf_new(){

		$this->load->library('Pdf');
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		

		// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, NULL, '', array(0,0,0), array(255,255,255));
		$pdf->SetFooterData(array(0,0,0), array(255,255,255));

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(10, PDF_MARGIN_TOP-10, 10);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		    require_once(dirname(__FILE__).'/lang/eng.php');
		    $pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('helvetica', '', 10);

		// add a page
		$pdf->AddPage();

		

		// define some HTML content with style
		//new code start

		$data['quotation_details'] = array();
		$rfq_id = $this->uri->segment(3, 0);
		$vendor_id = $this->uri->segment(4, 0);
		if(!empty($rfq_id) && !empty($vendor_id)) {

			$rfq_to_vendor = $this->common_model->get_dynamic_data_sales_db('*', array('rfq_id'=> $rfq_id, 'vendor_id'=> $vendor_id), 'rfq_to_vendor');
			// echo $this->common_model->db->last_query(),"<hr>";
			// echo "<pre>";print_r($rfq_to_vendor);echo"</pre><hr>";exit;
			if(!empty($rfq_to_vendor)){

				foreach ($rfq_to_vendor as $rfq_to_vendor_values) {
					
					$rfq_to_vendor_sub = array();
					if(!empty($rfq_to_vendor_values['quotation_mst_id'])){

						$rfq_to_vendor_sub = $this->common_model->get_dynamic_data_sales_db('*', array('quotation_dtl_id'=> $rfq_to_vendor_values['quotation_mst_id']), 'quotation_dtl','row_array');
					}
					$data['quotation_details'][] = $rfq_to_vendor_sub;
				}
			}
		}
		$data['rfq_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('rfq_mst_id'=> $rfq_id), 'rfq_mst','row_array');

		$data['vendor_details'] = $this->common_model->get_dynamic_data_sales_db('vendor_id, vendor_name, country', array('vendor_id'=>$vendor_id), 'vendors','row_array');
		$country_details = $this->common_model->get_dynamic_data_sales_db('*', array('id'=> $data['vendor_details']['country']), 'country_mst', 'row_array');
		$user_details = $this->common_model->get_dynamic_data_sales_db('*', array('user_id'=> $data['rfq_details']['assigned_to']), 'users', 'row_array');
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		//new code end
		// $data['rfq_details'] = $this->procurement_model->getRFQDetails($connect_id);
		// $data['vendor_details'] = $this->procurement_model->getVendorDetails($connect_id);
		$vendor_sales_person = "Sir/Madam,";
		if(!empty($data['vendor_details']) && $data['vendor_details']['vendor_name']) {

			$vendor_sales_person = $data['vendor_details']['vendor_name'].',';
		}
		$units_details = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'), 'units'), 'unit_value','unit_id');
		$table_str = '';

		$i=0;
    	// echo "<pre>";print_r($data['quotation_details']);echo"</pre><hr>";exit;
        foreach ($data['quotation_details'] as $key => $value) { 
			$bg_color = '';
			if($i%2 != 0){
				$bg_color = '#e4e1e1;';
			}

			$table_str .= '<tr style="background-color: '.$bg_color.'; font-size: 11px; font-family: courier;">
				<td style="text-align: right;">'.++$i.'</td>
				<td>'.$value['description'].'</td>
				<td style="text-align: right;"> '.$value['quantity'].'</td>
				<td>'.$units_details[$value['unit']].'.</td>
			</tr>';
        }

        $date = date('d-m-Y', strtotime($data['rfq_details']['rfq_date']));
		$client_name = trim($data['vendor_details']['vendor_name']);

		$html = '<!-- EXAMPLE OF CSS STYLE -->
		<table cellpadding="5" cellspacing="0">
			<tr style="background-color: #fff;">
				<td width="50%" style="padding:5px;vertical-align: text-top; ">
					<img src="/assets/media/client-logos/logo.png" width="180" height="50" style="padding-left: 10px;"><br/>
					<strong style="font-size: 17px;">OM TUBES & FITTINGS INDUSTRIES</strong>
					<div class="left_addrs" style="font-size: 14px;color: #484545; line-height: 20px;">
						10 Bordi Bunglow, 1st Panjarapole Lane, CP Tank, Mumbai, Maharashtra, India <br/>
						GSTIN 27AFRPM5323E1ZC
					</div>
					<table style="margin-top: -15px;">
						<tr style="background-color: #fff;">
							<td style="font-size: 14px; color: #484545; line-height: 20px;" width="50%">+91 (22) 6743 7634</td>
						</tr>
						<tr style="background-color: #fff;">
							<td style="font-size: 14px; color: #484545; line-height: 20px;" align="left">www.omtubes.com</td>
						</tr>
					</table>
				</td>
			<td width="50%">
				<strong style="font-size: 25px; text-align: right; line-height: 35px;">Request For Quotation</strong><br/>
				<table style="line-height: 22px;">
					<tr style="background-color: #e4e1e1; border-bottom: 1px solid;">
						<td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;"> RFQ # : </strong></td>
						<td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;">Date : </strong></td>
					</tr>
					<tr>
						<td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;">'.$data['rfq_details']['rfq_no'].'</td>
						<td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;">'.$date.'</td>
					</tr>
					<tr style="background-color: #e4e1e1;">
						<td colspan="2" style="padding:5px;vertical-align: text-top;"><strong style="font-size: 12px;"> Seller:</strong></td>
					</tr>
					<tr>
						<td colspan="2" style="padding:5px;vertical-align: text-top; font-family: courier;">
							<strong>'.$client_name.'</strong>
							<div class="left_addrs" style="font-size: 11px;color: #484545; line-height: 17px; font-family: courier;">'.$country_details['lookup_value'].'<br/>'.$data['vendor_details']['vendor_name'].'
							</div>
						</td>
					</tr>
					<tr style="background-color: #e4e1e1;">
						<td colspan="2" style="padding:5px;vertical-align: text-top;"><strong style="font-size: 12px;">Our reference :</strong></td>
					</tr>
					<tr>
						<td colspan="2" style="font-family: courier; font-size: 11px;color: #484545;">'.$data['rfq_details']['reference'].'</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr style="background-color: #fff;">
			<td colspan="2" style="padding:5px;vertical-align: text-top; font-family: courier;"><strong style="font-size: 16px;">URGENT</strong><br/><br/>
			Dear '.$vendor_sales_person.'<br/><br/>
			please submit your quotation according to the following specification<br/>
			- price terms: as specified, including standard packing<br/>
			- validity of quotation: 90 days minimum<br/>
			- delivery time<br/>
			- resales discount<br/>
			- estimated total weight/ -volume<br/><br/>
			Thank you very much.
			</td>
					</tr>
					<tr>
						<td colspan="2"></td>
					</tr>
					<tr>
						<td colspan="2">
							<table cellspacing="0" cellpadding="10" border="0">
								<thead>
									<tr style="background-color: #e4e1e1;">
										<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="7%;">#</td>
										<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="63%">Item Description</td>
										<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="15%">Quantity</td>
										<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="15%">Unit</td>
									</tr>
								</thead>
								<tbody>'.$table_str.'</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td>
						</td>
						<td></td>
					</tr>
					<tr>
						<td style="font-size: 11px; font-family: courier;">
							<b style="font-size: 16px;">Additional Notes</b><br/>
							Please provide reason/explanation for possible deviation/discrepancy to our enquiry-specification and support<br/><br/>OM Tubes & Fittings Industries has conducted its business for decades with integrity. 
			Therefore, we also expect our suppliers to ensure our values. 
			For additional details, please contact us.
						</td>
						<td align="center">
							Thank you for your business<br/>
							For Om Tubes & Fittings Industries<br/><br/>
							<img src="/assets/media/stamp.png"/><br/><br/>
							<table>
								<tr>
									<td width="26%" rowspan="3"></td>
								 	<td align="left" width="74%" >Name : <span style="font-family: courier; ">'.$user_details['name'].'</span></td>
								</tr>
								<tr>
									<td align="left">Email : <span style="font-family: courier;">'.$user_details['email'].'</span></td>
								</tr>
								<tr>
									<td align="left">Mobile : <span style="font-family: courier;">'.$user_details['mobile'].'</span></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>';

		// output the HTML content
		$pdf->writeHTML($html, true, false, true, false, '');

		//Close and output PDF document
		$pdf->Output(str_replace('/', '-', $data['rfq_details']['rfq_no']).'.pdf', 'I');
	
	}

	function viewPdf($connect_id){
		$data['rfq_details'] = $this->procurement_model->getRFQDetails($connect_id);
		$data['vendor_details'] = $this->procurement_model->getVendorDetails($connect_id);
		$vendor_sales_person = "Sir/Madam,";
		if(!empty($data['vendor_details']) && $data['vendor_details']['name']) {

			$vendor_sales_person = $data['vendor_details']['name'].',';
		}
		$this->load->library('Pdf');
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		

		// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, NULL, '', array(0,0,0), array(255,255,255));
		$pdf->SetFooterData(array(0,0,0), array(255,255,255));

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(10, PDF_MARGIN_TOP-10, 10);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		    require_once(dirname(__FILE__).'/lang/eng.php');
		    $pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('helvetica', '', 10);

		// add a page
		$pdf->AddPage();

		

		// define some HTML content with style


		$table_str = '';

		$i=0;
        foreach ($data['rfq_details'] as $key => $value) { 
			$bg_color = '';
			if($i%2 != 0){
				$bg_color = '#e4e1e1;';
			}

			if(strpos($value['quantity'], '.') === false){
				$value['quantity'] = $value['quantity'].".00";
			}

			$table_str .= '<tr style="background-color: '.$bg_color.'; font-size: 11px; font-family: courier;">
				<td style="text-align: right;">'.++$i.'</td>
				<td>'.$value['description'].'</td>
				<td style="text-align: right;"> '.$value['quantity'].'</td>
				<td>'.$value['unit_value'].'.</td>
			</tr>';
        }

        $date = date('d-m-Y', strtotime($data['rfq_details'][0]['rfq_date']));
		$client_name = trim($data['vendor_details']['vendor_name']);

		$html = '<!-- EXAMPLE OF CSS STYLE -->
		<table cellpadding="5" cellspacing="0">
			<tr style="background-color: #fff;">
				<td width="50%" style="padding:5px;vertical-align: text-top; ">
					<img src="/assets/media/client-logos/logo.png" width="180" height="50" style="padding-left: 10px;"><br/>
					<strong style="font-size: 17px;">OM TUBES & FITTINGS INDUSTRIES</strong>
					<div class="left_addrs" style="font-size: 14px;color: #484545; line-height: 20px;">
						10 Bordi Bunglow, 1st Panjarapole Lane, CP Tank, Mumbai, Maharashtra, India <br/>
						GSTIN 27AFRPM5323E1ZC
					</div>
					<table style="margin-top: -15px;">
						<tr style="background-color: #fff;">
							<td style="font-size: 14px; color: #484545; line-height: 20px;" width="50%">+91 (22) 6743 7634</td>
						</tr>
						<tr style="background-color: #fff;">
							<td style="font-size: 14px; color: #484545; line-height: 20px;" align="left">www.omtubes.com</td>
						</tr>
					</table>
				</td>
			<td width="50%">
				<strong style="font-size: 25px; text-align: right; line-height: 35px;">Request For Quotation</strong><br/>
				<table style="line-height: 22px;">
					<tr style="background-color: #e4e1e1; border-bottom: 1px solid;">
						<td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;"> RFQ # : </strong></td>
						<td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;">Date : </strong></td>
					</tr>
					<tr>
						<td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;">'.$data['rfq_details'][0]['rfq_no'].'</td>
						<td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;">'.$date.'</td>
					</tr>
					<tr style="background-color: #e4e1e1;">
						<td colspan="2" style="padding:5px;vertical-align: text-top;"><strong style="font-size: 12px;"> Seller:</strong></td>
					</tr>
					<tr>
						<td colspan="2" style="padding:5px;vertical-align: text-top; font-family: courier;">
							<strong>'.$client_name.'</strong>
							<div class="left_addrs" style="font-size: 11px;color: #484545; line-height: 17px; font-family: courier;">'.$data['vendor_details']['country'].'<br/>'.$data['vendor_details']['name'].'
							</div>
						</td>
					</tr>
					<tr style="background-color: #e4e1e1;">
						<td colspan="2" style="padding:5px;vertical-align: text-top;"><strong style="font-size: 12px;">Our reference :</strong></td>
					</tr>
					<tr>
						<td colspan="2" style="font-family: courier; font-size: 11px;color: #484545;">'.$data['rfq_details'][0]['reference'].'</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr style="background-color: #fff;">
			<td colspan="2" style="padding:5px;vertical-align: text-top; font-family: courier;"><strong style="font-size: 16px;">URGENT</strong><br/><br/>
			Dear '.$vendor_sales_person.'<br/><br/>
			please submit your quotation according to the following specification<br/>
			- price terms: as specified, including standard packing<br/>
			- validity of quotation: 90 days minimum<br/>
			- delivery time<br/>
			- resales discount<br/>
			- estimated total weight/ -volume<br/><br/>
			Thank you very much.
			</td>
					</tr>
					<tr>
						<td colspan="2"></td>
					</tr>
					<tr>
						<td colspan="2">
							<table cellspacing="0" cellpadding="10" border="0">
								<thead>
									<tr style="background-color: #e4e1e1;">
										<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="7%;">#</td>
										<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="63%">Item Description</td>
										<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="15%">Quantity</td>
										<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="15%">Unit</td>
									</tr>
								</thead>
								<tbody>'.$table_str.'</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td>
						</td>
						<td></td>
					</tr>
					<tr>
						<td style="font-size: 11px; font-family: courier;">
							<b style="font-size: 16px;">Additional Notes</b><br/>
							Please provide reason/explanation for possible deviation/discrepancy to our enquiry-specification and support<br/><br/>OM Tubes & Fittings Industries has conducted its business for decades with integrity. 
			Therefore, we also expect our suppliers to ensure our values. 
			For additional details, please contact us.
						</td>
						<td align="center">
							Thank you for your business<br/>
							For Om Tubes & Fittings Industries<br/><br/>
							<img src="/assets/media/stamp.png"/><br/><br/>
							<table>
								<tr>
									<td width="26%" rowspan="3"></td>
								 	<td align="left" width="74%" >Name : <span style="font-family: courier; ">'.$data['rfq_details'][0]['uname'].'</span></td>
								</tr>
								<tr>
									<td align="left">Email : <span style="font-family: courier;">'.$data['rfq_details'][0]['uemail'].'</span></td>
								</tr>
								<tr>
									<td align="left">Mobile : <span style="font-family: courier;">'.$data['rfq_details'][0]['umobile'].'</span></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>';

		// output the HTML content
		$pdf->writeHTML($html, true, false, true, false, '');

		//Close and output PDF document
		$pdf->Output(str_replace('/', '-', $data['rfq_details'][0]['rfq_no']).'.pdf', 'I');
	
	}

	function addNotes(){
		$note = $this->input->post('notes');
		$rfq_id = $this->input->post('rfq_id');

		$this->procurement_model->insertData('rfq_note_query', array('rfq_id' => $rfq_id, 'note' => $note, 'entered_on' => date('Y-m-d H:i:s'), 'entered_by' => $this->session->userdata('user_id'), 'type' => 'notes'));
		$res = $this->procurement_model->getData('rfq_note_query', "type='notes' and rfq_id = ".$rfq_id);
		foreach ($res as $key => $value) {
			$res[$key]['entered_on'] = date('d M h:i a', strtotime($value['entered_on']));
		}
		echo json_encode($res);
	}

	function addQuery(){
		$note = $this->input->post('notes');
		$rfq_id = $this->input->post('rfq_id');

		$quotation_details = $this->common_model->get_dynamic_data_sales_db('*',array('rfq_id'=> $rfq_id), 'quotation_mst', 'row_array');
		if(!empty($quotation_details)) {
			$client_details = $this->common_model->get_all_conditional_data_sales_db('*',array('client_id'=> $quotation_details['client_id']),'clients', 'row_array');
			$rfq_details = $this->common_model->get_all_conditional_data_sales_db('*',array('rfq_mst_id'=> $quotation_details['rfq_id']),'rfq_mst', 'row_array');
			$this->common_model->insert_data_sales_db('user_alert', array('module_name'=> 'rfq_query', 'alert_showed_user_id'=> $quotation_details['assigned_to'], 'alert_given_user_id'=> $this->session->userdata('user_id'), 'alert_subject'=> 'Query Submitted against rfq # '.$rfq_details['rfq_no'].' for client '.$client_details['client_name']));
		}
		$this->procurement_model->insertData('rfq_note_query', array('rfq_id' => $rfq_id, 'note' => $note, 'entered_on' => date('Y-m-d H:i:s'), 'entered_by' => $this->session->userdata('user_id'), 'type' => 'query'));
		$res = $this->procurement_model->getData('rfq_note_query', "type='query' and rfq_id = ".$rfq_id);
		foreach ($res as $key => $value) {
			$res[$key]['entered_on'] = date('d M h:i a', strtotime($value['entered_on']));
		}
		echo json_encode($res);
	}

	function searchLead(){
		$search = $this->input->post('search');
		$leads = $this->procurement_model->getClients($search);
		echo json_encode($leads); 
	}

	function deleteRfq(){
		$rfq_id = $this->input->post('rfq_id');
		$this->procurement_model->deleteData('rfq_note_query', array('rfq_id' => $rfq_id));
		$this->procurement_model->deleteData('rfq_to_vendor', array('rfq_id' => $rfq_id));
		$this->procurement_model->deleteData('rfq_dtl', array('rfq_mst_id' => $rfq_id));
		$this->procurement_model->deleteData('rfq_mst', array('rfq_mst_id' => $rfq_id));
	}

	public function procurement_listingz() {

		$data = $this->create_procurement_data(array('status'=> "Active"), array('column_name'=>'id', 'column_value'=>'desc'), 10, 0);
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;  
		$this->load->view('header', array('title' => 'Procurement Listing'));
		$this->load->view('sidebar', array('title' => 'PURCHASE ORDER LISTING'));
		$this->load->view('procurement/procurement_listingz', $data);
		$this->load->view('footer');
	}

	public function update_purchase_order() {

		$this->unset_session();
		$data = array();
		$data['procurement_data'] = array('id'=>'', 'subject'=>'', 'procurement_person'=> '','sales_person'=> '','vendor_name'=> '','vendor_country'=> '','vendor_sales_person_name'=> '','procurement_status'=> '','comments'=> '','delivery_to'=> '','currency'=> '','delivery_time'=> '','country_of_origin'=> '','payment'=> '','mtc_type'=> '','validity'=> '','packing_type'=>'', 'po_type'=>'');
		$data['next_count_number'] = 1;
		$procurement_id = $this->uri->segment(3, 0);
		if(!empty($procurement_id)) {

			$data['procurement_data'] = $this->procurement_model->get_all_data('*', array('status'=> "Active", 'id'=> $procurement_id), 'procurement','row_array');
			$data['product_details'] = $this->procurement_model->get_all_data('*', array('status'=> "Active", 'procurement_id'=> $procurement_id), 'procurement_product_information');
			$data['charges_details'] = $this->procurement_model->get_all_data('*', array('status'=> "Active", 'procurement_id'=> $procurement_id), 'procurement_charges_information');
			$this->generate_session($data['procurement_data'], $data['product_details'], $data['charges_details']);
			$data['next_count_number'] = count($data['product_details'])+1;
		}
		$data['sales_person'] = $this->common_model->get_sales_person('name, user_id');
		$data['procurement_person'] = $this->common_model->get_procurement_person();
		$data['vendor_list'] = $this->procurement_model->get_vendor_list();
		$data['vendor_sales_person'] = $this->procurement_model->get_vendor_sales_person();
		// $data['product_list'] = $this->procurement_model->get_lookup_data(259);
		// $data['material_list'] = $this->procurement_model->get_lookup_data(272);
		$data['product_list'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'product_mst');
		$data['material_list'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'material_mst');
		$data['units_list'] = $this->procurement_model->get_units_data();
		$data['delivery'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'delivery');
		$data['currency'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'currency');
		$data['delivery_time'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'delivery_time');
		$data['origin_country'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'origin_country');
		$data['payment_terms'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'payment_terms');
		$data['mtc_type'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'mtc_type');
		$data['validity'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'validity');
		$data['transport_mode'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'transport_mode');
		// $footer_text = $this->procurement_model->get_all_data('footer_text', array('status'=> 'Active', 'pdf_type'=> 'procurement', 'client_type'=> 'om'), 'pdf_footer', 'row_array');
		// $footer = '';
 		// $footer = $footer_text['footer_text'];
		// if($this->session->userdata('user_id') == 23) {

		// 	$footer = str_replace('procurement@omtubes.com', 'yash@omtubes.com', $footer);
		// } else if($this->session->userdata('user_id') == 45) {

		// 	$footer = str_replace('procurement@omtubes.com', 'rfq@omtubes.com', $footer);
		// } else if($this->session->userdata('user_id') == 77) {

		// 	$footer = str_replace('procurement@omtubes.com', 'sourcing@omtubes.com', $footer);
		// } else if($this->session->userdata('user_id') == 42) {

		// 	$footer = str_replace('procurement@omtubes.com', 'purchasing@omtubes.com', $footer);
		// } else if($this->session->userdata('user_id') == 33) {

		// 	$footer = str_replace('procurement@omtubes.com', 'technical@omtubes.com', $footer);
		// }
		// $data['footer_text'] = $footer;
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Purchase Order'));
		$this->load->view('sidebar', array('title' => 'Purchase Order'));
		$this->load->view('procurement/update_purchase_order', $data);
		$this->load->view('footer');
	}

	public function update_purchase_order_via_production() {

		$this->unset_session();
		$data = array();
		$data['procurement_data'] = array('wo'=>'', 'id'=>'', 'subject'=>'', 'procurement_person'=> '','sales_person'=> '','vendor_name'=> '','vendor_country'=> '','vendor_sales_person_name'=> '','procurement_status'=> '','comments'=> '','delivery_to'=> '','currency'=> '','delivery_time'=> '','country_of_origin'=> '','payment'=> '','mtc_type'=> '','validity'=> '','packing_type'=>'', 'po_type'=> '');
		$data['next_count_number'] = 1;
		$user_details = array_column($this->common_model->get_dynamic_data_sales_db('user_id, name', array(), 'users'), 'name', 'user_id');
		$data['sales_person'] = $this->common_model->get_sales_person('name, user_id');
		$data['procurement_person'] = $this->common_model->get_procurement_person();
		$data['vendor_list'] = $this->procurement_model->get_vendor_list();
		$data['vendor_sales_person'] = $this->procurement_model->get_vendor_sales_person();
		// $data['product_list'] = $this->procurement_model->get_lookup_data(259);
		// $data['material_list'] = $this->procurement_model->get_lookup_data(272);
		$data['product_list'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'product_mst');
		$data['material_list'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'material_mst');
		$data['units_list'] = $this->procurement_model->get_units_data();
		$data['delivery'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'delivery');
		$data['currency'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'currency');
		$data['delivery_time'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'delivery_time');
		$data['origin_country'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'origin_country');
		$data['payment_terms'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'payment_terms');
		$data['mtc_type'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'mtc_type');
		$data['validity'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'validity');
		$data['transport_mode'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'transport_mode');
		$footer_text = $this->procurement_model->get_all_data('footer_text', array('status'=> 'Active', 'pdf_type'=> 'procurement'), 'pdf_footer', 'row_array');
		$footer = '';
 		$footer = $footer_text['footer_text'];
		if($this->session->userdata('user_id') == 23) {

			$footer = str_replace('procurement@omtubes.com', 'yash@omtubes.com', $footer);
		} else if($this->session->userdata('user_id') == 45) {

			$footer = str_replace('procurement@omtubes.com', 'rfq@omtubes.com', $footer);
		} else if($this->session->userdata('user_id') == 77) {

			$footer = str_replace('procurement@omtubes.com', 'sourcing@omtubes.com', $footer);
		} else if($this->session->userdata('user_id') == 42) {

			$footer = str_replace('procurement@omtubes.com', 'purchasing@omtubes.com', $footer);
		} else if($this->session->userdata('user_id') == 33) {

			$footer = str_replace('procurement@omtubes.com', 'technical@omtubes.com', $footer);
		}
		$data['footer_text'] = $footer;

		$quotation_mst_id = $data['procurement_data']['id'] = $this->uri->segment(3, 0);
		$quotation_details = $this->common_model->get_dynamic_data_sales_db('*', array('quotation_mst_id'=> $quotation_mst_id), 'quotation_mst','row_array');
		$data['procurement_data']['wo'] = $quotation_details['work_order_no'];
		$client_static_array = array('omtubes'=> 'om', 'zengineer'=> 'zen', 'instinox'=> 'in');
		$data['procurement_data']['po_type'] = $client_static_array[$quotation_details['client_type']];
		// echo "<pre>";print_r($data);echo"</pre><hr>";
		// echo "<pre>";print_r($quotation_details);echo"</pre><hr>";exit;
		if(!empty($quotation_details)){
			$data['procurement_data']['sales_person'] = $user_details[$quotation_details['assigned_to']];
			$quotation_product_details = $this->procurement_model->get_all_data('*', array('quotation_mst_id'=> $quotation_mst_id), 'quotation_dtl');
			$product_details = array_column($this->common_model->get_dynamic_data_sales_db('id, name', array(), 'product_mst'), 'name', 'id');
			$material_details = array_column($this->common_model->get_dynamic_data_sales_db('id, name', array(), 'material_mst'), 'name', 'id');

			// $lookup_details = array_column($this->common_model->get_dynamic_data_sales_db('lookup_id, lookup_value', array(), 'lookup'), 'lookup_value', 'lookup_id');
			$unit_details = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status'=> 'Active'), 'units'), 'unit_value', 'unit_id');
			// echo "<pre>";print_r($unit_details);echo"</pre><hr>";exit;
			$net_total = 0;
			if(!empty($quotation_product_details)){

				//checking record already exist or not
				$procurement_details = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'work_order_no'=> $data['procurement_data']['wo']), 'procurement');

				if(empty($procurement_details)) {

					foreach($quotation_product_details as $product_key => $single_quotation_product_details){

						$data['product_details'][$product_key]['product_name'] = $product_details[$single_quotation_product_details['product_id']];
						$data['product_details'][$product_key]['material_name'] = $material_details[$single_quotation_product_details['material_id']];
						$data['product_details'][$product_key]['description_name'] = $single_quotation_product_details['description'];	
						$data['product_details'][$product_key]['quantity'] = $single_quotation_product_details['quantity'];	
						$data['product_details'][$product_key]['units'] = $unit_details[$single_quotation_product_details['unit']];	
						$data['product_details'][$product_key]['price'] = 0;	
						$data['product_details'][$product_key]['per_unit'] = 0;	
						$data['product_details'][$product_key]['total'] = 0;
					}
				}else{


					$procurement_product_details = $this->common_model->get_dynamic_data_sales_db_null_false('*', "status = 'Active' AND procurement_id IN ('".implode("', '", array_column($procurement_details, 'id'))."')", 'procurement_product_information');
					if(!empty($procurement_product_details)){

						// echo "<pre>";print_r($quotation_product_details);echo"</pre><hr>";
						// echo "<pre>";print_r($procurement_product_details);echo"</pre><hr>";exit;
						foreach($quotation_product_details as $single_quotation_product_details){

							$add_remove_flag = true;
							// echo "<pre>";print_r($single_quotation_product_details);echo"</pre><hr><hr><hr>";
							foreach($procurement_product_details as $single_procurement_product_details){

								// echo "<pre>";print_r($single_procurement_product_details);echo"</pre><hr>";
								if(
									$single_procurement_product_details['product_name'] == $product_details[$single_quotation_product_details['product_id']]
									&&
									$single_procurement_product_details['material_name'] == $material_details[$single_quotation_product_details['material_id']]
									&&
									$single_procurement_product_details['description_name'] == $single_quotation_product_details['description']
									&&
									$single_procurement_product_details['quantity'] == $single_quotation_product_details['quantity']
									&&
									$single_procurement_product_details['units'] == $unit_details[$single_quotation_product_details['unit']]
								){
									
									$add_remove_flag = false;
								}
							}
							if($add_remove_flag){
								
								$data['product_details'][] = array(
																'product_name' => $product_details[$single_quotation_product_details['product_id']],
																'material_name' => $material_details[$single_quotation_product_details['material_id']],
																'description_name' => $single_quotation_product_details['description'],
																'quantity' => $single_quotation_product_details['quantity'],
																'units' => $unit_details[$single_quotation_product_details['unit']],
																'price' => 0,
																'per_unit' => 0,
																'total' => 0,
															);
							}
						}
					}else{

						foreach($quotation_product_details as $product_key => $single_quotation_product_details){

							$data['product_details'][$product_key]['product_name'] = $product_details[$single_quotation_product_details['product_id']];
							$data['product_details'][$product_key]['material_name'] = $material_details[$single_quotation_product_details['material_id']];
							$data['product_details'][$product_key]['description_name'] = $single_quotation_product_details['description'];	
							$data['product_details'][$product_key]['quantity'] = $single_quotation_product_details['quantity'];	
							$data['product_details'][$product_key]['units'] = $unit_details[$single_quotation_product_details['unit']];	
							$data['product_details'][$product_key]['price'] = 0;	
							$data['product_details'][$product_key]['per_unit'] = 0;	
							$data['product_details'][$product_key]['total'] = 0;
						}
					}
				}
			}
			$this->generate_session(
									array(
										'net_total' => $net_total,
										'gross_total' => $net_total
									),
									$data['product_details'],
									array()
								);
			$data['next_count_number'] = count($data['product_details'])+1;
		}

		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Purchase Order'));
		$this->load->view('sidebar', array('title' => 'Purchase Order'));
		$this->load->view('procurement/update_purchase_order_via_production', $data);
		$this->load->view('footer');
	}

	private function generate_session($procurement_data, $product_details, $charges_details) {

		$charge_name_array = array('Added'=>'+', 'Deducted'=>'-');
		$order_details['net_total'] = $procurement_data['net_total'];
		$order_details['gross_total'] = $procurement_data['gross_total'];
		foreach ($product_details as $product_details_key => $product_details_value) {
			$order_details['all_net_total'][$product_details_key+1] = $product_details_value['total'];
		}	
		foreach ($charges_details as $charges_details_key => $charges_details_value) {
			
			$order_details['different_charges'][$charges_details_key]['charges_name'] = $charges_details_value['charge_name'];
			$order_details['different_charges'][$charges_details_key]['add_minus_charges'] = $charge_name_array[$charges_details_value['charge_deducted_or_added']];
			$order_details['different_charges'][$charges_details_key]['charge_value'] = $charges_details_value['charge_value'];
			$order_details['different_charges'][$charges_details_key]['add_minus_charges_value'] = $charges_details_value['charge_deducted_or_added'];
			$order_details['different_charges'][$charges_details_key]['gross_total'] = $charges_details_value['gross_total_after_charge'];
		}
		$this->session->set_userdata('order_details', $order_details);
	}

	public function unset_session() {
		$this->session->unset_userdata('order_details');
	}
	
	public function show_session() {

		echo "<pre>";print_r($this->session->userdata());echo"</pre><hr>";exit;
	}

	public function pdf(){

		$procurement_id = $this->uri->segment('3',0);
		$data['procurement_data'] = $this->procurement_model->get_all_data('*', array('status'=> "Active", 'id'=> $procurement_id), 'procurement','row_array');
		$data['product_details'] = $this->procurement_model->get_all_data('*', array('status'=> "Active", 'procurement_id'=> $procurement_id), 'procurement_product_information');
		$data['charges_details'] = $this->procurement_model->get_all_data('*', array('status'=> "Active", 'procurement_id'=> $procurement_id), 'procurement_charges_information');
		$data['grand_total_words'] = 'INR : '.$this->numberTowords($data['procurement_data']['gross_total'], 'paise');
		$data['purchase_person_details'] = $this->procurement_model->get_all_data('*', array('name'=> $data['procurement_data']['procurement_person']), 'users','row_array');
		// $data['country_list'] = array_column($this->procurement_model->get_all_data('country_id, country_name', array('status'=> "Active"), 'country'), 'country_name', 'country_id');
		$data['country_list'] = array_column($this->procurement_model->get_all_data('id, name', array('status'=> "Active"), 'country_mst'), 'name', 'id');
		if($this->session->userdata('user_id') == 65){

			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
			// $this->load->view('procurement/procurement_pdf', $data);
		}
		$data['pdf_information']['logo_path'] = 'assets/media/client-logos/logo.png';
		$data['pdf_information']['name'] = 'OM TUBES & FITTINGS INDUSTRIES';
		$data['pdf_information']['address'] = 'Plot no. 10, Bordi Bunglow, 1st Panjarapole Lane, CP Tank, Mumbai, Maharashtra 400004, India<br> GSTIN 27AFRPM5323E1ZC';
		$data['pdf_information']['number'] = '+91 (22) 6743 7634';
		$data['pdf_information']['email'] = 'www.omtubes.com';
		$data['pdf_information']['thank_you_name'] = 'Om Tubes & Fittings Industries';
		$data['pdf_information']['stamp_path'] = 'assets/media/stamp.png';
		if(in_array($data['procurement_data']['po_type'], array('in', 'zen'))){

			if($data['procurement_data']['po_type'] == 'in'){
				
				$data['pdf_information']['logo_path'] = 'assets/media/client-logos/Instinox_logo.png';
				$data['pdf_information']['name'] = 'INSTINOX LLP';
				$data['pdf_information']['address'] = 'Gr,3,32, Carpenter 2nd street, CP Tank, Mumbai 400004, India <br/>GSTIN 27AAIFI3989G1ZW';
				$data['pdf_information']['number'] = '+91 (22) 6743 9101';
				$data['pdf_information']['email'] = 'sales@instinox.com';
				$data['pdf_information']['thank_you_name'] = 'Instinox LLP';
				$data['pdf_information']['stamp_path'] = 'assets/media/client-logos/instinox_stamp.jpeg';
			}else if($data['procurement_data']['po_type'] == 'zen'){

				$data['pdf_information']['logo_path'] = 'assets/media/client-logos/logo_zengineer.PNG';
				$data['pdf_information']['name'] = 'Zengineer Manufacturing LLP';
				$data['pdf_information']['address'] = '1st Floor, Office No.05, Plot No. C.T.S 2951, Shree Jaya
				Building Premises, Co-Op Housing Society, 2nd Carpenter Street, Neo Galaxy, Carpenter street, Mumbai 400004, Maharashtra<br/>GSTIN 27AADFZ2705JlZl';
				$data['pdf_information']['number'] = '';
				$data['pdf_information']['email'] = 'info@zengineermfg.com';
				$data['pdf_information']['thank_you_name'] = 'Zengineer Manufacturing LLP';
				$data['pdf_information']['stamp_path'] = 'assets/media/client-logos/zengineer_stamp.png';
			}
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		// $data['']
		$this->load->library('Pdf');
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

			// set document information
			

			// set default header data
			$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, NULL, '', array(0,0,0), array(255,255,255));
			$pdf->SetFooterData(array(0,0,0), array(255,255,255));

			// set header and footer fonts
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			// set margins
			$pdf->SetMargins(10, PDF_MARGIN_TOP-10, 10);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER-5);

			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM-15);

			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			// set some language-dependent strings (optional)
			if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			    require_once(dirname(__FILE__).'/lang/eng.php');
			    $pdf->setLanguageArray($l);
			}

			// ---------------------------------------------------------

			// set font
			$pdf->SetFont('helvetica', '', 10);

		// add a page
		$pdf->AddPage();
		$html= $this->load->view('procurement/procurement_pdf', $data, true);	
		// output the HTML content
		$pdf->writeHTML($html, true, true, true, false, '<ur><li>L : left align</li></ul>');

		//Close and output PDF document
		$pdf->Output(str_replace('/', '-', $data['procurement_data']['procurement_no']).'.pdf', 'I');
	}

	function numberTowords($num, $dec_text)
	{

		$ones = array(
		0 =>"ZERO",
		1 => "ONE",
		2 => "TWO",
		3 => "THREE",
		4 => "FOUR",
		5 => "FIVE",
		6 => "SIX",
		7 => "SEVEN",
		8 => "EIGHT",
		9 => "NINE",
		10 => "TEN",
		11 => "ELEVEN",
		12 => "TWELVE",
		13 => "THIRTEEN",
		14 => "FOURTEEN",
		15 => "FIFTEEN",
		16 => "SIXTEEN",
		17 => "SEVENTEEN",
		18 => "EIGHTEEN",
		19 => "NINETEEN",
		"014" => "FOURTEEN"
		);
		$tens = array( 
		0 => "ZERO",
		1 => "TEN",
		2 => "TWENTY",
		3 => "THIRTY", 
		4 => "FORTY", 
		5 => "FIFTY", 
		6 => "SIXTY", 
		7 => "SEVENTY", 
		8 => "EIGHTY", 
		9 => "NINETY" 
		); 
		$hundreds = array( 
		"HUNDRED", 
		"THOUSAND", 
		"MILLION", 
		"BILLION", 
		"TRILLION", 
		"QUARDRILLION" 
		); /*limit t quadrillion */
		$num = number_format($num,2,".",","); 
		$num_arr = explode(".",$num); 
		$wholenum = $num_arr[0]; 
		$decnum = $num_arr[1]; 
		$whole_arr = array_reverse(explode(",",$wholenum)); 
		krsort($whole_arr,1); 
		$rettxt = ""; 
		foreach($whole_arr as $key => $i){
			
			if(!empty($i)){
				
				while(substr($i,0,1)=="0")
					$i=substr($i,1,5);
					if($i < 20){ 
					/* echo "getting:".$i; */
						if(!empty($ones[$i])) {

							$rettxt .= $ones[$i]; 
						}
					}elseif($i < 100){ 
					if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)]; 
					if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)]; 
					}else{ 
					if(substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
					if(substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)]; 
					if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)]; 
					} 
					if($key > 0){ 
					$rettxt .= " ".$hundreds[$key]." "; 
					}
			}
		} 
		if($decnum > 0){
			$rettxt .= " and ";
			if($decnum < 20){
				if(!empty($ones[$decnum])) {
					$rettxt .= $ones[$decnum];
				}
			}elseif($decnum < 100){
				$rettxt .= $tens[substr($decnum,0,1)];
				$rettxt .= " ".$ones[substr($decnum,1,1)];
			}

			$rettxt .= " ".$dec_text;
		}
		return $rettxt;
	}

	public function ajax_function() {

		if($this->input->is_ajax_request()){
			$call_type = $this->input->post('call_type');
			$response['status'] = 'successful';	
			switch ($call_type) {

				case 'rfq_pending_list':

					$filter_date = explode(' - ', $this->input->post('filter_date'));
					$filter_status = (!empty($this->input->post('filter_status'))) ? $this->input->post('filter_status'):'pending';
					$filter_sales_person = (!empty($this->input->post('filter_sales_person'))) ? $this->input->post('filter_sales_person'):'All';

					$date_where = array();	
					if(count($filter_date) > 1) {

						$date_where['entered_on >='] = date('Y-m-d', strtotime($filter_date[0]));
						$date_where['entered_on <='] = date('Y-m-d', strtotime($filter_date[1]));
					}else {

						$date_where['entered_on'] = date('Y-m-d', strtotime($filter_date[0]));
					}
					$pending_rfq_data = $this->procurement_model->get_rfq_graph_data($date_where, $filter_status, $filter_sales_person);
					$rfq_data = array();
					foreach ($pending_rfq_data as $value) {
						
						if($value['assigned_to_1'] != 0){

							if(!empty($rfq_data[$value['assigned_to_1']])){

								$rfq_data[$value['assigned_to_1']]['count'] += 1;
							}else{

								$rfq_data[$value['assigned_to_1']] = array(
																		'user_id' => $value['assigned_to_1'],
																		'count' => 1
																	);
							}
						}
						if($value['assigned_to_2'] != 0){
							
							if(!empty($rfq_data[$value['assigned_to_2']])){

								$rfq_data[$value['assigned_to_2']]['count'] += 1;
							}else{

								$rfq_data[$value['assigned_to_2']] = array(
																		'user_id' => $value['assigned_to_2'],
																		'count' => 1
																	);;
							}	
						}
						if($value['assigned_to_3'] != 0){
							
							if(!empty($rfq_data[$value['assigned_to_3']])){

								$rfq_data[$value['assigned_to_3']]['count'] += 1;
							}else{

								$rfq_data[$value['assigned_to_3']] = array(
																		'user_id' => $value['assigned_to_3'],
																		'count' => 1
																	);;
							}
						}
					}
					rsort($rfq_data);
					$user_details = array_column($this->common_model->get_dynamic_data_sales_db('*', array(), 'users'), 'name', 'user_id');
					foreach ($rfq_data as $rfq_user_details) {

						$response['highchart_data'][] = array(
															'name' => $user_details[$rfq_user_details['user_id']],
															'y' => (int)$rfq_user_details['count'],
															'drilldown' => $user_details[$rfq_user_details['user_id']]
														);
					}
					$sales_person = array();
					if(in_array($this->session->userdata('role'), array(1, 17))){

						$sales_person = array_column($this->procurement_model->getData('users', '(role = 5  OR role = 16) and status = 1'), 'name', 'user_id');
					} else if ($this->session->userdata('role') == '6') {

						$sales_person = array_column($this->procurement_model->getData('users', 'role = 5 and status = 1'), 'name', 'user_id');
					}
					$response['html_body']['filter_sales_person'] = $this->load->view(
																				'procurement/filter_sales_person',
																				array(
																					'sales_person' => $sales_person,
																					'filter_sales_person' => $filter_sales_person,
																					'sales_filter_used_for_type' => 'bar_graph'
																				),
																				TRUE);
					$response['html_body']['filter_sales_person_id'] = $filter_sales_person;
					$response['html_body']['filter_sales_person_name'] = ($filter_sales_person != 'All') ? $sales_person[$filter_sales_person] : $filter_sales_person;
					// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
				break;

				case 'add_charges_in_purchase_order':
					
					$charge_name_array = array('+'=>'Added', '-'=>'Deducted');
					$form_data = array_column($this->input->post('add_charges_in_purchase_order_form'), 'value', 'name');
					$form_data['add_minus_charges_value'] = $charge_name_array[$form_data['add_minus_charges']]; 
					$find_percentage_in_string = strpos($form_data['charge_value'], '%');
					$order_details = $this->session->userdata('order_details');
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";
					// echo "<pre>";print_r($order_details);echo"</pre><hr>";die;
					$gross_total = $order_details['gross_total'];
					if($find_percentage_in_string) {
						$percentage_value = $gross_total*(((int) $form_data['charge_value'] ) / 100 );
						if($form_data['add_minus_charges'] == '+') {

							$form_data['gross_total'] = $gross_total + $percentage_value; 
						} else if ($form_data['add_minus_charges'] == '-'){

							$form_data['gross_total'] = $gross_total - $percentage_value; 
						}
					} else {

						if($form_data['add_minus_charges'] == '+') {

							$form_data['gross_total'] = $gross_total + $form_data['charge_value']; 
						} else if ($form_data['add_minus_charges'] == '-'){

							$form_data['gross_total'] = $gross_total - $form_data['charge_value']; 
						}
					}
					
					if(empty($order_details['different_charges'])){

						$order_details['different_charges'][0] = $form_data;
					}else {

						$order_details['different_charges'][count($order_details['different_charges'])] = $form_data;
					}


					$order_details['gross_total'] = $form_data['gross_total'];
					// echo "<pre>";print_r($order_details);echo"</pre><hr>";exit;
					$this->session->set_userdata('order_details', $order_details);
					// echo "<pre>";print_r($form_data);echo"</pre><hr>";exit;
					break;

				case 'get_gst_or_discount_history':

					$response['charge_history_body'] = $this->load->view('procurement/charges_history', array(), true);
				break;	

				case 'set_order_detail':

					if(!empty($this->input->post('net_total'))) {
						if(empty($this->session->userdata('order_details'))) {

							$net_total = (int)$this->input->post('net_total');
							$gross_total = (int)$this->input->post('net_total');
							$all_net_total[$this->input->post('count_number')] = $net_total;
						} else {

							$all_net_total = $this->session->userdata('order_details')['all_net_total'];
							if(isset($all_net_total[$this->input->post('count_number')])) {

								$all_net_total[$this->input->post('count_number')] = (int)$this->input->post('net_total'); 
							} else {

								array_push($all_net_total, (int)$this->input->post('net_total'));
							}
							$net_total = array_sum($all_net_total);
						}

						$this->session->set_userdata(
												'order_details',
												array(
													'net_total' => $net_total,
													'gross_total' => $net_total,
													'all_net_total'=> $all_net_total
												)
											);
					}
					$response['net_total_gross_total_body'] = $this->load->view(
																'procurement/net_total_gross_total_body',
																array(),
																true);
				break;	
				
				case 'add_purchase_order':
					
					// echo "<pre>";print_r($this->input->post('person_details'));echo"</pre><hr>";
					// echo "<pre>";print_r($this->input->post('terms_conditon'));echo"</pre><hr>";
					// echo "<pre>";print_r($this->input->post('product_details'));echo"</pre><hr>";exit;
					$response['message'] = "Procurement Data are added";
					$form_data = array();
					$form_data['person_details'] = array_column($this->input->post('person_details'), 'value', 'name');
					$form_data['terms_conditon'] = array_column($this->input->post('terms_conditon'), 'value', 'name');
					$form_data['product_details'] = array_column($this->input->post('product_details'), 'value', 'name');
					// echo "<pre>";print_r($form_data);echo"</pre><hr>";die;
					// preparing procurement person information
						$insert_array = array_merge($form_data['person_details'], $form_data['terms_conditon']);
						$insert_array['term_condition_text'] = $this->input->post('term_condition_text');
						$insert_array['procurement_no'] = $this->get_quote_no($form_data['person_details']['po_type']);
						$insert_array['net_total'] = $this->session->userdata('order_details')['net_total'];
						$insert_array['gross_total'] = $this->session->userdata('order_details')['gross_total'];
						$procurement_id = $this->procurement_model->insertData('procurement', $insert_array);
					// preparing product information 
						for ($i=1; $i <= (count($this->input->post('product_details')) / 8) ; $i++) { 
							
							$product_insert_array[] = array(
														'procurement_id' => $procurement_id,
														'product_name' => $form_data['product_details']['product_name_'.$i], 
														'material_name' => $form_data['product_details']['material_name_'.$i], 
														'description_name' => $form_data['product_details']['description_name_'.$i], 
														'quantity' => $form_data['product_details']['quantity_'.$i], 
														'units' => $form_data['product_details']['units_'.$i], 
														'price' => $form_data['product_details']['price_'.$i], 
														'per_unit' => $form_data['product_details']['per_unit_'.$i], 
														'total' => $form_data['product_details']['total_'.$i], 
													);
						}
						// echo "<pre>";print_r($product_insert_array);echo"</pre><hr>";
						if(!empty($product_insert_array)) {

							$this->procurement_model->insert_data_batch('procurement_product_information', $product_insert_array);
						}
					// preparing charges information
						$charges_insert_array = array();
						if(!empty($this->session->userdata('order_details')['different_charges'])) {

							foreach ($this->session->userdata('order_details')['different_charges'] as $charge_key => $charges_details) {
								
								$charges_insert_array[$charge_key]['procurement_id'] = $procurement_id; 
								$charges_insert_array[$charge_key]['charge_name'] = $charges_details['charges_name']; 
								$charges_insert_array[$charge_key]['charge_value'] = $charges_details['charge_value']; 
								$charges_insert_array[$charge_key]['charge_deducted_or_added'] = $charges_details['add_minus_charges_value']; 
								$charges_insert_array[$charge_key]['gross_total_after_charge'] = $charges_details['gross_total']; 
							}
							// echo "<pre>";print_r($charges_insert_array);echo"</pre><hr>";exit;
							$this->procurement_model->insert_data_batch('procurement_charges_information', $charges_insert_array);
						}
				break;

				case 'update_purchase_order':
					
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					$response['message'] = "Procurement Data are updated";
					$form_data = array();
					$form_data['person_details'] = array_column($this->input->post('person_details'), 'value', 'name');
					$form_data['terms_conditon'] = array_column($this->input->post('terms_conditon'), 'value', 'name');
					$form_data['product_details'] = array_column($this->input->post('product_details'), 'value', 'name');
					$procurement_id = $this->input->post('procurement_id');
					// preparing procurement person information
						$update_array = array_merge($form_data['person_details'], $form_data['terms_conditon']);
						$update_array['term_condition_text'] = $this->input->post('term_condition_text');
						$update_array['net_total'] = $this->session->userdata('order_details')['net_total'];
						$update_array['gross_total'] = $this->session->userdata('order_details')['gross_total'];
						// echo "<pre>";print_r($update_array);echo"</pre><hr>";exit;
						$this->procurement_model->updateData('procurement', $update_array, array('id'=>$procurement_id));
					// preparing product information 
						$this->procurement_model->updateData('procurement_product_information', array('status'=>'Inactive'), array('procurement_id'=>$procurement_id));
						for ($i=1; $i <= (count($this->input->post('product_details')) / 8) ; $i++) { 
							
							$product_insert_array[] = array(
														'procurement_id' => $procurement_id,
														'product_name' => $form_data['product_details']['product_name_'.$i], 
														'material_name' => $form_data['product_details']['material_name_'.$i], 
														'description_name' => $form_data['product_details']['description_name_'.$i], 
														'quantity' => $form_data['product_details']['quantity_'.$i], 
														'units' => $form_data['product_details']['units_'.$i], 
														'price' => $form_data['product_details']['price_'.$i], 
														'per_unit' => $form_data['product_details']['per_unit_'.$i], 
														'total' => $form_data['product_details']['total_'.$i], 
													);
						}
						// echo "<pre>";print_r($product_insert_array);echo"</pre><hr>";
						if(!empty($product_insert_array)) {

							$this->procurement_model->insert_data_batch('procurement_product_information', $product_insert_array);
						}
					// preparing charges information
						$this->procurement_model->updateData('procurement_charges_information', array('status'=>'Inactive'), array('procurement_id'=>$procurement_id));
						$charges_insert_array = array();
						if(!empty($this->session->userdata('order_details')['different_charges'])) {

							foreach ($this->session->userdata('order_details')['different_charges'] as $charge_key => $charges_details) {
								
								$charges_insert_array[$charge_key]['procurement_id'] = $procurement_id; 
								$charges_insert_array[$charge_key]['charge_name'] = $charges_details['charges_name']; 
								$charges_insert_array[$charge_key]['charge_value'] = $charges_details['charge_value']; 
								$charges_insert_array[$charge_key]['charge_deducted_or_added'] = $charges_details['add_minus_charges_value']; 
								$charges_insert_array[$charge_key]['gross_total_after_charge'] = $charges_details['gross_total']; 
							}
							$this->procurement_model->insert_data_batch('procurement_charges_information', $charges_insert_array);
						}
						
				break;		

				case 'set_vendor_name_and_country':
					
					$response['message'] = "record not found";
					if(!empty($this->input->post('vendor_id'))){

						//getting vendor name and country name based on vendor id
						$vendor_details = $this->procurement_model->get_all_data('vendor_name, country', array('status'=> "Active", 'vendor_id'=> $this->input->post('vendor_id')), 'vendors', 'row_array');
						if(!empty($vendor_details)){

							$response['message'] = "record found";
							$response['vendor_details'] = $vendor_details;
							// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
						}
					}
				break;

				case 'get_product_body':
					
					if(!empty($this->input->post('next_count_number'))){

						$data['next_count_number'] = $this->input->post('next_count_number');
						$data['material_name'] = $this->input->post('material_name');
						$data['product_name'] = $this->input->post('product_name');
						$data['product_list'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'product_mst');
						$data['material_list'] = $this->procurement_model->get_all_data('*', array('status'=> 'Active'), 'material_mst');
						$data['units_list'] = $this->procurement_model->get_units_data();
						$response['product_list_body']	= $this->load->view('procurement/new_product_body', $data, true);
					}
					break;

				case 'delete_procurement_list_data':
					
					if(!empty($this->input->post('procurement_id'))){
						$this->procurement_model->updateData('procurement', array('status'=>'Inactive'), array('id'=>$this->input->post('procurement_id')));
					}
					break;	

				case 'search_filter':
					
					$response_html = $this->all_filter_data($this->input->post('search_form_data'), array('column_name'=>'id', 'column_value'=>'asc'), 10, 0);

					$response['procurement_body'] = $response_html['procurement_body']; 
					$response['procurement_search_filter'] = $response_html['procurement_search_filter'];
					$response['procurement_sorting'] = $response_html['procurement_sorting'];
					$response['procurement_paggination'] = $response_html['procurement_paggination'];

					break;

				case 'sort_filter':

					$response_html = $this->all_filter_data(
											$this->input->post('search_form_data'), 
											array(
												'column_name'=> $this->input->post('procurement_sort_name'), 
												'column_value'=>$this->input->post('procurement_sort_value')
											), 10, 0
										);
					$response['procurement_body'] = $response_html['procurement_body']; 
					$response['procurement_search_filter'] = $response_html['procurement_search_filter'];
					$response['procurement_sorting'] = $response_html['procurement_sorting'];
					$response['procurement_paggination'] = $response_html['procurement_paggination'];
					break;

				case 'paggination_filter':

					$response_html = $this->all_filter_data($this->input->post('search_form_data'), array('column_name'=>'id', 'column_value'=>'desc'), $this->input->post('limit'), $this->input->post('offset'));
					$response['procurement_body'] = $response_html['procurement_body']; 
					$response['procurement_search_filter'] = $response_html['procurement_search_filter'];
					$response['procurement_sorting'] = $response_html['procurement_sorting'];
					$response['procurement_paggination'] = $response_html['procurement_paggination'];
				break;		

				case 'delete_product_details':
					
					$order_details = $this->session->userdata('order_details');
					$get_net_total = $order_details['all_net_total'][$this->input->post('count_no')];
					unset($order_details['all_net_total'][$this->input->post('count_no')]);
					$order_details['net_total'] = $order_details['net_total']-$get_net_total;
					$order_details['gross_total'] = $order_details['gross_total']-$get_net_total;
					foreach ($order_details['different_charges'] as $charge_key => $charges_details) {
						
						$order_details['different_charges'][$charge_key]['gross_total'] = $order_details['different_charges'][$charge_key]['gross_total']-$get_net_total;
					}
					// echo "<pre>";print_r($order_details);echo"</pre><hr>";exit;
					$this->session->set_userdata('order_details', $order_details);
				break;	

				case 'delete_charge':

					$charge_key = $this->input->post('charge_id');
					$session_data = $this->session->userdata('order_details');
					if(array_key_exists($charge_key, $session_data['different_charges'])) {

 						$charge_data_of_delete = $session_data['different_charges'][$charge_key];
 						$gross_total = 0;
						$current_charge_value = 0;
						$gross_total_changed_flag = false;
 						foreach ($session_data['different_charges'] as $different_charges_key => $different_charges_value) {
 							
 							if($different_charges_key == $charge_key) {
								if($charge_data_of_delete['add_minus_charges_value'] == 'Added') {

									$current_charge_value = ($charge_data_of_delete['gross_total']- ($gross_total + $session_data['net_total']));
									$session_data['gross_total'] = $session_data['gross_total'] - $current_charge_value;
								}else if($charge_data_of_delete['add_minus_charges_value'] == 'Deducted') {
									
									$current_charge_value = (($gross_total + $session_data['net_total']) - $charge_data_of_delete['gross_total']);
									$session_data['gross_total'] = $session_data['gross_total'] + $current_charge_value;
								}
 							}else{
 								if($current_charge_value == 0) {

									if($different_charges_value['add_minus_charges_value'] == 'Added') {

		 								$gross_total += ($different_charges_value['gross_total']- ($gross_total + $session_data['net_total']));
									}else if($different_charges_value['add_minus_charges_value'] == 'Deducted') {

										// echo $gross_total,"<hr>";
		 								$gross_total -= (($gross_total + $session_data['net_total']) -$different_charges_value['gross_total']);
										// echo $gross_total;die;
									}
 								} else {

 									if($charge_data_of_delete['add_minus_charges_value'] == 'Added') {

	 									$session_data['different_charges'][$different_charges_key]['gross_total'] -= $current_charge_value;		
									}else if($charge_data_of_delete['add_minus_charges_value'] == 'Deducted') {

	 									$session_data['different_charges'][$different_charges_key]['gross_total'] += $current_charge_value;	
									}
 								}
 							}
 						}
						unset($session_data['different_charges'][$charge_key]);
						$this->session->set_userdata('order_details',$session_data);
					}
	 			break;

	 			case 'update_rating':

					$form_data = array_column($this->input->post('rating_value'), 'value', 'name');
					// echo "<pre>";print_r($form_data);echo"</pre><hr>";
					foreach ($form_data as $form_key => $form_value) {
						
						$explode = explode('_', $form_key);
						if(count($explode) == 4) {
							
							if(empty($form_data['update_rfq_priority_reason_'.$explode[3]])){

								$response['status'] = 'failed';
							} else {
								
								// getting quotation details
								$quote_details = $this->common_model->get_dynamic_data_sales_db('quotation_mst_id, rfq_id', array('rfq_id'=>$explode[3]), 'quotation_mst', 'row_array');

								$rfq_details = $this->common_model->get_dynamic_data_sales_db('rfq_mst_id', array('rfq_mst_id'=>$explode[3]), 'rfq_mst', 'row_array');
								// echo "<pre>";print_r($quote_details);echo"</pre><hr>";exit;
								if(!empty($quote_details) || !empty($rfq_details)){

									$this->common_model->update_data_sales_db('quotation_mst', array('quotation_priority' => (int)$form_value), array('quotation_mst_id'=>$quote_details['quotation_mst_id']));

									$this->common_model->update_data_sales_db('rfq_mst', array('priority' => (int)$form_value), array('rfq_mst_id'=>$rfq_details['rfq_mst_id']));
								}else{

									$response['status'] = 'failed';
								}
							}
							
						} elseif(count($explode) == 5) {

							// getting quotation details
							$quote_details = $this->common_model->get_dynamic_data_sales_db('quotation_mst_id, rfq_id', array('rfq_id'=>$explode[4]), 'quotation_mst', 'row_array');

							$rfq_details = $this->common_model->get_dynamic_data_sales_db('rfq_mst_id', array('rfq_mst_id'=>$explode[4]), 'rfq_mst', 'row_array');

							if(!empty($quote_details) || !empty($rfq_details)){

								$this->common_model->update_data_sales_db('quotation_mst', array('priority_reason' => $form_value), array('quotation_mst_id'=>$quote_details['quotation_mst_id']));

								$this->common_model->update_data_sales_db('rfq_mst', array('priority_reason' => $form_value), array('rfq_mst_id'=>$rfq_details['rfq_mst_id']));
							}else{

								$response['status'] = 'failed';
							}
						}
					}
				break;	

				case 'quotation_made_by_high_chart':

				    $where_string = '';
				    $where_string = "made_by != ''";
				    $filter_date = explode(' - ', $this->input->post('filter_date'));
				    $where_string .= " AND entered_on >= '".date('Y-m-d 00:00:00', strtotime($filter_date[0]))."'";
				    if(count($filter_date) > 1) {

				        $where_string .= " AND entered_on <= '".date('Y-m-d 23:59:59', strtotime($filter_date[1]))."'";
				    }else {

				        $where_string .= " AND entered_on <= '".date('Y-m-d 23:59:59', strtotime($filter_date[0]))."'";
				    }
				    // echo "<pre>";print_r($where_string);echo"</pre><hr>";exit;
				    $all_user_details = array_column($this->common_model->get_dynamic_data_sales_db('*',array(), 'users'),'name', 'user_id');
				   
				    $quotation_made_by = $this->procurement_model->get_quotation_proforma_highchart_data($where_string);
				    // echo "<pre>";print_r($quotation_made_by);echo"</pre><hr>";die();
				    foreach ($quotation_made_by as $quotation_made_value) {

				        $response['quotation_made_by_highchart'][] = array(
				                                                        'name' => $all_user_details[$quotation_made_value['made_by']],
				                                                        'y' => (int)$quotation_made_value['count'],
				                                                        'drilldown' => $all_user_details[$quotation_made_value['made_by']]);
				    }
				    //echo "<pre>";print_r($response);echo"</pre><hr>";exit;
				break;

				case 'procurement_status_highchart':
				       
			        $procurement_details=$this->procurement_model->get_procurement_list($this->input->post('person_type'));
					$response['procurement_stage_highchart_data'][0]['data']=array();
					$i=0;
					$response['procurement_stage_highchart_data'][0] = array();
					$response['procurement_stage_highchart_data'][0]['name'] = 'Issued';
					$response['procurement_stage_highchart_data'][0]['color'] = '#28a745';
					$response['procurement_stage_highchart_data'][1] = array();
					$response['procurement_stage_highchart_data'][1]['name'] = 'InProduction';
					$response['procurement_stage_highchart_data'][1]['color'] = '#007bff';
					$response['procurement_stage_highchart_data'][2] = array();
					$response['procurement_stage_highchart_data'][2]['name'] = 'Delivered';
					$response['procurement_stage_highchart_data'][2]['color'] = '#dc3545';
					$response['procurement_stage_highchart_data'][3] = array();
					$response['procurement_stage_highchart_data'][3]['name'] = 'Inspected';
					$response['procurement_stage_highchart_data'][3]['color'] = '#ffb822';
					foreach ($procurement_details as $key => $procurement_list_details) {						
						$response['procurement_stage_highchart_category'][] = $procurement_list_details[$this->input->post('person_type')];
						$response['procurement_stage_highchart_data'][0]['data'][] = (int)$procurement_list_details['Issued'];
						$response['procurement_stage_highchart_data'][1]['data'][] = (int)$procurement_list_details['InProduction'];
						$response['procurement_stage_highchart_data'][2]['data'][] = (int)$procurement_list_details['Delivered'];
						$response['procurement_stage_highchart_data'][3]['data'][] = (int)$procurement_list_details['Inspected'];
					}


				break;

				case 'rfq_list_data':
					
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";die;
					if(!empty($this->input->post('client_type'))){

						$this->session->set_userdata(array('rfq_client_type'=> $this->input->post('client_type')));
					}
					if(!empty($this->input->post('graph_type'))){

						$this->session->set_userdata(array('rfq_graph_type'=> $this->input->post('graph_type')));
					}
					if(in_array($this->session->userdata('role'), array(1, 4, 5, 6, 8, 16, 17, 18))){

						$user_details = array_column($this->common_model->get_dynamic_data_sales_db('*', array(), 'users'), 'name', 'user_id');
						$limit  = (!empty($this->input->post('limit')))?$this->input->post('limit'):10;
						$offset  = (!empty($this->input->post('offset')))?$this->input->post('offset'):00;
						$where_string = $this->prepare_rfq_list_where($this->input->post('search_form_data'));

						$data = $this->prepare_rfq_list_data($where_string, array('column_name'=> 'rfq_mst_id', 'column_value'=> 'desc'), $limit, $offset);

						$data['search_filter_form_data'] = $this->prepare_search_filter_data($this->input->post('search_form_data'));

						// echo "<pre>";print_r($data['search_filter_form_data']);echo"</pre><hr>";die;
						$response['rfq_list_search_filter'] = $this->load->view('procurement/rfq_list_search_filter', $data, true);
						$response['rfq_list_body'] = $this->load->view('procurement/rfq_index_body', $data, true);
						$response['rfq_list_paggination'] = $this->load->view('procurement/rfq_index_paggination', $data, true);

						if($this->session->userdata('rfq_graph_type') == 'sales'){

							$response['rfq_list_highchart_category'] = array();
							$response['rfq_list_highchart_series'] = array();
							$sales_count_map = $user_sales_data = $series_data = array();

							$sales_rfq_count_list = $this->procurement_model->get_sales_person_highchart_data($where_string);
							$sales_user_list = array_column($this->common_model->get_all_data('user_id, name', "status=1 AND role IN (5, 16) AND user_id not IN (125)", 'users'),  'name', 'user_id');

							foreach ($sales_rfq_count_list as $single_sales_rfq) {

								$sales_count_map[$single_sales_rfq['rfq_sentby']] = $single_sales_rfq['count'];
							}
							foreach ($sales_user_list as $user_id => $user_name) {
								$user_sales_data[] = [
									'name' => $user_name,
									'count' => isset($sales_count_map[$user_id]) ? (int) $sales_count_map[$user_id] : 0,
								];
							}

							usort($user_sales_data, function($a, $b) {
								return $b['count'] - $a['count'];
							});

							foreach ($user_sales_data as $user_data) {
								$response['rfq_list_highchart_category'][] = $user_data['name'];
								$series_data[] = $user_data['count'];
							}
							$response['rfq_list_highchart_series'][] = [
								'name' => 'Sales Count',
								'data' => $series_data
							];

						}else if($this->session->userdata('rfq_graph_type') == 'procurement'){

							// high chart data start
							$highchart_data = $this->procurement_model->get_rfq_list_highchart_data($where_string);
							$rfq_importance = $rfq_sentby = $assigned_to = $rfq_status = array();
							if(!empty($this->input->post('search_form_data'))) {

								foreach ($this->input->post('search_form_data') as $single_form_field) {

									if(!empty($single_form_field['value'])) {

										switch ($single_form_field['name']) {

											case 'assigned_to':

												$assigned_to[] = $single_form_field['value'];
											break;
											case 'procurement_person_status':

												$procurement_person_status[] = $single_form_field['value'];
											break;
										}
									}
								}
							}
							$category = $series = array();
							foreach ($highchart_data as $procurement_assign_details) {

								// echo "<pre>";print_r($procurement_assign_details);echo"</pre><hr>";
								if(!empty($assigned_to)) {

									for($i = 1; $i<4; $i++){

										if($procurement_assign_details['assigned_to_'.$i] != 0 && in_array($procurement_assign_details['assigned_to_'.$i], $assigned_to)) {

											if(empty($category[$procurement_assign_details['assigned_to_'.$i]])) {

												$category[$procurement_assign_details['assigned_to_'.$i]] = 0;
												$series[$procurement_assign_details['assigned_to_'.$i]] = array('waiting'=>0, 'pending'=>0, 'query'=>0, 'regret'=>0, 'done'=>0);
											}
											$category[$procurement_assign_details['assigned_to_'.$i]] += 1;
											$series[$procurement_assign_details['assigned_to_'.$i]][$procurement_assign_details['status_'.$i]] += 1;
										}
									}

								}else if(!empty($procurement_person_status)){

									for($i = 1; $i<4; $i++){

										if($procurement_assign_details['assigned_to_'.$i] != 0 && in_array($procurement_assign_details['status_'.$i], $procurement_person_status)) {

											if(empty($category[$procurement_assign_details['assigned_to_'.$i]])) {

												$category[$procurement_assign_details['assigned_to_'.$i]] = 0;
												$series[$procurement_assign_details['assigned_to_'.$i]] = array('waiting'=>0, 'pending'=>0, 'query'=>0, 'regret'=>0, 'done'=>0);
											}
											$category[$procurement_assign_details['assigned_to_'.$i]] += 1;
											$series[$procurement_assign_details['assigned_to_'.$i]][$procurement_assign_details['status_'.$i]] += 1;
										}
									}
								}else{

									for($i = 1; $i<4; $i++){

										if($procurement_assign_details['assigned_to_'.$i] != 0) {

											if(empty($category[$procurement_assign_details['assigned_to_'.$i]])) {

												$category[$procurement_assign_details['assigned_to_'.$i]] = 0;
												$series[$procurement_assign_details['assigned_to_'.$i]] = array('waiting'=>0, 'pending'=>0, 'query'=>0, 'regret'=>0, 'done'=>0);
											}
											$category[$procurement_assign_details['assigned_to_'.$i]] += 1;
											$series[$procurement_assign_details['assigned_to_'.$i]][$procurement_assign_details['status_'.$i]] += 1;
										}
									}
								}
							}
							$response['rfq_list_highchart_series'] = array(
																		array(
																			'name'=> 'waiting',
																			'color'=> 'DarkOrange',
																			'data'=> array()
																		),
																		array(
																			'name'=> 'pending',
																			'color'=> '#f3b712',
																			'data'=> array()
																		),
																		array(
																			'name'=> 'query',
																			'color'=> '#ff0000',
																			'data'=> array()
																		),
																		array(
																			'name'=> 'regret',
																			'color'=> '#000000',
																			'data'=> array()
																		),
																		array(
																			'name'=> 'done',
																			'color'=> '#0fbd83',
																			'data'=> array()
																		)
																	);
							arsort($category);
							foreach ($category as $procurement_user_id => $count) {

								if(!empty($user_details[$procurement_user_id])) {
									$response['rfq_list_highchart_category'][] = $user_details[$procurement_user_id];
									array_push($response['rfq_list_highchart_series'][0]['data'], (int)$series[$procurement_user_id]['waiting']);
									array_push($response['rfq_list_highchart_series'][1]['data'], (int)$series[$procurement_user_id]['pending']);
									array_push($response['rfq_list_highchart_series'][2]['data'], (int)$series[$procurement_user_id]['query']);
									array_push($response['rfq_list_highchart_series'][3]['data'], (int)$series[$procurement_user_id]['regret']);
									array_push($response['rfq_list_highchart_series'][4]['data'], (int)$series[$procurement_user_id]['done']);
								}
							}
						}
						// high chart data end
						// pie chart data start
						$response['total_product_family'] = 0;
						$piechart_data = $this->procurement_model->get_rfq_list_piechart_data($where_string);
						$temp_data = array();
						foreach ($piechart_data as $single_piechart_data) {

							$order_data = $this->procurement_model->get_rfq_same_order_data($where_string, $single_piechart_data['product_family']);

							$temp_data[$single_piechart_data['product_family']]['family_count'] = $single_piechart_data['count'];
							$temp_data[$single_piechart_data['product_family']]['order_count'] = $order_data['order_count'];
						}
						// echo "<pre>";print_r($temp_data);echo"</pre><hr>";die;

						$sort_temp_data = $temp_data;
						$start_count = 0;
						foreach ($sort_temp_data as $name => $single_sort_data) {

							$response['rfq_list_piechart_data'][$start_count]['name'] = $name;
							$response['rfq_list_piechart_data'][$start_count]['family_count'] = (int)$single_sort_data['family_count'] - $single_sort_data['order_count'];
							$response['rfq_list_piechart_data'][$start_count]['order_count'] = (int)$single_sort_data['order_count'];

							$response['total_product_family'] += (int)$single_sort_data['family_count'] - $single_sort_data['order_count'];
							$start_count++;
						}
						// echo "<pre>";print_r($response);echo"</pre><hr>";die;
					}
				break;

				case 'save_purchase_order_via_production':
					
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";die;
					$response['message'] = "Procurement Data are added";
					$form_data = array();
					$form_data['person_details'] = array_column($this->input->post('person_details'), 'value', 'name');
					// echo "<pre>";print_r($form_data);echo"</pre><hr>";die;
					$form_data['terms_conditon'] = array_column($this->input->post('terms_conditon'), 'value', 'name');
					$form_data['product_details'] = array_column($this->input->post('product_details'), 'value', 'name');
					// echo "<pre>";print_r($form_data);echo"</pre><hr>";
					// preparing procurement person information
						$insert_array = array_merge($form_data['person_details'], $form_data['terms_conditon']);
						$insert_array['term_condition_text'] = $this->input->post('term_condition_text');
						// $last_quote_id = $this->procurement_model->get_all_data('last_quote_id', array('status'=> "Active", 'quote_name'=>'procurement'), 'last_quote_created_number', 'row_array');
						// $next_quote_id = ((int)$last_quote_id['last_quote_id'])+1;
						// $next_quote_id = (string)$next_quote_id;
						// $next_quote_id = $next_quote_id;
						// $insert_array['procurement_no'] = 'OM/'.$next_quote_id.'/21-22';
						$insert_array['procurement_no'] = $this->get_quote_no($form_data['person_details']['po_type']);
						// OM/001/21-22
						$insert_array['net_total'] = $this->session->userdata('order_details')['net_total'];
						$insert_array['gross_total'] = $this->session->userdata('order_details')['gross_total'];
						// echo "<pre>";print_r($insert_array);echo"</pre><hr>";die;
						// $procurement_id = 1001;
						$procurement_id = $this->procurement_model->insertData('procurement', $insert_array);
						// $last_quote_id = $this->procurement_model->updateData('last_quote_created_number', array('last_quote_id'=>$next_quote_id), array('status'=> "Active", 'quote_name'=>'procurement'));
					// preparing product information 

						// echo "<pre>";print_r($this->input->post('product_details'));echo"</pre><hr>";
						$i = 0;
						foreach ($this->input->post('product_details') as $product_details_key => $product_details_single) {
							
							if(($product_details_key%8) == 0){
								
								$product_insert_array[$i]['procurement_id'] = $procurement_id;
								$product_insert_array[$i]['product_name'] = $product_details_single['value'];
							}else if(($product_details_key%8) == 1){

								$product_insert_array[$i]['material_name'] = $product_details_single['value'];
							}else if(($product_details_key%8) == 2){

								$product_insert_array[$i]['description_name'] = $product_details_single['value'];
							}else if(($product_details_key%8) == 3){

								$product_insert_array[$i]['quantity'] = $product_details_single['value'];
							}else if(($product_details_key%8) == 4){

								$product_insert_array[$i]['units'] = $product_details_single['value'];
							}else if(($product_details_key%8) == 5){

								$product_insert_array[$i]['price'] = $product_details_single['value'];
							}else if(($product_details_key%8) == 6){

								$product_insert_array[$i]['per_unit'] = $product_details_single['value'];
							}else if(($product_details_key%8) == 7){

								$product_insert_array[$i]['total'] = $product_details_single['value'];
								$i = $i + 1;
							}
						}
						// echo "<pre>";print_r($product_insert_array);echo"</pre><hr>";die;
						if(!empty($product_insert_array)) {

							$this->procurement_model->insert_data_batch('procurement_product_information', $product_insert_array);
						}
					// preparing charges information
						$charges_insert_array = array();
						if(!empty($this->session->userdata('order_details')['different_charges'])) {

							foreach ($this->session->userdata('order_details')['different_charges'] as $charge_key => $charges_details) {
								
								$charges_insert_array[$charge_key]['procurement_id'] = $procurement_id; 
								$charges_insert_array[$charge_key]['charge_name'] = $charges_details['charges_name']; 
								$charges_insert_array[$charge_key]['charge_value'] = $charges_details['charge_value']; 
								$charges_insert_array[$charge_key]['charge_deducted_or_added'] = $charges_details['add_minus_charges_value']; 
								$charges_insert_array[$charge_key]['gross_total_after_charge'] = $charges_details['gross_total']; 
							}
							// echo "<pre>";print_r($charges_insert_array);echo"</pre><hr>";exit;
							$this->procurement_model->insert_data_batch('procurement_charges_information', $charges_insert_array);
						}
						// die('debug');
				break;

				case 'save_vendor_details':

					$batch_insert_array =  array();
					$vendor_data = $this->input->post('vendor_data');
					$quotation_id_data = $this->input->post('quotation_id_data');
					$rfq_id = $this->input->post('rfq_id');
					if(!empty($rfq_id) && !empty($quotation_id_data) && !empty($vendor_data)){

						foreach ($quotation_id_data as $quotation_id) {

							foreach ($vendor_data as $vendor_details) {

								$batch_insert_array[] = array(
															'rfq_id' => $rfq_id,
															'vendor_id' => $vendor_details['value'],
															'quotation_mst_id' => $quotation_id,
															'entered_on' => date('Y-m-d H:i:s'),
															'entered_by' => $this->session->userdata('user_id'),
															'modified_on' => date('Y-m-d H:i:s'),
															'modified_by' => $this->session->userdata('user_id'),
														);
							}
						}
						if(!empty($batch_insert_array)){

							// echo "<pre>";print_r($batch_insert_array);echo"</pre><hr>";die;
							$this->common_model->insert_data_sales_db('rfq_to_vendor', $batch_insert_array, 'batch');
						}
					}
				break;

				case 'get_rfq_assign_to_vendor_details':
					
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";die;
					$rfq_id = $this->input->post('rfq_id');
					$response['rfq_to_vendor_body'] = '';
					if(!empty($rfq_id)) {
						
						$rfq_to_vendor = $this->common_model->get_dynamic_data_sales_db('*', array('rfq_id'=> $rfq_id), 'rfq_to_vendor');
						// echo "<pre>";print_r($rfq_to_vendor);echo"</pre><hr>";die;
						$data['rfq_to_vendor_main'] = array();
						$data['rfq_to_vendor_sub'] = array();
						// $data['lookup_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array(), 'lookup'), 'lookup_value', 'lookup_id');
						$data['product_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array(), 'product_mst'), 'name', 'id');
						$data['material_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array(), 'material_mst'), 'name', 'id');
						$data['units_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'), 'units'), 'unit_value','unit_id');
						if(!empty($rfq_to_vendor)){

							$static_array_for_evaluate = array(''=>'', 'h'=>'<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill kt-badge--rounded">High</span>', 'l'=>'<span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill kt-badge--rounded">Low</span>');
							$i = 0;
							foreach ($rfq_to_vendor as $rfq_to_vendor_values) {

								$rfq_to_vendor_sub = array();
								if(!array_key_exists($rfq_to_vendor_values['vendor_id'], $data['rfq_to_vendor_sub'])){

									$data['rfq_to_vendor_main'][$i]['connect_id'] = $rfq_to_vendor_values['connect_id'];
									$data['rfq_to_vendor_main'][$i]['rfq_id'] = $rfq_to_vendor_values['rfq_id'];
									$data['rfq_to_vendor_main'][$i]['vendor_id'] = $rfq_to_vendor_values['vendor_id'];
									$data['rfq_to_vendor_main'][$i]['quotation_mst_id'] = $rfq_to_vendor_values['quotation_mst_id'];
									$data['rfq_to_vendor_main'][$i]['vendor_status'] = $rfq_to_vendor_values['vendor_status'];
									$data['rfq_to_vendor_main'][$i]['evaluate_price'] = $static_array_for_evaluate[$rfq_to_vendor_values['evaluate_price']];
									$data['rfq_to_vendor_main'][$i]['evaluate_delivery'] = $static_array_for_evaluate[$rfq_to_vendor_values['evaluate_delivery']];
									$data['rfq_to_vendor_main'][$i]['vendor_name'] = $this->common_model->get_dynamic_data_sales_db('vendor_name', array('vendor_id'=> $rfq_to_vendor_values['vendor_id']), 'vendors', 'row_array')['vendor_name'];
									$i++;
								}
								if(!empty($rfq_to_vendor_values['quotation_mst_id'])){

									$rfq_to_vendor_sub = $this->common_model->get_dynamic_data_sales_db('*', array('quotation_dtl_id'=> $rfq_to_vendor_values['quotation_mst_id']), 'quotation_dtl','row_array');
								}
								$data['rfq_to_vendor_sub'][$rfq_to_vendor_values['vendor_id']][] = $rfq_to_vendor_sub;
							}
						}
						
						$response['rfq_to_vendor_body'] = $this->load->view('procurement/vendor_assgin_to_rfq_body', $data, true);
					}
				break;

				case 'get_data_for_update_rfq_assign_to_vendor':
						
					$rfq_id = $this->input->post('rfq_id');
					$vendor_id = $this->input->post('vendor_id');
					$response['update_form'] = '';
					if(!empty($rfq_id)) {		

						$data['rfq_to_vendor'] = array();
						$quotation_dtl_ids = array();
						if(!empty($vendor_id)){

							$data['rfq_to_vendor'] = $this->common_model->get_dynamic_data_sales_db('*', array('rfq_id'=> $rfq_id, 'vendor_id'=> $vendor_id), 'rfq_to_vendor');
							$quotation_dtl_ids = array_column($data['rfq_to_vendor'], 'quotation_mst_id');
						}
						// $data['lookup_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'), 'lookup'), 'lookup_value','lookup_id');
						$data['product_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'), 'product_mst'), 'name','id');
						$data['material_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'), 'material_mst'), 'name','id');


						$data['units_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'), 'units'), 'unit_value','unit_id');
						$data['vendor_list'] = array();
						$quotation_product_details = array();
						$quotation_details = $this->common_model->get_dynamic_data_sales_db('*', array('rfq_id'=> $rfq_id), 'quotation_mst', 'row_array');
						if(!empty($quotation_details)){

							$quotation_product_details = $this->common_model->get_dynamic_data_sales_db('*', array('quotation_mst_id'=> $quotation_details['quotation_mst_id']), 'quotation_dtl');
							foreach ($quotation_product_details as $quotation_product_key => $quotation_product_value) {

								$data['quotation_details'][$quotation_product_key]['product_name'] = $data['product_details'][$quotation_product_value['product_id']]; 
								$data['quotation_details'][$quotation_product_key]['material_name'] = $data['material_details'][$quotation_product_value['material_id']];
								$data['quotation_details'][$quotation_product_key]['description'] = $quotation_product_value['description'];
								$data['quotation_details'][$quotation_product_key]['quantity'] = $quotation_product_value['quantity'];
								$data['quotation_details'][$quotation_product_key]['id'] = $quotation_product_value['quotation_dtl_id'];
								$data['quotation_details'][$quotation_product_key]['vendor_id'] = $quotation_product_value['vendor_id'];
								$data['quotation_details'][$quotation_product_key]['units'] = $data['units_details'][$quotation_product_value['unit']];
								$data['quotation_details'][$quotation_product_key]['already_assign'] = false;
								$data['quotation_details'][$quotation_product_key]['tr_background'] = '';
								if(in_array($quotation_product_value['quotation_dtl_id'], $quotation_dtl_ids)){

									$data['quotation_details'][$quotation_product_key]['already_assign'] = true;
									$data['quotation_details'][$quotation_product_key]['tr_background'] = 'lightgray';
								}
								$product_list[] = $data['product_details'][$quotation_product_value['product_id']];
								$material_list[] = $data['material_details'][$quotation_product_value['material_id']];
							}
							$data['vendor_list'] = $this->procurement_model->get_vendor_name_on_product_name_and_material_name(array_unique($product_list), array_unique($material_list));
						}
						$data['count'] = count($quotation_dtl_ids);
						$response['update_form'] = $this->load->view('procurement/add_vendor_to_rfq_form', $data, true);
					}
				break;

				case 'update_vendor_details':
					
					$batch_insert_array = array();	
					$vendor_data = array_column($this->input->post('vendor_data'),'value','name');
					$quotation_id_data = $this->input->post('quotation_id_data');
					if(!empty($vendor_data) && !empty($quotation_id_data)){

						$this->procurement_model->deleteData('rfq_to_vendor', array('rfq_id'=>$vendor_data['rfq_id'], 'vendor_id'=>$vendor_data['vendor_id']));
						foreach ($quotation_id_data as $quotation_id) {
							
							$batch_insert_array[] = array(
														'rfq_id' => $vendor_data['rfq_id'],
														'vendor_id' => $vendor_data['vendor_id'],
														'quotation_mst_id' => $quotation_id,
														'vendor_status' => $vendor_data['vendor_status'],
														'evaluate_price' => $vendor_data['evaluate_price'],
														'evaluate_delivery' => $vendor_data['evaluate_delivery'],
														'entered_on' => date('Y-m-d H:i:s'),
														'entered_by' => $this->session->userdata('user_id'),
														'modified_on' => date('Y-m-d H:i:s'),
														'modified_by' => $this->session->userdata('user_id'),
													);
						}
						if(!empty($batch_insert_array)){

							// echo "<pre>";print_r($batch_insert_array);echo"</pre><hr>";die;
							$this->common_model->insert_data_sales_db('rfq_to_vendor', $batch_insert_array, 'batch');
						}
					}
				break;

				case 'save_chat_reply':
					
					if(!empty($this->input->post('rfq_mst_id')) && !empty($this->input->post('message'))){

						$chat_details = array();
						$old_chat_details = $this->common_model->get_dynamic_data_sales_db('chat_info', array('rfq_mst_id'=> $this->input->post('rfq_mst_id')), 'rfq_mst', 'row_array');
						if(!empty($old_chat_details)){

							$chat_details = json_decode($old_chat_details['chat_info'], true);
						}
						// echo "<pre>";print_r($chat_details);echo"</pre><hr>";
						$chat_details[] = array(
											'message'=> $this->input->post('message'),
											'user_id'=> $this->session->userdata('user_id'),
											'date_time'=> date("Y-m-d\TH:i:s\Z"),
										);
						if(!empty($chat_details)){
							// echo "<pre>";print_r(json_encode($chat_details));echo"</pre><hr>";exit;
							$this->procurement_model->updateData('rfq_mst', array('chat_info'=> json_encode($chat_details)), array('rfq_mst_id'=> $this->input->post('rfq_mst_id')));
						}
						// echo "<pre>";print_r(json_encode($chat_details));echo"</pre><hr>";exit;
					}
				break;

				case 'get_chat_conversation':
					
					$response['chat_history'] = '';
					if(!empty($this->input->post('rfq_mst_id'))){
						
						$chat_details = $this->common_model->get_dynamic_data_sales_db('rfq_no, chat_info', array('rfq_mst_id'=> $this->input->post('rfq_mst_id')), 'rfq_mst', 'row_array');
						// echo "<pre>";print_r($chat_details);echo"</pre><hr>";exit;
						if(!empty($chat_details)){
							
							$data['chat_details'] = array();
							$user_details = $this->common_model->get_all_conditional_data_sales_db(
								'users.user_id, users.name, people_information.profile_pic_file_path',
								array(),
								'users',
								'result_array',
								array(
									'table_name'=> 'people_information',
									'condition'=> 'people_information.user_id = users.user_id',
									'type'=> 'LEFT'
								)
							);
							foreach ($user_details as $single_user_details) {
								
								$user_information[$single_user_details['user_id']]['name'] = $single_user_details['name'];
								$user_information[$single_user_details['user_id']]['profile_pic_file_path'] = $single_user_details['profile_pic_file_path'];
							}
							// echo "<pre>";print_r($user_information);echo"</pre><hr>";
							foreach (json_decode($chat_details['chat_info'], true) as $chat_key => $single_user_details) {
								
								// echo "<pre>";print_r($single_user_details);echo"</pre><hr>";exit;
								$data['chat_details'][$chat_key]['profile_path'] = "assets/media/users/default.jpg";
								if(!empty($user_information[$single_user_details['user_id']]['profile_pic_file_path'])){

									$data['chat_details'][$chat_key]['profile_path'] = "assets/hr_document/profile_pic/".$user_information[$single_user_details['user_id']]['profile_pic_file_path'];
								}
								$data['chat_details'][$chat_key]['user_name'] = $user_information[$single_user_details['user_id']]['name'];
								$data['chat_details'][$chat_key]['tat'] = $this->creat_tat($single_user_details['date_time']);
								$data['chat_details'][$chat_key]['message'] = $single_user_details['message'];
							}

							// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
							$response['chat_history'] = $this->load->view('chat_history_procurement_person', $data, true);
							$response['rfq_no'] = $chat_details['rfq_no'];
						}
					}
				break;
		 		
				case 'rfq_file_upload':

					$response['msg'] = 'File is uploaded successfully!!!';
					$return_response['status'] = '';
					$rfq_mst_id = $this->session->userdata('rfq_mst_id');
					if(!empty($rfq_mst_id)){

						$return_response = $this->upload_doc_config();
					}
					$response['status'] = $return_response['status'];
					if($return_response['status'] == 'successful') {

						$rfq_file_path = array();
						$rfq_document_path = array();
						$rfq_details = $this->common_model->get_dynamic_data_sales_db('file_path_and_type, document_path_and_type',array('rfq_mst_id'=> $rfq_mst_id), 'rfq_mst', 'row_array');

						if($this->input->post('type') == 'files'){

							$rfq_file_path = json_decode($rfq_details['file_path_and_type'], true);
							$rfq_file_path[] = array('file_type' => $return_response['file_type'], 'file_name' => $return_response['file_name'], 'file_add_date' => $return_response['file_add_date']);

							$this->common_model->update_data_sales_db('rfq_mst', array('file_path_and_type'=> json_encode($rfq_file_path)), array('rfq_mst_id'=>$rfq_mst_id));
						}else{

							$rfq_document_path = json_decode($rfq_details['document_path_and_type'], true);
							$rfq_document_path[] = array('file_type' => $return_response['file_type'], 'file_name' => $return_response['file_name'], 'file_add_date' => $return_response['file_add_date']);

							$this->common_model->update_data_sales_db('rfq_mst', array('document_path_and_type'=> json_encode($rfq_document_path)), array('rfq_mst_id'=>$rfq_mst_id));
						}
						// echo "<pre>";print_r($rfq_file_path);echo"</pre><hr>";exit;
					}else {

						$response['msg'] = 'File is not uploaded.';
					}
				break;

				case 'get_rfq_file_history':

					$rfq_mst_id = (!empty($this->input->post('rfq_mst_id'))) ? $this->input->post('rfq_mst_id') : $this->session->userdata('rfq_mst_id');
					if(!empty($rfq_mst_id)) {

						$rfq_files_details = $this->common_model->get_dynamic_data_sales_db('rfq_mst_id, file_path_and_type',array('rfq_mst_id'=> $rfq_mst_id), 'rfq_mst', 'row_array');

						$rfq_documents_details = $this->common_model->get_dynamic_data_sales_db('rfq_mst_id, document_path_and_type',array('rfq_mst_id'=> $rfq_mst_id), 'rfq_mst', 'row_array');

						if(!empty($rfq_files_details) || !empty($rfq_documents_details)){

							$response['pdf_upload_history_rfq_file'] = $this->load->view('procurement/pdf_table', array('pdf_details'=> $rfq_files_details), true);
							$response['pdf_upload_history_rfq_document'] = $this->load->view('procurement/pdf_table', array('pdf_details'=> $rfq_documents_details), true);
						}
						$this->session->set_userdata('rfq_mst_id', $rfq_mst_id);
					}
				break;

				case 'delete_rfq_file':
				case 'delete_rfq_technical_document':

					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";die;
					if(!empty($this->input->post('rfq_mst_id')) && !empty($this->input->post('file_name'))){

						if($this->input->post('file_type') == 'file'){

							$column_name = 'file_path_and_type';
						}else if($this->input->post('file_type') == 'technical_document'){

							$column_name = 'document_path_and_type';
						}

						$file_details = $this->common_model->get_dynamic_data_sales_db($column_name, array('rfq_mst_id'=> $this->input->post('rfq_mst_id')), 'rfq_mst', 'row_array');

						if(!empty($file_details)){

							$arrayData = json_decode($file_details[$column_name], true);
							foreach($arrayData as $single_array){

								if($single_array['file_name'] !== $this->input->post('file_name')){

									$filter_array[] = $single_array;
								}
							}

							if(empty($filter_array)){

								$this->procurement_model->updateData('rfq_mst', array($column_name => ''), array('rfq_mst_id'=> $this->input->post('rfq_mst_id')));
							}else{

								$this->procurement_model->updateData('rfq_mst', array($column_name => json_encode($filter_array)), array('rfq_mst_id'=> $this->input->post('rfq_mst_id')));
							}
						}
					}
				break;

				case 'get_vendor_po_footer':

					// echo "<pre>";print_r($this->input->post('vendor_po_type'));echo"</pre><hr>";exit;
					$response['footer_text'] = '';
					if(!empty($this->input->post('vendor_po_type'))){

						$footer_text = $this->procurement_model->get_all_data('footer_text', array('status'=> 'Active', 'client_type'=>$this->input->post('vendor_po_type')), 'pdf_footer', 'row_array');

						if(!empty($footer_text)){

							$response['footer_text'] = $footer_text['footer_text'];
						}
					}
				break;
				default:
		 			$response['status'] = 'failed';	
		 			$response['message'] = "call type not found";
	 			break;
		 	}
		 	echo json_encode($response);

	       }else{

		 	die('access is not allowed to this function');
		 }
	}

	private function get_quote_no($client_type = 'om', $quote_type = 'purchase_order'){
		
		$year = date('y').'-'.(date('y') + 1);
		if(date('m') <= 3){

			$year = (date('y')-1)."-".date('y');
		}
		$last_query_id = $this->common_model->get_dynamic_data_sales_db('last_quote_id', array('status'=> "Active", 'module_name'=> 'procurement', 'sub_module_name'=> $quote_type, 'sub_module_type'=> 'om'), 'last_quote_created_number', 'row_array');
		$no = (((int)$last_query_id['last_quote_id'])+1);
		$this->common_model->update_data_sales_db('last_quote_created_number', array('last_quote_id'=>(((int)$last_query_id['last_quote_id'])+1)), array('status'=> "Active", 'module_name'=> 'procurement', 'sub_module_name'=> $quote_type, 'sub_module_type'=> 'om'));
		
		return strtoupper($client_type).'/'.$no.'/'.$year;
	}

	private function all_filter_data($search_form_data, $order_by_array, $limit, $offset) {

		$where_array = $return_response = array();
		$form_data = array_column($search_form_data, 'value', 'name');
		$where_array['status'] = 'Active';
		foreach ($form_data as $key => $value) {
			if(!empty($value)) {

				$where_array[$key] = $value;
			}
		}
		$data = $this->create_procurement_data($where_array, $order_by_array, $limit, $offset);
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$return_response['procurement_body'] = $this->load->view('procurement/procurement_body', $data, true);
		$return_response['procurement_search_filter'] = $this->load->view('procurement/procurement_search_filter_form', $data, true);
		$return_response['procurement_sorting'] = $this->load->view('procurement/procurement_head', $data, true);
		$return_response['procurement_paggination'] = $this->load->view('procurement/procurement_paggination', $data, true);
		return $return_response;
	}

	private function create_procurement_data($where_array, $order_by, $limit, $offset) {

		$data = $this->procurement_model->get_procurement_listing_data($where_array, $order_by, $limit, $offset);

		$data['country_list'] = array_column($this->procurement_model->get_all_data('id, name', array('status'=> "Active"), 'country_mst'), 'name', 'id');

		$data['sales_person'] = $this->common_model->get_sales_person('name, user_id');
		$data['procurement_person'] = $this->common_model->get_procurement_person();
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$data['po_list'] = $this->common_model->get_dynamic_data_sales_db('id, procurement_no', array('status'=>'Active'), 'procurement');
		$data['vendor_list'] = $this->procurement_model->get_vendor_list();
		$data['search_filter'] = $this->set_search_filter_value($where_array);
		$data['sorting'] = $this->set_sorting_value($order_by);
		$data['paggination_data']['limit'] = $limit;
		$data['paggination_data']['offset'] = $offset;
		return $data;
	}

	private function set_search_filter_value($where_array) {

		$return_array['procuremet_person_name'] = (!empty($where_array['procurement_person'])) ? $where_array['procurement_person']: ''; 
		$return_array['sales_person_name'] = (!empty($where_array['sales_person'])) ? $where_array['sales_person']: ''; 
		$return_array['vendor_name'] = (!empty($where_array['vendor_name'])) ? $where_array['vendor_name']: ''; 
		$return_array['procurement_status'] = (!empty($where_array['procurement_status'])) ? $where_array['procurement_status']: '';
		$return_array['procurement_no'] = (!empty($where_array['procurement_no'])) ? $where_array['procurement_no']: ''; 
		return $return_array;
	}

	private function set_sorting_value($sort_array) {

		$sorting_static_array = array('id', 'procurement_no', 'procurement_person', 'sales_person', 'add_time', 'vendor_name', 'net_total');
		$sorting = array();
		foreach ($sorting_static_array as $sorting_static_value) {
			
			$sorting[strtolower($sorting_static_value)]['class_name'] = 'sorting';
			$sorting[strtolower($sorting_static_value)]['value'] = 'asc';
			if($sort_array['column_name'] == $sorting_static_value) {
				if($sort_array['column_value'] == 'asc') {

					$sorting[strtolower($sorting_static_value)]['class_name'] = 'sorting_asc';
					$sorting[strtolower($sorting_static_value)]['value'] = 'desc';
				} else {
					$sorting[strtolower($sorting_static_value)]['class_name'] = 'sorting_desc';
				}			
			}
		}
		return $sorting;
	}

	public function transfer_assign_to_into_assign_to_1(){


		$rfq_data = $this->common_model->get_dynamic_data_sales_db('*', array(), 'rfq_mst');
		foreach ($rfq_data as $single_rfq_data) {
			 	
			// echo $single_rfq_data['rfq_mst_id'],"<hr>";
			$this->common_model->update_data_sales_db('rfq_mst', array('assigned_to_1'=>$single_rfq_data['assigned_to'], 'status_1'=>$single_rfq_data['rfq_status']), array('rfq_mst_id'=>$single_rfq_data['rfq_mst_id']));
			// die;

		}
	}

	private function prepare_rfq_search_filter_data($search_filter_form_data){

		$return_data = array('sales_person'=> array(), 'procurement_person'=> array(), 'rfq_no'=> '', 'rfq_company'=> '', 'rfq_date'=> '', 'rfq_importance'=> array(), 'rfq_status'=> array(), 'quotation_no'=> '', 'quotation_status'=> array(), 'product_family'=> array(), 'priority'=>'', 'rfq_subject'=> '');
		$all_rfq_data = $this->common_model->get_dynamic_data_sales_db('*', array(), 'rfq_mst');
		$sales_user_details = array_column($this->common_model->get_all_data('*', "status=1 AND role IN (5, 16)", 'users'), 'name', 'user_id');
		$procurement_user_details = array_column($this->common_model->get_all_data('*', "status=1 AND role IN (6, 8) AND user_id NOT IN('148', '133', '73', '85', '86')", 'users'), 'name', 'user_id');
		$rfq_importance = $rfq_sentby = $assigned_to = $rfq_status = $quotation_status = $procurement_person_status = $product_family = $priority = array();

		if(!empty($search_filter_form_data)) {
			foreach ($search_filter_form_data as $single_form_field) {

				if(!empty($single_form_field['value'])) {
					
					switch ($single_form_field['name']) {
						case 'rfq_no':
						case 'rfq_company':
						case 'rfq_date':
						case 'quotation_no':
						case 'priority':
						case 'rfq_subject':

							$return_data[$single_form_field['name']] = $single_form_field['value'];
						break;
	
						case 'rfq_importance':

							$rfq_importance[] = $single_form_field['value'];
						break;

						case 'rfq_sentby':
							
							$rfq_sentby[] = $single_form_field['value'];
						break;

						case 'assigned_to':
							
							$assigned_to[] = $single_form_field['value'];
						break;
						case 'rfq_status':
							
							$rfq_status[] = $single_form_field['value'];
						break;
						case 'quotation_status':
							
							$quotation_status[] = $single_form_field['value'];
						break;
						case 'procurement_person_status':
							
							$procurement_person_status[] = $single_form_field['value'];
						break;
						case 'product_family':
							
							$product_family[] = $single_form_field['value'];
						break;
					}
				}
			}
		}

		foreach ($all_rfq_data as $single_rfq_data) {
			
			if(!empty($sales_user_details[$single_rfq_data['rfq_sentby']]) && empty($return_data['sales_person'][$single_rfq_data['rfq_sentby']])) {
				$return_data['sales_person'][$single_rfq_data['rfq_sentby']] = array(
																	'id' => $single_rfq_data['rfq_sentby'],
																	'name' => $sales_user_details[$single_rfq_data['rfq_sentby']],
																	'selected' => (in_array($single_rfq_data['rfq_sentby'], $rfq_sentby))?'selected':''
																	);
			}
			if(!empty($procurement_user_details[$single_rfq_data['assigned_to_1']]) && empty($return_data['procurement_person'][$single_rfq_data['assigned_to_1']])) {
				$return_data['procurement_person'][$single_rfq_data['assigned_to_1']] = array(
					'id' => $single_rfq_data['assigned_to_1'],
					'name' => $procurement_user_details[$single_rfq_data['assigned_to_1']],
					'selected' => (in_array($single_rfq_data['assigned_to_1'], $assigned_to))?'selected':''
				);
			}
			if(!empty($procurement_user_details[$single_rfq_data['assigned_to_2']]) && empty($return_data['procurement_person'][$single_rfq_data['assigned_to_2']])) {
				$return_data['procurement_person'][$single_rfq_data['assigned_to_2']] = array(
					'id' => $single_rfq_data['assigned_to_2'],
					'name' => $procurement_user_details[$single_rfq_data['assigned_to_2']],
					'selected' => (in_array($single_rfq_data['assigned_to_2'], $assigned_to))?'selected':''
				);
			}
			if(!empty($procurement_user_details[$single_rfq_data['assigned_to_3']]) && empty($return_data['procurement_person'][$single_rfq_data['assigned_to_3']])) {
				$return_data['procurement_person'][$single_rfq_data['assigned_to_3']] = array(
					'id' => $single_rfq_data['assigned_to_3'],
					'name' => $procurement_user_details[$single_rfq_data['assigned_to_3']],
					'selected' => (in_array($single_rfq_data['assigned_to_3'], $assigned_to))?'selected':''
				);
			}
		}
		$return_data['procurement_person'][179] = array(
			'id' => 179,
			'name' => $procurement_user_details[179],
			'selected' => (in_array(179, $assigned_to))?'selected':''
		);
		$return_data['procurement_person'][180] = array(
			'id' => 180,
			'name' => $procurement_user_details[180],
			'selected' => (in_array(180, $assigned_to))?'selected':''
		);
	    $return_data['rfq_importance'] = array(
			array(
				'value'=> 'V',
				'name'=> 'Very High',
				'selected'=> (in_array('V', $rfq_importance))?'selected':''
			),
			array(
				'value'=> 'H',
				'name'=> 'High',
				'selected'=> (in_array('H', $rfq_importance))?'selected':''
			),
			array(
				'value'=> 'M',
				'name'=> 'Medium',
				'selected'=> (in_array('M', $rfq_importance))?'selected':''
			),
			array(
				'value'=> 'L',
				'name'=> 'Low',
				'selected'=> (in_array('L', $rfq_importance))?'selected':''
			)
		);
	    $return_data['rfq_status'] = array(
			array(
				'value'=> 'waiting',
				'name'=> 'waiting',
				'class'=> 'kt-badge kt-badge--orange kt-badge--sm kt-badge--inline kt-badge--rounded',
				'selected'=> (in_array('waiting', $rfq_status))?'selected':''
			),
			array(
				'value'=> 'pending',
				'name'=> 'Pending',
				'class'=> 'kt-badge kt-badge--warning kt-badge--sm kt-badge--inline kt-badge--rounded',
				'selected'=> (in_array('pending', $rfq_status))?'selected':''
			),
			array(
				'value'=> 'query',
				'name'=> 'Query',
				'class'=> 'kt-badge kt-badge--danger kt-badge--sm kt-badge--inline kt-badge--rounded',
				'selected'=> (in_array('query', $rfq_status))?'selected':''
			),
			array(
				'value'=> 'regret',
				'name'=> 'Regret',
				'class'=> 'kt-badge kt-badge--dark kt-badge--sm kt-badge--inline kt-badge--rounded',
				'selected'=> (in_array('regret', $rfq_status))?'selected':''
			),
			array(
				'value'=> 'done',
				'name'=> 'Done',
				'class'=> 'kt-badge kt-badge--success kt-badge--sm kt-badge--inline kt-badge--rounded',
				'selected'=> (in_array('done', $rfq_status))?'selected':''
			)
		);
	    $return_data['quotation_status'] = array(
			array(
				'value'=> 'draft',
				'name'=> 'Draft',
				'selected'=> (in_array('draft', $quotation_status))?'selected':''
			),
		);
		$return_data['procurement_person_status'] = array(
			array(
				'value'=> 'pending',
				'name'=> 'Pending',
				'class'=> 'kt-badge kt-badge--warning kt-badge--sm kt-badge--inline kt-badge--rounded',
				'selected'=> (in_array('pending', $procurement_person_status))?'selected':''
			),
			array(
				'value'=> 'query',
				'name'=> 'Query',
				'class'=> 'kt-badge kt-badge--danger kt-badge--sm kt-badge--inline kt-badge--rounded',
				'selected'=> (in_array('query', $procurement_person_status))?'selected':''
			),
			array(
				'value'=> 'regret',
				'name'=> 'Regret',
				'class'=> 'kt-badge kt-badge--dark kt-badge--sm kt-badge--inline kt-badge--rounded',
				'selected'=> (in_array('regret', $procurement_person_status))?'selected':''
			),
			array(
				'value'=> 'done',
				'name'=> 'Done',
				'class'=> 'kt-badge kt-badge--success kt-badge--sm kt-badge--inline kt-badge--rounded',
				'selected'=> (in_array('done', $procurement_person_status))?'selected':''
			)
		);
		$return_data['product_family'] = array(
			array(
				'value'=> 'piping',
				'name'=> 'Piping',
				'class'=> 'kt-badge kt-badge--unified-info kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline',
				'selected'=> (in_array('piping', $product_family))?'selected':''
			),
			array(
				'value'=> 'instrumentation',
				'name'=> 'Instrumentation',
				'class'=> 'kt-badge kt-badge--unified-warning kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline',
				'selected'=> (in_array('instrumentation', $product_family))?'selected':''
			),
			array(
				'value'=> 'precision',
				'name'=> 'Precision',
				'class'=> 'kt-badge kt-badge--unified-orange kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline',
				'selected'=> (in_array('precision', $product_family))?'selected':''
			),
			array(
				'value'=> 'tubing',
				'name'=> 'Tubing',
				'class'=> 'kt-badge kt-badge--unified-dark kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline',
				'selected'=> (in_array('tubing', $product_family))?'selected':''
			),
			array(
				'value'=> 'mro_items',
				'name'=> 'MRO Items',
				'class'=> 'kt-badge kt-badge--unified-success kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline',
				'selected'=> (in_array('mro_items', $product_family))?'selected':''
			),
			array(
				'value'=> 'fastener',
				'name'=> 'Fastener',
				'class'=> 'kt-badge kt-badge--unified-danger kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline',
				'selected'=> (in_array('fastener', $product_family))?'selected':''
			),
			array(
				'value'=> 'valve',
				'name'=> 'Valve',
				'class'=> 'kt-badge kt-badge--unified-dark kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline',
				'selected'=> (in_array('valve', $product_family))?'selected':''
			)
		);
		// echo "<pre>";print_r($return_data);echo"</pre><hr>";exit;
	    // echo "<pre>";print_r($return_data['procurement_person']);echo"</pre><hr>";exit;
		return $return_data;
	}

	private function prepare_search_filter_data($search_filter_form_data){

		$return_data = array('rfq_no'=> '', 'rfq_company'=> '', 'rfq_date'=> '', 'quotation_no'=> '','rfq_subject'=> '', 'priority'=>'', 'rfq_importance'=> array(), 'sales_person'=> array(),'procurement_person'=> array(), 'procurement_person_status'=> array(), 'quotation_status'=> array(), 'product_family'=> array(), 'rfq_status'=> array());

		$sales_user = array_column($this->common_model->get_all_data('*', "status=1 AND role IN (5, 16)", 'users'), 'name', 'user_id');

		$procurement_user = array_column($this->common_model->get_all_data('*', "status=1 AND role IN (6, 8) AND user_id NOT IN('73', '85', '86')", 'users'), 'name', 'user_id');

		$rfq_importance = $rfq_sentby = $assigned_to = $rfq_status = $quotation_status = $procurement_person_status = $product_family = $priority = array();

		if(!empty($search_filter_form_data)) {
			foreach ($search_filter_form_data as $single_form_field) {

				if(!empty($single_form_field['value'])) {

					switch ($single_form_field['name']) {
						case 'rfq_no':
						case 'rfq_company':
						case 'rfq_date':
						case 'quotation_no':
						case 'priority':
						case 'rfq_subject':

							$return_data[$single_form_field['name']] = $single_form_field['value'];
						break;

						case 'rfq_importance':

							$rfq_importance[] = $single_form_field['value'];
						break;

						case 'rfq_sentby':

							$rfq_sentby[] = $single_form_field['value'];
						break;

						case 'assigned_to':

							$assigned_to[] = $single_form_field['value'];
						break;
						case 'rfq_status':

							$rfq_status[] = $single_form_field['value'];
						break;
						case 'quotation_status':

							$quotation_status[] = $single_form_field['value'];
						break;
						case 'procurement_person_status':

							$procurement_person_status[] = $single_form_field['value'];
						break;
						case 'product_family':

							$product_family[] = $single_form_field['value'];
						break;
					}
				}
			}
		}

		$all_rfq_data = $this->common_model->get_dynamic_data_sales_db('rfq_company, rfq_mst_id, rfq_no, rfq_date, rfq_sentby, assigned_to_1, assigned_to_2, assigned_to_3', array(), 'rfq_mst');

		foreach ($all_rfq_data as $single_rfq_data) {

			if(!empty($sales_user[$single_rfq_data['rfq_sentby']]) && empty($return_data['sales_person'][$single_rfq_data['rfq_sentby']])) {
				$return_data['sales_person'][$single_rfq_data['rfq_sentby']] = array(
																	'id' => $single_rfq_data['rfq_sentby'],
																	'name' => $sales_user[$single_rfq_data['rfq_sentby']],
																	'selected' => (in_array($single_rfq_data['rfq_sentby'], $rfq_sentby))?'selected':''
																	);
			}
			if(!empty($procurement_user[$single_rfq_data['assigned_to_1']]) && empty($return_data['procurement_person'][$single_rfq_data['assigned_to_1']])) {
				$return_data['procurement_person'][$single_rfq_data['assigned_to_1']] = array(
					'id' => $single_rfq_data['assigned_to_1'],
					'name' => $procurement_user[$single_rfq_data['assigned_to_1']],
					'selected' => (in_array($single_rfq_data['assigned_to_1'], $assigned_to))?'selected':''
				);
			}
			if(!empty($procurement_user[$single_rfq_data['assigned_to_2']]) && empty($return_data['procurement_person'][$single_rfq_data['assigned_to_2']])) {
				$return_data['procurement_person'][$single_rfq_data['assigned_to_2']] = array(
					'id' => $single_rfq_data['assigned_to_2'],
					'name' => $procurement_user[$single_rfq_data['assigned_to_2']],
					'selected' => (in_array($single_rfq_data['assigned_to_2'], $assigned_to))?'selected':''
				);
			}
			if(!empty($procurement_user[$single_rfq_data['assigned_to_3']]) && empty($return_data['procurement_person'][$single_rfq_data['assigned_to_3']])) {
				$return_data['procurement_person'][$single_rfq_data['assigned_to_3']] = array(
					'id' => $single_rfq_data['assigned_to_3'],
					'name' => $procurement_user[$single_rfq_data['assigned_to_3']],
					'selected' => (in_array($single_rfq_data['assigned_to_3'], $assigned_to))?'selected':''
				);
			}
		}

		$return_data['procurement_person'][180] = array(
			'id' => 180,
			'name' => $procurement_user[180],
			'selected' => (in_array(180, $assigned_to))?'selected':''
		);
	    $return_data['rfq_importance'] = array(
			array(
				'value'=> 'V',
				'name'=> 'Very High',
				'selected'=> (in_array('V', $rfq_importance))?'selected':''
			),
			array(
				'value'=> 'H',
				'name'=> 'High',
				'selected'=> (in_array('H', $rfq_importance))?'selected':''
			),
			array(
				'value'=> 'M',
				'name'=> 'Medium',
				'selected'=> (in_array('M', $rfq_importance))?'selected':''
			),
			array(
				'value'=> 'L',
				'name'=> 'Low',
				'selected'=> (in_array('L', $rfq_importance))?'selected':''
			)
		);
	    $return_data['rfq_status'] = array(
			array(
				'value'=> 'waiting',
				'name'=> 'waiting',
				'class'=> 'kt-badge kt-badge--orange kt-badge--sm kt-badge--inline kt-badge--rounded',
				'selected'=> (in_array('waiting', $rfq_status))?'selected':''
			),
			array(
				'value'=> 'pending',
				'name'=> 'Pending',
				'class'=> 'kt-badge kt-badge--warning kt-badge--sm kt-badge--inline kt-badge--rounded',
				'selected'=> (in_array('pending', $rfq_status))?'selected':''
			),
			array(
				'value'=> 'query',
				'name'=> 'Query',
				'class'=> 'kt-badge kt-badge--danger kt-badge--sm kt-badge--inline kt-badge--rounded',
				'selected'=> (in_array('query', $rfq_status))?'selected':''
			),
			array(
				'value'=> 'regret',
				'name'=> 'Regret',
				'class'=> 'kt-badge kt-badge--dark kt-badge--sm kt-badge--inline kt-badge--rounded',
				'selected'=> (in_array('regret', $rfq_status))?'selected':''
			),
			array(
				'value'=> 'done',
				'name'=> 'Done',
				'class'=> 'kt-badge kt-badge--success kt-badge--sm kt-badge--inline kt-badge--rounded',
				'selected'=> (in_array('done', $rfq_status))?'selected':''
			)
		);
	    $return_data['quotation_status'] = array(
			array(
				'value'=> 'draft',
				'name'=> 'Draft',
				'selected'=> (in_array('draft', $quotation_status))?'selected':''
			),
		);
		$return_data['procurement_person_status'] = array(
			array(
				'value'=> 'pending',
				'name'=> 'Pending',
				'class'=> 'kt-badge kt-badge--warning kt-badge--sm kt-badge--inline kt-badge--rounded',
				'selected'=> (in_array('pending', $procurement_person_status))?'selected':''
			),
			array(
				'value'=> 'query',
				'name'=> 'Query',
				'class'=> 'kt-badge kt-badge--danger kt-badge--sm kt-badge--inline kt-badge--rounded',
				'selected'=> (in_array('query', $procurement_person_status))?'selected':''
			),
			array(
				'value'=> 'regret',
				'name'=> 'Regret',
				'class'=> 'kt-badge kt-badge--dark kt-badge--sm kt-badge--inline kt-badge--rounded',
				'selected'=> (in_array('regret', $procurement_person_status))?'selected':''
			),
			array(
				'value'=> 'done',
				'name'=> 'Done',
				'class'=> 'kt-badge kt-badge--success kt-badge--sm kt-badge--inline kt-badge--rounded',
				'selected'=> (in_array('done', $procurement_person_status))?'selected':''
			)
		);
		$return_data['product_family'] = array(
			array(
				'value'=> 'piping',
				'name'=> 'Piping',
				'class'=> 'kt-badge kt-badge--unified-info kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline',
				'selected'=> (in_array('piping', $product_family))?'selected':''
			),
			array(
				'value'=> 'instrumentation',
				'name'=> 'Instrumentation',
				'class'=> 'kt-badge kt-badge--unified-warning kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline',
				'selected'=> (in_array('instrumentation', $product_family))?'selected':''
			),
			array(
				'value'=> 'precision',
				'name'=> 'Precision',
				'class'=> 'kt-badge kt-badge--unified-orange kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline',
				'selected'=> (in_array('precision', $product_family))?'selected':''
			),
			array(
				'value'=> 'tubing',
				'name'=> 'Tubing',
				'class'=> 'kt-badge kt-badge--unified-dark kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline',
				'selected'=> (in_array('tubing', $product_family))?'selected':''
			),
			array(
				'value'=> 'industrial',
				'name'=> 'Industrial',
				'class'=> 'kt-badge kt-badge--unified-success kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline',
				'selected'=> (in_array('industrial', $product_family))?'selected':''
			),
			array(
				'value'=> 'fastener',
				'name'=> 'Fastener',
				'class'=> 'kt-badge kt-badge--unified-danger kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline',
				'selected'=> (in_array('fastener', $product_family))?'selected':''
			),
			array(
				'value'=> 'valve',
				'name'=> 'Valve',
				'class'=> 'kt-badge kt-badge--unified-dark kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline',
				'selected'=> (in_array('valve', $product_family))?'selected':''
			)
		);

		// echo "<pre>";print_r($return_data);echo"</pre><hr>";exit;
		return $return_data;
	}

	private function prepare_rfq_list_where($search_filter_form_data){

		$return_where_string = "rfq_mst.assigned_to_1 NOT IN('148', '133', '73', '85', '86') AND rfq_mst.assigned_to_2 NOT IN('148', '133', '73', '85', '86') AND rfq_mst.assigned_to_3 NOT IN('148', '133', '73', '85', '86') AND rfq_mst.rfq_sentby != 0";

		// echo "<pre>";print_r($search_filter_form_data);echo"</pre><hr>";exit;
		if(!empty($search_filter_form_data)) {

			$rfq_importance = $rfq_sentby = $assigned_to = $rfq_status = $quotation_status = $procurement_person_status = $product_family = $priority = array();
			foreach ($search_filter_form_data as $single_form_field) {

				if(!empty($single_form_field['value'])) {
					
					switch ($single_form_field['name']) {
						
						case 'rfq_no':
							
							$return_where_string .= " AND rfq_mst.rfq_no like '%".$single_form_field['value']."%'";
						break;

						case 'rfq_company':
						
							// $client_id = array();
							//client source :-> main
							// $client_main_id = $this->common_model->get_dynamic_data_sales_db('client_id as id', array('client_name LIKE'=> "%".$single_form_field['value']."%"), 'clients');
							$client_main_id = $this->common_model->get_dynamic_data_sales_db('id', array('name LIKE'=> "%".$single_form_field['value']."%"), 'customer_mst');
							//client source :-> hetro leads
							// $client_hetro_id = $this->common_model->get_dynamic_data_sales_db('lead_id as id', array('company_name LIKE'=> "%".$single_form_field['value']."%"), 'hetro_leads');
							// //client source :-> primary leads
							// $client_primary_id = $this->common_model->get_dynamic_data_marketing_db('lead_mst_id as id', array('IMPORTER_NAME LIKE'=> "%".$single_form_field['value']."%"), 'lead_mst');

							// echo "<pre>";print_r($client_main_id);echo"</pre><hr>";exit;
							// $client_id = array_merge($client_main_id, $client_hetro_id, $client_primary_id);
							// $client_id = $client_main_id;
							$return_where_string .= " AND rfq_mst.rfq_company IN ('".implode("', '", array_column($client_main_id,'id'))."')";
						break;

						case 'rfq_date':
							
							$filter_date = explode(' - ', $single_form_field['value']);
						    $return_where_string .= " AND rfq_mst.rfq_date >= '".date('Y-m-d 00:00:00', strtotime($filter_date[0]))."'";
						    if(count($filter_date) > 1) {

						        $return_where_string .= " AND rfq_mst.rfq_date <= '".date('Y-m-d 23:59:59', strtotime($filter_date[1]))."'";
						    }else {

						        $return_where_string .= " AND rfq_mst.rfq_date <= '".date('Y-m-d 23:59:59', strtotime($filter_date[0]))."'";
						    }
						break;
						
						case 'rfq_importance':

							$rfq_importance[] = $single_form_field['value'];
						break;

						case 'rfq_sentby':

							$rfq_sentby[] = $single_form_field['value'];
						break;

						case 'assigned_to':
							
							$assigned_to[] = $single_form_field['value'];
						break;

						case 'rfq_status':
							
							$rfq_status[] = $single_form_field['value'];
						break;

						case 'quotation_no':
							
							$return_where_string .= " AND quotation_mst.quote_no like '%".$single_form_field['value']."%'";
						break;

						case 'quotation_status':
							
							$quotation_status[] = $single_form_field['value'];
						break;

						case 'procurement_person_status':
							
							$procurement_person_status[] = $single_form_field['value'];
						break;
						case 'product_family':
							
							$product_family[] = $single_form_field['value'];
						break;
						case 'priority':

							$priority[] = $single_form_field['value'];
						break;
						case 'rfq_subject':

							$return_where_string .= " AND rfq_mst.rfq_subject like '%".$single_form_field['value']."%'";
						break;
					}
				}
			}

			if(!empty($rfq_importance)){

				$return_where_string .= " AND rfq_mst.rfq_importance IN ('".implode("', '", $rfq_importance)."')";
			}
			if(!empty($rfq_status)){

				$return_where_string .= " AND rfq_mst.rfq_status IN ('".implode("', '", $rfq_status)."')";
			}
			if(!empty($quotation_status)){

				$return_where_string .= " AND quotation_mst.stage IN ('".implode("', '", $quotation_status)."')";
			}
			if(!empty($procurement_person_status)){

				$return_where_string .= " AND (rfq_mst.status_1 IN ('".implode("', '", $procurement_person_status)."') OR rfq_mst.status_2 IN ('".implode("', '", $procurement_person_status)."') OR rfq_mst.status_3 IN ('".implode("', '", $procurement_person_status)."'))";
			}
			if(!empty($product_family)){

				$return_where_string .= " AND rfq_mst.product_family IN ('".implode("', '", $product_family)."')";
			}
			if(!empty($priority)){

				$return_where_string .= " AND rfq_mst.priority IN ('".implode("', '", $priority)."')";
			}
		}
		// echo "<pre>";print_r($rfq_sentby);echo"</pre><hr>";
		if(in_array($this->session->userdata('role'), array(1, 4, 6, 16, 17))){

			if(!empty($rfq_sentby)){

				$return_where_string .= " AND rfq_mst.rfq_sentby IN (".implode(', ', $rfq_sentby).")";
			}
			if(!empty($assigned_to)){

				$return_where_string .= " AND ( rfq_mst.assigned_to IN (".implode(', ', $assigned_to).") || rfq_mst.assigned_to_1 IN (".implode(', ', $assigned_to).") || rfq_mst.assigned_to_2 IN (".implode(', ', $assigned_to).") || rfq_mst.assigned_to_3 IN (".implode(', ', $assigned_to).") )";
			}
		}
		if($this->session->userdata('role') == 5){

			if(!empty($assigned_to)){

				$return_where_string .= " AND ( rfq_mst.assigned_to IN (".implode(', ', $assigned_to).") || rfq_mst.assigned_to_1 IN (".implode(', ', $assigned_to).") || rfq_mst.assigned_to_2 IN (".implode(', ', $assigned_to).") || rfq_mst.assigned_to_3 IN (".implode(', ', $assigned_to).") )";
			}
			$ids = $this->session->userdata('rfq_access')['rfq_sales_user_id'];
			if(!empty($rfq_sentby)){

				$ids = array();
				foreach ($rfq_sentby as $id => $sales_person_id) {
					
					if(in_array($sales_person_id, $this->session->userdata('rfq_access')['rfq_sales_user_id'])){

						$ids[] = $sales_person_id;
					}
				}
				if(empty($ids)){

					// echo "<pre>";print_r($ids);echo"</pre><hr>";exit;
					$ids = $this->session->userdata('rfq_access')['rfq_sales_user_id'];
				}
			}
			$return_where_string .= " AND rfq_mst.rfq_sentby IN (".implode(', ', $ids).")";
		}
		if($this->session->userdata('role') == 8){

			if(!empty($rfq_sentby)){

				$return_where_string .= " AND rfq_mst.rfq_sentby IN (".implode(', ', $rfq_sentby).")";
			}
			$ids = $this->session->userdata('rfq_access')['rfq_procurement_user_id'];
			if(!empty($assigned_to)){

				$ids = array();
				foreach ($assigned_to as $id => $procurement_person_id) {
					
					if(in_array($procurement_person_id, $this->session->userdata('rfq_access')['rfq_procurement_user_id'])){

						$ids[] = $procurement_person_id;
					}
				}
				if(empty($ids)){

					$ids = $this->session->userdata('rfq_access')['rfq_procurement_user_id'];
				}
			}
			$return_where_string .= " AND ( rfq_mst.assigned_to IN (".implode(', ', $ids).") || rfq_mst.assigned_to_1 IN (".implode(', ', $ids).") || rfq_mst.assigned_to_2 IN (".implode(', ', $ids).") || rfq_mst.assigned_to_3 IN (".implode(', ', $ids).") )";
		}
		if(!empty($this->session->userdata('rfq_client_type')) && $this->session->userdata('rfq_client_type') != 'all'){

			$return_where_string .= " AND rfq_mst.type IN ('".$this->session->userdata('rfq_client_type')."')";
		}
		if($this->session->userdata('rfq_access')['rfq_product_family_access']) {
			$return_where_string .= " AND rfq_mst.product_family IN ('".implode("', '", $this->session->userdata('rfq_access')['rfq_product_family_access'])."')";
		}

		// echo "<pre>";print_r($return_where_string);echo"</pre><hr>";exit;
		return $return_where_string;
	}
	
	// private function get_client_name($rfq_company, $rfq_buyer, $client_source) {
	private function get_client_name($rfq_company, $rfq_buyer) {
			
			// echo "<pre>";print_r($rfq_buyer);echo"</pre><hr>";exit;
		$company_details = array('name'=>'', 'member_name'=>'');
		//client source :-> main
		$company_table_select = "name, country_id";
		$company_table_name = "customer_mst";
		$company_table_where = array("id" => $rfq_company);
		$member_table_select = "member_name";
		$member_table_name = "customer_dtl";
		$member_table_where = array('comp_dtl_id' => $rfq_buyer);
		$common_model_function_name = "get_dynamic_data_sales_db";
		
		$company_name = $this->common_model->$common_model_function_name($company_table_select, $company_table_where, $company_table_name, 'row_array');

		
		if(!empty($company_name['name'])){
			$company_details['name'] = $company_name['name'];
			$company_details['country_id'] = $company_name['country_id'];
		}
		$member_name = $this->common_model->$common_model_function_name($member_table_select, $member_table_where, $member_table_name, 'row_array');
		// echo "<pre>";print_r($member_name);echo"</pre><hr>";exit;
		if(!empty($member_name['member_name'])){
			$company_details['member_name'] = $member_name['member_name']; 	
		}
		// echo "<pre>";print_r($company_details);echo"</pre><hr>";exit;
		return $company_details;
	}

	private function prepare_rfq_list_data($where_array, $order_by_array, $limit=10, $offset=0){

		$return_array = $this->procurement_model->get_rfq_list_data($where_array, $order_by_array, $limit, $offset);
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		$company_table_select = $company_table_name = $company_table_where = "";
		$member_table_select = $member_table_name = $member_table_where = "";
		$common_model_function_name = "";
		$user_details = array_column($this->common_model->get_dynamic_data_sales_db('*', array(), 'users'), 'name', 'user_id');
		$country_details = array_column($this->common_model->get_dynamic_data_sales_db('*', array(), 'country_mst'), 'name', 'id');
		$static_importance_value = array('V'=>'Very High', 'H'=>'High', 'M'=>'Medium', 'L'=>'Low');
		
		foreach ($return_array['rfq_list'] as $rfq_list_key => $single_rfq_details) {
			
			// echo "<pre>";print_r($single_rfq_details);echo"</pre><hr>";exit;
			if(!empty($single_rfq_details['assigned_to_date'])){

				$return_array['rfq_list'][$rfq_list_key]['assigned_to_date'] = date('M j, Y g:i A', strtotime($single_rfq_details['assigned_to_date']));
			}else{

				$return_array['rfq_list'][$rfq_list_key]['assigned_to_date'] = date('M j, Y', strtotime($single_rfq_details['rfq_date']));
			}

			// client name and member name start
			
			// $company_details = $this->get_client_name($single_rfq_details['rfq_company'], $single_rfq_details['rfq_buyer'], $single_rfq_details['client_source']);
			$company_details = $this->get_client_name($single_rfq_details['rfq_company'], $single_rfq_details['rfq_buyer']);
			// echo "<pre>";print_r($company_details);echo"</pre><hr>";exit;
			$return_array['rfq_list'][$rfq_list_key]['company_name'] = $company_details['name'];
			$return_array['rfq_list'][$rfq_list_key]['member_name'] = $company_details['member_name'];
			$return_array['rfq_list'][$rfq_list_key]['country_name'] = $country_details[$company_details['country_id']];
			
			// client name and member name and country name end

			// rfq start date and close date start here	
			$return_array['rfq_list'][$rfq_list_key]['date_diff'] = '';
			if(!empty($single_rfq_details['rfq_closedate'])){

				$today_datetime = date("Y-m-d h:i:s");
				$today  = strtotime($today_datetime);

				$datetime = new DateTime($single_rfq_details['rfq_closedate']);
				$close_datetime = $datetime->format('Y-m-d g:i:s');
				$close_time = strtotime($close_datetime);

				$day_difference = round( ($close_time - $today) / (60 * 60 * 24));
				$time = $datetime->format('g:i A');

				if($time != "12:00 AM"){
					$return_array['rfq_list'][$rfq_list_key]['close_time'] = $datetime->format('g:i A');
				}

				if(!empty($day_difference)){

					$return_array['rfq_list'][$rfq_list_key]['date_diff'] = $day_difference.' days';
				}else if(empty($day_difference)){

					$return_array['rfq_list'][$rfq_list_key]['date_diff'] = 'Closing Today';
				}
			}

			$return_array['rfq_list'][$rfq_list_key]['rfq_date'] = date('j, M', strtotime($single_rfq_details['rfq_date']));
			$return_array['rfq_list'][$rfq_list_key]['rfq_closedate'] = '';
			if(!empty($single_rfq_details['rfq_closedate'])){

				$return_array['rfq_list'][$rfq_list_key]['rfq_closedate'] = date('j, M', strtotime($single_rfq_details['rfq_closedate']));
			}	
			// rfq start date and close date end

			// rfq importance
			$return_array['rfq_list'][$rfq_list_key]['rfq_importance'] = $static_importance_value[$single_rfq_details['rfq_importance']];

			// sales person 
			$return_array['rfq_list'][$rfq_list_key]['sales_person'] = $user_details[$single_rfq_details['rfq_sentby']];

			// procurement person start
			$return_array['rfq_list'][$rfq_list_key]['procuremnt_person'][0] = array('name'=> '', 'status'=>'');
			$return_array['rfq_list'][$rfq_list_key]['procuremnt_person'][1] = array('name'=> '', 'status'=>'');
			$return_array['rfq_list'][$rfq_list_key]['procuremnt_person'][2] = array('name'=> '', 'status'=>'');
			if($single_rfq_details['assigned_to_1']) {

				$return_array['rfq_list'][$rfq_list_key]['procuremnt_person'][0]['name'] = $this->creat_first_name($user_details[$single_rfq_details['assigned_to_1']]);
			}
			$return_array['rfq_list'][$rfq_list_key]['procuremnt_person'][0]['status'] = $single_rfq_details['status_1'];
			if($single_rfq_details['assigned_to_2']) {

				$return_array['rfq_list'][$rfq_list_key]['procuremnt_person'][1]['name'] = $this->creat_first_name($user_details[$single_rfq_details['assigned_to_2']]);
			}
			$return_array['rfq_list'][$rfq_list_key]['procuremnt_person'][1]['status'] = $single_rfq_details['status_2'];
			if($single_rfq_details['assigned_to_3']) {

				$return_array['rfq_list'][$rfq_list_key]['procuremnt_person'][2]['name'] = $this->creat_first_name($user_details[$single_rfq_details['assigned_to_3']]);
			}
			$return_array['rfq_list'][$rfq_list_key]['procuremnt_person'][2]['status'] = $single_rfq_details['status_3'];
			// procurement person end

			// quotation details start	
			$return_array['rfq_list'][$rfq_list_key]['quote_no'] = '';
			$return_array['rfq_list'][$rfq_list_key]['quote_id'] = 0;
			
			$quote_arr = $this->common_model->get_dynamic_data_sales_db('*', array('rfq_id' => $single_rfq_details['rfq_mst_id']), 'quotation_mst', 'row_array');
			
			// echo "<pre>";print_r($quote_arr);echo"</pre><hr>";exit;
			
			if(!empty($quote_arr)){
				
				$return_array['rfq_list'][$rfq_list_key]['net_amount'] = $quote_arr['net_total'];
				$return_array['rfq_list'][$rfq_list_key]['quote_id'] = $quote_arr['quotation_mst_id'];
				$return_array['rfq_list'][$rfq_list_key]['quote_no'] = 'Draft';
				if($quote_arr['stage'] != 'draft'){
					
					$return_array['rfq_list'][$rfq_list_key]['quote_no'] = $quote_arr['quote_no'];	
				}
			}
			// quotation details end

			$return_array['rfq_list'][$rfq_list_key]['query'] = '';
			$return_array['rfq_list'][$rfq_list_key]['is_new'] = false;
			$query_res = $this->common_model->get_dynamic_data_sales_db('*', array('rfq_id' => $single_rfq_details['rfq_mst_id'], 'type' => 'query'), 'rfq_note_query');
			if(!empty($query_res)){
				$sent_by = '';
				$return_array['rfq_list'][$rfq_list_key]['query'] .= '<tr>
												<td>
													Description
												</td>
												<td>
													Date
												</td>
												<td>
													Person Name
												</td>
											</tr>';
				foreach ($query_res as $qkey => $qvalue) {
					$sent_by = $qvalue['entered_by'];
					$align = 'left';
					$return_array['rfq_list'][$rfq_list_key]['query'] .= '<tr>
													<td>
														'.$qvalue['note'].'
													</td>
													<td>
														'.date('d M H:i', strtotime($qvalue['entered_on'])).'
													</td>
													<td>
														'.$user_details[$qvalue['entered_by']].'
													</td>
												</tr>';
				}
				if($this->session->userdata('user_id') != $sent_by){
					$return_array['rfq_list'][$rfq_list_key]['is_new'] = true;
				}
			}
			$this->common_model->db->join('quotation_mst m', 'm.quotation_mst_id = q.query_for_id', 'inner');
			$this->common_model->db->join('rfq_mst r', 'r.rfq_mst_id = m.rfq_id', 'inner');
			$query_res = $this->common_model->db->get_where('query_mst q', array('r.rfq_mst_id' => $single_rfq_details['rfq_mst_id']))->row_array();
			$return_array['rfq_list'][$rfq_list_key]['has_query'] = false;
			$return_array['rfq_list'][$rfq_list_key]['quotation_mst_id'] = '';
			$return_array['rfq_list'][$rfq_list_key]['query_id'] = '';
			$return_array['rfq_list'][$rfq_list_key]['query_type'] = '';
			if(!empty($query_res)){
				$return_array['rfq_list'][$rfq_list_key]['has_query'] = true;
				$return_array['rfq_list'][$rfq_list_key]['quotation_mst_id'] = $query_res['quotation_mst_id'];
				$return_array['rfq_list'][$rfq_list_key]['query_id'] = $query_res['query_id'];
				$return_array['rfq_list'][$rfq_list_key]['query_type'] = $query_res['query_type'];
			}

			$return_array['rfq_list'][$rfq_list_key]['notes'] = '';
			$notes_res = $this->common_model->db->get_where('rfq_note_query', array('rfq_id' => $single_rfq_details['rfq_mst_id'], 'type' => 'notes'))->result_array();
			if(!empty($query_res)){
				$sent_by = '';
				foreach ($query_res as $qkey => $qvalue) {
					$sent_by = $qvalue['entered_by'];
					$return_array['rfq_list'][$rfq_list_key]['notes'] .= '<tr><td>'.$qvalue['note'].'<br/><span style="font-size: 10px; text-align:right;">'.date('d M H:i', strtotime($qvalue['entered_on'])).'</span>
					</td></tr>';
				}
			}

			//rfq rating
			$return_array['rfq_list'][$rfq_list_key]['priority_div'] = '<div class="kt-font-warning kt-demo-icon__preview" data-toggle="kt-tooltip" data-placement="left" title="" data-original-title="Tooltip title">';
			if(!empty($single_rfq_details['priority'])) {

				$return_array['rfq_list'][$rfq_list_key]['priority_div'] = '<div class="kt-font-warning kt-demo-icon__preview" data-toggle="kt-tooltip" data-placement="left" title="'.$single_rfq_details["rfq_priority_reason"].'" data-original-title="Tooltip title">';
				for ($i=0; $i < 5; $i++) { 
				
					if($i < $single_rfq_details['priority']) {
						$return_array['rfq_list'][$rfq_list_key]['priority_div'] .= '<i class="la la-star" style="line-height: 0;vertical-align: middle;font-size: 1.5rem !important;"></i>';
					}
				}
			}
			$user_details[0] = '';
			$return_array['rfq_list'][$rfq_list_key]['priority_div'] .= '</div>';

			//rfq comment start
			$return_array['rfq_list'][$rfq_list_key]['comment'] = array();

			if (!empty($single_rfq_details['chat_info'])) {
				$chat_info_array = json_decode($single_rfq_details['chat_info'], true);
				$comments_array = array();
				$users = array();

				foreach (array_reverse($chat_info_array) as $comment) {
					$user_id = $comment['user_id'];

					if (in_array($user_id, $users)) {
						continue;
					}

					$comment['name'] = $this->creat_first_name($user_details[$user_id]);
					$comment['tat'] = $this->creat_tat($comment['date_time']);

					$comments_array[] = $comment;
					$users[] = $user_id;

					if (count($comments_array) >= 3) {
						break;
					}
					// echo "<pre>";print_r($users);echo"</pre><hr>";exit;
				}
				
				$return_array['rfq_list'][$rfq_list_key]['hr_tag'] .= '<hr>';
				$return_array['rfq_list'][$rfq_list_key]['comment'] = $comments_array;
			}

			// echo "<pre>";print_r($single_rfq_details);echo"</pre><hr>";exit;
			$same_rfq_where = 'rfq_mst.rfq_company='.$single_rfq_details['rfq_company'].' AND rfq_mst.rfq_subject="'.$single_rfq_details['rfq_subject'].'" AND rfq_mst.rfq_mst_id !='.$single_rfq_details['rfq_mst_id'].' AND rfq_mst.product_family !="" AND ';
			$same_rfq_where .= $where_array;

			$double_rfq_list_details = $this->procurement_model->get_same_rfq_list_data($same_rfq_where);
			if(!empty($double_rfq_list_details)){

				$return_array['rfq_list'][$rfq_list_key]['background_class'] = "same_rfq_background_class";
			}
			//rfq comment end
		}

		$return_array['paggination_data']['limit'] = $limit;
		$return_array['paggination_data']['offset'] = $offset;
		$return_array['rfq_badge'] = array(
			'pending' => 'kt-badge kt-badge--warning kt-badge--sm kt-badge--rounded',
			'done' => 'kt-badge kt-badge--success kt-badge--sm kt-badge--rounded',
			'query' => 'kt-badge kt-badge--danger kt-badge--sm kt-badge--rounded',
			'regret' => 'kt-badge kt-badge--dark kt-badge--sm kt-badge--rounded',
			'waiting'=> 'kt-badge kt-badge--orange kt-badge--sm kt-badge--rounded'
		);
		$return_array['rfq_family_badge'] = array(
			'piping' => 'kt-badge kt-badge--unified-info kt-badge--sm kt-badge--rounded kt-badge--bold',
			'instrumentation' => 'kt-badge kt-badge--unified-warning kt-badge--sm kt-badge--rounded kt-badge--bold',
			'precision' => 'kt-badge kt-badge--unified-orange kt-badge--sm kt-badge--rounded kt-badge--bold',
			'tubing' => 'kt-badge kt-badge--unified-dark kt-badge--sm kt-badge--rounded kt-badge--bold',
			'industrial' => 'kt-badge kt-badge--unified-success kt-badge--sm kt-badge--rounded kt-badge--bold',
			'fastener' => 'kt-badge kt-badge--unified-danger kt-badge--sm kt-badge--rounded kt-badge--bold',
			'valve' => 'kt-badge kt-badge--unified-dark kt-badge--sm kt-badge--rounded kt-badge--bold',

		);
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		return $return_array;
	}
	private function creat_tat($date_time){

		$return_tat = '';
		// Get the timestamp of the chat message
		$chat_time = strtotime(str_replace("Z", "", $date_time)); // Replace with your chat message timestamp

		// Create DateTime objects for the chat message and current time
		$chat_date = new DateTime("@$chat_time");
		$current_date = new DateTime();
		// Get the difference between the two dates
		$date_diff = $current_date->diff($chat_date);

		// Output the result
		if ($date_diff->y > 0) {

			$return_tat = $date_diff->y . " year(s)";
		}else if ($date_diff->m > 0) {

			$return_tat = $date_diff->m . " month(s)";
		}else if ($date_diff->d > 0) {

			$return_tat = $date_diff->d . " day(s)";
		}else if ($date_diff->h > 0) {

			$return_tat = $date_diff->h . " hour(s)";
		}else if ($date_diff->i > 0) {

			$return_tat = $date_diff->i . " minute(s)";
		}else {

			$return_tat = $date_diff->s . " second(s)";
		}
		
		return $return_tat." ago";
	}
	private function creat_first_name($string){

		$return_string = "";

		if(!empty($string)){

			$string_explode = explode(" ", $string);
			$return_string = $string_explode[0];
		}
		return $return_string;
	}

	private function upload_doc_config() {

		// echo "<pre>";print_r($_FILES);echo"</pre><hr>";exit;
		$config['upload_path']          = './assets/rfq_files/';
        $config['allowed_types']        = 'pdf|doc|docx|csv|jpeg|gif|jpg|png';
        $config['overwrite']            = TRUE;

        $this->load->library('upload', $config);

		$data = array('status' => 'successful', 'msg' => '', 'file_name' => '');
        if ( ! $this->upload->do_upload('file'))
        {
			$error = array('error' => $this->upload->display_errors());
			$data['status'] = 'failed';
			$data['msg'] = $error['error'];
        }
        else
        {
			$file_dtls = $this->upload->data();
			// echo "<pre>";print_r($file_dtls);echo"</pre><hr>";exit;
			$data['file_type'] = $file_dtls['file_type'];
			$data['file_name'] = $file_dtls['file_name'];
			$data['file_add_date'] = date('Y-m-d H:i:s');
        }
        // echo "<pre>";print_r($data);echo"</pre><hr>";exit;
        return $data;
	}
} 