<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		
	<!--Begin::Section-->
	<div class="row">
		<div class="col-12">
			<div class="alert alert-solid-info alert-bold kt-ribbon kt-ribbon--clip kt-ribbon--right kt-ribbon--info" role="alert">
				<?php if(!empty($todays_birthday_information)) { $top_increment = 0?>
					<?php foreach($todays_birthday_information as $todays_birthday_information_details) {?>
						<div class="kt-ribbon__target" style="top: <?php echo $top_increment;?>px;">
							<span class="kt-ribbon__inner"></span>Today is <?php echo $todays_birthday_information_details['first_name'], ' ',$todays_birthday_information_details['last_name'];?> Birthday.
						</div>
					<?php $top_increment = $top_increment+40;}?>
				<?php }?>
				<div class="alert-text"><h4>Welcome: <?php echo $this->session->userdata('name');?></h4></div>
			</div>
		</div>
	</div>

	<!--End::Section-->
	<?php if(!empty($notice_type)){?>
	<!--Begin::Section-->
	<div class="row">
		<div class="col-xl-12">
			<!--begin:: Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__body" style="padding: 10px 25px;">
					<div class="kt-widget kt-widget--user-profile-3">
						<div class="kt-widget__top">
							<div class="kt-widget__content">
								<div class="kt-widget__head">
									<a href="#" class="kt-widget__username"></a>
									<div class="kt-widget__action">
										
											<?php foreach ($notice_type as $notice_name => $notice_count) { ?>
												<button type="button" class="btn btn-label-brand btn-sm btn-upper get_notice" notice_type="<?php echo $notice_name; ?>">
													<?php echo $notice_name; ?>
													<span class="kt-badge kt-badge--unified-warning kt-badge--sm kt-badge--rounded kt-badge--bolder">
													<?php echo $notice_count; ?>
													</span>
												</button>&nbsp;
											<?php } ?>
										
									</div>
								</div>
								<div class="kt-widget__info"></div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!--end:: Portlet-->
		</div>
	</div>

	<!--End::Section-->
	<?php } ?>
	<!--Begin::Section-->
	<div class="row notice_panel"></div>

	<!--End::Section-->
	<?php 
		$first_column = "#20c997";
		$second_column = "#007bff";
		$third_column = "#fd7e14";
		$fourth_column = "#ff0000";
		$fifth_column = "#ffb822";

		$procurement_team = "#007bff";
		$quality_team = "#fd7e14";
		$crm_team = "#ff0000";
		$sales_team = "#ffb822";
		$hr_team = "#007bff";
		$export_team = "#fd7e14";


		$image_not_found_array = array('success', 'danger', 'warning', 'info', 'dark', 'brand');
        $image_not_found_array_key = 0;
	?>
	<!--Begin::Section-->
	<div class="row">
		<div class="col-xl-12">

			<!--Begin::Portlet-->
			<div class="kt-portlet kt-portlet--height-fluid" style="background-color: #e4e8ee !important;">
				
				<div class="kt-portlet__body" style="padding: 0px 00px;">

					<div class="row" style="padding: 20px 0px;">
						<div class="col-xl-5"></div>
						<div class="col-xl-2" style="background-color: #fff !important;box-sizing: border-box;border-top: 0.5rem solid #fd7e14;">
							<!--begin::Widget -->
							<div class="kt-widget kt-widget--user-profile-2">
								<div class="kt-widget__head" style="margin-top: 0px;padding: 10px;">
									<div class="kt-widget__media">
										<img class="kt-widget__img kt-hidden" src="assets/media/users/100_3.jpg" alt="image">
										<div class="kt-widget__pic kt-widget__pic--brand kt-font-brand kt-font-boldest kt-hidden-">
											JM
										</div>
									</div>
									<?php  if($this->session->userdata('global_user_details')['user_name'] == 'jay') {?>
									<div class="kt-widget__info" style="width:215px;">
									<?php }else{?>
									<div class="kt-widget__info" style="width:240px;">
									<?php }?>
										<span href="javascript:void(0)" class="kt-widget__username">
											CEO
										</span>
										<span class="kt-widget__desc">
											Jay Mehta
										</span>
									</div>
									<?php  if($this->session->userdata('global_user_details')['user_name'] == 'jay') {?>
									<div class="kt-widget__info" style="width:25px; padding-left: 0px;">
										<span href="javascript:void(0)" class="kt-widget__username">
											<div class="dropdown dropdown-inline">
												<button type="button" class="btn btn-hover-brand btn-elevate-hover btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<i class="flaticon-more"></i>
												</button>
												<div class="dropdown-menu dropdown-menu-right">
													<a class="dropdown-item login_as_another_user" href="javascript:void(0)" user_name="<?php echo $this->session->userdata('global_user_details')['user_name']; ?>" password="<?php echo $this->session->userdata('global_user_details')['password']; ?>"><i class="la la-user"></i> Login</a>
													<a class="dropdown-item" href="<?php echo base_url('home/organization_chart');?>" target="_blank"><i class="la la-plus"></i>Manage Org Chart</a>
													<a class="dropdown-item" href="<?php echo base_url('common/user_assign_data');?>" target="_blank"><i class="la la-plus"></i>Manage Lead</a>
												</div>
											</div>
										</span>
										<span class="kt-widget__desc" style="padding-top: 50px;">
										</span>
									</div>
									<?php }?>
								</div>
							</div>

							<!--end::Widget -->
						</div>
						<div class="col-xl-5"></div>
					</div>
				</div>
			</div>

			<!--End::Portlet-->
		</div>
	</div>

	<!--End::Section-->
	<!--Begin::Section-->
	<div class="row" style="width: 15000px;">
		<div class="col-xl-12">

			<!--Begin::Portlet-->
			<div class="kt-portlet kt-portlet--height-fluid" style="background-color: #e4e8ee !important;">
				
				<div class="kt-portlet__body" style="padding: 0px 00px;">

					<?php if(!empty($org_chart_details['level_2'])) {?>
					<div class="row" style="padding: 20px 0px;">
						<?php foreach ($org_chart_details['level_2'] as $level_2_details) { ?>
						<div class="" style="width: <?php echo $level_2_details['width'],'px'; ?>;"></div>
						<div class="" style="width: 350px;background-color: #fff !important;box-sizing: border-box;border-top: 0.5rem solid <?php echo $level_2_details['color'];?>;">
							<!--begin::Widget -->
							<div class="kt-widget kt-widget--user-profile-2">
								<div class="kt-widget__head" style="margin-top: 0px;padding: 10px;">
									<div class="kt-widget__media" style="width: 90px;">
										<img class="kt-widget__img kt-hidden" src="assets/media/users/100_1.jpg" alt="image">
										<?php if(!empty($level_2_details['profile_pic_file_path'])) {?>

											<img class="kt-widget__img kt-hidden-" src="<?php echo "https://crm.omtubes.com/assets/hr_document/profile_pic/".$level_2_details['profile_pic_file_path'];?>" alt="image" style="max-height: 90px;">
										<?php } else {?>
										<div class="kt-widget__pic kt-widget__pic--<?php echo $image_not_found_array[$image_not_found_array_key%5];?> kt-font-<?php echo $image_not_found_array[$image_not_found_array_key%5];?> kt-font-boldest kt-hidden-">
											<?php 
												$explode_name = explode(' ', $level_2_details['name']);
												foreach ($explode_name as $single_word) {
													echo ucfirst($single_word[0]);
												}
											?>
										</div>
										<?php $image_not_found_array_key++;}?>
									</div>
									<?php  if(in_array($level_2_details['user_id'], $allow_other_person_login_array)  || $level_2_details['password'] == $this->session->userdata('global_user_details')['password']) {?>
									<div class="kt-widget__info" style="width:215px;">
									<?php }else{?>
									<div class="kt-widget__info" style="width:240px;">
									<?php }?>
										<span href="javascript:void(0)" class="kt-widget__username">
											<?php echo $level_2_details['user_designation']; ?>
										</span>
										<span class="kt-widget__desc">
											<?php echo $level_2_details['name']; ?>
										</span>
									</div>
									<?php  if(in_array($level_2_details['user_id'], $allow_other_person_login_array)  || $level_2_details['password'] == $this->session->userdata('global_user_details')['password']) {?>
										<div class="kt-widget__info" style="width:25px; padding-left: 0px;">
											<span href="javascript:void(0)" class="kt-widget__username">
												<div class="dropdown dropdown-inline">
													<button type="button" class="btn btn-hover-brand btn-elevate-hover btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<i class="flaticon-more"></i>
													</button>
													<div class="dropdown-menu dropdown-menu-right">
														<a class="dropdown-item login_as_another_user" href="javascript:void(0)" user_name="<?php echo $level_2_details['username']; ?>" password="<?php echo $level_2_details['password']; ?>"><i class="la la-user"></i> Login</a>
														<a class="dropdown-item" href="<?php echo base_url('home/calendar/'.$level_2_details['user_id']);?>" target="_blank"><i class="la la-plus"></i> Daily Report</a>
														<?php if(in_array($level_2_details['user_department'], array('Sales', 'Procurement'))) {?>
														<a class="dropdown-item" href="<?php echo base_url('common/view_user_data/'),$level_2_details['user_id'];?>" target="_blank"><i class="la la-plus"></i>Manage Lead</a>
														<?php } ?>
													</div>
												</div>
											</span>
											<span class="kt-widget__desc" style="padding-top: 50px;">
											</span>
										</div>
									<?php }?>
								</div>
							</div>

							<!--end::Widget -->
						</div>
						<?php } ?>
					</div>
					<?php }?>

					<?php if(!empty($org_chart_details['level_3'])) {?>
					<div class="row" style="padding: 20px 0px;">
					<?php foreach ($org_chart_details['level_3'] as $level_3_details) { ?>
						<div class="" style="width: <?php echo $level_3_details['width'],'px'; ?>;"></div>
						<?php if(!empty($level_3_details['user_designation'])) { ?>
						<div class="" style="width: 350px;background-color: #fff !important;box-sizing: border-box;border-top: 0.5rem solid <?php echo $level_3_details['color'];?>;">
							<!--begin::Widget -->
							<div class="kt-widget kt-widget--user-profile-2">
								<div class="kt-widget__head" style="margin-top: 0px;padding: 10px;">
									<div class="kt-widget__media" style="width: 90px;">
										<img class="kt-widget__img kt-hidden" src="assets/media/users/100_1.jpg" alt="image">
										<?php if(!empty($level_3_details['profile_pic_file_path'])) {?>

											<img class="kt-widget__img kt-hidden-" src="<?php echo "https://crm.omtubes.com/assets/hr_document/profile_pic/".$level_3_details['profile_pic_file_path'];?>" alt="image" style="max-height: 90px;">
										<?php } else {?>
										<div class="kt-widget__pic kt-widget__pic--<?php echo $image_not_found_array[$image_not_found_array_key%5];?> kt-font-<?php echo $image_not_found_array[$image_not_found_array_key%5];?> kt-font-boldest kt-hidden-">
											<?php 
												$explode_name = explode(' ', $level_3_details['name']);
												foreach ($explode_name as $single_word) {
													echo ucfirst($single_word[0]);
												}
											?>
										</div>
										<?php $image_not_found_array_key++;}?>
									</div>
									<?php  if(in_array($level_3_details['user_id'], $allow_other_person_login_array)  || $level_3_details['password'] == $this->session->userdata('global_user_details')['password']) {?>
									<div class="kt-widget__info" style="width:215px;">
									<?php }else{?>
									<div class="kt-widget__info" style="width:240px;">
									<?php }?>
										<a href="javascript:void(0)" class="kt-widget__username">
											<?php echo $level_3_details['user_designation']; ?>
										</a>
										<span class="kt-widget__desc">
											<?php echo $level_3_details['name']; ?>
										</span>
									</div>
									<?php  if(in_array($level_3_details['user_id'], $allow_other_person_login_array)  || $level_3_details['password'] == $this->session->userdata('global_user_details')['password']) {?>
										<div class="kt-widget__info" style="width:25px; padding-left: 0px;">
											<span href="javascript:void(0)" class="kt-widget__username">
												<div class="dropdown dropdown-inline">
													<button type="button" class="btn btn-hover-brand btn-elevate-hover btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<i class="flaticon-more"></i>
													</button>
													<div class="dropdown-menu dropdown-menu-right">
														<a class="dropdown-item login_as_another_user" href="javascript:void(0)" user_name="<?php echo $level_3_details['username']; ?>" password="<?php echo $level_3_details['password']; ?>"><i class="la la-user"></i> Login</a>
														<a class="dropdown-item" href="<?php echo base_url('home/calendar/'.$level_3_details['user_id']);?>" target="_blank"><i class="la la-plus"></i> Daily Report</a>
														<?php if(in_array($level_3_details['user_department'], array('Sales', 'Procurement'))) {?>
														<a class="dropdown-item" href="<?php echo base_url('common/view_user_data/'),$level_3_details['user_id'];?>" target="_blank"><i class="la la-plus"></i>Manage Lead</a>
														<?php } ?>
													</div>
												</div>
											</span>
											<span class="kt-widget__desc" style="padding-top: 50px;">
											</span>
										</div>
									<?php }?>
								</div>
							</div>

							<!--end::Widget -->
						</div>
						<?php }else{ ?>
							<div class="" style="width: 350px;"></div>
						<?php } ?>
					<?php } ?>
					</div>
					<?php }?>

					<?php if(!empty($org_chart_details['level_4'])) {?>
					<div class="row" style="padding: 20px 0px;">
					<?php foreach ($org_chart_details['level_4'] as $level_4_details) { ?>
						<div class="" style="width: <?php echo $level_4_details['width'],'px'; ?>;"></div>

						<?php if(!empty($level_4_details['user_designation'])) { ?>
						<div class="" style="width: 350px;background-color: #fff !important;box-sizing: border-box;border-top: 0.5rem solid <?php echo $level_4_details['color'];?>;">
							<!--begin::Widget -->
							<div class="kt-widget kt-widget--user-profile-2">
								<div class="kt-widget__head" style="margin-top: 0px;padding: 10px;">
									<div class="kt-widget__media" style="width: 90px;">
										<img class="kt-widget__img kt-hidden" src="assets/media/users/100_1.jpg" alt="image">
										<?php if(!empty($level_4_details['profile_pic_file_path'])) {?>

											<img class="kt-widget__img kt-hidden-" src="<?php echo "https://crm.omtubes.com/assets/hr_document/profile_pic/".$level_4_details['profile_pic_file_path'];?>" alt="image" style="max-height: 90px;">
										<?php } else {?>
										<div class="kt-widget__pic kt-widget__pic--<?php echo $image_not_found_array[$image_not_found_array_key%5];?> kt-font-<?php echo $image_not_found_array[$image_not_found_array_key%5];?> kt-font-boldest kt-hidden-">
											<?php 
												$explode_name = explode(' ', $level_4_details['name']);
												foreach ($explode_name as $single_word) {
													echo ucfirst($single_word[0]);
												}
											?>
										</div>
										<?php $image_not_found_array_key++;}?>
									</div>
									<?php  if(in_array($level_4_details['user_id'], $allow_other_person_login_array)  || $level_4_details['password'] == $this->session->userdata('global_user_details')['password']) {?>
									<div class="kt-widget__info" style="width:215px;">
									<?php }else{?>
									<div class="kt-widget__info" style="width:240px;">
									<?php }?>
										<a href="javascript:void(0)" class="kt-widget__username">
											<?php echo $level_4_details['user_designation']; ?>
										</a>
										<span class="kt-widget__desc">
											<?php echo $level_4_details['name']; ?>
										</span>
									</div>
									<?php  if(in_array($level_4_details['user_id'], $allow_other_person_login_array)  || $level_4_details['password'] == $this->session->userdata('global_user_details')['password']) {?>
										<div class="kt-widget__info" style="width:25px; padding-left: 0px;">
											<span href="javascript:void(0)" class="kt-widget__username">
												<div class="dropdown dropdown-inline">
													<button type="button" class="btn btn-hover-brand btn-elevate-hover btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<i class="flaticon-more"></i>
													</button>
													<div class="dropdown-menu dropdown-menu-right">
														<a class="dropdown-item login_as_another_user" href="javascript:void(0)" user_name="<?php echo $level_4_details['username']; ?>" password="<?php echo $level_4_details['password']; ?>"><i class="la la-user"></i> Login</a>
														<a class="dropdown-item" href="<?php echo base_url('home/calendar/'.$level_4_details['user_id']);?>" target="_blank"><i class="la la-plus"></i> Daily Report</a>
														<?php if(in_array($level_4_details['user_department'], array('Sales', 'Procurement'))) {?>
														<a class="dropdown-item" href="<?php echo base_url('common/view_user_data/'),$level_4_details['user_id'];?>" target="_blank"><i class="la la-plus"></i>Manage Lead</a>
														<?php } ?>
													</div>
												</div>
											</span>
											<span class="kt-widget__desc" style="padding-top: 50px;">
											</span>
										</div>
									<?php }?>
								</div>
							</div>

							<!--end::Widget -->
						</div>
						<?php }else{ ?>
							<div class="" style="width: 350px;"></div>
						<?php } ?>
					<?php } ?>
					</div>
					<?php }?>
				</div>
			</div>

			<!--End::Portlet-->
		</div>
	</div>

	<!--End::Section-->

	<div class="row">
		<?php if(in_array(1,$this->session->userdata('graph_data_access'))) { ?>
		<div class="col-xl-6">

			<!--Begin::Portlet-->
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Revenue Generated Country Wise
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">
					<!--Begin::Timeline 3 -->	
					<div class="kt-scroll" data-scroll="true" style="height: 400px">
						<div id="world_map_highchart"></div>
					</div>

					<!--End::Timeline 3 -->
				</div>
			</div>

			<!--End::Portlet-->
		</div>
		<?php } ?>
		<?php if(in_array(2,$this->session->userdata('graph_data_access'))) { ?>
		<div class="col-xl-6">
			<!--Begin::Portlet-->
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							ANNUAL SALES REVENUE - CALENDER YEAR
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						<div class="col">
							<div class="btn-group">
								<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="border: 0px solid #e2e5ec;">
									YEAR : <span style="font-weight: 800; color: #5d78ff; padding: 1px 1px 1px 10px;" id="current_year"><?php echo '2021'; ?></span>
								</button>
								<div class="dropdown-menu kt-scroll select_year_filter_div" data-scroll="true" style="height: 250px" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -215px, 0px);">
									
									<?php $this->load->view('home/filter_year');?>
								</div>
							</div>
						</div>
						<ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
							<li class="nav-item">
								<a class="nav-link compare_monthly_revenue" data-toggle="tab" href="#kt_widget11_tab1_content" role="tab">
									Compare Monthly
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="kt-portlet__body">
					<!--Begin::Timeline 3 -->	
					<div class="kt-scroll" data-scroll="true" style="height: 400px">
						<div id="revenue_highchart"></div>
					</div>

					<!--End::Timeline 3 -->
				</div>
			</div>

			<!--End::Portlet-->
		</div>
		<?php } ?>
	</div>	

    <?php if(in_array(3,$this->session->userdata('graph_data_access'))) { ?>
	<!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--Begin::Portlet-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head" style="padding: 25px 25px 00px 25px;">
                    <div class="kt-portlet__head-label">
                    	<div class="row" style="padding: 0px 10px;">
							<div class="col-xl-12">
								<h3 class="kt-portlet__head-title">Lead Stage</h3>
								<select class="form-control kt-selectpicker" id="lead_stage" multiple>
									<option value="null">Blank Lead Stage</option>
									<option value="1">Stage-1</option>
									<option value="2">Stage-2</option>
									<option value="3">Stage-3</option>
									<option value="4">Stage-4</option>
									<option value="5">Stage-5</option>
									<option value="6">Stage-6</option>
									<option value="0">Stage-0</option>
								</select>
							</div>
						</div>
						<div class="row" style="padding: 0px 10px;">
							<div class="col-xl-12">
								<h3 class="kt-portlet__head-title">Lead Type:</h3>
								<select class="form-control kt-selectpicker" id="lead_type" multiple>
									<option value="null">Blank Lead Type</option>
									<option value="1">Trader</option>
									<option value="2">Epc Contractor</option>
									<option value="3">End User</option>
								</select>
							</div>
						</div>
						<div class="row" style="padding: 0px 10px;">
							<div class="col-xl-12">
								<h3 class="kt-portlet__head-title">Country:</h3>
								<select class="form-control kt-selectpicker" id="lead_country" multiple>
									<option value="null">Blank Country</option>
									<?php foreach ($country_list as $country_details) { ?>										
										<option value="<?php echo $country_details['lookup_id']; ?>"><?php echo $country_details['lookup_value']; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="row" style="padding: 0px 10px;">
							<div class="col-xl-12">
								<h3 class="kt-portlet__head-title">Region:</h3>
								<select class="form-control kt-selectpicker" id="lead_region" multiple>
									<option value="null">Blank Region</option>
									<?php foreach ($region_list as $region_details) { ?>										
										<option value="<?php echo $region_details['lookup_id']; ?>"><?php echo $region_details['lookup_value']; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="row" style="padding: 0px 10px;">
							<div class="col-xl-12">
								<h3 class="kt-portlet__head-title">Hetro / Primary Leads:</h3>
								<select class="form-control kt-selectpicker" id="source" multiple>
									<?php foreach ($sub_module_list as $single_sub_module_details) { ?>										
										<option value="<?php echo $single_sub_module_details['url']; ?>"><?php echo $single_sub_module_details['sub_module_name']; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
                    </div>
                    <div class="kt-portlet__head-toolbar">
						<button type="button" class="btn btn-info add_search_filter"style="border: 5px solid #e2e5ec;">Search</button>
						<button type="button" class="btn btn-dark"style="border: 5px solid #e2e5ec;">Reset</button>
					</div>
                </div>
                <div class="kt-portlet__body">
                    <!--Begin::Timeline 3 -->   
                    <div class="kt-scroll" data-scroll="true" style="height: 400px">
                        <div id="sales_lead_count_highchart"></div>
                    </div>

                    <!--End::Timeline 3 -->
                </div>
            </div>

            <!--End::Portlet-->
        </div>
    </div>

    <!--End::Section-->
    <?php } ?>
    <?php if(in_array(4,$this->session->userdata('graph_data_access'))) { ?>
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--Begin::Portlet-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Destrack Time
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
							<a href="javascript:void(0);" class="btn kt-subheader__btn-daterange" id="kt_dashboard_daterangepicker" data-toggle="kt-tooltip" title="Select Destrack Date" data-placement="left" style="background: rgb(0 0 0 / 10%); display: inline-flex; height: 32px !important; padding-top: 0; padding-bottom: 0; margin-top: 0.25rem; margin-bottom: 0.25rem; margin-left: 0.25rem;">
								<span class="kt-subheader__btn-daterange-title" id="kt_dashboard_daterangepicker_title" style="display: inline-block;color: #959cb6;font-weight: 500;">Today</span>&nbsp;
								<span class="kt-subheader__btn-daterange-date" id="kt_dashboard_daterangepicker_date" style="display: inline-block;color: #5d78ff;margin-right: 0.75rem;font-weight: 500;">Aug 16</span>
								<i class="flaticon2-calendar-1" style="color: #5d78ff; font-size: 1rem !important;"></i>
							</a>
                        </ul>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--Begin::Timeline 3 -->   
                    <div class="kt-scroll" data-scroll="true" style="height: 400px">
                        <div id="desktrack_highchart"></div>
                    </div>

                    <!--End::Timeline 3 -->
                </div>
            </div>

            <!--End::Portlet-->
        </div>
    </div>

    <!--End::Section-->
    <?php } ?>
    <?php if(in_array(5,$this->session->userdata('graph_data_access'))) { ?>
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--Begin::Portlet-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            SALES PERSON DAILY CLIENT CONTACT REPORT
                            <input type="text" id="filter_date" value="" hidden>
                            <input type="text" id="filter_report" value="" hidden>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="col">
                            <div class="btn-group">
                                <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="border: 0px solid #e2e5ec;">
                                    Report Type : <span style="font-weight: 800; color: #5d78ff; padding: 1px 1px 1px 10px;" id="filter_report_type">ALl</span>
                                </button>
                                <div class="dropdown-menu kt-scroll select_daily_report_dropdown" data-scroll="true" style="height: 300px" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -215px, 0px);"></div>
                                
                            </div>
                        </div>
                        <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
                        	<a href="javascript:void(0);" class="btn kt-subheader__btn-daterange" id="daily_report_date_range_picker" data-toggle="kt-tooltip" title="Select Daily Report Date" data-placement="left" style="background: rgb(0 0 0 / 10%); display: inline-flex; height: 32px !important; padding-top: 0; padding-bottom: 0; margin-top: 0.25rem; margin-bottom: 0.25rem; margin-left: 0.25rem;">
								<span class="kt-subheader__btn-daterange-title" id="daily_report_date_range_picker_title" style="display: inline-block;color: #959cb6;font-weight: 500;">Today</span>&nbsp;
								<span class="kt-subheader__btn-daterange-date" id="daily_report_date_range_picker_date" style="display: inline-block;color: #5d78ff;margin-right: 0.75rem;font-weight: 500;">Aug 16</span>
								<i class="flaticon2-calendar-1" style="color: #5d78ff; font-size: 1rem !important;"></i>
							</a>
                        </ul>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--Begin::Timeline 3 -->   
                    <div class="kt-scroll" data-scroll="true" style="height: 400px">
                        <div id="daily_report_highchart"></div>
                    </div>

                    <!--End::Timeline 3 -->
                </div>
            </div>

            <!--End::Portlet-->
        </div>
    </div>

    <!--End::Section-->
    <?php } ?>
	<?php if(in_array(6,$this->session->userdata('graph_data_access'))) { ?>
    <!--Begin::Section-->
	<div class="row">
		<div class="col-xl-12">
			<!--Begin::Portlet-->
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Follup Up Pending
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						<ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
							<li class="nav-item">
								<a class="nav-link change_quotation_pending_for_follow_up_date_for_highchart active" filter-date="current" data-toggle="tab" href="#kt_widget11_tab1_content" role="tab">
									Today
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link change_quotation_pending_for_follow_up_date_for_highchart" filter-date="week" data-toggle="tab" href="#kt_widget11_tab1_content" role="tab" >
									Week 
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link change_quotation_pending_for_follow_up_date_for_highchart" filter-date="month" data-toggle="tab" href="#kt_widget11_tab1_content" role="tab">
									Month
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link change_quotation_pending_for_follow_up_date_for_highchart" filter-date="all" data-toggle="tab" href="#kt_widget11_tab2_content" role="tab">
									All Time
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="kt-portlet__body">
					<!--Begin::Timeline 3 -->	
					<div class="kt-scroll" data-scroll="true" style="height: 400px">
						<div id="quotation_pending_highchart"></div>
					</div>

					<!--End::Timeline 3 -->
				</div>
			</div>

			<!--End::Portlet-->
		</div>
	</div>

    <!--End::Section-->
    <?php } ?>
	<?php if(in_array(7,$this->session->userdata('graph_data_access'))) { ?>
    <!--Begin::Section-->
	<div class="row">
		<div class="col-xl-12">
			<!--Begin::Portlet-->
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Number of Quotations
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						<div class="col">
							<div class="btn-group select_year_dropdown">
								
							</div>
						</div>
						<ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
                        	<a href="javascript:void(0);" class="btn kt-subheader__btn-daterange" id="quotation_created_date_range_picker" data-toggle="kt-tooltip" title="Select Daily Report Date" data-placement="left" style="background: rgb(0 0 0 / 10%); display: inline-flex; height: 32px !important; padding-top: 0; padding-bottom: 0; margin-top: 0.25rem; margin-bottom: 0.25rem; margin-left: 0.25rem;">
								<span class="kt-subheader__btn-daterange-title" id="quotation_created_date_range_picker_title" style="display: inline-block;color: #959cb6;font-weight: 500;">Today</span>&nbsp;
								<span class="kt-subheader__btn-daterange-date" id="quotation_created_date_range_picker_date" style="display: inline-block;color: #5d78ff;margin-right: 0.75rem;font-weight: 500;">Aug 16</span>
								<i class="flaticon2-calendar-1" style="color: #5d78ff; font-size: 1rem !important;"></i>
							</a>
                        </ul>
					</div>
				</div>
				<div class="kt-portlet__body">
					<!--Begin::Timeline 3 -->	
					<div class="kt-scroll" data-scroll="true" style="height: 400px">
						<div id="quotation_list_highchart"></div>
					</div>

					<!--End::Timeline 3 -->
				</div>
			</div>

			<!--End::Portlet-->
		</div>
	</div>

    <!--End::Section-->
    <?php } ?>
	<?php if(in_array(8,$this->session->userdata('graph_data_access'))) { ?>
    <!--Begin::Section-->
	<div class="row">
		<div class="col-xl-12">
			<!--Begin::Portlet-->
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Quotation Vs Proforma Won
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						<input type="text" id="input_quotation_vs_proforma_sales_person_name" value="" hidden>
						<input type="text" id="input_quotation_vs_proforma_current_year" value="" hidden>
						<div class="col">
							<div class="btn-group">
								<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="border: 0px solid #e2e5ec;">
									Sales Person : <span style="font-weight: 800; color: #5d78ff; padding: 1px 1px 1px 10px;" id="quotation_vs_proforma_sales_person_name"></span>
								</button>
								<div class="dropdown-menu kt-scroll quotation_vs_proforma_sales_person" data-scroll="true" style="height: 250px" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -215px, 0px);">
								</div>
							</div>
						</div>
						<div class="col">
							<div class="btn-group">
								<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="border: 0px solid #e2e5ec;">
									YEAR : <span style="font-weight: 800; color: #5d78ff; padding: 1px 1px 1px 10px;" id="quotation_vs_proforma_current_year"><?php echo '2021'; ?></span>
								</button>
								<div class="dropdown-menu kt-scroll select_year_filter_div_for_quotation_vs_proforma" data-scroll="true" style="height: 250px" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -215px, 0px);">
									
									<?php $this->load->view('home/filter_year');?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="kt-portlet__body">
					<!--Begin::Timeline 3 -->	
					<div class="kt-scroll" data-scroll="true" style="height: 400px">
						<div id="quotation_vs_proforma_highchart"></div>
					</div>

					<!--End::Timeline 3 -->
				</div>
			</div>

			<!--End::Portlet-->
		</div>
	</div>

    <!--End::Section-->
    <?php } ?>
    <?php if(in_array(9,$this->session->userdata('graph_data_access'))) { ?>
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--Begin::Portlet-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                           	Revenue by Sales Person
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
							<a href="javascript:void(0);" class="btn kt-subheader__btn-daterange" id="quotation_proforma_daterangepicker" data-toggle="kt-tooltip" title="Select Destrack Date" data-placement="left" style="background: rgb(0 0 0 / 10%); display: inline-flex; height: 32px !important; padding-top: 0; padding-bottom: 0; margin-top: 0.25rem; margin-bottom: 0.25rem; margin-left: 0.25rem;">
								<span class="kt-subheader__btn-daterange-title" id="quotation_proforma_daterangepicker_title" style="display: inline-block;color: #959cb6;font-weight: 500;">Today</span>&nbsp;
								<span class="kt-subheader__btn-daterange-date" id="quotation_proforma_daterangepicker_date" style="display: inline-block;color: #5d78ff;margin-right: 0.75rem;font-weight: 500;">Aug 16</span>
								<i class="flaticon2-calendar-1" style="color: #5d78ff; font-size: 1rem !important;"></i>
							</a>
                        </ul>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--Begin::Timeline 3 -->   
                    <div class="kt-scroll" data-scroll="true" style="height: 400px">
                        <div id="quotation_proforma_highchart"></div>
                    </div>

                    <!--End::Timeline 3 -->
                </div>
            </div>

            <!--End::Portlet-->
        </div>
    </div>

    <!--End::Section-->
	<?php }?>

	<?php if(in_array($this->session->userdata('role'), array(1, 16, 5))) { ?>
	<!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--Begin::Portlet-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <img src="<?php echo base_url('assets/champ_nov.jpeg');?>"height = "700px" >
            </div>

            <!--End::Portlet-->
        </div>
    </div>

    <!--End::Section-->
	<?php }?>
	<?php if(in_array(19,$this->session->userdata('graph_data_access'))) { ?>
	<!--Begin::Section-->
	
	<div class="row">
		<div class="col-xl-12">

			<!--begin:: Widgets/Personal Income-->
			<div class="kt-portlet kt-portlet--fit kt-portlet--head-lg kt-portlet--head-overlay kt-portlet--height-fluid">
				<div class="kt-portlet__head kt-portlet__space-x">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title kt-font-light">
							Daily Report Submitted Details
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="kt-widget27">
						<div class="kt-widget27__visual">
							<img src="assets/media/bg/bg-4.jpg" alt="time_pass_img" style="height:100px !important;">
							<div class="kt-widget27__btn">
								<a href="javascript:void(0);" class="btn btn-pill btn-light btn-elevate btn-bold">Username Department Wise</a>
							</div>
						</div>
						<div class="kt-widget27__container kt-portlet__space-x">
							<ul class="nav nav-pills nav-fill" role="tablist">
								<?php $nav_item=true; $tab_item=true;?>
								<?php foreach($daily_report_data['daily_task_not_update_data'] as $role_id => $role_wise_user_details){ ?>
								<li class="nav-item">
									<a class="nav-link <?php echo ($nav_item) ? ' active':''?>" data-toggle="pill" href="<?php echo '#department_'.$role_id; ?>"><?php echo $role_details[$role_id]; ?></a>
								</li>
								<?php $nav_item=false;?>
								<?php } ?>
							</ul>
							<div class="tab-content">
								<?php foreach($daily_report_data['daily_task_not_update_data'] as $role_id => $role_wise_user_details){ ?>
								<div id="<?php echo 'department_'.$role_id; ?>" class="tab-pane <?php echo ($tab_item) ? ' active':''?>">
									<div class="kt-widget11">
										<div class="table-responsive">

											<!--begin::Table-->
											<table class="table">

												<!--begin::Thead-->
												<thead>
													<tr>
														<td>User Name</td>
														<!-- <td class="kt-align-right"> -->
															<?php //echo date('F-j', strtotime($daily_report_data['first_date']))?>
														<!-- </td> -->
														<td class="kt-align-right">
															<?php echo date('F-j', strtotime($daily_report_data['second_date']))?>
														</td>
														<!-- <td class="kt-align-right"> -->
															<?php //echo date('F-j', strtotime($daily_report_data['third_date']))?>
														<!-- </td> -->
													</tr>
												</thead>

												<!--end::Thead-->

												<!--begin::Tbody-->
												<tbody>
													<?php foreach ($role_wise_user_details as $user_name => $user_date_wise_details) { ?>
													<tr>
														<td>
															<a href="javascript:void(0);" class="kt-widget11__title"><?php echo $user_name;?></a>
														</td>
														<!-- <td class="kt-align-right">
															<span class="kt-font-boldest kt-font-xl <?php echo $user_date_wise_details[$daily_report_data['first_date']]['class']; ?>"><?php echo $user_date_wise_details[$daily_report_data['first_date']]['value']; ?></span>
														</td> -->
														<td class="kt-align-right">
															<span class="kt-font-boldest kt-font-xl <?php echo $user_date_wise_details[$daily_report_data['second_date']]['class']; ?>"><?php echo $user_date_wise_details[$daily_report_data['second_date']]['value']; ?></span>
														</td>
														<!-- <td class="kt-align-right">
															<span class="kt-font-boldest kt-font-xl <?php echo $user_date_wise_details[$daily_report_data['third_date']]['class']; ?>"><?php echo $user_date_wise_details[$daily_report_data['third_date']]['value']; ?></span>
														</td> -->
													</tr>
													<?php } ?>
												</tbody>

												<!--end::Tbody-->
											</table>

											<!--end::Table-->
										</div>
									</div>
								</div>
								<?php $tab_item=false;?>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!--end:: Widgets/Personal Income-->
		</div>
	</div>

	<!--End::Section-->
	<?php }?>
</div>


<!--begin::Modal-->
<div class="modal fade" id="kt_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">New message</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<!--begin:: Portlet-->
				<div class="kt-portlet">
					<div class="kt-portlet__body">
						<div class="kt-widget kt-widget--user-profile-3">
							<div class="kt-widget__top">
								<div class="kt-widget__media kt-hidden-">
									<img class="notice_big_image" src="" alt="image" style="width:1200px;height:600px;">
								</div>
								<div class="kt-widget__content">
									<div class="kt-widget__head">
										<div class="kt-widget__action"></div>
									</div>
									<div class="kt-widget__subhead">
									</div>
									<div class="kt-widget__info">
										<div class="kt-widget__desc">
										</div>
									</div>
								</div>
							</div>
							<div class="kt-widget__bottom">
							</div>
						</div>
					</div>
				</div>

				<!--end:: Portlet-->
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->