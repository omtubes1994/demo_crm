<?php 
class Procurement_model extends CI_Model{

	function __construct(){
		parent::__construct();
		$CI = &get_instance();
		$this->db2 = $CI->load->database('marketing', true);
	}
	
	function insertData($table, $data){
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	function updateData($table, $data, $where){
		$this->db->update($table, $data, $where);
	}

	function deleteData($table, $where){
		$this->db->delete($table, $where);
	}

	function getData($table, $where=''){
		if($where != ''){
			$this->db->where($where);
		}
		return $this->db->get($table)->result_array();
	}

	function getRfq($rfq_id){
		$this->db->join('rfq_dtl d', 'm.rfq_mst_id = d.rfq_mst_id', 'left');
		return $this->db->get_where('rfq_mst m', array('m.rfq_mst_id' => $rfq_id))->result_array();
	}

	// function getLeadName($client_id, $source){
	// 	if($source == 'main'){
	// 		$this->db->select('client_name');
	// 		$res = $this->db->get_where('clients', array('client_id' => $client_id))->row_array();
	// 		return $res['client_name'];
	// 	}else if($source == 'hetro leads'){
	// 		$this->db->select('company_name');
	// 		$res = $this->db->get_where('hetro_leads', array('lead_id' => $client_id))->row_array();
	// 		return $res['company_name'];
	// 	}else if($source == 'primary leads'){
	// 		$this->db2->select('importer_name');
	// 		$res = $this->db2->get_where('lead_mst', array('lead_mst_id' => $client_id))->row_array();
	// 		return $res['importer_name'];
	// 	}
	// }

	function getLeadName($client_id){
		
			$this->db->select('name');
			$res = $this->db->get_where('customer_mst', array('id' => $client_id))->row_array();
			return $res['name'];
		
	}

	function getRfqs(){
		$this->db->select('*');
		return $this->db->get('rfq')->result_array();
	}

	/*function getRfqDetails($rfq_id){
		return $this->db->get_where('rfq', array('rfq_id' => $rfq_id))->row_array();
	}*/

	// function getClients($search=''){
	// 	$client_arr = array();
	// 	$this->db->select('c.client_id, c.client_name, l.lookup_value country');
	// 	$this->db->join('lookup l', 'l.lookup_id = c.country', 'left');
	// 	if($search != ''){
	// 		$this->db->where("c.client_name like '%".$search."%'");
	// 	}
	// 	$clients = $this->db->get('clients c')->result_array();
	// 	$i = 0;
	// 	foreach ($clients as $c) {
	// 		$client_arr[$i]['client_id'] = $c['client_id'];
	// 		$client_arr[$i]['client_name'] = $c['client_name'];
	// 		$client_arr[$i]['client_rank'] = '';
	// 		$client_arr[$i]['last_purchased'] = '';
	// 		$client_arr[$i]['client_source'] = 'main';
	// 		$client_arr[$i]['country'] = $c['country'];
	// 		$i++;
	// 	}

	// 	/*$this->db->select('lead_id, company_name');
	// 	if($search != ''){
	// 		$this->db->where("company_name like '%".$search."%'");
	// 	}
	// 	$hetro_leads = $this->db->get('hetro_leads')->result_array();
	// 	foreach ($hetro_leads as $hl) {
	// 		$client_arr[$i]['client_id'] = $hl['lead_id'];
	// 		$client_arr[$i]['client_name'] = $hl['company_name'];
	// 		$client_arr[$i]['client_rank'] = '';
	// 		$client_arr[$i]['last_purchased'] = '';
	// 		$client_arr[$i]['client_source'] = 'hetro leads';
	// 		$i++;
	// 	}*/

	// 	$this->db2->select('lead_mst_id, importer_name, rank, last_purchased, country_of_destination');
	// 	if($search != ''){
	// 		$this->db2->where("importer_name like '%".$search."%'");
	// 	}
	// 	$primary_leads = $this->db2->get('lead_mst')->result_array();
	// 	foreach ($primary_leads as $pl) {
	// 		$client_arr[$i]['client_id'] = $pl['lead_mst_id'];
	// 		$client_arr[$i]['client_name'] = $pl['importer_name'];
	// 		$client_arr[$i]['client_rank'] = $pl['rank'];
	// 		$client_arr[$i]['last_purchased'] = '';
	// 		if($pl['last_purchased'] != ''){
	// 			$client_arr[$i]['last_purchased'] = date('d-m-Y', strtotime($pl['last_purchased']));
	// 		}
	// 		$client_arr[$i]['client_source'] = 'primary leads';
	// 		$client_arr[$i]['country'] = $pl['country_of_destination'];
	// 		$i++;
	// 	}
	// 	return $client_arr;
	// }

	function getClients($search=''){
		$client_arr = array();
		$this->db->select('c.id, c.name, l.name country, p.rank, p.last_purchased');
		$this->db->join('country_mst l', 'l.id = c.country_id', 'left');
		$this->db->join('customer_data p', 'p.customer_mst_id = c.id', 'left');
		if($search != ''){
			$this->db->where("c.name like '%".$search."%'");
		}
		$clients = $this->db->get('customer_mst c')->result_array();
		$i = 0;
		foreach ($clients as $c) {
			$client_arr[$i]['client_id'] = $c['id'];
			$client_arr[$i]['client_name'] = $c['name'];
			if($c['rank'] != ''){
				
				$client_arr[$i]['client_rank'] = $c['rank'];
			}
			if($c['last_purchased'] != ''){
				$client_arr[$i]['last_purchased'] = date('d-m-Y', strtotime($c['last_purchased']));
			}
			$client_arr[$i]['country'] = $c['country'];
			$i++;
		}

		/*$this->db->select('lead_id, company_name');
		if($search != ''){
			$this->db->where("company_name like '%".$search."%'");
		}
		$hetro_leads = $this->db->get('hetro_leads')->result_array();
		foreach ($hetro_leads as $hl) {
			$client_arr[$i]['client_id'] = $hl['lead_id'];
			$client_arr[$i]['client_name'] = $hl['company_name'];
			$client_arr[$i]['client_rank'] = '';
			$client_arr[$i]['last_purchased'] = '';
			$client_arr[$i]['client_source'] = 'hetro leads';
			$i++;
		}*/

		// $this->db2->select('lead_mst_id, importer_name, rank, last_purchased, country_of_destination');
		// if($search != ''){
		// 	$this->db2->where("importer_name like '%".$search."%'");
		// }
		// $primary_leads = $this->db2->get('lead_mst')->result_array();
		// foreach ($primary_leads as $pl) {
		// 	$client_arr[$i]['client_id'] = $pl['lead_mst_id'];
		// 	$client_arr[$i]['client_name'] = $pl['importer_name'];
		// 	$client_arr[$i]['client_rank'] = $pl['rank'];
		// 	$client_arr[$i]['last_purchased'] = '';
		// 	if($pl['last_purchased'] != ''){
		// 		$client_arr[$i]['last_purchased'] = date('d-m-Y', strtotime($pl['last_purchased']));
		// 	}
		// 	$client_arr[$i]['client_source'] = 'primary leads';
		// 	$client_arr[$i]['country'] = $pl['country_of_destination'];
		// 	$i++;
		// }
		return $client_arr;
	}

	// function getMembers($data){
	// 	$members = array();
	// 	if($data['source'] == 'main'){
	// 		$mem = $this->db->get_where('members', array('client_id' => $data['client_id']))->result_array();
	// 		foreach ($mem as $key => $value) {
	// 			$members[$key]['member_id'] = $value['member_id'];
	// 			$members[$key]['member_name'] = $value['name'];
	// 		}
	// 	}else if($data['source'] == 'hetro leads'){
	// 		$mem = $this->db->get_where('hetro_lead_detail', array('lead_id' => $data['client_id']))->result_array();
	// 		foreach ($mem as $key => $value) {
	// 			$members[$key]['member_id'] = $value['lead_dtl_id'];
	// 			$members[$key]['member_name'] = $value['member_name'];
	// 		}
	// 	}else if($data['source'] == 'primary leads'){
	// 		$mem = $this->db2->get_where('lead_detail', array('lead_mst_id' => $data['client_id']))->result_array();
	// 		foreach ($mem as $key => $value) {
	// 			$members[$key]['member_id'] = $value['lead_dtl_id'];
	// 			$members[$key]['member_name'] = $value['member_name'];
	// 		}
	// 	}
	// 	return $members;
	// }

	function getMembers($data){
		$members = array();

		$mem = $this->db->get_where('customer_dtl', array('comp_mst_id' => $data['client_id']))->result_array();
		foreach ($mem as $key => $value) {
				$members[$key]['member_id'] = $value['comp_dtl_id'];
				$members[$key]['member_name'] = $value['member_name'];
		}		
		return $members;
	}

	function getRFQListData($start, $length, $search, $order, $dir){
		// echo "<pre>";print_r($search);echo"</pre><hr>";exit;
		if($search['rfq_company'] != ''){
			$searched_clients = $this->getClients($search['rfq_company']);
		}

		$this->db->select('r.*, u1.name sentby_name, u2.name assigned_to_name, DATE_FORMAT(r.rfq_date, "%b/%d/%Y") rfq_date, DATE_FORMAT(r.rfq_closedate, "%b/%d/%Y") rfq_closedate, priority');//, q.stage quote_status
		//$this->db->distinct();
		$this->db->join('users u1', 'u1.user_id = r.rfq_sentby', 'inner');
		$this->db->join('users u2', 'u2.user_id = r.assigned_to', 'inner');
		//$this->db->join('quotation_mst q', 'q.rfq_id = r.rfq_mst_id', 'left');
		$this->db->limit($length, $start);
		$this->db->order_by($order, $dir);
		if(!empty($search)){
			foreach ($search as $key => $value) {
				if($value != ''){
					if($key == 'rfq_no'){
						$this->db->where($key." like '%".$value."%'");
					} else if($key == 'rfq_company' && !empty($searched_clients)){
						$this->db->group_start();
						foreach ($searched_clients as $key => $value) {
							$this->db->or_where("(rfq_company = ".$value['client_id']." and client_source = '".$value['client_source']."')");
						}
						$this->db->group_end();
					} else if($key == 'rfq_closedate' && !empty($value)){
						$explode = explode(' - ', $value);
						// echo "<pre>";print_r($explode);echo"</pre><hr>";exit;
						if(count($explode) == 1) {

							$this->db->where($key, date('Y-m-d',strtotime($value)));
						}else {
							if(date('Y-m-d', strtotime($explode[0])) == date('Y-m-d', strtotime($explode[1]))) {

								$this->db->where($key, date('Y-m-d',strtotime($explode[0])));
							}else {
								$this->db->where(array($key.' >='=>date('Y-m-d',strtotime($explode[0])), $key.' <='=>date('Y-m-d',strtotime($explode[1]))));
							}
						}
					} else if($key == 'r.assigned_to' && !empty($value)){

						$this->db->where("(assigned_to_1 = '{$value}' || assigned_to_2 = '{$value}' || assigned_to_3 = '{$value}')", null, false);
					} /*else if($key == 'quote_status'){
						$this->db->where("q.stage = '".$value."'");
					}*/ else{
						$this->db->where($key, $value);
					}
				}
			}
		}
		if($this->session->userdata('role') == 5){
			$this->db->where('rfq_sentby', $this->session->userdata('user_id'));
		}
		if($this->session->userdata('role') == 8){
			$this->db->where('r.assigned_to', $this->session->userdata('user_id'));
		}

		$res = $this->db->get('rfq_mst r')->result_array();
		// echo $this->db->last_query(),"<hr>";die('debug');
		$user_details = array_column($this->db->get_where('users', array('user_id >'=>0))->result_array(), 'name', 'user_id');
		$k=0;
		$result = array();
		foreach ($res as $key => $value) {
			$result[$key] = $value;
			if($value['client_source'] == 'main'){
				$clients = $this->db->get_where('clients', array('client_id' => $value['rfq_company']))->row_array();
				$result[$key]['company_name'] = $clients['client_name'];

				$members = $this->db->get_where('members', array('member_id' => $value['rfq_buyer']))->row_array();
				$result[$key]['member_name'] = $members['name'];
			}else if($value['client_source'] == 'hetro leads'){
				$clients = $this->db->get_where('hetro_leads', array('lead_id' => $value['rfq_company']))->row_array();
				$result[$key]['company_name'] = $clients['company_name'];

				$members = $this->db->get_where('hetro_lead_detail', array('lead_dtl_id' => $value['rfq_buyer']))->row_array();
				$result[$key]['member_name'] = $members['member_name'];
			}else if($value['client_source'] == 'primary leads'){
				$clients = $this->db2->get_where('lead_mst', array('lead_mst_id' => $value['rfq_company']))->row_array();
				$result[$key]['company_name'] = $clients['IMPORTER_NAME'];

				$members = $this->db2->get_where('lead_detail', array('lead_dtl_id' => $value['rfq_buyer']))->row_array();
				$result[$key]['member_name'] = $members['member_name'];
			}
			$result[$key]['record_id'] = ++$k;
			$result[$key]['quote_no'] = '';
			$result[$key]['quote_id'] = 0;
			$quote_arr = $this->db->get_where('quotation_mst', array('rfq_id' => $value['rfq_mst_id']))->row_array();
			if(!empty($quote_arr)){
				$result[$key]['quote_id'] = $quote_arr['quotation_mst_id'];
				if($quote_arr['stage'] == 'draft'){
					$result[$key]['quote_no'] = 'Draft';
				}else{
					$result[$key]['quote_no'] = $quote_arr['quote_no'];	
				}
			}

			$result[$key]['query'] = '';
			$result[$key]['is_new'] = false;
			$query_res = $this->db->get_where('rfq_note_query', array('rfq_id' => $value['rfq_mst_id'], 'type' => 'query'))->result_array();
			if(!empty($query_res)){
				$sent_by = '';
				$result[$key]['query'] .= '<tr>
												<td>
													Description
												</td>
												<td>
													Date
												</td>
												<td>
													Person Name
												</td>
											</tr>';
				foreach ($query_res as $qkey => $qvalue) {
					$sent_by = $qvalue['entered_by'];
					$align = 'left';
					// if($this->session->userdata('user_id') == $qvalue['entered_by']){
					// 	$align = 'right';
					// }
			// echo "<pre>";print_r($user_details[$qvalue['entered_by']]);echo"</pre><hr>";exit;
					$result[$key]['query'] .= '<tr>
													<td>
														'.$qvalue['note'].'
													</td>
													<td>
														'.date('d M H:i', strtotime($qvalue['entered_on'])).'
													</td>
													<td>
														'.$user_details[$qvalue['entered_by']].'
													</td>
												</tr>';
				}
				if($this->session->userdata('user_id') != $sent_by){
					$result[$key]['is_new'] = true;
				}
			}

			$result[$key]['notes'] = '';
			$notes_res = $this->db->get_where('rfq_note_query', array('rfq_id' => $value['rfq_mst_id'], 'type' => 'notes'))->result_array();
			if(!empty($query_res)){
				$sent_by = '';
				foreach ($query_res as $qkey => $qvalue) {
					$sent_by = $qvalue['entered_by'];
					$result[$key]['notes'] .= '<tr><td>'.$qvalue['note'].'<br/><span style="font-size: 10px; text-align:right;">'.date('d M H:i', strtotime($qvalue['entered_on'])).'</span>
					</td></tr>';
				}
			}

			$this->db->join('quotation_mst m', 'm.quotation_mst_id = q.query_for_id', 'inner');
			$this->db->join('rfq_mst r', 'r.rfq_mst_id = m.rfq_id', 'inner');
			$query_res = $this->db->get_where('query_mst q', array('r.rfq_mst_id' => $value['rfq_mst_id']))->row_array();
			if(!empty($query_res)){
				$result[$key]['has_query'] = true;
				$result[$key]['quotation_mst_id'] = $query_res['quotation_mst_id'];
				$result[$key]['query_id'] = $query_res['query_id'];
				$result[$key]['query_type'] = $query_res['query_type'];
			}else{
				$result[$key]['has_query'] = false;
				$result[$key]['quotation_mst_id'] = '';
				$result[$key]['query_id'] = '';
				$result[$key]['query_type'] = '';
			}

			$quotation_details = $this->db->get_where('quotation_mst', array('rfq_id'=>$value['rfq_mst_id']))->row_array();
			$result[$key]['priority'] = $quotation_details['quotation_priority'];
			$result[$key]['priority_reason'] = $quotation_details['priority_reason'];
			$result[$key]['priority_div'] = '<div class="kt-font-warning kt-demo-icon__preview" data-toggle="kt-tooltip" data-placement="left" title="'.$quotation_details["priority_reason"].'" data-original-title="Tooltip title">';
			for ($i=0; $i < 5; $i++) { 
			
				if($i < $quotation_details['quotation_priority']) {
					$result[$key]['priority_div'] .= '<i class="la la-star" style="line-height: 0;vertical-align: middle;font-size: 1.5rem !important;"></i>';
				}
			}
			$user_details[0] = '';
			$result[$key]['priority_div'] .= '</div>';
			$result[$key]['assigned_to_1'] = $this->creat_first_name($user_details[$value['assigned_to_1']]);
			$result[$key]['assigned_to_2'] = $this->creat_first_name($user_details[$value['assigned_to_2']]);
			$result[$key]['assigned_to_3'] = $this->creat_first_name($user_details[$value['assigned_to_3']]);
		}
		return $result;
	}

	private function creat_first_name($string){

		$return_string = "";

		if(!empty($string)){

			$string_explode = explode(" ", $string);
			$return_string = $string_explode[0];
		}
		return $return_string;
	}

	function getRFQListCount($search){
		if($search['rfq_company'] != ''){
			$searched_clients = $this->getClients($search['rfq_company']);
		}

		$this->db->select('r.*, u1.name sentby_name, u2.name assigned_to_name');
		$this->db->join('users u1', 'u1.user_id = r.rfq_sentby', 'inner');
		$this->db->join('users u2', 'u2.user_id = r.assigned_to', 'inner');
		//$this->db->join('quotation_mst q', 'q.rfq_id = r.rfq_mst_id', 'left');
		if(!empty($search)){
			foreach ($search as $key => $value) {
				if($value != ''){
					if($key == 'rfq_no'){
						$this->db->where($key." like '%".$value."%'");
					} else if($key == 'rfq_company' && !empty($searched_clients)){
						$this->db->group_start();
						foreach ($searched_clients as $key => $value) {
							$this->db->or_where("(rfq_company = ".$value['client_id']." and client_source = '".$value['client_source']."')");
						}
						$this->db->group_end();
					} /*else if($key == 'quote_status'){
						$this->db->where("q.stage = '".$value."'");
					}*/ else{
						$this->db->where($key, $value);
					}
				}
			}
		}
		if($this->session->userdata('role') == 5){
			$this->db->where('rfq_sentby', $this->session->userdata('user_id'));
		}
		if($this->session->userdata('role') == 8){
			$this->db->where('r.assigned_to', $this->session->userdata('user_id'));
		}
		$res = $this->db->get('rfq_mst r')->result_array();
		return sizeof($res);
	}

	function getRFQNo(){
		$res = $this->db->get_where('number_logic', array('logic_for' => 'rfq'))->row_array();
		$number = $res['logic_value'];
		$new_number = $number+1;
		$this->db->update('number_logic', array('logic_value' => $new_number), array('logic_for' => 'rfq'));
		return $number;
	}

	function getRFQDetails($connect_id){
		$this->db->select('rfq_id');
		$con = $this->db->get_where('rfq_to_vendor', array('connect_id' => $connect_id ))->row_array();
		$rfq_id = $con['rfq_id'];

		$this->db->select('r.*, rd.*, u.unit_value, us.name uname, us.email uemail, us.mobile umobile');
		$this->db->join('rfq_dtl rd', 'rd.rfq_mst_id = r.rfq_mst_id', 'left');
		$this->db->join('units u', 'u.unit_id = rd.unit', 'inner');
		$this->db->join('users us', 'us.user_id = r.assigned_to', 'inner');
		return $this->db->get_where('rfq_mst r', array('r.rfq_mst_id' => $rfq_id))->result_array();
	}

	function getVendorDetails($connect_id){
		$this->db->select('vendor_id');
		$con = $this->db->get_where('rfq_to_vendor', array('connect_id' => $connect_id ))->row_array();
		$vendor_id = $con['vendor_id'];

		$this->db->select('v.*, vd.*, l.lookup_value country');
		$this->db->join('vendor_dtl vd', 'vd.vendor_id = v.vendor_id', "inner and main_seller = 'yes'");
		$this->db->join('lookup l', 'l.lookup_id = v.country', 'left');
		return $this->db->get_where('vendors v', array('v.vendor_id' => $vendor_id))->row_array();
	}

	public function get_procurement_user_name($user_type = 'all') {

		$where_array = array();
		$where_array['status'] = 1;
		$this->db->where(array('status' => 1));
		if($user_type == 'all'){

			$this->db->where_in('role', array(6, 8));
		} else if($user_type == 'procurement_user') {

			$this->db->where_in('role', array(8));
		}
		return $this->db->get('users')->result_array();

	}

	public function get_pending_rfq_list($where_array) {

		$this->db->select("count('rfq_mst.assigned_to') as count, rfq_mst.assigned_to, users.name");
		$this->db->join('users', 'users.user_id = rfq_mst.assigned_to', 'LEFT');
		$this->db->where($where_array);
		$this->db->group_by('rfq_mst.assigned_to');
		$this->db->order_by('count', 'DESC');
		$res = $this->db->get('rfq_mst')->result_array();
		// echo $this->db->last_query(),"<hr>";
		// echo "<pre>";print_r($res);echo"</pre><hr>";exit;
		return $res;
	}

	public function get_rfq_graph_data($date_where, $rfq_status, $sales_person) {

		$this->db->select("assigned_to_1, assigned_to_2, assigned_to_3");
		$this->db->where("rfq_sentby != 0 AND (status_1 = '{$rfq_status}' || status_2 = '{$rfq_status}' || status_3 = '{$rfq_status}')", null, false);
		$this->db->where($date_where);
		if($sales_person != "All") {

			$this->db->where(array('rfq_sentby'=>$sales_person));
		}
		$res = $this->db->get('rfq_mst')->result_array();
		// echo $this->db->last_query(),"<hr>";
		// echo "<pre>";print_r($res);echo"</pre><hr>";exit;
		return $res;
	}

	public function get_sales_person($select) {

		$this->db->select($select);
		$this->db->where('status=1 AND role=5 OR role=16', null, false);
		return $this->db->get('users')->result_array();
	}

	public function get_procurement_person() {

		$this->db->select('name, user_id');
		$this->db->where('status=1 AND role=6 OR role=8', null, false);
		$res = $this->db->get('users')->result_array();
		return $res;
	}

	public function get_vendor_list() {

		$this->db->select('vendor_id, vendor_name');
		$this->db->where('status="Active"', null, false);
		return $this->db->get('vendors')->result_array();
	}

	public function get_vendor_sales_person() {

		$this->db->select('vendor_dtl_id, vendor_id, name, designation');
		$this->db->where('name != ""', null, false);
		return $this->db->get('vendor_dtl')->result_array();
	}

	public function get_lookup_data($lookup_group) {

		$this->db->select('lookup_id, lookup_value');
		$this->db->where('status = "Active" AND lookup_group = "'.$lookup_group.'"', null, false);
		return $this->db->get('lookup')->result_array();
	}

	public function get_units_data() {

		$this->db->select('*');
		$this->db->where('status = "Active"', null, false);
		return $this->db->get('units')->result_array();
	}

	public function get_all_data($select, $where_array, $table_name, $result_type = 'result_array') {

		$this->db->select($select);
		$this->db->where($where_array);
		return $this->db->get($table_name)->$result_type();		
	}

	public function get_procurement_listing_data($where_array, $order_by_array, $limit, $offset) {

		$return_array = array();
		$this->db->select('SQL_CALC_FOUND_ROWS *', FALSE);
		$this->db->where($where_array);
		$this->db->order_by($order_by_array['column_name'], $order_by_array['column_value']);
		$return_array['procurement_list'] = $this->db->get('procurement', $limit, $offset)->result_array();
		$return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows;")->row_array();

		return $return_array;
	}

	public function insert_data_batch($table_name, $insert_data) {

		$this->db->insert_batch($table_name, $insert_data);
	}

	public function get_quotation_proforma_highchart_data($where){
	 	
	 	$this->db->select('count(*) as count, made_by');
	 	$this->db->where($where,null,false);
	 	$this->db->group_by('made_by');
	 	$this->db->order_by('count', 'desc');
	 	$result=$this->db->get('quotation_mst')->result_array();
	 	return $result;
	}

	public function get_procurement_list($select) {

		$res = $this->db->query("
							SELECT ($select), 
							count('*') count, 
							count(IF(procurement_status= 'Issued', 1, NULL)) Issued,
							count(IF(procurement_status = 'InProduction', 1, NULL)) InProduction,
							count(IF(procurement_status= 'Delivered', 1, NULL)) Delivered,
							count(IF(procurement_status = 'Inspected', 1, NULL)) Inspected
							FROM procurement 
							WHERE (status = 'Active')
							group by ($select) 
							order by count DESC
						")->result_array();
		return $res;
	}

	public function get_rfq_list_data($where_array, $order_by_array, $limit, $offset){

		$return_array = array();
		$this->db->select('SQL_CALC_FOUND_ROWS *, rfq_mst.priority_reason as rfq_priority_reason, rfq_mst.client_type as rfq_client_type, rfq_mst.assigned_to_date', FALSE);
		if(!empty($where_array)) {

			$this->db->where($where_array, null, false);
		}
		if(!empty($order_by_array)) {

			$this->db->order_by($order_by_array['column_name'], $order_by_array['column_value']);
		}
		$this->db->join('quotation_mst', 'quotation_mst.rfq_id = rfq_mst.rfq_mst_id', 'left');
		$return_array['rfq_list'] = $this->db->get('rfq_mst', $limit, $offset)->result_array();
		// echo "<pre>";print_r($this->db->last_query());echo"</pre><hr>";exit;
		// echo "<pre>";print_r($return_array['rfq_list']);echo"</pre><hr>";exit;
		$return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows;")->row_array();
		return $return_array;
	}

	public function get_rfq_list_highchart_data($where) {

		$this->db->select("assigned_to_1, status_1, assigned_to_2, status_2, assigned_to_3, status_3");
		$this->db->where($where, null, false);
		$this->db->join('quotation_mst', 'quotation_mst.rfq_id = rfq_mst.rfq_mst_id', 'left');
		$res = $this->db->get('rfq_mst')->result_array();
		// echo $this->db->last_query(),"<hr>";
		// echo "<pre>";print_r($res);echo"</pre><hr>";exit;
		return $res;
	}

	public function get_sales_person_highchart_data($where) {

		$this->db->select("count(*) as count, rfq_mst.rfq_sentby");
		$this->db->where($where, null, false);
		$this->db->join('quotation_mst', 'quotation_mst.rfq_id = rfq_mst.rfq_mst_id', 'left');
		$this->db->group_by('rfq_mst.rfq_sentby');
		$res = $this->db->get('rfq_mst')->result_array();
		// echo $this->db->last_query(),"<hr>";
		// echo "<pre>";print_r($res);echo"</pre><hr>";exit;
		return $res;
	}

	public function get_same_rfq_list_data($where) {

		$this->db->select('SQL_CALC_FOUND_ROWS *', FALSE);
		$this->db->where($where, null, false);
		$this->db->join('quotation_mst', 'quotation_mst.rfq_id = rfq_mst.rfq_mst_id', 'left');
		$result = $this->db->get('rfq_mst')->result_array();
		// echo $this->db->last_query(),"<hr>";
		return $result;
	}

	public function get_rfq_list_piechart_data($where) {

		$this->db->select("count(*) as count, rfq_mst.product_family");
		$this->db->where("rfq_mst.product_family != ''");
		$this->db->where($where, null, false);
		$this->db->join('quotation_mst', 'quotation_mst.rfq_id = rfq_mst.rfq_mst_id', 'left');
		$this->db->group_by('product_family');
		$res = $this->db->get('rfq_mst')->result_array();
		// echo $this->db->last_query(),"<hr>";
		// echo "<pre>";print_r($res);echo"</pre><hr>";exit;
		return $res;
	}

	public function get_rfq_same_order_data($defult_where, $product_family){

		$query = ("
					SELECT
						SUM(count) AS order_count
					FROM (
						SELECT
							count(*) - 1 AS count
						FROM
							rfq_mst
						INNER JOIN
							`quotation_mst` ON `quotation_mst`.`rfq_id` = `rfq_mst`.`rfq_mst_id`
						WHERE
							$defult_where
							AND rfq_mst.product_family != ''
							AND rfq_mst.product_family IN ('".$product_family."')
						GROUP BY
							rfq_mst.rfq_company, rfq_mst.rfq_subject
					) AS total_sum_all
			");
		$result = $this->db->query($query)->row_array();
		// echo $this->db->last_query(),"<hr>";die;
		return $result;
	}

	public function get_vendor_name_on_product_name_and_material_name($product_name, $material_name){

		$this->db->select('vendors.vendor_id, vendors.vendor_name');
		$this->db->where("procurement.status = 'Active' AND vendors.vendor_name != '' AND (procurement_product_information.product_name IN('".implode("', '", $product_name)."') AND procurement_product_information.material_name IN('".implode("', '", $material_name)."'))");
		$this->db->join('procurement', 'procurement.vendor_name = vendors.vendor_name', 'left');
		$this->db->join('procurement_product_information', 'procurement_product_information.procurement_id = procurement.id', 'left');
		$this->db->group_by('procurement.vendor_name');
		$res = $this->db->get('vendors')->result_array();
		return $res;
		// echo "<pre>";print_r($res);echo"</pre><hr>";exit;
	}

	public function get_vendor_name_on_product_id_and_material_id($product_id, $material_id){

		$this->db->select('vendors.vendor_id, vendors.vendor_name');
		$this->db->where("vendors.status = 'Active' AND vendors.vendor_name != '' AND (vendor_products.product_id IN('".implode("', '", $product_id)."') AND vendor_products.material_id IN('".implode("', '", $material_id)."'))");
		$this->db->join('vendor_products', 'vendor_products.vendor_id = vendors.vendor_id', 'left');
		$this->db->group_by('vendors.vendor_name');
		$res = $this->db->get('vendors')->result_array();
		return $res;
	}

	public function get_client_name_on_id($cient_id){
		
		$this->db->select('c.id, c.name');
		$res = $this->db->get_where('customer_mst c', array('c.status' => 'Active', 'c.id'=>$cient_id))->result_array();
		return $res;
	}
} 
?>