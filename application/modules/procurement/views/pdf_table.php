<table class="table table-striped" style="margin: 0% 0% 0% 0%;">
	<thead>
		<tr>
			<th scope="col" class="kt-align-center" style="width:10%;">#</th>
			<th scope="col" class="kt-align-center" style="width:50%;">File Name</th>
			<th scope="col" class="kt-align-center" style="width:30%;">Date</th>
			<th scope="col" class="kt-align-center" style="width:20%;">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php if(!empty($pdf_details)) {?>
                <?php foreach(json_decode($pdf_details['file_path_and_type'], true) as $single_pdf_key => $single_pdf_details) {?>
				<tr>
					<th scope="row" class="kt-align-center"><?php echo $single_pdf_key+1;?></th>
					<td class="kt-font-bold kt-align-center"><?php echo $single_pdf_details['file_name']; ?></td>
					<td class="kt-font-bold kt-align-center"><?php echo date('d M, Y', strtotime($single_pdf_details['file_add_date'])); ?></td>
					<td class="kt-font-bold kt-align-center">
						<div class="row">
							<div class="col-md-12">

								<?php if($single_pdf_details['file_type'] == 'application/pdf'){ ?>

									<a class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View <?php echo $single_pdf_details['file_name']; ?>" href="<?php echo base_url('assets/rfq_files/'.$single_pdf_details['file_name']);?>" style="color: #212529;" target="_blank">
						    			<i class="la la-eye kt-font-bolder" style="color: #212529;"></i>
							    	</a>									
								<?php } else{ ?>

									<a class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View <?php echo $single_pdf_details['file_name']; ?>" href="<?php echo base_url('assets/rfq_files/'.$single_pdf_details['file_name']);?>" style="color: #212529;" target="_blank" download>
										<i class="fa fa-download kt-font-bolder" style="color: #212529;"></i>
									</a>
								<?php } ?>

								<?php if(in_array('file', $this->session->userdata('rfq_access')['rfq_document_delete_access'])){ ?>

									<button class="btn btn-sm btn-clean btn-icon btn-icon-md delete_rfq_file" title="Delete"  rfq_mst_id="<?php echo $pdf_details['rfq_mst_id'];?>" file_name="<?php echo $single_pdf_details['file_name'];?>" file_type="file" style="color: #212529;">
										<i class="la la-trash kt-font-bolder" style="color: #212529;"></i>
									</button>
								<?php } ?>
							</div>
						</div>
					</td>
				</tr>
                <?php } ?>
				<tr></tr>
				<?php foreach(json_decode($pdf_details['document_path_and_type'], true) as $single_pdf_key => $single_pdf_details) {?>
				<tr>
					<th scope="row" class="kt-align-center"><?php echo $single_pdf_key+1;?></th>
					<td class="kt-font-bold kt-align-center"><?php echo $single_pdf_details['file_name']; ?></td>
					<td class="kt-font-bold kt-align-center"><?php echo date('d M, Y', strtotime($single_pdf_details['file_add_date'])); ?></td>
					<td class="kt-font-bold kt-align-center">
						<div class="row">
							<div class="col-md-12">

								<?php if($single_pdf_details['file_type'] == 'application/pdf'){ ?>

									<a class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View <?php echo $single_pdf_details['file_name']; ?>" href="<?php echo base_url('assets/rfq_files/'.$single_pdf_details['file_name']);?>" style="color: #212529;" target="_blank">
						    			<i class="la la-eye kt-font-bolder" style="color: #212529;"></i>
							    	</a>									
								<?php } else{ ?>

									<a class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View <?php echo $single_pdf_details['file_name']; ?>" href="<?php echo base_url('assets/rfq_files/'.$single_pdf_details['file_name']);?>" style="color: #212529;" target="_blank" download>
										<i class="fa fa-download kt-font-bolder" style="color: #212529;"></i>
									</a>
								<?php } ?>

								<?php if(in_array('technical_document', $this->session->userdata('rfq_access')['rfq_document_delete_access'])){ ?>

									<button class="btn btn-sm btn-clean btn-icon btn-icon-md delete_rfq_technical_document" title="Delete"  rfq_mst_id="<?php echo $pdf_details['rfq_mst_id'];?>" file_name="<?php echo $single_pdf_details['file_name'];?>" file_type="technical_document" style="color: #212529;">
										<i class="la la-trash kt-font-bolder" style="color: #212529;"></i>
									</button>
								<?php } ?>
							</div>
						</div>
					</td>
				</tr>
                <?php } ?>
			<?php } ?>
	</tbody>
</table>