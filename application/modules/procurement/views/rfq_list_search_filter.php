<!--begin: Search Form -->
<form class="kt-form kt-form--fit kt-margin-b-20" id="rfq_search_filter_form">
	<div class="row kt-margin-b-20">
		<div class="col-lg-2 kt-margin-b-10-tablet-and-mobile kt-font-bold">
	      	<label>Rfq No#</label>
			<input type="text" class="form-control" value="<?php echo $search_filter_form_data['rfq_no']; ?>" name="rfq_no" placeholder="Enter Rfq No #" />
	   	</div>
		<div class="col-lg-2 kt-margin-b-10-tablet-and-mobile kt-font-bold">
	      	<label>Company Name</label>
			<input type="text" class="form-control" value="<?php echo $search_filter_form_data['rfq_company']; ?>" name="rfq_company" placeholder="Enter Company Name" />
	   	</div>
		<div class="col-lg-2 kt-margin-b-10-tablet-and-mobile kt-font-bold">
			<label>Rfq Importance</label>
			<select class="form-control procurement_select_picker" name="rfq_importance" multiple>
		      	<?php 
				foreach($search_filter_form_data['rfq_importance'] as $single_rfq_importance){
					echo "<option value='".$single_rfq_importance['value']."'".$single_rfq_importance['selected'].">".$single_rfq_importance['name']."</option>";
				} 
				?>
			</select>
	   	</div>
		<div class="col-lg-2 kt-margin-b-10-tablet-and-mobile kt-font-bold">
			<label>Sales Person</label>
			<select class="form-control procurement_select_picker" name="rfq_sentby" multiple>
				<?php 
				foreach($search_filter_form_data['sales_person'] as $single_sales_person){
					echo "<option value='".$single_sales_person['id']."'".$single_sales_person['selected'].">".$single_sales_person['name']."</option>";
				} 
				?>
			</select>
	   	</div>
		<div class="col-lg-4 col-md-9 col-sm-12 kt-font-bold">
			<label>Rfq Date</label>
				<div class="form-group-sub">
				<div class="kt-portlet__head-toolbar">
					<input type="text" class="form-control" value="<?php echo $search_filter_form_data['rfq_date']; ?>" name="rfq_date" hidden/>
                    <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
						<a href="javascript:void(0);" class="btn kt-subheader__btn-daterange" id="rfq_list_search_filter_date" data-toggle="kt-tooltip" title="Select rfq Date" data-placement="left" style="background: rgb(0 0 0 / 10%); display: inline-flex; height: 32px !important; padding-top: 0; padding-bottom: 0; margin-top: 0.25rem; margin-bottom: 0.25rem; margin-left: 0.25rem;">
							<span class="kt-subheader__btn-daterange-title" id="rfq_list_search_filter_date_title" style="display: inline-block;color: #959cb6;font-weight: 500;"></span>&nbsp;
							<span class="kt-subheader__btn-daterange-date" id="rfq_list_search_filter_date_date" style="display: inline-block;color: #5d78ff;margin-right: 0.75rem;font-weight: 500;"><?php echo $search_filter_form_data['rfq_date']; ?></span>
							<i class="flaticon2-calendar-1" style="color: #5d78ff; font-size: 1rem !important;"></i>
						</a>
                    </ul>
                </div>
			</div>
		</div>
	</div>
	<div class="row kt-margin-b-20">
		<div class="col-lg-2 kt-margin-b-10-tablet-and-mobile kt-font-bold">
			<label>Procurement Person</label>
			<select class="form-control procurement_select_picker" name="assigned_to" multiple>
		      	<?php 
				foreach($search_filter_form_data['procurement_person'] as $single_procurement_person){
					echo "<option value='".$single_procurement_person['id']."'".$single_procurement_person['selected'].">".$single_procurement_person['name']."</option>";
				} 
				?>
			</select>
	   	</div>
		<div class="col-lg-2 kt-margin-b-10-tablet-and-mobile kt-font-bold">
			<label>Rfq Status</label>
			<select class="form-control procurement_select_picker" name="rfq_status" multiple>
				<?php foreach($search_filter_form_data['rfq_status'] as $single_rfq_status){ ?>
					<option 
						value="<?php echo $single_rfq_status['value']; ?>"
						data-content="<span class='<?php echo $single_rfq_status['class'];?>'><?php echo $single_rfq_status['name'];?></span>"
						<?php echo $single_rfq_status['selected']; ?>>
							<?php echo $single_rfq_status['name'];?>
					</option>
				<?php } ?>
			</select>
	   	</div>
		<div class="col-lg-2 kt-margin-b-10-tablet-and-mobile kt-font-bold">
			<label>Procurement Person Status</label>
			<select class="form-control procurement_select_picker" name="procurement_person_status" multiple>
				<?php foreach($search_filter_form_data['procurement_person_status'] as $single_procurement_person_status){ ?>
					<option 
						value="<?php echo $single_procurement_person_status['value']; ?>"
						data-content="<span class='<?php echo $single_procurement_person_status['class'];?>'><?php echo $single_procurement_person_status['name'];?></span>"
						<?php echo $single_procurement_person_status['selected']; ?>>
							<?php echo $single_procurement_person_status['name'];?>
					</option>
				<?php } ?>
			</select>
	   	</div>
		<div class="col-lg-2 kt-margin-b-10-tablet-and-mobile kt-font-bold">
	      	<label>Quotation No#</label>
			<input type="text" class="form-control" value="<?php echo $search_filter_form_data['quotation_no']; ?>" name="quotation_no" placeholder="Enter Quotation No#" />
	   	</div>
		<div class="col-lg-2 kt-margin-b-10-tablet-and-mobile kt-font-bold">
	      	<label>Quotation Status</label>
			<select class="form-control procurement_select_picker" name="quotation_status" multiple>
		      	<?php 
				foreach($search_filter_form_data['quotation_status'] as $single_quotation_status){
					echo "<option value='".$single_quotation_status['value']."'".$single_quotation_status['selected'].">".$single_quotation_status['name']."</option>";
				} 
				?>
			</select>
	   	</div>
		<div class="col-lg-2 kt-margin-b-10-tablet-and-mobile kt-font-bold">
	      	<label>Product Family</label>
			<select class="form-control procurement_select_picker" name="product_family" multiple>
				<?php foreach($search_filter_form_data['product_family'] as $single_product_family){ ?>
					<option 
						value="<?php echo $single_product_family['value']; ?>"
						data-content="<span class='<?php echo $single_product_family['class'];?>'><?php echo $single_product_family['name'];?></span>"
						<?php echo $single_product_family['selected']; ?>>
							<?php echo $single_product_family['name'];?>
					</option>
				<?php } ?>
			</select>
	   	</div>
	</div>
	<div class="row kt-margin-b-20">
		<div class="col-md-2 kt-margin-b-10-tablet-and-mobile kt-font-bold">
			<label>Priority range</label>
			<div class="kt-font-info col-lg-10 col-md-9 col-sm-12">
				<div class="kt-ion-range-slider">
					<input type="hidden" id="rfq_priority" name="priority" value="<?php echo $search_filter_form_data['priority'];?>">
				</div>
			</div>
		</div>
		<div class="col-lg-2 kt-margin-b-10-tablet-and-mobile kt-font-bold">
			<label>Rfq Subject</label>
			<input type="text" class="form-control" value="<?php echo $search_filter_form_data['rfq_subject']; ?>" name="rfq_subject" placeholder="Enter Rfq Subject" />
		</div>
	</div>	      
	<div class="kt-separator kt-separator--md kt-separator--dashed"></div>
	<div class="row">
	    <div class="col-lg-12">
	        <button class="btn btn-primary btn-brand--icon rfq_search_filter_form_submit" type="reset">
	            <span>
	                <i class="la la-search"></i>
	                <span>Search</span>
	            </span>
	        </button>
	        &nbsp;&nbsp;
	        <button class="btn btn-secondary btn-secondary--icon rfq_search_filter_form_reset" type="reset">
	            <span>
	                <i class="la la-close"></i>
	                <span>Reset</span>
	            </span>
	        </button>
	    </div>
	</div>
</form>
