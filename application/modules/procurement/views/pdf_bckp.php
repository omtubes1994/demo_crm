<table cellpadding="5" cellspacing="0">
		<tr style="background-color: #fff;">
		<td width="50%" style="padding:5px;vertical-align: text-top; ">
		<img src="/assets/media/client-logos/logo.png" width="180" height="50" style="padding-left: 10px;"><br/>
		<strong style="font-size: 17px;">OM TUBES & FITTINGS INDUSTRIES</strong>
		<div class="left_addrs" style="font-size: 14px;color: #484545; line-height: 20px;">10 Bordi Bunglow, 1st Panjarapole Lane, CP Tank, Mumbai, Maharashtra, India<br> GSTIN 27AFRPM5323E1ZC
		</div>
		<table style="margin-top: -15px;">
		<tr style="background-color: #fff;">
		<td style="font-size: 14px; color: #484545; line-height: 20px;" width="50%">+91 (22) 6743 7634</td>
		</tr>
		<tr style="background-color: #fff;">
		<td style="font-size: 14px; color: #484545; line-height: 20px;" align="left">www.omtubes.com</td>
		</tr>
		</table>
		</td>
		<td width="50%">
		<strong style="font-size: 25px; text-align: right; line-height: 35px;">Purchase Order</strong><br/>
		<table style="line-height: 22px;">
		<tr style="background-color: #e4e1e1; border-bottom: 1px solid;">
		<td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;"> PO# : </strong></td>
		<td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;"> Date : </strong></td>
		</tr>
		<tr>
		<td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;">
			<?php echo $procurement_data['procurement_no']?>		
		</td>
		<td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;">
			<?php echo date('F j, Y', strtotime($procurement_data['add_time'])); ?>
		</td>
		</tr>
		<tr style="background-color: #e4e1e1;">
		<td colspan="2" style="padding:5px;vertical-align: text-top;"><strong style="font-size: 12px;"> Vendor:</strong></td>
		</tr>
		<tr>
		<td colspan="2" style="padding:5px;vertical-align: text-top; font-family: courier;">
		<strong>
			<?php echo $procurement_data['vendor_name']?>
		</strong>
		<div class="left_addrs" style="font-size: 11px;color: #484545; line-height: 17px; font-family: courier;">
			<?php echo $country_list[$procurement_data['vendor_country']];?><br/><?php echo $procurement_data['vendor_sales_person_name']?>
		</div>
		</td>
		</tr>
		<tr style="background-color: #e4e1e1;">
		<td colspan="2" style="padding:5px;vertical-align: text-top;"><strong style="font-size: 12px;">Reference :</strong></td>
		</tr>
		<tr>
		<td colspan="2" style="font-family: courier; font-size: 11px;color: #484545;"><?php echo $procurement_data['subject']?></td>
		</tr>
		</table>
		</td>
		</tr>
		<tr>
		<td colspan="2"></td>
		</tr>
		<tr>
		<td colspan="2">
		<table cellspacing="0" cellpadding="10" border="0">
		<thead>
		<tr style="background-color: #e4e1e1;">
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="7%;">#</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="36%">Item Description</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="12%">Qty</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="8%">Unit</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="13%">Price</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="9%">Unit</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="15%">Total</td>
		</tr>
		</thead>
		<tbody>
			<?php foreach($product_details as $product_key => $single_product_details) {
				$bg_color = '';
				if(($product_key)%2 != 0){
					$bg_color = '#e4e1e1;';
				} 
			?>
			<tr style="background-color: <?php echo $bg_color;?>; font-size: 11px; font-family: courier;">
				<td style="text-align: right;"><?php echo $product_key+1;?></td>
				<td><?php echo htmlentities($single_product_details['description_name']);?></td>
				<td style="text-align: right;"><?php echo $single_product_details['quantity'];?></td>
				<td><?php echo $single_product_details['units'];?></td>
				<td style="text-align: right;"><?php echo $single_product_details['price'];?></td>
				<td><?php echo 'P.', $single_product_details['units'];?></td>
				<td style="text-align: right;"><?php echo $single_product_details['total'];?></td>
			</tr>
			<?php }?>
		</tbody>
		</table>
		</td>
		</tr>
		<tr>
		<td>
		<strong>Total in words:</strong><br/><span style="font-family: courier; font-size: 11px;"><?php echo $grand_total_words?></span><br/>
		<hr/>
		</td>
		<td>
			<table cellspacing="0" cellpadding="10">
			<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;" width="50%" >Net Total</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;" width="50%"><?php echo $procurement_data['net_total']?></td>
			</tr>
			<?php if(!empty($charges_details)){ ?>
				<?php foreach($charges_details as $single_details) {?>
					<tr>
						<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;" width="50%" ><?php echo $single_details['charge_name'], ' (', $single_details['charge_value'], ') ';?></td>
						<td style="font-size: 12px; font-family: courier; border: solid 1px grey;" width="50%"><?php echo $single_details['gross_total_after_charge']-$procurement_data['net_total'], '.00';?></td>
					</tr>
				<?php }?>
			<?php }?>
			<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Grand Total</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;"><?php echo $procurement_data['gross_total']?></td>
		</tr>
		</table>
		</td>
		</tr>
		<tr>
		<td colspan="2"></td>
		</tr>
		<tr>
		<td colspan="2"></td>
		</tr>
		<tr>
		<td>
		<table>
		<tr>
		<td colspan="2"><strong>Terms and Conditions</strong></td>
		</tr>
		<tr>
		<td width="35%">Delivered To : </td><td width="65%"><span style="font-family: courier; float: right;">
		<?php echo $procurement_data['delivery_to']?></span></td>
		</tr>
		<tr>
		<td>Delivery Time : </td> <td><span style="font-family: courier; float: right;">
			<?php echo $procurement_data['delivery_time']?>
		</span></td>
		</tr>
		<tr>
		<td>Validity : </td> <td><span style="font-family: courier; float: right;">
			<?php echo $procurement_data['validity']?>
		</span></td>
		</tr>
		<tr>
		<td>Currency : </td> <td><span style="font-family: courier; float: right;">
			<?php echo $procurement_data['currency']?>
		</span></td>
		</tr>
		<tr>
		<td>Country of Origin : </td> <td><span style="font-family: courier; float: right;">
			<?php echo $procurement_data['country_of_origin']?>
		</span></td>
		</tr>
		<tr>
		<td>MTC Type : </td> <td><span style="font-family: courier; float: right;">
			<?php echo $procurement_data['mtc_type']?>
		</span></td>
		</tr>
		<tr>
		<td>Packing Type : </td> <td><span style="font-family: courier; float: right;">
			<?php echo $procurement_data['packing_type']?>
		</span></td>
		</tr>
		<tr>
		<td>Payment : </td> <td><span style="font-family: courier; align-items: right;"><?php echo $procurement_data['payment']?></span></td>
		</tr>
		</table>
		</td>
		<td align="center">
		Thank you for your business<br/>
		For Om Tubes & Fittings Industries<br/><br/>
		<img src="/assets/media/stamp.png" /><br/><br/>
		<table>
		<tr>
		<td width="26%" rowspan="3"></td>
		<td align="left" width="74%">Name : <span style="font-family: courier;"><?php echo $purchase_person_details['name']?></span></td>
		</tr>
		<tr>
		<td align="left">Email : <span style="font-family: courier;"><?php echo $purchase_person_details['email']?></span></td>
		</tr>
		<tr>
		<td align="left">Mobile : <span style="font-family: courier;"><?php echo $purchase_person_details['mobile']?></span></td>
		</tr>
		</table>
		</td>
		</tr>
		<tr>
			<td colspan="2">
				<table cellspacing="0" cellpadding="3" border="0">
					<tr>
						<td><strong>Additional Notes</strong></td>
					</tr>
					<tr>
						<td style="font-family: courier; font-size: 11px;">
							<?php echo $procurement_data['term_condition_text']?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>