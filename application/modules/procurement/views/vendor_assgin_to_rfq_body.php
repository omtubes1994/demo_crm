<?php 
if(!empty($rfq_to_vendor_main)){ 
	foreach ($rfq_to_vendor_main as $rfq_to_vendor_main_key => $rfq_to_vendor_main_value) { 
		$current_key= $rfq_to_vendor_main_key+1;
?>
	<tr>
		<td class="kt-font-dark kt-font-xl kt-font-bold">
			<?php echo $current_key; ?>
		</td>
		<td class="kt-font-dark kt-font-lg kt-font-bold kt-align-left">
			<?php echo $rfq_to_vendor_main_value['vendor_name']; ?>
		</td>
		<td class="kt-font-dark kt-font-lg kt-font-bold kt-align-center">
			<span class="kt-badge kt-badge--dark kt-badge--inline kt-badge--pill kt-badge--rounded"><?php echo $rfq_to_vendor_main_value['vendor_status']; ?></span>
		</td>
		<td class="kt-font-dark kt-font-lg kt-font-bold kt-align-center">
			<?php echo $rfq_to_vendor_main_value['evaluate_price']; ?>
		</td>
		<td class="kt-font-dark kt-font-lg kt-font-bold kt-align-center">
			<?php echo $rfq_to_vendor_main_value['evaluate_delivery']; ?>
		</td>
		<td class="kt-font-dark kt-font-lg kt-font-bold kt-align-center">
			<a href="javascript:void(0);" class="show_line_item" rfq_id="<?php echo $rfq_to_vendor_main_value['rfq_id'];?>" vendor_id="<?php echo $rfq_to_vendor_main_value['vendor_id'] ?>">
				<i class="la la-angle-double-down kt-font-dark kt-font-bolder" style="padding-left:10px"></i>
			</a>
			<a href="<?php echo site_url('pdf_management/rfq_vendor_pdf/'.$rfq_to_vendor_main_value['rfq_id'].'/'.$rfq_to_vendor_main_value['vendor_id']); ?>" target="_blank">
				<i class="la la-eye kt-font-dark kt-font-bolder" style="padding-left:10px"></i>
			</a>
			<a href="javascript:void(0);" class="update_rfq_assign_to_vendor" data-toggle="modal" data-target="#update_vendor_to_rfq" rfq_id="<?php echo $rfq_to_vendor_main_value['rfq_id'];?>" vendor_id="<?php echo $rfq_to_vendor_main_value['vendor_id'] ?>">
				<i class="la la-info-circle kt-font-dark kt-font-bolder" style="padding-left:10px"></i>
			</a>
		</td>
	</tr>
	<?php foreach ($rfq_to_vendor_sub[$rfq_to_vendor_main_value['vendor_id']] as $rfq_to_vendor_sub_key => $rfq_to_vendor_sub_value) { ?>
		
		<tr class="vendor_details_<?php echo $rfq_to_vendor_main_value['vendor_id']; ?>" style="display: none;">
			<td class="kt-font-dark kt-font-xl kt-font-bold kt-align-right">
				<?php echo $rfq_to_vendor_sub_key+1; ?>
			</td>
			<td class="kt-font-dark kt-font-lg kt-font-bold kt-align-left">
				<span>
					<em><?php echo $product_details[$rfq_to_vendor_sub_value['product_id']];?></em>
					<hr>
					<em><?php echo $material_details[$rfq_to_vendor_sub_value['material_id']];?></em>
				</span>
			</td>
			<td class="kt-font-dark kt-font-lg kt-font-bold kt-align-center" colspan="3">
				<?php echo $rfq_to_vendor_sub_value['description'];?>
			</td>
			<td class="kt-font-dark kt-font-lg kt-font-bold kt-align-center">
				<span>
					<em><?php echo $rfq_to_vendor_sub_value['quantity'];?></em>
					<hr>
					<em><?php echo $units_details[$rfq_to_vendor_sub_value['unit']];?></em>
				</span>
			</td>
		</tr>
	<?php } ?>
<?php  }
	}
?>