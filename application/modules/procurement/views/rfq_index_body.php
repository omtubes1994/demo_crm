<?php if(!empty($rfq_list)){ ?>
	<?php foreach ($rfq_list as $rfq_list_key => $single_rfq_list_details) { ?>
	<tr>
		<td class="kt-align-center">
			<?php echo $rfq_list_key+1; ?>
			<hr style="margin: 2px;">
			<?php if($single_rfq_list_details['type'] == 'om'){?>
				<span>
					<span class="kt-font-bolder" style="width: auto; padding: 10px;">OM</span>
					<hr style="margin: 2px;">
				</span>
			<?php }elseif($single_rfq_list_details['type'] == 'zen'){?>
				<span>
					<span class="kt-font-bolder" style="width: auto; padding: 10px;">Z</span>
					<hr style="margin: 2px;">
				</span>
			<?php }elseif($single_rfq_list_details['type'] == 'in'){?>
				<span>
					<span class="kt-font-bolder" style="width: auto; padding: 10px;">IN</span>
					<hr style="margin: 2px;">
				</span>
			<?php }?>
		</td>
		<td>
			<div class="row">
				<?php if($this->session->userdata('rfq_access')['rfq_list_client_name_access']){ ?>

					<!-- Company Name -->
					<span class="col-xl-12 kt-align-left kt-font-bold">
						Company Name:
						<span class="kt-align-left kt-align-center kt-font-bolder kt-font-brand">
							<?php echo $single_rfq_list_details['company_name']; ?>
						</span>
						<hr style="margin: 2px;">
					</span>
				<?php } ?>
				<?php if($this->session->userdata('rfq_access')['rfq_list_member_name_access']){ ?>

					<!-- Member Name -->
					<span class="col-xl-12 kt-align-left kt-font-bold">
						Member Name:
						<span class="kt-align-left kt-align-center kt-font-bolder kt-font-brand">
							<?php echo $single_rfq_list_details['member_name']; ?>
						</span>
						<hr style="margin: 2px;">
					</span>
				<?php } ?>
				
				<!-- Subject -->
				<span class="col-xl-12 kt-align-left kt-font-bold">
					Subject:
					<span class="kt-align-left kt-align-center kt-font-bolder kt-font-brand">
						<?php echo $single_rfq_list_details['rfq_subject']; ?>
					</span>
					<hr style="margin: 2px;">
				</span>

				<?php if($this->session->userdata('rfq_access')['rfq_list_value_and_country_access']){ ?>
				<!--Client Country -->
				<span class="col-xl-12 kt-align-left kt-font-bold">
					Country:
					<span class="kt-align-left kt-align-center kt-font-bolder kt-font-brand">
						<?php echo $single_rfq_list_details['country_name']; ?>
					</span>
					<hr style="margin: 2px;">
				</span>
				<?php } ?>

				<?php if($this->session->userdata('rfq_access')['rfq_list_client_type_access'] && !empty($single_rfq_list_details['rfq_client_type'])){ ?>
				<!--Client Type -->
				<span class="col-xl-6 kt-align-left kt-font-bold">
					Type:
					<span class="kt-align-left kt-align-center kt-font-bolder kt-font-brand">
						<?php echo ucfirst($single_rfq_list_details['rfq_client_type']); ?>
					</span>
					<hr style="margin: 2px;">
				</span>
				<?php } ?>

				<?php if($this->session->userdata('rfq_access')['rfq_list_rfq_type_access'] && !empty($single_rfq_list_details['rfq_client_type'])){ ?>
				<!--RFQ Type -->
				<span class="col-xl-6 kt-align-left kt-font-bold">
					RFQ Type:
					<span class="kt-align-left kt-align-center kt-font-bolder kt-font-brand">
						<?php echo ucfirst($single_rfq_list_details['rfq_type']); ?>
					</span>
					<hr style="margin: 2px;">
				</span>
				<?php } ?>

				<span class="col-xl-8 kt-align-left kt-font-bold">
					Assigned Date:
					<span class="kt-align-left kt-align-center kt-font-bolder kt-font-brand">
						<?php echo ucfirst($single_rfq_list_details['assigned_to_date']); ?>
					</span>
					<hr style="margin: 2px;">
				</span>
				<?php if(!empty($single_rfq_list_details['oem_number'])){ ?>
				<span class="col-xl-4 kt-align-right kt-font-bold">
					NO OF OEM:
					<span class="kt-align-left kt-align-right kt-font-bolder kt-font-brand">
						<?php echo $single_rfq_list_details['oem_number']; ?>
					</span>
					<hr style="margin: 2px;">
				</span>
				<?php }?>
			</div>
		</td>
		<td>
			<div class="row">

				<!-- RFQ no # -->
				<span class="col-xl-12 kt-align-left kt-font-bold">
					RFQ NO #:
					<span class="kt-align-left kt-align-center kt-font-bolder kt-font-brand">
						<?php echo $single_rfq_list_details['rfq_no']; ?>
					</span>
					<hr style="margin: 2px;">
				</span>
				<!-- Quote No -->
				<span class="col-xl-12 kt-align-left kt-font-bold">
					Quote No #:
					<span class="kt-align-left kt-align-center kt-font-bolder kt-font-brand">
						<?php echo $single_rfq_list_details['quote_no']; ?>
					</span>
					<hr style="margin: 2px;">
				</span>
				<!-- Sales Person -->
				<span class="col-xl-12 kt-align-left kt-font-bold">
					Sales Person #:
					<span class="kt-align-left kt-align-center kt-font-bolder kt-font-brand">
						<?php echo $single_rfq_list_details['sales_person']; ?>
					</span>
					<hr style="margin: 2px;">
				</span>
				<!-- Importance -->
				<span class="col-xl-6 kt-align-left kt-font-bold">
					Importance:
					<span class="kt-align-left kt-align-center kt-font-bolder kt-font-brand">
						<?php echo $single_rfq_list_details['rfq_importance']; ?>
					</span>
					<hr style="margin: 2px;">
				</span>
				<!-- Product Family -->
				<span class="col-xl-6 kt-align-left kt-font-bold">
					Family:
					<span class="kt-font-bolder <?php echo $rfq_family_badge[$single_rfq_list_details['product_family']]; ?>"  style="width: auto; padding: 10px;">
						<?php echo $single_rfq_list_details['product_family']; ?>
					</span>
					<hr style="margin: 2px;">
				</span>
				<!-- Rfq Date -->
				<span class="col-xl-6 kt-align-left kt-font-bold">
					Date:
					<span class="kt-align-left kt-font-brand kt-font-bolder">
						<?php echo $single_rfq_list_details['rfq_date'],"  -  "; ?>
					</span>
					<span class="kt-align-left kt-font-brand kt-font-bolder">
						<?php echo $single_rfq_list_details['rfq_closedate']; ?>
					</span>
					<hr style="margin: 2px;">
				</span>
				<!-- Rfq Closed Date -->
				<span class="col-xl-6">
					<?php if($single_rfq_list_details['date_diff'] == 'Closing Today'){?>

						<?php ?>
						<span class="kt-align-left kt-font-bolder kt-badge kt-badge--orange kt-badge--md kt-badge--rounded" style="width: auto; padding: 10px;">Today <?php echo $single_rfq_list_details['close_time'];?></span>

					<?php } else if($single_rfq_list_details['date_diff'] == ''){ ?>

						<span style="padding: 5px;"></span>

					<?php } else if($single_rfq_list_details['date_diff'] < 0){?>

						<span class="kt-align-left kt-font-bolder kt-badge kt-badge--danger kt-badge--sm kt-badge--rounded" style="width: auto; padding: 10px;">
							<?php echo $single_rfq_list_details['date_diff']; ?>
						</span>
					<?php } else{?>

						<span class="kt-align-left kt-font-bolder kt-badge kt-badge--brand kt-badge--md kt-badge--rounded" style="width: auto; padding: 10px;">
							<?php echo $single_rfq_list_details['date_diff']; ?>
							<?php echo $single_rfq_list_details['close_time'];?>
						</span>
					<?php }?>
					<hr style="margin: 2px;">
				</span>
			</div>
		</td>
		<td>
			<div class="row">
				
				<?php if(!empty($single_rfq_list_details['procuremnt_person'][0]['name'])){ ?>
					<span class="col-xl-12">
						<div class="row" style="padding: 0px 14px 0px 0px;">
							<span class="col-xl-8 kt-font-bold">
								<?php echo $single_rfq_list_details['procuremnt_person'][0]['name']; ?>
							</span>
							<span class="col-xl-4 kt-font-bolder <?php echo $rfq_badge[$single_rfq_list_details['procuremnt_person'][0]['status']]; ?>" style="width: auto; padding: 10px;">
								<?php echo $single_rfq_list_details['procuremnt_person'][0]['status']; ?>
							</span>
						</div>
						<hr style="margin: 2px;">
					</span>
				<?php } ?>
				<?php if(!empty($single_rfq_list_details['procuremnt_person'][1]['name'])){ ?>
					<span class="col-xl-12">
						<div class="row" style="padding: 0px 14px 0px 0px;">
							<span class="col-xl-8 kt-font-bold">
								<?php echo $single_rfq_list_details['procuremnt_person'][1]['name']; ?>
							</span>
							<span class="col-xl-4 kt-font-bolder <?php echo $rfq_badge[$single_rfq_list_details['procuremnt_person'][1]['status']]; ?>" style="width: auto; padding: 10px;">
								<?php echo $single_rfq_list_details['procuremnt_person'][1]['status']; ?>
							</span>
						</div>
						<hr style="margin: 2px;">
					</span>
				<?php } ?>
				<?php if(!empty($single_rfq_list_details['procuremnt_person'][2]['name'])){ ?>
					<span class="col-xl-12">
						<div class="row" style="padding: 0px 14px 0px 0px;">
							<span class="col-xl-8 kt-font-bold">
								<?php echo $single_rfq_list_details['procuremnt_person'][2]['name']; ?>
							</span>
							<span class="col-xl-4 kt-font-bolder <?php echo $rfq_badge[$single_rfq_list_details['procuremnt_person'][2]['status']]; ?>" style="width: auto; padding: 10px;">
								<?php echo $single_rfq_list_details['procuremnt_person'][2]['status']; ?>
							</span>
						</div>
						<hr style="margin: 2px;">
					</span>
				<?php } ?>
				<span class="col-xl-12">
					<div class="row" style="padding: 0px 14px 0px 0px;">
						<span class="col-xl-8 kt-font-bold"></span>
						<span class="col-xl-4 kt-font-bolder" style="width: auto; padding: 10px;"></span>
					</div>
					<hr style="margin: 2px;">
				</span>
				<!-- RFQ Status -->
				<span class="col-xl-12">
					<div class="row" style="padding: 0px 14px 0px 0px;">
						<span class="col-xl-8 kt-font-bold">
							RFQ Status:
						</span>
						<span class="col-xl-4 kt-font-bolder <?php echo $rfq_badge[$single_rfq_list_details['rfq_status']]; ?>" style="width: auto; padding: 10px;">
							<?php echo $single_rfq_list_details['rfq_status']; ?>
						</span>
					</div>
					<hr style="margin: 2px;">
				</span>

				<?php if($this->session->userdata('rfq_access')['rfq_list_value_and_country_access']){ ?>
				<!-- Quotation Net total -->
				<span class="col-xl-12 kt-align-left kt-font-bold">
					Value:	
					<span class="kt-align-left kt-align-center kt-font-bolder kt-font-brand">
						<?php echo $single_rfq_list_details['net_amount']; ?>
					</span>
					<hr style="margin: 2px;">
				</span>
				<?php } ?>

				<!-- Priority -->
				<span class="col-xl-12 kt-align-left kt-font-bold">
					<?php echo $single_rfq_list_details['priority_div']; ?>
				</span>
			</div>
		</td>
		<td class="<?php echo $single_rfq_list_details['background_class'];?>">
			<?php foreach ($single_rfq_list_details['comment'] as $comment) { ?>
				<div class="row">
					<div class="col-xl-12 kt-align-left kt-font-bold">
						<?php echo $comment['message']; ?>
					</div>
					<div class="col-xl-12 kt-align-left kt-font-bold"></div>
					<div class="col-xl-12 kt-align-left kt-font-bold" style="text-align: -webkit-right;">
						<a href="javascript:void(0);" class="kt-align-left kt-align-center kt-font-bolder kt-font-brand" >
							<?php echo $comment['name'] . ' ' . $comment['tat']; ?>
						</a>
					</div>
				</div>
				<?php if ($comment != end($single_rfq_list_details['comment'])) { ?>
					<?php echo $single_rfq_list_details['hr_tag']; ?>
				<?php } ?>
			<?php } ?>
		</td>
		<td>
			
			<?php if($this->session->userdata('quotation_access')['rfq_list_action_is_new_access'] && $single_rfq_list_details['is_new']){?>
				<span class="badge badge-danger" style="color: #fff; font-size: 8px;">New</span> <br>
			<?php } ?>
			<?php if($this->session->userdata('rfq_access')['rfq_list_action_edit_access']){ ?>
				<a href="<?php echo base_url('procurement/addRFQ/'.$single_rfq_list_details['rfq_mst_id']);?>" class="btn btn-bold btn-label-brand btn-sm" target="_blank" title="View RFQ" style="margin: 10px 0px 0px 0px; width: 45px; height: 35px;">
					<i class="la la-info-circle"></i>
				</a>
			<?php } ?>
			<?php if($this->session->userdata('rfq_access')['rfq_priority']){ ?>
				<button type="button" class="btn btn-bold btn-label-brand btn-sm update_rating" data-toggle="modal"
				rfq_id="<?php echo $single_rfq_list_details['rfq_mst_id'];?>"
				priority_value="<?php echo $single_rfq_list_details['priority'];?>"
				priority_reason="<?php echo $single_rfq_list_details['rfq_priority_reason'];?>"
				data-target="#update_rating_rfq" style="margin: 10px 0px 0px 0px; width: 45px; height: 35px;">
					<i class="flaticon-star"></i>
				</button>
			<?php } ?>
			<?php if($this->session->userdata('quotation_access')['quotation_list_action_view_pdf_access'] && $single_rfq_list_details['quote_id'] > 0){ ?>
				<a href="<?php echo base_url('pdf_management/quotation_pdf/'.$single_rfq_list_details['quote_id']);?>" target="_blank" class="btn btn-bold btn-label-brand btn-sm" title="Pdf" style="margin: 10px 0px 0px 0px; width: 45px; height: 35px;">
						<i class="la la-eye"></i>
					</a>
			<?php } ?>
			<?php if($this->session->userdata('quotation_access')['quotation_list_action_edit_access']){ ?>
				<a href="<?php echo base_url('quotations/add/'.$single_rfq_list_details['quote_id'].'/'.$single_rfq_list_details['rfq_mst_id']);?>" target="_blank" class="btn btn-bold btn-label-brand btn-sm" title="Make Quote" style="margin: 10px 0px 0px 0px; width: 45px; height: 35px;">
					<i class="la la-edit"></i>
				</a>
			<?php } ?>
			<?php if($this->session->userdata('rfq_access')['rfq_list_action_delete_access']){ ?>
				<button type="button" class="btn btn-bold btn-label-brand btn-sm deleteRfq" title="Delete"
					rfq_id="<?php echo $single_rfq_list_details['rfq_mst_id'];?>" style="margin: 10px 0px 0px 0px; width: 45px; height: 35px;">
					<i class="la la-trash"></i>
				</button>
			<?php } ?>
			<?php if($this->session->userdata('rfq_access')['rfq_list_action_query_access']){ ?>
				<a class="btn btn-bold btn-label-brand btn-sm add_query" title="Add Query" href="javascript:;" rfq_for="<?php echo $single_rfq_list_details['rfq_mst_id'];?>" style="margin: 10px 0px 0px 0px; width: 45px; height: 35px;">
					<i class="fa fa-question kt-font-bolder"></i>
				</a>
			<?php } ?>
			<?php if($this->session->userdata('rfq_access')['rfq_list_action_chat_conversation_access']){ ?>
				<a class="btn btn-bold btn-label-brand btn-sm get_chat_conversation" title="Add Comment" href="javascript:;" rfq_mst_id="<?php echo $single_rfq_list_details['rfq_mst_id'];?>" style="margin: 10px 0px 0px 0px; width: 45px; height: 35px;">
					<i class="fa fa-comments kt-font-bolder"></i>
				</a>
			<?php } ?>
			<?php if($this->session->userdata('rfq_access')['rfq_list_action_graph_access']){ ?>
				<a class="btn btn-bold btn-label-brand btn-sm  graph_details" title="view Graph" href="javascript:;" company_name="<?php echo $single_rfq_list_details['company_name'];?>" company_id="<?php echo $single_rfq_list_details['rfq_company'];?>" style="margin: 10px 0px 0px 0px; width: 45px; height: 35px;">
					<i class="fa fa-bar-chart kt-font-bolder"></i>
				</a>
			<?php }?>
			<?php if($this->session->userdata('rfq_access')['rfq_list_action_rfq_and_technical_document_access']){ ?>
				<?php if(empty($single_rfq_list_details['file_path_and_type']) && empty($single_rfq_list_details['document_path_and_type'])){ ?> 

					<button type="button" class="btn btn-bold btn-label-brand btn-sm upload_file" title="File Upload" data-toggle="modal" data-target="#rfq_file_upload" rfq_mst_id="<?php echo $single_rfq_list_details['rfq_mst_id']; ?>" style="margin: 10px 0px 0px 0px; width: 45px; height: 35px;">
						<i class="fa fa-upload kt-font-bolder"></i>
					</button>
				<?php } else{ ?>
					
					<button type="button" class="btn btn-bold btn-label-success btn-sm upload_file" title="File Upload" data-toggle="modal" data-target="#rfq_file_upload" rfq_mst_id="<?php echo $single_rfq_list_details['rfq_mst_id']; ?>" style="margin: 10px 0px 0px 0px; width: 45px; height: 35px;">
						<i class="fa fa-upload kt-font-bolder"></i>
					</button>
				<?php } ?>
			<?php } ?>	
		</td>
	</tr>
	<?php } ?>
<?php } ?>