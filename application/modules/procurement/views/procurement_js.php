jQuery(document).ready(function() {

	$('.procurement_select_picker').selectpicker();
	var method_name = '<?php echo $this->uri->segment("2");?>';
   	var third_params = '<?php echo $this->uri->segment("3",0);?>';
   	KTAutosize.init();
	$('option.all').hide();
   	if(method_name == 'update_purchase_order' && third_params > 0) {
   	
   		set_order_details();
   	}else if(method_name == 'update_purchase_order_via_production' && third_params > 0) {
   	
   		set_order_details();
   	} else if(method_name == 'rfq_list_new') {

		ajax_call_function(prepare_rfq_filter_data(), 'rfq_list_data');
   	} else if(method_name == 'addRFQ') {

		Closeddate_time_js.init();
		ajax_call_function({call_type: 'get_rfq_assign_to_vendor_details', 'rfq_id': "<?php echo $this->uri->segment(3, 0);?>"}, 'get_rfq_assign_to_vendor_details');
   	}
	$('a.change_rfq_pending_date_for_highchart').click(function(){
		ajax_call_function({call_type: 'rfq_pending_list', filter_date: $(this).attr('filter-date'), filter_sales_person: $('input#filter_sales_person_value_bar_graph').val()}, 'rfq_pending_list');
	});
	$('div.select_sales_person_bar_graph_dropdown').on('click', 'a.select_sales_person_bar_graph', function(){

		ajax_call_function({call_type: 'rfq_pending_list', filter_date: $('input#filter_date_value_bar_graph').val(), filter_sales_person: $(this).attr('sales-person')}, 'rfq_pending_list');
	});
	$('select#vendor_id').on('change', function(){
		$('option.all').hide();
		$('option.'+$(this).val()).show();
		//set vendor name and country based on vendor id
		ajax_call_function({call_type: 'set_vendor_name_and_country', vendor_id: $(this).val()}, 'set_vendor_name_and_country');
	});	    
	$('tbody#product_list_body').on('input', 'input.purchase_order_quantity, input.purchase_order_unit', function(){
		var count_number = $(this).attr('count_number');
		add_net_total(count_number);
	});
	$('button.add_charges_in_purchase_order').click(function(){

		ajax_call_function({call_type: 'add_charges_in_purchase_order', add_charges_in_purchase_order_form: $('form#add_charges_in_purchase_order_form').serializeArray()}, 'add_charges_in_purchase_order');
	});
	$('a.add_gst_or_discount_modal').click(function(){
		add_data_to_charge_history();
	});
	$('i.edit_terms_and_conditions').click(function(){
		var alert_text = $.trim($('div.terms_conditions_alert').html());
		$('textarea#terms_conditions').html('').html(alert_text);
	});
	$('button.update_terms_conditions').click(function(){
		var updated_term_condition = $.trim($('textarea#terms_conditions').val());
		$('div.terms_conditions_alert').html('').html(updated_term_condition);
		$('div#edit_terms_and_conditions').modal('hide');
	});
	$('button.add_purchase_order').click(function(){

		var procurement_id = $(this).attr('procurement_id');
		var call_type = 'update_purchase_order';
		if(procurement_id == ''){

			call_type = 'add_purchase_order';
		}
		ajax_call_function({
							call_type: call_type,
							procurement_id: procurement_id,
							person_details: $('form#purchase_order_user_details_form').serializeArray(),
							product_details: $('form#purchase_order_quantity_form').serializeArray(),
							terms_conditon: $('form#purchase_order_terms_condition_form').serializeArray(),
							term_condition_text: $.trim($('div.terms_conditions_alert').html())
						}, 'add_purchase_order');
	});
	$('a.add_item_to_procurement_product').click(function(){

		var next_count_number = $(this).attr('next_count_number');
		ajax_call_function({call_type: 'get_product_body', next_count_number: next_count_number,product_name: $('select[name="product_name_1"]').val(),material_name:$('select[name="material_name_1"]').val()}, 'get_product_body');
		$(this).attr('next_count_number', ++next_count_number);
	});
	$('tbody#procurement_body').on('click', 'a.delete_procurement_list_data', function(){

		swal({
			  title: "Are you sure?",
			  text: "Once deleted, you will not be able to recover this Procurement Details!",
			  icon: "warning",
			  buttons:  ["Cancel", "Delete"],
			  dangerMode: true,	
			})
			.then((willDelete) => {
			  	if (willDelete) {
	        		ajax_call_function({call_type: 'delete_procurement_list_data', procurement_id: $(this).attr('procurement_id')}, 'delete_procurement_list_data');	
			    	swal({
			    		title: "Procurement details is deleted",
			      		icon: "success",
			    	});
		  		} else {
				    swal({
			    		title: "Procurement is not deleted",
			      		icon: "info",
			    	});
			  	}
			});
	});
	$('a.add_procuremet_search_filter_form').click(function(){

		$('div.procuremet_search_filter').show();
	});
	$('div#procurement_search_filter_form_div').on('click', 'button.procurement_search_filter_form_submit', function(){
		set_reset_spinner($('button.procurement_search_filter_form_submit'));
		search_filter();
	});
	$('thead#procurement_head').on('click', 'th.sorting_search', function(){
		ajax_call_function({
							call_type: 'sort_filter',
							procurement_sort_name: $(this).attr('sorting_name'),
							procurement_sort_value: $(this).attr('sorting_value'),
							search_form_data: $('form#procurement_search_filter_form').serializeArray()
						},
						'sort_filter');
	});
	$('div#procurement_paggination').on('click', 'li.procurement_paggination_number', function(){

		ajax_call_function({
							call_type: 'paggination_filter',
							limit: $(this).attr('limit'),
							offset: $(this).attr('offset'),
							search_form_data: $('form#procurement_search_filter_form').serializeArray()
						}, 'paggination_filter');
	});
	$('div#procurement_paggination').on('change', 'select#set_limit', function(){

		ajax_call_function({
							call_type: 'paggination_filter',
							limit: $('select#set_limit').val(),
							offset: 0,
							search_form_data: $('form#procurement_search_filter_form').serializeArray()
						}, 'paggination_filter');
	});
	$('tbody#product_list_body').on('click', 'a.delete_product_details', function(){

		$('tr.count_'+$(this).attr('count_no')).remove();
		ajax_call_function({call_type: 'delete_product_details', count_no: $(this).attr('count_no')}, 'delete_product_details');
	});
	$('table#charge_history_table').on('click', 'a.delete_charges_details', function(){

		swal({
		  title: "Are you sure?",
		  text: "Once deleted, you will not be able to recover this Charge Details!",
		  icon: "warning",
		  buttons:  ["Cancel", "Delete"],
		  dangerMode: true,	
		})
		.then((willDelete) => {
		  	if (willDelete) {
        		ajax_call_function({call_type: 'delete_charge', charge_id: $(this).attr('charge_id')}, 'delete_charge');
        		add_data_to_charge_history();                 
		    	swal({
		    		title: "Charge is deleted",
		      		icon: "success",
		    	});
	  		} else {
			    swal({
		    		title: "Charge is not deleted",
		      		icon: "info",
		    	});
		  	}
		});
	});
	$('a.rfq_assign_status').click(function(){

		$('a.rfq_assign_status').removeClass('active');
		$(this).addClass('active');
        rfq_filter_call();
	});
	$('div.rfq_sales_person').on('click', 'a.select_sales_person_bar_graph', function(){

		$('#sales_person_name').val($(this).attr('sales-person'));
        rfq_filter_call();
	});
	// ajax_call_function({call_type: 'procurement_status_highchart',person_type: 'procurement_person'}, 'procurement_status_highchart');
	$('a.change_procurement_pending_for_follow_up_date_for_highchart').click(function(){

		ajax_call_function({call_type: 'procurement_status_highchart', person_type: $(this).attr('person-type')}, 'procurement_status_highchart');
	});

	//rfq js start

	$('a.rfq_search_filter_button').click(function(){

		var button_value = $(this).attr('button_value');
		if(button_value == 'show') {

			$('div#rfq_search_filter_div').slideDown();
			$(this).attr('button_value', 'hide');
			$(this).html('Hide Search Filter');
		}else{

			$('div#rfq_search_filter_div').slideUp();
			$(this).attr('button_value', 'show');
			$(this).html('Add Search Filter');
		}
	});

	$('div#rfq_search_filter_div').on('click', 'button.rfq_search_filter_form_submit', function(){

		ajax_call_function(prepare_rfq_filter_data(), 'rfq_list_data');
	});

	$('div#rfq_search_filter_div').on('click', 'button.rfq_search_filter_form_reset', function(){

		location.reload();
	});

	$("tbody#rfq_list_body").on('click', '.replyquery', function(){
		$("#query_response #rfq_id").val($(this).attr('rfq_id'));
		$("#query_table tbody").html($(this).attr('query'));
	});

	$("tbody#rfq_list_body").on('click', '.notes', function(){
		$("#notes_table tbody").html($(this).attr('notes'));
	});

	$("tbody#rfq_list_body").on('click', '.deleteRfq', function(){
		if(confirm('Are you sure?')){
			$.ajax({
				type: 'POST',
				url: "<?php echo site_url('procurement/deleteRfq'); ?>",
				data: {'rfq_id': $(this).attr('rfq_id')},
				success: function(res){
					location.reload();
				}
			});
		}
	});

	$("#query_response").validationEngine('attach', {
		onValidationComplete: function(form, status){
			if(status == true){
				var frm_data = $("#query_response").serialize();
				$.ajax({
					url: "<?php echo site_url('procurement/addQuery'); ?>",
					data: frm_data,
					type: 'post',
					success: function(res){
						var resp = $.parseJSON(res);
						var tbody = '';
						var align = '';
						Object.keys(resp).forEach(function(key) {
							align = 'left';
							if(resp[key].entered_by == "<?php echo $this->session->userdata('user_id'); ?>"){
								align = 'right';
							}
							tbody += `<tr>
								<td>
									<div style="text-align:`+align+`">
            							`+resp[key].note+`<br/>
            							<span style="font-size: 10px;">`+resp[key].entered_on+`</span>
            						</div>
								</td>
							</tr>`;
						});
						$("#notes").val('');
						$("#query_table tbody").html(tbody);
					}
				})
			}
			return false;
		},
		promptPosition: "inline",
	});

	$("tbody#rfq_list_body").on('click', '.query', function(){
		var quote_id = $(this).attr('quote_id');
		var query_id = $(this).attr('query_id');
		var query_type = $(this).attr('query_type');
		// alert(quote_id);
		$.ajax({
			type: "POST",
			data: {"quote_id": quote_id, 'query_type':query_type},
			url: "<?php echo site_url('quotations/getQueryHistory'); ?>",
			success: function(res){
				$("#query-popup #tab_history").html('');
				if(res != '[]'){
					resp = $.parseJSON(res);
					// alert(resp);
					var table = '<table class="table table-bordered table-stripped"><tr><th>Sr #</th><th>Query Details</th></tr>';
					var quote_no = '';
					var client_name = '';
					for(key in resp){
						sr_no = parseInt(key)+1;
						table += '<tr><td>'+sr_no+'</td><td>'+resp[key].query_text+'</td>';
						// quote_no = resp[key].quote_no;
						// client_name = resp[key].client_name;
					}
					table += '</table>';
					$("#query-popup #tab_history").html(table);
				}
				$("#query-popup #quote_id").val(quote_id);
				$("#query-popup #query_id").val(query_id);
				$("#query-popup").modal('show');
			}
		});
	});


	$("#query_form").validationEngine('attach', {
		onValidationComplete: function(form, status){
			if(status == true){
				var frm_data = $("#query_form").serialize();
	        	$.ajax({
	        		type: 'POST',
	        		data: frm_data,
	        		url: "<?php echo site_url('quotations/addQuery'); ?>",
	        		success: function(res){
	        			//toastr.success('Task created successfully!');
	        		}
	        	});
	        	query_form.reset();
	        	$("#query-popup").modal('hide');
			}
			return false;
		},
		promptPosition: "inline"
    });

	$("tbody#rfq_list_body").on('click', '.purchase_query', function(){
		var quote_id = $(this).attr('quote_id');
		var query_id = $(this).attr('query_id');
		var query_type = $(this).attr('query_type');
		// alert(quote_id);
		$.ajax({
			type: "POST",
			data: {"quote_id": quote_id, 'query_type':'purchase'},
			url: "<?php echo site_url('quotations/getQueryHistory'); ?>",
			success: function(res){
				$("#pquery-popup #tab_history").html('');
				var resp = '';
				if(res != '[]'){
					resp = $.parseJSON(res);
					// alert(resp);
					var table = '<table class="table table-bordered table-stripped"><tr><th>Sr #</th><th>Query Details</th></tr>';
					var quote_no = '';
					var client_name = '';
					for(key in resp){
						sr_no = parseInt(key)+1;
						table += '<tr><td>'+sr_no+'</td><td>'+resp[key].query_text+'</td>';
						// quote_no = resp[key].quote_no;
						// client_name = resp[key].client_name;
					}
					table += '</table>';
					$("#pquery-popup #tab_history").html(table);
				}
				$("#pquery-popup #quote_id").val(quote_id);
				if(resp != ''){
					$("#pquery-popup #query_id").val(resp[0].query_id);	
				}
				$("#pquery-popup").modal('show');
			}
		});
	});

	$("#pquery_form").validationEngine('attach', {
		onValidationComplete: function(form, status){
			if(status == true){
				var frm_data = $("#pquery_form").serialize();
	        	$.ajax({
	        		type: 'POST',
	        		data: frm_data,
	        		url: "<?php echo site_url('quotations/addQuery'); ?>",
	        		success: function(res){
	        			//toastr.success('Task created successfully!');
	        		}
	        	});
	        	pquery_form.reset();
	        	$("#pquery-popup").modal('hide');
			}
			return false;
		},
		promptPosition: "inline"
    });

	$('#update_rating_rfq').on('click', '.save_rating_rfq', function(){
		var data = {
			call_type: 'update_rating',
			rating_value: $('form#rfq_priority_form').serializeArray()
	    }
		console.log(data);
		$.ajax({
			type: 'POST',
			data: data,
			url: "<?php echo base_url('procurement/ajax_function'); ?>",
			dataType: 'JSON',
			success: function(res){
				setTimeout(function(){

					if(res.status == 'successful') {

						toastr.info("Rating is updated"); 
						$('#update_rating_rfq').modal('hide');
						setTimeout(function(){

							//location.reload();
							ajax_call_function(prepare_rfq_filter_data(), 'rfq_list_data');
						}, 1500)
					} else {
						toastr.error("Rating Reason is mandatory"); 
					}
				}, 1000);
			},
			beforeSend: function(response){
				toastr.warning("Rating is getting updated");
			}
		});
	});
	$("tbody#rfq_list_body").on('click', '.update_rating', function(){
		var rfq_id=$(this).attr('rfq_id');
		$('div#modal_body_update_rating').html('<input type="hidden" id="update_rfq_priority_'+rfq_id+'" name="update_rfq_priority_'+rfq_id+'" />');
		$('div#modal_body_update_rating_reason').html('<input type="text" class="form-control" name="update_rfq_priority_reason_'+rfq_id+'" value="'+$(this).attr('priority_reason')+'"/>');
	    $("#update_rfq_priority_"+rfq_id).ionRangeSlider({
            min: 0,
            max: 5,
            from: $(this).attr('priority_value')
        });
	});
	toastr.options = {
		"closeButton": true,
		"debug": false,
		"newestOnTop": true,
		"progressBar": true,
		"positionClass": "toast-top-right",
		"preventDuplicates": true,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	};
	var KTIONRangeSlider = function () {

	    // Private functions
	    var demos = function (lead_start) {
	    	
	        // min & max values
	        $('#rfq_priority').ionRangeSlider({
	            min: 0,
	            max: 5,
	            from: lead_start
	        });
	    }

	    return {
	        // public functions
	        init: function(lead_start) {
	            demos(lead_start); 
	        }
	    };
	}();		
	$("#rfq_priority").ionRangeSlider({
        min: 0,
        max: 5,
        from: 0
    });
	
    $('div#rfq_list_paggination').on('click', 'li.rfq_paggination_number', function(){
		ajax_call_function(prepare_rfq_filter_data('', $(this).attr('limit'), $(this).attr('offset'), ''), 'rfq_list_data');
    });
    $('div#rfq_list_paggination').on('change', 'select#set_limit', function(){
		ajax_call_function(prepare_rfq_filter_data('', $('select#set_limit').val(), 0, ''), 'rfq_list_data');
    });

    $('button.add_rfq_query').click(function(){
		ajax_call_function({call_type: 'save_rfq_query', form_data: $('form#rfq_query_add_form').serializeArray()},'save_rfq_query',"<?php echo base_url('query/ajax_function'); ?>");
	});

    $("tbody#rfq_list_body").on('click', '.add_query', function(){
        ajax_call_function({call_type: 'get_rfq_query_history', rfq_query_rfq_id: $(this).attr('rfq_for')},'get_rfq_query_history',"<?php echo base_url('query/ajax_function'); ?>");
	});

	document.addEventListener('keydown', function (event) {

		if (event.shiftKey) {

			if (event.key === 'I') {

				ajax_call_function(prepare_rfq_filter_data('in', 10, 0, ''), 'rfq_list_data');

			} else if (event.key === 'Z') {

				ajax_call_function(prepare_rfq_filter_data('zen', 10, 0, ''), 'rfq_list_data');

			} else if (event.key === 'O') {

				ajax_call_function(prepare_rfq_filter_data('om', 10, 0, ''), 'rfq_list_data');

			} else if (event.key === 'A') {

				ajax_call_function(prepare_rfq_filter_data('all', 10, 0, ''), 'rfq_list_data');

			}
		}
	});

	$('input[name="radio_company_type"]').click(function () {

		var type = $("input[type='radio'][name='radio_company_type']:checked").val();
		if (type == '') {
			type = 'all';
		}
		ajax_call_function(prepare_rfq_filter_data(type, 10, 0, ''), 'rfq_list_data');
	});

	$('tbody#rfq_list_body').on('click', '.graph_details', function () {

		ajax_call_function({
			call_type: 'get_rfq_graph_details',
			company_name: $(this).attr('company_name'),
			company_id: $(this).attr('company_id'),
			module_type: 'rfq',
		}, 'get_rfq_graph_details',"<?php echo base_url('graph_management/ajax_function'); ?>");
	});
	//rfq js end



	$('button.add_purchase_order_via_production').click(function(){

		var quotation_mst_id = $(this).attr('quotation_mst_id');
		var call_type = 'save_purchase_order_via_production';

		ajax_call_function({
							call_type: call_type,
							quotation_mst_id: quotation_mst_id,
							person_details: $('form#purchase_order_user_details_form').serializeArray(),
							product_details: $('form#purchase_order_quantity_form').serializeArray(),
							terms_conditon: $('form#purchase_order_terms_condition_form').serializeArray(),
							term_condition_text: $.trim($('div.terms_conditions_alert').html())
						}, call_type);
	});

	$("select[name='rfq_status']").change(function(){

		$('div.rfq_regret_reason_div').hide();
		var status = $("select[name='rfq_status']").val();
		if(status == 'regret'){
			$('div.rfq_regret_reason_div').show();
		}
	});

	$('a.add_vendor_modal').click(function(){

		var quotation_id = get_quotation_id();
		if(quotation_id == ''){
			swal({
				title: "Please select Line Item",
				icon: "warning"
			});
			return false;
		}
		$('.vendor_name_select_picker').selectpicker();
	});

	$('button.submit_add_vendor_form').click(function(){

		var quotation_id = get_quotation_id();
		if(quotation_id == ''){
			swal({
				title: "Please select Line Item",
				icon: "danger"
			});
			return false;
		}
		var rfq_id = "<?php echo $this->uri->segment(3);?>";
		ajax_call_function({call_type: 'save_vendor_details', vendor_data:$('form#add_vendor_for_rfq').serializeArray(), quotation_id_data: quotation_id, rfq_id: rfq_id}, 'save_vendor_details');
	});

	$('tbody.vendor_assign_to_rfq_body').on('click', 'a.show_line_item', function(){

		$('tr.vendor_details_'+$(this).attr('vendor_id')).slideDown();
	});
	
	$('tbody.vendor_assign_to_rfq_body').on('click', 'a.update_rfq_assign_to_vendor', function(){
		
		$('div.update_vendor_for_rfq_div').html('');
		ajax_call_function({call_type: 'get_data_for_update_rfq_assign_to_vendor', rfq_id: $(this).attr('rfq_id'), vendor_id: $(this).attr('vendor_id')}, 'get_data_for_update_rfq_assign_to_vendor');
	});
	$('button.close_update_vendor_form').click(function(){

		$('div.update_vendor_for_rfq_div').html('');
	});
	$('button.submit_update_vendor_form').click(function(){

		var quotation_id = [];
		$("input[name='quotation_id_update[]']:checked").each(function(res){
			quotation_id.push(this.value);
		});

		if(quotation_id == ''){
			swal({
				title: "Please select Line Item",
				icon: "warning"
			});
			return false;
		}
		ajax_call_function({call_type: 'update_vendor_details', vendor_data:$('form#update_vendor_for_rfq').serializeArray(), quotation_id_data: quotation_id}, 'update_vendor_details');
	});
	$("input[name='quotation_id_all']").click(function(){

		var count = 0;
    	$('input[name="quotation_id[]"]').prop('checked', false);
    	$('tr.all_tr').css('background-color','');
    	if($("input[name='quotation_id_all']:checked").is(':checked') == true){

			$('input[name="quotation_id[]"]').each(function(res) {
	    		count = count+1;
			});
			$('input[name="quotation_id[]"]').prop('checked', true);
    		$('tr.all_tr').css('background-color','lightgray');
    	}
    	$('span.checkbox_checked_count').attr('count',count);
    	$('span.checkbox_checked_count').html('('+count+')');
	});
	$("input[name='quotation_id[]']").click(function(){

		var count = parseInt($('span.checkbox_checked_count').attr('count'));
		if($(this).prop('checked') == true){

			$(this).closest('tr').css('background-color','lightgray');
			count += 1;

		}else{

			$(this).closest('tr').css('background-color','');
			count -= 1;
		}
		$('span.checkbox_checked_count').attr('count',count);
    	$('span.checkbox_checked_count').html('('+count+')');
	});

	$('div.update_vendor_for_rfq_div').on('click', "input[name='quotation_id_update_all']", function(){

		var count = 0;
    	$('input[name="quotation_id_update[]"]').prop('checked', false);
    	$('tr.all_tr_update').css('background-color','');
    	if($("input[name='quotation_id_update_all']:checked").is(':checked') == true){

			$('input[name="quotation_id_update[]"]').each(function(res) {

	    		count = count+1;
			});
			$('input[name="quotation_id_update[]"]').prop('checked', true);
    		$('tr.all_tr_update').css('background-color','lightgray');
    	}
    	$('span.checkbox_checked_count_update').attr('count',count);
    	$('span.checkbox_checked_count_update').html('('+count+')');
	});
	$('div.update_vendor_for_rfq_div').on('click', "input[name='quotation_id_update[]']", function(){

		var count = parseInt($('span.checkbox_checked_count_update').attr('count'));
		if($(this).prop('checked') == true){

			$(this).closest('tr').css('background-color','lightgray');
			count += 1;

		}else{

			$(this).closest('tr').css('background-color','');
			count -= 1;
		}
		$('span.checkbox_checked_count_update').attr('count',count);
    	$('span.checkbox_checked_count_update').html('('+count+')');
	});
	$('div.client_name_search').click(function(){

		var search_value = $("input#client_search_value").val();
		alert(search_value);
		ajax_call_function({ call_type: 'get_client_details', search_data: search_value }, 'get_client_details', "<?php echo base_url('quotations/ajax_function')?>");

	});
	$('select[name="rfq_company"]').change(function(){

		ajax_call_function({ call_type: 'get_client_member_details', client_id: $(this).val() }, 'get_client_member_details', "<?php echo base_url('quotations/ajax_function')?>");
	});

	$("button.save_chat_reply").click(function(){

		var rfq_mst_id = parseInt($(this).attr('rfq_id_for_chat'));
		console.log(rfq_mst_id);
		if(rfq_mst_id > 0){
			ajax_call_function({ call_type: 'save_chat_reply', rfq_mst_id: rfq_mst_id, message: $('#textarea_id_for_chat_message').val()}, 'save_chat_reply');
		}
	});

	$("tbody#rfq_list_body").on('click', "a.get_chat_conversation", function(){

		var rfq_mst_id = parseInt($(this).attr('rfq_mst_id'));
		// console.log(rfq_mst_id);
		if(rfq_mst_id > 0){

			$('div.div_chat_history').html('');
			$('div.rfq_chat_loader').show();
			ajax_call_function({ call_type: 'get_chat_conversation', rfq_mst_id: rfq_mst_id}, 'get_chat_conversation');
		}
	});

	$('form#addrfq_form').on('select', 'select[name="assigned_to_1"], select[name="assigned_to_2"], select[name="assigned_to_3"]', function () {
		var selectName = $(this).attr('name');
		var selectVal = $(this).val();
		var assignedToDateInput = $('div.assigned_to_date input');

		console.log('Clicked:', selectName, 'Value:', selectVal);
		if (selectVal){
			$('.assigned_to_date').show();
			assignedToDateInput.attr('name', 'assigned_to_date');
		}
	});

	$('form#addrfq_form').on('change', 'select[name="assigned_to_1"], select[name="assigned_to_2"], select[name="assigned_to_3"]', function () {
		var val_1 = $('select[name="assigned_to_1"]').val();
		var val_2 = $('select[name="assigned_to_2"]').val();
		var val_3 = $('select[name="assigned_to_3"]').val();

		var assignedToDateInput = $('div.assigned_to_date input');

		if (val_1 || val_2 || val_3) {
			$('div.assigned_to_date').show();
			assignedToDateInput.attr('name', 'assigned_to_date'); // Add name attribute
		} else {
			$('div.assigned_to_date').hide();
			assignedToDateInput.removeAttr('name'); // Remove name attribute
		}
	});

	$('form#addrfq_form').on('change', '#rfq_priority', function () {

		$('div.rfq_priority_reason').hide();
		var val = $(this).val();
		if (val > 0) {
			$('div.rfq_priority_reason').show();
		}
	});
	$("tbody#rfq_list_body").on('click', "button.upload_file", function(){

		ajax_call_function({call_type: 'get_rfq_file_history', rfq_mst_id : $(this).attr('rfq_mst_id')},'get_rfq_file_history');

	});
	$("div#rfq_file_history_list").on('click', "button.delete_rfq_file", function () {

		swal({
			title: "Are you sure?",
			text: "Once deleted, you will not be able to recover this File!",
			icon: "warning",
			buttons: ["Cancel", "Delete"],
			dangerMode: true,
		})
		.then((willDelete) => {
			if (willDelete) {

				ajax_call_function({ call_type: 'delete_rfq_file', rfq_mst_id: $(this).attr('rfq_mst_id'), file_name: $(this).attr('file_name'), file_type: $(this).attr('file_type') }, 'delete_rfq_file');
				swal({
					title: "File is deleted",
					icon: "success",
				});
				setTimeout(function () {

					location.reload();
				}, 3000);
			} else {
				swal({
					title: "File is not deleted",
					icon: "info",
				});
			}
		});
	});
	$("div#rfq_document_history_list").on('click', "button.delete_rfq_technical_document", function () {

		swal({
			title: "Are you sure?",
			text: "Once deleted, you will not be able to recover this File!",
			icon: "warning",
			buttons: ["Cancel", "Delete"],
			dangerMode: true,
		})
		.then((willDelete) => {
			if (willDelete) {

				ajax_call_function({ call_type: 'delete_rfq_technical_document', rfq_mst_id: $(this).attr('rfq_mst_id'), file_name: $(this).attr('file_name'), file_type: $(this).attr('file_type') }, 'delete_rfq_technical_document');
				swal({
					title: "File is deleted",
					icon: "success",
				});
				setTimeout(function () {

					location.reload();
				}, 3000);
			} else {
				swal({
					title: "File is not deleted",
					icon: "info",
				});
			}
		});
	});


	$('form#purchase_order_user_details_form').on('change', 'select.vendor_po_type', function () {

		var vendor_po_type = $(this).val();
		ajax_call_function({ call_type: 'get_vendor_po_footer', vendor_po_type: vendor_po_type }, 'get_vendor_po_footer');

	});

	$("button.procurement_to_sales_switch_button").click(function () {

		var button_value = $(this).attr('graph_button');
		if (button_value == 'sales') {

			$(this).attr('graph_button', 'procurement');
			$(this).html('procurement Person Graph');
			ajax_call_function(prepare_rfq_filter_data('', '', '', button_value), 'rfq_list_data');
		} else {
			$(this).attr('graph_button', 'sales');
			$(this).html('Sales Person Graph');
			ajax_call_function(prepare_rfq_filter_data('', '', '', button_value), 'rfq_list_data');
		}
	});
});

function get_quotation_id(){

	var quotation_id = [];
	$("input[name='quotation_id[]']:checked").each(function(res){
		quotation_id.push(this.value);
	});
	return quotation_id;
}

function prepare_rfq_filter_data(client_type = "", limit = $('input#rfq_list_limit').val(), offset = $('input#rfq_list_offset').val(), graph_type = "") {

	return { call_type: 'rfq_list_data', search_form_data: $('div#rfq_search_filter_div form#rfq_search_filter_form').serializeArray(), limit: limit, offset: offset, client_type: client_type, graph_type: graph_type };
}

function search_filter() {
	ajax_call_function(
					{
						call_type: 'search_filter',
						search_form_data: $('form#procurement_search_filter_form').serializeArray()
					},
					'search_filter'
				);
}

function add_data_to_charge_history() {

	ajax_call_function({call_type: 'get_gst_or_discount_history'}, 'get_gst_or_discount_history');
}

function add_net_total(count_number) {
	var quantity = $('input[name="quantity_'+count_number+'"]').val();
	var price = $('input[name="price_'+count_number+'"]').val();
	var net_total = price * quantity;
	$('input[name="total_'+count_number+'"]').val(net_total);
	if(net_total > 0) {

		set_order_details(net_total, count_number);
	}
}

function set_order_details(net_total= "", count_number=0) {

	var data={call_type: 'set_order_detail'};
	if(net_total != '') {
		data.net_total = net_total;
	}
	if(count_number != 0) {
		data.count_number = count_number;
	}
	ajax_call_function(data, 'net_total_gross_total_body');
}

// Class definition

var KTAutosize = function () {
    
    // Private functions
    var demos = function () {
        // basic demo
        var demo1 = $('#terms_conditions');
        autosize(demo1);
        $('.vendor_name_select_picker').selectpicker();
    }
    var rfq_list_date_range_picker_Init = function() {

        if ($('#rfq_list_date_range_picker').length == 0) {
            return;
        }
        var picker = $('#rfq_list_date_range_picker');
        var start = moment().startOf('month');
        var end = moment().endOf('month');

        function cb(start, end, label) {
            var title = '';
            var range = '';

            if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D');
            } else {
                range = start.format('MMM D') + ' - ' + end.format('MMM D');
            }

            $('#rfq_list_date_range_picker_date').html(range);
            $('#rfq_list_date_range_picker_title').html(title);
            rfq_filter_call();
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'This Week': [moment().startOf('week'), moment().endOf('week')],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end, 'This Month');
    }
    var daterangepickerInit = function() {
        if ($('#quotation_made_by_daterangepicker').length == 0) {
            return;
        }

        var picker = $('#quotation_made_by_daterangepicker');
        var start = moment().startOf('month');
        var end = moment().endOf('month');

        function cb(start, end, label) {
            var title = '';
            var range = '';
            if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D');
            } else {
                range = start.format('MMM D') + ' - ' + end.format('MMM D');
            }

            $('#quotation_made_by_daterangepicker_date').html(range);
            $('#quotation_made_by_daterangepicker_title').html(title);
            ajax_call_function({call_type: 'quotation_made_by_high_chart', filter_date: range}, 'quotation_made_by_high_chart');
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end, 'This Month');
    }

	var startDate, endDate;
    var rfq_list_search_filter_Init = function() {

        if ($('#rfq_list_search_filter_date').length == 0) {
            return;
        }
        var picker = $('#rfq_list_search_filter_date');
		var defaultStart = moment().subtract(1, 'month').startOf('month');
		var defaultEnd = moment().endOf('month');

		startDate = startDate || defaultStart;
		endDate = endDate || defaultEnd;

        function cb(start, end, label) {
            var title = '';
            var range = '';

			if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D Y');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D Y');
            } else {
                range = start.format('MMM D Y') + ' - ' + end.format('MMM D Y');
            }

            $('#rfq_list_search_filter_date_date').html(range);
            $('#rfq_list_search_filter_date_title').html(title);
            $('input[name="rfq_date"]').val(range);

			startDate = start;
			endDate = end;
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
			startDate: startDate,
			endDate: endDate,
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'This Week': [moment().startOf('week'), moment().endOf('week')],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            }
        }, cb);

		cb(startDate, endDate);
    }
	var priority_value = function () {

		$('#rfq_priority').ionRangeSlider({
			min: 0,
			max: 5
		});
	}

    return {
        // public functions
        init: function() {
            demos();
            rfq_list_date_range_picker_Init();
            daterangepickerInit();
            rfq_list_search_filter_Init();
			priority_value();
        }
    };
}();


var Closeddate_time_js = function () {

	var rfq_closeddate_time = function () {
		$('#rfq_closedate_time').datetimepicker({
			todayHighlight: true,
			autoclose: true,
			todayBtn: true,
			format: 'yyyy-mm-dd hh:ii'
		});
	}
    return {
        // public functions
        init: function() {
			rfq_closeddate_time();
        }
    };
}();


var FileUpload_js = function () {

    // Private functions
	var rfq_file_upload = function () {
        // set the dropzone container id
        var id = '#kt_dropzone_rfq_file_upload';

        // set the preview element template
        var previewNode = $(id + " .dropzone-item");
        previewNode.id = "";
        var previewTemplate = previewNode.parent('.dropzone-items').html();
        previewNode.remove();

        var myDropzone4 = new Dropzone(id, { // Make the whole body a dropzone
            url: "<?php echo base_url('procurement/ajax_function');?>", // Set the url for your upload script location
            params: {'call_type': 'rfq_file_upload', 'type':'files' },
            parallelUploads: 3,
            previewTemplate: previewTemplate,
            maxFilesize: 5, // Max filesize in MB
            autoQueue: false, // Make sure the files aren't queued until manually added
            previewsContainer: id + " .dropzone-items", // Define the container to display the previews
            clickable: id + " .dropzone-select" // Define the element that should be used as click trigger to select files.
        });

        myDropzone4.on("addedfile", function(file) {
            // Hookup the start button
            file.previewElement.querySelector(id + " .dropzone-start").onclick = function() { myDropzone4.enqueueFile(file); };
            $(document).find( id + ' .dropzone-item').css('display', '');
            $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'inline-block');
        });

        // Update the total progress bar
        myDropzone4.on("totaluploadprogress", function(progress) {
            $(this).find( id + " .progress-bar").css('width', progress + "%");
        });

        myDropzone4.on("sending", function(file) {
            // Show the total progress bar when upload starts
            $( id + " .progress-bar").css('opacity', '1');
            // And disable the start button
            file.previewElement.querySelector(id + " .dropzone-start").setAttribute("disabled", "disabled");
        });

        // Hide the total progress bar when nothing's uploading anymore
        myDropzone4.on("complete", function(progress) {
            var thisProgressBar = id + " .dz-complete";
            setTimeout(function(){
                $( thisProgressBar + " .progress-bar, " + thisProgressBar + " .progress, " + thisProgressBar + " .dropzone-start").css('opacity', '0');
            }, 300)
            toastr.success("RFQ File is Uploaded!!!.");
            ajax_call_function({call_type: 'get_rfq_file_history'},'get_rfq_file_history');
        });

        // Setup the buttons for all transfers
        document.querySelector( id + " .dropzone-upload").onclick = function() {
            myDropzone4.enqueueFiles(myDropzone4.getFilesWithStatus(Dropzone.ADDED));
        };

        // Setup the button for remove all files
        document.querySelector(id + " .dropzone-remove-all").onclick = function() {
            $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
            myDropzone4.removeAllFiles(true);
        };

        // On all files completed upload
        myDropzone4.on("queuecomplete", function(progress){
            $( id + " .dropzone-upload").css('display', 'none');
        });

        // On all files removed
        myDropzone4.on("removedfile", function(file){
            if(myDropzone4.files.length < 1){
                $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
            }
        });
    }
	var rfq_document_upload = function () {
		// set the dropzone container id
		var id = '#kt_dropzone_rfq_document_upload';

		// set the preview element template
		var previewNode = $(id + " .dropzone-item");
		previewNode.id = "";
		var previewTemplate = previewNode.parent('.dropzone-items').html();
		previewNode.remove();

		var myDropzone4 = new Dropzone(id, { // Make the whole body a dropzone
			url: "<?php echo base_url('procurement/ajax_function');?>", // Set the url for your upload script location
			params: { 'call_type': 'rfq_file_upload', 'type': 'documents' },
			parallelUploads: 3,
			previewTemplate: previewTemplate,
			maxFilesize: 5, // Max filesize in MB
			autoQueue: false, // Make sure the files aren't queued until manually added
			previewsContainer: id + " .dropzone-items", // Define the container to display the previews
			clickable: id + " .dropzone-select" // Define the element that should be used as click trigger to select files.
		});

		myDropzone4.on("addedfile", function (file) {
			// Hookup the start button
			file.previewElement.querySelector(id + " .dropzone-start").onclick = function () { myDropzone4.enqueueFile(file); };
			$(document).find(id + ' .dropzone-item').css('display', '');
			$(id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'inline-block');
		});

		// Update the total progress bar
		myDropzone4.on("totaluploadprogress", function (progress) {
			$(this).find(id + " .progress-bar").css('width', progress + "%");
		});

		myDropzone4.on("sending", function (file) {
			// Show the total progress bar when upload starts
			$(id + " .progress-bar").css('opacity', '1');
			// And disable the start button
			file.previewElement.querySelector(id + " .dropzone-start").setAttribute("disabled", "disabled");
		});

		// Hide the total progress bar when nothing's uploading anymore
		myDropzone4.on("complete", function (progress) {
			var thisProgressBar = id + " .dz-complete";
			setTimeout(function () {
				$(thisProgressBar + " .progress-bar, " + thisProgressBar + " .progress, " + thisProgressBar + " .dropzone-start").css('opacity', '0');
			}, 300)
			toastr.success("RFQ Documents is Uploaded!!!.");
			ajax_call_function({ call_type: 'get_rfq_file_history' }, 'get_rfq_file_history');
		});

		// Setup the buttons for all transfers
		document.querySelector(id + " .dropzone-upload").onclick = function () {
			myDropzone4.enqueueFiles(myDropzone4.getFilesWithStatus(Dropzone.ADDED));
		};

		// Setup the button for remove all files
		document.querySelector(id + " .dropzone-remove-all").onclick = function () {
			$(id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
			myDropzone4.removeAllFiles(true);
		};

		// On all files completed upload
		myDropzone4.on("queuecomplete", function (progress) {
			$(id + " .dropzone-upload").css('display', 'none');
		});

		// On all files removed
		myDropzone4.on("removedfile", function (file) {
			if (myDropzone4.files.length < 1) {
				$(id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
			}
		});
	}
    return {
        // public functions
        init: function() {
			'<?php if($this->uri->segment("2", "") == "rfq_list_new") { ?>'
				rfq_file_upload();
				rfq_document_upload();
			'<?php } ?>'
        }
    };
}();

function rfq_filter_call(date_range = $('#rfq_list_date_range_picker_date').html(), rfq_status = $('a.rfq_assign_status.active').attr('filter-status'), sales_person_id = $('#sales_person_name').val()) {

	ajax_call_function({call_type: 'rfq_pending_list', filter_date: date_range, filter_status: rfq_status, filter_sales_person: sales_person_id}, 'rfq_pending_list');
}


function ajax_call_function(data, callType, url = "<?php echo base_url('procurement/ajax_function'); ?>") {

	$.ajax({
		type: 'POST',
		data: data,
		url: url,
		dataType: 'JSON',
		success: function(res){
			if(res.status == 'successful') {
				
				if(callType == 'rfq_pending_list') {
					if(data.filter_date != ''){
						swal({
				    		title: "RFQ Pending List Updated ",
				      		icon: "success",
				    	});
					}
			    	rfq_pending_list_highchart(res.highchart_data);
					$('div.rfq_sales_person').html('').html(res.html_body.filter_sales_person);
					$('input#sales_person_name').val(res.html_body.filter_sales_person_id);
					$('span#span_sales_person_name').html('').html(res.html_body.filter_sales_person_name);
				} else if(callType == 'add_charges_in_purchase_order') {
					swal({
			    		title: "Charges are added",
			      		icon: "success",
			    	});
			    	add_data_to_charge_history();
			    	set_order_details();
				} else if(callType == 'get_gst_or_discount_history') {
					$('table#charge_history_table').html('').html(res.charge_history_body);
				} else if(callType == 'net_total_gross_total_body') {
					$('table#net_total_gross_total_body tbody').html('').html(res.net_total_gross_total_body);
				} else if(callType == 'add_purchase_order' || callType == 'save_purchase_order_via_production') {
					swal({
			    		title: res.message,
			      		icon: "success",
			    	});
			    	setTimeout(function(){
			    	
			    		location.reload();
			    	}, 3000);
				} else if(callType == 'set_vendor_name_and_country') {

					if(res.message == 'record found') {

						$('input[name="vendor_name"]').val(res.vendor_details.vendor_name);
						$('input[name="vendor_country"]').val(res.vendor_details.country);
					}
				} else if(callType == 'get_product_body') {
					
					$('tbody#product_list_body').append(res.product_list_body);
				} else if(callType == 'search_filter' || callType == 'sort_filter' || callType == 'paggination_filter') {
					$('tbody#procurement_body').html('').html(res.procurement_body);
					$('div#procurement_search_filter_form_div').html('').html(res.procurement_search_filter);
					$('thead#procurement_head').html('').html(res.procurement_sorting);
					$('div#procurement_paggination').html('').html(res.procurement_paggination);
				} else if(callType == 'quotation_made_by_high_chart') {
					quotation_made_by_high_chart(res.quotation_made_by_highchart);

				} else if(callType == 'procurement_status_highchart') {
					procurement_status_highchart(res.procurement_stage_highchart_category, res.procurement_stage_highchart_data);
				} else if(callType == 'rfq_list_data') {
					$('div#rfq_search_filter_div').html('').html(res.rfq_list_search_filter);
					$('tbody#rfq_list_body').html('').html(res.rfq_list_body);
					$('div#rfq_list_paggination').html('').html(res.rfq_list_paggination);
					$('div#rfq_table_loader').hide();
					$('.procurement_select_picker').selectpicker();
   					KTAutosize.init();
					rfq_list_highchart(res.rfq_list_highchart_category, res.rfq_list_highchart_series);
					rfq_list_piechart(res.rfq_list_piechart_data);
					$('h3.product_family_count').html('').html('Product Family : '+res.total_product_family);

				} else if(callType == 'get_rfq_query_history')
				{
					$('div#kt_chat_modal').modal('show');
					$('em.header_1').html('').html(res.query_reference);
					$('span.header_2').html('').html('RFQ Quote No#');
					$('div.div_chat_history').html(res.rfq_query_history);
					$('textarea[name="query_text"]').val('');
					autosize($('textarea[name="query_text"]'));
					$('input[name="query_reference"]').val(res.query_reference);
                    $('input[name="query_reference_id"]').val(res.query_reference_id);
                    $('input[name="query_creator_id"]').val(res.query_creator_id);
                    $('input[name="query_assigned_id"]').val(res.query_assigned_id);
					$('form#rfq_query_add_form').show();
					$('form#rfq_comment_add_form').hide();
					$('.kt-scroll').scrollTop($('.kt-scroll')[0].scrollHeight);
					$('div.rfq_chat_loader').hide();
				} else if(callType == 'save_rfq_query'){

					toastr.clear();
					toastr.success(res.message);
				} else if(callType == 'delete_product_details'){

					set_order_details();
				} else if(callType == 'save_vendor_details' || callType == 'update_vendor_details'){

					swal({
			    		title: "Vendor details is saved!",
			      		icon: "success",
			    	});
			    	setTimeout(function(){

						location.reload();
					}, 1500);
				} else if(callType == 'get_rfq_assign_to_vendor_details'){

					$('tbody.vendor_assign_to_rfq_body').html('').html(res.rfq_to_vendor_body);
					$('div.layer-white').hide();
				} else if(callType == 'get_data_for_update_rfq_assign_to_vendor'){

					$('div.update_vendor_for_rfq_div').html('').html(res.update_form);
				} else if (callType == 'get_client_details') {
					
					$('select.client_details').html('').html(res.client_option_tag);
					$('.kt-selectpicker').selectpicker();
				} else if (callType == 'get_client_member_details') {

					$('select.mem_details').html('').html(res.client_member_option_tag);
				} else if (callType == 'get_chat_conversation') {

					$('div#kt_chat_modal').modal('show');
					$('em.header_1').html('').html(res.rfq_no);
					$('span.header_2').html('').html('RFQ Quote No#');
					$('div.div_chat_history').html('').html(res.chat_history);
					$('#textarea_id_for_chat_message').val('');
					autosize($('#textarea_id_for_chat_message'));
					$('button.save_chat_reply').attr('rfq_id_for_chat', data.rfq_mst_id);
					$('form#rfq_comment_add_form').show();
					$('form#rfq_query_add_form').hide();
					$('.kt-scroll').scrollTop($('.kt-scroll')[0].scrollHeight);
					$('div.rfq_chat_loader').hide();
				} else if(callType == 'save_chat_reply'){

					toastr.info('Comment Added Successfully!');
					ajax_call_function({ call_type: 'get_chat_conversation', rfq_mst_id: data.rfq_mst_id}, 'get_chat_conversation');
					$('#textarea_id_for_chat_message').val('')

				} else if (callType == 'get_rfq_graph_details'){

					$('h2.graph_company_name').html('').html(res.company_name);
					quotation_vs_proforma_yearly(res.quotation_vs_proforma_yearly);
					quotation_vs_proforma_monthly(res.quotation_vs_proforma_monthly);
					export_stats_yearly(res.export_stats_yearly);
					export_stats_yearly_internal(res.export_stats_yearly_internal);
					exporters_stats(res.exporters_data);
					production_status_wise(res.production_status_wise);
					$("div#stats-modal").modal('show');
					$('div#rfq_table_loader').hide();
				}else if (callType == 'get_rfq_file_history'){

					$('div#rfq_file_history_list').html('').html(res.pdf_upload_history_rfq_file);
					$('div#rfq_document_history_list').html('').html(res.pdf_upload_history_rfq_document);
					FileUpload_js.init();
				}else if (callType == 'get_vendor_po_footer'){

					$('div.terms_conditions_alert').html('').html(res.footer_text);
				}

			}
		},
		beforeSend: function(response){
			if(callType == 'rfq_list_data') {
				$('div#rfq_table_loader').show();
			}else if(callType == 'get_rfq_assign_to_vendor_details'){
				$('div.layer-white').show();
			} else if (callType == 'get_rfq_graph_details'){
				$('div#rfq_table_loader').show();
			}
		}
	});
};

function set_reset_spinner(obj, set_unset_flag = true) {

	if(set_unset_flag) {

		$(obj).addClass('kt-spinner');
    	$(obj).addClass('kt-spinner--right');
    	$(obj).addClass('kt-spinner--sm');
    	$(obj).addClass('kt-spinner--light');	
	} else{

		$(obj).removeClass('kt-spinner');
    	$(obj).removeClass('kt-spinner--right');
    	$(obj).removeClass('kt-spinner--sm');
    	$(obj).removeClass('kt-spinner--light');
	}
}

function rfq_list_highchart(category, series) {

	// console.log(category, series);
	Highcharts.chart('rfq_list_highchart', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: ''
	    },
	    xAxis: {
	        categories: category
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Total Number of RFQ Pending'
	        },
	        stackLabels: {
	            enabled: true,
	            style: {
	                fontWeight: 'bold',
	                color: ( // theme
	                    Highcharts.defaultOptions.title.style &&
	                    Highcharts.defaultOptions.title.style.color
	                ) || 'gray'
	            }
	        }
	    },
	    legend: {
	        align: 'right',
	        x: -30,
	        verticalAlign: 'top',
	        y: 25,
	        floating: true,
	        backgroundColor:
	            Highcharts.defaultOptions.legend.backgroundColor || 'white',
	        borderColor: '#CCC',
	        borderWidth: 1,
	        shadow: false
	    },
	    tooltip: {
	        headerFormat: '<b>{point.x}</b><br/>',
	        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
	    },
	    plotOptions: {
	        column: {
	            stacking: 'normal',
	            dataLabels: {
	                enabled: true
	            }
	        }
	    },
	    series: series
	});

}

function rfq_list_piechart(highchart_data) {

	var categories = highchart_data.map(function (item) {
		return item.name;
	});
	var count = highchart_data.map(function (item) {
		return item.family_count;
	});
	var order_count = highchart_data.map(function (item) {
		return item.order_count;
	});

	Highcharts.chart('rfq_list_piechart', {
		chart: {
			type: 'column'
		},
		title: {
			align: 'left',
			text: ''
		},
		accessibility: {
			announceNewData: {
				enabled: true
			}
		},
		xAxis: {
			categories: categories,
		},
		yAxis: {
			title: {
				text: 'Count of product family'
			},
			stackLabels: {
				enabled: true,
				style: {
					fontWeight: 'bold',
					color: ( // theme
						Highcharts.defaultOptions.title.style &&
						Highcharts.defaultOptions.title.style.color
					) || 'gray'
				}
			}
		},
		plotOptions: {
			series: {
				borderWidth: 0,
				dataLabels: {
					enabled: true,
					format: '{point.y}'
				}
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size: 10px">{point.key}</span><br/>',
			formatter: function () {
				var s = '<b>' + this.x + '</b>';
				s += '<br/><span style="color:' + this.points[0].series.color + '">\u25CF</span>Unique Count: ' + this.points[0].y;
				s += '<br/><span style="color:' + this.points[1].series.color + '">\u25CF</span>Dublicate Count: ' + this.points[1].y;
				return s;
			},
			shared: true
		},
		series: [{
			name: 'Unique Count',
			data: count
		}, {
			name: 'Dublicate Count',
			data: order_count
		}]
	});
}
// create user wise rfq pending highchart
function rfq_pending_list_highchart(highchart_data) {

	// Create the chart
	Highcharts.chart('rfq_pending_list_highchart', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: ''
	    },
	    subtitle: {
	        text: ''
	    },
	    accessibility: {
	        announceNewData: {
	            enabled: true
	        }
	    },
	    xAxis: {
	        type: 'category'
	    },
	    yAxis: {
	        title: {
	            text: 'Total Number of RFQ Pending'
	        }

	    },
	    legend: {
	        enabled: false
	    },
	    plotOptions: {
	        series: {
	            borderWidth: 0,
	            dataLabels: {
	                enabled: true,
	                format: '{point.y:f}'
	            }
	        }
	    },

	    tooltip: {
	        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> RFQ Pending<br/>'
	    },

	    series: [
	        {
	            name: "OM Tubes",
	            colorByPoint: true,
	            data: highchart_data
	        }
	    ]
	});
}

function quotation_made_by_high_chart(highchart_data) {

	Highcharts.chart('quotation_made_by_high_chart', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: ''
	    },
	    subtitle: {
	        text: ''
	    },
	    accessibility: {
	        announceNewData: {
	            enabled: true
	        }
	    },
	    xAxis: {
	        type: 'category'
	    },
	    yAxis: {
	        title: {
	            text: 'Sales done in INR'
	        }

	    },
	    legend: {
	        enabled: false
	    },
	    plotOptions: {
	        series: {
	            borderWidth: 0,
	            dataLabels: {
	                enabled: true,
	                format: '{point.y:f}'
	            }
	        }
	    },

	    tooltip: {
	        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> INR<br/>'
	    },

	    series: [
	        {
	            name: "OM Tubes",
	            colorByPoint: true,
	            data: highchart_data
	        }
	    ]

	});
}

function procurement_status_highchart(category, highchart_data) {

	Highcharts.chart('procurement_status_highchart', {
		// Create the chart
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: ''
	    },
	    xAxis: {
	        // categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
	        categories: category
	    },
	    yAxis: {
	        min: 0,

	        title: {
	            text: 'Total Quotation'
	        },
	    
	        stackLabels: {
	            enabled: true,
	            style: {
	                fontWeight: 'bold',
	                color: ( // theme
	                    Highcharts.defaultOptions.title.style &&
	                    Highcharts.defaultOptions.title.style.color
	                ) || 'gray'
	            }
	        }
	    },
	    legend: {
	        align: 'right',
	        x: -30,
	        verticalAlign: 'top',
	        y: 25,
	        floating: true,
	        backgroundColor:
	            Highcharts.defaultOptions.legend.backgroundColor || 'white',
	        borderColor: '#CCC',
	        borderWidth: 1,
	        shadow: false
	    },
	    tooltip: {
	        headerFormat: '<b>{point.x}</b><br/>',
	        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
	    },
	    plotOptions: {
	        column: {
	            stacking: 'normal',
	            dataLabels: {
	                enabled: true
	            }

	        }
	    },
	    series: highchart_data
	});
}

//Graph
function quotation_vs_proforma_yearly(highchart_data) {

	if(highchart_data.length === 0){

        $('div.quotation_vs_proforma_yearly_body').css('display','hide');
        $('h3.quotation_vs_proforma_yearly_title').html('').html('Quotation Vs Proforma Won <span class="kt-font-danger">No Data Found</span>');
        return false;
    }
    $('div.quotation_vs_proforma_yearly_body').css('display','block');
    $('h3.quotation_vs_proforma_yearly_title').html('').html('Quotation Vs Proforma Won Yearly');

	Highcharts.chart('quotation_vs_proforma_highchart_yearly', {
		// Create the chart
		chart: {
			type: 'column'
		},
		title: {
			text: ''
		},
		xAxis: {
			categories: highchart_data.category
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Total Quotation'
			},
			stackLabels: {
				enabled: true,
				style: {
					fontWeight: 'bold',
					color: ( // theme
						Highcharts.defaultOptions.title.style &&
						Highcharts.defaultOptions.title.style.color
					) || 'gray'
				}
			}
		},
		legend: {
			align: 'right',
			x: -30,
			verticalAlign: 'top',
			y: 25,
			floating: true,
			backgroundColor:
				Highcharts.defaultOptions.legend.backgroundColor || 'white',
			borderColor: '#CCC',
			borderWidth: 1,
			shadow: false
		},
		tooltip: {
			headerFormat: '<b>{point.x}</b><br/>',
			pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
		},
		plotOptions: {
			column: {
				stacking: 'normal',
				dataLabels: {
					enabled: true
				}
			}
		},
		series: highchart_data.series
	});
}
function quotation_vs_proforma_monthly(highchart_data) {

	if(highchart_data.length === 0){

        $('div.quotation_vs_proforma_body').css('display','hide');
        $('h3.quotation_vs_proforma_title').html('').html('Quotation Vs Proforma Won <span class="kt-font-danger">No Data Found</span>');
        return false;
    }
    $('div.quotation_vs_proforma_body').css('display','block');
    $('h3.quotation_vs_proforma_title').html('').html('Quotation Vs Proforma Won Monthly');
	Highcharts.chart('quotation_vs_proforma_highchart', {
		// Create the chart
		chart: {
			type: 'column'
		},
		title: {
			text: ''
		},
		xAxis: {
			categories: highchart_data.category
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Total Quotation'
			},
			stackLabels: {
				enabled: true,
				style: {
					fontWeight: 'bold',
					color: ( // theme
						Highcharts.defaultOptions.title.style &&
						Highcharts.defaultOptions.title.style.color
					) || 'gray'
				}
			}
		},
		legend: {
			align: 'right',
			x: -30,
			verticalAlign: 'top',
			y: 25,
			floating: true,
			backgroundColor:
				Highcharts.defaultOptions.legend.backgroundColor || 'white',
			borderColor: '#CCC',
			borderWidth: 1,
			shadow: false
		},
		tooltip: {
			headerFormat: '<b>{point.x}</b><br/>',
			pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
		},
		plotOptions: {
			column: {
				stacking: 'normal',
				dataLabels: {
					enabled: true
				}
			}
		},
		series: highchart_data.series
	});
}
function export_stats_yearly(highchart_data) {

	if(highchart_data.length === 0){

        $('div.export_stats_yearly_body').css('display','none');
        $('h3.export_stats_yearly_title').html('').html('Yearly Buying Trends Total <span class="kt-font-danger">No Data Found</span>');
        return false;
    }
    $('div.export_stats_yearly_body').css('display','block');
    $('h3.export_stats_yearly_title').html('').html('Yearly Buying Trends Total: Rupees '+ highchart_data.total);
	Highcharts.chart('export_stats_yearly_container', {
		chart: {
			type: 'column'
		},
		title: {
			text: ''
		},
		xAxis: {
			categories: highchart_data.category,
			crosshair: true
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Exporter Wise import values'
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				'<td style="padding:0"><b>${point.y}</b></td></tr>',
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
		plotOptions: {
			series: {
				pointPadding: 0,
				borderWidth: 0,
				dataLabels: {
					enabled: true,
					format: '${point.y}'
				}
			}
		},
		series: highchart_data.series
	});
}
function export_stats_yearly_internal(highchart_data) {

	if(highchart_data.length === 0){

        $('div.export_stats_yearly_internal_body').css('display','none');
        $('h3.export_stats_yearly_internal_title').html('').html('Yearly Buying Trends <span class="kt-font-danger">No Data Found</span>');
        return false;
    }
    $('div.export_stats_yearly_internal_body').css('display','block');
	$('h3.export_stats_yearly_internal_title').html('').html('Internal Data : $' +highchart_data.total);
	Highcharts.chart('export_stats_yearly_internal_container', {
		chart: {
			type: 'column'
		},
		title: {
			text: ''
		},

		accessibility: {
			announceNewData: {
				enabled: true
			}
		},
		xAxis: {
			type: 'category'
		},
		yAxis: {
			title: {
				text: 'Year wise data'
			}

		},
		legend: {
			enabled: false
		},
		plotOptions: {
			series: {
				borderWidth: 0,
				dataLabels: {
					enabled: true,
					format: '${point.y}'
				}
			}
		},

		tooltip: {
			headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>${point.y}</b><br/>'
		},

		series: [
			{
				name: 'Total',
				colorByPoint: true,
				data: highchart_data.series
			}
		]
	});
}
function exporters_stats(highchart_data) {

	if(highchart_data.length === 0){

        $('div.exporter_stats_div').css('display','none');
        return false;
    }
    $('div.exporter_stats_div').css('display','block');
    $('h3.exporter_stats_title').html('').html('Top Exporter');
	Highcharts.chart('exporter_stats_container', {
		chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y}',
                    connectorColor: 'silver'
                }
            }
        },
        series: [{
            name: 'Share',
            data: highchart_data
        }],
        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        },
	});
}
function production_status_wise(highchart_data) {
	
	if(highchart_data.length === 0){

        $('div.production_status_wise_div').css('display','none');
        return false;
    }
    $('div.production_status_wise_div').css('display','block');
	$('h3.production_status_wise_title').html('').html('Total: $' +highchart_data.total);
	Highcharts.chart('production_status_wise', {
		chart: {
			type: 'column'
		},
		title: {
			text: ''
		},

		accessibility: {
			announceNewData: {
				enabled: true
			}
		},
		xAxis: {
			type: 'category'
		},
		yAxis: {
			title: {
				text: 'Year wise data'
			}

		},
		legend: {
			enabled: false
		},
		plotOptions: {
			series: {
				borderWidth: 0,
				dataLabels: {
					enabled: true,
					format: '${point.y}'
				}
			}
		},

		tooltip: {
			headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>${point.y}</b><br/>'
		},

		series: [
			{
				name: 'Total',
				colorByPoint: true,
				data: highchart_data.series
			}
		]
	});
}