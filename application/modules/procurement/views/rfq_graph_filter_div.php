<div class="col">
	<div class="btn-group">
		<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="border: 0px solid #e2e5ec;">
			Sales Person : <span style="font-weight: 800; color: #5d78ff; padding: 1px 1px 1px 10px;" id="sales_person_name_bar_graph"></span>
		</button>
		<div class="dropdown-menu kt-scroll select_sales_person_bar_graph_dropdown" data-scroll="true" style="height: 250px" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -215px, 0px);">
		</div>
	</div>
</div>
<div class="col">
	<div class="btn-group">
		<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="border: 0px solid #e2e5ec;">
			RFQ Status : <span style="font-weight: 800; color: #5d78ff; padding: 1px 1px 1px 10px;" id="sales_person_name_bar_graph"></span>
		</button>
		<div class="dropdown-menu kt-scroll" data-scroll="true" style="height: 250px" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -215px, 0px);">
			<h6 class="dropdown-header">Select RFQ Status</h6>
			<a class="dropdown-item" rfq-status="pending">Pending</a>
			<a class="dropdown-item" rfq-status="done">Done</a>
			<a class="dropdown-item" rfq-status="regret">Regret</a>
			<a class="dropdown-item" rfq-status="waiting">Waiting</a>
			<a class="dropdown-item" rfq-status="query">Query</a>
		</div>
	</div>
</div>
