<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	
	<div class="kt-portlet kt-portlet--mobile">
		<?php if(in_array(17,$this->session->userdata('graph_data_access'))) { ?>
		<div class="row">
			<div class="col-xl-12">
				<!--Begin::Portlet-->
				<div class="kt-portlet kt-portlet--height-fluid">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">
								PURCHASE ORDER STATUS GRAPH
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
								<li class="nav-item">
									<a class="nav-link change_procurement_pending_for_follow_up_date_for_highchart active" person-type="procurement_person" data-toggle="tab" href="#kt_widget11_tab1_content" role="tab">
										procurement person
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link change_procurement_pending_for_follow_up_date_for_highchart" person-type="sales_person" data-toggle="tab" href="#kt_widget11_tab1_content" role="tab" >
									  sales person
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="kt-portlet__body">
						<!--Begin::Timeline 3 -->	
						<div class="kt-scroll" data-scroll="true" style="height: 400px">
							<div id="procurement_status_highchart"></div>
						</div>

						<!--End::Timeline 3 -->
					</div>
				</div>

				<!--End::Portlet-->
			</div>
		</div>
		<?php } ?>
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					PURCHASE ORDER LIST
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
		        <div class="kt-portlet__head-wrapper">
		            <div class="kt-portlet__head-actions">
		                <a href="javascript:;" class="btn btn-brand btn-elevate btn-icon-sm add_procuremet_search_filter_form">
		                    Add Search Filter
		                </a>
		                &nbsp;
		                <a href="<?php echo base_url('procurement/update_purchase_order');?>" class="btn btn-brand btn-elevate btn-icon-sm">
		                    <i class="la la-plus"></i>
		                    New Record
		                </a>
		            </div>
		        </div>
	    	</div>
		</div>
		<div class="kt-portlet__body procuremet_search_filter" style="display: none;">

		    <!--begin: Search Form -->
		    <form class="kt-form kt-form--fit kt-margin-b-20" id="procurement_search_filter_form">
		        <div id="procurement_search_filter_form_div">
		          <?php $this->load->view('procurement/procurement_search_filter_form');?>
		        </div>
		    </form>
	  	</div>
		<div class="kt-portlet__body">

			<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
				<div class="row">
					<div class="col-sm-12">
						<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 1536px;">
							<thead id="procurement_head">
          			<?php $this->load->view('procurement/procurement_head');?>
							</thead>
							<tbody id="procurement_body">
          			<?php $this->load->view('procurement/procurement_body');?>
							</tbody>
						</table>
						<div id="kt_table_1_processing" class="dataTables_processing card" style="display: none; top: 10% !important;">Processing...</div>
					</div>
				</div>
				<div class="row" id="procurement_paggination">
          <?php $this->load->view('procurement/procurement_paggination');?>
				</div>
			</div>
		</div>

	</div>

</div>

<!-- end:: Content -->