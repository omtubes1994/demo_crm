
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col">
			<div class="alert alert-light alert-elevate fade show" role="alert">
				<div class="alert-icon"><i class="flaticon-edit kt-font-brand"></i></div>
				<div class="alert-text">
					Purchase Order.
				</div>
				<div>
					<button type="reset" class="btn btn-success add_purchase_order" procurement_id="<?php echo $procurement_data['id'];?>">Save</button>
				</div>
			</div>
		</div>
	</div>
	<!--begin::Portlet-->
	<div class="kt-portlet">
		<div class="kt-portlet__body">
			<div class="kt-form__section kt-form__section--first">
				<!--begin::Form-->
				<form class="kt-form" id="purchase_order_user_details_form">
					<div class="form-group row">
						<label class="col-lg-2 col-form-label">Subject of Order:</label>
						<div class="col-lg-2">
							<input type="text" class="form-control" name="subject" value="<?php echo $procurement_data['subject'];?>">
						</div>
						<label class="col-lg-2 col-form-label">Procurement Person:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control" name="procurement_person">
								<option value="">Select Procurement Person</option>
								<?php foreach($procurement_person as $procurement_person_name) {?>
									<option 
									value="<?php echo $procurement_person_name['name'];?>" 
									<?php 
										echo ($procurement_data['procurement_person'] == $procurement_person_name['name']) ? 'selected': '';
									?>>
									<?php echo ucfirst(strtolower($procurement_person_name['name']));?>
									</option>
								<?php } ?>
							</select>
						</div>
						<label class="col-lg-2 col-form-label">Sales Person:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control" name="sales_person">
								<option value="">Select</option>
								<?php foreach($sales_person as $sales_person_name) {?>
									<option 
									value="<?php echo $sales_person_name['name'];?>" 
									<?php 
										echo ($procurement_data['sales_person'] == $sales_person_name['name']) ? 'selected': '';
									?>>
									<?php echo ucfirst(strtolower($sales_person_name['name']));?>
									</option>
								<?php } ?>
							</select>
						</div>						
					</div>
					<div class="form-group row">
						<label class="col-lg-2 col-form-label">Vendor Name:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control vendor_name_select_picker" data-size="7" data-live-search="true" id="vendor_id">
								<option value="">Select</option>
								<?php foreach($vendor_list as $vendor) {?>
									<option 
									value="<?php echo $vendor['vendor_id'];?>" 
									<?php 
										echo ($procurement_data['vendor_name'] == $vendor['vendor_id']) ? 'selected': '';
									?>>
									<?php echo ucfirst(strtolower($vendor['vendor_name']));?>
									</option>
								<?php } ?>
							</select>
						</div>
						<input type="text" name="vendor_name" value="<?php echo $procurement_data['vendor_name'];?>" hidden>
						<input type="text" name="vendor_country" value="<?php echo $procurement_data['vendor_country'];?>" hidden>
						<label class="col-lg-2 col-form-label">Vendor Sales Person:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control" name="vendor_sales_person_name">
								<option value="">Select</option>
								<?php foreach($vendor_sales_person as $vendor_sales_person_single) {?>
									<option 
									class="<?php echo 'all  ', $vendor_sales_person_single['vendor_id'];?>"
									value="<?php echo $vendor_sales_person_single['name'];?>" 
									<?php 
										echo ($procurement_data['vendor_sales_person_name'] == $vendor_sales_person_single['name']) ? 'selected': '';
									?>>
									<?php echo ucfirst(strtolower($vendor_sales_person_single['name'])), '(', $vendor_sales_person_single['designation'], ')';?>
									</option>
								<?php } ?>
							</select>
						</div>
						<label class="col-lg-2 col-form-label">Status:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control" name="procurement_status">
								<option value="" <?php echo ($procurement_data['procurement_status'] == '') ? 'selected': ''; ?>>Select</option>
								<option value="Issued" <?php echo ($procurement_data['procurement_status'] == 'Issued') ? 'selected': ''; ?>>Issued</option>
								<option value="InProduction" <?php echo ($procurement_data['procurement_status'] == 'InProduction') ? 'selected': ''; ?>>InProduction</option>
								<option value="Delivered" <?php echo ($procurement_data['procurement_status'] == 'Delivered') ? 'selected': ''; ?>>Delivered</option>
								<option value="Inspected" <?php echo ($procurement_data['procurement_status'] == 'Inspected') ? 'selected': ''; ?>>Inspected</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-2 col-form-label">Comments:</label>
						<div class="col-lg-2">
							<input type="text" class="form-control" name="comments" value="<?php echo $procurement_data['comments'];?>">
						</div>

					</div>
				</form>

				<!--end::Form-->
				<!--begin::Form-->
				<form class="kt-form" id="purchase_order_quantity_form">
					<div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>
					<div class="row">
						<div class="col-lg-12" style="padding: 0px 0px 0px 0px !important;">
							<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info">
								<thead id="competitor_rank_head">
									<tr role="row">
										<th colspan="8" style="text-align: center;">ADD ORDER ITEMS</th>		
										<th style="text-align: center;">
											<a href="javascript:;" class="btn-sm btn btn-label-success btn-bold add_item_to_procurement_product" next_count_number="<?php echo $next_count_number;?>">
												<i class="la la-plus">Add</i>
											</a>
										</th>		
									</tr>
									<tr role="row">
										<th class="sorting_disabled" style="width: 12%;">Product</th>
										<th class="sorting_disabled" style="width: 12%;">Material</th>
										<th class="sorting_disabled" style="width: 24%;">Description</th>
										<th class="sorting_disabled" style="width: 10%;">Quantity</th>
										<th class="sorting_disabled" style="width: 10%;">Unit</th>
										<th class="sorting_disabled" style="width: 10%;">Price</th>
										<th class="sorting_disabled" style="width: 8%; display: none;">Per Unit</th>
										<th class="sorting_disabled" style="width: 10%;">Net Total</th>
										<th class="sorting_disabled" style="width: 10%;">Actions</th>
									</tr>
								</thead>
								<tbody id="product_list_body">
									<?php if(!empty($product_details)){ ?>
										<?php $count_no = 1;?>
										<?php foreach($product_details as $single_product_info) {?>
											<tr class="<?php echo 'count_', $count_no; ?>">
												<td>
													<select class="form-control" name="product_name_<?php echo $count_no;?>">
														<option value="">Select</option>
														<?php foreach($product_list as $product_name) {?>
															<option 
															value="<?php echo $product_name['lookup_value'];?>" 
															<?php 
																echo ($single_product_info['product_name'] == $product_name['lookup_value']) ? 'selected': '';
															?>>
															<?php echo $product_name['lookup_value'];?>
															</option>
														<?php } ?>
													</select>
												</td>
												<td>
													<select class="form-control" name="material_name_<?php echo $count_no;?>">
														<option value="">Select</option>
														<?php foreach($material_list as $material_name) {?>
															<option 
															value="<?php echo $material_name['lookup_value'];?>" 
															<?php 
																echo ($single_product_info['material_name'] == $material_name['lookup_value']) ? 'selected': '';
															?>>
															<?php echo $material_name['lookup_value'];?>
															</option>
														<?php } ?>
													</select>
												</td>
												<td>
													<input type="text" class="form-control" name="description_name_<?php echo $count_no;?>" value="<?php echo $single_product_info['description_name']; ?>">
												</td>
												<td>
													<input type="number" class="form-control purchase_order_quantity" count_number="<?php echo $count_no;?>" name="quantity_<?php echo $count_no;?>"
													 value="<?php echo $single_product_info['quantity']; ?>">
												</td>
												<td>
													<select class="form-control" name="units_<?php echo $count_no;?>">
														<option value="">Select</option>
														<?php foreach($units_list as $units_name) {?>
															<option 
															value="<?php echo $units_name['unit_value'];?>" 
															<?php 
																echo ($single_product_info['units'] == $units_name['unit_value']) ? 'selected': '';
															?>>
															<?php echo $units_name['unit_value'];?>
															</option>
														<?php } ?>
													</select>
												</td>
												<td>
													<input type="number" class="form-control purchase_order_unit" count_number="<?php echo $count_no;?>" name="price_<?php echo $count_no;?>" value="<?php echo $single_product_info['price']; ?>">
												</td>
												<td style="display: none;">
													<input type="number" class="form-control" name="per_unit_<?php echo $count_no;?>" value="<?php echo $single_product_info['per_unit']; ?>">
												</td>
												<td>
													<input type="number" class="form-control" name="total_<?php echo $count_no;?>" readonly value="<?php echo $single_product_info['total']; ?>">
												</td>
												<td>
													<a href="javascript:;" class="btn-sm btn btn-label-danger btn-bold delete_product_details" count_no="<?php echo $count_no;?>">
														<i class="la la-trash-o"></i>
													</a>
												</td>
											</tr>
										<?php $count_no++;} ?>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
					
				</form>

				<!--end::Form-->
				<div class="kt-invoice__container" style="padding-top: 2%;">
					<div class="table table-striped- table-bordered table-responsive">
						<table class="table" id="net_total_gross_total_body">
							<thead>
								<tr>
									<th>
										Different Charges or Discount
										<a href="#" class="btn btn-icon add_gst_or_discount_modal" data-toggle="modal" data-target="#add_gst_or_discount">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--md">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<polygon points="0 0 24 0 24 24 0 24"></polygon>
													<path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
													<path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" fill="#000000"></path>
												</g>
											</svg>
										</a>
									</th>
									<th style="width: 20%;">NET AMOUNT</th>
									<th style="width: 20%;">GROSS AMOUNT</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
				<!--begin::Form-->
				<form class="kt-form" id="purchase_order_terms_condition_form">
					<div class="kt-invoice__container" style="padding-top: 2%;">
						<div class="table table-striped- table-bordered table-responsive">
							<table class="table">
								<thead>
									<tr role="row">
										<th colspan="4" style="text-align: center;">TERMS AND CONDITIONS</th>		
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="kt-font-dark kt-font-lg">Delivered To</td>
										<td class="kt-font-dark kt-font-xl kt-font-boldest">
											<select class="form-control" name="delivery_to" style="width: 50%;">
												<option value="">Select</option>
												<?php foreach($delivery as $delivery_details) {?>
													<option 
													value="<?php echo $delivery_details['delivery_name'];?>" 
													<?php 
														echo ($procurement_data['delivery_to'] == $delivery_details['delivery_name']) ? 'selected': '';
													?>>
													<?php echo $delivery_details['delivery_name'];?>
													</option>
												<?php } ?>
	             				</select>
	           				</td>
	           				<td class="kt-font-dark kt-font-lg">Curreny</td>
										<td class="kt-font-dark kt-font-xl kt-font-boldest">
											<select class="form-control" name="currency" style="width: 50%;">
												<option value="">Select</option>
		               			<?php foreach($currency as $currency_details) {?>
		               				<option 
													value="<?php echo $currency_details['currency'];?>" 
													<?php 
														echo ($procurement_data['currency'] == $currency_details['currency']) ? 'selected': '';
													?>>
													<?php echo $currency_details['currency'];?>
													</option>
												<?php } ?>
	             				</select>
	           				</td>
									</tr>
									<tr>
										<td class="kt-font-dark kt-font-lg">Delivery Time</td>
										<td class="kt-font-dark kt-font-xl kt-font-boldest">
											<select class="form-control" name="delivery_time" style="width: 50%;">
	             					<option value="">Select</option>
												<?php foreach($delivery_time as $delivery_time_details) {?>
													<option 
													value="<?php echo $delivery_time_details['dt_value'];?>" 
													<?php 
														echo ($procurement_data['delivery_time'] == $delivery_time_details['dt_value']) ? 'selected': '';
													?>>
													<?php echo $delivery_time_details['dt_value'];?>
													</option>
												<?php } ?>
	             				</select>
	           				</td>
	           				<td class="kt-font-dark kt-font-lg">Country Of Origin</td>
										<td class="kt-font-dark kt-font-xl kt-font-boldest">
											<select class="form-control" name="country_of_origin" style="width: 50%;">
	             					<option value="">Select</option>
												<?php foreach($origin_country as $origin_country_details) {?>
													<option 
													value="<?php echo $origin_country_details['country'];?>" 
													<?php 
														echo ($procurement_data['country_of_origin'] == $origin_country_details['country']) ? 'selected': '';
													?>>
													<?php echo $origin_country_details['country'];?>
													</option>
												<?php } ?>
	             				</select>
	           				</td>
									</tr>
									<tr>
										<td class="kt-font-dark kt-font-lg">Payment</td>
										<td class="kt-font-dark kt-font-xl kt-font-boldest">
											<select class="form-control" name="payment" style="width: 50%;">
               					<option value="">Select</option>
												<?php foreach($payment_terms as $payment_terms_details) {?>
													<option 
													value="<?php echo $payment_terms_details['term_value'];?>" 
													<?php 
														echo ($procurement_data['payment'] == $payment_terms_details['term_value']) ? 'selected': '';
													?>>
													<?php echo $payment_terms_details['term_value'];?>
													</option>
												<?php } ?>
               				</select>
             				</td>
             				<td class="kt-font-dark kt-font-lg">MTC Type</td>
										<td class="kt-font-dark kt-font-xl kt-font-boldest">
											<select class="form-control" name="mtc_type" style="width: 50%;">
	             					<option value="">Select</option>
												<?php foreach($mtc_type as $mtc_type_details) {?>
													<option 
													value="<?php echo $mtc_type_details['mtc_value'];?>" 
													<?php 
														echo ($procurement_data['mtc_type'] == $mtc_type_details['mtc_value']) ? 'selected': '';
													?>>
													<?php echo $mtc_type_details['mtc_value'];?>
													</option>
												<?php } ?>
	             				</select>
	           				</td>
									</tr>
									<tr>
										<td class="kt-font-dark kt-font-lg">Validity</td>
										<td class="kt-font-dark kt-font-xl kt-font-boldest">
											<select class="form-control" name="validity" style="width: 50%;">
	           						<option value="">Select</option>
												<?php foreach($validity as $validity_details) {?>
													<option 
													value="<?php echo $validity_details['validity_value'];?>" 
													<?php 
														echo ($procurement_data['validity'] == $validity_details['validity_value']) ? 'selected': '';
													?>>
													<?php echo $validity_details['validity_value'];?>
													</option>
												<?php } ?>
	             				</select>
	           				</td>
	         					<td class="kt-font-dark kt-font-lg">Packing Type</td>
										<td class="kt-font-dark kt-font-xl kt-font-boldest">
											<select class="form-control" name="packing_type" style="width: 50%;">
	             					<option value="">Select</option>
												<?php foreach($transport_mode as $transport_mode_details) {?>
													<option 
													value="<?php echo $transport_mode_details['mode'];?>" 
													<?php 
														echo ($procurement_data['packing_type'] == $transport_mode_details['mode']) ? 'selected': '';
													?>>
													<?php echo $transport_mode_details['mode'];?>
													</option>
												<?php } ?>
	             				</select>
	           				</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</form>

				<!--end::Form-->
			</div>	
		</div>
	</div>
	<div class="row">
		<div class="col">
			<div class="alert alert-light alert-elevate fade show" role="alert">
				<div class="alert-icon"><i class="flaticon-edit kt-font-brand"></i></div>
				<div class="alert-text terms_conditions_alert">
					<?php echo $footer_text;?>
				</div>
				<div class="alert-icon"  data-toggle="modal" data-target="#edit_terms_and_conditions"><i class="flaticon-edit-1 kt-font-brand edit_terms_and_conditions"></i></div>
			</div>
		</div>
	</div>

	<!--end::Portlet-->
</div>

<!-- end:: Content -->

<div class="modal fade" id="invoice_follow_up" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Follow Up</h5>
				<button type="reset" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
                <form id="add_follow_up_history_form">
               		<div class="row">
               			<div class="col-md-4 form-group">
               				<label for="contact_date">Connect Date</label>
               				<input type="text" name="contact_date" class="form-control validate hasdatepicker" value="<?php echo date('d-m-Y'); ?>">
               			</div>

               			<div class="col-md-4 form-group">
               				<label for="contact_mode">Connect Mode</label>
               				<select class="form-control validate[required]" name="contact_mode">
               					<option value=""></option>
               					<option value="WhatsApp">Whatsapp</option>
               					<option value="Call">Call</option>
               					<option value="Linkedin">LinkedIn</option>
               					<option value="Email">Email</option>
               				</select>
               			</div>

               			<div class="col-md-4 form-group">
               				<label for="email_status">Email Sent</label>
               				<select class="form-control validate[required]" name="email_status">
               					<option value=""></option>
               					<option value="Yes">Yes</option>
               					<option value="No">No</option>
               				</select>
               			</div>

               			<div class="col-md-12 form-group">
               				<label for="comments">Comments</label>
               				<textarea name="comments" class="form-control validate[maxSize[100],required]"></textarea>
               			</div>
               		</div>
               		<div class="clearfix"></div>
               		<div class="row">
               			<div class="col-md-6 align-self-center">
               				<input type="hidden" id="details_id" name="details_id">
               				<button class="btn btn-success add_follow_up_history" type="reset">Submit</button>
               				<button type="reset" class="btn btn-primary" data-toggle="modal" data-backdrop="static" data-target="#taskModal" id="createTask">Create Task</button>
               			</div>
               		</div>
               	</form>
               	<hr/>
               	<h4>Connect History</h4>
                <div>
                	<table class="table table-bordered" id="invoice_connect_history">
                		<thead>
	                		<tr>
	                			<th>Contacted On</th>
	                			<th>Contact Mode</th>
	                			<th>Email Sent</th>
	                			<th>Comments</th>
	                		</tr>
	                	</thead>
	                	<tbody>
                		<?php if(!empty($follow_up_details)) {?>
                			<?php foreach($follow_up_details as $follow_up_single_details) {?>
                			<tr details_id="<?php echo $follow_up_single_details['details_id']; ?>" style="display: none;">
                				<td><?php echo date('d F, Y', strtotime($follow_up_single_details['contact_date']));?></td>
                				<td><?php echo $follow_up_single_details['contact_mode']; ?></td>
                				<td><?php echo $follow_up_single_details['email_status']; ?></td>
                				<td><?php echo $follow_up_single_details['comments']; ?></td>
                			</tr>	
                			<?php }?>
                		<?php }?>
                		</tbody>
                	</table>
                </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="taskModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<form id="add_task_form_data">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">New Task</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
               			<div class="col-12 align-self-center form-group">
               				<textarea class="form-control validate[required]" placeholder="Task Details" id="task_detail" name="task_detail"></textarea>
               			</div>
               			<div class="col-12 align-self-center form-group">
               				<input type="text" class="form-control validate[required]" id="deadline" name="deadline" placeholder="Task Deadline">
               			</div>
               		</div>
				</div>
				<div class="modal-footer">
					<button type="reset" class="btn btn-success add_task">Save Task</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="add_gst_or_discount" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Other Charges</h5>
				<button type="reset" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
       	<form id="add_charges_in_purchase_order_form">
       		<div class="row">
       			<div class="col-md-6 form-group">
       				<label for="contact_date">Charge Name</label>
       				<input type="text" name="charges_name" class="form-control" value="">
       			</div>
       			<div class="col-md-6 form-group">
       				<label for="contact_date">Charge Should Be Added Or Deducted From Net Total</label>
       				<select class="form-control" name="add_minus_charges">
       					<option value=""></option>
       					<option value="+">Add To Net Total</option>
       					<option value="-">Subtract To Net Total</option>
       				</select>
       			</div>
       		</div>
       		<div class="row">
       			
       			<div class="col-md-6 form-group">
       				<label for="contact_date">Charge Value</label>
       				<input type="text" name="charge_value" class="form-control" value="">
       			</div>
       			<div class="col-md-6 form-group" style="padding: 3% 2% 0% 0%;">
       				<button class="btn btn-success add_charges_in_purchase_order" type="reset" style="float: right;">Submit</button>
       			</div>
       		</div>
       		<div class="clearfix"></div>
       	</form>
       	<hr/>
       	<h4>Charges History</h4>
       <div>
        	<table class="table table-bordered" id="charge_history_table">
        		
        	</table>
       </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="edit_terms_and_conditions" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Update Terms And Condition</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<div class="col-lg-12 col-md-9 col-sm-12">
					<textarea class="form-control" id="terms_conditions" rows="3"></textarea>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary update_terms_conditions">Update</button>
			</div>
		</div>
	</div>
</div>
