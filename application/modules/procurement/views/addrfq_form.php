<style type="text/css">
	.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
		padding-top: 4% !important;
	}
	.layer-white{
	    display: none;
	    position: fixed;
	    top: 0em !important;
	    left: 0em !important;
	    width: 100%;
	    height: 100%;
	    text-align: center;
	    vertical-align: middle;
	    background-color: rgba(255, 255, 255, 0.55);
	    opacity: 1;
	    line-height: 1;
	    -webkit-animation-fill-mode: both;
	    animation-fill-mode: both;
	    -webkit-animation-duration: 0.5s;
	    animation-duration: 0.5s;
	    -webkit-transition: background-color 0.5s linear;
	    transition: background-color 0.5s linear;
	    -webkit-user-select: none;
	    -moz-user-select: none;
	    -ms-user-select: none;
	    user-select: none;
	    will-change: opacity;
	    z-index: 9;
	}
	.div-loader{
	    position: absolute;
		top: 50%;
		left: 50%;
		margin: 0px;
		text-align: center;
		z-index: 1000;
		-webkit-transform: translateX(-50%) translateY(-50%);
		transform: translateX(-50%) translateY(-50%);
	}
	.kt-spinner:before {
	    width: 50px;
	    height: 50px;
	    margin-top: -10px;
	}
	.rfq_font {
            font-size: 1rem;
            font-weight: 500;
            line-height: 1.5rem;
            -webkit-transition: color 0.3s ease;
            transition: color 0.3s ease;
            color: #000;
        }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
			<div class="col-lg-12 col-xl-12 order-lg-1 order-xl-1">
				<div class="kt-portlet">
					<?php echo form_open('', array("class" => "kt-form kt-form--label-right", "id" => "addrfq_form", "onsubmit" => "loader()")); ?>
						<div class="kt-portlet__body rfq_font">
							<?php
								$readonly = true;
								if($this->session->userdata('role') == 1 || in_array($this->session->userdata('user_id'), array(23, 38, 65, 73, 85, 158, 189)) || $this->uri->segment('3', 0) == 0) {

									$readonly = false;
								}
							?>
							<div class="form-group row">
								<div class="col-lg-3">
									<label class="">Procurement Person 1</label>
									<div class="form-group row">
										<div class="col-lg-7">
											<select class="form-control validate[]" id="assigned_to_1" name="assigned_to_1">
												<option value="">Choose Purchase Manager</option>
												<?php 
												if($readonly){ 
													foreach ($purchase_person as $value) {
														if(isset($rfq_details) && $rfq_details[0]['assigned_to_1'] == $value['user_id']){
												
															echo "<option value='".$value['user_id']."' selected='selected'>".$value['name']."</option>";
														}
													}
												}else{
													foreach ($purchase_person as $value) {
														$selected = '';
														if(isset($rfq_details) && $rfq_details[0]['assigned_to_1'] == $value['user_id']){
															$selected = 'selected="selected"';
														}
														echo "<option value='".$value['user_id'],"'",$selected.">".$value['name']."</option>";
													}
												}
												?>
											</select>
										</div>
										<div class="col-lg-5">
											<select class="form-control validate[]" name="status_1">
												<option value="">Choose Status</option>
												<?php 
												if($readonly){ 
													
													if(isset($rfq_details) && $rfq_details[0]['assigned_to_1'] == $this->session->userdata('user_id')) {

														foreach (array('pending', 'done', 'query', 'regret') as $status_value) {
														
															$selected = '';
															if(isset($rfq_details) && $rfq_details[0]['status_1'] == $status_value){
																$selected = 'selected="selected"';
															}
															echo "<option value='".$status_value."'".$selected.">".$status_value."</option>";
														}
													}else{

														foreach (array('pending', 'done', 'query', 'regret') as $status_value) {
															
															$selected = '';
															if(isset($rfq_details) && $rfq_details[0]['status_1'] == $status_value){
																$selected = 'selected="selected"';
																echo "<option value='".$status_value."'".$selected.">".$status_value."</option>";
															}
														}
													}
												}else{

													foreach (array('pending', 'done', 'query', 'regret') as $status_value) {
														
														$selected = '';
														if(isset($rfq_details) && $rfq_details[0]['status_1'] == $status_value){
															$selected = 'selected="selected"';
														}
														echo "<option value='".$status_value."'".$selected.">".$status_value."</option>";
													}
												}
												?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-lg-3">
									<label class="">Procurement Person 2</label>
									<div class="form-group row">
										<div class="col-lg-7">
											<select class="form-control validate[]" name="assigned_to_2">
												<option value="">Choose Purchase Manager</option>
												<?php 
												if($readonly){ 
													foreach ($purchase_person as $value) {
														if(isset($rfq_details) && $rfq_details[0]['assigned_to_2'] == $value['user_id']){
												
															echo "<option value='".$value['user_id']."' selected='selected'>".$value['name']."</option>";
														}
													}
												}else{
													foreach ($purchase_person as $value) {
														$selected = '';
														if(isset($rfq_details) && $rfq_details[0]['assigned_to_2'] == $value['user_id']){
															$selected = 'selected="selected"';
														}
														echo "<option value='".$value['user_id'],"'",$selected.">".$value['name']."</option>";
													}
												}
												?>
											</select>
										</div>
										<div class="col-lg-5">
											<select class="form-control validate[]" name="status_2">
												<option value="">Choose Status</option>
												<?php 
												if($readonly){ 
													
													if(isset($rfq_details) && $rfq_details[0]['assigned_to_2'] == $this->session->userdata('user_id')) {

														foreach (array('pending', 'done', 'query', 'regret') as $status_value) {
														
															$selected = '';
															if(isset($rfq_details) && $rfq_details[0]['status_2'] == $status_value){
																$selected = 'selected="selected"';
															}
															echo "<option value='".$status_value."'".$selected.">".$status_value."</option>";
														}
													}else{
														
														foreach (array('pending', 'done', 'query', 'regret') as $status_value) {
															
															$selected = '';
															if(isset($rfq_details) && $rfq_details[0]['status_2'] == $status_value){
																$selected = 'selected="selected"';
																echo "<option value='".$status_value."'".$selected.">".$status_value."</option>";
															}
														}
													}
												}else{

													foreach (array('pending', 'done', 'query', 'regret') as $status_value) {
														
														$selected = '';
														if(isset($rfq_details) && $rfq_details[0]['status_2'] == $status_value){
															$selected = 'selected="selected"';
														}
														echo "<option value='".$status_value."'".$selected.">".$status_value."</option>";
													}
												}
												?>	
											</select>
										</div>
									</div>
								</div>
								<div class="col-lg-3">
									<label class="">Procurement Person 3</label>
									<div class="form-group row">
										<div class="col-lg-7">
											<select class="form-control validate[]" name="assigned_to_3">
												<option value="">Choose Purchase Manager</option>
												<?php 
												if($readonly){ 
													foreach ($purchase_person as $value) {
														if(isset($rfq_details) && $rfq_details[0]['assigned_to_3'] == $value['user_id']){
												
															echo "<option value='".$value['user_id']."' selected='selected'>".$value['name']."</option>";
														}
													}
												}else{
													foreach ($purchase_person as $value) {
														$selected = '';
														if(isset($rfq_details) && $rfq_details[0]['assigned_to_3'] == $value['user_id']){
															$selected = 'selected="selected"';
														}
														echo "<option value='".$value['user_id'],"'",$selected.">".$value['name']."</option>";
													}
												}
												?>
											</select>
										</div>
										<div class="col-lg-5">
											<select class="form-control validate[]" name="status_3">
												<option value="">Choose Status</option>
												<?php 
												if($readonly){ 
													
													if(isset($rfq_details) && $rfq_details[0]['assigned_to_3'] == $this->session->userdata('user_id')) {

														foreach (array('pending', 'done', 'query', 'regret') as $status_value) {
														
															$selected = '';
															if(isset($rfq_details) && $rfq_details[0]['status_3'] == $status_value){
																$selected = 'selected="selected"';
															}
															echo "<option value='".$status_value."'".$selected.">".$status_value."</option>";
														}
													}else{
														
														foreach (array('pending', 'done', 'query', 'regret') as $status_value) {
															
															$selected = '';
															if(isset($rfq_details) && $rfq_details[0]['status_3'] == $status_value){
																$selected = 'selected="selected"';
																echo "<option value='".$status_value."'".$selected.">".$status_value."</option>";
															}
														}
													}
												}else{

													foreach (array('pending', 'done', 'query', 'regret') as $status_value) {
														
														$selected = '';
														if(isset($rfq_details) && $rfq_details[0]['status_3'] == $status_value){
															$selected = 'selected="selected"';
														}
														echo "<option value='".$status_value."'".$selected.">".$status_value."</option>";
													}
												}
												?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-lg-3 assigned_to_date" style="display:none">
									<label class="">Assigned To Date</label>
									<input type="text" class="form-control validate[]" value="<?php echo (isset($rfq_details) && !empty($rfq_details[0]['assigned_to_date'])) ? date('M j, Y g:i A', strtotime($rfq_details[0]['assigned_to_date'])) : date('M j, Y g:i A'); ?>" readonly>
								</div>
								<div class="col-lg-3">
									<label class="">Sent By</label>
									<select class="form-control validate[]" name="rfq_sentby" id="rfq_sentby" <?php if($this->session->userdata('role') != 6 && $this->session->userdata('role') != 1 && $this->session->userdata('user_id') != 22 && $this->session->userdata('user_id') != 38 && $this->session->userdata('user_id') != 33 && $this->session->userdata('user_id') != 248){ echo 'readonly'; }?>>
										<option value="">Choose Sales Person</option>
										<?php foreach ($sales_person as $value) {
											$selected = '';
											if(isset($rfq_details) && $rfq_details[0]['rfq_sentby'] == $value['user_id']){
												$selected = 'selected="selected"';
											}
										?>
											<option value="<?php echo $value['user_id']; ?>" <?php echo $selected; ?>><?php echo $value['name']; ?></option>
										<?php } ?>
									</select>
								</div>
								<?php 
									$company_display = "none";
									if($this->session->userdata('rfq_access')['rfq_add_client_name_access']){ 
										$company_display = "block";
									}
								?>
								<div class="col-lg-3">
									<label class="">Company</label>
									<div class="input-group desai" style="display:<?php echo $company_display; ?>">
										<div class="input-group">
											<input type="text" id="client_search_value" class="form-control" placeholder="Enter Client Name">
											<div class="input-group-prepend client_name_search">
												<span class="input-group-text">
													<i class="la la-search"></i>
												</span>
											</div>
										</div>
										<select class="form-control kt-selectpicker client_details validate[required]" data-size="7" data-live-search="true" tabindex="-98" name="rfq_company">
											<option value="">Select</option>
											<?php 
												if(!empty($clients)){
													foreach ($clients as $key => $value) {
														echo '<option selected value="'.$value['id'].'">'.ucwords(strtolower($value['name'])).' ('.$value['country'].')</option>';
													}
												}
											?>
										</select>
										<div class="input-group-append">
											<button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#add_company">+</button>
										</div>
									</div>
								</div>
								<?php 
									$buyer_display = "none";
									if($this->session->userdata('rfq_access')['rfq_add_member_name_access']){ 
										$buyer_display = "block";
									}
								?>
								<div class="col-lg-3">
									<label class="">Name of Buyer</label>
									<div class="input-group" style="display:<?php echo $buyer_display; ?>">
										<select class="form-control mem_details" data-size="7" data-live-search="true" tabindex="-98" name="rfq_buyer">
											<option value="">Choose Buyer</option>
											<?php 
												if(!empty($members)){
													foreach ($members as $key => $value) {
														$selected = '';
														if(isset($rfq_details) && $rfq_details[0]['rfq_buyer'] == $value['comp_dtl_id']) {
															$selected='selected="selected"';
														}
														echo '<option '.$selected.' value="'.$value['comp_dtl_id'].'">'.ucwords(strtolower($value['member_name'])).'</option>';
													}
												}
											?>
										</select>
										<div class="input-group-append">
											<button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#add_member" id="addNewMember" disabled="disabled">+</button>
										</div>
									</div>
								</div>
								<?php 
								if(in_array($this->session->userdata('user_id'), array(2, 15, 16, 23, 38, 178, 65, 73, 158, 317))){ 
								
									echo '<div class="col-lg-3">';
								}else{

									echo '<div class="col-lg-3 kt-hidden">';
								}								
								?>
									<label>Company Type</label>
									<div class="input-group">
										<div class="input-group-prepend"><span class="input-group-text"><i class="la la-contao"></i></span></div>
										<select class="form-control validate[required]" name="type">
											<option value="">Select Type</option>
											
											<?php
												$omtubes_option = "selected";
												$zengineer_option = $instinox_option = "";
												if(isset($rfq_details)){

													if($rfq_details[0]['type'] == 'zen'){
														
														$zengineer_option = "selected";
													}elseif($rfq_details[0]['type'] == 'in'){
														
														$instinox_option = "selected";
													}
												}
												echo '<option value="om" '.$omtubes_option.'>Om Tubes</option>';
												echo '<option value="zen" '.$zengineer_option.'>Zengineer</option>';
												echo '<option value="in" '.$instinox_option.'>Instinox</option>';
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-lg-3">
									<label class="">Rank</label>
									<input type="text" class="form-control validate[]" name="rfq_rank" value="<?php if(isset($rfq_details)) echo $rfq_details[0]['rfq_rank']; ?>" id="rfq_rank" <?php if($this->session->userdata('role') != 6 && $this->session->userdata('user_id') != 22 && $this->session->userdata('user_id') != 38 && $this->session->userdata('role') != 1 && $this->session->userdata('user_id') != 33 && $this->session->userdata('user_id') != 248){ echo 'readonly'; }?>>
									<!-- <input type="hidden" name="client_source" value="<?php if(isset($rfq_details)) echo $rfq_details[0]['client_source']; ?>" id="client_source"> -->
								</div>

								<div class="col-lg-3">
									<label class="">Last Buy</label>
									<input type="text" class="form-control validate[]" name="rfq_lastbuy" value="<?php if(isset($rfq_details)) echo $rfq_details[0]['rfq_lastbuy']; ?>" id="rfq_lastbuy" <?php if($this->session->userdata('role') != 6 && $this->session->userdata('user_id') != 22 && $this->session->userdata('user_id') != 38 && $this->session->userdata('role') != 1 && $this->session->userdata('user_id') != 33 && $this->session->userdata('user_id') != 178 && $this->session->userdata('user_id') != 317){ echo 'readonly'; }?>>
								</div>

								
								<?php /*<div class="col-lg-3">
									<label class="">Reference</label>
									<input type="text" class="form-control validate[]" value="<?php if(isset($rfq_details)) echo $rfq_details[0]['reference']; ?>" name="reference">
								</div> */?>

								<div class="col-lg-6">
									<label>RFQ Subject</label>
									<input type="text"  class="form-control validate[]" name="rfq_subject" id="rfq_subject" value="<?php if(isset($rfq_details)) echo $rfq_details[0]['rfq_subject']; ?>" <?php if($this->session->userdata('role') != 6 && $this->session->userdata('user_id') != 22 && $this->session->userdata('user_id') != 38 && $this->session->userdata('role') != 1 && $this->session->userdata('user_id') != 33 && $this->session->userdata('user_id') != 178 && $this->session->userdata('user_id') != 174 && $this->session->userdata('user_id') != 317){ echo 'readonly'; }?>>
								</div>
							</div>
							<div class="form-group row">
								
								<div class="col-lg-3">
									<label class="">Importance</label>
									<select class="form-control validate[]" name="rfq_importance">
										<option value="">Select</option>
										<option value="L" <?php if(isset($rfq_details) && $rfq_details[0]['rfq_importance'] == 'L'){ echo 'selected="selected"'; } ?>>Low</option>
										<option value="M" <?php if(isset($rfq_details) && $rfq_details[0]['rfq_importance'] == 'M'){ echo 'selected="selected"'; } ?>>Medium</option>
										<option value="H" <?php if(isset($rfq_details) && $rfq_details[0]['rfq_importance'] == 'H'){ echo 'selected="selected"'; } ?>>High</option>
										<option value="V" <?php if(isset($rfq_details) && $rfq_details[0]['rfq_importance'] == 'V'){ echo 'selected="selected"'; } ?>>Very High</option>
									</select>
								</div>
								<div class="col-lg-3">
									<label class="">Status</label>
									<select class="form-control validate[]" name="rfq_status">
										<option value="waiting" <?php if(isset($rfq_details) && $rfq_details[0]['rfq_status'] == 'waiting'){ echo 'selected="selected"'; } ?>>Waiting</option>
										<option value="pending" <?php if(isset($rfq_details) && $rfq_details[0]['rfq_status'] == 'pending'){ echo 'selected="selected"'; } ?>>Pending</option>
										<option value="query" <?php if(isset($rfq_details) && $rfq_details[0]['rfq_status'] == 'query'){ echo 'selected="selected"'; } ?>>Query</option>
										<option value="regret" <?php if(isset($rfq_details) && $rfq_details[0]['rfq_status'] == 'regret'){ echo 'selected="selected"'; } ?>>Regret</option>
										<option value="done" <?php if(isset($rfq_details) && $rfq_details[0]['rfq_status'] == 'done'){ echo 'selected="selected"'; } ?>>Done</option>
									</select>
								</div>
								<div class="col-lg-3 rfq_regret_reason_div" style="display:<?php echo (isset($rfq_details) && $rfq_details[0]['rfq_status'] == 'regret') ?'block':'none'; ?>">
									<label class="">Regret Reason</label>
									<select class="form-control validate[required]" name="rfq_regret_reason">

										<option value="" <?php if(isset($rfq_details) && $rfq_details[0]['rfq_regret_reason'] == '0'){ echo 'selected="selected"'; } ?>>Select Regret Reason</option>
										<?php foreach ($close_reason as $key => $value) { ?>

											<option value="<?php echo $value['id'];?>" <?php if(isset($rfq_details) && $rfq_details[0]['rfq_regret_reason'] == $value['id']){ echo 'selected="selected"'; } ?>><?php echo $value['name'];?></option>
										<?php }?>
									</select>
								</div>
								<?php if(isset($rfq_id)){ ?>
								<div class="col-lg-3">
									<button type="button" data-toggle="modal" data-target="#notes-modal" class="btn btn-xl btn-primary notes" title="Add Notes" style="margin-top: 25px;">Notes</button>
									<!-- <button type="button" data-toggle="modal" data-target="#query-modal" class="btn btn-xl btn-primary queries" title="Query" style="margin-top: 25px;">Query</button> -->
								</div>
								<?php } ?>
							</div>
							<div class="form-group row">
								<div class="col-lg-3">
									<label class="">Closing Date</label>
									<input type="text" class="form-control validate[]" id="rfq_closedate_time"  value="<?php if(isset($rfq_details) && $rfq_details[0]['rfq_closedate'] != ''){ echo $rfq_details[0]['rfq_closedate']; } ?>" name="rfq_closedate">
								</div>
								<div class="col-lg-6">
									<label class="">Reference</label>
									<input type="text" class="form-control validate[]" value="<?php if(isset($rfq_details)) echo $rfq_details[0]['reference']; ?>" name="reference">
								</div>
								<div class="col-lg-3">
									<label class="">Product Family</label>
									<select class="form-control validate[required]" name="product_family">
										<option value="">Select Product Family</option>
										<option value="piping" 
										<?php echo ($rfq_details[0]['product_family'] == 'piping')?'selected':''?>>
											Piping
										</option>
										<option value="instrumentation"
										<?php echo ($rfq_details[0]['product_family'] == 'instrumentation')?'selected':''?>>
											Instrumentation
										</option>
										<option value="precision"
										<?php echo ($rfq_details[0]['product_family'] == 'precision')?'selected':''?>>
											Precision
										</option>
										<option value="tubing"
										<?php echo ($rfq_details[0]['product_family'] == 'tubing')?'selected':''?>>
											Tubing
										</option>
										<option value="industrial"
										<?php echo ($rfq_details[0]['product_family'] == 'industrial')?'selected':''?>>
											Industrial
										</option>
										<option value="fastener"
										<?php echo ($rfq_details[0]['product_family'] == 'fastener')?'selected':''?>>
											Fastener
										</option>
										<option value="valve"
										<?php echo ($rfq_details[0]['product_family'] == 'valve')?'selected':''?>>
											Valve
										</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-lg-3">
									<label class="">Client Type</label>
									<select class="form-control validate[]" name="client_type">
										<option value="">Select</option>
										<option value="trader"
										<?php echo ($rfq_details[0]['client_type'] == 'trader')?'selected':''?>>
											Trader
										</option>
										<option value="contractor"
										<?php echo ($rfq_details[0]['client_type'] == 'contractor')?'selected':''?>>
											Contractor
										</option>
										<option value="end_user"
										<?php echo ($rfq_details[0]['client_type'] == 'end_user')?'selected':''?>>
											End User
										</option>
									</select>
								</div>
								<div class="col-lg-3">
									<label class="">RFQ Type</label>
									<select class="form-control validate[]" name="rfq_type">
										<option value="">Select</option>
										<option value="bid"
										<?php echo ($rfq_details[0]['rfq_type'] == 'bid')?'selected':''?>>
											Bid
										</option>
										<option value="buy"
										<?php echo ($rfq_details[0]['rfq_type'] == 'buy')?'selected':''?>>
											Buy
										</option>
									</select>
								</div>
								<div class="col-lg-3">
									<label>Number OF OEM</label>
									<input type="text"  class="form-control validate[]" name="oem_number" value="<?php if(isset($rfq_details)) echo $rfq_details[0]['oem_number']; ?>">
								</div>
							<?php if($this->session->userdata('rfq_access')['rfq_priority']){ ?>
								<div class="col-lg-3">
									<label class="form-control-label">Priority</label>
									<div class="kt-font-info col-lg-10 col-md-9 col-sm-12">
										<input type="hidden" id="rfq_priority" name="priority" value="<?php if(isset($rfq_details)) echo $rfq_details[0]['priority'];?>" />
									</div>
								</div>
								<div class="col-lg-3 form-group row rfq_priority_reason" style="display:<?php echo (isset($rfq_details) && $rfq_details['0']['priority'] != 0) ? 'block' : 'none';?>">
									<label class="form-control-label">Priority Reason</label>
									<input type="text" class="form-control validate[required]" id="priority_reason" name="priority_reason" value="<?php if(isset($rfq_details)) echo $rfq_details[0]['priority_reason'];?>">
									<small class="form-text text-danger" id="reason_error"></small>
								</div>
							<?php } ?>
							</div>
							<!-- new -->
							<div class="form-group row">
								<table class="table table-bordered">
					        		<thead>
									   	<tr>
									    	<th class="kt-font-dark kt-font-lg kt-font-bold" colspan="5" style="width: 10%;">
									    		Add Vendor
									    		<a href="#" class="btn btn-icon add_vendor_modal" data-toggle="modal" data-target="#add_gst_or_discount">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--md">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<polygon points="0 0 24 0 24 24 0 24"></polygon>
															<path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
															<path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" fill="#000000"></path>
														</g>
													</svg>
												</a>
									    	</th>
									   	</tr>
									   	<tr>
									   		<th class="kt-font-dark kt-font-lg kt-font-bold kt-align-left" style="width: 5%;">
									   			#<span class="checkbox_checked_count" count=0>(0)</span>
									   			<hr>
									   			<label class="kt-checkbox kt-checkbox--bold kt-checkbox--dark">
													<input type="checkbox" name="quotation_id_all" value="">
													<span></span>
												</label>
									   		</th>
									   		<th class="kt-font-dark kt-font-lg kt-font-bold kt-align-center" style="width: 15%;">
									   			Product
									   			<hr>
									   			Material
									   		</th>
									   		<th class="kt-font-dark kt-font-lg kt-font-bold kt-align-left" style="width: 70%;">
									   			Description
									   			<hr>
									   			Details
									   		</th>
									    	<th class="kt-font-dark kt-font-lg kt-font-bold kt-align-center" style="width: 10%;">
									    		Quantity
									    		<hr>
									    		Unit
									    	</th>
									   	</tr>
									</thead>
									<tbody>
										<?php 
											if(!empty($quotation_details)){ 
												foreach ($quotation_details as $quotation_details_key => $quotation_details_value) { $current_key= $quotation_details_key+1;
										?>
										<tr class="all_tr">
											<td class="kt-font-dark kt-font-xl kt-font-bold kt-align-left">
												<label class="kt-checkbox kt-checkbox--bold kt-checkbox--dark">
													<input type="checkbox" name="quotation_id[]" value="<?php echo $quotation_details_value['id']; ?>"> <?php echo $current_key; ?>
													<span></span>
												</label>
											</td>
											<td class="kt-font-dark kt-font-lg kt-font-bold kt-align-center">
												<span>
													<em><?php echo $quotation_details_value['product_name'];?></em>
													<hr>
													<em><?php echo $quotation_details_value['material_name'];?></em>
												</span>
											</td>
											<td class="kt-font-dark kt-font-lg kt-font-bold kt-align-left">
												<?php echo $quotation_details_value['description'];?>
											</td>
											<td class="kt-font-dark kt-font-lg kt-font-bold kt-align-center">
												<span>
													<em><?php echo $quotation_details_value['quantity'];?></em>
													<hr>
													<em><?php echo $quotation_details_value['units'];?></em>
												</span>
											</td>
										</tr>
										<?php  }
											}
										?>
									</tbody>
					        	</table>
							</div>
							<div class="form-group row">
								<table class="table table-bordered">
									<thead>
									   	<tr>
									   		<th class="kt-font-dark kt-font-lg kt-font-bold" style="width: 10%;">Sr. No</th>
									   		<th class="kt-font-dark kt-font-lg kt-font-bold kt-align-left" style="width: 20%;">Vendor Name</th>
									   		<th class="kt-font-dark kt-font-lg kt-font-bold kt-align-center" style="width: 20%;">Vendor Status</th>
									   		<th class="kt-font-dark kt-font-lg kt-font-bold kt-align-center" style="width: 15%;">Evaluate Price</th>
									    	<th class="kt-font-dark kt-font-lg kt-font-bold kt-align-center" style="width: 15%;">Evaluate Delivery</th>
									    	<th class="kt-font-dark kt-font-lg kt-font-bold kt-align-center" style="width: 20%;">Action</th>
									   	</tr>
									</thead>
									<div class="layer-white">
										<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
									</div>
									<tbody class="vendor_assign_to_rfq_body">

									</tbody>
								</table>
							</div>
							
							<?php
							$display = "none"; 
							if(isset($rfq_details) && $rfq_details[0]['rfq_dtl_id'] >0){
								$display = "block";
							}  
							?>
							<!-- <div class="row" id="preview_div" style="display: <?php echo $display; ?>">
								<div class="col-12">
									<table class="table table-bordered">
										<thead>
												<tr>
												<th colspan="7" style="">
													Add RFQ Items
												</th>
											</tr>
											<tr>
												<th width="5%">Sr. #</th>
												<th width="20%">Product</th>
												<th width="20%">Material</th>
												<th width="30%">Description</th>
												<th width="5%">Quantity</th>
												<th width="10%">Unit</th>
												<th width="10%">Action</th>
											</tr>
										</thead>
										<tbody id="tbody">
										<?php 
											if(!empty($rfq_details) && $rfq_details[0]['product_id'] != ''){ 
												foreach ($rfq_details as $key => $value) {
										?>
											<tr>
												<td><?php echo $key+1; ?></td>
												<td>
													<select class="form-control products" name="product_id[]">
														<option value="">Select Product</option>
														<?php foreach($product as $pd){
															$selected = '';
															if($value['product_id'] == $pd['id']){
																$selected = 'selected="selected"';
															}
															echo '<option value="'.$pd['id'].'" '.$selected.'>'.$pd['name'].'</option>';
														} ?>
													</select>
												</td>
												<td>
													<select class="form-control products" name="material_id[]">
														<option value="">Select Material</option>
														<?php foreach($material as $mt){
															$selected = '';
															if($value['material_id'] == $mt['id']){
																$selected = 'selected="selected"';
															}
															echo '<option value="'.$mt['id'].'" '.$selected.'>'.$mt['name'].'</option>';
														} ?>
													</select>
												</td>
												<td>
													<textarea class="form-control validate[required]" name="description[]"><?php echo $value['description']; ?></textarea>
												</td>
												<td>
													<input type="text" class="form-control validate[required,custom[onlyNumberSp]] quantity" name="quantity[]" value="<?php echo $value['quantity']; ?>">
												</td>
												<td>
													<select class="form-control products" name="unit[]">
														<option value="">Select Unit</option>
														<?php foreach($units as $un){
															$selected = '';
															if($value['unit'] == $un['unit_id']){
																$selected = 'selected="selected"';
															}
															echo '<option value="'.$un['unit_id'].'" '.$selected.'>'.$un['unit_value'].'</option>';
														} ?>
													</select>
												</td>
												<td>
													<button type="button" class="btn btn-sm btn-danger delRow">Delete</button>
												</td>
											</tr>	
										<?php  	}
											}
										?>
										</tbody>
									</table>
								</div>
							</div>	 -->					
						</div>
						<div class="kt-portlet__foot">
							<div class="kt-form__actions">
								<div class="row">
									<div class="col-lg-4"></div>
									<div class="col-lg-8">
										<?php if(isset($rfq_details)) echo '<input type="hidden" name="rfq_mst_id" value="'.$rfq_id.'">'; ?>
										<button type="submit" class="btn btn-primary" name="save_rfq">Save RFQ</button>
									</div>
								</div>
							</div>
						</div>
					<?php echo form_close(); ?>

					<br/><br/>
					<?php if(isset($rfq_id)){ ?>
					<!-- <div class="row">
						<div class="col-12">
							<div class="tab-content" style="padding: 50px;">
								<div class="tab-pane active" id="kt_tabs_1_1" role="tabpanel">
									<button type="button" class="btn btn-primary btn-sm" id="add_rfq_vendor" style="float: right;">Add Row</button>
									<table class="table table-bordered" id="vendor_table">
										<thead>
											<tr>
												<th colspan="5" style="">
													Add vendor for this rfq
												</th>
											</tr>
											<tr>
												<th>Sr. #</th>
												<th>Vendor</th>
												<th>Status</th>
												<th>Evaluate Offer</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
											<?php 
												if(!empty($rfq_to_vendor)){
													$i=0;
													foreach ($rfq_to_vendor as $key => $value) { 
											?>
												<tr>
													<td><?php echo ++$i; ?></td>
													<td>
														<select class="form-control vendor" disabled="disabled">
														<?php foreach($vendors as $ven){
															$selected = '';
															if($ven['vendor_id'] == $value['vendor_id']){
																$selected = 'selected="selected"';
															}
															echo '<option value="'.$ven['vendor_id'].'" '.$selected.'>'.$ven['vendor_name'].'</option>';
														} ?>
														</select>
													</td>
													<td>
														<select class="form-control vendor_status">
															<option value="pending" <?php if($value['vendor_status'] == 'pending'){echo 'selected="selected"';}?>>Pending</option>
															<option value="query" <?php if($value['vendor_status'] == 'query'){echo 'selected="selected"';}?>>Query</option>
															<option value="regret" <?php if($value['vendor_status'] == 'regret'){echo 'selected="selected"';}?>>Regret</option>
															<option value="done" <?php if($value['vendor_status'] == 'done'){echo 'selected="selected"';}?>>Done</option>
														</select>
													</td>
													<td>
														<select class="form-control evaluate_price" style="width:30%; display: inline-block;">
															<option value="" <?php if($value['evaluate_price'] == ''){echo 'selected="selected"';}?>>Select Price</option>
															<option value="high" <?php if($value['evaluate_price'] == 'high'){echo 'selected="selected"';}?>>H</option>
															<option value="low" <?php if($value['evaluate_price'] == 'low'){echo 'selected="selected"';}?>>L</option>
														</select>
														<select class="form-control evaluate_delivery" style="width:45%; display: inline-block; margin-left: 5%">
															<option value="" <?php if($value['evaluate_delivery'] == ''){echo 'selected="selected"';}?>>Select Delivery</option>
															<option value="high" <?php if($value['evaluate_delivery'] == 'high'){echo 'selected="selected"';}?>>High</option>
															<option value="low" <?php if($value['evaluate_delivery'] == 'low'){echo 'selected="selected"';}?>>Low</option>
														</select>
													</td>
													<td>
														<a href="<?php echo site_url('procurement/viewPdf/'.$value['connect_id']); ?>" target="_blank" class="btn btn-xl btn-icon btn-icon-md viewRFQ" title="View PDF"><i class="la la-eye"></i></a> 
														<button type="button" class="btn btn-xl btn-icon btn-icon-md saveVendor" title="Save"><i class="la la-save"></i></button>
													</td>
												</tr>
											<?php 
													}
												}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div> -->
					<?php } ?>

					<!-- <div class="row">
						<div class="col-12">
							<div class="tab-content" style="padding: 10px;">
								<div class="tab-pane active" id="kt_tabs_1_1" role="tabpanel">
									<form class="kt-form kt-form--label-right" id="save_vendor_information">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th width="5%">Sr. #</th>
													<th width="15%">Product</th>
													<th width="15%">Material</th>
													<th>Description</th>
													<th width="5%">Quantity</th>
													<th width="15%">Vendor</th>
												</tr>
											</thead>
											<tbody id="tbody">
											<?php 
												if(!empty($quotation_details)){ 
													foreach ($quotation_details as $quotation_details_key => $quotation_details_value) { $current_key= $quotation_details_key+1;
											?>
												<tr>
													<td><?php echo $current_key; ?></td>
													<input type="text" name="<?php echo 'id_'.$current_key; ?>" value="<?php echo $quotation_details_value['id']; ?>" hidden>
													<td>
														<select class="form-control" name="<?php echo 'product_id_'.$current_key; ?>">
															<option value="">Select Product</option>
															<?php foreach($product as $single_product){
																$selected = '';
																if($single_product['name'] == $quotation_details_value['product_name']){
																	$selected = 'selected="selected"';
																}
																echo '<option value="'.$single_product['id'].'" '.$selected.'>'.$single_product['name'].'</option>';
															} ?>
														</select>
													</td>
													<td>
														<select class="form-control" name="<?php echo 'material_id_'.$current_key; ?>">
															<option value="">Select Material</option>
															<?php foreach($material as $single_material){
																$selected = '';
																if($single_material['name'] == $quotation_details_value['material_name']){
																	$selected = 'selected="selected"';
																}
																echo '<option value="'.$single_material['id'].'" '.$selected.'>'.$single_material['name'].'</option>';
															} ?>
														</select>
													</td>
													<td>
														<input type="text" class="form-control" value="<?php echo $quotation_details_value['description']; ?>">
													</td>
													<td>
														<input type="text" class="form-control" value="<?php echo $quotation_details_value['quantity']; ?>">
													</td>
													<td>
														<select class="form-control products" name="<?php echo 'vendor_name_'.$current_key; ?>">
															<option value="">Select Vendor</option>
															<?php foreach($quotation_details_value['vendor_list'] as $single_vendor_list){
																$selected = '';
																if($single_vendor_list['vendor_id'] == $quotation_details_value['vendor_id']){
																	$selected = 'selected="selected"';
																}
																echo '<option value="'.$single_vendor_list['vendor_id'].'" '.$selected.'>'.$single_vendor_list['vendor_name'].'</option>';
															} ?>
														</select>
													</td>
												</tr>
											<?php  }
												}
											?>
											</tbody>
										</table>
									</form>
								</div>
							</div>
							<div class="kt-portlet__foot">
								<div class="kt-form__actions">
									<div class="row">
										<div class="col-lg-4"></div>
										<div class="col-lg-8">
											<button type="submit" class="btn btn-primary save_vendor_details">Save Vendor Details</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<div class="tab-content" style="padding: 10px;">
								<div class="tab-pane active" id="kt_tabs_1_1" role="tabpanel">
									<form class="kt-form kt-form--label-right" id="save_vendor_information">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th width="5%">Sr. #</th>
													<th width="15%">Product</th>
													<th width="15%">Material</th>
													<th>Description</th>
													<th width="5%">Quantity</th>
													<th width="15%">Vendor</th>
												</tr>
											</thead>
											<tbody id="tbody">
											<?php 
												if(!empty($quotation_details)){ 
													foreach ($quotation_details as $quotation_details_key => $quotation_details_value) { $current_key= $quotation_details_key+1;
											?>
												<tr>
													<td><?php echo $current_key; ?></td>
													<input type="text" name="<?php echo 'id_'.$current_key; ?>" value="<?php echo $quotation_details_value['id']; ?>" hidden>
													<td>
														<select class="form-control" name="<?php echo 'product_id_'.$current_key; ?>">
															<option value="">Select Product</option>
															<?php foreach($product as $single_product){
																$selected = '';
																if($single_product['name'] == $quotation_details_value['product_name']){
																	$selected = 'selected="selected"';
																}
																echo '<option value="'.$single_product['id'].'" '.$selected.'>'.$single_product['name'].'</option>';
															} ?>
														</select>
													</td>
													<td>
														<select class="form-control" name="<?php echo 'material_id_'.$current_key; ?>">
															<option value="">Select Material</option>
															<?php foreach($material as $single_material){
																$selected = '';
																if($single_material['name'] == $quotation_details_value['material_name']){
																	$selected = 'selected="selected"';
																}
																echo '<option value="'.$single_material['id'].'" '.$selected.'>'.$single_material['name'].'</option>';
															} ?>
														</select>
													</td>
													<td>
														<input type="text" class="form-control" value="<?php echo $quotation_details_value['description']; ?>">
													</td>
													<td>
														<input type="text" class="form-control" value="<?php echo $quotation_details_value['quantity']; ?>">
													</td>
													<td>
														<select class="form-control products" name="<?php echo 'vendor_name_'.$current_key; ?>">
															<option value="">Select Vendor</option>
															<?php foreach($quotation_details_value['vendor_list'] as $single_vendor_list){
																$selected = '';
																if($single_vendor_list['vendor_id'] == $quotation_details_value['vendor_id']){
																	$selected = 'selected="selected"';
																}
																echo '<option value="'.$single_vendor_list['vendor_id'].'" '.$selected.'>'.$single_vendor_list['vendor_name'].'</option>';
															} ?>
														</select>
													</td>
												</tr>
											<?php  }
												}
											?>
											</tbody>
										</table>
									</form>
								</div>
							</div>
							<div class="kt-portlet__foot">
							</div>
						</div>
					</div> -->
					<th>
					</th>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="add_gst_or_discount" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Update Vendor Assign to RFQ</h5>
				<button type="reset" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
		       	<form id="add_vendor_for_rfq">
		       		<div class="row">
		       			<div class="col-md-12 form-group">
		       				<label for="contact_date">Vendor</label>
		       				<!-- <select class="form-control vendor_name_select_picker" name="vendor_id" search="true" multiple> -->
							<select class="form-control vendor_name_select_picker" multiple="" data-actions-box="true" name="vendor_id" tabindex="-98" data-live-search="true">
								<?php foreach($vendor_list as $single_vendor_list){
									echo '<option value="'.$single_vendor_list['vendor_id'].'" data-subtext="'.$single_vendor_list['vendor_sub_name'].'">'.$single_vendor_list['vendor_name'].'</option>';
								} ?>
							</select>
		       			</div>
		       		</div>
		       		<div class="clearfix"></div>
		       	</form>
		       	<hr/>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
				<button class="btn btn-success submit_add_vendor_form" type="reset" style="float: right;">Submit</button>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="update_vendor_to_rfq" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Update Vendor Assign to RFQ</h5>
				<button type="reset" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body update_vendor_for_rfq_div kt-scroll" data-scroll="true" style="height: 600px">
		       	
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger close_update_vendor_form" data-dismiss="modal" aria-label="Close">Close</button>
				<button class="btn btn-success submit_update_vendor_form" type="reset" style="float: right;">Update</button>
			</div>
		</div>
	</div>
</div>

<!--begin::Client form-->
<div class="modal fade" id="notes-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Notes</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
                <form action="<?php echo site_url('procurement/addNotes'); ?>" method="post" id="rfq_note_frm">
               		<div class="row">
               			<div class="col-md-6 form-group">
               				<label for="notes">Add Note</label>
               				<textarea id="notes" name="notes" class="form-control validate[maxSize[100],required]"></textarea>
               			</div>
               			<div class="col-md-6 align-self-center">
               				<input type="hidden" name="rfq_id" value="<?php echo $rfq_id; ?>">
               				<button class="btn btn-success" type="submit">Submit</button>
               			</div>
               		</div>
               	</form>
               	<hr/>
               	<h4>Notes History</h4>
                <div id="tab_history">
                	<table class="table table-bordered" id="notes_table">
                		<thead>
	                		<tr>
	                			<th width="70%">Notes</th>
	                			<th width="30%">Added On</th>
	                		</tr>
	                	</thead>
	                	<tbody>
                		<?php 
                			foreach ($rfq_notes as $key => $value) {
                				echo '<tr connect_id = "'.$value['connect_id'].'" type="'.$value['type'].'">
                					<td>'.$value['note'].'</td>
                					<td>'.date('d M h:i:a', strtotime($value['entered_on'])).'</td>
                				</tr>';
                			}
                		?>
                		</tbody>
                	</table>
                </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="query-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">RFQ Queries</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
                <form action="<?php echo site_url('procurement/addQuery'); ?>" method="post" id="rfq_query_frm">
               		<div class="row">
               			<div class="col-md-6 form-group">
               				<label for="notes"><?php if($this->session->userdata('role') == 5) { ?>Reply Query <?php } if($this->session->userdata('role') != 5) { ?>Add Query<?php } ?></label>
               				<textarea id="notes" name="notes" class="form-control validate[maxSize[100],required]"></textarea>
               			</div>
               			<div class="col-md-6 align-self-center">
               				<input type="hidden" name="rfq_id" value="<?php echo $rfq_id; ?>">
               				<button class="btn btn-success" type="submit">Submit</button>
               			</div>
               		</div>
               	</form>
               	<hr/>
               	<h4>Queries History</h4>
                <div id="tab_history">
                	<table class="table" id="query_table">
                		<tbody>
                		<?php 
                			foreach ($rfq_query as $key => $value) {
                				$align="left";
                				if($value['entered_by'] == $this->session->userdata('user_id')){
                					$align="right";
                				}
                				echo '<tr connect_id = "'.$value['connect_id'].'" type="'.$value['type'].'">
                					<td>
                						<div style="text-align:'.$align.'">
                							'.$value['note'].'<br/>
                							<span style="font-size: 10px;">'.date('d M h:i a', strtotime($value['entered_on'])).'</span>
                						</div>
                					</td>
                				</tr>';
                			}
                		?>
                		</tbody>
                	</table>
                </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="add_company" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<?php echo form_open('', array('id' => 'rfq_addCompany', 'class' => 'kt-form kt-form--label-right'));?>
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add Client</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="recipient-name" class="form-control-label">Company Name:</label>
								<input type="text" class="form-control validate[required] name" id="name" name="name">
							</div>
							<div class="row">
								<div class="col-12">
									<div id="company_result_client" style="background-color: #fff; z-index: 100; position: absolute; border: 1px solid; width: 80%; max-height: 100px; height: 100px; overflow-y: scroll; display: none; margin-top: -25px;">
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="message-text" class="form-control-label">Country:</label>
								<select class="form-control validate[required]" id="country_id" name="country_id">
									<option value="" disbaled>Select</option>
									<?php 
										foreach ($country as $key => $value) {
											echo '<option value="'.$value['id'].'" region_id="'.$value['region_id'].'" readonly>'.ucwords(strtolower($value['name'])).'</option>';
										}
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="message-text" class="form-control-label">Region:</label>
								<select class="form-control validate[required]" id="region_id" name="region_id">
									<option value="" disbaled>Select</option>
									<?php 
										foreach ($region as $key => $value) {
											echo '<option value="'.$value['id'].'" >'.ucwords(strtolower($value['name'])).'</option>';
										}
									?>
								</select>
							</div>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="message-text" class="form-control-label">Website:</label>
								<input type="text" class="form-control validate[custom[url]]" id="website" name="website">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Add Client</button>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>

<div class="modal fade" id="add_member" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<?php echo form_open('', array('id' => 'addMember', 'class' => 'kt-form kt-form--label-right'));?>
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add Company Member</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-4 col-sm-12">
							<label for="message-text" class="form-control-label">Name:</label>
							<input type="text" class="form-control validate[required]" id="member_name" name="member_name">
						</div>

						<div class="col-md-4 col-sm-12">
							<label for="message-text" class="form-control-label">Email:</label>
							<input type="email" class="form-control validate[required,custom[email]]" id="email" name="email">
						</div>

						<div class="col-md-4 col-sm-12">
							<label for="message-text" class="form-control-label">Mobile #:</label>
							<input type="number" class="form-control validate[custom[onlyNumberSp]]" id="mobile" name="mobile">
						</div>
					</div>
					<div class="row">
						<div class="col-md-4 col-sm-12">
							<label for="message-text" class="form-control-label">Telephone:</label>
							<input type="number" class="form-control validate[custom[onlyNumberSp]]" id="telephone" name="telephone">
						</div>
					
						<div class="col-md-4 col-sm-12">
							<label for="message-text" class="form-control-label">Whatsapp #:</label>
							<!-- <input type="number" class="form-control validate[custom[onlyNumberSp]]" id="is_whatsapp" name="is_whatsapp"> -->
							<select class="form-control" id="is_whatsapp" name="is_whatsapp">
								<!-- <option value="">Select</option> -->
								<option value="Yes">Yes</option>
								<option value="No">No</option>
							</select>
						</div>
						<div class="col-md-4 col-sm-12">
							<label for="message-text" class="form-control-label">Skype ID:</label>
							<input type="text" class="form-control" id="skype" name="skype">
						</div>						
					</div>
				</div>
				<div class="modal-footer">
					<button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" id="addMemberBtn" class="btn btn-primary">Add Member</button>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>