<tr role="row">
	<th 
	class="<?php echo $sorting['id']['class_name'];?> sorting_search" 
	sorting_name="id"
	sorting_value="<?php echo $sorting['id']['value'];?>"
	style="width: 5%;">Sr#</th>
	<th 
	class="<?php echo $sorting['procurement_no']['class_name'];?> sorting_search" 
	sorting_name="procurement_no"
	sorting_value="<?php echo $sorting['procurement_no']['value'];?>"
	style="width: 8%;">PO#</th>
	<th 
	class="sorting_search" 
	sorting_name="procurement_person"
	sorting_value=""
	style="width: 8%;">Work Order No</th>
	<th 
	class="<?php echo $sorting['procurement_person']['class_name'];?> sorting_search" 
	sorting_name="procurement_person"
	sorting_value="<?php echo $sorting['procurement_person']['value'];?>"
	style="width: 8%;">Made by</th>
	<th 
	class="<?php echo $sorting['sales_person']['class_name'];?> sorting_search" 
	sorting_name="sales_person"
	sorting_value="<?php echo $sorting['sales_person']['value'];?>"
	style="width: 8%;">Sales Person</th>
	<th 
	class="<?php echo $sorting['add_time']['class_name'];?> sorting_search" 
	sorting_name="add_time"
	sorting_value="<?php echo $sorting['add_time']['value'];?>"
	style="width: 8%;">Date</th>
	<th 
	class="<?php echo $sorting['vendor_name']['class_name'];?> sorting_search" 
	sorting_name="vendor_name"
	sorting_value="<?php echo $sorting['vendor_name']['value'];?>"
	style="width: 11%;">Vendor Name</th>
	<th 
	class="<?php echo $sorting['net_total']['class_name'];?> sorting_search" 
	sorting_name="net_total"
	sorting_value="<?php echo $sorting['net_total']['value'];?>"
	style="width: 8%;">Value</th>
	<th class="sorting_disabled" style="width: 8%;">Country</th>
	<th class="sorting_disabled" style="width: 12%;">Delivery Time</th>
	<th class="sorting_disabled" style="width: 8%;">Status</th>
	<th class="sorting_disabled" style="width: 8%;">Comments</th>
	<th class="sorting_disabled" style="width: 8%;">Actions</th>
</tr>