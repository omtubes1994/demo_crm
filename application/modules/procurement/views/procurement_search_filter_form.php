<div class="row kt-margin-b-20">
    <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
        <label>Made By:</label>
        <select class="form-control" name="procurement_person">
            <option value="">Select</option>
            <?php foreach($procurement_person as $single_procurement_person) {?>
                <option 
                value="<?php echo $single_procurement_person['name'];?>"
                <?php echo ($search_filter['procuremet_person_name'] == $single_procurement_person['name']) ? 'selected': ''; ?>
                >
                <?php echo trim(ucfirst(strtolower($single_procurement_person['name'])));?>
                </option>
            <?php } ?>     
        </select>
    </div>
    <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
        <label>Sales Person:</label>
        <select class="form-control" name="sales_person">
            <option value="">Select</option>
            <?php foreach($sales_person as $single_sales_person) {?>
                <option 
                value="<?php echo $single_sales_person['name'];?>"
                <?php echo ($search_filter['sales_person_name'] == $single_sales_person['name']) ? 'selected': ''; ?>
                >
                <?php echo trim(ucfirst(strtolower($single_sales_person['name'])));?>
                </option>
            <?php } ?>     
        </select>
    </div>
    <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
        <label>Vendor List:</label>
        <select class="form-control" name="vendor_name">
            <option value="">Select</option>
            <?php foreach($vendor_list as $single_vendor) {?>
                <option 
                value="<?php echo $single_vendor['vendor_name'];?>"
                <?php echo ($search_filter['vendor_name'] == $single_vendor['vendor_name']) ? 'selected': ''; ?>
                >
                <?php echo trim(ucfirst(strtolower($single_vendor['vendor_name'])));?>
                </option>
            <?php } ?>     
        </select>
    </div>
    <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
        <label>Status:</label>
        <select class="form-control" name="procurement_status">
            <option value="" <?php echo ($search_filter['procurement_status'] == '') ? 'selected': ''; ?>>Select</option>
            <option value="Issued" <?php echo ($search_filter['procurement_status'] == 'Issued') ? 'selected': ''; ?>>Issued</option>
            <option value="InProduction" <?php echo ($search_filter['procurement_status'] == 'InProduction') ? 'selected': ''; ?>>InProduction</option>
            <option value="Delivered" <?php echo ($search_filter['procurement_status'] == 'Delivered') ? 'selected': ''; ?>>Delivered</option>
            <option value="Inspected" <?php echo ($search_filter['procurement_status'] == 'Inspected') ? 'selected': ''; ?>>Inspected</option>
        </select>
    </div>
</div>
<div class="row kt-margin-b-20">
    <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
        <label>PO No:</label>
        <select class="form-control" name="procurement_no">
            <option value="">Select</option>
            <?php foreach($po_list as $single_po) {?>
                <option value="<?php echo $single_po['procurement_no'];?>" <?php echo ($search_filter['procurement_no'] == $single_po['procurement_no']) ? 'selected': ''; ?> >
                <?php echo $single_po['procurement_no'];?>
                </option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="kt-separator kt-separator--md kt-separator--dashed"></div>
<div class="row">
    <div class="col-lg-12">
        <button class="btn btn-primary btn-brand--icon procurement_search_filter_form_submit" type="reset">
            <span>
                <i class="la la-search"></i>
                <span>Search</span>
            </span>
        </button>
        &nbsp;&nbsp;
        <button class="btn btn-secondary btn-secondary--icon procurement_search_filter_form_reset" type="reset">
            <span>
                <i class="la la-close"></i>
                <span>Reset</span>
            </span>
        </button>
    </div>
</div>