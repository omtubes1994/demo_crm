<form id="update_vendor_for_rfq">
	<div class="row">
		<input type="text" name="rfq_id" value="<?php echo $rfq_to_vendor[0]['rfq_id'];?>" hidden>
		<div class="col-md-6 form-group">
			<label for="contact_date">Vendor</label>
			<select class="form-control" name="vendor_id">
				<?php 
				foreach($vendor_list as $single_vendor_list){
					if ($single_vendor_list['vendor_id'] == $rfq_to_vendor[0]['vendor_id']) {
						
						echo '<option value="'.$single_vendor_list['vendor_id'].'" selected>'.$single_vendor_list['vendor_name'].'</option>';
					}
				} ?>
			</select>
		</div>
		<div class="col-md-6 form-group">
			<label for="contact_date">Status</label>
			<select class="form-control" name="vendor_status">
				<option value="">Select Status</option>
				<option value="pending" <?php echo ($rfq_to_vendor[0]['vendor_status']=='pending')?'selected':'';?>>Pending</option>
				<option value="query"<?php echo ($rfq_to_vendor[0]['vendor_status']=='query')?'selected':'';?>>Query</option>
				<option value="regret"<?php echo ($rfq_to_vendor[0]['vendor_status']=='regret')?'selected':'';?>>Regret</option>
				<option value="done"<?php echo ($rfq_to_vendor[0]['vendor_status']=='done')?'selected':'';?>>Done</option>
			</select>
		</div>
		<div class="col-md-6 form-group">
			<label for="contact_date">Evaluate Offer Price</label>
			<select class="form-control" name="evaluate_price">
				<option value="">Select Price</option>
				<option value="h" <?php echo ($rfq_to_vendor[0]['evaluate_price']=='h')?'selected':'';?>>High</option>
				<option value="l" <?php echo ($rfq_to_vendor[0]['evaluate_price']=='l')?'selected':'';?>>Low</option>
			</select>
		</div>
		<div class="col-md-6 form-group">
			<label for="contact_date">Evaluate Offer Delivery</label>
			<select class="form-control" name="evaluate_delivery">
				<option value="">Select Delivery</option>
				<option value="h" <?php echo ($rfq_to_vendor[0]['evaluate_delivery']=='h')?'selected':'';?>>High</option>
				<option value="l" <?php echo ($rfq_to_vendor[0]['evaluate_delivery']=='l')?'selected':'';?>>Low</option>
			</select>
		</div>
	</div>
	<div class="clearfix"></div>	       		
</form>
<hr/>
<table class="table table-bordered">
	<thead>
	   	<tr>
	   		<th class="kt-font-dark kt-font-lg kt-font-bold" style="width: 5%;">
	   			#<span class="checkbox_checked_count_update" count="<?php echo $count;?>">(<?php echo $count;?>)</span>
	   			<hr>
		   		<label class="kt-checkbox kt-checkbox--bold kt-checkbox--dark">
					<input type="checkbox" name="quotation_id_update_all" value="">
					<span></span>
				</label>
	   		</th>
	   		<th class="kt-font-dark kt-font-lg kt-font-bold" style="width: 20%;">
	   			Product
	   			<hr>
	   			Material
	   		</th>
	   		<th class="kt-font-dark kt-font-lg kt-font-bold" style="width: 55%;">
	   			Description
	   			<hr>
	   			Details
	   		</th>
	    	<th class="kt-font-dark kt-font-lg kt-font-bold" style="width: 10%;">
	    		Quantity
	    		<hr>
	    		unit
	    	</th>
	   	</tr>
	   	<tr>
	   		<th class="kt-font-dark kt-font-lg kt-font-bold" style="width: 5%;"></th>
	   		<th class="kt-font-dark kt-font-lg kt-font-bold" style="width: 20%;"></th>
	   		<th class="kt-font-dark kt-font-lg kt-font-bold" style="width: 65%;"></th>
	    	<th class="kt-font-dark kt-font-lg kt-font-bold" style="width: 10%;"></th>
	   	</tr>
	</thead>
	<tbody>
		<?php 
			if(!empty($quotation_details)){ 
				foreach ($quotation_details as $quotation_details_key => $quotation_details_value) { $current_key= $quotation_details_key+1;
		?>
		<tr class="all_tr_update" style="background-color: <?php echo $quotation_details_value['tr_background'];?>;">
			<td class="kt-font-dark kt-font-xl kt-font-bold" style="width: 5%;">
				<label class="kt-checkbox kt-checkbox--bold kt-checkbox--dark">
					<input type="checkbox" name="quotation_id_update[]" value="<?php echo $quotation_details_value['id']; ?>" <?php echo ($quotation_details_value['already_assign'])?'checked':''; ?>> <?php echo $current_key; ?>
					<span></span>
				</label>
			</td>
			<td class="kt-font-dark kt-font-lg kt-font-bold kt-align-left">
				<span>
					<em><?php echo $quotation_details_value['product_name'];?></em>
					<hr>
					<em><?php echo $quotation_details_value['material_name'];?></em>
				</span>
			</td>
			<td class="kt-font-dark kt-font-lg kt-font-bold kt-align-center">
				<?php echo $quotation_details_value['description'];?>
			</td>
			<td class="kt-font-dark kt-font-lg kt-font-bold kt-align-center">
				<span>
					<em><?php echo $quotation_details_value['quantity'];?></em>
					<hr>
					<em><?php echo $quotation_details_value['units'];?></em>
				</span>
			</td>
		</tr>
		<?php  }
			}
		?>
	</tbody>
</table>