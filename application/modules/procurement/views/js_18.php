jQuery(document).ready(function() {

	$('a.change_rfq_pending_date_for_highchart').click(function(){
		ajax_call_function({call_type: 'rfq_pending_list', filter_date: $(this).attr('filter-date'), filter_sales_person: $('input#filter_sales_person_value_bar_graph').val()}, 'rfq_pending_list');
	});
	$('div.select_sales_person_bar_graph_dropdown').on('click', 'a.select_sales_person_bar_graph', function(){

		ajax_call_function({call_type: 'rfq_pending_list', filter_date: $('input#filter_date_value_bar_graph').val(), filter_sales_person: $(this).attr('sales-person')}, 'rfq_pending_list');
	});
	var method_name = '<?php echo $this->uri->segment("2");?>';
   	var third_params = '<?php echo $this->uri->segment("3",0);?>';

   	if(method_name == 'update_purchase_order' && third_params > 0) {
   		set_order_details();
   	}
   	KTAutosize.init();
	$('option.all').hide();
	$('select#vendor_id').on('change', function(){
		$('option.all').hide();
		$('option.'+$(this).val()).show();
		//set vendor name and country based on vendor id
		ajax_call_function({call_type: 'set_vendor_name_and_country', vendor_id: $(this).val()}, 'set_vendor_name_and_country');
	});	    
	$('tbody#product_list_body').on('input', 'input.purchase_order_quantity, input.purchase_order_unit', function(){
		var count_number = $(this).attr('count_number');
		add_net_total(count_number);
	});
	$('button.add_charges_in_purchase_order').click(function(){

		ajax_call_function({call_type: 'add_charges_in_purchase_order', add_charges_in_purchase_order_form: $('form#add_charges_in_purchase_order_form').serializeArray()}, 'add_charges_in_purchase_order');
	});
	$('a.add_gst_or_discount_modal').click(function(){
		add_data_to_charge_history();
	});
	$('i.edit_terms_and_conditions').click(function(){
		var alert_text = $.trim($('div.terms_conditions_alert').html());
		$('textarea#terms_conditions').html('').html(alert_text);
	});
	$('button.update_terms_conditions').click(function(){
		var updated_term_condition = $.trim($('textarea#terms_conditions').val());
		$('div.terms_conditions_alert').html('').html(updated_term_condition);
		$('div#edit_terms_and_conditions').modal('hide');
	});

	$('button.add_purchase_order').click(function(){

		var procurement_id = $(this).attr('procurement_id');
		var call_type = 'update_purchase_order';
		if(procurement_id == ''){

			call_type = 'add_purchase_order';
		}
		ajax_call_function({
							call_type: call_type,
							procurement_id: procurement_id,
							person_details: $('form#purchase_order_user_details_form').serializeArray(),
							product_details: $('form#purchase_order_quantity_form').serializeArray(),
							terms_conditon: $('form#purchase_order_terms_condition_form').serializeArray(),
							term_condition_text: $.trim($('div.terms_conditions_alert').html())
						}, 'add_purchase_order');
	});

	$('a.add_item_to_procurement_product').click(function(){

		var next_count_number = $(this).attr('next_count_number');
		ajax_call_function({call_type: 'get_product_body', next_count_number: next_count_number,product_name: $('select[name="product_name_1"]').val(),material_name:$('select[name="material_name_1"]').val()}, 'get_product_body');
		$(this).attr('next_count_number', ++next_count_number);
	});

	$('tbody#procurement_body').on('click', 'a.delete_procurement_list_data', function(){

		swal({
			  title: "Are you sure?",
			  text: "Once deleted, you will not be able to recover this Procurement Details!",
			  icon: "warning",
			  buttons:  ["Cancel", "Delete"],
			  dangerMode: true,	
			})
			.then((willDelete) => {
			  	if (willDelete) {
	        		ajax_call_function({call_type: 'delete_procurement_list_data', procurement_id: $(this).attr('procurement_id')}, 'delete_procurement_list_data');	
			    	swal({
			    		title: "Procurement details is deleted",
			      		icon: "success",
			    	});
		  		} else {
				    swal({
			    		title: "Procurement is not deleted",
			      		icon: "info",
			    	});
			  	}
			});
	});

	$('a.add_procuremet_search_filter_form').click(function(){

		$('div.procuremet_search_filter').show();
	});

	$('div#procurement_search_filter_form_div').on('click', 'button.procurement_search_filter_form_submit', function(){
		set_reset_spinner($('button.procurement_search_filter_form_submit'));
		search_filter();
	});

	$('thead#procurement_head').on('click', 'th.sorting_search', function(){
		ajax_call_function({
							call_type: 'sort_filter',
							procurement_sort_name: $(this).attr('sorting_name'),
							procurement_sort_value: $(this).attr('sorting_value'),
							search_form_data: $('form#procurement_search_filter_form').serializeArray()
						},
						'sort_filter');
	});

	$('div#procurement_paggination').on('click', 'li.procurement_paggination_number', function(){

		ajax_call_function({
							call_type: 'paggination_filter',
							limit: $(this).attr('limit'),
							offset: $(this).attr('offset'),
							search_form_data: $('form#procurement_search_filter_form').serializeArray()
						}, 'paggination_filter');
	});
	$('div#procurement_paggination').on('change', 'select#set_limit', function(){

		ajax_call_function({
							call_type: 'paggination_filter',
							limit: $('select#set_limit').val(),
							offset: 0,
							search_form_data: $('form#procurement_search_filter_form').serializeArray()
						}, 'paggination_filter');
	});
	$('tbody#product_list_body').on('click', 'a.delete_product_details', function(){

		$('tr.count_'+$(this).attr('count_no')).remove();
		ajax_call_function({call_type: 'delete_product_details', count_no: $(this).attr('count_no')}, 'delete_product_details');
	});
	$('table#charge_history_table').on('click', 'a.delete_charges_details', function(){

		swal({
		  title: "Are you sure?",
		  text: "Once deleted, you will not be able to recover this Charge Details!",
		  icon: "warning",
		  buttons:  ["Cancel", "Delete"],
		  dangerMode: true,	
		})
		.then((willDelete) => {
		  	if (willDelete) {
        		ajax_call_function({call_type: 'delete_charge', charge_id: $(this).attr('charge_id')}, 'delete_charge');
        		add_data_to_charge_history();                 
		    	swal({
		    		title: "Charge is deleted",
		      		icon: "success",
		    	});
	  		} else {
			    swal({
		    		title: "Charge is not deleted",
		      		icon: "info",
		    	});
		  	}
		});
	});
	$('div.rfq_status_dropdown').on('click', 'a.change_rfq_status', function(){
        rfq_filter_call('', $(this).attr('rfq-status'));
	});
	<?php if(in_array($this->session->userdata('role'), array(1, 6))) { ?>
	
		ajax_call_function({call_type: 'procurement_status_highchart',person_type: 'procurement_person'}, 'procurement_status_highchart');
	<?php }?>
	$('a.change_procurement_pending_for_follow_up_date_for_highchart').click(function(){

		ajax_call_function({call_type: 'procurement_status_highchart', person_type: $(this).attr('person-type')}, 'procurement_status_highchart');
	});
});

function search_filter() {
	ajax_call_function(
					{
						call_type: 'search_filter',
						search_form_data: $('form#procurement_search_filter_form').serializeArray()
					},
					'search_filter'
				);
}
function add_data_to_charge_history() {

	ajax_call_function({call_type: 'get_gst_or_discount_history'}, 'get_gst_or_discount_history');
}

function add_net_total(count_number) {
	var quantity = $('input[name="quantity_'+count_number+'"]').val();
	var price = $('input[name="price_'+count_number+'"]').val();
	var net_total = price * quantity;
	$('input[name="total_'+count_number+'"]').val(net_total);
	if(net_total > 0) {

		set_order_details(net_total, count_number);
	}
}
function set_order_details(net_total= "", count_number=0) {

	var data={call_type: 'set_order_detail'};
	if(net_total != '') {
		data.net_total = net_total;
	}
	if(count_number != 0) {
		data.count_number = count_number;
	}
	ajax_call_function(data, 'net_total_gross_total_body');
}
// Class definition

var KTAutosize = function () {
    
    // Private functions
    var demos = function () {
        // basic demo
        var demo1 = $('#terms_conditions');
        autosize(demo1);
        $('.vendor_name_select_picker').selectpicker();
    }
    var rfq_list_date_range_picker_Init = function() {

        if ($('#rfq_list_date_range_picker').length == 0) {
            return;
        }
        var picker = $('#rfq_list_date_range_picker');
        var start = moment().startOf('month');
        var end = moment().endOf('month');

        function cb(start, end, label) {
            var title = '';
            var range = '';

            if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D');
            } else {
                range = start.format('MMM D') + ' - ' + end.format('MMM D');
            }

            $('#rfq_list_date_range_picker_date').html(range);
            $('#rfq_list_date_range_picker_title').html(title);
            rfq_filter_call();
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'This Week': [moment().startOf('week'), moment().endOf('week')],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end, 'This Month');
    }
    var daterangepickerInit = function() {
        if ($('#quotation_made_by_daterangepicker').length == 0) {
            return;
        }

        var picker = $('#quotation_made_by_daterangepicker');
        var start = moment().startOf('month');
        var end = moment().endOf('month');

        function cb(start, end, label) {
            var title = '';
            var range = '';
            if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D');
            } else {
                range = start.format('MMM D') + ' - ' + end.format('MMM D');
            }

            $('#quotation_made_by_daterangepicker_date').html(range);
            $('#quotation_made_by_daterangepicker_title').html(title);
            ajax_call_function({call_type: 'quotation_made_by_high_chart', filter_date: range}, 'quotation_made_by_high_chart');
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end, 'This Month');
    }
    return {
        // public functions
        init: function() {
            demos(); 
            rfq_list_date_range_picker_Init();
            daterangepickerInit();
        }
    };
}();

function rfq_filter_call(sales_person = $('#sales_person_name').val(), rfq_status = $('#rfq_status_name').val(), date_range = $('#rfq_list_date_range_picker_date').html()) {

	ajax_call_function({call_type: 'rfq_pending_list', filter_sales_person: sales_person, filter_rfq_status: rfq_status, filter_date: date_range}, 'rfq_pending_list');
}

function ajax_call_function(data, callType, url = "<?php echo base_url('procurement/ajax_function'); ?>") {

	$.ajax({
		type: 'POST',
		data: data,
		url: url,
		dataType: 'JSON',
		success: function(res){
			if(res.status == 'successful') {
				
				if(callType == 'rfq_pending_list') {
					if(data.filter_date != ''){
						swal({
				    		title: "RFQ Pending List Updated ",
				      		icon: "success",
				    	});
					}
			    	rfq_pending_list_highchart(res.highchart_data);
					$('div.rfq_sales_person').html('').html(res.html_body.filter_sales_person);
					$('input#sales_person_name').val(res.html_body.filter_sales_person_id);
					$('span#span_sales_person_name').html('').html(res.html_body.filter_sales_person_name);

					$('div.rfq_status_dropdown').html('').html(res.html_body.filter_rfq_status);
					$('input#rfq_status_name').val(res.html_body.filter_rfq_status_name);
					$('span#span_rfq_status_name').html('').html(res.html_body.filter_rfq_status_name);

				} else if(callType == 'add_charges_in_purchase_order') {
					swal({
			    		title: "Charges are added",
			      		icon: "success",
			    	});
			    	add_data_to_charge_history();
			    	set_order_details();
				} else if(callType == 'get_gst_or_discount_history') {
					$('table#charge_history_table').html('').html(res.charge_history_body);
				} else if(callType == 'net_total_gross_total_body') {
					$('table#net_total_gross_total_body tbody').html('').html(res.net_total_gross_total_body);
				} else if(callType == 'add_purchase_order') {
					swal({
			    		title: res.message,
			      		icon: "success",
			    	});
				} else if(callType == 'set_vendor_name_and_country') {

					if(res.message == 'record found') {

						$('input[name="vendor_name"]').val(res.vendor_details.vendor_name);
						$('input[name="vendor_country"]').val(res.vendor_details.country);
					}
				} else if(callType == 'get_product_body') {
					
					$('tbody#product_list_body').append(res.product_list_body);
				} else if(callType == 'search_filter' || callType == 'sort_filter' || callType == 'paggination_filter') {
					$('tbody#procurement_body').html('').html(res.procurement_body);
					$('div#procurement_search_filter_form_div').html('').html(res.procurement_search_filter);
					$('thead#procurement_head').html('').html(res.procurement_sorting);
					$('div#procurement_paggination').html('').html(res.procurement_paggination);
				} else if(callType == 'quotation_made_by_high_chart') {
					quotation_made_by_high_chart(res.quotation_made_by_highchart);

				} else if(callType == 'procurement_status_highchart') {
					procurement_status_highchart(res.procurement_stage_highchart_category, res.procurement_stage_highchart_data);
				}

			}
		},
		beforeSend: function(response){
			
		}
	});
};

function set_reset_spinner(obj, set_unset_flag = true) {

	if(set_unset_flag) {

		$(obj).addClass('kt-spinner');
    	$(obj).addClass('kt-spinner--right');
    	$(obj).addClass('kt-spinner--sm');
    	$(obj).addClass('kt-spinner--light');	
	} else{

		$(obj).removeClass('kt-spinner');
    	$(obj).removeClass('kt-spinner--right');
    	$(obj).removeClass('kt-spinner--sm');
    	$(obj).removeClass('kt-spinner--light');
	}
}

// create user wise rfq pending highchart
function rfq_pending_list_highchart(highchart_data) {

	Highcharts.chart('rfq_pending_list_highchart', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: ''
	    },
	    subtitle: {
	        text: ''
	    },
	    accessibility: {
	        announceNewData: {
	            enabled: true
	        }
	    },
	    xAxis: {
	        type: 'category'
	    },
	    yAxis: {
	        title: {
	            text: 'Total Number of RFQ Pending'
	        }

	    },
	    legend: {
	        enabled: false
	    },
	    plotOptions: {
	        series: {
	            borderWidth: 0,
	            dataLabels: {
	                enabled: true,
	                format: '{point.y:f}'
	            }
	        }
	    },

	    tooltip: {
	        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> RFQ Pending<br/>'
	    },

	    series: [
	        {
	            name: "OM Tubes",
	            colorByPoint: true,
	            data: highchart_data
	        }
	    ]

	});
}

function quotation_made_by_high_chart(highchart_data) {

	Highcharts.chart('quotation_made_by_high_chart', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: ''
	    },
	    subtitle: {
	        text: ''
	    },
	    accessibility: {
	        announceNewData: {
	            enabled: true
	        }
	    },
	    xAxis: {
	        type: 'category'
	    },
	    yAxis: {
	        title: {
	            text: 'Sales done in INR'
	        }

	    },
	    legend: {
	        enabled: false
	    },
	    plotOptions: {
	        series: {
	            borderWidth: 0,
	            dataLabels: {
	                enabled: true,
	                format: '{point.y:f}'
	            }
	        }
	    },

	    tooltip: {
	        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> INR<br/>'
	    },

	    series: [
	        {
	            name: "OM Tubes",
	            colorByPoint: true,
	            data: highchart_data
	        }
	    ]

	});
}

function procurement_status_highchart(category, highchart_data) {

	Highcharts.chart('procurement_status_highchart', {
		// Create the chart
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: ''
	    },
	    xAxis: {
	        // categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
	        categories: category
	    },
	    yAxis: {
	        min: 0,

	        title: {
	            text: 'Total Quotation'
	        },
	    
	        stackLabels: {
	            enabled: true,
	            style: {
	                fontWeight: 'bold',
	                color: ( // theme
	                    Highcharts.defaultOptions.title.style &&
	                    Highcharts.defaultOptions.title.style.color
	                ) || 'gray'
	            }
	        }
	    },
	    legend: {
	        align: 'right',
	        x: -30,
	        verticalAlign: 'top',
	        y: 25,
	        floating: true,
	        backgroundColor:
	            Highcharts.defaultOptions.legend.backgroundColor || 'white',
	        borderColor: '#CCC',
	        borderWidth: 1,
	        shadow: false
	    },
	    tooltip: {
	        headerFormat: '<b>{point.x}</b><br/>',
	        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
	    },
	    plotOptions: {
	        column: {
	            stacking: 'normal',
	            dataLabels: {
	                enabled: true
	            }

	        }
	    },
	    series: highchart_data
	});
}