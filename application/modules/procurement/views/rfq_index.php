<style type="text/css">
	.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
		padding-top: 5% !important;
	}
	.layer-white{
	    display: none;
	    position: fixed;
	    top: 0em !important;
	    left: 0em !important;
	    width: 100%;
	    height: 100%;
	    text-align: center;
	    vertical-align: middle;
	    background-color: rgba(255, 255, 255, 0.55);
	    opacity: 1;
	    line-height: 1;
	    -webkit-animation-fill-mode: both;
	    animation-fill-mode: both;
	    -webkit-animation-duration: 0.5s;
	    animation-duration: 0.5s;
	    -webkit-transition: background-color 0.5s linear;
	    transition: background-color 0.5s linear;
	    -webkit-user-select: none;
	    -moz-user-select: none;
	    -ms-user-select: none;
	    user-select: none;
	    will-change: opacity;
	    z-index: 9;
	}
	.div-loader{
	    position: absolute;
		top: 50%;
		left: 50%;
		margin: 0px;
		text-align: center;
		z-index: 1000;
		-webkit-transform: translateX(-50%) translateY(-50%);
		transform: translateX(-50%) translateY(-50%);
	}
	.kt-spinner:before {
	    width: 50px;
	    height: 50px;
	    margin-top: -10px;
	}

	.kt-badge.kt-badge--orange {
		color: #111111;
    	background: #fd7e14;
	}
	.kt-badge.kt-badge--unified-orange {
		color: #fd7e14;
		background: #FFDAB9;
	}
	.kt-badge.kt-badge--yellow {
		color: #111111;
		background: #ffc107;
	}
	td.same_rfq_background_class{
		background-color: #f7f69c;
	}
</style>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					RFQ List
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
		        <div class="kt-portlet__head-wrapper">
		            <div class="kt-portlet__head-actions">
		                <a href="javascript:;" class="btn btn-brand btn-elevate btn-icon-sm rfq_search_filter_button" button_value = "show">
		                    Add Search Filter
		                </a>
		            </div>
		        </div>
	    	</div>
		</div>
		<div class="kt-portlet__body" style="display: none;" id="rfq_search_filter_div">
			<?php $this->load->view('procurement/rfq_list_search_filter', 
				array(
					'search_filter_form_data' =>
					array(
							'rfq_no'=> '',
							'rfq_company'=> '',
							'rfq_date'=> '',
							'quotation_no'=> '',
							'rfq_subject'=> '',
							'priority'=>'',
							'rfq_importance'=> array(),
							'sales_person'=> array(),
							'procurement_person'=> array(),
							'procurement_person_status'=> array(),
							'rfq_status'=> array(
												array(
													'value'=> 'waiting',
													'name'=> 'waiting',
													'selected'=> 'selected'
												),
												array(
													'value'=> 'pending',
													'name'=> 'Pending',
													'selected'=> 'selected'
												),
												array(
													'value'=> 'query',
													'name'=> 'Query',
													'selected'=> 'selected'
												)
											),
							'quotation_status'=> array(),
							'product_family'=> array(),
					)
				)
			);?>
		</div>
	</div>




	<?php if(in_array(16,$this->session->userdata('graph_data_access'))) { ?>
		<!--Begin::Section-->
		<div class="row">
			<div class="col-xl-8">
				<!--Begin::Portlet-->
				<div class="kt-portlet kt-portlet--height-fluid">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">
								RFQ Analytics
								<input type="text" id="sales_person_name" value="" hidden>
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<button type="button" class="btn btn-brand btn-elevate btn-icon-sm procurement_to_sales_switch_button" graph_button="sales">Sales Person Graph</button>
						</div>
					</div>
					<div class="kt-portlet__body">
						<!--Begin::Timeline 3 -->	
						<div class="kt-scroll" data-scroll="true" style="height: 400px">
							<div id="rfq_list_highchart"></div>
						</div>

						<!--End::Timeline 3 -->
					</div>
				</div>

				<!--End::Portlet-->
			</div>
			<div class="col-xl-4">
				<!--Begin::Portlet-->
				<div class="kt-portlet kt-portlet--height-fluid">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title product_family_count"></h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							
						</div>
					</div>
					<div class="kt-portlet__body">
						<!--Begin::Timeline 3 -->	
						<div class="kt-scroll" data-scroll="true" style="height: 400px">
							<div id="rfq_list_piechart"></div>
						</div>

						<!--End::Timeline 3 -->
					</div>
				</div>

				<!--End::Portlet-->
			</div>
		</div>
		<!--End::Section-->
	<?php }?>
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__body">
			<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
				<div class="row">
					<div class="col-sm-12">
						<input type="text" id="column_name" value="" hidden>
						<input type="text" id="column_value" value="" hidden>
						<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 100%; min-height: 400px;">
							<thead class="kt-hidden" id="rfq_list_header">
	      						<tr>
									<th class="sorting_disabled kt-align-center" style="width: 5%;">Sr.No</th>
									<th 
										class="sorting_asc sorting_search kt-align-center" 
										sorting_name="invoice_mst.invoice_no"
										sorting_value=""
										style="width: 10%;">
										Rfq No #
									</th>
									<th 
										class="sorting_search kt-align-left row" 
										sorting_name=""
										sorting_value=""
										style="padding-left: 20px;">
										Rfq Details
									</th>
									<th 
										class="sorting_search kt-align-center"
										sorting_name=""
										sorting_value=""
										style="width: 10%;">
										Sales Person
									</th>
									<th 
										class="sorting_search kt-align-center"
										sorting_name=""
										sorting_value=""
										style="width: 10%;">
										Procurement Person
									</th>    
									<th 
										class="sorting_search kt-align-center"
										sorting_name=""
										sorting_value=""
										style="width: 10%;">
										Rfq Status
									</th>
									<th 
										class="sorting_search kt-align-center"
										sorting_name=""
										sorting_value=""
										style="width: 10%;">
										Quotation #
									</th>
									<th class="sorting_disabled kt-align-center" style="width: 15%;">Actions</th>
								</tr>
							</thead>
							<thead id="rfq_list_header">
	      						<tr>
									<th class="sorting_disabled kt-align-center" style="width: 3%;">Sr.No</th>
									<th 
										class="sorting_disabled kt-align-center" 
										sorting_name=""
										sorting_value=""
										style="width: 27%;">
										Company Details
									</th>
									<th class="sorting_disabled kt-align-center" style="width: 25%;">Quotation Details</th>
									<th class="sorting_disabled kt-align-center" style="width: 15%;">Rfq Details</th>
									<th class="sorting_disabled kt-align-center" style="width: 20%;">Comment</th>
									<th class="sorting_disabled kt-align-center" style="width: 10%;">Actions</th>
								</tr>
							</thead>
							<tbody id="rfq_list_body"></tbody>
						</table>
					</div>
				</div>
				<div class="row" id="rfq_list_paggination">
					
				</div>
			</div>
		</div>
	</div>
	<div id="rfq_table_loader" class="layer-white">
		<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
	</div>
</div>


<!-- end:: Content -->
<div class="modal fade" id="query-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Queries</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<form id="query_response">
					<div class="row">
               			<div class="col-md-6 form-group">
               				<label for="notes">Reply Query</label>
               				<textarea id="notes" name="notes" class="form-control validate[maxSize[100],required]"></textarea>
               			</div>
               			<div class="col-md-6 align-self-center">
               				<input type="hidden" name="rfq_id" id="rfq_id" value="">
               				<button class="btn btn-success" type="submit">Submit</button>
               			</div>
               		</div>
				</form>
               	<hr/>
               	<h4>Queries History</h4>
				<div id="tab_history">
		        	<table class="table table-bordered" id="query_table">
		        		<tbody>
		        		<?php 
		        			if(!empty($rfq_notes)) {
			        			foreach ($rfq_notes as $key => $value) {
			        				echo '<tr connect_id = "'.$value['connect_id'].'" type="'.$value['type'].'">
			        					<td>'.$value['note'].'</td>
			        					<td>'.date('d M', strtotime($value['entered_on'])).'</td>
			        				</tr>';
			        			}
		        			}
		        		?>
		        		</tbody>
		        	</table>
		        </div>
			</div>
		</div>
	</div>
</div>
<!-- end query_model -->


<div class="modal fade" id="notes-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Queries</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<h4>Notes History</h4>
				<div id="tab_history">
		        	<table class="table table-bordered" id="notes_table">
		        		<tbody></tbody>
		        	</table>
		        </div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="query-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Query</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
                <form action="<?php echo site_url('quotations/addQuery'); ?>" method="post" id="query_form" name="query_form">
               		<div class="row">
               			<div class="col-md-6">
               				<label for="query_text">Query Details</label>
               				<textarea id="query_text" name="query_text" class="form-control validate[required]"></textarea>
               			</div>
               		</div>
               		<div class="clearfix"></div>
               		<div class="row">
               			<div class="col-md-6 align-self-center">
               				<input type="hidden" id="quote_id" name="quote_id">
               				<input type="hidden" id="query_id" name="query_id">
               				<input type="hidden" name="query_type" value="sales">
               				<button class="btn btn-success" type="submit">Reply Query</button>
               			</div>
               		</div>
               	</form>
               	<hr/>
               	<h4>Query History</h4>
                <div id="tab_history"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="pquery-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Query</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
                <form action="<?php echo site_url('quotations/addQuery'); ?>" method="post" id="pquery_form" name="pquery_form">
               		<div class="row">
               			<div class="col-md-6">
               				<label for="query_text">Query Details</label>
               				<textarea id="query_text" name="query_text" class="form-control validate[required]"></textarea>
               			</div>
               			<div class="col-md-6" id="close_query" style="display: none;">
               				<label for="close_query">Close Query</label>
               				<select class="form-control" name="query_status" id="query_status">
               					<option value="open">No</option>
               					<option value="closed">Yes</option>
               				</select>
               			</div>
               		</div>
               		<div class="clearfix"></div>
               		<div class="row">
               			<div class="col-md-6 align-self-center">
               				<input type="hidden" id="quote_id" name="quote_id">
               				<input type="hidden" id="query_id" name="query_id">
               				<input type="hidden" name="query_type" value="purchase">
               				<button class="btn btn-success" type="submit">Add / Reply Query</button>
               			</div>
               		</div>
               	</form>
               	<hr/>
               	<h4>Query History</h4>
                <div id="tab_history"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
			</div>
		</div>
	</div>
</div>
<!--begin::Modal-->
<div class="modal fade" id="update_rating_rfq" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Update Rating</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form id="rfq_priority_form">
					<div class="row">
						<div class="col-md-12 form-group row">
							<label class="col-form-label col-lg-4 col-sm-12">RFQ Priority</label>
							<div class="kt-font-info col-lg-6 col-md-9 col-sm-12" id="modal_body_update_rating"></div>
						</div>	
						<div class="col-md-12 form-group row">
							<label class="col-form-label col-lg-4 col-sm-12">Priority Reason</label>
							<div class="kt-font-info col-lg-6 col-md-9 col-sm-12" id="modal_body_update_rating_reason"></div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary save_rating_rfq">Save changes</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="stats-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-body" style="background: aliceblue;">
				<div class="row">
					<div class="col-12">
						<h2 class="kt-font-info graph_company_name">h2. Heading 2</h2>
					</div>
					<!-- QUOTATION VS PROFORMA Yearly -->
					<div class="col-6">
						<div class="kt-portlet kt-portlet--height-fluid">
							<div class="kt-portlet__head">
								<div class="kt-portlet__head-label">
									<h3 class="kt-portlet__head-title quotation_vs_proforma_yearly_title"></h3>
								</div>
							</div>
							<div class="kt-portlet__body quotation_vs_proforma_yearly_body" style="display:none;">
								<div class="kt-scroll" data-scroll="true" style="height: 400px">
									<figure class="highcharts-figure">
										<div id="quotation_vs_proforma_highchart_yearly"></div>
									</figure>
								</div>
							</div>
						</div>
					</div>
					<!-- QUOTATION VS PROFORMA Monthly -->
					<div class="col-6">
						<div class="kt-portlet kt-portlet--height-fluid">
							<div class="kt-portlet__head">
								<div class="kt-portlet__head-label">
									<h3 class="kt-portlet__head-title quotation_vs_proforma_title"></h3>
								</div>
							</div>
							<div class="kt-portlet__body quotation_vs_proforma_body" style="display:none;">
								<div class="kt-scroll" data-scroll="true" style="height: 400px">
									<figure class="highcharts-figure">
										<div id="quotation_vs_proforma_highchart"></div>
									</figure>
								</div>
							</div>
						</div>
					</div>
					<!-- EXPORT STATS YEARLY -->
					<div class="col-6">
						<div class="kt-portlet kt-portlet--height-fluid">
							<div class="kt-portlet__head">
								<div class="kt-portlet__head-label">
									<h3 class="kt-portlet__head-title export_stats_yearly_title"></h3>
								</div>
							</div>
							<div class="kt-portlet__body export_stats_yearly_body" style="display:none;">
								<div class="kt-scroll" data-scroll="true" style="height: 400px">
									<figure class="highcharts-figure">
										<div id="export_stats_yearly_container"></div>
									</figure>
								</div>
							</div>
						</div>
					</div>
					<!-- EXPORT STATS YEARLY INTERNAL -->
					<div class="col-6">
						<div class="kt-portlet kt-portlet--height-fluid">
							<div class="kt-portlet__head">
								<div class="kt-portlet__head-label">
									<h3 class="kt-portlet__head-title export_stats_yearly_internal_title"></h3>
								</div>
							</div>
							<div class="kt-portlet__body export_stats_yearly_internal_body" style="display:none;">
								<div class="kt-scroll" data-scroll="true" style="height: 400px">
									<figure class="highcharts-figure">
										<div id="export_stats_yearly_internal_container"></div>
									</figure>
								</div>
							</div>
						</div>
					</div>
					<!-- EXPORTER DATA -->
					<div class="col-6 exporter_stats_div" style="display:none;">
						<div class="kt-portlet kt-portlet--height-fluid">
							<div class="kt-portlet__head">
								<div class="kt-portlet__head-label">
									<h3 class="kt-portlet__head-title exporter_stats_title"></h3>
								</div>
							</div>
							<div class="kt-portlet__body">
								<div class="kt-scroll" data-scroll="true" style="height: 400px">
									<figure class="highcharts-figure">
										<div id="exporter_stats_container"></div>
									</figure>
								</div>
							</div>
						</div>
					</div>
					<!-- Production Status Wise Graph -->
					<div class="col-6 production_status_wise_div">
						<div class="kt-portlet kt-portlet--height-fluid">
							<div class="kt-portlet__head">
								<div class="kt-portlet__head-label">
									<h3 class="kt-portlet__head-title production_status_wise_title"></h3>
								</div>
							</div>
							<div class="kt-portlet__body">
								<div class="kt-scroll" data-scroll="true" style="height: 400px">
									<figure class="highcharts-figure">
										<div id="production_status_wise"></div>
									</figure>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="stats-modals" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="row">
					<div class="col-12">
						<h2 class="kt-font-info graph_company_name">h2. Heading 2</h2>
					</div>
					<div class="col-6">
						<figure class="highcharts-figure">
							<div id="quotation_vs_proforma_highchart_yearly"></div>
						</figure>
					</div>
					<div class="col-6">
						<figure class="highcharts-figure">
							<div id="quotation_vs_proforma_highchart"></div>
						</figure>
					</div>




					<div class="col-6">
						<figure class="highcharts-figure">
							<div id="production_status_wise"></div>
						</figure>
					</div>
					<div class="col-6">
						<figure class="highcharts-figure">
							<div id="export-stats-yearly-container_internal"></div>
						</figure>
					</div>
					<div class="col-6">
						<figure class="highcharts-figure">
							<div id="export-stats-yearly-container"></div>
						</figure>
					</div>
					<div class="col-6">
						<figure class="highcharts-figure">
							<div id="import-stats-container"></div>
						</figure>
					</div>
					<!-- EXPORTER DATA -->
					<div class="col-6 exporter_stats_div" style="display:none;">
						<div class="kt-portlet kt-portlet--height-fluid">
							<div class="kt-portlet__head">
								<div class="kt-portlet__head-label">
									<h3 class="kt-portlet__head-title exporter_stats_title"></h3>
								</div>
							</div>
							<div class="kt-portlet__body">
								<div class="kt-scroll" data-scroll="true" style="height: 400px">
									<figure class="highcharts-figure">
										<div id="exporter_stats_container"></div>
									</figure>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="rfq_file_upload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Upload RFQ And Documents:</h5>
			</div>
			<div class="modal-body row">
				<?php if(!in_array($this->session->userdata('user_id'), array(234, 288))){ ?>
				<div class="form-group form-group-last col-lg-5 row rfq_file_upload_div" style="padding: 10px 25px;">
					<label class="col-lg-3 col-form-label kt-font-bolder kt-font-lg">RFQ </label>
					<div class="col-lg-9">
						<div class="dropzone dropzone-multi" id="kt_dropzone_rfq_file_upload">
							<div class="dropzone-panel">
								<a class="dropzone-select btn btn-label-brand btn-bold btn-sm">Attach files</a>
								<a class="dropzone-upload btn btn-label-brand btn-bold btn-sm">Upload All</a>
								<a class="dropzone-remove-all btn btn-label-brand btn-bold btn-sm">Remove All</a>
							</div>
							<div class="dropzone-items">
								<div class="dropzone-item" style="display:none">
									<div class="dropzone-file">
										<div class="dropzone-filename" title="some_image_file_name.jpg"><span data-dz-name>some_image_file_name.jpg</span> <strong>(<span  data-dz-size>340kb</span>)</strong></div>
										<div class="dropzone-error" data-dz-errormessage></div>
									</div>
									<div class="dropzone-progress">
										<div class="progress">
											<div class="progress-bar kt-bg-brand" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress></div>
										</div>
									</div>
									<div class="dropzone-toolbar">
										<span class="dropzone-start"><i class="flaticon2-arrow"></i></span>
										<span class="dropzone-cancel" data-dz-remove style="display: none;"><i class="flaticon2-cross"></i></span>
										<span class="dropzone-delete" data-dz-remove><i class="flaticon2-cross"></i></span>
									</div>
								</div>
							</div>
						</div>
						<span class="form-text text-muted kt-font-bold">Max file size is 1MB and max number of files is 5.</span>
					</div>
				</div>
				<div class="form-group form-group-last col-lg-2 row" style="padding: 10px 25px;"></div>
				<?php } ?>
				<div class="form-group form-group-last col-lg-5 row rfq_document_upload_div" style="padding: 10px 25px;">
					<label class="col-lg-3 col-form-label kt-font-bolder kt-font-lg"> Technical Document</label>
					<div class="col-lg-9">
						<div class="dropzone dropzone-multi" id="kt_dropzone_rfq_document_upload">
							<div class="dropzone-panel">
								<a class="dropzone-select btn btn-label-brand btn-bold btn-sm">Attach files</a>
								<a class="dropzone-upload btn btn-label-brand btn-bold btn-sm">Upload All</a>
								<a class="dropzone-remove-all btn btn-label-brand btn-bold btn-sm">Remove All</a>
							</div>
							<div class="dropzone-items">
								<div class="dropzone-item" style="display:none">
									<div class="dropzone-file">
										<div class="dropzone-filename" title="some_image_file_name.jpg"><span data-dz-name>some_image_file_name.jpg</span> <strong>(<span  data-dz-size>340kb</span>)</strong></div>
										<div class="dropzone-error" data-dz-errormessage></div>
									</div>
									<div class="dropzone-progress">
										<div class="progress">
											<div class="progress-bar kt-bg-brand" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress></div>
										</div>
									</div>
									<div class="dropzone-toolbar">
										<span class="dropzone-start"><i class="flaticon2-arrow"></i></span>
										<span class="dropzone-cancel" data-dz-remove style="display: none;"><i class="flaticon2-cross"></i></span>
										<span class="dropzone-delete" data-dz-remove><i class="flaticon2-cross"></i></span>
									</div>
								</div>
							</div>
						</div>
						<span class="form-text text-muted kt-font-bold">Max file size is 1MB and max number of files is 5.</span>
					</div>
				</div>

				<div class="col-lg-6" id="rfq_file_history_list" style="padding: 10px 20px 10px 20px;"></div>
				<div class="col-lg-6" id="rfq_document_history_list" style="padding: 10px 20px 10px 0px;"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger rfq_file_upload_close" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!--end::Modal-->

<!--Begin:: Chat-->
<div class="modal fade- modal-sticky-bottom-right" id="kt_chat_modal" role="dialog" data-backdrop="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="kt-chat">
				<div class="kt-portlet kt-portlet--last">
					<div class="kt-portlet__head">
						<div class="kt-chat__head ">
							<div class="kt-chat__left">
								<div class="kt-chat__label">
									<a href="#" class="kt-chat__title">
										<em class= "header_1"></em>
									</a>
									<span class="kt-chat__status header_2"></span>
								</div>
							</div>
							<div class="kt-chat__right"></div>
						</div>
					</div>
					<div class="kt-portlet__body">
						<div class="kt-scroll kt-scroll--pull chat_scroll" data-height="410" data-mobile-height="225">
							<div class="layer-white rfq_chat_loader">
								<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
							</div>
							<div class="kt-chat__messages kt-chat__messages--solid div_chat_history"></div>
						</div>
					</div>
					<div class="kt-portlet__foot">
						<form id="rfq_query_add_form"  style="display:none;">
							<div class="kt-chat__input">
								<div class="kt-chat__editor">
									<input type="text" class="form-control" name="query_type" value="rfq_query" hidden />
									<input type="text" class="form-control" name="query_reference" value="" hidden />
									<input type="text" class="form-control" name="query_reference_id" value="" hidden />
									<textarea class="kt-font-dark" style="height: 50px" placeholder="Type here..." name="query_text"></textarea>
									<input type="text" class="form-control" name="query_creator_id" value="" hidden />
									<input type="text" class="form-control" name="query_assigned_id" value="" hidden />
									<input type="text" class="form-control" name="query_status" value="open" hidden />
								</div>
								<div class="kt-chat__toolbar">
									<div class="kt_chat__tools">
										<div class="query_type"></div>
									</div>
									<div class="kt_chat__actions">
										<button type="button" class="btn btn-brand btn-md btn-font-sm btn-upper btn-bold add_rfq_query">reply</button>
										<button type="button" class="btn btn-brand btn-md btn-font-sm btn-upper btn-bold" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</form>
						<form  id="rfq_comment_add_form" style="display:none;">
							<div class="kt-chat__input">
								<div class="kt-chat__editor">
									<textarea class="kt-font-dark" id = "textarea_id_for_chat_message" placeholder="Type here..." style="height: 50px"></textarea>
								</div>
								<div class="kt-chat__toolbar">
									<div class="kt_chat__tools">
										<div class="query_type"></div>
									</div>
									<div class="kt_chat__actions">
										<button type="button" class="btn btn-brand btn-md btn-font-sm btn-upper btn-bold save_chat_reply" rfq_id_for_chat = 0>reply</button>
										<button type="button" class="btn btn-brand btn-md btn-font-sm btn-upper btn-bold" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--ENd:: Chat-->