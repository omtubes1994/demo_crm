<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					Invoice Leads List
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
        <div class="kt-portlet__head-wrapper">
            <div class="kt-portlet__head-actions">
                <a href="javascript:;" class="btn btn-brand btn-elevate btn-icon-sm add_procuremet_search_filter_form">
                    Add Search Filter
                </a>
                &nbsp;
                <a href="<?php echo base_url('procurement/update_purchase_order');?>" class="btn btn-brand btn-elevate btn-icon-sm">
                    <i class="la la-plus"></i>
                    New Record
                </a>
            </div>
        </div>
    	</div>
		</div>
		<div class="kt-portlet__body procuremet_search_filter" style="display: none;">

      <!--begin: Search Form -->
      <form class="kt-form kt-form--fit kt-margin-b-20" id="procurement_search_filter_form">
        <div id="procurement_search_filter_form_div">
          <?php $this->load->view('procurement/procurement_search_filter_form');?>
        </div>
      </form>
	  </div>
		<div class="kt-portlet__body">

			<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
				<div class="row">
					<div class="col-sm-12">
						<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 1536px;">
							<thead id="procurement_head">
          			<?php $this->load->view('procurement/procurement_head');?>
							</thead>
							<tbody id="procurement_body">
          			<?php $this->load->view('procurement/procurement_body');?>
							</tbody>
						</table>
						<div id="kt_table_1_processing" class="dataTables_processing card" style="display: none; top: 10% !important;">Processing...</div>
					</div>
				</div>
				<div class="row" id="procurement_paggination">
          <?php $this->load->view('procurement/procurement_paggination');?>
				</div>
			</div>
		</div>

	</div>

</div>

<!-- end:: Content -->