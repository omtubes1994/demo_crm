<?php 
	if(!empty($procurement_list)) {
		foreach ($procurement_list as $procurement_list_key => $procurement_list_value) {
	?>			
	<tr role="row" class="<?php echo (($procurement_list_key+1)/2 == 00)?'even':'odd'?>">
		
		<td><?php echo $procurement_list_key+1; ?></td>
		<td><?php echo $procurement_list_value['procurement_no']; ?></td>
		<td><?php echo $procurement_list_value['procurement_person']; ?></td>
		<td><?php echo $procurement_list_value['sales_person']; ?></td>
		<td><?php echo date('F, j Y', strtotime($procurement_list_value['add_time'])); ?></td>
		<td><?php echo $procurement_list_value['vendor_name']; ?></td>
		<td><?php echo $procurement_list_value['gross_total']; ?></td>
		<td><?php echo $country_list[$procurement_list_value['vendor_country']]; ?></td>
		<td><?php echo $procurement_list_value['delivery_time']; ?></td>
		<td><?php echo $procurement_list_value['procurement_status']; ?></td>
		<td><?php echo $procurement_list_value['comments']; ?></td>
		<td>
			<a href="<?php echo base_url('procurement/update_purchase_order/'.$procurement_list_value['id']);?>" target="_blank" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
				<i class="la la-edit"></i>
			</a>
			<a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_procurement_list_data" procurement_id="<?php echo $procurement_list_value['id']; ?>" title="Delete">
				<i class="la la-trash"></i>
			</a>
		</td>
	</tr>
<?php }	} ?>