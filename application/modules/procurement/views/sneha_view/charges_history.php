<thead>
   <tr>
   	<th>Charge Name</th>
   	<th>Charge Added Or Deducted</th>
   	<th>Charge Value</th>
   	<th>Net Total: <?php echo $this->session->userdata('order_details')['net_total']; ?></th>
      <th>Action</th>
   </tr>
</thead>
<tbody>
   <?php if(!empty($this->session->userdata('order_details')['different_charges'])) {?>
      <?php foreach($this->session->userdata('order_details')['different_charges'] as $charges_key => $charges_details){?>
         <tr>
         	<td><?php echo $charges_details['charges_name'];?></td>
         	<td><?php echo $charges_details['add_minus_charges_value'];?></td>
         	<td><?php echo $charges_details['charge_value'];?></td>
         	<td><?php echo $charges_details['gross_total'];?></td>
            <td>
              <a href="javascript:;" class="btn-sm btn btn-label-danger btn-bold delete_charges_details" charges_id="<?php echo $charges_key;?>">
                <i class="la la-trash-o"></i>
               </a>
            </td>
         </tr>
      <?php }?>
   <?php }?>
   <tr>
   	<td class="kt-font-success kt-font-xl kt-font-boldest" colspan="3" style="text-align: center;">GROSS TOTAL</td>
   	<td class="kt-font-success kt-font-xl kt-font-boldest" style="text-align: center;">
         <?php  echo $this->session->userdata('order_details')['gross_total']; ?>  
      </td>
   </tr>	
</tbody>