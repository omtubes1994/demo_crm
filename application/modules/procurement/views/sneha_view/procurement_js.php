jQuery(document).ready(function() {
	
	<?php if(in_array($this->session->userdata('role'), array(1, 6))) { ?>
		<?php if($this->uri->segment(2) == 'rfq_list') {?>
			ajax_call_function({call_type: 'rfq_pending_list', filter_date: '', filter_sales_person: ''}, 'rfq_pending_list');
			ajax_call_function({call_type: 'rfq_done_list', filter_date: '', filter_sales_person: ''}, 'rfq_done_list');
		<?php }?>
	<?php }?>

	$('a.change_rfq_pending_date_for_highchart').click(function(){
		ajax_call_function({call_type: 'rfq_pending_list', filter_date: $(this).attr('filter-date'), filter_sales_person: $('input#filter_sales_person_value_bar_graph').val()}, 'rfq_pending_list');
	});
	$('div.select_sales_person_bar_graph_dropdown').on('click', 'a.select_sales_person_bar_graph', function(){

		ajax_call_function({call_type: 'rfq_pending_list', filter_date: $('input#filter_date_value_bar_graph').val(), filter_sales_person: $(this).attr('sales-person')}, 'rfq_pending_list');
	});

	$('a.change_rfq_done_date_for_highchart').click(function(){
		ajax_call_function({call_type: 'rfq_done_list', filter_date: $(this).attr('filter-date'), filter_sales_person: $('input#filter_sales_person_value_line_graph').val()}, 'rfq_done_list');
	});
	$('div.select_sales_person_line_graph_dropdown').on('click', 'a.select_sales_person_line_graph', function(){

		ajax_call_function({call_type: 'rfq_done_list', filter_date: $('input#filter_date_value_line_graph').val(), filter_sales_person: $(this).attr('sales-person')}, 'rfq_done_list');
	});
	var method_name = '<?php echo $this->uri->segment("2");?>';
   	var third_params = '<?php echo $this->uri->segment("3",0);?>';

   	if(method_name == 'update_purchase_order' && third_params > 0) {
   		set_order_details();
   	}
   	KTAutosize.init();
	$('option.all').hide();
	$('select#vendor_id').on('change', function(){
		$('option.all').hide();
		$('option.'+$(this).val()).show();
		//set vendor name and country based on vendor id
		ajax_call_function({call_type: 'set_vendor_name_and_country', vendor_id: $(this).val()}, 'set_vendor_name_and_country');
	});	    
	$('tbody#product_list_body').on('input', 'input.purchase_order_quantity, input.purchase_order_unit', function(){
		var count_number = $(this).attr('count_number');
		add_net_total(count_number);
	});
	$('button.add_charges_in_purchase_order').click(function(){

		ajax_call_function({call_type: 'add_charges_in_purchase_order', add_charges_in_purchase_order_form: $('form#add_charges_in_purchase_order_form').serializeArray()}, 'add_charges_in_purchase_order');
	});
	$('a.add_gst_or_discount_modal').click(function(){
		add_data_to_charge_history();
	});
	$('i.edit_terms_and_conditions').click(function(){
		var alert_text = $.trim($('div.terms_conditions_alert').html());
		$('textarea#terms_conditions').html('').html(alert_text);
	});
	$('button.update_terms_conditions').click(function(){
		var updated_term_condition = $.trim($('textarea#terms_conditions').val());
		$('div.terms_conditions_alert').html('').html(updated_term_condition);
		$('div#edit_terms_and_conditions').modal('hide');
	});

	$('button.add_purchase_order').click(function(){

		var procurement_id = $(this).attr('procurement_id');
		var call_type = 'update_purchase_order';
		if(procurement_id == ''){

			call_type = 'add_purchase_order';
		}
		ajax_call_function({
							call_type: call_type,
							procurement_id: procurement_id,
							person_details: $('form#purchase_order_user_details_form').serializeArray(),
							product_details: $('form#purchase_order_quantity_form').serializeArray(),
							terms_conditon: $('form#purchase_order_terms_condition_form').serializeArray(),
							term_condition_text: $.trim($('div.terms_conditions_alert').html())
						}, 'add_purchase_order');
	});

	$('a.add_item_to_procurement_product').click(function(){

		var next_count_number = $(this).attr('next_count_number');
		ajax_call_function({call_type: 'get_product_body', next_count_number: next_count_number,product_name: $('select[name="product_name_1"]').val(),material_name:$('select[name="material_name_1"]').val()}, 'get_product_body');
		$(this).attr('next_count_number', ++next_count_number);
	});

	$('tbody#procurement_body').on('click', 'a.delete_procurement_list_data', function(){

		swal({
			  title: "Are you sure?",
			  text: "Once deleted, you will not be able to recover this Procurement Details!",
			  icon: "warning",
			  buttons:  ["Cancel", "Delete"],
			  dangerMode: true,	
			})
			.then((willDelete) => {
			  	if (willDelete) {
	        		ajax_call_function({call_type: 'delete_procurement_list_data', procurement_id: $(this).attr('procurement_id')}, 'delete_procurement_list_data');                 
			    	swal({
			    		title: "Procurement details is deleted",
			      		icon: "success",
			    	});
		  		} else {
				    swal({
			    		title: "Procurement is not deleted",
			      		icon: "info",
			    	});
			  	}
			});
	});

	$('a.add_procuremet_search_filter_form').click(function(){

		$('div.procuremet_search_filter').show();
	});

	$('div#procurement_search_filter_form_div').on('click', 'button.procurement_search_filter_form_submit', function(){
		set_reset_spinner($('button.procurement_search_filter_form_submit'));
		search_filter();
	});

	$('thead#procurement_head').on('click', 'th.sorting_search', function(){
		ajax_call_function({
							call_type: 'sort_filter',
							procurement_sort_name: $(this).attr('sorting_name'),
							procurement_sort_value: $(this).attr('sorting_value'),
							search_form_data: $('form#procurement_search_filter_form').serializeArray()
						},
						'sort_filter');
	});

	$('div#procurement_paggination').on('click', 'li.procurement_paggination_number', function(){

		ajax_call_function({
							call_type: 'paggination_filter',
							limit: $(this).attr('limit'),
							offset: $(this).attr('offset'),
							search_form_data: $('form#procurement_search_filter_form').serializeArray()
						}, 'paggination_filter');
	});
	$('div#procurement_paggination').on('change', 'select#set_limit', function(){

		ajax_call_function({
							call_type: 'paggination_filter',
							limit: $('select#set_limit').val(),
							offset: 0,
							search_form_data: $('form#procurement_search_filter_form').serializeArray()
						}, 'paggination_filter');
	});
	$('tbody#product_list_body').on('click', 'a.delete_product_details', function(){

		$('tr.count_'+$(this).attr('count_no')).remove();
		ajax_call_function({call_type: 'delete_product_details', count_no: $(this).attr('count_no')}, 'delete_product_details');
	});
	$('table#charge_history_table').on('click', 'a.delete_charges_details', function(){

		swal({
			  title: "Are you sure?",
			  text: "Once deleted, you will not be able to recover this charges Details!",
			  icon: "warning",
			  buttons:  ["Cancel", "Delete"],
			  dangerMode: true,	
			})
			.then((willDelete) => {
			  	if (willDelete) {
	        		ajax_call_function({call_type: 'delete_charge', charges_id: $(this).attr('charges_id')}, 'delete_charge');                 
			    	swal({
			    		title: "charges details is deleted",
			      		icon: "success",
			    	});
		  		} else {
				    swal({
			    		title: "charges is not deleted",
			      		icon: "info",
			    	});
			  	}
			});
	}); 
	 $('table#charge_history_table').on('click', 'a.delete_charges', function(){

		$('tr.charges_'+$(this).attr('charges_key')).remove();
		ajax_call_function({call_type: 'delete_charges', charges_key: $(this).attr('charges_key')}, 'delete_charges_details');
    }); 
});

function search_filter() {
	ajax_call_function(
					{
						call_type: 'search_filter',
						search_form_data: $('form#procurement_search_filter_form').serializeArray()
					},
					'search_filter'
				);
}
function add_data_to_charge_history() {

	ajax_call_function({call_type: 'get_gst_or_discount_history'}, 'get_gst_or_discount_history');
}

function add_net_total(count_number) {
	var quantity = $('input[name="quantity_'+count_number+'"]').val();
	var price = $('input[name="price_'+count_number+'"]').val();
	var net_total = price * quantity;
	$('input[name="total_'+count_number+'"]').val(net_total);
	if(net_total > 0) {

		set_order_details(net_total, count_number);
	}
}
function set_order_details(net_total= "", count_number=0) {

	var data={call_type: 'set_order_detail'};
	if(net_total != '') {
		data.net_total = net_total;
	}
	if(count_number != 0) {
		data.count_number = count_number;
	}
	ajax_call_function(data, 'net_total_gross_total_body');
}
// Class definition

var KTAutosize = function () {
    
    // Private functions
    var demos = function () {
        // basic demo
        var demo1 = $('#terms_conditions');
        autosize(demo1);
        $('.vendor_name_select_picker').selectpicker();
    }

    return {
        // public functions
        init: function() {
            demos(); 
        }
    };
}();
function ajax_call_function(data, callType, url = "<?php echo base_url('procurement/ajax_function'); ?>") {

	$.ajax({
		type: 'POST',
		data: data,
		url: url,
		dataType: 'JSON',
		success: function(res){
			if(res.status == 'successful') {
				
				if(callType == 'rfq_pending_list') {
					if(data.filter_date != ''){

						swal({
				    		title: "RFQ Pending List Updated ",
				      		icon: "success",
				    	});
					}

			    	rfq_pending_list_highchart(res.highchart_data);
			    	$('input#filter_date_value_bar_graph').val(res.html_body.filter_date_name);
					$('input#filter_sales_person_value_bar_graph').val(res.html_body.filter_sales_person_id);
					$('span#sales_person_name_bar_graph').html('').html(res.html_body.filter_sales_person_name);
					$('div.select_sales_person_bar_graph_dropdown').html('').html(res.html_body.filter_sales_person);
				} else if(callType == 'rfq_done_list') {
					if(data.filter_date != ''){

						swal({
				    		title: "RFQ Done List Updated ",
				      		icon: "success",
				    	});
					}

			    	rfq_done_highchart(res.highchart_data);
			    	$('input#filter_date_value_line_graph').val(res.html_body.filter_date_name);
					$('input#filter_sales_person_value_line_graph').val(res.html_body.filter_sales_person_id);
					$('span#sales_person_name_line_graph').html('').html(res.html_body.filter_sales_person_name);
					$('div.select_sales_person_line_graph_dropdown').html('').html(res.html_body.filter_sales_person);
				} else if(callType == 'add_charges_in_purchase_order') {
					swal({
			    		title: "Charges are added",
			      		icon: "success",
			    	});
			    	add_data_to_charge_history();
			    	set_order_details();
				} else if(callType == 'get_gst_or_discount_history') {
					$('table#charge_history_table').html('').html(res.charge_history_body);
				} else if(callType == 'net_total_gross_total_body') {
					$('table#net_total_gross_total_body tbody').html('').html(res.net_total_gross_total_body);
				} else if(callType == 'add_purchase_order') {
					swal({
			    		title: res.message,
			      		icon: "success",
			    	});
				} else if(callType == 'set_vendor_name_and_country') {

					if(res.message == 'record found') {

						$('input[name="vendor_name"]').val(res.vendor_details.vendor_name);
						$('input[name="vendor_country"]').val(res.vendor_details.country);
					}
				} else if(callType == 'get_product_body') {
					
					$('tbody#product_list_body').append(res.product_list_body);
				} else if(callType == 'search_filter' || callType == 'sort_filter' || callType == 'paggination_filter') {
					$('tbody#procurement_body').html('').html(res.procurement_body);
					$('div#procurement_search_filter_form_div').html('').html(res.procurement_search_filter);
					$('thead#procurement_head').html('').html(res.procurement_sorting);
					$('div#procurement_paggination').html('').html(res.procurement_paggination);
				}

			}
		},
		beforeSend: function(response){
			
		}
	});
};

function set_reset_spinner(obj, set_unset_flag = true) {

	if(set_unset_flag) {

		$(obj).addClass('kt-spinner');
    	$(obj).addClass('kt-spinner--right');
    	$(obj).addClass('kt-spinner--sm');
    	$(obj).addClass('kt-spinner--light');	
	} else{

		$(obj).removeClass('kt-spinner');
    	$(obj).removeClass('kt-spinner--right');
    	$(obj).removeClass('kt-spinner--sm');
    	$(obj).removeClass('kt-spinner--light');
	}
}
// create user wise rfq pending highchart
function rfq_pending_list_highchart(highchart_data) {

	Highcharts.chart('rfq_pending_list_highchart', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: 'Procurement Person Wise RFQ Pending'
	    },
	    subtitle: {
	        text: ''
	    },
	    accessibility: {
	        announceNewData: {
	            enabled: true
	        }
	    },
	    xAxis: {
	        type: 'category'
	    },
	    yAxis: {
	        title: {
	            text: 'Total Number of RFQ Pending'
	        }

	    },
	    legend: {
	        enabled: false
	    },
	    plotOptions: {
	        series: {
	            borderWidth: 0,
	            dataLabels: {
	                enabled: true,
	                format: '{point.y:f}'
	            }
	        }
	    },

	    tooltip: {
	        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> RFQ Pending<br/>'
	    },

	    series: [
	        {
	            name: "OM Tubes",
	            colorByPoint: true,
	            data: highchart_data
	        }
	    ]

	});
}

// create user wise rfq done highchart
function rfq_done_highchart(highchart_data) {

	Highcharts.chart('rfq_done_list_highchart', {
		
		chart: {
	        type: 'line'
	    },
	    title: {
	        text: 'Procurement Done'
	    },
	    subtitle: {
	        text: ''
	    },
	    xAxis: {
	        // categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
	        categories: highchart_data.category
	    },
	    yAxis: {
	        title: {
	            text: 'Number of Procurement Done'
	        }
	    },
	    plotOptions: {
	        line: {
	            dataLabels: {
	                enabled: true
	            },
	            enableMouseTracking: false
	        }
	    },
	    // series: [{
	    //     name: 'Tokyo',
	    //     data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
	    // }, {
	    //     name: 'London',
	    //     data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
	    // }]
	    series: highchart_data.series

	});
}

// chart: {
//        type: 'line'
//    },
//    title: {
//        text: 'Procurement Done'
//    },
//    subtitle: {
//        text: ''
//    },
//    xAxis: {
//        // categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
//        categories: highchart_data.category
//    },
//    yAxis: {
//        title: {
//            text: 'Number of Procurement Done'
//        }
//    },
//    plotOptions: {
//        line: {
//            dataLabels: {
//                enabled: true
//            },
//            enableMouseTracking: false
//        }
//    },
//    series: [
//    	{
//         name: 'Procurement Done',
//         // data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
//         data: highchart_data.data
//     }
//    ]



//  $('tbody#charges_history').on('click', 'a.delete_charge', function(){

// 		swal({
// 			  title: "Are you sure?",
// 			  text: "Once deleted, you will not be able to recover this charges Details!",
// 			  icon: "warning",
// 			  buttons:  ["Cancel", "Delete"],
// 			  dangerMode: true,	
// 			})
// 			.then((willDelete) => {
// 			  	if (willDelete) {
// 	        		ajax_call_function({call_type: 'delete_charge', charges_id: $(this).attr('charges_id')}, 'delete_charge');                 
// 			    	swal({
// 			    		title: "charges details is deleted",
// 			      		icon: "success",
// 			    	});
// 		  		} else {
// 				    swal({
// 			    		title: "charges is not deleted",
// 			      		icon: "info",
// 			    	});
// 			  	}
// 			});
// 	}); 
// $('').on('click', 'a.delete_charges', function(){

// 		$('tr.charges_'+$(this).attr('charges_key')).remove();
// 		ajax_call_function({call_type: 'delete_charges', charges_key: $(this).attr('charges_key')}, 'delete_charges_details');
// 	}); 