<tr class="<?php echo 'count_', $next_count_number; ?>">
	<td>
		<?php echo $next_count_number;?>
	</td>
	<td>
		<select class="form-control" name="product_name_<?php echo $next_count_number;?>">
			<option value="">Select</option>
			<?php foreach($product_list as $single_product_name) {?>
				<option 
					value="<?php echo $single_product_name['lookup_value'];?>"
					<?php echo 
			         	($product_name == $single_product_name['lookup_value'])
			         	? 'selected'
			         	:'';
		         	?>
				>
				<?php echo $single_product_name['lookup_value'];?></option>
			<?php } ?>
		</select>
	</td>
	<td>
		<select class="form-control" name="material_name_<?php echo $next_count_number;?>">
			<option value="">Select</option>
				<?php foreach($material_list as $single_material_name) {?>
               		<option 
               			value="<?php echo $single_material_name['lookup_value'];?>"
	               		<?php echo 
				         	($material_name == $single_material_name['lookup_value'])
				         	? 'selected'
				         	:'';
			         	?>
               		>
               			<?php echo $single_material_name['lookup_value'];?>
               		</option>
           		<?php } ?> 
		</select>
	</td>
	<td>
		<input type="text" class="form-control" name="description_name_<?php echo $next_count_number;?>">
	</td>
	<td>
		<input type="number" class="form-control purchase_order_quantity" count_number="<?php echo $next_count_number;?>" name="quantity_<?php echo $next_count_number;?>">
	</td>
	<td>
		<select class="form-control" name="units_<?php echo $next_count_number;?>">
			<option value="">Select</option>
			<?php foreach($units_list as $units_name) {?>
				<option value="<?php echo $units_name['unit_value'];?>"><?php echo $units_name['unit_value'];?></option>
			<?php } ?>
		</select>
	</td>
	<td>
		<input type="number" class="form-control purchase_order_unit" count_number="<?php echo $next_count_number;?>" name="price_<?php echo $next_count_number;?>">
	</td>
	<td style="display: none;">
		<input type="number" class="form-control" name="per_unit_<?php echo $next_count_number;?>">
	</td>
	<td>
		<input type="number" class="form-control" name="total_<?php echo $next_count_number;?>" readonly>
	</td>
	<td>
		<a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold delete_product_details" count_no="<?php echo $next_count_number;?>">
			<i class="la la-trash-o"></i>
		</a>
	</td>
</tr>