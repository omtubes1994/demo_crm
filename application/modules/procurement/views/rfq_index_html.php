<style type="text/css">
	.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
		padding-top: 4% !important;
	}
	.layer-white{
	    display: none;
	    position: absolute;
	    top: 0em !important;
	    left: 0em !important;
	    width: 100%;
	    height: 100%;
	    text-align: center;
	    vertical-align: middle;
	    background-color: rgba(255, 255, 255, 0.55);
	    opacity: 1;
	    line-height: 1;
	    -webkit-animation-fill-mode: both;
	    animation-fill-mode: both;
	    -webkit-animation-duration: 0.5s;
	    animation-duration: 0.5s;
	    -webkit-transition: background-color 0.5s linear;
	    transition: background-color 0.5s linear;
	    -webkit-user-select: none;
	    -moz-user-select: none;
	    -ms-user-select: none;
	    user-select: none;
	    will-change: opacity;
	    z-index: 9;
	}
	.div-loader{
	    position: absolute;
		top: 50%;
		left: 50%;
		margin: 0px;
		text-align: center;
		z-index: 1000;
		-webkit-transform: translateX(-50%) translateY(-50%);
		transform: translateX(-50%) translateY(-50%);
	}
	.kt-spinner:before {
	    width: 50px;
	    height: 50px;
	    margin-top: -10px;
	}
</style>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					RFQ List
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
		        <div class="kt-portlet__head-wrapper">
		            <div class="kt-portlet__head-actions">
		                <a href="javascript:;" class="btn btn-brand btn-elevate btn-icon-sm">
		                    Add Search Filter
		                </a>
		            </div>
		        </div>
	    	</div>
		</div>
		<div class="kt-portlet__body" style="display: block;" id="">

        	<!--begin: Search Form -->
			<form class="kt-form kt-form--fit kt-margin-b-20" id="">
				<div id="invoice_search_filter_form_div">
					<div class="row kt-margin-b-20">
					   	<div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
					      	<label>Rfq No #</label>
					      	<input type="text" class="form-control" value="" name="" placeholder="Enter Rfq No #" />
					   	</div>
					   	<div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
					      	<label>Company Name</label>
							<input type="text" class="form-control" value="" name="" placeholder="Enter Company Name" />
					   	</div>
					   	<div class="col-lg-2 col-md-9 col-sm-12">
					      	<label>Rfq Date</label>
			  				<div class="form-group-sub">
								<div class="kt-portlet__head-toolbar">
									<input type="text" class="form-control" value="" name="" id="" hidden/>
				                    <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
										<a href="javascript:void(0);" class="btn kt-subheader__btn-daterange" id="rfq_list_search_filter_date" data-toggle="kt-tooltip" title="Select Destrack Date" data-placement="left" style="background: rgb(0 0 0 / 10%); display: inline-flex; height: 32px !important; padding-top: 0; padding-bottom: 0; margin-top: 0.25rem; margin-bottom: 0.25rem; margin-left: 0.25rem;">
											<span class="kt-subheader__btn-daterange-title" id="rfq_list_search_filter_date_title" style="display: inline-block;color: #959cb6;font-weight: 500;">Today</span>&nbsp;
											<span class="kt-subheader__btn-daterange-date" id="rfq_list_search_filter_date_date" style="display: inline-block;color: #5d78ff;margin-right: 0.75rem;font-weight: 500;">Aug 16</span>
											<i class="flaticon2-calendar-1" style="color: #5d78ff; font-size: 1rem !important;"></i>
										</a>
				                    </ul>
				                </div>
							</div>
						</div>
					   	<div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
					      	<label>Rfq Importance</label>
							<select class="form-control procurement_select_picker" name="" multiple>
						      	<option value="">Select</option>
						        <option value="V">Very High</option>
						        <option value="H">High</option>
						        <option value="M">Medium</option>
						        <option value="L">Low</option>
							</select>
					   	</div>
					   	<div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
					      	<label>Sales Person</label>
							<select class="form-control procurement_select_picker" name="" multiple>
						      	<option value="">Select</option>
						        <option value="V">Very High</option>
						        <option value="H">High</option>
						        <option value="M">Medium</option>
						        <option value="L">Low</option>
							</select>
					   	</div>
					   	<div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
					      	<label>Procurement Person</label>
							<select class="form-control procurement_select_picker" name="" multiple>
						      	<option value="">Select</option>
						        <option value="V">Very High</option>
						        <option value="H">High</option>
						        <option value="M">Medium</option>
						        <option value="L">Low</option>
							</select>
					   	</div>
					   	<div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
					      	<label>Rfq Status</label>
							<select class="form-control procurement_select_picker" name="" multiple>
						      	<option value="">Select</option>
						      	<option value="waiting">Waiting</option>
								<option value="Pending">Pending</option>
								<option value="query">Query</option>
								<option value="regret">Regret</option>
								<option value="done">Done</option>
							</select>
					   	</div>
					   	<div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
					      	<label>Quotation #</label>
							<select class="form-control procurement_select_picker" name="" multiple>
						      	<option value="">Select</option>
						        <option value="draft">Draft</option>
							</select>
					   	</div>
					</div>	      
					<div class="kt-separator kt-separator--md kt-separator--dashed"></div>
					<div class="row">
					    <div class="col-lg-12">
					        <button class="btn btn-primary btn-brand--icon invoice_search_filter_form_submit" type="reset">
					            <span>
					                <i class="la la-search"></i>
					                <span>Search</span>
					            </span>
					        </button>
					        &nbsp;&nbsp;
					        <button class="btn btn-secondary btn-secondary--icon invoice_search_filter_form_reset" type="reset">
					            <span>
					                <i class="la la-close"></i>
					                <span>Reset</span>
					            </span>
					        </button>
					    </div>
					</div>
				</div>	
			</form>
		</div>
		<div class="kt-portlet__body">
			<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
				<div class="row">
					<div class="col-sm-12">
						<input type="text" id="column_name" value="" hidden>
    					<input type="text" id="column_value" value="" hidden>
						<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 100%;">
							<thead id="invoice_header">
          						<tr>
									<th class="sorting_disabled kt-align-center" style="width: 5%;">Sr.No</th>
									<th 
										class="sorting_asc sorting_search kt-align-center" 
										sorting_name="invoice_mst.invoice_no"
										sorting_value=""
										style="width: 10%;">
										Rfq No #
									</th>
									<th 
										class="sorting_search kt-align-left row" 
										sorting_name=""
										sorting_value=""
										style="padding-left: 20px;">
										Rfq Details
									</th>
									<th 
										class="sorting_search kt-align-center"
										sorting_name=""
										sorting_value=""
										style="width: 10%;">
										Sales Person
									</th>
									<th 
										class="sorting_search kt-align-center"
										sorting_name=""
										sorting_value=""
										style="width: 10%;">
										Procurement Person
									</th>    
									<th 
										class="sorting_search kt-align-center"
										sorting_name=""
										sorting_value=""
										style="width: 10%;">
										Rfq Status
									</th>
									<th 
										class="sorting_search kt-align-center"
										sorting_name=""
										sorting_value=""
										style="width: 10%;">
										Quotation #
									</th>
									<th class="sorting_disabled kt-align-center" style="width: 15%;">Actions</th>
								</tr>
							</thead>
							<tbody id="invoice_body">
								<tr>  
							        <td><span class="kt-align-center">1</span></td>
									<td><span class="kt-align-center kt-font-bolder kt-font-brand">OM/77057/20-21</span></td>
									<td>
										<div class="row">
											<!-- Company Name -->
											<span class="col-xl-8 kt-align-left kt-font-bold">
												Company Name:
												<span class="kt-align-left">PSI SPECIALTIES LLC</span>
											</span>
											<!-- Member Name -->
											<span class="col-xl-8 kt-align-left kt-font-bold">
												Member Name:
												<span class="kt-align-left">Mark Domangue</span>
											</span>
											<!-- Rfq Date -->
											<span class="col-xl-4 kt-align-left kt-font-bold"></span>
											<span class="col-xl-6 kt-align-left kt-font-bold">
												Start Date:
												<span class="kt-align-left kt-font-brand">09,Apr 2022</span>
											</span>
											<span class="col-xl-6 kt-align-left kt-font-bold">
												Closing Date:
												<span class="kt-align-left kt-font-brand">09,Apr 2022</span>
											</span>
											<!-- Importance -->
											<span class="col-xl-8 kt-align-left kt-font-bold">
												Importance:
												<span class="kt-align-left">High</span>
											</span>
											<!-- Subject -->
											<span class="col-xl-8 kt-align-left kt-font-bold">
												Subject:
												<span class="kt-align-left">Subject</span>
											</span>
										</div>
									</td>
									<td>
										<div class="row">
											<span class="col-xl-12 kt-align-center kt-font-bold">
												Manish k
											</span>
										</div>
									</td>
									<td>
										<div class="row">
											<span class="col-xl-12 kt-align-center kt-font-bold">
												Sarvesh p
											</span>
										</div>	
									</td>
									<td>
										<div class="row">
											<span class="col-xl-12 kt-align-center kt-font-bold">
												Pending
											</span>
										</div>
									</td>
									<td>
										<div class="row">
											<span class="col-xl-12 kt-align-center kt-font-bolder" style="color: rgb(113, 106, 202);">
												Draft
											</span>
										</div>
									</td>
									<td>
										<a href="" class="btn btn-sm btn-clean btn-icon btn-icon-md" target="_blank" title="View RFQ"><i class="la la-info-circle"></i></a>
										<button type="button" data-toggle="modal" data-target="#query-modal" class="btn btn-sm btn-clean btn-icon btn-icon-md replyquery pull-right" title="View / Reply Query" member_id="">
											<i class="la la-comment"></i>
										</button>
										<button type="button" data-toggle="modal" data-target="#notes-modal" class="btn btn-sm btn-clean btn-icon btn-icon-md notes" title="View Notes" rfq_id="">
											<i class="la la-sticky-note"></i>
										</button>
										<button class="btn btn-sm btn-clean btn-icon btn-icon-md query" title="Sales Query" query_id="" quote_id="" query_type="">
			                        		<i class="fa fa-question-circle" style="color: red;"></i>
			                        	</button>
			                        	<a href="" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View Quotation" target="_blank">
		                            		<i class="la la-eye"></i>
		                        		</a>
		                        		<span class="badge badge-danger" style="color: #fff; font-size: 8px;">New</span>
		                        		<button class="btn btn-sm btn-clean btn-icon btn-icon-md purchase_query" title="Purchase Query" query_id="" quote_id="" query_type="">
			                        		<i class="fa fa-question-circle" style="color: green;"></i>
			                        	</button>
			                        	<button type="button" class="btn btn-bold btn-label-brand btn-sm update_rating" data-toggle="modal" rfq_id="" priority_value="" priority_reason="" data-target="#update_rating_rfq"><i class="flaticon-star"></i></button>
			                        	<a href="" class="btn btn-sm btn-clean btn-icon btn-icon-md" target="_blank" title="Make Quote"><i class="fa fa-file-invoice"></i></a>
			                        	<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md deleteRfq pull-right" title="Delete" rfq_id="" >
											<i class="la la-trash"></i>
										</button>
									</td>
							    </tr>
							</tbody>
						</table>
						<div id="invoice_table_loader" class="layer-white">
							<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
						</div>
					</div>
				</div>
				<div class="row" id="invoice_paggination">
					<?php if(!empty($paggination_data)){ ?>
					<div class="col-xl-2 dataTables_pager">
					    <input type="text" id="invoice_limit" value="<?php echo $paggination_data['limit'];?>" hidden>
					    <input type="text" id="invoice_offset" value="<?php echo $paggination_data['offset'];?>" hidden>
					    <div class="dataTables_info" id="kt_table_1_info" role="status" aria-live="polite" style="font-weight: 500;">Showing <?php echo $paggination_data['offset'] +1; ?> to <?php echo $paggination_data['limit'] + $paggination_data['offset']; ?> of <?php echo $paggination_data['total_rows']; ?> entries</div>
					</div>
					<div class="col-xl-10 dataTables_pager">
					    <div class="dataTables_length" id="kt_table_1_length">
					        <label style="font-weight: 500;">Display
					            <select name="kt_table_1_length" aria-controls="kt_table_1" id="set_limit" class="custom-select custom-select-sm form-control form-control-sm hr_user_list_paggination" style="width: 50% !important;">
					                <option value="5" <?php echo ($paggination_data['limit'] == 5) ? 'selected': ''; ?>>5</option>
					                <option value="10" <?php echo ($paggination_data['limit'] == 10) ? 'selected': ''; ?>>10</option>
					                <option value="25" <?php echo ($paggination_data['limit'] == 25) ? 'selected': ''; ?>>25</option>
					                <option value="50" <?php echo ($paggination_data['limit'] == 50) ? 'selected': ''; ?>>50</option>
					                <option value="100" <?php echo ($paggination_data['limit'] == 100) ? 'selected': ''; ?>>100</option>
					                <option value="500" <?php echo ($paggination_data['limit'] == 500) ? 'selected': ''; ?>>500</option>
					            </select>
					        </label>
					    </div>
					    <div class="dataTables_paginate paging_simple_numbers" id="kt_table_1_paginate">
					        <ul class="pagination">
					            <?php
					                $last_page_offset = ((round($paggination_data['total_rows']/$paggination_data['limit'])*$paggination_data['limit']));
					                if($paggination_data['offset'] < $paggination_data['limit']*4) {

					                    $start_value = $paggination_data['limit'];
					                    $first_blank=false;
					                    $last_blank=true;
					                    $increment_amount = $start_value + $paggination_data['limit']*4;
					                } elseif($paggination_data['offset'] >= $paggination_data['limit']*4 && $paggination_data['offset'] < ($last_page_offset-$paggination_data['limit']*4)) {

					                    $start_value = $paggination_data['offset'] - $paggination_data['limit'];
					                    $first_blank=true;
					                    $last_blank=true;
					                    $increment_amount = $start_value + $paggination_data['limit']*3;
					                } elseif($paggination_data['offset'] >= $paggination_data['limit']*4 && $paggination_data['offset'] >= ($last_page_offset-$paggination_data['limit']*4)) {

					                    $start_value = $last_page_offset-$paggination_data['limit']*5;
					                    $first_blank=true;
					                    $last_blank=false;
					                    $increment_amount = $start_value + $paggination_data['limit']*4;
					                }
					                if($increment_amount > $paggination_data['total_rows']) {

					                    $last_blank=false;
					                }
					            ?>
					            <?php if($paggination_data['offset'] > 0) {?>
					                <li class="paginate_button page-item previous invoice_paggination_number" id="kt_table_1_previous" limit="<?php echo $paggination_data['limit']; ?>" offset="<?php echo $paggination_data['offset']-$paggination_data['limit']; ?>">
					                    <a href="javascript:void(0)" aria-controls="kt_table_1" data-dt-idx="0" tabindex="0" class="page-link">
					                        <i class="la la-angle-left"></i>
					                    </a>
					                </li>
					            <?php }?>   
					            <li class="paginate_button page-item invoice_paggination_number <?php echo($paggination_data['offset'] == 0)? 'active': ''?>" limit="<?php echo $paggination_data['limit']; ?>" offset="<?php echo 00; ?>">
					                <a href="javascript:;" aria-controls="kt_table_1" data-dt-idx="1" tabindex="0" class="page-link">1</a>
					            </li>
					            <?php if($first_blank) {?>
					                <li class="paginate_button page-item disabled" id="kt_table_1_ellipsis">
					                    <a href="javascript:void(0)" aria-controls="kt_table_1" class="page-link">…</a>
					                </li>
					            <?php }?>
					            <?php for ($i=$start_value; $i < $increment_amount; $i+=$paggination_data['limit']) { ?>
					                <?php if($i < $paggination_data['total_rows']) { ?>
					                <li class="paginate_button page-item invoice_paggination_number <?php echo($paggination_data['offset'] == $i)? 'active': ''?>" limit="<?php echo $paggination_data['limit']; ?>" offset="<?php echo $i; ?>">
					                    <a href="javascript:;" aria-controls="kt_table_1" class="page-link"><?php echo ($paggination_data['limit']+$i)/$paggination_data['limit']; ?></a>
					                </li>
					                <?php } ?>
					            <?php } ?>
					            <?php if($last_blank) {?>
					                <li class="paginate_button page-item disabled" id="kt_table_1_ellipsis">
					                    <a href="javascript:void(0)" aria-controls="kt_table_1" class="page-link">…</a>
					                </li>
					            <?php }?>
					            <li class="paginate_button page-item invoice_paggination_number <?php echo($paggination_data['offset'] == ($last_page_offset-$paggination_data['limit']))? 'active': ''?>" limit="<?php echo $paggination_data['limit']; ?>" offset="<?php echo ($last_page_offset-$paggination_data['limit']); ?>">
					                <a href="javascript:void(0)" aria-controls="kt_table_1" class="page-link"><?php echo round($last_page_offset/$paggination_data['limit']); ?></a>
					            </li>
					            <?php if($paggination_data['offset'] < $last_page_offset-$paggination_data['limit']) {?>
					                <li class="paginate_button page-item next invoice_paggination_number" id="kt_table_1_next" limit="<?php echo $paggination_data['limit']; ?>" offset="<?php echo ($paggination_data['offset']+$paggination_data['limit']); ?>">
					                    <a href="javascript:void(0)" aria-controls="kt_table_1" class="page-link">
					                        <i class="la la-angle-right"></i>
					                    </a>
					                </li>
					            <?php } ?>  
					        </ul>
					    </div>
					</div>
					<?php }?>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- end:: Content -->