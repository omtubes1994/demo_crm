<h6 class="dropdown-header">Select RFQ Status</h6>
<a class="dropdown-item change_rfq_status <?php echo ($filter_rfq_status == 'pending')? 'disabled':' '?>" rfq-status="pending">Pending</a>
<a class="dropdown-item change_rfq_status <?php echo ($filter_rfq_status == 'done')? 'disabled':' '?>" rfq-status="done">Done</a>
<a class="dropdown-item change_rfq_status <?php echo ($filter_rfq_status == 'regret')? 'disabled':' '?>" rfq-status="regret">Regret</a>
<a class="dropdown-item change_rfq_status <?php echo ($filter_rfq_status == 'waiting')? 'disabled':' '?>" rfq-status="waiting">Waiting</a>
<a class="dropdown-item change_rfq_status <?php echo ($filter_rfq_status == 'query')? 'disabled':' '?>" rfq-status="query">Query</a>