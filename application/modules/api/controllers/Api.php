<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends MX_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('api_model');
	}

	function getMachineTime(){
		$arr = array(
            "company_id" => 6617,
            "user_id" => "0",
            "cmd" => "get_user_data",
            "from_date" => date('Y-m-d', strtotime('-1 Day')),
            "to_date" => date('Y-m-d', strtotime('-1 Day')),
            "access_token" => "NUW7GH4UK7YPjXLPh_A49uBEm13JG6tF"
        );
        $post_data = json_encode(array('data' => $arr));

        // $url = "https://desktrack.timentask.com/api/desktop_tracking/web/v1/index/index";
        $url = "https://apidesktrack.timentask.com/api/desktop_tracking/web/v1/index";
        //$token = $this->token;
        $headers = array(
            'content-type:application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);
        // echo "<pre>";print_r(json_decode($result, true));echo"</pre><hr>";die;
        $res = json_decode($result);
        foreach ($res->data->employees_data as $key => $value) {
        	$temp_arr = array();
        	foreach ($value as $key => $value) {
        		if($key == 'userid'){
        			$key = 'desktrack_userid';
        		}
        		$temp_arr[$key] = $value;
                $temp_arr['entered_on'] = date('Y-m-d H:i:s');
        	}
        	$insert_arr = $temp_arr;
        	$insert_arr['complete_data'] = json_encode($insert_arr);
        	$this->api_model->insertData('machine_time', $insert_arr);
        }
	}


    function clearMargins(){
        $ts = strtotime(date('Y-m-d'));
        $ts_back = strtotime("-2 month", $ts);
        $date_back = date('Y-m-d', $ts_back);
        $quotes = $this->api_model->clearMargins($date_back);
    }
    ###### URL: api/clear_margins_proforma/2023-03-28  ########
    function clear_margins_proforma(){

        $date = $this->uri->segment(3, 0);
        if(!empty($date)){

            $date = date('y-m-d', strtotime($date));
            echo "Process Started For Date: {$date}","<hr>";
            $proforma_data = $this->api_model->get_proforma($date);
            // echo $this->api_model->db->last_query();
            // echo "<pre>";print_r($proforma_data);echo"</pre><hr>";die;
            if(!empty($proforma_data)){

                echo "Proforma Data Found","<hr>";
                foreach ($proforma_data as $count => $single_details) {
                
                    echo "Getting Quotation Line item","<hr>";
                    $quotation_line_item_details = $this->api_model->get_data('*', array('margin >'=> 0, 'quotation_mst_id'=> $single_details['quotation_mst_id']), 'quotation_dtl');
                    if(!empty($quotation_line_item_details)){

                        echo "Quotation Line item Found","<hr>";
                        foreach ($quotation_line_item_details as $line_item_count => $single_quotation_line_item_details) {
        
                            //checking current line_item exist or not in remove margin table
                            $margin_remove_exist = $this->api_model->get_data(
                                '*',
                                array(
                                    'status'=> 'Active',
                                    'quotation_mst_id'=> $single_details['quotation_mst_id'],
                                    'quotation_dtl_id'=> $single_quotation_line_item_details['quotation_dtl_id']
                                ),
                                'margin_remove_details',
                                'row_array'
                            );
            
                            $id = 0;        
                            if(empty($margin_remove_exist)){
            
                                $id = $this->api_model->insert_data(
                                    'margin_remove_details',
                                    array(
                                        'quotation_mst_id'=> $single_details['quotation_mst_id'],
                                        'quotation_dtl_id'=> $single_quotation_line_item_details['quotation_dtl_id'],
                                        'details'=> json_encode(
                                                        array(
                                                            array(
                                                                'unit_rate'=> $single_quotation_line_item_details['unit_rate'],
                                                                'margin'=> $single_quotation_line_item_details['margin'],
                                                                'packing_charge'=> $single_quotation_line_item_details['packing_charge'],
                                                                'date_time'=> date('Y-m-d h:i:s')
                                                            )
                                                        )
                                                    )
                                    )
                                );
                            }else{
            
                                $id = $margin_remove_exist['id'];
                                $details_array = json_decode($margin_remove_exist['details'], true);
                                $details_array[count($details_array)] = array(
                                                                'unit_rate'=> $single_quotation_line_item_details['unit_rate'],
                                                                'margin'=> $single_quotation_line_item_details['margin'],
                                                                'packing_charge'=> $single_quotation_line_item_details['packing_charge'],
                                                                'date_time'=> date('Y-m-d h:i:s')
                                );
                                $this->api_model->update_data(
                                    'margin_remove_details',
                                    array(
                                        'details'=> json_encode($details_array)
                                    ),
                                    array(
                                        'id'=> $margin_remove_exist['id']
                                    )
                                );
                            }
            
                            if($id > 0){
            
                                $this->api_model->update_data(
                                    'quotation_dtl',
                                    array(
                                        'margin'=> 0.00
                                    ),
                                    array(
                                        'quotation_mst_id'=> $single_details['quotation_mst_id'],
                                        'quotation_dtl_id'=> $single_quotation_line_item_details['quotation_dtl_id'],
                                    )
                                );
                            }
            
                            // die('done one process!!!');
                        }
                    }else{

                        echo "Quotation Line item Not Found","<hr>";
                    }
                    
                    // echo "<pre>";print_r($quotation_line_item_details);echo"</pre><hr>";
                }
            }else{

                echo "Proforma Data Not Found","<hr>";
            }

            echo "Process Ended For Date: {$date}","<hr>";
        }else{

            echo "Date Not Found","<hr>";
        }
        
        
        // echo "<pre>";print_r($proforma_data);echo"</pre><hr>";die;
    }
}