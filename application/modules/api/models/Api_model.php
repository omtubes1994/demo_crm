<?php 
Class Api_model extends CI_Model{

	function insertData($table, $data){
		$this->db->insert($table, $data);
	}

	function clearMargins($date){
		$quotes = $this->db->get_where('quotation_mst', array('entered_on <= ' => $date))->result_array();
		foreach ($quotes as $key => $value) {
			$this->db->update('quotation_dtl', array('unit_rate' => null, 'margin' => null, 'packing_charge' => null), array('quotation_mst_id' => $value['quotation_mst_id']));
		}
	}

	public function get_proforma($date){

		$res = $this->db->get_where('quotation_mst', array('stage'=> 'proforma', 'status'=> 'Won', 'proforma_no !='=> '', 'confirmed_on >='=> $date.' 00:00:00', 'confirmed_on <='=> $date.' 23:59:59'))->result_array();
		return $res;
	}

	public function get_data($select_string = '*', $where_array, $table_name, $return_type = 'result_array', $limit = 0, $offset = 0) {

		$this->db->select($select_string);
		$this->db->where($where_array);
		return $this->db->get($table_name, $limit, $offset)->$return_type();
	}

	public function insert_data($table_name, $insert_data){

		$this->db->insert($table_name, $insert_data);
		return $this->db->insert_id();
	}

	public function update_data($table_name, $update_data, $where_array){
		
		return $this->db->update($table_name, $update_data, $where_array);
	}
}