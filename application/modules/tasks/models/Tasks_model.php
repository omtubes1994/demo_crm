<?php 
Class Tasks_model extends CI_Model{

	function __construct(){
		parent::__construct();
		$CI = &get_instance();
		$this->db2 = $CI->load->database('marketing', true);
	}

	function insertData($table, $data){
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	function insertDataDB2($table, $data){
		$this->db2->insert($table, $data);
		return $this->db2->insert_id();
	}

	function updateData($table, $data, $where){
		$this->db->update($table, $data, $where);
	}

	function deleteData($table, $where){
		$this->db->delete($table, $where);
	}

	function getLookup($lookup_id){
		$this->db->order_by('lookup_value');
		return $this->db->get_where('lookup', array('lookup_group' => $lookup_id))->result_array();
	}

	function getData($table, $where=''){
		if($where != ''){
			$this->db->where($where);
		}
		return $this->db->get($table)->result_array();
	}

	function getTaskListData($start, $length, $search, $order, $dir){
		$this->db->select('t.*, a.name assigned_to_name, c.name created_by_name');
		$this->db->join('users c', 'c.user_id = t.created_by', 'inner');
		$this->db->join('users a', 'a.user_id = t.created_by', 'inner');
		$this->db->limit($length, $start);
		$this->db->order_by($order, $dir);
		/*foreach ($search as $key => $value) {
			if($key == 1 && $value != ''){
				$exp_arr = explode(',', $value);
				$this->db->where_in('EXPORTER_NAME', $exp_arr);
			} else if($key == 2 && $value != ''){
				$imp_arr = explode(',', $value);
				$this->db->where_in('IMPORTER_NAME', $imp_arr);
			} else if($key == 3 && $value != ''){
				$new_imp_arr = explode(',', $value);
				$this->db->where_in('NEW_IMPORTER_NAME', $new_imp_arr);
			} else if($key == 5 && $value != ''){
				$country_arr = explode(',', $value);
				$this->db->where_in('COUNTRY_OF_DESTINATION', $country_arr);
			}
		}*/
		if($search['datewise'] == 'open_tasks'){
			$this->db->where('t.status', 'Open');
		}else if($search['datewise'] == 'due_today'){
			$this->db->where("date_format(t.deadline, '%Y-%m-%d') = ", date('Y-m-d'));
			$this->db->where('t.status', 'Open');
		}else if($search['datewise'] == 'due_week'){
			$monday = strtotime("last monday");
			$monday = date('w', $monday)==date('w') ? $monday+7*86400 : $monday;
			$sunday = strtotime(date("Y-m-d",$monday)." +6 days");
			$this_week_sd = date("Y-m-d",$monday);
			$this_week_ed = date("Y-m-d",$sunday);

			$this->db->where("date_format(t.deadline, '%Y-%m-%d') >= ", $this_week_sd);
			$this->db->where("date_format(t.deadline, '%Y-%m-%d') <= ", $this_week_ed);
			$this->db->where('t.status', 'Open');
		}else if($search['datewise'] == 'overdue'){
			$this->db->where('t.deadline < ', date('Y-m-d'));
			$this->db->where('t.status', 'Open');
		}
		if($this->session->userdata('role') != 1){
			$this->db->where('assigned_to', $this->session->userdata('user_id'));
		}
		$res = $this->db->get('tasks t')->result_array();
		$k=0;
		$result = array();
		foreach ($res as $key => $value) {
			$result[$key] = $value;
			$result[$key]['record_id'] = ++$k;
			if($result[$key]['lead_id'] != '' || $result[$key]['lead_id'] != null){
				if($result[$key]['lead_source'] == 'main'){
					$this->db->select('client_name');
					$res = $this->db->get_where('clients', array('client_id' => $result[$key]['lead_id']))->row_array();
					$result[$key]['client_name'] = $res['client_name'];

					$res_mem = $this->db->get_where('members', array('member_id' => $result[$key]['member_id']))->row_array();
					$result[$key]['member_name'] = $res_mem['name'];
				}/*else if($result[$key]['lead_source'] == 'hetro leads'){
					$this->db->select('company_name');
					$res = $this->db->get_where('hetro_leads', array('lead_id' => $result[$key]['lead_id']))->row_array();
					$result[$key]['client_name'] = $res['company_name'];

					$res_mem = $this->db->get_where('hetro_lead_detail', array('lead_dtl_id' => $data['member_id']))->row_array();
					$result[$key]['member_name'] = $res_mem['member_name'];
				}*/else if($result[$key]['lead_source'] == 'primary'){
					$this->db2->select('importer_name');
					$res = $this->db2->get_where('lead_mst', array('lead_mst_id' => $result[$key]['lead_id']))->row_array();
					$result[$key]['client_name'] = $res['importer_name'];

					$res_mem = $this->db2->get_where('lead_detail', array('lead_dtl_id' => $result[$key]['member_id']))->row_array();
					$result[$key]['member_name'] = $res_mem['member_name'];
				}
			}
			$result[$key]['deadline'] = date('d M H:i a', strtotime($value['deadline']));
			if($value['assigned_to'] == $this->session->userdata('user_id')){
				$result[$key]['created_by_name'] = 'Self';
			}

			if($value['last_deadline'] == date('Y-m-d')){
				$result[$key]['status'] = 'Postponed';
			}
		}
		return $result;
	}

	function getTaskListCount($search){
		$this->db->select('t.*, a.name assigned_to_name, c.name created_by_name');
		$this->db->join('users c', 'c.user_id = t.created_by', 'inner');
		$this->db->join('users a', 'a.user_id = t.created_by', 'inner');
		/*foreach ($search as $key => $value) {
			if($key == 1 && $value != ''){
				$exp_arr = explode(',', $value);
				$this->db->where_in('EXPORTER_NAME', $exp_arr);
			} else if($key == 2 && $value != ''){
				$imp_arr = explode(',', $value);
				$this->db->where_in('IMPORTER_NAME', $imp_arr);
			} else if($key == 3 && $value != ''){
				$new_imp_arr = explode(',', $value);
				$this->db->where_in('NEW_IMPORTER_NAME', $new_imp_arr);
			} else if($key == 5 && $value != ''){
				$country_arr = explode(',', $value);
				$this->db->where_in('COUNTRY_OF_DESTINATION', $country_arr);
			}
		}*/
		if($this->session->userdata('role') != 1){
			$this->db->where('assigned_to', $this->session->userdata('user_id'));
		}
		$res = $this->db->get('tasks t')->result_array();
		return sizeof($res);
	}

	public function get_production_status_wise_count($where_string){

		$this->db->select('count(*), user_id');
		$this->db->join('production_process_information', 'production_process_information.work_order_no = task_management.work_order_no', 'left');
		$this->db->join('quotation_mst', 'quotation_mst.work_order_no = task_management.work_order_no', 'left');
		$this->db->where($where_string);
		$this->db->group_by('task_management.user_id');
		$this->db->order_by('count(*)', 'desc');
		$res = $this->db->get('task_management')->result_array();

		// echo "<pre>";print_r($this->db->last_query());echo"</pre><hr>";
		return $res;
	}

	public function get_task_list_data($where_string, $order_by_string, $limit, $offset){

		$return_array = array();
		$query = "SELECT
					SQL_CALC_FOUND_ROWS
						task_management.id,
						task_management.type,
						task_management.user_id,
						task_management.name,
						task_management.work_order_no,
						task_management.comment,
						task_management.file_uploaded_details,
						task_management.vendor_file_name,
						task_management.client_file_name,
						task_management.task_status,
						task_management.approved_by,
						task_management.done_time,
						task_management.add_time,
						task_management.marking_revised,
						task_management.revised_comment,
						production_process_information.production_status,
						production_process_information.product_family,
						production_process_information.technical_document_file_and_path, production_process_information.work_order_sheet_approved_by,production_process_information.work_order_sheet_approved_by_status,
						quotation_mst.quotation_mst_id,
						quotation_mst.client_id,
						quotation_mst.quote_no,
						quotation_mst.purchase_order,
						quotation_mst.rfq_id
				FROM
					`task_management`
				LEFT JOIN
					production_process_information
				on
					production_process_information.work_order_no = task_management.work_order_no
				LEFT JOIN
					quotation_mst
				on
					quotation_mst.work_order_no = task_management.work_order_no

				WHERE
					{$where_string}
				ORDER BY
					{$order_by_string}
				LIMIT
					{$limit}
				OFFSET
					{$offset}";
		$return_array['task_list'] = $this->db->query($query)->result_array();
		// echo "<pre>";print_r($this->db->last_query());echo"</pre><hr>";
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";die;
		$return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows")->row_array();
		return $return_array;
	}

	public function get_task_user_list($task_type){

		$this->db->select('users.name, task_management.user_id as value, "" as selected');
		$this->db->join('users', 'users.user_id = task_management.user_id', 'left');
		$this->db->where('users.status=1 AND users.role IN (8, 10, 11, 14, 21) AND task_management.type = "'.$task_type.'"');
		$this->db->group_by('task_management.user_id');
		$res = $this->db->get('task_management')->result_array();

		// echo "<pre>";print_r($this->db->last_query());echo"</pre><hr>";die;
		return $res;
	}
}
?>