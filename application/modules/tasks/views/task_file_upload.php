<?php if(!empty($file_details)){ ?>
    <?php foreach ($file_details as $single_file_details) { ?>
        <div class="col-xl-12">
            <div class="kt-widget kt-widget--user-profile-1" style="padding-bottom: 0px;">
                <div class="kt-widget__body row">
                    <div class="kt-widget__items col-xl-12 row">
                        <div class="col-xl-12"><?php echo $single_file_details['version_tab_div']; ?></div>
                    </div>
                </div>
            </div>
        </div>	
        <div class="kt-uppy col-xl-6" id="<?php echo $single_file_details['kt_uppy_id']; ?>">
            <div class="kt-uppy__dashboard"></div>
            <div class="kt-uppy__progress"></div>
        </div>
        <div class="col-xl-6 <?php echo $single_file_details['image_path_div_class']; ?>">
            <?php echo $single_file_details['image_path']; ?>
        </div>
        <div class="col-xl-12">
            <hr>
        </div>

    <?php } ?>
<?php } ?>    
