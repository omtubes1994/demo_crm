jQuery(document).ready(function () {

    Task_Default_Js.init();
    dropdown_set_click_event();
    $('tbody.task_management_list_body').on('click', 'a.get_comment_query_chat', function(event){

        event.preventDefault();
        ajax_call_function({
            call_type: 'get_comment_history_and_add_new_form',
            id: this.getAttribute('id'),
        }, 'get_comment_history_and_add_new_form');
    });
    $('button.save_task_comment_add_form').click(function(event){

        event.preventDefault();
        const id = this.getAttribute('id');
        const comment = $('textarea#textarea_id_for_chat_message').val();
        if(typeof id !== 'undefined' && id.length !== 0 && comment.length !== 0){

            ajax_call_function({
                call_type: 'save_task_comment_add_form',
                id: id,
                comment: comment
            }, 'save_task_comment_add_form');
        }else{

            swal({
                title: "Comment cannot be empty!",
                text: 'Try again to reply, once you enter comment',
                icon: "info",
            });
        }
    });
    $('tbody.task_management_list_body').on('click', 'a.get_upload_document_tab_name', function(event){

        event.preventDefault();
        $('div.file_upload_div').addClass('kt-hidden');
        var id = this.getAttribute('id');
        var work_order_no = this.getAttribute('work_order_no');
        var type = this.getAttribute('type');

        if(typeof id !== 'undefined' && id.length !== 0){

            $('input#task_id_for_upload_document').val(id);
            $('input#work_order_no').val(work_order_no);
            $('input#type').val(type);

            const id_new = $('input#task_id_for_upload_document').val();
            const work_order_no_new = $('input#work_order_no').val();
            const type_new = $('input#type').val();

            if(id_new !== 0 && work_order_no_new !== 0 && type_new !== ''){

                ajax_call_function({
                    call_type: 'get_upload_document_tab_name',
                    task_type: type,
                }, 'get_upload_document_tab_name');
            }else{

                swal({
                    title: "Something went wrong. Contact CRM developer",
                    icon: "info",
                });
            }
            
        }else{

            swal({
                title: "Something went wrong. Contact CRM developer",
                icon: "info",
            });
        }
    });
    $('div.upload_tab').on('click', 'a.get_upload_document', function(event){

        event.preventDefault();
        $('div#task_management_document_loader').show();
        $('div.file_upload_div').addClass('kt-hidden');
        $('a.get_upload_document').removeClass('kt-widget__item--active');
        $(this).addClass('kt-widget__item--active');
        var document_name = this.getAttribute('document_name');
        if(typeof document_name !== 'undefined' && document_name.length !== 0){

            ajax_call_function({
                call_type: 'get_upload_document',
                task_id: $('input#task_id_for_upload_document').val(),
                task_type: $('input#type').val(),
                document_name: document_name,
            }, 'get_upload_document');
            
        }else{

            swal({
                title: "Something went wrong. Contact CRM developer",
                icon: "info",
            });
        }
        
    });
    $('a.change_task_management_list_tab').click(function(){

        $('a.change_task_management_list_tab').removeClass('active');
        $(this).addClass('active');
        get_task_management_list_details();
    });
    $('a.change_task_management_list_type').click(function(){

        $('a.change_task_management_list_type').removeClass('kt-widget__item--active');
        $(this).addClass('kt-widget__item--active');
        get_task_management_list_details();
    });

    $('a.change_task_management_list_type_production_wise').click(function(){

        $('a.change_task_management_list_type_production_wise').removeClass('kt-widget__item--active');
        $(this).addClass('kt-widget__item--active');
        get_task_management_list_details();
        $('div.production_status_wise_graph').css('display', 'block');
    });

    $('div.task_management_list_paggination').on('click', 'li.paggination_class', function(event){

        event.preventDefault();
        var limit = this.getAttribute('limit');
        var offset = this.getAttribute('offset');

        if(typeof limit !== 'undefined' && typeof offset !== 'undefined'){

            get_task_management_list_details(limit, offset);
        }else{

            swal({
                title: "Something went wrong. Contact CRM developer",
                icon: "info",
            });
        }
    });
    $('div.task_management_list_paggination').on('change', 'select.limit_change', function(event){

        event.preventDefault();
        var limit = $('select.limit_change').val();

        if(typeof limit !== 'undefined'){

            get_task_management_list_details(limit, 0);
        }else{

            swal({
                title: "Something went wrong. Contact CRM developer",
                icon: "info",
            });
        }
    });
    $('tbody.task_management_list_body').on('click', 'a.get_line_item_data', function(event){

        event.preventDefault();
        ajax_call_function({
            call_type: 'get_line_item_data',
            work_order_no: this.getAttribute('work_order_no'),
        }, 'get_line_item_data');
    });
    $('div.project_management_comment_inner').on('input', 'input#client_name', function () {

        var search_text = $('input#client_name').val();
        if (search_text.length <= 3) {

            toastr.info('Please Enter more than 3 character to update company name!');
            return false;
        }
        $('select[name="client_id"]').html('');
        $('div#task_management_list_loader').show();
        ajax_call_function({
            call_type: 'get_client_name_task',
            client_name: search_text,
        }, 'get_client_name_task');
    });
    $('a.add_task_management_search_filter').click(function(event){
        
        event.preventDefault();
        $('div.project_management_comment_box').css('display','block');
    });
    $('div.project_management_comment_box').on('click', 'a.filter_search', function () {
        get_task_management_list_details();
    });
    $('div.project_management_comment_box').on('click', 'a.reset_filter_search', function () {

        $('form#search_filter_form')[0].reset();
        get_task_management_list_details();
    });
    $('div.project_management_comment_box').on('click', 'a.close_filter_by', function () {
        
        $('div.project_management_comment_box').css('display','none');
    });
    $('div.file_upload_div').on('click', 'a.get_document', function(event){

        event.preventDefault();
        type = this.getAttribute('file_type');

        if(type == 'vendor' || type == 'client' || type == 'marking' || type == 'mtc' || type == 'invoice' || type == 'proforma_invoice' || type == 'pmi_photos' || type == 'dimension_photos' || type == 'marking_photos' || type == 'finishing_photos' || type == 'packing_and_dispatch' || type == 'reports_pdf'){
            
            $('a.'+type+'_document').removeClass('active');
            $(this).addClass('active');
            ajax_call_function({
                call_type: 'get_document_tab_wise',
                log_id: this.getAttribute('log_id'),
                file_type: type
            }, 'get_document_tab_wise');
        }
    });
    $('div.tab_vendor_div').on('click', 'a.get_document', function(event){

        event.preventDefault();
        type = this.getAttribute('file_type');

        if(type == 'vendor' || type == 'client'){

            if(type == 'vendor'){

                $('a.vendor_document').removeClass('active');
            }else if(type == 'client'){

                $('a.client_document').removeClass('active');
            }
            $(this).addClass('active');
            ajax_call_function({
                call_type: 'get_document_tab_wise',
                log_id: this.getAttribute('log_id'),
                file_type: type
            }, 'get_document_tab_wise');
        }
    });
    $('div.tab_client_div').on('click', 'a.get_document', function(event){

        event.preventDefault();
        type = this.getAttribute('file_type');

        if(type == 'vendor' || type == 'client'){

            if(type == 'vendor'){

                $('a.vendor_document').removeClass('active');
            }else if(type == 'client'){

                $('a.client_document').removeClass('active');
            }
            $(this).addClass('active');
            ajax_call_function({
                call_type: 'get_document_tab_wise',
                log_id: this.getAttribute('log_id'),
                file_type: type
            }, 'get_document_tab_wise');
        }
    });
    $('tbody.task_management_list_body').on('click', 'a.add_weight_dimension', function(event){

        event.preventDefault();
        ajax_call_function({
            call_type: 'get_weight_dimension',
            task_id: this.getAttribute('task_id'),
        }, 'get_weight_dimension');
    });
    $('button.save_weight_dimension').click(function(event){

        event.preventDefault();
        ajax_call_function({
            call_type: 'save_weight_dimension',
            task_id: this.getAttribute('task_id'),
            weight_dimension: $('#weight_dimension_text').val(),
        }, 'save_weight_dimension');
    });
    $('th.sorting_search').click(function(event){

        event.preventDefault();
        $('th.sorting_search').removeClass('sorting');
        $('th.sorting_search').removeClass('sorting_asc');
        $('th.sorting_search').removeClass('sorting_desc');
        $('th.sorting_search').removeClass('sorting_active');
        $('th.sorting_search').addClass('sorting');
        var sorting_value = $(this).attr('sorting_value');
        if(sorting_value == 'desc'){

            $(this).attr('sorting_value', 'asc');
            $(this).addClass('sorting_asc');
        }else{

            $(this).attr('sorting_value', 'desc');
            $(this).addClass('sorting_desc');
        }
        $(this).addClass('sorting_active');
        get_task_management_list_details();
    });
    $('tbody.task_management_list_body').on('click', 'a.get_all_file_upload_history_new', function(event){

        event.preventDefault();
        var id = this.getAttribute('id');
        var work_order_no = this.getAttribute('work_order_no');
        var task_type = this.getAttribute('task_type');

        if(typeof id !== 'undefined' && id.length !== 0){

            $('input#task_id_for_upload_document_new').val(id);
            $('input#work_order_no_new').val(work_order_no);
            $('input#type_new').val(task_type);

            const id_new = $('input#task_id_for_upload_document_new').val();
            const work_order_no_new = $('input#work_order_no_new').val();
            const type_new = $('input#type_new').val();

            if(id_new !== 0 && work_order_no_new !== 0 && type_new !== ''){

                $('div.upload_history_loader').show();
                $('#kt_modal_4_2_new').modal('show');
                ajax_call_function({call_type: 'get_all_file_upload_history_new', work_order_no: work_order_no_new, task_type: type_new, sub_task_type: $(this).attr('sub_task_type')},'get_all_file_upload_history_new'); 
            }else{

                swal({
                    title: "Something went wrong. Contact CRM developer",
                    icon: "info",
                });
            }
            
        }else{

            swal({
                title: "Something went wrong. Contact CRM developer",
                icon: "info",
            });
        }
    });
    $('div.main_tab_html, div.sub_tab_html').on('click', 'div.get_photos_history', function(){

        $('div.upload_history_loader').show();
        ajax_call_function({call_type: 'get_all_file_upload_history_new', work_order_no: $(this).attr('work_order_no'), task_type: $(this).attr('task_type'), sub_task_type: $(this).attr('sub_task_type')},'get_all_file_upload_history_new');
    });
    $('div.sidebar_photos').on('click', '.change_main_photos', function(){
        
        ajax_call_function({call_type: 'set_main_photos', file_name: $(this).attr('file_name'), main_photos_height: $(this).attr('main_photos_height'), type: $(this).attr('type'), sub_task_type: $(this).attr('sub_task_type')},'set_main_photos');
        $('.change_main_photos').removeClass('active');
        $(this).addClass('active');
    });
    $('div.sidebar_photos').on('click', 'button.download_btn', function () {

        ajax_call_function({ call_type: 'download_image', id: $(this).attr('image_id'), file_name: $(this).attr('file_name') }, 'download_image');
    });
    $('div.sidebar_photos').on('click', 'button.delete_btn', function () {

        swal({
			  title: "Are you sure?",
			  text: "Once deleted, you will not be able to recover this Document!",
			  icon: "warning",
			  buttons:  ["Cancel", "Delete"],
			  dangerMode: true,	
			})
		.then((willDelete) => {
		  	if (willDelete) {
				ajax_call_function({ call_type: 'delete_image', id: $(this).attr('image_id'), file_name: $(this).attr('file_name') }, 'delete_image');
	  		} else {
			    swal({
		    		title: "Document is not deleted",
		      		icon: "info",
		    	});
		  	}
		});
    });

    $('tbody.task_management_list_body').on('click', 'button.upload_pdf', function(){

        ajax_call_function({call_type: 'get_upload_history', query_number: $(this).attr('query_number')},'get_upload_history', "<?php echo base_url('query/ajax_function'); ?>");
    });

    $('tbody.task_management_list_body').on('click', 'a.mtc_download', function () {
        ajax_call_function({ call_type: 'get_mtc_document_history', work_order_no: $(this).attr('work_order_no'), type: $(this).attr('type') }, 'get_mtc_document_history');
    });

    $('tbody.task_management_list_body').on('click', 'a.marking_download', function () {
        ajax_call_function({ call_type: 'get_marking_document_history', work_order_no: $(this).attr('work_order_no'), type: $(this).attr('type') }, 'get_marking_document_history');
    });

    $('tbody.task_management_list_body').on('click', 'a.technical_document_history', function () {
        ajax_call_function({ call_type: 'get_technical_document_history', work_order_no: $(this).attr('work_order_no')}, 'get_technical_document_history');
    });

    $('tbody.task_management_list_body').on('click', 'a.add_revised_marking_comment', function () {
        ajax_call_function({ call_type: 'get_revised_marking_comment_history', work_order_no: $(this).attr('work_order_no'), type: $(this).attr('type') }, 'get_revised_marking_comment_history');
    });

    $('form#marking_revised_information').on('click', 'button.save_revised_marking', function () {
        ajax_call_function({ call_type: 'save_revised_marking_comment', work_order_no: $(this).val(), form_data: $('form#marking_revised_information').serializeArray() }, 'save_revised_marking_comment');
    });
});


function ajax_call_function(data, call_type, url = "<?php echo base_url('tasks/ajax_function'); ?>") {
    $.ajax({
        type: 'POST',
        data: data,
        url: url,
        dataType: 'JSON',
        success: function (res) {
            toastr.clear();
            if (res.status == 'successful') {
                switch (call_type) {

                    case 'change_task_status':
                    case 'change_assigned_to_status':

                        swal({
                            title: "Changes Done",
                            icon: "success",
                        });
                        get_task_management_list_details($('input#task_management_limit').val(), $('input#task_management_offset').val());
                    break;
                    case 'get_comment_history_and_add_new_form':

                        $('div.div_chat_history').html('').html(res.chat_history);
                        $('em.header_1').html('').html('Work Order No #');
                        $('span.header_2').html('').html(res.work_order_no);
                        $('button.save_task_comment_add_form').attr('id', res.id);
                        $('div#kt_chat_modal').modal('show');
                        setTimeout(function(){

                            $('.kt-scroll').scrollTop($('.kt-scroll')[0].scrollHeight);
                        }, 300);
                    break;
                    case 'save_task_comment_add_form':
                        
                        swal({
                            title: "Comment is added",
                            icon: "success",
                        });
                        $('div#kt_chat_modal').modal('hide');
                        get_task_management_list_details($('input#task_management_limit').val(), $('input#task_management_offset').val());
                        ajax_call_function({
                            call_type: 'get_comment_history_and_add_new_form',
                            id: data.id,
                        }, 'get_comment_history_and_add_new_form');
                        $('textarea#textarea_id_for_chat_message').val('    ');
                    break;
                    case 'get_task_management_list_details':

                        if(data.tab_name != 'recent_activities'){

                            $('tbody.task_management_list_body').html('').html(res.task_management_list_body);
                            $('div.project_management_comment_inner').html('').html(res.task_management_search_filter);
                            $('div.task_management_list_paggination').html('').html(res.task_management_list_paggination);
                            // console.log(res.total);
                            if (res.total === 0){

                                $('div.production_status_wise_graph').css('display', 'none');
                            }else{
                                task_assigned_highchart(res.total, res.task_highchart_category, res.task_highchart_data);
                            }
                            dropdown_set_click_event();
                        }else{

                            $('div.recent_activities_listing').html('').html(res.task_management_recent_activities);
                        }
                        $('div#task_management_list_loader').hide();
                    break;
                    case 'get_upload_document_tab_name':
                        
                        $('div.upload_tab').html('').html(res.upload_tab_html);
                        $('div#get_upload_history_and_new_add_form').modal('show');
                    break;
                    case 'get_upload_document':

                        $('div.file_upload_div').html('').html(res.file_upload_div);
                        Task_Document_Js.init(data.document_name);
                        $('div.file_upload_div').removeClass('kt-hidden');
                        $('div#task_management_document_loader').hide();
                    break;
                    case 'get_line_item_data':
                        
                        $('tbody.quotation_line_item_tbody').html('').html(res.quotations_line_item_body);
                    break;
                    case 'get_document_tab_wise':

                        $('div.'+data.file_type).html('').html(res.image_path);
                    break;
                    case 'get_weight_dimension':
                        
                        $('div#add_weight_dimension').modal('show');
                        $('button.save_weight_dimension').attr('task_id', data.task_id);
                        $('#weight_dimension_text').val(res.weight_dimension);
                        $('#weight_dimension_text').css('height', '300px');
                        autosize($('#weight_dimension_text'));
                    break;
                    case 'save_weight_dimension':
                        
                        swal("Weight And Dimension Saved")
                    break;
                    case 'get_all_file_upload_history_new':

                        console.log(res.main_photos_html);
                        $('span.work_order_no').html('').html(res.work_order_no);
                        $('div.main_tab_html').html('').html(res.main_tab_html);
                        $('div.sidebar_photos').html('').html(res.sidebar_photos_html);
                        $('div.main_photos').html('').html(res.main_photos_html);
                        if(data.sub_task_type != 'all'){

                            Task_Document_Js.init(data.sub_task_type);
                        }
                        $('div.upload_history_loader').hide();
                    break;
                    case 'set_main_photos':
                        
                        $('div.main_photos').html('').html(res.main_photos_html);
                        if(data.type == 'upload_document'){

                            Task_Document_Js.init(data.sub_task_type);
                        }
                    break;
                    case 'delete_image':
                        swal({
                            title: "Image Delete Successfuly",
                            icon: "success",
                        });
                    break;
                    case 'download_image':
                        toastr.clear();
                        toastr.info('Download Successfully!');
                    break;
                    case 'get_upload_history':

                        $('div#pdf_upload_history_mtc').html('').html(res.pdf_upload_history_mtc);
                        $('div#pdf_upload_history_drawing').html('').html(res.pdf_upload_history_drawing);
                    break;
                    case 'get_mtc_document_history':
                    case 'get_marking_document_history':

                        $('div#document_history_list').html('').html(res.document_history);
                    break;
                    case 'get_technical_document_history':

                        $('div#technical_document_history_list').html('').html(res.technical_document_history);
                    break;
                    case 'get_revised_marking_comment_history':

                        $('form#marking_revised_information').html('').html(res.revised_marking_comment_history);
                        $('button[id=task_id]').val(data.work_order_no);
                    break;
                    case 'save_revised_marking_comment':

                        toastr.clear();
                        toastr.success("Comment is Added Successfully!");
                    break;

                    case 'get_client_name_task':

                        $('div.project_management_comment_inner select[name="client_id"]').html(res.client_list_html);
                        $('div#task_management_list_loader').hide();
                        toastr.info('Company Name is Updated!');
                    break;
                }
            }
        },
        beforeSend: function (response) {

            switch (call_type) {

            }
        }
    });
};
var Task_Default_Js = function () {

    return {
        // public functions
        init: function () {

            $('.select_picker').selectpicker();
        }
    };
}();
var Task_Document_Js = function () {

    const Tus = Uppy.Tus;
    const ProgressBar = Uppy.ProgressBar;
    const StatusBar = Uppy.StatusBar;
    const FileInput = Uppy.FileInput;
    const Informer = Uppy.Informer;
    const Dashboard = Uppy.Dashboard;
    const Dropbox = Uppy.Dropbox;
    const GoogleDrive = Uppy.GoogleDrive;
    const Instagram = Uppy.Instagram;
    const Webcam = Uppy.Webcam;

    // Private functions
    var drawing_vendor = function(){

        var id = '#drawing_vendor';
        var options = {
            proudlyDisplayPoweredByUppy: false,
            target: id,
            inline: true,
            replaceTargetContent: true,
            showProgressDetails: true,
            note: 'No filetype restrictions.',
            height: 470,
            metaFields: [
                { id: 'name', name: 'Name', placeholder: 'file name' },
                { id: 'caption', name: 'Caption', placeholder: 'describe what the image is about' }
            ],
            browserBackButtonClose: true
        };

        var uppyDashboard = Uppy.Core({ 
            autoProceed: true,
            restrictions: {
                maxFileSize: 20000000, // 10mb
                maxNumberOfFiles: 101,
                minNumberOfFiles: 1
            } 
        });

        uppyDashboard.use(Dashboard, options);  
		uppyDashboard.use(Tus, { endpoint: 'https://master.tus.io/files/' });
		// uppyDashboard.use(GoogleDrive, { target: Dashboard, companionUrl: 'https://companion.uppy.io' });
		uppyDashboard.use(Webcam, { target: Dashboard });

        // Listen to when files are added/uploaded
        uppyDashboard.on('complete', function(result) {

            console.log(result)
            if (result.successful.length > 0) {

                console.log(result.successful.length);
                for (let index = 0; index < result.successful.length; index++) {

                    var file = result.successful[index]; // Assuming only one file is uploaded
                    var formData = new FormData();
                    formData.append('call_type', 'upload_task_document');
                    formData.append('file', file.data);
                    formData.append('task_id', $('input#task_id_for_upload_document_new').val());
                    formData.append('work_order_no', $('input#work_order_no_new').val());
                    formData.append('type', $('input#type_new').val());
                    formData.append('file_type', 'vendor');

                    // Send the file to your server via AJAX
                    $.ajax({
                        url: '<?php echo base_url("tasks/ajax_function"); ?>',
                        type: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                        dataType: 'JSON',
                        success: function(response) {
                            // Handle success response from the server
                            console.log(response);
                        },
                        error: function(error) {
                            // Handle error response from the server
                            console.error(error);
                        }
                    });
                }
                swal({
                    title: "Vendor Photos Uploaded",
                    text: "Successfully",
                    icon: "info",
                });
            }
        });
    };
    var drawing_client = function(){

        var id = '#drawing_client';
        var options = {
            proudlyDisplayPoweredByUppy: false,
            target: id,
            inline: true,
            replaceTargetContent: true,
            showProgressDetails: true,
            note: 'No filetype restrictions.',
            height: 470,
            metaFields: [
                { id: 'name', name: 'Name', placeholder: 'file name' },
                { id: 'caption', name: 'Caption', placeholder: 'describe what the image is about' }
            ],
            browserBackButtonClose: true
        };

        var uppyDashboard = Uppy.Core({ 
            autoProceed: true,
            restrictions: {
                maxFileSize: 20000000, // 10mb
                maxNumberOfFiles: 101,
                minNumberOfFiles: 1
            } 
        });

        uppyDashboard.use(Dashboard, options);  
		uppyDashboard.use(Tus, { endpoint: 'https://master.tus.io/files/' });
		// uppyDashboard.use(GoogleDrive, { target: Dashboard, companionUrl: 'https://companion.uppy.io' });
		uppyDashboard.use(Webcam, { target: Dashboard });

        // Listen to when files are added/uploaded
        uppyDashboard.on('complete', function(result) {

            console.log(result)
            if (result.successful.length > 0) {

                console.log(result.successful.length);
                for (let index = 0; index < result.successful.length; index++) {

                    var file = result.successful[index]; // Assuming only one file is uploaded
                    var formData = new FormData();
                    formData.append('call_type', 'upload_task_document');
                    formData.append('file', file.data);
                    formData.append('task_id', $('input#task_id_for_upload_document_new').val());
                    formData.append('work_order_no', $('input#work_order_no_new').val());
                    formData.append('type', $('input#type_new').val());
                    formData.append('file_type', 'client');

                    // Send the file to your server via AJAX
                    $.ajax({
                        url: '<?php echo base_url("tasks/ajax_function"); ?>',
                        type: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                        dataType: 'JSON',
                        success: function(response) {
                            // Handle success response from the server
                            console.log(response);
                        },
                        error: function(error) {
                            // Handle error response from the server
                            console.error(error);
                        }
                    });
                }
                swal({
                    title: "Client Photos Uploaded",
                    text: "Successfully",
                    icon: "info",
                });    
            }
        });
    };
    var marking = function(){

        var id = '#marking';
        var options = {
            proudlyDisplayPoweredByUppy: false,
            target: id,
            inline: true,
            replaceTargetContent: true,
            showProgressDetails: true,
            note: 'No filetype restrictions.',
            height: 470,
            metaFields: [
                { id: 'name', name: 'Name', placeholder: 'file name' },
                { id: 'caption', name: 'Caption', placeholder: 'describe what the image is about' }
            ],
            browserBackButtonClose: true
        };

        var uppyDashboard = Uppy.Core({ 
            autoProceed: true,
            restrictions: {
                maxFileSize: 20000000, // 10mb
                maxNumberOfFiles: 101,
                minNumberOfFiles: 1
            } 
        });

        uppyDashboard.use(Dashboard, options);  
		uppyDashboard.use(Tus, { endpoint: 'https://master.tus.io/files/' });
		// uppyDashboard.use(GoogleDrive, { target: Dashboard, companionUrl: 'https://companion.uppy.io' });
		uppyDashboard.use(Webcam, { target: Dashboard });

        // Listen to when files are added/uploaded
        uppyDashboard.on('complete', function(result) {

            console.log(result)
            if (result.successful.length > 0) {

                console.log(result.successful.length);
                for (let index = 0; index < result.successful.length; index++) {

                    var file = result.successful[index]; // Assuming only one file is uploaded
                    var formData = new FormData();
                    formData.append('call_type', 'upload_task_document');
                    formData.append('file', file.data);
                    formData.append('task_id', $('input#task_id_for_upload_document_new').val());
                    formData.append('work_order_no', $('input#work_order_no_new').val());
                    formData.append('type', $('input#type_new').val());
                    formData.append('file_type', 'marking');

                    // Send the file to your server via AJAX
                    $.ajax({
                        url: '<?php echo base_url("tasks/ajax_function"); ?>',
                        type: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                        dataType: 'JSON',
                        success: function(response) {
                            // Handle success response from the server
                            console.log(response);
                        },
                        error: function(error) {
                            // Handle error response from the server
                            console.error(error);
                        }
                    });
                }
                swal({
                    title: "Marking Photos Uploaded",
                    text: "Successfully",
                    icon: "info",
                });
            }
        });
    };
    var mtc = function(){

        var id = '#mtc';
        var options = {
            proudlyDisplayPoweredByUppy: false,
            target: id,
            inline: true,
            replaceTargetContent: true,
            showProgressDetails: true,
            note: 'No filetype restrictions.',
            height: 470,
            metaFields: [
                { id: 'name', name: 'Name', placeholder: 'file name' },
                { id: 'caption', name: 'Caption', placeholder: 'describe what the image is about' }
            ],
            browserBackButtonClose: true
        };

        var uppyDashboard = Uppy.Core({ 
            autoProceed: true,
            restrictions: {
                maxFileSize: 20000000, // 10mb
                maxNumberOfFiles: 101,
                minNumberOfFiles: 1
            } 
        });

        uppyDashboard.use(Dashboard, options);  
		uppyDashboard.use(Tus, { endpoint: 'https://master.tus.io/files/' });
		// uppyDashboard.use(GoogleDrive, { target: Dashboard, companionUrl: 'https://companion.uppy.io' });
		uppyDashboard.use(Webcam, { target: Dashboard });

        // Listen to when files are added/uploaded
        uppyDashboard.on('complete', function(result) {

            console.log(result)
            if (result.successful.length > 0) {

                console.log(result.successful.length);
                for (let index = 0; index < result.successful.length; index++) {

                    var file = result.successful[index]; // Assuming only one file is uploaded
                    var formData = new FormData();
                    formData.append('call_type', 'upload_task_document');
                    formData.append('file', file.data);
                    formData.append('task_id', $('input#task_id_for_upload_document_new').val());
                    formData.append('work_order_no', $('input#work_order_no_new').val());
                    formData.append('type', $('input#type_new').val());
                    formData.append('file_type', 'mtc');

                    // Send the file to your server via AJAX
                    $.ajax({
                        url: '<?php echo base_url("tasks/ajax_function"); ?>',
                        type: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                        dataType: 'JSON',
                        success: function(response) {
                            // Handle success response from the server
                            console.log(response);
                        },
                        error: function(error) {
                            // Handle error response from the server
                            console.error(error);
                        }
                    });
                }
                swal({
                    title: "MTC Photos Uploaded",
                    text: "Successfully",
                    icon: "info",
                });
            }
        });
    };
    var invoice = function(){

        var id = '#invoice';
        var options = {
            proudlyDisplayPoweredByUppy: false,
            target: id,
            inline: true,
            replaceTargetContent: true,
            showProgressDetails: true,
            note: 'No filetype restrictions.',
            height: 470,
            metaFields: [
                { id: 'name', name: 'Name', placeholder: 'file name' },
                { id: 'caption', name: 'Caption', placeholder: 'describe what the image is about' }
            ],
            browserBackButtonClose: true
        };

        var uppyDashboard = Uppy.Core({ 
            autoProceed: true,
            restrictions: {
                maxFileSize: 20000000, // 10mb
                maxNumberOfFiles: 101,
                minNumberOfFiles: 1
            } 
        });

        uppyDashboard.use(Dashboard, options);  
		uppyDashboard.use(Tus, { endpoint: 'https://master.tus.io/files/' });
		// uppyDashboard.use(GoogleDrive, { target: Dashboard, companionUrl: 'https://companion.uppy.io' });
		uppyDashboard.use(Webcam, { target: Dashboard });

        // Listen to when files are added/uploaded
        uppyDashboard.on('complete', function(result) {

            console.log(result)
            if (result.successful.length > 0) {

                console.log(result.successful.length);
                for (let index = 0; index < result.successful.length; index++) {

                    var file = result.successful[index]; // Assuming only one file is uploaded
                    var formData = new FormData();
                    formData.append('call_type', 'upload_task_document');
                    formData.append('file', file.data);
                    formData.append('task_id', $('input#task_id_for_upload_document_new').val());
                    formData.append('work_order_no', $('input#work_order_no_new').val());
                    formData.append('type', $('input#type_new').val());
                    formData.append('file_type', 'invoice');

                    // Send the file to your server via AJAX
                    $.ajax({
                        url: '<?php echo base_url("tasks/ajax_function"); ?>',
                        type: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                        dataType: 'JSON',
                        success: function(response) {
                            // Handle success response from the server
                            console.log(response);
                        },
                        error: function(error) {
                            // Handle error response from the server
                            console.error(error);
                        }
                    });
                }
                swal({
                    title: "Invoice Photos Uploaded",
                    text: "Successfully",
                    icon: "info",
                });
            }
        });
    };
    var proforma_invoice = function(){

        var id = '#proforma_invoice';
        var options = {
            proudlyDisplayPoweredByUppy: false,
            target: id,
            inline: true,
            replaceTargetContent: true,
            showProgressDetails: true,
            note: 'No filetype restrictions.',
            height: 470,
            metaFields: [
                { id: 'name', name: 'Name', placeholder: 'file name' },
                { id: 'caption', name: 'Caption', placeholder: 'describe what the image is about' }
            ],
            browserBackButtonClose: true
        };

        var uppyDashboard = Uppy.Core({ 
            autoProceed: true,
            restrictions: {
                maxFileSize: 20000000, // 10mb
                maxNumberOfFiles: 101,
                minNumberOfFiles: 1
            } 
        });

        uppyDashboard.use(Dashboard, options);  
		uppyDashboard.use(Tus, { endpoint: 'https://master.tus.io/files/' });
		// uppyDashboard.use(GoogleDrive, { target: Dashboard, companionUrl: 'https://companion.uppy.io' });
		uppyDashboard.use(Webcam, { target: Dashboard });

        // Listen to when files are added/uploaded
        uppyDashboard.on('complete', function(result) {

            console.log(result)
            if (result.successful.length > 0) {

                console.log(result.successful.length);
                for (let index = 0; index < result.successful.length; index++) {
                    
                    var file = result.successful[index]; // Assuming only one file is uploaded
                    var formData = new FormData();
                    formData.append('call_type', 'upload_task_document');
                    formData.append('file', file.data);
                    formData.append('task_id', $('input#task_id_for_upload_document_new').val());
                    formData.append('work_order_no', $('input#work_order_no_new').val());
                    formData.append('type', $('input#type_new').val());
                    formData.append('file_type', 'proforma_invoice');

                    // Send the file to your server via AJAX
                    $.ajax({
                        url: '<?php echo base_url("tasks/ajax_function"); ?>',
                        type: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                        dataType: 'JSON',
                        success: function(response) {
                            // Handle success response from the server
                            console.log(response);
                        },
                        error: function(error) {
                            // Handle error response from the server
                            console.error(error);
                        }
                    });
                }
                swal({
                    title: "Proforma Invoice Photos Uploaded",
                    text: "Successfully",
                    icon: "info",
                });
            }
        });
    };
    var pmi_photos = function(){

        var id = '#pmi_photos';
        var options = {
            proudlyDisplayPoweredByUppy: false,
            target: id,
            inline: true,
            replaceTargetContent: true,
            showProgressDetails: true,
            note: 'No filetype restrictions.',
            height: 470,
            metaFields: [
                { id: 'name', name: 'Name', placeholder: 'file name' },
                { id: 'caption', name: 'Caption', placeholder: 'describe what the image is about' }
            ],
            browserBackButtonClose: true
        };

        var uppyDashboard = Uppy.Core({ 
            autoProceed: true,
            restrictions: {
                maxFileSize: 20000000, // 10mb
                maxNumberOfFiles: 101,
                minNumberOfFiles: 1
            } 
        });

        uppyDashboard.use(Dashboard, options);  
		uppyDashboard.use(Tus, { endpoint: 'https://master.tus.io/files/' });
		// uppyDashboard.use(GoogleDrive, { target: Dashboard, companionUrl: 'https://companion.uppy.io' });
		uppyDashboard.use(Webcam, { target: Dashboard });

        // Listen to when files are added/uploaded
        uppyDashboard.on('complete', function(result) {

            console.log(result)
            if (result.successful.length > 0) {

                console.log(result.successful.length);
                for (let index = 0; index < result.successful.length; index++) {
                
                    var file = result.successful[index]; // Assuming only one file is uploaded
                    var formData = new FormData();
                    formData.append('call_type', 'upload_task_document');
                    formData.append('file', file.data);
                    formData.append('task_id', $('input#task_id_for_upload_document_new').val());
                    formData.append('work_order_no', $('input#work_order_no_new').val());
                    formData.append('type', $('input#type_new').val());
                    formData.append('file_type', 'pmi_photos');

                    // Send the file to your server via AJAX
                    $.ajax({
                        url: '<?php echo base_url("tasks/ajax_function"); ?>',
                        type: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                        dataType: 'JSON',
                        success: function(response) {
                            // Handle success response from the server
                            console.log(response);
                        },
                        error: function(error) {
                            // Handle error response from the server
                            console.error(error);
                        }
                    });
                }
                swal({
                    title: "PMI Photos Uploaded",
                    text: "Successfully",
                    icon: "info",
                });
            }
        });
    };
    var dimension_photos = function(){

        var id = '#dimension_photos';
        var options = {
            proudlyDisplayPoweredByUppy: false,
            target: id,
            inline: true,
            replaceTargetContent: true,
            showProgressDetails: true,
            note: 'No filetype restrictions.',
            height: 470,
            metaFields: [
                { id: 'name', name: 'Name', placeholder: 'file name' },
                { id: 'caption', name: 'Caption', placeholder: 'describe what the image is about' }
            ],
            browserBackButtonClose: true
        };

        var uppyDashboard = Uppy.Core({ 
            autoProceed: true,
            restrictions: {
                maxFileSize: 20000000, // 10mb
                maxNumberOfFiles: 101,
                minNumberOfFiles: 1
            } 
        });

        uppyDashboard.use(Dashboard, options);  
		uppyDashboard.use(Tus, { endpoint: 'https://master.tus.io/files/' });
		// uppyDashboard.use(GoogleDrive, { target: Dashboard, companionUrl: 'https://companion.uppy.io' });
		uppyDashboard.use(Webcam, { target: Dashboard });

        // Listen to when files are added/uploaded
        uppyDashboard.on('complete', function(result) {

            console.log(result)
            if (result.successful.length > 0) {

                console.log(result.successful.length);
                for (let index = 0; index < result.successful.length; index++) {

                    var file = result.successful[index]; // Assuming only one file is uploaded
                    var formData = new FormData();
                    formData.append('call_type', 'upload_task_document');
                    formData.append('file', file.data);
                    formData.append('task_id', $('input#task_id_for_upload_document_new').val());
                    formData.append('work_order_no', $('input#work_order_no_new').val());
                    formData.append('type', $('input#type_new').val());
                    formData.append('file_type', 'dimension_photos');

                    // Send the file to your server via AJAX
                    $.ajax({
                        url: '<?php echo base_url("tasks/ajax_function"); ?>',
                        type: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                        dataType: 'JSON',
                        success: function(response) {
                            // Handle success response from the server
                            console.log(response);
                        },
                        error: function(error) {
                            // Handle error response from the server
                            console.error(error);
                        }
                    });
                }
                swal({
                    title: "Dimension Photos Uploaded",
                    text: "Successfully",
                    icon: "info",
                });
            }
        });
    };
    var marking_photos = function(){

        var id = '#marking_photos';
        var options = {
            proudlyDisplayPoweredByUppy: false,
            target: id,
            inline: true,
            replaceTargetContent: true,
            showProgressDetails: true,
            note: 'No filetype restrictions.',
            height: 470,
            metaFields: [
                { id: 'name', name: 'Name', placeholder: 'file name' },
                { id: 'caption', name: 'Caption', placeholder: 'describe what the image is about' }
            ],
            browserBackButtonClose: true
        };

        var uppyDashboard = Uppy.Core({ 
            autoProceed: true,
            restrictions: {
                maxFileSize: 20000000, // 10mb
                maxNumberOfFiles: 101,
                minNumberOfFiles: 1
            } 
        });

        uppyDashboard.use(Dashboard, options);  
		uppyDashboard.use(Tus, { endpoint: 'https://master.tus.io/files/' });
		// uppyDashboard.use(GoogleDrive, { target: Dashboard, companionUrl: 'https://companion.uppy.io' });
		uppyDashboard.use(Webcam, { target: Dashboard });

        // Listen to when files are added/uploaded
        uppyDashboard.on('complete', function(result) {

            console.log(result)
            if (result.successful.length > 0) {

                console.log(result.successful.length);
                for (let index = 0; index < result.successful.length; index++) {

                    var file = result.successful[index]; // Assuming only one file is uploaded
                    var formData = new FormData();
                    formData.append('call_type', 'upload_task_document');
                    formData.append('file', file.data);
                    formData.append('task_id', $('input#task_id_for_upload_document_new').val());
                    formData.append('work_order_no', $('input#work_order_no_new').val());
                    formData.append('type', $('input#type_new').val());
                    formData.append('file_type', 'marking_photos');

                    // Send the file to your server via AJAX
                    $.ajax({
                        url: '<?php echo base_url("tasks/ajax_function"); ?>',
                        type: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                        dataType: 'JSON',
                        success: function(response) {
                            // Handle success response from the server
                            console.log(response);
                        },
                        error: function(error) {
                            // Handle error response from the server
                            console.error(error);
                        }
                    });
                }
                swal({
                    title: "Marking Photos Uploaded",
                    text: "Successfully",
                    icon: "info",
                });
            }
        });
    };
    var finishing_photos = function(){

        var id = '#finishing_photos';
        var options = {
            proudlyDisplayPoweredByUppy: false,
            target: id,
            inline: true,
            replaceTargetContent: true,
            showProgressDetails: true,
            note: 'No filetype restrictions.',
            height: 470,
            metaFields: [
                { id: 'name', name: 'Name', placeholder: 'file name' },
                { id: 'caption', name: 'Caption', placeholder: 'describe what the image is about' }
            ],
            browserBackButtonClose: true
        };

        var uppyDashboard = Uppy.Core({ 
            autoProceed: true,
            restrictions: {
                maxFileSize: 20000000, // 10mb
                maxNumberOfFiles: 101,
                minNumberOfFiles: 1
            } 
        });

        uppyDashboard.use(Dashboard, options);  
		uppyDashboard.use(Tus, { endpoint: 'https://master.tus.io/files/' });
		// uppyDashboard.use(GoogleDrive, { target: Dashboard, companionUrl: 'https://companion.uppy.io' });
		uppyDashboard.use(Webcam, { target: Dashboard });

        // Listen to when files are added/uploaded
        uppyDashboard.on('complete', function(result) {

            console.log(result)
            if (result.successful.length > 0) {

                console.log(result.successful.length);
                for (let index = 0; index < result.successful.length; index++) {

                    var file = result.successful[index]; // Assuming only one file is uploaded
                    var formData = new FormData();
                    formData.append('call_type', 'upload_task_document');
                    formData.append('file', file.data);
                    formData.append('task_id', $('input#task_id_for_upload_document_new').val());
                    formData.append('work_order_no', $('input#work_order_no_new').val());
                    formData.append('type', $('input#type_new').val());
                    formData.append('file_type', 'finishing_photos');

                    // Send the file to your server via AJAX
                    $.ajax({
                        url: '<?php echo base_url("tasks/ajax_function"); ?>',
                        type: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                        dataType: 'JSON',
                        success: function(response) {
                            // Handle success response from the server
                            console.log(response);
                        },
                        error: function(error) {
                            // Handle error response from the server
                            console.error(error);
                        }
                    });
                }
                swal({
                    title: "Finishing Photos Uploaded",
                    text: "Successfully",
                    icon: "info",
                });
            }
        });
    };
    var packing_and_dispatch = function(){

        var id = '#packing_and_dispatch';
        var options = {
            proudlyDisplayPoweredByUppy: false,
            target: id,
            inline: true,
            replaceTargetContent: true,
            showProgressDetails: true,
            note: 'No filetype restrictions.',
            height: 470,
            metaFields: [
                { id: 'name', name: 'Name', placeholder: 'file name' },
                { id: 'caption', name: 'Caption', placeholder: 'describe what the image is about' }
            ],
            browserBackButtonClose: true
        };

        var uppyDashboard = Uppy.Core({ 
            autoProceed: true,
            restrictions: {
                maxFileSize: 20000000, // 10mb
                maxNumberOfFiles: 101,
                minNumberOfFiles: 1
            } 
        });

        uppyDashboard.use(Dashboard, options);  
		uppyDashboard.use(Tus, { endpoint: 'https://master.tus.io/files/' });
		// uppyDashboard.use(GoogleDrive, { target: Dashboard, companionUrl: 'https://companion.uppy.io' });
		uppyDashboard.use(Webcam, { target: Dashboard });

        // Listen to when files are added/uploaded
        uppyDashboard.on('complete', function(result) {

            console.log(result)
            if (result.successful.length > 0) {

                console.log(result.successful.length);
                for (let index = 0; index < result.successful.length; index++) {

                    var file = result.successful[index]; // Assuming only one file is uploaded
                    var formData = new FormData();
                    formData.append('call_type', 'upload_task_document');
                    formData.append('file', file.data);
                    formData.append('task_id', $('input#task_id_for_upload_document_new').val());
                    formData.append('work_order_no', $('input#work_order_no_new').val());
                    formData.append('type', $('input#type_new').val());
                    formData.append('file_type', 'packing_and_dispatch');

                    // Send the file to your server via AJAX
                    $.ajax({
                        url: '<?php echo base_url("tasks/ajax_function"); ?>',
                        type: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                        dataType: 'JSON',
                        success: function(response) {
                            // Handle success response from the server
                            console.log(response);
                        },
                        error: function(error) {
                            // Handle error response from the server
                            console.error(error);
                        }
                    });
                }
                swal({
                    title: "Packing & Dispatch Photos Uploaded",
                    text: "Successfully",
                    icon: "info",
                });
            }
        });
    };
    var reports_pdf = function(){

        var id = '#reports_pdf';
        var options = {
            proudlyDisplayPoweredByUppy: false,
            target: id,
            inline: true,
            replaceTargetContent: true,
            showProgressDetails: true,
            note: 'No filetype restrictions.',
            height: 470,
            metaFields: [
                { id: 'name', name: 'Name', placeholder: 'file name' },
                { id: 'caption', name: 'Caption', placeholder: 'describe what the image is about' }
            ],
            browserBackButtonClose: true
        };

        var uppyDashboard = Uppy.Core({ 
            autoProceed: true,
            restrictions: {
                maxFileSize: 20000000, // 10mb
                maxNumberOfFiles: 101,
                minNumberOfFiles: 1
            } 
        });

        uppyDashboard.use(Dashboard, options);  
		uppyDashboard.use(Tus, { endpoint: 'https://master.tus.io/files/' });
		// uppyDashboard.use(GoogleDrive, { target: Dashboard, companionUrl: 'https://companion.uppy.io' });
		uppyDashboard.use(Webcam, { target: Dashboard });

        // Listen to when files are added/uploaded
        uppyDashboard.on('complete', function(result) {
            
            console.log(result)
            if (result.successful.length > 0) {

                console.log(result.successful.length);
                for (let index = 0; index < result.successful.length; index++) {

                    var file = result.successful[index]; // Assuming only one file is uploaded
                    var formData = new FormData();
                    formData.append('call_type', 'upload_task_document');
                    formData.append('file', file.data);
                    formData.append('task_id', $('input#task_id_for_upload_document_new').val());
                    formData.append('work_order_no', $('input#work_order_no_new').val());
                    formData.append('type', $('input#type_new').val());
                    formData.append('file_type', 'reports_pdf');

                    // Send the file to your server via AJAX
                    $.ajax({
                        url: '<?php echo base_url("tasks/ajax_function"); ?>',
                        type: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                        dataType: 'JSON',
                        success: function(response) {
                            // Handle success response from the server
                            console.log(response);
                        },
                        error: function(error) {
                            // Handle error response from the server
                            console.error(error);
                        }
                    });
                }
                swal({
                    title: "Reports PDF Uploaded",
                    text: "Successfully",
                    icon: "info",
                });
            }
        });
    };
    return {
        // public functions
        init: function (document_type) {

            console.log(document_type);
            switch (document_type) {
                case 'vendor':
                    
                    drawing_vendor();
                break;
                case 'client':

                    drawing_client();
                break;
                case 'marking':

                    marking();
                break;
                case 'mtc':

                    mtc();
                break;
                case 'invoice':

                    invoice();
                break;
                case 'proforma_invoice':

                    proforma_invoice();
                break;
                case 'pmi_photos':

                    pmi_photos();
                break;
                case 'dimension_photos':

                    dimension_photos();
                break;
                case 'marking_photos':

                    marking_photos();
                break;
                case 'finishing_photos':

                    finishing_photos();
                break;
                case 'packing_and_dispatch':

                    packing_and_dispatch();
                break;
                case 'reports_pdf':

                    reports_pdf();
                break;                
            }
        }
    };
}();

function get_task_management_list_details(limit = 10, offset = 0){

    $('div.table_listing_div').hide();
    $('div.recent_activities_div').hide();
    if($('a.change_task_management_list_tab.active').attr('tab_name') != 'recent_activities'){

        $('div.table_listing_div').show();
    }else{

        $('div.recent_activities_div').show();
    }
    var data = {
        call_type: 'get_task_management_list_details',
        type: $('a.change_task_management_list_type.kt-widget__item--active').attr('type_name'),
        tab_name: $('a.change_task_management_list_tab.active').attr('tab_name'),
        production_tab: $('a.change_task_management_list_type_production_wise.kt-widget__item--active').attr('production_tab'),
        limit: limit,
        offset: offset,
        order_by_name: $('th.sorting_search.sorting_active').attr('sorting_name'),
        order_by_value: $('th.sorting_search.sorting_active').attr('sorting_value'),
        search_filter_form: $('form#search_filter_form').serializeArray()
    }
    $('div#task_management_list_loader').show();
    ajax_call_function(data, 'get_task_management_list_details');
}

function dropdown_set_click_event(){

    const dropdownItems = document.querySelectorAll("tbody.task_management_list_body .dropdown-item.change_task_status");
    dropdownItems.forEach(item => {
        item.addEventListener("click", function(event) {
            event.preventDefault();
            const value = this.getAttribute("value");
            const id = this.getAttribute("id");

            if(typeof id !== 'undefined' && id.length !== 0 && typeof value !== 'undefined' && value.length !== 0){

                ajax_call_function({
                    call_type: 'change_task_status',
                    id: id,
                    task_status: value,
                    type: $('a.change_task_management_list_type.kt-widget__item--active').attr('type_name'),
                }, 'change_task_status');
            }
        });
    });

    const dropdownItems_2 = document.querySelectorAll("tbody.task_management_list_body .dropdown-item.change_assigned_to_status");
    dropdownItems_2.forEach(item => {
        item.addEventListener("click", function(event) {
            event.preventDefault();
            const value = this.getAttribute("value");
            const id = this.getAttribute("id");

            if(typeof id !== 'undefined' && id.length !== 0 && typeof value !== 'undefined' && value.length !== 0){

                ajax_call_function({
                    call_type: 'change_assigned_to_status',
                    id: id,
                    user_id: value,
                }, 'change_assigned_to_status');
            }
        });
    });
}

function task_assigned_highchart(total, category, highchart_data) {

    Highcharts.chart('task_assigned_highchart', {
        // Create the chart
        chart: {
            type: 'column'
        },
        title: {
            text: 'Total Work Order :' + total
        },
        xAxis: {
            categories: category
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total Work Order'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.category}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.category}</span>: ' +
                '<b>{point.y}</b><br/>'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: highchart_data
    });
}