<table class="table table-striped" style="margin: 0% 0% 0% 0%;">
	<thead>
		<tr>
			<th scope="col" class="kt-align-center" style="width:5%;">#</th>
			<th scope="col" class="kt-align-center" style="width:50%;">File Name</th>
			<th scope="col" class="kt-align-center" style="width:30%;">Date</th>
			<th scope="col" class="kt-align-center" style="width:25%;">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php if(!empty($document_details)) {?>
                <?php foreach($document_details as $single_document_key => $single_document_details) {?>
				<tr>
					<th scope="row" class="kt-align-center"><?php echo $single_document_key+1;?></th>
					<td class="kt-font-bold kt-align-center"><?php echo $single_document_details['file_name']; ?></td>
					<td class="kt-font-bold kt-align-center"><?php echo $single_document_details['date_time']; ?></td>
					<td class="kt-font-bold kt-align-center">
						<div class="row">
							<div class="col-md-12">

							<?php if($type == 'marking'){ ?>

								<a class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View" href="https://docs.google.com/viewerng/viewer?url=https://storage.googleapis.com/documentation_bucket_name/<?php echo $single_document_details['file_name']; ?>&embedded=true" style="color: #212529;" target="_blank">
									<i class="la la-eye kt-font-bolder" style="color: #212529;"></i>
								</a>

                                <a class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Download <?php echo $single_document_details['file_name']; ?>" href="<?php echo base_url('assets/google_drive/marking/'.$single_document_details['file_name']);?>" style="color: #212529;" target="_blank" download>
                                    <i class="fa fa-download kt-font-bolder" style="color: #212529;"></i>
                                </a>
							<?php }else if($type == 'mtc'){?>

								<a class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View <?php echo $single_document_details['file_name']; ?>" href="<?php echo base_url('assets/google_drive/mtc/'.$single_document_details['file_name']);?>" style="color: #212529;" target="_blank">
									<i class="la la-eye kt-font-bolder" style="color: #212529;"></i>
								</a>

								<a class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Download <?php echo $single_document_details['file_name']; ?>" href="<?php echo base_url('assets/google_drive/mtc/'.$single_document_details['file_name']);?>" style="color: #212529;" target="_blank" download>
									<i class="fa fa-download kt-font-bolder" style="color: #212529;"></i>
								</a>
							<?php }?>
							</div>
						</div>
					</td>
				</tr>
                <?php } ?>

			<?php } else{ ?>

				<th class="row"></th>
				<td class="kt-font-bold kt-align-right">Data Not Found !!</td>
				<td></td>
				<td></td>
			<?php }?>
	</tbody>
</table>