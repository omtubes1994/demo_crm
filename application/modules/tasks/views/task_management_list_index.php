<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <style type="text/css">
		.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
			padding-top: 3% !important;
		}
		.layer-white{
		    display: none;
		    position: fixed;
		    top: 0em !important;
		    left: 0em !important;
		    width: 100%;
		    height: 100%;
		    text-align: center;
		    vertical-align: middle;
		    background-color: rgba(255, 255, 255, 0.55);
		    opacity: 1;
		    line-height: 1;
		    -webkit-animation-fill-mode: both;
		    animation-fill-mode: both;
		    -webkit-animation-duration: 0.5s;
		    animation-duration: 0.5s;
		    -webkit-transition: background-color 0.5s linear;
		    transition: background-color 0.5s linear;
		    -webkit-user-select: none;
		    -moz-user-select: none;
		    -ms-user-select: none;
		    user-select: none;
		    will-change: opacity;
		    z-index: 9;
		}
		.div-loader{
		    position: absolute;
			top: 50%;
			left: 50%;
			margin: 0px;
			text-align: center;
			z-index: 1000;
			-webkit-transform: translateX(-50%) translateY(-50%);
			transform: translateX(-50%) translateY(-50%);
		}
		.kt-spinner:before {
		    width: 50px;
		    height: 50px;
		    margin-top: -10px;
		}

        ul.menu-tab {
            margin: 0px;
            padding: 0px;
            border: 1px solid #E7E7E7;
            overflow: hidden;
            font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
            font-size: 14px;
            line-height: 20px;
            color: #333;
        }
        ul.menu-tab li{
            width: 50%;
            text-align: center;
            float: left;
        }
        ul.menu-tab li.active_list{
            border-bottom: 0.40rem solid #343a40 !important;
        }
        ul.menu-tab li a{

            cursor: pointer;
            display: inline-block;
            outline: none;
            text-align: center;
            width: 100%;
            background-color: #5578eb;
            border-color: #5578eb;
            color: #ffffff;           
            font-size: 18px;
            font-family: 'latomedium';
            padding: 3px 3px 20px 3px;
        }
        ul.menu-tab li a i{
            display: inline-block;
            width: 35px;
            height: 28px;
            font-style: normal;
            background-size: 100%;
            position: relative;
            top: 6px;
        }
        .task_management_font {
            font-size: 1rem;
            font-weight: 500;
            line-height: 1.5rem;
            -webkit-transition: color 0.3s ease;
            transition: color 0.3s ease;
            color: #000;
        }
        .change_task_management_list_tab{

            font-family: Montserrat !important;
            font-weight: bold !important;
        }
        .change_task_management_list_tab.active{

			color: #fff !important;
			border: 0.20rem solid #000 !important;
            font-weight: bold !important;
        }
		.change_task_management_list_type_production_wise{
			background-color: #a3c2c2;
			margin-right: 5px !important;
			margin-left: 5px !important;
		}
		.kt-widget__item--active{
			background-color: #a3c2c2 !important;
			border: 0.20rem solid #000 !important;
			margin-right: 5px !important;
			margin-left: 5px !important;
		}
		.change_task_management_list_type{
			background-color: #a3c2c2;
			margin-right: 5px !important;
			margin-left: 5px !important;
		}
		.kt-widget__desc{
			color: #000 !important;
            font-size: 16px;
		}
		.kt-widget__item.change_task_management_list_type.kt-widget__item--active .kt-widget__section .kt-widget__desc {
			color: #fff !important;
			font-size: 16px;
		}
		.kt-widget__item.change_task_management_list_type_production_wise.kt-widget__item--active .kt-widget__section .kt-widget__desc {
			color: #fff !important;
			font-size: 16px;
		}

		.tab{

			border-bottom: 0.25rem solid rgba(0, 0, 0, 0.1) !important;
			color: #646c9a !important;
			font-family: Montserrat !important;
			font-weight: bold !important;
		}
		.tab.active{

			border-bottom: 0.25rem solid #5d78ff !important;
			color: #1a73e8 !important;
			background-color: #fff !important;
			font-weight: bold !important;
		}
        .kt-badge.kt-badge--unified-orange {
            color: #fd7e14;
            background: #FFDAB9;
        }
		html, body {
			
			font-size: 13px;
			font-weight: 300;
			font-family: Poppins, Helvetica, sans-serif !important;
			-webkit-font-smoothing: antialiased;
		}
		.image-container {
			position: relative;
			display: inline-block;
		}

		.icon {
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);
			opacity: 0; /* Initially hide the icon */
			transition: opacity 0.3s ease-in-out; /* Add a smooth transition effect */
		}

		.image-container:hover .icon {
			opacity: 1; /* Show the icon on hover */
		}

		.sidebar_container {
			position: relative;
		}

		.sidebar_image{
			position: relative;
			background-color: #ccc;
			display: block;
			margin-bottom: 8px;
			width: 100%;
			border-radius: 12px;
			cursor: pointer;
		}
		.sidebar_image.active{

			border: 4px solid #6c757d;
		}

		.sidebar_container:hover .sidebar_image {
			opacity: 0.4;
		}

		.overlay {
			transition: .3s ease;
			opacity: 0;
			position: absolute;
			top: 20%;
			left: 40%;
			transform: translate(-10%, -10%);
			text-align: center;
		}

		.sidebar_container:hover .overlay {
			opacity: 1;
		}

		.download_btn {
			background-color: #5867dd;
			color: white;
			padding: 10px 10px;
        }

		.delete_btn {
			background-color: #ff0000;
			color: white;
			padding: 10px 10px;
        }
		.tab_type{
			width: 3.8rem !important;
			height: 2rem !important;
		}
		.task_font {
			font-size: 2rem;
			font-weight: 500;
			line-height: 1.5rem;
			-webkit-transition: color 0.3s ease;
			transition: color 0.3s ease;
			color: #000;
		}
		.kt-badge--gainsboro, .kt-font-gainsboro{
		color: SlateGrey;
		}
		.kt-badge--Salmon, .kt-font-Salmon{
			color: Salmon;
		}
		.kt-badge--darkkhaki, .kt-font-darkkhaki{
			color: darkkhaki;
		}
    </style>
	<link href="<?php echo base_url('assets/css/ticket/ticket_css_2.css');?>" rel='stylesheet' type='text/css'  media='all'>
    <!-- begin:: Container -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="kt-portlet kt-portlet--mobile">
					<div class="kt-portlet__body">
						<div class="kt-section kt-section--first">
							<div class="kt-section__body">
								<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
									<div id="task_management_list_process" class="layer-white">
										<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
									</div>  
									<div class="row production_status_wise_graph" style="display:none;">
										<div class="col-xl-12">
											<!--Begin::Portlet-->
											<div class="kt-portlet kt-portlet--height-fluid">
												<div class="kt-portlet__body">
													<!--Begin::Timeline 1 -->
													<div class="kt-scroll" data-scroll="true" style="height: 400px">
														<div id="task_assigned_highchart"></div>
													</div>
													<!--End::Timeline 1 -->
												</div>
											</div>
											<!--End::Portlet-->
										</div>
									</div>
									<div class="row">
										<div class="col-xl-12">
											<div class="kt-portlet ">
												<div class="kt-portlet__body kt-portlet__body--fit-y" style="padding:0px;">
													<!--begin::Widget -->
													<div class="kt-widget kt-widget--user-profile-1" style="padding-bottom: 0px;">
														<div class="kt-widget__body">
															<div class="kt-widget__items row">
															<?php if(in_array('drawing', $this->session->userdata('task_access')['task_list_type_access'])){?>
																<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type" type_name="drawing" style="padding: 10px 14px; width:12%;">
																	<span class="kt-widget__section">
																		<span class="kt-widget__icon">
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon tab_type">
																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																					<polygon points="0 0 24 0 24 24 0 24" />
																					<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero" />
																					<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3" />
																				</g>
																			</svg>
																		</span>
																		<span class="kt-widget__desc">
																			Drawing
																		</span>
																	</span>
																</a>
															<?php } ?>
															<?php if(in_array('marking', $this->session->userdata('task_access')['task_list_type_access'])){?>

																<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type" type_name="marking" style="padding: 10px 14px; width:12%;">
																	<span class="kt-widget__section">
																		<span class="kt-widget__icon">
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon tab_type">
																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																					<polygon points="0 0 24 0 24 24 0 24" />
																					<path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
																					<path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
																				</g>
																			</svg>
																		</span>
																		<span class="kt-widget__desc">
																			Marking
																		</span>
																	</span>
																</a>
															<?php } ?>
															<?php if(in_array('mtc', $this->session->userdata('task_access')['task_list_type_access'])){?>
																<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type" type_name="mtc" style="padding: 10px 14px; width:10%;">
																	<span class="kt-widget__section">
																		<span class="kt-widget__icon">
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon tab_type">
																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																					<rect x="0" y="0" width="24" height="24" />
																					<path d="M2.56066017,10.6819805 L4.68198052,8.56066017 C5.26776695,7.97487373 6.21751442,7.97487373 6.80330086,8.56066017 L8.9246212,10.6819805 C9.51040764,11.267767 9.51040764,12.2175144 8.9246212,12.8033009 L6.80330086,14.9246212 C6.21751442,15.5104076 5.26776695,15.5104076 4.68198052,14.9246212 L2.56066017,12.8033009 C1.97487373,12.2175144 1.97487373,11.267767 2.56066017,10.6819805 Z M14.5606602,10.6819805 L16.6819805,8.56066017 C17.267767,7.97487373 18.2175144,7.97487373 18.8033009,8.56066017 L20.9246212,10.6819805 C21.5104076,11.267767 21.5104076,12.2175144 20.9246212,12.8033009 L18.8033009,14.9246212 C18.2175144,15.5104076 17.267767,15.5104076 16.6819805,14.9246212 L14.5606602,12.8033009 C13.9748737,12.2175144 13.9748737,11.267767 14.5606602,10.6819805 Z" fill="#000000" opacity="0.3" />
																					<path d="M8.56066017,16.6819805 L10.6819805,14.5606602 C11.267767,13.9748737 12.2175144,13.9748737 12.8033009,14.5606602 L14.9246212,16.6819805 C15.5104076,17.267767 15.5104076,18.2175144 14.9246212,18.8033009 L12.8033009,20.9246212 C12.2175144,21.5104076 11.267767,21.5104076 10.6819805,20.9246212 L8.56066017,18.8033009 C7.97487373,18.2175144 7.97487373,17.267767 8.56066017,16.6819805 Z M8.56066017,4.68198052 L10.6819805,2.56066017 C11.267767,1.97487373 12.2175144,1.97487373 12.8033009,2.56066017 L14.9246212,4.68198052 C15.5104076,5.26776695 15.5104076,6.21751442 14.9246212,6.80330086 L12.8033009,8.9246212 C12.2175144,9.51040764 11.267767,9.51040764 10.6819805,8.9246212 L8.56066017,6.80330086 C7.97487373,6.21751442 7.97487373,5.26776695 8.56066017,4.68198052 Z" fill="#000000" />
																				</g>
																			</svg>
																		</span>
																		<span class="kt-widget__desc">
																			MTC
																		</span>
																	</span>
																</a>
															<?php } ?>
															<?php if(in_array('logistics_weight_and_dimension', $this->session->userdata('task_access')['task_list_type_access'])){?>
																<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type" type_name="logistics_weight_and_dimension" style="padding: 10px 14px; width:24%;">
																	<span class="kt-widget__section">
																		<span class="kt-widget__icon">
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon tab_type">
																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																					<rect x="0" y="0" width="24" height="24" />
																					<path d="M4,4 L11.6314229,2.5691082 C11.8750185,2.52343403 12.1249815,2.52343403 12.3685771,2.5691082 L20,4 L20,13.2830094 C20,16.2173861 18.4883464,18.9447835 16,20.5 L12.5299989,22.6687507 C12.2057287,22.8714196 11.7942713,22.8714196 11.4700011,22.6687507 L8,20.5 C5.51165358,18.9447835 4,16.2173861 4,13.2830094 L4,4 Z" fill="#000000" opacity="0.3" />
																					<path d="M12,11 C10.8954305,11 10,10.1045695 10,9 C10,7.8954305 10.8954305,7 12,7 C13.1045695,7 14,7.8954305 14,9 C14,10.1045695 13.1045695,11 12,11 Z" fill="#000000" opacity="0.3" />
																					<path d="M7.00036205,16.4995035 C7.21569918,13.5165724 9.36772908,12 11.9907452,12 C14.6506758,12 16.8360465,13.4332455 16.9988413,16.5 C17.0053266,16.6221713 16.9988413,17 16.5815,17 C14.5228466,17 11.463736,17 7.4041679,17 C7.26484009,17 6.98863236,16.6619875 7.00036205,16.4995035 Z" fill="#000000" opacity="0.3" />
																				</g>
																			</svg>
																		</span>
																		<span class="kt-widget__desc">
																			Logistics - Weight & Dimensions
																		</span>
																	</span>
																</a>
															<?php } ?>
															<?php if(in_array('logistics_invoice_and_pl', $this->session->userdata('task_access')['task_list_type_access'])){?>
																<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type" type_name="logistics_invoice_and_pl" style="padding: 10px 14px; width:19%;">
																	<span class="kt-widget__section">
																		<span class="kt-widget__icon">
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon tab_type">
																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																					<rect x="0" y="0" width="24" height="24" />
																					<path d="M6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,12 C19,12.5522847 18.5522847,13 18,13 L6,13 C5.44771525,13 5,12.5522847 5,12 L5,3 C5,2.44771525 5.44771525,2 6,2 Z M7.5,5 C7.22385763,5 7,5.22385763 7,5.5 C7,5.77614237 7.22385763,6 7.5,6 L13.5,6 C13.7761424,6 14,5.77614237 14,5.5 C14,5.22385763 13.7761424,5 13.5,5 L7.5,5 Z M7.5,7 C7.22385763,7 7,7.22385763 7,7.5 C7,7.77614237 7.22385763,8 7.5,8 L10.5,8 C10.7761424,8 11,7.77614237 11,7.5 C11,7.22385763 10.7761424,7 10.5,7 L7.5,7 Z" fill="#000000" opacity="0.3" />
																					<path d="M3.79274528,6.57253826 L12,12.5 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 Z" fill="#000000" />
																				</g>
																			</svg>
																		</span>
																		<span class="kt-widget__desc">
																			Logistics - Invoice & PL
																		</span>
																	</span>
																</a>
															<?php } ?>
															<?php if(in_array('quality_inspection', $this->session->userdata('task_access')['task_list_type_access'])){?>
																<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type" type_name="quality_inspection" style="padding: 10px 14px; width:18%;">
																	<span class="kt-widget__section">
																		<span class="kt-widget__icon">
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon tab_type">
																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																					<rect x="0" y="0" width="24" height="24" />
																					<rect fill="#000000" x="2" y="5" width="19" height="4" rx="1" />
																					<rect fill="#000000" opacity="0.3" x="2" y="11" width="19" height="10" rx="1" />
																				</g>
																			</svg> 
																		</span>
																		<span class="kt-widget__desc">
																			Quality Inspection
																		</span>
																	</span>
																</a>
															<?php } ?>
															</div>
														</div>
													</div>

													<!--end::Widget -->
												</div>
											</div>
										</div>
										<div class="col-sm-2"></div>
										<div class="col-sm-8">
											<ul class="nav nav-pills nav-fill mont_font_use" role="tablist" style="margin: 0px 0px 5px 0px;">
												<li class="nav-item">
													<a class="nav-link task_management_font change_task_management_list_tab active" tab_name="Pending_and_Done" style="background-color:#ffb822;font-size: 16px;">Pending / Done</a>
												</li>
												<li class="nav-item">
													<a class="nav-link task_management_font change_task_management_list_tab" tab_name="Approved_and_Not_required" style="background-color: #2fc44a; font-size: 16px;">Approved / NR</a>
												</li>
												<li class="nav-item">
													<a class="nav-link task_management_font change_task_management_list_tab" tab_name="recent_activities" style="background-color:#668cff; font-size: 16px;">Recent Activities</a>
												</li>
												<!-- <li class="nav-item">
													<a class="nav-link task_management_font change_task_management_list_tab" tab_name="Dispatch" href="javascript:void(0);" style="">Dispatch</a>
												</li> -->
											</ul>			
										</div>
										<div class="col-sm-2">
											<div class="pending_create_new_issue project_mng_main_wrapper">
												<div class="new_conversation_sort12 project_mng_sort_filter">
													<a href="javascript:void(0);" class="idattrlink-new add-overlay task_management_font add_task_management_search_filter" icon="">
														<em>Filter By</em>
													</a>
													<div class="project_management_comment_box" style="display:none; width: 600px;">
													<?php $this->load->view(
															'tasks/search_filter_form',
															array(
																'search_filter_form_data' =>
																	array(
																		'work_order_no' => '',
																		'product_family' => array(),
																		'production_status' => array(),
																		'task_status' => array(),
																		'assigned_to' => array(),
																		'client_id' => array(),
																		'search_task_client_name' => '',
																	)
															)
														);
													?>
													</div>
												</div>
											</div>	
										</div>
										<div class="col-xl-12" style="margin-top:10px;">
											<div class="kt-portlet ">
												<div class="kt-portlet__body kt-portlet__body--fit-y" style="padding:0px;">
													<!--begin::Widget -->
														<div class="kt-widget kt-widget--user-profile-1" style="padding-bottom: 0px;">
															<div class="kt-widget__body">
																<div class="kt-widget__items row">
																	<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type_production_wise" production_tab="pending_order" style="padding: 10px 14px;margin-bottom: 0px; width:20%;">
																		<span class="kt-widget__section">
																			<span class="kt-widget__icon">
																				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon tab_type">
																					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																						<rect x="0" y="0" width="24" height="24"/>
																						<path d="M12,21 C7.581722,21 4,17.418278 4,13 C4,8.581722 7.581722,5 12,5 C16.418278,5 20,8.581722 20,13 C20,17.418278 16.418278,21 12,21 Z" fill="#000000" opacity="0.3"/>
																						<path d="M13,5.06189375 C12.6724058,5.02104333 12.3386603,5 12,5 C11.6613397,5 11.3275942,5.02104333 11,5.06189375 L11,4 L10,4 C9.44771525,4 9,3.55228475 9,3 C9,2.44771525 9.44771525,2 10,2 L14,2 C14.5522847,2 15,2.44771525 15,3 C15,3.55228475 14.5522847,4 14,4 L13,4 L13,5.06189375 Z" fill="#000000"/>
																						<path d="M16.7099142,6.53272645 L17.5355339,5.70710678 C17.9260582,5.31658249 18.5592232,5.31658249 18.9497475,5.70710678 C19.3402718,6.09763107 19.3402718,6.73079605 18.9497475,7.12132034 L18.1671361,7.90393167 C17.7407802,7.38854954 17.251061,6.92750259 16.7099142,6.53272645 Z" fill="#000000"/>
																						<path d="M11.9630156,7.5 L12.0369844,7.5 C12.2982526,7.5 12.5154733,7.70115317 12.5355117,7.96165175 L12.9585886,13.4616518 C12.9797677,13.7369807 12.7737386,13.9773481 12.4984096,13.9985272 C12.4856504,13.9995087 12.4728582,14 12.4600614,14 L11.5399386,14 C11.2637963,14 11.0399386,13.7761424 11.0399386,13.5 C11.0399386,13.4872031 11.0404299,13.4744109 11.0414114,13.4616518 L11.4644883,7.96165175 C11.4845267,7.70115317 11.7017474,7.5 11.9630156,7.5 Z" fill="#000000"/>
																					</g>
																				</svg>

																			</span>
																			<span class="kt-widget__desc">
																				Pending Order
																			</span>
																		</span>
																	</a>
																	<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type_production_wise" production_tab="semi_ready" style="padding: 10px 14px;margin-bottom: 0px; width:20%;">
																		<span class="kt-widget__section">
																			<span class="kt-widget__icon">
																				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon tab_type">
																					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																						<rect x="0" y="0" width="24" height="24"/>
																						<path d="M21.4451171,17.7910156 C21.4451171,16.9707031 21.6208984,13.7333984 19.0671874,11.1650391 C17.3484374,9.43652344 14.7761718,9.13671875 11.6999999,9 L11.6999999,4.69307548 C11.6999999,4.27886191 11.3642135,3.94307548 10.9499999,3.94307548 C10.7636897,3.94307548 10.584049,4.01242035 10.4460626,4.13760526 L3.30599678,10.6152626 C2.99921905,10.8935795 2.976147,11.3678924 3.2544639,11.6746702 C3.26907199,11.6907721 3.28437331,11.7062312 3.30032452,11.7210037 L10.4403903,18.333467 C10.7442966,18.6149166 11.2188212,18.596712 11.5002708,18.2928057 C11.628669,18.1541628 11.6999999,17.9721616 11.6999999,17.7831961 L11.6999999,13.5 C13.6531249,13.5537109 15.0443703,13.6779456 16.3083984,14.0800781 C18.1284272,14.6590944 19.5349747,16.3018455 20.5280411,19.0083314 L20.5280247,19.0083374 C20.6363903,19.3036749 20.9175496,19.5 21.2321404,19.5 L21.4499999,19.5 C21.4499999,19.0068359 21.4451171,18.2255859 21.4451171,17.7910156 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.254964, 11.721538) scale(-1, 1) translate(-12.254964, -11.721538) "/>
																					</g>
																				</svg>
																			</span>
																			<span class="kt-widget__desc">
																				Semi Ready
																			</span>
																		</span>
																	</a>
																	<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type_production_wise" production_tab="ice" style="padding: 10px 14px;margin-bottom: 0px; width:20%;">
																		<span class="kt-widget__section">
																			<span class="kt-widget__icon">
																				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon tab_type">
																					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																						<polygon points="0 0 24 0 24 24 0 24"/>
																						<rect fill="#000000" opacity="0.3" transform="translate(6.000000, 11.000000) rotate(-180.000000) translate(-6.000000, -11.000000) " x="5" y="5" width="2" height="12" rx="1"/>
																						<path d="M8.29289322,14.2928932 C8.68341751,13.9023689 9.31658249,13.9023689 9.70710678,14.2928932 C10.0976311,14.6834175 10.0976311,15.3165825 9.70710678,15.7071068 L6.70710678,18.7071068 C6.31658249,19.0976311 5.68341751,19.0976311 5.29289322,18.7071068 L2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 C2.68341751,13.9023689 3.31658249,13.9023689 3.70710678,14.2928932 L6,16.5857864 L8.29289322,14.2928932 Z" fill="#000000" fill-rule="nonzero"/>
																						<rect fill="#000000" opacity="0.3" transform="translate(18.000000, 13.000000) scale(1, -1) rotate(-180.000000) translate(-18.000000, -13.000000) " x="17" y="7" width="2" height="12" rx="1"/>
																						<path d="M20.2928932,5.29289322 C20.6834175,4.90236893 21.3165825,4.90236893 21.7071068,5.29289322 C22.0976311,5.68341751 22.0976311,6.31658249 21.7071068,6.70710678 L18.7071068,9.70710678 C18.3165825,10.0976311 17.6834175,10.0976311 17.2928932,9.70710678 L14.2928932,6.70710678 C13.9023689,6.31658249 13.9023689,5.68341751 14.2928932,5.29289322 C14.6834175,4.90236893 15.3165825,4.90236893 15.7071068,5.29289322 L18,7.58578644 L20.2928932,5.29289322 Z" fill="#000000" fill-rule="nonzero" transform="translate(18.000000, 7.500000) scale(1, -1) translate(-18.000000, -7.500000) "/>
																					</g>
																				</svg>
																			</span>
																			<span class="kt-widget__desc">
																				ICE
																			</span>
																		</span>
																	</a>
																	<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type_production_wise" production_tab="import" style="padding: 10px 14px;margin-bottom: 0px; width:18%;">
																		<span class="kt-widget__section">
																			<span class="kt-widget__icon">
																				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon tab_type">
																					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																						<polygon points="0 0 24 0 24 24 0 24"/>
																						<rect fill="#000000" opacity="0.3" transform="translate(6.000000, 11.000000) rotate(-180.000000) translate(-6.000000, -11.000000) " x="5" y="5" width="2" height="12" rx="1"/>
																						<path d="M8.29289322,14.2928932 C8.68341751,13.9023689 9.31658249,13.9023689 9.70710678,14.2928932 C10.0976311,14.6834175 10.0976311,15.3165825 9.70710678,15.7071068 L6.70710678,18.7071068 C6.31658249,19.0976311 5.68341751,19.0976311 5.29289322,18.7071068 L2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 C2.68341751,13.9023689 3.31658249,13.9023689 3.70710678,14.2928932 L6,16.5857864 L8.29289322,14.2928932 Z" fill="#000000" fill-rule="nonzero"/>
																						<rect fill="#000000" opacity="0.3" transform="translate(18.000000, 13.000000) scale(1, -1) rotate(-180.000000) translate(-18.000000, -13.000000) " x="17" y="7" width="2" height="12" rx="1"/>
																						<path d="M20.2928932,5.29289322 C20.6834175,4.90236893 21.3165825,4.90236893 21.7071068,5.29289322 C22.0976311,5.68341751 22.0976311,6.31658249 21.7071068,6.70710678 L18.7071068,9.70710678 C18.3165825,10.0976311 17.6834175,10.0976311 17.2928932,9.70710678 L14.2928932,6.70710678 C13.9023689,6.31658249 13.9023689,5.68341751 14.2928932,5.29289322 C14.6834175,4.90236893 15.3165825,4.90236893 15.7071068,5.29289322 L18,7.58578644 L20.2928932,5.29289322 Z" fill="#000000" fill-rule="nonzero" transform="translate(18.000000, 7.500000) scale(1, -1) translate(-18.000000, -7.500000) "/>
																					</g>
																				</svg>
																			</span>
																			<span class="kt-widget__desc">
																				Import
																			</span>
																		</span>
																	</a>
																	<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type_production_wise" production_tab="query" style="padding: 10px 14px;margin-bottom: 0px; width:18%;">
																		<span class="kt-widget__section">
																			<span class="kt-widget__icon">
																				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon tab_type">
																					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																						<rect x="0" y="0" width="24" height="24"/>
																						<circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10"/>
																						<path d="M12,16 C12.5522847,16 13,16.4477153 13,17 C13,17.5522847 12.5522847,18 12,18 C11.4477153,18 11,17.5522847 11,17 C11,16.4477153 11.4477153,16 12,16 Z M10.591,14.868 L10.591,13.209 L11.851,13.209 C13.447,13.209 14.602,11.991 14.602,10.395 C14.602,8.799 13.447,7.581 11.851,7.581 C10.234,7.581 9.121,8.799 9.121,10.395 L7.336,10.395 C7.336,7.875 9.31,5.922 11.851,5.922 C14.392,5.922 16.387,7.875 16.387,10.395 C16.387,12.915 14.392,14.868 11.851,14.868 L10.591,14.868 Z" fill="#000000"/>
																					</g>
																				</svg>
																			</span>
																			<span class="kt-widget__desc">
																				Query
																			</span>
																		</span>
																	</a>
																</div>
																<div class="kt-widget__items row">
																	<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type_production_wise" production_tab="in_transit_ice" style="padding: 10px 14px;margin-bottom: 0px; width:20%;">
																		<span class="kt-widget__section">
																			<span class="kt-widget__icon">
																				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon tab_type">
																					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																						<polygon points="0 0 24 0 24 24 0 24"/>
																						<path d="M22,15 L22,19 C22,20.1045695 21.1045695,21 20,21 L8,21 C5.790861,21 4,19.209139 4,17 C4,14.790861 5.790861,13 8,13 L20,13 C21.1045695,13 22,13.8954305 22,15 Z M7,19 C8.1045695,19 9,18.1045695 9,17 C9,15.8954305 8.1045695,15 7,15 C5.8954305,15 5,15.8954305 5,17 C5,18.1045695 5.8954305,19 7,19 Z" fill="#000000" opacity="0.3"/>
																						<path d="M15.5421357,5.69999981 L18.3705628,8.52842693 C19.1516114,9.30947552 19.1516114,10.5758055 18.3705628,11.3568541 L9.88528147,19.8421354 C8.3231843,21.4042326 5.79052439,21.4042326 4.22842722,19.8421354 C2.66633005,18.2800383 2.66633005,15.7473784 4.22842722,14.1852812 L12.7137086,5.69999981 C13.4947572,4.91895123 14.7610871,4.91895123 15.5421357,5.69999981 Z M7,19 C8.1045695,19 9,18.1045695 9,17 C9,15.8954305 8.1045695,15 7,15 C5.8954305,15 5,15.8954305 5,17 C5,18.1045695 5.8954305,19 7,19 Z" fill="#000000" opacity="0.3"/>
																						<path d="M5,3 L9,3 C10.1045695,3 11,3.8954305 11,5 L11,17 C11,19.209139 9.209139,21 7,21 C4.790861,21 3,19.209139 3,17 L3,5 C3,3.8954305 3.8954305,3 5,3 Z M7,19 C8.1045695,19 9,18.1045695 9,17 C9,15.8954305 8.1045695,15 7,15 C5.8954305,15 5,15.8954305 5,17 C5,18.1045695 5.8954305,19 7,19 Z" fill="#000000"/>
																					</g>
																				</svg>
																			</span>
																			<span class="kt-widget__desc">
																				In Transit
																			</span>
																		</span>
																	</a>
																	<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type_production_wise" production_tab="rfd" style="padding: 10px 14px;margin-bottom: 0px; width:20%;">
																		<span class="kt-widget__section">
																			<span class="kt-widget__icon">
																				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon tab_type">
																					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																						<rect x="0" y="0" width="24" height="24"/>
																						<path d="M20.4061385,6.73606154 C20.7672665,6.89656288 21,7.25468437 21,7.64987309 L21,16.4115967 C21,16.7747638 20.8031081,17.1093844 20.4856429,17.2857539 L12.4856429,21.7301984 C12.1836204,21.8979887 11.8163796,21.8979887 11.5143571,21.7301984 L3.51435707,17.2857539 C3.19689188,17.1093844 3,16.7747638 3,16.4115967 L3,7.64987309 C3,7.25468437 3.23273352,6.89656288 3.59386153,6.73606154 L11.5938615,3.18050598 C11.8524269,3.06558805 12.1475731,3.06558805 12.4061385,3.18050598 L20.4061385,6.73606154 Z" fill="#000000" opacity="0.3"/>
																						<polygon fill="#000000" points="14.9671522 4.22441676 7.5999999 8.31727912 7.5999999 12.9056825 9.5999999 13.9056825 9.5999999 9.49408582 17.25507 5.24126912"/>
																					</g>
																				</svg>


																			</span>
																			<span class="kt-widget__desc">
																				R F D
																			</span>
																		</span>
																	</a>
																	<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type_production_wise" production_tab="dispatch_order" style="padding: 10px 14px;margin-bottom: 0px; width:20%;">
																		<span class="kt-widget__section">
																			<span class="kt-widget__icon">
																				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon tab_type">
																					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																						<rect x="0" y="0" width="24" height="24"/>
																						<path d="M3.51471863,18.6568542 L13.4142136,8.75735931 C13.8047379,8.36683502 14.4379028,8.36683502 14.8284271,8.75735931 L16.2426407,10.1715729 C16.633165,10.5620972 16.633165,11.1952621 16.2426407,11.5857864 L6.34314575,21.4852814 C5.95262146,21.8758057 5.31945648,21.8758057 4.92893219,21.4852814 L3.51471863,20.0710678 C3.12419433,19.6805435 3.12419433,19.0473785 3.51471863,18.6568542 Z" fill="#000000" opacity="0.3"/>
																						<path d="M9.87867966,6.63603897 L13.4142136,3.10050506 C13.8047379,2.70998077 14.4379028,2.70998077 14.8284271,3.10050506 L21.8994949,10.1715729 C22.2900192,10.5620972 22.2900192,11.1952621 21.8994949,11.5857864 L18.363961,15.1213203 C17.9734367,15.5118446 17.3402718,15.5118446 16.9497475,15.1213203 L9.87867966,8.05025253 C9.48815536,7.65972824 9.48815536,7.02656326 9.87867966,6.63603897 Z" fill="#000000"/>
																						<path d="M17.3033009,4.86827202 L18.0104076,4.16116524 C18.2056698,3.96590309 18.5222523,3.96590309 18.7175144,4.16116524 L20.8388348,6.28248558 C21.0340969,6.47774772 21.0340969,6.79433021 20.8388348,6.98959236 L20.131728,7.69669914 C19.9364658,7.89196129 19.6198833,7.89196129 19.4246212,7.69669914 L17.3033009,5.5753788 C17.1080387,5.38011665 17.1080387,5.06353416 17.3033009,4.86827202 Z" fill="#000000" opacity="0.3"/>
																					</g>
																				</svg>
																			</span>
																			<span class="kt-widget__desc">
																				Dispatch Order
																			</span>
																		</span>
																	</a>
																	<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type_production_wise" production_tab="on_holder_order" style="padding: 10px 14px;margin-bottom: 0px; width:18%;">
																		<span class="kt-widget__section">
																			<span class="kt-widget__icon">
																				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon tab_type">
																					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																						<rect x="0" y="0" width="24" height="24"/>
																						<path d="M8,6 L10,6 C10.5522847,6 11,6.44771525 11,7 L11,17 C11,17.5522847 10.5522847,18 10,18 L8,18 C7.44771525,18 7,17.5522847 7,17 L7,7 C7,6.44771525 7.44771525,6 8,6 Z M14,6 L16,6 C16.5522847,6 17,6.44771525 17,7 L17,17 C17,17.5522847 16.5522847,18 16,18 L14,18 C13.4477153,18 13,17.5522847 13,17 L13,7 C13,6.44771525 13.4477153,6 14,6 Z" fill="#000000"/>
																					</g>
																				</svg>
																			</span>
																			<span class="kt-widget__desc">
																				On Hold Order
																			</span>
																		</span>
																	</a>
																	<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type_production_wise" production_tab="order_cancelled" style="padding: 10px 14px;margin-bottom: 0px;width:18%;">
																		<span class="kt-widget__section">
																			<span class="kt-widget__icon">
																				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon tab_type">
																					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																						<rect x="0" y="0" width="24" height="24"/>
																						<circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10"/>
																						<path d="M12.0355339,10.6213203 L14.863961,7.79289322 C15.2544853,7.40236893 15.8876503,7.40236893 16.2781746,7.79289322 C16.6686989,8.18341751 16.6686989,8.81658249 16.2781746,9.20710678 L13.4497475,12.0355339 L16.2781746,14.863961 C16.6686989,15.2544853 16.6686989,15.8876503 16.2781746,16.2781746 C15.8876503,16.6686989 15.2544853,16.6686989 14.863961,16.2781746 L12.0355339,13.4497475 L9.20710678,16.2781746 C8.81658249,16.6686989 8.18341751,16.6686989 7.79289322,16.2781746 C7.40236893,15.8876503 7.40236893,15.2544853 7.79289322,14.863961 L10.6213203,12.0355339 L7.79289322,9.20710678 C7.40236893,8.81658249 7.40236893,8.18341751 7.79289322,7.79289322 C8.18341751,7.40236893 8.81658249,7.40236893 9.20710678,7.79289322 L12.0355339,10.6213203 Z" fill="#000000"/>
																					</g>
																				</svg>

																			</span>
																			<span class="kt-widget__desc">
																				Order Cancelled
																			</span>
																		</span>
																	</a>
																</div>
															</div>
														</div>
													<!--end::Widget -->
												</div>
											</div>
										</div>
										<div class="col-sm-12 recent_activities_div" style="display:none;">

											<!--Begin::Portlet-->
											<div class="kt-portlet kt-portlet--height-fluid">
												<div class="kt-portlet__body">

													<!--Begin::Timeline 3 -->
													<div class="kt-timeline-v2">
														<div class="kt-timeline-v2__items  kt-padding-top-25 kt-padding-bottom-30 recent_activities_listing"></div>
													</div>

													<!--End::Timeline 3 -->
												</div>
											</div>

											<!--End::Portlet-->
										</div>
										<div class="col-sm-12 table_listing_div">
											<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 1536px;">
												<thead id="task_management_list_head">
													<tr role="row">
														<th class="kt-align-left sorting_disabled" style="width: 1%;">Sr</th>
														<th class="kt-align-center" style="width: 8%;">Task</th>
														<th class="kt-align-center sorting sorting_search" sorting_name="add_time" sorting_value="desc" style="width: 6%;">Date</th>
														<th class="kt-align-center" style="width: 14%;">Work Order no</th>
														<th class="kt-align-center" style="width: 10%;">Production Status</th>
														<th class="kt-align-center" style="width: 10%;">Product Family</th>
														<th class="kt-align-center" style="width: 7%;">Assigned To</th>
														<th class="kt-align-center" style="width: 8%;">Status</th>
														<th class="kt-align-center" style="width: 6%;">TAT</th>
														<th class="kt-align-center" style="width: 22%;">Comment</th>
														<th class="kt-align-center" style="width: 8%;">Actions</th>
													</tr>
												</thead>
												<tbody class="task_management_list_body">
													<?php $this->load->view('tasks/task_management_list_body'); ?>
												</tbody>
											</table> 
											<div class="row task_management_list_paggination"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="task_management_list_loader" class="layer-white">
			<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
		</div>
	</div>
    
    <!-- end:: Container -->
</div>

<!--end::Content-->
<!--Begin:: Chat-->
    <div class="modal fade- modal-sticky-bottom-right" id="kt_chat_modal" role="dialog" data-backdrop="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="kt-chat">
                    <div class="kt-portlet kt-portlet--last">
                        <div class="kt-portlet__head">
                            <div class="kt-chat__head ">
                                <div class="kt-chat__left">
                                    <div class="kt-chat__label">
                                        <a href="#" class="kt-chat__title">
                                            <em class= "header_1"></em>
                                        </a>
                                        <span class="kt-chat__status header_2"></span>
                                    </div>
                                </div>
                                <div class="kt-chat__right"></div>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <div class="kt-scroll kt-scroll--pull chat_scroll" data-height="410" data-mobile-height="225">
                                <div class="layer-white rfq_chat_loader">
                                    <div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
                                </div>
                                <div class="kt-chat__messages kt-chat__messages--solid div_chat_history"></div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-chat__input">
                                <div class="kt-chat__editor">
                                    <textarea class="kt-font-dark" id = "textarea_id_for_chat_message" placeholder="Type here..." style="height: 50px"></textarea>
                                </div>
                                <div class="kt-chat__toolbar">
                                    <div class="kt_chat__tools">
                                        <div class="query_type"></div>
                                    </div>
                                    <div class="kt_chat__actions">
                                        <button type="button" class="btn btn-brand btn-md btn-font-sm btn-upper btn-bold save_task_comment_add_form" id = 0>reply</button>
                                        <button type="button" class="btn btn-brand btn-md btn-font-sm btn-upper btn-bold" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="get_upload_history_and_new_add_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content row">
				<div id="task_management_document_loader" class="layer-white">
					<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
				</div>
				<input id="task_id_for_upload_document" type="number" value="0" hidden>
				<input id="work_order_no" type="number" value="0" hidden>
				<input id="type" type="text" value="" hidden>
				<div class="kt-portlet col-xl-12" style="padding: 10px 10px 0px 25px; margin-bottom: 0px;">
					<div class="kt-portlet__body kt-portlet__body--fit-y" style="padding: 0px;">
						<!--begin::Widget -->
						<div class="kt-widget kt-widget--user-profile-1" style="padding-bottom: 0px;">
							<div class="kt-widget__body row">
								<div class="kt-widget__items col-xl-11 row upload_tab">
									
								</div>
								<div class="col-xl-1" style="position: relative;">
									<a href="javascript:void(0);" class="btn btn-md btn-clean btn-icon btn-icon-md" data-dismiss="modal" aria-label="Close">
										<i class="la la-close" title="Close Upload Document" style="position: absolute; top: 0; right: 0; padding: 10px;"></i>
									</a>
								</div>
							</div>
						</div>

						<!--end::Widget -->
					</div>
				</div>
                <div class="modal-body task_management_font file_upload_div row" style="padding-top: 0px; height: 600px;"></div>
            </div>
        </div>
    </div>
    <!--begin::Modal-->
    <div class="modal fade" id="kt_modal_4_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table">
                        <thead id="">
                            <tr role="row" class="task_management_font">
                                <th class="kt-align-left" style="width: 1%;">Sr</th>
                                <th class="kt-align-center" style="width: 10%;">Product/Material</th>
                                <th class="kt-align-center">Description</th>
                                <th class="kt-align-center" style="width: 10%;">Quantity</th>
                                <th class="kt-align-center" style="width: 20%;">Actions</th>
                            </tr>
                        </thead>
                        <tbody class="quotation_line_item_tbody"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!--end::Modal-->
	<!--begin::Modal-->
	<div class="modal fade" id="add_weight_dimension" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Add Weight & Dimension</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<textarea id="weight_dimension_text" class="form-control validate[required]" style="height:300px;"></textarea>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary save_weight_dimension" task_id="0">Save</button>
				</div>
			</div>
		</div>
	</div>

	<!--end::Modal-->
	<!--begin::Modal-->
	<div class="modal fade" id="kt_modal_4_2_new" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content" style="padding:0px 25px; border-radius: 25px;">
				<div class="modal-header" style="padding: 25px 0px 0px 10px;border-bottom: 0px solid #ebedf2;">
					<h5 class="modal-title task_management_font" id="exampleModalLabel">Work Order # <span class="work_order_no"></span></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body row">
					<div class="layer-white upload_history_loader">
						<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
					</div>
					<input id="task_id_for_upload_document_new" type="number" value="0" hidden>
					<input id="work_order_no_new" type="number" value="0" hidden>
					<input id="type_new" type="text" value="" hidden>
					<div class="col-xl-12 row main_tab_html"></div>
					<div class="col-xl-4" style="padding-top: 24px;">
						<div class="kt-notes kt-scroll kt-scroll--pull ps ps--active-y row sidebar_photos" data-scroll="true" style="max-height: 700px; overflow: hidden;"></div>					
					</div>
					<div class="col-xl-8 main_photos"style="padding-top: 24px; height: 700px;">
						
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--end::Modal-->
	<!--begin::Modal-->
	<div class="modal modal-stick-to-bottom fade" id="mtc_upload" role="dialog" data-backdrop="false">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Upload Files:</h5>
				</div>
				<div class="modal-body row">
					<div class="col-lg-6" id="pdf_upload_history_mtc" style="padding: 10px 20px 10px 20px;"></div>
					<div class="col-lg-6" id="pdf_upload_history_drawing" style="padding: 10px 20px 10px 0px;"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary mtc_upload_close" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<!--end::Modal-->

	<!--begin::Modal-->
	<div class="modal fade" id="add_revised_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add Revised</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<form id="marking_revised_information">

					</form>
				</div>
			</div>
		</div>
	</div>

	<!--end::Modal-->
	<!--begin::Modal-->
	<div class="modal fade" id="document_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Documents list</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="col-lg-12" id="document_history_list" style="padding: 10px 20px 10px 20px;"></div>
				</div>
			</div>
		</div>
	</div>

	<!--end::Modal-->
	<!--begin::Modal-->
		<div class="modal fade" id="technical_document" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Technical Documents:</h5>
					</div>
					<div class="modal-body row">
						<div class="col-lg-12" id="technical_document_history_list" style="padding: 10px 20px 10px 20px;">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger document_close" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

	<!--end::Modal-->
<!--ENd:: Chat-->