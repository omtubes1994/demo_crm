<div class="project_management_comment_inner">
    <span class="task_management_font">Filter By</span>
    <div class="wrapper scroll-remove-margin" id="filter_elements_div">
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" id="search_filter_form">
            <ul class="project_filter_element">
                <li class="form-group task_management_font">
                    <label class="col-form-label">Work Order No</label>
                    <input type="text" name="work_order_no" class="form-control" value="<?php echo $search_filter_form_data['work_order_no'];?>" placeholder="Enter work order no">
                    <span class="form-text text-muted">Please enter your Work Order No</span>
                    <label class="col-form-label">Product Family</label>
                    <select class="form-control select_picker" name="product_family">
                        <option value="">Select</option>
                        <?php foreach($search_filter_form_data['product_family'] as $single_details){

                            echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
                        }  ?>
                    </select>
                    <label class="col-form-label">Assigned To</label>
                    <select class="form-control select_picker" name="assigned_to">
                        <option value="">Select</option>
                        <?php foreach($search_filter_form_data['assigned_to'] as $single_details){

                            echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
                        }  ?>
                    </select><br/>
                    <?php if($this->session->userdata('task_access')['task_list_client_name_access']){ ?>
                    <label>Company Name :</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="client_name" name="search_task_client_name" value="<?php echo $search_filter_form_data['search_task_client_name']; ?>" placeholder="min 4 character">
                    </div>
                    <select class="form-control select_picker" name="client_id" data-live-search="true">
                        <option value="">Select Name</option>
                        <?php foreach($search_filter_form_data['client_id'] as $single_details){

                            echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
                        }  ?>
                    </select><br/>                    
                    <?php } ?>
                    <label class="col-form-label">Production Status</label>
                    <select class="form-control" name="production_status">
                        <option value="">Select</option>
                        <?php foreach($search_filter_form_data['production_status'] as $single_details){

                            echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
                        }  ?>
                    </select><br>
                    <label class="col-form-label">Task Status</label>
                    <select class="form-control" name="task_status">
                        <option value="">Select</option>
                        <?php foreach($search_filter_form_data['task_status'] as $single_details){
                            echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
                        }  ?>
                    </select>
                </li>
            </ul>
        </form>

        <!--end::Form-->
    </div>
    <div class="project_management_comment_footer">
        <a href="javascript:void(0);" class="lead-save-btn filter_search">Filter</a>
        <a href="javascript:void(0);" class="lead-save-btn reset_filter_search">Reset Filter</a>
        <a class="lead-close-btn close_filter_by" style="color: #898989 !important;">Close</a>
    </div>
</div>