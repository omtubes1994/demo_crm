<div class="form-group">
    <label for="recipient-name" class="form-control-label task_font">Revised / Not - Revised :</label>
    <select class="form-control select_picker marking_revised" name="marking_revised">
        <option value="">Select</option>                                        
        <option value="not_revised" <?php echo ($marking_revised == 'not_revised') ? 'selected' : ''; ?>>Not Revised</option>
        <option value="revised" <?php echo ($marking_revised == 'revised') ? 'selected' : ''; ?>>Revised</option>
    </select>
</div>
<div class="form-group">
    <label for="message-text" class="form-control-label task_font">Comment :</label>
    <textarea class="form-control validate[required]" name="revised_comment" id="message-text" style="height:100px;"></textarea>
</div>
<div></div>
<div>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Sr</th>
                <th>Comment</th>
                <th>Date</th>
            </tr>
        </thead>
		<tbody>
		<?php if(!empty($revised_comment_details)){ ?>
            <?php foreach ($revised_comment_details as $comment_key => $single_comment_details) { ?>
                <tr>
                    <td><?php echo $comment_key+1; ?></td>
                    <td><?php echo $single_comment_details['comment']; ?></td>
                    <td><?php echo  date('j F, Y', strtotime($single_comment_details['date_time'])); ?></td>
                </tr>
            <?php } ?>
        <?php } ?>
		</tbody>
	</table>
</div>
<div style="text-align: right;">
    <button class="btn btn-success save_revised_marking" type="reset" id="task_id">
        Update Marking
    </button>
</div>