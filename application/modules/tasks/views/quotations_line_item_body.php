<?php if(!empty($quotation_dtl_data)){ ?>
    <?php foreach ($quotation_dtl_data as $quotation_dtl_data_key => $quotation_dtl_details) { ?>
        <tr role="row" class="task_management_font">
            <td class="kt-align-left">
                <?php echo $quotation_dtl_data_key+1; ?>
            </td>
            <td class="kt-align-center">
                <?php echo (!empty($product_mst[$quotation_dtl_details['product_id']])) ? $product_mst[$quotation_dtl_details['product_id']] : ''; ?>
                <?php echo (!empty($material_mst[$quotation_dtl_details['material_id']])) ? $material_mst[$quotation_dtl_details['material_id']] : ''; ?></td>
            <td class="kt-align-center">
                <?php echo $quotation_dtl_details['description']; ?>
            </td>
            <td class="kt-align-center">
                <?php echo $quotation_dtl_details['quantity']; ?>
            </td>
            <td style="display:inline-flex;"></td>
        </tr>
    <?php } ?>
<?php } ?>