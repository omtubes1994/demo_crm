<?php 
    $color_array = array('danger', 'success', 'info', 'warning', 'primary');
?>

<?php if(!empty($notification_list)){ ?>
    <?php foreach ($notification_list as $notification_date => $notification_date_wise_details) { ?>
        <div class="kt-timeline-v2__item">
            <span class="kt-timeline-v2__item-time"><?php echo date('j M', strtotime($notification_date)); ?></span>
            <div class="kt-timeline-v2__item-cricle">
               
            </div>
            <div class="kt-timeline-v2__item-text kt-timeline-v2__item-text--bold kt-padding-top-5">
                
            </div>
        </div>
        <?php foreach ($notification_date_wise_details as $notification_key => $notification_details) { ?>
            <div class="kt-timeline-v2__item">
                <span class="kt-timeline-v2__item-time"><?php echo date('h:i', strtotime($notification_details['add_time'])); ?></span>
                <div class="kt-timeline-v2__item-cricle">
                    <i class="fa fa-genderless kt-font-<?php echo $color_array[$notification_key%5]?>"></i>
                </div>
                <div class="kt-timeline-v2__item-text kt-timeline-v2__item-text--bold kt-padding-top-5">
                    <?php echo $notification_details['name']; ?>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
<?php } ?>