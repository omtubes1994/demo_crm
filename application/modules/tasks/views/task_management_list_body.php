<?php if(!empty($task_list)){ ?>
    <?php foreach ($task_list as $single_task_details) { ?>
        <tr role="row" class="task_management_font">
            <td class="kt-align-left"><?php echo $single_task_details['sr_no']; ?></td>
            <td class="kt-align-center"><?php echo $single_task_details['name']; ?><br/><br/>
                <?php if(in_array($single_task_details['type'], array('marking', 'quality_inspection')) && $single_task_details['marking_revised'] == 'revised'){ ?>
                        <sup>
                            <span class="badge badge-sm badge-info" style="width: auto; font-size: 10px; ">REV - <?php echo $single_task_details['revised_count']; ?></span>
                        </sup>
                <?php } ?>
            </td>
            <td class="kt-align-center"><?php echo $single_task_details['date']; ?></td>
            <td class="">
                <div class="kt-align-center">
                    <?php echo $single_task_details['work_order_no']; ?>
                </div>
                <?php if($this->session->userdata('task_access')['task_list_client_name_access'] && in_array($single_task_details['type'], array('drawing', 'marking', 'mtc'))){ ?>
                    <div class="kt-align-left"><hr style="margin: 2px;">Name:
                        <span class="kt-font-bolder kt-font-brand">
                            <?php echo $single_task_details['client_name'];?>
                        </span>
                    </div>
                <?php }?>
                <?php if($this->session->userdata('task_access')['task_list_rfq_and_quote_no_access'] &&$single_task_details['type'] == 'drawing'){?>
                    <?php if(!empty($single_task_details['rfq_no'])){?>
                        <div class="kt-align-left"><hr style="margin: 2px;">RFQ No:
                        <span class="kt-font-bolder kt-font-brand">
                            <?php echo $single_task_details['rfq_no'];?>
                        </span>
                        </div>
                    <?php }?>
                    <?php if(!empty($single_task_details['quote_no'])){?>
                        <div class="kt-align-left"><hr style="margin: 2px;">Quote No:
                        <span class="kt-font-bolder kt-font-brand">
                            <?php echo $single_task_details['quote_no'];?>
                        </span>
                        </div>
                    <?php }?>
                <?php }?>
            </td>
            <td class="kt-align-center">
                <em class="kt-font-bolder kt-font-<?php echo $single_task_details['production_status']['color'];?>" style="width: 200px; padding: 0px 0px 0px 0px;">
                    <?php echo $single_task_details['production_status']['name'];?>
                </em>
            </td>
            <td class="kt-align-center"><?php echo $single_task_details['product_family_span']; ?></td>
            <td class="kt-align-center"><?php echo $single_task_details['assigned_to_name']; ?></td>
            <td class="kt-align-center"><?php echo $single_task_details['task_status_span']; ?><br>
                <?php if(!empty($single_task_details['aproved_by_name'])){
                    echo 'by '.$single_task_details['aproved_by_name'];
                } ?>
            </td>
            <td class="kt-align-center"><?php echo $single_task_details['tat']; ?></td>
            <td class="">
                <div class="row">
                <?php if(!empty($single_task_details['comment'])){ ?>
                    <div class="col-xl-12 kt-align-left kt-font-bold"><?php echo $single_task_details['comment']['message']; ?></div>
                    <div class="col-xl-6 kt-align-left kt-font-bold"></div>
                    <div class="col-xl-6 kt-align-left kt-font-bold">
                        <a href="javascript:void(0);" class="kt-align-left kt-align-center kt-font-bolder kt-font-brand"><?php echo $single_task_details['comment']['user_name']; ?></a>
                    </div>
                    <div class="col-xl-6 kt-align-left kt-font-bold"></div>
                    <div class="col-xl-6 kt-align-left kt-font-bold">
                        <span class="kt-align-left kt-align-center kt-font-bolder kt-font-brand"><?php echo $single_task_details['comment']['tat']; ?></span>
                    </div>
                <?php } ?>
                </div></br></br>
                <?php if($single_task_details['type'] == 'marking' || $single_task_details['type'] == 'quality_inspection'){ ?>
                    <?php if(!empty($single_task_details['revised_comment'])){ ?>
                        <span class="kt-font-bolder kt-font-brand" style="font-size: 12px; text-align:left;">
                            Revised :
                        </span>
                        <span style="font-size: 12px; text-align:left;">
                            <?php echo $single_task_details['revised_comment']['comment']; ?>
                        </span>
                    <?php } ?>
                <?php } ?>
            </td>
            <td>
                <div style="display:inline-flex;">
                <?php if($single_task_details['work_order_sheet_approved_by_status'] == 'approved'){ ?>
                <?php if($single_task_details['action_tab_flag']['work_order_sheet']){ ?>
                    <a href="<?php echo base_url("production/pdf/").$single_task_details['work_order_no']; ?>" target="_blank" title="Work Order Sheet"
                    class="btn btn-md btn-clean btn-icon btn-icon-md">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--md">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M3,12 C3,12 5.45454545,6 12,6 C16.9090909,6 21,12 21,12 C21,12 16.9090909,18 12,18 C5.45454545,18 3,12 3,12 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                <path d="M12,15 C10.3431458,15 9,13.6568542 9,12 C9,10.3431458 10.3431458,9 12,9 C13.6568542,9 15,10.3431458 15,12 C15,13.6568542 13.6568542,15 12,15 Z" fill="#000000" opacity="0.3"/>
                            </g>
                        </svg>
                    </a>
                    <?php } ?>
                <?php } ?>
                <?php if($single_task_details['action_tab_flag']['proforma_invoice']){ ?>
                    <a href="<?php echo base_url("pdf_management/proforma_pdf/").$single_task_details['quotation_mst_id']; ?>" target="_blank"
                    class="btn btn-md btn-clean btn-icon btn-icon-md">
                        <i class="flaticon-eye" title="View Proforma"></i>
                    </a>
                <?php } ?>
                <?php if($single_task_details['action_tab_flag']['purchase_order']){ ?>
                    <a href="<?php echo base_url("assets/purchase_orders/").$single_task_details['purchase_order']; ?>" target="_blank"
                    class="btn btn-md btn-clean btn-icon btn-icon-md">
                        <i class="fa fa-file-pdf-o kt-font-bolder" title="View Purchase Order"></i>
                    </a>
                <?php } ?>
                <?php if($single_task_details['action_tab_flag']['task_status']){ ?>
                    <div class="dropdown">                 
                        <a href="javascript:void(0);" class="btn btn-md btn-clean btn-icon btn-icon-md" data-toggle="dropdown">
                            <i class="flaticon2-check-mark" title="Change task status"></i>                            
                        </a>                          
                        <div class="dropdown-menu dropdown-menu-right" data-size="7" data-live-search="true">
                        <?php if(!empty($single_task_details['task_status_array'])){ ?>
                            <?php foreach ($single_task_details['task_status_array'] as $task_status_details) { ?>
                                <a class="dropdown-item change_task_status <?php echo $task_status_details['class'];?>" value="<?php echo $task_status_details['value']; ?>" id="<?php echo $single_task_details['id'];?>">
                                    <?php echo $task_status_details['display_name']; ?>
                                </a> 
                            <?php } ?>
                        <?php } ?>      
                        </div>
                    </div>
                <?php } ?>
                <?php if($single_task_details['action_tab_flag']['comment_query']){ ?>
                    <a href="javascript:void(0);" class="btn btn-md btn-clean btn-icon btn-icon-md get_comment_query_chat" id="<?php echo $single_task_details['id'];?>">
                        <i class="flaticon2-talk" title="Comment / Query"></i>
                    </a>
                <?php } ?>
                <?php if($single_task_details['action_tab_flag']['upload_document']){ ?>
                    <a href="javascript:void(0);" class="btn btn-md btn-clean btn-icon btn-icon-md get_upload_document_tab_name kt-hidden" id="<?php echo $single_task_details['id'];?>" work_order_no="<?php echo $single_task_details['work_order_no']; ?>" type="<?php echo $single_task_details['type']; ?>">
                        <i class="flaticon-upload" title="Upload Document"></i>
                    </a>
                    <a href="javascript:void(0)" class="get_all_file_upload_history_new" id="<?php echo $single_task_details['id'];?>" work_order_no="<?php echo $single_task_details['work_order_no']; ?>" task_type="<?php echo $single_task_details['type']; ?>" sub_task_type="all">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon <?php echo $single_task_details['document_upload_color'];?> kt-svg-icon--md">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <path d="M3.5,21 L20.5,21 C21.3284271,21 22,20.3284271 22,19.5 L22,8.5 C22,7.67157288 21.3284271,7 20.5,7 L10,7 L7.43933983,4.43933983 C7.15803526,4.15803526 6.77650439,4 6.37867966,4 L3.5,4 C2.67157288,4 2,4.67157288 2,5.5 L2,19.5 C2,20.3284271 2.67157288,21 3.5,21 Z" fill="#000000" opacity="0.3"/>
                            <polygon fill="#000000" opacity="0.3" points="4 19 10 11 16 19"/>
                            <polygon fill="#000000" points="11 19 15 14 19 19"/>
                            <path d="M18,12 C18.8284271,12 19.5,11.3284271 19.5,10.5 C19.5,9.67157288 18.8284271,9 18,9 C17.1715729,9 16.5,9.67157288 16.5,10.5 C16.5,11.3284271 17.1715729,12 18,12 Z" fill="#000000" opacity="0.3"/>
                        </g>
                    </svg>
                </a>
                <?php } ?>
                </div>
                <div style="display:inline-flex;">
                <?php if($single_task_details['action_tab_flag']['assigned_to']){ ?>
                    <div class="dropdown">                 
                        <a href="javascript:void(0);" class="btn btn-md btn-clean btn-icon btn-icon-md" data-toggle="dropdown">
                            <i class="flaticon2-avatar" title="Change assigned to"></i>                            
                        </a>                          
                        <div class="dropdown-menu dropdown-menu-bottom">
                            <div class="kt-scroll kt-scroll--pull chat_scroll" data-height="410" data-mobile-height="225">
                            <?php if(!empty($single_task_details['task_assigned_to_array'])){ ?>
                                <?php foreach ($single_task_details['task_assigned_to_array'] as $task_assigned_to_details) { ?>
                                    <a class="dropdown-item change_assigned_to_status <?php echo $task_assigned_to_details['class']; ?>" value="<?php echo $task_assigned_to_details['user_id']; ?>" id="<?php echo $single_task_details['id'];?>">
                                        <?php echo $task_assigned_to_details['name']; ?>
                                    </a>
                                <?php } ?>
                            <?php } ?>
                            </div> 
                        </div>
                    </div>
                <?php } ?>
                <?php if($single_task_details['action_tab_flag']['line_item_details']){ ?>
                    <a href="javascript:void(0);" class="btn btn-md btn-clean btn-icon btn-icon-md get_line_item_data" work_order_no="<?php echo $single_task_details['work_order_no']; ?>" data-toggle="modal" data-target="#kt_modal_4_2">
                        <i class="flaticon-edit" title="View line item"></i>
                    </a>
                <?php } ?>
                <?php if($single_task_details['action_tab_flag']['weight_dimension']){ ?>
                    <a href="javascript:void(0);" class="btn btn-md btn-clean btn-icon btn-icon-md add_weight_dimension" task_id="<?php echo $single_task_details['id']; ?>" title="Add weight and dimension">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"/>
                                    <path d="M14.8890873,14.1109127 C11.8515212,14.1109127 9.3890873,11.6484788 9.3890873,8.6109127 C9.3890873,5.57334658 11.8515212,3.1109127 14.8890873,3.1109127 C17.9266534,3.1109127 20.3890873,5.57334658 20.3890873,8.6109127 C20.3890873,11.6484788 17.9266534,14.1109127 14.8890873,14.1109127 Z M14.8890873,10.6109127 C15.9936568,10.6109127 16.8890873,9.7154822 16.8890873,8.6109127 C16.8890873,7.5063432 15.9936568,6.6109127 14.8890873,6.6109127 C13.7845178,6.6109127 12.8890873,7.5063432 12.8890873,8.6109127 C12.8890873,9.7154822 13.7845178,10.6109127 14.8890873,10.6109127 Z M14.8890873,9.6109127 C15.441372,9.6109127 15.8890873,9.16319745 15.8890873,8.6109127 C15.8890873,8.05862795 15.441372,7.6109127 14.8890873,7.6109127 C14.3368025,7.6109127 13.8890873,8.05862795 13.8890873,8.6109127 C13.8890873,9.16319745 14.3368025,9.6109127 14.8890873,9.6109127 Z" fill="#000000" opacity="0.3" transform="translate(14.889087, 8.610913) rotate(-405.000000) translate(-14.889087, -8.610913) "/>
                                    <rect fill="#000000" opacity="0.3" transform="translate(9.232233, 14.974874) rotate(-45.000000) translate(-9.232233, -14.974874) " x="1.23223305" y="11.9748737" width="16" height="6" rx="3"/>
                                    <path d="M16.267767,18.0822059 C12.2738464,17.3873004 9.26776695,14.3606699 9.26776695,10.732233 C9.26776695,7.10379618 12.2738464,4.07716572 16.267767,3.38226022 L16.267767,18.0822059 Z" fill="#000000" transform="translate(12.767767, 10.732233) rotate(-405.000000) translate(-12.767767, -10.732233) "/>
                                </g>
                        </svg>
                    </a>
                <?php } ?>
                <?php if($single_task_details['action_tab_flag']['sample_drawing']){ ?>
                    <button type="button" class="btn btn-sm btn-icon btn-icon-md btn-label-success upload_pdf" title="Download" data-toggle="modal" data-target="#mtc_upload" query_number="<?php echo $single_task_details['query_number']; ?>">
                        <i class="fa fa-upload kt-font-bolder"></i>
                    </button>
                <?php } ?>
                <?php if($single_task_details['action_tab_flag']['technical_document']){ ?>

                    <a href="javascript:void(0)" class="technical_document_history" title="Technical Document" data-toggle="modal" data-target="#technical_document" work_order_no="<?php echo $single_task_details['work_order_no'];?>">

                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="32px" viewBox="0 0 32 32" version="1.1" width="32px" class="kt-svg-icon <?php echo $single_task_details['technical_document_color'];?> kt-svg-icon--md">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="m21 13v-3l-6-7h-10.99723987c-1.10609388 0-2.00276013.89833832-2.00276013 2.00732994v22.98534016c0 1.1086177.89092539 2.0073299 1.99742191 2.0073299h15.00515619c1.1031457 0 1.9974219-.8982124 1.9974219-1.9907951v-2.0092049h7.9931517c1.6606364 0 3.0068483-1.3422643 3.0068483-2.9987856v-7.0024288c0-1.6561835-1.3359915-2.9987856-3.0068483-2.9987856zm-1 13v2.0066023c0 .5484514-.4476974.9933977-.9999602.9933977h-15.0000796c-.54525127 0-.9999602-.4456813-.9999602-.995457v-23.00908597c0-.54019415.44573523-.99545703.9955775-.99545703h10.0044225v4.99408095c0 1.11936425.8944962 2.00591905 1.9979131 2.00591905h4.0020869v2h-7.9931517c-1.6606364 0-3.0068483 1.3422643-3.0068483 2.9987856v7.0024288c0 1.6561835 1.3359915 2.9987856 3.0068483 2.9987856zm-5-21.5v4.49121523c0 .55713644.4506511 1.00878477.9967388 1.00878477h3.7032124zm-3.0054385 9.5c-1.1015659 0-1.9945615.9001762-1.9945615 1.992017v7.015966c0 1.1001606.9023438 1.992017 1.9945615 1.992017h17.010877c1.1015659 0 1.9945615-.9001762 1.9945615-1.992017v-7.015966c0-1.1001606-.9023438-1.992017-1.9945615-1.992017zm10.5054385 9h-.5l-1.5-3-1.5 3h-.5-.5v-7h1v5l1-2h.5.5l1 2v-5h1v7zm-10.5-7v7h2.9951185c1.1072655 0 2.0048815-.8865548 2.0048815-2.0059191v-2.9881618c0-1.1078385-.8938998-2.0059191-2.0048815-2.0059191zm1 1v5h2.0010434c.5517085 0 .9989566-.4437166.9989566-.9998075v-3.000385c0-.5521784-.4426603-.9998075-.9989566-.9998075zm15 3v2h-2.0001925c-.5560909 0-.9998075-.4476291-.9998075-.9998075v-3.000385c0-.5560909.4476291-.9998075.9998075-.9998075h3.0001925v-1h-2.9951185c-1.1072655 0-2.0048815.8865548-2.0048815 2.0059191v2.9881618c0 1.1078385.8938998 2.0059191 2.0048815 2.0059191h2.9951185v-.75-2.25-1h-3v1z" fill="#000000"/>
                            </g>
                        </svg>
                    </a>
                <?php } ?>
                <?php if($single_task_details['action_tab_flag']['revised_marking']){ ?>

                    <a href="javascript:void(0)" class="btn btn-md btn-clean btn-icon btn-icon-md add_revised_marking_comment" title="Revised Marking" data-toggle="modal" data-target="#add_revised_modal" work_order_no="<?php echo $single_task_details['work_order_no']; ?>" type="<?php echo $single_task_details['type']; ?>">
                        <i class="fa fa-reply-all"></i>
                    </a>
                <?php } ?>

                <?php if($single_task_details['action_tab_flag']['marking_document'] && $this->session->userdata('task_access')['task_list_action_marking_view_access']){ ?>

                    <a href="javascript:void(0)" class="btn btn-md btn-clean btn-icon btn-icon-md marking_download" title="Marking Download" data-toggle="modal" data-target="#document_modal" work_order_no="<?php echo $single_task_details['work_order_no']; ?>" type="marking">
                        <i class="fa fa-check-circle kt-font-success"></i>
                    </a>
                <?php } ?>
                <?php if($single_task_details['action_tab_flag']['mtc_document'] && $this->session->userdata('task_access')['task_list_action_mtc_view_access']){ ?>

                    <a href="javascript:void(0)" class="btn btn-md btn-clean btn-icon btn-icon-md mtc_download" title="MTC Download" data-toggle="modal" data-target="#document_modal" work_order_no="<?php echo $single_task_details['work_order_no']; ?>" type="mtc">
                        <i class="fa fa-check-double kt-font-success"></i>
                    </a>
                <?php } ?>
            </td>
        </tr>
    <?php } ?>
<?php } ?>