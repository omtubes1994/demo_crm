<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tasks extends MX_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('user_id')){
			redirect('login', 'refresh');
			exit;
		}
		$this->load->model('tasks_model');
		$this->load->model('common/common_model');
		error_reporting(0);
	}

	function index(){

		redirect('tasks/task_management_list');
	}

	function add_task(){
		if(!empty($this->input->post())){

		}else{
			$data = array();
			$this->load->view('header', array('title' => 'Add / Edit Task'));
			$this->load->view('sidebar', array('title' => 'Add / Edit Task'));
			$this->load->view('add_task', $data);
			$this->load->view('footer');
		}
	}

	function add_task_ajax(){
		$insert_arr = array(
			'lead_id' => $this->input->post('task_lead_id'),
			'member_id' => $this->input->post('task_member_id'),
			'lead_source' => $this->input->post('lead_source'),
			'task_detail' => $this->input->post('task_detail'),
			'deadline' => date('Y-m-d H:i:s', strtotime($this->input->post('deadline'))),
			'created_by' => $this->session->userdata('user_id'),
			'assigned_to' => $this->session->userdata('user_id'),
			'status' => 'Open',
			'entered_on' => date('Y-m-d H:i:s')
		);
		$this->tasks_model->insertData('tasks', $insert_arr);
	}

	function list(){

		redirect('tasks/task_management_list');
		// $this->load->view('header', array('title' => 'Task List'));
		// $this->load->view('sidebar', array('title' => 'Task List'));
		// $this->load->view('task_list');
		// $this->load->view('footer');
	}

	function list_data(){
		$search = array();
		/*foreach ($this->input->post('columns') as $key => $value) {
			$search_key = '';
			if($key == 1){
				$search_key = 'IMPORTER_NAME';
			}else if($key == 2){
				$search_key = 'lead_type';
			}

			$search[$search_key] = $this->input->post('columns')[$key]['search']['value'];
		}*/
		$search['datewise'] = $this->input->post('searchByDatewise');
		$order_by = $this->input->post('columns')[$this->input->post('order')[0]['column']]['data'];
		if($order_by == 'record_id'){
			$order_by = 'task_id';
		}
		$dir = $this->input->post('order')[0]['dir'];
		$records = $this->tasks_model->getTaskListData($this->input->post('start'), $this->input->post('length'), $search, $order_by, $dir);
		$data['iTotalDisplayRecords'] = $data['iTotalRecords'] = $this->tasks_model->getTaskListCount($search);
		$data['aaData'] = $records;
		//echo "<pre>";print_r($data);
		echo json_encode($data);
	}

	function updateTask(){
		switch ($this->input->post('lead_source')) {
			case 'primary':
				$insert_arr = array(
					'lead_id' => $this->input->post('task_lead_id'),
					'member_id' => $this->input->post('task_member_id'),
					'comments' => $this->input->post('contact_details'),
					'connected_on' => date('Y-m-d', strtotime($this->input->post('contact_date'))),
					'entered_on' => date('Y-m-d H:i:s'),
					'connect_mode' => $this->input->post('connect_mode'),
					'email_sent' => $this->input->post('email_sent')
				);
				$this->tasks_model->insertDataDB2('lead_connects', $insert_arr);
				break;

			case 'hetro leads':
				$insert_arr = array(
					'lead_id' => $this->input->post('task_lead_id'),
					'member_id' => $this->input->post('task_member_id'),
					'comments' => $this->input->post('contact_details'),
					'connected_on' => date('Y-m-d', strtotime($this->input->post('contact_date'))),
					'entered_on' => date('Y-m-d H:i:s'),
					'connect_mode' => $this->input->post('connect_mode'),
					'email_sent' => $this->input->post('email_sent')
				);
				$this->tasks_model->insertData('lead_connects', $insert_arr);
				break;
		}

		$update_arr = array(
			'status' => $this->input->post('task_status'),
			'modified_on' => date('Y-m-d H:i:s'),
			'modified_by' => $this->session->userdata('user_id')
		);

		if($this->input->post('task_status') == 'Open'){
			$this->db->query('update tasks set last_deadline = deadline where task_id = '.$this->input->post('task_id'));
			$update_arr['deadline'] = date('Y-m-d H:i:s', strtotime($this->input->post('deadline')));
			$update_arr['is_postponed'] = 'Y';
		}
		$this->tasks_model->updateData('tasks', $update_arr, array('task_id' => $this->input->post('task_id')));
	}
	
	public function task_management_list(){
		
		$data = array();


		// echo "<pre>";print_r($data);echo"</pre><hr>";die;
		$this->load->view('header', array('title' => 'Task List'));
		$this->load->view('sidebar', array('title' => 'Task List'));
		$this->load->view('tasks/task_management_list_index', $data);
		$this->load->view('footer');
	}

	public function ajax_function() {

		if($this->input->is_ajax_request()){
			$post_details = $this->input->post();
			$response['status'] = 'successful';	
			switch ($post_details['call_type']) {

				case 'change_task_status':

					$update_data = array('task_status'=> $post_details['task_status']);
					$update_data['done_time'] = null;
					if($post_details['task_status'] == 'Approved'){
						
						// die('came in');
						$update_data['done_time'] = $this->get_indian_time();
						$update_data['approved_by'] = $this->session->userdata('user_id');
						$task_management_details = $this->common_model->get_dynamic_data_sales_db('work_order_no', array('id'=> $post_details['id']), 'task_management', 'row_array');
						if($post_details['type'] == 'logistics_weight_and_dimension' && !empty($task_management_details)){

							$this->common_model->insert_data_sales_db('task_management', array('type'=> 'logistics_invoice_and_pl', 'user_id'=> 40, 'name'=> 'Logistics invoice and pi', 'work_order_no'=> $task_management_details['work_order_no']));
						}
						$this->add_task_notification($post_details['type'], $post_details['id'], 'task_is_mark_done', $task_management_details['work_order_no']);
					}
					
					$this->common_model->update_data_sales_db('task_management', $update_data, array('id'=> $post_details['id']));
				break;
				case 'get_comment_history_and_add_new_form':
					
					$chat_details = $this->common_model->get_dynamic_data_sales_db('work_order_no, comment', array('id'=> $post_details['id']), 'task_management', 'row_array');
					$user_list = array_column($this->common_model->get_dynamic_data_sales_db('user_id, name', array(), 'users'), 'name', 'user_id');
					$data = array('chat_details'=> array());
					foreach (json_decode($chat_details['comment'], true) as $chat_key => $single_chat_details) {
								
						$data['chat_details'][$chat_key]['profile_path'] = "assets/media/users/default.jpg";
						$data['chat_details'][$chat_key]['user_name'] = $this->create_first_name($user_list[$single_chat_details['user_id']]);
						$data['chat_details'][$chat_key]['tat'] = $this->create_tat($single_chat_details['date_time']);
						$data['chat_details'][$chat_key]['message'] = $single_chat_details['message'];
					}
					$response['chat_history'] = $this->load->view('tasks/chat_history', $data, true);
					$response['work_order_no'] = $chat_details['work_order_no'];
					$response['id'] = $post_details['id'];
				break;
				case 'save_task_comment_add_form':

					$chat_details = array();
					$old_chat_details = $this->common_model->get_dynamic_data_sales_db('comment', array('id'=> $post_details['id']), 'task_management', 'row_array');
					if(!empty($old_chat_details)){

						$chat_details = json_decode($old_chat_details['comment'], true);
					}
					$chat_details[] = array(
										'message'=> $post_details['comment'],
										'user_id'=> $this->session->userdata('user_id'),
										'date_time'=> $this->get_indian_time(),
									);
					if(!empty($chat_details)){

						$this->common_model->update_data_sales_db('task_management', array('comment'=> json_encode($chat_details)), array('id'=> $post_details['id']));
					}
				break;
				case 'upload_task_document':
					
					if(!empty($post_details['task_id']) && !empty($post_details['work_order_no']) && !empty($post_details['type']) && !empty($post_details['file_type'])){

						$response['msg'] = 'File is uploaded successfully!!!';
						$response['title'] = 'File Upload Done!';

						if($post_details['type'] == 'marking'){

							$marking_details = $this->common_model->get_dynamic_data_sales_db('task_id, count(work_order_no) as file_count', array('task_id'=> $post_details['task_id'], 'task_type'=>$post_details['type'], 'status'=>'Active'), 'google_cloud_upload_logs', 'row_array');
							if($marking_details['file_count'] >= 1){

								$file_name = $post_details['type'].'_revised_'.$post_details['work_order_no'].'_'.$this->session->userdata('user_id').'_'.time();
							}else{

								$file_name = $post_details['type'].'_'.$post_details['work_order_no'].'_'.$this->session->userdata('user_id').'_'.time();
							}
						}else{

							$file_name = $post_details['type'].'_'.$post_details['work_order_no'].'_'.$this->session->userdata('user_id').'_'.time();
						}
						$insert_array = array();
						$insert_array = array(

							'task_id'       => $post_details['task_id'],
							'user_id'       => $this->session->userdata('user_id'),
							'work_order_no' => $post_details['work_order_no'],
							'task_type'     => $post_details['type'],
							'file_name'     => $file_name,
							'add_time'      => date('Y-m-d H:i:s'),
							'file_type'     => $post_details['file_type']
						);
						$insert_id = $this->common_model->insert_data_sales_db(
													'google_cloud_upload_logs',
													$insert_array
												);
						$return_response = $this->upload_to_google_cloud($post_details['work_order_no'], $post_details['type'], $file_name);
						$response['status'] = $return_response['status'];
						if($return_response['status'] == 'successful' && $return_response['file_name'] != '' && $return_response['google_response'] != ''){

							$this->common_model->update_data_sales_db('google_cloud_upload_logs', array('upload_status'=> 'Done', 'file_name'=> $return_response['file_name'], 'google_response'=> $return_response['google_response']), array('id'=> $insert_id));
							$task_insert_array = array();
							$get_file_uploaded_details = $this->common_model->get_dynamic_data_sales_db('file_uploaded_details', array('id'=> $post_details['task_id']), 'task_management', 'row_array');
							$all_file_uploaded_details = array();
							$create_new_insert_array_flag = true;
							if(!empty($get_file_uploaded_details['file_uploaded_details'])){

								$all_file_uploaded_details = json_decode($get_file_uploaded_details['file_uploaded_details'], true);
								foreach ($all_file_uploaded_details as $all_file_uploaded_details_key => $single_file_uploaded_details) {
									
									if($single_file_uploaded_details['file_type'] == $post_details['file_type']){

										$all_file_uploaded_details[$all_file_uploaded_details_key]['file_name'] = $return_response['file_name'];
										$create_new_insert_array_flag = false;
									}
								}
							}
							if($create_new_insert_array_flag){

								$all_file_uploaded_details[] = array('file_type'=> $post_details['file_type'], 'file_name'=> $return_response['file_name']);
							}
							
							$task_insert_array['file_uploaded_details'] = json_encode($all_file_uploaded_details);
							$this->add_task_notification($post_details['type'], $post_details['task_id'], $post_details['file_type'], $post_details['work_order_no']);
							$this->common_model->update_data_sales_db('task_management', $task_insert_array, array('id'=> $post_details['task_id']));
							
						}else {

							$response['title'] = 'File Upload Failed!';
							$response['msg'] = $return_response['msg'];
						}
					}
				break;
				case 'get_task_management_list_details':
					
					if(
						(
							in_array($this->session->userdata('role'), array(1, 6, 7, 17, 24))
							|| 
							in_array($this->session->userdata('role'), array(8, 10, 11, 18, 21))
						)
						&& 
						!empty($post_details['type'])
						&&
						!empty($post_details['tab_name']) 
						&&
						!empty($post_details['production_tab']) 
					){

						// echo "<pre>";print_r($post_details);echo"</pre><hr>";die;
						$data = array();
						if($post_details['tab_name'] != 'recent_activities'){

							$explode_tab_name = explode("_and_", $post_details['tab_name']);
							$where_string = "task_management.status = 'Active' AND task_management.type = '{$post_details['type']}' AND task_management.task_status IN ('".$explode_tab_name[0]."', '".$explode_tab_name[1]."')";

							switch ($post_details['type']) {
								case 'drawing':
									
									$where_string .= " AND production_process_information.production_status != 'dispatched'";
									if(!in_array($this->session->userdata('user_id'), array(2, 23, 33, 65, 73, 120, 158, 276, 309, 354))){

										$where_string .= " AND task_management.user_id = ".$this->session->userdata('user_id');
									}
								break;
								case 'marking':

									if(!in_array($this->session->userdata('user_id'), array(2, 23, 33, 65, 73, 120, 158, 169, 190, 297, 302, 354))){

										$where_string .= " AND task_management.user_id = ".$this->session->userdata('user_id');
									}
								break;
								case 'mtc':

									if(!in_array($this->session->userdata('user_id'), array(2, 23, 33, 65, 73, 120, 158, 297, 302, 354))){

										$where_string .= " AND task_management.user_id = ".$this->session->userdata('user_id');
									}
								break;
								case 'logistics_weight_and_dimension':
									
									if(!in_array($this->session->userdata('user_id'), array(2, 23, 40, 65, 73, 158))){

										$where_string .= " AND task_management.user_id = ".$this->session->userdata('user_id');
									}
								break;
								case 'logistics_invoice_and_pl':
									
									if(!in_array($this->session->userdata('user_id'), array(2, 23, 40, 65, 73, 158))){

										$where_string .= " AND task_management.user_id = ".$this->session->userdata('user_id');
									}
								break;
								case 'quality_inspection':
									
									if(!in_array($this->session->userdata('user_id'), array(2, 23, 33, 65, 73, 83, 158, 120, 190, 267, 272, 276, 354))){

										$where_string .= " AND task_management.user_id = ".$this->session->userdata('user_id');
									}
								break;
							}

							switch ($post_details['production_tab']) {
								case 'pending_order':

									$where_string .= " AND production_process_information.production_status NOT IN('pending_order', 'semi_ready', 'merchant_trade', 'mtt', 'import', 'query', 'mtt_rfd', 'in_transit_import', 'in_transit_mtt', 'ready_for_dispatch', 'Dispatched', 'on_hold', 'order_cancelled')";
								break;
								case 'semi_ready':

									$where_string .= " AND production_process_information.production_status = 'semi_ready'";
								break;
								case 'ice':

									$where_string .= " AND production_process_information.production_status IN ('merchant_trade','mtt')";
								break;
								case 'import':

									$where_string .= " AND production_process_information.production_status = 'import'";
								break;
								case 'query':

									$where_string .= " AND production_process_information.production_status = 'query'";
								break;
								case 'in_transit_ice':

									$where_string .= " AND production_process_information.production_status IN ('mtt_rfd','in_transit_import','in_transit_mtt')";
								break;
								case 'rfd':

									$where_string .= " AND production_process_information.production_status = 'ready_for_dispatch'";
								break;
								case 'dispatch_order':

									$where_string .= " AND production_process_information.production_status = 'Dispatched'";
								break;
								case 'on_holder_order':

									$where_string .= " AND production_process_information.production_status = 'on_hold'";
								break;
								case 'order_cancelled':

									$where_string .= " AND production_process_information.production_status = 'order_cancelled'";
								break;
							}

							$order_by_string = "work_order_no desc";
							if(!empty($post_details['order_by_name']) && !empty($post_details['order_by_value'])){

								$order_by_string = $post_details['order_by_name']." ".$post_details['order_by_value'];
							}
							$where_string .= $this->create_where_on_search_filter($post_details['search_filter_form']);
							
							$data = $this->create_task_management_list_data($where_string, $order_by_string, $post_details['limit'], $post_details['offset']);

							$data['search_filter_form_data'] = $this->prepare_search_filter_data($post_details['search_filter_form'], $post_details['type']);

							$response['total'] = 0;
							if(!empty($post_details['production_tab']) && $post_details['type'] == 'marking' || $post_details['type'] == 'mtc'){

								$highchart_data = $this->tasks_model->get_production_status_wise_count($where_string);
								$user_details = array_column($this->common_model->get_dynamic_data_sales_db('name, user_id', array('status'=>1), 'users'), 'name', 'user_id');

								foreach ($highchart_data as $single_details) {
									if(!empty($user_details[$single_details['user_id']])){

										$response['task_highchart_category'][] = $user_details[$single_details['user_id']];
										$response['task_highchart_data'][0]['data'][] = (int)$single_details['count(*)'];
										$response['total'] += (int)$single_details['count(*)'];
									}
								}
							}

							// echo "<pre>";print_r($response);echo"</pre><hr>";die;
							$response['task_management_search_filter'] = $this->load->view('tasks/search_filter_form', $data, true);
							$response['task_management_list_body'] = $this->load->view('tasks/task_management_list_body', $data, true);
							$response['task_management_list_paggination'] = $this->load->view('tasks/task_management_list_paggination', $data, true);
						}else{

							$task_type = $this->common_model->get_all_conditional_data_sales_db('id, work_order_no', array('type'=>$post_details['type'], 'status'=> 'Active'), 'task_management', 'result_array', array(), array('column_name'=> 'id', 'column_value'=> 'desc'));

							foreach ($task_type as $single_task_type_details) {

								$notification_data = $this->common_model->get_all_conditional_data_sales_db('task_id, user_id, name, work_order_no, add_time', array('task_id'=>$single_task_type_details['id'], 'status'=> 'Active'), 'task_notification', 'result_array', array(), array('column_name'=> 'add_time', 'column_value'=> 'desc'));

								foreach ($notification_data as $single_task_notification_details) {

									$task_notification_data['notification_list'][date('Y-m-d', strtotime($single_task_notification_details['add_time']))][] = $single_task_notification_details;
								}
							}
							// echo "<pre>";print_r($task_notification_data);echo"</pre><hr>";die;
							$response['task_management_recent_activities'] = $this->load->view('tasks/task_recent_activities_listing', $task_notification_data, true);
						}
						
					}
				break;
				case 'get_upload_document_tab_name':
					
					$response['upload_tab_html'] = '';
					if(!empty($post_details['task_type'])){

						switch ($post_details['task_type']) {
							case 'drawing':
								
								$response['upload_tab_html'] = '<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type get_upload_document" document_name="vendor" style="padding: 10px 14px;">
									<span class="kt-widget__section">
										<span class="kt-widget__desc">
											Vendor copy
										</span>
									</span>
								</a>
								<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type get_upload_document" document_name="client" style="padding: 10px 14px;">
									<span class="kt-widget__section">
										<span class="kt-widget__desc">
											Client copy
										</span>
									</span>
								</a>';
							break;
							case 'marking':
								
								$response['upload_tab_html'] = '<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type get_upload_document" document_name="marking" style="padding: 10px 14px;">
									<span class="kt-widget__section">
										<span class="kt-widget__desc">
											Marking
										</span>
									</span>
								</a>';
							break;
							case 'mtc':
								
								$response['upload_tab_html'] = '<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type get_upload_document" document_name="mtc" style="padding: 10px 14px;">
									<span class="kt-widget__section">
										<span class="kt-widget__desc">
											MTC
										</span>
									</span>
								</a>';
							break;
							case 'logistics_invoice_and_pl':
								
								$response['upload_tab_html'] = '<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type get_upload_document" document_name="invoice" style="padding: 10px 14px;">
									<span class="kt-widget__section">
										<span class="kt-widget__desc">
											Invoice
										</span>
									</span>
								</a>
								<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type get_upload_document" document_name="proforma_invoice" style="padding: 10px 14px;">
									<span class="kt-widget__section">
										<span class="kt-widget__desc">
											Proforma Invoice
										</span>
									</span>
								</a>';
							break;
							case 'quality_inspection':
								
								$response['upload_tab_html'] = '<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type get_upload_document" document_name="pmi_photos" style="padding: 10px 14px;">
									<span class="kt-widget__section">
										<span class="kt-widget__desc">
											PMI Photos
										</span>
									</span>
								</a>
								<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type get_upload_document" document_name="dimension_photos" style="padding: 10px 14px;">
									<span class="kt-widget__section">
										<span class="kt-widget__desc">
											Dimension Photos
										</span>
									</span>
								</a>
								<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type get_upload_document" document_name="marking_photos" style="padding: 10px 14px;">
									<span class="kt-widget__section">
										<span class="kt-widget__desc">
											Marking Photos
										</span>
									</span>
								</a>
								<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type get_upload_document" document_name="finishing_photos" style="padding: 10px 14px;">
									<span class="kt-widget__section">
										<span class="kt-widget__desc">
											Finishing Photos
										</span>
									</span>
								</a>
								<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type get_upload_document" document_name="packing_and_dispatch" style="padding: 10px 14px;">
									<span class="kt-widget__section">
										<span class="kt-widget__desc">
											Packing & Dispatch
										</span>
									</span>
								</a>
								<a href="javascript:void(0);" class="kt-widget__item change_task_management_list_type get_upload_document" document_name="reports_pdf" style="padding: 10px 14px;">
									<span class="kt-widget__section">
										<span class="kt-widget__desc">
											Reports pdf
										</span>
									</span>
								</a>';
							break;
						}
					}
				break;
				case 'get_upload_document':
					
					if(!empty($post_details['task_id']) && !empty($post_details['task_type']) && !empty($post_details['document_name'])){

						$data = array('file_details' => array());
						$all_file_uploaded_details = array();
						$task_management_details = $this->common_model->get_dynamic_data_sales_db('file_uploaded_details', array('id'=> $post_details['task_id']), 'task_management', 'row_array');
						if(!empty($task_management_details['file_uploaded_details'])){

							$all_file_uploaded_details = array_column(json_decode($task_management_details['file_uploaded_details'], true), 'file_name', 'file_type');
						}
						switch ($post_details['document_name']) {

							case 'vendor':
								
								$key = count($data['file_details']);
								$data['file_details'][$key]['document_upload_name'] = 'Vendor copy';
								$data['file_details'][$key]['version_tab_div'] = $this->get_different_version_number_html($post_details['task_id'], 'vendor');
								$data['file_details'][$key]['kt_uppy_id'] = 'drawing_vendor';
								$data['file_details'][$key]['image_path_div_class'] = 'vendor';
								$data['file_details'][$key]['image_path'] = '';
								if(!empty($all_file_uploaded_details['vendor'])){

									$data['file_details'][$key]['image_path'] = $this->add_image_html_based_on_document_type($all_file_uploaded_details['vendor']);	
								}
							break;
							case 'client':
								
								$key = count($data['file_details']);
								$data['file_details'][$key]['document_upload_name'] = 'Client copy';
								$data['file_details'][$key]['version_tab_div'] = $this->get_different_version_number_html($post_details['task_id'], 'client');
								$data['file_details'][$key]['kt_uppy_id'] = 'drawing_client';
								$data['file_details'][$key]['image_path_div_class'] = 'client';
								$data['file_details'][$key]['image_path'] = '';
								if(!empty($all_file_uploaded_details['client'])){

									$data['file_details'][$key]['image_path'] = $this->add_image_html_based_on_document_type($all_file_uploaded_details['client']);	
								}
							break;
							case 'marking':
								
								$key = count($data['file_details']);
								$data['file_details'][$key]['document_upload_name'] = 'Upload Marking';
								$data['file_details'][$key]['version_tab_div'] = $this->get_different_version_number_html($post_details['task_id'], 'marking');
								$data['file_details'][$key]['kt_uppy_id'] = 'marking';
								$data['file_details'][$key]['image_path_div_class'] = 'marking';
								$data['file_details'][$key]['image_path'] = '';
								if(!empty($all_file_uploaded_details['marking'])){

									$data['file_details'][$key]['image_path'] = $this->add_image_html_based_on_document_type($all_file_uploaded_details['marking']);	
								}
							break;
							case 'mtc':
								
								$key = count($data['file_details']);
								$data['file_details'][$key]['document_upload_name'] = 'Upload MTC';
								$data['file_details'][$key]['version_tab_div'] = $this->get_different_version_number_html($post_details['task_id'], 'mtc');
								$data['file_details'][$key]['kt_uppy_id'] = 'mtc';
								$data['file_details'][$key]['image_path_div_class'] = 'mtc';
								$data['file_details'][$key]['image_path'] = '';
								if(!empty($all_file_uploaded_details['mtc'])){

									$data['file_details'][$key]['image_path'] = $this->add_image_html_based_on_document_type($all_file_uploaded_details['mtc']);	
								}
							break;
							case 'invoice':
								
								$key = count($data['file_details']);
								$data['file_details'][$key]['document_upload_name'] = 'Upload Invoice';
								$data['file_details'][$key]['version_tab_div'] = $this->get_different_version_number_html($post_details['task_id'], 'invoice');
								$data['file_details'][$key]['kt_uppy_id'] = 'invoice';
								$data['file_details'][$key]['image_path_div_class'] = 'invoice';
								$data['file_details'][$key]['image_path'] = '';
								if(!empty($all_file_uploaded_details['invoice'])){

									$data['file_details'][$key]['image_path'] = $this->add_image_html_based_on_document_type($all_file_uploaded_details['invoice']);	
								}
							break;
							case 'proforma_invoice':
								
								$key = count($data['file_details']);
								$data['file_details'][$key]['document_upload_name'] = 'Upload profoma Invoice';
								$data['file_details'][$key]['version_tab_div'] = $this->get_different_version_number_html($post_details['task_id'], 'proforma_invoice');
								$data['file_details'][$key]['kt_uppy_id'] = 'proforma_invoice';
								$data['file_details'][$key]['image_path_div_class'] = 'proforma_invoice';
								$data['file_details'][$key]['image_path'] = '';
								if(!empty($all_file_uploaded_details['proforma_invoice'])){

									$data['file_details'][$key]['image_path'] = $this->add_image_html_based_on_document_type($all_file_uploaded_details['proforma_invoice']);	
								}
							break;
							case 'pmi_photos':

								$key = count($data['file_details']);
								$data['file_details'][$key]['document_upload_name'] = 'Upload PMI Photos';
								$data['file_details'][$key]['version_tab_div'] = $this->get_different_version_number_html($post_details['task_id'], 'pmi_photos');
								$data['file_details'][$key]['kt_uppy_id'] = 'pmi_photos';
								$data['file_details'][$key]['image_path_div_class'] = 'pmi_photos';
								$data['file_details'][$key]['image_path'] = '';
								if(!empty($all_file_uploaded_details['pmi_photos'])){

									$data['file_details'][$key]['image_path'] = $this->add_image_html_based_on_document_type($all_file_uploaded_details['pmi_photos']);	
								}
							break;
							case 'dimension_photos':
								
								$key = count($data['file_details']);
								$data['file_details'][$key]['document_upload_name'] = 'Upload Dimension Photos';
								$data['file_details'][$key]['version_tab_div'] = $this->get_different_version_number_html($post_details['task_id'], 'dimension_photos');
								$data['file_details'][$key]['kt_uppy_id'] = 'dimension_photos';
								$data['file_details'][$key]['image_path_div_class'] = 'dimension_photos';
								$data['file_details'][$key]['image_path'] = '';
								if(!empty($all_file_uploaded_details['dimension_photos'])){

									$data['file_details'][$key]['image_path'] = $this->add_image_html_based_on_document_type($all_file_uploaded_details['dimension_photos']);	
								}
							break;
							case 'marking_photos':
								
								$key = count($data['file_details']);
								$data['file_details'][$key]['document_upload_name'] = 'Upload Marking Photos';
								$data['file_details'][$key]['version_tab_div'] = $this->get_different_version_number_html($post_details['task_id'], 'marking_photos');
								$data['file_details'][$key]['kt_uppy_id'] = 'marking_photos';
								$data['file_details'][$key]['image_path_div_class'] = 'marking_photos';
								$data['file_details'][$key]['image_path'] = '';
								if(!empty($all_file_uploaded_details['marking_photos'])){

									$data['file_details'][$key]['image_path'] = $this->add_image_html_based_on_document_type($all_file_uploaded_details['marking_photos']);	
								}
							break;
							case 'finishing_photos':
								
								$key = count($data['file_details']);
								$data['file_details'][$key]['document_upload_name'] = 'Upload Finishing Photos';
								$data['file_details'][$key]['version_tab_div'] = $this->get_different_version_number_html($post_details['task_id'], 'finishing_photos');
								$data['file_details'][$key]['kt_uppy_id'] = 'finishing_photos';
								$data['file_details'][$key]['image_path_div_class'] = 'finishing_photos';
								$data['file_details'][$key]['image_path'] = '';
								if(!empty($all_file_uploaded_details['finishing_photos'])){

									$data['file_details'][$key]['image_path'] = $this->add_image_html_based_on_document_type($all_file_uploaded_details['finishing_photos']);	
								}
							break;
							case 'packing_and_dispatch':
								
								$key = count($data['file_details']);
								$data['file_details'][$key]['document_upload_name'] = 'Upload Packing & Dispatch Photos';
								$data['file_details'][$key]['version_tab_div'] = $this->get_different_version_number_html($post_details['task_id'], 'packing_and_dispatch');
								$data['file_details'][$key]['kt_uppy_id'] = 'packing_and_dispatch';
								$data['file_details'][$key]['image_path_div_class'] = 'packing_and_dispatch';
								$data['file_details'][$key]['image_path'] = '';
								if(!empty($all_file_uploaded_details['packing_and_dispatch'])){

									$data['file_details'][$key]['image_path'] = $this->add_image_html_based_on_document_type($all_file_uploaded_details['packing_and_dispatch']);	
								}
							break;
							case 'reports_pdf':

								$key = count($data['file_details']);
								$data['file_details'][$key]['document_upload_name'] = 'Upload Reports PDF';
								$data['file_details'][$key]['version_tab_div'] = $this->get_different_version_number_html($post_details['task_id'], 'reports_pdf');
								$data['file_details'][$key]['kt_uppy_id'] = 'reports_pdf';
								$data['file_details'][$key]['image_path_div_class'] = 'reports_pdf';
								$data['file_details'][$key]['image_path'] = '';
								if(!empty($all_file_uploaded_details['reports_pdf'])){

									$data['file_details'][$key]['image_path'] = $this->add_image_html_based_on_document_type($all_file_uploaded_details['reports_pdf']);	
								}
							break;
						}
						$response['file_upload_div'] = $this->load->view('task_file_upload', $data, true);
					}
				break;
				case 'change_assigned_to_status':

					$update_data = array('user_id'=> $post_details['user_id']);					
					$this->common_model->update_data_sales_db('task_management', $update_data, array('id'=> $post_details['id']));
				break;
				case 'get_line_item_data':
					
					$response['quotations_line_item_body'] = '';
					if(!empty($post_details['work_order_no'])){


						$quotation_mst_details = $this->common_model->get_dynamic_data_sales_db('quotation_mst_id', array('work_order_no'=> $post_details['work_order_no']), 'quotation_mst', 'row_array');
						if(!empty($quotation_mst_details)){

							$data['quotation_dtl_data'] = $this->common_model->get_dynamic_data_sales_db('*', array('quotation_mst_id '=> $quotation_mst_details['quotation_mst_id']), 'quotation_dtl');
							$data['product_mst'] = array_column($this->common_model->get_dynamic_data_sales_db('id, name', array('status'=> 'Active'), 'product_mst'), 'name', 'id');
							$data['material_mst'] = array_column($this->common_model->get_dynamic_data_sales_db('id, name', array('status'=> 'Active'), 'material_mst'), 'name', 'id');

							// echo "<pre>";print_r($data);echo"</pre><hr>";die;
							$response['quotations_line_item_body'] = $this->load->view('tasks/quotations_line_item_body', $data, true);
						}
					}					
				break;
				case 'get_document_tab_wise':
					
					$google_cloud_upload_logs_vendor = $this->common_model->get_dynamic_data_sales_db('file_name', array('id'=> $post_details['log_id']), 'google_cloud_upload_logs', 'row_array');
					$response['image_path'] = $this->add_image_html_based_on_document_type($google_cloud_upload_logs_vendor['file_name']);
				break;
				case 'get_weight_dimension':
					
					$response['weight_dimension'] = '';
					$weight_dimension_details = $this->common_model->get_dynamic_data_sales_db('weight_dimension', array('id'=> $post_details['task_id']), 'task_management', 'row_array');
					if(!empty($weight_dimension_details)){

						$response['weight_dimension'] = $weight_dimension_details['weight_dimension'];
					}
				break;
				case 'save_weight_dimension':
					
					if(!empty($post_details['task_id']) && !empty($post_details['weight_dimension'])){

						$this->common_model->update_data_sales_db('task_management', array('weight_dimension'=> $post_details['weight_dimension']), array('id'=> $post_details['task_id']));
					}
				break;
				case 'download_document':
					
					$this->download_image($post_details['file_name']);
				break;
				case 'get_all_file_upload_history_new':
					
					$main_tab = array(
						'call_type'=> 'main_tab',
						'main_tab'=> array(
							'all'=> array(
										'background_color'=> 'bisque',
										'border'=> '1px solid bisque',
										'image_src'=> '',
										'name'=> 'All',
										'task_type'=> $post_details['task_type'],
										'sub_task_type'=> 'all',
										'work_order_no'=> $post_details['work_order_no']
									)
						)
					);
					$sidebar = array(
						'call_type'=> 'sidebar',
						'sidebar_photos'=> array()
					);
					$main_photos = array(
						'call_type'=> 'main_photos',
						'main_photos_html'=> ''
					);
					switch ($post_details['task_type']) {
						case 'drawing':
								
							$main_tab['main_tab']['vendor'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'Vendor',
								'task_type'=> $post_details['task_type'],
								'sub_task_type'=> 'vendor',
								'work_order_no'=> $post_details['work_order_no']
							);
							$main_tab['main_tab']['client'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'Client',
								'task_type'=> $post_details['task_type'],
								'sub_task_type'=> 'client',
								'work_order_no'=> $post_details['work_order_no']
							);
						break;
						case 'marking':
							
							$main_tab['main_tab']['marking'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'Marking',
								'task_type'=> $post_details['task_type'],
								'sub_task_type'=> 'marking',
								'work_order_no'=> $post_details['work_order_no']
							);
						break;
						case 'mtc':
							
							$main_tab['main_tab']['mtc'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'MTC',
								'task_type'=> $post_details['task_type'],
								'sub_task_type'=> 'mtc',
								'work_order_no'=> $post_details['work_order_no']
							);
						break;
						case 'logistics_invoice_and_pl':
							
							$main_tab['main_tab']['invoice'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'Invoice',
								'task_type'=> $post_details['task_type'],
								'sub_task_type'=> 'invoice',
								'work_order_no'=> $post_details['work_order_no']
							);
							$main_tab['main_tab']['proforma_invoice'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'Proforma Invoice',
								'task_type'=> $post_details['task_type'],
								'sub_task_type'=> 'proforma_invoice',
								'work_order_no'=> $post_details['work_order_no']
							);
						break;
						case 'quality_inspection':
							
							$main_tab['main_tab']['pmi_photos'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'PMI Photos',
								'task_type'=> $post_details['task_type'],
								'sub_task_type'=> 'pmi_photos',
								'work_order_no'=> $post_details['work_order_no']
							);
							$main_tab['main_tab']['dimension_photos'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'Dimension Photos',
								'task_type'=> $post_details['task_type'],
								'sub_task_type'=> 'dimension_photos',
								'work_order_no'=> $post_details['work_order_no']
							);
							$main_tab['main_tab']['marking_photos'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'Marking Photos',
								'task_type'=> $post_details['task_type'],
								'sub_task_type'=> 'marking_photos',
								'work_order_no'=> $post_details['work_order_no']
							);
							$main_tab['main_tab']['finishing_photos'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'Finishing Photos',
								'task_type'=> $post_details['task_type'],
								'sub_task_type'=> 'finishing_photos',
								'work_order_no'=> $post_details['work_order_no']
							);
							$main_tab['main_tab']['packing_and_dispatch'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'Packing & Dispatch',
								'task_type'=> $post_details['task_type'],
								'sub_task_type'=> 'packing_and_dispatch',
								'work_order_no'=> $post_details['work_order_no']
							);
							$main_tab['main_tab']['reports_pdf'] = array(
								'background_color'=> '#fff',
								'border'=> '1px solid #dadce0',
								'image_src'=> '',
								'name'=> 'Reports PDF',
								'task_type'=> $post_details['task_type'],
								'sub_task_type'=> 'reports_pdf',
								'work_order_no'=> $post_details['work_order_no']
							);
						break;
					}
					if($post_details['sub_task_type'] != 'all'){

						$main_tab['main_tab']['all']['background_color'] = '#fff';
						$main_tab['main_tab']['all']['border'] = '1px solid #dadce0';
						$main_tab['main_tab'][$post_details['sub_task_type']]['background_color'] = 'bisque';
						$main_tab['main_tab'][$post_details['sub_task_type']]['border'] = '1px solid bisque';
					}
					$update_history_where = "status = 'Active' AND upload_status = 'Done' AND work_order_no IN(".$post_details['work_order_no'].") AND task_type = '".$post_details['task_type']."'";
					if($post_details['sub_task_type'] != 'all'){

						$update_history_where .= " AND file_type = '".$post_details['sub_task_type']."'";
					}
					$upload_history = $this->common_model->get_dynamic_data_sales_db_null_false('id, file_name, "" as selected_class', $update_history_where, 'google_cloud_upload_logs', 'result_array');
					$sidebar['sidebar_photos'] = $this->add_image_html_based_on_document_type_new($upload_history, "700px", $post_details['sub_task_type']);
					if($post_details['sub_task_type'] == 'all'){
						if(!empty($upload_history[0]) && !empty($upload_history[0]['file_name'])){

							$main_photos['main_photos_html'] = $this->main_photos_html($upload_history[0]['file_name'], "700px");
						}
					}else{

						$kt_uppy_id_array = array(
							'vendor'=> 'drawing_vendor',
							'client'=> 'drawing_client',
							'marking'=> 'marking',
							'mtc'=> 'mtc',
							'invoice'=> 'invoice',
							'proforma_invoice'=> 'proforma_invoice',
							'pmi_photos'=> 'pmi_photos',
							'dimension_photos'=> 'dimension_photos',
							'marking_photos'=> 'marking_photos',
							'finishing_photos'=> 'finishing_photos',
							'packing_and_dispatch'=> 'packing_and_dispatch',
							'reports_pdf'=> 'reports_pdf',
						);
						$main_photos['main_photos_html'] = '<div class="col-xl-12 kt-uppy" id="'.$kt_uppy_id_array[$post_details['sub_task_type']].'"><div class="kt-uppy__dashboard"></div><div class="kt-uppy__progress"></div></div>';
					}

					// echo "<pre>";print_r($main_tab);echo"</pre><hr>";exit;
					$response['work_order_no'] = $post_details['work_order_no'];
					$response['main_tab_html'] = $this->load->view('tasks/task_document_add_view', $main_tab, true);
					$response['sidebar_photos_html'] = $this->load->view('tasks/task_document_add_view', $sidebar, true);
					$response['main_photos_html'] = $this->load->view('tasks/task_document_add_view', $main_photos, true);
				break;

				case 'set_main_photos':
					
					if($post_details['type'] == 'upload_document'){

						$kt_uppy_id_array = array(
							'vendor'=> 'drawing_vendor',
							'client'=> 'drawing_client',
							'marking'=> 'marking',
							'mtc'=> 'mtc',
							'invoice'=> 'invoice',
							'proforma_invoice'=> 'proforma_invoice',
							'pmi_photos'=> 'pmi_photos',
							'dimension_photos'=> 'dimension_photos',
							'marking_photos'=> 'marking_photos',
							'finishing_photos'=> 'finishing_photos',
							'packing_and_dispatch'=> 'packing_and_dispatch',
							'reports_pdf'=> 'reports_pdf',
						);
						$response['main_photos_html'] = '<div class="col-xl-12 kt-uppy" id="'.$kt_uppy_id_array[$post_details['sub_task_type']].'"><div class="kt-uppy__dashboard"></div><div class="kt-uppy__progress"></div></div>';
					}else if($post_details['type'] == 'view_document'){

						$response['main_photos_html'] = $this->main_photos_html($post_details['file_name'], $post_details['main_photos_height']);
					}
				break;

				case 'download_image':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";die;
					if(!empty($post_details['file_name'])){
						$this->download_image($post_details['file_name']);
					}
				break;
				case 'delete_image':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";die;
					if(!empty($post_details['id']) && !empty($post_details['file_name'])){

						$this->common_model->update_data_sales_db('google_cloud_upload_logs', array('status'=>'Inactive', 'update_time'=>date('Y-m-d H:i:s')), array('id'=> $post_details['id']));
					}
				break;
				case 'get_technical_document_history':

					// echo "<pre>";print_r($post_details['work_order_no']);echo"</pre><hr>";die;
					if(!empty($post_details['work_order_no'])) {

						$technical_documents_details = $this->common_model->get_dynamic_data_sales_db('technical_document_file_and_path',array('work_order_no'=>$post_details['work_order_no']), 'production_process_information', 'row_array');

						$response['technical_document_history'] = $this->load->view('production/document_pdf_table', array('document_details'=> $technical_documents_details), true);
					}
				break;
				case 'get_mtc_document_history':
				case 'get_marking_document_history':

					// echo "<pre>";print_r($post_details);echo"</pre><hr>";die;
					if(!empty($post_details['work_order_no']) && !empty($post_details['type'])){

						$document_details = $this->common_model->get_all_conditional_data_sales_db('*', array('work_order_no'=>$post_details['work_order_no'], 'task_type'=>$post_details['type'], 'status'=>'Active'), 'google_cloud_upload_logs', 'result_array', array(), array('column_name'=>'id', 'column_value'=>'desc'));

						foreach($document_details as $single_document_key => $single_document_details){

							$date_time = $this->get_indian_time($single_document_details['add_time']);
							$document_details[$single_document_key]['date_time'] = date('d M, Y g:i A', strtotime($date_time));
						}

						$response['document_history'] = $this->load->view('tasks/document_pdf_table', array('document_details'=> $document_details, 'type'=>$post_details['type']), true);
					}
				break;
				case 'get_revised_marking_comment_history':

					if(!empty($post_details['work_order_no']) && !empty($post_details['type'])){

						$marking_details = $this->common_model->get_dynamic_data_sales_db('*',array('work_order_no'=>$post_details['work_order_no'], 'type'=>$post_details['type']), 'task_management', 'row_array');

						if(!empty($marking_details['revised_comment'])){

							$data['marking_revised'] = $marking_details['marking_revised'];
							$data['revised_comment_details'] = json_decode($marking_details['revised_comment'], true);
						}
						// echo "<pre>";print_r($data);echo"</pre><hr>";die;
						$response['revised_marking_comment_history'] = $this->load->view('tasks/marking_revised_details', $data, true);
					}
				break;
				case 'save_revised_marking_comment':

					// echo "<pre>";print_r($post_details);echo"</pre><hr>";die;
					if(!empty($post_details['work_order_no'])){

						$form_data = array_column($post_details['form_data'], 'value', 'name');
						$update_array[] = array('comment' => $form_data['revised_comment'], 'date_time' => date('Y-m-d H:i:s'));

						$this->common_model->update_data_sales_db('task_management', array('marking_revised'=>$form_data['marking_revised'], 'revised_comment'=>json_encode($update_array)), "work_order_no = '".$post_details['work_order_no']."' AND type IN ('marking', 'quality_inspection')");
					}
				break;
				case 'get_client_name_task':

					$search_client_name = $post_details['client_name'];
					$where_string = "status = 'Active' AND name LIKE '%".$search_client_name."%'";

					$client_details = $this->common_model->get_dynamic_data_sales_db_null_false('id, name', $where_string, 'customer_mst');
					$response['client_list_html'] = '<option value="">Select Name</option>';
					foreach($client_details as $single_details){

						$response['client_list_html'] .= '<option value="'.$single_details['id'].'">'.$single_details['name'].'</option>';
                    }
				break;
				default:
		 			$response['status'] = 'failed';	
		 			$response['message'] = "call type not found";
	 			break;
		 	}
		 	echo json_encode($response);

       	}else{
		 	die('access is not allowed to this function');
		}
	}

	private function create_task_management_list_data($where, $order_by, $limit, $offset){

		$data = array();
		// echo "<pre>";print_r($where);echo"</pre><hr>";die('came in');
		$data = $this->tasks_model->get_task_list_data($where, $order_by, $limit, $offset);
		// echo "<pre>";print_r($data);echo"</pre><hr>";die('came in');
		$user_list = array_column($this->common_model->get_dynamic_data_sales_db('user_id, name', array(), 'users'), 'name', 'user_id');
		$task_assigned_to_quality_documentation_array = $task_assigned_to_quality_inspection_array = $task_assigned_to_design_array = array();
		foreach ($this->common_model->get_dynamic_data_sales_db_null_false('user_id, name, role', "status = 1 AND role IN(8, 10, 11, 21)", 'users') as $single_details) {

			if(in_array($single_details['role'], array(8, 10, 11))){

				if(in_array($single_details['user_id'], array(189, 211, 260, 297, 302, 311))){

					$task_assigned_to_quality_documentation_array[$single_details['user_id']] = array('user_id' => $single_details['user_id'], 'name' => $single_details['name'], 'class' => "");
				}else if(!in_array($single_details['role'], array(8))){

					$task_assigned_to_quality_inspection_array[$single_details['user_id']] = array('user_id' => $single_details['user_id'], 'name' => $single_details['name'], 'class' => "");
				}
			}else if(in_array($single_details['role'], array(21))){

				$task_assigned_to_design_array[$single_details['user_id']] = array('user_id' => $single_details['user_id'], 'name' => $single_details['name'], 'class' => "");
			}
		}
		$product_family_span = array(
			'piping'=> '<span class="kt-badge kt-badge--unified-info kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline task_management_font">Piping</span>',
			'instrumentation'=> '<span class="kt-badge kt-badge--unified-warning kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline task_management_font">Instrumentation</span>',
			'precision'=> '<span class="kt-badge kt-badge--unified-orange kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline task_management_font">Precision</span>',
			'tubing'=> '<span class="kt-badge kt-badge--unified-dark kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline task_management_font">Tubing</span>',
			'industrial'=> '<span class="kt-badge kt-badge--unified-success kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline task_management_font">Industrial</span>',
			'fastener'=> '<span class="kt-badge kt-badge--unified-danger kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline task_management_font">Fastener</span>',
			'valve'=> '<span class="kt-badge kt-badge--unified-dark kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline task_management_font">Valve</span>',
		);
		$sr_no = (int)$offset;
		foreach ($data['task_list'] as $task_list_key => $task_list_details) {
			
			$sr_no = $sr_no + 1;
			$data['task_list'][$task_list_key]['sr_no'] = $sr_no;
			$data['task_list'][$task_list_key]['date'] = date('d M, y', strtotime($task_list_details['add_time']));
			// production status name
			$data['task_list'][$task_list_key]['production_status'] = $this->create_production_status_data('single', $task_list_details['production_status']);
			// production family name
			$data['task_list'][$task_list_key]['product_family_span'] = "";
			if(!empty($task_list_details['product_family'])){

				$data['task_list'][$task_list_key]['product_family_span'] = $product_family_span[$task_list_details['product_family']];
			}
			
			$data['task_list'][$task_list_key]['assigned_to_name'] = $this->create_first_name($user_list[$task_list_details['user_id']]);
			$data['task_list'][$task_list_key]['task_status_array'] = array(
				'Pending'     =>	array('value'=> 'Pending',      'display_name'=> 'Pending',      'class'=> ''),
				'Done'        =>	array('value'=> 'Done',         'display_name'=> 'Done',         'class'=> ''),
			);
			if($this->session->userdata('task_access')['task_list_action_status_access']){

				$data['task_list'][$task_list_key]['task_status_array']['Approved'] = array('value'=> 'Approved',     'display_name'=> 'Approved',     'class'=> '');
				$data['task_list'][$task_list_key]['task_status_array']['Not_required'] = array('value'=> 'Not_required',     'display_name'=> 'Not required',     'class'=> '');
			}
			switch ($task_list_details['task_status']) {

				case 'Pending':
					
					$data['task_list'][$task_list_key]['task_status_span'] = '<span class="kt-badge kt-badge--unified-warning kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline task_management_font ">'.$task_list_details['task_status'].'</span>';
					$data['task_list'][$task_list_key]['task_status_array']['Pending']['class'] = 'active';
				break;
				case 'Done':

					$data['task_list'][$task_list_key]['task_status_span'] = '<span class="kt-badge kt-badge--unified-info kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline task_management_font ">'.$task_list_details['task_status'].'</span>';
					$data['task_list'][$task_list_key]['task_status_array']['Done']['class'] = 'active';
				break;
				case 'Approved':

					$data['task_list'][$task_list_key]['task_status_span'] = '<span class="kt-badge kt-badge--unified-success kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline task_management_font ">'.$task_list_details['task_status'].'</span>';
					$data['task_list'][$task_list_key]['task_status_array']['Approved']['class'] = 'active';
				break;
				case 'Not_required':
					
					$data['task_list'][$task_list_key]['task_status_span'] = '<span class="kt-badge kt-badge--unified-danger kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline task_management_font ">'.$task_list_details['task_status'].'</span>';
					$data['task_list'][$task_list_key]['task_status_array']['Not_required']['class'] = 'active';
				break;
			}
			$data['task_list'][$task_list_key]['tat'] = $this->create_tat($task_list_details['done_time']);
			$data['task_list'][$task_list_key]['aproved_by_name'] = $this->create_first_name($user_list[$task_list_details['approved_by']]);
			$data['task_list'][$task_list_key]['comment'] = array();
			$comment_array = json_decode($task_list_details['comment'], true);
			if(!empty($comment_array)){

				$comment_array = $comment_array[count($comment_array)-1];
				$data['task_list'][$task_list_key]['comment']['message'] = $comment_array['message'];
				$data['task_list'][$task_list_key]['comment']['user_name'] = $this->create_first_name($user_list[$comment_array['user_id']]);
				$data['task_list'][$task_list_key]['comment']['tat'] = $this->create_tat($comment_array['date_time']);
			}
			$data['task_list'][$task_list_key]['revised_comment'] = array();
			$revised_comment_array = json_decode($task_list_details['revised_comment'], true);
			if(!empty($revised_comment_array)){

				$revised_comment_array = $revised_comment_array[count($revised_comment_array)-1];
				$data['task_list'][$task_list_key]['revised_comment']['comment'] = $revised_comment_array['comment'];
				$data['task_list'][$task_list_key]['revised_comment']['tat'] = $this->create_tat($revised_comment_array['date_time']);
			}
			$data['task_list'][$task_list_key]['action_tab_flag'] = array(
																		'work_order_sheet'=> true,
																		'proforma_invoice'=> false,
																		'purchase_order'=> false,
																		'task_status'=> true,
																		'comment_query'=> true,
																		'upload_document'=> true,
																		'assigned_to'=> false,
																		'line_item_details'=> false,
																		'weight_dimension'=> false,
																		'sample_drawing'=> false,
																		'technical_document'=> false,
																		'revised_marking'=> false,
																		'marking_document'=> false,
																		'mtc_document'=> false,
																	);
			$data['task_list'][$task_list_key]['query_number'] = '';
			$data['task_list'][$task_list_key]['task_assigned_to_array'] = array();
			switch ($task_list_details['type']) {
				case 'drawing':
					
					$data['task_list'][$task_list_key]['task_assigned_to_array'] = $task_assigned_to_design_array;
					$data['task_list'][$task_list_key]['action_tab_flag']['technical_document'] = true;
					if(in_array($this->session->userdata('user_id'), array(2, 23, 65, 73, 120, 158, 276, 309, 354))){

						$data['task_list'][$task_list_key]['action_tab_flag']['assigned_to'] = true;
					}
				break;
				
				case 'marking':
					
					$data['task_list'][$task_list_key]['task_assigned_to_array'] = $task_assigned_to_quality_documentation_array;
					$data['task_list'][$task_list_key]['action_tab_flag']['technical_document'] = true;
					$data['task_list'][$task_list_key]['action_tab_flag']['revised_marking'] = true;
					if(in_array($this->session->userdata('user_id'), array(2, 23, 65, 73, 120, 158, 297, 302, 354))){

						$data['task_list'][$task_list_key]['action_tab_flag']['assigned_to'] = true;
						$data['task_list'][$task_list_key]['action_tab_flag']['proforma_invoice'] = true;
						$data['task_list'][$task_list_key]['action_tab_flag']['purchase_order'] = true;
					}
				break;
				
				case 'mtc':
					
					$data['task_list'][$task_list_key]['task_assigned_to_array'] = $task_assigned_to_quality_documentation_array;
					$data['task_list'][$task_list_key]['action_tab_flag']['technical_document'] = true;
					if(in_array($this->session->userdata('user_id'), array(2, 23, 65, 73, 120, 158, 297, 302, 354))){

						$data['task_list'][$task_list_key]['action_tab_flag']['assigned_to'] = true;
						$data['task_list'][$task_list_key]['action_tab_flag']['proforma_invoice'] = true;
						$data['task_list'][$task_list_key]['action_tab_flag']['purchase_order'] = true;
					}
				break;

				case 'logistics_weight_and_dimension':
					
					$data['task_list'][$task_list_key]['task_status_array']['Not_required']['class'] = 'kt-hidden';
					$data['task_list'][$task_list_key]['action_tab_flag']['upload_document'] = false;
					$data['task_list'][$task_list_key]['action_tab_flag']['weight_dimension'] = true;
				break;

				case 'logistics_invoice_and_pl':
					
					$data['task_list'][$task_list_key]['task_status_array']['Not_required']['class'] = 'kt-hidden';
				break;
				
				case 'quality_inspection':
					
					$data['task_list'][$task_list_key]['task_assigned_to_array'] = $task_assigned_to_quality_inspection_array;
					$data['task_list'][$task_list_key]['task_status_array']['Not_required']['class'] = 'kt-hidden';
					if(in_array($this->session->userdata('user_id'), array(2, 23, 65, 73, 120, 158, 276, 354))){

						$data['task_list'][$task_list_key]['action_tab_flag']['assigned_to'] = true;
					}
				break;
			}
			if(!empty($task_list_details['user_id']) && !empty($data['task_list'][$task_list_key]['task_assigned_to_array'][$task_list_details['user_id']])){

				$data['task_list'][$task_list_key]['task_assigned_to_array'][$task_list_details['user_id']]['class'] = 'active'; 
			}

			$rfq_details = $this->common_model->get_dynamic_data_sales_db('rfq_mst_id, rfq_no', array('rfq_mst_id'=> $task_list_details['rfq_id']), 'rfq_mst', 'row_array');
			if(!empty($rfq_details)){

				$data['task_list'][$task_list_key]['rfq_no'] = $rfq_details['rfq_no'];
			}

			$client_details = $this->common_model->get_dynamic_data_sales_db('id, name', array('id'=> $task_list_details['client_id']), 'customer_mst', 'row_array');
			if(!empty($client_details)){

				$data['task_list'][$task_list_key]['client_name'] = $client_details['name'];
			}

			$query_master_details = $this->common_model->get_dynamic_data_sales_db('*', array('query_type'=> 'quotation_sample_query', 'query_reference'=> $task_list_details['quote_no']), 'query_master', 'row_array');

				if(!empty($query_master_details)){

					$query_other_information_details = $this->common_model->get_dynamic_data_sales_db('*', array('query_number'=> $query_master_details['query_number']), 'query_other_information', 'row_array');
					if(!empty($query_other_information_details)){
					
						$data['task_list'][$task_list_key]['action_tab_flag']['sample_drawing'] = true;
						$data['task_list'][$task_list_key]['query_number'] = $query_other_information_details['query_number'];
					}
				}

			if(!empty($task_list_details['file_uploaded_details'])){

				$data['task_list'][$task_list_key]['document_upload_color'] = 'kt-svg-icon--success';
			}else{
				$data['task_list'][$task_list_key]['document_upload_color'] = 'kt-svg-icon--info';
			}

			if($task_list_details['marking_revised'] == 'revised'){

				$revised_marking_count = $this->common_model->get_dynamic_data_sales_db('work_order_no, count(work_order_no) as revised_count', array('status'=>'Active','task_type'=>$task_list_details['type'], 'work_order_no'=> $task_list_details['work_order_no']), 'google_cloud_upload_logs');
				if(!empty($revised_marking_count['revised_count'])){

					$data['task_list'][$task_list_key]['revised_count'] = $revised_marking_count['revised_count'];
				}
			}

			if(!empty($task_list_details['technical_document_file_and_path'])){

				$data['task_list'][$task_list_key]['technical_document_color'] = 'kt-svg-icon--success';
			}else{
				$data['task_list'][$task_list_key]['technical_document_color'] = 'kt-svg-icon--info';
			}

			$google_cloud_upload_documention = $this->common_model->get_dynamic_data_sales_db('task_id, task_type', array('work_order_no'=>$task_list_details['work_order_no'], 'status'=>'Active'), 'google_cloud_upload_logs');

			if(!empty($google_cloud_upload_documention)){

				foreach ($google_cloud_upload_documention as $documention_key => $documention_details) {

					if($task_list_details['type'] == 'quality_inspection' && $documention_details['task_type'] == 'marking'){

						$data['task_list'][$task_list_key]['action_tab_flag']['marking_document'] = true;
					}
					if($task_list_details['type'] == 'quality_inspection' && $documention_details['task_type'] == 'mtc'){

						$data['task_list'][$task_list_key]['action_tab_flag']['mtc_document'] = true;
					}
				}
			}
		}
		$data['paggination_data']['limit'] = $limit;
		$data['paggination_data']['offset'] = $offset;

		// echo "<pre>";print_r($data);echo"</pre><hr>";die;
		return $data;
	}

	private function create_first_name($string){

		$return_string = "";

		if(!empty($string)){

			$string_explode = explode(" ", $string);
			$return_string = $string_explode[0];
		}
		return $return_string;
	}

	private function create_tat($date_time){

		$return_tat = '';
		if(empty($date_time) || $date_time === NULL){

			return $return_tat;
		}
		
		// Get the timestamp of the chat message
		$chat_time = strtotime(str_replace("Z", "", $date_time)); // Replace with your chat message timestamp

		// Create DateTime objects for the chat message and current time
		$chat_date = new DateTime("@$chat_time");
		$current_date = new DateTime();
		// Get the difference between the two dates
		$date_diff = $current_date->diff($chat_date);

		// Output the result
		if ($date_diff->y > 0) {

			$return_tat = $date_diff->y . " year(s)";
		}else if ($date_diff->m > 0) {

			$return_tat = $date_diff->m . " month(s)";
		}else if ($date_diff->d > 0) {

			$return_tat = $date_diff->d . " day(s)";
		}else if ($date_diff->h > 0) {

			$return_tat = $date_diff->h . " hour(s)";
		}else if ($date_diff->i > 0) {

			$return_tat = $date_diff->i . " minute(s)";
		}else {

			$return_tat = $date_diff->s . " second(s)";
		}
		
		return $return_tat;
	}

	private function get_indian_time($date_format = 'Y-m-d H:i:s'){

		date_default_timezone_set('Asia/Kolkata');
		return date($date_format);
	}

	private function upload_doc_config() {

		// echo "<pre>";print_r($_FILES);echo"</pre><hr>";exit;
		if($_FILES['file']['name'] === 'blob'){

			$_FILES['file']['name'] = 'abhi.jpeg';
		}
		// echo "<pre>";print_r($_FILES);echo"</pre><hr>";exit;
		$config['upload_path']          = './assets/google_drive/mtc/';
        $config['allowed_types']        = 'pdf|jpeg|gif|jpg|png|';//'jpeg|gif|jpg|png';
        $config['file_name']            = 'mtc_'.$this->session->userdata('user_id').'_'.time();
        $config['overwrite']            = TRUE;

        $this->load->library('upload', $config);

		$data = array('status' => 'successful', 'msg' => '', 'file_name' => '');
        if ( ! $this->upload->do_upload('file'))
        {
			$error = array('error' => $this->upload->display_errors());
			$data['status'] = 'failed';
			$data['msg'] = strip_tags($error['error']);
        }
        else
        {
        	$file_dtls = $this->upload->data();
			$data['file_name'] = $file_dtls['file_name'];
        }
        // echo "<pre>";print_r($data);echo"</pre><hr>";exit;
        return $data;
	}
	private function upload_to_google_cloud($work_order_no, $type, $file_name) {

		// echo "<pre>";print_r($_FILES);echo"</pre><hr>";
		if($_FILES['file']['name'] === 'blob'){

			// echo "<pre>";print_r($_FILES);echo"</pre><hr>";die;
			$_FILES['file']['name'] = 'abhi.jpeg';
		}
        $config['upload_path']   = './assets/google_drive/'.$type.'/';
        $config['allowed_types'] = 'pdf|jpeg|gif|jpg|png|docx'; // Allowed image types
		$config['file_name']     = $file_name;
        $config['overwrite']     = TRUE;
		$config['max_size']      = 50000; // Maximum file size in KB
		// echo "<pre>";print_r($config);echo"</pre><hr>";die;
        $this->load->library('upload', $config);

		$data = array('status' => 'successful', 'msg' => '', 'file_name' => '', 'google_response' => '');
        if ($this->upload->do_upload('file')) {

            $upload_data = $this->upload->data();
            $file_path = $upload_data['full_path'];
			$data['file_name'] = $upload_data['file_name'];
			$this->compress_and_resize_image($file_path, 80);
			$this->add_watermark($file_path, $work_order_no);

			// Upload the file to Google Cloud Storage
            $bucketName = 'documentation_bucket_name'; // Replace with your actual bucket name.
            $localFilePath = $file_path; // Replace with the path to your local image file.
            $destinationObjectName = $upload_data['file_name']; // Replace with the desired object name in the bucket.

            $data['google_response'] = $this->upload_image_direct($bucketName, $localFilePath, $destinationObjectName);
			// echo "<pre>";print_r($google_upload_status);echo"</pre><hr>";die;
            // Optionally, you can delete the local file after uploading to GCS
            // unlink($file_path);
        } else {

			$error = array('error' => $this->upload->display_errors());
			$data['status'] = 'failed';
			$data['msg'] = strip_tags($error['error']);
        }

		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
        return $data;
    }
	private function add_watermark($file_path, $work_order_no){

		$work_order_no_type = $this->common_model->get_dynamic_data_sales_db('type', array('work_order_no'=>$work_order_no), 'production_process_information', 'row_array');
		$logo_type = $this->common_model->get_dynamic_data_sales_db('*', array('type'=>$work_order_no_type['type']), 'watermark_logo', 'row_array');
		// Load the watermark library
		$this->load->library('image_lib');
		// Watermark configuration
		$watermark_config['source_image'] = $file_path;
		$watermark_config['wm_type'] = 'overlay';
		// $watermark_config['wm_overlay_path'] = FCPATH .'assets/media/logos/water_mark_omtubes.jpg'; // Path to your watermark image
		$watermark_config['wm_overlay_path'] = FCPATH .$logo_type['logo_path']; // Path to your watermark image
		$watermark_config['wm_opacity'] = 100; // Opacity of the watermark
		$watermark_config['wm_vrt_alignment'] = 'bottom'; // Vertical alignment
		$watermark_config['wm_hor_alignment'] = 'right'; // Horizontal alignment
		$watermark_config['wm_padding'] = '5';
		$watermark_config['wm_hor_offset'] = '20';
		$watermark_config['wm_vrt_offset'] = '5';
		// Apply the watermark
		$this->image_lib->initialize($watermark_config);
		$this->image_lib->watermark();
		// Clear the library settings for the next image
		$this->image_lib->clear();
	}
	private function compress_and_resize_image($file_path, $quality) {
		$this->load->library('image_lib');
	
		$config['image_library'] = 'gd2';
		$config['source_image'] = $file_path;
		$config['quality'] = $quality; // Set the desired image quality
		$config['maintain_ratio'] = TRUE;
	
		// Resize the image if needed
		$config['width'] = 800; // Set the desired width
		$config['height'] = 600; // Set the desired height
	
		$this->image_lib->initialize($config);
		$this->image_lib->resize();
		$this->image_lib->clear();
	}
	function download_image($destinationObjectName) {
		$credentialsPath = APPPATH . 'third_party/google/documentation-394212-55e47a10bd05.json';
	
		$url = "https://storage.googleapis.com/storage/v1/b/documentation_bucket_name/o/$destinationObjectName?alt=media";
	
		$headers = [
			'Authorization: Bearer ' .$this->get_access_token($credentialsPath),
		];
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
		$this->load->helper('download');
		force_download($destinationObjectName, $response);
	}
	function download_image_old($destinationObjectName) {

		$destinationPath = getenv('USERPROFILE') . "\\Downloads\\$destinationObjectName";

		$credentialsPath = APPPATH . 'third_party/google/documentation-394212-55e47a10bd05.json';
	
		$url = "https://storage.googleapis.com/storage/v1/b/documentation_bucket_name/o/$destinationObjectName?alt=media";
	
		$headers = [
            'Content-Type: application/octet-stream',
            'Authorization: Bearer ' . $this->get_access_token($credentialsPath),
        ];
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);

		if (curl_errno($ch)) {
			echo 'cURL Error: ' . curl_error($ch); exit;
		}
		curl_close($ch);

		if (file_put_contents($destinationPath, $response) !== false) {
			// echo 'File downloaded successfully';
		} else {
			// echo 'Error saving file';
		}

	}
    private function upload_image_direct($bucketName, $localFilePath, $destinationObjectName) {

        $credentialsPath = APPPATH . 'third_party/google/documentation-394212-55e47a10bd05.json';
    
        $fileContent = file_get_contents($localFilePath);
        $base64Data = base64_encode($fileContent);
    
        $url = "https://storage.googleapis.com/upload/storage/v1/b/$bucketName/o?uploadType=media&name=$destinationObjectName";
    
        $headers = [
            'Content-Type: application/octet-stream',
            'Authorization: Bearer ' . $this->get_access_token($credentialsPath),
        ];
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fileContent);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
    
        return $response;
    }
    private function get_access_token($credentialsPath) {
		
        $authUrl = "https://www.googleapis.com/oauth2/v4/token";
    
        $postFields = [
            'grant_type' => 'urn:ietf:params:oauth:grant-type:jwt-bearer',
            'assertion' => $this->get_assertion_jwt($credentialsPath),
        ];
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $authUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postFields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
    
        $decodedResponse = json_decode($response, true);
        return $decodedResponse['access_token'];
    }
    private function get_assertion_jwt($credentialsPath) {

        $now = time();
        $expiration = $now + 3600; // 1 hour from now
    
        $keyJson = json_decode(file_get_contents($credentialsPath), true);
        $privateKey = $keyJson['private_key'];
        $serviceEmail = $keyJson['client_email'];
    
        $header = base64_encode(json_encode(['alg' => 'RS256', 'typ' => 'JWT']));
        $payload = base64_encode(json_encode([
            'iss' => $serviceEmail,
            'scope' => 'https://www.googleapis.com/auth/devstorage.read_write',
            'aud' => 'https://www.googleapis.com/oauth2/v4/token',
            'exp' => $expiration,
            'iat' => $now,
        ]));
    
        $data = "$header.$payload";
        openssl_sign($data, $signature, $privateKey, 'sha256');
        $jwt = "$data." . base64_encode($signature);
    
        return $jwt;
    }

	private function create_production_status_data($call_type='all', $production_status_name = ''){

		$static_production_status =  array(
										'yet_to_start_production'=> array('name'=>'Yet To Start Production','color'=>'dark'),
										'pending_order'=> array('name'=>'Pending Order','color'=>'dark'),										
										'in_production'=> array('name'=>'In Production', 'color'=>'warning'),
										'ready_for_dispatch'=>array('name'=>'Ready For Dispatch', 'color'=>'primary'),
										'delay_in_delivery'=>array('name'=>'Delay in Delivery', 'color'=>'danger'),
										'on_hold'=>array('name'=>'Order On Hold', 'color'=>'gainsboro'),
										'dispatched'=>array('name'=>'Dispatched', 'color'=>'success'),
										'semi_ready'=>array('name'=>'Semi Ready', 'color'=>'primary'),
										'merchant_trade'=>array('name'=>'Import cum Export (ICE)', 'color'=>'warning'),
										'mtt'=>array('name'=>'M T T', 'color'=>'primary'),
										'import'=>array('name'=>'Import', 'color'=>'success'),
										'query'=>array('name'=>'Query', 'color'=>'gainsboro'),
										'mtt_rfd'=>array('name'=>'In Transit ICE', 'color'=>'dark'),
										'in_transit_import'=>array('name'=>'In Transit Import', 'color'=>'warning'),
										'in_transit_mtt'=>array('name'=>'In Transit MTT', 'color'=>'primary'),
										'order_cancelled'=>array('name'=>'Order Cancelled', 'color'=>'danger')
									);
		if($call_type == 'all') {

			return $static_production_status;
		} else {

			return (!empty($production_status_name)) ? $static_production_status[$production_status_name] : array('name'=>'','color'=>'');
		}
	}

	private function add_task_notification($task_type, $task_id, $notification_type, $work_order_no){

		if(!empty($task_id) && !empty($notification_type) && !empty($work_order_no)){

			$insert_array = array(
								'task_id'=> $task_id,
								'user_id'=> $this->session->userdata('user_id'),
								'work_order_no'=> $work_order_no,
								'add_time'=> $this->get_indian_time()
							);
			switch ($notification_type) {
				case 'task_is_mark_done':
					
					$insert_array['name'] = $this->session->userdata('name').' has <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">completed '.str_replace("_", " ", $task_type).'</a> for Work Order No <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">'.$work_order_no.'</a>';
				break;
				
				case 'vendor':
					
					$insert_array['name'] = $this->session->userdata('name').' has <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">uploaded vendor drawing</a> for Work Order No <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">'.$work_order_no.'</a>';
				break;
				case 'client':
					
					$insert_array['name'] = $this->session->userdata('name').' has <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">uploaded client drawing</a> for Work Order No <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">'.$work_order_no.'</a>';
				break;
				case 'marking':
					
					$insert_array['name'] = $this->session->userdata('name').' has <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">uploaded Marking</a> for Work Order No <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">'.$work_order_no.'</a>';
				break;
				case 'mtc':
					
					$insert_array['name'] = $this->session->userdata('name').' has <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">uploaded MTC</a> for Work Order No <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">'.$work_order_no.'</a>';
				break;
				case 'invoice':
					
					$insert_array['name'] = $this->session->userdata('name').' has <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">uploaded invoice</a> for Work Order No <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">'.$work_order_no.'</a>';
				break;
				case 'proforma_invoice':
					
					$insert_array['name'] = $this->session->userdata('name').' has <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">uploaded proforma invoice</a> for Work Order No <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">'.$work_order_no.'</a>';
				break;
				case 'pmi_photos':
					
					$insert_array['name'] = $this->session->userdata('name').' has <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">uploaded pmi photos</a> for Work Order No <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">'.$work_order_no.'</a>';
				break;
				case 'dimension_photos':
					
					$insert_array['name'] = $this->session->userdata('name').' has <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">uploaded dimension photos</a> for Work Order No <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">'.$work_order_no.'</a>';
				break;
				case 'marking_photos':
					
					$insert_array['name'] = $this->session->userdata('name').' has <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">uploaded marking photos</a> for Work Order No <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">'.$work_order_no.'</a>';
				break;
				case 'finishing_photos':
					
					$insert_array['name'] = $this->session->userdata('name').' has <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">uploaded finishing photos</a> for Work Order No <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">'.$work_order_no.'</a>';
				break;
				case 'packing_and_dispatch':
					
					$insert_array['name'] = $this->session->userdata('name').' has <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">uploaded packing and dispatch photos</a> for Work Order No <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">'.$work_order_no.'</a>';
				break;
				case 'reports_pdf':
					
					$insert_array['name'] = $this->session->userdata('name').' has <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">uploaded reports pdf photos</a> for Work Order No <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder">'.$work_order_no.'</a>';
				break;
				default:
				break;
			}
			$this->common_model->insert_data_sales_db('task_notification', $insert_array);
		}
		return false;
	}

	private function add_image_html_based_on_document_type($file_name){

		$return_image_path = '';
		if(!empty($file_name)){

			$explode_file_path = explode('.', $file_name);
			if(!empty($explode_file_path)){

				if(in_array($explode_file_path[count($explode_file_path)-1], array('jpg', 'jpeg', 'gif', 'png'))){

					$return_image_path = '<img src="https://storage.googleapis.com/documentation_bucket_name/'.$file_name.'" alt="image" style="width:100%;">';
				}else{

					$return_image_path = '<iframe src="https://docs.google.com/gview?url=https://storage.googleapis.com/documentation_bucket_name/'.$file_name.'&embedded=true" frameborder="0" style="width:100%; height:100%;"></iframe>';
				}
			}
		}
		
		return $return_image_path;
	}

	private function get_different_version_number_html($task_id, $file_type){

		$return_html = '';
		$google_cloud_upload_logs = $this->common_model->get_dynamic_data_sales_db('id', array('status'=> 'Active', 'upload_status'=> 'Done', 'task_id'=> $task_id, 'file_type'=> $file_type), 'google_cloud_upload_logs');
		// echo "<pre>";print_r($google_cloud_upload_logs);echo"</pre><hr>";die;
		if(!empty($google_cloud_upload_logs)){

			$return_html = '<ul class="nav nav-pills nav-fill mont_font_use" role="tablist" style="margin: 0px 0px 5px 0px;">';
			for ($i=1; $i <= count($google_cloud_upload_logs); $i++) { 
				
				$active = ($i==count($google_cloud_upload_logs)) ? "active": "";
				$return_html .= '
					<li class="nav-item">
						<a class="nav-link task_management_font tab '.$file_type.'_document get_document '.$active.'"  href="javascript:void(0);"  file_type = "'.$file_type.'" log_id = '.$google_cloud_upload_logs[$i-1]['id'].' style="">Version '.$i.'</a>
					</li>
				';
			}
			$return_html .= '</ul>';
		}
		
		return $return_html;
	}

	private function create_where_on_search_filter($search_filter_data){

		$return_where = '';
		$product_family = $production_status = $assigned_to = $client_id = $task_status = array();
		foreach ($search_filter_data as $single_form_data) {
			
			$name = $single_form_data['name'];
			if(!empty($single_form_data['value'])){

				switch ($name) {
					case 'work_order_no':
						
						$return_where .= " AND task_management.work_order_no = ".$single_form_data['value'];
					break;
					case 'product_family':
					case 'production_status':
					case 'task_status':
					case 'assigned_to':
					case 'client_id':

						$$name[] = $single_form_data['value'];
					break;
				}
			}	
		}
		if(!empty($product_family)){

			$return_where .= " AND production_process_information.product_family IN (";
			$return_where .= "'".$product_family[0]."'";
			for ($i=1; $i < count($product_family); $i++) { 
				
				$return_where .= ", '".$product_family[$i]."'";
			}
			$return_where .= ")";
		}
		if(!empty($production_status)){

			$return_where .= " AND production_process_information.production_status IN (";
			$return_where .= "'".$production_status[0]."'";
			for ($i=1; $i < count($production_status); $i++) {

				$return_where .= ", '".$production_status[$i]."'";
			}
			$return_where .= ")";
		}
		if(!empty($task_status)){

			$return_where .= " AND task_management.task_status IN (";
			$return_where .= "'".$task_status[0]."'";
			for ($i=1; $i < count($task_status); $i++) {

				$return_where .= ", '".$task_status[$i]."'";
			}
			$return_where .= ")";
		}
		if(!empty($assigned_to)){

			$return_where .= " AND task_management.user_id IN (";
			$return_where .= "'".$assigned_to[0]."'";
			for ($i=1; $i < count($assigned_to); $i++) {

				$return_where .= ", '".$assigned_to[$i]."'";
			}
			$return_where .= ")";
		}
		if(!empty($client_id)){

			$return_where .= " AND quotation_mst.client_id IN (";
			$return_where .= "'".$client_id[0]."'";
			for ($i=1; $i < count($client_id); $i++) {

				$return_where .= ", '".$client_id[$i]."'";
			}
			$return_where .= ")";
		}

		// echo "<pre>";print_r($return_where);echo"</pre><hr>";exit;
		return $return_where;
	}
	private function main_photos_html($file_name, $height){

		if(empty($file_name)){

			return "";
		}
		$return_html = "";
		$explode_file_path = explode('.', $file_name);
		if(!empty($explode_file_path)){

			if(in_array($explode_file_path[count($explode_file_path)-1], array('jpg', 'jpeg', 'gif', 'png'))){

				$return_html = '<img src="https://storage.googleapis.com/documentation_bucket_name/'.$file_name.'" class="set_main_photos" alt="Photo" style="touch-action: auto; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); background: #000; border-radius: 12px; margin-left: 25px; max-height:'.$height.'; width: 100%;">';
			}else{

				$return_html = '<iframe src="https://docs.google.com/gview?url=https://storage.googleapis.com/documentation_bucket_name/'.$file_name.'&embedded=true" class="sidebar_image change_main_photos" frameborder="0" style="max-height:'.$height.';"></iframe>';
			}
		}

		return $return_html;
	}
	private function add_image_html_based_on_document_type_new($file_name_array, $height, $sub_task_type){

		// echo "<pre>";print_r($file_name_array);echo"</pre><hr>";exit;
		$return_array = array();
		if($sub_task_type != 'all'){

			$return_array[] = array(
				'image_html'=> '<div class="kt-section__content kt-section__content--solid sidebar_image change_main_photos" file_name="" main_photos_height ="" type="upload_document" sub_task_type="'.$sub_task_type.'" style="padding: 30px 20px;position: relative; background-color: #ccc; display: block; margin-bottom: 8px; width: 100%; border-radius: 12px; cursor: pointer;"><button type="button" class="btn btn-elevate btn-elevate-air task_management_font" style="width:100%;">Upload Photos</button></div>',
				'div_width'=> '48',
				'div_class'=> "sidebar_container"
			);
		}
		if(!empty($file_name_array)){

			foreach ($file_name_array as $file_key => $single_file_name) {
				
				$explode_file_path = explode('.', $single_file_name['file_name']);
				if(!empty($explode_file_path)){

					if(in_array($explode_file_path[count($explode_file_path)-1], array('jpg', 'jpeg', 'gif', 'png'))){

						if(count($return_array) > 0){

							if(count($return_array) > 1){

								if(!empty($return_array[count($return_array)-1]) && $return_array[count($return_array)-1]['div_width'] == '48' && !empty($return_array[count($return_array)-2]) && $return_array[count($return_array)-2]['div_width'] != '4'){

									$return_array[] = array(
										'image_html'=> '',
										'div_width'=> '4',
										'div_class'=> ''
									);
								}
							}elseif(count($return_array) == 1){

								if($return_array[count($return_array)-1]['div_width'] == '48'){

									$return_array[] = array(
										'image_html'=> '',
										'div_width'=> '4',
										'div_class'=> ''

									);
								}
							}
						}
						$return_array[] = array(
							'image_html'=> '<img src="https://storage.googleapis.com/documentation_bucket_name/'.$single_file_name['file_name'].'" class="sidebar_image change_main_photos '.$single_file_name['selected_class'].'" alt="Photo" height="101" width="141" file_name="'.$single_file_name['file_name'].'" main_photos_height="'.$height.'" type="view_document" sub_task_type="">
							<div class="overlay">
								<a href="'.base_url("tasks/download_image/quality_inspection_254_276_1694262419.jpg").'" target="_blank">
									<i class="flaticon-download"></i>
								</a>
								&nbsp;
								<button class="delete_btn" image_id="'.$single_file_name['id'].'" file_name="'.$single_file_name['file_name'].'"><i class="flaticon-delete"></i></button>
							</div>',
							'div_width'=> '48',
							'div_class'=> "sidebar_container"

						);
						
					}else if(in_array($explode_file_path[count($explode_file_path)-1], array('pdf', 'docx'))){

						if(count($return_array) > 0){

							if(count($return_array) > 1){

								if(!empty($return_array[count($return_array)-1]) && $return_array[count($return_array)-1]['div_width'] == '100' && !empty($return_array[count($return_array)-2]) && $return_array[count($return_array)-2]['div_width'] != '4'){

									$return_array[] = array(
										'image_html'=> '',
										'div_width'=> '100',
										'div_class'=> ''
									);
								}
							}elseif(count($return_array) == 1){

								if($return_array[count($return_array)-1]['div_width'] == '100'){

									$return_array[] = array(
										'image_html'=> '',
										'div_width'=> '100',
										'div_class'=> ''

									);
								}
							}
						}

						$return_array[] = array(
							'image_html'=> '<iframe src="https://docs.google.com/gview?url=https://storage.googleapis.com/documentation_bucket_name/'.$single_file_name['file_name'].'&embedded=true" class="sidebar_image change_main_photos '.$single_file_name['selected_class'].'" frameborder="0" file_name="'.$single_file_name['file_name'].'" main_photos_height="'.$height.'" type="view_document" sub_task_type="" style="height:300px;"></iframe>
								<div class="overlay">
									<a href="'.base_url("tasks/download_image/quality_inspection_254_276_1694262419.jpg").'" target="_blank">
										<i class="flaticon-download"></i>
									</a>
									&nbsp;
									<button class="delete_btn" image_id="'.$single_file_name['id'].'" file_name="'.$single_file_name['file_name'].'"><i class="flaticon-delete"></i></button>
								</div>',
							'div_width'=> '100',
							'div_class'=> "sidebar_container"

						);
					}else{

						$return_array[] = array(
							'image_html'=> '<iframe src="https://docs.google.com/gview?url=https://storage.googleapis.com/documentation_bucket_name/'.$single_file_name['file_name'].'&embedded=true" class="sidebar_image change_main_photos '.$single_file_name['selected_class'].'" frameborder="0" file_name="'.$single_file_name['file_name'].'" main_photos_height="'.$height.'" type="view_document" sub_task_type="" style="height:300px;"></iframe>',
							'div_width'=> '100',
							'div_class'=> ''
						);
					}
				}
			}	
		}
		
		return $return_array;
	}


	private function prepare_search_filter_data($search_filter_form_data, $type){

		// echo "<pre>";print_r($search_filter_form_data);echo"</pre><hr>";exit;
		$data['assigned_to']  =	$this->change_key_as_per_value(
									$this->tasks_model->get_task_user_list($type)
		);
		$data['product_family'] = array(
							'piping' => array(
								'name' => 'Piping',
								'value' => 'piping',
								'selected' => ''
							),
							'instrumentation' => array(
								'name' => 'Instrumentation',
								'value' => 'instrumentation',
								'selected' => ''
							),
							'precision' => array(
								'name' => 'Precision',
								'value' => 'precision',
								'selected' => ''
							),
							'tubing' => array(
								'name' => 'Tubing',
								'value' => 'tubing',
								'selected' => ''
							),
							'industrial' => array(
								'name' => 'Industrial',
								'value' => 'industrial',
								'selected' => ''
							),
							'fastener' => array(
								'name' => 'Fastener',
								'value' => 'fastener',
								'selected' => ''
							),
							'valve' => array(
								'name' => 'Valve',
								'value' => 'valve',
								'selected' => ''
							),
		);
		$data['production_status'] = array(
							'yet_to_start_production' => array(
								'name' => 'Yet To Start Production',
								'value' => 'yet_to_start_production',
								'selected' => ''
							),
							'in_production' => array(
								'name' => 'In Production',
								'value' => 'in_production',
								'selected' => ''
							),
							'pending_order' => array(
								'name' => 'Pending Order',
								'value' => 'pending_order',
								'selected' => ''
							),
							'semi_ready' => array(
								'name' => 'Semi Ready',
								'value' => 'semi_ready',
								'selected' => ''
							),
							'merchant_trade' => array(
								'name' => 'Import cum Export (ICE)',
								'value' => 'merchant_trade',
								'selected' => ''
							),
							'mtt' => array(
								'name' => 'MTT',
								'value' => 'mtt',
								'selected' => ''
							),
							'import' => array(
								'name' => 'Import',
								'value' => 'import',
								'selected' => ''
							),
							'query' => array(
								'name' => 'Query',
								'value' => 'query',
								'selected' => ''
							),
							'mtt_rfd' => array(
								'name' => 'In Transit ICE',
								'value' => 'mtt_rfd',
								'selected' => ''
							),
							'in_transit_import' => array(
								'name' => 'In Transit Import',
								'value' => 'in_transit_import',
								'selected' => ''
							),
							'in_transit_mtt' => array(
								'name' => 'In Transit MTT',
								'value' => 'in_transit_mtt',
								'selected' => ''
							),
							'ready_for_dispatch' => array(
								'name' => 'Ready For Dispatch',
								'value' => 'ready_for_dispatch',
								'selected' => ''
							),
							'dispatched' => array(
								'name' => 'Dispatched',
								'value' => 'dispatched',
								'selected' => ''
							),
							'on_hold' => array(
								'name' => 'On Hold',
								'value' => 'on_hold',
								'selected' => ''
							),
							'delay_in_delivery' => array(
								'name' => 'Delay In Delivery',
								'value' => 'delay_in_delivery',
								'selected' => ''
							),
							'order_cancelled' => array(
								'name' => 'Order Cancelled',
								'value' => 'order_cancelled',
								'selected' => ''
							),
		);
		$data['task_status'] = array(
							'Pending' => array(
								'name' => 'Pending',
								'value' => 'Pending',
								'selected' => ''
							),
							'Done' => array(
								'name' => 'Done',
								'value' => 'Done',
								'selected' => ''
							),
		);
		if($this->session->userdata('task_access')['task_list_action_status_access']){
			$data['task_status']['Approved'] = array(
								'name' => 'Approved',
								'value' => 'Approved',
								'selected' => ''
			);
			$data['task_status']['Not_required'] = array(
								'name' => 'Not_required',
								'value' => 'Not_required',
								'selected' => ''
			);
		}

		$data['search_task_client_name'] = '';

		if(!empty($search_filter_form_data)){

			foreach ($search_filter_form_data as $form_data) {

				if(!empty($form_data['value'])){

					switch ($form_data['name']) {

						case 'work_order_no':

							$data[$form_data['name']] = $form_data['value'];
						break;
						case 'search_task_client_name':

							$data['search_task_client_name'] = $form_data['value'];
							$where_string = "status = 'Active' AND name LIKE '%".$form_data['value']."%'";

							$data['client_id'] = $this->change_key_as_per_value(
								$this->common_model->get_dynamic_data_sales_db_null_false(
									'id as value, name, "" as selected',
									$where_string,
									'customer_mst'
								)
							);
							$data['search_task_client_name'] = $form_data['value'];
						break;
						case 'client_id':
						case 'assigned_to':
						case 'product_family':
						case 'production_status':
						case 'task_status':

							$data[$form_data['name']][$form_data['value']]['selected'] = 'selected';
						break;

						default:
						break;
					}
				}
			}
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		return $data;
	}

	private function change_key_as_per_value($data, $changing_key_column_name = 'value'){

		$return_data = array();
		foreach ($data as $single_data) {

			$return_data[$single_data[$changing_key_column_name]] = $single_data;
		}
		return $return_data;
	}
}	
