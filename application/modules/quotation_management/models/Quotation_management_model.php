<?php 
Class Quotation_management_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$CI = &get_instance();
	}


    public function	get_listing_data($join_string, $where_string, $order_by_string, $limit, $offset){

		$return_array = array();
		$query = " 	SELECT
						SQL_CALC_FOUND_ROWS
						quotation_mst.client_id,
						quotation_mst.quotation_mst_id,
						quotation_mst.quote_no,
						CAST(REPLACE(quotation_mst.quote_no, 'OM/', '') as INT) quote, quotation_mst.work_order_no,
						quotation_mst.is_new,
						quotation_mst.order_no,
						quotation_mst.proforma_no,
						CAST(REPLACE(quotation_mst.proforma_no, 'OM/', '') as INT) proforma, quotation_mst.confirmed_on,
						DATE_FORMAT(quotation_mst.confirmed_on, '%d-%b') confirmed_on_date,
						quotation_mst.made_by,
						quotation_mst.assigned_to,
						quotation_mst.stage,
						quotation_mst.quotation_priority,
						quotation_mst.priority_reason,
						quotation_mst.member_id,
						quotation_mst.reference,
						quotation_mst.additional_notes,
						quotation_mst.net_total,
						quotation_mst.freight,
						quotation_mst.bank_charges,
						quotation_mst.gst,
						quotation_mst.discount_type,
						quotation_mst.discount,
						quotation_mst.other_charges,
						quotation_mst.grand_total,
						quotation_mst.grand_total as value,
						quotation_mst.delivered_through,
						quotation_mst.delivery_time,
						quotation_mst.payment_term,
						quotation_mst.comments,
						quotation_mst.validity,
						quotation_mst.currency,
						quotation_mst.origin_country,
						quotation_mst.mtc_type,
						quotation_mst.transport_mode,
						quotation_mst.client_type,
						quotation_mst.status,
						quotation_mst.yet_to_acknowledge,
						quotation_mst.acknowledge,
						quotation_mst.importance,
						quotation_mst.close_reason,
						quotation_mst.followup_date,
						DATE_FORMAT(quotation_mst.followup_date, '%d-%b') follow_up_date,
						quotation_mst.entered_on,
						quotation_mst.entered_on as years,
						DATE_FORMAT(quotation_mst.entered_on, '%e-%b<br>%h:%i %p') entered_on_date,
						quotation_mst.modified_by,
						quotation_mst.modified_on,
						quotation_mst.currency_rate,
						quotation_mst.rfq_id,
						quotation_mst.additional_comment,
						quotation_mst.purchase_order,
						quotation_mst.po_add_time,
						customer_mst.name customer_name,
						country_mst.name country_name,
						region_mst.name region_name,
						rfq_mst.product_family,
						rfq_mst.assigned_to procurement_person
					FROM
						`quotation_mst`
					{$join_string}
					WHERE
						{$where_string}";
		$query .= "
					ORDER BY
						{$order_by_string}
					LIMIT
						{$limit}
					OFFSET
						{$offset}";


		$return_array['quotation_list'] = $this->db->query($query)->result_array();

		// echo "<pre>";print_r($this->db->last_query());echo"</pre><hr>";exit;
		$return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows")->row_array();
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";
		return $return_array;

	}

	public function getData($table, $where = ''){
		if($where != ''){
			$this->db->where($where);
		}
		return $this->db->get($table)->result_array();
	}

    public function get_Financial_Years_date() {
		$res = $this->db->query("	SELECT
										(
											CASE WHEN MONTH(entered_on)>3
											THEN concat(YEAR(entered_on), '-',YEAR(entered_on)+1)
											ELSE concat(YEAR(entered_on)-1,'-', YEAR(entered_on)) 
											END
										)AS value, '' as selected
									FROM quotation_mst
									WHERE Year(entered_on) != 1970
									GROUP BY value
									order by entered_on desc;"

								)->result_array();
		// echo "<pre>";print_r($res);	echo"</pre><hr>";exit;
		return $res;
	}

	public function deleteData($table, $where){
		$this->db->delete($table, $where);
	}

	public function get_client_name($search){
		$this->db->select('c.id, c.name, l.name country_name');
		$this->db->join('country_mst l', 'l.id = c.country_id', 'left');
		$this->db->order_by('name');
		$this->db->where("c.name like '%".$search."%'");
		$res = $this->db->get_where('customer_mst c', array('c.status' => 'Active'), 10)->result_array();
		// echo "<pre>";print_r($this->db->last_query());echo"</pre><hr>";
		// echo "<pre>";print_r($res);echo"</pre><hr>";exit;

		return $res;
	}

	public function get_client_name_on_id($cient_id){
		
		$this->db->select('c.id, c.name');
		$res = $this->db->get_where('customer_mst c', array('c.status' => 'Active', 'c.id'=>$cient_id))->result_array();
		return $res;
	}

	public function get_quotation_client_list_data($where_string) {

		$this->db->select('customer_mst.id, customer_mst.name');
		$this->db->join('customer_mst', 'customer_mst.id = quotation_mst.client_id', 'left');
		$this->db->where($where_string, null, false);
		$this->db->group_by('quotation_mst.client_id');
		$res = $this->db->get('quotation_mst')->result_array();
		// echo "<pre>";print_r($this->db->last_query());echo"</pre><hr>";exit;
		return $res;
	}
}