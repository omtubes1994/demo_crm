<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <style type="text/css">
		.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
			padding-top: 3% !important;
		}
		.layer-white{
		    display: none;
		    position: fixed;
		    top: 0em !important;
		    left: 0em !important;
		    width: 100%;
		    height: 100%;
		    text-align: center;
		    vertical-align: middle;
		    background-color: rgba(255, 255, 255, 0.55);
		    opacity: 1;
		    line-height: 1;
		    -webkit-animation-fill-mode: both;
		    animation-fill-mode: both;
		    -webkit-animation-duration: 0.5s;
		    animation-duration: 0.5s;
		    -webkit-transition: background-color 0.5s linear;
		    transition: background-color 0.5s linear;
		    -webkit-user-select: none;
		    -moz-user-select: none;
		    -ms-user-select: none;
		    user-select: none;
		    will-change: opacity;
		    z-index: 9;
		}
		.div-loader{
		    position: absolute;
			top: 50%;
			left: 50%;
			margin: 0px;
			text-align: center;
			z-index: 1000;
			-webkit-transform: translateX(-50%) translateY(-50%);
			transform: translateX(-50%) translateY(-50%);
		}
		.kt-spinner:before {
		    width: 50px;
		    height: 50px;
		    margin-top: -10px;
		}

        ul.menu-tab {
            margin: 0px;
            padding: 0px;
            border: 1px solid #E7E7E7;
            overflow: hidden;
            font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
            font-size: 14px;
            line-height: 20px;
            color: #333;
        }
        ul.menu-tab li{
            width: <?php echo $menu_tab_width;?>;
            display: inline;
            text-align: center;
            float: left;
        }
        ul.menu-tab li.active_list{
            border-bottom: 0.40rem solid #343a40 !important;
        }
        ul.menu-tab li a{

            cursor: pointer;
            display: inline-block;
            outline: none;
            text-align: center;
            width: 100%;
            background-color: #5578eb;
            border-color: #5578eb;
            color: #ffffff;           
            font-size: 18px;
            font-family: 'latomedium';
            padding: 3px 3px 20px 3px;
        }
        ul.menu-tab li a i{
            display: inline-block;
            width: 35px;
            height: 28px;
            font-style: normal;
            background-size: 100%;
            position: relative;
            top: 6px;
        }
        tr.quotation_total_above_25k_color{
            background-color: antiquewhite;
        }
        
        .kt-badge.kt-badge--orange {
            color: #111111;
            background: #fd7e14;
        }
        .kt-badge.kt-badge--unified-orange {
            color: #fd7e14;
            background: #FFDAB9;
        }
        .quote_font {
            font-size: 1rem;
            font-weight: 500;
            line-height: 1.5rem;
            -webkit-transition: color 0.3s ease;
            transition: color 0.3s ease;
            color: #000;
        }
    </style>

    <!-- begin:: Container -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">

                <div class="kt-portlet kt-portlet--mobile">
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="kt-font-brand flaticon2-line-chart"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                QUOTATION LIST                       
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-wrapper">
                                <div class="kt-portlet__head-actions">
                                    <a href="javascript:;" class="btn btn-brand btn-elevate btn-icon-sm add_search_filter" button_value = "show">
                                        Add Search Filter
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body search_filter_data" style="display: none;"></div>           
                </div>

                <div class="kt-portlet kt-portlet--mobile">
                    <div class="kt-portlet__body">
                        <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-sm-12">
                                    <ul class="menu-tab kt-font-bolder" style="">
                                        <?php if(in_array('list', $this->session->userdata('quotation_access')['quotation_list_tab_access'])){?>
                                            <li class="tab_name_click quotation_list active_list" tab-name="quotation_list">
                                                <a>
                                                    <span style="top: 6px;position: relative;">List</span>
                                                </a>
                                            </li>
                                        <?php } ?>
                                        <?php if(in_array('draft', $this->session->userdata('quotation_access')['quotation_list_tab_access'])){?>
                                            <li class="tab_name_click draft_list" tab-name="draft_list" style="padding: 0px 0px 0px 10px;">
                                                <a>
                                                    <span style="top: 6px;position: relative;">Draft List</span>
                                                </a>
                                            </li>
                                        <?php }?>
                                        <?php if(in_array('follow_up', $this->session->userdata('quotation_access')['quotation_list_tab_access'])){?>
                                            <li class="tab_name_click followup_list" tab-name="followup_list" style="padding: 0px 0px 0px 10px;">
                                                <a>
                                                    <span style="top: 6px;position: relative;">Followup List</span>
                                                </a>
                                            </li>
                                        <?php }?>
                                        <?php if(in_array('proforma', $this->session->userdata('quotation_access')['quotation_list_tab_access'])) {?>
                                            <li class="tab_name_click proforma_list" tab-name="proforma_list" style="padding: 0px 0px 0px 10px;">
                                                <a>
                                                    <span style="top: 6px;position: relative;">Proforma List</span>
                                                </a>
                                            </li>
                                        <?php }?>
                                    </ul>							
                                </div>

                                <?php if(in_array('list', $this->session->userdata('quotation_access')['quotation_list_tab_access']) ||
                                in_array('draft', $this->session->userdata('quotation_access')['quotation_list_tab_access']) ||
                                in_array('follow_up', $this->session->userdata('quotation_access')['quotation_list_tab_access']) ||
                                in_array('proforma', $this->session->userdata('quotation_access')['quotation_list_tab_access'])){?>
                                <div class="col-sm-12">
                                    <table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 1536px;">
                                        <thead id="quotation_list_head"></thead>
                                        <tbody id="quotation_list_body"></tbody>
                                    </table> 
                                    <div id="quotation_list_process" class="layer-white">
                                        <div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
                                    </div>                            
                                </div>
                                <?php }?>
                            </div>
                            <div class="row quotation_paggination"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    
    <!-- end:: Container -->
    
    
    <!-- start::Model -->
    <div class="modal fade" id="update_rating" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Update Rating</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<form id="save_rating_form">
						
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button class="btn btn-success save_rating" type="reset" id="quotation_id" style="float:right;"> Save </button>
				</div>
			</div>
		</div>
	</div>

    <div class="modal fade" id="add_followup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Follow Up</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="add_followup_data_form">
                        
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add_query" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <form id="sales_query_form">
                        <div class="row">
                            
                            <!--Begin:: App Content-->
                            <div class="kt-grid__item kt-grid__item--fluid kt-app__content" id="kt_chat_content" style="width:100%; padding: 0px 24px 0px 24px;">
                                <div class="kt-chat">
                                    <div class="kt-portlet kt-portlet--head-lg kt-portlet--last">
                                        <div class="kt-portlet__head">
                                            <div class="kt-chat__head ">
                                                <div class="kt-chat__left">

                                                    <!--begin:: Aside Mobile Toggle -->
                                                    <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md kt-hidden-desktop" id="kt_chat_aside_mobile_toggle">
                                                        <i class="flaticon2-open-text-book"></i>
                                                    </button>

                                                    <!--end:: Aside Mobile Toggle-->
                                                    <div class="dropdown dropdown-inline">
                                                        <div class="kt-chat__label">
                                                            <a href="#" class="kt-chat__title">Sales Query Informations</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="kt-chat__center">
                                                    <div class="kt-chat__label">
                                                        <!-- <a href="#" class="kt-chat__title">Jason Muller</a>
                                                        <span class="kt-chat__status">
                                                            <span class="kt-badge kt-badge--dot kt-badge--success"></span> Active
                                                        </span> -->
                                                    </div>
                                                    <div class="kt-chat__pic kt-hidden">
                                                        <span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Jason Muller">
                                                            <img src="assets/media/users/300_12.jpg" alt="image">
                                                        </span>
                                                        <span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Nick Bold">
                                                            <img src="assets/media/users/300_11.jpg" alt="image">
                                                        </span>
                                                        <span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Milano Esco">
                                                            <img src="assets/media/users/100_14.jpg" alt="image">
                                                        </span>
                                                        <span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Teresa Fox">
                                                            <img src="assets/media/users/100_4.jpg" alt="image">
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="kt-chat__right">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="kt-scroll sales_query_history" data-scroll="true" data-height="400">
                                                
                                            </div>
                                        </div>
                                        <div class="kt-portlet__foot">
                                            <div class="kt-chat__input">
                                                <div class="kt-chat__editor">
                                                    <input type="text" class="form-control" name="query_type" value="sales_query" hidden />
                                                    <input type="text" class="form-control" name="query_reference" value="" hidden />
                                                    <input type="text" class="form-control" name="query_reference_id" value="" hidden />
                                                    <textarea style="height: 50px" placeholder="Type here..." name="query_text"></textarea>
                                                    <input type="text" class="form-control" name="query_creator_id" value="" hidden />
                                                    <input type="text" class="form-control" name="query_assigned_id" value="" hidden />
                                                    <input type="text" class="form-control" name="query_status" value="open" hidden />
                                                </div>
                                                <div class="kt-chat__toolbar">
                                                    <div class="kt_chat__tools">
                                                        <div class="query_type">
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="kt_chat__actions">
                                                        <button type="button" class="btn btn-brand btn-md btn-upper btn-bold kt-chat__reply save_sales_query">reply</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--End:: App Content-->
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade show" id="add_po"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none; padding-right: 21px;" aria-modal="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Upload Purchase Order</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<form class="kt-form kt-form--label-right" id="po_upload">

					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

    <div class="modal fade" id="stats-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-body" style="background: aliceblue;">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="kt-font-info graph_company_name">h2. Heading 2</h2>
                        </div>
                        <!-- QUOTATION VS PROFORMA Yearly -->
                        <div class="col-6">
                            <div class="kt-portlet kt-portlet--height-fluid">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title quotation_vs_proforma_yearly_title"></h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body quotation_vs_proforma_yearly_body" style="display:none;">
                                    <div class="kt-scroll" data-scroll="true" style="height: 400px">
                                        <figure class="highcharts-figure">
                                            <div id="quotation_vs_proforma_highchart_yearly"></div>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- QUOTATION VS PROFORMA Monthly -->
                        <div class="col-6">
                            <div class="kt-portlet kt-portlet--height-fluid">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title quotation_vs_proforma_title"></h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body quotation_vs_proforma_body" style="display:none;">
                                    <div class="kt-scroll" data-scroll="true" style="height: 400px">
                                        <figure class="highcharts-figure">
                                            <div id="quotation_vs_proforma_highchart"></div>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- EXPORT STATS YEARLY -->
                        <div class="col-6">
                            <div class="kt-portlet kt-portlet--height-fluid">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title export_stats_yearly_title"></h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body export_stats_yearly_body" style="display:none;">
                                    <div class="kt-scroll" data-scroll="true" style="height: 400px">
                                        <figure class="highcharts-figure">
                                            <div id="export_stats_yearly_container"></div>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- EXPORT STATS YEARLY INTERNAL -->
                        <div class="col-6">
                            <div class="kt-portlet kt-portlet--height-fluid">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title export_stats_yearly_internal_title"></h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body export_stats_yearly_internal_body" style="display:none;">
                                    <div class="kt-scroll" data-scroll="true" style="height: 400px">
                                        <figure class="highcharts-figure">
                                            <div id="export_stats_yearly_internal_container"></div>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- EXPORTER DATA -->
                        <div class="col-6 exporter_stats_div" style="display:none;">
                            <div class="kt-portlet kt-portlet--height-fluid">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title exporter_stats_title"></h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <div class="kt-scroll" data-scroll="true" style="height: 400px">
                                        <figure class="highcharts-figure">
                                            <div id="exporter_stats_container"></div>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Production Status Wise Graph -->
                        <div class="col-6 production_status_wise_div">
                            <div class="kt-portlet kt-portlet--height-fluid">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title production_status_wise_title"></h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <div class="kt-scroll" data-scroll="true" style="height: 400px">
                                        <figure class="highcharts-figure">
                                            <div id="production_status_wise"></div>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end::Model-->
  
</div>
<!--end::Content-->
