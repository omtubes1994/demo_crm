<?php if(in_array($this->session->userdata('quotation_tab_name'), array('quotation_list', 'draft_list', 'followup_list'))) {?>
    
    <tr role="row">
        <th style="width: 5%;">Sr</th>
        <th style="width: 14%;">Quote #</th>
        <th style="width: 5%;">Assigned To</th>
        <th style="width: 7%;">Date</th>
        <?php if($this->session->userdata('quotation_access')['quotation_list_client_name_access']){ ?>
            <th style="width: 15%;">Client</th>
        <?php }?>
        <?php if($this->session->userdata('quotation_access')['quotation_list_value_access']){ ?>
            <th class="kt-align-right" style="width: 5%;">Value</th>
        <?php }?>
        <th style="width: 7%;">Country</th>
        <th style="width: 7%;">FUDate</th>
        <th style="width: 5%;">Status</th>
        <th style="width: 22%;">Follow Up</th>
        <th style="width: 8%;">Actions</th>
    </tr>    
<?php } else if(in_array($this->session->userdata('quotation_tab_name'), array('proforma_list'))) {?>
    
    <tr role="row">
        <th style="width: 7%;">Sr</th>
        <th style="width: 14%;">Proforma#</th>
        <th style="width: 10%;">Assigned To</th>
        <th style="width: 10%;">Purchase Person</th>
        <th style="width: 8%;">Date</th>
        <?php if($this->session->userdata('quotation_access')['quotation_list_client_name_access']){ ?>
        <th style="width: 25%;">Client</th>
        <?php }?>
        <?php if($this->session->userdata('quotation_access')['quotation_list_value_access']){ ?>
            <th class="kt-align-right" style="width: 2%;">Value</th>
        <?php }?>
        <th style="width: 5%;">Country</th>
        <th style="width: 5%;">Status</th>
        <th style="width: 10%;">Actions</th>
    </tr>    
<?php } ?>

    

