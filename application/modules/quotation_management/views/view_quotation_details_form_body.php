<?php if($tab_name == "quotation_details"){?>
    <div class="qutation_details_body">
        <table class='table table-bordered'>
            <thead>
                <tr>
                    <td style='width: 5%;'>#</td>
                    <td style='width: 45%;'>Item Description</td>
                    <td style='width: 10%;'>Qty</td>
                    <td style='width: 10%;'>Unit</td>
                    <td style='width: 10%;'>Price</td>
                    <td style='width: 10%;'>Unit</td>
                    <td style='width: 10%;'>Total</td>
                </tr>
            </thead>
            <tbody id="quotation_details_body">
                <?php if(!empty($quotation_details)){ ?>
                    <?php $count_no = 1;?>
                    <?php foreach($quotation_details as $quotation_details_key => $single_quotation_details){ ?>

                        <tr class="<?php echo $count_no;?>">

                            <td style="text-align: center;">
                                <?php echo ++$quotation_details_key;?>
                            </td>
                            <td>
                                <?php echo $single_quotation_details['description'];?>
                            </td>
                            <td style="text-align: right;">
                                <?php if(strpos($single_quotation_details['quantity'], '.') === false){
                                    $single_quotation_details['quantity'] = $single_quotation_details['quantity'].".00";
                                } ?>
                                <?php echo $single_quotation_details['quantity'];?>
                            </td>
                            <td style="text-align: left;">
                                <?php echo ucwords(strtolower($single_quotation_details['unit_value']));?>.
                            </td>
                            <td style="text-align: right;">
                                <?php echo $currency_details['currency_icon'].$single_quotation_details['unit_price'];?>
                            </td>
                            <td style="text-align: left;">P.
                                <?php echo ucwords(strtolower($single_quotation_details['unit_value']));?>.
                            </td>
                            <td style="text-align: right;">
                                <?php echo $currency_details['currency_icon'].$single_quotation_details['row_price']; ?>
                            </td>
                    
                        </tr>
                    <?php $count_no++;}?>
                <?php }?>

            </tbody>
        </table>
        <table cellspacing="0" cellpadding="10" style="width:30%" align="right">
                
            <tr>
                <td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;" width="50%" >Net Total</td>
                <td style="font-size: 12px; border: solid 1px grey;" width="50%">
                    <?php
                        $net_total = (!empty($quotation_data)) ? $quotation_data['net_total'] : 00;
                        if(strpos($net_total, '.') === false){
                            $net_total = $net_total.'.00';
                        }
                    ?>
                    <?php echo $currency_details['currency_icon'].$net_total;?>
                </td>
            </tr>
            <?php if($quotation_data['freight'] > 0){
                if(strpos($quotation_data['freight'], '.') === false){
                    $quotation_data['freight'] = $quotation_data['freight'].'.00';
                } ?>
                <tr>
                    <td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Freight Charges</td>
                    <td style="font-size: 12px; border: solid 1px grey;"><?php echo $currency_details['currency_icon'].$quotation_data['freight'];?></td>
                </tr>
            <?php }?>
            <?php if($quotation_data['bank_charges'] > 0){
                if(strpos($quotation_data['bank_charges'], '.') === false){
                    $quotation_data['bank_charges'] = $quotation_data['bank_charges'].'.00';
                } ?>
                <tr>
                    <td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Bank Charges</td>
                    <td style="font-size: 12px; border: solid 1px grey;"><?php echo $currency_details['currency_icon'].$quotation_data['bank_charges'];?></td>
                </tr>
            <?php }?>
            <?php if($quotation_data['gst'] > 0 && $currency_details['currency'] == 'INR'){
                if(strpos($quotation_data['gst'], '.') === false){
                    $quotation_data['gst'] = $quotation_data['gst'].'.00';
                } ?>
                <tr>
                    <td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">GST</td>
                    <td style="font-size: 12px; border: solid 1px grey;"><?php echo $currency_details['currency_icon'].$quotation_data['gst'];?></td>
                </tr>
            <?php }?>
            <?php if($quotation_data['other_charges'] > 0){
                if(strpos($quotation_data['other_charges'], '.') === false){
                    $quotation_data['other_charges'] = $quotation_data['other_charges'].'.00';
                } ?>
                <tr>
                    <td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Other Charges</td>
                    <td style="font-size: 12px; border: solid 1px grey;"><?php echo $currency_details['currency_icon'].$quotation_data['other_charges'];?></td>
                </tr>
            <?php }?>
            <?php if($quotation_data['discount'] > 0){
                if(strpos($quotation_data['discount'], '.') === false){
                    $quotation_data['discount'] = $quotation_data['discount'].'.00';
                } ?>
                <tr>
                    <td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Discount</td>
                    <td style="font-size: 12px; border: solid 1px grey;"><?php echo $currency_details['currency_icon'].$quotation_data['discount'];?></td>
                </tr>
            <?php }?>
            <?php if(!empty($quotation_data['grand_total'] && $quotation_data['grand_total'] > 0)){

                $grand_total = (!empty($quotation_data)) ? $quotation_data['grand_total'] : 00;
                if(strpos($quotation_data['grand_total'], '.') === false){
                    $grand_total = $quotation_data['grand_total'].'.00';
                }?>

                <tr>
                    <td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Grand Total</td>
                    <td style="font-size: 12px; border: solid 1px grey;"><?php echo $currency_details['currency_icon'].$grand_total;?></td>
                </tr>
            <?php }?>
        </table>
        <table  class='table table-bordered' style="width:30%" align="left">
            
            <tr><td colspan="2"><strong>Terms and Conditions</strong></td></tr>
            <tr><td width="35%">Delivered To : </td>
                <td width="65%"><?php echo $delivery_to_details['delivery_name'];?></td>
            </tr>
            <tr>
                <td>Delivery Time : </td> <td><?php echo $delivery_time_details['dt_value'];?></td>
            </tr>
            <tr>
                <td>Validity : </td> <td><?php echo $validity_details['validity_value'];?></td>
            </tr>
            <tr>
                <td>Currency : </td> <td><?php echo $currency_details['currency']?></td>
            </tr>
            <tr>
                <td>Country of Origin : </td> <td><?php echo $origin_country_details['country'];?></td>
            </tr>
            <tr>
                <td>MTC Type : </td> <td><?php echo $mtc_type_details['mtc_value'];?></td>
            </tr>
            <tr>
                <td>Packing Type : </td> <td><?php echo $transport_details['mode'];?></td>
            </tr>
            <tr>
                <td>Payment : </td> <td><?php echo $payment_details['term_value'];?></td>
            </tr>
        </table>
    <div>
<?php } else if($tab_name == "followup_details"){ ?>

    <a href="javascript:void(0);" class="btn btn-brand btn-elevate btn-icon-sm followup_modal" title="Follow Up" data-toggle="modal" data-target="#add_followup_data" style="float:right;" quotation_id="<?php echo $quotation_data['quotation_mst_id'];?>" client_id="<?php echo $quotation_data['client_id'];?>">Add Follow up Details</a>
	<table class="table table-bordered">
        <thead>
            <tr>
                <th style='width:10%;'>Sr #</th>
                <th style='width:10%;'>Date</th>
                <th style='width:80%;'>Remarks</th>
            </tr>
        <thead>
        <tbody>
            <?php if(!empty($followup_details)){ ?>
                <?php $count_no = 1;?>
                <?php foreach($followup_details as $followup_details_key => $single_followup_details){ ?>

                    <tr class="<?php echo $count_no;?>">

                        <td style="text-align: center;"><?php echo ++$followup_details_key;?></td>
                        <td><?php echo date('d-m-Y', strtotime($single_followup_details['followedup_on']));?></td>
                        <td><?php echo $single_followup_details['follow_up_text'];?></td>
                    
                    </tr>
                <?php $count_no++;}?>
            <?php }?>
        </tbody>
    </table>
<?php } else if($tab_name == "client_details"){  ?>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-stripped">
                <tr>
                    <th width="30%">Company Name</th>
                    <td width="70%"><?php echo $client_details['name'];?></td>
                </tr>
                <tr>
                    <th>Country</th>
                    <td><?php echo $country_details['name'];?></td>
                </tr>
                <tr>
                    <th>Region</th>
                    <td><?php echo $region_details['name'];?></td>
                </tr>                
            </table><br>
            <table class="table table-bordered">
                 <thead>
                    <tr>
                        <th style='width:5%;'>Sr #</th>
                        <th style='width:20%;'>Member Name</th>
                        <th style='width:20%;'>Email</th>
                        <th style='width:20%;'>Telephone</th>
                        <th style='width:20%;'>Mobile</th>
                        <th style='width:15%;'>Skype Id</th>
                    </tr>
                <thead>
                <tbody>
                    <?php if(!empty($member_details)){ ?>
                        <?php $count_no = 1;?>
                        <?php foreach($member_details as $member_details_key => $single_member_details){ ?>

                            <tr class="<?php echo $count_no;?>">

                                <td style="text-align: center;"><?php echo ++$member_details_key;?></td>
                                <td><?php 
                                    if(empty($single_member_details['member_name'])){
                                        echo 'Unknown';
                                    }
                                    echo $single_member_details['member_name'];?>
                                </td>
                                <td><?php echo $single_member_details['email'];?></td>
                                <td><?php echo $single_member_details['telephone'];?></td>
                                <td>
                                    <?php echo $single_member_details['mobile'];?>
                                    <?php if($single_member_details['is_whatsapp'] == 'Yes'){?>
                                        <a href="https://web.whatsapp.com/send?phone=<?php echo $single_member_details['mobile'];?>&text=" class="btn btn-xs btn-clean btn-icon btn-icon-sm" title="View Quotation Details" target="_blank" >
                                            <i class="la la-whatsapp"></i>
                                        </a>
                                    <?php } ?>
                                </td>
                                <td><?php echo $single_member_details['skype'];?></td>
                            </tr>
                        <?php $count_no++;}?>
                    <?php }?>
                </tbody>
            </table>
        </div>
    </div>
<?php } else if($tab_name == "other_details"){  ?>

    <table class="table table-bordered">
        <thead>
            <tr>
                <th style='width:5%;'>Sr #</th>
                <th style='width:20%;'>Quotation #</th>
                <th style='width:20%;'>Value</th>
                <th style='width:20%;'>Action</th>
            </tr>
        <thead>
        <tbody id="other_details_body">
            <?php if(!empty($quotation_siblings)){ ?>
                <?php $count_no = 1;?>
                <?php foreach($quotation_siblings as $quotation_siblings_key => $single_quotation_siblings){ ?>

                    <tr class="<?php echo $count_no;?>">

                        <td style="text-align: center;"><?php echo ++$quotation_siblings_key;?></td>
                        <td><?php echo $single_quotation_siblings['quote_no'];?></td>
                        <td><?php echo $single_quotation_siblings['grand_total'];?></td>
                        <td>
                            <a href="<?php echo base_url('quotation_management/viewQuotation/'.$single_quotation_siblings['quotation_mst_id']);?>" target="_blank" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View Quotation Details">
                                <i class="fa fa-file-invoice"></i>
                            </a>
                            <a href="<?php echo base_url('quotation_management/pdf/'.$single_quotation_siblings['quotation_mst_id']);?>" target="_blank" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Pdf" >
                                <i class="la la-eye"></i>
                            </a>
                            <a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md followup_modal" title="Follow Up" data-toggle="modal" data-target="#add_followup_data" quotation_id="<?php echo $single_quotation_siblings['quotation_mst_id'];?>" client_id="<?php echo $single_quotation_siblings['client_id'];?>">
                                <i class="la la-calendar-plus-o"></i>
                            </a>
                        </td>
                    </tr>
                <?php $count_no++;}?>
            <?php }?>
        </tbody>
    </table>

<?php }?>