<div class="row">
		<div class="col-md-2 form-group">
    	<label for="contact_date">Follow Up Date</label>
    	<input class="form-control quotation_date_picker" type="date" name="followedup_on" value="<?php echo $follow_up_date; ?>" readonly/>
  	</div>
    <div class="col-md-2 form-group">
    	<label for="contact_date"> Next Follow_Up Date</label>
    	<input class="form-control quotation_date_picker" type="text" name="followup_date" value="<?php echo $next_follow_up_date; ?>">
  	</div>
    <div class="col-md-4 form-group">
        <label for="contact_date">Member Name</label>
        <select class="form-control select_picker" name="member_id">
        	<option value="">Select Member</option>
        	 <?php foreach ($members_details as $single_member_details) { ?>
	            <option value="<?php echo $single_member_details['comp_dtl_id']; ?>">
	        	    <?php echo $single_member_details['member_name']; ?>
	            </option>
	        <?php } ?>
        </select>
    </div>
	<div class="col-md-2">
		<label for="close_query">Email</label>
		<select class="form-control" name="email">
			<option value="No">No</option>
			<option value="Yes">Yes</option>
		</select>
	</div>              
	<div class="col-md-2 form-group">
    	<label for="contact_date">Connect Mode</label>
    	<select class="form-control select_picker" name="connect_mode">
    		<option value="">select</option>
        	<option value="call_connected">Call Connected</option>
            <option value="call_attempted">Call Attempted</option>
            <option value="email">Email</option>
            <option value="linkedin">Linkedin</option>
            <option value="whatsapp">Whatsapp</option>
    	</select>
    </div>
</div>
<div class="row">	                  
  	
    <div class="col-md-8 form-group">
        <label for="contact_date">Follow Up Details</label>
        <textarea class="form-control" name="follow_up_text" value=""></textarea>
    </div>
	<div class="col-md-2"></div>
	<div class="col-md-2" style="padding: 3% 0% 0% 0%;">
        <button class="btn btn-success save_followup" type="reset" id="quotation_id">
			Update Follow Up Details
		</button>
    </div>
</div>
<hr/>
<h4>Follow Up History</h4>
<div>
	<table class="table table-bordered">
	   <thead>
		   <tr>
			   	<th>Sr</th>
			   	<th>Follow Up Date</th>
			   	<th>Follow Up Details</th>
		   </tr>
		</thead>
		<tbody>
		<?php if(!empty($follow_up_details)){ ?>
			<?php foreach ($follow_up_details as $follow_up_details_key => $single_follow_up_details) { ?>
			  	<tr>
				   	<td><?php echo $follow_up_details_key+1; ?></td>
				   	<td><?php echo $single_follow_up_details['followedup_on']; ?></td>
				   	<td><?php echo $single_follow_up_details['follow_up_text']; ?></td>
			   </tr>
			<?php } ?>
		<?php } ?>
		</tbody>
	</table>
</div>