<table cellpadding="5" cellspacing="0">
    <tr style="background-color: #fff;">
    <td width="50%" style="padding:5px;vertical-align: text-top; ">
    <img src="<?php echo $pdf_information['logo_path'];?>" width="180" height="50" style="padding-left: 10px;"><br/>
    <strong style="font-size: 17px;"><?php echo $pdf_information['name'];?></strong>
    <div class="left_addrs" style="font-size: 14px;color: #484545; line-height: 20px;"><?php echo $pdf_information['address'];?>
    </div>
    <table style="margin-top: -15px;">
    <tr style="background-color: #fff;">
    <td style="font-size: 14px; color: #484545; line-height: 20px;" width="50%"><?php echo $pdf_information['number'];?></td>
    </tr>
    <tr style="background-color: #fff;">
    <td style="font-size: 14px; color: #484545; line-height: 20px;" align="left"><?php echo $pdf_information['email'];?></td>
    </tr>
    </table>
    </td>
    <?php if(in_array($this->session->userdata('quotation_tab_name'), array('quotation_list', 'followup_list', 'draft_list'))){ ?>
        <td width="50%"><strong style="font-size: 25px; text-align: right; line-height: 35px;">Quotation</strong><br/>
        <table style="line-height: 22px;">
        <tr style="background-color: #e4e1e1; border-bottom: 1px solid;">
        <td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;"> Quote # : </strong></td>
        <td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;">Date : </strong></td>
        </tr>
        <tr>
        <td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;"><?php echo $quotation_data['quote_no'];?></td>
        <td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;"><?php echo date('d-m-Y', strtotime($quotation_data['entered_on']));?></td>
        </tr>

    <?php } if(in_array($this->session->userdata('quotation_tab_name'), array('proforma_list'))){ ?>

		<td width="50%"><strong style="font-size: 25px; text-align: right; line-height: 35px;">Proforma Invoice</strong><br/>
        <table style="line-height: 22px;">
        <tr style="background-color: #e4e1e1; border-bottom: 1px solid;">
        <td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;"> Proforma # : </strong></td>
        <td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;">Date : </strong></td>
        </tr>
        <tr>
        <td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;"><?php echo $quotation_data['proforma_no'];?></td>
        <td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;"><?php echo date('d-m-Y', strtotime($quotation_data['entered_on']));?></td>
        </tr>
    <?php }?>
    <tr style="background-color: #e4e1e1;">
    <td colspan="2" style="padding:5px;vertical-align: text-top;"><strong style="font-size: 12px;"> Customer:</strong></td>
    </tr>
    <tr>
    <?php if($this->session->userdata('quotation_access')['quotation_pdf_client_name_access']){?>
    <td colspan="2" style="padding:5px;vertical-align: text-top; font-family: courier;"><strong><?php echo $client_details['name'];?></strong>
    <div class="left_addrs" style="font-size: 11px;color: #484545; line-height: 17px; font-family: courier;"><?php echo $country_details['name'];?><br/><?php echo $member_details['member_name']?>
    </div>
    </td>
    <?php }?>
    </tr>
    <?php if(in_array($this->session->userdata('quotation_tab_name'), array('quotation_list', 'followup_list', 'draft_list'))){ ?>
        <tr style="background-color: #e4e1e1;">
        <td colspan="2" style="padding:5px;vertical-align: text-top;"><strong style="font-size: 12px;">Reference :</strong></td>
        </tr>
        <tr>
        <td colspan="2" style="font-family: courier; font-size: 11px;color: #484545;"><?php echo $quotation_data['reference'];?></td>
        </tr>
    <?php } if(in_array($this->session->userdata('quotation_tab_name'), array('proforma_list'))){ ?>

        <tr style="background-color: #e4e1e1;">
        <td colspan="2" style="padding:5px;vertical-align: text-top;"><strong style="font-size: 12px;">Order # :</strong></td>
        </tr>
        <tr>
        <td colspan="2" style="font-family: courier; font-size: 11px;color: #484545;"><?php echo $quotation_data['order_no'];?></td>
        </tr>
    <?php }?>
    </table>
    </td>
    </tr>
    <tr>
    <td colspan="2"></td>
    </tr>
    <tr>
    <td colspan="2"><table cellspacing="0" cellpadding="10" border="0">
    <thead>
    <tr style="background-color: #e4e1e1;">
    <td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="7%;">#</td>
    <td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="36%">Item Description</td>
    <td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="12%">Qty</td>
    <td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="8%">Unit</td>
    <td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="13%">Price</td>
    <td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="9%">Unit</td>
    <td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="15%">Total</td>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($quotation_details as $quotation_key => $single_quotation_details) {
            $bg_color = '';
            if(($quotation_key)%2 != 0){
                $bg_color = '#e4e1e1;';
            } ?>

            <tr style="background-color: <?php echo $bg_color;?>; font-size: 11px; font-family: courier;">
                <td style="text-align: right;"><?php echo ++$quotation_key;?></td>
                <td><?php echo htmlentities($single_quotation_details['description']);?></td>

                <td style="text-align: right;">
                    <?php if(strpos($single_quotation_details['quantity'], '.') === false){
                        $single_quotation_details['quantity'] = $single_quotation_details['quantity'].".00";
                    } ?>
                    <?php echo $single_quotation_details['quantity'];?>
                </td>

                <td><?php echo ucwords(strtolower($single_quotation_details['unit_value']));?>.</td>

                <?php if($this->session->userdata('quotation_access')['quotation_pdf_line_item_amount_access']){

                    if((int)$single_quotation_details['unit_price'] > 0 || (int)$single_quotation_details['row_price'] > 0) {
                
                        if(strpos($single_quotation_details['unit_price' ], '.') === false){
                            $single_quotation_details['unit_price' ] = $single_quotation_details['unit_price' ].".00";
                        } ?>

                        <td style="text-align: center;"><?php echo $currency_details['currency_icon'].$single_quotation_details['unit_price'];?></td>
                        <td style="text-align: center;">P.<?php echo ucwords(strtolower($single_quotation_details['unit_value']));?>.</td>
                        <td style="text-align: center;"><?php echo $currency_details['currency_icon'].$single_quotation_details['row_price']; ?></td>
                    
                    <?php }else { ?>
                            
                            <td style="text-align: center;">REGRET</td>		
                            <td style="text-align: center;">P.<?php echo ucwords(strtolower($unit_details[$quotation_key]['unit_value']));?>.</td>
                            <td style="text-align: center;">REGRET</td>		
                    <?php } 
                } ?>
            </tr>
        <?php }?>
    </tbody>
    </table>
    </td>
    </tr>
    <tr>
    <td><strong>Total in words:</strong><br/><span style="font-family: courier; font-size: 11px;"><?php echo $grand_total_words;?></span><br/>
    <hr/>
    <table cellspacing="0" cellpadding="3" border="0">
    <tr>
    <td><strong>Additional Notes</strong></td>
    </tr>
    <tr>
    <td style="font-family: courier; font-size: 11px;">We reserve the right to correct the pricing offered due to any typographical errors.<br/>This offer is not valid for end users from Iran, Iraq, North Korea, Cuba, Sudan & Syria.<?php echo $additional_comment;?>
    </td>
    </tr>
    </table>
    </td>
    <td>
    <table cellspacing="0" cellpadding="10">
        
        <?php if($this->session->userdata('quotation_access')['quotation_pdf_total_amount_access']){ ?>
            <tr>
                <td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;" width="50%" >Net Total</td>
                <td style="font-size: 12px; font-family: courier; border: solid 1px grey;" width="50%"><?php echo $currency_details['currency_icon'].$net_total;?></td>
            </tr>
            <?php if($quotation_data['freight'] > 0){
                if(strpos($quotation_data['freight'], '.') === false){
                    $quotation_data['freight'] = $quotation_data['freight'].'.00';
                } ?>
                <tr>
                    <td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Freight Charges</td>
                    <td style="font-size: 12px; font-family: courier; border: solid 1px grey;"><?php echo $currency_details['currency_icon'].$quotation_data['freight'];?></td>
                </tr>
            <?php }?>
            <?php if($quotation_data['bank_charges'] > 0){
                if(strpos($quotation_data['bank_charges'], '.') === false){
                    $quotation_data['bank_charges'] = $quotation_data['bank_charges'].'.00';
                } ?>
                <tr>
                    <td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Bank Charges</td>
                    <td style="font-size: 12px; font-family: courier; border: solid 1px grey;"><?php echo $currency_details['currency_icon'].$quotation_data['bank_charges'];?></td>
                </tr>
            <?php }?>
            <?php if($quotation_data['gst'] > 0 && $currency_details['currency'] == 'INR'){
                if(strpos($quotation_data['gst'], '.') === false){
                    $quotation_data['gst'] = $quotation_data['gst'].'.00';
                } ?>
                <tr>
                    <td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">GST</td>
                    <td style="font-size: 12px; font-family: courier; border: solid 1px grey;"><?php echo $currency_details['currency_icon'].$quotation_data['gst'];?></td>
                </tr>
            <?php }?>
            <?php if($quotation_data['other_charges'] > 0){
                if(strpos($quotation_data['other_charges'], '.') === false){
                    $quotation_data['other_charges'] = $quotation_data['other_charges'].'.00';
                } ?>
                <tr>
                    <td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Other Charges</td>
                    <td style="font-size: 12px; font-family: courier; border: solid 1px grey;"><?php echo $currency_details['currency_icon'].$quotation_data['other_charges'];?></td>
                </tr>
            <?php }?>
            <?php if($quotation_data['discount'] > 0){
                if(strpos($quotation_data['discount'], '.') === false){
                    $quotation_data['discount'] = $quotation_data['discount'].'.00';
                } ?>
                <tr>
                    <td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Discount</td>
                    <td style="font-size: 12px; font-family: courier; border: solid 1px grey;"><?php echo $currency_details['currency_icon'].$quotation_data['discount'];?></td>
                </tr>
            <?php }?>
            <?php 
            if(!empty($quotation_data['grand_total'] && $quotation_data['grand_total'] > 0)){
                if(strpos($quotation_data['grand_total'], '.') === false){
                    $quotation_data['grand_total'] = $quotation_data['grand_total'].'.00';
                } ?>

                <tr>
                    <td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Grand Total</td>
                    <td style="font-size: 12px; font-family: courier; border: solid 1px grey;"><?php echo $currency_details['currency_icon'].$grand_total;?></td>
                </tr>
            <?php  }?>

        <?php }else{?>
            <tr>
                <td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;" width="50%" >Net Total</td>
                <td style="font-size: 12px; font-family: courier; border: solid 1px grey;" width="50%"><?php echo $currency_details['currency_icon'].'0';?></td>
            </tr>
            <tr>
                <td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Grand Total</td>
                <td style="font-size: 12px; font-family: courier; border: solid 1px grey;"><?php echo $currency_details['currency_icon'].'0';?></td>
            </tr>
        <?php  }?>
  
    </table>
    </td>
    </tr>
    <br/>
    <br/>
    <tr>
    <td>
    <table>
    <tr>
    <td colspan="2"><strong>Terms and Conditions</strong></td>
    </tr>
    <tr>
    <td width="35%">Delivered To : </td> <td width="65%"><span style="font-family: courier;"><?php echo $delivery_to_details['delivery_name'];?></span></td>
    </tr>
    <tr>
    <td>Delivery Time : </td> <td><span style="font-family: courier;"><?php echo $delivery_time_details['dt_value'];?></span></td>
    </tr>
    <tr>
    <td>Validity : </td> <td><span style="font-family: courier;"><?php echo $validity_details['validity_value'];?></span></td>
    </tr>
    <tr>
    <td>Currency : </td> <td><span style="font-family: courier;"><?php echo $currency_details['currency']?></span></td>
    </tr>
    <tr>
    <td>Country of Origin : </td> <td><span style="font-family: courier;"><?php echo $origin_country_details['country'];?></span></td>
    </tr>
    <tr>
    <td>MTC Type : </td> <td><span style="font-family: courier;"><?php echo $mtc_type_details['mtc_value'];?></span></td>
    </tr>
    <tr>
    <td>Packing Type : </td> <td><span style="font-family: courier;"><?php echo $transport_details['mode'];?></span></td>
    </tr>
    <tr>
    <td>Payment : </td> <td><span style="font-family: courier;"><?php echo $payment_details['term_value'];?></span></td>
    </tr>
    </table>
    <?php if(in_array($this->session->userdata('quotation_tab_name'), array('proforma_list'))){ ?>
        <br/><br/>
        <table>
        <tr>
            <td colspan="2"><strong>    Bank Details</strong></td>
        </tr>
        <tr>
        <td width="35%">    Bank Name : </td> <td width="65%"><span style="font-family: courier;"><?php echo $pdf_information['bank_name'];?></span></td>
        </tr>
        <tr>
        <td>    Branch Name : </td><td><span style="font-family: courier;"><?php echo $pdf_information['branch_name'];?></span></td>
        </tr>
        <tr>
        <td>    Beneficiary Name : </td><td><span style="font-family: courier;"><?php echo $pdf_information['beneficiary_name'];?></span></td>
        </tr>
        <tr>
        <td>    Account Number : </td><td><span style="font-family: courier;"><?php echo $pdf_information['account_number'];?></span></td>
        </tr>
        <tr>
        <td>    Swift Code : </td><td><span style="font-family: courier;"><?php echo $pdf_information['swift_code'];?></span></td>
        </tr>
        <tr>
        <td>    IFSC CODE : </td><td><span style="font-family: courier;"><?php echo $pdf_information['ifsc_code'];?></span></td>
        </tr>
        </table>
    <?php }?>
    </td>
    <td align="center">
    Thank you for your business<br/>
    For <?php echo $pdf_information['name'];?><br/><br/>
    <img src="<?php echo $pdf_information['stamp_path'];?>" height="100px" weight="100px" /><br/><br/>
    <table>
    <tr>
    <td width="26%" rowspan="3"></td>
    <td align="left" width="74%">Name : <span style="font-family: courier;"><?php echo $assigned_to_person['name'];?></span></td>
    </tr>
    <tr>
    <td align="left">Email : <span style="font-family: courier;"><?php echo $assigned_to_person['email'];?></span></td>
    </tr>
    <tr>
    <td align="left">Mobile : <span style="font-family: courier;"><?php echo $assigned_to_person['mobile'];?></span></td>
    </tr>
    </table>
    </td>
    </tr>
</table>
<?php if(in_array($this->session->userdata('quotation_tab_name'), array('quotation_list', 'followup_list', 'draft_list'))){

    if(!empty($country_details['name']) && $country_details['name'] == 'Saudi Arabia') { ?>

		<div style="width:100%">
			<img src="/assets/media/saudi_extra_banner.png" style="padding: 2% 0% 0% 20%;"/>
		</div>
	<?php }elseif(!empty($country_details['name']) && $country_details['name'] == 'Qatar'){ ?>

		<div style="width:70%">
			<img src="/assets/media/qatar_extra_banner.png" style="padding: 2% 10% 10% 20%;"/>
		</div>
	<?php }
}?>
