<?php if(in_array($this->session->userdata('quotation_tab_name'), array('quotation_list', 'draft_list'))) {?>

    <?php if(!empty($quotation_list)) { ?>
        <?php foreach ($quotation_list as $quotation_list_key => $single_list_data_value) { ?>	

            <tr role="row" class="quote_font <?php echo (($quotation_list_key+1)/2 == 00)?'even':'odd'?> <?php  echo $single_list_data_value['backgound']?>">

                <td class="kt-align-center"><?php echo $quotation_list_key+1; ?>
                <?php if($single_list_data_value['importance'] > 0){ ?>
                    <div class="kt-font-warning kt-demo-icon__preview" data-toggle="kt-tooltip" data-placement="left" title="<?php echo $single_list_data_value['importance']?>" data-original-title="Tooltip title">
                        <i class="la la-star" style="line-height: 0;vertical-align: middle;font-size: 1.5rem !important;"></i>
                    
                    </div>
                <?php } ?>
                <hr style="margin: 2px;">
                <?php if($single_list_data_value['client_type'] == 'omtubes'){?>
                    <span>
                        <span class="kt-font-bolder" style="width: auto; padding: 10px;">OM</span>
                        <hr style="margin: 2px;">
                    </span>
                <?php }elseif($single_list_data_value['client_type'] == 'zengineer'){?>
                    <span>
                        <span class="kt-font-bolder" style="width: auto; padding: 10px;">Z</span>
                        <hr style="margin: 2px;">
                    </span>
                <?php }elseif($single_list_data_value['client_type'] == 'instinox'){?>
                    <span>
                        <span class="kt-font-bolder" style="width: auto; padding: 10px;">IN</span>
                        <hr style="margin: 2px;">
                    </span>
                <?php }?>
                </td>
                <td class="kt-align">
                    <span>
                        <?php echo $single_list_data_value['quote_no']; ?>  
                        <?php if($single_list_data_value['quote_no'] != ''){

                            if($single_list_data_value['is_new'] == 'Y'){ ?>
                                <sup>
                                    <span class="badge badge-sm badge-danger">New</span>
                                </sup>
                            <?php }
                        }?>
                        <hr style="margin-top: 0px;margin-bottom: 0px;"></hr>
                        <span><?php echo $single_list_data_value['procurement_person_name']; ?></span>
                        <hr style="margin-top: 0px;margin-bottom: 0px;"></hr>			    
                        <span><?php echo $single_list_data_value['made_by_name']; ?></span>
                        <hr style="margin-top: 0px;margin-bottom: 0px;"></hr>
                        <span>
                            <span class="kt-font-bolder <?php echo $quotation_family_badge[$single_list_data_value['product_family']];?>" style="width: auto; padding: 10px;">
                                <?php echo $single_list_data_value['product_family'];?>
                            </span>
                            <hr style="margin: 2px;">
                            <?php echo $single_list_data_value['priority_div'];?>
                        </span>
                    </span>
                </td>
                <td><?php echo $single_list_data_value['assigned_to']; ?></td>
                <td><?php echo $single_list_data_value['entered_on_date'];?></td>
                <?php if($this->session->userdata('quotation_access')['quotation_list_client_name_access']){ ?>
                <td>
                    <?php echo $single_list_data_value['customer_name'];?>
                    <?php if(!empty($single_list_data_value['member_name'])){?>
                        <hr style="margin-top: 0px;margin-bottom: 0px;"></hr>
                        <span>
                            <?php echo $single_list_data_value['member_name'];?>
                        </span>
                    <?php } ?> 
                </td>
                <?php }?>
                <?php if($this->session->userdata('quotation_access')['quotation_list_value_access']){ ?>
                    <td class="kt-align-right" style="padding: 4px;">
                        <?php echo $single_list_data_value['value']; ?><br>
                        <?php if($this->session->userdata('quotation_access')['quotation_list_avg_margin_access']){
                            if(!empty($single_list_data_value['avg_margin'])){
                            if(!empty($single_list_data_value['selling_margin_color'])){ ?>

                                <span style="color: <?php echo $single_list_data_value['selling_margin_color']; ?>">
                                    ( <?php echo $single_list_data_value['avg_margin']; ?>%)
                                </span>
                            <?php } else { ?>

                                <span style="color:#151215 !important">
                                    ( <?php echo $single_list_data_value['avg_margin']; ?>%)
                                </span>
                            <?php } ?>
                        <?php }
                        } ?>
                    </td>
                <?php }?>
                <td><?php echo $single_list_data_value['country_name'];?></td>
                <td><?php echo $single_list_data_value['follow_up_date'];?></td>
                <td><?php if($single_list_data_value['status'] == 'Open'){?>
                        <span class="badge badge-primary" style="color: #fff;">
                            <?php echo $single_list_data_value['status'];?>
                        </span>
                    <?php } else if($single_list_data_value['status'] == 'Won'){?>
                        <span class="badge badge-success" style="color: #fff;">
                            <?php echo $single_list_data_value['status'];?>
                        </span>
                    <?php } else if($single_list_data_value['status'] == 'Closed'){?>
                        <span class="badge badge-danger" style="color: #fff;">
                            <?php echo $single_list_data_value['status'];?>
                        </span>
                    <?php }?>
                </td>
                <td><?php echo $single_list_data_value['follow_up_text'];?></br></br>
                    <span style="font-size: 10px;">
                        <?php echo $single_list_data_value['followedup_on'];?>
                    </span><br/>
                    <?php echo $single_list_data_value['close_reason_text']; ?>				
                </td>
                <td>
                    <?php if($this->session->userdata('quotation_access')['quotation_list_action_view_quotation_details_access']){ ?>
                        <a href="<?php echo base_url('quotations/viewQuotation/'.$single_list_data_value['quotation_mst_id']);?>" target="_blank" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View Quotation Details">
                            <i class="fa fa-file-invoice"></i>
                        </a>
                    <?php } ?>
                    <?php if($this->session->userdata('quotation_access')['quotation_list_action_view_pdf_access']){ ?>
                        <a href="<?php echo base_url('pdf_management/quotation_pdf/'.$single_list_data_value['quotation_mst_id']);?>" target="_blank" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Pdf" >
                            <i class="la la-eye"></i>
                        </a>
                    <?php } ?>
                    <?php if($this->session->userdata('quotation_access')['quotation_list_action_edit_access']){ ?>
                        <a href="<?php echo base_url('quotations/add/'.$single_list_data_value['quotation_mst_id'].'/'.$single_list_data_value['rfq_id']);?>" target="_blank" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">
                            <i class="la la-edit"></i>
                        </a>
                    <?php } ?>
                    <?php if($this->session->userdata('quotation_access')['quotation_list_action_delete_access']){ ?>
                        <a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_quotation" title="Delete" quotation_id="<?php echo $single_list_data_value['quotation_mst_id'];?>">
                            <i class="la la-trash"></i>
                        </a>
                    <?php } ?>
                    <?php if($this->session->userdata('quotation_access')['quotation_list_action_follow_up_access'] && $single_list_data_value['stage'] == 'publish'){ ?>
                        <a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md add_followup_modal" title="Follow Up" data-toggle="modal" data-target="#add_followup" quotation_id="<?php echo $single_list_data_value['quotation_mst_id'];?>" client_id="<?php echo $single_list_data_value['client_id'];?>">
                            <i class="la la-calendar-plus-o"></i>
                        </a>
                    <?php } ?>
                    <?php if($this->session->userdata('quotation_access')['quotation_list_action_query_access']){ ?>
                        <a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon btn-icon-md sales_query" title="Sales Query" data-toggle="modal" data-target="#add_query" query_type="sales" quotation_id="<?php echo $single_list_data_value['quotation_mst_id'];?>" >
                            <i class="fa fa-question-circle" style="color: bluew;"></i>
                        </a>
                    <?php } ?>
                    <?php if($this->session->userdata('quotation_access')['quotation_list_action_graph_access']){ ?>
                        <a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon btn-icon-md graph_details" title="view Graph" company_name="<?php echo $single_list_data_value['customer_name'];?>" company_id="<?php echo $single_list_data_value['client_id'];?>">
						    <i class="fa fa-bar-chart"></i>
					    </a>
                    <?php } ?>
                    <?php if($this->session->userdata('quotation_access')['quotation_list_action_rating_access']){ ?>
                        <a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon btn-icon-md add_rating" title="Rating" quotation_id="<?php echo $single_list_data_value['quotation_mst_id'];?>" quotation_priority="<?php echo $single_list_data_value['quotation_priority'];?>" priority_reason="<?php echo $single_list_data_value['priority_reason'];?>" data-toggle="modal" data-target="#update_rating">
                            <i class="flaticon-star"></i>
                        </a>
                    <?php } ?>
                </td>
            </tr>
        <?php }	
    } ?>

<?php } else if(in_array($this->session->userdata('quotation_tab_name'), array('followup_list'))) {?>

    <?php if(!empty($quotation_list)) { ?>
        <?php foreach ($quotation_list as $quotation_list_key => $single_list_data_value) { ?>	

            <tr role="row" class="quote_font <?php echo (($quotation_list_key+1)/2 == 00)?'even':'odd'?>">

                <td class="kt-align-center"><?php echo $quotation_list_key+1; ?>
                <hr style="margin: 2px;">
                <?php if($single_list_data_value['client_type'] == 'omtubes'){?>
                    <span>
                        <span class="kt-font-bolder" style="width: auto; padding: 10px;">OM</span>
                        <hr style="margin: 2px;">
                    </span>
                <?php }elseif($single_list_data_value['client_type'] == 'zengineer'){?>
                    <span>
                        <span class="kt-font-bolder" style="width: auto; padding: 10px;">Z</span>
                        <hr style="margin: 2px;">
                    </span>
                <?php }elseif($single_list_data_value['client_type'] == 'instinox'){?>
                    <span>
                        <span class="kt-font-bolder" style="width: auto; padding: 10px;">IN</span>
                        <hr style="margin: 2px;">
                    </span>
                <?php }?>
                </td>
                <td class="kt-align">
                    <span>
                        <?php echo $single_list_data_value['quote_no']; ?>
                        <?php echo $single_list_data_value['priority_div'];?>
                    </span>
                </td>
                <td><?php echo $single_list_data_value['assigned_to']; ?></td>
                <td><?php echo $single_list_data_value['entered_on_date'];?></td>
                <?php if($this->session->userdata('quotation_access')['quotation_list_client_name_access']){ ?>
                <td>
                    <?php echo $single_list_data_value['customer_name'];?>
                    <hr style="margin-top: 0px;margin-bottom: 0px;"></hr>
                    <?php if(!empty($single_list_data_value['member_name'])){?>
                        <span>Member :-
                            <?php echo $single_list_data_value['member_name'];?>
                        </span>
                    <?php } ?> 
                </td>
                <?php }?>
                <?php if($this->session->userdata('quotation_access')['quotation_list_value_access']){ ?>
                    <td class="kt-align-right" style="padding: 4px;">
                        <?php echo $single_list_data_value['value']; ?><br>
                        <?php if($this->session->userdata('quotation_access')['quotation_list_avg_margin_access']){
                            if(!empty($single_list_data_value['avg_margin'])){
                            if(!empty($single_list_data_value['selling_margin_color'])){ ?>

                                <span style="color: <?php echo $single_list_data_value['selling_margin_color']; ?>">
                                    ( <?php echo $single_list_data_value['avg_margin']; ?>%)
                                </span>
                            <?php } else { ?>

                                <span style="color:#151215 !important">
                                    ( <?php echo $single_list_data_value['avg_margin']; ?>%)
                                </span>
                            <?php }?>
                        <?php }
                        } ?>
                    </td>
                <?php }?>
                <td><?php echo $single_list_data_value['country_name'];?></td>
                <td><?php echo $single_list_data_value['follow_up_date'];?></td>
                <td><?php if($single_list_data_value['status'] == 'Open'){?>
                        <span class="badge badge-primary" style="color: #fff;">
                            <?php echo $single_list_data_value['status'];?>
                        </span>
                    <?php } else if($single_list_data_value['status'] == 'Won'){?>
                        <span class="badge badge-success" style="color: #fff;">
                            <?php echo $single_list_data_value['status'];?>
                        </span>
                    <?php } else if($single_list_data_value['status'] == 'Closed'){?>
                        <span class="badge badge-danger" style="color: #fff;">
                            <?php echo $single_list_data_value['status'];?>
                        </span>
                    <?php }?>
                </td>
                <td><?php echo $single_list_data_value['follow_up_text'];?></br></br>
                    <span style="font-size: 10px;">
                        <?php echo $single_list_data_value['followedup_on'];?>
                    </span><br/>
                    <?php echo $single_list_data_value['close_reason_text']; ?>				
                </td>
                <td>
                    <?php if($this->session->userdata('quotation_access')['quotation_list_action_view_quotation_details_access']){ ?>
                        <a href="<?php echo base_url('quotations/viewQuotation/'.$single_list_data_value['quotation_mst_id']);?>" target="_blank" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View Quotation Details">
                            <i class="fa fa-file-invoice"></i>
                        </a>
                    <?php } ?>
                    <?php if($this->session->userdata('quotation_access')['quotation_list_action_view_pdf_access']){ ?>
                        <a href="<?php echo base_url('pdf_management/quotation_pdf/'.$single_list_data_value['quotation_mst_id']);?>" target="_blank" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Pdf" >
                            <i class="la la-eye"></i>
                        </a>
                    <?php } ?>
                    <?php if($this->session->userdata('quotation_access')['quotation_list_action_edit_access']){ ?>
                        <a href="<?php echo base_url('quotations/add/'.$single_list_data_value['quotation_mst_id']);?>" target="_blank" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">
                            <i class="la la-edit"></i>
                        </a>
                    <?php } ?>
                    <?php if($this->session->userdata('quotation_access')['quotation_list_action_delete_access']){ ?>
                        <a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_quotation" title="Delete" quotation_id="<?php echo $single_list_data_value['quotation_mst_id'];?>">
                            <i class="la la-trash"></i>
                        </a>
                    <?php } ?>
                    <?php if($this->session->userdata('quotation_access')['quotation_list_action_follow_up_access'] && $single_list_data_value['stage'] == 'publish'){ ?>
                        <a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md add_followup_modal" title="Follow Up" data-toggle="modal" data-target="#add_followup" quotation_id="<?php echo $single_list_data_value['quotation_mst_id'];?>" client_id="<?php echo $single_list_data_value['client_id'];?>">
                            <i class="la la-calendar-plus-o"></i>
                        </a>  
                    <?php } ?>
                    <?php if($this->session->userdata('quotation_access')['quotation_list_action_graph_access']){ ?>
                        <a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon btn-icon-md graph_details" title="view Graph" company_name="<?php echo $single_list_data_value['customer_name'];?>" company_id="<?php echo $single_list_data_value['client_id'];?>">
						    <i class="fa fa-bar-chart"></i>
					    </a>
                    <?php }?>
                    <?php if($this->session->userdata('quotation_access')['quotation_list_action_rating_access']){ ?>
                        <a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon btn-icon-md add_rating" title="Rating" quotation_id="<?php echo $single_list_data_value['quotation_mst_id'];?>" quotation_priority="<?php echo $single_list_data_value['quotation_priority'];?>" priority_reason="<?php echo $single_list_data_value['priority_reason'];?>" data-toggle="modal" data-target="#update_rating">
                            <i class="flaticon-star"></i>
                        </a>
                    <?php } ?>
                </td>
            </tr>
        <?php }	
    } ?>

<?php } else if(in_array($this->session->userdata('quotation_tab_name'), array('proforma_list'))) {?>
    
    <?php if(!empty($quotation_list)) { ?>
		<?php foreach ($quotation_list as $quotation_list_key => $single_list_data_value) { ?>	
            <tr role="row" class="quote_font <?php echo (($quotation_list_key+1)/2 == 00)?'even':'odd'?>">
                
                <td class="kt-align-center"><?php echo $quotation_list_key+1; ?>
                <hr style="margin: 2px;">
                <?php if($single_list_data_value['client_type'] == 'omtubes'){?>
                    <span>
                        <span class="kt-font-bolder" style="width: auto; padding: 10px;">OM</span>
                        <hr style="margin: 2px;">
                    </span>
                <?php }elseif($single_list_data_value['client_type'] == 'zengineer'){?>
                    <span>
                        <span class="kt-font-bolder" style="width: auto; padding: 10px;">Z</span>
                        <hr style="margin: 2px;">
                    </span>
                <?php }elseif($single_list_data_value['client_type'] == 'instinox'){?>
                    <span>
                        <span class="kt-font-bolder" style="width: auto; padding: 10px;">IN</span>
                        <hr style="margin: 2px;">
                    </span>
                <?php }?>
                </td>
                <td><?php echo $single_list_data_value['proforma_no'];?>
                    <?php echo $single_list_data_value['priority_div'];?>
                </td>
                <td><?php echo $single_list_data_value['assigned_to'];?></td>
                <td><?php echo $single_list_data_value['procurement_person_name'];?></td>
                <td><?php echo $single_list_data_value['confirmed_on_date'];?></td>
                <?php if($this->session->userdata('quotation_access')['quotation_list_client_name_access']){ ?>
                <td>
                    <?php echo $single_list_data_value['customer_name'];?>
                    <hr style="margin-top: 0px;margin-bottom: 0px;"></hr>
                    <?php if(!empty($single_list_data_value['member_name'])){?>
                        <span><?php echo $single_list_data_value['member_name'];?></span>
                    <?php } ?> 
                </td>
                <?php }?>
                <?php if($this->session->userdata('quotation_access')['quotation_list_value_access']){ ?>
                    <td class="kt-align-right" style="padding: 4px;">
                        <?php echo $single_list_data_value['value']; ?><br>
                        <?php if($this->session->userdata('quotation_access')['quotation_list_avg_margin_access']){
                            if(!empty($single_list_data_value['avg_margin'])){
                            if(!empty($single_list_data_value['selling_margin_color'])){ ?>

                                <span style="color: <?php echo $single_list_data_value['selling_margin_color']; ?>">
                                    ( <?php echo $single_list_data_value['avg_margin']; ?>%)
                                </span>
                            <?php } else { ?>

                                <span style="color:#151215 !important">
                                    ( <?php echo $single_list_data_value['avg_margin']; ?>%)
                                </span>
                            <?php } ?>
                        <?php }
                        } ?>
                    </td>
                <?php }?>
                <td><?php echo $single_list_data_value['country_name'];?></td>
                <td><span class="badge badge-success" style="color: #fff;">
                        <?php echo $single_list_data_value['status'];?>
                    </span>
                </td>
                <td>
                    <?php if($this->session->userdata('quotation_access')['quotation_list_action_view_proforma_pdf_access']){ ?>
                        <a href="<?php echo base_url('pdf_management/proforma_pdf/'.$single_list_data_value['quotation_mst_id']);?>" target="_blank" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Pdf" >
                            <i class="la la-eye"></i>
                        </a>
                    <?php } ?>
                    <?php if($this->session->userdata('quotation_access')['quotation_list_action_delete_access']){ ?>
                        <a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_quotation" title="Delete" quotation_id="<?php echo $single_list_data_value['quotation_mst_id'];?>">
                            <i class="la la-trash"></i>
                        </a>
                    <?php } ?>
                    <?php if($this->session->userdata('quotation_access')['quotation_list_action_purchase_order_add_access']){ ?>
                        <?php if($single_list_data_value['purchase_order'] != '' && $single_list_data_value['purchase_order'] != NULL){ ?>
                            <a href="<?php echo site_url('/assets/purchase_orders/');?><?php echo $single_list_data_value['purchase_order'];?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View Purchase Order" target="_blank" >
                                <i class="fa fa-file-pdf-o"></i>
                            </a>
                            <a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_purchase_order" title="Delete Purchase Order" quotation_id="<?php echo $single_list_data_value['quotation_mst_id'];?>">
                                <i class="fa fa-folder-minus"></i>
                            </a>
                        <?php } else{ ?>

                            <a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon btn-icon-md upload_purchase_order_modal" title="Uploadpo" data-toggle="modal" data-target="#add_po" quotation_id="<?php echo $single_list_data_value['quotation_mst_id'];?>" >
                                <i class="fa fa-file-upload"></i>
                            </a>
                        <?php } ?>
                    <?php } ?>
                    <?php if($this->session->userdata('quotation_access')['quotation_list_action_purchase_order_view_access']){ ?>
                        <?php if($single_list_data_value['purchase_order'] != '' && $single_list_data_value['purchase_order'] != NULL){ ?>
                            <a href="<?php echo site_url('/assets/purchase_orders/');?><?php echo $single_list_data_value['purchase_order'];?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View Purchase Order" target="_blank" >
                                <i class="fa fa-file-pdf-o"></i>
                            </a>
                        <?php } ?>
                    <?php } ?>
                    <?php if($this->session->userdata('quotation_access')['quotation_list_action_graph_access']){ ?>
                        <a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon btn-icon-md graph_details" title="view Graph" company_name="<?php echo $single_list_data_value['customer_name'];?>" company_id="<?php echo $single_list_data_value['client_id'];?>">
						    <i class="fa fa-bar-chart"></i>
					    </a>
                    <?php }?>
                </td>
            </tr>
	    <?php }	
    } ?>
    
<?php } ?>




