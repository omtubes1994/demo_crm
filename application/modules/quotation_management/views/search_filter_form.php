<form class="kt-form kt-form--fit kt-margin-b-20" id="search_filter_form">
    <div class="quote_font" id="search_filter_div">
        <div class="row kt-margin-b-20"> 
            <?php if($this->session->userdata('quotation_access')['quotation_list_client_name_access']){ ?>
                <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                    <label>Company Name :</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="client_name" name="search_quotation_client_name" value="<?php echo $search_filter_form_data['search_quotation_client_name']; ?>" placeholder="min 4 character">
                    </div>
                    <select class="form-control select_picker" name="client_id" data-live-search = "true">
                        <option value="">Select Name</option>
                        <?php foreach($search_filter_form_data['client_id'] as $single_details){
                            
                            echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
                        }  ?>
                    </select>                
                </div>
                <div class="col-lg-2 kt-margin-b-10-tablet-and-mobile member_div <?php (count($search_filter_form_data['member']) == 0)?'kt-hidden':''; ?>">
                    <label>Member:</label>
                    <select class="form-control select_picker" name="member" data-live-search = "true">
                        <option value="">Select Name</option>
                        <?php foreach($search_filter_form_data['member'] as $single_details){
                            
                            echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['member_name'].'</option>';
                        }  ?>       
                    </select>
                </div> 
            <?php } ?>    
            <div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
                <label>Financial Year:</label>
                <select class="form-control select_picker" name="years" multiple>
                    <option value="">Select</option>
                    <?php foreach($search_filter_form_data['years'] as $single_details){
                        
                        echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['value'].'</option>';
                    }  ?>               
                </select>
            </div>                  
            <div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
                 <label>Sales Person :</label>
                <select class="form-control select_picker" name="assigned_to" data-live-search = "true" multiple>
                    <option value="">Select</option>
                    <?php foreach($search_filter_form_data['assigned_to'] as $single_details){
                        
                        echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
                    }  ?>
                </select>
            </div>
            
            <div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
                <label>Country :</label>
                <select class="form-control select_picker" name="country_name" data-live-search = "true" multiple>
                    <option value="">Select</option>
                    <?php foreach($search_filter_form_data['country_name'] as $single_details){
                        
                        echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['value'].'</option>';
                    }  ?>
                </select>
            </div>
            
        </div>
        <div class="row kt-margin-b-20">
            <div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
                <label>Value:</label>
                <select class="form-control select_picker" name="value" multiple>
                    <option value="">Select</option>
                    <?php foreach($search_filter_form_data['value'] as $single_details){
                        
                        echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
                    }?>
                </select>                
            </div>
            <div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
                <label>IMP:</label>
                <select class="form-control select_picker" name="importance" multiple>
                    <option value="">Select Type</option>
                    <?php foreach($search_filter_form_data['importance'] as $single_details){
                        
                        echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['value'].'</option>';
                    }?>  
                </select>
            </div>
            <?php if(in_array($tab_name, array('proforma_list'))) {?>
                <div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
                    <label>Proforma#</label>
                    <input type="text" class="form-control" name="proforma_no" value="<?php echo $search_filter_form_data['proforma_no'];?>" placeholder="Enter Proforma#"/>
                </div>
                <div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
                    <label>Purchase Person:</label>
                    <select class="form-control select_picker" name="procurement_person" data-live-search = "true" multiple>
                        <option value="">Select</option>
                        <?php foreach($search_filter_form_data['procurement_person'] as $single_details){

                            echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
                        }  ?>
                    </select>
                </div>
                <div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
                    <label>Date</label>
                    <input type="date" class="form-control quotation_date_picker" name="confirmed_on" value="<?php echo $search_filter_form_data['confirmed_on'];?>">
                </div>
            <?php } ?>
            <?php if(in_array($tab_name, array('quotation_list', 'draft_list', 'followup_list'))) {?>
                <div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
                    <label>Quote#</label>
                    <input type="text" class="form-control" name="quote_no" value="<?php echo $search_filter_form_data['quote_no'];?>" placeholder="Enter Quote#"/>
                </div>
                <div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
                    <label>Quotation New::</label>
                    <select class="form-control select_picker" name="quotation_new" data-live-search = "true" multiple>
                        <option value="">Select</option>
                        <?php foreach($search_filter_form_data['quotation_new'] as $single_details){

                            echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
                        } ?>
                    </select>
                </div>
                <div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
                    <label>Date</label>
                    <input type="date" class="form-control quotation_date_picker" name="entered_on" value="<?php echo $search_filter_form_data['entered_on'];?>">
                </div>
                <div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
                    <label>FUDate</label>
                    <input type="date" class="form-control quotation_date_picker" name="followup_date" value="<?php echo $search_filter_form_data['followup_date'];?>">
                </div>
            <?php } ?>
        </div>
        <div class="row kt-margin-b-20">
            <div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
                <label>Product Family</label>
                <select class="form-control select_picker" name="product_family" data-live-search = "true" multiple>
                    <option value="">Select</option>
                    <?php foreach($search_filter_form_data['product_family'] as $single_details){
                        
                        echo '<option value="'.$single_details['value'].'" data-content="'.$single_details['class'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';

                    } ?>
                </select>
            </div>
            <div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
                <label>Status:</label>
                <select class="form-control select_picker" name="status" multiple>
                    <option value="">Select</option>
                    <?php foreach($search_filter_form_data['status'] as $single_details){

                        echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
                    }?>
                </select>
            </div>
            <div class="col-md-2 kt-margin-b-10-tablet-and-mobile">
                <label>Priority range</label>
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="kt-ion-range-slider">
                        <input class="form-control quotation_priority "type="hidden" id="quotation_priority" name="quotation_priority" value="<?php echo $search_filter_form_data['quotation_priority'];?>">
                    </div>
		        </div>
            </div>
        </div>
        <div class="kt-separator kt-separator--md kt-separator--dashed"></div>
        <div class="row">
            <div class="col-lg-12">
                <button class="btn btn-primary btn-brand--icon search_filter_submit" type="reset">
                    <span>
                        <i class="la la-search"></i>
                        <span>Search</span>
                    </span>
                </button>
                &nbsp;&nbsp;
                <button class="btn btn-secondary btn-secondary--icon search_filter_reset" type="reset">
                    <span>
                        <i class="la la-close"></i>
                        <span>Reset</span>
                    </span>
                </button>
            </div>
        </div>
    </div>
</form>
                       