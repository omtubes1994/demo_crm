<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <style type="text/css">
		.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
			padding-top: 3% !important;
		}
    </style>

    <!-- begin:: Container -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">

                <div class="kt-portlet kt-portlet--mobile">
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label col-md-3">
                            <h3 class="kt-portlet__head-title">
                                <?php echo $quotation_data['quote_no'];?>
                            </h3>
					    </div>
                        <div class="kt-portlet__head-label col-md-3">
                            <h3 class="kt-portlet__head-title">
                                Company Name: <?php echo $customer_details['name'];?>
                            </h3>
					    </div>
                        <div class="kt-portlet__head-label col-md-3">
                            <h3 class="kt-portlet__head-title">
                                Value: <?php echo $currency_details['currency_icon'].$quotation_data['grand_total'];?>
                            </h3>
					    </div>
                    </div>

                    <div class="kt-portlet__body">
                        <div class="card">
						    <div class="card-body">
                                <form class="kt-form" id="quotation_view_details">
                                    <div class="form-group row">
                                        <div class="col-lg-4">
                                            <label class="">Importance</label>
                                            <select class="form-control select_picker" id="importance" name="importance">
                                                <option value="">Select</option>
                                                <option value="L" <?php echo ($quotation_data["importance"] == 'L') ? 'selected': '';?>>Low</option>
                                                <option value="M" <?php echo ($quotation_data["importance"] == 'M') ? 'selected': '';?>>Medium</option>
                                                <option value="H" <?php echo ($quotation_data["importance"] == 'H') ? 'selected': '';?>>High</option>
                                                <option value="V" <?php echo ($quotation_data["importance"] == 'V') ? 'selected': '';?>>Very High</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-4">
                                            <label class="">Status</label>
                                            <select class="form-control select_picker quotation_status" name="status">
                                                <option value="">Select</option>
                                                <option value="open" <?php echo ($quotation_data["status"] == 'open') ? 'selected': '';?>>Open</option>
                                                <option value="Won" <?php echo ($quotation_data["status"] == 'Won') ? 'selected': '';?>>Won</option>
                                                <option value="Closed" <?php echo ($quotation_data["status"] == 'Closed') ? 'selected': '';?>>Closed</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-4">
                                            <label class="form-control-label">Reason</label>
                                            <select class="form-control close_reason" name="close_reason" style="display:<?php echo ($quotation_data['status'] == 'Closed') ? 'block' : 'none';?>">
                                                <?php foreach ($reasons as $single_reasons) { ?>
                                                <option value="<?php echo $single_reasons['reason_id'];?>" 
                                                    <?php 
                                                        echo ($quotation_data['close_reason'] == $single_reasons['reason_id']) ? 'selected': '';
                                                    ?>>
                                                    <?php echo $single_reasons['reason_text']; ?>
                                                </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group" style="justify-content: left;">
                                        <button class="btn btn-success update_quotation" type="reset" quotation_id=<?php echo $quotation_data['quotation_mst_id'];?> style="float:left;"> Update </button>
                                    </div>
                                </form>
                            </div>
                        </div><br/><br/>

                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item active_list" id="quotation_details" view-sub-tab="quotation_details" quotation_id="<?php echo $quotation_data['quotation_mst_id'];?>">
                                <a class="nav-link">Quotation Details</a>
                            </li>
                            <li class="nav-item" view-sub-tab="followup_details" quotation_id="<?php echo $quotation_data['quotation_mst_id'];?>">
                                <a class="nav-link">Follow Up</a>
                            </li>
                            <li class="nav-item" view-sub-tab="client_details" quotation_id="<?php echo $quotation_data['quotation_mst_id'];?>">
                                <a class="nav-link">Contact Client</a>
                            </li>
                            <li class="nav-item" view-sub-tab="other_details" quotation_id="<?php echo $quotation_data['quotation_mst_id'];?>">
                                <a class="nav-link">Other Active Quotations</a>
                            </li>
					    </ul>

                        <div class="tab-content" id="quotation_details">
                            <!-- all tab data -->
                        </div>
                        
                        
                    </div>
                    
                </div>

            </div>
        </div>
    </div>
    <!-- end:: Container --> 

    <!-- begin:: model -->    
        <div class="modal fade" id="add_followup_data" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Follow Up</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="add_followup_data_form">
                            
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
                    </div>
                </div>
            </div>
        </div>
    <!-- end:: model -->
    
</div>
<!-- begin:: Content -->

