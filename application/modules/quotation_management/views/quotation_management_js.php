$(document).ready(function () {

    Quotation_Js.init();
    set_limit_offset(10, 0);
    get_quotation_list_body();

    $('a.add_search_filter').click(function () {

        var button_value = $(this).attr('button_value');
        if (button_value == 'show') {

            $('div.search_filter_data').slideDown();
            $(this).attr('button_value', 'hide');
            $(this).html('Hide Search Filter');
        } else {
            $('div.search_filter_data').slideUp();
            $(this).attr('button_value', 'show');
            $(this).html('Add Search Filter');
        }
    });

    $('div.search_filter_data').on('click', 'button.search_filter_submit', function () {

        set_limit_offset(10, 0);
        get_quotation_list_body();
    });

    $('div.search_filter_data').on('click', 'button.search_filter_reset', function () {

        set_limit_offset(10, 0);
        get_quotation_list_body(
                        "",
                        "",
                        []
                    );
    });

    $('div.quotation_paggination').on('click', 'li.paggination_number', function () {

        $('input#limit').val($(this).attr('limit'));
        $('input#offset').val($(this).attr('offset'));
        get_quotation_list_body();
    });

    $('div.quotation_paggination').on('change', 'select.limit_change', function () {

        $('input#limit').val($('select#set_limit').val());
        $('input#offset').val(0);
        get_quotation_list_body();
    });

    $('li.tab_name_click').click(function () {
        
        var tab_name = $(this).attr('tab-name');
        if (!$('li.' + tab_name).hasClass('active_list')) {

            $('li.active_list').removeClass('active_list');
            $(this).addClass('active_list');
            set_limit_offset(10, 0);
            get_quotation_list_body(tab_name);
        }
    });

    $('form#quotation_view_details').on('change', 'select.quotation_status', function () {

        $('select.close_reason').hide();
        var val = $(this).val();
        if (val == 'Closed') {
            $('select.close_reason').show();
        }
    });

    $('li.nav-item').click(function () {

        var sub_tab = $(this).attr('view-sub-tab');
        if (!$('li.' + sub_tab).hasClass('active_list')) {
            $('li.active_list').removeClass('active_list');
            $(this).addClass('active_list');
            ajax_call_function({
                call_type: 'view_quotation_details_tab',
                sub_tab: $('li.nav-item.active_list').attr('view-sub-tab'),
                quotation_id: $(this).attr('quotation_id'),

            }, 'view_quotation_details_tab');
        }
    });

    $('div#quotation_details').on('click', 'a.followup_modal', function () {

        ajax_call_function({
            call_type: 'get_view_quotation_details_followup_data',
            quotation_id: $(this).attr('quotation_id'),
            client_id: $(this).attr('client_id')
        }, 'get_view_quotation_details_followup_data');
    });

    $('tbody#quotation_list_body').on('click', 'a.delete_quotation', function () {

        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover Quotation",
            icon: "warning",
            buttons: ["Cancel", "Delete"],
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    ajax_call_function({
                        call_type: 'delete_quotation',
                        quotation_id: $(this).attr('quotation_id'),
                    }, 'delete_quotation');
                    swal({
                        title: "Quotation Deleted",
                        icon: "success",
                    });
                } else {
                    swal({
                        title: "Quotation Not Delete",
                        icon: "info",
                    });
                }
            });
    });

    $('tbody#quotation_list_body').on('click', 'a.add_followup_modal', function () {
        ajax_call_function({
            call_type: 'get_quotation_list_followup_data',
            quotation_id: $(this).attr('quotation_id'),
            client_id: $(this).attr('client_id')
        }, 'get_quotation_list_followup_data');
    });

    $('form#add_followup_data_form').on('click', 'button.save_followup', function () {

        ajax_call_function({
            call_type: 'save_followup_data',
            quotation_id: $(this).val(),
            add_followup_form: $('form#add_followup_data_form').serializeArray(),
        }, 'save_followup_data');
    });

    $('tbody#quotation_list_body').on('click', 'a.add_rating', function () {

        ajax_call_function({
            call_type: 'update_rating_model',
            quotation_id: $(this).attr('quotation_id'),
            quotation_priority: $(this).attr('quotation_priority'),
            priority_reason: $(this).attr('priority_reason'),
        }, 'update_rating_model');
    });

    $('form#save_rating_form').on('change', '#quotation_priority_value', function () {

        $('div.quotation_priority_reason').hide();
        var val = $(this).val();
        if (val > 0) {
            $('div.quotation_priority_reason').show();
        }
    });

    $('div#update_rating').on('click', 'button.save_rating', function () {

        var priority_reason = $('#quotation_priority_reason').val();
        var status = false;
        if (priority_reason == "") {

            $("#reason_error").html("<span class='text-denger'>Please Enter Priority reason</span>");
            status = false;
        } else {
            $("#reason_error").html("");
            status = true;
        }
        // alert(status); 
        if (status == true) {
            ajax_call_function({
                call_type: 'add_update_rating',
                quotation_mst_id: $(this).val(),
                save_rating_data: $('form#save_rating_form').serializeArray()
            }, 'add_update_rating');
        }
    });

    $("tbody#quotation_list_body").on('click', 'a.sales_query', function () {

        var quote_id = $(this).attr('quotation_id');
        var query_type = $(this).attr('query_type');

        // alert(quote_id);
        $.ajax({
            type: "POST",
            data: { "call_type": "get_sales_query_history", 'sales_query_quotation_id': quote_id },
            url: "<?php echo site_url('query/ajax_function'); ?>",
            dataType: 'JSON',
            success: function (res) {

                $('div#add_query').modal('show');
                $('input[name="query_reference"]').val(res.quote_no);
                $('input[name="query_reference_id"]').val(res.quotation_id);
                $('input[name="query_creator_id"]').val(res.sales_person_id);
                $('input[name="query_assigned_id"]').val(res.query_assigned_id);
                $('textarea[name="query_text"]').val('');
                $('div.query_type').html(res.query_type_option_tag);
                $('div.sales_query_history').html(res.query_history_html);
                $('.kt-selectpicker').selectpicker();
            }
        });
    });

    $("form#sales_query_form").on('change', 'select[name="sub_query_type"]', function () {
        var type = $(this).val();
        var assigned_id = $("input[name='query_assigned_id']").val();

        if (type == 'Sample MTC') {

            $("input[name='query_assigned_id']").val(302);
        } else if (type == 'Drawing') {

            $("input[name='query_assigned_id']").val(325);
        } else {

            $("input[name='query_assigned_id']").val(assigned_id);
        }
    });

    $('button.save_sales_query').click(function () {

        $.ajax({
            type: 'POST',
            data: {
                'call_type': 'save_sales_query',
                form_data: $('form#sales_query_form').serializeArray()
            },
            url: "<?php echo base_url('query/ajax_function'); ?>",
            dataType: 'JSON',
            success: function (res) {
                toastr.clear();
                if (res.status == 'successful') {
                    $('div#add_query').modal('hide');
                    toastr.success(res.message);
                } else {

                    toastr.error(res.message);
                }
            },
            beforeSend: function (response) {
                toastr.clear();
                toastr.warning('Saving of the query has begun.');
            }
        });
    });

    $('tbody#quotation_list_body').on('click', 'a.delete_purchase_order', function () {

        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover PO",
            icon: "warning",
            buttons: ["Cancel", "Delete"],
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    ajax_call_function({
                        call_type: 'delete_quotation_purchase_order',
                        quotation_id: $(this).attr('quotation_id'),
                    }, 'delete_quotation_purchase_order');
                    swal({
                        title: "Quotation PO Deleted",
                        icon: "success",
                    });
                } else {
                    swal({
                        title: "Quotation PO Not Delete",
                        icon: "info",
                    });
                }
            });
    });

    document.addEventListener('keydown', function (event) {

            if (event.shiftKey) {

                if (event.key === 'I') {

                    set_limit_offset(10, 0);
                    get_quotation_list_body(
                        "",
                        "instinox"
                    );
                } else if (event.key === 'Z') {

                    set_limit_offset(10, 0);
                    get_quotation_list_body(
                        "",
                        "zengineer"
                    );
                } else if (event.key === 'O') {

                    set_limit_offset(10, 0);
                    get_quotation_list_body(
                        "",
                        "omtubes"
                    );
                } else if (event.key === 'A') {

                    set_limit_offset(10, 0);
                    get_quotation_list_body(
                        "",
                        "all"
                    );
                }
        }

    });

    $('input[name="radio_company_type"]').click(function () {

        var type = $("input[type='radio'][name='radio_company_type']:checked").val();
        if (type == 'in') {

            var client_type = "instinox";
        } else if (type == 'zen') {

            var client_type = "zengineer";
        } else if (type == 'om') {

            var client_type = "omtubes";
        } else if (type == '') {

            var client_type = "all";
        }
        set_limit_offset(10, 0);
        get_quotation_list_body(
            "",
            client_type
        );
    });

    $('tbody#quotation_list_body').on('click', 'a.upload_purchase_order_modal', function () {

        ajax_call_function({
            call_type: 'open_pruchase_order_modal',
            quotation_id: $(this).attr('quotation_id'),
        }, 'open_pruchase_order_modal');
    });

    $('div.search_filter_data').on('input', 'input#client_name', function(){

        var search_text = $('input#client_name').val();
        if(search_text.length <= 3){

            // toastr.info('Please Enter more than 3 character to update company name!');
            return false;
        }
        $('select[name="client_id"]').html('');
        $('div#quotation_list_process').show();
        $('div.member_div').addClass('kt-hidden');
        ajax_call_function({
            call_type: 'get_client_name_quotation',
            client_name: search_text,
        }, 'get_client_name_quotation');
    });

    $('div.search_filter_data').on('change', 'select[name="client_id"]', function(){

        $('div.search_filter_data select[name="member"]').html('').selectpicker('refresh');
        $('div#quotation_list_process').show();
        ajax_call_function({
            call_type: 'get_member_name_quotation',
            client_id: $('select[name="client_id"]').val(),
        }, 'get_member_name_quotation');
        set_limit_offset(10, 0);
        get_quotation_list_body();
    });

    $('tbody#quotation_list_body').on('click', '.graph_details', function () {
 
        ajax_call_function({
            call_type: 'get_quotation_graph_details',
            company_name: $(this).attr('company_name'),
            company_id: $(this).attr('company_id'),
            module_type: 'quotation',
        }, 'get_quotation_graph_details', "<?php echo base_url('graph_management/ajax_function'); ?>");
    });
});


function ajax_call_function(data, call_type, url = "<?php echo base_url('quotation_management/ajax_function'); ?>") {
    $.ajax({
        type: 'POST',
        data: data,
        url: url,
        dataType: 'JSON',
        success: function (res) {
            toastr.clear();
            if (res.status == 'successful') {
                switch (call_type) {

                    case 'get_search_filter_div':
                        $('div.search_filter_data').html('').html(res.search_filter);
                    break;

                    case 'get_quotation_list_body':

                        $('thead#quotation_list_head').html('').html(res.quotation_list_head);
                        $('tbody#quotation_list_body').html('').html(res.quotation_list_body);
                        $('div.search_filter_data').html('').html(res.quotation_search_filter);
                        $('div.quotation_paggination').html('').html(res.quotation_paggination);
                        Quotation_Js.init();
                        $('div#quotation_list_process').hide();
                    break;

                    case 'view_quotation_details_tab':

                        $("div#quotation_details").html('').html(res.quotation_details);
                    break;

                    case 'get_quotation_list_followup_data':

                        $('form#add_followup_data_form').html('').html(res.follow_up_html);
                        $('button[id=quotation_id]').val(res.quotation_id);
                        Quotation_Js.init();
                    break;

                    case 'save_followup_data':
                        get_quotation_list_body();
                        $('div#add_followup').modal('hide');
                    break;

                    case 'get_view_quotation_details_followup_data':
                        $('form#add_followup_data_form').html('').html(res.follow_up_html);
                        $('button[id=quotation_id]').val(res.quotation_id);
                        Quotation_Js.init();
                    break;

                    case 'update_rating_model':

                        $('form#save_rating_form').html('').html(res.update_rating_html);
                        $('button[id=quotation_id]').val(res.quotation_id);
                        Quotation_Js.init();
                    break;

                    case 'add_update_rating':
                        swal({
                            title: "Priority updated",
                            icon: "success",
                        });
                        setTimeout(function () {

                            location.reload();
                        }, 1000);
                    break;

                    case 'open_pruchase_order_modal':
                        $('form#po_upload').html('').html(res.upload_po_model);
                    break;

                    case 'get_client_name_quotation':

                        $('div.search_filter_data select[name="client_id"]').html(res.client_list_html).selectpicker('refresh');
                        $('div#quotation_list_process').hide();
                        toastr.info('Company Name is Updated!');
                    break;

                    case 'get_member_name_quotation':

                        $('div.member_div').removeClass('kt-hidden');
                        $('div.search_filter_data select[name="member"]').html(res.client_member_list_html).selectpicker('refresh');
                        $('div#quotation_list_process').hide();
                    break;

                    case 'get_quotation_graph_details':
                        // console.log(res);
                        $('h2.graph_company_name').html('').html(res.company_name);
                        quotation_vs_proforma_monthly(res.quotation_vs_proforma_monthly);
                        quotation_vs_proforma_yearly(res.quotation_vs_proforma_yearly);
                        export_stats_yearly(res.export_stats_yearly);
                        export_stats_yearly_internal(res.export_stats_yearly_internal);
                        exporters_stats(res.exporters_data);
                        production_status_wise(res.production_status_wise);
                        $("div#stats-modal").modal('show');
                        $('div#quotation_list_process').hide();
                    break;
                }
            }
        },
        beforeSend: function (response) {

            switch (call_type) {

                case 'get_quotation_list_body':
                case 'get_quotation_graph_details':

                    $('div#quotation_list_process').show();
                break;
            }
        }
    });
};


var Quotation_Js = function () {

    var entered_on_quotation_search_filter_Init = function () {

        if ($('#entered_on_search_filter_date').length == 0) {
            return;
        }
        var picker = $('#entered_on_search_filter_date');
        var start = moment().subtract(29, 'day').startOf('day');
        var end = moment().endOf('day');
        var month = moment().startOf('month') - moment().endOf('month');

        function cb(start, end) {

            var range = start.format('MMMM D Y') + ' - ' + end.format('MMMM D Y');
            // alert(range);
            $('#entered_on_search_filter_date_date').html(range);
            $('input#entered_on').val(range)
        }

        picker.daterangepicker({

            startDate: start,
            endDate: end,
            opens: 'right',
            ranges: month
        }, cb);

        cb(start, end);
    }

    var followup_quotation_search_filter_Init = function () {

        if ($('#followup_search_filter_date').length == 0) {
            return;
        }
        var picker = $('#followup_search_filter_date');
        var start = moment().subtract(29, 'day').startOf('day');
        var end = moment().endOf('day');
        var month = moment().startOf('month') - moment().endOf('month');

        function cb(start, end) {

            var range = start.format('MMMM D Y') + ' - ' + end.format('MMMM D Y');
            // alert(range);
            $('#followup_search_filter_date_date').html(range);
            $('input#followup_date').val(range)
        }

        picker.daterangepicker({

            startDate: start,
            endDate: end,
            opens: 'right',
            ranges: month
        }, cb);

        cb(start, end);
    }

    var demos = function () {
        $('.quotation_priority').ionRangeSlider({
            min: 0,
            max: 5
        });
    }

    // Private functions
    var quotation_datepicker = function () {
        // enable clear button 
        $('.quotation_date_picker').datepicker({
            rtl: KTUtil.isRTL(),
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: true,
            format: 'yyyy-mm-dd',
        });
    };
    return {
        // public functions
        init: function () {
            entered_on_quotation_search_filter_Init();
            followup_quotation_search_filter_Init();
            quotation_datepicker();
            demos();
            $('.select_picker').selectpicker();
        }
    };
}();

function set_limit_offset(limit, offset){

    $('input#limit').val(limit);
    $('input#offset').val(offset);
}
function get_quotation_list_body(
                                tab_name = "",
                                client_type = "",
                                search_form_data = $('form#search_filter_form').serializeArray()
                            ){
    data = {
        call_type: 'get_quotation_list_body',
        search_form_data: search_form_data
    }
    var limit = $('input#limit').val();
    if(typeof limit === 'undefined'){

        limit = 10
    }
    data.limit = limit
    var offset = $('input#offset').val();
    if(typeof offset === 'undefined'){

        offset = 0
    }
    data.offset = offset
    if(tab_name != ''){

        data.tab_name = tab_name
    }
    if(client_type != ''){

        data.client_type = client_type
    }

    ajax_call_function(
        data,
        'get_quotation_list_body'
    );
}

//graph
function quotation_vs_proforma_monthly(highchart_data) {

    if (highchart_data.length === 0) {

        $('div.quotation_vs_proforma_body').css('display', 'hide');
        $('h3.quotation_vs_proforma_title').html('').html('Quotation Vs Proforma Won <span class="kt-font-danger">No Data Found</span>');
        return false;
    }
    $('div.quotation_vs_proforma_body').css('display', 'block');
    $('h3.quotation_vs_proforma_title').html('').html('Quotation Vs Proforma Won Monthly');
    Highcharts.chart('quotation_vs_proforma_highchart', {
        // Create the chart
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: highchart_data.category
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total Quotation'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: highchart_data.series
    });
}
function quotation_vs_proforma_yearly(highchart_data) {

    if (highchart_data.length === 0) {

        $('div.quotation_vs_proforma_yearly_body').css('display', 'hide');
        $('h3.quotation_vs_proforma_yearly_title').html('').html('Quotation Vs Proforma Won <span class="kt-font-danger">No Data Found</span>');
        return false;
    }
    $('div.quotation_vs_proforma_yearly_body').css('display', 'block');
    $('h3.quotation_vs_proforma_yearly_title').html('').html('Quotation Vs Proforma Won Yearly');

    Highcharts.chart('quotation_vs_proforma_highchart_yearly', {
        // Create the chart
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: highchart_data.category
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total Quotation'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: highchart_data.series
    });
}
function export_stats_yearly(highchart_data) {

    if (highchart_data.length === 0) {

        $('div.export_stats_yearly_body').css('display', 'none');
        $('h3.export_stats_yearly_title').html('').html('Yearly Buying Trends Total <span class="kt-font-danger">No Data Found</span>');
        return false;
    }
    $('div.export_stats_yearly_body').css('display', 'block');
    $('h3.export_stats_yearly_title').html('').html('Yearly Buying Trends Total: Rupees ' + highchart_data.total);
    Highcharts.chart('export_stats_yearly_container', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: highchart_data.category,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Exporter Wise import values'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>${point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            series: {
                pointPadding: 0,
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '${point.y}'
                }
            }
        },
        series: highchart_data.series
    });
}
function export_stats_yearly_internal(highchart_data) {

    if (highchart_data.length === 0) {

        $('div.export_stats_yearly_internal_body').css('display', 'none');
        $('h3.export_stats_yearly_internal_title').html('').html('Yearly Buying Trends <span class="kt-font-danger">No Data Found</span>');
        return false;
    }
    $('div.export_stats_yearly_internal_body').css('display', 'block');
    $('h3.export_stats_yearly_internal_title').html('').html('Internal Data : $' + highchart_data.total);
    Highcharts.chart('export_stats_yearly_internal_container', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },

        accessibility: {
            announceNewData: {
                enabled: true
            }
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Year wise data'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '${point.y}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>${point.y}</b><br/>'
        },

        series: [
            {
                name: 'Total',
                colorByPoint: true,
                data: highchart_data.series
            }
        ]
    });
}
function exporters_stats(highchart_data) {

    if (highchart_data.length === 0) {

        $('div.exporter_stats_div').css('display', 'none');
        return false;
    }
    $('div.exporter_stats_div').css('display', 'block');
    $('h3.exporter_stats_title').html('').html('Top Exporter');
    Highcharts.chart('exporter_stats_container', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y}',
                    connectorColor: 'silver'
                }
            }
        },
        series: [{
            name: 'Share',
            data: highchart_data
        }],
        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        },
    });
}
function production_status_wise(highchart_data) {

    if (highchart_data.length === 0) {

        $('div.production_status_wise_div').css('display', 'none');
        return false;
    }
    $('div.production_status_wise_div').css('display', 'block');
    $('h3.production_status_wise_title').html('').html('Total: $' + highchart_data.total);
    Highcharts.chart('production_status_wise', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },

        accessibility: {
            announceNewData: {
                enabled: true
            }
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Year wise data'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '${point.y}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>${point.y}</b><br/>'
        },

        series: [
            {
                name: 'Total',
                colorByPoint: true,
                data: highchart_data.series
            }
        ]
    });
}