<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quotation_management extends MX_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('user_id')){
			redirect('login', 'refresh');
			exit;
		}
		else{
			$access_count = 0;
			if(!empty($this->session->userdata('main_module_access')) && in_array(29, array_column($this->session->userdata('main_module_access'), 'module_id'))) {

				foreach ($this->session->userdata('sub_module_access') as $key => $value) {
					
					if($value['module_id'] == 29 && $value['status'] == 'Active') {
						$access_count++;
					}
				}
			}
			if($access_count == 0){
				redirect($this->session->userdata('home/dashboard'));
				exit;
			}
		}
		error_reporting(0);
		$this->load->model('quotation_management_model');
		$this->load->model('common/common_model');
	}

	public function index(){

		$data = array();
		// Loading Default Session Value
			$this->session->set_userdata(array('quotation_client_type'=> 'all'));

			$tab_width = 0;
			//list tab access
			if(in_array('proforma', $this->session->userdata('quotation_access')['quotation_list_tab_access'])) {

				$this->session->set_userdata(array('quotation_tab_name'=> 'proforma_list'));
				$tab_width += 1;
			}
			if(in_array('follow_up', $this->session->userdata('quotation_access')['quotation_list_tab_access'])){

				$this->session->set_userdata(array('quotation_tab_name'=> 'followup_list'));
				$tab_width += 1;
			}
			if(in_array('draft', $this->session->userdata('quotation_access')['quotation_list_tab_access'])){

				$this->session->set_userdata(array('quotation_tab_name'=> 'draft_list'));
				$tab_width += 1;
			}
			if(in_array('list', $this->session->userdata('quotation_access')['quotation_list_tab_access'])){

				$this->session->set_userdata(array('quotation_tab_name'=> 'quotation_list'));
				$tab_width += 1;
			}

			$data['menu_tab_width'] = 'calc(100%/'.$tab_width.') !important';
		// Ended
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Quotation List'));
		$this->load->view('sidebar', array('title' => 'Quotation List'));
		$this->load->view('quotation_management/list', $data);
		$this->load->view('footer');
	}

	public function pdf($quote_id){

		redirect('pdf_management/quotation_pdf/'.$quote_id, 'refresh');
		if(!empty($quote_id)){

			$data = $this->get_pdf_information($quote_id);
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";die;

		$this->load->library('Pdf');
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// set document information

		// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, NULL, '', array(0,0,0), array(255,255,255));
		$pdf->SetFooterData(array(0,0,0), array(255,255,255));

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		// set margins
		$pdf->SetMargins(10, PDF_MARGIN_TOP-10, 10);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER-5);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM-15);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------
		
		// set font
		$pdf->SetFont('helvetica', '', 10);

		// add a page
		$pdf->AddPage();

		if(in_array($this->session->userdata('quotation_tab_name'), array('quotation_list', 'followup_list', 'draft_list'))){

			$html= $this->load->view('quotation_management/quotation_pdf', $data, true);
			// output the HTML content
			$pdf->writeHTML($html, true, true, true, false, '<ur><li>L : left align</li></ul>');
			//Close and output PDF document
			$pdf->Output(str_replace('/', '-', $data['quotation_data']['quote_no']).'.pdf', 'I');
		}
		if(in_array($this->session->userdata('quotation_tab_name'), array('proforma_list'))){

			$html= $this->load->view('quotation_management/quotation_pdf', $data, true);
			// output the HTML content
			$pdf->writeHTML($html, true, true, true, false, '<ur><li>L : left align</li></ul>');
			//Close and output PDF document
			$pdf->Output(str_replace('/', '-', $data['quotation_data']['proforma_no']).'.pdf', 'I');
		}
	}

	public function viewQuotation($quote_id){
		
		if(!empty($quote_id)){

			$data['quotation_data'] = $this->common_model->get_all_conditional_data_sales_db('*', array('quotation_mst_id'=> $quote_id), 'quotation_mst','row_array');

			if(!empty($data['quotation_data'])) {
		
				$data['customer_details'] = $this->common_model->get_all_conditional_data_sales_db('*', array('id'=> $data['quotation_data']['client_id']),'customer_mst','row_array');
					
				$data['currency_details'] = $this->common_model->get_all_conditional_data_sales_db('*', array('currency_id'=> $data['quotation_data']['currency']),'currency','row_array');		

				$data['reasons'] = $this->common_model->get_all_conditional_data_sales_db('*', array('status'=>'Active'), 'close_reason');
			}
		}
		//  echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Quotation List'));
		$this->load->view('sidebar', array('title' => 'Quotation List'));
		$this->load->view('quotation_management/view_quotation_details_form',$data);
		$this->load->view('footer');
	}

   	public function ajax_function() {

		if($this->input->is_ajax_request()){
			$post_details = $this->input->post();
			$call_type = $this->input->post('call_type');
			$response['status'] = 'successful';	
			switch ($call_type) {

				case 'get_quotation_list_body':

					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					if(!empty($this->input->post('tab_name'))){

						$this->session->set_userdata(array('quotation_tab_name'=> $this->input->post('tab_name')));
					}
					if(!empty($this->input->post('client_type'))){

						$this->session->set_userdata(array('quotation_client_type'=> $this->input->post('client_type')));
					}
					$data = $this->prepare_all_data($this->input->post('search_form_data'), $this->input->post('limit'), $this->input->post('offset'));
					// echo "<pre>";print_r($client_details);echo"</pre><hr>";exit;

					$response['quotation_list_head'] = $this->load->view('quotation_management/list_head', $data,true);
					$response['quotation_list_body'] = $this->load->view('quotation_management/list_body', $data, true);
					$response['quotation_search_filter'] = $this->load->view('quotation_management/search_filter_form', $data, true);
					$response['quotation_paggination'] = $this->load->view('quotation_management/paggination', $data, true);
				break;

				case 'view_quotation_details_tab':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					if(!empty($this->input->post('sub_tab'))){
						
						$data = $this->prepare_quotation_details($this->input->post());
						
						// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
						$response['quotation_details'] = $this->load->view('quotation_management/view_quotation_details_form_body', $data, true);
					}
				break;

				case 'delete_quotation':
					if(!empty($this->input->post('quotation_id'))){

						$this->quotation_management_model->deleteData('quotation_mst', array('quotation_mst_id'=>$this->input->post('quotation_id')));
					}
				break;

				case 'get_quotation_list_followup_data':
				case 'get_view_quotation_details_followup_data':

					// echo "<pre>";print_r($this->input->post('quotation_id'));echo"</pre><hr>";exit;
 					$response['follow_up_html'] = "";
					$response['quotation_id'] = $this->input->post('quotation_id');

					if(!empty($this->input->post('client_id'))){

						$data['members_details'] = $this->common_model->get_dynamic_data_sales_db('comp_dtl_id,member_name',array('status'=>'Active', 'comp_mst_id'=>$this->input->post('client_id')),'customer_dtl');

 						$data['follow_up_date'] = date('Y-m-d');
 						$data['next_follow_up_date'] = date('Y-m-d', strtotime(" +7 days"));
						
						//getting follow up history
						$data['follow_up_details'] = $this->common_model->get_all_conditional_data_sales_db('*',array('quotation_mst_id'=>$this->input->post('quotation_id')),'follow_up','result_array', array(), array('column_name'=>'entered_on', 'column_value'=>'desc'), array(), 0,0);

 						$response['follow_up_html'] = $this->load->view('quotation_management/follow_up_html', $data, true);						
					}
				break;

                case 'save_followup_data':
					// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;
					if(!empty($this->input->post('quotation_id'))){
						$form_data = array_column($this->input->post('add_followup_form'), 'value', 'name');
						$quotation_id = $this->input->post('quotation_id');
						
						$insert_array = array(
												'quotation_mst_id' => $quotation_id,
												'member_id' => $form_data['member_id'],
												'connect_mode' => $form_data['connect_mode'],
												'follow_up_text' => $form_data['follow_up_text'],
												'followedup_on' => date('Y-m-d', strtotime($form_data['followedup_on'])),
												'entered_on' => date('Y-m-d H:i:s'),
												'entered_by' => $this->session->userdata('user_id'),
												'email' =>$form_data['email'],
						);
						// echo "<pre>";print_r($insert_array);echo"</pre><hr>";exit;
						if(!empty($insert_array)){

							$this->insert_into_daily_work_sales_on_quotations_data($insert_array);
							$this->common_model->insert_data_sales_db('follow_up', $insert_array);
							$this->common_model->update_data_sales_db('quotation_mst', array('followup_date' => date('Y-m-d', strtotime($form_data['followup_date']))), array('quotation_mst_id' => $quotation_id));
						}
					}
				break;

				case 'update_rating_model':
				
					$response['update_rating_html'] = "";
					$response['quotation_id'] = $this->input->post('quotation_id');
						
					if(!empty($this->input->post('quotation_id'))){
							
						$data['quotation_id'] = $this->input->post('quotation_id');
						$data['quotation_priority'] = $this->input->post('quotation_priority');
						$data['priority_reason'] = $this->input->post('priority_reason');
							
						// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
						$response['update_rating_html'] = $this->load->view('quotation_management/update_rating_html', $data, true);
					}				
				break;

				case 'add_update_rating':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					if(!empty($this->input->post('quotation_mst_id'))){
						
						$form_data = array_column($this->input->post('save_rating_data'), 'value', 'name');

						$form_data['modified_on'] = date('Y-m-d H:i:s');
						$form_data['modified_by'] = $this->session->userdata('user_id');

						// echo "<pre>";print_r($form_data);echo"</pre><hr>";exit;
						$quotation_details = $this->common_model->get_dynamic_data_sales_db('quotation_mst_id, rfq_id', array('quotation_mst_id' => $this->input->post('quotation_mst_id')), 'quotation_mst', 'row_array');

						if(!empty($quotation_details['rfq_id'])){

							$this->common_model->update_data_sales_db('rfq_mst', array('priority'=>$form_data['quotation_priority'], 'priority_reason'=>$form_data['priority_reason']), array('rfq_mst_id' => $quotation_details['rfq_id']));
						}
						$this->common_model->update_data_sales_db('quotation_mst', $form_data, array('quotation_mst_id' => $this->input->post('quotation_mst_id')));						
					}
				break;

				case 'delete_quotation_purchase_order':
					
					if(!empty($this->input->post('quotation_id'))){

						$this->common_model->update_data_sales_db('quotation_mst', array('purchase_order'=>''), array('quotation_mst_id'=>$this->input->post('quotation_id')));
					}
				break;

				case 'open_pruchase_order_modal':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					if(!empty($this->input->post('quotation_id'))){

						$data['quotation_id'] = $this->input->post('quotation_id');
						$response['id'] = $this->input->post('quotation_id');
						$response['upload_po_model'] = $this->load->view('quotation_management/upload_po_model', $data, true);
					}
				break;

				case 'upload_purchase_order':

						// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
						$response['msg'] = 'File is uploaded successfully!!!';
						$return_response = $this->upload_doc_config($this->input->post('id'));
						$response['status'] = $return_response['status'];
						if($return_response['status'] == 'successful') {

						$quotation_id  = $this->input->post('id');
						$quotation_data = $this->common_model->get_dynamic_data_sales_db('*',array('quotation_mst_id'=>$quotation_id), 'quotation_mst', 'row_array');

						if(empty($quotation_data['purchase_order'])) {

							$insert_array = array(
								'purchase_order'=>$return_response['file_name'],
								'po_add_time'=>date('Y-m-d'),
							);
							if(!empty($insert_array)){

								$this->common_model->update_data_sales_db('quotation_mst', $insert_array, array('quotation_mst_id'=>$quotation_id));
							}
						} else {
							$update_array = array(
												'purchase_order'=>$quotation_data['purchase_order'].','.$return_response['file_name'],
												'po_add_time'=>date('Y-m-d'),
							);
							if(!empty($update_array)){

								$this->common_model->update_data_sales_db('quotation_mst', $update_array, array('quotation_mst_id'=>$quotation_id));
							}
						}
					}else {
						$response['msg'] = 'File is not uploaded.';
					}
				break;

				case 'get_client_name_quotation':
					
					$search_client_name = $post_details['client_name'];
					$where_string = "customer_mst.status = 'Active' AND customer_mst.name LIKE '%".$search_client_name."%'";
					if($this->session->userdata('role') == 5){

						$where_string .= " AND quotation_mst.assigned_to IN ('".implode("', '", $this->session->userdata('quotation_access')['quotation_sales_user_id'])."')";
					}
					$client_details = $this->quotation_management_model->get_quotation_client_list_data($where_string);
					$response['client_list_html'] = '<option value="">Select Name</option>';
					foreach($client_details as $single_details){
                        
                        $response['client_list_html'] .= '<option value="'.$single_details['id'].'">'.$single_details['name'].'</option>';
                    }
					// echo "<pre>";print_r($client_details);echo"</pre><hr>";exit;
				break;

				case 'get_member_name_quotation':
					
					$client_id = $post_details['client_id'];
					$client_details = $this->common_model->get_dynamic_data_sales_db_null_false('comp_dtl_id, member_name', "status = 'Active' AND comp_mst_id = ".$client_id."", 'customer_dtl');
					// echo "<pre>";print_r($client_details);echo"</pre><hr>";exit;
					$response['client_member_list_html'] = '<option value="">Select Name</option>';
					foreach($client_details as $single_details){
                        
                        $response['client_member_list_html'] .= '<option value="'.$single_details['comp_dtl_id'].'">'.$single_details['member_name'].'</option>';
                    }
					
				break;

                default:
					$response['status'] = 'failed';	
					$response['message'] = "call type not found";
				break;
			}
			echo json_encode($response);

		} else{
			die('access is not allowed to this function');
		}
	}

    private function prepare_all_data($search_form_data, $limit, $offset){

		$data = array();
		$prepare_table_body_data_params = $this->prepare_where_and_orderby_for_table($search_form_data, $limit, $offset);

		$data = $this->prepare_table_body_data(
								$prepare_table_body_data_params['join'],
								$prepare_table_body_data_params['where'],
								$prepare_table_body_data_params['order_by'],
								$prepare_table_body_data_params['limit'],
								$prepare_table_body_data_params['offset']
							);

		$data['search_filter_form_data'] = $this->prepare_search_filter_data($search_form_data);

		$data['tab_name'] = $this->session->userdata('quotation_tab_name');

		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		return $data;
	}

	private function prepare_search_filter_data($search_filter_form_data){

		// echo "<pre>";print_r($search_filter_form_data);echo"</pre><hr>";exit;
		$data = array('entered_on'=>'', 'followup_date'=>'', 'importance'=>'', 'quote_no'=>'', 'proforma_no'=>'','confirmed_on'=>'','value'=>'', 'search_quotation_client_name'=>'', 'client_id'=> array(), 'member'=> array(), 'quotation_priority'=>'');
		$data['country_name'] = $this->change_key_as_per_value(
									$this->common_model->get_all_conditional_data_sales_db(
										'name as value, "" as selected',
										array('status'=>'Active'),
										'country_mst'
									)
		);
		$data['assigned_to'] =	$this->change_key_as_per_value(
									$this->common_model->get_dynamic_data_sales_db_null_false(
										'name, user_id as value, "" as selected',
										"status=1 AND role IN (5, 16)",
										'users'
									)
		);
		$data['procurement_person'] =	$this->change_key_as_per_value(
									$this->common_model->get_dynamic_data_sales_db_null_false(
										'name, user_id as value, "" as selected',
										"status=1 AND role IN (6, 8)",
										'users'
									)
		);
		$data['years']  =$this->change_key_as_per_value($this->quotation_management_model->get_Financial_Years_date());
		$data['value'] =array(
							'0-999' => array(
								'name' => '0-999',
								'value' => '0-999',
								'selected' => ''
							),
							'1000-4999' => array(
								'name' => '1000-4999',
								'value' => '1000-4999',
								'selected' => ''
							),
							'5000-9999' => array(
								'name' => '5000-9999',
								'value' => '5000-9999',
								'selected' => ''
							),
							'10000-99999' => array(
								'name' => '10000-99999',
								'value' => '10000-99999',
								'selected' => ''
							),
							'100000-100000' => array(
								'name' => '100000 +',
								'value' => '100000-100000',
								'selected' => ''
							)
		);
		$data['importance'] = $this->change_key_as_per_value(
									$this->common_model->get_all_conditional_data_sales_db(
										'importance as value, "" as selected',
										array('importance !='=>''),
										'quotation_mst'
									)
		);
		$data['quotation_new'] = array(
										'Y' => array(
											'name' => 'New',
											'value' => 'Y',
											'selected' => ''
										),
		);
		$data['status'] =array(
							'Open' => array(
								'name' => 'Open',
								'value' => 'Open',
								'selected' => ''
							),
							'Closed' => array(
								'name' => 'Closed',
								'value' => 'Closed',
								'selected' => ''
							),
							'Won' => array(
								'name' => 'Won',
								'value' => 'Won',
								'selected' => ''
							)
		);
		$data['product_family'] = array(
				'piping' => array(
					'name' => 'Piping',
					'value' => 'piping',
					'class' => "<span class='kt-badge kt-badge--unified-info kt-badge--sm kt-badge--inline kt-badge--rounded kt-badge--bold'>Piping</span>",
					'selected' => ''
				),
				'instrumentation' => array(
					'name' => 'Instrumentation',
					'value' => 'instrumentation',
					'class' => "<span class='kt-badge kt-badge--unified-warning kt-badge--sm kt-badge--inline kt-badge--rounded kt-badge--bold'>Instrumentation</span>",
					'selected' => ''
				),
				'precision' => array(
					'name' => 'Precision',
					'value' => 'precision',
					'class' => "<span class='kt-badge kt-badge--unified-orange kt-badge--sm kt-badge--inline kt-badge--rounded kt-badge--bold'>Precision</span>",
					'selected' => ''
				),
				'tubing' => array(
					'name' => 'Tubing',
					'value' => 'tubing',
					'class' => "<span class='kt-badge kt-badge--unified-dark kt-badge--sm kt-badge--inline kt-badge--rounded kt-badge--bold'>Tubing</span>",
					'selected' => ''
				),
				'industrial' => array(
					'name' => 'Industrial',
					'value' => 'industrial',
					'class' => "<span class='kt-badge kt-badge--unified-success kt-badge--sm kt-badge--inline kt-badge--rounded kt-badge--bold'>Industrial</span>",
					'selected' =>''
				),
				'fastener' => array(
					'name'=> 'Fastener',
					'value'=> 'fastener',
					'class'=> "<span class='kt-badge kt-badge--unified-danger kt-badge--sm kt-badge--inline kt-badge--rounded kt-badge--bold'>Fastener</span>",
					'selected'=> ''
				),
				'valve' => array(
					'name'=> 'Valve',
					'value'=> 'valve',
					'class'=> "<span class='kt-badge kt-badge--unified-dark kt-badge--sm kt-badge--inline kt-badge--rounded kt-badge--bold'>Valve</span>",
					'selected'=> ''
				)
		);
		$data['quotation_priority'] = $this->change_key_as_per_value(
									$this->common_model->get_all_conditional_data_sales_db(
										'quotation_priority as value, "" as selected',
										array('quotation_priority !='=>0),
										'quotation_mst'
									)
		);
		// echo "<pre>";print_r($search_filter_form_data);echo"</pre><hr>";exit;
		if(!empty($search_filter_form_data)){

			foreach ($search_filter_form_data as $form_data) {

				if(!empty($form_data['value'])){

					switch ($form_data['name']) {

						case 'search_quotation_client_name':
							
							$where_string = "status = 'Active' AND name LIKE '%".$form_data['value']."%'";
							if($this->session->userdata('role') == 5){

								$where_string .= " AND assigned_to IN ('".implode("', '", $this->session->userdata('quotation_access')['quotation_sales_user_id'])."')";
							}
							$data['client_id'] = $this->change_key_as_per_value(
								$this->common_model->get_dynamic_data_sales_db_null_false(
									'id as value, name, "" as selected',
									$where_string,
									'customer_mst'
								)
							);
							$data['search_quotation_client_name'] = $form_data['value'];

						break;
						case 'client_id':
							
							$data[$form_data['name']][$form_data['value']]['selected'] = 'selected';
							$data['member'] = $this->change_key_as_per_value(
								$this->common_model->get_dynamic_data_sales_db_null_false(
									'comp_dtl_id as value, member_name, "" as selected',
									"status = 'Active' AND comp_mst_id = ".$form_data['value'],
									'customer_dtl'
								)
							);
							// echo "<pre>";print_r($data['member']);echo"</pre><hr>";exit;
						break;
						
						case 'entered_on':
						case 'confirmed_on':
						case 'followup_date':
						case 'quote_no':
						case 'proforma_no':
						case 'quotation_priority':
							$data[$form_data['name']] = $form_data['value'];

						break;

						case 'member':
						case 'country_name':
						case 'assigned_to':
						case 'procurement_person':
						case 'years':
						case 'value':
						case 'importance':
						case 'quotation_new':
						case 'status':
						case 'product_family':
							$data[$form_data['name']][$form_data['value']]['selected'] = 'selected';

						break;
						default:
						break;
					}
				}
			}
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		return $data;
	}

	private function change_key_as_per_value($data, $changing_key_column_name = 'value'){

		$return_data = array();
		foreach ($data as $single_data) {

			$return_data[$single_data[$changing_key_column_name]] = $single_data;
		}

		return $return_data;
	}

	private function prepare_table_body_data($join_string, $where_array, $order_by, $limit, $offset){

		$data = $this->quotation_management_model->get_listing_data($join_string, $where_array, $order_by, $limit, $offset);
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;

		$user_list = array_column($this->common_model->get_dynamic_data_sales_db('user_id, name', array(), 'users'), 'name', 'user_id');
		$user_list['']='';
		$user_list[0]='';

		$currency_list = array_column($this->common_model->get_dynamic_data_sales_db('currency_id, decimal_number', array('status' => 'Active'), 'currency'), 'decimal_number', 'currency_id');
		$currency_list['']='';
		$currency_list[0]='';

		$close_reason_list = array_column($this->common_model->get_dynamic_data_sales_db('reason_id, reason_text', array('status' => 'Active'), 'close_reason'), 'reason_text', 'reason_id');
		$close_reason_list['']='';
		$close_reason_list[0]='';
		$currency_details =  array_column($this->db->select('currency_id, currency_rate')->where(array('status'=>'Active'))->get('currency')->result_array(), 'currency_rate', 'currency_id');
		$currency_value = $currency_details[1];
		// $member_list = array_column($this->common_model->get_dynamic_data_sales_db('comp_dtl_id, member_name', array(), 'customer_dtl'), 'member_name', 'comp_dtl_id');
		// $member_list['']='';
		// $member_list[0]='';
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		foreach ($data['quotation_list'] as $quotation_list_key => $single_list_data_value) {

			$data['quotation_list'][$quotation_list_key]['made_by_name'] = $user_list[$single_list_data_value['made_by']];

			$data['quotation_list'][$quotation_list_key]['assigned_to'] = $user_list[$single_list_data_value['assigned_to']];

			$data['quotation_list'][$quotation_list_key]['procurement_person_name'] = $user_list[$single_list_data_value['procurement_person']];

			$data['quotation_list'][$quotation_list_key]['member_name'] = '';
			$member_details = $this->common_model->get_dynamic_data_sales_db('member_name', array('comp_dtl_id'=> $single_list_data_value['member_id']), 'customer_dtl', 'row_array');
			if(!empty($member_details)){

				$data['quotation_list'][$quotation_list_key]['member_name'] = $member_details['member_name'];
			}

			$data['quotation_list'][$quotation_list_key]['value'] = $currency_list[$single_list_data_value['currency']].number_format($single_list_data_value['grand_total'],2,'.',',');

			$data['quotation_list'][$quotation_list_key]['close_reason_text'] = $close_reason_list[$single_list_data_value['close_reason']];

			$last_followup_details = $this->common_model->get_all_conditional_data_sales_db('follow_up_text, followedup_on, entered_on', array('quotation_mst_id'=>$single_list_data_value['quotation_mst_id'], ),'follow_up','row_array', array(),array('column_name'=>'entered_on', 'column_value'=>'desc'),array(),0,0);
			// echo "<pre>";print_r($last_followup_details);echo"</pre><hr>";
			$data['quotation_list'][$quotation_list_key]['followedup_on'] = '';
			$data['quotation_list'][$quotation_list_key]['follow_up_text'] = '';
			if(!empty($last_followup_details)){
				
				$explode_date = explode(" ", $last_followup_details['entered_on']);
				// echo "<pre>";print_r($explode_date);echo"</pre><hr>";
				$random_date = $last_followup_details['followedup_on'].' '.$explode_date[1];
				// echo $random_date;die('debug');
				$data['quotation_list'][$quotation_list_key]['followedup_on'] = $this->creat_tat($random_date);
				$data['quotation_list'][$quotation_list_key]['follow_up_text'] = $last_followup_details['follow_up_text'];
			}			
			
			if(round(((int)$single_list_data_value['grand_total']) * ($currency_details[$single_list_data_value['currency']] / $currency_value)) >= 25000){
				$data['quotation_list'][$quotation_list_key]['backgound'] = 'quotation_total_above_25k_color';
			}

			//quotation rating
			$data['quotation_list'][$quotation_list_key]['priority_div'] = '<div class="kt-font-warning kt-demo-icon__preview" data-toggle="kt-tooltip" data-placement="left" title="" data-original-title="Tooltip title">';
			if(!empty($single_list_data_value['quotation_priority'])) {

				$data['quotation_list'][$quotation_list_key]['priority_div'] = '<div class="kt-font-warning kt-demo-icon__preview" data-toggle="kt-tooltip" data-placement="left" title="'.$single_list_data_value["priority_reason"].'" data-original-title="Tooltip title">';
				for ($i=0; $i < 5; $i++) {

					if($i < $single_list_data_value['quotation_priority']) {
						$data['quotation_list'][$quotation_list_key]['priority_div'] .= '<i class="la la-star" style="line-height: 0;vertical-align: middle;font-size: 1.5rem !important;"></i>';
					}
				}
			}
			$data['quotation_list'][$quotation_list_key]['priority_div'] .= '</div>';

			$total_selling = $total_cost = $total_net_total = 0;
			$quotation_line_item_details = $this->common_model->get_dynamic_data_sales_db('*', array('quotation_mst_id'=> $single_list_data_value['quotation_mst_id']), 'quotation_dtl');

			foreach($quotation_line_item_details as $line_item_key => $single_line_item){

				if(empty($single_line_item['margin']) || $single_line_item['margin'] == 0){
					$data['quotation_list'][$quotation_list_key]['selling_margin_color'] = "#ffb822 !important";
					continue;
				}

				$total_cost += $single_line_item['quantity'] * $single_line_item['unit_rate'];
				$total_net_total += $single_line_item['row_price'];
			}
			$total_selling = ($total_net_total * $single_list_data_value['currency_rate']);

			$avg_margin = round(((($total_selling - $total_cost) / $total_cost) * 100),1);
			if(!empty($avg_margin) && !is_nan($avg_margin) && !is_infinite($avg_margin)){

				$data['quotation_list'][$quotation_list_key]['avg_margin'] = $avg_margin;
			}
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$data['paggination_data']['limit'] = $limit;
		$data['paggination_data']['offset'] = $offset;
		$data['quotation_family_badge'] = array(
			'piping' => 'kt-badge kt-badge--unified-info kt-badge--sm kt-badge--rounded kt-badge--bold',
			'instrumentation' => 'kt-badge kt-badge--unified-warning kt-badge--sm kt-badge--rounded kt-badge--bold',
			'precision' => 'kt-badge kt-badge--unified-orange kt-badge--sm kt-badge--rounded kt-badge--bold',
			'tubing' => 'kt-badge kt-badge--unified-dark kt-badge--sm kt-badge--rounded kt-badge--bold',
			'industrial' => 'kt-badge kt-badge--unified-success kt-badge--sm kt-badge--rounded kt-badge--bold',
			'fastener' => 'kt-badge kt-badge--unified-danger kt-badge--sm kt-badge--rounded kt-badge--bold',
			'valve' => 'kt-badge kt-badge--unified-dark kt-badge--sm kt-badge--rounded kt-badge--bold',
			'' => ''
		);
		return $data;
	}

	private function prepare_where_and_orderby_for_table($search_filter_form_data, $limit, $offset){

		// echo "<pre>";print_r($search_filter_form_data);echo"</pre><hr>";exit;
		$return_array = array();
		$return_array['join'] = 'LEFT JOIN
									customer_mst
										ON customer_mst.id = quotation_mst.client_id
								LEFT JOIN
									country_mst
										ON country_mst.id = customer_mst.country_id
								LEFT JOIN
									region_mst
										ON region_mst.id = customer_mst.region_id
								LEFT JOIN
									rfq_mst
										ON rfq_mst.rfq_mst_id = quotation_mst.rfq_id
								LEFT JOIN
									users
										ON users.user_id = quotation_mst.assigned_to';
		$return_array['where'] = '';
		$return_array['order_by'] = '';
		$return_array['limit'] = $limit;
		$return_array['offset'] = $offset;

		if(in_array($this->session->userdata('quotation_tab_name'), array('quotation_list'))){

			$return_array['where'] = "quotation_mst.stage IN ('publish', 'proforma') ";
			$return_array['order_by'] = 'entered_on desc';

		}else if(in_array($this->session->userdata('quotation_tab_name'), array('draft_list'))){

			$return_array['where'] = "quotation_mst.stage IN ('draft') ";
			$return_array['order_by'] = 'entered_on desc';

		}else if(in_array($this->session->userdata('quotation_tab_name'), array('followup_list'))){

			$return_array['where'] = "quotation_mst.quote_no is not NULL AND quotation_mst.quote_no != '' AND quotation_mst.status= 'open' AND quotation_mst.followup_date <= '".date('Y-m-d')."'";
			$return_array['order_by'] = "entered_on desc";

		}else if(in_array($this->session->userdata('quotation_tab_name'), array('proforma_list'))){

			$return_array['where'] = "quotation_mst.client_id is not NULL AND quotation_mst.quote_no is not NULL AND quotation_mst.status = 'won' AND quotation_mst.stage IN ('proforma') ";
			$return_array['order_by'] = "confirmed_on desc";
		}

		if(!empty($this->session->userdata('quotation_client_type')) && $this->session->userdata('quotation_client_type') != 'all'){

			$return_array['where'] .= " AND quotation_mst.client_type IN ('".$this->session->userdata('quotation_client_type')."')";
		}
		if($this->session->userdata('role') == 5){

			$return_array['where'] .= " AND quotation_mst.assigned_to IN ('".implode("', '", $this->session->userdata('quotation_access')['quotation_sales_user_id'])."')";
		}elseif($this->session->userdata('role') == 8){

			$return_array['where'] .= " AND ( rfq_mst.assigned_to IN (".implode(', ', $this->session->userdata('quotation_access')['quotation_procurement_user_id']).") || rfq_mst.assigned_to_1 IN (".implode(', ', $this->session->userdata('quotation_access')['quotation_procurement_user_id']).") || rfq_mst.assigned_to_2 IN (".implode(', ', $this->session->userdata('quotation_access')['quotation_procurement_user_id']).") || rfq_mst.assigned_to_3 IN (".implode(', ', $this->session->userdata('quotation_access')['quotation_procurement_user_id']).") )";
		}
		if($this->session->userdata('rfq_access')['rfq_product_family_access']) {
			$return_array['where'] .= " AND rfq_mst.product_family IN ('".implode("', '", $this->session->userdata('rfq_access')['rfq_product_family_access'])."')";
		}

		// echo "<pre>";print_r($search_filter_form_data);echo"</pre><hr>";exit;

		if(!empty($search_filter_form_data)){

			$client_id = $country_name = $region_name = array();
			$assigned_to  = $years = $value = array();
			$importance = $quotation_new = $status  = $product_family = $quotation_priority = array();


			// echo "<pre>";print_r(array_column($search_filter_form_data, 'value', 'name'));echo"</pre><hr>";
			//  echo "<pre>";print_r($search_filter_form_data);echo"</pre><hr>";exit;

			foreach ($search_filter_form_data as $form_data) {

				$name = $form_data['name'];
				if(!empty($form_data['value'])){

					switch ($name) {

						case 'quote_no':

							$return_array['where'] .= " AND quotation_mst.quote_no LIKE '%".trim($form_data['value'])."%'";
						break;
						case 'proforma_no':

							$return_array['where'] .= " AND quotation_mst.proforma_no LIKE '%".trim($form_data['value'])."%'";
						break;
						case 'entered_on':

							$filter_date = explode(' - ', $form_data['value']);
						    $return_array['where'] .= " AND quotation_mst.entered_on >= '".date('Y-m-d 00:00:00', strtotime($filter_date[0]))."'";
						    if(count($filter_date) > 1) {

						        $return_array['where'] .= " AND quotation_mst.entered_on <= '".date('Y-m-d 23:59:59', strtotime($filter_date[1]))."'";
						    }else {

						        $return_array['where'] .= " AND quotation_mst.entered_on <= '".date('Y-m-d 23:59:59', strtotime($filter_date[0]))."'";
						    }
						break;
						case 'confirmed_on':

							$filter_date = explode(' - ', $form_data['value']);
						    $return_array['where'] .= " AND quotation_mst.confirmed_on >= '".date('Y-m-d 00:00:00', strtotime($filter_date[0]))."'";
						    if(count($filter_date) > 1) {

						        $return_array['where'] .= " AND quotation_mst.confirmed_on <= '".date('Y-m-d 23:59:59', strtotime($filter_date[1]))."'";
						    }else {

						        $return_array['where'] .= " AND quotation_mst.confirmed_on <= '".date('Y-m-d 23:59:59', strtotime($filter_date[0]))."'";
						    }
						break;
						case 'followup_date':

							$filter_date = explode(' - ', $form_data['value']);
						    $return_array['where'] .= " AND quotation_mst.followup_date >= '".date('Y-m-d 00:00:00', strtotime($filter_date[0]))."'";
						    if(count($filter_date) > 1) {

						        $return_array['where'] .= " AND quotation_mst.followup_date <= '".date('Y-m-d 23:59:59', strtotime($filter_date[1]))."'";
						    }else {

						        $return_array['where'] .= " AND quotation_mst.followup_date <= '".date('Y-m-d 23:59:59', strtotime($filter_date[0]))."'";
						    }
						break;

						case 'client_id':
						case 'country_name':
						case 'assigned_to':
						case 'procurement_person':
						case 'years':
						case 'value':
						case 'importance':
						case 'quotation_new':
						case 'status':
						case 'product_family':
						case 'quotation_priority':

							$$name[] = $form_data['value'];
						break;
						case 'member':
							
							$return_array['join'] .= '
													LEFT JOIN
														customer_dtl
															ON quotation_mst.member_id = customer_dtl.comp_dtl_id';

							$return_array['where'] .= " AND quotation_mst.member_id = ".$form_data['value'];
							
						break;
					}
				}
			}

			//  echo "<pre>";print_r($client_id);echo"</pre><hr>";exit;

			if(!empty($client_id)){

				$return_array['where'] .= " AND quotation_mst.client_id IN (".implode(", ", $client_id).")";
				// $return_array['where'] .= " AND customer_dtl.comp_mst_id IN (".implode(", ", $client_id).")";
			}
			if(!empty($country_name)){

				$return_array['where'] .= " AND country_mst.name IN ('".implode("', '", $country_name)."')";
			}
			if(!empty($assigned_to)){

				$return_array['where'] .= " AND quotation_mst.assigned_to IN ('".implode("', '", $assigned_to)."')";
			}
			if(!empty($procurement_person)){

				$return_array['where'] .= " AND rfq_mst.assigned_to IN ('".implode("', '", $procurement_person)."')";
			}
			if(!empty($years)){

				$return_array['where'] .= " AND (";
				for ($i=0; $i < count($years); $i++) {

					if($i > 0){

						$return_array['where'] .= " OR ";
					}
					$explode_years = explode("-", $years[$i]);

					$return_array['where'] .= "quotation_mst.entered_on >= '".$explode_years[0].'-04-01'."' AND quotation_mst.entered_on <= '".$explode_years[1].'-03-31'."'";

				}
				$return_array['where'] .= ")";
			}else{
				if(in_array($this->session->userdata('quotation_tab_name'), array('followup_list'))){
					$month = date('m');
					if($month > 3){

						$return_array['where'] .= " AND quotation_mst.entered_on >= '".date('Y')."-04-01 00:00:00' AND quotation_mst.entered_on <= '".( (int)(date('Y')) +1 )."-03-31 23:59:59'";
					}else{

						$return_array['where'] .= " AND quotation_mst.entered_on >= '".( (int)(date('Y')) -1 )."-04-01 00:00:00' AND quotation_mst.entered_on <= '".date('Y')."-03-31 23:59:59'";
					}
				}
			}
			if(!empty($value)){

				$return_array['where'] .= " AND (";
				for ($i=0; $i < count($value); $i++) {
					if($i > 0){
						$return_array['where'] .= " OR ";
					}
					$explode_value = explode("-", $value[$i]);
					if(!empty($explode_value)) {
						if(trim($explode_value[0]) != trim($explode_value[1])) {

							$return_array['where'] .= "CASE
									WHEN quotation_mst.currency = 1 THEN quotation_mst.grand_total >= ".$explode_value[0]." AND quotation_mst.grand_total <= ".$explode_value[1]."
									WHEN quotation_mst.currency = 2 THEN round(quotation_mst.grand_total * 1.09002) >= ".$explode_value[0]." AND round(quotation_mst.grand_total * 1.09002) <= ".$explode_value[1]."
									WHEN quotation_mst.currency = 3 THEN round(grand_total / 77.1379) >= ".$explode_value[0]." AND round(quotation_mst.grand_total / 77.1379) <= ".$explode_value[1]."
									WHEN quotation_mst.currency = 4 THEN round(quotation_mst.grand_total * 1.31013) >= ".$explode_value[0]." AND round(quotation_mst.grand_total * 1.31013) <= ".$explode_value[1]."
									WHEN quotation_mst.currency = 5 THEN round(quotation_mst.grand_total * 0.7278) >= ".$explode_value[0]." AND round(quotation_mst.grand_total * 0.7278) <= ".$explode_value[1]."
									WHEN quotation_mst.currency = 6 THEN round(quotation_mst.grand_total * 0.2722) >= ".$explode_value[0]." AND round(quotation_mst.grand_total * 0.2722) <= ".$explode_value[1]."
								END";
						}else{

							$return_array['where'] .= "CASE
									WHEN quotation_mst.currency = 1 THEN quotation_mst.grand_total >= ".$explode_value[0]."
									WHEN quotation_mst.currency = 2 THEN round(quotation_mst.grand_total * 1.09002) >= ".$explode_value[0]."
									WHEN quotation_mst.currency = 3 THEN round(quotation_mst.grand_total / 77.1379) >= ".$explode_value[0]."
									WHEN quotation_mst.currency = 4 THEN round(quotation_mst.grand_total * 1.31013) >= ".$explode_value[0]."
									WHEN quotation_mst.currency = 5 THEN round(quotation_mst.grand_total * 0.7278) >= ".$explode_value[0]."
									WHEN quotation_mst.currency = 6 THEN round(quotation_mst.grand_total * 0.2722) >= ".$explode_value[0]."
								END ";
						}
					}
				}
				$return_array['where'] .= ")";
			}
			if(!empty($importance)){

				$return_array['where'] .= " AND quotation_mst.importance IN ('".implode("', '", $importance)."')";
			}
			if(!empty($quotation_new)){

				$return_array['where'] .= " AND quotation_mst.is_new IN ('".implode("', '", $quotation_new)."')";
			}
			if(!empty($status)){

				$return_array['where'] .= " AND quotation_mst.status IN ('".implode("', '", $status)."')";
			}
			if(!empty($product_family)){

				$return_array['where'] .= " AND rfq_mst.product_family IN ('".implode("', '", $product_family)."')";
			}
			if(!empty($quotation_priority)){

				$return_array['where'] .= " AND quotation_mst.quotation_priority IN ('".implode("', '", $quotation_priority)."')";
			}

		}

		// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		return $return_array;
	}
    private function creat_tat($date_time){

		$return_tat = '';
		// Get the timestamp of the chat message
		$chat_time = strtotime(str_replace("Z", "", $date_time)); // Replace with your chat message timestamp

		// Create DateTime objects for the chat message and current time
		$chat_date = new DateTime("@$chat_time");
		$current_date = new DateTime();
		// Get the difference between the two dates
		$date_diff = $current_date->diff($chat_date);

		// Output the result
		if ($date_diff->y > 0) {

			$return_tat = $date_diff->y . " year(s)";
		}else if ($date_diff->m > 0) {

			$return_tat = $date_diff->m . " month(s)";
		}else if ($date_diff->d > 0) {

			$return_tat = $date_diff->d . " day(s)";
		}else if ($date_diff->h > 0) {

			$return_tat = $date_diff->h . " hour(s)";
		}else if ($date_diff->i > 0) {

			$return_tat = $date_diff->i . " minute(s)";
		}else {

			$return_tat = $date_diff->s . " second(s)";
		}
		
		return $return_tat." ago";
	}
	private function get_last_followup_data($data) {

		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$return_array = array('last_followup' => '', 'last_comments' => '');
		if(!empty($data)){

			$date1 = date_create($data['followedup_on']);
			$date2 = date_create(date('Y-m-d'));
			$diff_obj = date_diff($date1, $date2);
			$diff = $diff_obj->format("%a");
			if($diff < 8){
				$return_array['last_followup'] = $diff.' days ago';
			}else if($diff < 30){
				$weeks = round($diff / 7);
				$return_array['last_followup'] = $weeks.' weeks ago';
			}else if($diff < 365){
				$months = round($diff / 30);
				$return_array['last_followup'] = $months.' months ago';
			}else if($diff > 365){
				$years = round($diff / 365);
				$return_array['last_followup'] = $years.' years ago';
			}
			$return_array['last_comments'] = $data['follow_up_text'];
		}
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		return $return_array;
	}

	private function prepare_quotation_details($req_details){
		$data = array();
		// echo "<pre>";print_r($req_details['quotation_id']);echo"</pre><hr>";exit;

		$data['quotation_data'] = $this->common_model->get_all_conditional_data_sales_db('*', array('quotation_mst_id'=> $req_details['quotation_id']), 'quotation_mst','row_array');

		if(!empty($data['quotation_data'])) {
		
			$data['quotation_details'] = $this->common_model->get_all_conditional_data_sales_db('*', array('quotation_mst_id'=> $req_details['quotation_id']), 'quotation_dtl');
			
			$data['transport_details'] = $this->common_model->get_all_conditional_data_sales_db('*', array('mode_id'=> $data['quotation_data']['transport_mode']),'transport_mode','row_array');
			
			$data['payment_details'] = $this->common_model->get_all_conditional_data_sales_db('*', array('term_id'=> $data['quotation_data']['payment_term']),'payment_terms','row_array');
		
			$data['delivery_to_details'] = $this->common_model->get_all_conditional_data_sales_db('*', array('delivery_id'=> $data['quotation_data']['delivered_through']),'delivery','row_array');

			$data['delivery_time_details'] = $this->common_model->get_all_conditional_data_sales_db('*', array('dt_id'=> $data['quotation_data']['delivery_time']),'delivery_time','row_array');
			
			$data['origin_country_details'] = $this->common_model->get_all_conditional_data_sales_db('*', array('country_id'=> $data['quotation_data']['origin_country']),'origin_country','row_array');

			$data['currency_details'] = $this->common_model->get_all_conditional_data_sales_db('*', array('currency_id'=> $data['quotation_data']['currency']),'currency','row_array');

			$data['validity_details'] = $this->common_model->get_all_conditional_data_sales_db('*', array('validity_id'=> $data['quotation_data']['validity']),'validity','row_array');

			$data['mtc_type_details'] = $this->common_model->get_all_conditional_data_sales_db('*', array('mtc_id'=> $data['quotation_data']['mtc_type']),'mtc_type','row_array');
			
			$unit_list = array_column($this->common_model->get_dynamic_data_sales_db('unit_id, unit_value', array('status' => 'Active'), 'units'), 'unit_value', 'unit_id');
			$unit_list['']='';
			$unit_list[0]='';

			foreach ($data['quotation_details'] as $quotation_details_key => $single_quotation_details){

				$data['quotation_details'][$quotation_details_key]['unit_value'] = $unit_list[ $single_quotation_details['unit']];
			}

			$data['followup_details'] = $this->common_model->get_all_conditional_data_sales_db('*', array('quotation_mst_id'=> $req_details['quotation_id']), 'follow_up');

			$data['client_details'] = $this->common_model->get_all_conditional_data_sales_db('*', array('id'=> $data['quotation_data']['client_id']),'customer_mst','row_array');
								
			$data['member_details'] = $this->common_model->get_all_conditional_data_sales_db('*', array('comp_mst_id'=> $data['quotation_data']['client_id']),'customer_dtl');

			$data['region_details'] = $this->common_model->get_all_conditional_data_sales_db('*', array('id'=> $data['client_details']['region_id']),'region_mst','row_array');
				
			$data['country_details'] = $this->common_model->get_all_conditional_data_sales_db('*', array('id'=> $data['client_details']['country_id']),'country_mst','row_array');
		
			$data['quotation_siblings'] = $this->common_model->get_all_conditional_data_sales_db('*', array('client_id'=> $data['client_details']['id'], 'quotation_mst_id !='=>$data['quotation_data']['quotation_mst_id']),'quotation_mst');

		}

		$data['tab_name'] = $req_details['sub_tab'];
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		return $data;
	}

	private function get_pdf_information($quote_id){

		if (!empty($quote_id)) {

			$data['quotation_data'] = $this->common_model->get_dynamic_data_sales_db('*', array('quotation_mst_id'=> $quote_id), 'quotation_mst','row_array');

			$data['quotation_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('quotation_mst_id'=> $quote_id), 'quotation_dtl','result_array');

			if (!empty($data['quotation_data'])) {

				$data['client_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('id'=> $data['quotation_data']['client_id']),'customer_mst','row_array');

				$data['member_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('comp_dtl_id'=> $data['quotation_data']['member_id']),'customer_dtl','row_array');

				$data['transport_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('mode_id'=> $data['quotation_data']['transport_mode']),'transport_mode','row_array');

				$data['payment_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('term_id'=> $data['quotation_data']['payment_term']),'payment_terms','row_array');

				$data['delivery_to_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('delivery_id'=> $data['quotation_data']['delivered_through']),'delivery','row_array');

				$data['delivery_time_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('dt_id'=> $data['quotation_data']['delivery_time']),'delivery_time','row_array');

				$data['origin_country_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('country_id'=> $data['quotation_data']['origin_country']),'origin_country','row_array');

				$data['currency_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('currency_id'=> $data['quotation_data']['currency']),'currency','row_array');

				$data['validity_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('validity_id'=> $data['quotation_data']['validity']),'validity','row_array');

				$data['mtc_type_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('mtc_id'=> $data['quotation_data']['mtc_type']),'mtc_type','row_array');

				$data['country_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('id'=> $data['client_details']['country_id']),'country_mst','row_array');

				$data['region_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('id'=> $data['client_details']['region_id']),'region_mst','row_array');

				$data['assigned_to_person'] = $this->common_model->get_dynamic_data_sales_db('*', array('user_id'=> $data['quotation_data']['assigned_to']),'users','row_array');

				$unit_list = array_column($this->common_model->get_dynamic_data_sales_db('unit_id, unit_value', array('status' => 'Active'), 'units'), 'unit_value', 'unit_id');
				$unit_list['']='';
				$unit_list[0]='';

				foreach ($data['quotation_details'] as $quotation_key => $single_quotation_details){

					$data['quotation_details'][$quotation_key]['unit_value'] = $unit_list[ $single_quotation_details['unit']];
				}
			}
		}

		$dec_text = 'paise';
		if ($data['currency_details']['currency'] != 'INR') {
			$dec_text = 'cents';
		}

		$data['grand_total_words'] = $data['currency_details']['currency']." : ".$this->numberTowords($data['quotation_data']['grand_total'], $dec_text);

		$data['additional_comment'] = '';
		if($data['quotation_data']['additional_comment'] != ''){
			$data['additional_comment'] = '<br/><br/>'.$data['quotation_data']['additional_comment'];
		}

		$data['net_total'] = (!empty($data['quotation_data'])) ? $data['quotation_data']['net_total'] : 00;
		if(strpos($data['net_total'], '.') === false){
			$data['net_total'] = $data['net_total'].'.00';
		}

		$data['grand_total'] = (!empty($data['quotation_data'])) ? $data['quotation_data']['grand_total'] : 00;
		if(strpos($data['quotation_data']['grand_total'], '.') === false){
			$data['grand_total'] = $data['quotation_data']['grand_total'].'.00';
		}

		//client_type
		if($data['quotation_data']['client_type'] == 'omtubes'){

			$data['pdf_information'] = $this->common_model->get_dynamic_data_sales_db('*', array('status' => 'Active', 'type'=> 'om'), 'pdf_information', 'row_array');
		}else if($data['quotation_data']['client_type'] == 'zengineer'){

			$data['pdf_information'] = $this->common_model->get_dynamic_data_sales_db('*', array('status' => 'Active', 'type'=> 'zen'), 'pdf_information', 'row_array');

		}else if($data['quotation_data']['client_type'] == 'instinox'){

			$data['pdf_information'] = $this->common_model->get_dynamic_data_sales_db('*', array('status' => 'Active', 'type'=> 'in'), 'pdf_information', 'row_array');
		}

		if($this->session->userdata('role') == 5 || $this->session->userdata('role') == 16){
			$this->common_model->update_data_sales_db('quotation_mst', array('is_new' => NULL), array('quotation_mst_id' => $quote_id));
		}
		if(($data['delivery_to_details']['delivery_name'] == 'CFR Port Name' || $data['delivery_to_details']['delivery_name'] == 'CIF Port Name') && $data['transport_details']['mode'] == 'Sea Worthy'){
			$port_name = $this->quotation_management_model->getPortName('CIF', 'sea', $data['country_details']['name']);
			if($data['delivery_to_details']['delivery_name'] == 'CFR Port Name'){
				$data['delivery_to_details']['delivery_name'] = 'CFR '.$port_name;
			} else if($data['delivery_to_details']['delivery_name'] == 'CIF Port Name'){
				$data['delivery_to_details']['delivery_name'] = 'CIF '.$port_name;
			}
		}

		return $data;
	}

	private function numberTowords($num, $dec_text){

		$ones = array(
			'' =>"ZERO",
			0 =>"ZERO",
			1 => "ONE",
			2 => "TWO",
			3 => "THREE",
			4 => "FOUR",
			5 => "FIVE",
			6 => "SIX",
			7 => "SEVEN",
			8 => "EIGHT",
			9 => "NINE",
			10 => "TEN",
			11 => "ELEVEN",
			12 => "TWELVE",
			13 => "THIRTEEN",
			14 => "FOURTEEN",
			15 => "FIFTEEN",
			16 => "SIXTEEN",
			17 => "SEVENTEEN",
			18 => "EIGHTEEN",
			19 => "NINETEEN",
			"014" => "FOURTEEN"
		);
		$tens = array( 
			0 => "ZERO",
			1 => "TEN",
			2 => "TWENTY",
			3 => "THIRTY", 
			4 => "FORTY", 
			5 => "FIFTY", 
			6 => "SIXTY", 
			7 => "SEVENTY", 
			8 => "EIGHTY", 
			9 => "NINETY" 
		); 
		$hundreds = array( 
			"HUNDRED", 
			"THOUSAND", 
			"MILLION", 
			"BILLION", 
			"TRILLION", 
			"QUARDRILLION" 
		); /*limit t quadrillion */
		$num = number_format($num,2,".",","); 
		$num_arr = explode(".",$num); 
		$wholenum = $num_arr[0]; 
		$decnum = $num_arr[1]; 
		$whole_arr = array_reverse(explode(",",$wholenum)); 
		krsort($whole_arr,1); 
		$rettxt = ""; 
		foreach($whole_arr as $key => $i){
			
		while(substr($i,0,1)=="0")
				$i=substr($i,1,5);
		if($i < 20){ 
		/* echo "getting:".$i; */
		$rettxt .= $ones[$i]; 
		}elseif($i < 100){ 
		if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)]; 
		if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)]; 
		}else{ 
		if(substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
		if(substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)]; 
		if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)]; 
		} 
		if($key > 0){ 
		$rettxt .= " ".$hundreds[$key]." "; 
		}
		} 
		if($decnum > 0){
			$rettxt .= " and ";
			if($decnum < 20){
				if(!empty($ones[$decnum])) {
					$rettxt .= $ones[$decnum];
				}
			}elseif($decnum < 100){
				$rettxt .= $tens[substr($decnum,0,1)];
				$rettxt .= " ".$ones[substr($decnum,1,1)];
			}

			$rettxt .= " ".$dec_text;
		}
		return $rettxt;
	}

	private function upload_doc_config($quotation_id) {

		// echo "<pre>";print_r($_FILES);echo"</pre><hr>";
		// echo "<pre>";print_r($quotation_id);echo"</pre><hr>";exit;
		$config['upload_path']          = './assets/purchase_orders/';
        $config['allowed_types']        = 'pdf';//'jpeg|gif|jpg|png';
        $config['file_name']            = 'PO_'.$quotation_id.'-'.time();
        $config['overwrite']            = TRUE;

        $this->load->library('upload', $config);

		$data = array('status' => 'successful', 'msg' => '', 'file_name' => '');
        if ( ! $this->upload->do_upload('file'))
        {
			$error = array('error' => $this->upload->display_errors());
			$data['status'] = 'failed';
			$data['msg'] = $error['error'];
        }
        else
        {
			$file_dtls = $this->upload->data();
			$data['file_name'] = $file_dtls['file_name'];
        }
        // echo "<pre>";print_r($data);echo"</pre><hr>";exit;
        return $data;
	}

	private function insert_into_daily_work_sales_on_quotations_data($data) {

		$insert_array_for_daily_report['user_id'] = $this->session->userdata('user_id');
		$insert_array_for_daily_report['quotation_id'] = $data['quotation_mst_id'];
		$quotation_details = $this->common_model->get_dynamic_data_sales_db('client_id, quote_no, grand_total', array('quotation_mst_id' => $data['quotation_mst_id']), 'quotation_mst','row_array');
		$insert_array_for_daily_report['quotation_no'] = '';
		$insert_array_for_daily_report['client_id'] = '';
		$insert_array_for_daily_report['value'] = '';
		$insert_array_for_daily_report['client_name'] = '';
		$insert_array_for_daily_report['country'] = '';
		$insert_array_for_daily_report['member_name'] = '';
		$insert_array_for_daily_report['email_sent'] = '';
		if(!empty($quotation_details)) {

			$insert_array_for_daily_report['quotation_no'] = $quotation_details['quote_no'];
			$insert_array_for_daily_report['client_id'] = $quotation_details['client_id'];
			$insert_array_for_daily_report['value'] = $quotation_details['grand_total'];
			
			$client_details = $this->common_model->get_dynamic_data_sales_db('name, country_id', array('id' => $quotation_details['client_id']), 'customer_mst','row_array');
			if(!empty($client_details)){

				$insert_array_for_daily_report['client_name'] = $client_details['name'];
				if(!empty($client_details['country_id'])){

					$country_details = $this->common_model->get_dynamic_data_sales_db('name', array('id' => $client_details['country_id']), 'country_mst','row_array');
					if(!empty($country_details)){

						$insert_array_for_daily_report['country'] = $country_details['name'];
					}
				}
			}			
		}
		$insert_array_for_daily_report['member_id'] = $data['member_id'];
		// $member_details = $this->common_model->get_dynamic_data_sales_db('name', array('member_id' => $data['member_id']), 'members','row_array');
		$member_details = $this->common_model->get_dynamic_data_sales_db('member_name', array('comp_dtl_id' => $data['member_id']), 'customer_dtl','row_array');
		if(!empty($member_details)){

			$insert_array_for_daily_report['member_name'] = $member_details['member_name'];
		}
		$insert_array_for_daily_report['contact_date'] = date('Y-m-d h:i:s');
		$insert_array_for_daily_report['contact_mode'] = $data['connect_mode'];
		$insert_array_for_daily_report['comments'] = $data['follow_up_text'];
		$insert_array_for_daily_report['email_sent'] = $data['email'];
		$insert_array_for_daily_report['date_indian_timezone'] = $this->get_indian_time();

		// echo "<pre>";print_r($insert_array_for_daily_report);echo"</pre><hr>";exit;
		$this->common_model->insert_data_sales_db('daily_work_sales_on_quotation_data', $insert_array_for_daily_report);
	}
	private function get_indian_time($date_format = 'Y-m-d H:i:s'){

		date_default_timezone_set('Asia/Kolkata');
		return date($date_format);
	}
}