<?php foreach($manpower_list as $manpower_list_key=> $manpower_list_data){?>
<tr>
    <td class="kt-align-center"><?php echo $manpower_list_key+1; ?></td>
    <td class="kt-align-center"><?php echo $manpower_list_data['mrf_no'];?></td>
    <td class="kt-align-center"><?php echo date('j, F y', strtotime($manpower_list_data['date']));?></td>
    <td class="kt-align-center"><?php echo $manpower_list_data['total_no_of_positions']?></td>
    <td class="kt-align-center"><?php echo $manpower_list_data['candidate_count'];?></td>
    <td class="kt-align-center">
        <?php if(!empty($manpower_list_data['assigned_to_list'])){
            foreach($manpower_list_data['assigned_to_list'] as $single_key => $single_user_id){

                $explode_name = explode(" ", $assigned_details[$single_user_id['user_id']]);
                echo $manpower_list_data['assigned_to_'.$single_key] =  $explode_name[0];
                ?><hr style="margin: 2px;"><?php
            }
        }?>
    </td>
    <td class="kt-align-center"><?php echo $manpower_list_data['department_name'];?></td>
    <td class="kt-align-center"><?php echo $manpower_list_data['user_designation_name'];?></td>
    <td class="kt-align-center"><?php echo $manpower_list_data['reporting_name'];?></td>
    <td class="kt-align-center"><?php echo $manpower_list_data['location_site'];?></td>
    <td class="kt-align-center"><?php echo $manpower_list_data['priority_span'];?></td>
    <td class="kt-align-center">
        <div class=""  style="display:inline-flex;">
            <?php if($this->session->userdata('hr_access')['mrf_list_action_stage_and_clone_access']){ ?>
                <div class="dropdown kt-hidden">
                    <a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md " data-toggle="dropdown">
                        <i class="flaticon2-avatar" title="Reassign MRF"></i>                            
                    </a>                           
                    <div class="dropdown-menu dropdown-menu-right ">
                        <?php foreach($assigned_to as $value){?>
                        <a class="dropdown-item reassign <?php echo ($manpower_list_data['assigned_to'] == $value['user_id'])?'active':''; ?>" value="<?php echo $value['user_id'] ;?>" id="<?php echo $manpower_list_data['id'];?>"><?php echo $value['name'] ;?></a>
                        <?php }?>                                          
                    </div>                      
                </div>
                <div class="dropdown">                         
                    <a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md " data-toggle="dropdown">
                        <i class="flaticon2-check-mark" title="Change Stage"></i>                            
                    </a>                           
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item change_stage" value="open" id="<?php echo $manpower_list_data['id'];?>">Open</a>
                        <a class="dropdown-item change_stage" value="close" id="<?php echo $manpower_list_data['id'];?>">Close</a>
                        <a class="dropdown-item change_stage" value="join" id="<?php echo $manpower_list_data['id'];?>">Join</a>
                        <a class="dropdown-item change_stage" value="hold" id="<?php echo $manpower_list_data['id'];?>">Hold</a>         
                    </div>
                </div>
                <?php if($manpower_list_data['no_of_positions'] > 1){ ?>
                    <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md clone_mrf_position" title="Clone" id="<?php echo $manpower_list_data['id']?>">
                        <i class="la la-copy"></i>
                    </a>
                <?php } ?>
            <?php } ?>
            <a href="<?php echo base_url('common/mrf/'.$manpower_list_data['id']); ?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">
                <i class="la la-edit"></i>
            </a>
            <a href="<?php echo base_url('common/candidate_list/'.$manpower_list_data['id']);?>" target="_blank" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Candidate List">
                <i class="flaticon2-list-1"></i>
            </a>
            <?php if ($this->session->userdata('hr_access')['mrf_list_action_delete_access']){ ?>
            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_mrf_data" title="Delete" value="<?php echo $manpower_list_data['id']?>">
                <i class="la la-trash"></i>
            </a>
            <?php }?>
        </div>	
    </td>
</tr>
<?php }?>