<table class="kt-datatable__table" style="display: block;">
    <thead class="kt-datatable__head">
        <tr class="kt-datatable__row" style="left: 0px;">
            <th class="kt-align-center" style="width: 5%;"><span>ID</span></th>
            <th class="kt-align-center" style="width: 15%;"><span>User Name</span></th>
            <th class="kt-align-left" style="width: 15%;"><span>Procurement</span></th>
            <th class="kt-align-left" style="width: 15%;"><span>Request For Quoatations</span></th>
            <th class="kt-align-left" style="width: 15%;"><span>Query</span></th>
            <th class="kt-align-left" style="width: 15%;"><span>Vendor</span></th>
            <th class="kt-align-center" style="width: 10%;"><span>Action</span></th>
        </tr>
    </thead>
    <tbody class="kt-datatable__body" style="">
        <?php if(!empty($lead_data)) { ?>
            <?php foreach($lead_data as $lead_key => $lead_details) {?>
                <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                    <td class="kt-datatable__cell kt-align-center" style="width: 5%;"><span><?php echo $lead_key+1; ?></span></td>
                    <td class="kt-datatable__cell kt-align-center" style="width: 15%;"><span><?php echo $lead_details['name']; ?></span></td>
                    <td data-autohide-disabled="false" class="kt-datatable__cell kt-align-left" style="width: 15%;">
                        <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/""/procurement'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['procurement']; ?></a>
                    </td>
                    <td data-autohide-disabled="false" class="kt-datatable__cell kt-align-left" style="width: 15%;">
                        <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/""/rfq_mst'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['rfq']; ?></a>
                    </td>
                    <td data-autohide-disabled="false" class="kt-datatable__cell kt-align-left" style="width: 15%;">
                        <span class="kt-font-bold kt-font-primary"><?php echo $lead_details['query']; ?></span>
                    </td>
                    <td data-autohide-disabled="false" class="kt-datatable__cell kt-align-left" style="width: 15%;">
                        <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/""/vendors'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['vendor']; ?></a>
                    </td>
                    <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell kt-align-center" style=" width: 10%;">
                        <span style="overflow: visible; position: relative;">
                            <a href="<?php echo base_url('common/view_user_data/'), $lead_details['user_id'];?>" target="_blank" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit details">
                                <i class="la la-edit"></i>
                            </a>
                        </span>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
    </tbody>
</table>