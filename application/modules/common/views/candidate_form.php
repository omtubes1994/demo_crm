<style type="text/css">
	.candidate_font {
		font-size: 1rem;
		font-weight: 500;
		line-height: 1.5rem;
		-webkit-transition: color 0.3s ease;
		transition: color 0.3s ease;
		color: #000;
	}
</style>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="kt-portlet">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					Candidate Form
				</h3>
			</div>
		</div>
		<form class="kt-form kt-form--label-right candidate_font" id="candidate_form">
			<input type="text" name="mrf_id" value="<?php echo $mrf_id;?>" hidden>
			<input type="text" name="hr_user_id" value="<?php echo $candidate_data['hr_user_id'];?>" hidden>
			<div class="kt-portlet__body">
				<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
				<div class="form-group row">
					<label class="col-form-label col-lg-3 col-sm-12">Name</label>
					<div class="col-lg-4 col-md-9 col-sm-12">
						<input type="text" class="form-control" id="candidate_name" name="name" value="<?php echo $candidate_data['name'];?>" placeholder="Enter name">
						<small class="form-text text-danger" id="name_error"></small>
					</div>
				</div>

				<div class="form-group row">
					<label class="col-form-label col-lg-3 col-sm-12">Email address</label>
					<div class="col-lg-4 col-md-9 col-sm-12">
						<input type="text" class="form-control" id="candidate_email" name="email" value="<?php echo $candidate_data['email'];?>" placeholder="Enter your email">
						<small class="form-text text-danger" id="email_error"></small>
					</div>
				</div>

				<div class="form-group row">
					<label class="col-form-label col-lg-3 col-sm-12">Mobile</label>
					<div class="col-lg-4 col-md-9 col-sm-12">
						<input class="form-control" id="candidate_number" name="mobile" maxlength="10" value="<?php echo $candidate_data['mobile'];?>" placeholder="Enter your Number">
						<small class="form-text text-danger" id="number_error"></small>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-form-label col-lg-3 col-sm-12">Source</label>
					<div class="col-lg-4 col-md-9 col-sm-12">
						<select class="form-control kt-select2" id="candidate_source" name="source_id">
							<option value="">Select Source</option>
							<?php foreach($source_list as $single_source){ ?>

								<option value="<?php echo $single_source['id']; ?>"
									<?php echo ($candidate_data['source_id'] == $single_source['id']) ? 'selected': ''; ?>>
									<?php echo $single_source['name']; ?>
								</option>
							<?php } ?>
						</select>
						<small class="form-text text-danger" id="source_error"></small>
					</div>
				</div>

				<div class="form-group row">
					<label class="col-form-label col-lg-3 col-sm-12">Resume:</label>
					<input type="text" name="resume" value="<?php echo $candidate_data['resume']?>" hidden>
					<div class="col-xl-6">
						<div class="dropzone dropzone-multi" id="resume_dropzone">
							<div class="dropzone-panel">
								<?php if($candidate_data['resume'] != ''){
										echo '<a href="'.site_url('assets/hr_document/candidate_resume/'.$candidate_data['resume']).'" target="_blank">View Resume</a>';
								} ?>
								<a class="dropzone-select btn btn-label-brand btn-bold btn-sm">Upload</a>
							</div>
							<div class="dropzone-items">
								<div class="dropzone-item" style="display:none">
									<div class="dropzone-file">
										<div class="dropzone-filename" title="some_image_file_name.jpg">
											<span data-dz-name>some_image_file_name.jpg</span> <strong>(<span  data-dz-size>340kb</span>)</strong>
										</div>
										<div class="dropzone-error" data-dz-errormessage></div>
									</div>
									<div class="dropzone-progress">
										<div class="progress">
											<div class="progress-bar kt-bg-brand" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress></div>
										</div>
									</div>
									<div class="dropzone-toolbar">
										<span class="dropzone-delete" data-dz-remove><i class="flaticon2-cross"></i></span>
									</div>
								</div>
							</div>
						</div>
						<span class="form-text text-muted">Max file size is 2MB and max number of files is 1.</br>Only PDF, docx, etc File Accepted.</span>
					</div>
				</div>

				<div class="kt-portlet__foot">
					<div class=" ">
						<div class="row">
							<div class="col-lg-9 ml-lg-auto">
								<button type="button" class="btn btn-primary submit_candidate" mrf_id="<?php echo $mrf_id;?>" value="<?php echo $candidate_data['id']; ?>">Submit</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>