<style type="text/css">
    .kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
        padding-top: 4% !important;
    }
    .layer-white{
        display: none;
        position: absolute;
        top: 0em !important;
        left: 0em !important;
        width: 100%;
        height: 100%;
        text-align: center;
        vertical-align: middle;
        background-color: rgba(255, 255, 255, 0.55);
        opacity: 1;
        line-height: 1;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
        -webkit-animation-duration: 0.5s;
        animation-duration: 0.5s;
        -webkit-transition: background-color 0.5s linear;
        transition: background-color 0.5s linear;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        will-change: opacity;
        z-index: 9;
    }
    .div-loader{
        position: absolute;
        top: 50%;
        left: 50%;
        margin: 0px;
        text-align: center;
        z-index: 1000;
        -webkit-transform: translateX(-50%) translateY(-50%);
        transform: translateX(-50%) translateY(-50%);
    }
    .kt-spinner:before {
        width: 50px;
        height: 50px;
        margin-top: -10px;
    }
    .kt-aside--fixed .kt-wrapper {
        padding-left: 130px !important;
    }
    .kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper {
        padding-top: 30px !important;
    }
    .salary_slip thead tr th{
        background-color: transparent;
        padding: 1rem 0 0.5rem 0;
        color: #74788d;
        border-top: 0;
        border-bottom: 1px solid #ebedf2;
    }
    .salary_slip tbody tr td{
        background-color: transparent;
        padding: 1rem 0 1rem 0;
        border-top: none;
        font-weight: 700;
        font-size: 1.1rem;
        color: #595d6e;
        padding-top: 1.8rem;
    }
    .grand_total thead tr th{
        font-size: 1.1rem;
        text-transform: capitalize;
        font-weight: 500;
        color: #74788d;
        border-top: 0;
        border-bottom: 1px solid #ebedf2;
        padding: 10px 10px 10px 0;
        background-color: transparent;
    }
    .grand_total tbody tr td{

        font-size: 1.1rem;
        text-transform: capitalize;
        background-color: transparent;
        font-weight: 500;
        color: #595d6e;
        padding: 10px 10px 10px 0;
    }
    span.badge_stage_6{
        background: #33ccff !important;
    }
    span.stage_6{
        color: #33ccff !important;
    }
    span.badge_stage_5{
        background: #28a745 !important;
    }
    span.stage_5{
        color: #28a745 !important;
    }
    span.badge_stage_4{
        background: #007bff !important;
    }
    span.stage_4{
        color: #007bff !important;
    }
    span.badge_stage_3{
        background: #fd7e14 !important;
    }
    span.stage_3{
        color: #fd7e14 !important;
    }
    span.badge_stage_2{
        background: #ffc107 !important;
    }
    span.stage_2{
        color: #ffc107 !important;
    }
    span.badge_stage_1{
        background: #969ca0 !important;
    }
    span.stage_1{
        color: #969ca0 !important;
    }
    span.badge_stage_0{
        background: #ff0000 !important;
    }
    span.stage_0{
        color: #ff0000 !important;
    }
    span.badge_stage_null{
        background: #6c757d !important;
    }
    span.stage_null{
        color: #6c757d !important;
    }
</style>
<!-- end:: Header Mobile -->
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

               <!-- begin:: Content -->
               <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

                   <!--Begin::App-->
                   <div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">

                       <!--Begin:: App Aside Mobile Toggle-->
                       <button class="kt-app__aside-close" id="kt_user_profile_aside_close">
                           <i class="la la-close"></i>
                       </button>

                       <!--End:: App Aside Mobile Toggle-->

                       <!--Begin:: App Aside-->
                       <div class="kt-grid__item kt-app__toggle kt-app__aside" id="kt_user_profile_aside">

                           <!--begin:: Widgets/Applications/User/Profile1-->
                           <div class="kt-portlet">
                               <div class="kt-portlet__head  kt-portlet__head--noborder"></div>
                               <div class="kt-portlet__body kt-portlet__body--fit-y">

                                   <!--begin::Widget -->
                                   <div class="kt-widget kt-widget--user-profile-1">
                                        <div class="kt-widget__head">
                                        <?php if(!empty($people_information)) {?>
                                           <div class="kt-widget__media">
                                               <img src="<?php echo base_url('assets/hr_document/profile_pic/'),$people_information['profile_pic_file_path'];?>" alt="image">
                                           </div>
                                           <div class="kt-widget__content">
                                               <div class="kt-widget__section">
                                                   <a href="javascript:void(0);" class="kt-widget__username">
                                                       <?php echo $people_information['first_name'],' ',$people_information['last_name'];?>
                                                       <i class="flaticon2-correct kt-font-success"></i>
                                                   </a>
                                                   <span class="kt-widget__subtitle">
                                                       <?php echo $people_information['department']; ?>
                                                   </span>
                                               </div>
                                           </div>
                                        <?php }else{?>
                                            <div class="kt-widget__media">
                                               <img src="<?php echo base_url('assets/media/users/default.jpg');?>" alt="image">
                                           </div>
                                           <div class="kt-widget__content">
                                               <div class="kt-widget__section">
                                                   <a href="javascript:void(0);" class="kt-widget__username">
                                                       <?php echo 'Not available';?>
                                                       <i class="flaticon2-correct kt-font-success"></i>
                                                   </a>
                                                   <span class="kt-widget__subtitle">
                                                       <?php echo 'Not available'; ?>
                                                   </span>
                                               </div>
                                           </div>
                                        <?php } ?>
                                        </div>
                                        <div class="kt-widget__body">
                                            <?php if(!empty($people_information)) {?>
                                            <div class="kt-widget__content">
                                               <div class="kt-widget__info">
                                                   <span class="kt-widget__label">Email:</span>
                                                   <a href="javascript:void(0);" class="kt-widget__data"><?php echo $people_information['official_email']; ?></a>
                                               </div>
                                               <div class="kt-widget__info">
                                                   <span class="kt-widget__label">Phone:</span>
                                                   <a href="javascript:void(0);" class="kt-widget__data"><?php echo $people_information['official_number']; ?></a>
                                               </div>
                                               <div class="kt-widget__info">
                                                   <span class="kt-widget__label">Location:</span>
                                                   <span class="kt-widget__data"><?php echo $people_information['job_location']; ?></span>
                                               </div>
                                            </div>
                                            <?php }else{?>
                                            <div class="kt-widget__content">
                                               <div class="kt-widget__info">
                                                   <span class="kt-widget__label">Email:</span>
                                                   <a href="javascript:void(0);" class="kt-widget__data"><?php echo 'Not available'; ?></a>
                                               </div>
                                               <div class="kt-widget__info">
                                                   <span class="kt-widget__label">Phone:</span>
                                                   <a href="javascript:void(0);" class="kt-widget__data"><?php echo 'Not available'; ?></a>
                                               </div>
                                               <div class="kt-widget__info">
                                                   <span class="kt-widget__label">Location:</span>
                                                   <span class="kt-widget__data"><?php echo 'Not available'; ?></span>
                                               </div>
                                            </div>
                                           <?php } ?>
                                           <div class="kt-widget__items kt-scroll" data-scroll="true" style="height: 428px">
                                            <?php if(!empty($lead_data)) {?>
                                                <?php foreach ($lead_data as $lead_details) { ?>
                                                <a href="javascript:void(0);" class="kt-widget__item get_lead_information" table_name="<?php echo $lead_details['table_name']; ?>" lead_name="<?php echo $lead_details['lead_type']; ?>">
                                                    <span class="kt-widget__section">
                                                        <span class="kt-widget__icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                   <rect x="0" y="0" width="24" height="24" />
                                                                   <rect fill="#000000" x="2" y="5" width="19" height="4" rx="1" />
                                                                   <rect fill="#000000" opacity="0.3" x="2" y="11" width="19" height="10" rx="1" />
                                                                </g>
                                                            </svg> </span>
                                                        <span class="kt-widget__desc">
                                                           <?php echo $lead_details['lead_name']; ?>
                                                           <span class="kt-font-bolder kt-font-success">(<?php echo $lead_details['count']; ?>)</span>
                                                        </span>
                                                    </span>
                                                </a>
                                                <?php }?>
                                            <?php }?>
                                           </div>
                                       </div>
                                   </div>

                                   <!--end::Widget -->
                               </div>
                           </div>

                           <!--end:: Widgets/Applications/User/Profile1-->
                       </div>

                       <!--End:: App Aside-->

                       <!--Begin:: App Content-->
                       <div class="kt-grid__item kt-grid__item--fluid kt-app__content listing">
                            <div class="row" style="display:block;">
                                <div class="kt-portlet ">
                                    <div class="kt-portlet__head"  style="padding:25px 15px;">
                                        <div class="col-lg-11">
                                            <div class="row kt-portlet__head-label">
                                                <label class="col-lg-3"><h3 class="kt-portlet__head-title">Country</h3></label>
                                                <label class="col-lg-3"><h3 class="kt-portlet__head-title">Last Contact</h3></label>
                                                <label class="col-lg-4"><h3 class="kt-portlet__head-title">Last Purchase</h3></label>
                                                <label class="col-lg-2"><h3 class="kt-portlet__head-title">Stage</h3></label>
                                            </div>
                                            <div class="row kt-portlet__head-label">
                                                <div class="col-lg-3">
                                                    <select class="form-control kt-selectpicker" name="assign_lead_country" data-live-search="true" multiple>
                                                        <optgroup label="Country" data-max-options="2">
                                                            <?php foreach ($country as $country_details) { ?>
                                                            <option value="<?php echo $country_details['id'], '_', $country_details['name']; ?>"><?php echo $country_details['name']; ?></option>
                                                            <?php }?>
                                                        </optgroup>
                                                    </select>
                                                </div>
                                                <div class="col-lg-3">
                                                    <select class="form-control kt-selectpicker" name="assign_lead_last_contact">
                                                        <option value="">Select</option>
                                                        <option value="0-8">Less Than 1 Week</option>
                                                        <option value="8-30">1 Week To 4 Week</option>
                                                        <option value="30-1000">1 Month+</option>
                                                        <option value="blank">Blank</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-2">
                                                    <select class="form-control kt-selectpicker" name="assign_lead_last_purchase_month" multiple>
                                                        <option value="1">Jan</option>
                                                        <option value="2">Feb</option>
                                                        <option value="3">March</option>
                                                        <option value="4">April</option>
                                                        <option value="5">May</option>
                                                        <option value="6">June</option>
                                                        <option value="7">July</option>
                                                        <option value="8">August</option>
                                                        <option value="9">September</option>
                                                        <option value="10">October</option>
                                                        <option value="11">November</option>
                                                        <option value="12">December</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-2">
                                                    <select class="form-control kt-selectpicker" name="assign_lead_last_purchase_year" multiple>
                                                        <option value="2022">2022</option>
                                                        <option value="2021">2021</option>
                                                        <option value="2020">2020</option>
                                                        <option value="2019">2019</option>
                                                        <option value="2018">2018</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-2">
                                                    <select class="form-control kt-selectpicker" name="assign_lead_stage" multiple>
                                                        <optgroup label="Stage" data-max-options="0">
                                                            <option value="blank">Blank Lead</option>
                                                            <?php foreach ($lead_stage as $stage_details) { ?>
                                                            <option value="<?php echo $stage_details['lead_stage_id']; ?>"><?php echo $stage_details['stage_name']; ?></option>
                                                            <?php }?>
                                                        </optgroup>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-1">
                                            <button type="reset" class="btn btn-brand search_filter" table_name="" lead_name="">Search</button>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__body kt-scroll" data-scroll="true" style="height: 620px">
                                        <div id="hr_edit_listing_table_loader" class="layer-white">
                                            <div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
                                        </div>
                                        <table class="kt-datatable__table">
                                            <thead class="kt-datatable__head">
                                                <tr class="kt-datatable__row">
                                                    <th class="kt-align-right" colspan="6" style="padding: 10px 50px;">
                                                        <label class="col-lg-2"><h3 class="kt-portlet__head-title">User</h3></label>
                                                        <select class="col-lg-2 form-control kt-selectpicker" name="user_name">
                                                            <option value=""></option>
                                                            <?php foreach ($user_list as $department_name => $user_details) { ?>
                                                            <optgroup label="<?php echo $department_name; ?>" data-max-options="2">
                                                                <?php foreach ($user_details as $single_user_details) { ?>
                                                                <option value="<?php echo $single_user_details['user_id']; ?>"><?php echo $single_user_details['name']; ?></option>
                                                                <?php }?>
                                                            </optgroup>
                                                            <?php }?>
                                                        </select>
                                                        <button type="reset" class="btn btn-brand change_assigned_to" table_name="" id_column_name="" assigned_column_name="">Submit</button>
                                                    </th>
                                                </tr>
                                            </thead>
                                        </table>
                                        <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="local_data" style="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                       <!--End:: App Content-->
                   </div>

                   <!--End::App-->
               </div>

               <!-- end:: Content -->
            </div>

            <!-- begin:: Footer -->
            <div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
                <div class="kt-container  kt-container--fluid ">
                    <div class="kt-footer__copyright">
                        2020&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Keenthemes</a>
                    </div>
                    <div class="kt-footer__menu">
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">About</a>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Team</a>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Contact</a>
                    </div>
                </div>
            </div>

            <!-- end:: Footer -->
        </div>
    </div>
</div>

<!-- end:: Page -->

<!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="fa fa-arrow-up"></i>
</div>

<!-- end::Scrolltop -->