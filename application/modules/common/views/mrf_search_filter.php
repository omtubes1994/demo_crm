<form class="kt-form kt-form--fit kt-margin-b-20" id="search_filter_form">
    <div class="mrf_font" id="search_filter_div">
        <div class="row kt-margin-b-20">
            <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                <label>MRF No :</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="mrf_no" name="mrf_no" value="<?php echo $search_filter['mrf_no']?>" placeholder="Search MRF No">
                </div>
            </div>
            <div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
                <label>Assigned To:</label>
                <select class="form-control kt-selectpicker" name="assigned_to"  multiple>
                    <?php foreach($mrf_assigned_to as $single_assigned_to){ ?>
                        <option value="<?php echo $single_assigned_to['user_id']; ?>"
                            <?php echo (in_array($single_assigned_to['user_id'], $search_filter['assigned_to'])) ? 'selected': ''; ?>>
                            <?php echo $single_assigned_to['name']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
                <label>Department:</label>
                <select class="form-control kt-selectpicker" name="mrf_department_id"  multiple data-live-search = "true">
                    <?php foreach($deparment_list as $single_deparment){ ?>
                        <option value="<?php echo $single_deparment['id']; ?>"
                            <?php echo (in_array($single_deparment['id'], $search_filter['mrf_department_id'])) ? 'selected': ''; ?>>
                            <?php echo $single_deparment['name']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
                <label>Job Title:</label>
                <select class="form-control kt-selectpicker" name="mrf_designation_id" multiple data-live-search = "true">
                    <?php foreach($degignation_list as $single_degignation){ ?>
                        <option value="<?php echo $single_degignation['id']; ?>"
                            <?php echo (in_array($single_degignation['id'], $search_filter['mrf_designation_id'])) ? 'selected': ''; ?>>
                            <?php echo $single_degignation['name']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
                <label>Reporting To:</label>
                <select class="form-control kt-selectpicker" name="reporting_to" multiple data-live-search = "true">
                    <?php foreach($reporting_to_list as $single_reporting_to){ ?>
                        <option value="<?php echo $single_reporting_to['user_id']; ?>"
                            <?php echo (in_array($single_reporting_to['user_id'], $search_filter['reporting_to'])) ? 'selected': ''; ?>>
                            <?php echo $single_reporting_to['name']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
                <label>Date</label>
                <input type="date" class="form-control mrf_date_picker" name="date" value="<?php echo $search_filter['date']?>">
            </div>
        </div>
        <div class="kt-separator kt-separator--md kt-separator--dashed"></div>
        <div class="row">
            <div class="col-lg-12">
                <button class="btn btn-primary btn-brand--icon mrf_search_filter_submit" type="reset">
                    <span>
                        <i class="la la-search"></i>
                        <span>Search</span>
                    </span>
                </button>
                &nbsp;&nbsp;
                <button class="btn btn-secondary btn-secondary--icon search_filter_reset" type="reset">
                    <span>
                        <i class="la la-close"></i>
                        <span>Reset</span>
                    </span>
                </button>
            </div>
        </div>
    </div>
</form>