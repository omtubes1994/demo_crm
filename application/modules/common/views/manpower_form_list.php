<?php //echo "<pre>";print_r($manpower_list);echo"</pre><hr>";die; ?>
<style type="text/css">
	.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
		padding-top: 5% !important;
	}
	.layer-white{
	    display: none;
	    position: fixed;
	    top: 0em !important;
	    left: 0em !important;
	    width: 100%;
	    height: 100%;
	    text-align: center;
	    vertical-align: middle;
	    background-color: rgba(255, 255, 255, 0.55);
	    opacity: 1;
	    line-height: 1;
	    -webkit-animation-fill-mode: both;
	    animation-fill-mode: both;
	    -webkit-animation-duration: 0.5s;
	    animation-duration: 0.5s;
	    -webkit-transition: background-color 0.5s linear;
	    transition: background-color 0.5s linear;
	    -webkit-user-select: none;
	    -moz-user-select: none;
	    -ms-user-select: none;
	    user-select: none;
	    will-change: opacity;
	    z-index: 9;
	}
	.div-loader{
	    position: absolute;
		top: 50%;
		left: 50%;
		margin: 0px;
		text-align: center;
		z-index: 1000;
		-webkit-transform: translateX(-50%) translateY(-50%);
		transform: translateX(-50%) translateY(-50%);
	}
	.kt-spinner:before {
	    width: 50px;
	    height: 50px;
	    margin-top: -10px;
	}

	.kt-badge.kt-badge--orange {
		color: #111111;
    	background: #fd7e14;
	}
	.kt-badge.kt-badge--unified-orange {
		color: #fd7e14;
		background: #FFDAB9;
	}
	.kt-badge.kt-badge--yellow {
		color: #111111;
		background: #ffc107;
	}

	.mrf_font {
		font-size: 1rem !important;
		font-weight: 500 !important;
		line-height: 1.5rem !important;
		color: #000;
	}
	ul.menu-tab {
		margin: 0px;
		padding: 0px;
		border: 1px solid #E7E7E7;
		overflow: hidden;
		font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
		font-size: 14px;
		line-height: 20px;
		color: #333;
	}
	ul.menu-tab li{
		width: calc(100%/4) !important;
		display: inline;
		text-align: center;
		float: left;
	}
	ul.menu-tab li.active_list{
		border-bottom: 0.40rem solid #343a40 !important;
	}
	ul.menu-tab li a{

		cursor: pointer;
		display: inline-block;
		outline: none;
		text-align: center;
		width: 100%;
		background-color: #5578eb;
		border-color: #5578eb;
		color: #ffffff;
		font-size: 18px;
		font-family: 'latomedium';
		padding: 3px 3px 20px 3px;
	}
	ul.menu-tab li a i{
		display: inline-block;
		width: 35px;
		height: 28px;
		font-style: normal;
		background-size: 100%;
		position: relative;
		top: 6px;
	}
	.mrf_font {
		font-size: 1rem;
		font-weight: 500;
		line-height: 1.5rem;
		-webkit-transition: color 0.3s ease;
		transition: color 0.3s ease;
		color: #000;
	}
	
</style>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					Manpower List
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
		        <div class="kt-portlet__head-wrapper">
		            <div class="kt-portlet__head-actions">
						<a href="javascript:;" class="btn btn-brand btn-elevate btn-icon-sm add_search_filter" button_value = "show">
							Add Search Filter
						</a>
		                <a href="<?php echo base_url('common/mrf'); ?>" target="" class="btn btn-brand btn-elevate btn-icon-sm">
		                    Add New
		                </a>
		            </div>
		        </div>
	    	</div>
		</div>
		<div class="kt-portlet__body search_filter_data" style="display: none;">
			<?php $this->load->view('common/mrf_search_filter');?>
		</div>
	</div>
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__body">
			<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
				<div class="row">
					<div class="col-sm-12" >
						<ul class="menu-tab kt-font-bolder" id='tab' style="">
							<li class="tab_name_click open_list active_list" tab-name="open_list">
								<a>
									<span style="top: 6px;position: relative;">Open</span>
								</a>
							</li>
							<li class="tab_name_click hold_list" tab-name="hold_list" style="padding: 0px 0px 0px 10px;">
								<a>
									<span style="top: 6px;position: relative;">Hold</span>
								</a>
							</li>
							<li class="tab_name_click join_list" tab-name="join_list" style="padding: 0px 0px 0px 10px;">
								<a>
									<span style="top: 6px;position: relative;">Join</span>
								</a>
							</li>
							<li class="tab_name_click closed_list" tab-name="closed_list" style="padding: 0px 0px 0px 10px;">
								<a>
									<span style="top: 6px;position: relative;">Closed</span>
								</a>
							</li>
						</ul>
					</div>

					<div class="col-sm-12" id="mrf_table_data">
						<!-- <input type="text" id="column_name" value="" hidden>
						<input type="text" id="column_value" value="" hidden> -->
						<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline mrf_font" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 100%; min-height: 400px;">
							<thead id="mrf_list_header">
								<tr>
									<th class="sorting_disabled kt-align-center" style="width: 2%;">Sr.No</th>
									<th class="sorting_disabled kt-align-center" style="width: 10%;">No</th>
									<th class="sorting_disabled kt-align-center" style="width: 10%;">Open Date</th>
									<th class="sorting_disabled kt-align-center" style="width: 3%;">NO OF POSITIONS</th>
									<th class="sorting_disabled kt-align-center" style="width: 3%;">NO OF Candidate</th>
									<th class="sorting_disabled kt-align-center" style="width: 10%;">Assigned To</th>
									<th class="sorting_disabled kt-align-center" style="width: 10%;">DEPARTMENT</th>
									<th class="sorting_disabled kt-align-center" style="width: 10%;">JOB TITLE</th>
									<th class="sorting_disabled kt-align-center" style="width: 10%;">REPORTING TO</th>
									<th class="sorting_disabled kt-align-center" style="width: 10%;">LOCATION / SITE</th>
									<th class="sorting_disabled kt-align-center" style="width: 10%;">PRIORITY / CLOSING DATE</th>
									<th class="sorting_disabled kt-align-center" style="width: 15%;">Actions</th>
								</tr>
							</thead>
							<tbody id="mrf_list_body">
								<?php $this->load->view('common/manpower_list_body');?>
							</tbody>
						</table>
						<div id="mrf_list_process" class="layer-white">
							<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
						</div>
					</div>
				</div>
				<div class="row" id="mrf_list_paggination">
					<?php echo $this->load->view('common/paggination');?>
				</div>
			</div>
		</div>
	</div>
</div>