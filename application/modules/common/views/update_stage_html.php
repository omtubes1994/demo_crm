<div class="row">
	<div class="col-md-12 form-group row">
	    <label class="col-form-label col-lg-3 col-sm-12">Stage</label>
		<select class="form-control kt-select2 stage_id" id="kt_select2" name="stage_id">
            <option>Select Stage</option>
            <?php foreach($stage_list as $single_stage){ ?>
                <option value="<?php echo $single_stage['id']; ?>"
                    <?php echo ($stage_id == $single_stage['id']) ? 'selected': ''; ?>>
                    <?php echo $single_stage['name']; ?>
                </option>
            <?php } ?>
        </select>
	</div>

    <div class="col-md-12 form-group row candidate_stage_reason" style="display:<?php echo($stage_id == 7) ? 'block': 'none';?>">
	    <label class="col-form-label col-lg-4 col-sm-12">Stage Reason</label>
		<select class="form-control kt-select2 stage_reason_id" name="stage_reason_id">
            <option value="">Select Source</option>
            <?php foreach($stage_reason_list as $single_stage_reason){ ?>

                <option value="<?php echo $single_stage_reason['id']; ?>"
                    <?php echo ($stage_reason_id == $single_stage_reason['id']) ? 'selected': ''; ?>>
                    <?php echo $single_stage_reason['name']; ?>
                </option>
            <?php } ?>
        </select>
	</div>

    <div class="col-md-12 form-group row other_stage_reason" style="display:<?php echo($stage_reason_id == 4) ? 'block': 'none';?>">
	    <label class="col-form-label col-lg-4 col-sm-12">Other Reason</label>
		<input type="text" class="form-control" name="other_reason" value="<?php echo $customer_data['other_reason'];?>">
	</div>
</div>