<table class="kt-datatable__table" style="display: block;">
	<thead class="kt-datatable__head">
		<tr class="kt-datatable__row" style="left: 0px;">
			<th class="kt-align-left" style="width: 5%;">
				<span>
					<label class="kt-checkbox kt-checkbox--single kt-checkbox--all kt-checkbox--solid">
						<input class="checked_all" type="checkbox">&nbsp;<span></span>
					</label>
					<span class="checkbox_checked_count" count = 0>(0)</span>
				</span>
			</th>
			<?php if(in_array($table_name, array('customer_mst'))){ ?>

				<?php if(in_array($lead_name, array('tubes', 'pipes', 'process_control', 'tubing', 'hammer union', 'internal_data'))){ ?>
					<th class="kt-align-left" style="width: 5%;"><span>Rank</span></th>
					<th class="kt-align-left" style="width: 35%;"><span>Company Name</span></th>
					<th class="kt-align-center" style="width: 15%;"><span>Country</span></th>
					<th class="kt-align-left" style="width: 15%;"><span>Last Contact</span></th>
					<th class="kt-align-left" style="width: 15%;"><span>Last Purchased</span></th>
					<th class="kt-align-center" style="width: 10%;"><span>Stage</span></th>

				<?php } else if(in_array($lead_name, array('chemical companies', 'epc companies', 'distributors', 'shipyards', 'water companies', 'forged fittings', 'heteregenous tubes india', 'sugar companies', 'pvf companies', 'miscellaneous leads', 'Hydraulic fitting', 'Fasteners'))){ ?>
					
					<th class="kt-align-left" style="width: 5%;"><span>Id</span></th>
					<?php if(!in_array($lead_name, array('distributors'))){ ?>
						<th class="kt-align-left" style="width: 50%;"><span>Company Name</span></th>
					<?php } else { ?>
						<th class="kt-align-left" style="width: 35%;"><span>Company Name</span></th>
						<th class="kt-align-left" style="width: 15%;"><span>Brand</span></th>
					<?php } ?>
					<th class="kt-align-center" style="width: 15%;"><span>Country</span></th>
					<th class="kt-align-left" style="width: 15%;"><span>Last Contact</span></th>
					<th class="kt-align-center" style="width: 10%;"><span>Stage</span></th>	
				<?php } ?>
			<?php } else{?>
				<th class="" style="width: 10%;"><span>ID</span></th>
				<th class="" style="width: 65%;"><span>Company Name</span></th>
				<th class="" style="width: 20%;"><span>Stage</span></th>
			<?php } ?>
		</tr>
	</thead>
	<tbody class="kt-datatable__body" style="color: #74788d;font-weight: 500;">
		<?php if(!empty($lead_data)){?>
			<?php if(in_array($table_name, array('customer_mst'))){ ?>
				<?php if(in_array($lead_name, array('tubes', 'pipes', 'process_control', 'tubing', 'hammer union', 'internal_data'))){ ?>

					<?php foreach ($lead_data as $lead_key => $lead_details) { ?>
						<tr class="kt-datatable__row tr_<?php echo $lead_details['id']; ?> all_tr" style="left: 0px;">
							<td class="kt-align-left" style="width: 5% !important;">
								<span>
									<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid" style="margin: 5px 0px 5px 0px;">
										<input class="checkbox_checked checkbox_<?php echo $lead_details['id']; ?>" type="checkbox" value="<?php echo $lead_details['id']; ?>" name="lead_id[]">&nbsp;<span></span>
									</label>
								</span>
							</td>
							<td class="kt-align-left" style="width: 5% !important;"><span><?php echo $lead_details['rank']; ?></span></td>
							<td class="kt-align-left" style="width: 35% !important;"><span><?php echo $lead_details['name']; ?></span></td>
							<td class="kt-align-center" style="width: 15% !important;">
								<span>
									<?php if(!empty($country_details[$lead_details['country_id']])) {?>
										<?php echo $country_details[$lead_details['country_id']]; ?>
									<?php } ?>
								</span>	
							</td>
							<td class="kt-align-left" style="width: 15% !important;"><span><?php echo $lead_details['last_contact']; ?></span></td>
							<td class="kt-align-left" style="width: 15% !important;"><span><?php echo date('M Y', strtotime($lead_details['last_purchased'])); ?></span></td>
							<?php if(array_key_exists('lead_stage', $lead_details)) {?>
								<td class="kt-align-center" style="width: 10% !important;">
									<span>
										<span class="kt-badge kt-badge--primary kt-badge--dot badge_stage_<?php echo $lead_details['lead_stage'];?>"></span>
										&nbsp;
										<span class="kt-font-bold kt-font-primary stage_<?php echo $lead_details['lead_stage'];?>">Stage <?php echo $lead_details['lead_stage']; ?></span>
									</span>
								</td>
							<?php }?>
						</tr>
					<?php } ?>

				<?php } else if(in_array($lead_name, array('chemical companies', 'epc companies', 'distributors', 'shipyards', 'water companies', 'forged fittings', 'heteregenous tubes india', 'sugar companies', 'pvf companies', 'miscellaneous leads', 'Hydraulic fitting', 'Fasteners'))){ ?>

					<?php foreach ($lead_data as $lead_key => $lead_details) { ?>
						<tr class="kt-datatable__row tr_<?php echo $lead_details['id']; ?> all_tr" style="left: 0px;">
							<td class="kt-align-left" style="width: 5% !important;">
								<span>
									<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
										<input class="checkbox_checked checkbox_<?php echo $lead_details['id']; ?>" type="checkbox" value="<?php echo $lead_details['id']; ?>" name="lead_id[]">&nbsp;<span></span>
									</label>
								</span>
							</td>
							<td class="kt-align-left" style="width: 5% !important;"><span><?php echo $lead_key+1; ?></span></td>
							<?php if(!in_array($lead_name, array('distributors'))){ ?>
								<td class="kt-align-left" style="width: 50% !important;"><span><?php echo $lead_details['name']; ?></span></td>
							<?php }else { ?>

								<td class="kt-align-left" style="width: 35% !important;"><span><?php echo $lead_details['name']; ?></span></td>
								<td class="kt-align-left kt-font-info" style="width: 15% !important;"><span><?php echo $lead_details['brand']; ?></span></td>
							<?php } ?>
							<td class="kt-align-center" style="width: 15% !important;">
								<span>
									<?php if(!empty($country_details[$lead_details['country_id']])) {?>
										<?php echo $country_details[$lead_details['country_id']]; ?>
									<?php } ?>
								</span>
							</td>
							<td class="kt-align-left" style="width: 15% !important;"><span><?php echo $lead_details['last_contact']; ?></span></td>
							<?php if(array_key_exists('lead_stage', $lead_details)) {?>
								<td class="kt-align-center" style="width: 15% !important;">
									<span>
										<?php if(!empty($lead_details['lead_stage'])) {?>
											<span class="kt-badge kt-badge--primary kt-badge--dot badge_stage_<?php echo $lead_details['lead_stage']; ?>"></span>
											&nbsp;
											<span class="kt-font-bold kt-font-primary stage_<?php echo $lead_details['lead_stage']; ?>">Stage <?php echo $lead_details['lead_stage']; ?></span>
										<?php }else{ ?>
											<span class="kt-badge kt-badge--primary kt-badge--dot badge_stage_null"></span>
											&nbsp;
											<span class="kt-font-bold kt-font-primary stage_null">Null</span>
										<?php } ?>
									</span>
								</td>
							<?php }?>
						</tr>
					<?php } ?>

				<?php } else if(in_array($lead_name, array('Pre Qualification'))){?>

					<?php foreach ($lead_data as $lead_key => $lead_details) { ?>
						<tr class="kt-datatable__row tr_<?php echo $lead_details['id']; ?> all_tr" style="left: 0px;">
							<td style="width: 5% !important;">
								<span>
									<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
										<input class="checkbox_checked checkbox_<?php echo $lead_details['id']; ?>" type="checkbox" value="<?php echo $lead_details['id']; ?>" name="lead_id[]">&nbsp;<span></span>
									</label>
								</span>
							</td>
							<td style="width: 10% !important;"><span><?php echo $lead_key+1; ?></span></td>
							<td style="width: 65% !important;"><span><?php echo $lead_details['name']; ?></span></td>
							<td style="width: 20% !important;">
							<?php if(array_key_exists('lead_stage', $lead_details)) {?>
								<span>
									<span class="kt-badge kt-badge--primary kt-badge--dot"></span>
									&nbsp;
									<span class="kt-font-bold kt-font-primary">Stage <?php echo $lead_details['lead_stage']; ?></span>
								</span>
							<?php }?>
							</td>
						</tr>
					<?php } ?>

				<?php } ?>
			<?php }else{?>				
				<?php foreach ($lead_data as $lead_key => $lead_details) { ?>
					<tr class="kt-datatable__row tr_<?php echo $lead_details['id']; ?> all_tr" style="left: 0px;">
						<td style="width: 5% !important;">
							<span>
								<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
									<input class="checkbox_checked checkbox_<?php echo $lead_details['id']; ?>" type="checkbox" value="<?php echo $lead_details['id']; ?>" name="lead_id[]">&nbsp;<span></span>
								</label>
							</span>
						</td>
						<td style="width: 10% !important;"><span><?php echo $lead_key+1; ?></span></td>
						<td style="width: 65% !important;"><span><?php echo $lead_details['name']; ?></span></td>
						<td style="width: 20% !important;">
						<?php if(array_key_exists('lead_stage', $lead_details)) {?>
							<span>
								<span class="kt-badge kt-badge--primary kt-badge--dot"></span>
								&nbsp;
								<span class="kt-font-bold kt-font-primary">Stage <?php echo $lead_details['lead_stage']; ?></span>
							</span>
						<?php }?>
						</td>
					</tr>
				<?php } ?>
			<?php } ?>	
		<?php } ?>
	</tbody>
	
</table>


