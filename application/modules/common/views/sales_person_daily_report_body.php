<?php if(!empty($call_type)) { ?>

    <?php if($call_type == 'daily_work_count') { ?>
        <div class="col-xl-12">
            <span class="kt-font-xl kt-font-bold" style="padding: 0px 15px 0px 0px;">
                Desktrack:  <span class="kt-font-info"><?php echo $desktopTime; ?></span>
            </span>
        </div>
        <?php if(!empty($lead_count)) { ?>
        <div class="col-xl-12">
            <span class="kt-font-xl kt-font-bold" style="padding: 0px 15px 0px 0px;">
                Call Connectd:  <span class="kt-font-info"><?php echo $lead_count['call']; ?></span>
            </span>
            <span class="kt-font-xl kt-font-bold" style="padding: 0px 15px 0px 0px;">
                Call Attempted:  <span class="kt-font-info"><?php echo $lead_count['call_attempted']; ?></span>
            </span>
            <span class="kt-font-xl kt-font-bold" style="padding: 0px 15px 0px 0px;">
                Email:  <span class="kt-font-info"><?php echo $lead_count['email']; ?></span>
            </span>
            <span class="kt-font-xl kt-font-bold" style="padding: 0px 15px 0px 0px;">
                LinkedIn:  <span class="kt-font-info"><?php echo $lead_count['linkedin']; ?></span>
            </span>
            <span class="kt-font-xl kt-font-bold" style="padding: 0px 15px 0px 0px;">
                WhatsApp:  <span class="kt-font-info"><?php echo $lead_count['whatsapp']; ?></span>
            </span>
        </div>
        <?php }?>
    <?php }else if($call_type == 'daily_work_lead_table_body'){ ?>
        <thead>
            <tr>
                <th style="width: 5% !important;">Sr.no</th>
                <th style="width: 15% !important;">Category Name</th>
                <th style="width: 15% !important;">Company Name</th>
                <th style="width: 10% !important;">Country</th>
                <th style="width: 10% !important;">Date</th>
                <th style="width: 10% !important;">Member</th>
                <th style="width: 10% !important;">Medium</th>
                <th class="kt-align-center" style="width: 25% !important;">Comment</th>
            </tr>
        </thead>
        <?php if(!empty($lead_last_contacted)){ ?>
            <?php foreach ($lead_last_contacted as $last_contacted_key => $last_contacted_details) { ?>
            <tbody>
                <tr>
                    <td class="kt-font-bold"><?php echo $last_contacted_key+1; ?></td>
                    <td class="kt-font-bold"><?php echo $last_contacted_details['sub_lead_name']; ?></td>
                    <td class="kt-font-bold"><?php echo $last_contacted_details['name']; ?></td>
                    <td class="kt-font-bold"><?php echo $last_contacted_details['country']; ?></td>
                    <td class="kt-font-bold"><?php echo $last_contacted_details['contact_date']; ?></td>
                    <td class="kt-font-bold"><?php echo $last_contacted_details['contact_member_name']; ?></td>
                    <td class="kt-font-bold"><?php echo $last_contacted_details['contact_medium']; ?></td>
                    <td class="kt-font-bold kt-align-center"><?php echo $last_contacted_details['contact_comment']; ?></td>
                </tr>
            </tbody>
            <?php } ?>
        <?php }else{ ?>
            <tbody>
                <tr>
                    <td class="kt-font-bold"></td>
                    <td class="kt-font-bold">No Data Found!!!</td>
                    <td class="kt-font-bold"></td>
                    <td class="kt-font-bold"></td>
                    <td class="kt-font-bold"></td>
                    <td class="kt-font-bold"></td>
                    <td class="kt-font-bold"></td>
                    <td class="kt-font-bold kt-align-center"></td>
                </tr>
            </tbody>
        <?php } ?>    
    <?php }else if($call_type == 'daily_work_quotation_table_body'){ ?>
        <thead>
            <tr>
                <th style="width: 5% !important;">Sr.no</th>
                <th style="width: 15% !important;">Quotation No#</th>
                <th style="width: 15% !important;">Company Name</th>
                <th style="width: 10% !important;">Country</th>
                <th style="width: 10% !important;">Value</th>
                <th style="width: 10% !important;">Date</th>
                <th style="width: 10% !important;">Member</th>
                <th style="width: 10% !important;">Medium</th>
                <th class="kt-align-center" style="width: 25% !important;">Comment</th> 
            </tr>
        </thead>
        <?php if(!empty($quotation_report)){?>
        <tbody>
            <?php 
                foreach ($quotation_report as $quotation_report_key => $single_quotation_report_details) {
            ?>
            <tr>
                <td class="kt-font-bold"><?php echo $quotation_report_key+1;?></td>
                <td class="kt-font-bold">
                    <?php if(!empty($single_quotation_report_details['purchase_order'])){?>
                    <a href="<?php echo base_url('assets/purchase_orders/'), $single_quotation_report_details['purchase_order']; ?>" target="_blank">
                        <?php echo $single_quotation_report_details['quotation_no'];?>
                    </a>
                    <?php }else{ 
                        echo $single_quotation_report_details['quotation_no'];
                    }?>

                </td>
                <td class="kt-font-bold"><?php echo $single_quotation_report_details['company_name'];?></td>
                <td class="kt-font-bold"><?php echo $single_quotation_report_details['country'];?></td>
                <td class="kt-font-bold"><?php echo $single_quotation_report_details['value'];?></td>
                <td class="kt-font-bold"><?php echo $single_quotation_report_details['date'];?></td>
                <td class="kt-font-bold"><?php echo $single_quotation_report_details['member_name'];?></td>
                <td class="kt-font-bold"><?php echo $single_quotation_report_details['connect_mode'];?></td>
                <td class="kt-font-bold kt-align-center"><?php echo $single_quotation_report_details['follow_up_text'];?></td> 
            </tr>
            <?php } ?>
        </tbody>
        <?php }else{ ?>
            <tbody>
                <tr>
                    <td class="kt-font-bold"></td>
                    <td class="kt-font-bold">No Data Found!!!</td>
                    <td class="kt-font-bold"></td>
                    <td class="kt-font-bold"></td>
                    <td class="kt-font-bold"></td>
                    <td class="kt-font-bold"></td>
                    <td class="kt-font-bold"></td>
                    <td class="kt-font-bold"></td>
                    <td class="kt-font-bold kt-align-center"></td>
                </tr>
            </tbody>
        <?php } ?>
    <?php } ?>
<?php } ?>

