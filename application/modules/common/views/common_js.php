jQuery(document).ready(function() {
	$('div#kt_footer').css('display','none');
    $('.kt-selectpicker').selectpicker();

    <?php if($this->uri->segment('2', '') == "user_assign_data") { ?>
        <?php if($this->session->userdata('role') == 1) { ?>
                ajax_call_function({ call_type: 'sales_lead_count_highchart' }, 'sales_lead_count_highchart');
        <?php } if(in_array(3, $this -> session -> userdata('graph_data_access'))) { ?>
                ajax_call_function({ call_type: 'sales_lead_count_highchart' }, 'sales_lead_count_highchart');
        <?php } ?>
    <?php } ?>

    $('a.get_lead_information').click(function(){
    	$('a.get_lead_information').removeClass('kt-widget__item--active');
    	$(this).addClass('kt-widget__item--active');
    	ajax_call_function({call_type: 'get_lead_information', user_id: "<?php echo $this->uri->segment(3);?>", lead_name: $(this).attr('lead_name'), table_name: $(this).attr('table_name')},'get_lead_information');
    });
    $('button.change_assigned_to').click(function(){
    	var user_id = $('select[name="user_name"]').val();
    	var lead_id = [];
    	$('input[name="lead_id[]"]:checked').each(function(res) {
		   lead_id.push(this.value);
		});
		if(user_id != '' && lead_id != []){

	    	ajax_call_function({call_type: 'assign_lead_to_other_person', user_id_old: "<?php echo $this->uri->segment(3);?>", user_id_new: user_id, lead_id: lead_id, table_name: $(this).attr('table_name'), id_column_name: $(this).attr('id_column_name'), assigned_column_name: $(this).attr('assigned_column_name')},'assign_lead_to_other_person');
		}
    });
    $('a.change_department').click(function(){

    	$('a.change_department').removeClass('btn-brand');
    	$('a.change_department').removeClass('btn-outline-brand');
    	$('a.change_department').addClass('btn-outline-brand');
    	$(this).removeClass('btn-outline-brand');
    	$(this).addClass('btn-brand');
    	ajax_call_function({call_type: 'change_assign_department', department_name: $(this).attr('department')},'change_assign_department');

    });
    $('div#local_data').on('click', 'input.checked_all', function(){

    	$('.layer-white').show();
    	setTimeout(function(){
	    	var count = 0;
	    	$('input[name="lead_id[]"]').prop('checked', false);
	    	$('tr.all_tr').css('background-color','');
	    	if($('input.checked_all:checked').is(':checked') == true){

				$('input[name="lead_id[]"]').each(function(res) {
		    		count = count+1;
				});
				$('input[name="lead_id[]"]').prop('checked', true);
	    		$('tr.all_tr').css('background-color','lightgray');
	    	}
			$('span.checkbox_checked_count').attr('count', count);
	    	$('span.checkbox_checked_count').html('('+count+')');
	    	$('.layer-white').hide();
    	}, 1000);
    });
    $('button.search_filter').click(function(){

    	ajax_call_function({call_type: 'get_lead_information', user_id: "<?php echo $this->uri->segment(3);?>", lead_name: $(this).attr('lead_name'), table_name: $(this).attr('table_name'), country: $('select[name="assign_lead_country"]').val(), last_contact: $('select[name="assign_lead_last_contact"]').val(), assign_lead_last_purchase_month: $('select[name="assign_lead_last_purchase_month"]').val(), assign_lead_last_purchase_year: $('select[name="assign_lead_last_purchase_year"]').val(), stage: $('select[name="assign_lead_stage"]').val()},'get_lead_information');
    });

    $('div#local_data').on('click', 'input.checkbox_checked', function(){

    	$('.layer-white').show();
    	var count = parseInt($('span.checkbox_checked_count').attr('count'));
    	var id = $(this).val();
    	setTimeout(function(){
			
	    	if($('input.checkbox_'+id).is(':checked') == true){

	    		$('tr.tr_'+id).css('background-color','lightgray');
	    		count = count+1;

	    	}else{

	    		$('tr.tr_'+id).css('background-color','');
	    		count = count-1;
	    	}
	    	$('span.checkbox_checked_count').attr('count', count);
	    	$('span.checkbox_checked_count').html('('+count+')');
			$('.layer-white').hide();

    	}, 1000);
    });

    $('a.get_sales_person_daily_report').click(function(){

    	ajax_call_function({call_type: 'daily_report_data_for_sales_person', daily_report_type: get_daily_report_type(), sales_person_id: $(this).attr('user_id'), filter_date: $('#kt_dashboard_daterangepicker_date').html()},'daily_report_data_for_sales_person');
    });
    KTDateRange.init();
    $('button.daily_report_search_filter').click(function(){

    	ajax_call_function({call_type: 'daily_report_data_for_sales_person', daily_report_type: get_daily_report_type(), sales_person_id: $(this).attr('user_id'), filter_date: $('#kt_dashboard_daterangepicker_date').html(), connect_mode: $('select[name="connect_mode"]').val()},'daily_report_data_for_sales_person');
    });
    $('span.daily_report').click(function(){

		$('span.daily_report').removeClass('daily_report_active');
    	ajax_call_function({call_type: 'daily_report_data_for_sales_person', daily_report_type: $(this).attr('report_type'), sales_person_id: $('button.daily_report_search_filter').attr('user_id'), filter_date: $('#kt_dashboard_daterangepicker_date').html(), connect_mode: $('select[name="connect_mode"]').val()},'daily_report_data_for_sales_person');
    	$(this).addClass('daily_report_active');
    });
	$('div#sales_person_daily_report_listing').on('click', 'a.get_data', function(){
		
		ajax_call_function({call_type: 'daily_report_listing_data_for_sales_person', user_id: $(this).attr('user_id'), image_name: $(this).attr('image_name'), current_key: $('input#current_user_id_key').val(), filter_date: $('span#sales_person_daily_report_daterangepicker_date').html() },'daily_report_listing_data_for_sales_person');
	});

    $('button.add_sales_person_protocol').click(function(){

        if(!$(this).hasClass('kt-spinner')) {

            $('form#sales_person_protocol_form').html('');
            set_reset_spinner($('button.add_sales_person_protocol'));
            ajax_call_function({call_type: 'add_sales_person_protocol'},'add_sales_person_protocol');
        }
    });

    $('a.edit_sales_person_protocol').click(function(){

        if(!$(this).hasClass('kt-spinner')) {

            $('form#sales_person_protocol_form').html('');
            set_reset_spinner($('a.edit_sales_person_protocol'));
            ajax_call_function({call_type: 'add_sales_person_protocol', user_id: $(this).attr('user_id')},'add_sales_person_protocol');
        }
    });

    $('button.save_sales_person_protocol').click(function(){

        if(!$(this).hasClass('kt-spinner')) {

            $('span.user_id_error').hide();        
            $('span.call_connected_error').hide();
            $('span.email_error').hide();
            // $('span.desktrack_error').hide();
            // console.log('email:-> ', $('input[name="emails"]').val());
            // console.log('Desktrack:-> ', $('input[name="desktrack_hours"]').val());
            // console.log('Desktrack:-> ', $('input[name="desktrack_minutes"]').val());
            if($('select.sales_person_select_picker').val()== ''){

                $('span.user_id_error').show();
                return false;
            }else if($('input[name="call_connected"]').val() == ''){

                $('span.call_connected_error').show();
                return false;
            }else if($('input[name="emails"]').val() == ''){

                $('span.email_error').show();
                return false;
            }else if($('input[name="desktrack_hours"]').val() == '' || $('input[name="desktrack_minutes"]').val() == ''){
                
                $('span.desktrack_error').show();
                return false;
            }else{
                set_reset_spinner($('button.save_sales_person_protocol'));
                ajax_call_function({call_type: 'save_sales_person_protocol', form_data: $('form#sales_person_protocol_form').serializeArray(), id: $(this).attr('id')},'save_sales_person_protocol');
            }
        }
    });

    var controller_name = "<?php echo $this->uri->segment(2, 0);?>";
	if(controller_name == 'view_user_data') {

		ajax_call_function({call_type: 'get_lead_information', user_id: "<?php echo $this->uri->segment(3, 0);?>", lead_name: "<?php echo $this->uri->segment(4, '');?>", table_name: "<?php echo $this->uri->segment(5, '');?>"},'get_lead_information');
	}else if(controller_name == 'daily_report_all'){

		var current_key = $('input#current_user_id_key').val();
		if(parseInt(current_key) > 0){

			$("a."+current_key).trigger("click");
		}
        
	}

    $('button.add_search_filter').click(function () {
        ajax_call_function(
            {
                call_type: 'sales_lead_count_highchart', lead_stage: $('select#lead_stage').val(),
                lead_type: $('select#lead_type').val(), lead_source: $('select#source').val(),
                lead_country: $('select#lead_country').val(), lead_region: $('select#lead_region').val()
            }, 'sales_lead_count_highchart');
    });

    $('a.edit_sales_person_routine').click(function () {

        var user_id = $(this).attr('user_id');
        if(typeof user_id === 'undefined'){

            swal('something went wrong. Please contact Developer Team');
            return false;
        }
        if(!$(this).hasClass('kt-spinner')){

            $('form#sales_person_routine_form').html('');
            set_reset_spinner($('a.edit_sales_person_routine'));
            ajax_call_function({ call_type: 'get_sales_person_routine', user_id: user_id }, 'get_sales_person_routine');
        }
    });

    $('form#sales_person_routine_form').on('change', '.routine_user_id', function () {

        var routine_id = $(this).val();
        var user_id = $(this).attr('user_id')
        if(typeof routine_id === 'undefined' || typeof user_id === 'undefined'){

            swal('something went wrong. Please contact Developer Team');
            return false;
        }
        ajax_call_function({
            call_type: 'update_sales_person_routine',
            user_id: user_id,
            routine_id: routine_id,
        }, 'update_sales_person_routine');
    });

    $('button.add_manpower').click(function(){
        var manpower_id = $(this).val();
        // alert(manpower_id);
        if(manpower_id == 0){

            var call_type = 'add_manpower_form';
        }else{
           var call_type = 'update_manpower_form';
        }
        // console.log(call_type);return false

        // alert(manpower_id);
        ajax_call_function({
            call_type: call_type,
            manpower_id:manpower_id,
            manpower_form : $('form#manpower_details_form').serializeArray(),
        }, 'add_manpower_form');
    });

    $('#department').change(function(){
		ajax_call_function({
            call_type: 'get_department_details',
            department_id: $(this).val()
        }, 'get_department_details');
	});

    if($('.radio').is(':checked') == true)
    {
        $(".input_type_name").show();
    }

    $('input[type="radio"]').click(function(){
        if ($(this).attr("value") == "Old" || $(this).attr("value") == "Replacement") {
            $(".input_type_name").show();
        }else if($(this).attr("value") == "New" || $(this).attr("value") == "Confidential"){
            $(".input_type_name").hide();
        }
    })
    
    $('div#mrf_table_data').on('click','a.delete_mrf_data',function(){
		// alert('hello')
        swal({
            title: "Are you sure?",
            icon: "warning",
            buttons:  ["Cancel", "remove MRF"],
            dangerMode: true, 
        })
        .then((willDelete) => {
            if (willDelete) {
                ajax_call_function({call_type:'delete_manpower_list_data',id:$(this).attr('value')},'delete_manpower_list_data');
                swal({
                    title: "removed MRF successfully",
                    icon: "success",
                });
            } else {
                swal({
                    title: "removed MRF failed ",
                    icon: "info",
                });
            }
        });
	});

    $('a.reassign').click(function(){
        ajax_call_function({
            call_type: 'reassign_mrf_form',
            id: $(this).attr('id'),
            user_id: $(this).attr('value'),
        }, 'reassign_mrf_form');
    });

    $('a.change_stage').click(function(){

        ajax_call_function({
            call_type: 'change_stage_mrf_form',
            id: $(this).attr('id'),
            stage: $(this).attr('value'),
        }, 'change_stage_mrf_form');
    });

    $('a.clone_mrf_position').click(function(){

        ajax_call_function({
            call_type: 'clone_mrf_position',
            id: $(this).attr('id'),
        }, 'clone_mrf_position');
    });

    $('button.submit_candidate').click(function(){

        var candidate_name = $('#candidate_name').val();
        var candidate_email = $('#candidate_email').val();
        var candidate_number = $('#candidate_number').val();
        var candidate_source = $('#candidate_source').val();
        var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

        var status = false;
        if (candidate_name == "") {

            $("#name_error").html("<span class='text-denger'>*Please Enter Candidate Name*</span>");
            status = false;
        } else {
            $("#name_error").html("");
            status = true;
        }

        var status_1 = false;
        if (candidate_email == "") {

            $("#email_error").html("<span class='text-denger'>*Please Enter Valid Email*</span>");
            status_1 = false;
        } else {
            if ((candidate_email.match(validRegex))){

                $("#email_error").html("");
                status_1 = true;
            }else{
                $("#email_error").html("<span class='text-denger'>*Please Enter Valid Email*</span>");
                status_1 = false;
            }
        }

        var status_2 = false;
        if (candidate_number == "") {

            $("#number_error").html("<span class='text-denger'>*Please Enter Valid Number*</span>");
            status_2 = false;
        } else if (candidate_number.length !== 10) {

            $("#number_error").html("<span class='text-denger'>*Must be 10 Digits*</span>");
            status_2 = false;

        } else if (!($.isNumeric(candidate_number))) {

            $("#number_error").html("<span class='text-denger'>*this field cannot contain letters*</span>");
            status_2 = false;
        } else {
            $("#number_error").html("");
            status_2 = true;
        }

        var status_3 = false;
        if (candidate_source == "") {

            $("#source_error").html("<span class='text-denger'>*Please Select Valid Source*</span>");
            status_3 = false;
        } else {
            $("#source_error").html("");
            status_3 = true;
        }

        if ((status == true) && (status_1 == true) && (status_2 == true) && (status_3 == true)) {

            var candidate_id = $(this).val();
            var mrf_id = $(this).attr('mrf_id');
            var hr_user_id = $(this).attr('hr_user_id');

            if (candidate_id == '') {

                var call_type = 'add_candidate_data';
            } else {
                var call_type = 'update_candidate_data';
            }

            ajax_call_function({
                call_type: call_type,
                mrf_id: mrf_id,
                hr_user_id: hr_user_id,
                candidate_id:candidate_id,
                candidate_form : $('form#candidate_form').serializeArray()
            }, 'add_candidate_data');
        }
    });

    $('a.delete_candidate').click(function(){
		// alert('hello')
        swal({
            title: "Are you sure?",
            icon: "warning",
            buttons:  ["Cancel", "remove candidate"],
            dangerMode: true, 
          })
          .then((willDelete) => {
              if (willDelete) {
                  ajax_call_function({call_type:'delete_candidate_data',candidate_id:$(this).attr('value')},'delete_candidate_data');
                  swal({
                      title: "removed exclude successfully",
                      icon: "success",
                  });
              } else {
                  swal({
                      title: "removed exclude failed ",
                      icon: "info",
                  });
              }
          });
	});

    $('li.tab_name_click').click(function () {

        var tab_name = $(this).attr('tab-name');
        if (!$('li.' + tab_name).hasClass('active_list')) {

            $('li.active_list').removeClass('active_list');
            $(this).addClass('active_list');
            $('input#limit').val(10);
            $('input#offset').val(0);
            get_mrf_listing_data();
        }
    });

    $('div#mrf_list_paggination').on('click', '.paggination_number', function(){

        get_mrf_listing_data("", [], $(this).attr('limit'), $(this).attr('offset'));
    });
    $('div#mrf_list_paggination').on('change', 'select#set_limit', function(){

        get_mrf_listing_data("", [], $('select#set_limit').val(), 0);
    });

    $('a.add_search_filter').click(function () {

        var button_value = $(this).attr('button_value');
        if (button_value == 'show') {
            ajax_call_function({call_type: 'get_search_filter_data'}, 'get_search_filter_data');
            $('div.search_filter_data').slideDown();
            $(this).attr('button_value', 'hide');
            $(this).html('Hide Search Filter');
        } else {
            $('div.search_filter_data').slideUp();
            $(this).attr('button_value', 'show');
            $(this).html('Add Search Filter');
        }
    });

    $('div.search_filter_data').on('click', 'button.mrf_search_filter_submit', function () {

        get_mrf_listing_data();
		// ajax_call_function(get_mrf_listing_data(), 'get_mrf_listing_data');

    });

    $('div.search_filter_data').on('click', 'button.search_filter_reset', function () {

        // get_mrf_listing_data("", "", [] );
        location.reload();
    });

    $('tbody#candidate_list_body').on('click', 'a.stage_modal', function () {

        ajax_call_function({
            call_type: 'get_candidate_stage',
            candidate_id: $(this).attr('candidate_id'),
            stage_id: $(this).attr('stage_id'),
            stage_reason_id: $(this).attr('stage_reason_id'),
            other_reason: $(this).attr('other_reason'),
        }, 'get_candidate_stage');
    });

    $('form#update_stage_form').on('change', 'select.stage_id', function () {

        $('div.candidate_stage_reason').hide();
        var val = $(this).val();
        if (val == 7) {
            $('div.candidate_stage_reason').show();
        }
    });

    $('form#update_stage_form').on('change', 'select.stage_reason_id', function () {

        $('div.other_stage_reason').hide();
        var val = $(this).val();
        if (val == 4) {
            $('div.other_stage_reason').show();
        }
    });

    $('div#update_stage').on('click', 'button.save_stage', function () {

        ajax_call_function({
                call_type: 'save_candidate_stage',
                candidate_id: $(this).val(),
                update_stage_form: $('form#update_stage_form').serializeArray()
            }, 'save_candidate_stage');
    });

    $("tbody#candidate_list_body").on('click', "a.get_candidate_comment", function () {

        var candidate_id = $(this).attr('candidate_id');
        // console.log(rfq_mst_id);
        if (candidate_id > 0) {

            $('div.div_comment_history').html('');
            $('div.candidate_comment_loader').show();
            ajax_call_function({ call_type: 'get_candidate_comment', candidate_id: candidate_id }, 'get_candidate_comment');
        }
    });

    $("button.save_candidate_comment").click(function () {

        var candidate_id = $(this).attr('candidate_id_for_comment');
        console.log(candidate_id);
        if (candidate_id > 0) {
            ajax_call_function({ call_type: 'save_candidate_comment', candidate_id: candidate_id, message: $('#textarea_id_for_comment').val() }, 'save_candidate_comment');
        }
    });

    $('div#candidate_list_paggination').on('click', 'li.paggination_number', function () {

        ajax_call_function({
            call_type: 'paggination_filter',
            mrf_id: <?php echo $this->uri->segment('3', 0);?>,
            search_form_data : $('form#search_filter_form').serializeArray(),
            limit: $(this).attr('limit'),
            offset: $(this).attr('offset'),
        }, 'paggination_filter');
    });
    $('div#candidate_list_paggination').on('change', 'select.limit_change', function () {

        ajax_call_function({
            call_type: 'paggination_filter',
            mrf_id: <?php echo $this->uri->segment('3', 0);?>,
            search_form_data : $('form#search_filter_form').serializeArray(),
            limit: $('select.limit_change').val(),
            offset: 0,
        }, 'paggination_filter');
    });


    $('a.add_candidate_search_filter').click(function () {

        var button_value = $(this).attr('button_value');
        if (button_value == 'show') {
            ajax_call_function({call_type: 'get_candidate_search_filter_data', mrf_id: <?php echo $this->uri->segment('3', 0);?>}, 'get_candidate_search_filter_data');
            $('div.search_filter_data').slideDown();
            $(this).attr('button_value', 'hide');
            $(this).html('Hide Search Filter');
        }else{
            $('div.search_filter_data').slideUp();
            $(this).attr('button_value', 'show');
            $(this).html('Add Search Filter');
        }
    });

    $('div.search_filter_data').on('click', 'button.candidate_search_filter_submit', function () {
        // ajax_call_function(get_mrf_listing_data(), 'get_mrf_listing_data');
        var mrf_id = <?php echo $this->uri->segment('3', 0); ?>;
        ajax_call_function({
            call_type: 'candidate_search_filter',
            mrf_id: mrf_id,
            search_form_data : $('form#search_filter_form').serializeArray(),
            limit : 10,
            offset : 0
        }, 'candidate_search_filter');
    });

    $('div.search_filter_data').on('click', 'button.candidate_search_filter_reset', function () {
        // candidate_search_filter("", "", [] );
        location.reload();
    });

    $('button.add_candidate_evaluation_form').click(function () {

        var evaluation_id = $(this).val();
        var candidate_id = $(this).attr('candidate_id');
        var candidate_mrf_id = $(this).attr('mrf_id');

        // alert(candidate_mrf_id);

        if(evaluation_id == '') {
            var call_type = 'add_candidate_evaluation';
        }else{
            var call_type = 'update_candidate_evaluation';
        }
        ajax_call_function({
            call_type: call_type,
            evaluation_id: evaluation_id,
            candidate_id: candidate_id,
            candidate_mrf_id: candidate_mrf_id,
            candidate_evaluation_form: $('form#candidate_evaluation_form').serializeArray()
        }, 'add_candidate_evaluation');
    });

    $('tbody#candidate_list_body').on('click', 'a.mrf_modal', function () {

        ajax_call_function({
            call_type: 'get_candidate_mrf_id',
            candidate_id: $(this).attr('candidate_id'),
            mrf_id: $(this).attr('mrf_id'),
        }, 'get_candidate_mrf_id');
    });

    $('div#update_mrf').on('click', 'button.save_mrf', function () {

        ajax_call_function({
                call_type: 'save_candidate_mrf',
                candidate_id: $(this).val(),
                update_mrf_form: $('form#update_mrf_form').serializeArray()
            }, 'save_candidate_mrf');
    });

});

function ajax_call_function(data, callType, url = "<?php echo base_url('common/ajax_function'); ?>") {

	$.ajax({
		type: 'POST',
		data: data,
		url: url,
		dataType: 'JSON',
		success: function(res){
			if(res.status == 'successful') {
				switch (callType) {
					case 'get_lead_information':

                        // console.log(res);
						if(res.table_html != '') {

							$('div#local_data').html('').html(res.table_html);
						}
						if(res.table_name != '' && res.id_column_name != '' && res.assigned_column_name != '') {						
							$('button.change_assigned_to').attr('table_name',res.table_name);
							$('button.change_assigned_to').attr('id_column_name',res.id_column_name);
							$('button.change_assigned_to').attr('assigned_column_name',res.assigned_column_name);
							$('button.search_filter').attr('table_name',res.table_name);
							$('button.search_filter').attr('lead_name',res.lead_name);
							$('.show_assign_user').show();
						}
						$('a.get_lead_information').removeClass('kt-widget__item--active');
    					$('a[lead_name="'+decodeURI(data.lead_name)+'"]').addClass('kt-widget__item--active');
						$('.layer-white').hide();
					break;
					case 'assign_lead_to_other_person':
						toastr.success('Data is assign successfully!');
					break;

					case 'change_assign_department':
						$('div#local_data').html('').html(res.table_html);
					break;

					case 'daily_report_data_for_sales_person':
						$('table#daily_report_table').html('').html(res.table_html);
						$('a.get_sales_person_daily_report').removeClass('kt-widget__item--active');
						$('a[user_id="'+decodeURI(data.sales_person_id)+'"]').addClass('kt-widget__item--active');
						$('div#daily_report_count').html('').html(res.count_html);
						$('button.daily_report_search_filter').attr('user_id', data.sales_person_id);
						$('.layer-white').hide();		
					break;
					
					case 'daily_report_listing_data_for_sales_person':
						$('div#sales_person_daily_report_listing_single_'+data.user_id).html(res.table_html);
						$('.layer-white_'+data.user_id).hide();
						var increment_value = parseInt(data.current_key)+1;
						$('input#current_user_id_key').val(increment_value);
						var current_key = $('input#current_user_id_key').val();
						if(parseInt(current_key) > 0){

							$("a."+current_key).trigger("click");
						}
					break;

                    case 'add_sales_person_protocol':
                        
                        set_reset_spinner($('button.add_sales_person_protocol'), false);
                        $('form#sales_person_protocol_form').html('').html(res.modal_body);
                        $('div#sales_person_protocol_modal').modal('show');
                        $('.sales_person_select_picker').selectpicker();
                        $('button.save_sales_person_protocol').attr('id', res.id);
                    break;

                    case 'save_sales_person_protocol':
                        swal('Sales person protocol is saved');
                        setTimeout(function(){
                            location.reload();
                        }, 600);
                    break;

                    case 'sales_lead_count_highchart':
                        sales_lead_count_highchart(res.sales_lead_count_highchart);
                        $('div.select_sales_person_dropdown').html('').html(res.html_body.filter_sales_person);
                    break;

                    case 'get_sales_person_routine':
                        if(res.modal_body != ''){

                            $('form#sales_person_routine_form').html('').html(res.modal_body);
                            $('a#kt_sales_routine_daterangepicker').attr('user_id', data.user_id);
                            $('div#sales_person_routine_modal').modal('show');
                        }
                        set_reset_spinner($('a.edit_sales_person_routine'), false);
                        
                    break;

                    case 'update_sales_person_routine':
                        swal('Sales person routine is saved');
                        setTimeout(function () {
                            
                            $('form#sales_person_routine_form').html('');
                            ajax_call_function({ call_type: 'get_sales_person_routine', user_id: data.user_id }, 'get_sales_person_routine');
                        }, 1000);
                    break;

                    case 'add_manpower_form':
                    case 'update_manpower_form':

                        location.href = "<?php echo base_url('common/mrf_list');?>";
                    break;

                    case 'delete_manpower_list_data':
                        // location.reload();
                        location.href = "<?php echo base_url('common/mrf_list'); ?>";
                    break;

                    case 'get_department_details':
                        $('.kt-selectpicker').selectpicker('destroy');
                        $('select.job_title').html('').html(res.job_title_option_tag);
                        $('select.reporting_to').html('').html(res.reporting_to_option_tag);
                        $('select[name="type_of_name"]').html('').html(res.all_type_of_name_option_tag);
                        $('.kt-selectpicker').selectpicker();
                    break;

                    case 'reassign_mrf_form':
                    case 'change_stage_mrf_form':
                        
                        toastr.success('Changes Saved Successfully!');
                        get_mrf_listing_data();
                    break;

                    case 'add_candidate_data':
                    case 'update_candidate_data':

                        if(res.message == 'Candidate Successfully Added') {
                            swal({
                                title: res.message,
                                icon: "success",
                            });
                            setTimeout(function () {

                                location.href = "<?php echo base_url('common/candidate_list/'); ?>" + data.mrf_id;
                            }, 1000);
                        }else{
                            swal({
                                title: res.message,
                                icon: "warning",
                            });
                            setTimeout(function () {

                                location.reload();
                            }, 3000);
                        }
                    break;

                    case 'delete_candidate_data':
                        location.href = "<?php echo base_url('common/candidate_list'); ?>";
                    break;

                    case 'get_mrf_listing_data':

                        $('tbody#mrf_list_body').html('').html(res.list_body);
                        $('div.search_filter_data').html('').html(res.search_filter);
                        $('div#mrf_list_paggination').html('').html(res.mrf_list_paggination);
                        $('select.kt-selectpicker').selectpicker();
                        KTDateRange.init();
                        dropdown_set_click_event();
                        $('div#mrf_list_process').hide();
                    break;

                    case 'get_search_filter_data':
                        $('div.search_filter_data').html('').html(res.search_filter);
                        $('select.kt-selectpicker').selectpicker();
                        KTDateRange.init();
                    break;
                    case 'get_candidate_stage':
                        $('form#update_stage_form').html('').html(res.update_stage_html);
                        $('button[id=candidate_id]').val(res.candidate_id);
                    break;

                    case 'save_candidate_stage':
                        swal({
                            title: "Candidate Stage updated",
                            icon: "success",
                        });
                        setTimeout(function () {

                            location.reload();
                        }, 1000);
                    break;

                    case 'get_candidate_comment':

                        $('div#kt_chat_modal').modal('show');
                        $('em.header_1').html('').html('Candicate Name :');
                        $('em.header_2').html('').html(res.name);
                        $('div.div_comment_history').html('').html(res.comment_history);
                        $('#textarea_id_for_comment').val('');
                        autosize($('#textarea_id_for_comment'));
                        $('button.save_candidate_comment').attr('candidate_id_for_comment', data.candidate_id);
                        $('form#candidate_comment_add_form').show();
                        $('.kt-scroll').scrollTop($('.kt-scroll')[0].scrollHeight);
                        $('div.candidate_comment_loader').hide();
                    break;

                    case 'save_candidate_comment':

                        toastr.success('Comment Added Successfully!');
                        ajax_call_function({ call_type: 'get_candidate_comment', candidate_id: data.candidate_id }, 'get_candidate_comment');
                        $('#textarea_id_for_comment').val('');
                    break;

                    case 'add_candidate_evaluation':
                    case 'update_candidate_evaluation':

                        swal({
                            title: 'Candidate Evaluation Successfully Added',
                            icon: "success",
                        });
                        setTimeout(function () {

                            location.href = "<?php echo base_url('common/candidate_list/'); ?>" + data.candidate_mrf_id;
                        }, 1000);
                    break;

                    case 'candidate_search_filter':
                    case 'paggination_filter':

                        $('tbody#candidate_list_body').html('').html(res.list_body);
                        $('div.search_filter_data').html('').html(res.search_filter);
                        $('div#candidate_list_paggination').html('').html(res.paggination);
                        $('div#candidate_list_process').hide();
                        $('.kt-selectpicker').selectpicker();
                    break;

                    case 'get_candidate_search_filter_data':
                        $('div.search_filter_data').html('').html(res.search_filter);
                        $('.kt-selectpicker').selectpicker();
                        KTDateRange.init();
                    break;

                    case 'get_candidate_mrf_id':
                        $('form#update_mrf_form').html('').html(res.change_mrf);
                        $('button[id=candidate_id]').val(res.candidate_id);
                    break;

                    case 'save_candidate_mrf':
                        swal({
                            title: "Candidate MRF Updated",
                            icon: "success",
                        });
                        setTimeout(function () {

                            location.reload();
                        }, 1000);
                    break;

					default:
					
					break;	
				}
			}
		},
		beforeSend: function(response){
			switch (callType) {
				case 'get_lead_information':
				case 'daily_report_data_for_sales_person':
					$('.layer-white').show();
				break;
				case 'daily_report_listing_data_for_sales_person':
					$('.layer-white_'+data.user_id).show();
					$('div#sales_person_daily_report_listing_single_'+data.user_id).html('');
				break;
				default:
				
				break;	
			}
		}
	});
};


function get_mrf_listing_data(
    limit = $('input#limit').val(),
    offset = $('input#offset').val(),
){
    data = {
        call_type: 'get_mrf_listing_data',
        search_form_data: $('form#search_filter_form').serializeArray(),
        limit: limit,
        offset: offset,
        tab_name: $('li.active_list').attr('tab-name'),
    }
    ajax_call_function(
        data,
        'get_mrf_listing_data'
    );
}

function set_reset_spinner(obj, set_unset_flag = true, spinner_color = 'kt-spinner--light') {

	if(set_unset_flag) {

		$(obj).addClass('kt-spinner');
    	$(obj).addClass('kt-spinner--right');
    	$(obj).addClass('kt-spinner--sm');
    	$(obj).addClass(spinner_color);	
	} else{

		$(obj).removeClass('kt-spinner');
    	$(obj).removeClass('kt-spinner--right');
    	$(obj).removeClass('kt-spinner--sm');
    	$(obj).removeClass(spinner_color);
	}
}

var KTDateRange = function () {
    
    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }
    // Private functions
    var daterangepickerInit = function() {
        if ($('#kt_dashboard_daterangepicker').length == 0) {
            return;
        }

        var picker = $('#kt_dashboard_daterangepicker');
        var start = moment().subtract(1, 'days');
        var end = moment().subtract(1, 'days');

        function cb(start, end, label) {
            var title = '';
            var range = '';
            if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D Y');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D Y');
            } else {
                range = start.format('MMM D Y') + ' - ' + end.format('MMM D Y');
            }

            $('#kt_dashboard_daterangepicker_date').html(range);
            $('#kt_dashboard_daterangepicker_title').html(title);
			ajax_call_function({call_type: 'daily_report_data_for_sales_person', daily_report_type: get_daily_report_type(), sales_person_id: $('button.daily_report_search_filter').attr('user_id'), filter_date: range, connect_mode: $('select[name="connect_mode"]').val()},'daily_report_data_for_sales_person');
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end, 'Yesterday');
    }

    var salespersondailyreportdaterangepickerInit = function() {
        if ($('#sales_person_daily_report_daterangepicker').length == 0) {
            return;
        }

        var picker = $('#sales_person_daily_report_daterangepicker');
        var start = moment();
        var end = moment();

        function cb(start, end, label) {
            var title = '';
            var range = '';
            if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D Y');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D Y');
            } else {
                range = start.format('MMM D Y') + ' - ' + end.format('MMM D Y');
            }

            $('#sales_person_daily_report_daterangepicker_date').html(range);
            $('#sales_person_daily_report_daterangepicker_title').html(title);
            $('input#current_user_id_key').val(1);
            var current_key = $('input#current_user_id_key').val();
			if(parseInt(current_key) > 0){
				$("a."+parseInt(current_key)).trigger("click");
			}
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end, 'Today');
    }
    var sales_routine_daterangepickerInit = function() {
        if ($('#kt_sales_routine_daterangepicker').length == 0) {
            return;
        }

        var picker = $('#kt_sales_routine_daterangepicker');
        var start = moment();
        var end = moment();

        function cb(start, end, label) {
            var title = '';
            var range = '';
            if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D Y');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D Y');
            } else {
                range = start.format('MMM D Y') + ' - ' + end.format('MMM D Y');
            }

            $('#kt_sales_routine_daterangepicker_date').html(range);
            $('#kt_sales_routine_daterangepicker_title').html(title);
            $('form#sales_person_routine_form').html('');
            set_reset_spinner($('a.edit_sales_person_routine'));
            ajax_call_function({ call_type: 'get_sales_person_routine', user_id: $('a#kt_sales_routine_daterangepicker').attr('user_id'), filter_date: range}, 'get_sales_person_routine');
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            }
        }, cb);

        cb(start, end, 'Today');
    }

    var mrf_date_picker = function(){

        $('.mrf_date_picker').datepicker({
            rtl: KTUtil.isRTL(),
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: false,
            templates: arrows,
            format: 'yyyy-mm-dd',
        });
    }

    var candidate_resume = function () {

        // set the dropzone container id
        var id = '#resume_dropzone';

        // set the preview element template
        var previewNode = $(id + " .dropzone-item");
        previewNode.id = "";
        var previewTemplate = previewNode.parent('.dropzone-items').html();
        previewNode.remove();


        var candidate_cv = new Dropzone(id, { // Make the whole body a dropzone
            url: "<?php echo base_url('common/ajax_function');?>", // Set the url for your upload script location
            paramName: "file", // The name that will be used to transfer the file
            params: { 'call_type': 'upload_candidate_resume', 'resume': 'candidate_resume' },
            maxFiles: 1,
            maxFilesize: 2, // Max filesize in MB
            previewTemplate: previewTemplate,
            previewsContainer: id + " .dropzone-items", // Define the container to display the previews
            acceptedFiles: ".pdf,.docx,.jpeg,.jpg,.png",
            clickable: id + " .dropzone-select" // Define the element that should be used as click trigger to select files.
        });

        candidate_cv.on("addedfile", function (file) {
            // Hookup the start button
            $(document).find(id + ' .dropzone-item').css('display', '');
        });

        // Update the total progress bar
        candidate_cv.on("totaluploadprogress", function (progress) {
            $(id + " .progress-bar").css('width', progress + "%");
        });

        candidate_cv.on("sending", function (file) {
            // Show the total progress bar when upload starts
            $(id + " .progress-bar").css('opacity', "1");
        });

        // Hide the total progress bar when nothing's uploading anymore
        candidate_cv.on("complete", function (progress) {
            var thisProgressBar = id + " .dz-complete";
            setTimeout(function () {
                $(thisProgressBar + " .progress-bar, " + thisProgressBar + " .progress").css('opacity', '0');
            }, 300);
            swal({
                title: "File is Uploaded Successfully",
                icon: "success",
            });
        });
    }

    var profile_pic = function () {
        // set the dropzone container id
        var id = '#candidate_photo';
        // set the preview element template
        var previewNode = $(id + " .dropzone-item");
        previewNode.id = "";
        var previewTemplate = previewNode.parent('.dropzone-items').html();
        previewNode.remove();
        var profile_pic = new Dropzone(id, { // Make the whole body a dropzone
            url: "<?php echo base_url('common/ajax_function');?>", // Set the url for your upload script location
            paramName: "file", // The name that will be used to transfer the file
            params: { 'call_type': 'upload_candidate_photo', 'candidate_id': "<?php echo $this->uri->segment('3', 0)?>" },
            maxFiles: 1,
            maxFilesize: 2, // Max filesize in MB
            previewTemplate: previewTemplate,
            previewsContainer: id + " .dropzone-items", // Define the container to display the previews
            acceptedFiles: ".jpeg, .jpg, .png",
            clickable: id + " .dropzone-select" // Define the element that should be used as click trigger to select files.
        });
        profile_pic.on("addedfile", function (file) {
            // Hookup the start button
            $(document).find(id + ' .dropzone-item').css('display', '');
        });
        // Update the total progress bar
        profile_pic.on("totaluploadprogress", function (progress) {
            $(id + " .progress-bar").css('width', progress + "%");
        });
        profile_pic.on("sending", function (file) {
            // Show the total progress bar when upload starts
            $(id + " .progress-bar").css('opacity', "1");
        });
        // Hide the total progress bar when nothing's uploading anymore
        profile_pic.on("complete", function (progress) {
            var thisProgressBar = id + " .dz-complete";
            setTimeout(function () {
                $(thisProgressBar + " .progress-bar, " + thisProgressBar + " .progress").css('opacity', '0');
            }, 300);
            swal({
                title: "File is Uploaded Successfully",
                icon: "success",
            });
        });
    }

    return {
        // public functions
        init: function() {
            daterangepickerInit();
            salespersondailyreportdaterangepickerInit();
            sales_routine_daterangepickerInit();
            mrf_date_picker();
            <?php if ($this->uri->segment('2', '') == "candidate") { ?>
            candidate_resume();
            <?php } ?>
            <?php if ($this->uri->segment('2', '') == "interview_evaluation") { ?>
                profile_pic();
            <?php } ?>
        }
    };
}();

function get_daily_report_type(){

	return $('span.daily_report_active').attr('report_type');
}

function sales_lead_count_highchart(highchart_data) {

    Highcharts.chart('sales_lead_count_highchart', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Lead distribution'
        },
        xAxis: {
            categories: highchart_data.category
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total number of leads'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: [{
            name: 'Stage - 6',
            data: highchart_data.stage_6,
            color: '#33ccff'
        }, {
            name: 'Stage - 5',
            data: highchart_data.stage_5,
            color: '#28a745'
        }, {
            name: 'Stage - 4',
            data: highchart_data.stage_4,
            color: '#007bff'
        }, {
            name: 'Stage - 3',
            data: highchart_data.stage_3,
            color: '#fd7e14'
        }, {
            name: 'Stage - 2',
            data: highchart_data.stage_2,
            color: '#ffc107'
        }, {
            name: 'Stage - 1',
            data: highchart_data.stage_1,
            color: '#969ca0'
        }, {
            name: 'Stage - 0',
            data: highchart_data.stage_0,
            color: '#ff0000'
        }, {
            name: 'Blank Stage',
            data: highchart_data.blank_stage,
            color: '#17a2b8'
        }]
    });
}

function dropdown_set_click_event(){

    const dropdownItems = document.querySelectorAll("tbody#mrf_list_body .dropdown-item.change_stage");
    dropdownItems.forEach(item => {
        item.addEventListener("click", function(event) {
            event.preventDefault();
            const stage = this.getAttribute("value");
            const id = this.getAttribute("id");

            if(typeof id !== 'undefined' && id.length !== 0 && typeof stage !== 'undefined' && stage.length !== 0){

                ajax_call_function({
                    call_type: 'change_stage_mrf_form',
                    id: $(this).attr('id'),
                    stage: $(this).attr('value'),
                }, 'change_stage_mrf_form');
            }
        });
    });
}