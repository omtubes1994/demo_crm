<?php foreach($comment_details as $comment_key => $single_user_comment_details){ ?>
    <?php if($chat_key%2 == 0){ ?>
    <div class="kt-chat__message kt-chat__message--success">
        <div class="kt-chat__user">
            <span class="kt-media kt-media--circle kt-media--sm">
                <img src="<?php echo $single_user_comment_details['profile_path']; ?>" alt="image">
            </span>
            <a href="javascript:void(0);" class="kt-chat__username"><?php echo $single_user_comment_details['user_name']; ?></span></a>
            <span class="kt-chat__datetime"><?php echo $single_user_comment_details['tat']; ?></span>
        </div>
        <div class="kt-chat__text">
            <?php echo $single_user_comment_details['message']; ?>
        </div>
    </div>
    <?php }else{ ?>
    <div class="kt-chat__message kt-chat__message--right kt-chat__message--brand">
        <div class="kt-chat__user">
            <span class="kt-chat__datetime"><?php echo $single_user_comment_details['tat']; ?></span>
            <a href="javascript:void(0);" class="kt-chat__username"><?php echo $single_user_comment_details['user_name']; ?></span></a>
            <span class="kt-media kt-media--circle kt-media--sm">
                <img src="<?php echo $single_user_comment_details['profile_path']; ?>" alt="image">
            </span>
        </div>
        <div class="kt-chat__text">
        <?php echo $single_user_comment_details['message']; ?>
        </div>
    </div>
    <?php } ?>
<?php } ?>