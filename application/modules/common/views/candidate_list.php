<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

	<style type="text/css">
		.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
			padding-top: 5% !important;
		}
		.layer-white{
			display: none;
			position: fixed;
			top: 0em !important;
			left: 0em !important;
			width: 100%;
			height: 100%;
			text-align: center;
			vertical-align: middle;
			background-color: rgba(255, 255, 255, 0.55);
			opacity: 1;
			line-height: 1;
			-webkit-animation-fill-mode: both;
			animation-fill-mode: both;
			-webkit-animation-duration: 0.5s;
			animation-duration: 0.5s;
			-webkit-transition: background-color 0.5s linear;
			transition: background-color 0.5s linear;
			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
			will-change: opacity;
			z-index: 9;
		}
		.div-loader{
			position: absolute;
			top: 50%;
			left: 50%;
			margin: 0px;
			text-align: center;
			z-index: 1000;
			-webkit-transform: translateX(-50%) translateY(-50%);
			transform: translateX(-50%) translateY(-50%);
		}
		.kt-spinner:before {
			width: 50px;
			height: 50px;
			margin-top: -10px;
		}

		.kt-badge.kt-badge--orange {
			color: #111111;
			background: #fd7e14;
		}
		.kt-badge.kt-badge--unified-orange {
			color: #fd7e14;
			background: #FFDAB9;
		}
		.kt-badge.kt-badge--yellow {
			color: #111111;
			background: #ffc107;
		}
		.candidate_font {
            font-size: 1rem;
            font-weight: 500;
            line-height: 1.5rem;
            -webkit-transition: color 0.3s ease;
            transition: color 0.3s ease;
            color: #000;
        }
	</style>

	<!-- begin:: container -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon2-line-chart"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						Candidate List
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="kt-portlet__head-actions">
							<a href="javascript:;" class="btn btn-brand btn-elevate btn-icon-sm add_candidate_search_filter" button_value = "show">
								Add Search Filter
							</a>
							<a href="<?php echo base_url('common/candidate/'.$mrf_id);?>" target="" class="btn btn-brand btn-elevate btn-icon-sm">
								Add New
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet__body search_filter_data" style="display: none;"></div>
		</div>
		<div class="kt-portlet kt-portlet--mobile" id="candidate_list">
			<div class="kt-portlet__body">
				<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
					<div class="row">
						<div class="col-sm-12">
							<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline mrf_font" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 1536px;">
								<thead id="candidate_list_header">
									<tr role="row">
										<th class="kt-align-center" style="width: 5%;">Sr.No</th>
										<th class="kt-align-center" style="width: 10%;">Name</th>
										<th class="kt-align-center" style="width: 10%;">Mobile</th>
										<th class="kt-align-center" style="width: 15%;">Email</th>
										<th class="kt-align-center" style="width: 7%;">Source</th>
										<th class="kt-align-center" style="width: 8%;">Stage</th>
										<th class="kt-align-center" style="width: 5%;">Assigned To</th>
										<th class="kt-align-center" style="width: 15%;">Comment</th>
										<th class="kt-align-center" style="width: 15%;">Evaluation Comment</th>
										<th class="kt-align-center" style="width: 10%;">Actions</th>
									</tr>
								</thead>
								<tbody id="candidate_list_body">
									<h4>
										Total :<?php echo $total_candidate_count['total_candidate'];?>,&nbsp;
										Call_connect/WhatsApp:<?php echo $whatsapp_candidate_count['whatsapp_candidate'];?>,&nbsp;&nbsp;
										Interview Scheduled: <?php echo $interview_candidate_count['interview_candidate'];?>,&nbsp;&nbsp;
										Interviewed: <?php echo $interviewed_count['Interviewed'];?>,&nbsp;&nbsp;
										Approved: <?php echo $approved_candidate_count['approve_candidate'];?>,&nbsp;&nbsp;
										Joined: <?php echo $join_candidate_count['join_candidate'];?>,&nbsp;&nbsp;
										Rejected: <?php echo $reject_candidate_count['reject_candidate'];?>,&nbsp;&nbsp;
										Interviewed & Rejected: <?php echo $interview_rejected_count['interview_rejected'];?>,
									</h4>
									<?php $this->load->view('common/candidate_list_body');?>
								</tbody>
							</table>
							<div id="candidate_list_process" class="layer-white">
								<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader">
								</div>
							</div>
						</div>
					</div>
					<div class="row" id="candidate_list_paggination">
						<?php $this->load->view('common/paggination');?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end::Container -->

	<!-- start::Model -->
    <div class="modal fade" id="update_stage" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Update Stage</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<form class="candidate_font" id="update_stage_form">

					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button class="btn btn-success save_stage" type="reset" id="candidate_id" style="float:right;"> Save </button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade- modal-sticky-bottom-right" id="kt_chat_modal" role="dialog" data-backdrop="false">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="kt-chat">
					<div class="kt-portlet kt-portlet--last">
						<div class="kt-portlet__head">
							<div class="kt-chat__head ">
								<div class="kt-chat__left">
									<div class="kt-chat__label">
										<a href="#" class="kt-chat__title">
											<em class= "header_1"></em>
											<em class= "header_2"></em>
										</a>
									</div>
								</div>
								<div class="kt-chat__right"></div>
							</div>
						</div>
						<div class="kt-portlet__body">
							<div class="kt-scroll kt-scroll--pull chat_scroll" data-height="410" data-mobile-height="225">
								<div class="layer-white candidate_comment_loader">
									<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
								</div>
								<div class="kt-chat__messages kt-chat__messages--solid div_comment_history"></div>
							</div>
						</div>
						<div class="kt-portlet__foot">
							<form  id="candidate_comment_add_form" style="display:none;">
								<div class="kt-chat__input">
									<div class="kt-chat__editor">
										<textarea class="kt-font-dark" id = "textarea_id_for_comment" placeholder="Type here..." style="height: 50px"></textarea>
									</div>
									<div class="kt-chat__toolbar">
										<div class="kt_chat__tools">
											<div class="query_type"></div>
										</div>
										<div class="kt_chat__actions">
											<button type="button" class="btn btn-brand btn-md btn-font-sm btn-upper btn-bold save_candidate_comment" candidate_id_for_comment = 0>reply</button>
											<button type="button" class="btn btn-brand btn-md btn-font-sm btn-upper btn-bold" data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="update_mrf" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Update MRF</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<form class="mrf_font" id="update_mrf_form">

					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					<button class="btn btn-success save_mrf" type="reset" id="candidate_id" style="float:right;"> Save </button>
				</div>
			</div>
		</div>
	</div>
	<!-- end::Model -->

</div>