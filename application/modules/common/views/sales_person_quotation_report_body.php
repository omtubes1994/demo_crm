<thead>
    <tr>
        <th style="width: 5% !important;">Sr.no</th>
        <th style="width: 15% !important;">Quotation No#</th>
        <th style="width: 15% !important;">Company Name</th>
        <th style="width: 10% !important;">Country</th>
        <th style="width: 10% !important;">Value</th>
        <th style="width: 10% !important;">Date</th>
        <!-- <th style="width: 10% !important;">Member</th>
        <th style="width: 10% !important;">Medium</th>-->
        <th class="kt-align-center" style="width: 25% !important;">Comment</th> 
    </tr>
</thead>
<?php if(!empty($quotation_report)){?>
<tbody>
    <?php 
        foreach ($quotation_report as $quotation_report_key => $single_quotation_report_details) {
    ?>
    <tr>
        <td class="kt-font-bold"><?php echo $quotation_report_key+1;?></td>
        <td class="kt-font-bold"><?php echo $single_quotation_report_details['quotation_no'];?></td>
        <td class="kt-font-bold"><?php echo $single_quotation_report_details['company_name'];?></td>
        <td class="kt-font-bold"><?php echo $single_quotation_report_details['country'];?></td>
        <td class="kt-font-bold"><?php echo $single_quotation_report_details['value'];?></td>
        <td class="kt-font-bold"><?php echo $single_quotation_report_details['date'];?></td>
        <!-- <td class="kt-font-bold"></td>
        <td class="kt-font-bold"></td>-->
        <td class="kt-font-bold kt-align-center"><?php echo $single_quotation_report_details['follow_up_text'];?></td> 
    </tr>
    <?php } ?>
</tbody>
<?php } ?>