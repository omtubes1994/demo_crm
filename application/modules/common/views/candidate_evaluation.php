<style type="text/css">
	.candidate_review {
		font-size: 1rem;
		font-weight: 500;
		line-height: 1.5rem;
		-webkit-transition: color 0.3s ease;
		transition: color 0.3s ease;
		color: #000;
	}
    input[type="radio"] {
        display: none;
    }
    input[type="radio"] + label{
        display: inline-block;
        height: 17px;
        width: 17px;
        border: 1px solid black;
        position: relative;
        cursor: pointer;
        border-radius: 3px;
        margin: unset;
    }
    input[type="radio"]:checked + label{
        background-color: #0075FF;
        border: 1px solid #0075FF;
    }
    input[type="radio"]:checked + label::after{
        position: absolute;
        content: "";
        height: 13px;
        width: 7px;
        border: 3px solid #fff;
        left: 1px;
        right: 0;
        top: -6px;
        bottom: 0;
        margin: auto;
        transform: rotate(45deg);
        border-top-color: transparent;
        border-left-color: transparent;
    }
</style>
<div class="col-lg-12" style="padding-left: 25px;padding-right: 25px;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Applicant Interview Evaluation Form
                </h3>
            </div>
        </div>
        <div class="container candidate_review">
            <input type="text" name="candidate_id" value="<?php echo $candidate_id; ?>" hidden>
            <div class="row">
                <div class="col-md-8">
                    <form class="kt-form kt-form--fit kt-form--label-right" id="candidate_evaluation_form">
                        <div class="kt-portlet__body">
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3 col-sm-12">Applicant's Name:</label>
                                <div class="col-lg-8 col-md-9 col-sm-12">
                                    <input type="text" class="form-control" value="<?php echo $candidate_data['name'];?>" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3 col-sm-12">Position Applied For:</label>
                                <div class="col-lg-8 col-md-9 col-sm-12">
                                    <input type="text" class="form-control" value="<?php echo $candidate_degignation['name'];?>" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3 col-sm-12">Department/Division:</label>
                                <div class="col-lg-8 col-md-9 col-sm-12">
                                    <input type="text" class="form-control" value="<?php echo $candidate_department['name'];?>" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3 col-sm-12">Interview By:</label>
                                <div class="col-lg-8 col-md-9 col-sm-12">
                                    <input type="text" class="form-control" value="<?php echo $candidate_reporting_to['name'];?>" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3 col-sm-12">Sub Interview By:</label>
                                <div class="col-lg-8 col-md-9 col-sm-12">
                                    <select class="form-control kt-selectpicker" name="sub_reporting_to" multiple data-live-search = "true">
                                        <?php foreach($sub_reporting_list as $single_reporting){ ?>
                                            <option value="<?php echo $single_reporting['user_id'];?>"

                                                <?php foreach($evaluation_data['sub_reporting_to_list'] as $single_user){?>
                                                    <?php echo ($single_user['user_id'] == $single_reporting['user_id']) ? 'selected': '';?>
                                                <?php }?>>
                                                <?php echo $single_reporting['name']; ?>
                                            </option>
                                        <?php }?>
                                    <select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-4">
                    <form class="kt-form kt-form--fit kt-form--label-right" id="candidate_evaluation_form">
                        <input type="text" name="photo" value="<?php echo $evaluation_data['photo']?>" hidden>
                        <div class="form-group row">
                            <div class="col-sm-2">
                                <div class="kt-avatar kt-avatar--outline" id="kt_user_avatar">
                                    <div class="kt-avatar__holder" style="background-image: url(<?php echo base_url('assets/hr_document/candidate_photo/'.$evaluation_data['photo']); ?>);margin-top: 58px;"></div>
                                </div>
                            </div>
                            <div class="col-sm-6" style="margin-left: 240px;margin-top: -113px;">
                                <div class="dropzone dropzone-multi" id="candidate_photo">
                                    <div class="dropzone-panel">
                                        <a class="dropzone-select btn btn-label-brand btn-bold btn-sm dz-clickable">Upload</a>
                                    </div>
                                    <div class="dropzone-items">
                                        <div class="dropzone-item" style="display:none">
                                            <div class="dropzone-file">
                                                <div class="dropzone-filename" title="some_image_file_name.jpg">
                                                    <span data-dz-name>some_image_file_name.jpg</span> <strong>(<span  data-dz-size>340kb</span>)</strong>
                                                </div>
                                                <div class="dropzone-error" data-dz-errormessage></div>
                                            </div>
                                            <div class="dropzone-progress">
                                                <div class="progress">
                                                    <div class="progress-bar kt-bg-brand" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress></div>
                                                </div>
                                            </div>
                                            <div class="dropzone-toolbar">
                                                <span class="dropzone-delete" data-dz-remove><i class="flaticon2-cross"></i></span>
                                        </div>
                                    </div>
                                    </div>
                                <div class="dz-default dz-message"><span>Drop files here to upload</span></div></div>
                                <span class="form-text text-muted">Max file size is 1MB and max number of files is 1.<br>Only JPG, AND PNG File Accepted.</span>
                            </div>
                        </div>
                    </form>
                </div>
                <form class="col-lg-12" id="candidate_evaluation_form">
                    <table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="">
                        <thead id="">
                            <tr>
                                <th class="kt-align-center" style="width: 20%;">Competency</th>
                                <th class="kt-align-center" style="width: 15%;">Excellent</th>
                                <th class="kt-align-center" style="width: 15%;">Good</th>
                                <th class="kt-align-center" style="width: 15%;">Average</th>
                                <th class="kt-align-center" style="width: 15%;">Poor</th>
                                <th class="kt-align-center" style="width: 20%;">Comments</th>
                            </tr>
                        </thead>
                        <tbody id="">
                            <tr>
                                <td class="kt-align-center">Edu.Background</td>
                                <td class="kt-align-center">
                                    <input type="radio" name="education_background" id="excellent" value="excellent"<?php echo $evaluation_data['education_background'] == "excellent" ? 'Checked': ''?>>
                                    <label for="excellent"></label>
                                </td>
                                <td class="kt-align-center">
                                    <input type="radio" name="education_background" id="good" value="good" <?php echo $evaluation_data['education_background'] == "good" ? 'Checked': ''?>>
                                    <label for="good"></label>
                                </td>
                                <td class="kt-align-center">
                                    <input type="radio" name="education_background" id="average" value="average" <?php echo $evaluation_data['education_background'] == "average" ? 'Checked': ''?>>
                                    <label for="average"></label>
                                </td>
                                <td class="kt-align-center">
                                    <input type="radio" name="education_background" id="poor" value="poor" <?php echo $evaluation_data['education_background'] == "poor" ? 'Checked': ''?>>
                                    <label for="poor"></label>
                                </td>
                                <td class="kt-align-center"><input type="text" class="form-control" name="education_background_comments" value="<?php echo $evaluation_data['education_background_comments']?>"></td>
                            </tr>
                            <tr>
                                <td class="kt-align-center">Job Knowledge</td>
                                <td class="kt-align-center">
                                    <input type="radio" name ="job_knowledge" id="excellent1" value="excellent" <?php echo $evaluation_data['job_knowledge'] == "excellent" ? 'Checked': ''?>>
                                    <label for="excellent1"></label>
                                </td>
                                <td class="kt-align-center">
                                    <input type="radio" name="job_knowledge" id="good1" value="good" <?php echo $evaluation_data['job_knowledge'] == "good" ? 'Checked': ''?>>
                                    <label for="good1"></label>
                                </td>
                                <td class="kt-align-center">
                                    <input type="radio" name="job_knowledge" id="average1" value="average" <?php echo $evaluation_data['job_knowledge'] == "average" ? 'Checked': ''?>>
                                    <label for="average1"></label>
                                </td>
                                <td class="kt-align-center">
                                    <input type="radio" name="job_knowledge" id="poor1" value="poor" <?php echo $evaluation_data['job_knowledge'] == "poor" ? 'Checked': ''?>>
                                    <label for="poor1"></label>
                                </td>
                                <td class="kt-align-center"><input type="text" class="form-control" name="job_knowledge_comments" value="<?php echo $evaluation_data['job_knowledge_comments']?>"></td>
                            </tr>
                            <tr>
                                <td class="kt-align-center">Prior Work Experience</td>
                                <td class="kt-align-center">
                                    <input type="radio" name="prior_work_experience" id="excellent2" value="excellent" <?php echo $evaluation_data['prior_work_experience'] == "excellent" ? 'Checked': ''?>>
                                    <label for="excellent2"></label>
                                </td>
                                <td class="kt-align-center">
                                    <input type="radio" name="prior_work_experience" id="good2" value="good" <?php echo $evaluation_data['prior_work_experience'] == "good" ? 'Checked': ''?>>
                                    <label for="good2"></label>
                                </td>
                                <td class="kt-align-center">
                                    <input type="radio" name="prior_work_experience" id="average2" value="average" <?php echo $evaluation_data['prior_work_experience'] == "average" ? 'Checked': ''?>>
                                    <label for="average2"></label>
                                </td>
                                <td class="kt-align-center">
                                    <input type="radio" name="prior_work_experience" id="poor2" value="poor" <?php echo $evaluation_data['prior_work_experience'] == "poor" ? 'Checked': ''?>>
                                    <label for="poor2"></label>
                                </td>
                                <td class="kt-align-center"><input type="text" class="form-control" name="prior_work_experience_comments" value="<?php echo $evaluation_data['prior_work_experience_comments']?>"></td>
                            </tr>
                            <tr>
                                <td class="kt-align-center">Communication Skills</td>
                                <td class="kt-align-center">
                                    <input type="radio" name="communication_skills" id="excellent3" value="excellent" <?php echo $evaluation_data['communication_skills'] == "excellent" ? 'Checked': ''?>>
                                    <label for="excellent3"></label>
                                </td>
                                <td class="kt-align-center">
                                    <input type="radio" name="communication_skills" id="good3" value="good" <?php echo $evaluation_data['communication_skills'] == "good" ? 'Checked': ''?>>
                                    <label for="good3"></label>
                                </td>
                                <td class="kt-align-center">
                                    <input type="radio" name="communication_skills" id="average3" value="average" <?php echo $evaluation_data['communication_skills'] == "average" ? 'Checked': ''?>>
                                    <label for="average3"></label>
                                </td>
                                <td class="kt-align-center">
                                    <input type="radio" name="communication_skills" id="poor3" value="poor" <?php echo $evaluation_data['communication_skills'] == "poor" ? 'Checked': ''?>>
                                    <label for="poor3"></label>
                                </td>
                                <td class="kt-align-center"><input type="text" class="form-control" name="communication_skills_comments" value="<?php echo $evaluation_data['communication_skills_comments']?>"></td>
                            </tr>
                            <tr>
                                <td class="kt-align-center">Required Skills</td>
                                <td class="kt-align-center">
                                    <input type="radio" name="required_skills" id="excellent4" value="excellent" <?php echo $evaluation_data['required_skills'] == "excellent" ? 'Checked': ''?>>
                                    <label for="excellent4"></label>
                                </td>
                                <td class="kt-align-center">
                                    <input type="radio" name="required_skills" id="good4"value="good" <?php echo $evaluation_data['required_skills'] == "good" ? 'Checked': ''?>>
                                    <label for="good4"></label>
                                </td>
                                <td class="kt-align-center">
                                    <input type="radio" name="required_skills" id="average4"value="average" <?php echo $evaluation_data['required_skills'] == "average" ? 'Checked': ''?>>
                                    <label for="average4"></label>
                                </td>
                                <td class="kt-align-center">
                                    <input type="radio" name="required_skills" id="poor4"value="poor" <?php echo $evaluation_data['required_skills'] == "poor" ? 'Checked': ''?>>
                                    <label for="poor4"></label>
                                </td>
                                <td class="kt-align-center"><input type="text" class="form-control" name="required_skills_comments" value="<?php echo $evaluation_data['required_skills_comments']?>"></td>
                            </tr>
                            <tr>
                                <td class="kt-align-center">Overall Impression</td>
                                <td class="kt-align-center">
                                    <input type="radio" name="overall_impression" id="excellent5" value="excellent" <?php echo $evaluation_data['overall_impression'] == "excellent" ? 'Checked': ''?>>
                                    <label for="excellent5"></label>
                                </td>
                                <td class="kt-align-center">
                                    <input type="radio" name="overall_impression" id="good5" value="good" <?php echo $evaluation_data['overall_impression'] == "good" ? 'Checked': ''?>>
                                    <label for="good5"></label>
                                </td>
                                <td class="kt-align-center">
                                    <input type="radio" name="overall_impression" id="average5" value="average" <?php echo $evaluation_data['overall_impression'] == "average" ? 'Checked': ''?>>
                                    <label for="average5"></label>
                                </td>
                                <td class="kt-align-center">
                                    <input type="radio" name="overall_impression" id="poor5" value="poor" <?php echo $evaluation_data['overall_impression'] == "poor" ? 'Checked': ''?>>
                                    <label for="poor5"></label>
                                </td>
                                <td class="kt-align-center">
                                    <input type="text" class="form-control" name="overall_impression_comments" value="<?php echo $evaluation_data['overall_impression_comments']?>">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2 col-sm-12">Notice Period:</label>
                            <div class="col-lg-5 col-md-9 col-sm-12">
                                <input type="text" class="form-control" name="notice_period" value="<?php echo $evaluation_data['notice_period']?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2 col-sm-12">Current CTC:</label>
                            <div class="col-lg-5 col-md-9 col-sm-12">
                                <input type="text" class="form-control" name="current_ctc" value="<?php echo $evaluation_data['current_ctc']?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2 col-sm-12">Expected CTC:</label>
                            <div class="col-lg-5 col-md-9 col-sm-12">
                                <input type="text" class="form-control" name="expected_ctc" value="<?php echo $evaluation_data['expected_ctc']?>">
                            </div>
                        </div>
                        <div class="">
                            <div class=" form-group row">
                                <table style="width:100%" class="table table-striped- table-bordered table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="">
                                    <thead>
                                    <tr>
                                        <th class="kt-align-center" style="width: 30%;border-bottom-color: transparent;"><h6 style="margin-bottom: -16px;">Interview Status</h6></th>
                                        <th class="kt-align-center" style="width: 35%;">Selected</th>
                                        <th class="kt-align-center" style="width: 35%;">Not Selected</th>
                                    </tr>
                                    </thead>
                                    <tbody id="">
                                        <tr>
                                            <td class="kt-align-center" style="border-top-color: transparent;"></td>
                                            <td class="kt-align-center" >
                                                <input type="radio" name="interview_status" id="selected" value="selected" <?php echo $evaluation_data['interview_status'] == "selected" ? 'Checked': ''?>>
                                                <label for="selected"></label>
                                            </td>
                                            <td class="kt-align-center">
                                                <input type="radio" name="interview_status" id="not_selected" value="not_selected" <?php echo $evaluation_data['interview_status'] == "not_selected" ? 'Checked': ''?>>
                                                <label for="not_selected"></label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="">
                            <label class="col-form-label col-lg-2 col-sm-12">Any Other Comments:</label>
                            <div class=" form-group row">
                                <textarea class="form-control" name ="other_comments" rows="5"><?php echo $evaluation_data['other_comments'];?></textarea>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <button type="button" class="btn btn-primary add_candidate_evaluation_form" candidate_id="<?php echo $candidate_data['id'];?>" mrf_id="<?php echo $candidate_data['mrf_id'];?>" value="<?php echo $evaluation_data['id']?>">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>