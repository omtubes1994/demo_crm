<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet ">
                <div class="kt-portlet__head">
                    <?php if(in_array(3,$this->session->userdata('graph_data_access'))) { ?>
                        <!--Begin::Portlet-->
                        <div class="kt-portlet kt-portlet--height-fluid">
                            <div class="kt-portlet__head" style="padding: 25px 25px 00px 25px;">
                                <div class="kt-portlet__head-label">
                                    <div class="row" style="padding: 0px 10px;">
                                        <div class="col-lg-12">
                                            <h3 class="kt-portlet__head-title">Lead Stage</h3>
                                            <select class="form-control kt-selectpicker" id="lead_stage" multiple>
                                                <option value="null">Blank Lead Stage</option>
                                                <option value="1">Stage-1</option>
                                                <option value="2">Stage-2</option>
                                                <option value="3">Stage-3</option>
                                                <option value="4">Stage-4</option>
                                                <option value="5">Stage-5</option>
                                                <option value="6">Stage-6</option>
                                                <option value="0">Stage-0</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row" style="padding: 0px 10px;">
                                        <div class="col-lg-12">
                                            <h3 class="kt-portlet__head-title">Lead Type:</h3>
                                            <select class="form-control kt-selectpicker" id="lead_type" multiple>
                                                <option value="null">Blank Lead Type</option>
                                                <option value="1">Trader</option>
                                                <option value="2">Epc Contractor</option>
                                                <option value="3">End User</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row" style="padding: 0px 10px;">
                                        <div class="col-lg-12">
                                            <h3 class="kt-portlet__head-title">Country:</h3>
                                            <select class="form-control kt-selectpicker" id="lead_country" multiple>
                                                <option value="null">Blank Country</option>
                                                <?php foreach ($country_list as $country_details){ ?>

                                                    <option value="<?php echo $country_details['id']; ?>"><?php echo $country_details['name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row" style="padding: 0px 10px;">
                                        <div class="col-lg-12">
                                            <h3 class="kt-portlet__head-title">Region:</h3>
                                            <select class="form-control kt-selectpicker" id="lead_region" multiple>
                                                <option value="null">Blank Region</option>
                                                <?php foreach ($region_list as $region_details){ ?>

                                                    <option value="<?php echo $region_details['id']; ?>"><?php echo $region_details['name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row" style="padding: 0px 10px;">
                                        <div class="col-lg-12">
                                            <h3 class="kt-portlet__head-title">Hetro / Primary Leads:</h3>
                                            <select class="form-control kt-selectpicker" id="source" multiple>
                                                <?php foreach ($sub_module_list as $single_sub_module_details) { ?>
                                                    <option value="<?php echo $single_sub_module_details['url']; ?>"><?php echo $single_sub_module_details['sub_module_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-portlet__head-toolbar">
                                    <button type="button" class="btn btn-info add_search_filter"style="border: 5px solid #e2e5ec;">Search</button>
                                    <button type="button" class="btn btn-dark"style="border: 5px solid #e2e5ec;">Reset</button>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <!--Begin::Timeline 3 -->
                                <div class="kt-scroll" data-scroll="true" style="height: 500px">
                                    <div id="sales_lead_count_highchart"></div>
                                </div>
                                <!--End::Timeline 3 -->
                            </div>
                        </div>

                        <!--End::Portlet-->
                    <?php }?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">

            <!--begin::Portlet-->
            <div class="kt-portlet ">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title kt-font-primary">
                            User Assign List
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-actions">
                            <a href="javascript:void(0);" class="btn btn-brand btn-bold btn-sm change_department" department="sales_active">
                                Sales:- Active
                            </a>
                            <a href="javascript:void(0);" class="btn btn-outline-brand btn-bold btn-sm change_department" department="sales_inactive">
                                Sales:- Inactive
                            </a>
                            <a href="javascript:void(0);" class="btn btn-outline-brand btn-sm btn-bold change_department" department="procurement_active">
                                Procurement:- Active
                            </a>
                            <a href="javascript:void(0);" class="btn btn-outline-brand btn-sm btn-bold change_department" department="procurement_inactive">
                                Procurement:- Inactive
                            </a>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">

                   <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="local_data" style="">
                        <?php $this->load->view('common/sales_list');?>
                    </div>
                </div>
            </div>
            <!--end::Portlet-->

        </div>
    </div>

</div>

<!-- end:: Content -->