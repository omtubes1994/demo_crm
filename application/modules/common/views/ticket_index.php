<!-- begin:: Content -->
<link href="<?php echo base_url('assets/css/ticket/ticket_css.css');?>" rel='stylesheet' type='text/css'  media='all'>
<link href="<?php echo base_url('assets/css/ticket/ticket_css_2.css');?>" rel='stylesheet' type='text/css'  media='all'>
<style type="text/css">
	.ticket_add_form_label{
	 	color: #898989 !important;
	 	font-size: 17px !important;
	 	font-weight: bold !important;
	 	padding: 0 7px 0 4px !important;
	 	font-family: 'latomedium' !important;
	}
	.project_mng_sort_filter > a::before {
		top: 3px;
	}
</style>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

	<!--Begin::Dashboard 6-->
	<div class="wrapper contennmenu_wrap" style="margin-top:0px !important;">
	    <div class="row">
	    	<div class="col-md-12" id="activity_top_numbers">
	    		<div class="white_grid wrapper">
				    <div class="common_heading"><i class="custom_heading_icon overview_data_icon"></i> Overview </div>
				    <ul class="keyword_ranking_legend project_mng_legend wrapper">
				        <li>
				            <div class="keyword_ranking_legend">
				                <i class="lead_tracking_legend_icon active_tasks_legend_icon"></i>
				                <span class="ticket_status_change" status_name="Accept" style="cursor:pointer;">
				                    <abbr><?php echo (!empty($ticket_overall['Accept'])) ? $ticket_overall['Accept']:0; ?></abbr>
				                    <em>Accepted Tickets</em>
				                </span>
				            </div>
				        </li>
				        <li>
				            <div class="keyword_ranking_legend">
				                <i class="lead_tracking_legend_icon pending_complete_icon"></i>
				                <span class="ticket_status_change" status_name="Done" style="cursor:pointer;">
				                    <abbr><?php echo (!empty($ticket_overall['Done'])) ? $ticket_overall['Done']:0; ?></abbr>
				                    <em>Completed Tickets</em>
				                </span>
				            </div>
				        </li>
				        <li>
				            <div class="keyword_ranking_legend">
				                <i class="lead_tracking_legend_icon pending_all_issue_icon"></i>
				                <span class="ticket_status_change" status_name="Pending" style="cursor:pointer;">
				                    <abbr><?php echo (!empty($ticket_overall['Pending'])) ? $ticket_overall['Pending']:0; ?></abbr>
				                    <em>Pending Tickets</em>
				                </span>
				            </div>
				        </li>
				        <li>
				            <div class="keyword_ranking_legend">
				                <i class="lead_tracking_legend_icon project_overdue_tasks_icon"></i>
				                <span class="ticket_status_change" status_name="Overdue" style="cursor:pointer;">
				                    <abbr><?php echo (!empty($ticket_overall['Overdue'])) ? $ticket_overall['Overdue']:0; ?></abbr>
				                    <em>Overdue Tickets</em>
				                </span>
				            </div>
				        </li>
				        <li>
				            <div class="keyword_ranking_legend">
				                <i class="lead_tracking_legend_icon non_active_legend_icon"></i>
				                <span class="ticket_status_change" status_name="Reject" style="cursor:pointer;">
				                    <abbr><?php echo (!empty($ticket_overall['Reject'])) ? $ticket_overall['Reject']:0; ?></abbr>
				                    <em>Rejected Tickets</em>
				                </span>
				            </div>
				        </li>
				    </ul>
				</div>
			</div>
	        <div class="col-md-12">
	            <div class="white_grid wrapper">
	            	<div class="wrapper" id="pmgmt_top_filter_bar">
						<div class="col-md-12">
						    <div class="pending_issue_search_bar_wrapper project_mng_search_section ">
						        <div class="new_lead_form_inner_search new_lead_form_inner_search12 project-mng-search-bar">
						        	<abbr>
										<button type="button" class="btn btn-bold btn-label-brand btn-sm create_ticket" data-toggle="modal" data-target="#add_new_ticket">Create Ticket</button>
						        	</abbr>
						    		<div class="pending_create_new_issue project_mng_main_wrapper">
										<div class="new_conversation_sort12 project_mng_sort_filter">
						                    <a href="javascript:void(0);" class="idattrlink-new add-overlay add_filter_by" icon=""><em>Filter By</em></a>
						                    <div class="project_management_comment_box" style="display:none;">
												<div class="project_management_comment_inner">
													<span>Filter By</span>
													<div class="wrapper scroll-remove-margin" style="height:auto;" id="filter_elements_div">
														<!--begin::Form-->
														<form class="kt-form kt-form--label-right" id="search_filter_form">
															<ul class="project_filter_element">
																<li>
																<?php if(!empty($ticket_search_filter['ticket_status'])){ ?>
																	<span>Ticket Status</span>
																	<ul class="project_mng_services_filter">
																		<select class="form-control" id="ticket_status" name="ticket_status" multiple="multiple" style="min-width: 100px!important;">
																			<optgroup label="Select Ticket Status">
																			<?php foreach ($ticket_search_filter['ticket_status'] as $single_ticket_status) { ?>
																				<option value="<?php echo $single_ticket_status; ?>">
																					<?php echo $single_ticket_status; ?>
																				</option>
																			<?php } ?>
																			</optgroup>
																		</select>
																	</ul>
																<?php } ?>
																</li>
																<li>
																<?php if(!empty($ticket_search_filter['ticket_type'])){ ?>
																	<span>Ticket Type</span>
																	<ul class="project_mng_services_filter">
																		<select class="form-control" id="ticket_type" name="ticket_id" multiple="multiple">
																			<optgroup label="Select Ticket Type">
																			<?php foreach ($ticket_search_filter['ticket_type'] as $single_ticket_type_details) { ?>
																				<option value="<?php echo $single_ticket_type_details['id']; ?>">
																					<?php echo $single_ticket_type_details['name']; ?>
																				</option>
																			<?php } ?>
																			</optgroup>
																		</select>
																	</ul>
																<?php } ?>
																</li>
																<?php if(in_array($this->session->userdata('role'), array(15, 17))) { ?>
																	<li>
																	<?php if(!empty($ticket_search_filter['ticket_user_list'])){ ?>
																		<span>User</span>
																		<ul class="project_mng_services_filter">
																			<select class="form-control" id="ticket_user" name="user_id" multiple="multiple">
																				<optgroup label="Select Ticket User">
																				<?php foreach ($ticket_search_filter['ticket_user_list'] as $single_user_details) { ?>
																					<option value="<?php echo $single_user_details['user_id']; ?>">
																						<?php echo $single_user_details['user_name']; ?>
																					</option>
																				<?php } ?>
																				</optgroup>
																			</select>
																		</ul>
																	<?php } ?>
																	</li>
																	<li>
																	<?php if(!empty($ticket_search_filter['ticket_user_department'])){ ?>
																		<span>Department</span>
																		<ul class="project_mng_services_filter">
																			<select class="form-control" id="ticket_department" name="user_department_id" multiple="multiple">
																				<optgroup label="Select Ticket Department">
																				<?php foreach ($ticket_search_filter['ticket_user_department'] as $single_user_details) { ?>
																					<option value="<?php echo $single_user_details['department_id']; ?>">
																						<?php echo $single_user_details['department_name']; ?>
																					</option>
																				<?php } ?>
																				</optgroup>
																			</select>
																		</ul>
																	<?php } ?>
																	</li>
																<?php } ?>	
															</ul>
														</form>

														<!--end::Form-->
													</div>
													<div class="project_management_comment_footer">
														<a href="javascript:void(0);" id="" class="lead-save-btn submit_search_filter">Filter</a>
														<a href="javascript:void(0);" id="" class="lead-save-btn reset_search_filter">Reset Filter</a>
							                            <a class="lead-close-btn close_filter_by" style="color: #898989 !important;">Cancel</a>
													</div>
												</div>
											</div>
						                </div>
						    		</div>
						        </div>
						    </div>
						</div>
					</div>
					<div class="wrapper conversation_main_wrapper">
						<div class="keyword_ranking_tab_content content_scroller mCustomScrollbar _mCS_1 mCS_no_scrollbar" style="height:auto;">
							<div id="activity_list_container" class="wrapper">
								<div class="glo_app layer white loader_class" style="display: none;">
									<div class="glo_app apploader large"></div>
								</div>
								<div class="pending_issues_table_data_section project_mng_data_section">
									<table class="pending_active_issues_table_data project_management_table_data">
										<!--begin::Accordion-->
										<div class="accordion accordion-solid accordion-panel accordion-toggle-svg" id="accordionExample8">
											<div class="card">
												<div class="card-header" id="headingOne8">
													<div class="card-title" data-toggle="collapse" data-target="#collapseOne8" aria-expanded="true" aria-controls="collapseOne8">
														Product Inventory <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
															<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																<polygon points="0 0 24 0 24 24 0 24" />
																<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
																<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
															</g>
														</svg> </div>
												</div>
												<div id="collapseOne8" class="collapse show" aria-labelledby="headingOne8" data-parent="#accordionExample8">
													<div class="card-body">
														Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
													</div>
												</div>
											</div>
											<div class="card">
												<div class="card-header" id="headingTwo8">
													<div class="card-title collapsed" data-toggle="collapse" data-target="#collapseTwo8" aria-expanded="false" aria-controls="collapseTwo8">
														Order Statistics <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
															<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																<polygon points="0 0 24 0 24 24 0 24" />
																<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
																<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
															</g>
														</svg> </div>
												</div>
												<div id="collapseTwo8" class="collapse" aria-labelledby="headingTwo8" data-parent="#accordionExample8">
													<div class="card-body">
														Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
													</div>
												</div>
											</div>
											<div class="card">
												<div class="card-header" id="headingThree8">
													<div class="card-title collapsed" data-toggle="collapse" data-target="#collapseThree8" aria-expanded="false" aria-controls="collapseThree8">
														eCommerce Reports <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
															<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																<polygon points="0 0 24 0 24 24 0 24" />
																<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
																<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
															</g>
														</svg> </div>
												</div>
												<div id="collapseThree8" class="collapse" aria-labelledby="headingThree8" data-parent="#accordionExample8">
													<div class="card-body">
														Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
													</div>
												</div>
											</div>
										</div>

										<!--end::Accordion-->
										<tbody id="ticket_list">
											<?php $this->load->view('common/ticket_list_body'); ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
	            </div>
	        </div>
	    </div>
	</div>
	<!--End::Dashboard 6-->
</div>

<!-- end:: Content -->


<!--begin::Modal-->
<div class="modal fade" id="add_new_ticket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header" style="padding: 15px 25px;">
				<h5 class="modal-title" id="exampleModalLabel ">New message</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">

				<!--begin::Form-->
				<form class="kt-form kt-form--label-right" id="kt_form_1">
					<div class="kt-portlet__body" style="padding: 10px 25px;">
						<div class="form-group form-group-last kt-hide">
							<div class="alert alert-danger" role="alert" id="kt_form_1_msg">
								<div class="alert-icon"><i class="flaticon-warning"></i></div>
								<div class="alert-text">
									Oh snap! Change a few things up and try submitting again.
								</div>
								<div class="alert-close">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="la la-close"></i></span>
									</button>
								</div>
							</div>
						</div>
						
						<div class="form-group row">
							<label class="col-form-label col-lg-2 col-sm-12 ticket_add_form_label" style="text-align: left;">Ticket Type</label>
							<div class="col-lg-4 col-md-9 col-sm-12 form-group-sub ticket_add_form_label">
								<select class="form-control kt-selectpicker" name="ticket_id" data-size="7" data-live-search="true">
									<option value="">Select</option>
									<option value="AF">Afghanistan</option>
									<option value="AX">Åland Islands</option>
								</select>
								<span class="form-text text-muted">Please select an option.</span>
							</div>
						</div>
						
						<div class="form-group row">
							<label class="col-form-label col-lg-2 col-sm-12 ticket_add_form_label" style="text-align: left;">Comment</label>
							<div class="col-lg-9 col-md-9 col-sm-12 ticket_add_form_label">
								<textarea class="form-control" name="user_comment" placeholder="Enter a menu" rows="8"></textarea>
								<span class="form-text text-muted">Please enter a menu within text length range 10 and 100.</span>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-2 col-sm-12 ticket_add_form_label" style="text-align: left;">Closing Date</label>
							<div class="col-lg-4 col-md-9 col-sm-12 ticket_add_form_label">
								<div class="input-group date">
									<input type="text" class="form-control" name="ticket_close_time" readonly value="" id="kt_datepicker_3" />
								</div>
								<span class="form-text text-muted">Enable clear and today helper buttons</span>
							</div>
						</div>
						<div class="kt-separator kt-separator--border-dashed kt-separator--space-xl"></div>
					</div>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<div class="row">
								<div class="col-lg-9 ml-lg-auto">
									<button type="submit" class="btn btn-brand">Validate</button>
									<button type="reset" class="btn btn-secondary">Cancel</button>
								</div>
							</div>
						</div>
					</div>
				</form>

				<!--end::Form-->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Send message</button>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->