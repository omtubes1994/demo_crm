<?php if(!empty($candidate_list)) {?>
    <?php foreach ($candidate_list as $candidate_list_key => $single_list_data_value) { ?>

        <tr role="row" class="candidate_font">

            <td class="kt-align-center"><?php echo $candidate_list_key+1; ?></td>
            <td><?php echo $single_list_data_value['name'];?></td>
            <td><?php echo $single_list_data_value['mobile']; ?></td>
            <td><?php echo $single_list_data_value['email'];?></td>
            <td><?php echo $single_list_data_value['source_name'];?></td>
            <td><?php echo $single_list_data_value['stage_name'];?></br></br>
                <span style="font-size: 10px;">
                    <?php echo $single_list_data_value['stage_reason'];?>
                </span>
            </td>
            <td><?php echo $single_list_data_value['hr_name'];?></td>
            <td><?php echo $single_list_data_value['last_comment'];?></br></br>
                <span style="font-size: 10px;">
                    <?php echo $single_list_data_value['user_name'];?></br>
                    <?php echo $single_list_data_value['tat'];?>
                </span>
            </td>
            <td><?php echo $single_list_data_value['evaluation_last_comment'];?></br></br>
                <span style="font-size: 10px;">
                    <?php echo $single_list_data_value['interviewer_name'];?></br>
                </span>
            </td>
            <td>
                <?php if(!empty($single_list_data_value['resume'])){?>
                    <a href="<?php echo base_url('assets/hr_document/candidate_resume/'.$single_list_data_value['resume']);?>" target="_blank" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Resume" >
                        <i class="la la-eye"></i>
                    </a>
                <?php }?>
                <?php if($this->session->userdata('hr_access')['candidate_list_action_tab_access']) { ?>

                <a href="<?php echo base_url('common/candidate/'.$single_list_data_value['mrf_id'].'/'.$single_list_data_value['id']); ?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">
                    <i class="la la-edit"></i>
                </a>
                <a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon btn-icon-md stage_modal" title="Stage" data-toggle="modal" data-target="#update_stage" candidate_id="<?php echo $single_list_data_value['id'];?>" stage_id="<?php echo $single_list_data_value['stage_id'];?>" stage_reason_id="<?php echo $single_list_data_value['stage_reason_id'];?>" other_reason="<?php echo $single_list_data_value['other_reason'];?>">
                    <i class="fa fa-file-upload"></i>
                </a>
                <a class="btn btn-sm btn-clean btn-icon btn-icon-md get_candidate_comment" title="Add Comment" href="javascript:;" candidate_id="<?php echo $single_list_data_value['id'];?>">
                    <i class="fa fa-comments"></i>
                </a>
                <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_candidate" title="Delete" value="<?php echo $single_list_data_value['id']?>">
                    <i class="la la-trash"></i>
                </a>
            <?php }?>
                <a href="<?php echo base_url('common/interview_evaluation/'.$single_list_data_value['id']); ?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Interview Evaluation">
                    <i class="fa fa-copy"></i>
                </a>
                <?php if($this->session->userdata('hr_access')['candidate_list_action_change_mrf_access']){?>
                    <a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon btn-icon-md mrf_modal" title="Change Mrf" data-toggle="modal" data-target="#update_mrf" Candidate_id="<?php echo $single_list_data_value['id'];?>" mrf_id="<?php echo $single_list_data_value['mrf_id'];?>">
                        <i class="flaticon2-edit"></i>
                    </a>
                <?php } ?>
            </td>
        </tr>
    <?php }
} ?>