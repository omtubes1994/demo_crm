<?php if(!empty($ticket_list)){ ?>
	<tr class="table-full-block">
		<th width="5%">
			<span class="">
			    <label for="check_all">Sr. No</label>
			</span>
		</th>
		<th class="table_data_left">Project Activities (<span id="projActCnt"><?php echo count($ticket_list)?></span>)</th>
		<th class="table_data_center">Current Status</th>
		<th class="table_data_center">Action</th>
	</tr>
	<?php foreach ($ticket_list as $ticket_key => $ticket_details) { ?>
		<tr id="" class="table-full-block task ">
		   	<td>
				<span class="">
				    <label for=""><?php echo $ticket_key+1; ?></label>
				</span>
			</td>
			<td class="table_data_left">
				<abbr><?php echo $ticket_details['ticket_name']; ?>
					<ul class="project_mng_name_list">
						<li>
							<abbr>
								<a href="javascript:void(0)"><?php echo $ticket_details['user_name']; ?></a>
							</abbr>
						</li>
						<li>
							<abbr><i>Comment:</i><?php echo $ticket_details['user_comment']; ?></abbr>
						</li>
					</ul>
					<ul class="project_mng_start_date">
						<li>
							<span>Create Date: </span><?php echo $ticket_details['start_date']; ?>
						</li>
						<li>
							<span>Due Date: </span><?php echo $ticket_details['end_date']; ?>
						</li>
					</ul>
				</abbr>
			</td>
			<td>
				<em class="pending-activity-color"><?php echo $ticket_details['ticket_status']; ?></em>
			</td>
			<td class="activity_action">
				<ul>
					<?php if($ticket_details['access_ticket_active']){ ?>
					<li>
						<a href="javascript:void(0);" class="action" data-toggle="tooltip" data-placement="top" title="Mark Ticket as Active" id="<?php echo $ticket_details['id']; ?>"></a>
					</li>
					<?php } ?>
					<?php if($ticket_details['access_ticket_completed']){ ?>
					<li>
						<a href="javascript:void(0);" class="prject_mng_active_task_active action" data-toggle="tooltip" data-placement="top" title="Mark Ticket as Completed" id="<?php echo $ticket_details['id']; ?>"></a>
					</li>
					<?php } ?>
					<?php if($ticket_details['access_ticket_reject']){ ?>
					<li>
						<a href="javascript:void(0);" class="action" data-toggle="tooltip" data-placement="top" title="Mark Ticket as N/A" id="<?php echo $ticket_details['id']; ?>"></a>
					</li>
					<?php } ?>
					<?php if($ticket_details['access_ticket_delete']){ ?>
					<li>
						<a href="javascript:void(0);" class="flaticon-delete-1 action" data-toggle="tooltip" data-placement="top" title="Delete Ticket" id="<?php echo $ticket_details['id']; ?>"></a>
					</li>
					<?php } ?>
				</ul>
			</td>
		</tr>
	<?php } ?>
<?php }else{ ?>	
	<tr class="table-full-block">
		<th width="5%">
			<span class="">
			    <label for="check_all">Sr. No</label>
			</span>
		</th>
		<th class="table_data_left">Project Activities (<span id="projActCnt"><?php echo 0; ?></span>)</th>
		<th class="table_data_center">Current Status</th>
		<th class="table_data_center">Action</th>
	</tr>
	<tr id="" class="table-full-block task ">
	   	<td><span class=""></span></td>
		<td class="table_data_left"><abbr>No Record Found</abbr></td>
		<td></td>
		<td class="activity_action"></td>
	</tr>
<?php } ?>