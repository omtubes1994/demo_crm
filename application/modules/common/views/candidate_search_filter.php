<form class="kt-form kt-form--fit kt-margin-b-20" id="search_filter_form">
    <div class="candidate_font" id="search_filter_div">
        <div class="row kt-margin-b-20">
            <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                <!-- <label>Name :</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="name" name="name" value="<?php echo $candidate_search_filter['name']?>" placeholder="Search Your Name">
                </div> -->
                <label>Candidate Name:</label>
                <select class="form-control kt-selectpicker" name="candidate_id" multiple data-live-search = "true">
                    <option value="">Select</option>
                    <?php foreach($candidate_detail as $single_candidate){ ?>
                        <option value="<?php echo $single_candidate['id']; ?>"
                            <?php echo (in_array($single_candidate['id'], $search_filter['candidate_id'])) ? 'selected': ''; ?>>
                            <?php echo $single_candidate['name']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                <label>Source:</label>
                <select class="form-control kt-selectpicker" name="source_id" multiple data-live-search = "true">
                    <option value="">Select</option>
                    <?php foreach($source_list as $single_source){ ?>
                        <option value="<?php echo $single_source['id']; ?>"
                            <?php echo (in_array($single_source['id'], $search_filter['source_id'])) ? 'selected': ''; ?>>
                            <?php echo $single_source['name']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                <label>Satge:</label>
                <select class="form-control kt-selectpicker" name="stage_id" multiple data-live-search = "true">
                    <option value="">Select</option>
                        <?php foreach($stage_list as $single_stage){ ?>
                            <option value="<?php echo $single_stage['id']; ?>"
                                <?php echo (in_array($single_stage['id'], $search_filter['stage_id'])) ? 'selected': ''; ?>>
                                <?php echo $single_stage['name']; ?>
                            </option>
                        <?php } ?>
                </select>
            </div>
            <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                <label>Assigned To:</label>
                <select class="form-control kt-selectpicker" name="hr_user_id" multiple data-live-search = "true">
                    <option value="">Select</option>
                        <?php foreach($assigned_to_list as $single_assigned_to){ ?>
                            <option value="<?php echo $single_assigned_to['user_id']; ?>"
                                <?php echo (in_array($single_assigned_to['user_id'], $search_filter['hr_user_id'])) ? 'selected': ''; ?>>
                                <?php echo $single_assigned_to['name']; ?>
                            </option>
                        <?php } ?>
                </select>
            </div>
        </div>
        <div class="kt-separator kt-separator--md kt-separator--dashed"></div>
        <div class="row">
            <div class="col-lg-12">
                <button class="btn btn-primary btn-brand--icon candidate_search_filter_submit" type="reset">
                    <span>
                        <i class="la la-search"></i>
                        <span>Search</span>
                    </span>
                </button>
                &nbsp;&nbsp;
                <button class="btn btn-secondary btn-secondary--icon candidate_search_filter_reset" type="reset">
                    <span>
                        <i class="la la-close"></i>
                        <span>Reset</span>
                    </span>
                </button>
            </div>
        </div>
    </div>
</form>