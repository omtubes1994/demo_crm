<style type="text/css">
    .kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
        padding-top: 4% !important;
    }
    .layer-white{
        display: none;
        position: absolute;
        top: 0em !important;
        left: 0em !important;
        width: 100%;
        height: 100%;
        text-align: center;
        vertical-align: middle;
        background-color: rgba(255, 255, 255, 0.55);
        opacity: 1;
        line-height: 1;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
        -webkit-animation-duration: 0.5s;
        animation-duration: 0.5s;
        -webkit-transition: background-color 0.5s linear;
        transition: background-color 0.5s linear;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        will-change: opacity;
        z-index: 9;
    }
    .div-loader{
        position: absolute;
        top: 50%;
        left: 50%;
        margin: 0px;
        text-align: center;
        z-index: 1000;
        -webkit-transform: translateX(-50%) translateY(-50%);
        transform: translateX(-50%) translateY(-50%);
    }
    .kt-spinner:before {
        width: 50px;
        height: 50px;
        margin-top: -10px;
    }
    .kt-aside--fixed .kt-wrapper {
        padding-left: 130px !important;
    }
    .kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper {
        padding-top: 30px !important;
    }
    .salary_slip thead tr th{
        background-color: transparent;
        padding: 1rem 0 0.5rem 0;
        color: #74788d;
        border-top: 0;
        border-bottom: 1px solid #ebedf2;
    }
    .salary_slip tbody tr td{
        background-color: transparent;
        padding: 1rem 0 1rem 0;
        border-top: none;
        font-weight: 700;
        font-size: 1.1rem;
        color: #595d6e;
        padding-top: 1.8rem;
    }
    .grand_total thead tr th{
        font-size: 1.1rem;
        text-transform: capitalize;
        font-weight: 500;
        color: #74788d;
        border-top: 0;
        border-bottom: 1px solid #ebedf2;
        padding: 10px 10px 10px 0;
        background-color: transparent;
    }
    .grand_total tbody tr td{

        font-size: 1.1rem;
        text-transform: capitalize;
        background-color: transparent;
        font-weight: 500;
        color: #595d6e;
        padding: 10px 10px 10px 0;
    }
    span.badge_stage_6{
        background: #33ccff !important;
    }
    span.stage_6{
        color: #33ccff !important;
    }
    span.badge_stage_5{
        background: #28a745 !important;
    }
    span.stage_5{
        color: #28a745 !important;
    }
    span.badge_stage_4{
        background: #007bff !important;
    }
    span.stage_4{
        color: #007bff !important;
    }
    span.badge_stage_3{
        background: #fd7e14 !important;
    }
    span.stage_3{
        color: #fd7e14 !important;
    }
    span.badge_stage_2{
        background: #ffc107 !important;
    }
    span.stage_2{
        color: #ffc107 !important;
    }
    span.badge_stage_1{
        background: #969ca0 !important;
    }
    span.stage_1{
        color: #969ca0 !important;
    }
    span.badge_stage_0{
        background: #ff0000 !important;
    }
    span.stage_0{
        color: #ff0000 !important;
    }
    span.badge_stage_null{
        background: #6c757d !important;
    }
    span.stage_null{
        color: #6c757d !important;
    }
    span.daily_report{
       cursor: pointer;
       display: inline-block;
       outline: none;
       text-align: center;
       width: 100%;
       background: #F5F5F5;
       color: #282a3c;
       font-size: 15px;
       font-family: 'latomedium';
       background-color: #5d78ffd9;
       padding: 3px 3px 12px 3px;"
    }
    span.daily_report_active{
        border-bottom: 0.25rem solid #282a3c !important;
    }

</style>
<!-- end:: Header Mobile -->
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

               <!-- begin:: Content -->
               <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

                   <!--Begin::App-->
                   <div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">

                       <!--Begin:: App Aside Mobile Toggle-->
                       <button class="kt-app__aside-close" id="kt_user_profile_aside_close">
                           <i class="la la-close"></i>
                       </button>

                       <!--End:: App Aside Mobile Toggle-->

                       <!--Begin:: App Aside-->
                       <div class="kt-grid__item kt-app__toggle kt-app__aside" id="kt_user_profile_aside" style="width: 200px;">

                           <!--begin:: Widgets/Applications/User/Profile1-->
                           <div class="kt-portlet">
                               <div class="kt-portlet__head  kt-portlet__head--noborder" style="min-height: 30px !important;"></div>
                               <div class="kt-portlet__body kt-portlet__body--fit-y">

                                    <!--begin::Widget -->
                                    <div class="kt-widget kt-widget--user-profile-1">
                                        <div class="kt-widget__head">
                                            <div class="kt-widget__content" style="padding-left: 10px !important;">
                                               <div class="kt-widget__section">
                                                   <a href="javascript:void(0);" class="kt-widget__username">
                                                       Sales Person List
                                                   </a>
                                               </div>
                                            </div>
                                        </div>
                                        <div class="kt-widget__body">
                                            <div class="kt-widget__items kt-scroll" data-scroll="true" style="height: 770px">
                                            <div id="hr_edit_listing_table_loader" class="layer-white">
                                                <div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
                                            </div>
                                            <?php if(!empty($user_list)) {?>
                                                <?php foreach ($user_list as $single_user_list) { ?>
                                                <a href="javascript:void(0);" class="kt-widget__item get_sales_person_daily_report" user_id="<?php echo $single_user_list['user_id']; ?>">
                                                    <span class="kt-widget__section">
                                                        <span class="kt-widget__desc">
                                                           <?php echo $single_user_list['name']; ?>
                                                           <span class="kt-font-bolder kt-font-success"></span>
                                                        </span>
                                                    </span>
                                                </a>
                                                <?php }?>
                                            <?php }?>
                                           </div>
                                       </div>
                                    </div>

                                    <!--end::Widget -->
                                </div>
                            </div>

                           <!--end:: Widgets/Applications/User/Profile1-->
                       </div>

                       <!--End:: App Aside-->

                       <!--Begin:: App Content-->
                       <div class="kt-grid__item kt-grid__item--fluid kt-app__content listing">
                            <div class="row">
                                <div class="kt-portlet">
                                    <div class="row" style="padding: 10px 30px 0px 30px;">
                                        <div class="col-xl-9">
                                            <div class="col-lg-6">
                                                <span class="kt-font-xl kt-font-bold" style="padding: 0px 15px 0px 0px;">
                                                    Medium:  
                                                    <select class="form-control kt-selectpicker col-xl-3" name="connect_mode" multiple>
                                                        <option value=""></option>
                                                        <optgroup label="Medium" data-max-options="2">
                                                            <option value="call">Call Connected</option>
                                                            <option value="call_attempted">Call Attempted</option>
                                                            <option value="email">Email</option>
                                                            <option value="linkedin">Linkedin</option>
                                                            <option value="whatsapp">Whatsapp</option>
                                                        </optgroup>
                                                    </select>
                                                </span>
                                                <button type="reset" class="btn btn-brand daily_report_search_filter" user_id="">Search</button>
                                            </div>
                                        </div>
                                        <div class="col-xl-3">
                                            <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
                                                <a href="javascript:void(0);" class="btn kt-subheader__btn-daterange" id="kt_dashboard_daterangepicker" data-toggle="kt-tooltip" title="Select Destrack Date" data-placement="left" style="background: rgb(0 0 0 / 10%); display: inline-flex; height: 32px !important; padding-top: 0; padding-bottom: 0; margin-top: 0.25rem; margin-bottom: 0.25rem; margin-left: 0.25rem;">
                                                    <span class="kt-subheader__btn-daterange-title" id="kt_dashboard_daterangepicker_title" style="display: inline-block;color: #959cb6;font-weight: 500;">Today</span>&nbsp;
                                                    <span class="kt-subheader__btn-daterange-date" id="kt_dashboard_daterangepicker_date" style="display: inline-block;color: #5d78ff;margin-right: 0.75rem;font-weight: 500;">Aug 16</span>
                                                    <i class="flaticon2-calendar-1" style="color: #5d78ff; font-size: 1rem !important;"></i>
                                                </a>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="row" style="padding: 10px 30px 0px 30px;" id="daily_report_count">
                                    </div>
                                    <div class="kt-portlet__body">
                                        <div class="row">
                                            <div class="col-xl-6 kt-align-center">
                                                <span class="kt-font-xl kt-font-bolder daily_report daily_report_active" report_type="lead">Lead</span>
                                            </div>
                                            <div class="col-xl-6 kt-align-center">
                                                <span class="kt-font-xl kt-font-bolder daily_report" report_type="quotation">Quotation</span>
                                            </div>
                                        </div>
                                        <!--begin::Accordion-->
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <div class="table-responsive kt-scroll" data-scroll="true" style="height: 625px">
                                                    <div id="hr_edit_listing_table_loader" class="layer-white">
                                                        <div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
                                                    </div>
                                                    <table class="table" id="daily_report_table">
                                                        
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                       <!--End:: App Content-->
                   </div>

                   <!--End::App-->
               </div>

               <!-- end:: Content -->
            </div>

            <!-- begin:: Footer -->
            <div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
                <div class="kt-container  kt-container--fluid ">
                    <div class="kt-footer__copyright">
                        2020&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Keenthemes</a>
                    </div>
                    <div class="kt-footer__menu">
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">About</a>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Team</a>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Contact</a>
                    </div>
                </div>
            </div>

            <!-- end:: Footer -->
        </div>
    </div>
</div>

<!-- end:: Page -->

<!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="fa fa-arrow-up"></i>
</div>

<!-- end::Scrolltop -->