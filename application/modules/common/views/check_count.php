<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Task List</title>

  <!-- Custom fonts for this template -->
  <link href="http://localhost/employee_management_system/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="http://localhost/employee_management_system/css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="http://localhost/employee_management_system/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow" style="height:1rem;">

          


        </nav>
        <!-- End of Topbar -->
                <!-- Begin Page Content -->
                <div class="container-fluid">

<!-- Page Heading -->

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>#</th>
                    <th>id</th>
                    <th>Name</th>
                    <th>Region</th>
                    <th>Country</th>
                    <th>Sales Person</th>
                    <th>rfq_mst</th>
                    <th>quotation_mst</th>
                    <th>query_master</th>
                    <th>pq_lead_detail</th>
                    <th>pq_lead_connects</th>
                    <th>pq_client</th>
                    <th>pq_activity</th>
                    <th>lead_management</th>
                    <th>invoice_mst</th>
                    <th>daily_work_sales_on_quotation_data</th>
                    <th>daily_work_sales_on_lead_data</th>
                    <th>customer_dtl</th>
                    <th>customer_data</th>
                    <th>customer_connect</th>
                    <th>competitor_ranks_invoice</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>#</th>
                    <th>id</th>
                    <th>Name</th>
                    <th>Region</th>
                    <th>Country</th>
                    <th>Sales Person</th>
                    <th>rfq_mst</th>
                    <th>quotation_mst</th>
                    <th>query_master</th>
                    <th>pq_lead_detail</th>
                    <th>pq_lead_connects</th>
                    <th>pq_client</th>
                    <th>pq_activity</th>
                    <th>lead_management</th>
                    <th>invoice_mst</th>
                    <th>daily_work_sales_on_quotation_data</th>
                    <th>daily_work_sales_on_lead_data</th>
                    <th>customer_dtl</th>
                    <th>customer_data</th>
                    <th>customer_connect</th>
                    <th>competitor_ranks_invoice</th>
                    <th>Status</th>
                </tr>
            </tfoot>
            <tbody>
                <?php 
                    foreach ($list_data as $list_data_key => $single_list_data) {
                        
                        echo "<tr>";
                            echo "<td>".($list_data_key+1)."</td>";
                            echo "<td>{$single_list_data['id']}</td>";
                            echo "<td>{$single_list_data['name']}</td>";
                            echo "<td>{$single_list_data['region']}</td>";
                            echo "<td>{$single_list_data['country']}</td>";
                            echo "<td>{$single_list_data['sales_person']}</td>";
                            echo "<td>{$single_list_data['rfq_mst_count']}</td>";
                            echo "<td>{$single_list_data['quotation_mst_count']}</td>";
                            echo "<td>{$single_list_data['query_master_count']}</td>";
                            echo "<td>{$single_list_data['pq_lead_detail_count']}</td>";
                            echo "<td>{$single_list_data['pq_lead_connects_count']}</td>";
                            echo "<td>{$single_list_data['pq_client_count']}</td>";
                            echo "<td>{$single_list_data['pq_activity_count']}</td>";
                            echo "<td>{$single_list_data['lead_management_count']}</td>";
                            echo "<td>{$single_list_data['invoice_mst_count']}</td>";
                            echo "<td>{$single_list_data['daily_work_sales_on_quotation_data_count']}</td>";
                            echo "<td>{$single_list_data['daily_work_sales_on_lead_data_count']}</td>";
                            echo "<td>{$single_list_data['customer_dtl_count']}</td>";
                            echo "<td>{$single_list_data['customer_data_count']}</td>";
                            echo "<td>{$single_list_data['customer_connect_count']}</td>";
                            echo "<td>{$single_list_data['competitor_ranks_invoice_count']}</td>";
                            echo "<td>{$single_list_data['status']}</td>";
                        echo "</tr>";
                    }
                ?>
            </tbody>
      </table>
    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Bootstrap core JavaScript-->
  <script src="http://localhost/employee_management_system//vendor/jquery/jquery.min.js"></script>
  <script src="http://localhost/employee_management_system//vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="http://localhost/employee_management_system//vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="http://localhost/employee_management_system//js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="http://localhost/employee_management_system//vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="http://localhost/employee_management_system//vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="http://localhost/employee_management_system//js/demo/datatables-demo.js"></script>

</body>

</html>
