<div class="row">
    <div class="col-md-12 form-group row">
        <label class="col-form-label col-lg-3 col-sm-12">MRF</label>
        <select class="form-control kt-select2 mrf_id" id="kt_select2" name="mrf_id">
            <option>Select MRF</option>
            <?php foreach($mrf_id_list as $single_mrf){ ?>
                <option value="<?php echo $single_mrf['id']; ?>"
                    <?php echo ($mrf_id == $single_mrf['id']) ? 'selected': ''; ?>>
                    <?php echo $single_mrf['mrf_no']; ?>
                </option>
            <?php } ?>
        </select>
    </div>
</div>