<div class="kt-portlet__body">
	<div class="form-group row form-group-marginless kt-margin-t-20">
		<label class="col-lg-2 col-form-label kt-font-bolder" style="text-align: left !important; padding-bottom: 16px;">Sales Person:</label>
		<div class="col-lg-4" style="padding-bottom: 16px;">
			<select class="form-control sales_person_select_picker kt-font-bold" name="user_id" data-live-search="true">
				<?php if(empty($form_data['user_id'])){?>
					<?php if(!empty($user_list)){?>
						<option value="">Select Sales Person</option>
						<?php foreach ($user_list as $single_user_details) { ?>
							<option value="<?php echo $single_user_details['user_id'].'_'.$single_user_details['role'];?>"
							<?php //echo (in_array($single_user_details['user_id'], $search_filter['name']))?'selected':''?>>
								<?php echo $single_user_details['name'];?>
							</option>
						<?php } ?>
					<?php } ?>
				<?php }else{ ?>
					<?php if(!empty($user_list)){?>
						<option value="">Select Sales Person</option>
						<?php foreach ($user_list as $single_user_details) { ?>
							<option value="<?php echo $single_user_details['user_id'].'_'.$single_user_details['role'];?>"
							<?php echo 'selected'; ?>>
								<?php echo $single_user_details['name'];?>
							</option>
						<?php } ?>
					<?php } ?>
				<?php } ?>
			</select>
			<span class="form-text text-muted kt-font-bold kt-font-danger user_id_error" style="display: none;">Please select sales person</span>
		</div>
		<label class="col-lg-2 col-form-label kt-font-bolder" style="text-align: left !important; padding-bottom: 16px;">Call Connect:</label>
		<div class="col-lg-4" style="padding-bottom: 16px;">
			<input type="text" class="form-control kt-font-bold" placeholder="Call Connect Count" name="call_connected" value="<?php echo $form_data['call_connected']; ?>">
			<span class="form-text text-muted kt-font-bold kt-font-danger call_connected_error" style="display: none;">Please enter your call connected count</span>
		</div>
	</div>
	<div class="form-group row form-group-marginless">
		<label class="col-lg-2 col-form-label" style="text-align: left !important; display: none;">Call Attempt:</label>
		<div class="col-lg-4" style="display: none;">
			<input type="text" class="form-control" placeholder="Call Attempted Count" name="call_attempted" value=0>
			<span class="form-text text-muted">Please enter your call attempted count</span>
		</div>
		<label class="col-lg-2 col-form-label" style="text-align: left !important; display: none;">LinkedIn:</label>
		<div class="col-lg-4" style="display: none;">
			<input type="text" class="form-control" placeholder="Linkedin Count" name="linkedin" value=0>
			<span class="form-text text-muted">Please enter your linkedin count</span>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-lg-2 col-form-label" style="text-align: left !important; display: none;">WhatsApp:</label>
		<div class="col-lg-4" style="display: none;">
			<input type="text" class="form-control" placeholder="Whatsapp Count" name="whatsapp" value="0">
			<span class="form-text text-muted">Please enter your whastapp count</span>
		</div>
		<label class="col-lg-2 col-form-label kt-font-bolder" style="text-align: left !important; padding-bottom: 16px;">Email:</label>
		<div class="col-lg-4" style="padding-bottom: 16px;">
			<input type="text" class="form-control kt-font-bold" placeholder="Email Count" name="emails" value="<?php echo $form_data['emails']; ?>">
			<span class="form-text text-muted kt-font-bold kt-font-danger email_error" style="display: none;">Please enter your email count</span>
		</div>
		<label class="col-lg-2 col-form-label kt-font-bolder" style="text-align: left !important; padding-bottom: 16px;">Desktrack:</label>
		<div class="col-lg-4" style="padding-bottom: 16px;">
			<div class="row">
				<div class="col-lg-6">
					<input type="text" class="form-control kt-font-bold" placeholder="Hours" name="desktrack_hours" value="<?php echo $form_data['desktrack_hours']; ?>">
				</div>
				<div class="col-lg-6">
					<input type="text" class="form-control kt-font-bold" placeholder="Minutes" name="desktrack_minutes" value="<?php echo $form_data['desktrack_minutes']; ?>">
				</div>
			</div>
			<span class="form-text text-muted kt-font-bold kt-font-danger desktrack_error" style="display: none;">Please enter your desktrack time</span>
		</div>
	</div>
</div>