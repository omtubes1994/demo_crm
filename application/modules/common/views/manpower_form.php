<style>
    .main-container{
        border-style: double;
        padding: 20px 20px 20px 20px;
    }
    .h2{
        text-align: center;
        margin: 2px 89px 0 89px;
    }
</style>
<h2 Class="h2" style="padding: 5px 0 5px 0;"> MANPOWER REQUISITION FORM </h2>

<div class="container main-container">
    <div class="row">
        <input type="text" name="manpower_id" value="<?php echo $id; ?>" hidden>
        <div class="col-md-6" style="border-right:double;border-bottom:double;">
            <form class="form" id="manpower_details_form">
                <div class="row">
                    <div class="col">
                        <label>DEPARTMENT:</label>
                        <select class="form-control kt-selectpicker" name="mrf_department_id" data-size="7" data-live-search="true">
                            <option value="">Select</option>
                            <?php foreach ($all_department as $single_department) { ?>
                                <option value="<?php echo $single_department['id']; ?>"
                                    <?php echo ($manpower_data['mrf_department_id'] == $single_department['id']) ? 'selected': ''; ?>>
                                    <?php echo $single_department['name']; ?>
                                </option>
                            <?php } ?>
                        <select>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label>NO OF POSITIONS:</label>
                        <input type="number" class="form-control" name= "no_of_positions" value="<?php echo $manpower_data['no_of_positions'];?>">
                    </div>
                </div>
                <?php
                    $display = 'disabled';
                    if($this->session->userdata('hr_access')['mrf_form_assigned_to_access']) {
                        $display = '';
                    }
                ?>
                <div class="row">
                    <div class="col">
                        <label>ASSIGNED TO:</label>
                        <select class="form-control kt-selectpicker" name="assigned_to" <?php echo $display;?> multiple>
                            <?php foreach($assigned_to as $single_assigned_to){ ?>
                                <option value="<?php echo $single_assigned_to['user_id'];?>"

                                    <?php foreach($manpower_data['assigned_to_list'] as $single_assigned){?>
                                        <?php echo ($single_assigned['user_id'] == $single_assigned_to['user_id']) ? 'selected': '';?>
                                    <?php }?>
                                    >
                                    <?php echo $single_assigned_to['name']; ?>
                                </option>
                            <?php }?>
                        <select>
                    </div>
                </div>
            </form>
            <br>
        </div>

        <div class="col-md-6" style="border-bottom:double;">
            <!--begin::Form-->
                <form class="common-form" id="manpower_details_form">
                    <div class="row">
                        <div class="col">
                            <label>JOB TITLE:</label>
                            <select class="form-control kt-select2 job_title kt-selectpicker" id="job_title" name="mrf_designation_id" data-size="7" data-live-search="true">
                                <option value=""> Select Job Title</option>
                                <?php foreach ($job_title as $single_job_title) { ?>
                                    <option value="<?php echo $single_job_title['id'];?>" 
                                        <?php echo ($manpower_data['mrf_designation_id'] == $single_job_title['id']) ? 'selected': '';?>>
                                        <?php echo $single_job_title['name']; ?></option>
                                <?php } ?>
                            <select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label class="reporting_to">REPORTING TO:</label>
                            <select class="form-control kt-selectpicker" name="reporting_to" data-size="7" data-live-search="true">
                                <option value="">Select Reporting To</option>
                                <?php foreach ($reporting_to as $single_reporting_to) { ?>
                                    <option value="<?php echo $single_reporting_to['user_id'];?>"
                                    <?php echo ($manpower_data['reporting_to'] == $single_reporting_to['user_id']) ? 'selected': '';?>>
                                    <?php echo $single_reporting_to['name']; ?>
                                </option>
                            <?php } ?>
                            <select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label>LOCATION / SITE:</label>
                            <select class="form-control kt-selectpicker" name="location_site" data-size="7" data-live-search="true" >
                                <option value="">Select Location</option>
                                <option value="Corporate Office" <?php echo ($manpower_data['location_site'] == 'Corporate Office') ? 'selected': '';?>>Corporate Office</option>

                                <option value="Taloja" <?php echo ($manpower_data['location_site'] == 'Taloja') ? 'selected': '';?>>Taloja</option>

                                <option value="Head Office" <?php echo ($manpower_data['location_site'] == 'Head Office') ? 'selected': '';?>>Head Office</option>
                            <select>
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col">
                            <label>PRIORITY / CLOSING DATE:</label>
                            <select class="form-control kt-selectpicker" name="priority_closingdate" data-size="7" data-live-search="true">
                                <option value="">Select Priority</option>

                                <option value="Low" <?php echo ($manpower_data['priority_closingdate'] == 'Low') ? 'selected': '';?>>Low</option>

                                <option value="Medium" <?php echo ($manpower_data['priority_closingdate'] == 'Medium') ? 'selected': '';?>>Medium</option>

                                <option value="High" <?php echo ($manpower_data['priority_closingdate'] == 'High') ? 'selected': '';?>>High</option>
                            <select>
                        </div>
                    </div>
                </form>
            <!--end::Form-->
        </div>
        <div class="col-md-12">
            <div class="kt-portlet__body">
                <!--begin::Form-->
                    <form class="common-form kt-form" id="manpower_details_form">
                        <div class="form-group row">
                            <label class="col-2 col-form-label">TYPE OF:</label>
                            <div class="col-9">
                                <div class="kt-radio-inline">
                                    <label class="kt-radio">
                                        <input type="radio" class="form-control" name="type_of" value ="New" <?php echo $manpower_data['type_of'] == "New" ? 'Checked': ''?>> New
                                        <span></span>
                                    </label>
                                    <label class="kt-radio" style="left: 51px;">
                                        <input type="radio" class="form-control radio" name="type_of" value ="Old" <?php echo $manpower_data['type_of'] == "Old" ? 'Checked': ''?>> Back up of Existing Employee
                                        <span></span>
                                    </label>
                                    <div class="input_type_name" style="display: none;position: absolute;">
                                        <label class="col-12 col-form-label" style="left: 430px;bottom: 33px;">Name:
                                            <select class="form-control kt-select2 kt-selectpicker" name="type_of_name" data-size="7" data-live-search="true">
                                                <option value="">Select To</option>
                                                <?php foreach ($all_type_of_name as $single_all_type_of_name) { ?>
                                                    <option value="<?php echo $single_all_type_of_name['user_id'];?>"<?php echo ($manpower_data['type_of_name'] == $single_all_type_of_name['user_id']) ? 'selected': '';
                                                    ?>><?php echo $single_all_type_of_name['name']; ?>
                                                </option>
                                            <?php } ?>
                                            <select>
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">REQUIREMENT:</label>
                            <div class="col-9">
                                <div class="kt-radio-inline">
                                    <label class="kt-radio">
                                        <input type="radio" name= "type_of" value ="Confidential" <?php echo $manpower_data['type_of'] == "Confidential" ? 'Checked': ''?>> Confidential
                                        <span></span>
                                    </label>
                                    <label class="kt-radio">
                                        <input type="radio" class="radio" name= "type_of" value ="Replacement" <?php echo $manpower_data['type_of'] == "Replacement" ? 'Checked': ''?>> Replacement of Resigned Employee
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                <!--end::Form-->
            </div>
        </div>
        <hr style="width: 100%;background-color: #646C9A;">
        <br>
        <form class="common-form" id="manpower_details_form">
            <div class="">
                <div class="form-group row">
                    <label class="col-form-label col-lg-2 col-sm-12">JOB DESCRIPTION :</label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <textarea class="form-control" name="job_description" style="height: 200px;width:850px;"><?php echo $manpower_data['job_description']; ?></textarea>
                    </div>
                </div>
            </div>
        </form>
        <hr style="width:100%;background-color: #646C9A; ">
        <br>
        <div class="col-lg-12">
            <div class="">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon kt-hidden">
                            <i class="la la-gear"></i>
                        </span>
                        <h4 class="kt-portlet__head-title">
                            GENERAL REQUIREMENT
                        </h4>
                    </div>
                </div> <br>
                <!--begin::Form-->
                    <form class="common-form kt-form" id="manpower_details_form" >
                        <div class="kt-portlet__body">
                            <div class="kt-form__section kt-form__section--first">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label">TOTAL EXPERIENCE:</label>
                                    <div class="col-lg-4">
                                        <select class="form-control kt-selectpicker" name="total_experience" data-size="7" data-live-search="true">
                                            <option value="">Select Total Experience</option>
                                            <option value="0 To 1 Years"  <?php echo ($manpower_data['total_experience'] == '0 To 1 Years') ? 'selected': ''; ?>>0 To 1 Years</option>
                                            <option value="1 To 2 Years"  <?php echo ($manpower_data['total_experience'] == '1 To 2 Years') ? 'selected': ''; ?>>1 To 2 Years</option>
                                            <option value="2 To 3 Years"  <?php echo ($manpower_data['total_experience'] == '2 To 3 Years') ? 'selected': ''; ?>>2 To 3 Years</option>
                                            <option value="3 To 4 Years"  <?php echo ($manpower_data['total_experience'] == '3 To 4 Years') ? 'selected': ''; ?>>3 To 4 Years</option>
                                            <option value="4 To 5 Years"  <?php echo ($manpower_data['total_experience'] == '4 To 5 Years') ? 'selected': ''; ?>>4 To 5 Years</option>
                                            <option value="5 To 6 Years"  <?php echo ($manpower_data['total_experience'] == '5 To 6 Years') ? 'selected': ''; ?>>5 To 6 Years</option>
                                            <option value="6 To 7 Years"  <?php echo ($manpower_data['total_experience'] == '6 To 7 Years') ? 'selected': ''; ?>>6 To 7 Years</option>
                                            <option value="7 To 8 Years"  <?php echo ($manpower_data['total_experience'] == '7 To 8 Years') ? 'selected': ''; ?>>7 To 8 Years</option>
                                            <option value="8 To 9 Years"  <?php echo ($manpower_data['total_experience'] == '8 To 9 Years') ? 'selected': ''; ?>>8 To 9 Years</option>
                                            <option value="9 To 10 Years" <?php echo ($manpower_data['total_experience'] == '9 To 10 Years') ? 'selected': ''; ?>>9 To 10 Years</option>
                                        <select>
                                    </div>
                                    <label class="col-lg-2 col-form-label">QUALIFICATION:</label>
                                    <div class="col-lg-4">
                                        <select class="form-control kt-selectpicker" name="qualification" data-size="7" data-live-search="true">
                                            <option value="0">Select Your Qualification</option>
                                            <?php foreach ($qualification as $single_qualification) { ?>
                                                <option value="<?php echo $single_qualification['id']; ?>" <?php echo ($manpower_data['qualification'] == $single_qualification['id']) ? 'selected': ''; ?>>
                                                    <?php echo $single_qualification['name']; ?>
                                                </option>
                                            <?php } ?>
                                        <select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label">SALARY RANGE:</label>
                                    <div class="col-lg-4">
                                        <select class="form-control kt-selectpicker" name="salary_range" data-size="7" data-live-search="true">
                                            <option value="">Select Salary Range</option>
                                            <option value="10 TO 15K" <?php echo ($manpower_data['salary_range'] == '10 TO 15K') ? 'selected': ''; ?>>10 To 15K</option>
                                            <option value="15 TO 20K" <?php echo ($manpower_data['salary_range'] == '15 TO 20K') ? 'selected': ''; ?>>15 To 20K</option>
                                            <option value="20 TO 25K" <?php echo ($manpower_data['salary_range'] == '20 TO 25K') ? 'selected': ''; ?>>20 To 25K</option>
                                            <option value="25 TO 30K" <?php echo ($manpower_data['salary_range'] == '25 TO 30K') ? 'selected': ''; ?>>25 To 30K</option>
                                            <option value="30 TO 35K" <?php echo ($manpower_data['salary_range'] == '30 TO 35K') ? 'selected': ''; ?>>30 To 35K</option>
                                            <option value="35 TO 40K" <?php echo ($manpower_data['salary_range'] == '35 TO 40K') ? 'selected': ''; ?>>35 To 40K</option>
                                            <option value="40 TO 45K" <?php echo ($manpower_data['salary_range'] == '40 TO 45K') ? 'selected': ''; ?>>40 To 45K</option>
                                            <option value="45 TO 50K" <?php echo ($manpower_data['salary_range'] == '45 TO 50K') ? 'selected': ''; ?>>45 To 50K</option>
                                            <option value="50 TO 55K" <?php echo ($manpower_data['salary_range'] == '50 TO 55K') ? 'selected': ''; ?>>50 To 55K</option>
                                            <option value="55 TO 60K" <?php echo ($manpower_data['salary_range'] == '55 TO 60K') ? 'selected': ''; ?>>55 To 60K</option>
                                        <select>
                                    </div>
                                    <label class="col-lg-2 col-form-label">Date</label>
                                    <div class="col-lg-4">
                                        <div class="input-group">
                                            <input type="text" class="form-control mrf_date_picker" name="date" value="<?php echo date('Y-m-d', strtotime($manpower_data['date']));?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                <!--end::Form-->
            </div>
        </div>
        <hr style="width:100%;background-color: #646C9A; ">
        <br>
        <div class="form-group">
            <button type="submit" id="add_form" class="btn btn-brand add_manpower" value="<?php echo $manpower_data['id'];?>">Submit</button>
        </div>
    </div>
</div>