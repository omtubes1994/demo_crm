<style type="text/css">
    .kt-aside--fixed .kt-wrapper {
        padding-left: 130px !important;
    }
    .kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper {
        padding-top: 60px !important;
    }
    .layer-white{
        display: none;
        position: absolute;
        top: 0em !important;
        left: 0em !important;
        width: 100%;
        height: 100%;
        text-align: center;
        vertical-align: middle;
        background-color: rgba(255, 255, 255, 0.55);
        opacity: 1;
        line-height: 1;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
        -webkit-animation-duration: 0.5s;
        animation-duration: 0.5s;
        -webkit-transition: background-color 0.5s linear;
        transition: background-color 0.5s linear;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        will-change: opacity;
        z-index: 9;
    }
    .div-loader{
        position: absolute;
        top: 50%;
        left: 50%;
        margin: 0px;
        text-align: center;
        z-index: 1000;
        -webkit-transform: translateX(-50%) translateY(-50%);
        transform: translateX(-50%) translateY(-50%);
    }
    .kt-spinner:before {
        width: 50px;
        height: 50px;
        margin-top: -10px;
    }
</style>
<!-- end:: Header Mobile -->
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <!-- begin:: Content Head -->
                <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                    <div class="kt-container  kt-container--fluid ">
                        <div class="kt-subheader__main">
                            <h3 class="kt-subheader__title"></h3>
                            <span class="kt-subheader__separator kt-hidden"></span>
                            <div class="kt-subheader__breadcrumbs">
                                <a href="javascript:void(0);" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                                <span class="kt-subheader__breadcrumbs-separator"></span>
                                <a href="" class="kt-subheader__breadcrumbs-link">Sales</a>
                                <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Sales Employee Listing</span>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- end:: Content Head -->

                <!-- begin:: Content -->
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

                    <div class="row" id="search_filter">
                        <div class="kt-portlet kt-portlet--mobile">
                            <div class="kt-portlet__head kt-portlet__head--lg">
                                <div class="kt-portlet__head-label">
                                    <span class="kt-portlet__head-icon">
                                        <i class="kt-font-brand flaticon2-line-chart"></i>
                                    </span>
                                    <h3 class="kt-portlet__head-title">
                                        Sales Employee Listing
                                    </h3>
                                    
                                </div>
                                <div class="kt-widget__action" style="padding: 15px 0px 0px 0px;">
                                    <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
                                        <a href="javascript:void(0);" class="btn kt-subheader__btn-daterange" id="sales_person_daily_report_daterangepicker" data-toggle="kt-tooltip" title="Select Destrack Date" data-placement="left" style="background: rgb(0 0 0 / 10%); display: inline-flex; height: 32px !important; padding-top: 0; padding-bottom: 0; margin-top: 0.25rem; margin-bottom: 0.25rem; margin-left: 0.25rem;">
                                            <span class="kt-subheader__btn-daterange-title" id="sales_person_daily_report_daterangepicker_title" style="display: inline-block;color: #959cb6;font-weight: 500;"></span>&nbsp;
                                            <span class="kt-subheader__btn-daterange-date" id="sales_person_daily_report_daterangepicker_date" style="display: inline-block;color: #5d78ff;margin-right: 0.75rem;font-weight: 500;"></span>
                                            <i class="flaticon2-calendar-1" style="color: #5d78ff; font-size: 1rem !important;"></i>
                                        </a>
                                    </ul>
                                </div>
                            </div>
                        </div>            
                    </div>    
                    <div>
                    	<div class="row" id="sales_person_daily_report_listing">
                    		<?php 
                    			if(!empty($user_list)){
                    				
                    				$image_not_found_array = array('success', 'danger', 'warning', 'info', 'dark', 'brand');
        							$image_not_found_array_key = 0;
                    			?>
                                <input type="text" id="current_user_id_key" value="1" hidden>
                    			<?php foreach($user_list as $current_key => $single_user_details){?>
                    				<div class="col-xl-6">
						                <!--begin:: Portlet-->
						                <div class="kt-portlet">
						                    <div class="kt-portlet__body">
						                        <div class="kt-widget kt-widget--user-profile-3">
						                            <div class="kt-widget__top">
					                            	<?php 
					                            		$current_image_name = $image_not_found_array[$image_not_found_array_key%5];
					                            	?>   
				                                    <?php if(!empty($single_user_details['profile_pic_file_path'])) {?>
					                                    <div class="kt-widget__media kt-hidden-">
					                                        <img src="<?php echo 'https://crm.omtubes.com/assets/hr_document/profile_pic/'.$single_user_details['profile_pic_file_path'];?>" alt="image"  style="max-width: 110px;max-height: 110px;">
					                                    </div>
					                                <?php } else {?>
					                                    <div class="kt-widget__pic kt-widget__pic--<?php echo $current_image_name;?> kt-font-<?php echo $current_image_name;?> kt-font-boldest kt-font-light kt-hidden-">
				                                           <?php 
                                                                if(!empty($single_user_details['first_name']) && !empty($single_user_details['last_name'])) {

                                                                    echo ucfirst($single_user_details['first_name'][0]), ucfirst($single_user_details['last_name'][0]);
                                                                }?>
					                                    </div>
					                                <?php }?>
						                                <div class="kt-widget__content">
						                                    <div class="kt-widget__head">
						                                        <a href="javascript:void(0);" class="kt-widget__username">
						                                            <?php echo trim($single_user_details['first_name']),' ', trim($single_user_details['last_name']);?>
						                                            <i class="flaticon2-correct kt-font-brand"></i>
						                                        </a>
						                                        <div class="kt-widget__action">
						                                            <a href="javascript:void(0);" class="btn btn-brand btn-sm btn-upper get_data <?php echo $current_key+1; ?>" user_id="<?php echo $single_user_details['user_id'];?>" image_name="<?php echo $current_image_name;?>">
						                                                Get Data
						                                            </a>
                                                                    <a href="<?php echo base_url('common/daily_report/'); ?>" class="btn btn-brand btn-sm btn-upper">
                                                                        View Lead
                                                                    </a>
						                                        </div>
						                                    </div>
						                                    <div class="kt-widget__subhead">
                                                                <div class="layer-white_<?php echo $single_user_details['user_id'];?>" style="display: none;">
                                                                    <div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
                                                                </div>
                                                                <div class="row" id="sales_person_daily_report_listing_single_<?php echo $single_user_details['user_id'];?>">
    					                                    		
                                                                </div>
						                                    </div>
						                                </div>
						                                
						                                <?php $image_not_found_array_key++;?>
						                            </div>
						                        </div>
						                    </div>
						                </div>
						                <!--end:: Portlet-->
						            </div>
					        	<?php } ?>
					        <?php } ?>
                    	</div>
                    </div>           
                </div>

                <!-- end:: Content -->
            </div>

            <!-- begin:: Footer -->
            <div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
                <div class="kt-container  kt-container--fluid ">
                    <div class="kt-footer__copyright">
                        2020&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Keenthemes</a>
                    </div>
                    <div class="kt-footer__menu">
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">About</a>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Team</a>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Contact</a>
                    </div>
                </div>
            </div>

            <!-- end:: Footer -->
        </div>
    </div>
</div>

<!-- end:: Page -->

<!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="fa fa-arrow-up"></i>
</div>

<!-- end::Scrolltop -->