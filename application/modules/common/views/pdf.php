<html lang="en">
    <!-- begin::Head -->
    <head>
		<base href="https://localhost/demo_crm/">
		<meta charset="utf-8">
		<title>CRM | </title>
		<meta name="description" content="Updates and statistics">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
		<!--begin::Fonts -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
		<!--begin::Fonts -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

		<!--end::Fonts -->

		<!--begin::Global Theme Styles(used by all pages) -->
		<!-- <link href="https://crm.omtubes.com/assets/css/style.bundle.css?v1.3.8" rel="stylesheet" type="text/css"> -->

		<!--end::Global Theme Styles -->

    </head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body style="background: #f2f3f8; display: inline-block; -webkit-box-orient: vertical; -webkit-box-direction: normal; -ms-flex-direction: column; flex-direction: column; color: #646c9a;">

		<!-- begin:: Page -->
        <div style="display: inline-block; -webkit-box-orient: vertical; -webkit-box-direction: normal; -ms-flex-direction: column; flex-direction: column;">
            <div style="display: inline-block; -webkit-box-orient: horizontal; -webkit-box-direction: normal; -ms-flex-direction: row; flex-direction: row;">

                <div style="width: 100%;
    padding: 25px;">

                    <div style="">
                        <!-- begin:: Content -->
                        <div style="">
                            <div>
                                <div style="display: inline-block; -ms-flex-wrap: wrap; flex-wrap: wrap; margin-right: -10px; margin-left: -10px;">
                                    <div style="-webkit-box-flex: 0; -ms-flex: 0 0 100%; flex: 0 0 100%; width: 100%;">
                                        <!--begin:: Portlet-->
                                        <div style="display: -webkit-box; display: -ms-flexbox; display: inline-block; -webkit-box-flex: 1; -ms-flex-positive: 1; flex-grow: 1; -webkit-box-orient: vertical; -webkit-box-direction: normal; -ms-flex-direction: column; flex-direction: column;-webkit-box-shadow: 0px 0px 13px 0px rgba(82, 63, 105, 0.05); box-shadow: 0px 0px 13px 0px rgba(82, 63, 105, 0.05); background-color: #ffffff; margin-bottom: 20px; border-radius: 4px;">
                                            <div style="display: inline-block; -webkit-box-orient: vertical; -webkit-box-direction: normal; -ms-flex-direction: column; flex-direction: column; padding: 25px; border-radius: 4px;">
                                                <div style="">
                                                    <div style="display: inline-block; -webkit-box-align: start; -ms-flex-align: start; align-items: flex-start;">
                                                        <div style="">
                                                            <div style="display: inline-block; -webkit-box-pack: justify; -ms-flex-pack: justify; justify-content: space-between; -webkit-box-align: center; -ms-flex-align: center; align-items: center; -ms-flex-wrap: wrap; flex-wrap: wrap;"></div>
                                                            <div style="padding: 0.6rem 0 0.8rem 0;">
                                                                <div style="">
                                                                    <div style=""><hr style="margin-top: 0rem;margin-bottom: 1rem;border: 0;border-top: 3px solid rgba(0, 0, 0, 0.1);"></div>
                                                                    <div style="width:600px; display: inline-block;">

                                                                        <span style="font-family: cursive; font-weight: 700 !important; color: #6c757d;">
                                                                            <?php echo $report_title; ?>
                                                                        </span>
                                                                    </div>
                                                                    <div style="width:400px; display: inline-block;">
                                                                        <span style="font-family: cursive; font-weight: 700 !important; color: #6c757d;">
                                                                            Date : <?php echo $report_date; ?>
                                                                        </span>
                                                                    </div>
                                                                    <div style=""><hr style="margin-top: 0rem;margin-bottom: 3rem;border: 0;border-top: 3px solid rgba(0, 0, 0, 0.1);"></div>
                                                                    <?php foreach ($sales_routine_data as $sales_person_name => $routine_details) { ?> 
                                                                        <div style="width:600px; display: inline-block;">

                                                                            <span style="font-family: cursive; font-weight: 700 !important; color: #6c757d;">
                                                                                <?php echo $sales_person_name; ?>
                                                                            </span>
                                                                        </div>
                                                                        <div style="width:400px; display: inline-block;">
                                                                            <span style="font-family: cursive; font-weight: 700 !important;">
                                                                                
                                                                            </span>
                                                                        </div>
                                                                        <div style=""><hr style="margin-top: 0rem;margin-bottom: 1rem;border: 0;border-top: 3px solid rgba(0, 0, 0, 0.1);"></div>
                                                                        <div style="width:80px; display: inline-block;">

                                                                            <span style="font-family: cursive; font-weight: 700 !important;color: #6c757d;">
                                                                                Sr. No
                                                                            </span>
                                                                        </div>
                                                                        <div style="width:150px; display: inline-block;">

                                                                            <span style="font-family: cursive; font-weight: 700 !important;color: #6c757d;">
                                                                                Date
                                                                            </span>
                                                                        </div>
                                                                        <div style="width:400px; display: inline-block;">

                                                                            <span style="font-family: cursive; font-weight: 700 !important;color: #6c757d;">
                                                                                Name
                                                                            </span>
                                                                        </div>
                                                                        <div style="width:100px; display: inline-block;">
                                                                            <span style="font-family: cursive; font-weight: 700 !important; color: #6c757d;">
                                                                                Status
                                                                            </span>
                                                                        </div>
                                                                        <div style="width:500px; display: inline-block;">
                                                                            <span style="font-family: cursive; font-weight: 700 !important;color: #6c757d;">
                                                                                Comments
                                                                            </span>
                                                                        </div>
                                                                        <div style=""><hr style="margin-top: 0rem;margin-bottom: 1rem;border: 0;border-top: 3px solid rgba(0, 0, 0, 0.1);"></div>
                                                                        <?php foreach($routine_details as $single_data_key => $single_data){ ?>
                                                                            <div style="width:80px; display: inline-block;">

                                                                                <span  style="font-family: cursive; padding-bottom: 30px; font-weight: 700 !important; color: #6c757d;">
                                                                                    <?php echo $single_data_key+1; ?>
                                                                                </span>
                                                                            </div>
                                                                            <div style="width:150px; display: inline-block;">

                                                                                <span  style="font-family: cursive; padding-bottom: 30px; font-weight: 700 !important; color: #6c757d;">
                                                                                    <?php echo $single_data['date']; ?>
                                                                                </span>
                                                                            </div>
                                                                            <div style="width:400px; display: inline-block;">

                                                                                <span  style="font-family: cursive; padding-bottom: 30px; font-weight: 700 !important;">
                                                                                    <?php echo $single_data['name']; ?>
                                                                                </span>
                                                                            </div>
                                                                            <div style="width:100px; display: inline-block;">
                                                                                <span  style="font-family: cursive; padding-bottom: 30px;  font-weight: 700 !important; color: #0abb87 !important;">
                                                                                    <?php echo $single_data['status']; ?>
                                                                                </span>
                                                                            </div>
                                                                            <div style="width:500px; display: inline-block;">
                                                                                <span  style="font-family: cursive; padding-bottom: 30px; font-weight: 700 !important;">
                                                                                    <?php echo $single_data['comment']; ?>
                                                                                </span>
                                                                            </div>
                                                                            <div style=""><hr style="margin-top: 0rem;margin-bottom: 0rem;border: 0;border-top: 3px solid rgba(0, 0, 0, 0.1);"></div>
                                                                        <?php } ?>
                                                                        <div style=""><hr style="margin-top: 0rem;margin-bottom: 0rem;border: 0;border-top: 3px solid rgba(0, 0, 0, 0.1);"></div>
                                                                        <div style=""><hr style="margin-top: 0rem;margin-bottom: 3rem;border: 0;border-top: 3px solid rgba(0, 0, 0, 0.1);"></div>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                        <!--end:: Portlet-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end:: Content -->
                     </div>
                </div>
            </div>
        </div>

        <!-- end:: Page -->
	</body>
    <!-- end::Body -->
</html>