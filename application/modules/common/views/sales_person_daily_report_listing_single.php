<?php if(!empty($user_list)) {?>
	<div class="col-xl-6">
		<a href="javascript:void(0);" class="kt-font-boldest">Call Connected: 
            <i class="kt-font-<?php echo $call_connect_color;?>">
            <?php echo $user_list['call']; ?>
            </i>
        </a>
	</div>
    <div class="col-xl-6">
		<a href="javascript:void(0);" class="kt-font-boldest">Call Attempted:
            <i class="kt-font-<?php echo $call_attempt_color;?>">
            <?php echo $user_list['call_attempted']; ?>
            </i>
        </a>
	</div>
    <div class="col-xl-6">
        <a href="javascript:void(0);" class="kt-font-boldest">Linkedin:
            <i class="kt-font-<?php echo $linkedin_color;?>">
            <?php echo $user_list['linkedin']; ?>
            </i>
        </a>
    </div>
    <div class="col-xl-6">
        <a href="javascript:void(0);" class="kt-font-boldest">Whatsapp: 
            <i class="kt-font-<?php echo $whatsapp_color;?>">
            <?php echo $user_list['whatsapp']; ?>
            </i>
        </a>
    </div>
    <div class="col-xl-12">
        <a href="javascript:void(0);" class="kt-font-boldest">Total: 
            <i class="kt-font-<?php echo $whatsapp_color;?>">
            <?php echo '('.$user_submitted_count.'/'.$user_protocol_count.')'; ?>
            </i>
        </a>
    </div>
    <div class="col-xl-12">
        <a href="javascript:void(0);" class="kt-font-boldest">Email:
            <i class="kt-font-<?php echo $email_color;?>">
            <?php echo '('.$user_list['email'].'/'.$user_protocol_count_email.')'; ?>
            </i>
        </a>
    </div>
    <div class="col-xl-12">
        <a href="javascript:void(0);" class="kt-font-boldest">Desktrack: 
            <i class="kt-font-<?php echo $desktrack_color;?>">
            <?php echo $user_list['desktrack']; ?>
            </i>
        </a>
    </div>
    <div class="col-xl-12">
        <a href="javascript:void(0);" class="kt-font-boldest">Total Desktrack: 
            <i class="kt-font-<?php echo $desktrack_color;?>">
            <?php echo $user_protocol_count_desktrack; ?>
            </i>
        </a>
    </div>    
<?php } ?>