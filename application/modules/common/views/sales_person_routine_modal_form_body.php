<div class="row row-no-padding row-col-separator-xl">
    <div class="col-md-6">
        <div class="tab-pane active" id="kt_widget2_tab1_content">
            <h5 class="tab-pane" id="exampleModalLabel">Routine allowed</h5><br/>
            <div class="kt-widget2">
                <?php foreach($routine_list['routine_assign'] as $routine_key => $single_routine){?>
                <div class="kt-widget2__item <?php echo $single_routine['color']?>">
                    <div class="kt-widget2__checkbox">
                        <label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
                            <input class="routine_user_id" type="checkbox" user_id="<?php echo $user_id;?>" value="<?php echo $single_routine['id']?>" name="<?php echo $single_routine['id']?>" <?php echo $single_routine['selected']?>>
                            <span></span>
                        </label>
                    </div>
                    <div class="kt-widget2__info">
                        <a href="javascript:void(0);" class="kt-widget2__title">
                            <?php echo $single_routine['name']?>
                        </a>
                        <a href="javascript:void(0);" class="kt-widget2__title">
                        <?php if(!empty($single_routine['work_completed'])){ ?>
                                Status: <?php echo $single_routine['work_completed']; ?>
                            <?php }if(!empty($single_routine['work_comment'])){ ?>    
                                Comment: <?php echo $single_routine['work_comment']; ?>
                            <?php } ?>
                        </a>
                    </div>
                    <div class="kt-widget2__actions"></div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="tab-pane active" id="kt_widget2_tab1_content">
            <h5 class="tab-pane" id="exampleModalLabel">Routine not allowed</h5><br/>
            <div class="kt-widget2">
                <?php foreach($routine_list['routine_unassign'] as $routine_key => $single_routine){?>
                <div class="kt-widget2__item <?php echo $single_routine['color']?>">
                    <div class="kt-widget2__checkbox">
                        <label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
                            <input class="routine_user_id" type="checkbox" user_id="<?php echo $user_id;?>" value="<?php echo $single_routine['id']?>" name="<?php echo $single_routine['id']?>" <?php echo $single_routine['selected']?>>
                            <span></span>
                        </label>
                    </div>
                    <div class="kt-widget2__info">
                        <a href="javascript:void(0);" class="kt-widget2__title">
                            <?php echo $single_routine['name']?>
                        </a>
                        <a href="javascript:void(0);" class="kt-widget2__title">
                            <?php if(!empty($single_routine['work_completed'])){ ?>
                                Status: <?php echo $single_routine['work_completed']; ?>
                            <?php }if(!empty($single_routine['work_comment'])){ ?>    
                                Comment: <?php echo $single_routine['work_comment']; ?>
                            <?php } ?>
                        </a>
                    </div>
                    <div class="kt-widget2__actions"></div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
