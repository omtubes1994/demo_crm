<?php if(!empty($paggination_data)){ ?>
	<div class="col-sm-2">
	    <input type="text" id="limit" value="<?php echo $paggination_data['limit'];?>" hidden>
	    <input type="text" id="offset" value="<?php echo $paggination_data['offset'];?>" hidden>
	    <div class="dataTables_info" id="kt_table_1_info" role="status" aria-live="polite" style="font-weight: 500; padding-right: 50px;">
	    	<?php if(!empty($paggination_data['limit'])) {?>
	    		Showing <?php echo $paggination_data['offset'] +1; ?> to <?php echo ($paggination_data['limit'] + $paggination_data['offset']); ?> of <?php echo $paggination_data['total_rows']; ?> entries</div>
	    	<?php } else { ?>
	    		Showing <?php echo $paggination_data['total_rows']; ?> of <?php echo $paggination_data['total_rows']; ?> entries</div>
	    	<?php }?>
	</div>
	<div class="col-sm-10 dataTables_pager">
	    <div class="dataTables_length" id="kt_table_1_length">
	        <label style="font-weight: 500;">Display
	            <select name="paggination_limit" aria-controls="kt_table_1" id="set_limit" class="custom-select custom-select-sm form-control form-control-sm" style="width: 50% !important;">
	                <option value="5" <?php echo ($paggination_data['limit'] == 5) ? 'selected': ''; ?>>5</option>
	                <option value="10" <?php echo ($paggination_data['limit'] == 10) ? 'selected': ''; ?>>10</option>
	                <option value="25" <?php echo ($paggination_data['limit'] == 25) ? 'selected': ''; ?>>25</option>
	                <?php if(!in_array($paggination_data['limit'], array(5, 10, 25))) { ?>
	                	<?php if($paggination_data['limit'] == 0) { ?>
		                	<option value="<?php echo $paggination_data['total_rows'];?>" selected><?php echo $paggination_data['total_rows'];?></option>
	                	<?php }?>
		            <?php }else{ ?>
	                	<option value="<?php echo $paggination_data['total_rows'];?>" <?php echo ($paggination_data['limit'] == $paggination_data['total_rows']) ? 'selected': ''; ?>><?php echo $paggination_data['total_rows'];?></option>
            		<?php } ?>    	
	            </select>
	        </label>
	    </div>
	    <?php if(!empty($paggination_data['limit'])) {?>
		    <div class="dataTables_paginate paging_simple_numbers" id="kt_table_1_paginate">
		        <ul class="pagination">
		            <?php
		                $last_page_offset = ((round($paggination_data['total_rows']/$paggination_data['limit'])*$paggination_data['limit']));
		                if($paggination_data['offset'] < $paggination_data['limit']*4) {

		                    $start_value = $paggination_data['limit'];
		                    $first_blank=false;
		                    $last_blank=true;
		                    $increment_amount = $start_value + $paggination_data['limit']*4;
		                } elseif($paggination_data['offset'] >= $paggination_data['limit']*4 && $paggination_data['offset'] < ($last_page_offset-$paggination_data['limit']*4)) {

		                    $start_value = $paggination_data['offset'] - $paggination_data['limit'];
		                    $first_blank=true;
		                    $last_blank=true;
		                    $increment_amount = $start_value + $paggination_data['limit']*3;
		                } elseif($paggination_data['offset'] >= $paggination_data['limit']*4 && $paggination_data['offset'] >= ($last_page_offset-$paggination_data['limit']*4)) {

		                    $start_value = $last_page_offset-$paggination_data['limit']*5;
		                    $first_blank=true;
		                    $last_blank=false;
		                    $increment_amount = $start_value + $paggination_data['limit']*4;
		                }
		                if($increment_amount > $paggination_data['total_rows']) {

		                    $last_blank=false;
		                }
		            ?>
		            <?php if($paggination_data['offset'] > 0) {?>
		                <li class="paginate_button page-item previous listing_paggination_number" id="kt_table_1_previous" limit="<?php echo $paggination_data['limit']; ?>" offset="<?php echo $paggination_data['offset']-$paggination_data['limit']; ?>">
		                    <a href="javascript:void(0)" aria-controls="kt_table_1" data-dt-idx="0" tabindex="0" class="page-link">
		                        <i class="la la-angle-left"></i>
		                    </a>
		                </li>
		            <?php }?>   
		            <li class="paginate_button page-item listing_paggination_number <?php echo($paggination_data['offset'] == 0)? 'active': ''?>" limit="<?php echo $paggination_data['limit']; ?>" offset="<?php echo 00; ?>">
		                <a href="javascript:;" aria-controls="kt_table_1" data-dt-idx="1" tabindex="0" class="page-link">1</a>
		            </li>
		            <?php if($first_blank) {?>
		                <li class="paginate_button page-item disabled" id="kt_table_1_ellipsis">
		                    <a href="javascript:void(0)" aria-controls="kt_table_1" class="page-link">…</a>
		                </li>
		            <?php }?>
		            <?php for ($i=$start_value; $i < $increment_amount; $i+=$paggination_data['limit']) { ?>
		                <?php if($i < $paggination_data['total_rows']) { ?>
		                <li class="paginate_button page-item listing_paggination_number <?php echo($paggination_data['offset'] == $i)? 'active': ''?>" limit="<?php echo $paggination_data['limit']; ?>" offset="<?php echo $i; ?>">
		                    <a href="javascript:;" aria-controls="kt_table_1" class="page-link"><?php echo ($paggination_data['limit']+$i)/$paggination_data['limit']; ?></a>
		                </li>
		                <?php } ?>
		            <?php } ?>
		            <?php if($last_blank) {?>
		                <li class="paginate_button page-item disabled" id="kt_table_1_ellipsis">
		                    <a href="javascript:void(0)" aria-controls="kt_table_1" class="page-link">…</a>
		                </li>
		            <?php }?>
		            <li class="paginate_button page-item listing_paggination_number <?php echo($paggination_data['offset'] == ($last_page_offset-$paggination_data['limit']))? 'active': ''?>" limit="<?php echo $paggination_data['limit']; ?>" offset="<?php echo ($last_page_offset-$paggination_data['limit']); ?>">
		                <a href="javascript:void(0)" aria-controls="kt_table_1" class="page-link"><?php echo round($last_page_offset/$paggination_data['limit']); ?></a>
		            </li>
		            <?php if($paggination_data['offset'] < $last_page_offset-$paggination_data['limit']) {?>
		                <li class="paginate_button page-item next listing_paggination_number" id="kt_table_1_next" limit="<?php echo $paggination_data['limit']; ?>" offset="<?php echo ($paggination_data['offset']+$paggination_data['limit']); ?>">
		                    <a href="javascript:void(0)" aria-controls="kt_table_1" class="page-link">
		                        <i class="la la-angle-right"></i>
		                    </a>
		                </li>
		            <?php } ?>  
		        </ul>
		    </div>
		<?php }?>
	</div>
<?php }?>