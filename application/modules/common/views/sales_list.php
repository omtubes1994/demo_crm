<table class="kt-datatable__table" style="display: block;">
    <thead class="kt-datatable__head">
        <tr class="kt-datatable__row" style="left: 0px;">
            <th class="kt-align-center" style="width: 5%;"><span>ID</span></th>
            <th class="kt-align-center" style="width: 10%;"><span>User Name</span></th>
            <th class="kt-align-left" style="" colspan="6"><span>Lead Count</span></th>
            <th class="kt-align-center" style="width: 10%;"><span>Action</span></th>
        </tr>
    </thead>
    <tbody class="kt-datatable__body" style="">
        <?php if(!empty($lead_data)) { ?>
            <?php foreach($lead_data as $lead_key => $lead_details) {?>
                <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                    <td class="kt-datatable__cell kt-align-center" style="width: 5%;"><span><?php echo $lead_key+1; ?></span></td>
                    <td class="kt-datatable__cell kt-align-center" style="width: 10%;"><span><?php echo $lead_details['name']; ?></span></td>       
                    <td data-autohide-disabled="false" class="kt-datatable__cell kt-align-left">
                        <span>
                            Primary Leads - Piping Products
                            <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/pipes/customer_mst'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['pipes']; ?></a>
                        </span>
                        <span>
                            Piping Shipyards Companies
                            <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/shipyards/customer_mst'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['shipyards']; ?></a>
                        </span>
                        <span>
                            Piping Chemical Companies
                            <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/chemical companies/customer_mst'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['chemical companies']; ?></a>
                        </span>
                        <span>
                            Piping Water Companies
                            <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/water companies/customer_mst'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['water companies']; ?></a>
                        </span>
                    </td>
                    <td data-autohide-disabled="false" class="kt-datatable__cell kt-align-left">
                        <span>
                            Piping EPC Companies
                            <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/epc companies/customer_mst'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['epc companies']; ?></a>
                        </span>
                        <span>
                            Piping Sugar Companies
                            <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/sugar companies/customer_mst'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['sugar companies']; ?></a>
                        </span>
                        <span>
                            Primary Leads - Tube Fittings & Valves
                            <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/tubes/customer_mst'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['tubes']; ?></a>
                        </span>
                        <span>
                            Primary Leads - Process Control
                            <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/process_control/customer_mst'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['process_control']; ?></a>
                        </span>
                    </td>
                    <td data-autohide-disabled="false" class="kt-datatable__cell kt-align-left">
                        <span>
                            Tube Fittings Distributors
                            <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/distributors/customer_mst'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['distributors']; ?></a>
                        </span>
                        <span>
                            Tube Fittings & Valves India
                            <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/heteregenous tubes india/customer_mst'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['heteregenous tubes india']; ?></a>
                        </span>
                        <span>
                            Instrumentation PVF Companies
                            <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/pvf companies/customer_mst'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['pvf companies']; ?></a>
                        </span>
                        <span>
                            Primary Leads - Tubing
                            <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/tubing/customer_mst'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['tubing']; ?></a>
                        </span>
                    </td>
                    <td data-autohide-disabled="false" class="kt-datatable__cell kt-align-left">
                        <span>
                            Primary Leads - Hammer Union
                            <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/hammer union/customer_mst'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['hammer union']; ?></a>
                        </span>
                        <span>
                            Forged Fittings
                            <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/forged fittings/customer_mst'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['forged fittings']; ?></a>
                        </span>
                        <span>
                            Miscellaneous Leads
                            <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/miscellaneous leads/customer_mst'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['miscellaneous leads']; ?></a>
                        </span>
                        <span>
                            Internal Data
                            <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/internal_data/customer_mst'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['internal_data']; ?></a>
                        </span>
                    </td>
                    <td data-autohide-disabled="false" class="kt-datatable__cell kt-align-left">
                        <span>
                            Pre Qualification
                            <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/pre_qualifiction/customer_mst'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['pre_qualifiction']; ?></a>
                        </span>
                        <span>
                            Purchase Order
                            <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/""/procurement'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['procurement']; ?></a>
                        </span>
                        <span>RFQ
                            <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/""/rfq_mst'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['rfq']; ?></a>
                        </span>
                        <span>
                            Query
                            <span class="kt-font-bold kt-font-primary"><?php echo $lead_details['query']; ?></span>
                        </span>
                    </td>
                    <td data-autohide-disabled="false" class="kt-datatable__cell kt-align-left">
                        <span>
                            Quotation
                            <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/""/quotation_mst'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['quotations']; ?></a>
                        </span>
                        <span>
                            Vendor
                            <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/""/vendors'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['vendor']; ?></a>
                        </span>
                        <span>
                            Hydraulic fitting
                            <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/Hydraulic fitting/customer_mst'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['Hydraulic fitting']; ?></a>
                        </span>
                        <span>
                            Fasteners
                            <a href="<?php echo base_url('common/view_user_data/'.$lead_details['user_id'].'/Fasteners/customer_mst'); ?>" target="_blank" class="kt-font-bold kt-font-primary"><?php echo $lead_details['Fasteners']; ?></a>
                        </span>
                    </td>
                    <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell kt-align-center" style=" width: 10%;">
                        <span style="overflow: visible; position: relative;">
                            <a href="<?php echo base_url('common/view_user_data/'), $lead_details['user_id'];?>" target="_blank" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit details">
                                <i class="la la-edit"></i>
                            </a>
                        </span>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
    </tbody>
</table>