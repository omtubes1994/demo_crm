<style type="text/css">
    .kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
        padding-top: 4% !important;
    }
    .layer-white{
        display: none;
        position: absolute;
        top: 0em !important;
        left: 0em !important;
        width: 100%;
        height: 100%;
        text-align: center;
        vertical-align: middle;
        background-color: rgba(255, 255, 255, 0.55);
        opacity: 1;
        line-height: 1;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
        -webkit-animation-duration: 0.5s;
        animation-duration: 0.5s;
        -webkit-transition: background-color 0.5s linear;
        transition: background-color 0.5s linear;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        will-change: opacity;
        z-index: 9;
    }
    .div-loader{
        position: absolute;
        top: 50%;
        left: 50%;
        margin: 0px;
        text-align: center;
        z-index: 1000;
        -webkit-transform: translateX(-50%) translateY(-50%);
        transform: translateX(-50%) translateY(-50%);
    }
    .kt-spinner:before {
        width: 50px;
        height: 50px;
        margin-top: -10px;
    }
    .kt-aside--fixed .kt-wrapper {
        padding-left: 130px !important;
    }
    .kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper {
        padding-top: 30px !important;
    }
    .salary_slip thead tr th{
        background-color: transparent;
        padding: 1rem 0 0.5rem 0;
        color: #74788d;
        border-top: 0;
        border-bottom: 1px solid #ebedf2;
    }
    .salary_slip tbody tr td{
        background-color: transparent;
        padding: 1rem 0 1rem 0;
        border-top: none;
        font-weight: 700;
        font-size: 1.1rem;
        color: #595d6e;
        padding-top: 1.8rem;
    }
    .grand_total thead tr th{
        font-size: 1.1rem;
        text-transform: capitalize;
        font-weight: 500;
        color: #74788d;
        border-top: 0;
        border-bottom: 1px solid #ebedf2;
        padding: 10px 10px 10px 0;
        background-color: transparent;
    }
    .grand_total tbody tr td{

        font-size: 1.1rem;
        text-transform: capitalize;
        background-color: transparent;
        font-weight: 500;
        color: #595d6e;
        padding: 10px 10px 10px 0;
    }
</style>
<!-- end:: Header Mobile -->
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

               <!-- begin:: Content -->
               <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

                   <!--Begin::App-->
                   <div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">

                       <!--Begin:: App Aside Mobile Toggle-->
                       <button class="kt-app__aside-close" id="kt_user_profile_aside_close">
                           <i class="la la-close"></i>
                       </button>

                       <!--End:: App Aside Mobile Toggle-->

                       <!--Begin:: App Content-->
                       <div class="kt-grid__item kt-grid__item--fluid kt-app__content listing">
                            <div class="row" style="display:block;">
                                <div class="kt-portlet ">
                                    <div class="kt-portlet__head">
                                        <div class="kt-portlet__head-label">
                                            <h3 class="kt-portlet__head-title">
                                                All Lead Count
                                            </h3>
                                        </div>
                                        <div class="kt-portlet__head-toolbar">
                                            
                                        </div>
                                    </div>
                                    <?php if(!empty($all_lead_details)){ ?>
                                    	<?php $i=0;$show_value="show";?>
                                    <div class="kt-portlet__body">
                                		<?php foreach ($all_lead_details as $lead_name => $lead_list) { ?>
                                			
                                        	<!--begin::Accordion-->
	                                        <div class="accordion accordion-solid accordion-toggle-plus" id="accordionExample<?php echo $i;?>">
	                                            <div class="card">
	                                                <div class="card-header" id="headingOne<?php echo $i;?>">
	                                                    <div class="card-title" data-toggle="collapse" data-target="#collapseOne<?php echo $i;?>" aria-expanded="true" aria-controls="collapseOne<?php echo $i;?>">
	                                                     <?php echo $lead_name;?>
	                                                    </div>
	                                                </div>
	                                                <div id="collapseOne<?php echo $i;?>" class="collapse <?php echo $show_value;?>" aria-labelledby="headingOne<?php echo $i;?>" data-parent="#accordionExample<?php echo $i;?>" style="">
	                                                    <div class="card-body">
	                                                        <div class="row">   
	                                                            <div class="col-xl-12">
	                                                                <div class="table-responsive">
	                                                                    <table class="table salary_slip grand_total">
	                                                                        <thead>
	                                                                            <tr>
	                                                                                <th>Sr.No</th>
	                                                                                <th>Sales Person</th>
	                                                                                <th>Total Leads</th>
	                                                                            </tr>
	                                                                        </thead>
	                                                                        <tbody>
	                                                                        	<?php foreach ($lead_list as $lead_list_key => $lead_list_single) { ?>
	                                                                            <tr>
	                                                                                <td><?php echo $lead_list_key+1; ?></td>
	                                                                                <td><?php echo $user_details[$lead_list_single['user_id']]; ?></td>
	                                                                                <td class="kt-font-primary kt-font-xl kt-font-boldest"><?php echo (int)$lead_list_single['count']; ?></td>
	                                                                            </tr>
	                                                                            <?php } ?>
	                                                                            <tr>
	                                                                                <td></td>
	                                                                                <td class="kt-font-danger kt-font-xl kt-font-boldest"><?php echo "Total"; ?></td>
	                                                                                <td class="kt-font-danger kt-font-xl kt-font-boldest"><?php echo (int)array_sum(array_column($lead_list, 'count')); ?></td>
	                                                                            </tr>
	                                                                        </tbody>
	                                                                    </table>
	                                                                </div>
	                                                            </div>  
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
                                        	<!--end::Accordion-->
                                    	<?php $i++;$show_value="";?>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div id="hr_edit_listing_table_loader" class="layer-white">
                                <div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
                            </div>
                        </div>

                       <!--End:: App Content-->
                   </div>

                   <!--End::App-->
               </div>

               <!-- end:: Content -->
            </div>

            <!-- begin:: Footer -->
            <div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
                <div class="kt-container  kt-container--fluid ">
                    <div class="kt-footer__copyright">
                        2020&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Keenthemes</a>
                    </div>
                    <div class="kt-footer__menu">
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">About</a>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Team</a>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Contact</a>
                    </div>
                </div>
            </div>

            <!-- end:: Footer -->
        </div>
    </div>
</div>

<!-- end:: Page -->

<!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="fa fa-arrow-up"></i>
</div>

<!-- end::Scrolltop -->