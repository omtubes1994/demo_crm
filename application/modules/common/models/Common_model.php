<?php 
Class Common_model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->db2 = $this->load->database('marketing', true);
		// $this->db = $this->load->database('crm_sales', true);
		// $this->db2 = $this->load->database('crm_marketing', true);
		if($this->session->userdata('user_id') == 65) {

		}
	}

	public function insert_data_sales_db($table_name, $insert_data, $insert_type = 'single_insert'){

		if($insert_type == 'single_insert') {

			$this->db->insert($table_name, $insert_data);
			return $this->db->insert_id();
		} else if ($insert_type == 'batch') {

			$this->db->insert_batch($table_name, $insert_data);
		}
	}

	public function update_data_sales_db($table_name, $update_data, $where_array, $update_type = 'single_update'){
		
		if($update_type == 'single_update') {

			return $this->db->update($table_name, $update_data, $where_array);
		} else if ($update_type == 'batch') {

			$this->db->update_batch($table_name, $update_data, $where_array);	
		}
	}

	public function insert_data_marketing_db($table_name, $insert_data, $insert_type = 'single_insert'){
		
		if($insert_type == 'single_insert') {

			$this->db2->insert($table_name, $insert_data);
			return $this->db2->insert_id();
		} else if ($insert_type == 'batch') {

			$this->db2->insert_batch($table_name, $insert_data);
		}
	}

	public function update_data_marketing_db($table_name, $update_data, $where_array, $update_type = 'single_update'){
		if($update_type == 'single_update') {

			return $this->db2->update($table_name, $update_data, $where_array);
		} else if ($update_type == 'batch') {

			$this->db2->update_batch($table_name, $update_data, $where_array);	
		}
	}

	public function get_all_conditional_data_sales_db($select_string = '*', $where_array, $table_name, $return_type = 'result_array', $join_array = array(), $order_by_array = array(), $group_by_array = array(), $limit = 0, $offset = 0) {

		$this->db->select($select_string);
		$this->db->where($where_array);
		if(!empty($join_array)) {
			$this->db->join($join_array['table_name'], $join_array['condition'], $join_array['type']);
		}
		if(!empty($group_by_array)) {
			$this->db->group_by($group_by_array);
		}
		if(!empty($order_by_array)) {
			$this->db->order_by($order_by_array['column_name'], $order_by_array['column_value']);
		}
		return $this->db->get($table_name, $limit, $offset)->$return_type();
	}

	public function get_dynamic_data_sales_db($select_string = '*', $where_array, $table_name, $return_type = 'result_array', $limit = 0, $offset = 0) {

		$this->db->select($select_string);
		$this->db->where($where_array);
		return $this->db->get($table_name, $limit, $offset)->$return_type();
	}

	public function get_reporting_to_all_data() {
		$data = array();
		$this->db->where('status', 1);
		$this->db->order_by('name', 'ASC');
		$data = $this->db->get('users')->result_array();
		return $data;
	}

	public function get_organization_data() {
		$this->db->select('user_organization_chart.id,user_organization_chart.reporting_manager_id,users.user_id,users.name');
		$this->db->join('users ', 'user_organization_chart.reporting_manager_id = users.user_id');
		// $this->db->where($where_string);
		$this->db->group_by('user_organization_chart.reporting_manager_id');
		return $this->db->get('user_organization_chart`')->result_array();
	}

	public function get_dynamic_data_sales_db_null_false($select_string = '*', $where_array, $table_name, $return_type = 'result_array', $limit = 0, $offset = 0) {

		$this->db->select($select_string);
		$this->db->where($where_array, null, false);
		return $this->db->get($table_name, $limit, $offset)->$return_type();
	}

	public function get_dynamic_data_marketing_db_null_false($select_string = '*', $where_array, $table_name, $return_type = 'result_array', $limit = 0, $offset = 0) {

		$this->db2->select($select_string);
		$this->db2->where($where_array, null, false);
		return $this->db2->get($table_name, $limit, $offset)->$return_type();
	}

	public function get_all_conditional_data_marketing_db($select_string = '*', $where_array, $table_name, $return_type = 'result_array', $join_array = array(), $order_by_array = array(), $group_by_array = array(), $limit = 0, $offset = 0) {

		$this->db2->select($select_string);
		$this->db2->where($where_array);
		if(!empty($group_by_array)) {
			$this->db2->group_by($group_by_array);
		}
		if(!empty($order_by_array)) {
			$this->db2->order_by($order_by_array['column_name'], $order_by_array['column_value']);
		}
		return $this->db2->get($table_name, $limit, $offset)->$return_type();
	}

	public function get_dynamic_data_marketing_db($select_string = '*', $where_array, $table_name, $return_type = 'result_array', $limit = 0, $offset = 0) {

		$this->db2->select($select_string);
		$this->db2->where($where_array);
		return $this->db2->get($table_name, $limit, $offset)->$return_type();
	}

	public function get_sales_person($select= '*', $sales_person_type= 'all') {

		$this->db->select($select);
		$role_array = array(5, 16);
		if($sales_person_type == 'sales_admin') {

			$role_array = array(16);
		} else if ($sales_person_type == 'sales_people'){

			$role_array = array(5);
		}
		$this->db->where_in('role', $role_array);
		return $this->db->get_where('users', array('status'=>1))->result_array();
	}

	public function get_procurement_person($select= '*', $procurement_person_type= 'all') {

		$this->db->select($select);

		$role_array = array(6, 8);
		if($procurement_person_type == 'procurement_admin') {

			$role_array = array(6);
		} else if ($procurement_person_type == 'procurement_people'){

			$role_array = array(8);
		}
		$this->db->where_in('role', $role_array);
		return $this->db->get_where('users', array('status'=>1))->result_array();
	}

	// public function get_lead_module_name($main_module, $sub_module) {

	// 	$return_array = array();
	// 	if($main_module == 'yes') {

	// 		$this->db->select('module_id, module_name');
	// 		$this->db->where_in('module_id', array('5', '6', '7'));
	// 		$return_array['main_module_details'] = $this->db->get('modules')->result_array();
	// 	}
	// 	if($sub_module == 'yes') {

	// 		$this->db->select('module_id, sub_module_id, sub_module_name, url');
	// 		$this->db->where_in('module_id', array('5', '6', '7'));
	// 		$return_array['sub_module_details'] = $this->db->get('sub_module')->result_array();
	// 	}
	// 	return $return_array;
	// }

	public function get_lead_module_name($main_module, $sub_module) {

		$return_array = array();
		if($main_module == 'yes') {

			$this->db->select('module_id, module_name');
			$this->db->where_in('module_id', array('28'));
			$return_array['main_module_details'] = $this->db->get('modules')->result_array();
		}
		if($sub_module == 'yes') {

			$this->db->select('module_id, sub_module_id, sub_module_name, url');
			$this->db->where_in('module_id', array('28'));
			$return_array['sub_module_details'] = $this->db->get('sub_module')->result_array();
		}
		return $return_array;
	}

	public function get_country_list() {

		return $this->db->get_where('country_mst', array('status'=>'Active'))->result_array();
	}

	public function get_region_list() {

		return $this->db->get_where('region_mst', array('status'=>'Active'))->result_array();
	}

	// Manpower query's

	public function get_manpower_list($where_array, $limit, $offset){

		$this->db->select('SQL_CALC_FOUND_ROWS *', FALSE);
		$this->db->where($where_array);
	   $this->db->order_by('id', 'asc');
	   $return_array['manpower_list'] = $this->db->get('manpower_requisition_form', $limit, $offset)->result_array();
		// echo "<pre>";print_r($this->db->last_query());echo"</pre><hr>";exit;
	   $return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows;")->row_array();
	   return $return_array;
    }

	// candidate query's
	public function get_candidate_list($where_array, $order_by_array, $limit, $offset){

		$this->db->select('SQL_CALC_FOUND_ROWS *', FALSE);
		$this->db->where($where_array);
	    $this->db->order_by($order_by_array['column_name'], $order_by_array['column_value']);
	   $return_array['candidate_list'] = $this->db->get('candidate', $limit, $offset)->result_array();
	   $return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows;")->row_array();
	   return $return_array;
    }

	// end candidate query's
	public function create_table_dump() {
		$this->db = $this->load->database('crm_sales', true);
		// $this->db = $this->load->database('crm_marketing', true);
		$table_array = $this->uri->segment('3', array());
		// if(empty($table_array)) {

		// 	$flag = $this->uri->segment('4', false);
		// 	if(!$flag) {
		// 		echo "<pre>";print_r($table_array);echo"</pre><hr>";exit;
		// 	}
		// }
	    // $all_tables = $this->db->query('SHOW TABLES')->result_array();
	    // echo "<pre>";print_r($all_tables);echo"</pre><hr>";exit;
	    // $array_chunk = array_chunk(array_column($all_tables, 'Tables_in_qmznzeyjub'), 20);
	    // echo "<pre>";print_r($array_chunk);echo"</pre><hr>";exit;
	    // echo "<pre>";print_r(array_chunk(array_column($all_tables, 'Tables_in_qmznzeyjub'), 20));echo"</pre><hr>";exit;
	    
	    // Load the DB utility class
	    $this->load->dbutil($this->db);
	    // foreach ($all_tables as $table_name) {
	            
	                 
	      // Backup your entire database and assign it to a variable
	      $prefs = array(
	              'tables'        => $table_array,   // Array of tables to backup.
	              'ignore'        => array(),                     // List of tables to omit from the backup
	              'format'        => 'zip',                       // gzip, zip, txt
	              'filename'      => 'sales_db_bckp.sql',  // File name - NEEDED ONLY WITH ZIP FILES
	              'add_drop'      => TRUE,                        // Whether to add DROP TABLE statements to backup file
	              'add_insert'    => TRUE,                        // Whether to add INSERT data to backup file
	              'newline'       => "\n"                         // Newline character used in backup file
	      );

	      $backup = $this->dbutil->backup($prefs);
	      // echo "<pre>";print_r($backup);echo"</pre><hr>";exit;
	      // $this->load->helper('file');
	      // echo "<pre>";var_dump(write_file('/public_html/assets/db_bckp', $backup));echo"</pre><hr>";
	      $this->load->helper('download');
	      force_download('sales_db_bckp.zip', $backup);
	      die('done one process');
	    // }


	    // Load the download helper and send the file to your desktop
	    // $this->load->helper('download');
	    // force_download('sales_db_bckp.zip', $backup);
	}

	public function create_table_dump_marketing() {

		$this->db = $this->load->database('crm_marketing', true);
		$table_array = $this->uri->segment('3', array());
	    $this->load->dbutil($this->db);
	    // foreach ($all_tables as $table_name) {
	            
	                 
	      // Backup your entire database and assign it to a variable
	      $prefs = array(
	              'tables'        => $table_array,   // Array of tables to backup.
	              'ignore'        => array(),                     // List of tables to omit from the backup
	              'format'        => 'zip',                       // gzip, zip, txt
	              'filename'      => 'sales_db_bckp.sql',  // File name - NEEDED ONLY WITH ZIP FILES
	              'add_drop'      => TRUE,                        // Whether to add DROP TABLE statements to backup file
	              'add_insert'    => TRUE,                        // Whether to add INSERT data to backup file
	              'newline'       => "\n"                         // Newline character used in backup file
	      );

	      $backup = $this->dbutil->backup($prefs);
	      // echo "<pre>";print_r($backup);echo"</pre><hr>";exit;
	      // $this->load->helper('file');
	      // echo "<pre>";var_dump(write_file('/public_html/assets/db_bckp', $backup));echo"</pre><hr>";
	      $this->load->helper('download');
	      force_download('sales_db_bckp.zip', $backup);
	      die('done one process');
	    // }


	    // Load the download helper and send the file to your desktop
	    // $this->load->helper('download');
	    // force_download('sales_db_bckp.zip', $backup);
	}

	public function get_all_lead_details($table_name, $lead_type) {

		$return_array = array();
		if($table_name == 'lead_mst') {

			$return_array = $this->db2->get_where($table_name,array('data_category'=>$lead_type))->result_array();
		}elseif ($table_name == 'clients') {
			
			$return_array = $this->db->get_where($table_name,array('source'=>$lead_type))->result_array();
		}
		return $return_array;
	}

	public function lead_of_box_yes($lead_type, $lead_source) {

		if(empty($lead_type) || empty($lead_source)) {
			die('Lead Type or Lead Source not given!!!');
		}
		$csv_file = '';
		if($lead_type == 'hetregenous_leads') {

			$this->db = $this->load->database('crm_sales', true);
		    // Load the DB utility class
		    $this->load->dbutil($this->db);

			$query = $this->db->query("
									SELECT clients.client_name, lookup.lookup_value as Country_name, clients.box_comment, users.name as Sales_person 
									FROM clients
									LEFT JOIN lookup ON lookup.lookup_id =  clients.country
									LEFT JOIN users ON users.user_id =  clients.assigned_to
									WHERE clients.deleted is null AND clients.source = '{$lead_source}' AND clients.box = 'Yes'
									");

			$csv_file =  $this->dbutil->csv_from_result($query);

		} else if($lead_type == 'primary_leads') {

			$this->db = $this->load->database('crm_marketing', true);
			// Load the DB utility class
		    $this->load->dbutil($this->db);

			$query = $this->db->query("
									SELECT lead_mst.IMPORTER_NAME as client_name, lead_mst.COUNTRY_OF_DESTINATION as Country_name, lead_mst.box_comment, users.name as Sales_person
									FROM lead_mst
									LEFT JOIN users ON users.user_id =  lead_mst.assigned_to
									WHERE lead_mst.deleted is null AND lead_mst.imp_id > 0 AND lead_mst.data_category = '{$lead_source}' AND lead_mst.box = 'Yes'
									");

			$csv_file =  $this->dbutil->csv_from_result($query);

		}
	    
		if(!empty($csv_file)) {

		    $this->load->helper('download');
		    force_download('lead.csv', $csv_file);

		}
	}

	public function lead_of_box_no($lead_type, $lead_source) {

		if(empty($lead_type) || empty($lead_source)) {
			die('Lead Type or Lead Source not given!!!');
		}
		$csv_file = '';
		if($lead_type == 'hetregenous_leads') {

			$this->db = $this->load->database('crm_sales', true);
		    // Load the DB utility class
		    $this->load->dbutil($this->db);
		    // $query = $this->db->query("
						// 			SELECT clients.client_name, clients.website as website,  users.name as Sales_person 
						// 			FROM clients
						// 			LEFT JOIN lookup ON lookup.lookup_id =  clients.country
						// 			LEFT JOIN users ON users.user_id =  clients.assigned_to
						// 			WHERE clients.deleted is null AND clients.assigned_to IN (28,30) AND clients.source = 'distributors' AND clients.box = 'No' AND clients.lead_stage IN (1,2,3,4,5,6)
						// 			");
     
			$query = $this->db->query("
									SELECT clients.client_name, clients.website as website, clients.box_comment, users.name as Sales_person 
									FROM clients
									LEFT JOIN lookup ON lookup.lookup_id =  clients.country
									LEFT JOIN users ON users.user_id =  clients.assigned_to
									WHERE clients.deleted is null AND clients.source = '{$lead_source}' AND clients.box = 'No' AND clients.lead_stage IN (1,2,3,4,5,6)
									");

			$csv_file =  $this->dbutil->csv_from_result($query);
		   // echo "<pre>";print_r($csv_file);echo"</pre><hr>";exit;

		} else if($lead_type == 'primary_leads') {

			$this->db = $this->load->database('crm_marketing', true);
			// Load the DB utility class
		    $this->load->dbutil($this->db);

			$query = $this->db->query("
									SELECT lead_mst.IMPORTER_NAME as client_name, lead_mst.COUNTRY_OF_DESTINATION as Country_name, lead_mst.box_comment, users.name as Sales_person
									FROM lead_mst
									LEFT JOIN users ON users.user_id =  lead_mst.assigned_to
									WHERE lead_mst.deleted is null AND lead_mst.imp_id > 0 AND lead_mst.data_category = '{$lead_source}' AND lead_mst.box = 'No'
									");

			$csv_file =  $this->dbutil->csv_from_result($query);

		}
	    
		if(!empty($csv_file)) {

		    $this->load->helper('download');
		    force_download('lead.csv', $csv_file);

		}
	}

	public function sales_db_backup() {

		$this->db = $this->load->database('crm_sales', true);
		$table_array = $this->uri->segment('3', array());
	    // Load the DB utility class
	    $this->load->dbutil($this->db);
	            
      	// Backup your entire database and assign it to a variable
      	$prefs = array(
    	  	'tables'        => array('bank_details','blood_group','client_connect','clients','close_reason','competitor_ranks_invoice', 'country','country_flags','currency','daily_task_update','delivery','delivery_time', 'follow_up', 'graph_information','invoice_dtl','invoice_export_info','invoice_mst','invoice_order_charges_information', 'job_location', 'last_quote_created_number','lead_connects','lead_details','lead_industry','lead_management','lead_management_connect','lead_management_details','lead_stage_reasons','lead_stages','lead_type','leads_source','login_verfication','login_verification_ignore','lookup','machine_time','members','modules', 'number_logic','organization_chart','payment_terms','pdf_footer','people_address','people_information', 'people_leaves_info', 'people_request_info','people_reference','procurement','procurement_charges_information','procurement_product_information','production_process_information','query_master','query_details','quotation_dtl','quotation_mst','rfq_mst','role', 'salary_slip_default', 'salary_slip_info','search_engine_access_url','search_engine_primary_data','sub_module','tasks', 'ticket', 'ticket_management_details','transport_mode','trending_invoice','units','user_alert','user_notice','user_to_module','user_to_sub_module','users','users_to_graph','validity','vendor_dtl','vendor_products','vendors', 'video_url_access_details', 'video_url_details', 'zone_to_country'),   // Array of tables to backup.
    	  	// 'ignore'        => array('ac_blocked', 'ac_contacts', 'ac_groupchat', 'ac_guests', 'ac_guestsac_guests_messages', 'ac_messages', 'ac_profiles', 'ac_settings', 'ac_users_messages', 'calendar_events','client_connect_bp','clients_bp', 'clients_old', 'cod_code', 'cod_code_pipe'),
    	  	'ignore'        => array(),
    	  	                  // List of tables to omit from the backup
          	'format'        => 'zip',                       // gzip, zip, txt
          	'filename'      => 'sales_db_bckp.sql',  // File name - NEEDED ONLY WITH ZIP FILES
          	'add_drop'      => TRUE,                        // Whether to add DROP TABLE statements to backup file
          	'add_insert'    => TRUE,                        // Whether to add INSERT data to backup file
    	  	'newline'       => "\n"                         // Newline character used in backup file
      	);

      	$backup = $this->dbutil->backup($prefs);
      	// echo "<pre>";print_r($backup);echo"</pre><hr>";exit;
      	$this->load->helper('download');
      	force_download('sales_db_bckp.zip', $backup);
      	die('done one process');
	}

	public function marketing_db_backup() {

		$this->db = $this->load->database('crm_marketing', true);
		$table_array = $this->uri->segment('3', array());
	    // Load the DB utility class
	    $this->load->dbutil($this->db);
	            
      	// Backup your entire database and assign it to a variable
      	$prefs = array(
    	  	'tables'        => array('lead_mst','lead_detail','lead_connects'),   // Array of tables to backup.
    	  	'ignore'        => array(),
    	  	                  // List of tables to omit from the backup
          	'format'        => 'zip',                       // gzip, zip, txt
          	'filename'      => 'marketing_db_bckp.sql',  // File name - NEEDED ONLY WITH ZIP FILES
          	'add_drop'      => TRUE,                        // Whether to add DROP TABLE statements to backup file
          	'add_insert'    => TRUE,                        // Whether to add INSERT data to backup file
    	  	'newline'       => "\n"                         // Newline character used in backup file
      	);

      	$backup = $this->dbutil->backup($prefs);
      	// echo "<pre>";print_r($backup);echo"</pre><hr>";exit;
      	$this->load->helper('download');
      	force_download('marketing_db_bckp.zip', $backup);
      	die('done one process');
	}

	public function create_lead_count($type) {

		$this->db2->where('deleted is null');
		$this->db2->where('imp_id > ', 0);
		return $this->db2->get_where('lead_mst lm', array('lm.data_category' => $type))->result_array();
	}

	public function create_lead_count_hetro($type) {

		$this->db->where('deleted is null');
		return $this->db->get_where('clients', array('source' => $type))->result_array();
	}

	public function csv_file_converson($quotation_mst_id){
		if(empty($quotation_mst_id)){
			die('quotation_mst_id not given!!!');
		}
		$csv_file = '';

	    // Load the DB utility class
	    $this->load->dbutil($this->db);
		// $query = $this->db->query("
		// 	                        SELECT product.lookup_value as product_name, material.lookup_value as material_name, quotation_dtl.description,quotation_dtl.quantity,units.unit_value as unit,quotation_dtl.unit_rate as cost,quotation_dtl.margin,quotation_dtl.packing_charge,quotation_dtl.unit_price,quotation_dtl.row_price as total_price
		// 								FROM quotation_dtl
		// 								LEFT JOIN  lookup product ON product.lookup_id = quotation_dtl.product_id
		// 								LEFT JOIN  lookup material ON material.lookup_id = quotation_dtl.material_id
		// 								LEFT JOIN  units ON units.unit_id = quotation_dtl.unit
		// 								WHERE quotation_dtl.quotation_mst_id ='{$quotation_mst_id}'
		// 								AND product.status='Active'
		// 								AND material.status='Active'
		// 								AND units.status='Active';
		// 								 ");

		$query = $this->db->query("
			                        SELECT product_mst.name as product_name, material_mst.name as material_name, quotation_dtl.description,quotation_dtl.quantity,units.unit_value as unit,quotation_dtl.unit_rate as cost,quotation_dtl.margin,quotation_dtl.packing_charge,quotation_dtl.unit_price,quotation_dtl.row_price as total_price
										FROM quotation_dtl 
										LEFT JOIN product_mst ON product_mst.id = quotation_dtl.product_id
										LEFT JOIN material_mst ON material_mst.id = quotation_dtl.material_id
										LEFT JOIN  units ON units.unit_id = quotation_dtl.unit
										WHERE quotation_dtl.quotation_mst_id ='{$quotation_mst_id}'
										AND product_mst.status='Active'
										AND material_mst.status='Active'
										AND units.status='Active';
										 ");
		$csv_file =  $this->dbutil->csv_from_result($query);
		// echo "<pre>";print_r($csv_file);echo"</pre><hr>";
		//echo "<pre>";print_r(explode('"', $csv_file));echo"</pre><hr>";
		// $new_csv_file = '';
		// $i = 0;
		// foreach (explode('"', $csv_file) as $csv_key => $csv_value) {
			
		// 	if(!empty($csv_value)){

		// 		if($csv_value != "," && $csv_value != "\n") {

		// 			$new_csv_file[$i] = '"'.$csv_value.'"'; 
		// 			echo $csv_key, "</hr>", $csv_value;die;
		// 		} else {

		// 			$new_csv_file[$i] = $csv_value; 
		// 		}
		// 	}
		// }

		// for($i=1; $i <= count(explode('"', $csv_file)); $i+2){

		// 	if(!empty($csv_file[$i])){
		// 		$new_csv_file .= '"'.$csv_file[$i].'"'; 
		// 		$new_csv_file .= $csv_file[$i+1]; 
		// 	}
		// }
        // echo "<pre>";print_r($csv_file);echo"</pre><hr>";
		if(!empty($csv_file)) {

		    $this->load->helper('download');
		    force_download('quotations.csv', $csv_file);

		}

	}
	//client lead
	// public function get_client_lead_count($where_string) {

	// 	$this->db->select('count(*) as count, source');
	// 	$this->db->where($where_string, NULL, FALSE);
	// 	$this->db->group_by('source');
	// 	return $this->db->get('clients')->result_array();
	// }

	// public function get_client_lead_data($select_string, $where_string) {

	// 	$this->db->select($select_string);
	// 	$this->db->where($where_string, NULL, FALSE);
	// 	return $this->db->get('clients')->result_array();
	// }
	
	public function get_primary_hetro_lead_count($where_string) {

		$this->db->select('count(*) as count, product_category.product_name');
		$this->db->join('customer_data', 'customer_data.customer_mst_id = customer_mst.id');
		$this->db->join('product_category', 'product_category.id = customer_data.product_category_id');
		$this->db->where($where_string, NULL, FALSE);
		$this->db->group_by('product_category.product_name');
		return $this->db->get('customer_mst')->result_array();
	}

	public function get_internal_lead_count($where_string) {

		$this->db->select('count(*) as count');
		$this->db->join('customer_data', 'customer_data.customer_mst_id = customer_mst.id');
		$this->db->where($where_string, NULL, FALSE);
		return $this->db->get('customer_mst')->row_array();
	}

	public function	get_primary_lead_data($where_array){
		
		$return_array = array();
		$this->db->select('customer_mst.id, customer_mst.name, customer_mst.region_id, customer_mst.country_id, customer_mst.assigned_to, customer_mst.website, customer_mst.no_of_employees, customer_mst.lead_type, customer_mst.lead_stage, customer_mst.stage_reason, customer_mst.box, customer_mst.box_comment, customer_mst.sample, customer_mst.sample_comment, customer_mst.lead_priority, customer_mst.priority_reason, customer_mst.entered_by, customer_mst.modified_by, customer_mst.deleted, customer_mst.purchase_factor_1, customer_mst.purchase_factor_2, customer_mst.purchase_factor_3, customer_mst.sales_notes, customer_mst.product_pitch, customer_mst.purchase_comments, customer_mst.margins, customer_mst.delete_reason, customer_mst.status, customer_mst.entered_on, customer_mst.modified_on, customer_data.rank, customer_data.last_purchased, product_category.product_name', FALSE);
		$this->db->join('customer_data', 'customer_data.customer_mst_id = customer_mst.id', 'left');
		$this->db->join('product_category', 'product_category.id = customer_data.product_category_id', 'left');
		$this->db->where($where_array, null, false);
		$this->db->order_by('customer_data.rank', 'asc');		
		$return_array = $this->db->get('customer_mst')->result_array();
		// echo "<pre>";print_r($this->db->last_query());echo"</pre><hr>";exit;
		return $return_array;		
	}

	public function	get_hetro_lead_data($where_array){
		
		$return_array = array();
		$this->db->select('customer_mst.id, customer_mst.name, customer_mst.region_id, customer_mst.country_id, customer_mst.assigned_to, customer_mst.website, customer_mst.no_of_employees, customer_mst.lead_type, customer_mst.lead_stage, customer_mst.stage_reason, customer_mst.box, customer_mst.box_comment, customer_mst.sample, customer_mst.sample_comment, customer_mst.lead_priority, customer_mst.priority_reason, customer_mst.entered_by, customer_mst.modified_by, customer_mst.deleted, customer_mst.delete_reason, customer_mst.brand, customer_mst.status, customer_mst.entered_on, customer_mst.modified_on, customer_data.last_purchased, product_category.product_name', FALSE);
		$this->db->join('customer_data', 'customer_data.customer_mst_id = customer_mst.id', 'left');
		$this->db->join('product_category', 'product_category.id = customer_data.product_category_id', 'left');
		$this->db->where($where_array, null, false);		
		$return_array = $this->db->get('customer_mst')->result_array();
		// echo "<pre>";print_r($this->db->last_query());echo"</pre><hr>";exit;
		return $return_array;		
	}

	public function	get_internal_lead_data($where_array){
		
		$return_array = array();
		$this->db->select('customer_mst.id, customer_mst.name, customer_mst.region_id, customer_mst.country_id, customer_mst.assigned_to, customer_mst.website, customer_mst.no_of_employees, customer_mst.lead_type, customer_mst.lead_stage, customer_mst.stage_reason, customer_mst.box, customer_mst.box_comment, customer_mst.sample, customer_mst.sample_comment, customer_mst.lead_priority, customer_mst.priority_reason, customer_mst.entered_by, customer_mst.modified_by, customer_mst.deleted, customer_mst.purchase_factor_1, customer_mst.purchase_factor_2, customer_mst.purchase_factor_3, customer_mst.sales_notes, customer_mst.product_pitch, customer_mst.purchase_comments, customer_mst.margins, customer_mst.delete_reason, customer_mst.brand, customer_mst.status, customer_mst.entered_on, customer_mst.modified_on, customer_data.rank, customer_data.source, customer_data.last_purchased, product_category.product_name', FALSE);
		$this->db->join('customer_data', 'customer_data.customer_mst_id = customer_mst.id', 'left');
		$this->db->join('product_category', 'product_category.id = customer_data.product_category_id', 'left');
		$this->db->where($where_array, null, false);		
		$return_array = $this->db->get('customer_mst')->result_array();
		// echo "<pre>";print_r($this->db->last_query());echo"</pre><hr>";exit;
		return $return_array;		
	}




	//hetro lead
	// public function get_lead_mst_lead_count($where_string) {

	// 	$this->db2->select('count(*) as count, data_category');
	// 	$this->db2->where($where_string, NULL, FALSE);
	// 	$this->db2->group_by('data_category');
	// 	return $this->db2->get('lead_mst')->result_array();
	// }

	// public function get_lead_mst_lead_data($select_string, $where_string) {

	// 	$this->db2->select($select_string);
	// 	$this->db2->where($where_string, NULL, FALSE);
	// 	$this->db2->order_by('RANK', 'asc');
	// 	return $this->db2->get('lead_mst')->result_array();
	// }

	// other module
	public function get_count($where_string, $table_name) {

		$this->db->select('count(*) as count');
		$this->db->where($where_string, NULL, FALSE);
		return $this->db->get($table_name)->row_array();
	}

	public function get_all_data($select_string, $where_string, $table_name) {

		$this->db->select($select_string);
		$this->db->where($where_string, NULL, FALSE);
		return $this->db->get($table_name)->result_array();
	}

	public function get_people_information_details(){

		$return_array = array();
		$this->db->select("users.user_id, people_information.first_name, people_information.last_name, people_information.profile_pic_file_path");
		$this->db->where("users.status = 1 AND users.role IN (5) AND people_information.status = 'Active'", null, false);
		$this->db->join('people_information', 'people_information.user_id = users.user_id');
		$return_array = $this->db->get('users')->result_array();
		return $return_array;
	}

	public function get_sales_person_protocol_list(){

		$return_array = array();
		$this->db->select("users.user_id, people_information.first_name, people_information.last_name, people_information.profile_pic_file_path, employee_protocol.protocol_information");
		$this->db->where("users.status = 1 AND users.role IN (5) AND people_information.status = 'Active' AND employee_protocol.status = 'Active'", null, false);
		$this->db->join('people_information', 'people_information.user_id = users.user_id');
		$this->db->join('employee_protocol', 'employee_protocol.user_id = users.user_id');
		$return_array = $this->db->get('users')->result_array();
		return $return_array;
	}

	public function	get_internal_listing_data($where_array){
		
		$return_array = array();
		$this->db->select('customer_mst.*, customer_data.source,  customer_data.rank', FALSE);
		$this->db->join('customer_data', 'customer_data.customer_mst_id = customer_mst.id', 'left');
		$this->db->where($where_array, null, false);		
		$return_array = $this->db->get('customer_mst')->result_array();
		// echo "<pre>";print_r($this->db->last_query());echo"</pre><hr>";exit;
		return $return_array;		
	}

	public function get_sales_person_lead_assign_data($client_where) {

		$this->db->select('
				customer_mst.assigned_to sales_person_id, count(customer_mst.assigned_to) count,
				count(IF(customer_mst.lead_stage = 6, 1, NULL)) stage_6,
				count(IF(customer_mst.lead_stage = 5, 1, NULL)) stage_5,
				count(IF(customer_mst.lead_stage = 4, 1, NULL)) stage_4,
			    count(IF(customer_mst.lead_stage = 3, 1, NULL)) stage_3,
			    count(IF(customer_mst.lead_stage = 2, 1, NULL)) stage_2,
			    count(IF(customer_mst.lead_stage = 1, 1, NULL)) stage_1,
			    count(IF(customer_mst.lead_stage = 0, 1, NULL)) stage_0,
			    count(IF(customer_mst.lead_stage is NULL, 1, NULL)) blank_stage');
		$this->db->where(implode(' AND ', $client_where), null, false);
		$this->db->group_by('customer_mst.assigned_to');
		$client_details =  $this->db->get('customer_mst')->result_array();
		// echo $this->db->last_query(),"<hr>";
		return array('client_leads'=>$client_details);
	}
}