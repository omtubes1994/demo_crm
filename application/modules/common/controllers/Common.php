<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends MX_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('common_model');
		// ini_set('max_execution_time', 1200);
		ini_set('memory_limit', -1);
		error_reporting(0);
	}

	public function index(){
	}

	public function create_table_dump() {

		$this->common_model->create_table_dump();
	}

	public function create_table_dump_marketing() {

		$this->common_model->create_table_dump_marketing();
	}

	public function create_data() {
		
		$this->common_model->create_data();
	}

	// public function assign_lead_in_batch() {

		// 	$limit = $this->uri->segment('3',0);
		// 	$all_lead = $this->common_model->get_all_lead_details('lead_mst', 'tubing');
		//    	$update_data=array();
		//    	foreach($all_lead as $all_lead_key => $all_lead_details){
		//    	 	if (count($update_data)<$limit && $all_lead_key % 2 != 0){
		//    		    $update_data[]=array('lead_mst_id'=>$all_lead_details['lead_mst_id'],'assigned_to'=>89);
		//  		}
		//    	}
		//  	$this->common_model->update_data_marketing_db('lead_mst',$update_data,'lead_mst_id','batch');
		// }

		// public function lead_of_box_yes() {
		// 	$this->common_model->lead_of_box_yes($this->uri->segment('3'), $this->uri->segment('4'));
		// }

		// public function lead_of_box_no() {
		// 	$this->common_model->lead_of_box_no($this->uri->segment('3'), $this->uri->segment('4'));
	// }

	public function user_assign_data() {

		$data = array();
		$data = $this->prepare_listing_data_for_sales();
		if(in_array($this->session->userdata('role'), array(1))) {

			$sub_module_list = $this->common_model->get_lead_module_name('no', 'yes');
			$data['sub_module_list'] = $sub_module_list['sub_module_details'];
			$data['country_list'] = $this->common_model->get_country_list();
			$data['region_list'] = $this->common_model->get_region_list();
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => ''));
		$this->load->view('sidebar', array('title' => 'User Data'));
		$this->load->view('common/user_assign_data', $data);
		$this->load->view('footer');
	}

	public function view_user_data() {

		if($this->session->userdata('role') == 1 || $this->session->userdata('role') == 16 || $this->session->userdata('role') == 17) {

			$user_id = $this->uri->segment(3,0);
		} else {
			$user_id = $this->session->userdata('user_id');
		}
		if(!empty($user_id)) {

			// echo $user_id; die;
			if($user_id != 'unassigned') {

				//getting user details
				$user_details = $this->common_model->get_dynamic_data_sales_db('*', 'user_id='.$user_id, 'users', 'row_array');
				if(in_array($user_details['role'], array(5, 16))) {
					
					$data = $this->prepare_sales_lead_data_for_update($user_details);
					$data['department'] = 'sales'; 
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
				}else if(in_array($user_details['role'], array(6, 8))) {

					$data = $this->prepare_procurement_lead_data_for_update($user_details);
					$data['department'] = 'procurement';
				}
			}else{

				$data = $this->prepare_sales_lead_data_for_update(array());
				$data['department'] = 'sales';
			}
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
			$this->load->view('header', array('title' => ''));
			$this->load->view('sidebar', array('title' => 'User Data'));
			$this->load->view('common/user_assign_data_update', $data);
			$this->load->view('footer');
		}else {
			die('id not passed');
		}
	}

	public function sales_db_backup() {
		$this->common_model->sales_db_backup();
	}

	public function marketing_db_backup() {
		$this->common_model->marketing_db_backup();
	}

	// public function create_lead_count() {

		// 	$lead_data = array();
		// 	$static_lead_name = array('Chemical Companies'=> 'Piping Chemical Companies', 'distributors'=> 'Tube Fittings Distributors', 
		// 							'EPC companies'=> 'Piping EPC Companies',	'Forged Fittings'=> 'Forged Fittings',
		// 							'Heteregenous Tubes India'=> 'Tube Fittings & Valves India', 'pvf companies'=> 'Instrumentation PVF Companies',
		// 							'Shipyards'=> 'Piping Shipyards Companies', 'sugar companies'=> 'Piping Sugar Companies', 'Water Companies'=> 'Piping Water Companies',	'HAMMER UNION'=> 'Primary Leads - Hammer Union', 'PIPES'=> 'Primary Leads - Piping Products',
		// 							'PROCESS CONTROL'=> 'Primary Leads - Process Control','TUBES'=> 'Primary Leads - Tube Fittings & Valves', 
		// 							'TUBING'=> 'Primary Leads - Tubing','Miscellaneous Leads'=> 'Miscellaneous Leads');
		// 	$lead_details = $this->common_model->create_lead_count('pipes');
		// 	// echo "<pre>";print_r($lead_details);echo"</pre><hr>";exit;
		// 	foreach ($lead_details as $single_lead_details) {
		// 		if(!empty($lead_data[$static_lead_name[$single_lead_details['data_category']]][$single_lead_details['assigned_to']])) {

		// 			$lead_data[$static_lead_name[$single_lead_details['data_category']]][$single_lead_details['assigned_to']] += 1;
		// 		}else {

		// 			$lead_data[$static_lead_name[$single_lead_details['data_category']]][$single_lead_details['assigned_to']] = 1;
		// 		}
		// 	}

		// 	$lead_details = $this->common_model->create_lead_count_hetro('Shipyards');
		// 	// echo "<pre>";print_r($lead_details);echo"</pre><hr>";exit;
		// 	foreach ($lead_details as $single_lead_details) {
		// 		if(!empty($lead_data[$static_lead_name[$single_lead_details['source']]][$single_lead_details['assigned_to']])) {

		// 			$lead_data[$static_lead_name[$single_lead_details['source']]][$single_lead_details['assigned_to']] += 1;
		// 		}else {

		// 			$lead_data[$static_lead_name[$single_lead_details['source']]][$single_lead_details['assigned_to']] = 1;
		// 		}
		// 	}
		// 	$lead_details = $this->common_model->create_lead_count_hetro('Chemical Companies');
		// 	// echo "<pre>";print_r($lead_details);echo"</pre><hr>";exit;
		// 	foreach ($lead_details as $single_lead_details) {
		// 		if(!empty($lead_data[$static_lead_name[$single_lead_details['source']]][$single_lead_details['assigned_to']])) {

		// 			$lead_data[$static_lead_name[$single_lead_details['source']]][$single_lead_details['assigned_to']] += 1;
		// 		}else {

		// 			$lead_data[$static_lead_name[$single_lead_details['source']]][$single_lead_details['assigned_to']] = 1;
		// 		}
		// 	}
		// 	$lead_details = $this->common_model->create_lead_count_hetro('Water Companies');
		// 	// echo "<pre>";print_r($lead_details);echo"</pre><hr>";exit;
		// 	foreach ($lead_details as $single_lead_details) {
		// 		if(!empty($lead_data[$static_lead_name[$single_lead_details['source']]][$single_lead_details['assigned_to']])) {

		// 			$lead_data[$static_lead_name[$single_lead_details['source']]][$single_lead_details['assigned_to']] += 1;
		// 		}else {

		// 			$lead_data[$static_lead_name[$single_lead_details['source']]][$single_lead_details['assigned_to']] = 1;
		// 		}
		// 	}
		// 	$lead_details = $this->common_model->create_lead_count_hetro('EPC companies');
		// 	// echo "<pre>";print_r($lead_details);echo"</pre><hr>";exit;
		// 	foreach ($lead_details as $single_lead_details) {
		// 		if(!empty($lead_data[$static_lead_name[$single_lead_details['source']]][$single_lead_details['assigned_to']])) {

		// 			$lead_data[$static_lead_name[$single_lead_details['source']]][$single_lead_details['assigned_to']] += 1;
		// 		}else {

		// 			$lead_data[$static_lead_name[$single_lead_details['source']]][$single_lead_details['assigned_to']] = 1;
		// 		}
		// 	}
		// 	$lead_details = $this->common_model->create_lead_count_hetro('sugar companies');
		// 	// echo "<pre>";print_r($lead_details);echo"</pre><hr>";exit;
		// 	foreach ($lead_details as $single_lead_details) {
		// 		if(!empty($lead_data[$static_lead_name[$single_lead_details['source']]][$single_lead_details['assigned_to']])) {

		// 			$lead_data[$static_lead_name[$single_lead_details['source']]][$single_lead_details['assigned_to']] += 1;
		// 		}else {

		// 			$lead_data[$static_lead_name[$single_lead_details['source']]][$single_lead_details['assigned_to']] = 1;
		// 		}
		// 	}
		// 	$lead_details = $this->common_model->create_lead_count('tubing');
		// 	// echo "<pre>";print_r($lead_details);echo"</pre><hr>";exit;
		// 	foreach ($lead_details as $single_lead_details) {
		// 		if(!empty($lead_data[$static_lead_name[$single_lead_details['data_category']]][$single_lead_details['assigned_to']])) {

		// 			$lead_data[$static_lead_name[$single_lead_details['data_category']]][$single_lead_details['assigned_to']] += 1;
		// 		}else {

		// 			$lead_data[$static_lead_name[$single_lead_details['data_category']]][$single_lead_details['assigned_to']] = 1;
		// 		}
		// 	}
		// 	$lead_details = $this->common_model->create_lead_count('hammer union');
		// 	// echo "<pre>";print_r($lead_details);echo"</pre><hr>";exit;
		// 	foreach ($lead_details as $single_lead_details) {
		// 		if(!empty($lead_data[$static_lead_name[$single_lead_details['data_category']]][$single_lead_details['assigned_to']])) {

		// 			$lead_data[$static_lead_name[$single_lead_details['data_category']]][$single_lead_details['assigned_to']] += 1;
		// 		}else {

		// 			$lead_data[$static_lead_name[$single_lead_details['data_category']]][$single_lead_details['assigned_to']] = 1;
		// 		}
		// 	}
		// 	$lead_details = $this->common_model->create_lead_count_hetro('Forged Fittings');
		// 	// echo "<pre>";print_r($lead_details);echo"</pre><hr>";exit;
		// 	foreach ($lead_details as $single_lead_details) {
		// 		if(!empty($lead_data[$static_lead_name[$single_lead_details['source']]][$single_lead_details['assigned_to']])) {

		// 			$lead_data[$static_lead_name[$single_lead_details['source']]][$single_lead_details['assigned_to']] += 1;
		// 		}else {

		// 			$lead_data[$static_lead_name[$single_lead_details['source']]][$single_lead_details['assigned_to']] = 1;
		// 		}
		// 	}
		// 	$lead_details = $this->common_model->create_lead_count_hetro('Miscellaneous Leads');
		// 	// echo "<pre>";print_r($lead_details);echo"</pre><hr>";exit;
		// 	foreach ($lead_details as $single_lead_details) {
		// 		if(!empty($lead_data[$static_lead_name[$single_lead_details['source']]][$single_lead_details['assigned_to']])) {

		// 			$lead_data[$static_lead_name[$single_lead_details['source']]][$single_lead_details['assigned_to']] += 1;
		// 		}else {

		// 			$lead_data[$static_lead_name[$single_lead_details['source']]][$single_lead_details['assigned_to']] = 1;
		// 		}
		// 	}
		// 	foreach ($lead_data as $lead_name => $lead_details) {
		// 		arsort($lead_details);
		// 		foreach ($lead_details as $sales_person_id => $lead_count) {

		// 			$data['all_lead_details'][$lead_name][] = array('user_id'=>$sales_person_id, 'count'=>$lead_count);
		// 		}
		// 	}
		// 	$data['user_details'] =  array_column($this->common_model->get_dynamic_data_sales_db('name, user_id', array(), 'users'), 'name', 'user_id');
		// 	$data['user_details'][0]='Not Assigned';
		// 	$data['user_details']['']='Not Assigned';
		// 	// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		// 	$this->load->view('header', array('title' => ''));
		// 	$this->load->view('sidebar', array('title' => 'User Data'));
		// 	$this->load->view('common/show_lead_count', array());
		// 	$this->load->view('footer');
	// }

	public function csv_file_converson() {
		$this->common_model->csv_file_converson($this->uri->segment('3'));
	}
	public function daily_report() {

		if(in_array($this->session->userdata('role'), array(1, 5, 16, 17))){

			$data = array();
			//getting user list of sales team
			$user_list = $this->common_model->get_dynamic_data_sales_db('name, user_id', 'status=1 AND (role= 16 OR role= 5)', 'users');
			if(!in_array($this->session->userdata('user_id'), array(214)) && in_array($this->session->userdata('role'), array(5))){

	 			$user_reporting_to_current_user_id = array_column($this->common_model->get_dynamic_data_sales_db('id, user_id', 'status="Active" AND reporting_manager="'.$this->session->userdata('user_id').'"', 'organization_chart'),'user_id');
		 		foreach ($user_list as $user_details) {
		 			
		 			if(in_array($user_details['user_id'], $user_reporting_to_current_user_id)) {

		 				$data['user_list'][] = $user_details;
		 			} else if($user_details['user_id'] == $this->session->userdata('user_id')){

		 				$data['user_list'][] = $user_details;
		 			}
		 		}
		 	}else{

				$data['user_list'] = $user_list;
		 	}

			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
			$this->load->view('header', array('title' => ''));
			$this->load->view('sidebar', array('title' => 'User Data'));
			$this->load->view('common/sales_person_daily_report', $data);
			$this->load->view('footer');
		}else{
			die('Soory You Dont Have Access to this!!!');
		}
	}
	public function daily_report_all(){

		$data = array();
		$data['user_list'] = array();
		$user_data = $this->common_model->get_all_conditional_data_sales_db('user_id, name', array('status'=> 1, 'role'=> 5), 'users', 'result_array', array(), array('column_name'=> 'user_id', 'column_value'=> 'asc'));

		foreach ($user_data as $single_user_data) {
			
			$explode_name = explode(" ", $single_user_data['name']);
			$profile_pic_data = $this->common_model->get_all_conditional_data_sales_db('profile_pic_file_path', array('status'=> 'Active', 'people_information.user_id'=> $single_user_data['user_id']), 'people_information', 'row_array');
			$data['user_list'][] = array(
				'user_id'=> $single_user_data['user_id'],
				'first_name'=> $explode_name[0],
				'last_name'=> (!empty($explode_name[1])) ? $explode_name[1]: '',
				'profile_pic_file_path'=> (!empty($profile_pic_data) && !empty($profile_pic_data['profile_pic_file_path'])) ? $profile_pic_data['profile_pic_file_path'] : '',
			);
		}
		
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => ''));
		$this->load->view('sidebar', array('title' => 'User Data'));
		$this->load->view('common/sales_person_daily_report_all', $data);
		$this->load->view('footer');
	}
	public function manage_salesperson_daily_report(){

		$data = array();
		$user_list = $this->common_model->get_sales_person_protocol_list();
		if(!empty($user_list)) {
			foreach ($user_list as $user_list_key => $single_user_details) {
				
				$data['user_list'][$user_list_key] = json_decode($single_user_details['protocol_information'], true);
				$data['user_list'][$user_list_key]['user_id'] = $single_user_details['user_id'];
				$data['user_list'][$user_list_key]['first_name'] = $single_user_details['first_name'];
				$data['user_list'][$user_list_key]['last_name'] = $single_user_details['last_name'];
				$data['user_list'][$user_list_key]['profile_pic_file_path'] = $single_user_details['profile_pic_file_path'];
			}
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;

		$this->load->view('header', array('title' => ''));
		$this->load->view('sidebar', array('title' => 'User Data'));
		$this->load->view('common/manage_salesperson_protocol', $data);
		$this->load->view('footer');
	}
	// public function store_daily_report_data(){


		// 	$date = $this->uri->segment(3, date('Y-m-d', strtotime('-1 days')));	
		// 	$user_id = $this->uri->segment(4, 0);
		// 	$where_array = array();
		// 	$where_array['status'] = 1;
		// 	$where_array['role'] = 5;
		// 	if(!empty($user_id)){
		// 		$where_array['user_id'] = $user_id;
		// 	}

		// 	// echo "<pre>";print_r($date);echo"</pre><hr>";
		// 	// echo "<pre>";print_r($where_array);echo"</pre><hr>";exit;
		// 	$user_list = $this->common_model->get_dynamic_data_sales_db('*', $where_array, 'users', 'result_array');
		// 	// echo "<pre>";print_r($user_list);echo"</pre><hr>";exit;
		// 	foreach ($user_list as $single_user_details) {

		// 		//check data exists or not
		// 		if(empty($this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'report_time'=>$date, 'user_id'=>$single_user_details['user_id']), 'daily_report', 'row_array'))){

		// 			$data =  $this->prepare_daily_report_data_of_sales_person($single_user_details['user_id'], array('filter_date'=> $date))['lead_count'];
		// 			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		// 			$this->common_model->insert_data_sales_db('daily_report', array('user_id'=>$single_user_details['user_id'], 'report_time'=>$date, 'call_connected'=> $data['call'], 'call_attempted'=> $data['call_attempted'], 'email'=> $data['email'], 'whatsapp'=> $data['whatsapp'], 'linkedin'=> $data['linkedin']));
		// 			echo "User id:-->".$single_user_details['user_id']." is done!!!<hr>";
		// 		}else{

		// 			echo "User id:-->".$single_user_details['user_id']." is Already done!!!<hr>";
		// 		}
		// 	}
	// }
	public function daily_routine_all(){

		$data = array();
		$data['user_list'] = array();
		$user_data = $this->common_model->get_all_conditional_data_sales_db('user_id, name', array('status'=> 1, 'role'=> 5), 'users', 'result_array', array(), array('column_name'=> 'user_id', 'column_value'=> 'asc'));

		foreach ($user_data as $single_user_data) {
			
			$explode_name = explode(" ", $single_user_data['name']);
			$profile_pic_data = $this->common_model->get_all_conditional_data_sales_db('profile_pic_file_path', array('status'=> 'Active', 'people_information.user_id'=> $single_user_data['user_id']), 'people_information', 'row_array');
			$data['user_list'][] = array(
				'user_id'=> $single_user_data['user_id'],
				'first_name'=> $explode_name[0],
				'last_name'=> (!empty($explode_name[1])) ? $explode_name[1]: '',
				'profile_pic_file_path'=> (!empty($profile_pic_data) && !empty($profile_pic_data['profile_pic_file_path'])) ? $profile_pic_data['profile_pic_file_path'] : '',
			);
		}
		
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => ''));
		$this->load->view('sidebar', array('title' => 'User Data'));
		$this->load->view('common/sales_person_daily_routine_all', $data);
		$this->load->view('footer');
	}
	public function mrf(){

		$data['manpower_data'] = array('id'=>'','current_department_id'=>'', 'no_of_positions'=>'', 'current_job_title'=>'', 'reporting_to'=>array(), 'current_reporting_to'=>'', 'location_site'=>'', 'priority_closingdate'=>'', 'type_of'=>'', 'type_of_name'=>'', 'all_type_of_name'=>array(), 'job_description'=>'', 'total_experience'=>'', 'qualification'=>'', 'salary_range'=>'','assigned_to'=>array());
		$data['manpower_data']['date'] = $this->get_indian_time('Y-m-d');

		$manpower_id = $this->uri->segment('3', 0);
		if(!empty($manpower_id)){

			$data['manpower_data'] = $this->common_model->get_dynamic_data_sales_db('*',array('id'=>$manpower_id), 'manpower_requisition_form','row_array');

			$data['manpower_data']['assigned_to_list'] = json_decode($data['manpower_data']['assigned_to'], true);
		}

		$data['job_title'] = $this->common_model->get_dynamic_data_sales_db('id, name', array('status'=>'Active'),'mrf_designation');

		$data['reporting_to'] = $this->common_model->get_dynamic_data_sales_db('user_id, name', array('status'=>'Active'), 'mrf_reporting_manager');

		$data['all_type_of_name'] = $this->common_model->get_dynamic_data_sales_db("user_id, name", array(), 'users');

		$data['all_department']	= $this->common_model->get_dynamic_data_sales_db('id, name', array(),'mrf_department');

		$data['assigned_to'] = $this->common_model->get_dynamic_data_sales_db('user_id, name', array('status'=>1, 'role'=> 13),'users');

		$data['qualification'] = $this->common_model->get_dynamic_data_sales_db('id, name', array('status'=>'Active'),'mrf_qualification');

		// echo "<pre>";print_r($data);echo"</pre><br>";exit;
		$this->load->view('header', array('title' => ''));
		$this->load->view('sidebar', array('title' => 'MANPOWER REQUISITION FORM'));
		$this->load->view('common/manpower_form', $data);
		$this->load->view('footer');
	}
	public function mrf_list(){

		$where_string = "stage = 'open' AND status = 'Active'";
		if(!in_array($this->session->userdata('role'), array(1, 17))){
			$hr_id = $this->session->userdata('hr_access')['hr_user_id'];
			if($this->session->userdata('role') == 13){

				$where_string .= " AND (";
				foreach($hr_id as $key => $single_hr_id){

					if($key > 0){
						$where_string .= " OR ";
					}
					$where_string .= " assigned_to LIKE '%".$single_hr_id."%'";
				}
				$where_string .= ")";

			}else{

				$where_string .= " AND reporting_to IN (".implode(', ',$this->session->userdata('hr_access')['reporting_to_user_id']).")";
			}
		}

		$data = $this->prepare_listing_data($where_string, 10, 0, 'open_list');
		// echo "<pre>";print_r($data);echo"</pre><br>";exit;
		$this->load->view('header', array('title' => ''));
		$this->load->view('sidebar', array('title' => 'Manpower List'));
		$this->load->view('common/manpower_form_list', $data);
		$this->load->view('footer');
	}

	public function candidate(){

		$this->session->unset_userdata('candidate_resume');
		$data = array();
		$mrf_id = $this->uri->segment('3', 0);
		if(!empty($mrf_id)){

			$data['candidate_data'] = array('id'=>'', 'name'=>'', 'mobile'=>'', 'email'=>'', 'source_id'=>'', 'resume'=>'', 'stage_id'=>'', 'hr_user_id'=>'');
			$data['mrf_id'] = $mrf_id;

			$candidate_id = $this->uri->segment('4', 0);
			if(!empty($candidate_id)){

				$data['candidate_data'] = $this->common_model->get_dynamic_data_sales_db('*',array('id'=>$candidate_id, 'status'=>'Active'), 'candidate','row_array');
			}

			$data['source_list'] = $this->common_model->get_dynamic_data_sales_db('id, name', array('status'=>'Active'),'candidate_source');
			$data['stage_list'] = $this->common_model->get_dynamic_data_sales_db('id, name', array('status'=>'Active'),'candidate_stage');

		}else{

			redirect('common/mrf_list', 'refresh');
		}
		// echo "<pre>";print_r($candidate_data);echo"</pre><br>";exit;
		$this->load->view('header', array('title' => ''));
		$this->load->view('sidebar', array('title' => 'Candidate Form'));
		$this->load->view('common/candidate_form',$data);
		$this->load->view('footer');
	}

	public function candidate_list(){

		$mrf_id = $this->uri->segment('3', 0);
		if(!empty($mrf_id)){

			$data = $this->prepare_candidate_list_data(array('status' => 'Active', 'mrf_id'=>$mrf_id), array('column_name'=>'id', 'column_value'=>'desc'), 10, 0);
			$data['mrf_id'] = $mrf_id;

			$data['total_candidate_count'] = $this->common_model->get_dynamic_data_sales_db('count(id) as total_candidate', array('status'=>'Active', 'mrf_id'=>$mrf_id),'candidate', 'row_array');
			if(!empty($data['total_candidate_count'])){

				$data['whatsapp_candidate_count'] = $this->common_model->get_dynamic_data_sales_db_null_false('count(id) as whatsapp_candidate', 'status="Active" AND stage_id IN(2, 3) AND mrf_id ='.$mrf_id.'','candidate', 'row_array');

				$data['interview_candidate_count'] = $this->common_model->get_dynamic_data_sales_db_null_false('count(id) as interview_candidate', 'status="Active" AND stage_id IN(4) AND mrf_id ='.$mrf_id.'','candidate', 'row_array');

				$data['interviewed_count'] = $this->common_model->get_dynamic_data_sales_db_null_false('count(id) as Interviewed', 'status="Active" AND stage_id IN(5) AND mrf_id ='.$mrf_id.'','candidate', 'row_array');

				$data['approved_candidate_count'] = $this->common_model->get_dynamic_data_sales_db_null_false('count(id) as approve_candidate', 'status="Active" AND stage_id IN(6) AND mrf_id ='.$mrf_id.'','candidate', 'row_array');

				$data['reject_candidate_count'] = $this->common_model->get_dynamic_data_sales_db_null_false('count(id) as reject_candidate', 'status="Active" AND stage_id IN(7) AND mrf_id ='.$mrf_id.'','candidate', 'row_array');

				$data['join_candidate_count'] = $this->common_model->get_dynamic_data_sales_db_null_false('count(id) as join_candidate', 'status="Active" AND stage_id IN(12) AND mrf_id ='.$mrf_id.'','candidate', 'row_array');

				$data['interview_rejected_count'] = $this->common_model->get_dynamic_data_sales_db_null_false('count(id) as interview_rejected', 'status="Active" AND stage_id IN(13) AND mrf_id ='.$mrf_id.'','candidate', 'row_array');
			}
		}
		// echo "<pre>";print_r($data);echo"</pre><br>";exit;
		$this->load->view('header', array('title' => ''));
		$this->load->view('sidebar', array('title' => 'Candidate List'));
		$this->load->view('common/candidate_list',$data);
		$this->load->view('footer');
	}

	public function interview_evaluation(){

		$data['evaluation_data'] =  array('id'=>'', 'sub_reporting_to'=>array(), 'photo'=>'', 'education_background'=>'', 'job_knowledge'=>'', 'prior_work_experience'=>'', 'communication_skills'=>'', 'required_skills'=>'', 'overall_impression'=>'', 'comments'=>'', 'notice_period'=>'','current_ctc'=>'', 'expected_ctc'=>'', 'interview_status'=>'', 'other_comments'=>'', 'education_background_comments'=>'', 'job_knowledge_comments'=>'','prior_work_experience_comments'=>'','communication_skills_comments'=>'', 'required_skills_comments'=>'', 'overall_impression_comments'=>'');

		$this->session->unset_userdata('candidate_photo');
		$candidate_id = $this->uri->segment('3', 0);
		if(!empty($candidate_id)){

			$data['candidate_id'] = $candidate_id;
			$data['evaluation_data'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'candidate_id'=> $candidate_id), 'candidate_evaluation', 'row_array');

			if(!empty($data['evaluation_data'])){

				$data['evaluation_data']['sub_reporting_to_list'] = json_decode($data['evaluation_data']['sub_reporting_to'], true);
			}
			$data['candidate_data'] = $this->common_model->get_dynamic_data_sales_db('*',array('id'=>$candidate_id, 'status'=>'Active'), 'candidate','row_array');

			if(!empty($data['candidate_data'])){

				$data['candidate_mrf_data'] = $this->common_model->get_dynamic_data_sales_db('*',array('id'=> $data['candidate_data']['mrf_id'], 'status'=>'Active'), 'manpower_requisition_form','row_array');

				$data['sub_reporting_list'] = $this->common_model->get_dynamic_data_sales_db_null_false('user_id, name', 'status= 1 AND role not IN(19, 20)', 'users');

				if(!empty($data['candidate_mrf_data'])){

					$data['candidate_department'] = $this->common_model->get_dynamic_data_sales_db('*',array('id'=> $data['candidate_mrf_data']['mrf_department_id'], 'status'=>'Active'), 'mrf_department','row_array');

					$data['candidate_degignation'] = $this->common_model->get_dynamic_data_sales_db('*',array('id'=> $data['candidate_mrf_data']['mrf_designation_id'], 'status'=>'Active'), 'mrf_designation','row_array');

					$data['candidate_reporting_to'] = $this->common_model->get_dynamic_data_sales_db('*',array('user_id'=> $data['candidate_mrf_data']['reporting_to'], 'status'=>'Active'), 'mrf_reporting_manager','row_array');
				}
			}
		}
		// echo "<pre>";print_r($data);echo"</pre><br>";exit;
		$this->load->view('header', array('title' => 'Interview Evaluation'));
		$this->load->view('sidebar', array('title' => 'Interview Evaluation'));
		$this->load->view('common/candidate_evaluation',$data);
		$this->load->view('footer');
	}

	public function check_count(){

		$data = array();
		$all_data = $this->common_model->get_dynamic_data_sales_db("id, name, region_id, country_id, assigned_to, status", "id > 0", "customer_mst", 'result_array', 0, 0);

		$rfq_mst_count_all = array_column($this->common_model->get_all_conditional_data_sales_db("rfq_company, count(rfq_mst_id) as count", "rfq_company > 0 ", "rfq_mst", 'result_array', array(), array(), array("rfq_company")), 'count', 'rfq_company');

		$quotation_mst_count_all = array_column($this->common_model->get_all_conditional_data_sales_db("client_id, count(quotation_mst_id) as count", "client_id > 0 ", "quotation_mst", 'result_array', array(), array(), array("client_id")), 'count', 'client_id');

		$query_master_count_all = array_column($this->common_model->get_all_conditional_data_sales_db("query_reference_id, count(id) as count", "status = 'Active' AND query_type IN ('lead_sample_query') AND query_reference_id > 0 ", "query_master", 'result_array', array(), array(), array("query_reference_id")), 'count', 'query_reference_id');

		$lead_management_count_all = array_column($this->common_model->get_all_conditional_data_sales_db("client_id, count(lead_id) as count", "delete_status = 'Active'", "lead_management", 'result_array', array(), array(), array("client_id")), 'count', 'client_id');

		$invoice_mst_count_all = array_column($this->common_model->get_all_conditional_data_sales_db("company_id, count(invoice_mst_id) as count", "company_id > 0 ", "invoice_mst", 'result_array', array(), array(), array("company_id")), 'count', 'company_id');

		$daily_work_sales_on_quotation_data_count_all = array_column($this->common_model->get_all_conditional_data_sales_db("client_id, count(id) as count", "status = 'Active' AND client_id > 0 ", "daily_work_sales_on_quotation_data", 'result_array', array(), array(), array("client_id")), 'count', 'client_id');

		$daily_work_sales_on_lead_data_count_all = array_column($this->common_model->get_all_conditional_data_sales_db("client_id, count(id) as count", "status = 'Active' AND client_id > 0 ", "daily_work_sales_on_lead_data", 'result_array', array(), array(), array("client_id")), 'count', 'client_id');

		$customer_dtl_all = array_column($this->common_model->get_all_conditional_data_sales_db("comp_mst_id, count(comp_dtl_id) as count", "status = 'Active' AND comp_mst_id > 0 ", "customer_dtl", 'result_array', array(), array(), array("comp_mst_id")), 'count', 'comp_mst_id');

		$customer_data_all = array_column($this->common_model->get_all_conditional_data_sales_db("customer_mst_id, count(id) as count", "customer_mst_id > 0 ", "customer_data", 'result_array', array(), array(), array("customer_mst_id")), 'count', 'customer_mst_id');

		$customer_connect_count_all = array_column($this->common_model->get_all_conditional_data_sales_db("comp_mst_id, count(comp_connect_id) as count", "comp_mst_id > 0 ", "customer_connect", 'result_array', array(), array(), array("comp_mst_id")), 'count', 'comp_mst_id');

		$competitor_ranks_invoice_count_all = array_column($this->common_model->get_all_conditional_data_sales_db("client_id, count(client_id) as count", "client_id > 0 ", "competitor_ranks_invoice", 'result_array', array(), array(), array("client_id")), 'count', 'client_id');

		$region_all = array_column($this->common_model->get_all_conditional_data_sales_db("id, name", "id > 0 ", "region_mst", 'result_array', array(), array(), array()), 'name', 'id');

		$country_all = array_column($this->common_model->get_all_conditional_data_sales_db("id, name", "id > 0 ", "country_mst", 'result_array', array(), array(), array()), 'name', 'id');

		$sales_person_all = array_column($this->common_model->get_all_conditional_data_sales_db("user_id, name", "user_id > 0 ", "users", 'result_array', array(), array(), array()), 'name', 'user_id');

		foreach ($all_data as $single_all_data) {

			$data['list_data'][] = array(

				'id'=> $single_all_data['id'],
				'name'=> $single_all_data['name'],
				'region'=> $region_all[$single_all_data['region_id']],
				'country'=> $country_all[$single_all_data['country_id']],
				'sales_person'=> $sales_person_all[$single_all_data['assigned_to']],
				'rfq_mst_count'=> (isset($rfq_mst_count_all[$single_all_data['id']])) ? $rfq_mst_count_all[$single_all_data['id']]:0,
				'quotation_mst_count'=> (isset($quotation_mst_count_all[$single_all_data['id']])) ? $quotation_mst_count_all[$single_all_data['id']]:0,
				'query_master_count'=> (isset($query_master_count_all[$single_all_data['id']])) ? $query_master_count_all[$single_all_data['id']]:0,
				'pq_lead_detail_count'=> 0,
				'pq_lead_connects_count'=> 0,
				'pq_client_count'=> 0,
				'pq_activity_count'=> 0,
				'lead_management_count'=> (isset($lead_management_count_all[$single_all_data['id']])) ? $lead_management_count_all[$single_all_data['id']]:0,
				'invoice_mst_count'=> (isset($invoice_mst_count_all[$single_all_data['id']])) ? $invoice_mst_count_all[$single_all_data['id']]:0,
				'daily_work_sales_on_quotation_data_count'=> (isset($daily_work_sales_on_quotation_data_count_all[$single_all_data['id']])) ? $daily_work_sales_on_quotation_data_count_all[$single_all_data['id']]:0,
				'daily_work_sales_on_lead_data_count'=> (isset($daily_work_sales_on_lead_data_count_all[$single_all_data['id']])) ? $daily_work_sales_on_lead_data_count_all[$single_all_data['id']]:0,
				'customer_dtl_count'=> (isset($customer_dtl_all[$single_all_data['id']])) ? $customer_dtl_all[$single_all_data['id']]:0,
				'customer_data_count'=> (isset($customer_data_all[$single_all_data['id']])) ? $customer_data_all[$single_all_data['id']]:0,
				'customer_connect_count'=> (isset($customer_connect_count_all[$single_all_data['id']])) ? $customer_connect_count_all[$single_all_data['id']]:0,
				'competitor_ranks_invoice_count'=> (isset($competitor_ranks_invoice_count_all[$single_all_data['id']])) ? $competitor_ranks_invoice_count_all[$single_all_data['id']]:0,
				'status'=> $single_all_data['status'],
			);
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		// $this->load->view('header', array('title' => ''));
		// $this->load->view('sidebar', array('title' => 'User Data'));
		$this->load->view('common/check_count', $data);
		// $this->load->view('footer');
	}

	public function ajax_function() {

		if($this->input->is_ajax_request()){
			$post_details = $this->input->post();
			$response['status'] = 'successful';	
			switch ($post_details['call_type']) {

				// case 'alert_viewed':
					// 	if(!empty($post_details['id'])) {

					// 		$this->common_model->update_data_sales_db('user_alert', array('alert_viewed'=>'True'), array('id'=> $post_details['id']));
					// 	}
				// break;

				// case 'alert_showed_delete':
					// 	if(!empty($post_details['id'])) {

					// 		$this->common_model->update_data_sales_db('user_alert', array('alert_showed_user_id_status'=>'False', 'alert_viewed'=>'True'), array('id'=> $post_details['id']));
					// 	}
				// break;

				// case 'alert_given_delete':
					// 	if(!empty($post_details['id'])) {

					// 		$this->common_model->update_data_sales_db('user_alert', array('alert_given_user_id_status'=>'False', 'alert_viewed'=>'True'), array('id'=> $post_details['id']));
					// 	}
				// break;

				case 'get_lead_information':
					
					
					$data = array();
					$response['table_name'] = '';
					$response['id_column_name'] = '';
					$response['assigned_column_name'] = '';
					$response['table_html'] = '';
					$country_id_filter_string = "";
					$country_name_filter_string = "";
					$stage_filter_string = "";
					$last_purchase_filter_string = "";
					if(!empty($post_details['country'])){
						// echo "<pre>";print_r($post_details['country']);echo"</pre><hr>";exit;
						$country_id_filter_string .= " AND country_id IN (";
						$country_name_filter_string .= " AND COUNTRY_OF_DESTINATION IN (";
						foreach ($post_details['country'] as $country_key => $country_details) {
							
							$country_details_explode = explode('_', $country_details);
							if($country_key == 0) {

								$country_id_filter_string .= "'".$country_details_explode[0]."'";
								$country_name_filter_string .= "'".$country_details_explode[1]."'";
							}else {

								$country_id_filter_string .= ", '".$country_details_explode[0]."'";
								$country_name_filter_string .= ", '".$country_details_explode[1]."'";
							}
						}
						$country_name_filter_string .= ")";
						$country_id_filter_string .= ")";
					}
					if(!empty($post_details['stage'])){

						$stage_filter_string .= " AND (";
						foreach ($post_details['stage'] as $key => $stage_value) {
							
							if($key != 0){

								$stage_filter_string .= " OR ";
							}
							if($stage_value == 'blank') {

								if($post_details['table_name'] == 'customer_mst') {

									$stage_filter_string .= "lead_stage = 0";
								}else{

									$stage_filter_string .= "lead_stage IS NULL";
								}
							}else {

								$stage_filter_string .= "lead_stage = '".$stage_value."'";
							}
						}
						$stage_filter_string .= " )";
					}
					if(!empty($post_details['assign_lead_last_purchase_month'])){

						$last_purchase_filter_string .= " AND month(customer_data.last_purchased) IN ('".implode("', '", $post_details['assign_lead_last_purchase_month'])."')";
						// echo $last_purchase_filter_string;die;
					}
					if(!empty($post_details['assign_lead_last_purchase_year'])){

						$last_purchase_filter_string .= " AND year(customer_data.last_purchased) IN ('".implode("', '", $post_details['assign_lead_last_purchase_year'])."')";
						// echo $last_purchase_filter_string;die;
					}
					if(!empty($post_details['user_id']) && !empty($post_details['lead_name']) && !empty($post_details['table_name'])){
						
						$data['table_name'] = $post_details['table_name'];
						$data['lead_name'] = $post_details['lead_name'];
						$response['lead_name'] = $post_details['lead_name'];
						$response['table_name'] = $post_details['table_name'];

						if($post_details['table_name'] == 'customer_mst'){
							
							if($post_details['lead_name'] == 'tubes' || $post_details['lead_name'] == 'pipes' ||  $post_details['lead_name'] == 'process_control' || $post_details['lead_name'] =='tubing' || $post_details['lead_name'] =='hammer union'){	
											
								if($post_details['user_id'] != 'unassigned'){

									$data['lead_data'] = $this->common_model->get_primary_lead_data(array("customer_mst.deleted is NULL AND customer_data.rank is NOT NULL AND customer_mst.assigned_to = '".$post_details['user_id']."' AND product_category.product_name = '".urldecode($post_details['lead_name'])."'".$country_id_filter_string.$stage_filter_string.$last_purchase_filter_string));

								}else{

									$data['lead_data'] = $this->common_model->get_primary_lead_data(array("customer_mst.deleted is NULL AND customer_data.rank is NOT NULL AND customer_mst.assigned_to IS Null AND product_category.product_name = '".urldecode($post_details['lead_name'])."'".$country_id_filter_string.$stage_filter_string.$last_purchase_filter_string));
								}

								$data['country_details'] = array_column($this->common_model->get_dynamic_data_sales_db('name, id', array('status'=>'Active'), 'country_mst'), 'name', 'id');

								foreach ($data['lead_data'] as $lead_key => $single_lead_data) {
									
									//getting last contact information
									$last_contact_information = $this->common_model->get_all_conditional_data_sales_db('*, DATEDIFF(CURDATE(), connected_on) as diff_date', array('comp_mst_id'=>$single_lead_data['id']), 'customer_connect', 'row_array', array(), array('column_name'=> 'connected_on', 'column_value'=> 'desc'));

									$data['lead_data'][$lead_key]['last_contact'] = 'Not Contacted Yet';
									if(!empty($post_details['last_contact'])){
										
										if($post_details['last_contact'] != 'blank') {

											if(!empty($last_contact_information)){
												
												$last_contact_explode = explode("-", $post_details['last_contact']);
												if($last_contact_explode[0] <= $last_contact_information['diff_date'] && $last_contact_information['diff_date'] < $last_contact_explode[1]) {

													$data['lead_data'][$lead_key]['last_contact'] = $this->get_days_agieng($last_contact_information['diff_date']);
												}else{
													unset($data['lead_data'][$lead_key]);
												}
											}else{
												unset($data['lead_data'][$lead_key]);
											}									
										}else{

											if(!empty($last_contact_information)){
												unset($data['lead_data'][$lead_key]);
											}
										}

									}else{

										if(!empty($last_contact_information)){

											$data['lead_data'][$lead_key]['last_contact'] = $this->get_days_agieng($last_contact_information['diff_date']);
										}
									}
								}

								$response['id_column_name'] = 'id';
								$response['assigned_column_name'] = 'assigned_to';

								// echo "<pre>";print_r($data['lead_data']);echo"</pre><hr>";exit;
							}

							else if($post_details['lead_name'] == 'chemical companies' || $post_details['lead_name'] == 'epc companies' ||  $post_details['lead_name'] == 'distributors' || $post_details['lead_name'] == 'shipyards' || $post_details['lead_name'] == 'water companies' || $post_details['lead_name'] == 'forged fittings' || $post_details['lead_name'] == 'heteregenous tubes india' || $post_details['lead_name'] == 'sugar companies' || $post_details['lead_name'] == 'pvf companies' || $post_details['lead_name'] == 'miscellaneous leads' || $post_details['lead_name'] == 'Hydraulic fitting' || $post_details['lead_name'] == 'Fasteners'){

								if($post_details['user_id'] != 'unassigned'){

									$data['lead_data'] = $this->common_model->get_hetro_lead_data(array("customer_mst.deleted is NULL AND customer_mst.assigned_to = '".$post_details['user_id']."' AND product_category.product_name = '".urldecode($post_details['lead_name'])."'".$country_id_filter_string.$stage_filter_string));

							 	}else{
									$data['lead_data'] = $this->common_model->get_hetro_lead_data(array("customer_mst.deleted is NULL AND customer_mst.assigned_to IS Null AND product_category.product_name = '".urldecode($post_details['lead_name'])."'".$country_id_filter_string.$stage_filter_string));
								}

								$data['country_details'] = array_column($this->common_model->get_dynamic_data_sales_db('name, id', array('status'=>'Active'), 'country_mst'), 'name', 'id');

								foreach ($data['lead_data'] as $lead_key => $single_lead_data) {
									
									//getting last contact information
									$last_contact_information = $this->common_model->get_all_conditional_data_sales_db('*, DATEDIFF(CURDATE(), connected_on) as diff_date', array('comp_mst_id'=>$single_lead_data['id']), 'customer_connect', 'row_array', array(), array('column_name'=> 'connected_on', 'column_value'=> 'desc'));

									$data['lead_data'][$lead_key]['last_contact'] = 'Not Contacted Yet';
									if(!empty($post_details['last_contact'])){
										
										if($post_details['last_contact'] != 'blank') {

											if(!empty($last_contact_information)){
												
												$last_contact_explode = explode("-", $post_details['last_contact']);
												if($last_contact_explode[0] <= $last_contact_information['diff_date'] && $last_contact_information['diff_date'] < $last_contact_explode[1]) {

													$data['lead_data'][$lead_key]['last_contact'] = $this->get_days_agieng($last_contact_information['diff_date']);
												}else{
													unset($data['lead_data'][$lead_key]);
												}
											}else{
												unset($data['lead_data'][$lead_key]);
											}									
										}else{

											if(!empty($last_contact_information)){
												unset($data['lead_data'][$lead_key]);
											}
										}

									}else{

										if(!empty($last_contact_information)){

											$data['lead_data'][$lead_key]['last_contact'] = $this->get_days_agieng($last_contact_information['diff_date']);
										}
									}
								}

								$response['id_column_name'] = 'id';
								$response['assigned_column_name'] = 'assigned_to';
							}

							else if($post_details['lead_name'] == 'internal_data'){

								if($post_details['user_id'] != 'unassigned'){

									$data['lead_data'] = $this->common_model->get_internal_lead_data(array("customer_mst.deleted is NULL AND customer_data.source = 'internal' AND customer_mst.assigned_to = '".$post_details['user_id']."'".$country_id_filter_string.$stage_filter_string));

								}else{

									$data['lead_data'] = $this->common_model->get_internal_lead_data(array("customer_mst.deleted is NULL AND customer_mst.assigned_to IS Null AND customer_data.source = 'internal'".$country_id_filter_string.$stage_filter_string));
								}

								$data['country_details'] = array_column($this->common_model->get_dynamic_data_sales_db('name, id', array('status'=>'Active'), 'country_mst'), 'name', 'id');

								foreach ($data['lead_data'] as $lead_key => $single_lead_data) {
									
									//getting last contact information
									$last_contact_information = $this->common_model->get_all_conditional_data_sales_db('*, DATEDIFF(CURDATE(), connected_on) as diff_date', array('comp_mst_id'=>$single_lead_data['id']), 'customer_connect', 'row_array', array(), array('column_name'=> 'connected_on', 'column_value'=> 'desc'));

									$data['lead_data'][$lead_key]['last_contact'] = 'Not Contacted Yet';
									if(!empty($post_details['last_contact'])){
										
										if($post_details['last_contact'] != 'blank') {

											if(!empty($last_contact_information)){
												
												$last_contact_explode = explode("-", $post_details['last_contact']);
												if($last_contact_explode[0] <= $last_contact_information['diff_date'] && $last_contact_information['diff_date'] < $last_contact_explode[1]) {

													$data['lead_data'][$lead_key]['last_contact'] = $this->get_days_agieng($last_contact_information['diff_date']);
												}else{
													unset($data['lead_data'][$lead_key]);
												}
											}else{
												unset($data['lead_data'][$lead_key]);
											}									
										}else{

											if(!empty($last_contact_information)){
												unset($data['lead_data'][$lead_key]);
											}
										}

									}else{

										if(!empty($last_contact_information)){

											$data['lead_data'][$lead_key]['last_contact'] = $this->get_days_agieng($last_contact_information['diff_date']);
										}
									}
								}

								$response['id_column_name'] = 'id';
								$response['assigned_column_name'] = 'assigned_to';

							}

							else if($post_details['lead_name'] == 'Pre Qualification'){

								if($post_details['user_id'] != 'unassigned'){

									$data['lead_data'] = $this->common_model->get_all_data('*', array('is_pq '=>'"Yes"', 'assigned_to'=>$post_details['user_id']), 'customer_mst');

								}else{

									$data['lead_data'] = $this->common_model->get_all_data('*', array('is_pq '=>'"Yes"', 'assigned_to is null'), 'customer_mst');
								}

								$response['id_column_name'] = 'id';
								$response['assigned_column_name'] = 'assigned_to';
							}
						}
						else if($post_details['table_name'] == 'procurement') {

							if($post_details['user_id'] != 'unassigned'){
								$user_details = $this->common_model->get_dynamic_data_sales_db('*', 'user_id='.$post_details['user_id'], 'users', 'row_array');
								if(in_array($user_details['role'], array(5, 16))) {

									$data['lead_data'] = $this->common_model->get_all_data('id, procurement_no as name', "status = 'Active' AND sales_person = '".$user_details['name']."'", 'procurement');
									$response['id_column_name'] = 'id';
									$response['assigned_column_name'] = 'sales_person';

								}else if(in_array($user_details['role'], array(6, 8))) {

									$data['lead_data'] = $this->common_model->get_all_data('id, procurement_no as name', "status = 'Active' AND procurement_person = '".$user_details['name']."'", 'procurement');
									$response['id_column_name'] = 'id';
									$response['assigned_column_name'] = 'procurement_person';

								}
							}else{

								$data['lead_data'] = $this->common_model->get_all_data('id, procurement_no as name', "status = 'Active' AND sales_person IS Null", 'procurement');
								$response['id_column_name'] = 'id';
								$response['assigned_column_name'] = 'sales_person';
															
							}

						}else if($post_details['table_name'] == 'rfq_mst') {

							if($post_details['user_id'] != 'unassigned'){
								$user_details = $this->common_model->get_dynamic_data_sales_db('*', 'user_id='.$post_details['user_id'], 'users', 'row_array');
								if(in_array($user_details['role'], array(5, 16))) {

									$data['lead_data'] = $this->common_model->get_all_data('rfq_mst_id as id, rfq_no as name', "rfq_sentby = '".$post_details['user_id']."'", 'rfq_mst');
									$response['id_column_name'] = 'rfq_mst_id';
									$response['assigned_column_name'] = 'rfq_sentby';

								}else if(in_array($user_details['role'], array(6, 8))) {

									$data['lead_data'] = $this->common_model->get_all_data('rfq_mst_id as id, rfq_no as name', "assigned_to = '".$post_details['user_id']."'", 'rfq_mst');
									$response['id_column_name'] = 'rfq_mst_id';
									$response['assigned_column_name'] = 'assigned_to';

								}
							}else{

								$data['lead_data'] = $this->common_model->get_all_data('rfq_mst_id as id, rfq_no as name', 'rfq_sentby = 0', 'rfq_mst');
								$response['id_column_name'] = 'rfq_mst_id';
								$response['assigned_column_name'] = 'rfq_sentby';
							}

						}else if($post_details['table_name'] == 'quotation_mst') {

							if($post_details['user_id'] != 'unassigned'){

								$data['lead_data'] = $this->common_model->get_all_data('quotation_mst_id as id, quote_no as name', "assigned_to ='".$post_details['user_id']."'", 'quotation_mst');
								$response['id_column_name'] = 'quotation_mst_id';
								$response['assigned_column_name'] = 'assigned_to';
							}else{
							
								$data['lead_data'] = $this->common_model->get_all_data('quotation_mst_id as id, quote_no as name', 'assigned_to is null', 'quotation_mst');
								$response['id_column_name'] = 'quotation_mst_id';
								$response['assigned_column_name'] = 'assigned_to';
							}

						}else if($post_details['table_name'] == 'vendors') {

							if($post_details['user_id'] != 'unassigned'){
								
								$data['lead_data'] = $this->common_model->get_all_data('vendor_id as id, vendor_name as name', "entered_by ='".$post_details['user_id']."'", 'vendors');
								$response['id_column_name'] = 'vendor_id';
								$response['assigned_column_name'] = 'entered_by';
							}else{

								$data['lead_data'] = $this->common_model->get_all_data('vendor_id as id, vendor_name as name', 'entered_by is null', 'vendors');
								$response['id_column_name'] = 'vendor_id';
								$response['assigned_column_name'] = 'entered_by';
							}

						} 

						// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
						$response['table_html'] = $this->load->view('common/user_assign_table', $data, true);
					}
				break;

				case 'assign_lead_to_other_person':
					
					// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;
					if(!empty($post_details['user_id_old']) && !empty($post_details['user_id_new']) && !empty($post_details['lead_id']) && !empty($post_details['table_name']) && !empty($post_details['id_column_name']) && !empty($post_details['assigned_column_name'])) {

						$old_user_id = $post_details['user_id_old'];
						// if(in_array($post_details['table_name'], array('customer_mst', 'pq_client', 'rfq_mst', 'quotation_mst', 'vendors'))) {
						if(in_array($post_details['table_name'], array('customer_mst', 'rfq_mst', 'quotation_mst', 'vendors'))) {

							// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;
							foreach ($post_details['lead_id'] as $single_lead_id) {
								$this->common_model->update_data_sales_db($post_details['table_name'], array($post_details['assigned_column_name']=>$post_details['user_id_new']), array($post_details['id_column_name']=> $single_lead_id));

								if($post_details['table_name'] == 'customer_mst'){

									$customer_details = $this->common_model->get_dynamic_data_sales_db('*', array('id'=>$single_lead_id, 'status'=>'Active'), 'customer_mst', 'row_array');

									if(!empty($customer_details) && $customer_details['lead_status']=='New'){

										if($post_details['user_id_new'] != 125){

											$this->common_model->update_data_sales_db('customer_mst', array('lead_status'=>''), array($post_details['id_column_name']=> $single_lead_id));
										}
									}
								}
							}

						}else if($post_details['table_name'] == 'procurement') {

							$user_details = $this->common_model->get_dynamic_data_sales_db('*', array('user_id'=>$post_details['user_id_new']), 'users', 'row_array');
							foreach ($post_details['lead_id'] as $single_lead_id) {
								$this->common_model->update_data_sales_db($post_details['table_name'], array($post_details['assigned_column_name']=>$user_details['name']), array($post_details['id_column_name']=> $single_lead_id));
							}

						}
					}
				break;

				case 'change_assign_department':
					
					// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;
					$data = array();
					if($post_details['department_name'] == 'sales_active') {

						$data = $this->prepare_listing_data_for_sales();
						$response['table_html'] = $this->load->view('sales_list', $data, true);

					}else if($post_details['department_name'] == 'sales_inactive') {

						$data = $this->prepare_listing_data_for_sales(0);
						$response['table_html'] = $this->load->view('sales_list', $data, true);

					}else if($post_details['department_name'] == 'procurement_active') {

						$data = $this->prepare_listing_data_for_procurement();
						// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
						$response['table_html'] = $this->load->view('procurement_list', $data, true);
					}else if($post_details['department_name'] == 'procurement_inactive') {

						$data = $this->prepare_listing_data_for_procurement(0);
						$response['table_html'] = $this->load->view('procurement_list', $data, true);

					}
				break;

				case 'daily_report_data_for_sales_person':

					// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;
					$table_data = array();
					$count_data = array();
					$count_data['desktopTime'] = '';
					$count_data['lead_count'] = array('call_connected'=>0, 'call_attempted'=>0, 'email'=>0, 'linkedin'=>0, 'whatsapp'=>0);
					if(!empty($post_details['sales_person_id'])){

						$where_array['contact_date >='] = date('Y-m-d 00:00:00', strtotime('-1 days'));
						$where_array['contact_date <='] = date('Y-m-d 23:59:59', strtotime('-1 days'));
						$where_array['user_id'] = $post_details['sales_person_id'];
						$daily_report_where_array = array('user_id'=> $post_details['sales_person_id']);
						$desktrack_where_array = array();
						// $where_array['filter_date'] = date('Y-m-d', strtotime('-1 days'));
						if(!empty($post_details['filter_date'])) {

							$explode_filter_date = explode(' - ', $post_details['filter_date']);
							if(count($explode_filter_date) == 1){

								$where_array['contact_date >='] = date('Y-m-d 00:00:00', strtotime($explode_filter_date[0]));
								$where_array['contact_date <='] = date('Y-m-d 23:59:59', strtotime($explode_filter_date[0]));
								$daily_report_where_array['date'] = date('Y-m-d', strtotime($explode_filter_date[0]));
								$desktrack_where_array['date'] = date('Y-m-d', strtotime($explode_filter_date[0]));
							}elseif(count($explode_filter_date) == 2){

								if($explode_filter_date[0] != $explode_filter_date[1]){

									$where_array['contact_date >='] = date('Y-m-d 00:00:00', strtotime($explode_filter_date[0]));
									$where_array['contact_date <='] = date('Y-m-d 23:59:59', strtotime($explode_filter_date[1]));
									$daily_report_where_array['date >='] = date('Y-m-d', strtotime($explode_filter_date[0]));
									$daily_report_where_array['date <='] = date('Y-m-d', strtotime($explode_filter_date[1]));
									$desktrack_where_array['date >='] = date('Y-m-d', strtotime($explode_filter_date[0]));
									$desktrack_where_array['date <='] = date('Y-m-d', strtotime($explode_filter_date[1]));
								}else{

									$where_array['contact_date >='] = date('Y-m-d 00:00:00', strtotime($explode_filter_date[0]));
									$where_array['contact_date <='] = date('Y-m-d 23:59:59', strtotime($explode_filter_date[0]));
									$daily_report_where_array['date'] = date('Y-m-d', strtotime($explode_filter_date[0]));
									$desktrack_where_array['date'] = date('Y-m-d', strtotime($explode_filter_date[0]));
								}
							}
							// $where_array['filter_date'] = $post_details['filter_date'];
						}
						if(!empty($post_details['connect_mode'])) {

							// $where_array['connect_mode'] = $post_details['connect_mode'];
							$where_array['contact_mode'] = $post_details['connect_mode'];
						}
						if($post_details['daily_report_type'] == 'lead'){

							// $table_data =  $this->prepare_daily_report_data_of_sales_person($post_details['sales_person_id'], $where_array);
							$table_data =  $this->create_daily_report_for_sales_person('lead', $where_array);

							$table_data['call_type'] = 'daily_work_lead_table_body';
						}else if($post_details['daily_report_type'] == 'quotation'){

							// $table_data =  $this->prepare_quotation_data_of_sales_person($post_details['sales_person_id'], $where_array);
							$table_data =  $this->create_daily_report_for_sales_person('quotation', $where_array);
							$table_data['call_type'] = 'daily_work_quotation_table_body';
						}
						// echo "<pre>";print_r($table_data);echo"</pre><hr>";exit;
						$user_data = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>1, 'user_id'=> $post_details['sales_person_id']), 'users', 'row_array');
						$desktrack_where_array['desktrack_userid'] = $user_data['desktrack_user_id'];
						$desktop_time = $this->common_model->get_dynamic_data_sales_db('desktopTime', $desktrack_where_array,'machine_time');
						$desktrack_time_array = array();
						if(!empty($desktop_time)) {

							$desktrack_time_array[0] = 0;
							$desktrack_time_array[1] = 0;
							$desktrack_time_array[2] = 0;
							foreach ($desktop_time as $single_desktop_time) {

								$desktop_time_explode = explode(':', $single_desktop_time['desktopTime']);
								$desktrack_time_array[0] += $desktop_time_explode[0];
								$desktrack_time_array[1] += $desktop_time_explode[1];
								$desktrack_time_array[2] += $desktop_time_explode[2];
							}
							$count_data['desktopTime'] = $desktrack_time_array[0]." hrs ".$desktrack_time_array[1]." mins ".$desktrack_time_array[2]. " secs"; 
						}
						$count_data['lead_count'] = $table_data['lead_count'];
						$count_data['call_type'] = 'daily_work_count';
					}

					$response['table_html'] = $this->load->view('common/sales_person_daily_report_body', $table_data, true);
					$response['count_html'] = $this->load->view('common/sales_person_daily_report_body', $count_data, true);
				break;

				case 'daily_report_listing_data_for_sales_person':

					if(!empty($this->input->post('user_id'))) {
						$data['user_list'] = array('call_connected'=>0, 'call_attempted'=>0, 'email'=>0, 'linkedin'=>0, 'whatsapp'=>0, 'desktrack'=>0);
						$data['call_connect_color'] = $data['call_attempt_color'] = $data['email_color'] = $data['linkedin_color'] = $data['whatsapp_color'] = $data['desktrack_color'] = 'brand';
						$user_data = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>1, 'user_id'=>$this->input->post('user_id')), 'users', 'row_array');
						$no_of_daily_report = 1;
						$hours = $minutes = $seconds = 0;
						$daily_report_where_array = $desktrack_where_array = array();
						$explode_filter_date = explode('-', $post_details['filter_date']);
						if(!empty($explode_filter_date)){

							$daily_report_where_array['contact_date >='] = date('Y-m-d 00:00:00', strtotime($explode_filter_date[0]));
							$daily_report_where_array['contact_date <='] = date('Y-m-d 23:59:59', strtotime($explode_filter_date[0]));
							$desktrack_where_array['date >='] = date('Y-m-d', strtotime($explode_filter_date[0]));
							$desktrack_where_array['date <='] = date('Y-m-d', strtotime($explode_filter_date[0]));
							if(count($explode_filter_date) == 2){

								if($explode_filter_date[0] != $explode_filter_date[1]){

									$daily_report_where_array['contact_date <='] = date('Y-m-d 23:59:59', strtotime($explode_filter_date[1]));
									$desktrack_where_array['date <='] = date('Y-m-d', strtotime($explode_filter_date[1]));
								}
							}
						}
						$daily_report_where_array['status'] = 'Active';
						$daily_report_where_array['user_id'] = $this->input->post('user_id');
						$data['user_list'] = $this->create_daily_report_count_for_sales_person($daily_report_where_array);
						// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
						$desktrack_where_array['desktrack_userid'] = $user_data['desktrack_user_id'];
						$desktop_time = $this->common_model->get_dynamic_data_sales_db('desktopTime', $desktrack_where_array,'machine_time');
						
						if(!empty($desktop_time)) {

							foreach ($desktop_time as $single_desktop_time) {

								$desktop_time_explode = explode(':', $single_desktop_time['desktopTime']);

								$hours += (int)$desktop_time_explode[0];
								$minutes += (int)$desktop_time_explode[1];
								$seconds += (int)$desktop_time_explode[2];
							}
							if($seconds > 59){

								$minutes += round($seconds/60); 
								$seconds = $seconds%60; 
							}
							if($minutes > 59){

								$hours += round($minutes/60);
								$minutes = $minutes%60;
							}
						}
						$data['user_list']['desktrack'] = $hours." hours ".$minutes." minutes ".$seconds. " seconds";
						$sales_person_protocol = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'user_id'=>$this->input->post('user_id')), 'employee_protocol', 'row_array');
						if(!empty($sales_person_protocol)) {

							$explode_protocol_information =  json_decode($sales_person_protocol['protocol_information'], true);
							if($explode_protocol_information){

								$data['user_submitted_count'] = ((int)$data['user_list']['call'] + (int)$data['user_list']['call_attempted'] + (int)$data['user_list']['linkedin'] + (int)$data['user_list']['whatsapp']);
								$data['user_protocol_count'] = (($explode_protocol_information['call_connected'] + $explode_protocol_information['call_attempted']+ $explode_protocol_information['linkedin'] + $explode_protocol_information['whatsapp'])*$no_of_daily_report);
								$data['user_protocol_count_email'] = $explode_protocol_information['emails'];
								if($data['user_submitted_count'] >= $data['user_protocol_count']) {

									$data['call_connect_color'] = $data['call_attempt_color'] = $data['linkedin_color'] = $data['whatsapp_color'] = 'success';
								}else{

									$data['call_connect_color'] = $data['call_attempt_color'] = $data['linkedin_color'] = $data['whatsapp_color'] = 'danger';
								}

								if(((int)$data['user_list']['email']) >= ($explode_protocol_information['emails']*$no_of_daily_report)) {

									$data['email_color'] = 'success';
								}else{

									$data['email_color'] = 'danger';
								}
								$data['user_protocol_count_desktrack'] = $explode_protocol_information['desktrack_hours']." hours ".$explode_protocol_information['desktrack_minutes']." minutes";
								if($hours > $explode_protocol_information['desktrack_hours']){

									$data['desktrack_color'] = 'success';
								}else if($hours == $explode_protocol_information['desktrack_hours'] && $minutes >= $explode_protocol_information['desktrack_minutes']){
									$data['desktrack_color'] = 'success';
								}else{

									$data['desktrack_color'] = 'danger';
								}
							}
						}else{

							$data['user_submitted_count'] = $data['user_protocol_count'] = $data['user_protocol_count_email'] = $data['user_protocol_count_desktrack'] = 0;
						}
						// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
						$response['table_html'] = $this->load->view('common/sales_person_daily_report_listing_single', $data, true);
					}
				break;

				case 'add_sales_person_protocol':
					
					$where_string = "status = 1 AND role = 5";
					$data = array();
					$id = 0;
					$data['form_data']['user_id'] = $data['form_data']['call_connected'] = $data['form_data']['emails'] = $data['form_data']['desktrack_hours'] = $data['form_data']['desktrack_minutes'] = '';
					if(empty($post_details['user_id'])) {

						$sales_person_exits = $this->common_model->get_dynamic_data_sales_db('user_id', array('status'=>'Active'), 'employee_protocol');
						if(!empty($sales_person_exits)) {
							$where_string .= " AND user_id NOT IN ('".implode("', '", array_column($sales_person_exits, 'user_id'))."')";
						} 
					}else{

						$sales_person_details = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'user_id'=> $this->input->post('user_id')), 'employee_protocol', 'row_array');
						// echo "<pre>";print_r($sales_person_details);echo"</pre><hr>";
						if(!empty($sales_person_details)){

							$explode_protocol_information =  json_decode($sales_person_details['protocol_information'], true);
							// echo "<pre>";print_r($explode_protocol_information);echo"</pre><hr>";exit;
							if(!empty($explode_protocol_information)) {

								$id = $sales_person_details['id'];
								$data['form_data']['user_id'] = $sales_person_details['user_id']."_".$sales_person_details['department'];
								$data['form_data']['call_connected'] = $explode_protocol_information['call_connected'];
								$data['form_data']['emails'] = $explode_protocol_information['emails'];
								$data['form_data']['desktrack_hours'] = $explode_protocol_information['desktrack_hours'];
								$data['form_data']['desktrack_minutes'] = $explode_protocol_information['desktrack_minutes'];

							}
						}
						$where_string .= " AND user_id = ".$post_details['user_id'];
						
					}
					$data['user_list'] = $this->common_model->get_all_data('user_id, name, role', $where_string, 'users');

					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
					$response['modal_body'] = $this->load->view('common/sales_person_protocol_modal_form_body', $data, true);
					$response['id'] = $id;
				break;

				case 'save_sales_person_protocol':

					// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;
					// echo "<pre>";print_r($this->input->post('form_data'));echo"</pre><hr>";exit;
					$protocol_information = $insert_data = array();
					if(!empty($post_details['form_data'])) {

						foreach($post_details['form_data'] as $form_data){

							if(in_array($form_data['name'], array('user_id'))){

								$explode_user_details =  explode("_", $form_data['value']); 
								$insert_data['user_id'] = $explode_user_details[0];
								$insert_data['department'] = $explode_user_details[1];

							}else{

								$protocol_information[$form_data['name']] = $form_data['value'];
							}

						}
						// echo "<pre>";print_r($insert_data);echo"</pre><hr>";
						// echo "<pre>";print_r($protocol_information);echo"</pre><hr>";exit;

						if(!empty($protocol_information)) {
							if($post_details['id'] == 0){
								$this->common_model->insert_data_sales_db('employee_protocol',array('user_id'=> $insert_data['user_id'], 'department'=> $insert_data['department'], 'protocol_information'=>json_encode($protocol_information)));
							}else {

								$this->common_model->update_data_sales_db('employee_protocol',array('protocol_information'=>json_encode($protocol_information)), array('id'=> $post_details['id']));
							}
						}
					}
				break;
				
				case 'make_module_active':
					
					// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;
					if(!empty($post_details['main_module_id']) && !empty($post_details['sub_module_id'])){

						$this->session->set_userdata('active_module_id', $post_details['main_module_id']);
						$this->session->set_userdata('active_sub_module_id', $post_details['sub_module_id']);
					}
				break;

				case 'sales_lead_count_highchart':

					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					$lead_stage = (!empty($this->input->post('lead_stage'))) ? $this->input->post('lead_stage'):'';
					$lead_type = (!empty($this->input->post('lead_type'))) ? $this->input->post('lead_type'):'';
					$lead_source = (!empty($this->input->post('lead_source'))) ? $this->input->post('lead_source'):'';
					$lead_country = (!empty($this->input->post('lead_country'))) ? $this->input->post('lead_country'):'';
					$lead_region = (!empty($this->input->post('lead_region'))) ? $this->input->post('lead_region'):'';
					$all_user_details = array_column($this->common_model->get_dynamic_data_sales_db('*',array(), 'users'),'name', 'user_id');
					$all_user_details[0] = 'Not Assigned';
					$sales_person_details = array_column($this->common_model->get_sales_person(),'name', 'user_id');
					$sales_person_details[0] = 'Blank Lead';
					$clients_where[0] = 'customer_mst.deleted is NULL';
					// echo "<pre>";print_r($all_user_details);echo"</pre><hr>";
					if(!empty($lead_stage)) {

						if(count($lead_stage) == 1){

							$clients_where[] = 'customer_mst.lead_stage = "'.implode(' ', $lead_stage).'"';
						} else {
							$clients_where[] = 'customer_mst.lead_stage in ("'.implode('","', $lead_stage).'")';
						}
					}
					if(!empty($lead_type)) {

						if(count($lead_type) == 1){

							$clients_where[] = 'customer_mst.lead_type = "'.implode(' ', $lead_type).'"';

						} else {
							$clients_where[] = 'customer_mst.lead_type in ("'.implode('","', $lead_type).'")';

						}
					}
					if(!empty($lead_country)) {

						if(count($lead_country) == 1){

							$clients_where[] = 'customer_mst.country_id = "'.implode(' ', $lead_country).'"';
						} else {
							$clients_where[] = 'customer_mst.country_id in ("'.implode('","', $lead_country).'")';

						}
					}
					if(!empty($lead_region)) {

						if(count($lead_region) == 1){

							$clients_where[] = 'customer_mst.region_id = "'.implode(' ', $lead_region).'"';

						} else {
							$clients_where[] = 'customer_mst.region_id in ("'.implode('","', $lead_region).'")';

						}
					}
					$lead_assign_data = $this->common_model->get_sales_person_lead_assign_data($clients_where);
					// echo "<pre>";print_r($lead_assign_data);echo"</pre><hr>";exit;


					$all_lead_data = array();
					foreach($lead_assign_data['client_leads'] as $single_lead_data) {

						$all_lead_data[$single_lead_data['sales_person_id']]['count'] += $single_lead_data['count'];
						$all_lead_data[$single_lead_data['sales_person_id']]['stage_6'] += $single_lead_data['stage_6'];
						$all_lead_data[$single_lead_data['sales_person_id']]['stage_5'] += $single_lead_data['stage_5'];
						$all_lead_data[$single_lead_data['sales_person_id']]['stage_4'] += $single_lead_data['stage_4'];
						$all_lead_data[$single_lead_data['sales_person_id']]['stage_3'] += $single_lead_data['stage_3'];
						$all_lead_data[$single_lead_data['sales_person_id']]['stage_2'] += $single_lead_data['stage_2'];
						$all_lead_data[$single_lead_data['sales_person_id']]['stage_1'] += $single_lead_data['stage_1'];
						$all_lead_data[$single_lead_data['sales_person_id']]['stage_0'] += $single_lead_data['stage_0'];
						$all_lead_data[$single_lead_data['sales_person_id']]['blank_stage'] += $single_lead_data['blank_stage'];
					}
					// echo "<pre>";print_r($all_lead_data);echo"</pre><hr>";die('debug');
					// arsort($all_lead_data);
					$i=0;
					foreach($all_lead_data as $sales_person_id => $lead_details) {

						if(!empty($sales_person_id)) {
							$response['sales_lead_count_highchart']['category'][$i] = $all_user_details[$sales_person_id];
							$response['sales_lead_count_highchart']['stage_6'][$i] = (int)$lead_details['stage_6'];
							$response['sales_lead_count_highchart']['stage_5'][$i] = (int)$lead_details['stage_5'];
							$response['sales_lead_count_highchart']['stage_4'][$i] = (int)$lead_details['stage_4'];
							$response['sales_lead_count_highchart']['stage_3'][$i] = (int)$lead_details['stage_3'];
							$response['sales_lead_count_highchart']['stage_2'][$i] = (int)$lead_details['stage_2'];
							$response['sales_lead_count_highchart']['stage_1'][$i] = (int)$lead_details['stage_1'];
							$response['sales_lead_count_highchart']['stage_0'][$i] = (int)$lead_details['stage_0'];
							$response['sales_lead_count_highchart']['blank_stage'][$i] = (int)$lead_details['blank_stage'];
							$i++;
						}
					}

					// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
					$response['html_body']['filter_sales_person'] = $this->load->view(
																				'quotations/filter_sales_person',
																				array(
																					'sales_person' => $sales_person_details,
																					'filter_sales_person' => 'All'
																				),
																				TRUE);
				break;

				case 'get_sales_person_routine':

					$data = array();
					$response['modal_body'] = '';
					// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;
					if(!empty($post_details['user_id'])){

						$all_color = array(
							0 => 'kt-widget2__item--primary',
							1 => 'kt-widget2__item--warning',
							2 => 'kt-widget2__item--brand',
							3 => 'kt-widget2__item--success',
							4 => 'kt-widget2__item--danger',
						);
						$data['user_id'] = $post_details['user_id'];
						$routine_list = $this->common_model->get_dynamic_data_sales_db('id, name', array('status'=>'Active'), 'sales_routine', 'result_array');
						$routine_assign_id = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'user_id'=> $post_details['user_id']), 'user_to_sales_routine'),'sales_routine_id');
						
						$data['routine_list'] = array();
						$data['routine_list']['routine_assign'] = $data['routine_list']['routine_unassign'] =  array();
						$date_filter = date('y-m-d');
						if(!empty($post_details['filter_date'])){

							$explode_filter_date = explode(" - ", $post_details['filter_date']);
							$date_filter = date('y-m-d', strtotime($explode_filter_date[0]));
						}
						foreach ($routine_list as $single_routine_list) {
							
							$status = '';
							$comment = '';
							$routine_details_today = $this->common_model->get_dynamic_data_sales_db('work_completed, work_comment', array('status'=>'Active', 'user_id'=> $data['user_id'], 'sales_routine_id'=> $single_routine_list['id'], 'date(work_date)'=> $date_filter), 'daily_work_on_sales_routine');
							if(!empty($routine_details_today)){

								$status = $routine_details_today[count($routine_details_today)-1]['work_completed'];
								$comment = $routine_details_today[count($routine_details_today)-1]['work_comment'];
							}

							if(in_array($single_routine_list['id'], $routine_assign_id)){

								
								$data['routine_list']['routine_assign'][] =  array(
									'id' => $single_routine_list['id'],
									'name' => $single_routine_list['name'],
									'selected' => 'checked',
									'work_completed' => $status,
									'work_comment' => $comment,
									'color' => $all_color[count($data['routine_list']['routine_assign'])%5],
								);
							}else{

								$data['routine_list']['routine_unassign'][] =  array(
									'id' => $single_routine_list['id'],
									'name' => $single_routine_list['name'],
									'selected' => '',
									'work_completed' => $status,
									'work_comment' => $comment,
									'color' => $all_color[count($data['routine_list']['routine_unassign'])%5],
								);
							}
						}
						// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
						$response['id'] = $post_details['user_id'];
						$response['modal_body'] = $this->load->view('common/sales_person_routine_modal_form_body', $data, true);
					}
					
				break;

				case 'update_sales_person_routine':

					$data = array();
					$data['user_id'] = $post_details['user_id'];
					$data['sales_routine_id'] = $post_details['routine_id'];
					
					$sales_person_routine = $this->common_model->get_dynamic_data_sales_db('*', $data, 'user_to_sales_routine','row_array');
					if(!empty($sales_person_routine)){

						$update_data = array('status'=>'Inactive');
						if($sales_person_routine['status']=='Inactive'){

							$update_data = array('status'=>'Active');
						}
						$this->common_model->update_data_sales_db('user_to_sales_routine', $update_data, $data);
					}else{

						$this->common_model->insert_data_sales_db('user_to_sales_routine', $data);
					}
				break;

				case 'get_department_details':

					$department_details = $this->common_model->get_dynamic_data_sales_db('id, name, mrf_department_id', array('status'=>'Active'), 'mrf_designation');
					$reporting_details = $this->common_model->get_dynamic_data_sales_db('user_id, name', array('status'=>'Active'), 'mrf_reporting_manager');
					$all_type_of_name = $this->common_model->get_dynamic_data_sales_db("user_id, name",array(), 'users');					
					$response['job_title_option_tag'] = '<option value="">Select Title</option>';
					foreach ($department_details as $single_department_details) {

						$response['job_title_option_tag'] .= '<option value="'.$single_department_details['id'].'">'.$single_department_details['name'].'</option>';
					}

					$response['reporting_to_option_tag'] = '<option value="">Select To</option>';
					foreach ($reporting_details as $single_reporting_details) {

						$response['reporting_to_option_tag'] .= '<option value="'.$single_reporting_details['user_id'].'">'.$single_reporting_details['name'].'</option>';
					}

					$response['all_type_of_name_option_tag'] = '<option value="">Select User</option>';
					foreach ($all_type_of_name as $single_all_type_of_name) {

						$response['all_type_of_name_option_tag'] .= '<option value="'.$single_all_type_of_name['user_id'].'">'.$single_all_type_of_name['name'].'</option>';
					}
				break;

				case 'add_manpower_form':
				case 'update_manpower_form':
					// echo "<pre>";print_r($post_details);echo"</pre><br>";exit;
					$assigned_to_array = $insert_update_array = array();
					foreach ($post_details['manpower_form'] as $single_form_data) {

						if($single_form_data['name'] == 'assigned_to'){

							$assigned_to_array[] = array('user_id'=> $single_form_data['value']);
						}else{

							$insert_update_array[$single_form_data['name']] = $single_form_data['value'];
						}
					}
					if(!empty($assigned_to_array)){
						$insert_update_array['assigned_to'] = json_encode($assigned_to_array);
					}

					if(!empty($insert_update_array)){

						if(empty($post_details['manpower_id'])){

							$insert_update_array['mrf_no'] = $this->get_mrf_no();
							$this->common_model->insert_data_sales_db('manpower_requisition_form',$insert_update_array);
						}else{

							$this->common_model->update_data_sales_db('manpower_requisition_form',$insert_update_array,array('id'=>$post_details['manpower_id']));
						}
					}
				break;

				case 'reassign_mrf_form':

					$response['status'] = 'failed';
					if(!empty($this->input->post('id')) && !empty($this->input->post('user_id'))){

						$this->common_model->update_data_sales_db('manpower_requisition_form',array('assigned_to'=>$this->input->post('user_id')),array('id'=>$this->input->post('id')));
						$response['status'] = 'successful';
					}
				break;

				case 'change_stage_mrf_form':
				
					$response['status'] = 'failed';
					if(!empty($this->input->post('id')) && !empty($this->input->post('stage'))){

						$this->common_model->update_data_sales_db('manpower_requisition_form',array('stage'=>$this->input->post('stage')),array('id'=>$this->input->post('id')));
						$response['status'] = 'successful';
					}
				break;

				case 'clone_mrf_position':

					$response['status'] = 'failed';
					if(!empty($this->input->post('id'))){

						$manpower_requisition_form_data = $this->common_model->get_dynamic_data_sales_db('mrf_no, mrf_department_id, no_of_positions, assigned_to, mrf_designation_id, reporting_to, location_site, priority_closingdate, type_of, type_of_name, job_description, total_experience, qualification, salary_range, stage, date',array('id'=>$this->input->post('id')), 'manpower_requisition_form', 'row_array');
						if(!empty($manpower_requisition_form_data)){

							$no_of_position = (int)$manpower_requisition_form_data['no_of_positions'];
							if($no_of_position > 1){

								$batch_insert_array = array();
								for ($i=2; $i <= $no_of_position; $i++) { 

									$batch_insert_array[$i] = $manpower_requisition_form_data;
									$batch_insert_array[$i]['mrf_no'] = $this->get_mrf_no();
									$batch_insert_array[$i]['no_of_positions'] = 1;
								}
								if(!empty($batch_insert_array)){

									$this->common_model->insert_data_sales_db('manpower_requisition_form', $batch_insert_array, 'batch');
									$response['status'] = 'successful';
									$this->common_model->update_data_sales_db('manpower_requisition_form',array('no_of_positions'=>1),array('id'=>$this->input->post('id')));
								}
							}
						}
					}
				break;	

				case 'delete_manpower_list_data':
					// echo "<pre>";print_r($post_details['id']);echo"</pre><br>";exit;
					$this->common_model->update_data_sales_db('manpower_requisition_form', array('status'=> 'Inactive'), array('id'=>$post_details['id']));
				break;

				case 'add_candidate_data':
				case 'update_candidate_data':
					// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;
					$candidate_data = array_column(($post_details['candidate_form']), 'value', 'name');
					if(!empty($post_details['candidate_id'])){

						if(empty($candidate_data['resume'])){

							if(!empty($this->session->userdata('candidate_resume'))){

								$candidate_data['resume'] = $this->session->userdata('candidate_resume');
								$this->common_model->update_data_sales_db('candidate',$candidate_data,array('id'=>$post_details['candidate_id']));
							}else{

								$this->common_model->update_data_sales_db('candidate',$candidate_data, array('id'=>$post_details['candidate_id']));
							}
						}
						else{

							$this->common_model->update_data_sales_db('candidate',$candidate_data, array('id'=>$post_details['candidate_id']));
						}
						$response['message'] = 'Candidate Successfully Added';
					}
					else{

						$candidate_list = $this->common_model->get_dynamic_data_sales_db('*', array('mobile'=>$candidate_data['mobile'], 'status'=>'Active'), 'candidate');

						if(empty($candidate_list)){

							$candidate_data['resume'] = $this->session->userdata('candidate_resume');
							$candidate_data['hr_user_id'] = $this->session->userdata('user_id');
							$this->common_model->insert_data_sales_db('candidate', $candidate_data);
							$response['message'] = 'Candidate Successfully Added';

						}else{

							foreach($candidate_list as $candidate_list_key => $single_candidate){

								$mrf_details = $this->common_model->get_dynamic_data_sales_db('*', array('id'=>$single_candidate['mrf_id']), 'manpower_requisition_form','row_array');

								if($candidate_data['mobile'] == $single_candidate['mobile']){

									$response['message'] = 'Already add this candidate '.$single_candidate['name'].' '.$single_candidate['mobile'].' aginst this '. $mrf_details['mrf_no'];
								}
							}
						}
					}
				break;

				case 'delete_candidate_data':
					$this->common_model->update_data_sales_db('candidate', array('status'=> 'Inactive'), array('id'=>$post_details['candidate_id']));
				break;

				case 'get_mrf_listing_data':

					// echo "<pre>";print_r($this->input->post());echo"</pre><br>";exit;
					if(!empty($this->input->post('tab_name'))){

						$where_response = $this->create_where_response_on_tab_name($this->input->post('search_form_data'), $this->input->post('tab_name'));
						$data = $this->prepare_listing_data($where_response['where'], $this->input->post('limit'), $this->input->post('offset'), $this->input->post('tab_name'));

						$data['search_filter'] = $where_response['search_filter'];
						$data['mrf_list'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'manpower_requisition_form');
						$data['mrf_assigned_to'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=> 1, 'role'=>13),'users');
						$data['deparment_list'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'mrf_department');
						$data['degignation_list'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'mrf_designation');
						$data['reporting_to_list'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'mrf_reporting_manager');
					}
					// echo "<pre>";print_r($data);echo"</pre><br>";exit;
					$response['list_body'] = $this->load->view('common/manpower_list_body', $data, true);
					$response['search_filter'] = $this->load->view('common/mrf_search_filter', $data, true);
					$response['mrf_list_paggination'] = $this->load->view('common/paggination', $data, true);
				break;

				case 'upload_candidate_resume':
					// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;
					$return_response = $this->upload_doc_config($resume);
					if($return_response['status'] == 'failed') {

						$return_response['msg'] = 'File is not uploaded.';
					} else {

						$set_session_data = $this->session->userdata();
						$set_session_data['candidate_resume'] = $return_response['file_name'];
						$this->session->set_userdata($set_session_data);
					}
				break;

				case 'get_search_filter_data':
						// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;
					$data['mrf_list'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'manpower_requisition_form');
					$data['mrf_assigned_to'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=> 1, 'role'=>13),'users');
					$data['deparment_list'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'mrf_department');
					$data['degignation_list'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'mrf_designation');
					$data['reporting_to_list'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'mrf_reporting_manager');

					$data['search_filter'] = array('mrf_no'=>'' , 'assigned_to'=>'','mrf_department_id'=>'', 'mrf_designation_id'=>'', 'user_id'=> '', 'date'=>'');
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
					$response['search_filter'] = $this->load->view('common/mrf_search_filter', $data, true);
				break;
				case 'get_candidate_stage':
					// echo "<pre>";print_r($this->input->post());echo"</pre><br>";exit;
					$response['update_stage_html'] = "";
					$response['candidate_id'] = $this->input->post('candidate_id');
					if(!empty($this->input->post('candidate_id'))){

						$data['candidate_id'] = $this->input->post('candidate_id');
						$data['stage_id'] = $this->input->post('stage_id');
						$data['stage_reason_id'] = $this->input->post('stage_reason_id');
						$data['other_reason'] = $this->input->post('other_reason');

						$data['stage_list'] = $this->common_model->get_dynamic_data_sales_db('id, name', array('status'=>'Active'),'candidate_stage');
						$data['stage_reason_list'] = $this->common_model->get_dynamic_data_sales_db('id, name', array('status'=>'Active'),'stage_reason');
						// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
						$response['update_stage_html'] = $this->load->view('common/update_stage_html', $data, true);
					}
				break;

				case 'save_candidate_stage':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					if(!empty($this->input->post('candidate_id'))){

						$form_data = array_column($this->input->post('update_stage_form'), 'value', 'name');
						$form_data['update_time'] = date('Y-m-d H:i:s');

						$this->common_model->update_data_sales_db('candidate', $form_data, array('id'=> $this->input->post('candidate_id')));
					}
				break;

				case 'get_candidate_comment':

					$response['comment_history'] = '';
					if(!empty($this->input->post('candidate_id'))){

						$comment_details = $this->common_model->get_dynamic_data_sales_db('name, comment_info', array('id'=> $this->input->post('candidate_id')), 'candidate', 'row_array');

						// echo "<pre>";print_r($comment_details);echo"</pre><hr>";exit;
						if(!empty($comment_details)){

							$data['comment_details'] = array();
							$user_details = $this->common_model->get_all_conditional_data_sales_db(
								'users.user_id, users.name, people_information.profile_pic_file_path',
								array(),
								'users',
								'result_array',
								array(
									'table_name'=> 'people_information',
									'condition'=> 'people_information.user_id = users.user_id',
									'type'=> 'LEFT'
								)
							);
							foreach ($user_details as $single_user_details) {

								$user_information[$single_user_details['user_id']]['name'] = $single_user_details['name'];
								$user_information[$single_user_details['user_id']]['profile_pic_file_path'] = $single_user_details['profile_pic_file_path'];
							}
							// echo "<pre>";print_r($user_information);echo"</pre><hr>";
							foreach (json_decode($comment_details['comment_info'], true) as $comment_key => $single_user_details) {

								// echo "<pre>";print_r($single_user_details);echo"</pre><hr>";exit;
								$data['comment_details'][$comment_key]['profile_path'] = "assets/media/users/default.jpg";
								if(!empty($user_information[$single_user_details['user_id']]['profile_pic_file_path'])){

									$data['comment_details'][$comment_key]['profile_path'] = "assets/hr_document/profile_pic/".$user_information[$single_user_details['user_id']]['profile_pic_file_path'];
								}
								$data['comment_details'][$comment_key]['user_name'] = $user_information[$single_user_details['user_id']]['name'];
								$data['comment_details'][$comment_key]['tat'] = $this->creat_tat($single_user_details['date_time']);
								$data['comment_details'][$comment_key]['message'] = $single_user_details['message'];
							}

							// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
							$response['comment_history'] = $this->load->view('common/candidate_comment_history', $data, true);
							$response['name'] = $comment_details['name'];
						}
					}
				break;

				case 'save_candidate_comment':

					$response['status'] = 'failed';
					if(!empty($this->input->post('candidate_id')) && !empty($this->input->post('message'))){

						$comment_details = array();
						$old_comment_details = $this->common_model->get_dynamic_data_sales_db('comment_info', array('id'=> $this->input->post('candidate_id')), 'candidate', 'row_array');
						if(!empty($old_comment_details)){

							$comment_details = json_decode($old_comment_details['comment_info'], true);
						}
						// echo "<pre>";print_r($comment_details);echo"</pre><hr>";
						$comment_details[] = array(
											'message'=> $this->input->post('message'),
											'user_id'=> $this->session->userdata('user_id'),
											'date_time'=> date("Y-m-d\TH:i:s\Z"),
										);
						if(!empty($comment_details)){
							// echo "<pre>";print_r(json_encode($comment_details));echo"</pre><hr>";exit;
							$this->common_model->update_data_sales_db('candidate', array('comment_info'=> json_encode($comment_details)), array('id'=> $this->input->post('candidate_id')));
							$response['status'] = 'successful';
						}
						// echo "<pre>";print_r(json_encode($comment_details));echo"</pre><hr>";exit;
					}
				break;

				case 'add_candidate_evaluation':
				case 'update_candidate_evaluation':
					// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;
					$evaluation_form = array_column(($post_details['candidate_evaluation_form']), 'value', 'name');

					$sub_reporting_to_array = array();
					foreach ($post_details['candidate_evaluation_form'] as $single_form_data) {

						if($single_form_data['name'] == 'sub_reporting_to'){

							$sub_reporting_to_array[] = array('user_id'=> $single_form_data['value']);
						}else{

							$evaluation_form[$single_form_data['name']] = $single_form_data['value'];
						}
					}
					if(!empty($sub_reporting_to_array)){
						$evaluation_form['sub_reporting_to'] = json_encode($sub_reporting_to_array);
					}

					// echo "<pre>";print_r($evaluation_form['sub_reporting_to']);echo"</pre><hr>";exit;

					if(!empty($post_details['evaluation_id'])){
						$evaluation_form['candidate_id'] = $post_details['candidate_id'];
						$evaluation_form['update_time'] = date('Y-m-d H:i:s');
						if(!empty($this->session->userdata('candidate_photo'))){

							$evaluation_form['photo'] = $this->session->userdata('candidate_photo');
						}
						$this->common_model->update_data_sales_db('candidate_evaluation', $evaluation_form, array('id'=>$post_details['evaluation_id']));
					}else{

						$evaluation_form['candidate_id'] = $post_details['candidate_id'];
						if(!empty($this->session->userdata('candidate_photo'))){

							$evaluation_form['photo'] = $this->session->userdata('candidate_photo');
						}
						$this->common_model->insert_data_sales_db('candidate_evaluation', $evaluation_form);
					}
				break;

				case 'upload_candidate_photo':
					// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;
					$return_response = $this->upload_photo_config($photo);
					if($return_response['status'] == 'failed') {
						$return_response['msg'] = 'File is not uploaded.';
					} else {
						$set_session_data = $this->session->userdata();
						$set_session_data['candidate_photo'] = $return_response['file_name'];
						$this->session->set_userdata($set_session_data);
					}
				break;

				case 'candidate_search_filter':
				case 'paggination_filter':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					if(!empty($this->input->post('mrf_id'))){
						$where_response = $this->create_where_response_on_search($this->input->post('search_form_data'),$this->input->post('mrf_id'));
						$data = $this->prepare_candidate_list_data($where_response['where'], array('column_name'=>'id', 'column_value'=>'desc'), $this->input->post('limit'), $this->input->post('offset'));
						$data['mrf_id'] = $this->input->post('mrf_id');
						$data['search_filter'] = $where_response['search_filter'];
						$data['candidate_detail'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'mrf_id'=>$this->input->post('mrf_id')),'candidate');
						$data['source_list'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'candidate_source');
						$data['stage_list'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'candidate_stage');
						$data['assigned_to_list'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=> 1, 'role'=>13),'users');

						$response['list_body'] = $this->load->view('common/candidate_list_body', $data, true);
						$response['search_filter'] = $this->load->view('common/candidate_search_filter', $data, true);
						$response['paggination'] = $this->load->view('common/paggination', $data, true);
					}
				break;

				case 'get_candidate_search_filter_data':
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
					if(!empty($this->input->post('mrf_id'))){
						$data['candidate_detail'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'mrf_id'=>$this->input->post('mrf_id')),'candidate');
						$data['source_list'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'candidate_source');
						$data['stage_list'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'candidate_stage');
						$data['assigned_to_list'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=> 1, 'role'=>13),'users');

						$data['search_filter'] = array('candidate_id'=>array() ,'source_id'=>array(), 'stage_id'=>array(), 'hr_user_id'=>array());
						$response['search_filter'] = $this->load->view('common/candidate_search_filter', $data, true);
					}
				break;

				case 'get_candidate_mrf_id':
					$response['change_mrf'] = "";
					$response['candidate_id'] = $this->input->post('candidate_id');
					if(!empty($this->input->post('candidate_id'))){

						$data['candidate_id'] = $this->input->post('candidate_id');
						$data['mrf_id'] = $this->input->post('mrf_id');
						$data['mrf_id_list'] = $this->common_model->get_dynamic_data_sales_db('id, mrf_no', array('status'=>'Active'),'manpower_requisition_form');

						$response['change_mrf'] = $this->load->view('common/change_mrf', $data, true);
					}
				break;

				case 'save_candidate_mrf':
					if(!empty($this->input->post('candidate_id'))){

						$form_data = array_column($this->input->post('update_mrf_form'), 'value', 'name');
						$form_data['update_time'] = date('Y-m-d H:i:s');
						$this->common_model->update_data_sales_db('candidate', $form_data, array('id'=> $this->input->post('candidate_id')));
					}
				break;

		 		default:
		 			$response['status'] = 'failed';	
		 			$response['message'] = "call type not found";
	 			break;
		 	}
		 	echo json_encode($response);

       	}else{
		 	die('access is not allowed to this function');
		}
	}


	private function prepare_listing_data($where_array, $limit, $offset, $tab_name){

		$data = $this->common_model->get_manpower_list($where_array, $limit, $offset);

		$data['assigned_details'] = array_column($this->common_model->get_dynamic_data_sales_db('user_id, name', array('status'=>1, 'role'=> 13),'users'), 'name', 'user_id');

		$user_list = array_column($this->common_model->get_dynamic_data_sales_db('user_id, name', array('status'=>1),'users'), 'name', 'user_id');
		$user_list[0] = '';

		$department_list = array_column($this->common_model->get_dynamic_data_sales_db('id, name', array('status'=>'Active'),'mrf_department'), 'name','id');
		$department_list[0] = '';

		$designation_list = array_column($this->common_model->get_dynamic_data_sales_db('id, name', array(),'mrf_designation'), 'name','id');
		$designation_list[0] = '';

		foreach($data['manpower_list'] as $key => $val){

			$explode_name = explode(" ", $user_list[$val['reporting_to']]);
			$data['manpower_list'][$key]['reporting_name'] = $explode_name[0];
			$data['manpower_list'][$key]['department_name'] = $department_list[$val['mrf_department_id']];
			$data['manpower_list'][$key]['user_designation_name'] = $designation_list[$val['mrf_designation_id']];
			$data['manpower_list'][$key]['stage_span'] = $designation_list[$val['mrf_designation_id']];
			$data['manpower_list'][$key]['no_of_position'] = (int)$val['no_of_positions'];
			
			if($val['priority_closingdate'] == 'Low'){

				$data['manpower_list'][$key]['priority_span'] = '<span class="kt-badge kt-badge--unified-success kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline mrf_font ">'.$val['priority_closingdate'].'</span>';
			}elseif($val['priority_closingdate'] == 'Medium'){

				$data['manpower_list'][$key]['priority_span'] = '<span class="kt-badge kt-badge--unified-warning kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline mrf_font ">'.$val['priority_closingdate'].'</span>';
			}elseif($val['priority_closingdate'] == 'High'){

				$data['manpower_list'][$key]['priority_span'] = '<span class="kt-badge kt-badge--unified-danger kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline mrf_font ">'.$val['priority_closingdate'].'</span>';
			}

			$get_candidate_count = $this->common_model->get_dynamic_data_sales_db('count(mrf_id) as mrf_id', array('status'=>'Active','mrf_id'=>$val['id']),'candidate', 'row_array');
			$data['manpower_list'][$key]['candidate_count'] = $get_candidate_count['mrf_id'];

			$data['manpower_list'][$key]['assigned_to_list'] = json_decode($val['assigned_to'], true);

			$join_candidate_count = $this->common_model->get_dynamic_data_sales_db('count(id) as join_count', array('status'=>'Active','mrf_id'=>$val['id'],'stage_id'=>12),'candidate', 'row_array');

			if(!empty($join_candidate_count)){

				$data['manpower_list'][$key]['total_no_of_positions'] = $data['manpower_list'][$key]['no_of_positions'] - $join_candidate_count['join_count'];
			}
		}

		$data['paggination_data']['limit'] = $limit;
		$data['paggination_data']['offset'] = $offset;
		$data['tab_name'] = $tab_name;
		// echo "<pre>";print_r($data);echo"</pre><br>";exit;

		return $data;
	}
	private function create_where_response_on_tab_name($search_form_data, $tab_name){

		// echo "<pre>";print_r($search_form_data);echo"</pre><br>";die;
		$return_where['search_filter'] = array('mrf_no'=>'', 'assigned_to'=>array(), 'mrf_department_id'=>array(), 'mrf_designation_id'=>array(), 'reporting_to'=>array(), 'date'=>'');
		$mrf_assigned_to = $mrf_department_id = $mrf_designation_id = $reporting_to = array();

		$return_where['where'] = "stage IN ('open') AND status = 'Active'";
		if($tab_name == 'closed_list') {

			$return_where['where'] = "stage IN ('close') AND status = 'Active'";

		}else if($tab_name == 'join_list') {
			$return_where['where'] = "stage IN ('join') AND status = 'Active'";

		}else if($tab_name == 'hold_list') {
			$return_where['where'] = "stage IN ('hold') AND status = 'Active'";
		}

		if(!empty($search_form_data)){

			foreach ($search_form_data as $form_data) {
				if(!empty($form_data['value'])){

					switch ($form_data['name']) {

						case 'mrf_no':
							$return_where['where'] .= " AND mrf_no like '%".$form_data['value']."%'"; 
							$return_where['search_filter']['mrf_no'] = $form_data['value'];
						break;

						case 'assigned_to':
							$mrf_assigned_to[] .= " assigned_to LIKE '%".$form_data['value']."%'";
							$return_where['search_filter']['assigned_to'][] = $form_data['value'];
						break;

						case 'mrf_department_id':
							$mrf_department_id[] = $form_data['value'];
							$return_where['search_filter']['mrf_department_id'][] = $form_data['value'];
						break;

						case 'mrf_designation_id':
							$mrf_designation_id[] = $form_data['value'];
							$return_where['search_filter']['mrf_designation_id'][] = $form_data['value'];
						break;

						case 'reporting_to':
							$reporting_to[] = $form_data['value'];
							$return_where['search_filter']['reporting_to'][] = $form_data['value'];
						break;

						case 'date':
							$filter_date = explode(' - ', $form_data['value']);
						    $return_where['where'] .= " AND date >= '".date('Y-m-d 00:00:00', strtotime($filter_date[0]))."'";
						    $return_where['search_filter']['date'] = $form_data['value'];
						    if(count($filter_date) > 1) {
						        $return_where['where'] .= " AND date <= '".date('Y-m-d 23:59:59', strtotime($filter_date[1]))."'";
						    }else {
						        $return_where['where'] .= " AND date <= '".date('Y-m-d 23:59:59', strtotime($filter_date[0]))."'";
						    }
							$data[$form_data['name']] = $form_data['value'];
						break;
						default:
						break;
					}
				}
			}

			if (!empty($mrf_assigned_to)) {
				$return_where['where'] .= " AND (" . implode(' OR ', $mrf_assigned_to) . ")";
			}
			if (!empty($mrf_department_id)) {
				$return_where['where'] .= " AND mrf_department_id IN (" . implode(', ', $mrf_department_id) . ")";
			}
			if (!empty($mrf_designation_id)) {
				$return_where['where'] .= " AND mrf_designation_id IN (" . implode(', ', $mrf_designation_id) . ")";
			}
			if(!empty($reporting_to)){
				$return_where['where'] .= " AND reporting_to IN (" . implode(', ', $reporting_to) . ")";
			}
			// echo "<pre>";print_r($return_where);echo"</pre><br>";exit;
		}

		if(!in_array($this->session->userdata('role'), array(1, 17))){

			$hr_id = $this->session->userdata('hr_access')['hr_user_id'];
			if($this->session->userdata('role') == 13){

				$return_where['where'] .= " AND (";
				foreach($hr_id as $key => $single_hr_id){

					if($key > 0){
						$return_where['where'] .= " OR ";
					}
					$return_where['where'] .= " assigned_to LIKE '%".$single_hr_id."%'";
				}
				$return_where['where'] .= ")";

			}else{

				$return_where['where'] .= " AND reporting_to IN (".implode(', ',$this->session->userdata('hr_access')['reporting_to_user_id']).")";
			}
		}
		// echo "<pre>";print_r($return_where);echo"</pre><br>";exit;
		return $return_where;
	}
	public function create_access_for_all(){

		$users_details = $this->common_model->get_dynamic_data_sales_db_null_false('*', "status = 1", 'users');
		
		foreach ($users_details as $single_users_details) {
			
			//check record already exist or not
			$single_users_check = $this->common_model->get_dynamic_data_sales_db('*',array('status'=>'Active', 'user_id'=>$single_users_details['user_id']), 'manager_access_details');
			if(empty($single_users_check)){

				$this->common_model->insert_data_sales_db('manager_access_details', array('user_id'=>$single_users_details['user_id']));
			}
		}

		###PRODUCTION MODULE###		
		foreach ($users_details as $single_users_details) {
			
			if(in_array($single_users_details['role'], array(1, 5, 6, 16, 17))){

				$production_access_details = array(array('name'=>'yta_tab_access', 'access'=>'true'), array('name'=>'listing_filter_client_name', 'access'=>'true'));
				//check record already exist or not
				$check_user_exists = $this->common_model->get_dynamic_data_sales_db('*',array('status'=>'Active', 'user_id'=>$single_users_details['user_id']), 'manager_access_details', 'row_array');
				if(!empty($check_user_exists)){

					if($single_users_details['role'] == 1 || in_array($check_user_exists['user_id'], array(65, 16, 15))){

						$production_access_details[] = array('name'=>'status_wise_count', 'access'=>'true');
					}
					if(in_array($check_user_exists['user_id'], array(218, 219))){

						if(in_array($check_user_exists['user_id'], array(218))){

							$production_access_details[] = array('name'=>'sales_user_id', 'ids'=>'8, 11, 15, 129, 219');
						}else{

							$production_access_details[] = array('name'=>'sales_user_id', 'ids'=>'8, 11, 15, 129, 218');
						}
						
					}
					if(in_array($single_users_details['user_id'], array(210))){
						$production_access_details[] = array('name'=>'production_list_action_tab_access_procurment_user', 'access'=>'true');
					}
					if(!empty($check_user_exists['production'])){

						$this->common_model->update_data_sales_db('manager_access_details', array('production'=> json_encode($production_access_details)), array('id'=> $check_user_exists['id']));
					}
					$this->common_model->insert_data_sales_db('manager_access_details', array('production'=> json_encode($production_access_details)), array('id'=> $check_user_exists['id']));
				}
			}
		}


		### QUERY MODULE ###		
		foreach ($users_details as $single_users_details) {
			
			// if(in_array($single_users_details['role'], array(1, 5, 6, 8, 16, 17))){

			$query_access_details = array();
			//check record already exist or not
			$check_user_exists = $this->common_model->get_dynamic_data_sales_db('*',array('status'=>'Active', 'user_id'=>$single_users_details['user_id']), 'manager_access_details', 'row_array');
			if(!empty($check_user_exists)){

				if($check_user_exists['user_id'] == 218){

					$query_access_details[] = array('name'=>'sales_user_id', 'ids'=>'8, 11, 15, 129, 219');
				}elseif($check_user_exists['user_id'] == 219){

					$query_access_details[] = array('name'=>'sales_user_id', 'ids'=>'8, 11, 15, 129, 218');
				}elseif($check_user_exists['user_id'] == 193){

					$query_access_details[] = array('name'=>'procurement_user_id', 'ids'=>'189, 205, 225');
				}

				if(in_array($single_users_details['user_id'], array(2, 59, 245))){

					$query_access_details[] = array('name'=>'query_list_comment_tab_access', 'access'=>'true');
				}

				if(!empty($check_user_exists['query'])){

					$this->common_model->update_data_sales_db('manager_access_details', array('query'=> json_encode($query_access_details)), array('id'=> $check_user_exists['id']));
				}
				$this->common_model->insert_data_sales_db('manager_access_details', array('query'=> json_encode($query_access_details)), array('id'=> $check_user_exists['id']));
			}
			// }
		}

		### RFQ MODULE ###		
		foreach ($users_details as $single_users_details) {

			$rfq_access_details = array();
			//check record already exist or not
			$check_user_exists = $this->common_model->get_dynamic_data_sales_db('*',array('status'=>'Active', 'user_id'=>$single_users_details['user_id']), 'manager_access_details', 'row_array');
			if(!empty($check_user_exists)){

				if($check_user_exists['user_id'] == 218){

					$rfq_access_details[] = array('name'=>'sales_user_id', 'ids'=>'8, 11, 15, 129, 219');
				}elseif($check_user_exists['user_id'] == 219){

					$rfq_access_details[] = array('name'=>'sales_user_id', 'ids'=>'8, 11, 15, 129, 218');
				}elseif($check_user_exists['user_id'] == 193){

					$rfq_access_details[] = array('name'=>'procurement_user_id', 'ids'=>'189, 205, 225');
				}
				$this->common_model->update_data_sales_db('manager_access_details', array('rfq'=> json_encode($rfq_access_details)), array('id'=> $check_user_exists['id']));
			}
		}

		### PQ MODULE ###		
		foreach ($users_details as $single_users_details) {

			$pq_access_details = array();
			//check record already exist or not
			$check_user_exists = $this->common_model->get_dynamic_data_sales_db('*',array('status'=>'Active', 'user_id'=>$single_users_details['user_id']), 'manager_access_details', 'row_array');
			if(!empty($check_user_exists)){

				if($check_user_exists['user_id'] == 218){

					$pq_access_details[] = array('name'=>'sales_user_id', 'ids'=>'8, 11, 15, 129, 219');
				}elseif($check_user_exists['user_id'] == 219){

					$pq_access_details[] = array('name'=>'sales_user_id', 'ids'=>'8, 11, 15, 129, 218');
				}
				$this->common_model->update_data_sales_db('manager_access_details', array('pq'=> json_encode($pq_access_details)), array('id'=> $check_user_exists['id']));
			}
		}

		### LEAD MODULE ###		
		foreach ($users_details as $single_users_details) {

			$lead_access_details = array();
			//check record already exist or not
			$check_user_exists = $this->common_model->get_dynamic_data_sales_db('*',array('status'=>'Active', 'user_id'=>$single_users_details['user_id']), 'manager_access_details', 'row_array');
			if(!empty($check_user_exists)){

				if($check_user_exists['user_id'] == 218){

					$lead_access_details[] = array('name'=>'sales_user_id', 'ids'=>'8, 11, 15, 129, 219');
					$lead_access_details[] = array('name'=>'country_id', 'ids'=>'198');
				}elseif($check_user_exists['user_id'] == 219){

					$lead_access_details[] = array('name'=>'sales_user_id', 'ids'=>'8, 11, 15, 129, 218');
					$lead_access_details[] = array('name'=>'country_id', 'ids'=>'198');
				}
				$this->common_model->update_data_sales_db('manager_access_details', array('lead'=> json_encode($lead_access_details)), array('id'=> $check_user_exists['id']));
			}
		}

		### QUOTATION MODULE ###		
		foreach ($users_details as $single_users_details) {

			$quotation_access_details = array();
			//check record already exist or not
			$check_user_exists = $this->common_model->get_dynamic_data_sales_db('*',array('status'=>'Active', 'user_id'=>$single_users_details['user_id']), 'manager_access_details', 'row_array');
			if(!empty($check_user_exists)){

				if($check_user_exists['user_id'] == 218){

					$quotation_access_details[] = array('name'=>'sales_user_id', 'ids'=>'8, 11, 15, 129, 219');
				}elseif($check_user_exists['user_id'] == 219){

					$quotation_access_details[] = array('name'=>'sales_user_id', 'ids'=>'8, 11, 15, 129, 218');
				}elseif(in_array($single_users_details['user_id'], array(210))){

					$quotation_access_details[] = array('name'=>'quotation_form_margin_and_unit_price_access', 'access'=>'true');
				}
				if(!in_array($single_users_details['user_id'], array(79,85,133,141,179,180,189,205,225,234))){

					$quotation_access_details[] = array('name'=>'quotation_pdf_client_name_access', 'access'=>'true');
				}
				if(!in_array($single_users_details['user_id'], array(209, 216, 191))){

					$quotation_access_details[] = array('name'=>'quotation_pdf_line_item_amount_access', 'access'=>'true');
				}
				if(!in_array($single_users_details['user_id'], array(191))){

					$quotation_access_details[] = array('name'=>'quotation_pdf_total_amount_access', 'access'=>'true');
				}

				if(!empty($check_user_exists['quotation'])){

					$this->common_model->update_data_sales_db('manager_access_details', array('quotation'=> json_encode($quotation_access_details)), array('id'=> $check_user_exists['id']));
				}
				$this->common_model->insert_data_sales_db('manager_access_details', array('quotation'=> json_encode($quotation_access_details)), array('id'=> $check_user_exists['id']));
 			}
		}
	}	
	private function prepare_sales_lead_data_for_update($user_details) {

		$static_lead_name = array(
			'chemical companies'=> 'Piping Chemical Companies', 'distributors'=> 'Tube Fittings Distributors',
			'epc companies'=> 'Piping EPC Companies', 'forged fittings'=> 'Forged Fittings',
			'heteregenous tubes india'=>'Tube Fittings & Valves India','pvf companies'=>'Instrumentation PVF Companies', 'shipyards'=> 'Piping Shipyards Companies', 'sugar companies'=> 'Piping Sugar Companies', 'water companies'=> 'Piping Water Companies', 'hammer union'=> 'Primary Leads - Hammer Union', 'pipes'=> 'Primary Leads - Piping Products', 'process_control'=> 'Primary Leads - Process Control', 'tubes'=> 'Primary Leads - Tube Fittings & Valves', 'tubing'=> 'Primary Leads - Tubing',
			'miscellaneous leads'=> 'Miscellaneous Leads', 'Internal Data'=> 'Internal Data',
			'process control'=> 'PROCESS CONTROL OLD', 'Hydraulic fitting'=> 'Hydraulic fitting','Fasteners'=> 'Fasteners');

		$data['lead_data'] = array();
		if(!empty($user_details)){

			//getting user list of sales team
			$user_list = $this->common_model->get_dynamic_data_sales_db('name, user_id, role', 'status=1 AND (role= 16 OR role= 5) AND user_id!='.$user_details['user_id'], 'users');
			$role_list = array_column($this->common_model->get_dynamic_data_sales_db('*', array(), 'role'), 'role_name', 'role_id');
			foreach ($user_list as $value) {
				if(!empty($role_list[$value['role']])) {

					$data['user_list'][$role_list[$value['role']]][]= $value;
				}
			}

			//getting client details for sales person
			$client_data = $this->common_model->get_primary_hetro_lead_count('deleted IS Null AND product_name IS not Null AND assigned_to ='.$user_details['user_id']);

			if(!empty($client_data)) {
				foreach ($client_data as $single_client_data) {
					array_push($data['lead_data'], array('table_name'=> 'customer_mst', 'lead_type'=> $single_client_data['product_name'], 'lead_name'=> $static_lead_name[$single_client_data['product_name']], 'count'=> $single_client_data['count']));
				}
			}
			
			// getting pre qualification count
			$pre_qualifiction_data = $this->common_model->get_count('is_pq = "Yes" AND assigned_to ='.$user_details['user_id'], 'customer_mst');
			if($pre_qualifiction_data['count'] != 0) {

				array_push($data['lead_data'], array('table_name'=> 'customer_mst', 'lead_type'=> 'Pre Qualification', 'lead_name'=> 'Pre Qualification', 'count'=> $pre_qualifiction_data['count']));
			}

			// getting procurement count
			$procuremet_data = $this->common_model->get_count('status = "Active" AND sales_person ="'.$user_details['name'].'"', 'procurement');
			if($procuremet_data['count'] != 0) {

				array_push($data['lead_data'], array('table_name'=> 'procurement', 'lead_type'=> 'Purchase Order', 'lead_name'=> 'Purchase Order', 'count'=> $procuremet_data['count']));
			}

			// getting rfq count
			$rfq_data = $this->common_model->get_count('rfq_sentby ='.$user_details['user_id'], 'rfq_mst');
			if($rfq_data['count'] != 0) {

				array_push($data['lead_data'], array('table_name'=> 'rfq_mst', 'lead_type'=> 'RFQ', 'lead_name'=> 'RFQ', 'count'=> $rfq_data['count']));
			}
			// array_push($data['lead_data'], array('table_name'=> 'rfq_mst', 'lead_name'=> 'Query'));

			// getting quotations count
			$quotations_data = $this->common_model->get_count('assigned_to ='.$user_details['user_id'], 'quotation_mst');
			if($quotations_data['count'] != 0) {

				array_push($data['lead_data'], array('table_name'=> 'quotation_mst', 'lead_type'=> 'Quotations', 'lead_name'=> 'Quotations', 'count'=> $quotations_data['count']));
			}

			//getting people information data
			$data['people_information'] = $this->common_model->get_all_conditional_data_sales_db('*', array('user_id'=>$user_details['user_id']), 'people_information', 'row_array', array(), array(), '');

			//getting internal data for sales person
			$internal_data = $this->common_model->get_internal_listing_data(array('customer_mst.status'=> '"Active"', 'customer_data.source'=>'"internal"', 'assigned_to'=>$user_details['user_id']));
			if(!empty($internal_data)) {

				array_push($data['lead_data'], array('table_name'=> 'customer_mst', 'lead_type'=> 'internal_data', 'lead_name'=> 'internal Data', 'count'=> count($internal_data)));

			}
			// echo "<pre>";print_r($internal_data);echo"</pre><hr>";exit;
		}else{

			//getting user list of sales team
			$user_list = $this->common_model->get_dynamic_data_sales_db('name, user_id, role', 'status=1 AND (role= 16 OR role= 5)', 'users');
			$role_list = array_column($this->common_model->get_dynamic_data_sales_db('*', array(), 'role'), 'role_name', 'role_id');
			foreach ($user_list as $value) {
				if(!empty($role_list[$value['role']])) {

					$data['user_list'][$role_list[$value['role']]][]= $value;
				}
			}
			//getting client details for sales person
			$client_data = $this->common_model->get_primary_hetro_lead_count('deleted IS Null AND assigned_to IS Null AND product_name IS not Null');
			// echo "<pre>";print_r($client_data);echo"</pre><hr>";exit;
			if(!empty($client_data)) {
				foreach ($client_data as $single_client_data) {
					if(!empty($static_lead_name[$single_client_data['product_name']])) {
						array_push($data['lead_data'], array('table_name'=> 'customer_mst', 'lead_type'=> $single_client_data['product_name'], 'lead_name'=> $static_lead_name[$single_client_data['product_name']], 'count'=> $single_client_data['count']));
					}
				}
			}			

			//getting internal data for sales person
			$internal_data = $this->common_model->get_internal_listing_data(array('customer_mst.status'=> '"Active"', 'customer_data.source'=>'"internal"', 'assigned_to'=>'0'));
			if(!empty($internal_data)) {

				array_push($data['lead_data'], array('table_name'=> 'customer_mst', 'lead_type'=> 'internal_data', 'lead_name'=> 'internal Data', 'count'=> count($internal_data)));

			}
		}
		$array_column_lead_type = array_column($data['lead_data'], 'lead_type');
		
		$product_name_1 = array_column($this->common_model->get_dynamic_data_sales_db('id, product_name', array('status'=>'Active'), 'product_category'), 'product_name', 'id');
		$product_name_2 = array('Pre Qualification', 'Purchase Order', 'RFQ', 'Quotations');
		$data['product_name'] = array_merge($product_name_1, $product_name_2);
				
		$lead_data = array();
		foreach ($data['product_name'] as $name) {

			// echo $name,"<hr>";
			// echo "<pre>";var_dump(array_search($name, $array_column_lead_type));echo"</pre><hr>";exit;
			if(array_search($name, $array_column_lead_type) !== FALSE){
				
				$lead_data[] = $data['lead_data'][array_search($name, $array_column_lead_type)];

			}
		}
		$data['country'] = $this->common_model->get_dynamic_data_sales_db('id, name', 'status= "Active"', 'country_mst');
		$data['region'] = $this->common_model->get_dynamic_data_sales_db('id, name', 'status= "Active"', 'region_mst');
		$data['lead_stage'] = $this->common_model->get_dynamic_data_sales_db('lead_stage_id, stage_name', array(), 'lead_stages');
		$data['lead_data'] = $lead_data;
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		return $data;
	}
	private function prepare_procurement_lead_data_for_update($user_details) {

		$data['lead_data'] = array();
		
		// echo "<pre>";print_r($user_details);echo"</pre><hr>";exit;
		//getting user list of sales team
		$user_list = $this->common_model->get_dynamic_data_sales_db('name, user_id, role', 'status=1 AND (role= 8 OR role= 6) AND user_id!='.$user_details['user_id'], 'users');
		$role_list = array_column($this->common_model->get_dynamic_data_sales_db('*', array(), 'role'), 'role_name', 'role_id');
		foreach ($user_list as $value) {
			if(!empty($role_list[$value['role']])) {

				$data['user_list'][$role_list[$value['role']]][]= $value;
			}
		}

		// getting procurement count
		// $procuremet_data = $this->common_model->get_count('status = "Active" AND procurement_person ='.$user_details['name'], 'procurement');
		$procuremet_data = $this->common_model->get_count("status = 'Active' AND procurement_person like '%".$user_details['name']."%'", 'procurement');

		// echo "<pre>";print_r($procuremet_data);echo"</pre><hr>";exit;
		if($procuremet_data['count'] != 0) {

			array_push($data['lead_data'], array('table_name'=> 'procurement', 'lead_type'=> 'Purchase Order', 'lead_name'=> 'Purchase Order', 'count'=> $procuremet_data['count']));
		}

		// getting rfq count
		$rfq_data = $this->common_model->get_count('assigned_to ='.$user_details['user_id'], 'rfq_mst');
		if($rfq_data['count'] != 0) {

			array_push($data['lead_data'], array('table_name'=> 'rfq_mst', 'lead_type'=> 'RFQ', 'lead_name'=> 'RFQ', 'count'=> $rfq_data['count']));
		}

		// array_push($data['lead_data'], array('table_name'=> 'rfq_mst', 'lead_name'=> 'Query'));

		// getting Vendor count
		$vendor_data = $this->common_model->get_count('status = "Active" AND entered_by ='.$user_details['user_id'], 'vendors');
		if($vendor_data['count'] != 0) {

			array_push($data['lead_data'], array('table_name'=> 'vendors', 'lead_type'=> 'Vendors', 'lead_name'=> 'Vendors', 'count'=> $vendor_data['count']));
		}

		//getting people information data
		$data['people_information'] = $this->common_model->get_all_conditional_data_sales_db('*', array('user_id'=>$user_details['user_id']), 'people_information', 'row_array', array(), array(), '');
		return $data;
	}
	private function prepare_listing_data_for_sales($status = '1') {

		$data = array();

		$data['lead_data'][0] = array(
										'name' => 'Blank Leads',
										'user_id' => 'unassigned',
										'chemical companies' => 0, 'distributors' => 0, 'epc companies' => 0,
										'forged fittings' => 0, 'heteregenous tubes india'=>0, 'pvf companies'=>0, 'tubes'=> 0, 'tubing'=>0, 'shipyards'=>0, 'sugar companies'=>0, 'water companies'=> 0, 'hammer union'=> 0, 'pipes'=>0, 'process_control'=> 0, 'miscellaneous leads'=> 0, 'internal_data'=>0, 'pre_qualifiction'=> 0, 'procurement' => 0, 'rfq'=> 0, 'query'=>0,
										'quotations' => 0, 'vendor' => 0, 'Hydraulic fitting'=> 0, 'Fasteners'=>0
										);
		$client_data = $this->common_model->get_primary_hetro_lead_count('deleted IS Null AND assigned_to IS Null AND product_name IS not Null');		
		
        foreach($client_data as $single_client_details){
			
			$data['lead_data'][0][$single_client_details['product_name']] = $single_client_details['count'];
        }
			
        $pre_qualifiction = $this->common_model->get_count('is_pq = "Yes"', 'customer_mst');
    	$data['lead_data'][0]['pre_qualifiction'] = $pre_qualifiction['count'];

    	$procurement = $this->common_model->get_count('status = "Active" AND sales_person IS Null', 'procurement');
    	$data['lead_data'][0]['procurement'] = $procurement['count'];

        $rfq = $this->common_model->get_count('rfq_sentby = 0', 'rfq_mst');
    	$data['lead_data'][0]['rfq'] = $rfq['count'];
		
        $query = $this->common_model->get_count('status = "Active" AND query_assigned_id = 0', 'query_master');
    	$data['lead_data'][0]['query'] = $query['count'];
		
        $quotations = $this->common_model->get_count('assigned_to is null', 'quotation_mst');
    	$data['lead_data'][0]['quotations'] = $quotations['count'];
		
    	$users=$this->common_model->get_dynamic_data_sales_db('*','status = '.$status.' And (role = 5  OR role = 16)','users');

		// echo"<pre>";print_r($users);echo"</pre><hr>";exit;
		foreach($users as $users_key => $single_user_details){

			$data['lead_data'][$users_key+1] = array(
											'name' => $single_user_details['name'],
											'user_id' => $single_user_details['user_id'],
											'chemical companies' => 0, 'distributors' => 0, 'epc companies' => 0, 'forged fittings' => 0, 'heteregenous tubes india'=>0, 'pvf companies'=>0, 'tubes'=> 0, 'tubing'=>0, 'shipyards'=>0, 'sugar companies'=>0, 'water companies'=> 0, 'hammer union'=> 0, 'pipes'=>0, 'process_control'=> 0, 'miscellaneous leads'=> 0, 'internal_data'=>0, 'pre_qualifiction'=> 0, 'procurement' => 0, 'rfq'=> 0, 'query'=>0, 'quotations' => 0, 'vendor' => 0, 'Hydraulic fitting'=> 0, 'Fasteners'=>0
											);
											
			$client_data = $this->common_model->get_primary_hetro_lead_count('deleted IS Null AND product_name IS not Null AND assigned_to ='.$single_user_details['user_id']);

			foreach($client_data as $single_client_details){
				
				$data['lead_data'][$users_key+1][$single_client_details['product_name']] = $single_client_details['count'];
	        }

			// echo"<pre>";print_r($data);echo"</pre><hr>";exit;
	        $pre_qualifiction = $this->common_model->get_count('is_pq = "Yes" AND assigned_to ='.$single_user_details['user_id'], 'customer_mst');
        	$data['lead_data'][$users_key+1]['pre_qualifiction'] = $pre_qualifiction['count'];

        	$procurement = $this->common_model->get_count('status = "Active" AND sales_person ="'.$single_user_details['name'].'"', 'procurement');
        	$data['lead_data'][$users_key+1]['procurement'] = $procurement['count'];

	        $rfq = $this->common_model->get_count('rfq_sentby ='.$single_user_details['user_id'], 'rfq_mst');
        	$data['lead_data'][$users_key+1]['rfq'] = $rfq['count'];

	        $query = $this->common_model->get_count('status = "Active" AND query_assigned_id ='.$single_user_details['user_id'], 'query_master');
        	$data['lead_data'][$users_key+1]['query'] = $query['count'];

	        $quotations = $this->common_model->get_count('assigned_to ='.$single_user_details['user_id'], 'quotation_mst');
        	$data['lead_data'][$users_key+1]['quotations'] = $quotations['count'];

			$lead_management_data = $this->common_model->get_internal_lead_count('customer_mst.status = "Active" AND customer_data.source = "internal" AND customer_mst.assigned_to ='.$single_user_details['user_id']);
        	$data['lead_data'][$users_key+1]['internal_data'] = $lead_management_data['count'];

		}
		// echo"<pre>";print_r($data);echo"</pre><hr>";exit;
      	return $data;
	}
	private function prepare_listing_data_for_procurement($status = '1') {

		$data = array();
		$users=$this->common_model->get_dynamic_data_sales_db('*','status = '.$status.' And (role = 6  OR role = 8)','users');
		foreach($users as $users_key => $single_user_details){

			$data['lead_data'][$users_key] = array(
											'name' => $single_user_details['name'],
											'user_id' => $single_user_details['user_id'],
											'procurement' => 0, 'rfq'=> 0, 'query'=>0,
											'vendor' => 0
											);

        	$procurement = $this->common_model->get_count("status = 'Active' AND procurement_person like '%".$single_user_details['name']."%'", 'procurement');
        	// $procurement = $this->common_model->get_count('status = "Active" AND procurement_person ='.$single_user_details['name'], 'procurement');
        	$data['lead_data'][$users_key]['procurement'] = $procurement['count'];

	        $rfq = $this->common_model->get_count('assigned_to ='.$single_user_details['user_id'], 'rfq_mst');
        	$data['lead_data'][$users_key]['rfq'] = $rfq['count'];

        	$query = $this->common_model->get_count('status = "Active" AND query_assigned_id ='.$single_user_details['user_id'], 'query_master');
        	$data['lead_data'][$users_key]['query'] = $query['count'];

	        $vendor = $this->common_model->get_count('status = "Active" AND entered_by ='.$single_user_details['user_id'], 'vendors');
        	$data['lead_data'][$users_key]['vendor'] = $vendor['count'];
      	}
      	return $data;
	}
	private function get_days_agieng($days) {

		$return_string = "";
		if($days < 8){

			$return_string = $days.' days ago';
		}else if($days < 30){

			$return_string = round($days / 7).' weeks ago';
		}else if($days < 365){

			$return_string = round($days / 30).' months ago';
		}else if($days > 365){

			$return_string = round($days / 365).' years ago';
		}

		return $return_string;
	}
	// private function prepare_daily_report_data_of_sales_person($sales_person_id, $where_array=array()){

		// 	// echo "<pre>";print_r($where_array);echo"</pre><hr>";exit;
		// 	$return_array = $lead_last_contacted = array();
		// 	$return_array['lead_count'] = array('call'=>0, 'call_attempted'=>0, 'email'=>0, 'linkedin'=>0, 'whatsapp'=>0);
		// 	$static_lead_name = array('Chemical Companies'=> 'Piping Chemical Companies', 'distributors'=> 'Tube Fittings Distributors', 
		// 							'EPC companies'=> 'Piping EPC Companies',	'Forged Fittings'=> 'Forged Fittings',
		// 							'Heteregenous Tubes India'=> 'Tube Fittings & Valves India', 'pvf companies'=> 'Instrumentation PVF Companies',
		// 							'Shipyards'=> 'Piping Shipyards Companies', 'sugar companies'=> 'Piping Sugar Companies', 'Water Companies'=> 'Piping Water Companies',	'HAMMER UNION'=> 'Primary Leads - Hammer Union', 'PIPES'=> 'Primary Leads - Piping Products',
		// 							'PROCESS CONTROL'=> 'Primary Leads - Process Control', 'TUBES'=> 'Primary Leads - Tube Fittings & Valves',
		// 							'TUBING'=> 'Primary Leads - Tubing', 'Miscellaneous Leads'=> 'Miscellaneous Leads', 'Internal Data'=> 'Internal Data');

		// 	$lead_source_wise_count =  $this->common_model->get_client_lead_count('deleted IS Null AND source IS not Null AND assigned_to = '.$sales_person_id);
		// 	foreach ($lead_source_wise_count as $single_lead_source_wise_count) {

		// 		$all_lead_data_source_wise =  $this->common_model->get_client_lead_data('client_id as id, client_name as name, assigned_to, country', "deleted is NULL AND assigned_to = ".$sales_person_id." AND source = '".urldecode($single_lead_source_wise_count['source'])."'");
		// 		if(!empty($all_lead_data_source_wise)){
					
		// 			foreach ($all_lead_data_source_wise as $single_all_lead_data_source_wise) {

		// 				$lead_connects_where_string = "lead_id = '".$single_all_lead_data_source_wise['id']."'";
		// 				$explode_filter_date = explode(" - ", $where_array['filter_date']);
		// 				if(count($explode_filter_date) == 1){

		// 					$lead_connects_where_string .= " AND connected_on ='".date('Y-m-d', strtotime($explode_filter_date[0]))."'";
		// 				}else if(count($explode_filter_date) == 2){

		// 					$lead_connects_where_string .= " AND connected_on >='".date('Y-m-d', strtotime($explode_filter_date[0]))."' AND connected_on <='".date('Y-m-d', strtotime($explode_filter_date[1]))."'";
		// 				}
		// 				if(!empty($where_array) && !empty($where_array['connect_mode'])) {

		// 					$lead_connects_where_string .= " AND connect_mode IN ('";
		// 					$lead_connects_where_string .= implode("', '", $where_array['connect_mode']);
		// 					$lead_connects_where_string .= "')";
		// 				}
		// 				$lead_connect_data =  $this->common_model->get_dynamic_data_sales_db_null_false('*', $lead_connects_where_string, 'lead_connects');
		// 				if(!empty($lead_connect_data)) {
		// 					foreach ($lead_connect_data as $single_lead_connect_data) {

		// 						// getting member details
		// 						$member_details = array();
		// 						$member_details = $this->common_model->get_dynamic_data_sales_db('*', array('lead_dtl_id'=>$single_lead_connect_data['member_id']), 'hetro_lead_detail', 'row_array');
		// 						if(empty($member_details)) {

		// 							$member_details['member_name'] = "Member Name Not Available";
		// 						}
		// 						// getting country details
		// 						$country_details = array();
		// 						$country_details = $this->common_model->get_dynamic_data_sales_db('*', array('lookup_id'=>$single_all_lead_data_source_wise['country']), 'lookup', 'row_array');
		// 						if(empty($country_details)) {

		// 							$country_details['lookup_value'] = "Country Name Not Available";
		// 						}
		// 						$return_array['lead_count'][$single_lead_connect_data['connect_mode']] += 1;
		// 						if($single_lead_connect_data['email_sent'] == 'Yes') {

		// 							$return_array['lead_count']['email'] += 1;
		// 						}
		// 						$lead_last_contacted[] = array(
		// 												'sub_lead_name' => $static_lead_name[$single_lead_source_wise_count['source']],
		// 												'id' => $single_all_lead_data_source_wise['id'],
		// 												'name' => $single_all_lead_data_source_wise['name'],
		// 												'assigned_to' => $single_all_lead_data_source_wise['assigned_to'],
		// 												'country' => $country_details['lookup_value'],
		// 												'contact_date' => date('F j, Y, g:i a', strtotime($single_lead_connect_data['entered_on'])),
		// 												'time' => strtotime($single_lead_connect_data['entered_on']),
		// 												'contact_member_name' => $member_details['member_name'],
		// 												'contact_medium' => $single_lead_connect_data['connect_mode'],
		// 												'contact_comment' => $single_lead_connect_data['comments']
		// 											);
		// 					}	
		// 				}
		// 			}	
		// 		}
		// 	}
		// 	// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		// 	$lead_source_wise_count =  $this->common_model->get_lead_mst_lead_count('deleted IS Null AND data_category IS not Null AND assigned_to='.$sales_person_id);
		// 	foreach ($lead_source_wise_count as $single_lead_source_wise_count) {

		// 		$all_lead_data_source_wise =  $this->common_model->get_lead_mst_lead_data('lead_mst_id as id, IMPORTER_NAME as name, assigned_to, COUNTRY_OF_DESTINATION as country', "deleted IS Null AND assigned_to = '".$sales_person_id."' AND data_category ='".urldecode(strtolower($single_lead_source_wise_count['data_category']))."'");
		// 		if(!empty($all_lead_data_source_wise)){
		// 			foreach ($all_lead_data_source_wise as $single_all_lead_data_source_wise) {

		// 				$lead_connects_where_string = "lead_id = '".$single_all_lead_data_source_wise['id']."'";
		// 				$explode_filter_date = explode(" - ", $where_array['filter_date']);
		// 				if(count($explode_filter_date) == 1){

		// 					$lead_connects_where_string .= " AND connected_on ='".date('Y-m-d', strtotime($explode_filter_date[0]))."'";
		// 				}else if(count($explode_filter_date) == 2){

		// 					$lead_connects_where_string .= " AND connected_on >='".date('Y-m-d', strtotime($explode_filter_date[0]))."' AND connected_on <='".date('Y-m-d', strtotime($explode_filter_date[1]))."'";
		// 				}
		// 				if(!empty($where_array) && !empty($where_array['connect_mode'])) {

		// 					$lead_connects_where_string .= " AND connect_mode IN ('";
		// 					$lead_connects_where_string .= implode("', '", $where_array['connect_mode']);
		// 					$lead_connects_where_string .= "')";
		// 				}
		// 				$lead_connect_data =  $this->common_model->get_dynamic_data_marketing_db_null_false('*', $lead_connects_where_string, 'lead_connects');
		// 				if(!empty($lead_connect_data)) {

		// 					foreach ($lead_connect_data as $single_lead_connect_data) {

		// 						$member_details = array();
		// 						$member_details = $this->common_model->get_dynamic_data_marketing_db_null_false('*', array('lead_dtl_id'=>$single_lead_connect_data['member_id']), 'lead_detail', 'row_array');
		// 						if(empty($member_details)) {

		// 							$member_details['member_name'] = "Member Name Not Available";
		// 						}
		// 						$return_array['lead_count'][$single_lead_connect_data['connect_mode']] += 1;
		// 						if($single_lead_connect_data['email_sent'] == 'Yes') {

		// 							$return_array['lead_count']['email'] += 1;
		// 						}
		// 						$lead_last_contacted[] = array(
		// 												'sub_lead_name' => $static_lead_name[$single_lead_source_wise_count['data_category']],
		// 												'id' => $single_all_lead_data_source_wise['id'],
		// 												'name' => $single_all_lead_data_source_wise['name'],
		// 												'assigned_to' => $single_all_lead_data_source_wise['assigned_to'],
		// 												'country' => $single_all_lead_data_source_wise['country'],
		// 												'contact_date' => date('F j, Y, g:i a', strtotime($single_lead_connect_data['entered_on'])),
		// 												'time' => strtotime($single_lead_connect_data['entered_on']),
		// 												'contact_member_name' => $member_details['member_name'],
		// 												'contact_medium' => $single_lead_connect_data['connect_mode'],
		// 												'contact_comment' => $single_lead_connect_data['comments']
		// 											);
		// 					}	
		// 				}
		// 			}	
		// 		}
		// 	}
		// 	$time_wise_sorting = array_column($lead_last_contacted, 'time');
		// 	asort($time_wise_sorting);
		// 	foreach ($time_wise_sorting as $lead_last_contacted_key => $time_value) {
				
		// 		$return_array['lead_last_contacted'][] = $lead_last_contacted[$lead_last_contacted_key];
		// 	}
		// 	// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		// 	$quotation_details =  $this->common_model->get_dynamic_data_sales_db_null_false('quotation_mst_id, quote_no, client_id, grand_total, entered_on, purchase_order', "assigned_to = '".$sales_person_id."'", 'quotation_mst');
		// 	foreach ($quotation_details as $single_quotation_details) {

		// 		// getting follow up details
		// 		$follow_up_where_string = "quotation_mst_id='".$single_quotation_details['quotation_mst_id']."'";
		// 		$explode_filter_date = explode(" - ", $where_array['filter_date']);
		// 		if(count($explode_filter_date) == 1){

		// 			$follow_up_where_string .= " AND followedup_on ='".date('Y-m-d', strtotime($explode_filter_date[0]))."'";
		// 		}else if(count($explode_filter_date) == 2){

		// 			$follow_up_where_string .= " AND followedup_on >='".date('Y-m-d', strtotime($explode_filter_date[0]))."' AND followedup_on <='".date('Y-m-d', strtotime($explode_filter_date[1]))."'";
		// 		}
		// 		$follow_up_details =  $this->common_model->get_dynamic_data_sales_db_null_false('*', $follow_up_where_string, 'follow_up');
		//         //echo "<pre>";print_r($follow_up_details);echo"</pre><hr>";exit;
		// 		if(!empty($follow_up_details)) {

		// 			foreach ($follow_up_details as $single_follow_up_details) {
						
		// 				if(!empty($single_follow_up_details['connect_mode'])){
							
		// 					if($single_follow_up_details['connect_mode'] == 'call_connected') {

		// 						// $return_array['lead_count']['call_connected'] += 1;
		// 						$return_array['lead_count']['call'] += 1;
		// 					}else{

		// 						$return_array['lead_count'][$single_follow_up_details['connect_mode']] += 1;
		// 					}
		// 				}				
		// 			}
		// 		}

		// 	}
			
		// 	return $return_array;
	// }
	// private function prepare_quotation_data_of_sales_person($sales_person_id, $where_array=array()){

		// 	$return_array = $quotation_report = array();
		// 	$return_array['lead_count'] = array('call_connected'=>0, 'call_attempted'=>0, 'email'=>0, 'linkedin'=>0, 'whatsapp'=>0);
		// 	$follow_up_where_string = "";
		// 	$lead_source_wise_count =  $this->common_model->get_client_lead_count('deleted IS Null AND source IS not Null AND assigned_to = '.$sales_person_id);
		// 	foreach ($lead_source_wise_count as $single_lead_source_wise_count) {

		// 		$all_lead_data_source_wise =  $this->common_model->get_client_lead_data('client_id as id, client_name as name, assigned_to, country', "deleted is NULL AND assigned_to = ".$sales_person_id." AND source = '".urldecode($single_lead_source_wise_count['source'])."'");
		// 		if(!empty($all_lead_data_source_wise)){
					
		// 			foreach ($all_lead_data_source_wise as $single_all_lead_data_source_wise) {

		// 				$lead_connects_where_string = "lead_id = '".$single_all_lead_data_source_wise['id']."'";
		// 				$explode_filter_date = explode(" - ", $where_array['filter_date']);
		// 				if(count($explode_filter_date) == 1){

		// 					$lead_connects_where_string .= " AND connected_on ='".date('Y-m-d', strtotime($explode_filter_date[0]))."'";
		// 				}else if(count($explode_filter_date) == 2){

		// 					$lead_connects_where_string .= " AND connected_on >='".date('Y-m-d', strtotime($explode_filter_date[0]))."' AND connected_on <='".date('Y-m-d', strtotime($explode_filter_date[1]))."'";
		// 				}
		// 				if(!empty($where_array) && !empty($where_array['connect_mode'])) {

		// 					$lead_connects_where_string .= " AND connect_mode IN ('";
		// 					$lead_connects_where_string .= implode("', '", $where_array['connect_mode']);
		// 					$lead_connects_where_string .= "')";
		// 				}
		// 				$lead_connect_data =  $this->common_model->get_dynamic_data_sales_db_null_false('*', $lead_connects_where_string, 'lead_connects');
		// 				if(!empty($lead_connect_data)) {
		// 					foreach ($lead_connect_data as $single_lead_connect_data) {

		// 						// getting member details
		// 						$member_details = array();
		// 						$member_details = $this->common_model->get_dynamic_data_sales_db('*', array('lead_dtl_id'=>$single_lead_connect_data['member_id']), 'hetro_lead_detail', 'row_array');
		// 						if(empty($member_details)) {

		// 							$member_details['member_name'] = "Member Name Not Available";
		// 						}
		// 						// getting country details
		// 						$country_details = array();
		// 						$country_details = $this->common_model->get_dynamic_data_sales_db('*', array('lookup_id'=>$single_all_lead_data_source_wise['country']), 'lookup', 'row_array');
		// 						if(empty($country_details)) {

		// 							$country_details['lookup_value'] = "Country Name Not Available";
		// 						}
		// 						$return_array['lead_count'][$single_lead_connect_data['connect_mode']] += 1;
		// 						if($single_lead_connect_data['email_sent'] == 'Yes') {

		// 							$return_array['lead_count']['email'] += 1;
		// 						}
		// 					}	
		// 				}
		// 			}	
		// 		}
		// 	}
		// 	$lead_source_wise_count =  $this->common_model->get_lead_mst_lead_count('deleted IS Null AND data_category IS not Null AND assigned_to='.$sales_person_id);
		// 	foreach ($lead_source_wise_count as $single_lead_source_wise_count) {

		// 		$all_lead_data_source_wise =  $this->common_model->get_lead_mst_lead_data('lead_mst_id as id, IMPORTER_NAME as name, assigned_to, COUNTRY_OF_DESTINATION as country', "deleted IS Null AND assigned_to = '".$sales_person_id."' AND data_category ='".urldecode(strtolower($single_lead_source_wise_count['data_category']))."'");
		// 		if(!empty($all_lead_data_source_wise)){
		// 			foreach ($all_lead_data_source_wise as $single_all_lead_data_source_wise) {

		// 				$lead_connects_where_string = "lead_id = '".$single_all_lead_data_source_wise['id']."'";
		// 				$explode_filter_date = explode(" - ", $where_array['filter_date']);
		// 				if(count($explode_filter_date) == 1){

		// 					$lead_connects_where_string .= " AND connected_on ='".date('Y-m-d', strtotime($explode_filter_date[0]))."'";
		// 				}else if(count($explode_filter_date) == 2){

		// 					$lead_connects_where_string .= " AND connected_on >='".date('Y-m-d', strtotime($explode_filter_date[0]))."' AND connected_on <='".date('Y-m-d', strtotime($explode_filter_date[1]))."'";
		// 				}
		// 				if(!empty($where_array) && !empty($where_array['connect_mode'])) {

		// 					$lead_connects_where_string .= " AND connect_mode IN ('";
		// 					$lead_connects_where_string .= implode("', '", $where_array['connect_mode']);
		// 					$lead_connects_where_string .= "')";
		// 				}
		// 				$lead_connect_data =  $this->common_model->get_dynamic_data_marketing_db_null_false('*', $lead_connects_where_string, 'lead_connects');
		// 				if(!empty($lead_connect_data)) {

		// 					foreach ($lead_connect_data as $single_lead_connect_data) {

		// 						$member_details = array();
		// 						$member_details = $this->common_model->get_dynamic_data_marketing_db_null_false('*', array('lead_dtl_id'=>$single_lead_connect_data['member_id']), 'lead_detail', 'row_array');
		// 						if(empty($member_details)) {

		// 							$member_details['member_name'] = "Member Name Not Available";
		// 						}
		// 						$return_array['lead_count'][$single_lead_connect_data['connect_mode']] += 1;
		// 						if($single_lead_connect_data['email_sent'] == 'Yes') {

		// 							$return_array['lead_count']['email'] += 1;
		// 						}
		// 					}	
		// 				}
		// 			}	
		// 		}
		// 	}

		// 	$quotation_details =  $this->common_model->get_dynamic_data_sales_db_null_false('quotation_mst_id, quote_no, client_id, grand_total, entered_on, purchase_order', "assigned_to = '".$sales_person_id."'", 'quotation_mst');
		// 	// echo "<pre>";print_r($quotation_details);echo"</pre><hr>";exit;
		// 	foreach ($quotation_details as $single_quotation_details) {

		// 		// echo "<pre>";print_r($single_quotation_details);echo"</pre><hr>";exit;
				
		// 		// getting client details
		// 		$client_details =  $this->common_model->get_dynamic_data_sales_db_null_false('client_name, country', "client_id='".$single_quotation_details['client_id']."'", 'clients', 'row_array');

		// 		// getting country details
		// 		$country_details =  $this->common_model->get_dynamic_data_sales_db_null_false('lookup_value', "lookup_id='".$client_details['country']."'", 'lookup', 'row_array');

		// 		// getting follow up details
		// 		$follow_up_where_string = "quotation_mst_id='".$single_quotation_details['quotation_mst_id']."'";
		// 		$explode_filter_date = explode(" - ", $where_array['filter_date']);
		// 		if(count($explode_filter_date) == 1){

		// 			$follow_up_where_string .= " AND followedup_on ='".date('Y-m-d', strtotime($explode_filter_date[0]))."'";
		// 		}else if(count($explode_filter_date) == 2){

		// 			$follow_up_where_string .= " AND followedup_on >='".date('Y-m-d', strtotime($explode_filter_date[0]))."' AND followedup_on <='".date('Y-m-d', strtotime($explode_filter_date[1]))."'";
		// 		}
		// 		$follow_up_details =  $this->common_model->get_dynamic_data_sales_db_null_false('*', $follow_up_where_string, 'follow_up');
		//         //echo "<pre>";print_r($follow_up_details);echo"</pre><hr>";exit;
		// 		if(!empty($follow_up_details)) {

		// 			foreach ($follow_up_details as $single_follow_up_details) {
						
		// 				$member_details['name'] = "";
		// 				$member_details =  $this->common_model->get_dynamic_data_sales_db_null_false('*', "member_id='".$single_follow_up_details['member_id']."'", 'members', 'row_array');
						
		// 				$quotation_report[] = array(
		// 											'quotation_no'=> $single_quotation_details['quote_no'],
		// 											'company_name'=> $client_details['client_name'],
		// 											'country'=> $country_details['lookup_value'],
		// 											'value'=> $single_quotation_details['grand_total'],
		// 											'date'=> date('F j, Y, g:i a', strtotime($single_follow_up_details['entered_on'])),
		// 											'purchase_order'=> $single_quotation_details['purchase_order'],
		// 											'time' => strtotime($single_quotation_details['entered_on']),
		// 											'follow_up_text'=> $single_follow_up_details['follow_up_text'],
		// 											'member_name'=> $member_details['name'],
		// 											'connect_mode'=> $single_follow_up_details['connect_mode']
		// 										);
		// 				if(!empty($single_follow_up_details['connect_mode'])){
							
		// 					if($single_follow_up_details['connect_mode'] == 'call_connected') {

		// 						// $return_array['lead_count']['call_connected'] += 1;
		// 						$return_array['lead_count']['call'] += 1;
		// 					}else{

		// 						$return_array['lead_count'][$single_follow_up_details['connect_mode']] += 1;
		// 					}
		// 				}
		// 			}
		// 		}
		// 	}

		// 	$time_wise_sorting = array_column($quotation_report, 'time');
		// 	asort($time_wise_sorting);
		// 	foreach ($time_wise_sorting as $quotation_report_key => $time_value) {
				
		// 		$return_array['quotation_report'][] = $quotation_report[$quotation_report_key];
		// 	}
		// 	// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		// 	return $return_array;
	// }
	private function create_daily_report_count_for_sales_person($where_array){

		$return_array = array('call'=>0, 'call_attempted'=>0, 'email'=>0, 'linkedin'=>0, 'whatsapp'=>0);
		
		$daily_report_details_of_lead = $this->common_model->get_dynamic_data_sales_db(
													'user_id,
													count(IF(contact_mode = "call", 1, NULL)) call_connected,
													count(IF(contact_mode = "call_attempted", 1, NULL)) call_attempted,
													count(IF(contact_mode = "linkedin", 1, NULL)) linkedin,
													count(IF(contact_mode = "whatsapp", 1, NULL)) whatsapp,
													count(IF(contact_mode = "email", 1, NULL)) lead_email,
													count(IF(email_sent = "Yes", 1, NULL)) email'
													,$where_array,
													'daily_work_sales_on_lead_data',
													'row_array'
												);
		// echo "<pre>";print_r($daily_report_details_of_lead);echo"</pre><hr>";die;
		if(!empty($daily_report_details_of_lead)) {

			$return_array['call'] += (int)$daily_report_details_of_lead['call_connected'];
			$return_array['call_attempted'] += (int)$daily_report_details_of_lead['call_attempted'];
			$return_array['linkedin'] += (int)$daily_report_details_of_lead['linkedin'];
			$return_array['whatsapp'] += (int)$daily_report_details_of_lead['whatsapp'];
			$return_array['email'] += (int)$daily_report_details_of_lead['email'] + (int)$daily_report_details_of_lead['lead_email'];
		}

		$daily_report_details_of_quotations = $this->common_model->get_dynamic_data_sales_db(
													'user_id,
													count(IF(contact_mode = "call_connected", 1, NULL)) call_connected,
													count(IF(contact_mode = "call_attempted", 1, NULL)) call_attempted,
													count(IF(contact_mode = "linkedin", 1, NULL)) linkedin,
													count(IF(contact_mode = "whatsapp", 1, NULL)) whatsapp,
													count(IF(contact_mode = "email", 1, NULL)) quotation_email,
													count(IF(email_sent = "Yes", 1, NULL)) email'
													,$where_array,
													'daily_work_sales_on_quotation_data',
													'row_array'
												);
		// echo "<pre>";print_r($daily_report_details_of_quotations);echo"</pre><hr>";die;
		if(!empty($daily_report_details_of_quotations)) {

			$return_array['call'] += (int)$daily_report_details_of_quotations['call_connected'];
			$return_array['call_attempted'] += (int)$daily_report_details_of_quotations['call_attempted'];
			$return_array['linkedin'] += (int)$daily_report_details_of_quotations['linkedin'];
			$return_array['whatsapp'] += (int)$daily_report_details_of_quotations['whatsapp'];
			$return_array['email'] += (int)$daily_report_details_of_quotations['email'] + (int)$daily_report_details_of_quotations['quotation_email'];
		}

		return $return_array;
	}
	private function create_daily_report_for_sales_person($call_type, $where_array){

		$return_array['lead_count'] = $this->create_daily_report_count_for_sales_person($where_array);
		// echo "<pre>";print_r($where_array);echo"</pre><hr>";exit;
	
		if($call_type == 'lead'){

			$static_lead_name = array('Chemical Companies'=> 'Piping Chemical Companies', 'distributors'=> 'Tube Fittings Distributors', 
				'EPC companies'=> 'Piping EPC Companies',	'Forged Fittings'=> 'Forged Fittings',
				'Heteregenous Tubes India'=> 'Tube Fittings & Valves India', 'pvf companies'=> 'Instrumentation PVF Companies',
				'Shipyards'=> 'Piping Shipyards Companies', 'sugar companies'=> 'Piping Sugar Companies', 'Water Companies'=> 'Piping Water Companies',	'HAMMER UNION'=> 'Primary Leads - Hammer Union', 'PIPES'=> 'Primary Leads - Piping Products',
				'PROCESS CONTROL'=> 'Primary Leads - Process Control', 'PROCESS_CONTROL'=> 'Primary Leads - Process Control', 'TUBES'=> 'Primary Leads - Tube Fittings & Valves',
				'TUBING'=> 'Primary Leads - Tubing', 'Miscellaneous Leads'=> 'Miscellaneous Leads', 'internal data'=> 'Internal Data', 'internal'=> 'Internal Data', 'Hydraulic fitting'=> 'Hydraulic fitting','Fasteners'=> 'Fasteners');


			$daily_report_data = $this->common_model->get_dynamic_data_sales_db('*', $where_array, 'daily_work_sales_on_lead_data');

			// echo "<pre>";print_r($daily_report_data);echo"</pre><hr>";exit;
			foreach ($daily_report_data as $single_daily_report_data) {
				
				$return_array['lead_last_contacted'][] = array(
								'sub_lead_name' => $static_lead_name[$single_daily_report_data['lead_name']],
								'id' => $single_daily_report_data['client_id'],
								'name' => $single_daily_report_data['client_name'],
								'assigned_to' => $single_daily_report_data['user_id'],
								'country' => $single_daily_report_data['country'],
								'contact_date' => date('F j, Y, g:i', strtotime($single_daily_report_data['contact_date'])),
								'contact_member_name' => $single_daily_report_data['member_name'],
								'contact_medium' => $single_daily_report_data['contact_mode'],
								'contact_comment' => $single_daily_report_data['comments']
							);
			}
		}else if($call_type == 'quotation'){

			$daily_report_data = $this->common_model->get_all_conditional_data_sales_db('*', $where_array, 'daily_work_sales_on_quotation_data', 'result_array', array(), array('column_name' => 'contact_date', 'column_value'=>'asc'));
			foreach ($daily_report_data as $single_daily_report_data) {

				$return_array['quotation_report'][] = array(
													'quotation_no'=> $single_daily_report_data['quotation_no'],
													'company_name'=> $single_daily_report_data['client_name'],
													'country'=> $single_daily_report_data['country'],
													'value'=> $single_daily_report_data['value'],
													'date'=> date('F j, Y, g:i', strtotime($single_daily_report_data['contact_date'])),
													'member_name'=> $single_daily_report_data['member_name'],
													'connect_mode'=> $single_daily_report_data['contact_mode'],
													'follow_up_text'=> $single_daily_report_data['comments'],
												);
			}
		}

		// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		return $return_array;
	}
	private function get_indian_time($date_format = 'Y-m-d H:i:s'){

		date_default_timezone_set('Asia/Kolkata');
		return date($date_format);
	}

	private function get_mrf_no(){

		$last_query_id = $this->common_model->get_dynamic_data_sales_db('last_quote_id', array('status'=> "Active", 'module_name'=> 'hr', 'sub_module_name'=> 'mrf'), 'last_quote_created_number', 'row_array');
		$no = (((int)$last_query_id['last_quote_id'])+1);
		$year = date('y').'-'.(date('y') + 1);
		if(date('m') <= 3){

			$year = (date('y')-1)."-".date('y');
		}
		$this->common_model->update_data_sales_db('last_quote_created_number', array('last_quote_id'=>$no), array('status'=> "Active", 'module_name'=> 'hr', 'sub_module_name'=> 'mrf'));
		return 'MRF/'.$no.'/'.$year;
	}

	private function upload_doc_config($resume_folder) {

		$config['upload_path']          = './assets/hr_document/candidate_resume/';
        $config['allowed_types']        = 'pdf|docx|jpeg|jpg|png';
        $config['file_name']            = 'resume_'.$this->session->userdata('user_id').'-'.time();
        $config['overwrite']            = TRUE;

        $this->load->library('upload', $config);

		$data = array('status' => 'success', 'msg' => '', 'file_name' => '');
        if ( ! $this->upload->do_upload('file'))
        {
			$error = array('error' => $this->upload->display_errors());
			$data['status'] = 'failed';
			$data['msg'] = $error['error'];
        }
        else
        {
			$file_dtls = $this->upload->data();
			$data['msg'] = 'success';
			$data['file_name'] = $file_dtls['file_name'];
        }
        // echo "<pre>";print_r($data);echo"</pre><hr>";exit;
        return $data;
	}

	private function upload_photo_config($photo_folder) {
		$config['upload_path']          = './assets/hr_document/candidate_photo/';
        $config['allowed_types']        = 'jpeg|jpg|png';
        $config['file_name']            = 'photo_'.time();
        $config['overwrite']            = TRUE;
        $this->load->library('upload', $config);
		$data = array('status' => 'success', 'msg' => '', 'file_name' => '');
        if ( ! $this->upload->do_upload('file'))
        {
			$error = array('error' => $this->upload->display_errors());
			$data['status'] = 'failed';
			$data['msg'] = $error['error'];
        }
        else
        {
			$file_dtls = $this->upload->data();
			$data['msg'] = 'success';
			$data['file_name'] = $file_dtls['file_name'];
        }
        // echo "<pre>";print_r($data);echo"</pre><hr>";exit;
        return $data;
	}

	private function prepare_candidate_list_data($where_array, $order_by_array, $limit, $offset){

		$data= $this->common_model->get_candidate_list($where_array, $order_by_array, $limit, $offset);

		$source_dropdown = array_column($this->common_model->get_dynamic_data_sales_db('id, name', array('status'=>'Active'),'candidate_source'), 'name', 'id');
		$source_dropdown[''] = '';
		$source_dropdown[0] = '';

		$stage_dropdown = array_column($this->common_model->get_dynamic_data_sales_db('id, name', array('status'=>'Active'),'candidate_stage'), 'name', 'id');
		$stage_dropdown['']='';
		$stage_dropdown[0]='';

		$user_details = array_column($this->common_model->get_dynamic_data_sales_db('*', array(), 'users'), 'name', 'user_id');
		$user_details['']='';
		$user_details[0]='';

		$stage_reason_details = array_column($this->common_model->get_dynamic_data_sales_db('*', array(), 'stage_reason'), 'name', 'id');
		$stage_reason_details['']='';
		$stage_reason_details[0]='';

		$candidate_evaluation = array_column($this->common_model->get_dynamic_data_sales_db('*', array(), 'candidate_evaluation'), 'other_comments', 'candidate_id');
		$candidate_evaluation['']='';
		$candidate_evaluation[0]='';

		$interviewer_name = array_column($this->common_model->get_dynamic_data_sales_db('*', array(), 'manpower_requisition_form'), 'reporting_to', 'id');
		$interviewer_name['']='';
		$interviewer_name[0]='';

		// echo "<pre>";print_r($interview);echo"</pre><hr>";die;
		foreach($data['candidate_list'] as $key => $single_candidate_details){

			$explode_name = explode(" ", $user_details[$single_candidate_details['hr_user_id']]);
			$data['candidate_list'][$key]['hr_name'] = $explode_name[0];
			$data['candidate_list'][$key]['source_name'] = $source_dropdown[$single_candidate_details['source_id']];
			$data['candidate_list'][$key]['stage_name'] = $stage_dropdown[$single_candidate_details['stage_id']];

			if($single_candidate_details['stage_id'] == 7){

				if($single_candidate_details['stage_reason_id'] == 4){

					$data['candidate_list'][$key]['stage_reason'] = $single_candidate_details['other_reason'];
				}else{

					$data['candidate_list'][$key]['stage_reason'] = $stage_reason_details[$single_candidate_details['stage_reason_id']];
				}
			}

			if(!empty($single_candidate_details['comment_info'])){

				$comment_info_array = json_decode($single_candidate_details['comment_info'], true);

				$last_comment_data = $comment_info_array[count($comment_info_array)-1];
				$data['candidate_list'][$key]['last_comment'] = $last_comment_data['message'];
				$data['candidate_list'][$key]['user_name'] = $this->creat_first_name($user_details[$last_comment_data['user_id']]);
				$data['candidate_list'][$key]['tat'] = $this->creat_tat($last_comment_data['date_time']);
			}

			if(!empty($single_candidate_details['mrf_id'])) {

				$evaluation_comment = $single_candidate_details['mrf_id'];
				$data['candidate_list'][$key]['evaluation_last_comment'] = $candidate_evaluation[$single_candidate_details['id']];

				if (!empty($data['candidate_list'][$key]['evaluation_last_comment'])) {

					$data['candidate_list'][$key]['interviewer_name'] = $this->creat_first_name($user_details[$interviewer_name[$evaluation_comment]]);
				}

			}
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";die;

		$data['paggination_data']['limit'] = $limit;
		$data['paggination_data']['offset'] = $offset;
		return $data;
	}
	private function create_where_response_on_search($search_form_data, $mrf_id){

		// echo "<pre>";print_r($search_form_data);echo"</pre><br>";die;
		
		$return_where['where'] = "status = 'Active' AND mrf_id =".$mrf_id;
		$return_where['search_filter'] = array('candidate_id'=>array() ,'source_id'=>array(), 'stage_id'=>array(), 'hr_user_id'=>array());
		$candidate_id = $source_id = $stage_id = $hr_user_id = array();

		if(!empty($search_form_data)){

			foreach ($search_form_data as $form_data) {
				if(!empty($form_data['value'])){

					switch ($form_data['name']) {

						case 'candidate_id':

							$candidate_id[] = $form_data['value'];
							$return_where['search_filter']['candidate_id'][] = $form_data['value'];
						break;

						case 'source_id':

							$source_id[] = $form_data['value'];
							$return_where['search_filter']['source_id'][] = $form_data['value'];
						break;

						case 'stage_id':

							$stage_id[] = $form_data['value'];
							$return_where['search_filter']['stage_id'][] = $form_data['value'];
						break;

						case 'hr_user_id':

							$hr_user_id[] = $form_data['value'];
							$return_where['search_filter']['hr_user_id'][] = $form_data['value'];
						break;

						default:
						break;
					}
				}
			}

			if (!empty($candidate_id)) {
				$return_where['where'] .= " AND id IN (" . implode(', ', $candidate_id) . ")";
			}
			if (!empty($source_id)) {
				$return_where['where'] .= " AND source_id IN (" . implode(', ', $source_id) . ")";
			}
			if (!empty($stage_id)) {
				$return_where['where'] .= " AND stage_id IN (" . implode(', ', $stage_id) . ")";
			}
			if (!empty($hr_user_id)) {
				$return_where['where'] .= " AND hr_user_id IN (" . implode(', ', $hr_user_id) . ")";
			}
			// echo "<pre>";print_r($return_where);echo"</pre><br>";exit;
		}
		return $return_where;
	}
	private function creat_tat($date_time){

		$return_tat = '';
		// Get the timestamp of the chat message
		$chat_time = strtotime(str_replace("Z", "", $date_time)); // Replace with your chat message timestamp

		// Create DateTime objects for the chat message and current time
		$chat_date = new DateTime("@$chat_time");
		$current_date = new DateTime();
		// Get the difference between the two dates
		$date_diff = $current_date->diff($chat_date);

		// Output the result
		if ($date_diff->y > 0) {

			$return_tat = $date_diff->y . " year(s)";
		}else if ($date_diff->m > 0) {

			$return_tat = $date_diff->m . " month(s)";
		}else if ($date_diff->d > 0) {

			$return_tat = $date_diff->d . " day(s)";
		}else if ($date_diff->h > 0) {

			$return_tat = $date_diff->h . " hour(s)";
		}else if ($date_diff->i > 0) {

			$return_tat = $date_diff->i . " minute(s)";
		}else {

			$return_tat = $date_diff->s . " second(s)";
		}

		return $return_tat." ago";
	}

	private function creat_first_name($string){

		$return_string = "";

		if(!empty($string)){

			$string_explode = explode(" ", $string);
			$return_string = $string_explode[0];
		}
		return $return_string;
	}
}