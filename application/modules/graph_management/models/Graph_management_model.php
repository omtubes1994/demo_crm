<?php 
Class Graph_management_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$CI = &get_instance();
	}

    public function get_quotation_vs_proforma_highchart_list($company_id) {

		$where_string = "stage IN ('publish','proforma') AND YEAR(entered_on) = ".date('Y')." AND client_id = ".$company_id;
		$res = $this->db->query("
							SELECT 
							DATE_FORMAT(entered_on, '%b') as month,
							count('*') count, 
							count(IF(status = 'won', 1, NULL)) won,
							count(IF(status = 'open', 1, NULL)) open,
							count(IF(status = 'closed', 1, NULL)) closed
							FROM quotation_mst 
							WHERE {$where_string} 
							group by month
							order by entered_on ASC 
						")->result_array();
		// echo $this->db->last_query(),"<hr>";die;
		return $res;
	}

	public function get_quotation_vs_proforma_highchart_list_yearly($company_id) {

		$where_string = "stage IN ('publish','proforma') AND client_id = ".$company_id;
		$res = $this->db->query("
							SELECT
							YEAR(entered_on) year,
							count('*') count,
							count(IF(status = 'won', 1, NULL)) won,
							count(IF(status = 'open', 1, NULL)) open,
							count(IF(status = 'closed', 1, NULL)) closed
							FROM quotation_mst
							WHERE {$where_string}
							group by year
							order by entered_on ASC
						")->result_array();
		// echo $this->db->last_query(),"<hr>";die;
		return $res;
	}
}
