<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Graph_management extends MX_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('user_id')){
			redirect('login', 'refresh');
			exit;
		}
		else{
			
		}
		error_reporting(0);
		$this->load->model('graph_management_model');
		$this->load->model('common/common_model');
		$this->load->model('home/home_model');
		$this->load->model('production/production_model');
	}

	public function ajax_function() {

		if($this->input->is_ajax_request()){
			$post_details = $this->input->post();
			$call_type = $this->input->post('call_type');
			$response['status'] = 'successful';	
			switch ($call_type) {

				//rfq graph
				case 'get_graph_details_back':
					
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					$response['quotation_vs_proforma_highchart_data'][0] = array();
					$response['quotation_vs_proforma_highchart_data'][0]['name'] = 'WON';
					$response['quotation_vs_proforma_highchart_data'][0]['color'] = '#28a745';
					$response['quotation_vs_proforma_highchart_data'][1] = array();
					$response['quotation_vs_proforma_highchart_data'][1]['name'] = 'OPEN';
					$response['quotation_vs_proforma_highchart_data'][1]['color'] = '#007bff';
					$response['quotation_vs_proforma_highchart_data'][2] = array();
					$response['quotation_vs_proforma_highchart_data'][2]['name'] = 'CLOSED';
					$response['quotation_vs_proforma_highchart_data'][2]['color'] = '#dc3545';
					$response['quotation_vs_proforma_highchart_data_yearly'][0] = array();
					$response['quotation_vs_proforma_highchart_data_yearly'][0]['name'] = 'WON';
					$response['quotation_vs_proforma_highchart_data_yearly'][0]['color'] = '#28a745';
					$response['quotation_vs_proforma_highchart_data_yearly'][1] = array();
					$response['quotation_vs_proforma_highchart_data_yearly'][1]['name'] = 'OPEN';
					$response['quotation_vs_proforma_highchart_data_yearly'][1]['color'] = '#007bff';
					$response['quotation_vs_proforma_highchart_data_yearly'][2] = array();
					$response['quotation_vs_proforma_highchart_data_yearly'][2]['name'] = 'CLOSED';
					$response['quotation_vs_proforma_highchart_data_yearly'][2]['color'] = '#dc3545';
					$response['export_stats_yearly'] = array();
					$response['export_stats_yearly_total'] = 0;
					$response['export_stats_yearly_internal'] = array();
					$response['export_stats_yearly_internal_total'] = 0;
					$response['exporters_data'] = array();
					$response['production_status_wise'] = array();
					$response['production_status_wise_total'] = 0;
					$company_name = $this->input->post('company_name');
					$company_id = $this->input->post('company_id');
					$response['company_name'] = $company_name;
					if(!empty($company_name)){

						$response['export_stats_yearly'] = $this->export_stats_yearly($company_name);
						//export_stats_yearly_internal
						if(in_array($this->session->userdata('role'), array(1, 5, 16))){

							$export_stats_yearly_data = $this->common_model->get_all_conditional_data_sales_db("SUM(FOB_VALUE_INR) as value, EXPORTER_NAME, NEW_IMPORTER_NAME, INVOICE_YEAR","NEW_IMPORTER_NAME LIKE '%".strtoupper($company_name)."%'", 'trending_invoice', 'result_array', array(), array('column_name'=> 'INVOICE_YEAR', 'column_value'=> 'ASC'), array('INVOICE_YEAR'));

						}
						if(!empty($export_stats_yearly_data)){

							foreach($export_stats_yearly_data as $single_details){

								$response['export_stats_yearly_internal'][] = array(
									'name' => $single_details['INVOICE_YEAR'],
									'y' => (int)$single_details['value'],
									'drilldown' => $single_details['INVOICE_YEAR']
								);
								$response['export_stats_yearly_internal_total'] += $single_details['value'];
							}
							$response['export_stats_yearly_internal_total'] = $this->convert_number_into_indian_currency($response['export_stats_yearly_internal_total']);
						}

						////////////////Production Status Wise Graph///////////
						$return_series = array(
										array('name'=>'Pending Order', 'data'=> array(0)),
										array('name'=>'Semi Ready', 'data'=> array(0)),
										array('name'=>'MTT', 'data'=> array(0)),
										array('name'=>'Query', 'data'=> array(0)),
										array('name'=>'MTT/RFD', 'data'=> array(0)),
										array('name'=>'Ready To Dispatch', 'data'=> array(0))
									);
						$return_data = 0;
						foreach (array('pending_order', 'semi_ready', 'mtt', 'query', 'mtt_rfd', 'ready_to_dispatch_order') as $key => $status_name) {

							$order_total = $this->prepare_listing_data_new(implode(' AND ', $this->default_where_for_all_tab($status_name, $company_id)),array(),0,0);
							$total_in_rupees = $this->get_total_in_indian_currency($order_total['total_revenue_list']);
							$return_series[$key]['data'] = round($total_in_rupees['total']);
							$return_data += $total_in_rupees['total'];
						}
						$return_data = $this->convert_number_into_indian_currency($return_data);
						$response['production_status_wise_total'] = $return_data;

						foreach($return_series as $single_return_series){

							$response['production_status_wise'][] = array(
																		'name' => $single_return_series['name'],
																		'y' => $single_return_series['data'],
																		'drilldown' => $single_return_series['name']);
						}

						//////HS BUY DATA
						// $hs_buy_data = $this->common_model->get_all_conditional_data_marketing_db('*',"NEW_IMPORTER_NAME LIKE '%".$company_name."%'",'hs_buy_percent', 'result_array', array(), array('column_name'=>'FOB_VALUE_INR', 'column_value'=>'desc'));
						// if(!empty($hs_buy_data)){

						// 	$colors = array('#660033', '#CC6633', '#CCFFFF', '#996600', '#DCEDC8', '#CFD8DC', '#E91E63');
						// 	foreach ($hs_buy_data as $hs_key => $hs_value) {
								
						// 		if(round($hs_value['FOB_VALUE_INR']) > 0){
						// 			$bg_color = '';
						// 			if(strtolower(trim($hs_value['hs_desc'])) == 'smls pipe/tube in ss/cs/as'){
						// 				$bg_color = '#00FF00';
						// 			}else if(strtolower(trim($hs_value['hs_desc'])) == 'welded pipe/tube in ss/cs/as'){
						// 				$bg_color = '#FF7F00';
						// 			}else if(strtolower(trim($hs_value['hs_desc'])) == 'fittings flange ss/cs/as'){
						// 				$bg_color = '#FF0000';
						// 			}else if(strtolower(trim($hs_value['hs_desc'])) == 'fasteners'){
						// 				$bg_color = '#0000FF';
						// 			}else if(strtolower(trim($hs_value['hs_desc'])) == 'nickel alloy pipe/tube/fittings'){
						// 				$bg_color = '#4B0082';
						// 			}else{
						// 				$col_key = $hs_key;
						// 				if($hs_key > 6){
						// 					$col_key = $hs_key - $col_key;
						// 				}
						// 				$bg_color = $colors[$col_key];
						// 			}

						// 			$response['hs_buy_graph_data'][$hs_key]['name'] = $hs_value['hs_desc'];
						// 			$response['hs_buy_graph_data'][$hs_key]['y'] = round($hs_value['FOB_VALUE_INR']);
						// 		}
						// 	}
						// }
					
						//quotation_vs_proforma_highchart
						$quotation_list_details = $this->graph_management_model->get_quotation_vs_proforma_highchart_list($company_id);

						if(!empty($quotation_list_details)){

							foreach ($quotation_list_details as $key => $quotation_details) {
		
								$response['quotation_vs_proforma_highchart_category'][] = $quotation_details['month'];
								$response['quotation_vs_proforma_highchart_data'][0]['data'][] = (int)$quotation_details['won'];
								$response['quotation_vs_proforma_highchart_data'][1]['data'][] = (int)$quotation_details['open'];
								$response['quotation_vs_proforma_highchart_data'][2]['data'][] = (int)$quotation_details['closed'];						
							}
						}

						//quotation_vs_proforma_highchart_yearly
						$quotation_list_details_yearly = $this->graph_management_model->get_quotation_vs_proforma_highchart_list_yearly($company_id);

						if(!empty($quotation_list_details_yearly)){

							foreach ($quotation_list_details_yearly as $key => $quotation_details) {

								$response['quotation_vs_proforma_highchart_category_yearly'][] = $quotation_details['year'];
								$response['quotation_vs_proforma_highchart_data_yearly'][0]['data'][] = (int)$quotation_details['won'];
								$response['quotation_vs_proforma_highchart_data_yearly'][1]['data'][] = (int)$quotation_details['open'];
								$response['quotation_vs_proforma_highchart_data_yearly'][2]['data'][] = (int)$quotation_details['closed'];
							}
						}


						######################  EXPORTER DATA  ####################################
								
							$exporter_data_select = "exporter_name as name, exporter_fob as value";
							$exporter_data_where = "importer_name LIKE '%".strtoupper($company_name)."%'";
							$exporter_data_where .= "AND type = '".strtolower(str_replace(" ", "_", "PIPES"))."'";
							$exporter_data = $this->common_model->get_dynamic_data_marketing_db($exporter_data_select, $exporter_data_where, 'competitor_ranks_details');
							// echo $this->common_model->db2->last_query(),"</hr>";
							// echo "<pre>";print_r($exporter_data);echo"</pre><hr>";exit;

							if(!empty($exporter_data)){
							
								foreach ($exporter_data as $ex_key => $ex_value) {
									
									$response['exporters_data'][$ex_key]['name'] = $ex_value['name'];
									$response['exporters_data'][$ex_key]['y'] = (int)$ex_value['value'];
								}
							}
						##########################  END  ##########################################
					}
				break;

				case 'get_lead_graph_details':
				case 'get_rfq_graph_details':
				case 'get_quotation_graph_details':

					###############################################################################
					$response['export_stats_yearly'] = array();
					###############################################################################
					$response['export_stats_yearly_internal'] = array();
					##############################################################################
					$response['import_stats_data'] = array();
					###############################################################################
					$response['production_status_wise'] = array();
					###############################################################################
					$response['decision_maker_data'] = array();
					###############################################################################
					$response['exporters_data'] = array();
					###############################################################################
					$response['additional_information'] = "";
					###############################################################################
					$response['company_name'] = "";
					###############################################################################

					$company_name = $this->input->post('company_name');
					$company_id = $this->input->post('company_id');
					$product_category = $this->input->post('product_category');
					$module_type = $this->input->post('module_type');
					$response['company_name'] = $company_name;

					if(!empty($company_id)){

						######################## QUOTATION VS PROFORMA MONTHLY #####################

							$response['quotation_vs_proforma_monthly'] = $this->quotation_vs_proforma_monthly($company_id);
						###############################  END  #################################

						######################## QUOTATION VS PROFORMA YEARLY #####################

							$response['quotation_vs_proforma_yearly'] = $this->quotation_vs_proforma_yearly($company_id);
						###############################  END  ##################################

						######################## EXPORT STATS YEARLY ###############################

							$response['export_stats_yearly'] = $this->export_stats_yearly($company_name, $module_type, $product_category);
						###############################  END  ##################################

						######################## EXPORT STATS YEARLY INTERNAL ########################

							$response['export_stats_yearly_internal'] = $this->export_stats_yearly_internal($company_name, $module_type, $product_category);
						###############################  END  ###################################

						######################## EXPORTER DATA ####################################

							$exporter_data_select = "exporter_name as name, exporter_fob as value";
							$exporter_data_where = "importer_name LIKE '%".strtoupper($company_name)."%'";

							if(in_array($module_type, array('lead'))){

								if(in_array($product_category, array('PIPES', 'TUBES', 'TUBING', 'HAMMER UNION'))){

									$exporter_data_where .= "AND type = '".strtolower(str_replace(" ", "_", $product_category))."'";
								}
							}
							$exporter_data = $this->common_model->get_dynamic_data_marketing_db($exporter_data_select, $exporter_data_where, 'competitor_ranks_details');
							// echo "<pre>";print_r($exporter_data);echo"</pre><hr>";exit;

							if(!empty($exporter_data)){

								foreach ($exporter_data as $ex_key => $ex_value) {

									$response['exporters_data'][$ex_key]['name'] = $ex_value['name'];
									$response['exporters_data'][$ex_key]['y'] = (int)$ex_value['value'];
								}
							}
						###############################  END  ###################################

						######################## Production Status Wise Graph ############################

							if(in_array($this->session->userdata('role'), array(1, 17))){

								$response['production_status_wise'] = $this->get_order_wise_total_new($company_id, $module_type);
							}
						###############################  END  ###################################

						######################## IMPORT STATS ####################################

							$import_stats_data_select = "description as name, fob";
							$import_stats_data_where .= "importer_name LIKE '%".strtoupper($company_name)."%'";
							if(in_array($product_category, array('internal'))){

								$import_stats_data_select = "product as name, fob";
								$import_stats_data_where .= " AND type = '".strtolower(str_replace(" ", "_", $product_category))."'";
							}

							$import_stats_data = $this->common_model->get_all_conditional_data_marketing_db($import_stats_data_select, $import_stats_data_where, 'import_details', 'result_array', array(), array('column_name'=>'fob', 'column_value'=>'desc'));
							// echo "<pre>";print_r($import_stats_data);echo"</pre><hr>";exit;
							if(!empty($import_stats_data)){

								foreach ($import_stats_data as $import_stats_data_key => $import_stats_data_value) {
									if(round($import_stats_data_value['fob']) > 0){

										$response['import_stats_data'][$import_stats_data_key]['name'] = $import_stats_data_value['name'];
										$response['import_stats_data'][$import_stats_data_key]['y'] = round($import_stats_data_value['fob']);
									}
								}
							}
						###############################  END  ####################################

						####################### DECISION MAKER ##############################
							$decision_maker = $this->common_model->get_all_conditional_data_sales_db("member_name, decision_maker", array('comp_mst_id'=> $company_id, 'decision_maker > '=> 0), 'customer_dtl', 'result_array', array(), array('column_name'=>'decision_maker', 'column_value'=>'desc'));
							if(!empty($decision_maker)){

								foreach ($decision_maker as $dmkey => $dmvalue) {

									$response['decision_maker_data'][$dmkey]['name'] = $dmvalue['member_name'];
									$response['decision_maker_data'][$dmkey]['y'] = round($dmvalue['decision_maker']);
								}
							}
						################################ END ###############################

						####################### LEAD TABLE #################################
							$client_details = $this->common_model->get_dynamic_data_sales_db('*', array('id'=>$company_id), 'customer_mst', 'row_array');
							$response['additional_information'] =
							"
								<h4>Additional Information</h4>
								<table class='table table-bordered'>
									<tr>
										<th>Purchase Decision Factors</th>
										<td><b>Rank 1</b> - ".$client_details['purchase_factor_1']."</td>
										<td><b>Rank 2</b> - ".$client_details['purchase_factor_2']."</td>
										<td><b>Rank 3</b> - ".$client_details['purchase_factor_3']."</td>
									</tr>
									<tr>
										<th>No. of employees</th>
										<td>".$client_details['no_of_employees']."</td>
										<th>Last Buy</th>
										<td>".$client_details['last_purchased']."</td>
									</tr>
									<tr>
										<th>Specific Product to Pitch</th>
										<td>".$client_details['product_pitch']."</td>
										<th>Margins</th>
										<td>".$client_details['margins']."</td>
									</tr>
									<tr>
										<th>Sales Notes</th>
										<td colspan='3'>".$client_details['sales_notes']."</td>
									</tr>
									<tr>
										<th>Purchase Comments</th>
										<td colspan='3'>".$client_details['purchase_comments']."</td>
									</tr>
								</table>
							";
						############################### END ###############################
					}
				break;

				default:
					$response['status'] = 'failed';	
					$response['message'] = "call type not found";
				break;
			}
			echo json_encode($response);
		} else{
			die('access is not allowed to this function');
		}
	}

	private function export_stats_yearly($company_name, $module_type, $product_category = ''){

		$return_array = array();
		$table_name = "product_details";
		$return_array['category'] = array();
		$return_array['series'] = array();
		$return_array['total'] = 0;
		if(true){

			$export_stats_yearly_select = "SUM(fob) as value, '' as exporter_name, year, type";
			$export_stats_yearly_where = "new_importer_name LIKE '%".strtoupper($company_name)."%'";
			if(in_array($product_category, array('PIPES', 'TUBES', 'TUBING', 'HAMMER UNION', 'PROCESS_CONTROL'))){

				$export_stats_yearly_where .= " AND type = '".strtolower(str_replace(" ", "_", $product_category))."'";
			}
			$export_stats_yearly_group_by = array('year');

			if($this->session->userdata('lead_access')['lead_list_action_graph_expoter_name_access']){

				$export_stats_yearly_select = "SUM(fob) as value, exporter_name, year, type";
				if(in_array($module_type, array('quotation', 'rfq'))){

					$export_stats_yearly_group_by = array('year');
				}else{

					$export_stats_yearly_group_by = array('exporter_name', 'year');
				}
			}

			$export_stats_yearly_data = $this->common_model->get_all_conditional_data_marketing_db($export_stats_yearly_select, $export_stats_yearly_where, $table_name, 'result_array', array(), array('column_name'=> 'year', 'column_value'=> 'ASC'), $export_stats_yearly_group_by);

			if(in_array($module_type, array('quotation', 'rfq'))){

				$series_name = $series_data = array();
				foreach ($export_stats_yearly_data as $single_details) {

					if(!in_array($single_details['year'], $return_array['category'])){

						$return_array['category'][] = $single_details['year'];
					}
					if(!in_array($single_details['type'], $series_name)){

						$series_name[] = $single_details['type'];
					}
					$return_array['total'] += $single_details['value'];
				}
				$static_year_value = array();
				foreach ($return_array['category'] as $year_key => $year_name) {

					$static_year_value[] = 0;
				}
				foreach ($export_stats_yearly_data as $single_details) {

					$return_array['series'][array_search($single_details['type'], $series_name)]['name'] = $single_details['type'];
					$return_array['series'][array_search($single_details['type'], $series_name)]['data'] = $static_year_value;

				}
				foreach ($export_stats_yearly_data as $single_details) {

					$return_array['series'][array_search($single_details['type'], $series_name)]['data'][array_search($single_details['year'], $return_array['category'])] = (int) $single_details['value'];
				}
			}else{

				$series_name = array();
				foreach ($export_stats_yearly_data as $single_details) {

					if(!in_array($single_details['year'], $return_array['category'])){

						$return_array['category'][] = $single_details['year'];
					}
					if(!in_array($single_details['exporter_name'], $series_name)){

						$series_name[] = $single_details['exporter_name'];
					}
					$return_array['total'] += $single_details['value'];
				}
				$static_year_value = array();
				foreach ($return_array['category'] as $year_key => $year_name) {

					$static_year_value[] = 0;
				}
				foreach ($export_stats_yearly_data as $single_details) {

					$return_array['series'][array_search($single_details['exporter_name'], $series_name)]['name'] = $single_details['exporter_name'];
					$return_array['series'][array_search($single_details['exporter_name'], $series_name)]['data'] = $static_year_value;
				}
				foreach ($export_stats_yearly_data as $single_details) {

					$return_array['series'][array_search($single_details['exporter_name'], $series_name)]['data'][array_search($single_details['year'], $return_array['category'])] = (int) $single_details['value'];
				}
			}
		}
		$return_array['total'] = $this->convert_number_into_indian_currency($return_array['total']);
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		return (!empty($return_array['category']) && !empty($return_array['series']) && !empty($return_array['total'])) ? $return_array : array();
	}
	private function export_stats_yearly_internal($company_name, $module_type, $product_category = ''){

		$return_array = array();
		$table_name = "trending_invoice";
		$return_array['series'] = array();
		$return_array['total'] = 0;
		if(true){

			if(in_array($this->session->userdata('role'), array(1))){

				$export_stats_yearly_data = $this->common_model->get_all_conditional_data_sales_db("SUM(FOB_VALUE_INR) as value, EXPORTER_NAME, NEW_IMPORTER_NAME, INVOICE_YEAR","NEW_IMPORTER_NAME LIKE '%".strtoupper($company_name)."%'", $table_name, 'result_array', array(), array('column_name'=> 'INVOICE_YEAR', 'column_value'=> 'ASC'), array('INVOICE_YEAR'));

			}else{

				$export_stats_yearly_data = $this->common_model->get_all_conditional_data_sales_db("SUM(FOB_VALUE_INR) as value, '' as EXPORTER_NAME, '' as NEW_IMPORTER_NAME, INVOICE_YEAR", "NEW_IMPORTER_NAME LIKE '%".strtoupper($company_name)."%'", $table_name, 'result_array', array(), array('column_name'=> 'INVOICE_YEAR', 'column_value'=> 'ASC'), array('INVOICE_YEAR'));
			}

			if(in_array($module_type, array('quotation', 'rfq'))){

				if(!empty($export_stats_yearly_data)){

					foreach($export_stats_yearly_data as $single_details){

						$return_array['series'][] = array(
							'name' => $single_details['INVOICE_YEAR'],
							'y' => (int)$single_details['value'],
							'drilldown' => $single_details['INVOICE_YEAR']
						);
						$return_array['total'] += $single_details['value'];
					}
				}
			}else if(in_array($module_type, array('lead'))){

				$series_name = array();
				foreach ($export_stats_yearly_data as $single_details) {
					
					if(!in_array($single_details['EXPORTER_NAME'], $return_array['category'])){

						$return_array['category'][] = $single_details['EXPORTER_NAME'];
					}
					if(!in_array($single_details['INVOICE_YEAR'], $series_name)){

						$series_name[] = $single_details['INVOICE_YEAR'];
					}
					$return_array['total'] += $single_details['value'];
				}
				$static_year_value = array();
				foreach ($return_array['category'] as $year_key => $year_name) {

					$static_year_value[] = 0;
				}
				foreach ($export_stats_yearly_data as $single_details) {

					$return_array['series'][array_search($single_details['INVOICE_YEAR'], $series_name)]['name'] = $single_details['INVOICE_YEAR'];
					$return_array['series'][array_search($single_details['INVOICE_YEAR'], $series_name)]['data'] = $static_year_value;
				}
				foreach ($export_stats_yearly_data as $single_details) {

					$return_array['series'][array_search($single_details['INVOICE_YEAR'], $series_name)]['data'][array_search($single_details['EXPORTER_NAME'], $return_array['category'])] = (int) $single_details['value'];
				}
			}
			$return_array['total'] = $this->convert_number_into_indian_currency($return_array['total']);
		}
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		return (!empty($return_array['series']) && !empty($return_array['total'])) ? $return_array : array();
	}
	private function quotation_vs_proforma_monthly($company_id){

		$return_array = array();
		$return_array['series'][0] = array();
		$return_array['series'][0]['name'] = 'WON';
		$return_array['series'][0]['color'] = '#28a745';
		$return_array['series'][1] = array();
		$return_array['series'][1]['name'] = 'OPEN';
		$return_array['series'][1]['color'] = '#007bff';
		$return_array['series'][2] = array();
		$return_array['series'][2]['name'] = 'CLOSED';
		$return_array['series'][2]['color'] = '#dc3545';

		if(!empty($company_id)){

			$quotation_list_details = $this->graph_management_model->get_quotation_vs_proforma_highchart_list($company_id);

			if(!empty($quotation_list_details)){
				foreach ($quotation_list_details as $key => $quotation_details) {

					$won_percentage = 0;
					if(!empty($quotation_details['won']) || $quotation_details['won'] > 0){
						$won_percentage = number_format((($quotation_details['won'] / $quotation_details['count']) * 100),2);
					}
					$return_array['category'][] = $quotation_details['month']."<br/>(".$won_percentage."%)";
					$return_array['series'][0]['data'][] = (int)$quotation_details['won'];
					$return_array['series'][1]['data'][] = (int)$quotation_details['open'];
					$return_array['series'][2]['data'][] = (int)$quotation_details['closed'];
				}
			}
		}
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		return $return_array;
	}
	private function quotation_vs_proforma_yearly($company_id){

		$return_array = array();

		$return_array['series'][0] = array();
		$return_array['series'][0]['name'] = 'WON';
		$return_array['series'][0]['color'] = '#28a745';
		$return_array['series'][1] = array();
		$return_array['series'][1]['name'] = 'OPEN';
		$return_array['series'][1]['color'] = '#007bff';
		$return_array['series'][2] = array();
		$return_array['series'][2]['name'] = 'CLOSED';
		$return_array['series'][2]['color'] = '#dc3545';

		if(!empty($company_id)){

			$quotation_list_details = $this->graph_management_model->get_quotation_vs_proforma_highchart_list_yearly($company_id);

			if(!empty($quotation_list_details)){

				foreach ($quotation_list_details as $key => $quotation_details) {

					$won_percentage = 0;
					if(!empty($quotation_details['won']) || $quotation_details['won'] > 0){

						$won_percentage = number_format((($quotation_details['won'] / $quotation_details['count']) * 100),2);
					}
					$return_array['category'][] = $quotation_details['year']."<br/>(".$won_percentage."%)";
					$return_array['series'][0]['data'][] = (int)$quotation_details['won'];
					$return_array['series'][1]['data'][] = (int)$quotation_details['open'];
					$return_array['series'][2]['data'][] = (int)$quotation_details['closed'];
				}
			}
		}
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		return $return_array;
	}
	private function get_order_wise_total_new($company_id, $module_type){

		if(in_array($module_type, array('lead'))){

			$return_data['series'] = array(
										array('name'=>'Pending Order', 'data'=> array(0)),
										array('name'=>'Semi Ready', 'data'=> array(0)),
										array('name'=>'MTT', 'data'=> array(0)),
										array('name'=>'Query', 'data'=> array(0)),
										array('name'=>'MTT/RFD', 'data'=> array(0)),
										array('name'=>'Ready To Dispatch', 'data'=> array(0))
									);
			$return_data['total'] = 0;
			foreach (array('pending_order', 'semi_ready', 'mtt', 'query', 'mtt_rfd', 'ready_to_dispatch_order') as $key => $status_name) {

				$order_total = $this->prepare_listing_data_new(implode(' AND ', $this->default_where_for_all_tab($status_name, $company_id)),array(),0,0);
				// echo "<pre>";print_r($order_total);echo"</pre><hr>";die('done');
				$total_in_rupees = $this->get_total_in_indian_currency($order_total['total_revenue_list']);
				// echo "<pre>";print_r($total_in_rupees);echo"</pre><hr>";die('done');
				$return_data['series'][$key]['data'] = array(round($total_in_rupees['total']));
				$return_data['total'] += $total_in_rupees['total'];
			}
			$return_data['total'] = $this->convert_number_into_indian_currency($return_data['total']);

		}else if(in_array($module_type, array('quotation', 'rfq'))){

			$return_series = array(
											array('name'=>'Pending Order', 'data'=> 0),
											array('name'=>'Semi Ready', 'data'=> 0),
											array('name'=>'MTT', 'data'=> 0),
											array('name'=>'Query', 'data'=> 0),
											array('name'=>'MTT/RFD', 'data'=> 0),
											array('name'=>'Ready To Dispatch', 'data'=> 0)
										);
			$return_total = 0;
			foreach (array('pending_order', 'semi_ready', 'mtt', 'query', 'mtt_rfd', 'ready_to_dispatch_order') as $key => $status_name) {

				$order_total = $this->prepare_listing_data_new(implode(' AND ', $this->default_where_for_all_tab($status_name, $company_id)),array(),0,0);
				// echo "<pre>";print_r($order_total);echo"</pre><hr>";die('done');
				$total_in_rupees = $this->get_total_in_indian_currency($order_total['total_revenue_list']);
				// echo "<pre>";print_r($total_in_rupees);echo"</pre><hr>";die('done');
				$return_series[$key]['data'] = round($total_in_rupees['total']);
				$return_total += $total_in_rupees['total'];
			}
			$return_data['total'] = $this->convert_number_into_indian_currency($return_total);

			foreach($return_series as $single_return_series){

				$return_data['series'][] = array(
															'name' => $single_return_series['name'],
															'y' => $single_return_series['data'],
															'drilldown' => $single_return_series['name']);
			}
		}
		// echo "<pre>";print_r($return_data);echo"</pre><hr>";die('done');
		return $return_data;
	}
	private function convert_number_into_indian_currency($num){

		$num = number_format($num,0,'','');
		$new_num = '';
		if(strlen($num) < 4) {
			$new_num = $num;
		}else{
			if(strlen($num) % 2 == 0) {
				for ($i=0; $i < strlen($num)-3; $i++) { 
					
					if(($i % 2) == 0) {

						$new_num .= $num[$i];
						$new_num .= ',';
					}else {

						$new_num .= $num[$i];
					}
				}
				$new_num .= $num[strlen($num)-3];
				$new_num .= $num[strlen($num)-2];
				$new_num .= $num[strlen($num)-1];
			}else {
				for ($i=0; $i < strlen($num)-3; $i++) { 
					
					if(($i % 2) != 0) {

						$new_num .= $num[$i];
						$new_num .= ',';
					}else {

						$new_num .= $num[$i];
					}
				}
				$new_num .= $num[strlen($num)-3];
				$new_num .= $num[strlen($num)-2];
				$new_num .= $num[strlen($num)-1];
			}
		}
		return $new_num;
	}
	private function get_total_in_indian_currency($total_details) {

		// getting currency details
		$currency_details = array_column($this->common_model->get_dynamic_data_sales_db('*',array('status'=>'Active'), 'currency'), 'currency_rate', 'currency_id');
		$currency_value = $currency_details[1];
		$num = 0;
		foreach ($total_details as $single_details) {

			$num += round((((int)$single_details['total']) * ($currency_details[$single_details['currency']] / $currency_value)));
			// $num +=  $single_details['total'] * $currency_details[$single_details['currency']];
		}
		return array('total'=>$num);
	}
	private function prepare_listing_data_new($where, $order_by, $limit, $offset, $search_filter_array = array()) {

		$data = $this->production_model->get_production_listingz_data($where, $order_by,$limit, $offset);
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$return_data['total_revenue_list'] = array();
		$return_data['paggination_data'] = $data['paggination_data'];
		foreach ($data['production_list'] as $production_list_key => $production_list_value) {


			$data['production_list'][$production_list_key]['total_status_wise'] = $this->create_total_status_wise($production_list_value);

			$tab_name = $this->session->userdata('production_tab_name');
			$static_tab_name_to_status_key_array = array(
														'semi_ready'=> array('semi_ready', 'semi_ready_new'),
														'mtt'=> array('mtt', 'mtt_new'),
														'query'=> array('query', 'query_new'),
														'mtt_rfd'=> array('mtt_rfd', 'mtt_rfd_new'),
														'ready_to_dispatch_order'=> array('ready_to_dispatch_order', 'ready_to_dispatch_order_new'),
														'dispatch_order'=> array('dispatch_order', 'dispatch_order_new'),
														'on_hold_order'=> array('on_hold_order', 'on_hold_order_new'),
														'order_cancelled'=> array('order_cancelled', 'order_cancelled_new'),
													);

			if (
				$this->session->userdata('production_tab_name') != 'all_order' &&
				$this->session->userdata('production_tab_name') != 'pending_order' &&
				$data['production_list'][$production_list_key]['total_status_wise'][$static_tab_name_to_status_key_array[$this->session->userdata('production_tab_name')][0]]['value'] == 0 && $data['production_list'][$production_list_key]['total_status_wise'][$static_tab_name_to_status_key_array[$this->session->userdata('production_tab_name')][1]]['value'] == 0) {

				unset($data['production_list'][$production_list_key]);
				$data['paggination_data']['total_rows']--;
				continue;
			}else{
				
				if(isset($data['production_list'][$production_list_key]['total_status_wise']['semi_ready_new'])){

					unset($data['production_list'][$production_list_key]['total_status_wise']['semi_ready_new']);
				}
				if(isset($data['production_list'][$production_list_key]['total_status_wise']['mtt_new'])){

					unset($data['production_list'][$production_list_key]['total_status_wise']['mtt_new']);
				}
				if(isset($data['production_list'][$production_list_key]['total_status_wise']['query_new'])){

					unset($data['production_list'][$production_list_key]['total_status_wise']['query_new']);
				}
				if(isset($data['production_list'][$production_list_key]['total_status_wise']['mtt_rfd_new'])){

					unset($data['production_list'][$production_list_key]['total_status_wise']['mtt_rfd_new']);
				}
				if(isset($data['production_list'][$production_list_key]['total_status_wise']['ready_to_dispatch_order_new'])){

					unset($data['production_list'][$production_list_key]['total_status_wise']['ready_to_dispatch_order_new']);
				}
				if(isset($data['production_list'][$production_list_key]['total_status_wise']['dispatch_order_new'])){

					unset($data['production_list'][$production_list_key]['total_status_wise']['dispatch_order_new']);
				}
				if(isset($data['production_list'][$production_list_key]['total_status_wise']['on_hold_order_new'])){

					unset($data['production_list'][$production_list_key]['total_status_wise']['on_hold_order_new']);
				}
				if(isset($data['production_list'][$production_list_key]['total_status_wise']['order_cancelled_new'])){

					unset($data['production_list'][$production_list_key]['total_status_wise']['order_cancelled_new']);
				}
			}

			if(!empty($data['production_list'][$production_list_key]['total_status_wise'])){

				foreach($data['production_list'][$production_list_key]['total_status_wise'] as $total_status_wise_key => $total_status_wise_value) {

					if($total_status_wise_key == $tab_name && $total_status_wise_value['value'] > 0){

						if(empty($return_data['total_revenue_list'][$production_list_value['currency']])) {

							$return_data['total_revenue_list'][$production_list_value['currency']]['currency'] = $production_list_value['currency'];
							$return_data['total_revenue_list'][$production_list_value['currency']]['total'] = $total_status_wise_value['value'];
						} else {

							$return_data['total_revenue_list'][$production_list_value['currency']]['total'] += $total_status_wise_value['value'];
						}
					}
				}
			}

			// echo "<pre>";print_r($return_data['total_revenue_list']);echo"</pre><hr>";
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";die('came in');
		return $return_data;
	}
	private function default_where_for_all_tab($tab_name, $company_id){

		$this->session->set_userdata('production_tab_name',$tab_name);
		$return_where = array(
							0=> "quotation_mst.stage = 'proforma'",
							1=> "quotation_mst.status = 'Won'",
							2=> "quotation_mst.work_order_no != 0",
							3=> "(production_process_information.production_status is NULL OR production_process_information.production_status = 'yet_to_start_production' OR production_process_information.production_status = 'in_production' OR production_process_information.production_status = 'pending_order' OR (production_process_information.production_status = 'semi_ready' OR quotation_dtl.semi_ready_count > 0) OR (production_process_information.production_status = 'merchant_trade' OR quotation_dtl.mtt_count > 0) OR (production_process_information.production_status = 'query' OR quotation_dtl.query_count > 0) OR (production_process_information.production_status = 'mtt_rfd' OR quotation_dtl.mtt_rfd_count > 0) OR (production_process_information.production_status = 'ready_for_dispatch' OR quotation_dtl.ready_to_dispatch_order_count > 0))",
							4=> "(production_process_information.status is NULL or production_process_information.status = 'Active')");
		if($tab_name == 'dispatch_order') {
			$return_where[3] = "(production_process_information.production_status = 'dispatched' OR quotation_dtl.dispatch_order_count > 0)";
		
		}elseif ($tab_name == 'on_hold_order') {
			$return_where[3] = "(production_process_information.production_status = 'on_hold' OR quotation_dtl.on_hold_order_count > 0)";
		
		}elseif ($tab_name == 'ready_to_dispatch_order') {
			$return_where[3] = "(production_process_information.production_status = 'ready_for_dispatch' OR quotation_dtl.ready_to_dispatch_order_count > 0)";
		
		}elseif ($tab_name == 'semi_ready') {
			$return_where[3] = "(production_process_information.production_status = 'semi_ready' OR quotation_dtl.semi_ready_count > 0)";
		
		}elseif ($tab_name == 'order_cancelled') {
			$return_where[3] = "(production_process_information.production_status = 'order_cancelled' OR quotation_dtl.order_cancelled_count > 0)";
		
		}elseif ($tab_name == 'mtt') {
			$return_where[3] = "(production_process_information.production_status = 'merchant_trade' OR quotation_dtl.mtt_count > 0)";
		
		}elseif ($tab_name == 'query') {
			$return_where[3] = "(production_process_information.production_status = 'query' OR quotation_dtl.query_count > 0)";
		
		}elseif ($tab_name == 'mtt_rfd') {
			$return_where[3] = "(production_process_information.production_status = 'mtt_rfd' OR quotation_dtl.mtt_rfd_count > 0)";
		}elseif ($tab_name == 'all_order') {

			unset($return_where[3]);
		}
   
		if(in_array($this->session->userdata('role'), array(5, 8))) {
			$return_where[5] = "quotation_mst.assigned_to = ".$this->session->userdata('user_id');
			if($this->session->userdata('role') == 8) {
				$procurement_user_name = $this->common_model->get_dynamic_data_sales_db('name',array('user_id'=>$this->session->userdata('user_id')), 'users', 'row_array');
				$user_explode = explode(' ',$procurement_user_name['name']);
				$return_where[5] = "production_process_information.handled_by LIKE '%".$user_explode['0']."%'";
			}

		}else if(in_array($this->session->userdata('role'), array(18))) {
			
			$procurement_user_name = $this->common_model->get_dynamic_data_sales_db('name',array('user_id'=>$this->session->userdata('user_id')), 'users', 'row_array');
			$user_explode = explode(' ',$procurement_user_name['name']);
			$return_where[5] = "production_process_information.handled_by LIKE '%".$user_explode['0']."%'";
		}

		$return_where[] = "quotation_mst.client_id = ".$company_id;
		// echo "<pre>";print_r($return_where);echo"</pre><hr>";exit;
		return $return_where;
	}
	private function create_total_status_wise($product_details){

		$return_array = array(
			'pending_order' => array(
				'name'=> 'Pending',
				'value'=> 0
			),
			'semi_ready' => array(
				'name'=> 'Semi Ready',
				'value'=> 0
			),
			'semi_ready_new' => array(
				'name'=> 'Semi Ready NEW',
				'value'=> 0,
			),
			'mtt' => array(
				'name'=> 'Import cum Export(ICE)',
				'value'=> 0,
			),
			'mtt_new' => array(
				'name'=> 'Import cum Export(ICE) NEW',
				'value'=> 0,
			),
			'query' => array(
				'name'=> 'Query',
				'value'=> 0,
			),
			'query_new' => array(
				'name'=> 'Query NEW',
				'value'=> 0,
			),
			'mtt_rfd' => array(
				'name'=> 'In transit ICE',
				'value'=> 0,
			),
			'mtt_rfd_new' => array(
				'name'=> 'In transit ICE NEW',
				'value'=> 0,
			),
			'ready_to_dispatch_order' => array(
				'name'=> 'RFD',
				'value'=> 0,
			),
			'ready_to_dispatch_order_new' => array(
				'name'=> 'RFD NEW',
				'value'=> 0,
			),
			'dispatch_order' => array(
				'name'=> 'Dispatch',
				'value'=> 0,
			),
			'dispatch_order_new' => array(
				'name'=> 'Dispatch NEW',
				'value'=> 0,
			),
			'on_hold_order' => array(
				'name'=> 'Hold',
				'value'=> 0,
			),
			'on_hold_order_new' => array(
				'name'=> 'Hold NEW',
				'value'=> 0,
			),
			'order_cancelled' => array(
				'name'=> 'Cancelled',
				'value'=> 0,
			),
			'order_cancelled_new' => array(
				'name'=> 'Cancelled NEW',
				'value'=> 0,
			),
		);
		// echo "<pre>";print_r($product_details);echo"</pre><hr>";exit;

		$return_array['pending_order']['value'] += $product_details['pending_count'];
		$return_array['semi_ready']['value'] += $product_details['semi_ready_count'];
		$return_array['semi_ready_new']['value'] += $product_details['semi_ready_count_2'];
		$return_array['ready_to_dispatch_order']['value'] += $product_details['rfd_count'];
		$return_array['ready_to_dispatch_order_new']['value'] += $product_details['rfd_count_2'];
		$return_array['dispatch_order']['value'] += $product_details['dispatch_count'];
		$return_array['dispatch_order_new']['value'] += $product_details['dispatch_count_2'];
		$return_array['on_hold_order']['value'] += $product_details['hold_count'];
		$return_array['on_hold_order_new']['value'] += $product_details['hold_count_2'];
		$return_array['mtt']['value'] += $product_details['mtt_count'];
		$return_array['mtt_new']['value'] += $product_details['mtt_count_2'];
		$return_array['query']['value'] += $product_details['query_count'];
		$return_array['query_new']['value'] += $product_details['query_count_2'];
		$return_array['mtt_rfd']['value'] += $product_details['mtt_rfd_count'];
		$return_array['mtt_rfd_new']['value'] += $product_details['mtt_rfd_count_2'];
		$return_array['order_cancelled']['value'] += $product_details['cancel_count'];
		$return_array['order_cancelled_new']['value'] += $product_details['cancel_count_2'];

		return $return_array;
	}
}