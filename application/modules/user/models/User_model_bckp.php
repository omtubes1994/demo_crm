<?php 
Class User_model extends CI_Model{

	function __construct(){
		parent::__construct();
		$CI = &get_instance();
		$this->db2 = $CI->load->database('marketing', true);
	}

	public function get_all_conditional_data($select = '*', $where, $table_name, $return_type = 'result_array', $join_array = array(), $order_by_array = array(), $group_by_array = array(),$limit = 0, $offset = 0) {

		$this->db->select($select);
		$this->db->where($where);
		if(!empty($group_by_array)) {
			$this->db->group_by($group_by_array);
		}
		if(!empty($order_by_array)) {
			$this->db->order_by($order_by_array['column_name'], $order_by_array['column_value']);
		}
		return $this->db->get($table_name, $limit, $offset)->$return_type();
	}

	public function insert_data($table, $data){
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	public function update_data($table, $data, $where){              
		return $this->db->update($table, $data, $where);
	}

	public function update_data_sales_db($table, $data, $where){
		$this->db2->where($where, null, false);
		return $this->db2->update($table, $data);
	}

	public function get_dynamic_data($select = '*', $where, $table_name, $return_type = 'result_array', $limit = 0, $offset = 0) {

		$this->db->select($select);
		$this->db->where($where);
		return $this->db->get($table_name, $limit, $offset)->$return_type();
	}


	public function get_user_details($where, $limit, $offset) {

		$return_array = array();
		$this->db->select('SQL_CALC_FOUND_ROWS *', FALSE);
		if(!empty($where)) {
			$this->db->where($where);
		}
		$return_array['user_list'] = $this->db->get('users', $limit, $offset)->result_array();
		$return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows;")->row_array();
		return $return_array;
	}

	public function insert_data_batch($table_name, $insert_data) {

		$this->db->insert_batch($table_name, $insert_data);
	}

	public function update_data_batch($table_name, $update_data, $where_string) {

		$this->db->update_batch($table_name, $update_data, $where_string);	
	}

	public function get_login_verification_exclude() {

		$this->db->select('users.name as user_name,users.user_id as user_id,login_verification_ignore.id as id ,role.role_name as role');
		$this->db->join('users', 'users.user_id = login_verification_ignore.user_id', 'left');
		$this->db->join('role', 'users.role = role.role_id', 'left');
		$this->db->where(array('login_verification_ignore.status' => 'Active'));
		return $this->db->get('login_verification_ignore')->result_array();
	}

	public function get_not_excluded_users($login_excluded_id) {

		if(!empty($login_excluded_id)) {

			return $this->db->where_not_in('user_id', $login_excluded_id)->get_where('users', array('status'=> 1))->result_array();
		}else {

			return $this->db->get_where('users', array('status'=> 1))->result_array();
		}

	}
}