<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MX_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('user_id')){
			redirect('login', 'refresh');
			exit;
		}else{
			$access_count = 0;
			if(!empty($this->session->userdata('main_module_access')) && in_array(16, array_column($this->session->userdata('main_module_access'), 'module_id'))) {

				foreach ($this->session->userdata('sub_module_access') as $key => $value) {
					
					if($value['module_id'] == 16 && $value['status'] == 'Active') {
						$access_count++;
					}
				}
			}
			if($access_count == 0){
				redirect($this->session->userdata('home/dashboard'));
				exit;
			}
		}
		error_reporting(E_ALL);
		$this->load->model('user_model');
	}

	public function index(){
		redirect('user/user_management');
	}

	public function user_management() {

		$data = array();
		$data = $this->create_user_details(array('status'=>1), 100, 00);
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'User Management'));
		$this->load->view('sidebar', array('title' => 'User Management'));
		$this->load->view('user/user_dashboard', $data);
		$this->load->view('footer');
	}
	
	public function ajax_function() {

		if($this->input->is_ajax_request()){
			$call_type = $this->input->post('call_type');
			$response['status'] = 'successful';	
			switch ($call_type) {

				case 'get_single_user_details':
					
					$user_id = $this->input->post('user_id');
					$role = $this->user_model->get_dynamic_data('*', array(), 'role');
					$data['role'] = array_column($role, 'role_name', 'role_id');
					$data['single_user_details'] = $this->user_model->get_dynamic_data('*', array('user_id'=>$user_id), 'users', 'row_array');
					$data['modules'] = $this->user_model->get_dynamic_data('*', array('status'=>'active'), 'modules');
					$module_access = $this->user_model->get_dynamic_data('*', array('user_id'=>$user_id, 'status'=> 'Active'), 'user_to_module');
					$data['module_access'] = array_unique(array_column($module_access, 'module_id', 'master_id'));					
					$sub_module = $this->user_model->get_dynamic_data('*', array('status'=>'Active'), 'sub_module');
					foreach ($sub_module as $sub_module_value) {
						
						$data['sub_module'][$sub_module_value['module_id']][] = array( 'sub_module_id' => $sub_module_value['sub_module_id'], 'sub_module_name' => $sub_module_value['sub_module_name']);
					}
					$sub_module_access = $this->user_model->get_dynamic_data('*', array('status'=>'Active', 'user_id'=> $user_id), 'user_to_sub_module');
					$data['sub_module_access'] = array_column($sub_module_access, 'sub_module_id', 'id');
					$response['html_body'] =  $this->load->view('user/single_user_details', $data, TRUE);
					break;

				case 'update_user_details':

					$user_id = (int)$this->input->post('user_id');
					
					// sub module code
						$sub_module_access = $this->user_model->get_dynamic_data('*', array('user_id'=>$user_id), 'user_to_sub_module');
						$sub_module_access_data = array();
						foreach ($sub_module_access as $sub_module_access_value) {
							$sub_module_access_data[$sub_module_access_value['sub_module_id']] = $sub_module_access_value;
						}
						$sub_module_status =  array();
						$sub_module_status[] = array('module_name' => 'dashboard_home', 'module_id' => 1, 'sub_module_id' => 1);
						$sub_module_status[] = array('module_name' => 'quotation_add', 'module_id' => 2, 'sub_module_id' => 2);
						$sub_module_status[] = array('module_name' => 'quotation_list', 'module_id' => 2, 'sub_module_id' => 3);
						$sub_module_status[] = array('module_name' => 'quotation_draft_list', 'module_id' => 2, 'sub_module_id' => 4);
						$sub_module_status[] = array('module_name' => 'quotation_follow_list', 'module_id' => 2, 'sub_module_id' => 5);
						$sub_module_status[] = array('module_name' => 'quotation_proforma_list', 'module_id' => 2, 'sub_module_id' => 6);
						$sub_module_status[] = array('module_name' => 'invoice_add', 'module_id' => 3, 'sub_module_id' => 7);
						$sub_module_status[] = array('module_name' => 'invoice_list', 'module_id' => 3, 'sub_module_id' => 8);
						$sub_module_status[] = array('module_name' => 'client_add', 'module_id' => 4, 'sub_module_id' => 9);
						$sub_module_status[] = array('module_name' => 'client_list', 'module_id' => 4, 'sub_module_id' => 10);
						$sub_module_status[] = array('module_name' => 'client_member_list', 'module_id' => 4, 'sub_module_id' => 11);
						$sub_module_status[] = array('module_name' => 'piping_1', 'module_id' => 5, 'sub_module_id' => 12);
						$sub_module_status[] = array('module_name' => 'piping_2', 'module_id' => 5, 'sub_module_id' => 13);
						$sub_module_status[] = array('module_name' => 'piping_3', 'module_id' => 5, 'sub_module_id' => 14);
						$sub_module_status[] = array('module_name' => 'piping_4', 'module_id' => 5, 'sub_module_id' => 15);
						$sub_module_status[] = array('module_name' => 'piping_5', 'module_id' => 5, 'sub_module_id' => 16);
						$sub_module_status[] = array('module_name' => 'piping_6', 'module_id' => 5, 'sub_module_id' => 17);
						$sub_module_status[] = array('module_name' => 'instrument_1', 'module_id' => 6, 'sub_module_id' => 18);
						$sub_module_status[] = array('module_name' => 'instrument_2', 'module_id' => 6, 'sub_module_id' => 19);
						$sub_module_status[] = array('module_name' => 'instrument_3', 'module_id' => 6, 'sub_module_id' => 20);
						$sub_module_status[] = array('module_name' => 'instrument_4', 'module_id' => 6, 'sub_module_id' => 21);
						$sub_module_status[] = array('module_name' => 'instrument_5', 'module_id' => 6, 'sub_module_id' => 22);
						$sub_module_status[] = array('module_name' => 'others_1', 'module_id' => 7, 'sub_module_id' => 23);
						$sub_module_status[] = array('module_name' => 'others_2', 'module_id' => 7, 'sub_module_id' => 24);
						$sub_module_status[] = array('module_name' => 'pq_add', 'module_id' => 8, 'sub_module_id' => 25);
						$sub_module_status[] = array('module_name' => 'pq_pending_list', 'module_id' => 8, 'sub_module_id' => 26);
						$sub_module_status[] = array('module_name' => 'pq_approved_list', 'module_id' => 8, 'sub_module_id' => 27);
						$sub_module_status[] = array('module_name' => 'procurement_add', 'module_id' => 9, 'sub_module_id' => 28);
						$sub_module_status[] = array('module_name' => 'procurement_list', 'module_id' => 9, 'sub_module_id' => 29);
						$sub_module_status[] = array('module_name' => 'quality_add', 'module_id' => 10, 'sub_module_id' => 30);
						$sub_module_status[] = array('module_name' => 'quality_list', 'module_id' => 10, 'sub_module_id' => 31);
						$sub_module_status[] = array('module_name' => 'marking_add', 'module_id' => 10, 'sub_module_id' => 32);
						$sub_module_status[] = array('module_name' => 'marking_proforma_list', 'module_id' => 10, 'sub_module_id' => 33);
						$sub_module_status[] = array('module_name' => 'marking_sample_list', 'module_id' => 10, 'sub_module_id' => 34);
						$sub_module_status[] = array('module_name' => 'task_list', 'module_id' => 11, 'sub_module_id' => 35);
						$sub_module_status[] = array('module_name' => 'vendors_add', 'module_id' => 12, 'sub_module_id' => 36);
						$sub_module_status[] = array('module_name' => 'vendors_list', 'module_id' => 12, 'sub_module_id' => 37);
						$sub_module_status[] = array('module_name' => 'reports_touch_point_list', 'module_id' => 13, 'sub_module_id' => 38);
						$sub_module_status[] = array('module_name' => 'reports_daily_task_list', 'module_id' => 13, 'sub_module_id' => 39);
						$sub_module_status[] = array('module_name' => 'reports_daily_report', 'module_id' => 13, 'sub_module_id' => 47);
						$sub_module_status[] = array('module_name' => 'queries_sales_open', 'module_id' => 14, 'sub_module_id' => 40);
						$sub_module_status[] = array('module_name' => 'queries_sales_closed', 'module_id' => 14, 'sub_module_id' => 41);
						$sub_module_status[] = array('module_name' => 'queries_proforma_open', 'module_id' => 14, 'sub_module_id' => 42);
						$sub_module_status[] = array('module_name' => 'queries_proforma_closed', 'module_id' => 14, 'sub_module_id' => 43);
						$sub_module_status[] = array('module_name' => 'queries_purchase_open', 'module_id' => 14, 'sub_module_id' => 44);
						$sub_module_status[] = array('module_name' => 'queries_purchase_closed', 'module_id' => 14, 'sub_module_id' => 45);
						$sub_module_status[] = array('module_name' => 'setting_currency', 'module_id' => 15, 'sub_module_id' => 46);
						$sub_module_status[] = array('module_name' => 'user_management', 'module_id' => 16, 'sub_module_id' => 48);
						$sub_module_status[] = array('module_name' => 'hr_add_employee', 'module_id' => 17, 'sub_module_id' => 49);
						$sub_module_status[] = array('module_name' => 'primary_internal_data', 'module_id' => 18, 'sub_module_id' => 50);
						$sub_module_status[] = array('module_name' => 'hr_employee_list', 'module_id' => 17, 'sub_module_id' => 51);
						$sub_module_status[] = array('module_name' => 'search_engine_primary_data', 'module_id' => 19, 'sub_module_id' => 52);
						$sub_module_status[] = array('module_name' => 'setting_vendor_management', 'module_id' => 15, 'sub_module_id' => 53);
						$sub_module_status[] = array('module_name' => 'procurement_listingz', 'module_id' => 9, 'sub_module_id' => 54);
						$sub_module_status[] = array('module_name' => 'others_3', 'module_id' => 7, 'sub_module_id' => 55);
						$sub_module_status[] = array('module_name' => 'production_status', 'module_id' => 20, 'sub_module_id' => 56);
						$sub_module_insert_data = $sub_module_update_data = array();
						foreach ($sub_module_status as $key => $value) {
							
							if(!empty($sub_module_access_data[$value['sub_module_id']])){

								$sub_module_update_data[$key]['id'] = $sub_module_access_data[$value['sub_module_id']]['id'];
								$sub_module_update_data[$key]['status'] = 'Inactive';
								if($this->input->post($value['module_name']) == 'Yes') {

									$sub_module_update_data[$key]['status'] = 'Active';
								}
							} else {

								$sub_module_insert_data[$key]['user_id'] = $user_id;
								$sub_module_insert_data[$key]['module_id'] = $value['module_id'];
								$sub_module_insert_data[$key]['sub_module_id'] = $value['sub_module_id'];
								$sub_module_insert_data[$key]['status'] = 'Inactive';
								if($this->input->post($value['module_name']) == 'Yes') {

									$sub_module_insert_data[$key]['status'] = 'Active';
								}
							}
						}
						if(!empty($sub_module_update_data)) {

							$this->user_model->update_data_batch('user_to_sub_module',$sub_module_update_data, 'id');
						}
						if(!empty($sub_module_insert_data)) {

							$this->user_model->insert_data_batch('user_to_sub_module',$sub_module_insert_data);
						}
					// sub module code end	
					
					// main module action code
						$module_access = $this->user_model->get_dynamic_data('*', array('user_id'=>$user_id), 'user_to_module');
						$module_access_data = array();
						foreach ($module_access as $module_access_key => $module_access_value) {
							$module_access_data[$module_access_value['module_id']] = $module_access_value;
						}
						$main_module_status =  array();
						$main_module_status[] = array('module_name' => 'dashboard_module_status', 'module_id' => 1);
						$main_module_status[] = array('module_name' => 'quotation_module_status', 'module_id' => 2);
						$main_module_status[] = array('module_name' => 'invoice_module_status', 'module_id' => 3);
						$main_module_status[] = array('module_name' => 'client_module_status', 'module_id' => 4);
						$main_module_status[] = array('module_name' => 'piping_module_status', 'module_id' => 5);
						$main_module_status[] = array('module_name' => 'instrument_module_status', 'module_id' => 6);
						$main_module_status[] = array('module_name' => 'others_module_status', 'module_id' => 7);
						$main_module_status[] = array('module_name' => 'pq_module_status', 'module_id' => 8);
						$main_module_status[] = array('module_name' => 'procurement_module_status', 'module_id' => 9);
						$main_module_status[] = array('module_name' => 'quality_module_status', 'module_id' => 10);
						$main_module_status[] = array('module_name' => 'task_module_status', 'module_id' => 11);
						$main_module_status[] = array('module_name' => 'vendors_module_status', 'module_id' => 12);
						$main_module_status[] = array('module_name' => 'reports_module_status', 'module_id' => 13);
						$main_module_status[] = array('module_name' => 'queries_module_status', 'module_id' => 14);
						$main_module_status[] = array('module_name' => 'setting_module_status', 'module_id' => 15);
						$main_module_status[] = array('module_name' => 'user_module_status', 'module_id' => 16);
						$main_module_status[] = array('module_name' => 'hr_module_status', 'module_id' => 17);
						$main_module_status[] = array('module_name' => 'internal_module_status', 'module_id' => 18);
						$main_module_status[] = array('module_name' => 'search_engine_module_status', 'module_id' => 19);
						$main_module_status[] = array('module_name' => 'production_module', 'module_id' => 20);
						$main_module_update_data = $main_module_insert_data = array();
						foreach ($main_module_status as $key => $value) {

							if(!empty($module_access_data[$value['module_id']])){

								$main_module_update_data[$key]['master_id'] = $module_access_data[$value['module_id']]['master_id'];
								$main_module_update_data[$key]['status'] = 'Inactive';
								if($this->input->post($value['module_name']) == 1) {

									$main_module_update_data[$key]['status'] = 'Active';
								}
							} else {

								$main_module_insert_data[$key]['user_id'] = $user_id;
								$main_module_insert_data[$key]['module_id'] = $value['module_id'];
								$main_module_insert_data[$key]['status'] = 'Inactive';
								if($this->input->post($value['module_name']) == 1) {

									$main_module_insert_data[$key]['status'] = 'Active';
								}
							}
						}
						if(!empty($main_module_update_data)) {

							$this->user_model->update_data_batch('user_to_module',$main_module_update_data, 'master_id');
						}
						if(!empty($main_module_insert_data)) {

							$this->user_model->insert_data_batch('user_to_module',$main_module_insert_data);
						}
					// main module code end	
					if(!empty($this->input->post('employee_name'))) {

						$update_data['name'] = $this->input->post('employee_name');
					}
					if(!empty($this->input->post('email'))) {

						$update_data['email'] = $this->input->post('email');
					}
					if(!empty($this->input->post('username'))) {

						$update_data['username'] = $this->input->post('username');
					}
					if(!empty($this->input->post('password'))) {

						$update_data['password'] = md5($this->input->post('password'));
					}
					if(!empty($this->input->post('mobile'))) {

						$update_data['mobile'] = $this->input->post('mobile');
					}
					if(!empty($this->input->post('role'))) {

						$update_data['role'] = $this->input->post('role');
					}
					$update_data['status'] = $this->input->post('status');

					$this->user_model->update_data('users', $update_data, array('user_id'=> $user_id));
					$this->update_session_data_for_side_bar($user_id);
					break;

				case 'paggination_data_for_user':
					
					$where_array = array();
					$limit = 100;
					$offset = 00;
					if(!empty($this->input->post('search_user_name'))) {
						$where_array['name LIKE '] = '%'.$this->input->post('search_user_name').'%';
					}
					if(!empty($this->input->post('search_role'))) {
						$where_array['role'] = $this->input->post('search_role');
					}
					$where_array['status'] = $this->input->post('search_status');
					if(!empty($this->input->post('limit'))) {
						$limit = $this->input->post('limit');
					}
					if(!empty($this->input->post('offset'))) {
						$offset = $this->input->post('offset');
					}
					$data = $this->create_user_details($where_array, $limit, $offset);	
					$response['html_body'] = $this->load->view('user/user_list_body', $data, TRUE);
					$response['html_paggination'] = $this->load->view('user/paggination', $data, TRUE);
					break;

				case 'get_module_details':
					
					$data = array();
					$data['main_module'] = $this->user_model->get_dynamic_data('*', array('status'=>'Active'), 'modules');
					$sub_module = $this->user_model->get_dynamic_data('*', array('status'=>'Active'), 'sub_module');
					$data['sub_module'] = array();
					foreach ($sub_module as $sub_module_value) {
						
						$data['sub_module'][$sub_module_value['module_id']][] = array('sub_module_id' => $sub_module_value['sub_module_id'], 'sub_module_name' => $sub_module_value['sub_module_name']); 
					}
					$response['html_body'] =  $this->load->view('user/add_module_access', $data, TRUE);
					break;

				case 'add_main_module_name':

					$module_name = $this->input->post('module_name');
					$insert_result = $this->user_model->insert_data('modules', array('module_name'=>$module_name, 'module_controller'=>$module_name));
					if(empty($insert_result)){
						$response['status'] = 'failed';	
					}
					break;

				case 'add_sub_module_name':

					$module_id = $this->input->post('main_module_id');
					$sub_module_name = $this->input->post('sub_module_name');
					$insert_result = $this->user_model->insert_data('sub_module', array('module_id'=>$module_id, 'sub_module_name'=>$sub_module_name));
					if(empty($insert_result)){
						$response['status'] = 'failed';	
					}
					break;
						
				case 'update_main_module_name':

					$module_id = $this->input->post('module_id');
					$module_name = $this->input->post('module_name');
					$update_status = $this->user_model->update_data('modules',array('module_name'=>$module_name),array('module_id'=>$module_id));
					if(empty($update_status)){
						$response['status'] = 'failed';	
					}
					break;		

				case 'update_sub_module_name':
					
					$sub_module_id = $this->input->post('sub_module_id');
					$sub_module_name = $this->input->post('sub_module_name');
					$update_status = $this->user_model->update_data('sub_module',array('sub_module_name'=>$sub_module_name),array('sub_module_id'=>$sub_module_id));
					if(empty($update_status)){
						$response['status'] = 'failed';	
					}
					break;	
					
				case 'delete_main_module_name':
					
					$module_id = $this->input->post('module_id');
					$update_status_module = $this->user_model->update_data('modules',array('status'=>'Inactive'),array('module_id'=>$module_id));
					$update_status_sub_module = $this->user_model->update_data('sub_module',array('status'=>'Inactive'),array('module_id'=>$module_id));
					if(empty($update_status_module) || empty($update_status_sub_module)){
						$response['status'] = 'failed';	
					}
					break;

				case 'delete_sub_module_name':
					
					$sub_module_id = $this->input->post('sub_module_id');
					$update_status = $this->user_model->update_data('sub_module',array('status'=>'Inactive'),array('sub_module_id'=>$sub_module_id));
					if(empty($update_status)){
						$response['status'] = 'failed';	
					}
					break;	

				case 'get_add_user_details':

					$data['role'] = $this->user_model->get_dynamic_data('*', array(), 'role');
					$response['html_body'] =  $this->load->view('user/add_user_form', $data, TRUE);
					break;	

				case 'add_user':
					
					$insert_data['name'] = $this->input->post('employee_name');
					$insert_data['email'] = $this->input->post('email');
					$insert_data['username'] = $this->input->post('username');
					$insert_data['password'] = md5($this->input->post('password'));
					$insert_data['mobile'] = $this->input->post('mobile');
					$insert_data['role'] = $this->input->post('role');
					$insert_data['entered_on'] = date('Y-m-d H:i:s');
					$insert_data['status'] = 1;
					$insert_result = $this->user_model->insert_data('users', $insert_data);
					if(empty($insert_result)){
						$response['status'] = 'failed';	
					}
					break;	

				case 'get_user_name':
					
					$user_details = $this->user_model->get_dynamic_data('*', array('status'=>1, 'role' => $this->input->post('role_id')), 'users');
					if(!empty($user_details)) {
						$response['html_body'] = '<option value="">Select User</option>';
						foreach ($user_details as $user_single_details) {
							
							$response['html_body'] = $response['html_body'] . '<option value="'.$user_single_details['user_id'].'">'.ucwords(strtolower($user_single_details['name'])).'</option>';
						}
					}
					break;

				case 'delete_user_and_assign_to_other_user':
					
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					$deleted_user_id = $this->input->post('deleted_user_id');
					$assigned_to_user_id = $this->input->post('assigned_to');
					// changing deleted user assign quotation
					$this->user_model->update_data('quotation_mst',array('assigned_to' => $assigned_to_user_id), array('assigned_to' =>$deleted_user_id));
					// changing deleted user assign procurement
					$this->user_model->update_data('pq_client',array('assigned_to' => $assigned_to_user_id), array('assigned_to' =>$deleted_user_id));
					// changing deleted user assign rfq
					$this->user_model->update_data('rfq_mst',array('assigned_to' => $assigned_to_user_id), array('assigned_to' =>$deleted_user_id));
					// changing deleted user assign mtc
					$this->user_model->update_data('mtc_mst',array('assigned_to' => $assigned_to_user_id), array('assigned_to' =>$deleted_user_id));
					// changing deleted user Lead management
					$lead_source_array['hetro_leads'] = array( 
															array('source_name' => 'chemical companies', 'sub_module_id' => 14),
															array('source_name' => 'distributors', 'sub_module_id' => 20),
															array('source_name' => 'epc companies', 'sub_module_id' => 16),
															array('source_name' => 'heteregenous tubes india', 'sub_module_id' => 21),
															array('source_name' => 'pvf companies', 'sub_module_id' => 22),
															array('source_name' => 'shipyards', 'sub_module_id' => 13),
															array('source_name' => 'sugar companies', 'sub_module_id' => 17),
															array('source_name' => 'water companies', 'sub_module_id' => 15)
														);
					$lead_source_array['primary_leads'] = array(
															array('source_name' => 'pipes', 'sub_module_id' => 12),
															array('source_name' => 'tubes', 'sub_module_id' => 18),
															array('source_name' => 'process control', 'sub_module_id' => 19),
															array('source_name' => 'tubing', 'sub_module_id' => 23),
															array('source_name' => 'hammer union', 'sub_module_id' => 24)
														);
					foreach ($lead_source_array['hetro_leads'] as $value) {

						$this->user_model->update_data('clients', array('assigned_to' => $assigned_to_user_id), array('source' => $value['source_name'], 'assigned_to' => $deleted_user_id));
					}
					foreach ($lead_source_array['primary_leads'] as $value) {

						$this->user_model->update_data_sales_db('lead_mst', array('assigned_to' => $assigned_to_user_id), array('deleted is' => 'Null', 'imp_id >' => 0, 'assigned_to' => $deleted_user_id, 'data_category' => "'".$value['source_name']."'"));
					}
					break;	
						
				case 'get_department_graph_data':
					
					$data = array();
					$user_data = $this->user_model->get_all_conditional_data('role, count("*") as role_count', array('status'=>1), 'users', 'result_array', array(), array('column_name'=>'role', 'column_value'=>'ASC'), array('role'));
					$array_column_user_data = array_column($user_data, 'role_count', 'role');
					$response_array = array(
										array('name' => 'Admin','role_id' => '1'),
										array('name' => 'Analyst','role_id' => '2-3-17'),
										array('name' => 'Data Entry','role_id' => '4'),
										array('name' => 'Sales Team','role_id' => '5-16'),
										array('name' => 'Procurement Team','role_id' => '6-8'),
										array('name' => 'Pre Qualification','role_id' => '7'),
										array('name' => 'Quality Team','role_id' => '10-11'),
										array('name' => 'Accounts','role_id' => '12'),
										array('name' => 'HR','role_id' => '13'),
										array('name' => 'Document Losgistics','role_id' => '14'),
										array('name' => 'IT','role_id' => '15')
									);
					foreach ($response_array as $response_array_key => $department_name) {
						
						$data[$response_array_key]['name'] = $department_name['name'];
						$data[$response_array_key]['y'] = 0;
						// explode role id
						$explode_role_id = explode('-', $department_name['role_id']);
						foreach ($explode_role_id as $role_id) {
							
							if(!empty($array_column_user_data[$role_id])) {

								$data[$response_array_key]['y'] += (int)$array_column_user_data[$role_id];;
							}
						}
						$data[$response_array_key]['drilldown'] = $department_name['name'];
					}

					$sort_array_column = array_column($data, 'y');
					arsort($sort_array_column);
					foreach ($sort_array_column as $sort_array_column_key => $sort_array_column_value) {
						
						$response['graph_data'][] = $data[$sort_array_column_key]; 						
					}

					break;		
				
				default:
					$response['status'] = 'failed';	
					$response['message'] = "call type not found";
					break;
			}
			echo json_encode($response);

		} else{
			die('access is not allowed to this function');
		}
	}

	// private function
	private function create_user_details($where, $limit, $offset) {

		$data = array();
		$data = $this->user_model->get_user_details($where, $limit, $offset);
		$role = $this->user_model->get_dynamic_data('*', array(), 'role');
		$data['role'] = array_column($role, 'role_name', 'role_id');
		$data['call_type'] = 'user_list'; 
		$data['paggination_data']['limit'] = $limit;
		$data['paggination_data']['offset'] = $offset;
		return $data;
	}

	private function update_session_data_for_side_bar($user_id) {

		// $sidebar_session_data = array();
		// $sidebar_session_data['main_module_access'] = $this->db->get_where('user_to_module', array('user_id'=>$user_id, 'status' => 'Active'))->result_array();
		// $sidebar_session_data['sub_module_access'] = $this->db->get_where('user_to_sub_module', array('user_id'=>$user_id, 'status' => 'Active'))->result_array();
		// $this->session->set_userdata($sidebar_session_data);
	}
}