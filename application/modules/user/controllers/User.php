<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MX_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('user_id')){
			redirect('login', 'refresh');
			exit;
		}else{
			$access_count = 0;
			if(!empty($this->session->userdata('main_module_access')) && in_array(16, array_column($this->session->userdata('main_module_access'), 'module_id'))) {

				foreach ($this->session->userdata('sub_module_access') as $key => $value) {
					
					if($value['module_id'] == 16 && $value['status'] == 'Active') {
						$access_count++;
					}
				}
			}
			if($access_count == 0){
				// redirect($this->session->userdata('home/dashboard'));
				// exit;
			}
		}
		error_reporting(0);
		$this->load->model('user_model');
		$this->load->model('common/common_model');
	}

	public function index(){
		redirect('user/user_management');
	}

	public function user_management() {

		$data = array();
		$data = $this->create_user_details(array('status'=>1), 00, 00);
		//echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'User Management'));
		$this->load->view('sidebar', array('title' => 'User Management'));
		$this->load->view('user/user_dashboard', $data);
		$this->load->view('footer');
	}
	
	public function exclude_listingz(){

        $where_string = "users.status = 1 AND users.user_id NOT IN (SELECT user_id FROM login_verification_ignore WHERE status = 'Active')";
		$data = $this->prepare_user_list_data($where_string, 10, 0, 'sms_list');

		// echo "<pre>";print_r($data);echo"</pre><hr>";die;
        $this->load->view('header', array('title' => 'User Exclude Listingz'));
        $this->load->view('sidebar', array('title' => 'User Exclude Listingz'));
        $this->load->view('user/exclude_login_list',$data);
        $this->load->view('footer');
    }

    public function learning_video(){

    	$data = array();
	    $data['user_data']=$this->user_model->get_dynamic_data('*', array('status'=>'Inactive','user_id'=>$this->session->userdata('user_id')),'people_information','row_array');
	    if(empty($data['user_data'])) {

	    	$data['user_data'] = array('profile_pic_file_path'=>'', 'first_name'=>'', 'last_name'=>'', 'department'=>'Not Available', 'official_email'=>'Not Available', 'official_number'=>'Not Available', 'job_location'=>'Not Available');
	    }
	    $user_video_access_details = $this->user_model->get_dynamic_data('*', array('user_id'=>$this->session->userdata('user_id'), 'status'=> 'Active'), 'video_url_access_details','row_array');
	    if(!empty($user_video_access_details)) {

		    $video_access=json_decode($user_video_access_details['video_id'],true);
		    foreach($video_access as $single_data){

				$module_name[]=$this->user_model->get_dynamic_data('module_name', array('id'=>$single_data, 'status'=> 'Active'), 'video_url_details','row_array');
			}
		    $data['module_name']=array_unique(array_column($module_name,'module_name'));
	    }
	    // echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Video list'));
		$this->load->view('sidebar', array('title' => 'Video list'));
		$this->load->view('user/learning_video', $data);
		$this->load->view('footer');
	}

	public function asset_list(){
       
        $data=$this->asset_data(array('status' =>'Active'));
        // echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Asset list'));
		$this->load->view('sidebar', array('title' => 'Asset list'));
		$this->load->view('user/asset_list', $data);
		$this->load->view('footer');
	}

	public function ajax_function() {

		if($this->input->is_ajax_request()){
			
			$call_type = $this->input->post('call_type');
			$post_details = $this->input->post();
			$response['status'] = 'successful';	
			switch ($call_type) {

				case 'get_single_user_details':
					
					$user_id = $this->input->post('user_id');
					$role = $this->user_model->get_dynamic_data('*', array(), 'role');
					$data['role'] = array_column($role, 'role_name', 'role_id');
					$data['single_user_details'] = $this->user_model->get_dynamic_data('*', array('user_id'=>$user_id), 'users', 'row_array');
					$data['modules'] = $this->user_model->get_dynamic_data('*', array('status'=>'active'), 'modules');
					$module_access = $this->user_model->get_dynamic_data('*', array('user_id'=>$user_id, 'status'=> 'Active'), 'user_to_module');
					$data['module_access'] = array_unique(array_column($module_access, 'module_id', 'master_id'));					
					$sub_module = $this->user_model->get_dynamic_data('*', array('status'=>'Active'), 'sub_module');
					foreach ($sub_module as $sub_module_value) {
						
						$data['sub_module'][$sub_module_value['module_id']][] = array( 'sub_module_id' => $sub_module_value['sub_module_id'], 'sub_module_name' => $sub_module_value['sub_module_name']);
					}
					$sub_module_access = $this->user_model->get_dynamic_data('*', array('status'=>'Active', 'user_id'=> $user_id), 'user_to_sub_module');

					// echo "<pre>";print_r($sub_module_access);echo"</pre><hr>";
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;

					$data['sub_module_access'] = array_column($sub_module_access, 'sub_module_id', 'id');
					$response['html_body'] =  $this->load->view('user/single_user_details', $data, TRUE);
				break;

				case 'update_user_details':

					$user_id = (int)$this->input->post('user_id');
					
					// sub module code
						$sub_module_access = $this->user_model->get_dynamic_data('*', array('user_id'=>$user_id), 'user_to_sub_module');
						$sub_module_access_data = array();
						foreach ($sub_module_access as $sub_module_access_value) {
							$sub_module_access_data[$sub_module_access_value['sub_module_id']] = $sub_module_access_value;
						}
						$sub_module_status =  array();
						$sub_module_status[] = array('module_name' => 'dashboard_home', 'module_id' => 1, 'sub_module_id' => 1);
						$sub_module_status[] = array('module_name' => 'quotation_add', 'module_id' => 2, 'sub_module_id' => 2);
						$sub_module_status[] = array('module_name' => 'quotation_list', 'module_id' => 2, 'sub_module_id' => 3);
						$sub_module_status[] = array('module_name' => 'quotation_draft_list', 'module_id' => 2, 'sub_module_id' => 4);
						$sub_module_status[] = array('module_name' => 'quotation_follow_list', 'module_id' => 2, 'sub_module_id' => 5);
						$sub_module_status[] = array('module_name' => 'quotation_proforma_list', 'module_id' => 2, 'sub_module_id' => 6);
						$sub_module_status[] = array('module_name' => 'invoice_add', 'module_id' => 3, 'sub_module_id' => 7);
						$sub_module_status[] = array('module_name' => 'invoice_list', 'module_id' => 3, 'sub_module_id' => 8);
						$sub_module_status[] = array('module_name' => 'client_add', 'module_id' => 4, 'sub_module_id' => 9);
						$sub_module_status[] = array('module_name' => 'client_list', 'module_id' => 4, 'sub_module_id' => 10);
						$sub_module_status[] = array('module_name' => 'client_member_list', 'module_id' => 4, 'sub_module_id' => 11);
						$sub_module_status[] = array('module_name' => 'client_add_new', 'module_id' => 4, 'sub_module_id' => 90);
						$sub_module_status[] = array('module_name' => 'piping_1', 'module_id' => 5, 'sub_module_id' => 12);
						$sub_module_status[] = array('module_name' => 'piping_2', 'module_id' => 5, 'sub_module_id' => 13);
						$sub_module_status[] = array('module_name' => 'piping_3', 'module_id' => 5, 'sub_module_id' => 14);
						$sub_module_status[] = array('module_name' => 'piping_4', 'module_id' => 5, 'sub_module_id' => 15);
						$sub_module_status[] = array('module_name' => 'piping_5', 'module_id' => 5, 'sub_module_id' => 16);
						$sub_module_status[] = array('module_name' => 'piping_6', 'module_id' => 5, 'sub_module_id' => 17);
						$sub_module_status[] = array('module_name' => 'instrument_1', 'module_id' => 6, 'sub_module_id' => 18);
						$sub_module_status[] = array('module_name' => 'instrument_2', 'module_id' => 6, 'sub_module_id' => 19);
						$sub_module_status[] = array('module_name' => 'instrument_3', 'module_id' => 6, 'sub_module_id' => 20);
						$sub_module_status[] = array('module_name' => 'instrument_4', 'module_id' => 6, 'sub_module_id' => 21);
						$sub_module_status[] = array('module_name' => 'instrument_5', 'module_id' => 6, 'sub_module_id' => 22);
						$sub_module_status[] = array('module_name' => 'others_1', 'module_id' => 7, 'sub_module_id' => 23);
						$sub_module_status[] = array('module_name' => 'others_2', 'module_id' => 7, 'sub_module_id' => 24);
						$sub_module_status[] = array('module_name' => 'pq_add', 'module_id' => 8, 'sub_module_id' => 25);
						$sub_module_status[] = array('module_name' => 'pq_pending_list', 'module_id' => 8, 'sub_module_id' => 26);
						$sub_module_status[] = array('module_name' => 'pq_approved_list', 'module_id' => 8, 'sub_module_id' => 27);
						$sub_module_status[] = array('module_name' => 'procurement_add', 'module_id' => 9, 'sub_module_id' => 28);
						$sub_module_status[] = array('module_name' => 'procurement_list', 'module_id' => 9, 'sub_module_id' => 29);
						$sub_module_status[] = array('module_name' => 'quality_add', 'module_id' => 10, 'sub_module_id' => 30);
						$sub_module_status[] = array('module_name' => 'quality_list', 'module_id' => 10, 'sub_module_id' => 31);
						$sub_module_status[] = array('module_name' => 'marking_add', 'module_id' => 10, 'sub_module_id' => 32);
						$sub_module_status[] = array('module_name' => 'marking_proforma_list', 'module_id' => 10, 'sub_module_id' => 33);
						$sub_module_status[] = array('module_name' => 'marking_sample_list', 'module_id' => 10, 'sub_module_id' => 34);
						$sub_module_status[] = array('module_name' => 'task_list', 'module_id' => 11, 'sub_module_id' => 35);
						$sub_module_status[] = array('module_name' => 'vendors_add', 'module_id' => 12, 'sub_module_id' => 36);
						$sub_module_status[] = array('module_name' => 'vendors_list', 'module_id' => 12, 'sub_module_id' => 37);
						$sub_module_status[] = array('module_name' => 'reports_touch_point_list', 'module_id' => 13, 'sub_module_id' => 38);
						$sub_module_status[] = array('module_name' => 'reports_daily_task_list', 'module_id' => 13, 'sub_module_id' => 39);
						$sub_module_status[] = array('module_name' => 'reports_daily_report', 'module_id' => 13, 'sub_module_id' => 47);
						$sub_module_status[] = array('module_name' => 'queries_sales_open', 'module_id' => 14, 'sub_module_id' => 40);
						$sub_module_status[] = array('module_name' => 'queries_sales_closed', 'module_id' => 14, 'sub_module_id' => 41);
						$sub_module_status[] = array('module_name' => 'queries_proforma_open', 'module_id' => 14, 'sub_module_id' => 42);
						$sub_module_status[] = array('module_name' => 'queries_proforma_closed', 'module_id' => 14, 'sub_module_id' => 43);
						$sub_module_status[] = array('module_name' => 'queries_purchase_open', 'module_id' => 14, 'sub_module_id' => 44);
						$sub_module_status[] = array('module_name' => 'queries_purchase_closed', 'module_id' => 14, 'sub_module_id' => 45);
						$sub_module_status[] = array('module_name' => 'setting_currency', 'module_id' => 15, 'sub_module_id' => 46);
						$sub_module_status[] = array('module_name' => 'user_management', 'module_id' => 16, 'sub_module_id' => 48);
						$sub_module_status[] = array('module_name' => 'hr_add_employee', 'module_id' => 17, 'sub_module_id' => 49);
						$sub_module_status[] = array('module_name' => 'primary_internal_data', 'module_id' => 18, 'sub_module_id' => 50);
						$sub_module_status[] = array('module_name' => 'hr_employee_list', 'module_id' => 17, 'sub_module_id' => 51);
						$sub_module_status[] = array('module_name' => 'search_engine_primary_data', 'module_id' => 19, 'sub_module_id' => 52);
						$sub_module_status[] = array('module_name' => 'setting_vendor_management', 'module_id' => 15, 'sub_module_id' => 53);
						$sub_module_status[] = array('module_name' => 'procurement_listingz', 'module_id' => 9, 'sub_module_id' => 54);
						$sub_module_status[] = array('module_name' => 'others_3', 'module_id' => 7, 'sub_module_id' => 55);
						$sub_module_status[] = array('module_name' => 'production_status', 'module_id' => 20, 'sub_module_id' => 56);
						$sub_module_status[] = array('module_name' => 'document_inspection', 'module_id' => 10, 'sub_module_id' => 57);
						$sub_module_status[] = array('module_name' => 'invoice_export_list', 'module_id' => 3, 'sub_module_id' => 58);
						$sub_module_status[] = array('module_name' => 'queries_list', 'module_id' => 14, 'sub_module_id' => 59);
						$sub_module_status[] = array('module_name' => 'user_login_manage', 'module_id' => 16, 'sub_module_id' => 60);
						$sub_module_status[] = array('module_name' => 'search_engine_master_view', 'module_id' => 19, 'sub_module_id' => 61);
						$sub_module_status[] = array('module_name' => 'others_miscellaneous', 'module_id' => 7, 'sub_module_id' => 62);
						$sub_module_status[] = array('module_name' => 'user_learning_video', 'module_id' => 16, 'sub_module_id' => 63);
						$sub_module_status[] = array('module_name' => 'dashboard_graph', 'module_id' => 1, 'sub_module_id' => 64);
						// $sub_module_status[] = array('module_name' => 'ticket_management_list', 'module_id' => 21, 'sub_module_id' => 65);
						$sub_module_status[] = array('module_name' => 'ticket_management_master', 'module_id' => 21, 'sub_module_id' => 66);
						$sub_module_status[] = array('module_name' => 'ticket_management_it_list', 'module_id' => 21, 'sub_module_id' => 67);
						$sub_module_status[] = array('module_name' => 'ticket_management_crm_list', 'module_id' => 21, 'sub_module_id' => 68);
						$sub_module_status[] = array('module_name' => 'ticket_management_admin_list', 'module_id' => 21, 'sub_module_id' => 69);
						$sub_module_status[] = array('module_name' => 'manage_sales_person_daily_report', 'module_id' => 22, 'sub_module_id' => 70);
						$sub_module_status[] = array('module_name' => 'manage_sales_person_daily_report_protocol', 'module_id' => 22, 'sub_module_id' => 71);
						$sub_module_status[] = array('module_name' => 'new_piping_1', 'module_id' => 24, 'sub_module_id' => 73);
						$sub_module_status[] = array('module_name' => 'new_piping_2', 'module_id' => 24, 'sub_module_id' => 74);
						$sub_module_status[] = array('module_name' => 'new_piping_3', 'module_id' => 24, 'sub_module_id' => 75);
						$sub_module_status[] = array('module_name' => 'new_piping_4', 'module_id' => 24, 'sub_module_id' => 76);
						$sub_module_status[] = array('module_name' => 'new_piping_5', 'module_id' => 24, 'sub_module_id' => 77);
						$sub_module_status[] = array('module_name' => 'new_piping_6', 'module_id' => 24, 'sub_module_id' => 78);
						$sub_module_status[] = array('module_name' => 'new_instrument_1', 'module_id' => 25, 'sub_module_id' => 79);
						$sub_module_status[] = array('module_name' => 'new_instrument_2', 'module_id' => 25, 'sub_module_id' => 80);
						$sub_module_status[] = array('module_name' => 'new_instrument_3', 'module_id' => 25, 'sub_module_id' => 81);
						$sub_module_status[] = array('module_name' => 'new_instrument_4', 'module_id' => 25, 'sub_module_id' => 82);
						$sub_module_status[] = array('module_name' => 'new_instrument_5', 'module_id' => 25, 'sub_module_id' => 83);
						$sub_module_status[] = array('module_name' => 'new_others_1', 'module_id' => 26, 'sub_module_id' => 84);
						$sub_module_status[] = array('module_name' => 'new_others_2', 'module_id' => 26, 'sub_module_id' => 85);
						$sub_module_status[] = array('module_name' => 'new_others_3', 'module_id' => 26, 'sub_module_id' => 86);
						$sub_module_status[] = array('module_name' => 'new_others_4', 'module_id' => 26, 'sub_module_id' => 87);
						$sub_module_status[] = array('module_name' => 'pre_qualification_1', 'module_id' => 27, 'sub_module_id' => 88);
						$sub_module_status[] = array('module_name' => 'pre_qualification_2', 'module_id' => 27, 'sub_module_id' => 89);
						$sub_module_status[] = array('module_name' => 'lead_management_list', 'module_id' => 28, 'sub_module_id' => 91);
						$sub_module_status[] = array('module_name' => 'quotation_management_list', 'module_id' => 29, 'sub_module_id' => 92);
						$sub_module_status[] = array('module_name' => 'mrf_list', 'module_id' => 17, 'sub_module_id' => 95);
						$sub_module_status[] = array('module_name' => 'candidate_list', 'module_id' => 17, 'sub_module_id' => 96);

						$sub_module_insert_data = $sub_module_update_data = array();
						foreach ($sub_module_status as $key => $value) {
							
							if(!empty($sub_module_access_data[$value['sub_module_id']])){

								$sub_module_update_data[$key]['id'] = $sub_module_access_data[$value['sub_module_id']]['id'];
								$sub_module_update_data[$key]['status'] = 'Inactive';
								if($this->input->post($value['module_name']) == 'Yes') {

									$sub_module_update_data[$key]['status'] = 'Active';
								}
							} else {

								$sub_module_insert_data[$key]['user_id'] = $user_id;
								$sub_module_insert_data[$key]['module_id'] = $value['module_id'];
								$sub_module_insert_data[$key]['sub_module_id'] = $value['sub_module_id'];
								$sub_module_insert_data[$key]['status'] = 'Inactive';
								if($this->input->post($value['module_name']) == 'Yes') {

									$sub_module_insert_data[$key]['status'] = 'Active';
								}
							}
						}
						if(!empty($sub_module_update_data)) {

							$this->user_model->update_data_batch('user_to_sub_module',$sub_module_update_data, 'id');
						}
						if(!empty($sub_module_insert_data)) {

							$this->user_model->insert_data_batch('user_to_sub_module',$sub_module_insert_data);
						}
					// sub module code end	
					
					// main module action code
						$module_access = $this->user_model->get_dynamic_data('*', array('user_id'=>$user_id), 'user_to_module');
						$module_access_data = array();
						foreach ($module_access as $module_access_key => $module_access_value) {
							$module_access_data[$module_access_value['module_id']] = $module_access_value;
						}
						$main_module_status =  array();
						$main_module_status[] = array('module_name' => 'dashboard_module_status', 'module_id' => 1);
						$main_module_status[] = array('module_name' => 'quotation_module_status', 'module_id' => 2);
						$main_module_status[] = array('module_name' => 'invoice_module_status', 'module_id' => 3);
						$main_module_status[] = array('module_name' => 'client_module_status', 'module_id' => 4);
						$main_module_status[] = array('module_name' => 'piping_module_status', 'module_id' => 5);
						$main_module_status[] = array('module_name' => 'instrument_module_status', 'module_id' => 6);
						$main_module_status[] = array('module_name' => 'others_module_status', 'module_id' => 7);
						$main_module_status[] = array('module_name' => 'pq_module_status', 'module_id' => 8);
						$main_module_status[] = array('module_name' => 'procurement_module_status', 'module_id' => 9);
						$main_module_status[] = array('module_name' => 'quality_module_status', 'module_id' => 10);
						$main_module_status[] = array('module_name' => 'task_module_status', 'module_id' => 11);
						$main_module_status[] = array('module_name' => 'vendors_module_status', 'module_id' => 12);
						$main_module_status[] = array('module_name' => 'reports_module_status', 'module_id' => 13);
						$main_module_status[] = array('module_name' => 'queries_module_status', 'module_id' => 14);
						$main_module_status[] = array('module_name' => 'setting_module_status', 'module_id' => 15);
						$main_module_status[] = array('module_name' => 'user_module_status', 'module_id' => 16);
						$main_module_status[] = array('module_name' => 'hr_module_status', 'module_id' => 17);
						$main_module_status[] = array('module_name' => 'internal_module_status', 'module_id' => 18);
						$main_module_status[] = array('module_name' => 'search_engine_module_status', 'module_id' => 19);
						$main_module_status[] = array('module_name' => 'production_module', 'module_id' => 20);
						$main_module_status[] = array('module_name' => 'ticket_management_module', 'module_id' => 21);
						$main_module_status[] = array('module_name' => 'manage_sales_person_module', 'module_id' => 22);
						$main_module_status[] = array('module_name' => 'new_piping_module_status', 'module_id' => 24);
						$main_module_status[] = array('module_name' => 'new_instrument_module_status', 'module_id' => 25);
						$main_module_status[] = array('module_name' => 'new_others_module_status', 'module_id' => 26);
						$main_module_status[] = array('module_name' => 'new_pre_qualification', 'module_id' => 27);
						$main_module_status[] = array('module_name' => 'lead_management', 'module_id' => 28);
						$main_module_status[] = array('module_name' => 'quotation_management', 'module_id' => 29);
						$main_module_update_data = $main_module_insert_data = array();
						foreach ($main_module_status as $key => $value) {

							if(!empty($module_access_data[$value['module_id']])){

								$main_module_update_data[$key]['master_id'] = $module_access_data[$value['module_id']]['master_id'];
								$main_module_update_data[$key]['status'] = 'Inactive';
								if($this->input->post($value['module_name']) == 1) {

									$main_module_update_data[$key]['status'] = 'Active';
								}
							} else {

								$main_module_insert_data[$key]['user_id'] = $user_id;
								$main_module_insert_data[$key]['module_id'] = $value['module_id'];
								$main_module_insert_data[$key]['status'] = 'Inactive';
								if($this->input->post($value['module_name']) == 1) {

									$main_module_insert_data[$key]['status'] = 'Active';
								}
							}
						}
						if(!empty($main_module_update_data)) {

							$this->user_model->update_data_batch('user_to_module',$main_module_update_data, 'master_id');
						}
						if(!empty($main_module_insert_data)) {

							$this->user_model->insert_data_batch('user_to_module',$main_module_insert_data);
						}
					// main module code end	
					if(!empty($this->input->post('employee_name'))) {

						$update_data['name'] = $this->input->post('employee_name');
					}
					if(!empty($this->input->post('email'))) {

						$update_data['email'] = $this->input->post('email');
					}
					if(!empty($this->input->post('username'))) {

						$update_data['username'] = $this->input->post('username');
					}
					if(!empty($this->input->post('password'))) {

						$update_data['password'] = md5($this->input->post('password'));
					}
					if(!empty($this->input->post('mobile'))) {

						$update_data['mobile'] = $this->input->post('mobile');
					}
					if(!empty($this->input->post('role'))) {

						$update_data['role'] = $this->input->post('role');
					}
					$update_data['status'] = $this->input->post('status');
					if($update_data['status'] == 1) {

						$this->user_model->update_data('people_information', array('status'=>'Active'), array('user_id'=> $user_id));
					} else {

						$this->user_model->update_data('people_information', array('status'=>'Inactive'), array('user_id'=> $user_id));
						$this->user_model->update_data('user_organization_chart', array('status'=>'Inactive'), array('user_id'=> $user_id));
					}

					$this->user_model->update_data('users', $update_data, array('user_id'=> $user_id));
					$this->update_session_data_for_side_bar($user_id);
				break;

				case 'paggination_data_for_user':
					
					$where_array = array();
					$limit = 100;
					$offset = 00;
					if(!empty($this->input->post('search_user_name'))) {
						$where_array['name LIKE '] = '%'.$this->input->post('search_user_name').'%';
					}
					if(!empty($this->input->post('search_role'))) {
						$where_array['role'] = $this->input->post('search_role');
					}
					$where_array['status'] = $this->input->post('search_status');
					if(!empty($this->input->post('limit'))) {
						$limit = $this->input->post('limit');
					}
					if(!empty($this->input->post('offset'))) {
						$offset = $this->input->post('offset');
					}
					$data = $this->create_user_details($where_array, $limit, $offset);	
					$response['html_body'] = $this->load->view('user/user_list_body', $data, TRUE);
					$response['html_paggination'] = $this->load->view('user/paggination', $data, TRUE);
				break;

				case 'get_module_details':
					
					$data = array();
					$data['main_module'] = $this->user_model->get_dynamic_data('*', array('status'=>'Active'), 'modules');
					$sub_module = $this->user_model->get_dynamic_data('*', array('status'=>'Active'), 'sub_module');
					$data['sub_module'] = array();
					foreach ($sub_module as $sub_module_value) {
						
						$data['sub_module'][$sub_module_value['module_id']][] = array('sub_module_id' => $sub_module_value['sub_module_id'], 'sub_module_name' => $sub_module_value['sub_module_name']); 
					}
					$response['html_body'] =  $this->load->view('user/add_module_access', $data, TRUE);
				break;

				case 'add_main_module_name':

					$module_name = $this->input->post('module_name');
					$insert_result = $this->user_model->insert_data('modules', array('module_name'=>$module_name, 'module_controller'=>$module_name));
					if(empty($insert_result)){
						$response['status'] = 'failed';	
					}
				break;

				case 'add_sub_module_name':

					$module_id = $this->input->post('main_module_id');
					$sub_module_name = $this->input->post('sub_module_name');
					$insert_result = $this->user_model->insert_data('sub_module', array('module_id'=>$module_id, 'sub_module_name'=>$sub_module_name));
					if(empty($insert_result)){
						$response['status'] = 'failed';	
					}
				break;
						
				case 'update_main_module_name':

					$module_id = $this->input->post('module_id');
					$module_name = $this->input->post('module_name');
					$update_status = $this->user_model->update_data('modules',array('module_name'=>$module_name),array('module_id'=>$module_id));
					if(empty($update_status)){
						$response['status'] = 'failed';	
					}
				break;		

				case 'update_sub_module_name':
					
					$sub_module_id = $this->input->post('sub_module_id');
					$sub_module_name = $this->input->post('sub_module_name');
					$update_status = $this->user_model->update_data('sub_module',array('sub_module_name'=>$sub_module_name),array('sub_module_id'=>$sub_module_id));
					if(empty($update_status)){
						$response['status'] = 'failed';	
					}
				break;	
					
				case 'delete_main_module_name':
					
					$module_id = $this->input->post('module_id');
					$update_status_module = $this->user_model->update_data('modules',array('status'=>'Inactive'),array('module_id'=>$module_id));
					$update_status_sub_module = $this->user_model->update_data('sub_module',array('status'=>'Inactive'),array('module_id'=>$module_id));
					if(empty($update_status_module) || empty($update_status_sub_module)){
						$response['status'] = 'failed';	
					}
				break;

				case 'delete_sub_module_name':
					
					$sub_module_id = $this->input->post('sub_module_id');
					$update_status = $this->user_model->update_data('sub_module',array('status'=>'Inactive'),array('sub_module_id'=>$sub_module_id));
					if(empty($update_status)){
						$response['status'] = 'failed';	
					}
				break;	

				case 'get_add_user_details':

					$data['role'] = $this->user_model->get_dynamic_data('*', array(), 'role');
					$response['html_body'] =  $this->load->view('user/add_user_form', $data, TRUE);
				break;	

				case 'add_user':
					
					$insert_data['name'] = $this->input->post('employee_name');
					$insert_data['email'] = $this->input->post('email');
					$insert_data['username'] = $this->input->post('username');
					$insert_data['password'] = md5($this->input->post('password'));
					$insert_data['mobile'] = $this->input->post('mobile');
					$insert_data['role'] = $this->input->post('role');
					$insert_data['entered_on'] = date('Y-m-d H:i:s');
					$insert_data['status'] = 1;
					$insert_result['user_id'] = $this->user_model->insert_data('users', $insert_data);

					if(empty($insert_result)){
						$response['status'] = 'failed';	
					}else{
						$this->user_model->insert_data('manager_access_details', array('user_id'=>$insert_result['user_id']));
					}
				break;	

				case 'get_user_name':
					
					$user_details = $this->user_model->get_dynamic_data('*', array('status'=>1, 'role' => $this->input->post('role_id')), 'users');
					if(!empty($user_details)) {
						$response['html_body'] = '<option value="">Select User</option>';
						foreach ($user_details as $user_single_details) {
							
							$response['html_body'] = $response['html_body'] . '<option value="'.$user_single_details['user_id'].'">'.ucwords(strtolower($user_single_details['name'])).'</option>';
						}
					}
				break;

				// case 'delete_user_and_assign_to_other_user':
					
					// 	// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					// 	$deleted_user_id = $this->input->post('deleted_user_id');
					// 	$assigned_to_user_id = $this->input->post('assigned_to');
					// 	// changing deleted user assign quotation
					// 	// $this->user_model->update_data('quotation_mst',array('assigned_to' => $assigned_to_user_id), array('assigned_to' =>$deleted_user_id));
					// 	// changing deleted user assign procurement
					// 	// $this->user_model->update_data('pq_client',array('assigned_to' => $assigned_to_user_id), array('assigned_to' =>$deleted_user_id));
					// 	// changing deleted user assign rfq
					// 	// $this->user_model->update_data('rfq_mst',array('assigned_to' => $assigned_to_user_id), array('assigned_to' =>$deleted_user_id));
					// 	// changing deleted user assign mtc
					// 	// $this->user_model->update_data('mtc_mst',array('assigned_to' => $assigned_to_user_id), array('assigned_to' =>$deleted_user_id));
					// 	// changing deleted user Lead management
					// 	// $lead_source_array['hetro_leads'] = array( 
					// 	// 										array('source_name' => 'chemical companies', 'sub_module_id' => 14),
					// 	// 										array('source_name' => 'distributors', 'sub_module_id' => 20),
					// 	// 										array('source_name' => 'epc companies', 'sub_module_id' => 16),
					// 	// 										array('source_name' => 'heteregenous tubes india', 'sub_module_id' => 21),
					// 	// 										array('source_name' => 'pvf companies', 'sub_module_id' => 22),
					// 	// 										array('source_name' => 'shipyards', 'sub_module_id' => 13),
					// 	// 										array('source_name' => 'sugar companies', 'sub_module_id' => 17),
					// 	// 										array('source_name' => 'water companies', 'sub_module_id' => 15)
					// 	// 									);
					// 	// $lead_source_array['primary_leads'] = array(
					// 	// 										array('source_name' => 'pipes', 'sub_module_id' => 12),
					// 	// 										array('source_name' => 'tubes', 'sub_module_id' => 18),
					// 	// 										array('source_name' => 'process control', 'sub_module_id' => 19),
					// 	// 										array('source_name' => 'tubing', 'sub_module_id' => 23),
					// 	// 										array('source_name' => 'hammer union', 'sub_module_id' => 24)
					// 	// 									);
					// 	$get_lead_assign_data = $this->user_model->get_dynamic_data('*', array('assigned_to' => $deleted_user_id), 'clients');
					// 	// echo "<pre>";print_r($get_lead_assign_data);echo"</pre><hr>";exit;
					// 	if(!empty($get_lead_assign_data)) {
					// 		// $this->user_model->update_data('clients', array('assigned_to' => $assigned_to_user_id), array('source' => $value['source_name'], '	assigned_to' => $deleted_user_id));
							
					// 	}
					// 	$get_lead_assign_data = $this->user_model->get_dynamic_data_marketing_db('*', array('assigned_to' => $deleted_user_id), 'lead_mst');
					// 	// echo "<pre>";print_r($get_lead_assign_data);echo"</pre><hr>";exit;
					// 	if(!empty($get_lead_assign_data)){
					// 		foreach ($get_lead_assign_data as $assign_data) {
								
					// 			$this->user_model->update_data_sales_db('lead_mst', array('assigned_to' => $assigned_to_user_id), array('lead_mst_id' => $assign_data['lead_mst_id']));
					// 		}
					// 	}
				// break;

				case 'delete_user_and_assign_to_other_user':
					
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					$deleted_user_id = $this->input->post('deleted_user_id');
					$assigned_to_user_id = $this->input->post('assigned_to');
													
					$get_lead_assign_data = $this->user_model->get_dynamic_data('*', array('assigned_to' => $deleted_user_id), 'customer_mst');
					// echo "<pre>";print_r($get_lead_assign_data);echo"</pre><hr>";exit;
					
					if(!empty($get_lead_assign_data)){
						foreach ($get_lead_assign_data as $assign_data) {
							
							$this->user_model->update_data_sales_db('customer_mst', array('assigned_to' => $assigned_to_user_id), array('id' => $assign_data['id']));
						}
					}
				break;
						
				case 'get_department_graph_data':
					
					$data = array();
					$user_data = $this->user_model->get_all_conditional_data('role, count("*") as role_count', array('status'=>1), 'users', 'result_array', array(), array('column_name'=>'role', 'column_value'=>'ASC'), array('role'));
					$array_column_user_data = array_column($user_data, 'role_count', 'role');
					$response_array = array(
										array('name' => 'Admin','role_id' => '1'),
										array('name' => 'Analyst','role_id' => '2-3-17'),
										array('name' => 'Data Entry','role_id' => '4'),
										array('name' => 'Sales Team','role_id' => '5-16'),
										array('name' => 'Procurement Team','role_id' => '6-8'),
										array('name' => 'Pre Qualification','role_id' => '7'),
										array('name' => 'Quality Team','role_id' => '10-11'),
										array('name' => 'Accounts','role_id' => '12'),
										array('name' => 'HR','role_id' => '13'),
										array('name' => 'Document Losgistics','role_id' => '14'),
										array('name' => 'IT','role_id' => '15')
									);
					foreach ($response_array as $response_array_key => $department_name) {
						
						$data[$response_array_key]['name'] = $department_name['name'];
						$data[$response_array_key]['y'] = 0;
						// explode role id
						$explode_role_id = explode('-', $department_name['role_id']);
						foreach ($explode_role_id as $role_id) {
							
							if(!empty($array_column_user_data[$role_id])) {

								$data[$response_array_key]['y'] += (int)$array_column_user_data[$role_id];;
							}
						}
						$data[$response_array_key]['drilldown'] = $department_name['name'];
					}

					$sort_array_column = array_column($data, 'y');
					arsort($sort_array_column);
					foreach ($sort_array_column as $sort_array_column_key => $sort_array_column_value) {
						
						$response['graph_data'][] = $data[$sort_array_column_key]; 						
					}

				break;		
				
				case 'get_single_graph_user_details':
                    
                    $user_id = $this->input->post('user_id');
                    $role = $this->user_model->get_dynamic_data('*', array(), 'role');
                    $data['role'] = array_column($role, 'role_name', 'role_id');
                    $data['single_user_details'] = $this->user_model->get_dynamic_data('*', array('user_id'=>$user_id), 'users', 'row_array');
                    $data['graph_information'] = $this->user_model->get_dynamic_data('*', array('status'=>'active'), 'graph_information');
                    $graph_information_access = $this->user_model->get_dynamic_data('*', array('user_id'=>$user_id, 'status'=> 'Active'), 'users_to_graph');
                    $data['graph_information_access'] = array_column($graph_information_access, 'graph_information_id', 'id');
                    $response['html_body'] =  $this->load->view('user/single_user_graph_details', $data, true);
                break;

                case'update_graph_data':

                    $user_id = $this->input->post('user_id');
                    $form_data=array_column($this->input->post('update_form_data'),'value','name');
                    $check_graph_id_exist=array_column( $this->user_model->get_dynamic_data('graph_information_id',array('user_id'=> $user_id), 'users_to_graph'),'graph_information_id'); 

                    foreach($form_data as $key=>$value){
                     
                        if($value==1){
                            if(!in_array($key, $check_graph_id_exist)){
                               $insert_array[]=array('user_id'=>$user_id,'graph_information_id'=>$key);
                            }else {
                                
                                $this->user_model->update_data('users_to_graph',array('status' => 'Active'),array('graph_information_id'=>$key,'user_id'=>$user_id) );
                            }
                        }else if($value==0){

                            
                            $this->user_model->update_data('users_to_graph',array('status' => 'Inactive'),array('graph_information_id'=>$key,'user_id'=>$user_id) );
                        }
                    }
                    if(!empty($insert_array)) {
                        $this->user_model->insert_data_batch('users_to_graph',$insert_array);
                    }                
                break;

                case'delete_form_exclude':

	                $response['status'] = 'failed';
					if(!empty($this->input->post('delete_id'))){
	                	$this->common_model->update_data_sales_db('login_verification_ignore',array('status'=>'Inactive'), array('id'=>$this->input->post('delete_id')));
	                    $response['status'] = 'successful';	
	                }
				break;

				case'add_user_to_exclude':
				
	                $response['status'] = 'failed';
					if(!empty($this->input->post('user_id'))){
		                    $this->common_model->insert_data_sales_db('login_verification_ignore',array('user_id'=>$this->input->post('user_id')));
		                    $response['status'] = 'successful';	
                    } 
			    break;

			    case'get_single_video_user_details':
			    case'get_single_video_details':
			    	
                    $user_id = $this->input->post('user_id');
                    $role = $this->user_model->get_dynamic_data('*', array(), 'role');
                    $data['role'] = array_column($role, 'role_name', 'role_id');
                    $data['single_user_details'] = $this->user_model->get_dynamic_data('*', array('user_id'=>$user_id), 'users', 'row_array');
                    $data['video_list']= $this->user_model->get_dynamic_data('*', array('status'=>'active','module_name'=>$this->input->post('department_name')), 'video_url_details');
                     $data['department_name']=array_column($this->common_model->get_all_conditional_data_sales_db('module_name', array('status'=>'active'), 'video_url_details','result_array','','','module_name'),'module_name');
                    $video_access = $this->user_model->get_dynamic_data('*', array('user_id'=>$user_id, 'status'=> 'Active'), 'video_url_access_details','row_array');
                    $data['video_access']=json_decode($video_access['video_id'],true);
                    if($call_type =='get_single_video_details'){
	                    $response['html_body'] =  $this->load->view('user/video_body', $data, true);
	                }else if($call_type =='get_single_video_user_details'){
	                    $response['html_body'] =  $this->load->view('user/user_to_video', $data, true);
	                }

			    break;

			    case'update_video_data':

                    $user_id = $this->input->post('user_id');
                    $form_data=array_column($this->input->post('update_form_data'),'value','name');
                    $check_video_id_exist=array_column($this->user_model->get_dynamic_data('video_id',array('user_id'=> $user_id), 'video_url_access_details'),'video_id');
                    $video_id = array();
	                if(!empty($form_data)){
	                    $video_id = array();
	                    foreach($form_data as $key=>$value){
	                        if($value==1){
	                    	    $video_id[]=$key;
	                        }
	                    }
                      	$insert_video_id=json_encode($video_id);
                       	if(!empty($check_video_id_exist)){
                    	
                    	    $this->user_model->update_data('video_url_access_details',array('video_id'=>$insert_video_id),array('user_id'=> $user_id,'status'=>'Active') );

                       	}else{
                            
                            $this->user_model->insert_data('video_url_access_details',array('user_id'=> $user_id,'  video_id'=>$insert_video_id) );

                        }
	                }
                break;

                case 'video_to_user':       
	                $data['video_details']= array();
	                $data['video_details']=$this->user_model->get_dynamic_data('*', array('status'=>'active','module_name'=>$this->input->post('tab_name') ),'video_url_details');
				    $video_access = $this->user_model->get_dynamic_data('*', array('user_id'=>$this->session->userdata('user_id'), 'status'=> 'Active'), 'video_url_access_details','row_array');
				    $video_access=json_decode($video_access['video_id'],true);
				    $data['video_access_details']=$video_access;
					$response['html_body'] = $this->load->view('user/video_list_body', $data, true);
					
                break;

                // case 'save_asset_details':
					//     if(!empty($this->input->post('form_data'))){

					//         foreach($this->input->post('form_data') as $single_assert_data){
							
					// 			if($single_assert_data['name']=='computer' && $single_assert_data['value']="Yes"){

					// 				$single_assert_data['name']="type";
					//             	$single_assert_data['value']="Computer";
					//             }
					// 			else if($single_assert_data['name']=='laptop' && $single_assert_data['value']="Yes"){
					//                 $single_assert_data['name']="type";
					//             	$single_assert_data['value']="Laptop";

					//             }
					//            if(!empty($single_assert_data['name']=='id')){
					// 				$update_id=$single_assert_data['value'];
					//             }
					//          	if($single_assert_data['value']=='on'){ $single_assert_data['value']="Yes";}
					//          	else if($single_assert_data['name']=='assign_date' ||  $single_assert_data['name']=='return_date'){
					//          		if(!empty($single_assert_data['value'])){
					//              		$single_assert_data['value']=date('Y-m-d',strtotime($single_assert_data['value']));
					//              	}
					//                 else if(empty($single_assert_data['value'])){ 
					//                     $single_assert_data['value']= date('Y-m-d');
					// 	         	}
					//             }
					//             $insert_array[$single_assert_data['name']]=$single_assert_data['value'];   
					//         }
					// 		if(!empty($update_id)){
					// 			unset($insert_array['id']);
								
					// 			$this->user_model->update_data('user_to_assets',$insert_array,array('id'=>$update_id));
					// 		}else{ 
							
					// 			$this->user_model->insert_data('user_to_assets',$insert_array);
					// 		}
					// 	}
					// break;

					// case 'open_model':
					//     $where_array=array('id'=>$this->input->post('id'),'status'=> 'Active');
					//     $data=$this->asset_data($where_array);
					//    // echo "<pre>";print_r($data);echo"</pre><hr>";exit;
					//     $response['html_body'] = $this->load->view('user/asset_edit_model',$data, true);
                // break;

				// case 'asset_search_filter_and_paggination':
					
					// 	if(!empty($this->input->post('form_data'))){
							
					// 		foreach($this->input->post('form_data') as $single_data){
								
					// 			if(!empty($single_data['value'])){
					//                 $where_array[$single_data['name']]=$single_data['value'];   
					//             }
					// 			$where_array['status']='Active';       
					//         }

					//         $data = $this->asset_data($where_array, $this->input->post('limit'), $this->input->post('offset'));
					//         $response['table_search_filter'] = $this->load->view('user/asset_search_filter_form', $data, true);
					// 		$response['table_body'] = $this->load->view('user/asset_table_body',$data, true);
					// 		$response['table_paggination'] = $this->load->view('common/common_paggination', $data, true);	
					//     }
				// break;

				// case 'delete_asset':
					// 	if(!empty($this->input->post('delete_id'))){
					// 		$this->common_model->update_data_sales_db('user_to_assets',array('status'=>'Inactive'), array('id'=>$this->input->post('delete_id')));
					// 	}
				// break;

				case 'get_search_filter_data':

					$data['user_list'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=> 1),'users');
					$data['designation_list'] = $this->common_model->get_dynamic_data_sales_db('*', array(),'role');

					$data['search_filter'] = array('user_id'=>'', 'designation_id'=>'');
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
					$response['search_filter_data'] = $this->load->view('user/search_filter_form', $data, true);
				break;

				case 'get_user_list_body':
					if(!empty($this->input->post('tab_name'))){

						$where_response = $this->create_where_response_on_tab_name($this->input->post('search_form_data'), $this->input->post('tab_name'));
						$data = $this->prepare_user_list_data($where_response['where'], $this->input->post('limit'), $this->input->post('offset'), $this->input->post('tab_name'));

						$data['search_filter'] = $where_response['search_filter'];
						$data['user_list'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=> 1),'users');
						$data['designation_list'] = $this->common_model->get_dynamic_data_sales_db('*', array(),'role');
					}
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
					$response['list_body'] = $this->load->view('user/exclude_list_body', $data, true);
					$response['search_filter_data'] = $this->load->view('user/search_filter_form', $data, true);
					$response['paggination'] = $this->load->view('user/user_paggination', $data, true);
				break;

				case 'delete_user':
				    // echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					if(!empty($this->input->post('user_id')) && !empty($this->input->post('tab_name'))){

						if($this->input->post('tab_name') == "sms_list"){

							$this->common_model->update_data_sales_db('users',array('status'=>0), array('user_id'=>$this->input->post('user_id')));
						}else{

							$this->common_model->update_data_sales_db('login_verification_ignore',array('status'=>'Inactive'), array('id'=>$this->input->post('user_id')));
						}
					}
				break;

				case 'change_login_type':

					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					if(!empty($this->input->post('user_id')) && !empty($this->input->post('type'))){

						if($this->input->post('type') == 'sms'){

							$user_details = $this->common_model->get_dynamic_data_sales_db('*', array('user_id'=>$this->input->post('user_id')),'login_verification_ignore', 'row_array');
							if(!empty($user_details)){

								$this->user_model->deleteData('login_verification_ignore',array('id'=>$user_details['id']));
							}
						}else{

							$form_data['user_id'] = $this->input->post('user_id');
							$form_data['login_type'] = $this->input->post('type');
							$form_data['status'] = 'Active';

							$user_details = $this->common_model->get_dynamic_data_sales_db('*', array('user_id'=>$this->input->post('user_id')),'login_verification_ignore', 'row_array');
							// echo "<pre>";print_r($form_data);echo"</pre><hr>";exit;
							if(empty($user_details)){

								$this->common_model->insert_data_sales_db('login_verification_ignore', $form_data);
							}else{

								$this->common_model->update_data_sales_db('login_verification_ignore',$form_data, array('id'=> $user_details['id']));
							}
						}
					}
				break;
				
				case 'get_module_details_access':
					
					$data = array(
						'single_user_module_details'=> array(
							'quotation'=> array(
								'name'=> 'Quotation Access',
								'access_details'=> array(
									'quotation_list_tab_access' => array(
										'label' => 'Tab',
										'name' => 'quotation_quotation_list_tab_access',
										'class' => 'col-xl-5',
										'input_type' => 'checkbox',
										'data' => array(
											'list'  => array(
														'name'=> 'list',
														'checked'=> '',
													),
											'draft' => array(
														'name'=> 'draft',
														'checked'=> '',
													),
											'follow_up' => array(
														'name'=> 'follow_up',
														'checked'=> '',
													),
											'proforma' => array(
														'name'=> 'proforma',
														'checked'=> '',
													),
										),
									),
									'quotation_sales_user_id' => array(
										'label' => 'Sales Person',
                                        'name' => 'quotation_quotation_sales_user_id',
										'class' => 'col-xl-6',
                                        'input_type' => 'dropdown',
										'data' => $this->change_key_as_per_value(
													$this->common_model->get_dynamic_data_sales_db_null_false(
														'user_id as value, name, "" as selected',
														"status = 1 and role IN(5, 16)",
														'users'
													)
												)
									),
									'quotation_procurement_user_id' => array(
										'label' => 'Procurement Person',
                                        'name' => 'quotation_quotation_procurement_user_id',
										'class' => 'col-xl-6',
                                        'input_type' => 'dropdown',
										'data' => $this->change_key_as_per_value(
													$this->common_model->get_dynamic_data_sales_db_null_false(
														'user_id as value, name, "" as selected',
														"status = 1 and role IN(6, 8)",
														'users'
													)
												)
									),
									'quotation_form_margin_and_unit_price_access' => array(
										'label' => 'Margin & Unit Price',
                                        'name' => 'quotation_quotation_form_margin_and_unit_price_access',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'quotation_pdf_client_name_access' => array(
										'label' => 'Pdf Client Name',
										'name' => 'quotation_quotation_pdf_client_name_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'quotation_pdf_line_item_amount_access' => array(
										'label' => 'Pdf Line Item Price',
										'name' => 'quotation_quotation_pdf_line_item_amount_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'quotation_pdf_total_amount_access' => array(
										'label' => 'Pdf Grand/Net Total',
										'name' => 'quotation_quotation_pdf_total_amount_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'quotation_pdf_before_pulish_line_item_amount_access' => array(
										'label' => 'Before Publish Pdf Line Item Price',
										'name' => 'quotation_quotation_pdf_before_pulish_line_item_amount_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'quotation_pdf_before_publish_total_amount_access' => array(
										'label' => 'Before Publish Pdf Grand/Net Total',
										'name' => 'quotation_quotation_pdf_before_publish_total_amount_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'quotation_form_stage_access' => array(
										'label' => 'Publish Access',
										'name' => 'quotation_quotation_form_stage_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'quotation_list_client_name_access' => array(
										'label' => 'Listing Client Name',
										'name' => 'quotation_quotation_list_client_name_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'quotation_list_value_access' => array(
										'label' => 'Listing Value',
										'name' => 'quotation_quotation_list_value_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'quotation_list_avg_margin_access' => array(
										'label' => 'Average Margin',
										'name' => 'quotation_quotation_list_avg_margin_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'quotation_list_action_view_quotation_details_access' => array(
										'label' => 'Listing Quotation Details',
										'name' => 'quotation_quotation_list_action_view_quotation_details_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'quotation_list_action_view_pdf_access' => array(
										'label' => 'Listing Pdf',
										'name' => 'quotation_quotation_list_action_view_pdf_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'quotation_list_action_edit_access' => array(
										'label' => 'Listing Edit',
										'name' => 'quotation_quotation_list_action_edit_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => '',
									),
									'quotation_list_action_delete_access' => array(
										'label' => 'Listing Delete',
										'name' => 'quotation_quotation_list_action_delete_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => '',
									),
									'quotation_list_action_follow_up_access' => array(
										'label' => 'Listing Follow Up',
										'name' => 'quotation_quotation_list_action_follow_up_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'quotation_list_action_query_access' => array(
										'label' => 'Listing Sales Query',
										'name' => 'quotation_quotation_list_action_query_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'quotation_list_action_graph_access' => array(
										'label' => 'Listing Graph',
										'name' => 'quotation_quotation_list_action_graph_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'quotation_list_action_rating_access' => array(
										'label' => 'Star Rating',
										'name' => 'quotation_quotation_list_action_rating_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'quotation_list_action_view_proforma_pdf_access' => array(
										'label' => 'Listing Proforma Invoice',
										'name' => 'quotation_quotation_list_action_view_proforma_pdf_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'quotation_list_action_purchase_order_add_access' => array(
										'label' => 'Listing Purchase Order Add',
										'name' => 'quotation_quotation_list_action_purchase_order_add_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'quotation_list_action_purchase_order_view_access' => array(
										'label' => 'Listing Purchase Order View',
										'name' => 'quotation_quotation_list_action_purchase_order_view_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'quotation_add_client_name_access' => array(
										'label' => 'Add Client Name',
										'name' => 'quotation_quotation_add_client_name_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'quotation_add_member_name_access'=> array(
										'label' => 'Add Member Name',
										'name' => 'quotation_quotation_add_member_name_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'quotation_pdf_banner_for_saudi_access'=> array(
										'label' => 'Saudi Banner',
										'name' => 'quotation_quotation_pdf_banner_for_saudi_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'quotation_pdf_banner_for_qatar_access'=> array(
										'label' => 'Qatar Banner',
										'name' => 'quotation_quotation_pdf_banner_for_qatar_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'quotation_form_add_new_product_access' => array(
										'label' => 'Add Product',
										'name' => 'quotation_quotation_form_add_new_product_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'quotation_form_add_new_unit_access' => array(
										'label' => 'Add Units',
										'name' => 'quotation_quotation_form_add_new_unit_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									)
								),
							),
							'lead'=> array(
								'name'=> 'Lead Access',
								'access_details'=> array(
									'lead_sales_user_id' => array(
										'label' => 'Sales Person',
                                        'name' => 'lead_lead_sales_user_id',
										'class' => 'col-xl-6',
                                        'input_type' => 'dropdown',
										'data' => $this->change_key_as_per_value(
													$this->common_model->get_dynamic_data_sales_db_null_false(
														'user_id as value, name, "" as selected',
														"status = 1 and role IN(5, 16)",
														'users'
													)
												)
									),
									'lead_country_id' => array(
										'label' => 'Country',
                                        'name' => 'lead_lead_country_id',
										'class' => 'col-xl-6',
                                        'input_type' => 'dropdown',
										'data' => $this->change_key_as_per_value(
													$this->common_model->get_dynamic_data_sales_db_null_false(
														'id as value, name, "" as selected',
														"status = 'Active'",
														'country_mst'
													)
												)
									),
									'lead_list_instrumentation_tab_access'=> array(
										'label' => 'Instrumentation Manager',
										'name' => 'lead_lead_list_instrumentation_tab_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'lead_list_pq_tab_access'=> array(
										'label' => 'PQ Tab',
										'name' => 'lead_lead_list_pq_tab_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'lead_list_action_pq_status_access'=> array(
										'label' => 'Listing PQ Status',
										'name' => 'lead_lead_list_action_pq_status_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'lead_list_action_pq_details_access'=> array(
										'label' => 'Listing PQ Details',
										'name' => 'lead_lead_list_action_pq_details_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'lead_list_action_graph_expoter_name_access'=> array(
										'label' => 'Graph Expoter Name',
										'name' => 'lead_lead_list_action_graph_expoter_name_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'lead_list_action_graph_access'=> array(
										'label' => 'Listing Graph',
										'name' => 'lead_lead_list_action_graph_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'lead_list_action_pq_query_access'=> array(
										'label' => 'Listing Query',
										'name' => 'lead_lead_list_action_pq_query_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'lead_list_action_special_comment_access'=> array(
										'label' => 'Listing Special Comment',
										'name' => 'lead_lead_list_action_special_comment_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									)
								)
							),
							'pq'=> array(
								'name'=> 'PQ Access',
								'access_details'=> array(
									'pq_sales_user_id' => array(
										'label' => 'Sales Person',
                                        'name' => 'pq_pq_sales_user_id',
										'class' => 'col-xl-6',
                                        'input_type' => 'dropdown',
										'data' => $this->change_key_as_per_value(
													$this->common_model->get_dynamic_data_sales_db_null_false(
														'user_id as value, name, "" as selected',
														"status = 1 and role IN(5, 16)",
														'users'
													)
												)
									),
								)
							),
							'rfq'=> array(
								'name'=> 'RFQ Access',
								'access_details'=> array(
									'rfq_sales_user_id' => array(
										'label' => 'Sales Person',
                                        'name' => 'rfq_rfq_sales_user_id',
										'class' => 'col-xl-6',
                                        'input_type' => 'dropdown',
										'data' => $this->change_key_as_per_value(
													$this->common_model->get_dynamic_data_sales_db_null_false(
														'user_id as value, name, "" as selected',
														"status = 1 and role IN(5, 16)",
														'users'
													)
												)
									),
									'rfq_procurement_user_id' => array(
										'label' => 'Procurement Person',
                                        'name' => 'rfq_rfq_procurement_user_id',
										'class' => 'col-xl-6',
                                        'input_type' => 'dropdown',
										'data' => $this->change_key_as_per_value(
													$this->common_model->get_dynamic_data_sales_db_null_false(
														'user_id as value, name, "" as selected',
														"status = 1 and role IN(6, 8)",
														'users'
													)
												)
									),
									'rfq_priority'=> array(
										'label' => 'Star Rating',
										'name' => 'rfq_rfq_priority',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'rfq_list_client_name_access'=> array(
										'label' => 'Listing Client Name',
										'name' => 'rfq_rfq_list_client_name_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'rfq_add_client_name_access'=> array(
										'label' => 'Form Client Name',
										'name' => 'rfq_rfq_add_client_name_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'rfq_list_action_edit_access'=> array(
										'label' => 'Listing Edit',
										'name' => 'rfq_rfq_list_action_edit_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'rfq_list_action_is_new_access'=> array(
										'label' => 'Listing New',
										'name' => 'rfq_rfq_list_action_is_new_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'rfq_list_action_delete_access'=> array(
										'label' => 'Listing Delete',
										'name' => 'rfq_rfq_list_action_delete_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'rfq_list_action_query_access'=> array(
										'label' => 'Listing RFQ Query',
										'name' => 'rfq_rfq_list_action_query_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'rfq_list_action_chat_conversation_access'=> array(
										'label' => 'Listing Comment',
										'name' => 'rfq_rfq_list_action_chat_conversation_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'rfq_list_action_graph_access'=> array(
										'label' => 'Listing Graph',
										'name' => 'rfq_rfq_list_action_graph_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'rfq_list_action_rfq_and_technical_document_access'=> array(
										'label' => 'Listing Document Upload',
										'name' => 'rfq_rfq_list_action_rfq_and_technical_document_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'rfq_list_member_name_access'=> array(
										'label' => 'Listing Member Name',
										'name' => 'rfq_rfq_list_member_name_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'rfq_list_value_and_country_access'=> array(
										'label' => 'Listing Value & Country',
										'name' => 'rfq_rfq_list_value_and_country_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'rfq_list_client_type_access'=> array(
										'label' => 'Listing Client Type',
										'name' => 'rfq_rfq_list_client_type_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'rfq_list_rfq_type_access'=> array(
										'label' => 'Listing RFQ Type',
										'name' => 'rfq_rfq_list_rfq_type_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'rfq_add_member_name_access'=> array(
										'label' => 'Form Member Name',
										'name' => 'rfq_rfq_add_member_name_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'rfq_document_delete_access' => array(
										'label' => 'File And Document Delete',
										'name' => 'rfq_rfq_document_delete_access',
										'class' => 'col-xl-6',
										'input_type' => 'checkbox',
										'data' => array(
											'file'  => array(
														'name'=> 'file',
														'checked'=> '',
													),
											'technical_document' => array(
														'name'=> 'technical_document',
														'checked'=> '',
													),
										),
									),
									'rfq_product_family_access' => array(
										'label' => 'Product Family',
										'name' => 'rfq_rfq_product_family_access',
										'class' => 'col-xl-6',
										'input_type' => 'checkbox',
										'data' => array(
											'piping'  => array(
														'name'=> 'piping',
														'checked'=> '',
													),
											'tubing' => array(
														'name'=> 'tubing',
														'checked'=> '',
													),
											'fastener' => array(
														'name'=> 'fastener',
														'checked'=> '',
													),
											'precision'  => array(
														'name'=> 'precision',
														'checked'=> '',
													),
											'valve' => array(
														'name'=> 'valve',
														'checked'=> '',
													),
											'industrial' => array(
														'name'=> 'industrial',
														'checked'=> '',
													),
											'instrumentation' => array(
														'name'=> 'instrumentation',
														'checked'=> '',
													),

										),
									),
								)
							),
							'query'=> array(
								'name'=> 'Query Access',
								'access_details'=> array(
									'query_sales_user_id' => array(
										'label' => 'Sales Person',
                                        'name' => 'query_query_sales_user_id',
										'class' => 'col-xl-6',
                                        'input_type' => 'dropdown',
										'data' => $this->change_key_as_per_value(
													$this->common_model->get_dynamic_data_sales_db_null_false(
														'user_id as value, name, "" as selected',
														"status = 1 and role IN(5, 16)",
														'users'
													)
												)
									),
									'query_procurement_user_id' => array(
										'label' => 'Procurement Person',
                                        'name' => 'query_query_procurement_user_id',
										'class' => 'col-xl-6',
                                        'input_type' => 'dropdown',
										'data' => $this->change_key_as_per_value(
													$this->common_model->get_dynamic_data_sales_db_null_false(
														'user_id as value, name, "" as selected',
														"status = 1 and role IN(6, 8)",
														'users'
													)
												)
									),
									'query_quality_user_id' => array(
										'label' => 'Quality Person',
                                        'name' => 'query_query_quality_user_id',
										'class' => 'col-xl-6',
                                        'input_type' => 'dropdown',
										'data' => $this->change_key_as_per_value(
													$this->common_model->get_dynamic_data_sales_db_null_false(
														'user_id as value, name, "" as selected',
														"status = 1 and role IN(10, 11)",
														'users'
													)
												)
									),
									'query_dataentry_user_id' => array(
										'label' => 'DataEntry Person',
                                        'name' => 'query_query_dataentry_user_id',
										'class' => 'col-xl-6',
                                        'input_type' => 'dropdown',
										'data' => $this->change_key_as_per_value(
													$this->common_model->get_dynamic_data_sales_db_null_false(
														'user_id as value, name, "" as selected',
														"status = 1 and role IN(4)",
														'users'
													)
												)
									),
									'query_production_user_id' => array(
										'label' => 'Production Person',
										'name' => 'query_query_production_user_id',
										'class' => 'col-xl-6',
										'input_type' => 'dropdown',
										'data' => $this->change_key_as_per_value(
											$this->common_model->get_dynamic_data_sales_db_null_false(
												'user_id as value, name, "" as selected',
												"status = 1 and role IN(18)",
												'users'
											)
										)
									),
									'query_pq_user_id' => array(
										'label' => 'PQ Person',
										'name' => 'query_query_pq_user_id',
										'class' => 'col-xl-6',
										'input_type' => 'dropdown',
										'data' => $this->change_key_as_per_value(
											$this->common_model->get_dynamic_data_sales_db_null_false(
												'user_id as value, name, "" as selected',
												"status = 1 and role IN(7)",
												'users'
											)
										)
									),
									'query_list_sample_query_reassigned_to_access' => array(
										'label' => 'Sample/Lead Reassigned To',
										'name' => 'query_query_list_sample_query_reassigned_to_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'query_list_client_name_access' => array(
										'label' => 'Listing Client Name',
                                        'name' => 'query_query_list_client_name_access',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'query_list_comment_tab_access' => array(
										'label' => 'Listing comment',
                                        'name' => 'query_query_list_comment_tab_access',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'query_list_mtc_drawing_tab_access' => array(
										'label' => 'Listing MTC',
                                        'name' => 'query_query_list_mtc_drawing_tab_access',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
								)
							),
							'production'=> array(
								'name'=> 'Production Access',
								'access_details'=> array(
									'production_sales_user_id' => array(
										'label' => 'Sales Person',
                                        'name' => 'production_production_sales_user_id',
										'class' => 'col-xl-6',
                                        'input_type' => 'dropdown',
										'data' => $this->change_key_as_per_value(
													$this->common_model->get_dynamic_data_sales_db_null_false(
														'user_id as value, name, "" as selected',
														"status = 1 and role IN(5, 16)",
														'users'
													)
												)
									),
									'production_procurement_user_id' => array(
										'label' => 'Procurement Person',
                                        'name' => 'production_production_procurement_user_id',
										'class' => 'col-xl-6',
                                        'input_type' => 'dropdown',
										'data' => $this->change_key_as_per_value(
													$this->common_model->get_dynamic_data_sales_db_null_false(
														'user_id as value, name, "" as selected',
														"status = 1 and role IN(6, 8)",
														'users'
													)
												)
									),
									'production_list_tab_access' => array(
										'label' => 'Tab',
										'name' => 'production_production_list_tab_access',
										'class' => 'col-xl-7',
										'input_type' => 'checkbox',
										'data' => array(
											'yta'  => array(
														'name'=> 'yta',
														'checked'=> '',
													),
											'all' => array(
														'name'=> 'all',
														'checked'=> '',
													),
											'pending' => array(
														'name'=> 'pending',
														'checked'=> '',
													),
											'semi_ready' => array(
														'name'=> 'semi_ready',
														'checked'=> '',
													),
											'mtt'  => array(
														'name'=> 'mtt',
														'checked'=> '',
													),
											'import'  => array(
														'name'=> 'import',
														'checked'=> '',
													),
											'query' => array(
														'name'=> 'query',
														'checked'=> '',
													),
											'mtt_rfd' => array(
														'name'=> 'mtt_rfd',
														'checked'=> '',
													),
											'ready_to_dispatch' => array(
														'name'=> 'ready_to_dispatch',
														'checked'=> '',
													),
											'dispatch'  => array(
														'name'=> 'dispatch',
														'checked'=> '',
													),
											'on_hold' => array(
														'name'=> 'on_hold',
														'checked'=> '',
													),
											'cancel' => array(
														'name'=> 'cancel',
														'checked'=> '',
													),
										),
									),
									'production_listing_filter_client_name' => array(
										'label' => 'Listing Filter Client Name',
                                        'name' => 'production_production_listing_filter_client_name',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'production_status_wise_count' => array(
										'label' => 'Status Wise Count',
                                        'name' => 'production_production_status_wise_count',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'production_list_delete' => array(
										'label' => 'Listing Delete',
                                        'name' => 'production_production_list_delete',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'production_mobile_client_type' => array(
										'label' => 'Mobile Access',
                                        'name' => 'production_production_mobile_client_type',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'production_list_action_edit_access' => array(
										'label' => 'Listing Edit',
                                        'name' => 'production_production_list_action_edit_access',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'production_form_data_view_access' => array(
										'label' => 'Form Data Access',
                                        'name' => 'production_production_form_data_view_access',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'production_list_action_query_access' => array(
										'label' => 'Listing Production Query',
                                        'name' => 'production_production_list_action_query_access',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'production_list_action_mtc_view_access' => array(
										'label' => 'Listing MTC View',
                                        'name' => 'production_production_list_action_mtc_view_access',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'production_list_action_technical_document_file_and_path_access' => array(
										'label' => 'Listing Technical Upload',
                                        'name' => 'production_production_list_action_technical_document_file_and_path_access',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'production_list_action_get_task_module_document_access' => array(
										'label' => 'Listing Task Module Document',
                                        'name' => 'production_production_list_action_get_task_module_document_access',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'production_list_action_show_in_details_access' => array(
										'label' => 'Listing full details view',
                                        'name' => 'production_production_list_action_show_in_details_access',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'production_list_action_rating_access' => array(
										'label' => 'Listing Star Rating',
                                        'name' => 'production_production_list_action_rating_access',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'production_list_action_rfq_edit_access' => array(
										'label' => 'Listing RFQ Edit',
                                        'name' => 'production_production_list_action_rfq_edit_access',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'production_list_action_quotation_edit_access' => array(
										'label' => 'Listing Quotation Edit',
                                        'name' => 'production_production_list_action_quotation_edit_access',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'production_list_search_filter_access' => array(
										'label' => 'Listing Search Filter',
                                        'name' => 'production_production_list_search_filter_access',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'production_list_work_order_sheet_add_access' => array(
										'label' => 'Listing Work Order Sheet Add',
                                        'name' => 'production_production_list_work_order_sheet_add_access',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'production_list_work_order_sheet_edit_access' => array(
										'label' => 'Listing Work Order Sheet Edit',
                                        'name' => 'production_production_list_work_order_sheet_edit_access',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'production_list_work_order_sheet_view_access' => array(
										'label' => 'Listing Work Order Sheet View',
                                        'name' => 'production_production_list_work_order_sheet_view_access',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'production_list_charges_and_grand_total_access' => array(
										'label' => 'Listing Charges and Grand total',
                                        'name' => 'production_production_list_charges_and_grand_total_access',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'production_technical_document_delete_access' => array(
										'label' => 'Technical Document Delete',
                                        'name' => 'production_production_technical_document_delete_access',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'production_product_family_access' => array(
										'label' => 'Product Family',
										'name' => 'production_production_product_family_access',
										'class' => 'col-xl-6',
										'input_type' => 'checkbox',
										'data' => array(
											'piping'  => array(
														'name'=> 'piping',
														'checked'=> '',
													),
											'tubing' => array(
														'name'=> 'tubing',
														'checked'=> '',
													),
											'fastener' => array(
														'name'=> 'fastener',
														'checked'=> '',
													),
											'precision'  => array(
														'name'=> 'precision',
														'checked'=> '',
													),
											'valve' => array(
														'name'=> 'valve',
														'checked'=> '',
													),
											'industrial' => array(
														'name'=> 'industrial',
														'checked'=> '',
													),
											'instrumentation' => array(
														'name'=> 'instrumentation',
														'checked'=> '',
													),

										),
									),
								)
							),
							'quality'=> array(
								'name'=> 'Quality Access',
								'access_details'=> array(
									'client_name_access' => array(
										'label' => 'Client Name',
                                        'name' => 'quality_client_name_access',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'quality_mtc_and_marking_upload_access' => array(
										'label' => 'Mtc & Marking Upload',
                                        'name' => 'quality_quality_mtc_and_marking_upload_access',
                                        'class' => 'col-xl-4',
										'input_type' => 'radio',
                                        'yes_value' => '',
                                        'no_value' => 'checked',
									),
									'quality_product_family_access' => array(
										'label' => 'Quality Product Family',
										'name' => 'quality_quality_product_family_access',
										'class' => 'col-xl-6',
										'input_type' => 'checkbox',
										'data' => array(
											'piping'  => array(
														'name'=> 'piping',
														'checked'=> '',
													),
											'tubing' => array(
														'name'=> 'tubing',
														'checked'=> '',
													),
											'fastener' => array(
														'name'=> 'fastener',
														'checked'=> '',
													),
											'precision'  => array(
														'name'=> 'precision',
														'checked'=> '',
													),
											'valve' => array(
														'name'=> 'valve',
														'checked'=> '',
													),
											'industrial' => array(
														'name'=> 'industrial',
														'checked'=> '',
													),
											'instrumentation' => array(
														'name'=> 'instrumentation',
														'checked'=> '',
													),

										),
									),
								)
							),
							'task'=> array(
								'name'=> 'Task Access',
								'access_details'=> array(
									'task_list_type_access' => array(
										'label' => 'List Tab',
										'name' => 'task_task_list_type_access',
										'class' => 'col-xl-7',
										'input_type' => 'checkbox',
										'data' => array(
											'drawing'  => array(
														'name'=> 'drawing',
														'checked'=> '',
													),
											'marking' => array(
														'name'=> 'marking',
														'checked'=> '',
													),
											'mtc' => array(
														'name'=> 'mtc',
														'checked'=> '',
													),
											'logistics_weight_and_dimension' => array(
														'name'=> 'logistics_weight_and_dimension',
														'checked'=> '',
													),
											'logistics_invoice_and_pl'  => array(
														'name'=> 'logistics_invoice_and_pl',
														'checked'=> '',
													),
											'quality_inspection' => array(
														'name'=> 'quality_inspection',
														'checked'=> '',
													),
										),
									),
									'task_list_action_marking_view_access' => array(
										'label' => 'Marking Access',
										'name' => 'task_task_list_action_marking_view_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'task_list_action_mtc_view_access' => array(
										'label' => 'MTC Access',
										'name' => 'task_task_list_action_mtc_view_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'task_list_client_name_access' => array(
										'label' => 'Client Name',
										'name' => 'task_task_list_client_name_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'task_list_rfq_and_quote_no_access' => array(
										'label' => 'RFQ & Quote No Access',
										'name' => 'task_task_list_rfq_and_quote_no_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'task_list_action_status_access' => array(
										'label' => 'Task Status Access',
										'name' => 'task_task_list_action_status_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
								)
							),
							'hr'=> array(
								'name'=> 'HR Access',
								'access_details'=> array(
									'hr_user_id' => array(
										'label' => 'HR Person',
										'name' => 'hr_hr_user_id',
										'class' => 'col-xl-6',
										'input_type' => 'dropdown',
										'data' => $this->change_key_as_per_value(
													$this->common_model->get_dynamic_data_sales_db_null_false(
														'user_id as value, name, "" as selected',
														"status = 1 and role IN(13)",
														'users'
													)
												)
									),
									'reporting_to_user_id' => array(
										'label' => 'Reporting To',
										'name' => 'hr_reporting_to_user_id',
										'class' => 'col-xl-6',
										'input_type' => 'dropdown',
										'data' => $this->change_key_as_per_value(
													$this->common_model->get_dynamic_data_sales_db_null_false(
														'user_id as value, name, "" as selected',
														"status = 'Active'",
														'mrf_reporting_manager'
													)
												)
									),
									'mrf_form_assigned_to_access' => array(
										'label' => 'MRF Form Assigned To',
										'name' => 'hr_mrf_form_assigned_to_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'mrf_list_action_stage_and_clone_access' => array(
										'label' => 'Stage And Clone Access',
										'name' => 'hr_mrf_list_action_stage_and_clone_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'mrf_list_action_delete_access' => array(
										'label' => 'MRF Delete',
										'name' => 'hr_mrf_list_action_delete_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'candidate_list_action_change_mrf_access' => array(
										'label' => 'Change MRF No',
										'name' => 'hr_candidate_list_action_change_mrf_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
									'candidate_list_action_tab_access' => array(
										'label' => 'Candidate Action tab',
										'name' => 'hr_candidate_list_action_tab_access',
										'class' => 'col-xl-4',
										'input_type' => 'radio',
										'yes_value' => '',
										'no_value' => 'checked',
									),
								)
							),
						),
						'user_id'=> 0,
						'user_name'=> ''
					);
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;

					if(!empty($post_details['user_id'])){

						$data['user_id'] = $post_details['user_id'];
						$data['user_name'] = $this->user_model->get_dynamic_data('name', array('user_id'=> $post_details['user_id']), 'users', 'row_array')['name'];
						$manager_access_details = $this->user_model->get_dynamic_data('*', array('status'=>'Active', 'user_id'=> $post_details['user_id']), 'manager_access_details', 'row_array');
						// echo "<pre>";print_r($manager_access_details);echo"</pre><hr>";exit;
						
						if(!empty($manager_access_details)){
							
							########################################################################################################
							############################################## QUOTATION ###############################################
							########################################################################################################
							if(!empty($manager_access_details['quotation'])){

								$quotation_access = json_decode($manager_access_details['quotation'], true);
								// echo "<pre>";print_r($quotation_access);echo"</pre><hr>";exit;
								foreach ($quotation_access as $single_quotation_access) {

									if(isset($data['single_user_module_details']['quotation']['access_details'][$single_quotation_access['name']])){

										if($data['single_user_module_details']['quotation']['access_details'][$single_quotation_access['name']]['input_type'] == 'radio'){

											if($single_quotation_access['access'] == 'true'){

												$data['single_user_module_details']['quotation']['access_details'][$single_quotation_access['name']]['yes_value'] = 'checked';
												$data['single_user_module_details']['quotation']['access_details'][$single_quotation_access['name']]['no_value'] = '';
											}
										}else if($data['single_user_module_details']['quotation']['access_details'][$single_quotation_access['name']]['input_type'] == 'checkbox'){

											foreach (explode(", ", $single_quotation_access['list_tab']) as $single_list) {

												$data['single_user_module_details']['quotation']['access_details'][$single_quotation_access['name']]['data'][$single_list]['checked'] = 'checked';
											}
										}else if($data['single_user_module_details']['quotation']['access_details'][$single_quotation_access['name']]['input_type'] == 'dropdown'){

											foreach (explode(", ", $single_quotation_access['ids']) as $single_id) {
												
												if(isset($data['single_user_module_details']['quotation']['access_details'][$single_quotation_access['name']]['data'][$single_id])){
													$data['single_user_module_details']['quotation']['access_details'][$single_quotation_access['name']]['data'][$single_id]['selected'] = 'selected';
												}
											}
										} 
									}
								}
							}
							########################################################################################################
							################################################# LEAD #################################################
							########################################################################################################
							if(!empty($manager_access_details['lead'])){

								$lead_access = json_decode($manager_access_details['lead'], true);
								foreach ($lead_access as $single_lead_access) {

									if(isset($data['single_user_module_details']['lead']['access_details'][$single_lead_access['name']])){

										if($data['single_user_module_details']['lead']['access_details'][$single_lead_access['name']]['input_type'] == 'radio'){

											if($single_lead_access['access'] == 'true'){

												$data['single_user_module_details']['lead']['access_details'][$single_lead_access['name']]['yes_value'] = 'checked';
												$data['single_user_module_details']['lead']['access_details'][$single_lead_access['name']]['no_value'] = '';
											}
										}else if($data['single_user_module_details']['lead']['access_details'][$single_lead_access['name']]['input_type'] == 'checkbox'){

											foreach (explode(", ", $single_lead_access['list_tab']) as $single_list) {

												$data['single_user_module_details']['lead']['access_details'][$single_lead_access['name']]['data'][$single_list]['checked'] = 'checked';
											}
										}else if($data['single_user_module_details']['lead']['access_details'][$single_lead_access['name']]['input_type'] == 'dropdown'){
											// echo "<pre>";print_r(explode(", ", $single_lead_access['ids']));echo"</pre><hr>";exit;
											foreach (explode(", ", $single_lead_access['ids']) as $single_id) {
												
												if(isset($data['single_user_module_details']['lead']['access_details'][$single_lead_access['name']]['data'][$single_id])){

													$data['single_user_module_details']['lead']['access_details'][$single_lead_access['name']]['data'][$single_id]['selected'] = 'selected';
												}
											}	
										} 
									}
								}
							}
							########################################################################################################
							################################################# PQ #################################################
							########################################################################################################
							if(!empty($manager_access_details['pq'])){

								$pq_access = json_decode($manager_access_details['pq'], true);
								foreach ($pq_access as $single_pq_access) {

									if(isset($data['single_user_module_details']['pq']['access_details'][$single_pq_access['name']])){

										if($data['single_user_module_details']['pq']['access_details'][$single_pq_access['name']]['input_type'] == 'radio'){

											if($single_pq_access['access'] == 'true'){

												$data['single_user_module_details']['pq']['access_details'][$single_pq_access['name']]['yes_value'] = 'checked';
												$data['single_user_module_details']['pq']['access_details'][$single_pq_access['name']]['no_value'] = '';
											}
										}else if($data['single_user_module_details']['pq']['access_details'][$single_pq_access['name']]['input_type'] == 'checkbox'){

											foreach (explode(", ", $single_pq_access['list_tab']) as $single_list) {

												$data['single_user_module_details']['pq']['access_details'][$single_pq_access['name']]['data'][$single_list]['checked'] = 'checked';
											}
										}else if($data['single_user_module_details']['pq']['access_details'][$single_pq_access['name']]['input_type'] == 'dropdown'){
											
											foreach (explode(", ", $single_pq_access['ids']) as $single_id) {
												
												if(isset($data['single_user_module_details']['pq']['access_details'][$single_pq_access['name']]['data'][$single_id])){

													$data['single_user_module_details']['pq']['access_details'][$single_pq_access['name']]['data'][$single_id]['selected'] = 'selected';
												}
											}	
										} 
									}
								}
							}
							########################################################################################################
							################################################# RFQ #################################################
							########################################################################################################
							if(!empty($manager_access_details['rfq'])){

								$rfq_access = json_decode($manager_access_details['rfq'], true);
								foreach ($rfq_access as $single_rfq_access) {

									if(isset($data['single_user_module_details']['rfq']['access_details'][$single_rfq_access['name']])){

										if($data['single_user_module_details']['rfq']['access_details'][$single_rfq_access['name']]['input_type'] == 'radio'){

											if($single_rfq_access['access'] == 'true'){

												$data['single_user_module_details']['rfq']['access_details'][$single_rfq_access['name']]['yes_value'] = 'checked';
												$data['single_user_module_details']['rfq']['access_details'][$single_rfq_access['name']]['no_value'] = '';
											}
										}else if($data['single_user_module_details']['rfq']['access_details'][$single_rfq_access['name']]['input_type'] == 'checkbox'){

											foreach (explode(", ", $single_rfq_access['list_tab']) as $single_list) {

												$data['single_user_module_details']['rfq']['access_details'][$single_rfq_access['name']]['data'][$single_list]['checked'] = 'checked';
											}
										}else if($data['single_user_module_details']['rfq']['access_details'][$single_rfq_access['name']]['input_type'] == 'dropdown'){
											
											foreach (explode(", ", $single_rfq_access['ids']) as $single_id) {
												
												if(isset($data['single_user_module_details']['rfq']['access_details'][$single_rfq_access['name']]['data'][$single_id])){

													$data['single_user_module_details']['rfq']['access_details'][$single_rfq_access['name']]['data'][$single_id]['selected'] = 'selected';
												}
											}	
										} 
									}
								}
							}
							########################################################################################################
							################################################# Query #################################################
							########################################################################################################
							if(!empty($manager_access_details['query'])){

								$query_access = json_decode($manager_access_details['query'], true);
								foreach ($query_access as $single_query_access) {

									if(isset($data['single_user_module_details']['query']['access_details'][$single_query_access['name']])){

										if($data['single_user_module_details']['query']['access_details'][$single_query_access['name']]['input_type'] == 'radio'){

											if($single_query_access['access'] == 'true'){

												$data['single_user_module_details']['query']['access_details'][$single_query_access['name']]['yes_value'] = 'checked';
												$data['single_user_module_details']['query']['access_details'][$single_query_access['name']]['no_value'] = '';
											}
										}else if($data['single_user_module_details']['query']['access_details'][$single_query_access['name']]['input_type'] == 'checkbox'){

											foreach (explode(", ", $single_query_access['list_tab']) as $single_list) {

												$data['single_user_module_details']['query']['access_details'][$single_query_access['name']]['data'][$single_list]['checked'] = 'checked';
											}
										}else if($data['single_user_module_details']['query']['access_details'][$single_query_access['name']]['input_type'] == 'dropdown'){
											
											foreach (explode(", ", $single_query_access['ids']) as $single_id) {
												
												if(isset($data['single_user_module_details']['query']['access_details'][$single_query_access['name']]['data'][$single_id])){

													$data['single_user_module_details']['query']['access_details'][$single_query_access['name']]['data'][$single_id]['selected'] = 'selected';
												}
											}	
										} 
									}
								}
							}
							########################################################################################################
							############################################# Production ###############################################
							########################################################################################################
							if(!empty($manager_access_details['production'])){

								$production_access = json_decode($manager_access_details['production'], true);
								foreach ($production_access as $single_production_access) {

									if(isset($data['single_user_module_details']['production']['access_details'][$single_production_access['name']])){

										if($data['single_user_module_details']['production']['access_details'][$single_production_access['name']]['input_type'] == 'radio'){

											if($single_production_access['access'] == 'true'){

												$data['single_user_module_details']['production']['access_details'][$single_production_access['name']]['yes_value'] = 'checked';
												$data['single_user_module_details']['production']['access_details'][$single_production_access['name']]['no_value'] = '';
											}
										}else if($data['single_user_module_details']['production']['access_details'][$single_production_access['name']]['input_type'] == 'checkbox'){

											foreach (explode(", ", $single_production_access['list_tab']) as $single_list) {

												$data['single_user_module_details']['production']['access_details'][$single_production_access['name']]['data'][$single_list]['checked'] = 'checked';
											}
										}else if($data['single_user_module_details']['production']['access_details'][$single_production_access['name']]['input_type'] == 'dropdown'){
											
											foreach (explode(", ", $single_production_access['ids']) as $single_id) {
												
												if(isset($data['single_user_module_details']['production']['access_details'][$single_production_access['name']]['data'][$single_id])){

													$data['single_user_module_details']['production']['access_details'][$single_production_access['name']]['data'][$single_id]['selected'] = 'selected';
												}
											}	
										} 
									}
								}
							}
							########################################################################################################
							############################################# Quality ###############################################
							########################################################################################################
							if(!empty($manager_access_details['quality'])){

								$quality_access = json_decode($manager_access_details['quality'], true);

								foreach ($quality_access as $single_quality_access) {

									if(isset($data['single_user_module_details']['quality']['access_details'][$single_quality_access['name']])){

										if($data['single_user_module_details']['quality']['access_details'][$single_quality_access['name']]['input_type'] == 'radio'){

											if($single_quality_access['access'] == 'true'){

												$data['single_user_module_details']['quality']['access_details'][$single_quality_access['name']]['yes_value'] = 'checked';
												$data['single_user_module_details']['quality']['access_details'][$single_quality_access['name']]['no_value'] = '';
											}
										}else if($data['single_user_module_details']['quality']['access_details'][$single_quality_access['name']]['input_type'] == 'checkbox'){

											foreach (explode(", ", $single_quality_access['list_tab']) as $single_list) {

												$data['single_user_module_details']['quality']['access_details'][$single_quality_access['name']]['data'][$single_list]['checked'] = 'checked';
											}
										}
									}
								}
							}
							########################################################################################################
							############################################# Task ###############################################
							########################################################################################################
							if(!empty($manager_access_details['task'])){

								$task_access = json_decode($manager_access_details['task'], true);
								foreach ($task_access as $single_task_access) {

									if(isset($data['single_user_module_details']['task']['access_details'][$single_task_access['name']])){

										if($data['single_user_module_details']['task']['access_details'][$single_task_access['name']]['input_type'] == 'radio'){

											if($single_task_access['access'] == 'true'){

												$data['single_user_module_details']['task']['access_details'][$single_task_access['name']]['yes_value'] = 'checked';
												$data['single_user_module_details']['task']['access_details'][$single_task_access['name']]['no_value'] = '';
											}
										}else if($data['single_user_module_details']['task']['access_details'][$single_task_access['name']]['input_type'] == 'checkbox'){

											foreach (explode(", ", $single_task_access['list_tab']) as $single_list) {

												$data['single_user_module_details']['task']['access_details'][$single_task_access['name']]['data'][$single_list]['checked'] = 'checked';
											}
										}else if($data['single_user_module_details']['task']['access_details'][$single_task_access['name']]['input_type'] == 'dropdown'){

											foreach (explode(", ", $single_task_access['ids']) as $single_id) {

												if(isset($data['single_user_module_details']['task']['access_details'][$single_task_access['name']]['data'][$single_id])){

													$data['single_user_module_details']['task']['access_details'][$single_task_access['name']]['data'][$single_id]['selected'] = 'selected';
												}
											}
										}
									}
								}
							}
							########################################################################################################
							############################################# HR ###############################################
							########################################################################################################
							if(!empty($manager_access_details['hr'])){

								$hr_access = json_decode($manager_access_details['hr'], true);
								foreach ($hr_access as $single_hr_access) {

									if(isset($data['single_user_module_details']['hr']['access_details'][$single_hr_access['name']])){

										if($data['single_user_module_details']['hr']['access_details'][$single_hr_access['name']]['input_type'] == 'radio'){

											if($single_hr_access['access'] == 'true'){

												$data['single_user_module_details']['hr']['access_details'][$single_hr_access['name']]['yes_value'] = 'checked';
												$data['single_user_module_details']['hr']['access_details'][$single_hr_access['name']]['no_value'] = '';
											}
										}else if($data['single_user_module_details']['hr']['access_details'][$single_hr_access['name']]['input_type'] == 'checkbox'){

											foreach (explode(", ", $single_hr_access['list_tab']) as $single_list) {

												$data['single_user_module_details']['hr']['access_details'][$single_hr_access['name']]['data'][$single_list]['checked'] = 'checked';
											}
										}else if($data['single_user_module_details']['hr']['access_details'][$single_hr_access['name']]['input_type'] == 'dropdown'){

											foreach (explode(", ", $single_hr_access['ids']) as $single_id) {

												if(isset($data['single_user_module_details']['hr']['access_details'][$single_hr_access['name']]['data'][$single_id])){

													$data['single_user_module_details']['hr']['access_details'][$single_hr_access['name']]['data'][$single_id]['selected'] = 'selected';
												}
											}
										}
									}
								}
							}
						}
					}

					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
					$response['html_body'] =  $this->load->view('user/single_user_module_details_access', $data, TRUE);
				break;

				case 'save_module_details_access':
					
					// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;
					if(!empty($post_details['user_id'])){

						$insert_update_array = array();
						$quotation = $quotation_list_tab_access = $quotation_sales_user_id = $quotation_procurement_user_id = array();
						$lead = $lead_sales_user_id = $lead_procurement_user_id = array();
						$pq = $pq_sales_user_id = array();
						$rfq = $rfq_sales_user_id = $rfq_procurement_user_id =
						$rfq_document_delete_access = $rfq_product_family_access = array();
						$query = $query_sales_user_id = $query_procurement_user_id =
						$query_quality_user_id = $query_dataentry_user_id =
						$query_production_user_id = $query_pq_user_id = array();
						$production = $production_list_tab_access = $production_sales_user_id = $production_procurement_user_id = $production_product_family_access= array();
						$quality = $quality_product_family_access = array();
						$task = $task_task_list_type_access = array();
						$hr = $hr_user_id = $reporting_to_user_id = array();

						foreach ($post_details['form_data'] as $single_form_data) {
							
							$explode_name = explode("_", $single_form_data['name']);
							
							if(!empty($explode_name)){

								$column_name = $explode_name[0];
								$access_name = "";
								for($i = 1; $i<(count($explode_name)-1); $i++){

									$access_name .= $explode_name[$i]."_";
								}
								$access_name .= $explode_name[count($explode_name)-1];
							}
							if($column_name == 'quotation'){

								if(!in_array($access_name, array('quotation_list_tab_access', 'quotation_sales_user_id', 'quotation_procurement_user_id'))){

									$quotation[] = array(
										'name'=> $access_name,
										'access'=> $this->get_boolean_value($single_form_data['value'])
									);
								}else{

									$$access_name[] = $single_form_data['value'];
								}
							}elseif($column_name == 'lead'){

								if(!in_array($access_name, array('lead_sales_user_id', 'lead_country_id'))){

									$lead[] = array(
										'name'=> $access_name,
										'access'=> $this->get_boolean_value($single_form_data['value'])
									);
								}else{

									$$access_name[] = $single_form_data['value'];
								}
							}elseif($column_name == 'pq'){

								if(!in_array($access_name, array('pq_sales_user_id'))){

									$pq[] = array(
										'name'=> $access_name,
										'access'=> $this->get_boolean_value($single_form_data['value'])
									);
								}else{

									$$access_name[] = $single_form_data['value'];
								}
							}elseif($column_name == 'rfq'){

								if(!in_array($access_name, array('rfq_sales_user_id', 'rfq_procurement_user_id', 'rfq_document_delete_access', 'rfq_product_family_access'))){

									$rfq[] = array(
										'name'=> $access_name,
										'access'=> $this->get_boolean_value($single_form_data['value'])
									);
								}else{

									$$access_name[] = $single_form_data['value'];
								}
							}elseif($column_name == 'query'){

								if(!in_array($access_name, array('query_sales_user_id', 'query_procurement_user_id', 'query_quality_user_id', 'query_dataentry_user_id', 'query_production_user_id', 'query_pq_user_id'))){

									$query[] = array(
										'name'=> $access_name,
										'access'=> $this->get_boolean_value($single_form_data['value'])
									);
								}else{

									$$access_name[] = $single_form_data['value'];
								}
							}elseif($column_name == 'production'){

								if(!in_array($access_name, array('production_sales_user_id', 'production_procurement_user_id', 'production_list_tab_access', 'production_product_family_access'))){

									$production[] = array(
										'name'=> $access_name,
										'access'=> $this->get_boolean_value($single_form_data['value'])
									);
								}else{

									$$access_name[] = $single_form_data['value'];
								}
							}elseif($column_name == 'quality'){

								if(!in_array($access_name, array('quality_product_family_access'))){

									$quality[] = array(
										'name'=> $access_name,
										'access'=> $this->get_boolean_value($single_form_data['value'])
									);
								}else{

									$$access_name[] = $single_form_data['value'];
								}
							}elseif($column_name == 'task'){

								if(!in_array($access_name, array('task_list_type_access'))){

									$task[] = array(
										'name'=> $access_name,
										'access'=> $this->get_boolean_value($single_form_data['value'])
									);
								}else{

									$$access_name[] = $single_form_data['value'];
								}
							}elseif($column_name == 'hr'){

								if(!in_array($access_name, array('hr_user_id', 'reporting_to_user_id'))){

									$hr[] = array(
										'name'=> $access_name,
										'access'=> $this->get_boolean_value($single_form_data['value'])
									);
								}else{

									$$access_name[] = $single_form_data['value'];
								}
							}
						}
						if(!empty($quotation_list_tab_access)){

							$quotation[] = array(
								'name'=> 'quotation_list_tab_access',
								'list_tab'=> implode(", ", $quotation_list_tab_access)
							);
						}
						if(!empty($quotation_sales_user_id)){

							$quotation[] = array(
								'name'=> 'quotation_sales_user_id',
								'ids'=> implode(", ", $quotation_sales_user_id)
							);
						}
						if(!empty($quotation_procurement_user_id)){

							$quotation[] = array(
								'name'=> 'quotation_procurement_user_id',
								'ids'=> implode(", ", $quotation_procurement_user_id)
							);
						}
						if(!empty($quotation)){

							$insert_update_array['quotation'] = json_encode($quotation);
						}

						if(!empty($lead_sales_user_id)){

							$lead[] = array(
								'name'=> 'lead_sales_user_id',
								'ids'=> implode(", ", $lead_sales_user_id)
							);
						}
						if(!empty($lead_country_id)){

							$lead[] = array(
								'name'=> 'lead_country_id',
								'ids'=> implode(", ", $lead_country_id)
							);
						}
						if(!empty($lead)){

							$insert_update_array['lead'] = json_encode($lead);
						}

						if(!empty($pq_sales_user_id)){

							$pq[] = array(
								'name'=> 'pq_sales_user_id',
								'ids'=> implode(", ", $pq_sales_user_id)
							);
						}
						if(!empty($pq)){

							$insert_update_array['pq'] = json_encode($pq);
						}

						if(!empty($rfq_sales_user_id)){

							$rfq[] = array(
								'name'=> 'rfq_sales_user_id',
								'ids'=> implode(", ", $rfq_sales_user_id)
							);
						}
						if(!empty($rfq_procurement_user_id)){

							$rfq[] = array(
								'name'=> 'rfq_procurement_user_id',
								'ids'=> implode(", ", $rfq_procurement_user_id)
							);
						}
						if(!empty($rfq_document_delete_access)){

							$rfq[] = array(
								'name'=> 'rfq_document_delete_access',
								'list_tab'=> implode(", ", $rfq_document_delete_access)
							);
						}
						if(!empty($rfq_product_family_access)){

							$rfq[] = array(
								'name'=> 'rfq_product_family_access',
								'list_tab'=> implode(", ", $rfq_product_family_access)
							);
						}
						if(!empty($rfq)){

							$insert_update_array['rfq'] = json_encode($rfq);
						}

						if(!empty($query_sales_user_id)){

							$query[] = array(
								'name'=> 'query_sales_user_id',
								'ids'=> implode(", ", $query_sales_user_id)
							);
						}
						if(!empty($query_procurement_user_id)){

							$query[] = array(
								'name'=> 'query_procurement_user_id',
								'ids'=> implode(", ", $query_procurement_user_id)
							);
						}
						if(!empty($query_quality_user_id)){

							$query[] = array(
								'name'=> 'query_quality_user_id',
								'ids'=> implode(", ", $query_quality_user_id)
							);
						}
						if(!empty($query_dataentry_user_id)){

							$query[] = array(
								'name'=> 'query_dataentry_user_id',
								'ids'=> implode(", ", $query_dataentry_user_id)
							);
						}
						if(!empty($query_production_user_id)){

							$query[] = array(
								'name'=> 'query_production_user_id',
								'ids'=> implode(", ", $query_production_user_id)
							);
						}
						if(!empty($query_pq_user_id)){

							$query[] = array(
								'name'=> 'query_pq_user_id',
								'ids'=> implode(", ", $query_pq_user_id)
							);
						}
						if(!empty($query)){

							$insert_update_array['query'] = json_encode($query);
						}

						if(!empty($production_sales_user_id)){

							$production[] = array(
								'name'=> 'production_sales_user_id',
								'ids'=> implode(", ", $production_sales_user_id)
							);
						}
						if(!empty($production_procurement_user_id)){

							$production[] = array(
								'name'=> 'production_procurement_user_id',
								'ids'=> implode(", ", $production_procurement_user_id)
							);
						}
						if(!empty($production_list_tab_access)){

							$production[] = array(
								'name'=> 'production_list_tab_access',
								'list_tab'=> implode(", ", $production_list_tab_access)
							);
						}
						if(!empty($production_product_family_access)){

							$production[] = array(
								'name'=> 'production_product_family_access',
								'list_tab'=> implode(", ", $production_product_family_access)
							);
						}
						if(!empty($production)){

							$insert_update_array['production'] = json_encode($production);
						}

						if(!empty($quality_product_family_access)){

							$quality[] = array(
								'name'=> 'quality_product_family_access',
								'list_tab'=> implode(", ", $quality_product_family_access)
							);
						}
						if(!empty($quality)){

							$insert_update_array['quality'] = json_encode($quality);
						}
						if(!empty($task_list_type_access)){

							$task[] = array(
								'name'=> 'task_list_type_access',
								'list_tab'=> implode(", ", $task_list_type_access)
							);
						}
						if(!empty($task)){

							$insert_update_array['task'] = json_encode($task);
						}

						if(!empty($hr_user_id)){

							$hr[] = array(
								'name'=> 'hr_user_id',
								'ids'=> implode(", ", $hr_user_id)
							);
						}
						if(!empty($reporting_to_user_id)){

							$hr[] = array(
								'name'=> 'reporting_to_user_id',
								'ids'=> implode(", ", $reporting_to_user_id)
							);
						}
						if(!empty($hr)){

							$insert_update_array['hr'] = json_encode($hr);
						}

						// echo "<pre>";print_r($insert_update_array);echo"</pre><hr>";exit;
						$insert_update_array['update_by'] = $this->session->userdata('user_id');
						if(!empty($insert_update_array)){

							$this->common_model->update_data_sales_db('manager_access_details',$insert_update_array, array('user_id'=>$post_details['user_id']));
						}
					}	
				break;

				default:
					$response['status'] = 'failed';	
					$response['message'] = "call type not found";
				break;
			}
			echo json_encode($response);

		} else{
			die('access is not allowed to this function');
		}
	}

	public function check_session() {

		echo "<pre>";print_r($this->session->userdata());echo"</pre><hr>";exit;
	}

	// private function
	private function get_boolean_value($value){

		if($value == 'true'){

			return true;
		}
		return false;
	}
	private function change_key_as_per_value($data, $changing_key_column_name = 'value'){

		$return_data = array();
		foreach ($data as $single_data) {
			
			$return_data[$single_data[$changing_key_column_name]] = $single_data;
		}

		return $return_data;
	}

	private function create_user_details($where, $limit, $offset) {

		$data = array();
		$data = $this->user_model->get_user_details($where, $limit, $offset);
		$role = $this->user_model->get_dynamic_data('*', array(), 'role');
		$data['role'] = array_column($role, 'role_name', 'role_id');
		$data['call_type'] = 'user_list'; 
		$data['paggination_data']['limit'] = $limit;
		$data['paggination_data']['offset'] = $offset;
		return $data;
	}

	private function update_session_data_for_side_bar($user_id) {

		// $sidebar_session_data = array();
		// $sidebar_session_data['main_module_access'] = $this->db->get_where('user_to_module', array('user_id'=>$user_id, 'status' => 'Active'))->result_array();
		// $sidebar_session_data['sub_module_access'] = $this->db->get_where('user_to_sub_module', array('user_id'=>$user_id, 'status' => 'Active'))->result_array();
		// $this->session->set_userdata($sidebar_session_data);
	}

	private function exclude_list_data() {

		$data=array();
		$data['exclude_details']=$this->user_model->get_login_verification_exclude();
		$data['users']=$this->user_model->get_not_excluded_users(array_column($data['exclude_details'], 'user_id'));
        return $data;		
	}

	private function asset_data($where_array,$limit=10,$offset = 0){
		$data=array();
		if(array_key_exists('id',$where_array)){
            $where=array('id'=>$where_array['id']);
            $where['status'] = 'Active';
			$data = $this->user_model->get_data_with_paggination('user_to_assets',$where,'row_array','asset_data',$limit,$offset);
		}else{
            $where_array['status'] = 'Active';
	        $data = $this->user_model->get_data_with_paggination('user_to_assets',$where_array,'result_array','asset_data',$limit,$offset);
		}
		$data['users']=array_column($this->user_model->get_dynamic_data('name,user_id', array('status'=>1),'users'),'name','user_id');
	    $data['code']=array_column($this->user_model->get_all_conditional_data('code', array('status'=>'Active'),'user_to_assets','result_array','','','code'),'code');
	    $data['user_id']=array_column($this->user_model->get_all_conditional_data('user_id', array('status'=>'Active'),'user_to_assets','result_array','','','user_id'),'user_id');
	    $data['search_filter'] = $this->set_search_filter_value($where_array);
		$data['paggination_data']['limit'] =$limit;
        $data['paggination_data']['offset'] =$offset;
	    return $data;
	}

	private function set_search_filter_value($where_array) {

		$return_array['code'] = (!empty($where_array['code'])) ? $where_array['code']: ''; 
		$return_array['user'] = (!empty($where_array['user_id'])) ? $where_array['user_id']: ''; 
		$return_array['type'] = (!empty($where_array['type'])) ? $where_array['type']: ''; 
		$return_array['assign_date'] = (!empty($where_array['assign_date'])) ? date('Y-m-d',strtotime($where_array['assign_date'])): ''; 
		return $return_array;
	}

	private function prepare_user_list_data($where_array, $limit, $offset, $tab_name){

		$data = $this->user_model->get_login_type_listing_data($where_array, $limit, $offset);
		// echo "<pre>";print_r($data);echo"</pre><hr>";die;

		$role_details = array_column($this->common_model->get_dynamic_data_sales_db('role_id, role_name', array(), 'role'), 'role_name', 'role_id');
		$role_details['']='';
		$role_details[0]='';

		foreach($data['login_user_list'] as $login_user_list_key => $single_list_data_value){

			$data['login_user_list'][$login_user_list_key]['designation'] = $role_details[$single_list_data_value['role']];
		}

		$data['paggination_data']['limit'] = $limit;
		$data['paggination_data']['offset'] = $offset;
		$data['tab_name'] = $tab_name;
		// echo "<pre>";print_r($data);echo"</pre><hr>";die;
		return $data;
	}

	private function create_where_response_on_tab_name($search_filter_form_data, $tab_name){

		$return_array['search_filter'] = array('user_id'=>array(), 'designation_id'=>array());
		$user_id = $designation_id  = array();

		$return_array['where'] = '';
		if($tab_name == 'sms_list'){

			$return_array['where'] = "users.status = 1 AND users.user_id NOT IN (SELECT user_id FROM login_verification_ignore WHERE status = 'Active')";

		}else if($tab_name == 'whatsapp_list'){

			$return_array['where'] = "users.status = 1 AND login_verification_ignore.status = 'Active' AND login_verification_ignore.login_type IN ('whatsapp')";

		}else if($tab_name == 'email_list'){

			$return_array['where'] = "users.status = 1 AND login_verification_ignore.status = 'Active' AND login_verification_ignore.login_type IN ('email') ";

		}else if($tab_name == 'ignore_list'){

			$return_array['where'] = "users.status = 1 AND login_verification_ignore.status = 'Active' AND login_verification_ignore.login_type IN ('all') ";
		}

		if(!empty($search_filter_form_data)){

			foreach ($search_filter_form_data as $form_data) {
				if(!empty($form_data['value'])){

					switch ($form_data['name']) {

						case 'user_id':
							$user_id[] = $form_data['value'];
							$return_array['search_filter']['user_id'][] = $form_data['value'];
						break;

						case 'designation_id':
							$designation_id[] = $form_data['value'];
							$return_array['search_filter']['designation_id'][] = $form_data['value'];
						break;

						default:
						break;
					}
				}
			}

			if (!empty($user_id)) {
				$return_array['where'] .= " AND users.user_id IN (" . implode(', ', $user_id) . ")";
			}
			if (!empty($designation_id)) {
				$return_array['where'] .= " AND users.role IN (" . implode(', ', $designation_id) . ")";
			}
		}
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		return $return_array;
	}
}