jQuery(document).ready(function() {
	KTBootstrapSelect.init();
	add_date_picker();
	$('.kt-selectpicker').selectpicker();
});
$('div#single_user_data').on('click', 'button.add_user', function(){
	var data = {
		call_type: 'add_user',
		employee_name: $('input#add_user_employee_name').val(),
		email: $('input#add_user_email').val(),
		username: $('input#add_user_username').val(),
		password: $('input#add_user_password').val(),
		mobile: $('input#add_user_mobile').val(),
		role: $('select#add_user_role').val()
	};
	ajax_call_function(data, 'add_user');
});				

$('div#user_left_view').on('click', 'button#add_user', function(){

	add_remove_right_side_details('add');
	ajax_call_function({call_type: 'get_add_user_details'}, 'user_details');
});

$('div#user_left_view').on('click', 'button#show_department_graph', function(){
	ajax_call_function({call_type: 'get_department_graph_data'}, 'get_department_graph_data');
});

$('div#single_user_data').on('click', 'button.add_main_module_name', function(){

	if($('input#new_main_module_name').val() != '') {

		ajax_call_function({call_type: 'add_main_module_name', module_name: $('input#new_main_module_name').val()}, 'added_main_module_name');
    	setTimeout(function() { 
			ajax_call_function({call_type: 'get_module_details'}, 'module_details');
	    }, 2000);
	} else {
		swal({
    		title: "Please enter module name",
      		icon: 'warning',
    	});
	}
});

$('div#single_user_data').on('click', 'button.add_sub_module_name', function(){

	if($('select#main_module_name').val() != '' && $('input#new_sub_module_name').val() != '') {

		ajax_call_function({call_type: 'add_sub_module_name', main_module_id: $('select#main_module_name').val(), sub_module_name: $('input#new_sub_module_name').val()}, 'added_sub_module_name');
		setTimeout(function() { 
			ajax_call_function({call_type: 'get_module_details'}, 'module_details');
	    }, 2000);
	} else {
		swal({
    		title: "Please select main module name and enter sub module name",
      		icon: 'warning',
    	});
	}
});

$('div#single_user_data').on('click', 'button.update_main_module_name', function(){

	var module_id = $(this).attr('module-id');
	ajax_call_function({call_type: 'update_main_module_name', module_id: module_id, module_name: $('input#main_module_'+module_id).val()}, 'update_main_module_name');
	setTimeout(function() { 
		ajax_call_function({call_type: 'get_module_details'}, 'module_details');
    }, 2000);
});

$('div#single_user_data').on('click', 'button.update_sub_module_name', function(){

	var sub_module_id = $(this).attr('sub-module-id');
	ajax_call_function({call_type: 'update_sub_module_name', sub_module_id: sub_module_id, sub_module_name: $('input#sub_module_'+sub_module_id).val()}, 'update_sub_module_name');
	setTimeout(function() { 
		ajax_call_function({call_type: 'get_module_details'}, 'module_details');
    }, 2000);
});

$('div#single_user_data').on('click', 'button.delete_main_module_name', function(){

	var module_id = $(this).attr('module-id');
	ajax_call_function({call_type: 'delete_main_module_name', module_id: module_id}, 'delete_main_module_name');
	setTimeout(function() { 
		ajax_call_function({call_type: 'get_module_details'}, 'module_details');
    }, 2000);
});

$('div#single_user_data').on('click', 'button.delete_sub_module_name', function(){

	var sub_module_id = $(this).attr('sub-module-id');
	ajax_call_function({call_type: 'delete_sub_module_name', sub_module_id: sub_module_id}, 'delete_sub_module_name');
	setTimeout(function() { 
		ajax_call_function({call_type: 'get_module_details'}, 'module_details');
    }, 2000);
});				

$('div#user_left_view').on('click', 'button#add_module', function(){

	add_remove_right_side_details('add');
	ajax_call_function({call_type: 'get_module_details'}, 'module_details');
});

$('tbody#user_details').on('click', 'abbr.single_user_details_view', function(){

	var user_id = $(this).attr('user_id');
	add_remove_right_side_details('add');
	ajax_call_function({call_type: 'get_single_user_details', user_id: user_id}, 'single_user_details');
});

$('tbody#user_details').on('click', 'a.single_user_details_view', function(){

	var user_id = $(this).attr('user_id');
	add_remove_right_side_details('add');
	ajax_call_function({call_type: 'get_single_user_details', user_id: user_id}, 'single_user_details');
});

	
$('div#single_user_data').on('click', 'button.update_user_details', function(){

	var user_id = $(this).attr('user_id');
	var employee_name = $('input#'+user_id+'_employee_name').val();
	var email = $('input#'+user_id+'_email').val();
	var username = $('input#'+user_id+'_username').val();
	var password = $('input#'+user_id+'_password').val();
	var mobile = $('input#'+user_id+'_mobile').val();
	var role = $('select#'+user_id+'_role').val();
	var status = $('select#'+user_id+'_status').val();

	var dashboard_module_status = $('select#module_id_1').val();
	var dashboard_home = $("input[type='radio'][name='radio_1']:checked").val();
	var dashboard_graph = $("input[type='radio'][name='radio_64']:checked").val();

	var quotation_module_status = $('select#module_id_2').val();
	var quotation_add = $("input[type='radio'][name='radio_2']:checked").val();
	var quotation_list = $("input[type='radio'][name='radio_3']:checked").val();
	var quotation_draft_list = $("input[type='radio'][name='radio_4']:checked").val();
	var quotation_follow_list = $("input[type='radio'][name='radio_5']:checked").val();
	var quotation_proforma_list = $("input[type='radio'][name='radio_6']:checked").val();

	var invoice_module_status = $('select#module_id_3').val();
	var invoice_add = $("input[type='radio'][name='radio_7']:checked").val();
	var invoice_list = $("input[type='radio'][name='radio_8']:checked").val();
	var invoice_export_list = $("input[type='radio'][name='radio_58']:checked").val();

	var client_module_status = $('select#module_id_4').val();
	var client_add = $("input[type='radio'][name='radio_9']:checked").val();
	var client_list = $("input[type='radio'][name='radio_10']:checked").val();
	var client_member_list = $("input[type='radio'][name='radio_11']:checked").val();
	var client_add_new = $("input[type='radio'][name='radio_90']:checked").val();

	var piping_module_status = $('select#module_id_5').val();
	var piping_1 = $("input[type='radio'][name='radio_12']:checked").val();
	var piping_2 = $("input[type='radio'][name='radio_13']:checked").val();
	var piping_3 = $("input[type='radio'][name='radio_14']:checked").val();
	var piping_4 = $("input[type='radio'][name='radio_15']:checked").val();
	var piping_5 = $("input[type='radio'][name='radio_16']:checked").val();
	var piping_6 = $("input[type='radio'][name='radio_17']:checked").val();

	var instrument_module_status = $('select#module_id_6').val();
	var instrument_1 = $("input[type='radio'][name='radio_18']:checked").val();
	var instrument_2 = $("input[type='radio'][name='radio_19']:checked").val();
	var instrument_3 = $("input[type='radio'][name='radio_20']:checked").val();
	var instrument_4 = $("input[type='radio'][name='radio_21']:checked").val();
	var instrument_5 = $("input[type='radio'][name='radio_22']:checked").val();

	var others_module_status = $('select#module_id_7').val();
	var others_1 = $("input[type='radio'][name='radio_23']:checked").val();
	var others_2 = $("input[type='radio'][name='radio_24']:checked").val();
	var others_3 = $("input[type='radio'][name='radio_55']:checked").val();
	var others_miscellaneous = $("input[type='radio'][name='radio_62']:checked").val();

	var pq_module_status = $('select#module_id_8').val();
	var pq_add = $("input[type='radio'][name='radio_25']:checked").val();
	var pq_pending_list = $("input[type='radio'][name='radio_26']:checked").val();
	var pq_approved_list = $("input[type='radio'][name='radio_27']:checked").val();

	var procurement_module_status = $('select#module_id_9').val();
	var procurement_add = $("input[type='radio'][name='radio_28']:checked").val();
	var procurement_list = $("input[type='radio'][name='radio_29']:checked").val();
	var procurement_listingz = $("input[type='radio'][name='radio_54']:checked").val();
	
	var quality_module_status = $('select#module_id_10').val();
	var quality_add = $("input[type='radio'][name='radio_30']:checked").val();
	var quality_list = $("input[type='radio'][name='radio_31']:checked").val();
	var marking_add = $("input[type='radio'][name='radio_32']:checked").val();
	var marking_proforma_list = $("input[type='radio'][name='radio_33']:checked").val();
	var marking_sample_list = $("input[type='radio'][name='radio_34']:checked").val();
	var document_inspection = $("input[type='radio'][name='radio_57']:checked").val();

	var task_module_status = $('select#module_id_11').val();
	var task_list = $("input[type='radio'][name='radio_35']:checked").val();

	var vendors_module_status = $('select#module_id_12').val();
	var vendors_add = $("input[type='radio'][name='radio_36']:checked").val();
	var vendors_list = $("input[type='radio'][name='radio_37']:checked").val();

	var reports_module_status = $('select#module_id_13').val();
	var reports_touch_point_list = $("input[type='radio'][name='radio_38']:checked").val();
	var reports_daily_task_list = $("input[type='radio'][name='radio_39']:checked").val();
	var reports_daily_report = $("input[type='radio'][name='radio_47']:checked").val();

	var queries_module_status = $('select#module_id_14').val();
	var queries_sales_open = $("input[type='radio'][name='radio_40']:checked").val();
	var queries_sales_closed = $("input[type='radio'][name='radio_41']:checked").val();
	var queries_proforma_open = $("input[type='radio'][name='radio_42']:checked").val();
	var queries_proforma_closed = $("input[type='radio'][name='radio_43']:checked").val();
	var queries_purchase_open = $("input[type='radio'][name='radio_44']:checked").val();
	var queries_purchase_closed = $("input[type='radio'][name='radio_45']:checked").val();
	var queries_list = $("input[type='radio'][name='radio_59']:checked").val();

	var setting_module_status = $('select#module_id_15').val();
	var setting_currency = $("input[type='radio'][name='radio_46']:checked").val();
	var setting_vendor_management = $("input[type='radio'][name='radio_53']:checked").val();

	var user_module_status = $('select#module_id_16').val();
	var user_management = $("input[type='radio'][name='radio_48']:checked").val();	
	var user_login_manage = $("input[type='radio'][name='radio_60']:checked").val();
	var user_learning_video = $("input[type='radio'][name='radio_63']:checked").val();

	var hr_module_status = $('select#module_id_17').val();
	var hr_add_employee = $("input[type='radio'][name='radio_49']:checked").val();
	var hr_employee_list = $("input[type='radio'][name='radio_51']:checked").val();
	var mrf_list = $("input[type='radio'][name='radio_95']:checked").val();
	var candidate_list = $("input[type='radio'][name='radio_96']:checked").val();

	var internal_module_status = $('select#module_id_18').val();
	var primary_internal_data = $("input[type='radio'][name='radio_50']:checked").val();

	var search_engine_module_status = $('select#module_id_19').val();
	var search_engine_primary_data = $("input[type='radio'][name='radio_52']:checked").val();
	var search_engine_master_view = $("input[type='radio'][name='radio_61']:checked").val();

	var production_module = $('select#module_id_20').val();
	var production_status = $("input[type='radio'][name='radio_56']:checked").val();

	var ticket_management_module = $('select#module_id_21').val();
	// var ticket_management_list = $("input[type='radio'][name='radio_65']:checked").val();
	var ticket_management_master = $("input[type='radio'][name='radio_66']:checked").val();
	var ticket_management_it_list = $("input[type='radio'][name='radio_67']:checked").val();
	var ticket_management_crm_list = $("input[type='radio'][name='radio_68']:checked").val();
	var ticket_management_admin_list = $("input[type='radio'][name='radio_69']:checked").val();

	var manage_sales_person_module = $('select#module_id_22').val();
	var manage_sales_person_daily_report = $("input[type='radio'][name='radio_70']:checked").val();
	var manage_sales_person_daily_report_protocol = $("input[type='radio'][name='radio_71']:checked").val();
	
	var new_piping_module_status = $('select#module_id_24').val();
	var new_piping_1 = $("input[type='radio'][name='radio_72']:checked").val();
	var new_piping_2 = $("input[type='radio'][name='radio_73']:checked").val();
	var new_piping_3 = $("input[type='radio'][name='radio_74']:checked").val();
	var new_piping_4 = $("input[type='radio'][name='radio_75']:checked").val();
	var new_piping_5 = $("input[type='radio'][name='radio_76']:checked").val();
	var new_piping_6 = $("input[type='radio'][name='radio_79']:checked").val();

	var new_instrument_module_status = $('select#module_id_25').val();
	var new_instrument_1 = $("input[type='radio'][name='radio_80']:checked").val();
	var new_instrument_2 = $("input[type='radio'][name='radio_81']:checked").val();
	var new_instrument_3 = $("input[type='radio'][name='radio_82']:checked").val();
	var new_instrument_4 = $("input[type='radio'][name='radio_83']:checked").val();
	var new_instrument_5 = $("input[type='radio'][name='radio_84']:checked").val();
	
	var new_others_module_status = $('select#module_id_26').val();
	var new_others_1 = $("input[type='radio'][name='radio_85']:checked").val();
	var new_others_2 = $("input[type='radio'][name='radio_86']:checked").val();
	var new_others_3 = $("input[type='radio'][name='radio_87']:checked").val();
	var new_others_4 = $("input[type='radio'][name='radio_88']:checked").val();

	var new_pre_qualification = $('select#module_id_27').val();
	var pre_qualification_1 = $("input[type='radio'][name='radio_89']:checked").val();
	var pre_qualification_2 = $("input[type='radio'][name='radio_90']:checked").val();
	
	var lead_management = $('select#module_id_28').val();
	var lead_management_list = $("input[type='radio'][name='radio_91']:checked").val();

	var quotation_management = $('select#module_id_29').val();
	var quotation_management_list = $("input[type='radio'][name='radio_92']:checked").val();
	
	var data = {

		call_type: 'update_user_details',
		user_id : user_id,
		employee_name : employee_name,
		email : email,
		username : username,
		password : password,
		mobile : mobile,
		role : role,
		status : status,
		// dashboard module
		dashboard_module_status : dashboard_module_status,
		dashboard_home : dashboard_home,
		dashboard_graph : dashboard_graph,
		// Quotation module
		quotation_module_status : quotation_module_status,
		quotation_add : quotation_add,
		quotation_list : quotation_list,
		quotation_draft_list : quotation_draft_list,
		quotation_follow_list : quotation_follow_list,
		quotation_proforma_list : quotation_proforma_list,
		// Invoice module
		invoice_module_status : invoice_module_status,
		invoice_add : invoice_add,
		invoice_list : invoice_list,
		invoice_export_list : invoice_export_list,
		// CLient module
		client_module_status : client_module_status,
		client_add : client_add,
		client_list : client_list,
		client_member_list : client_member_list,
		client_add_new: client_add_new,
		// Piping module
		piping_module_status : piping_module_status,
		piping_1 : piping_1,
		piping_2 : piping_2,
		piping_3 : piping_3,
		piping_4 : piping_4,
		piping_5 : piping_5,
		piping_6 : piping_6,
		// Instrument module
		instrument_module_status : instrument_module_status,
		instrument_1 : instrument_1,
		instrument_2 : instrument_2,
		instrument_3 : instrument_3,
		instrument_4 : instrument_4,
		instrument_5 : instrument_5,						
		// Other module
		others_module_status : others_module_status,
		others_1 : others_1, 
		others_2 : others_2,
		others_3 : others_3,
		others_miscellaneous : others_miscellaneous,
		// Pq module
		pq_module_status : pq_module_status,
		pq_add : pq_add,
		pq_pending_list : pq_pending_list, 
		pq_approved_list : pq_approved_list,						
		// Procurement module
		procurement_module_status : procurement_module_status,
		procurement_add : procurement_add,
		procurement_list : procurement_list,
		procurement_listingz : procurement_listingz,
		// Quality module
		quality_module_status : quality_module_status,
		quality_add : quality_add,
		quality_list : quality_list,
		marking_add : marking_add,
		marking_proforma_list : marking_proforma_list,
		marking_sample_list : marking_sample_list,
		document_inspection : document_inspection,
		// Task module
		task_module_status : task_module_status,
		task_list : task_list,
		// Vendor module
		vendors_module_status : vendors_module_status,
		vendors_add : vendors_add,
		vendors_list : vendors_list,
		// Reports module
		reports_module_status : reports_module_status,
		reports_touch_point_list : reports_touch_point_list,
		reports_daily_task_list : reports_daily_task_list,
		reports_daily_report : reports_daily_report,
		// Queries module
		queries_module_status : queries_module_status,
		queries_sales_open : queries_sales_open,
		queries_sales_closed : queries_sales_closed,
		queries_proforma_open : queries_proforma_open,
		queries_proforma_closed : queries_proforma_closed,
		queries_purchase_open : queries_purchase_open,
		queries_purchase_closed : queries_purchase_closed,
		queries_list : queries_list,
		// Setting module
		setting_module_status : setting_module_status,
		setting_currency : setting_currency,
		setting_vendor_management : setting_vendor_management,
		// User module
		user_module_status : user_module_status,
		user_management : user_management,
		user_login_manage : user_login_manage,
		user_learning_video : user_learning_video,
		// Hr module	
		hr_module_status : hr_module_status,
		hr_add_employee : hr_add_employee,
		hr_employee_list : hr_employee_list,
		mrf_list : mrf_list,
		candidate_list: candidate_list,
		// Internal Data module	
		internal_module_status : internal_module_status,
		primary_internal_data : primary_internal_data,
		// Search Engine module	
		search_engine_module_status : search_engine_module_status,
		search_engine_primary_data : search_engine_primary_data,
		search_engine_master_view : search_engine_master_view,
		// Production Module
		production_module : production_module,
		production_status : production_status,
		// Ticket Management Module
		ticket_management_module : ticket_management_module,
		// ticket_management_list : ticket_management_list,
		ticket_management_master : ticket_management_master,
		ticket_management_it_list : ticket_management_it_list,
		ticket_management_crm_list : ticket_management_crm_list,
		ticket_management_admin_list : ticket_management_admin_list,
		// Manage Sales Person
		manage_sales_person_module : manage_sales_person_module,
		manage_sales_person_daily_report : manage_sales_person_daily_report,
		manage_sales_person_daily_report_protocol : manage_sales_person_daily_report_protocol,

		// Piping module
		new_piping_module_status: new_piping_module_status,
		new_piping_1: new_piping_1,
		new_piping_2: new_piping_2,
		new_piping_3: new_piping_3,
		new_piping_4: new_piping_4,
		new_piping_5: new_piping_5,
		new_piping_6: new_piping_6,
		// Instrument module
		new_instrument_module_status: new_instrument_module_status,
		new_instrument_1: new_instrument_1,
		new_instrument_2: new_instrument_2,
		new_instrument_3: new_instrument_3,
		new_instrument_4: new_instrument_4,
		new_instrument_5: new_instrument_5,
		// Other module
		new_others_module_status: new_others_module_status,
		new_others_1: new_others_1,
		new_others_2: new_others_2,
		new_others_3: new_others_3,
		new_others_4: new_others_4,
		// pre_qualification module
		new_pre_qualification: new_pre_qualification,
		pre_qualification_1: pre_qualification_1,
		pre_qualification_2: pre_qualification_2,
		// Lead Management module
		lead_management: lead_management,
		lead_management_list: lead_management_list,
		// Quotation Management Module
		quotation_management: quotation_management,
		quotation_management_list: quotation_management_list,
	};

	ajax_call_function(data, 'user_details_updated');
});

$('div#paggination_data').on('click', 'span.mtc-paggination', function(){
						
	search_filter($('select#user_status').val(), $('select#user_role').val(), $('input#user_search').val(), $(this).attr('limit'), $(this).attr('offset'));
});

$('div#user_left_view').on('input', 'input#user_search', function(){
	search_filter($('select#user_status').val(), $('select#user_role').val(), $('input#user_search').val());
});

$('div#user_left_view').on('change', 'select#user_status', function(){
	search_filter($('select#user_status').val(), $('select#user_role').val(), $('input#user_search').val());
});

$('div#user_left_view').on('change', 'select#user_role', function(){
	search_filter($('select#user_status').val(), $('select#user_role').val(), $('input#user_search').val());
});

$('div#user_right_view').on('click', 'li.close_right_side_details', function(){
	add_remove_right_side_details('remove');
});

$('tbody#user_details').on('click', 'abbr.delete_user', function(){

	$("select#delete_user_select_role option[value='"+$(this).attr('delete_role_id')+"']").attr("selected","selected");
	$('input#delete_user_id').val($(this).attr('delete_user_id'));
	// finding user name as per role id
	ajax_call_function({call_type: 'get_user_name', role_id: $(this).attr('delete_role_id')}, 'add_delete_user_select_user_details');
});

$('select#delete_user_select_role').click(function() {

	ajax_call_function({call_type: 'get_user_name', role_id: $('select#delete_user_select_role').val()}, 'add_delete_user_select_user_details');
});

$('button.add_delete_user_user_id').click(function() {

	if($('select#delete_user_select_user').val() == ''){
		alert('Please select User');
		return false;
	}
	ajax_call_function({call_type: 'delete_user_and_assign_to_other_user', deleted_user_id: $('input#delete_user_id').val(),
		assigned_to: $('select#delete_user_select_user').val()}, 'delete_user_and_assign_to_other_user');
});

$('tbody#user_details').on('click', 'a.single_user_graph_details_view', function(){

	var user_id = $(this).attr('user_id');
	add_remove_right_side_details('add');
	ajax_call_function({call_type: 'get_single_graph_user_details', user_id: user_id}, 'single_user_details');
});

$('div#single_user_data').on('click', 'button.update_graph_details', function(){
	var user_id = $(this).attr('user_id');
	var employee_name = $('input#'+user_id+'_employee_name').val();
	var role = $('select#'+user_id+'_role').val();
	var status = $('select#'+user_id+'_status').val()
	var data = {
		call_type: 'update_graph_data',
		update_form_data:$('form#graph_update_data').serializeArray(),
		user_id : user_id,
		employee_name : employee_name,
		role : role,
		status : status};
	ajax_call_function(data, 'user_details_updated');
});

$('div#user_right_view').on('click', 'li.close_right_side_details', function(){
	add_remove_right_side_details('remove');
});

$('tbody#exclude_login').on('click', 'a.delete_exclude_login_data', function(){
  	swal({
      title: "Are you sure?",
      icon: "warning",
      buttons:  ["Cancel", "remove exclude"],
      dangerMode: true, 
    })
    .then((willDelete) => {
        if (willDelete) {
            ajax_call_function({call_type:'delete_form_exclude',delete_id:$(this).attr('delete_id')},'delete_form_exclude');
            swal({
                title: "removed exclude successfully",
                icon: "success",
            });
        } else {
            swal({
                title: "removed exclude failed ",
                icon: "info",
            });
	    }
    });

});

$('div.add_user').on('click','button.add_selected_user_id',function(){
	 ajax_call_function({call_type:'add_user_to_exclude',user_id: $('select#Add_user').val(),},'add_user_to_exclude');

});

$('div.play_video').click(function(){
	alert('came in');
	var vid = document.getElementById("myVideo");
		vid.ontimeupdate = function() {
		console.log(vid.currentTime);
		document.getElementById("demo").innerHTML = vid.currentTime;
	};
});
$('tbody#user_details').on('click', 'a.user_to_video', function(){
	add_remove_right_side_details('add');
	var user_id = $(this).attr('user_id');
	ajax_call_function({call_type: 'get_single_video_user_details', user_id: user_id,department_name:'sales'}, 'single_user_details');
});

$('div#single_user_data').on('click', 'button.update_video_details', function(){
	var user_id = $(this).attr('user_id');
	var employee_name = $('input#'+user_id+'_employee_name').val();
	var role = $('select#'+user_id+'_role').val();
	var status = $('select#'+user_id+'_status').val()
	var data = {
		call_type: 'update_video_data',
		update_form_data:$('form#video_update_data').serializeArray(),
		user_id : user_id,
		employee_name : employee_name,
		role : role,
		status : status};
	ajax_call_function(data, 'user_details_updated');
});
$('#user_right_view').on('change', 'select#department_name', function(){
	var user_id = $(this).attr('user_id');
	ajax_call_function({call_type: 'get_single_video_details', user_id: user_id,department_name: $('select#department_name').val()}, 'video_data');
});
$('a.update_information_tab').click(function(){
	$('a.update_information_tab').removeClass('kt-widget__item--active');
	$(this).addClass('kt-widget__item--active');
});
$('a.update_information_tab').click(function(){
	ajax_call_function({call_type:'video_to_user',tab_name:$(this).attr('tab_name')},'video_to_user');
});

function search_filter(user_status, user_role, user_name, limit=100, offset=0) {

	ajax_call_function({
		call_type: 'paggination_data_for_user',
		search_status: user_status,
		search_role: user_role,
		search_user_name: user_name
	}, 'user_list');
}

function add_remove_right_side_details(call_type) {

	if(call_type == 'add') {
		$('div#user_left_view').removeClass('col-xl-12');
		$('div#user_left_view').addClass('col-xl-6');
		$('div#user_right_view').show();
	} else if(call_type == 'remove') {
		$('div#user_left_view').removeClass('col-xl-6');
		$('div#user_left_view').addClass('col-xl-12');
		$('div#user_right_view').hide();
	}
}
///////new codes of asset//////

$('button.submit_asset_form').click(function(){

  ajax_call_function({call_type:'save_asset_details',form_data:$('#asset_details').serializeArray()},'save_asset_details');
});
$('tbody#asset_listing').on('click','a.edit_asset',function(){
  ajax_call_function({call_type:'open_model',id:$(this).attr('id')},'open_model');
});

$('div.form_body').on('change', 'select#show_div', function(){

	 var get_div_data = $('select#show_div').val();
	 if(get_div_data =="computer"){
	 	$('div.computer').show();
	    $('div.laptop').hide();   } 
	  else if(get_div_data =="laptop"){
		$('div.laptop').show();
	    $('div.computer').hide();
	    }
	 $('div.'+get_div_data).show();
});
$('div#asset_search_filter_form_div').on('click','button.asset_search_filter_form_submit',function(){
	
    ajax_call_function({call_type:'asset_search_filter_and_paggination',form_data:$('#asset_search_filter_form').serializeArray(), limit: $('select.common_paggination').val(), offset: 0,},'asset_search_filter');
});
$('div#asset_paggination').on('click', 'li.listing_paggination_number', function(){

    ajax_call_function({call_type: 'asset_search_filter_and_paggination',form_data:$('#asset_search_filter_form').serializeArray(),limit: $(this).attr('limit'), offset: $(this).attr('offset')},'asset_paggination');
});
$('div#asset_paggination').on('change', 'select.common_paggination', function(){

	ajax_call_function({call_type: 'asset_search_filter_and_paggination',form_data:$('#asset_search_filter_form').serializeArray(), limit: $('select.common_paggination').val(), offset: 0},'asset_paggination');
});
$('a.add_search_filter').click(function(){
	$('div.asset_search_filter').show();
})
$('div#asset_search_filter_form_div').on('click','button.asset_search_filter_form_reset',function(){
   $('div.asset_search_filter').hide();
});
$('div.asset_listing').on('click','a.edit_asset',function(){
 ajax_call_function({call_type:'open_model',id:''},'open_model');
});

$('tbody#asset_listing').on('click', 'a.delete_asset', function(){

	swal({
      title: "Are you sure?",
      icon: "warning",
      buttons:  ["Cancel", "Delete Asset"],
      dangerMode: true, 
    })
    .then((willDelete) => {
        if (willDelete) {
            ajax_call_function({call_type:'delete_asset',delete_id:$(this).attr('delete_id')},'delete_asset');
            
        } else {
            swal({
                title: "Asset is not deleted",
                icon: "info",
            });
	    }
    });
});


$('li.tab_name_click').click(function () {

	var tab_name = $(this).attr('tab-name');
	if (!$('li.' + tab_name).hasClass('active_list')) {

		$('li.active_list').removeClass('active_list');
		$(this).addClass('active_list');
		$('input#limit').val(10);
		$('input#offset').val(0);
		get_user_list_body();
	}
});

$('a.add_search_filter').click(function () {

	var button_value = $(this).attr('button_value');
	if (button_value == 'show') {
		ajax_call_function({call_type: 'get_search_filter_data'}, 'get_search_filter_data');
		$('div.search_filter_data').slideDown();
		$(this).attr('button_value', 'hide');
		$(this).html('Hide Search Filter');
	} else {
		$('div.search_filter_data').slideUp();
		$(this).attr('button_value', 'show');
		$(this).html('Add Search Filter');
	}
});

$('div.search_filter_data').on('click', 'button.search_filter_submit', function () {

	get_user_list_body();
});

$('div.search_filter_data').on('click', 'button.search_filter_reset', function () {

	location.reload();

});

$('div.paggination').on('click', 'li.paggination_number', function () {

	$('input#limit').val($(this).attr('limit'));
	$('input#offset').val($(this).attr('offset'));
	get_user_list_body();
});

$('div.paggination').on('change', 'select.limit_change', function () {

	$('input#limit').val($('select#set_limit').val());
    $('input#offset').val(0);
	get_user_list_body();
});

$('tbody#list_body').on('click', 'a.delete_user', function () {

	swal({
		title: "Are you sure?",
		text: "Once deleted, you will not be able to recover User",
		icon: "warning",
		buttons: ["Cancel", "Delete"],
		dangerMode: true,
	})
		.then((willDelete) => {
			if (willDelete) {
				ajax_call_function({
					call_type: 'delete_user',
					user_id: $(this).attr('user_id'),
					tab_name: $(this).attr('tab_name'),
				}, 'delete_user');
				swal({
					title: "User Deleted",
					icon: "success",
				});
			} else {
				swal({
					title: "User Not Delete",
					icon: "info",
				});
			}
		});
});

$('a.login_type').click(function(){

	ajax_call_function({
		call_type: 'change_login_type',
		user_id: $(this).attr('user_id'),
		type: $(this).attr('value'),
	}, 'change_login_type');
});

$('tbody#user_details').on('click', 'a.module_details_access', function(){

	var user_id = $(this).attr('user_id');
	add_remove_right_side_details('add');
	ajax_call_function({call_type: 'get_module_details_access', user_id: user_id}, 'get_module_details_access');
});

$('div#single_user_data').on('click', 'button.save_module_details_access', function(){
	
	ajax_call_function({call_type: 'save_module_details_access', form_data: $('form.save_module_details_access_form').serializeArray(), user_id: $(this).attr('user_id')}, 'save_module_details_access');
});
///end////

function ajax_call_function(data, callType, url = "<?php echo base_url('user/ajax_function'); ?>") {

	$.ajax({
		type: 'POST',
		data: data,
		url: url,
		dataType: 'JSON',
		success: function(res){
			if(res.status == 'successful') {
				if(callType == 'user_details_updated') {
					swal({
			    		title: "User Data Updated ",
			      		icon: "success",
			    	});
			    	//updating user listing details
			    	ajax_call_function({call_type: 'paggination_data_for_user', limit: $('input#user_limit').val(),
			    		offset: $('input#user_offset').val(), search_status: $('select#user_status').val()}, 'user_list');
				}else if(callType == 'single_user_details') {

					$('div#single_user_data').html('').html(res.html_body);
				}else if(callType == 'user_list') {
					
					$('tbody#user_details').html('').html(res.html_body);
					$('div#paggination_data').html('').html(res.html_paggination);
				}else if(callType == 'module_details') {

					$('div#single_user_data').html('').html(res.html_body);
				}else if(callType == 'added_main_module_name' || callType == 'added_sub_module_name' || callType == 'update_main_module_name' || callType == 'update_sub_module_name') {
					
					var sweetAlertText = 'Module name is added';
					if(callType == 'added_sub_module_name') {
						
						var sweetAlertText = 'Sub module name is added';
					} else if(callType == 'update_main_module_name') {

						var sweetAlertText = 'Module name is updated';
					} else if(callType == 'update_sub_module_name') {
						var sweetAlertText = 'Sub Module name is updated';
					}
					swal({
			    		title: sweetAlertText,
			      		icon: 'success',
			    	});

				}else if(callType == 'add_user') {
					swal({
			    		title: "User is added",
			      		icon: "success",
			    	});
			    	setTimeout(function() { 
						location.reload();
				    }, 2000);
				}else if(callType == 'delete_main_module_name' || callType == 'delete_sub_module_name') {

					var sweetAlertText = 'Module name is deleted';
					if(callType == 'delete_sub_module_name') {
						
						var sweetAlertText = 'Sub module name is deleted';
					}
					swal({
			    		title: sweetAlertText,
			      		icon: 'success',
			    	});
			    	setTimeout(function() { 
						location.reload();
				    }, 2000);	
				}else if(callType == 'user_details') {

					$('div#single_user_data').html('').html(res.html_body);
				}else if(callType == 'add_delete_user_select_user_details') {
					$('select#delete_user_select_user').html('').html(res.html_body);
				}else if(callType == 'delete_user_and_assign_to_other_user') {
					swal({
			    		title: 'User lead is reassigne',
			      		icon: 'success',
			    	});
			    	setTimeout(function() { 
						location.reload();
				    }, 2000);
					$('select#delete_user_select_user').html('').html(res.html_body);
				}else if(callType == 'get_department_graph_data') {
					create_highchart_for_department(res.graph_data);
				}else if(callType == 'delete_form_exclude' || callType == 'add_user_to_exclude') {
					location.reload()
				}else if(callType =='video_data'){
					$('tbody#video_body').html('').html(res.html_body);
				}else if(callType =='video_to_user'){
					$('div.video_list_data').html('').html(res.html_body);
				}else if(callType =='open_model'){
					$('div.form_body').html('').html(res.html_body);
				   add_date_picker();
				}else if(callType =='delete_asset'){
					$('div#asset_loader').hide();
					swal({
		                title: "Asset is deleted successfully",
		                icon: "success",
		            });
    				ajax_call_function({call_type: 'asset_search_filter_and_paggination',form_data:$('#asset_search_filter_form').serializeArray(),limit: $('input#limit').val(), offset: $('input#offset').val()},'asset_paggination');
				}else if(callType =='save_asset_details'){
					$('div#asset_loader').hide();
					swal({
		                title: "Asset is Added successfully",
		                icon: "success",
		            });
		            setTimeout(function() { 
						location.reload();
				    }, 2000)
				}else if(callType=='asset_paggination'||callType=='asset_search_filter'){``
				   	$('tbody#asset_listing').html('').html(res.table_body);
				   	$('div#asset_paggination').html('').html(res.table_paggination);
				   	$('div#asset_search_filter_form_div').html('').html(res.table_search_filter);
				   	add_date_picker();
				   	$('div#asset_loader').hide();
				}else if(callType == 'get_search_filter_data'){

					$('div.search_filter_data').html('').html(res.search_filter_data);
					$('select.kt-selectpicker').selectpicker();

				}else if(callType == 'get_user_list_body'){

					$('tbody#list_body').html('').html(res.list_body);
					$('div.search_filter_data').html('').html(res.search_filter_data);
					$('div.paggination').html('').html(res.paggination);
					dropdown_set_click_event();
					$('div#list_process').hide();
					$('select.kt-selectpicker').selectpicker();
				}else if(callType == 'change_login_type'){

                    toastr.success('Changes Saved Successfully!');
					get_user_list_body();
				}else if(callType == 'get_module_details_access'){

                    $('div#single_user_data').html('').html(res.html_body);
					$('select.kt-selectpicker').selectpicker();
				}

				$('.glo_app').hide();
			}
		},
		beforeSend: function(response){
			if(callType == 'add_delete_user_select_user_details' || callType == 'delete_user_and_assign_to_other_user') {
				$('.glo_app.add_delete_user_select_user_details').show();
			}if(callType=='asset_paggination'||callType=='asset_search_filter'||callType=='delete_asset'){
				$('div#asset_loader').show();
			} else {
				$('.glo_app').show();
			}
		}
	});
};

function create_highchart_for_department(graph_data) {

	Highcharts.chart('department_highchart', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: 'Department Wise Employeed Count'
	    },
	    subtitle: {
	        text: ''
	    },
	    accessibility: {
	        announceNewData: {
	            enabled: true
	        }
	    },
	    xAxis: {
	        type: 'category'
	    },
	    yAxis: {
	        title: {
	            text: 'Total Number of Employees'
	        }

	    },
	    legend: {
	        enabled: false
	    },
	    plotOptions: {
	        series: {
	            borderWidth: 0,
	            dataLabels: {
	                enabled: true,
	                format: '{point.y:f}'
	            }
	        }
	    },

	    tooltip: {
	        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> Employees<br/>'
	    },

	    series: [
	        {
	            name: "OM Tubes",
	            colorByPoint: true,
	            data: graph_data
	        }
	    ]
	});
}


function add_date_picker() {

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }
    $('#kt_datepicker_3, #kt_datepicker_3_validate').datepicker({
        rtl: KTUtil.isRTL(),
        todayBtn: "linked",
        clearBtn: true,
        todayHighlight: true,
        templates: arrows
    });    
}

function get_user_list_body(
    limit = $('input#limit').val(),
    offset = $('input#offset').val(),
){
    data = {
        call_type: 'get_user_list_body',
        search_form_data: $('form#search_filter_form').serializeArray(),
        limit: limit,
        offset: offset,
        tab_name: $('li.active_list').attr('tab-name'),
    }
    ajax_call_function(
        data,
        'get_user_list_body'
    );
}

function dropdown_set_click_event(){

    const dropdownItems = document.querySelectorAll("tbody#list_body .dropdown-item.login_type");
    dropdownItems.forEach(item => {
        item.addEventListener("click", function(event) {
            event.preventDefault();
            const type = this.getAttribute("value");
            const user_id = this.getAttribute("user_id");

            if(typeof user_id !== 'undefined' && user_id.length !== 0 && typeof type !== 'undefined' && type.length !== 0){

                ajax_call_function({
                    call_type: 'change_login_type',
					user_id: $(this).attr('user_id'),
					type: $(this).attr('value'),
                }, 'change_login_type');
            }
        });
    });
}