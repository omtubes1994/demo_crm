$('div#single_user_data').on('click', 'button.add_user', function(){
	var data = {
		call_type: 'add_user',
		employee_name: $('input#add_user_employee_name').val(),
		email: $('input#add_user_email').val(),
		username: $('input#add_user_username').val(),
		password: $('input#add_user_password').val(),
		mobile: $('input#add_user_mobile').val(),
		role: $('select#add_user_role').val()
	};
	ajax_call_function(data, 'add_user');
});				

$('div#user_left_view').on('click', 'button#add_user', function(){

	add_remove_right_side_details('add');
	ajax_call_function({call_type: 'get_add_user_details'}, 'user_details');
});

$('div#user_left_view').on('click', 'button#show_department_graph', function(){
	ajax_call_function({call_type: 'get_department_graph_data'}, 'get_department_graph_data');
});

$('div#single_user_data').on('click', 'button.add_main_module_name', function(){

	if($('input#new_main_module_name').val() != '') {

		ajax_call_function({call_type: 'add_main_module_name', module_name: $('input#new_main_module_name').val()}, 'added_main_module_name');
    	setTimeout(function() { 
			ajax_call_function({call_type: 'get_module_details'}, 'module_details');
	    }, 2000);
	} else {
		swal({
    		title: "Please enter module name",
      		icon: 'warning',
    	});
	}
});

$('div#single_user_data').on('click', 'button.add_sub_module_name', function(){

	if($('select#main_module_name').val() != '' && $('input#new_sub_module_name').val() != '') {

		ajax_call_function({call_type: 'add_sub_module_name', main_module_id: $('select#main_module_name').val(), sub_module_name: $('input#new_sub_module_name').val()}, 'added_sub_module_name');
		setTimeout(function() { 
			ajax_call_function({call_type: 'get_module_details'}, 'module_details');
	    }, 2000);
	} else {
		swal({
    		title: "Please select main module name and enter sub module name",
      		icon: 'warning',
    	});
	}
});

$('div#single_user_data').on('click', 'button.update_main_module_name', function(){

	var module_id = $(this).attr('module-id');
	ajax_call_function({call_type: 'update_main_module_name', module_id: module_id, module_name: $('input#main_module_'+module_id).val()}, 'update_main_module_name');
	setTimeout(function() { 
		ajax_call_function({call_type: 'get_module_details'}, 'module_details');
    }, 2000);
});

$('div#single_user_data').on('click', 'button.update_sub_module_name', function(){

	var sub_module_id = $(this).attr('sub-module-id');
	ajax_call_function({call_type: 'update_sub_module_name', sub_module_id: sub_module_id, sub_module_name: $('input#sub_module_'+sub_module_id).val()}, 'update_sub_module_name');
	setTimeout(function() { 
		ajax_call_function({call_type: 'get_module_details'}, 'module_details');
    }, 2000);
});

$('div#single_user_data').on('click', 'button.delete_main_module_name', function(){

	var module_id = $(this).attr('module-id');
	ajax_call_function({call_type: 'delete_main_module_name', module_id: module_id}, 'delete_main_module_name');
	setTimeout(function() { 
		ajax_call_function({call_type: 'get_module_details'}, 'module_details');
    }, 2000);
});

$('div#single_user_data').on('click', 'button.delete_sub_module_name', function(){

	var sub_module_id = $(this).attr('sub-module-id');
	ajax_call_function({call_type: 'delete_sub_module_name', sub_module_id: sub_module_id}, 'delete_sub_module_name');
	setTimeout(function() { 
		ajax_call_function({call_type: 'get_module_details'}, 'module_details');
    }, 2000);
});				

$('div#user_left_view').on('click', 'button#add_module', function(){

	add_remove_right_side_details('add');
	ajax_call_function({call_type: 'get_module_details'}, 'module_details');
});

$('tbody#user_details').on('click', 'abbr.single_user_details_view', function(){

	var user_id = $(this).attr('user_id');
	add_remove_right_side_details('add');
	ajax_call_function({call_type: 'get_single_user_details', user_id: user_id}, 'single_user_details');
});

$('tbody#user_details').on('click', 'a.single_user_details_view', function(){

	var user_id = $(this).attr('user_id');
	add_remove_right_side_details('add');
	ajax_call_function({call_type: 'get_single_user_details', user_id: user_id}, 'single_user_details');
});


$('div#single_user_data').on('click', 'button.update_user_details', function(){

	var user_id = $(this).attr('user_id');
	var employee_name = $('input#'+user_id+'_employee_name').val();
	var email = $('input#'+user_id+'_email').val();
	var username = $('input#'+user_id+'_username').val();
	var password = $('input#'+user_id+'_password').val();
	var mobile = $('input#'+user_id+'_mobile').val();
	var role = $('select#'+user_id+'_role').val();
	var status = $('select#'+user_id+'_status').val();

	var dashboard_module_status = $('select#module_id_1').val();
	var dashboard_home = $("input[type='radio'][name='radio_1']:checked").val();

	var quotation_module_status = $('select#module_id_2').val();
	var quotation_add = $("input[type='radio'][name='radio_2']:checked").val();
	var quotation_list = $("input[type='radio'][name='radio_3']:checked").val();
	var quotation_draft_list = $("input[type='radio'][name='radio_4']:checked").val();
	var quotation_follow_list = $("input[type='radio'][name='radio_5']:checked").val();
	var quotation_proforma_list = $("input[type='radio'][name='radio_6']:checked").val();

	var invoice_module_status = $('select#module_id_3').val();
	var invoice_add = $("input[type='radio'][name='radio_7']:checked").val();
	var invoice_list = $("input[type='radio'][name='radio_8']:checked").val();

	var client_module_status = $('select#module_id_4').val();
	var client_add = $("input[type='radio'][name='radio_9']:checked").val();
	var client_list = $("input[type='radio'][name='radio_10']:checked").val();
	var client_member_list = $("input[type='radio'][name='radio_11']:checked").val();

	var piping_module_status = $('select#module_id_5').val();
	var piping_1 = $("input[type='radio'][name='radio_12']:checked").val();
	var piping_2 = $("input[type='radio'][name='radio_13']:checked").val();
	var piping_3 = $("input[type='radio'][name='radio_14']:checked").val();
	var piping_4 = $("input[type='radio'][name='radio_15']:checked").val();
	var piping_5 = $("input[type='radio'][name='radio_16']:checked").val();
	var piping_6 = $("input[type='radio'][name='radio_17']:checked").val();

	var instrument_module_status = $('select#module_id_6').val();
	var instrument_1 = $("input[type='radio'][name='radio_18']:checked").val();
	var instrument_2 = $("input[type='radio'][name='radio_19']:checked").val();
	var instrument_3 = $("input[type='radio'][name='radio_20']:checked").val();
	var instrument_4 = $("input[type='radio'][name='radio_21']:checked").val();
	var instrument_5 = $("input[type='radio'][name='radio_22']:checked").val();

	var others_module_status = $('select#module_id_7').val();
	var others_1 = $("input[type='radio'][name='radio_23']:checked").val();
	var others_2 = $("input[type='radio'][name='radio_24']:checked").val();
	var others_3 = $("input[type='radio'][name='radio_55']:checked").val();

	var pq_module_status = $('select#module_id_8').val();
	var pq_add = $("input[type='radio'][name='radio_25']:checked").val();
	var pq_pending_list = $("input[type='radio'][name='radio_26']:checked").val();
	var pq_approved_list = $("input[type='radio'][name='radio_27']:checked").val();

	var procurement_module_status = $('select#module_id_9').val();
	var procurement_add = $("input[type='radio'][name='radio_28']:checked").val();
	var procurement_list = $("input[type='radio'][name='radio_29']:checked").val();
	var procurement_listingz = $("input[type='radio'][name='radio_54']:checked").val();
	
	var quality_module_status = $('select#module_id_10').val();
	var quality_add = $("input[type='radio'][name='radio_30']:checked").val();
	var quality_list = $("input[type='radio'][name='radio_31']:checked").val();
	var marking_add = $("input[type='radio'][name='radio_32']:checked").val();
	var marking_proforma_list = $("input[type='radio'][name='radio_33']:checked").val();
	var marking_sample_list = $("input[type='radio'][name='radio_34']:checked").val();

	var task_module_status = $('select#module_id_11').val();
	var task_list = $("input[type='radio'][name='radio_35']:checked").val();

	var vendors_module_status = $('select#module_id_12').val();
	var vendors_add = $("input[type='radio'][name='radio_36']:checked").val();
	var vendors_list = $("input[type='radio'][name='radio_37']:checked").val();

	var reports_module_status = $('select#module_id_13').val();
	var reports_touch_point_list = $("input[type='radio'][name='radio_38']:checked").val();
	var reports_daily_task_list = $("input[type='radio'][name='radio_39']:checked").val();
	var reports_daily_report = $("input[type='radio'][name='radio_47']:checked").val();

	var queries_module_status = $('select#module_id_14').val();
	var queries_sales_open = $("input[type='radio'][name='radio_40']:checked").val();
	var queries_sales_closed = $("input[type='radio'][name='radio_41']:checked").val();
	var queries_proforma_open = $("input[type='radio'][name='radio_42']:checked").val();
	var queries_proforma_closed = $("input[type='radio'][name='radio_43']:checked").val();
	var queries_purchase_open = $("input[type='radio'][name='radio_44']:checked").val();
	var queries_purchase_closed = $("input[type='radio'][name='radio_45']:checked").val();

	var setting_module_status = $('select#module_id_15').val();
	var setting_currency = $("input[type='radio'][name='radio_46']:checked").val();
	var setting_vendor_management = $("input[type='radio'][name='radio_53']:checked").val();

	var user_module_status = $('select#module_id_16').val();
	var user_management = $("input[type='radio'][name='radio_48']:checked").val();	

	var hr_module_status = $('select#module_id_17').val();
	var hr_add_employee = $("input[type='radio'][name='radio_49']:checked").val();
	var hr_employee_list = $("input[type='radio'][name='radio_51']:checked").val();

	var internal_module_status = $('select#module_id_18').val();
	var primary_internal_data = $("input[type='radio'][name='radio_50']:checked").val();

	var search_engine_module_status = $('select#module_id_19').val();
	var search_engine_primary_data = $("input[type='radio'][name='radio_52']:checked").val();

	var production_module = $('select#module_id_20').val();
	var production_status = $("input[type='radio'][name='radio_56']:checked").val();

	var data = {

		call_type: 'update_user_details',
		user_id : user_id,
		employee_name : employee_name,
		email : email,
		username : username,
		password : password,
		mobile : mobile,
		role : role,
		status : status,
		// dashboard module
		dashboard_module_status : dashboard_module_status,
		dashboard_home : dashboard_home,
		// Quotation module
		quotation_module_status : quotation_module_status,
		quotation_add : quotation_add,
		quotation_list : quotation_list,
		quotation_draft_list : quotation_draft_list,
		quotation_follow_list : quotation_follow_list,
		quotation_proforma_list : quotation_proforma_list,
		// Invoice module
		invoice_module_status : invoice_module_status,
		invoice_add : invoice_add,
		invoice_list : invoice_list,
		// CLient module
		client_module_status : client_module_status,
		client_add : client_add,
		client_list : client_list,
		client_member_list : client_member_list,
		// Piping module
		piping_module_status : piping_module_status,
		piping_1 : piping_1,
		piping_2 : piping_2,
		piping_3 : piping_3,
		piping_4 : piping_4,
		piping_5 : piping_5,
		piping_6 : piping_6,
		// Instrument module
		instrument_module_status : instrument_module_status,
		instrument_1 : instrument_1,
		instrument_2 : instrument_2,
		instrument_3 : instrument_3,
		instrument_4 : instrument_4,
		instrument_5 : instrument_5,						
		// Other module
		others_module_status : others_module_status,
		others_1 : others_1, 
		others_2 : others_2,
		others_3 : others_3,
		// Pq module
		pq_module_status : pq_module_status,
		pq_add : pq_add,
		pq_pending_list : pq_pending_list, 
		pq_approved_list : pq_approved_list,						
		// Procurement module
		procurement_module_status : procurement_module_status,
		procurement_add : procurement_add,
		procurement_list : procurement_list,
		procurement_listingz : procurement_listingz,
		// Quality module
		quality_module_status : quality_module_status,
		quality_add : quality_add,
		quality_list : quality_list,
		marking_add : marking_add,
		marking_proforma_list : marking_proforma_list,
		marking_sample_list : marking_sample_list,
		// Task module
		task_module_status : task_module_status,
		task_list : task_list,
		// Vendor module
		vendors_module_status : vendors_module_status,
		vendors_add : vendors_add,
		vendors_list : vendors_list,
		// Reports module
		reports_module_status : reports_module_status,
		reports_touch_point_list : reports_touch_point_list,
		reports_daily_task_list : reports_daily_task_list,
		reports_daily_report : reports_daily_report,
		// Queries module
		queries_module_status : queries_module_status,
		queries_sales_open : queries_sales_open,
		queries_sales_closed : queries_sales_closed,
		queries_proforma_open : queries_proforma_open,
		queries_proforma_closed : queries_proforma_closed,
		queries_purchase_open : queries_purchase_open,
		queries_purchase_closed : queries_purchase_closed,
		// Setting module
		setting_module_status : setting_module_status,
		setting_currency : setting_currency,
		setting_vendor_management : setting_vendor_management,
		// User module
		user_module_status : user_module_status,
		user_management : user_management,
		// Hr module	
		hr_module_status : hr_module_status,
		hr_add_employee : hr_add_employee,
		hr_employee_list : hr_employee_list,
		// Internal Data module	
		internal_module_status : internal_module_status,
		primary_internal_data : primary_internal_data,
		// Search Engine module	
		search_engine_module_status : search_engine_module_status,
		search_engine_primary_data : search_engine_primary_data,
		// Production Module
		production_module : production_module,
		production_status : production_status
	};

	ajax_call_function(data, 'user_details_updated');
});

$('div#paggination_data').on('click', 'span.mtc-paggination', function(){
						
	search_filter($('select#user_status').val(), $('select#user_role').val(), $('input#user_search').val(), $(this).attr('limit'), $(this).attr('offset'));
});

$('div#user_left_view').on('input', 'input#user_search', function(){
	search_filter($('select#user_status').val(), $('select#user_role').val(), $('input#user_search').val());
});

$('div#user_left_view').on('change', 'select#user_status', function(){
	search_filter($('select#user_status').val(), $('select#user_role').val(), $('input#user_search').val());
});

$('div#user_left_view').on('change', 'select#user_role', function(){
	search_filter($('select#user_status').val(), $('select#user_role').val(), $('input#user_search').val());
});

$('div#user_right_view').on('click', 'li.close_right_side_details', function(){
	add_remove_right_side_details('remove');
});

$('tbody#user_details').on('click', 'abbr.delete_user', function(){

	$("select#delete_user_select_role option[value='"+$(this).attr('delete_role_id')+"']").attr("selected","selected");
	$('input#delete_user_id').val($(this).attr('delete_user_id'));
	// finding user name as per role id
	ajax_call_function({call_type: 'get_user_name', role_id: $(this).attr('delete_role_id')}, 'add_delete_user_select_user_details');
});
$('select#delete_user_select_role').click(function() {

	ajax_call_function({call_type: 'get_user_name', role_id: $('select#delete_user_select_role').val()}, 'add_delete_user_select_user_details');
});
$('button.add_delete_user_user_id').click(function() {

	if($('select#delete_user_select_user').val() == ''){
		alert('Please select User');
		return false;
	}
	ajax_call_function({call_type: 'delete_user_and_assign_to_other_user', deleted_user_id: $('input#delete_user_id').val(),
		assigned_to: $('select#delete_user_select_user').val()}, 'delete_user_and_assign_to_other_user');
});

function search_filter(user_status, user_role, user_name, limit=100, offset=0) {

	ajax_call_function({
		call_type: 'paggination_data_for_user',
		search_status: user_status,
		search_role: user_role,
		search_user_name: user_name
	}, 'user_list');
}

function add_remove_right_side_details(call_type) {

	if(call_type == 'add') {
		$('div#user_left_view').removeClass('col-xl-12');
		$('div#user_left_view').addClass('col-xl-6');
		$('div#user_right_view').show();
	} else if(call_type == 'remove') {
		$('div#user_left_view').removeClass('col-xl-6');
		$('div#user_left_view').addClass('col-xl-12');
		$('div#user_right_view').hide();
	}
}

function ajax_call_function(data, callType, url = "<?php echo base_url('user/ajax_function'); ?>") {

	$.ajax({
		type: 'POST',
		data: data,
		url: url,
		dataType: 'JSON',
		success: function(res){
			if(res.status == 'successful') {
				if(callType == 'user_details_updated') {
					swal({
			    		title: "User Data Updated ",
			      		icon: "success",
			    	});
			    	//updating user listing details
			    	ajax_call_function({call_type: 'paggination_data_for_user', limit: $('input#user_limit').val(),
			    		offset: $('input#user_offset').val(), search_status: $('select#user_status').val()}, 'user_list');
				}
				if(callType == 'single_user_details') {

					$('div#single_user_data').html('').html(res.html_body);
				}
				if(callType == 'user_list') {
					
					$('tbody#user_details').html('').html(res.html_body);
					$('div#paggination_data').html('').html(res.html_paggination);
				}
				if(callType == 'module_details') {

					$('div#single_user_data').html('').html(res.html_body);
				}
				if(callType == 'added_main_module_name' || callType == 'added_sub_module_name' || callType == 'update_main_module_name' || callType == 'update_sub_module_name') {
					
					var sweetAlertText = 'Module name is added';
					if(callType == 'added_sub_module_name') {
						
						var sweetAlertText = 'Sub module name is added';
					} else if(callType == 'update_main_module_name') {

						var sweetAlertText = 'Module name is updated';
					} else if(callType == 'update_sub_module_name') {
						var sweetAlertText = 'Sub Module name is updated';
					}
					swal({
			    		title: sweetAlertText,
			      		icon: 'success',
			    	});

				}
				if(callType == 'add_user') {
					swal({
			    		title: "User is added",
			      		icon: "success",
			    	});
			    	setTimeout(function() { 
						location.reload();
				    }, 2000);
				}

				if(callType == 'delete_main_module_name' || callType == 'delete_sub_module_name') {

					var sweetAlertText = 'Module name is deleted';
					if(callType == 'delete_sub_module_name') {
						
						var sweetAlertText = 'Sub module name is deleted';
					}
					swal({
			    		title: sweetAlertText,
			      		icon: 'success',
			    	});
			    	setTimeout(function() { 
						location.reload();
				    }, 2000);	
				}
				if(callType == 'user_details') {

					$('div#single_user_data').html('').html(res.html_body);
				}
				if(callType == 'add_delete_user_select_user_details') {
					$('select#delete_user_select_user').html('').html(res.html_body);
				}
				if(callType == 'delete_user_and_assign_to_other_user') {
					swal({
			    		title: 'User lead is reassigne',
			      		icon: 'success',
			    	});
			    	setTimeout(function() { 
						location.reload();
				    }, 2000);
					$('select#delete_user_select_user').html('').html(res.html_body);
				}
				if(callType == 'get_department_graph_data') {
					create_highchart_for_department(res.graph_data);
				}
				$('.glo_app').hide();
			}
		},
		beforeSend: function(response){
			if(callType == 'add_delete_user_select_user_details' || callType == 'delete_user_and_assign_to_other_user') {
				$('.glo_app.add_delete_user_select_user_details').show();
			} else {
				$('.glo_app').show();
			}
		}
	});
};

function create_highchart_for_department(graph_data) {

	Highcharts.chart('department_highchart', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: 'Department Wise Employeed Count'
	    },
	    subtitle: {
	        text: ''
	    },
	    accessibility: {
	        announceNewData: {
	            enabled: true
	        }
	    },
	    xAxis: {
	        type: 'category'
	    },
	    yAxis: {
	        title: {
	            text: 'Total Number of Employees'
	        }

	    },
	    legend: {
	        enabled: false
	    },
	    plotOptions: {
	        series: {
	            borderWidth: 0,
	            dataLabels: {
	                enabled: true,
	                format: '{point.y:f}'
	            }
	        }
	    },

	    tooltip: {
	        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> Employees<br/>'
	    },

	    series: [
	        {
	            name: "OM Tubes",
	            colorByPoint: true,
	            data: graph_data
	        }
	    ]
	});
}