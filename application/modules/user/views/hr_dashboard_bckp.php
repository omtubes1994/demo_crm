<!-- begin:: Content -->
<link href="<?php echo base_url('assets/css/ticket/ticket_css.css');?>" rel='stylesheet' type='text/css'  media='all'>
<link href="<?php echo base_url('assets/css/ticket/ticket_css_2.css');?>" rel='stylesheet' type='text/css'  media='all'>

<style type="text/css">
    .my-scroll{
        border:1px solid #e1e1e1;
        height: 670px;
        width:auto;
        overflow-y: auto;
    }
</style>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid container_24 data-panel">
    <div class="wrapper pt20">
        <div class="row">
            <div class="apex_chat_right_left_side_main_wrapper" style="height:630px;">
                <div id="user_left_view" class="col-md-12 apex_left_side_wrapper_full">
                    <!-- <div id="user_left_view"  class="col-md-12 apex_left_side_wrapper_full apex_left_side_wrapper_heft"> -->
                    <div class="white_grid wrapper">
                        <div class="glo_app layer white" style="display: none;">
                            <div class="glo_app apploader large"></div>
                        </div>
                        <div class="new_lead_form_search">
                            <div class="new_lead_form_inner_search new_lead_form_inner_search12 res_apex_search_box res_apex_search_box_menu">
                                <input type="text" id="current_tab_name" value="<?php echo $call_type; ?>" hidden="hidden">
                                <abbr>
                                    <input type="text" placeholder="Search members" class="form-textbox" id="user_search">
                                </abbr>
                                <button type="button" class="btn btn-primary" id="add_employee">Add Employee</button>
                            </div>
                        </div>
                        <div class="col-md-12 scroll-remove-margin"> 
                            <div class="wrapper">
                                <div class="res_apex_chat_height content_scroller mCustomScrollbar _mCS_1" style="height:500px;">
                                    <div id="all_data">
                                        <div class="team_hevily_table_data">
                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 25%;">Username</th>
                                                        <th class="user_single_view1" style="width: 20%;"><i></i>Role</th>                         
                                                        <th style="width: 20%;"><i></i>Email ID</th>                          
                                                        <th class="user_single_view1" style="width: 20%;"><i></i>Mobile Number</th>                          
                                                        <!-- <th class="user_single_view_hide"><i></i>Modules</th> -->
                                                        <th style="width: 15%;">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="user_details">
                                                    <tr>
                                                        <td>
                                                           <a href="javascript:void(0)" class="chat_transcript_on_click" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"></a>
                                                        </td>
                                                        <td class="user_single_view1" style=""><span></span></td>
                                                        <td class="" style="">
                                                            <span></span>
                                                        </td>                        
                                                        <td class="user_single_view1" style="">
                                                            <span></span>
                                                        </td>
                                                        <!-- <td class="user_single_view_hide">
                                                            <span><?php echo 8; ?></span>
                                                            </td> -->
                                                        <td>
                                                            <abbr class="">
                                                                <a href="javascript:void(0)" class="chat_transcript_on_click" data-toggle="tooltip" data-placement="top" title="Edit user details" data-original-title="View"></a>
                                                            </abbr>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table> 
                                        </div>  
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div class="pagination-wrapper"  id= "paggination_data" style="padding: 10px 10px 10px 10px;"> 
                        <?php $this->load->view('user/paggination');?>
                        </div>
                    </div>
                </div>
                <div id="user_right_view" class="col-md-12 chat_histry_hidden_section apex_left_side_wrapper_full apex_left_side_wrapper_heft" style="display: block;">
                    <div class="glo_app layer white" style="display: none;">
                        <div class="glo_app apploader large"></div>
                    </div>
                    <div class="white_grid wrapper scroll-remove-margin">
                        <div id="single_user_data">
                        <?php $this->load->view('user/single_user_details');?>
                        </div>
                    </div>
                </div>		
            </div>
        </div>
    </div>
</div>