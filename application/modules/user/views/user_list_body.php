<?php 
	if(!empty($user_list)) {
		foreach ($user_list as $user_key => $user_list_value) {
?>	
			<tr>
				<td class="" style="">
			        <span><?php echo $user_key+1; ?></span>
			    </td>
			    <td>
			       <a  class="single_user_details_view" user_id="<?php echo $user_list_value['user_id'];?>" href="javascript:void(0)" class="chat_transcript_on_click" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><?php echo ucwords(strtolower($user_list_value['name'])); ?></a>
			    </td>
			    <td class="user_single_view1" style=""><span><?php echo (!empty($role[$user_list_value['role']])) ? $role[$user_list_value['role']] : 'No Role Present'; ?></span></td>
			    <td class="" style="">
			        <span><?php echo $user_list_value['email']; ?></span>
				</td>                        
			    <td class="user_single_view1" style="">
			        <span><?php echo $user_list_value['mobile']; ?></span>
			    </td>
				<!-- <td class="user_single_view_hide">
					<span><?php echo 8; ?></span>
			        </td> -->
			    <td>
			        <abbr class="single_user_details_view" user_id="<?php echo $user_list_value['user_id'];?>">
			            <a href="javascript:void(0)" class="chat_transcript_on_click" data-toggle="tooltip" data-placement="top" title="Edit user details" data-original-title="View"></a>
			        </abbr>
			        <abbr>
				    	<a class="single_user_graph_details_view" user_id="<?php echo $user_list_value['user_id'];?>" href="javascript:void(0)" class="chat_transcript_on_click" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="flaticon2-graphic-1"></i></a>
				    </abbr>
				    <abbr>
				    	<a class="user_to_video" user_id="<?php echo $user_list_value['user_id'];?>" href="javascript:void(0)" class="chat_transcript_on_click" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="fa fa-play-circle"></i></a>
				    </abbr>
					<abbr>
				    	<a class="module_details_access" user_id="<?php echo $user_list_value['user_id'];?>" href="javascript:void(0)" class="chat_transcript_on_click" data-toggle="tooltip" data-placement="top" title="Access" data-original-title="View"><i class="flaticon-notepad"></i></a>
				    </abbr>
			        <abbr class="delete_user" delete_user_id="<?php echo $user_list_value['user_id']; ?>" delete_role_id="<?php echo $user_list_value['role']; ?>">
			          	<a  href="javascript:void(0)" class="delete_user template-delete-icon custom-template-delete" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo"  data-placement="top" title="Delete" data-original-title="Delete"></a>
			        </abbr>
			    </td>
			</tr>
<?php
		}
	}
	?>			