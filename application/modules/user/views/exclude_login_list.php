<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <style type="text/css">
		.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
			padding-top: 3% !important;
		}
		.layer-white{
		    display: none;
		    position: fixed;
		    top: 0em !important;
		    left: 0em !important;
		    width: 100%;
		    height: 100%;
		    text-align: center;
		    vertical-align: middle;
		    background-color: rgba(255, 255, 255, 0.55);
		    opacity: 1;
		    line-height: 1;
		    -webkit-animation-fill-mode: both;
		    animation-fill-mode: both;
		    -webkit-animation-duration: 0.5s;
		    animation-duration: 0.5s;
		    -webkit-transition: background-color 0.5s linear;
		    transition: background-color 0.5s linear;
		    -webkit-user-select: none;
		    -moz-user-select: none;
		    -ms-user-select: none;
		    user-select: none;
		    will-change: opacity;
		    z-index: 9;
		}
		.div-loader{
		    position: absolute;
			top: 50%;
			left: 50%;
			margin: 0px;
			text-align: center;
			z-index: 1000;
			-webkit-transform: translateX(-50%) translateY(-50%);
			transform: translateX(-50%) translateY(-50%);
		}
		.kt-spinner:before {
		    width: 50px;
		    height: 50px;
		    margin-top: -10px;
		}

        ul.menu-tab {
            margin: 0px;
            padding: 0px;
            border: 1px solid #E7E7E7;
            overflow: hidden;
            font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
            font-size: 14px;
            line-height: 20px;
            color: #333;
        }
        ul.menu-tab li{
            width: calc(100%/4) !important;
            display: inline;
            text-align: center;
            float: left;
        }
        ul.menu-tab li.active_list{
            border-bottom: 0.40rem solid #343a40 !important;
        }
        ul.menu-tab li a{

            cursor: pointer;
            display: inline-block;
            outline: none;
            text-align: center;
            width: 100%;
            background-color: #5578eb;
            border-color: #5578eb;
            color: #ffffff;
            font-size: 18px;
            font-family: 'latomedium';
            padding: 3px 3px 20px 3px;
        }
        ul.menu-tab li a i{
            display: inline-block;
            width: 35px;
            height: 28px;
            font-style: normal;
            background-size: 100%;
            position: relative;
            top: 6px;
        }

        .user_font {
            font-size: 1rem;
            font-weight: 500;
            line-height: 1.5rem;
            -webkit-transition: color 0.3s ease;
            transition: color 0.3s ease;
            color: #000;
	    }
    </style>


    <!-- begin:: Container -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">

                <div class="kt-portlet kt-portlet--mobile">
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="kt-font-brand flaticon2-line-chart"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                USER LIST
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-wrapper">
                                <div class="kt-portlet__head-actions">
                                    <a href="javascript:;" class="btn btn-brand btn-elevate btn-icon-sm add_search_filter" button_value = "show">
                                        Add Search Filter
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body search_filter_data" style="display: none;">
                        <?php $this->load->view('user/search_filter_form');?>
                    </div>
                </div>

                <div class="kt-portlet kt-portlet--mobile">
                    <div class="kt-portlet__body">
                        <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-sm-12">
                                    <ul class="menu-tab kt-font-bolder" style="">
                                        <li class="tab_name_click sms_list active_list" tab-name="sms_list">
                                            <a>
                                                <span style="top: 6px;position: relative;">SMS List</span>
                                            </a>
                                        </li>
                                        <li class="tab_name_click whatsapp_list" tab-name="whatsapp_list" style="padding: 0px 0px 0px 10px;">
                                            <a>
                                                <span style="top: 6px;position: relative;">WhatsApp List</span>
                                            </a>
                                        </li>
                                        <li class="tab_name_click email_list" tab-name="email_list" style="padding: 0px 0px 0px 10px;">
                                            <a>
                                                <span style="top: 6px;position: relative;">Email List</span>
                                            </a>
                                        </li>
                                        <li class="tab_name_click ignore_list" tab-name="ignore_list" style="padding: 0px 0px 0px 10px;">
                                            <a>
                                                <span style="top: 6px;position: relative;">Ignore List</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-sm-12">
                                    <table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 1536px;">
                                        <thead id="list_head">
                                            <tr role="row">
                                                <th style="width: 5%;">Sr</th>
                                                <th style="width: 17%;">Name</th>
                                                <th style="width: 17%;">Number</th>
                                                <th style="width: 30%;">Email</th>
                                                <th style="width: 20%;">Designation</th>
                                                <th style="width: 10%;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="list_body">
                                            <?php $this->load->view('user/exclude_list_body');?>
                                        </tbody>
                                    </table>
                                    <div id="list_process" class="layer-white">
                                        <div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row paggination">
                                <?php $this->load->view('user/user_paggination');?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!--end::Content-->