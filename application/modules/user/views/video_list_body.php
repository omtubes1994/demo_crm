
   <div class="row">
        <?php if(!empty($video_details)){
                    foreach($video_details as $single_video_data){
                        if(in_array($single_video_data['id'],$video_access_details)){
                    ?>
            <div class="col-xl-6">
                <!--Begin::Portlet-->
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__body">

                        <!--begin::Widget -->
                        <div class="kt-widget kt-widget--user-profile-2">
                            
                            <div class="kt-widget__body">
                                <div class="col-md-12">
                                    <div class="embed-responsive embed-responsive-16by9 ">
                                        <iframe   width="514" height="279" class="play_video" id="myVideo" style="border-radius: 9px;" src="<?php echo trim($single_video_data['video_url']).'?modestbranding=1&start=10&controls=0&rel=0&disablekb=0'?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""
                                            sandbox="allow-forms allow-scripts allow-pointer-lock allow-same-origin allow-top-navigation"></iframe>
                                    </div>
                                </div>
                                <p>Playback position: <span id="demo"></span></p>
                            </div>
                        </div>

                        <!--end::Widget -->
                    </div>
                </div>

                <!--End::Portlet-->
            </div>
            
        <?php } } } ?>
    </div>