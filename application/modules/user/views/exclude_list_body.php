<?php if(!empty($login_user_list)) { ?>
    <?php foreach ($login_user_list as $login_user_list_key => $single_list_data_value) { ?>	

        <tr role="row" class="<?php echo (($login_user_list_key+1)/2 == 00)?'even':'odd'?>">

            <td class="kt-align-center"><?php echo $login_user_list_key+1; ?></td>
            <td class="kt-align"><?php echo $single_list_data_value['name']; ?></td>
            <td><?php echo $single_list_data_value['mobile']; ?></td>
            <td><?php echo $single_list_data_value['email'];?></td>
            <td><?php echo $single_list_data_value['designation'];?></td>
            
            <td>
                <div class=""  style="display:inline-flex;">
                    <div class="dropdown">                         
                        <a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md " data-toggle="dropdown">
                            <i class="flaticon2-check-mark" title="Login Type"></i>                            
                        </a>                           
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item login_type" value="sms" user_id="<?php echo $single_list_data_value['user_id'];?>">SMS</a>
                            <a class="dropdown-item login_type" value="whatsapp" user_id="<?php echo $single_list_data_value['user_id'];?>">WhatsApp</a>
                            <a class="dropdown-item login_type" value="email" user_id="<?php echo $single_list_data_value['user_id'];?>">Email</a>
                            <a class="dropdown-item login_type" value="all" user_id="<?php echo $single_list_data_value['user_id'];?>">Ignore</a>         
                        </div>
                    </div>
                    <?php if (in_array($this->session->userdata('user_id'), array(2, 65, 158))){ ?>
                        <a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_user" title="Delete" tab_name="<?php echo $tab_name;?>" user_id="<?php echo $single_list_data_value['user_id'];?>">
                            <i class="la la-trash"></i>
                        </a>
                    <?php }?>
                </div>
            </td>
        </tr>
    <?php }
} ?>