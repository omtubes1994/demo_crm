<style type="text/css">
    .my-scroll{
            border:1px solid #e1e1e1;
            height: 65vh;
            width:auto;
            overflow-y: auto;
    }
    .task_management_font {
        color: #767676;
        font-style: normal;
        font-family: 'latomedium';
        padding: 10px 20px;
        text-align: left;
    }
    .bootstrap-select > .dropdown-toggle.btn-light, .bootstrap-select > .dropdown-toggle.btn-secondary{

        border-color: #343a40 !important;
        color: #767676 !important;
        font-style: normal !important;
        font-family: 'latomedium' !important;
        padding: 10px 20px !important;
        text-align: left !important;
    }
</style>
<div class="common_heading apex_common_heading_float team_hively_heading_block">Update Users Access Details for <?php echo $user_name; ?></div>

<ul class="con_right_name apex_chat_screen_action">
    <li>
        <button type="button" class="btn btn-primary save_module_details_access" data-toggle="tooltip" data-placement="left" title="Save" user_id="<?php echo $user_id; ?>">Save</button>
    </li>
    <li class="close_right_side_details">
        <button type="button" class="btn btn-primary" data-toggle="tooltip" data-placement="left" title="Close">Close</button>
    </li>
</ul>
<div class="wrapper">
    <div class="rating_messages_table_wrapper content_scroller teamhively_right_side_section mCustomScrollbar _mCS_2 mCS_no_scrollbar res_rating_msg_height" style="height:65vh;">
        <div id="mCSB_1" class="my-scroll mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" style="max-height: none;" tabindex="0">
            <div id="mCSB_1_container" class="mCSB_container" style="position:relative; top:0; left:0;" dir="ltr">
                <div id="rating_message">
                    <div class="team_hevily_right_side_section" id="user_viw">
                        <div class="kt-portlet" style="background: #F5F5F5;">
                            <div class="kt-portlet__body" style="padding: 0px;">

                                <!--begin::Form-->
                                <form class="kt-form row kt-align-left task_management_font save_module_details_access_form" style="padding: 10px 0px 0px 0px; margin: 0px;">
                                <?php if(!empty($single_user_module_details)){ ?>
                                    <?php foreach ($single_user_module_details as $value) { ?>
                                        <div class="col-xl-12 form-group kt-align-center">
                                            <label class="kt-font-lg kt-font-bolder"><?php echo $value['name']; ?></label>
                                            <hr>
                                        </div>
                                        <div class="col-xl-12 row kt-align-center" style="padding: 0px 20px 0px 30px;">
                                            <?php foreach ($value['access_details'] as $single_access_details_array) { ?>
                                                <div class="<?php echo $single_access_details_array['class']; ?> form-group kt-align-justify" style="padding-right: 0px; padding-left: 0px;">
                                                    <label class="kt-font-lg kt-font-bolder"><?php echo $single_access_details_array['label']; ?></label>
                                                    <?php if($single_access_details_array['input_type'] == 'radio') {?>
                                                        <div class="kt-radio-inline">
                                                            <label class="kt-radio kt-font-lg kt-font-bolder kt-radio--dark">
                                                                <input type="radio" name="<?php echo $single_access_details_array['name']; ?>" value="true" <?php echo $single_access_details_array['yes_value']; ?>> Yes
                                                                <span></span>
                                                            </label>
                                                            <label class="kt-radio kt-font-lg kt-font-bolder kt-radio--dark">
                                                                <input type="radio" name="<?php echo $single_access_details_array['name']; ?>" value="false" <?php echo $single_access_details_array['no_value']; ?>> No
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    <?php }else if($single_access_details_array['input_type'] == 'checkbox'){ ?>
                                                        <div class="kt-checkbox-inline">
                                                            <?php foreach ($single_access_details_array['data'] as $single_access_details_array_data) { ?>
                                                                <label class="kt-checkbox kt-font-lg kt-font-bolder kt-checkbox--dark">
                                                                    <input type="checkbox" name="<?php echo $single_access_details_array['name']; ?>" value="<?php echo $single_access_details_array_data['name']; ?>" <?php echo $single_access_details_array_data['checked']; ?>> <?php echo $single_access_details_array_data['name']; ?>
                                                                    <span></span>
                                                                </label>
                                                            <?php } ?>
                                                        </div>
                                                    <?php }else if($single_access_details_array['input_type'] == 'dropdown'){ ?>
                                                        <div class="dropdown bootstrap-select form-control kt- dropup">
                                                            <select class="form-control kt-selectpicker kt-font-lg kt-font-bolder" name="<?php echo $single_access_details_array['name']; ?>" data-size="4" multiple data-live-search="true" tabindex="-98">
                                                                <option class="kt-font-lg kt-font-bolder" value="">Select</option>
                                                                <?php foreach ($single_access_details_array['data'] as $single_access_details_array_data) { ?>
                                                                    <option class="kt-font-lg kt-font-bolder" name="<?php echo $single_access_details_array['name']; ?>" value="<?php echo $single_access_details_array_data['value']; ?>" <?php echo $single_access_details_array_data['selected']; ?>>
                                                                        <?php echo $single_access_details_array_data['name']; ?>
                                                                    </option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        </div> 
                                    <?php } ?>
                                <?php } ?>
                                </form>

                                <!--end::Form-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>