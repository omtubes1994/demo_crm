<form class="kt-form kt-form--fit kt-margin-b-20" id="search_filter_form">
    <div class="user_font" id="search_filter_div">
        <div class="row kt-margin-b-20">
            <div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
                <label>User Name:</label>
                <select class="form-control kt-selectpicker" name="user_id" multiple data-live-search = "true">
                <option value="">Select</option>
                    <?php foreach($user_list as $single_user) {?>
                        <option value="<?php echo $single_user['user_id'];?>"
                            <?php echo (is_array($search_filter['user_id']) && in_array($single_user['user_id'], $search_filter['user_id'])) ? 'selected': '';?>>
                            <?php echo $single_user['name'];?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-lg-2 kt-margin-b-10-tablet-and-mobile">
                <label>Designation:</label>
                <select class="form-control kt-selectpicker" name="designation_id"  multiple data-live-search = "true">
                <option value="">Select</option>
                    <?php foreach($designation_list as $single_designation) {?>
                        <option value="<?php echo $single_designation['role_id'];?>"
                            <?php echo (is_array($search_filter['designation_id']) && in_array($single_designation['role_id'], $search_filter['designation_id'])) ? 'selected': '';?>>
                            <?php echo $single_designation['role_name'];?>
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="kt-separator kt-separator--md kt-separator--dashed"></div>
        <div class="row">
            <div class="col-lg-12">
                <button class="btn btn-primary btn-brand--icon search_filter_submit" type="reset">
                    <span>
                        <i class="la la-search"></i>
                        <span>Search</span>
                    </span>
                </button>
                &nbsp;&nbsp;
                <button class="btn btn-secondary btn-secondary--icon search_filter_reset" type="reset">
                    <span>
                        <i class="la la-close"></i>
                        <span>Reset</span>
                    </span>
                </button>
            </div>
        </div>
    </div>
</form>