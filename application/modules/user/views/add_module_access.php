<?php // if(!empty($single_user_details)) { ?>
	<style type="text/css">
	.my-scroll{
      border:1px solid #e1e1e1;
      height: 330px;
      width:auto;
      overflow-y: auto;
    }
</style>
	<div class="common_heading apex_common_heading_float team_hively_heading_block"> Add Module or sub module</div>
	    <ul class="con_right_name apex_chat_screen_action">
	        <!-- <li><a href="javascript:void(0)" class="con_right_side_full_screen" data-toggle="tooltip" data-placement="bottom" title="FullScreen" data-original-title="FullScreen"></a></li> -->
		    <li class="close_right_side_details">
		        <a href="javascript:void(0)" class="con_right_block_close full_screen_close1 " data-toggle="tooltip" data-placement="left" title="Close"></a>
		    </li>
	    </ul>
	    <div class="team_right_user_block">
	        <ul class="keyword_ranking_legend wrapper team_hively_legend team_hively_user_legend" id="hively_view">
	            <li class="total_user_score_right_border1" style="padding: 10px 5px 18px 2px; width: calc(100%/3);">
	                <div class="keyword_ranking_legend teamhively_legend_section">
	                <i class="lead_tracking_legend_icon teamhively_user_score_icon1"></i>
	                    <span>
	                        <abbr><?php echo count($main_module); ?><div class="team_hively_img"></div>   
	                        </abbr>
	                        <em>Total Main Module
	                        	<a href="#" class="team_hevily_score_view">
	                                <i></i>
	                                <ul class="team_hevily_score_view_content">
	                                	<?php 
	                                		foreach ($main_module as $modules_details) {
	                                	?>
		                                    <li>
		                                        <abbr><?php echo $modules_details['module_name']; ?></abbr>
		                                        <i><?php echo $modules_details['status']; ?></i>
		                                    </li>
	                                	<?php		
	                                		}
	                                	?>
	                                </ul>
	                            </a>
	                        </em>
	                    </span>
	                </div>
	            </li>
	            <li style="width: calc(100%/3);">
	                <div class="keyword_ranking_legend teamhively_legend_section">
	                    <span>
	                        <abbr style="font-size: 24px;"><input class="form-control" type="text" name="new_main_module_name" id="new_main_module_name"></abbr>
	                    	<em>New Main Module Name</em>
	                    </span>
	                </div>
	            </li>
	            <li style="width: calc(100%/3);">
	                <div class="keyword_ranking_legend teamhively_legend_section">
	                    <span>
	                        <button class="btn btn-warning add_main_module_name">Add Main Module</button>
	                    </span>
	                </div>
	            </li>
	        </ul>
	        <ul class="keyword_ranking_legend wrapper team_hively_legend team_hively_user_legend" id="hively_view">
	            <li class="total_user_score_right_border1" style="padding: 10px 5px 18px 2px; width: calc(100%/3);">
	                <div class="keyword_ranking_legend teamhively_legend_section">
	                <i class="lead_tracking_legend_icon teamhively_user_score_icon1"></i>
	                    <span>
	                        <abbr><?php echo count($sub_module); ?><div class="team_hively_img"></div>   
	                        </abbr>
	                        <em>Total Sub Module</em>
	                    </span>
	                </div>
	            </li>
	            <li style="width: calc(100%/3);">
	                <div class="keyword_ranking_legend teamhively_legend_section">
	                    <span>
	                    	<select class="form-control" id="main_module_name" name="main_module_name" style="border-radius: 10px;">
					            <option value="">Select Module Name</option>
					            <?php foreach ($main_module as $modules_details) { ?>
					            <option value="<?php echo $modules_details['module_id']; ?>"><?php echo $modules_details['module_name']; ?></option>';
					        	<?php }?>
					        </select>
	                        <abbr style="font-size: 24px;"><input class="form-control" type="text" name="new_sub_module_name" id="new_sub_module_name"></abbr>
	                    	<em>New Sub Module Name</em>
	                    </span>
	                </div>
	            </li>
	            <li style="width: calc(100%/3);">
	                <div class="keyword_ranking_legend teamhively_legend_section">
	                    <span>
	                        <button class="btn btn-warning add_sub_module_name">Add Sub Module</button>
	                    </span>
	                </div>
	            </li>
	        </ul>
	    </div>
	</div>
	<div class="wrapper">
	    <div class="common_heading apex_common_heading_float team_hively_heading_block"> Update Module Access Details </div>
	    <div class="rating_messages_table_wrapper content_scroller teamhively_right_side_section mCustomScrollbar _mCS_2 mCS_no_scrollbar res_rating_msg_height" style="height:320px;">
			<div id="mCSB_1" class="my-scroll mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" style="max-height: none;" tabindex="0">
				<div id="mCSB_1_container" class="mCSB_container" style="position:relative; top:0; left:0;" dir="ltr">
			    	<div id="rating_message"><div class="team_hevily_right_side_section" id="user_viw">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
						        <thead>
						            <tr>
						                <th>Update Main Module Name</th>
						                <th>Update Sub Module Name</th>
						            </tr>
						        </thead>
						        <tbody id="">
						        	<?php foreach($main_module as $main_module_details){ ?>
					                <tr>
					                    <td style="">
					                        <span><?php echo $main_module_details['module_name'];?></span>
					                        <abbr>
					                        	<span>
					                        		<input type="text" class="form-control" name="<?php echo 'main_module_'.$main_module_details['module_id'];?>" id="<?php echo 'main_module_'.$main_module_details['module_id'];?>" value="<?php echo $main_module_details['module_name'];?>">
					                        		<button class="btn btn-info update_main_module_name" module-id="<?php echo $main_module_details['module_id'];?>">Update</button>
					                        		<button class="btn btn-warning delete_main_module_name" module-id="<?php echo $main_module_details['module_id'];?>">Delete</button>
					                            </span>
					                        </abbr>
					                    </td>
					                    <td>
					                        <em>
					                            <i></i>
					                            <?php if(!empty($sub_module[$main_module_details['module_id']])){ ?>
					                    		<?php foreach($sub_module[$main_module_details['module_id']] as $sub_module_key => $sub_module_details){ ?>
					                            <?php if($sub_module_key < 4){ ?>
					                            <span style="width: 25%;">
					                            	<!-- <span class="title"> Add New: </span> -->
			                                        <input class="form-control" name="<?php echo 'sub_module_'.$sub_module_details['sub_module_id'];?>" id="<?php echo 'sub_module_'.$sub_module_details['sub_module_id'];?>" type="text" value="<?php echo $sub_module_details['sub_module_name'];?>">
			                                        <label for="<?php echo 'sub_module_'.$sub_module_details['sub_module_id'];?>">
			                                        	<span>
			                                        		<button class="btn btn-info update_sub_module_name" sub-module-id="<?php echo $sub_module_details['sub_module_id'];?>">Update</button>
			                                        		<button class="btn btn-warning delete_sub_module_name" sub-module-id="<?php echo $sub_module_details['sub_module_id'];?>">Delete</button>
			                                        	</span>
			                                        </label>
					                            </span>
					                        	<?php } ?>
					                        	<?php } ?>
					                        	<?php } ?>
					                        </em>
					                    </td>
					                </tr>
	                        		<?php } ?>
			                    </tbody>
							</table>
						</div>
					</div>
		    </div>
		</div>
	</div>
</div>
<?php //} ?>	