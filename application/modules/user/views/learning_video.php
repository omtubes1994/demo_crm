<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

   <!--Begin::App-->
   <div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">

       <!--Begin:: App Aside Mobile Toggle-->
       <button class="kt-app__aside-close" id="kt_user_profile_aside_close">
           <i class="la la-close"></i>
       </button>

       <!--End:: App Aside Mobile Toggle-->

       <!--Begin:: App Aside-->
       <div class="kt-grid__item kt-app__toggle kt-app__aside" id="kt_user_profile_aside">

           <!--begin:: Widgets/Applications/User/Profile1-->
           <div class="kt-portlet " style="height: 680px;">
               <div class="kt-portlet__head  kt-portlet__head--noborder"></div>
               <div class="kt-portlet__body kt-portlet__body--fit-y">

                   <!--begin::Widget -->
                   <div class="kt-widget kt-widget--user-profile-1">
                       <div class="kt-widget__head">
                           <div class="kt-widget__media">
                                <?php if(!empty($user_data['profile_pic_file_path'])){?>
                                    <img src="<?php echo base_url('assets/hr_document/profile_pic/'.$user_data['profile_pic_file_path']);?>" alt="employee_image">
                                <?php }else{ ?>
                                    <img src="<?php echo base_url('assets/media/users/default.jpg');?>" alt="default_image">
                                <?php }?>
                           </div>
                           <div class="kt-widget__content">
                               <div class="kt-widget__section">
                                   <a href="javascript:void(0);" class="kt-widget__username name">
                                        <?php if(!empty($user_data['first_name'])){?>
                                            <?php echo $user_data['first_name'],' ', $user_data['last_name']; ?>
                                            <i class="flaticon2-correct kt-font-success"></i>
                                        <?php }else{ ?>
                                            Name Not Found
                                        <?php }?>       
                                   </a>
                                   <span class="kt-widget__subtitle">
                                       <?php echo $user_data['department']; ?>
                                   </span>
                               </div>
                               <div class="kt-widget__action" style="display: none;">
                                   <button type="button" class="btn btn-info btn-sm">chat</button>&nbsp;
                                   <button type="button" class="btn btn-success btn-sm">follow</button>
                               </div>
                           </div>
                       </div>
                       <div class="kt-widget__body">
                           <div class="kt-widget__content">
                               <div class="kt-widget__info">
                                   <span class="kt-widget__label">Email:</span>
                                   <a href="javascript:void(0);" class="kt-widget__data"><?php echo $user_data['official_email']; ?></a>
                               </div>
                               <div class="kt-widget__info">
                                   <span class="kt-widget__label">Phone:</span>
                                   <a href="javascript:void(0);" class="kt-widget__data"><?php echo $user_data['official_number']; ?></a>
                               </div>
                               <div class="kt-widget__info">
                                   <span class="kt-widget__label">Location:</span>
                                   <span class="kt-widget__data"><?php echo $user_data['job_location']; ?></span>
                               </div>
                           </div>
                           <?php if(!empty($module_name)){
                                       foreach ($module_name as $key => $value) {?>
                           <div class="kt-widget__items">
                               <a href="javascript:void(0);" class="kt-widget__item update_information_tab <?php echo ($this->uri->segment(3) == $value) ? 'kt-widget__item--active':'';?> " tab_name="<?php echo $value;?>">
                                   <span class="kt-widget__section">
                                       <span class="kt-widget__icon">
                                           <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                               <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                   <polygon points="0 0 24 0 24 24 0 24" />
                                                   <path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero" />
                                                   <path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3" />
                                               </g>
                                           </svg> </span>
                                       <span class="kt-widget__desc">
                                       <?php echo $value  ; ?>
                                       </span>
                                   </span>
                               </a>
                           </div>
                       <?php } }?>
                       </div>
                   </div>

                   <!--end::Widget -->
               </div>
           </div>

           <!--end:: Widgets/Applications/User/Profile1-->
       </div>

       <!--End:: App Aside-->

       <!--Begin:: App Content-->
       <div class="kt-grid__item kt-grid__item--fluid kt-app__content">
               <?php $this->load->view('user/video_list_body');?>
           <div class="row">
               <div class="col-xl-12">
                   <div class="kt-portlet " style="height: 900px;">
                       <div class="kt-portlet__head">
                           <div class="kt-portlet__head-label">
                               <h3 class="kt-portlet__head-title">VIDEO SECTION</h3>
                           </div>
                       </div>
                      <form class="kt-form kt-form--label-right" id="employee_update_account_information">
                           <div class="kt-portlet__body">
                                <div class="kt-scroll ps ps--active-y listing video_list_data" data-scroll="true" style="height:auto ; overflow: hidden;">
                                   
                                </div>
                           </div>
                       </form>
                   </div>
               </div>
            </div>
            <div id="hr_edit_listing_table_loader" class="layer-white" style="display:none;">
                <div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
            </div>
        </div>
       <!--End:: App Content-->
   </div>

   <!--End::App-->
</div>

<!-- end:: Content -->
</div>
