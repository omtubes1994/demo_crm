<table cellpadding="5" cellspacing="0">
    <tr style="background-color: #fff;">
    <td width="50%" style="padding:5px;vertical-align: text-top; ">
    <img src="<?php echo $pdf_information['logo_path'];?>" width="180" height="50" style="padding-left: 10px;"><br/>
    <strong style="font-size: 17px;"><?php echo $pdf_information['name'];?></strong>
    <div class="left_addrs" style="font-size: 14px;color: #484545; line-height: 20px;"><?php echo $pdf_information['address'];?>
    </div>
    <table style="margin-top: -15px;">
    <tr style="background-color: #fff;">
    <td style="font-size: 14px; color: #484545; line-height: 20px;" width="50%"><?php echo $pdf_information['number'];?></td>
    </tr>
    <tr style="background-color: #fff;">
    <td style="font-size: 14px; color: #484545; line-height: 20px;" align="left"><?php echo $pdf_information['email'];?></td>
    </tr>
    </table>
    </td>
    <td width="50%"><strong style="font-size: 25px; text-align: right; line-height: 35px;">Request For Quotation</strong><br/>
    <table style="line-height: 22px;">
    <tr style="background-color: #e4e1e1; border-bottom: 1px solid;">
    <td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;">RFQ #:</strong></td>
    <td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;">Date : </strong></td>
    </tr>
    <tr>
    <td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;"><?php echo $rfq_data['rfq_no'];?></td>
    <td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;"><?php echo date('d-m-Y', strtotime($rfq_data['entered_on']));?></td>
    </tr>
    <tr style="background-color: #e4e1e1;">
    <td colspan="2" style="padding:5px;vertical-align: text-top;"><strong style="font-size: 12px;"> Seller:</strong></td>
    </tr>
    <tr>
    <td colspan="2" style="padding:5px;vertical-align: text-top; font-family: courier;"><strong><?php echo $vendor_data['vendor_name'];?></strong>
    <div class="left_addrs" style="font-size: 11px;color: #484545; line-height: 17px; font-family: courier;"><?php echo $country_details['name'];?><br/><?php echo $vendor_data['vendor_name']?>
    </div>
    </td>
    </tr>
    <tr style="background-color: #e4e1e1;">
    <td colspan="2" style="padding:5px;vertical-align: text-top;"><strong style="font-size: 12px;">Our Reference :</strong></td>
    </tr>
    <tr>
    <td colspan="2" style="font-family: courier; font-size: 11px;color: #484545;"><?php echo $rfq_data['reference'];?></td>
    </tr>   
    </table>
    </td>
    </tr>
    <tr>
    <td colspan="2"></td>
    </tr>
    <tr>
    <td colspan="2" style="padding:5px;vertical-align: text-top; font-family: courier;"><strong style="font-size: 16px;">URGENT</strong><br/><br/>
			Dear <?php echo $vendor_data['vendor_name'];?>,<br/><br/>
			please submit your quotation according to the following specification<br/>
			- price terms: as specified, including standard packing<br/>
			- validity of quotation: 90 days minimum<br/>
			- delivery time<br/>
			- resales discount<br/>
			- estimated total weight/ -volume<br/><br/>
			Thank you very much.
			</td>
    </tr><br>
    <tr>
    <td colspan="2"><table cellspacing="0" cellpadding="10" border="0">
    <thead>
    <tr style="background-color: #e4e1e1;">
    <td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="6%;">#</td>
    <td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="63%">Item Description</td>
    <td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="15%">Quantity</td>
    <td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="15%">Unit</td>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($line_item_data as $line_item_key => $single_line_item) {
                $bg_color = '';
                if(($line_item_key)%2 != 0){
                    $bg_color = '#e4e1e1;';
                }
                $description = str_replace(['<p>', '</p>', '<br />'], ['<br>', '', ''], $single_line_item['description']);
                ?>
            <tr style="background-color: <?php echo $bg_color;?>; font-size: 11px; font-family: courier;">
                <td style="text-align: right;"><?php echo ++$line_item_key;?></td>
                <td><?php echo $description; ?></td>

                <td style="text-align: right;">
                    <?php if(strpos($single_line_item['quantity'], '.') === false){
                        $single_line_item['quantity'] = $single_line_item['quantity'].".00";
                    } ?>
                    <?php echo $single_line_item['quantity'];?>
                </td>
                <td><?php echo ucwords(strtolower($single_line_item['unit_value']));?>.</td>
            </tr>
        <?php }?>
    </tbody>
    </table>
    </td>
    </tr>
    <tr>
    <td>
    <table cellspacing="0" cellpadding="3" border="0">
    <tr>
    <td><strong>Additional Notes</strong></td>
    </tr>
    <tr>
    <td style="font-family: courier; font-size: 11px;">Please provide reason/explanation for possible
        deviation/discrepancy to our enquiry-specification and support<br><br><?php echo $pdf_information['name'];?> has conducted its
        business for decades with integrity. Therefore,
        we also expect our suppliers to ensure our
        values. For additional details, please contact
        us.<?php //echo $additional_comment;?>
    </td>
    </tr>
    </table>
    </td>
    <!-- </tr>
    <tr> -->
    <td align="center">
    Thank you for your business<br/>
    For <?php echo $pdf_information['name'];?><br/><br/>
    <img src="<?php echo $pdf_information['stamp_path'];?>" height="100px" weight="100px" /><br/><br/>
    <table>
    <tr>
    <td width="26%" rowspan="3"></td>
    <td align="left" width="74%">Name : <span style="font-family: courier;"><?php echo $user_data['name'];?></span></td>
    </tr>
    <tr>
    <td align="left">Email : <span style="font-family: courier;"><?php echo $user_data['email'];?></span></td>
    </tr>
    <tr>
    <td align="left">Mobile : <span style="font-family: courier;"><?php echo $user_data['mobile'];?></span></td>
    </tr>
    </table>
    </td>
    </tr>
</table>




