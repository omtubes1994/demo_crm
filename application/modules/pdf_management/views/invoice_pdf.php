<table cellpadding="5" cellspacing="0">
    <tr style="background-color: #fff;">
    <td width="50%" style="padding:5px;vertical-align: text-top; ">
    </td>
    <td width="50%">
    <table>
    <tr style="background-color: #fff;">
        <td width="50%" style="padding:5px;vertical-align: text-top; ">
        </td>
        <td width="50%"><strong style="font-size: 25px; text-align: left; line-height: 35px;">Invoice</strong>
        </td>
    </tr> 
    </table>
    <table style="line-height: 22px;">
    <tr style="background-color: #e4e1e1; border-bottom: 1px solid;">
    <td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;">Invoice No:</strong></td>
    <td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;">Date : </strong></td>
    </tr>
    <tr>
    <td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;"><?php echo $invoice_data['invoice_no'];?></td>
    <td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;"><?php echo date('d-m-Y', strtotime($invoice_data['invoice_date']));?></td>
    </tr>    
    <tr style="background-color: #e4e1e1;">
    <td colspan="2" style="padding:5px;vertical-align: text-top;"><strong style="font-size: 12px;"> Customer:</strong></td>
    </tr>
    <tr>
    <td colspan="2" style="padding:5px;vertical-align: text-top; font-family: courier;"><strong><?php echo $client_details['name'];?></strong>
    <div class="left_addrs" style="font-size: 11px;color: #484545; line-height: 17px; font-family: courier;"><?php echo $country_details['name'];?><br/><?php echo $member_details['member_name']?>
    </div>
    </td>
    </tr>
    <tr style="background-color: #e4e1e1;">
    <td colspan="2" style="padding:5px;vertical-align: text-top;"><strong style="font-size: 12px;">Purcahse Order :</strong></td>
    </tr>
    <tr>
    <td colspan="2" style="font-family: courier; font-size: 11px;color: #484545;"><?php echo $invoice_data['remarks'];?></td>
    </tr>   
    </table>
    </td>
    </tr>
    <tr>
    <td colspan="2"></td>
    </tr>
    <tr>
    <td colspan="2"><table cellspacing="0" cellpadding="10" border="0">
    <thead>
    <tr style="background-color: #e4e1e1;">
    <td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="7%;">#</td>
    <td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="50%">Item Description</td>
    <td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="13%">Qty</td>
    <td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="15%">Price</td>
    <td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="15%">Total</td>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($invoice_details as $invoice_key => $single_invoice_details) {
            $bg_color = '';
            if(($invoice_key)%2 != 0){
                $bg_color = '#e4e1e1;';
            } ?>

            <tr style="background-color: <?php echo $bg_color;?>; font-size: 11px; font-family: courier;">
                <td style="text-align: right;"><?php echo ++$invoice_key;?></td>
                <td style="text-align: left;"><?php echo htmlentities($single_invoice_details['description']);?></td>

                <td style="text-align: center;">
                    <?php if(strpos($single_invoice_details['quantity'], '.') === false){
                        $single_invoice_details['quantity'] = $single_invoice_details['quantity'].".00";
                    } ?>
                    <?php echo $single_invoice_details['quantity'];?>
                </td>

                <?php if((int)$single_invoice_details['rate'] > 0 || (int)$single_invoice_details['price'] > 0) {

                    if(strpos($single_invoice_details['rate' ], '.') === false){
                        $single_invoice_details['rate' ] = $single_invoice_details['rate' ].".00";
                    } ?>

                    <td style="text-align: center;"><?php echo $currency_details['currency_icon'].$single_invoice_details['rate'];?></td>
                    <td style="text-align: center;"><?php echo $currency_details['currency_icon'].$single_invoice_details['price']; ?></td>

                <?php }else { ?>

                        <td style="text-align: center;">REGRET</td>
                        <td style="text-align: center;">REGRET</td>		
                <?php } ?>
            </tr>
        <?php }?>
    </tbody>
    </table>
    </td>
    </tr>
    <br/>
    <tr>
    <td><strong>Total in words:</strong><br/><span style="font-family: courier; font-size: 11px;"><?php echo $grand_total_words;?></span><br/>
    <hr/>
    </td>
    <td>
    <table cellspacing="0" cellpadding="10">
        <tr>
            <td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;" width="50%" >Net Total</td>
            <td style="font-size: 12px; font-family: courier; border: solid 1px grey;" width="50%"><?php echo $currency_details['currency_icon'].$net_total;?></td>
        </tr>
        <?php if($invoice_data['freight_charge'] > 0){
            if(strpos($invoice_data['freight_charge'], '.') === false){
                $invoice_data['freight_charge'] = $invoice_data['freight_charge'].'.00';
            } ?>
            <tr>
                <td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Freight Charges</td>
                <td style="font-size: 12px; font-family: courier; border: solid 1px grey;"><?php echo $currency_details['currency_icon'].$invoice_data['freight_charge'];?></td>
            </tr>
        <?php }?>
        <?php if($invoice_data['other_charge'] > 0){
            if(strpos($invoice_data['other_charge'], '.') === false){
                $invoice_data['other_charge'] = $invoice_data['other_charge'].'.00';
            } ?>
            <tr>
                <td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Other Charges</td>
                <td style="font-size: 12px; font-family: courier; border: solid 1px grey;"><?php echo $currency_details['currency_icon'].$invoice_data['other_charge'];?></td>
            </tr>
        <?php }?>
        <?php if($invoice_data['discount'] > 0){
            if(strpos($invoice_data['discount'], '.') === false){
                $invoice_data['discount'] = $invoice_data['discount'].'.00';
            } ?>
            <tr>
                <td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Discount</td>
                <td style="font-size: 12px; font-family: courier; border: solid 1px grey;"><?php echo $currency_details['currency_icon'].$invoice_data['discount'];?></td>
            </tr>
        <?php }?>
         <?php if(!empty($invoice_data['grand_total'] && $invoice_data['grand_total'] > 0)){
            if(strpos($invoice_data['grand_total'], '.') === false){
                $invoice_data['grand_total'] = $invoice_data['grand_total'].'.00';
            } ?>

            <tr>
                <td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Grand Total</td>
                <td style="font-size: 12px; font-family: courier; border: solid 1px grey;"><?php echo $currency_details['currency_icon'].$grand_total;?></td>
            </tr>
        <?php  }?>
    </table>
    </td>
    </tr>
</table>




