<?php 
Class Pdf_management_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$CI = &get_instance();
	}


	public function getPortName($port_type, $delivery_type, $country){
		$res = $this->db->get_where('ports', array('port_type' => $port_type, 'delivery_type' => $delivery_type, 'country' => $country))->row_array();
		return $res['port_name'];
	}
}
