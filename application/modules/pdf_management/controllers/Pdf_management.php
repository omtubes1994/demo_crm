<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdf_management extends MX_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('user_id')){
			redirect('login', 'refresh');
			exit;
		}
		
		error_reporting(0);
		$this->load->model('pdf_management_model');
		$this->load->model('common/common_model');
	}
	
	public function quotation_pdf($quote_id){

		if(!empty($quote_id)){

			$data = $this->get_pdf_information($quote_id);
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";die;

		$this->load->library('Pdf');
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// set document information

		// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, NULL, '', array(0,0,0), array(255,255,255));
		$pdf->SetFooterData(array(0,0,0), array(255,255,255));

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		// set margins
		$pdf->SetMargins(10, PDF_MARGIN_TOP-10, 10);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER-5);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM-15);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------
		
		// set font
		$pdf->SetFont('helvetica', '', 10);

		// add a page
		$pdf->AddPage();

		$html= $this->load->view('pdf_management/quotation_pdf', $data, true);
		// output the HTML content
		$pdf->writeHTML($html, true, true, true, false, '<ur><li>L : left align</li></ul>');
		//Close and output PDF document

		if(!empty($data['quotation_data']['quote_no']) && !empty($data['rfq_details']['rfq_no'])){

			$pdf->Output(str_replace('/', '-', $data['quotation_data']['quote_no']).'.pdf', 'I');

		}else if(!empty($data['quotation_data']['quote_no']) && empty($data['rfq_details']['rfq_no'])){

			$pdf->Output(str_replace('/', '-', $data['quotation_data']['quote_no']).'.pdf', 'I');

		}else if(empty($data['quotation_data']['quote_no']) && !empty($data['rfq_details']['rfq_no'])){

			$pdf->Output(str_replace('/', '-', $data['rfq_details']['rfq_no']).'.pdf', 'I');
		}
	}

	public function proforma_pdf($quote_id){

		if(!empty($quote_id)){

			$data = $this->get_pdf_information($quote_id);
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";die;

		$this->load->library('Pdf');
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// set document information

		// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, NULL, '', array(0,0,0), array(255,255,255));
		$pdf->SetFooterData(array(0,0,0), array(255,255,255));

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		// set margins
		$pdf->SetMargins(10, PDF_MARGIN_TOP-10, 10);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER-5);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM-15);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------
		
		// set font
		$pdf->SetFont('helvetica', '', 10);

		// add a page
		$pdf->AddPage();
		
		$html= $this->load->view('pdf_management/proforma_pdf', $data, true);
		// output the HTML content

		$pdf->writeHTML($html, true, true, true, false, '<ur><li>L : left align</li></ul>');
		//Close and output PDF document
		$pdf->Output(str_replace('/', '-', $data['quotation_data']['proforma_no']).'.pdf', 'I');

	}

	public function rfq_vendor_pdf(){

		$rfq_id = $this->uri->segment(3, 0);
		$vendor_id = $this->uri->segment(4, 0);
		if(!empty($rfq_id) && !empty($vendor_id)){

			$rfq_to_vendor_data = $this->common_model->get_dynamic_data_sales_db('*', array('rfq_id'=> $rfq_id, 'vendor_id'=> $vendor_id), 'rfq_to_vendor');
			foreach($rfq_to_vendor_data as $single_rfq_to_vendor_data){

				$data['line_item_data'][] = $this->common_model->get_dynamic_data_sales_db('*', array('quotation_dtl_id'=> $single_rfq_to_vendor_data['quotation_mst_id']), 'quotation_dtl','row_array');
			}

			$data['rfq_data'] = $this->common_model->get_dynamic_data_sales_db('*', array('rfq_mst_id'=> $rfq_id), 'rfq_mst','row_array');
			$data['vendor_data'] = $this->common_model->get_dynamic_data_sales_db('*', array('vendor_id'=> $vendor_id), 'vendors','row_array');
			if(!empty($data['vendor_data'])){

				$data['country_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('id'=> $data['vendor_data']['country']), 'country_mst','row_array');
			}

			if(!empty($data['rfq_data'])){

				$data['user_data'] = $this->common_model->get_dynamic_data_sales_db('*', array('user_id'=> $data['rfq_data']['assigned_to']), 'users','row_array');

				if($data['rfq_data']['type'] == 'om'){

					$data['pdf_information'] = $this->common_model->get_dynamic_data_sales_db('*', array('status' => 'Active', 'type'=> 'om'), 'pdf_information', 'row_array');
				}else if($data['rfq_data']['type'] == 'zen'){

					$data['pdf_information'] = $this->common_model->get_dynamic_data_sales_db('*', array('status' => 'Active', 'type'=> 'zen'), 'pdf_information', 'row_array');

				}else if($data['rfq_data']['type'] == 'in'){

					$data['pdf_information'] = $this->common_model->get_dynamic_data_sales_db('*', array('status' => 'Active', 'type'=> 'in'), 'pdf_information', 'row_array');
				}
			}
			// echo "<pre>";print_r($data);echo"</pre><hr>";die;
		}

		$this->load->library('Pdf');
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// set document information

		// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, NULL, '', array(0,0,0), array(255,255,255));
		$pdf->SetFooterData(array(0,0,0), array(255,255,255));

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		// set margins
		$pdf->SetMargins(10, PDF_MARGIN_TOP-10, 10);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER-5);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM-15);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('helvetica', '', 10);

		// add a page
		$pdf->AddPage();

		$html= $this->load->view('pdf_management/rfq_vendor_pdf', $data, true);
		// output the HTML content
		$pdf->writeHTML($html, true, true, true, false, '<ur><li>L : left align</li></ul>');
		//Close and output PDF document
		$pdf->Output(str_replace('/', '-', $data['rfq_data']['rfq_no']).'.pdf', 'I');

	}

	public function invoice_pdf($invoice_id){

		if(!empty($invoice_id)){

			$data = $this->get_invoice_pdf_information($invoice_id);
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";die;

		$this->load->library('Pdf');
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// set document information

		// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, NULL, '', array(0,0,0), array(255,255,255));
		$pdf->SetFooterData(array(0,0,0), array(255,255,255));

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		// set margins
		$pdf->SetMargins(10, PDF_MARGIN_TOP-10, 10);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER-5);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM-15);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('helvetica', '', 10);

		// add a page
		$pdf->AddPage();

		$html= $this->load->view('pdf_management/invoice_pdf', $data, true);
		// output the HTML content
		$pdf->writeHTML($html, true, true, true, false, '<ur><li>L : left align</li></ul>');
		//Close and output PDF document
		$pdf->Output(str_replace('/', '-', $data['invoice_data']['invoice_no']).'.pdf', 'I');

	}

	private function get_pdf_information($quote_id){

		if (!empty($quote_id)) {

			$data['quotation_data'] = $this->common_model->get_dynamic_data_sales_db('*', array('quotation_mst_id'=> $quote_id), 'quotation_mst','row_array');

			$data['quotation_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('quotation_mst_id'=> $quote_id), 'quotation_dtl','result_array');

			if (!empty($data['quotation_data'])) {

				$data['rfq_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('rfq_mst_id' =>  $data['quotation_data']['rfq_id']), 'rfq_mst', 'row_array');

				$data['client_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('id'=> $data['quotation_data']['client_id']),'customer_mst','row_array');

				$data['member_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('comp_dtl_id'=> $data['quotation_data']['member_id']),'customer_dtl','row_array');

				$data['transport_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('mode_id'=> $data['quotation_data']['transport_mode']),'transport_mode','row_array');

				$data['payment_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('term_id'=> $data['quotation_data']['payment_term']),'payment_terms','row_array');

				$data['delivery_to_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('delivery_id'=> $data['quotation_data']['delivered_through']),'delivery','row_array');

				$data['delivery_time_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('dt_id'=> $data['quotation_data']['delivery_time']),'delivery_time','row_array');

				$data['origin_country_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('country_id'=> $data['quotation_data']['origin_country']),'origin_country','row_array');

				$data['currency_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('currency_id'=> $data['quotation_data']['currency']),'currency','row_array');

				if($data['quotation_data']['currency'] == 3){

					$data['currency_details']['currency_icon'] = '<img src="https://crm.omtubes.com/assets/media/rupee.png" width="10px">';
				}
				$data['validity_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('validity_id'=> $data['quotation_data']['validity']),'validity','row_array');

				$data['mtc_type_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('mtc_id'=> $data['quotation_data']['mtc_type']),'mtc_type','row_array');

				$data['country_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('id'=> $data['client_details']['country_id']),'country_mst','row_array');

				$data['region_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('id'=> $data['client_details']['region_id']),'region_mst','row_array');

				$data['assigned_to_person'] = $this->common_model->get_dynamic_data_sales_db('*', array('user_id'=> $data['quotation_data']['assigned_to']),'users','row_array');

				$unit_list = array_column($this->common_model->get_dynamic_data_sales_db('unit_id, unit_value', array('status' => 'Active'), 'units'), 'unit_value', 'unit_id');
				$unit_list['']='';
				$unit_list[0]='';

				foreach ($data['quotation_details'] as $quotation_key => $single_quotation_details){

					$data['quotation_details'][$quotation_key]['unit_value'] = $unit_list[ $single_quotation_details['unit']];
				}
			}
		}

		$dec_text = 'paise';
		if ($data['currency_details']['currency'] != 'INR') {
			$dec_text = 'cents';
		}

		$data['grand_total_words'] = $data['currency_details']['currency']." : ".$this->numberTowords($data['quotation_data']['grand_total'], $dec_text);

		$data['additional_comment'] = '';
		if($data['quotation_data']['additional_comment'] != ''){
			$data['additional_comment'] = '<br/><br/>'.$data['quotation_data']['additional_comment'];
		}

		$data['net_total'] = (!empty($data['quotation_data'])) ? $data['quotation_data']['net_total'] : 00;
		if(strpos($data['net_total'], '.') === false){
			$data['net_total'] = $data['net_total'].'.00';
		}

		$data['grand_total'] = (!empty($data['quotation_data'])) ? $data['quotation_data']['grand_total'] : 00;
		if(strpos($data['quotation_data']['grand_total'], '.') === false){
			$data['grand_total'] = $data['quotation_data']['grand_total'].'.00';
		}

		if($data['quotation_data']['stage'] == 'draft'){
			if($this->session->userdata('quotation_access')['quotation_pdf_before_pulish_line_item_amount_access']){

				$data['before_line_item_amount_access'] = true;
			}
			if($this->session->userdata('quotation_access')['quotation_pdf_before_publish_total_amount_access']){
				$data['before_total_amount_access'] = true;
			}
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;

		//client_type
		if($data['quotation_data']['client_type'] == 'omtubes'){

			$data['pdf_information'] = $this->common_model->get_dynamic_data_sales_db('*', array('status' => 'Active', 'type'=> 'om'), 'pdf_information', 'row_array');
		}else if($data['quotation_data']['client_type'] == 'zengineer'){

			$data['pdf_information'] = $this->common_model->get_dynamic_data_sales_db('*', array('status' => 'Active', 'type'=> 'zen'), 'pdf_information', 'row_array');

		}else if($data['quotation_data']['client_type'] == 'instinox'){

			$data['pdf_information'] = $this->common_model->get_dynamic_data_sales_db('*', array('status' => 'Active', 'type'=> 'in'), 'pdf_information', 'row_array');
		}

		if($this->session->userdata('role') == 5 || $this->session->userdata('role') == 16){
			$this->common_model->update_data_sales_db('quotation_mst', array('is_new' => NULL), array('quotation_mst_id' => $quote_id));
		}
		if(($data['delivery_to_details']['delivery_name'] == 'CFR Port Name' || $data['delivery_to_details']['delivery_name'] == 'CIF Port Name') && $data['transport_details']['mode'] == 'Sea Worthy'){
			$port_name = $this->pdf_management_model->getPortName('CIF', 'sea', $data['country_details']['name']);
			if($data['delivery_to_details']['delivery_name'] == 'CFR Port Name'){
				$data['delivery_to_details']['delivery_name'] = 'CFR '.$port_name;
			} else if($data['delivery_to_details']['delivery_name'] == 'CIF Port Name'){
				$data['delivery_to_details']['delivery_name'] = 'CIF '.$port_name;
			} else {
				
				$data['delivery_to_details']['delivery_name'] = $port_name;
			}
		}

		if(!empty($data['quotation_data']['quote_no']) && !empty($data['rfq_details']['rfq_no'])){

			$data['type_name'] = "Quote #";
			$data['type_value'] = $data['quotation_data']['quote_no'];
		}else if(!empty($data['quotation_data']['quote_no']) && empty($data['rfq_details']['rfq_no'])){

			$data['type_name'] = "Quote #";
			$data['type_value'] = $data['quotation_data']['quote_no'];

		}else if(empty($data['quotation_data']['quote_no']) && !empty($data['rfq_details']['rfq_no'])){

			$data['type_name'] = "RFQ #";
			$data['type_value'] = $data['rfq_details']['rfq_no'];
		}

		return $data;
	}

	private function numberTowords($num, $dec_text){

		$ones = array(
			'' =>"ZERO",
			0 =>"ZERO",
			1 => "ONE",
			2 => "TWO",
			3 => "THREE",
			4 => "FOUR",
			5 => "FIVE",
			6 => "SIX",
			7 => "SEVEN",
			8 => "EIGHT",
			9 => "NINE",
			10 => "TEN",
			11 => "ELEVEN",
			12 => "TWELVE",
			13 => "THIRTEEN",
			14 => "FOURTEEN",
			15 => "FIFTEEN",
			16 => "SIXTEEN",
			17 => "SEVENTEEN",
			18 => "EIGHTEEN",
			19 => "NINETEEN",
			"014" => "FOURTEEN"
		);
		$tens = array( 
			0 => "ZERO",
			1 => "TEN",
			2 => "TWENTY",
			3 => "THIRTY", 
			4 => "FORTY", 
			5 => "FIFTY", 
			6 => "SIXTY", 
			7 => "SEVENTY", 
			8 => "EIGHTY", 
			9 => "NINETY" 
		); 
		$hundreds = array( 
			"HUNDRED", 
			"THOUSAND", 
			"MILLION", 
			"BILLION", 
			"TRILLION", 
			"QUARDRILLION" 
		); /*limit t quadrillion */
		$num = number_format($num,2,".",","); 
		$num_arr = explode(".",$num); 
		$wholenum = $num_arr[0]; 
		$decnum = $num_arr[1]; 
		$whole_arr = array_reverse(explode(",",$wholenum)); 
		krsort($whole_arr,1); 
		$rettxt = ""; 
		foreach($whole_arr as $key => $i){
			
		while(substr($i,0,1)=="0")
				$i=substr($i,1,5);
		if($i < 20){ 
		/* echo "getting:".$i; */
		$rettxt .= $ones[$i]; 
		}elseif($i < 100){ 
		if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)]; 
		if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)]; 
		}else{ 
		if(substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
		if(substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)]; 
		if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)]; 
		} 
		if($key > 0){ 
		$rettxt .= " ".$hundreds[$key]." "; 
		}
		} 
		if($decnum > 0){
			$rettxt .= " and ";
			if($decnum < 20){
				if(!empty($ones[$decnum])) {
					$rettxt .= $ones[$decnum];
				}
			}elseif($decnum < 100){
				$rettxt .= $tens[substr($decnum,0,1)];
				$rettxt .= " ".$ones[substr($decnum,1,1)];
			}

			$rettxt .= " ".$dec_text;
		}
		return $rettxt;
	}

	private function get_invoice_pdf_information($invoice_id){

		if (!empty($invoice_id)) {

			$data['invoice_data'] = $this->common_model->get_dynamic_data_sales_db('*', array('invoice_mst_id'=> $invoice_id), 'invoice_mst','row_array');

			$data['invoice_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('invoice_mst_id'=> $invoice_id), 'invoice_dtl','result_array');

			if (!empty($data['invoice_data'])) {

				$data['client_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('id'=> $data['invoice_data']['company_id']),'customer_mst','row_array');

				$data['member_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('comp_dtl_id'=> $data['invoice_data']['member_id']),'customer_dtl','row_array');

				$data['currency_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('currency_id'=> $data['invoice_data']['currency_id']),'currency','row_array');

				if($data['invoice_data']['currency_id'] == 3){

					$data['currency_details']['currency_icon'] = '<img src="https://crm.omtubes.com/assets/media/rupee.png" width="10px">';
				}
				$data['country_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('id'=> $data['client_details']['country_id']),'country_mst','row_array');

				$data['region_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('id'=> $data['client_details']['region_id']),'region_mst','row_array');

			}
		}

		$dec_text = 'paise';
		if ($data['currency_details']['currency'] != 'INR') {
			$dec_text = 'cents';
		}

		$data['grand_total_words'] = $data['currency_details']['currency']." : ".$this->numberTowords($data['invoice_data']['grand_total'], $dec_text);


		$data['net_total'] = (!empty($data['invoice_data'])) ? $data['invoice_data']['net_total'] : 00;
		if(strpos($data['net_total'], '.') === false){
			$data['net_total'] = $data['net_total'].'.00';
		}

		$data['grand_total'] = (!empty($data['invoice_data'])) ? $data['invoice_data']['grand_total'] : 00;
		if(strpos($data['invoice_data']['grand_total'], '.') === false){
			$data['grand_total'] = $data['invoice_data']['grand_total'].'.00';
		}

		return $data;
	}

}