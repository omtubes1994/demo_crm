jQuery(document).ready(function() {

	ajax_call_function({call_type: 'pq_status_highchart', status: 'pending'}, 'pq_status_highchart');
	ajax_call_function({call_type: 'pq_status_highchart', status: 'approved'}, 'pq_status_highchart');
})

function ajax_call_function(data, callType, url = "<?php echo base_url('pq/ajax_function'); ?>") {

	$.ajax({
		type: 'POST',
		data: data,
		url: url,
		dataType: 'JSON',
		success: function(res){
			if(res.status == 'successful') {
				if(callType == 'pq_status_highchart') {
					pq_status_highchart(res.pq_status_highchart_data);
				}
			}
		},
		beforeSend: function(response){
			
		}
	});
};

function pq_status_highchart (highchart_data) {
	var status_name = "<?php echo ucwords(str_replace("_", " ", $this->uri->segment(2)));?>";
	Highcharts.chart('pq_status_highchart', {
		// Create the chart
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: ''
	    },
	    subtitle: {
	        text: ''
	    },
	    accessibility: {
	        announceNewData: {
	            enabled: true
	        }
	    },
	    xAxis: {
	        type: 'category'
	    },
	    yAxis: {
	        title: {
	            text: status_name+' Count'
	        }

	    },
	    legend: {
	        enabled: false
	    },
	    plotOptions: {
	        series: {
	            borderWidth: 0,
	            dataLabels: {
	                enabled: true,
	                format: '{point.y}'
	            }
	        }
	    },

	    tooltip: {
	        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	        pointFormat: '<span style="color:{point.color}">{point.name}</span>:<b>{point.y}</b><br/>'
	    },

	    series: [
	        {
	            name: status_name,
	            colorByPoint: true,
	            data: highchart_data
	        }
	    ]
	});
}