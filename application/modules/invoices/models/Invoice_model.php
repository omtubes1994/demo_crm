<?php 
Class Invoice_model extends CI_Model{

	function __construct(){
		parent::__construct();
		// $this->db = $this->load->database('crm_sales', true); 
	}

	public function transer_data() {
		
		$invoice_mst_data = $this->db->get_where('invoice_mst', array('is_deleted'=> NULL, 'invoice_mst_id <'=> 2224), 0, 0)->result_array();
		// $invoice_mst_data = $this->db->get_where('invoice_mst', array('is_deleted'=> NULL, 'invoice_mst_id <'=> 2224, 'invoice_mst_id'=> 2142), 0, 0)->result_array();
		echo "<pre>";print_r($invoice_mst_data);echo"</pre><hr>";die('debug');
		foreach($invoice_mst_data as $invoice_details) {
			$current_gross = $invoice_details['net_total'];

			if($invoice_details['net_total'] != $invoice_details['grand_total']) {

				// echo (int)$invoice_details['discount'],"<hr>";
				// echo "<pre>";var_dump(empty((int)$invoice_details['discount']));echo"</pre><hr>";exit; 
				if(!empty((int)$invoice_details['discount']) || !empty((int)$invoice_details['freight_charge']) || !empty((int)$invoice_details['other_charge'])) {

					if(!empty((int)$invoice_details['discount'])) {
						$current_gross = ($current_gross - $invoice_details['discount']); 
						$discount_details = $this->db->get_where('invoice_order_charges_information', array('status'=>'Active', 'invoice_id'=>$invoice_details['invoice_mst_id'], 'charge_name'=>'discount'))->row_array();	
						if(empty($discount_details)) {
							$this->db->insert(
										'invoice_order_charges_information',
										array(
											'invoice_id'=>$invoice_details['invoice_mst_id'],
											'charge_name'=>'discount',
											'charge_value' => $invoice_details['discount'],
											'charge_deducted_or_added'=>'Deducted',
											'gross_total_after_charge'=>$current_gross
										)
									);
						}else {
							$this->db->update(
										'invoice_order_charges_information',
										array(
											'charge_value' => $invoice_details['discount'],
											'gross_total_after_charge'=>$current_gross
										),
										array(
											'id'=>$discount_details['id'],
										)
									);
						}
					}
					if(!empty((int)$invoice_details['freight_charge'])) {
						$current_gross = ($current_gross + $invoice_details['freight_charge']); 
						$freight_charge_details = $this->db->get_where('invoice_order_charges_information', array('status'=>'Active', 'invoice_id'=>$invoice_details['invoice_mst_id'], 'charge_name'=>'freight_charge'))->row_array();	
						if(empty($freight_charge_details)) {
							$this->db->insert(
										'invoice_order_charges_information',
										array(
											'invoice_id'=>$invoice_details['invoice_mst_id'],
											'charge_name'=>'freight_charge',
											'charge_value' => $invoice_details['freight_charge'],
											'charge_deducted_or_added'=>'Added',
											'gross_total_after_charge'=>$current_gross
										)
									);
						}else {
							$this->db->update(
										'invoice_order_charges_information',
										array(
											'charge_value' => $invoice_details['freight_charge'],
											'gross_total_after_charge'=>$current_gross
										),
										array(
											'id'=>$freight_charge_details['id'],
										)
									);
						}
					}
					if(!empty((int)$invoice_details['other_charge'])) {
						$current_gross = ($current_gross + $invoice_details['other_charge']); 
						$other_charge_details = $this->db->get_where('invoice_order_charges_information', array('status'=>'Active', 'invoice_id'=>$invoice_details['invoice_mst_id'], 'charge_name'=>'other_charge'))->row_array();	
						if(empty($other_charge_details)) {
							$this->db->insert(
										'invoice_order_charges_information',
										array(
											'invoice_id'=>$invoice_details['invoice_mst_id'],
											'charge_name'=>'other_charge',
											'charge_value' => $invoice_details['other_charge'],
											'charge_deducted_or_added'=>'Added',
											'gross_total_after_charge'=>$current_gross
										)
									);
						}else {
							$this->db->update(
										'invoice_order_charges_information',
										array(
											'charge_value' => $invoice_details['other_charge'],
											'gross_total_after_charge'=>$current_gross
										),
										array(
											'id'=>$other_charge_details['id'],
										)
									);
						}
					}
				} else {
					$this->db->update(
										'invoice_mst',
										array(
											'grand_total' => $invoice_details['net_total']
										),
										array(
											'invoice_mst_id'=>$invoice_details['invoice_mst_id'],
										)
									);
				}
			}	
		}
			die('done one process');
	}
	function insertInvoice($table, $data){
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	function updateInvoice($table, $data, $where){
		$this->db->update($table, $data, $where);
	}

	function getInvoiceList($start, $length, $where, $order, $dir, $searchByYear='all'){
		$this->db->select('i.*, c.client_name, m.name, l.lookup_value country, DATE_FORMAT(i.invoice_date, "%d-%b") invoice_date');
		$this->db->join('clients c', 'c.client_id = i.company_id', 'inner');
		$this->db->join('members m', 'm.member_id = i.member_id', 'left');
		$this->db->join('lookup l', 'l.lookup_id = c.country', 'left');
		$this->db->limit($length, $start);
		$this->db->order_by($order, $dir);
		$this->db->order_by('i.invoice_no', 'asc');
		
		if($where != ''){
			$this->db->group_start();
			$this->db->where('i.invoice_no like ', '%'.$where.'%');
			$this->db->or_where('i.invoice_date like ', '%'.$where.'%');
			$this->db->or_where('c.client_name like ', '%'.$where.'%');
			$this->db->or_where('m.name like ', '%'.$where.'%');
			$this->db->or_where('i.net_total like ', '%'.$where.'%');
			$this->db->or_where('i.discount like ', '%'.$where.'%');
			$this->db->or_where('i.freight_charge like ', '%'.$where.'%');
			$this->db->or_where('i.other_charge like ', '%'.$where.'%');
			$this->db->or_where('i.grand_total like ', '%'.$where.'%');
			$this->db->group_end();
		}
		if($searchByYear != 'all'){
			$years = explode('-', $searchByYear);
			$this->db->where('i.invoice_date >= ', date($years[0].'-04-01 00:00:00'));
			$this->db->where('i.invoice_date <= ', date($years[1].'-03-31 23:59:59'));
		}
		$res = $this->db->get_where('invoice_mst i', array('is_deleted' => NULL))->result_array();
		//echo $this->db->last_query();
		$k=0;
		$result = array();
		foreach ($res as $key => $value) {
			$result[$key] = $value;
			$result[$key]['record_id'] = ++$k;
		}
		return $result;
	}

	function getInvoiceListCount($where, $searchByYear='all'){
		$this->db->select('i.*, c.client_name, m.name');
		$this->db->join('clients c', 'c.client_id = i.company_id', 'inner');
		$this->db->join('members m', 'm.member_id = i.member_id', 'left');
		if($where != ''){
			$this->db->group_start();
			$this->db->where('i.invoice_no like ', '%'.$where.'%');
			$this->db->or_where('i.invoice_date like ', '%'.$where.'%');
			$this->db->or_where('c.client_name like ', '%'.$where.'%');
			$this->db->or_where('m.name like ', '%'.$where.'%');
			$this->db->or_where('i.net_total like ', '%'.$where.'%');
			$this->db->or_where('i.discount like ', '%'.$where.'%');
			$this->db->or_where('i.freight_charge like ', '%'.$where.'%');
			$this->db->or_where('i.other_charge like ', '%'.$where.'%');
			$this->db->or_where('i.grand_total like ', '%'.$where.'%');
			$this->db->group_end();
		}
		if($searchByYear != 'all'){
			$years = explode('-', $searchByYear);
			$this->db->where('i.invoice_date >= ', date($years[0].'-04-01 00:00:00'));
			$this->db->where('i.invoice_date <= ', date($years[1].'-03-31 23:59:59'));
		}
		$res = $this->db->get_where('invoice_mst i', array('is_deleted' => NULL))->result_array();
		return sizeof($res);
	}

	function getInvoiceDetails($invoice_id){
		$this->db->select('m.*, d.*, c.name customer_name, cm.member_name name, cm.email, cm.mobile, c.website, l3.name country, l1.name product, l2.name material');
		$this->db->join('invoice_dtl d', 'm.invoice_mst_id = d.invoice_mst_id AND d.status = "Active"', 'left');
		$this->db->join('customer_mst c', 'c.id = m.company_id', 'inner');
		$this->db->join('customer_dtl cm', 'cm.comp_dtl_id = m.member_id', 'left');
		$this->db->join('country_mst l3', 'l3.id = c.country_id', 'left');
		$this->db->join('product_mst l1', 'l1.id = d.product_id', 'left');
		$this->db->join('material_mst l2', 'l2.id = d.material_id', 'left');
		$this->db->order_by('d.invoice_dtl_id');
		return $this->db->get_where('invoice_mst m', array('m.invoice_mst_id' => $invoice_id))->result_array();
	}

	function deleteInvoice($table, $where){
		$this->db->delete($table, $where);
	}

	function getFinancialYears(){
		$this->db->select('case when month(invoice_date) > 3 then concat(year(invoice_date),"-",year(invoice_date)+1) 
    	else concat(year(invoice_date)-1,"-",year(invoice_date)) end as years');
    	$this->db->distinct();
    	$this->db->order_by('years', 'desc');
    	return $this->db->get('invoice_mst')->result_array();
	}

	public function batch_insert_data($table_name, $insert_data) {

		$this->db->insert_batch($table_name, $insert_data);
	}

	// public function get_invoice_data($where, $order_by, $limit=0, $offset=0) {

	// 	$this->db->select('SQL_CALC_FOUND_ROWS *, invoice_mst.invoice_mst_id, invoice_mst.invoice_no, invoice_mst.invoice_date, invoice_mst.grand_total, clients.client_name, members.name, lookup.lookup_value country', FALSE);
	// 	$this->db->join('clients', 'clients.client_id = invoice_mst.company_id', 'inner');
	// 	$this->db->join('members', 'members.member_id = invoice_mst.member_id', 'left');
	// 	$this->db->join('lookup', 'lookup.lookup_id = clients.country', 'left');
	// 	$this->db->where($where, null, false);
	// 	$this->db->order_by($order_by['column_name'], $order_by['column_value']);	
	// 	$return_array['invoice_list'] = $this->db->get('invoice_mst', $limit, $offset)->result_array();
	// 	// echo $this->db->last_query(),"<hr>";
	// 	// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
	//     $return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows;")->row_array();
	// 	return $return_array;
	// }

	public function get_invoice_data($where, $order_by, $limit=0, $offset=0) {

		$this->db->select('SQL_CALC_FOUND_ROWS invoice_mst.*, customer_mst.name customer_name, customer_dtl.member_name, country_mst.name country', FALSE);
		$this->db->join('customer_mst', 'customer_mst.id = invoice_mst.company_id', 'inner');
		$this->db->join('customer_dtl', 'customer_dtl.comp_dtl_id = invoice_mst.member_id', 'left');
		$this->db->join('country_mst', 'country_mst.id = customer_mst.country_id', 'left');
		$this->db->join('users', 'users.user_id = invoice_mst.assigned_to', 'left');
		$this->db->where($where, null, false);
		$this->db->order_by($order_by['column_name'], $order_by['column_value']);	
		$return_array['invoice_list'] = $this->db->get('invoice_mst', $limit, $offset)->result_array();
		// echo $this->db->last_query(),"<hr>";exit;
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
	    $return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows;")->row_array();
		return $return_array;
	}
	
	public function get_data($select, $where_array, $table_name, $result_type = 'result_array') {

		$this->db->select($select);
		$this->db->where($where_array);
		return $this->db->get($table_name)->$result_type();
	}

	function get_financial_years(){

		$this->db->select('Year(invoice_date) as year');
		$this->db->where(array('Year(invoice_date) !=' => '1970'));
		$this->db->group_by('year');
		$this->db->order_by('year', 'desc');
		$res = $this->db->get('invoice_mst')->result_array();
		return $this->create_year_list($res);
	}

	private function create_year_list($all_years){

		$return_array = array();

		if(!empty($all_years)) {

			foreach ($all_years as $key => $single_year) {
				
				$return_array[]['years'] = $single_year['year'].'-'.( ((int)$single_year['year']) +1);				
			}
			$return_array[]['years'] = ( ((int)$all_years[count($all_years)-1]['year']) -1).'-'.$all_years[count($all_years)-1]['year'];				
		}

		return $return_array;
	}

	public function get_invoice_total_highchart_data($year = '2021') {

		$this->db->select('MONTHNAME(invoice_date) as month, sum(grand_total) as total');
		$this->db->where(array('is_deleted' => NULL, 'YEAR(invoice_date)'=> $year));
		$this->db->group_by('month');
		$this->db->order_by('invoice_date');
		return $this->db->get('invoice_mst')->result_array();
	}

	public function get_all_invoice_details($where_array) {

		$this->db->select('invoice_mst_id, grand_total, currency_id, YEAR(invoice_date) year, MONTH(invoice_date) month');
		$this->db->where($where_array, null, false);
		$this->db->order_by('invoice_date','asc');
		return $this->db->get('invoice_mst')->result_array();
	}

	// public function get_total($where){
    //   	$this->db->select('invoice_mst.currency_id, SUM(grand_total) as total, currency.decimal_number');
    //   	$this->db->join('clients', 'clients.client_id = invoice_mst.company_id', 'inner');
	// 	$this->db->join('members', 'members.member_id = invoice_mst.member_id', 'left');
	// 	$this->db->join('lookup', 'lookup.lookup_id = clients.country', 'left');
	// 	$this->db->join('currency', 'currency.currency_id = invoice_mst.currency_id', 'left');
    //   	$this->db->where($where, null, false);
    //   	$this->db->group_by('invoice_mst.currency_id');
    //   	$res = $this->db->get('invoice_mst')->result_array();
    //   	return $res;
	// }

	public function get_total($where){
      	$this->db->select('invoice_mst.currency_id, SUM(grand_total) as total, currency.decimal_number');
      	$this->db->join('customer_mst', 'customer_mst.id = invoice_mst.company_id', 'inner');
		$this->db->join('customer_dtl', 'customer_dtl.comp_dtl_id = invoice_mst.member_id', 'left');
		$this->db->join('country_mst', 'country_mst.id = customer_mst.country_id', 'left');
		$this->db->join('currency', 'currency.currency_id = invoice_mst.currency_id', 'left');
      	$this->db->where($where, null, false);
      	$this->db->group_by('invoice_mst.currency_id');
      	$res = $this->db->get('invoice_mst')->result_array();
			// echo $this->db->last_query(),"<hr>";die('debug');
      	return $res;
	}

	// public function get_export_invoice_listing_data($where, $limit, $offset,$result_type = 'result_array')
	// { 
	// 	$this->db->select('SQL_CALC_FOUND_ROWS *,invoice_mst.invoice_mst_id, invoice_mst.invoice_no, invoice_mst.invoice_date, invoice_mst.grand_total, invoice_export_info.*', FALSE);
	// 	$this->db->join('invoice_export_info', 'invoice_export_info.invoice_id= invoice_mst.invoice_mst_id', 'left');
	// 	$this->db->order_by('invoice_mst.invoice_no', 'asc');
	// 	$this->db->where($where, null, false);
	// 	$return_array['invoice_export_list'] = $this->db->get('invoice_mst', $limit, $offset)->$result_type();
	// 	// echo $this->db->last_query(),"<hr>";die('debug');
	//     $return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows;")->row_array();
	//     return $return_array;
	// }

	public function get_export_invoice_listing_data($where, $limit, $offset,$result_type = 'result_array')
	{ 
		$this->db->select('SQL_CALC_FOUND_ROWS invoice_export_info.*, invoice_mst.invoice_mst_id, invoice_mst.invoice_no, invoice_mst.invoice_date, invoice_mst.grand_total', FALSE);
		$this->db->join('invoice_export_info', 'invoice_export_info.invoice_id= invoice_mst.invoice_mst_id', 'left');
		$this->db->order_by('invoice_mst.invoice_no', 'asc');
		$this->db->where($where, null, false);
		$return_array['invoice_export_list'] = $this->db->get('invoice_mst', $limit, $offset)->$result_type();
		// echo $this->db->last_query(),"<hr>";die('debug');
	    $return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows;")->row_array();
	    return $return_array;
	}

	public function get_invoice_export_update_data($where_array)
	{

		$this->db->select('invoice_mst.invoice_mst_id, invoice_mst.invoice_no, invoice_mst.invoice_date, invoice_mst.grand_total, invoice_export_info.*', FALSE);
		$this->db->join('invoice_export_info', 'invoice_export_info.invoice_id= invoice_mst.invoice_mst_id', 'left');
		return $this->db->get_where('invoice_mst', $where_array)->row_array();

	}
}
?>