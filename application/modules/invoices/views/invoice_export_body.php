<?php foreach($export_list as $single_detail_key=>$single_detail_value){?>
<tr>
    <td class="first_div" style="width: 30px; padding: 0px 0px 0px 15px;">
        <span>
            <abbr>
                <em class="kt-font-bolder">
                    <i><?php echo $single_detail_key+1; ?></i>
                </em>
            </abbr>
        </span>
    </td>
    <td style="width: 250px; padding: 0px 0px 0px 10px;">
        <span>
            <abbr>
                <em class="kt-font-bolder">
                    <i><?php echo $single_detail_value['consignee_name']; ?></i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-center" style="width: 70px;">
        <a href="<?php echo base_url('invoices/invoice_export_update/'.$single_detail_value['invoice_id'])?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit" target="_blank">
            <i class="la la-edit kt-font-bolder" style="color: #ACACAC;"></i>
        </a>
        <button class="btn btn-sm btn-clean btn-icon btn-icon-md delete_production_list" title="Delete" production_id="">
            <i class="la la-trash kt-font-bolder" style="color: #ACACAC;"></i>
        </button>
    </td>
    <td class="kt-align-left" style="width: 200px;">
        <span>
            <abbr>
                <em class="kt-font-bolder"> NO #: 
                    <i><?php echo $single_detail_value['invoice_no']; ?> </i>
                </em>
                <em class="kt-font-bolder"> Date: 
                    <i class=""><?php echo $single_detail_value['invoice_date']; ?> </i>
                </em>
                <em class="kt-font-bolder"> value: 
                    <i class=""><?php echo $single_detail_value['grand_total']; ?> </i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-center" style="width: 200px;">
        <span>
            <abbr>
                <em class="kt-font-bolder">
                    <i><?php echo $single_detail_value['port_of_loading']; ?> </i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-center" style="width: 200px;">
        <span>
            <abbr>
                <em class="kt-font-bolder">
                    <i><?php echo $single_detail_value['port_of_discharge']; ?> </i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-center" style="width: 200px;">
        <span>
            <abbr>
                <em class="kt-font-bolder">
                    <i><?php echo $single_detail_value['final_destination']; ?> </i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-center" style="width: 200px;">
        <span>
            <abbr>
                <em class="kt-font-bolder">
                    <i><?php echo $single_detail_value['buyer_other_then_consignee']; ?></i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-center" style="width: 100px;">
        <span>
            <abbr>
                <em class="kt-font-bolder">
                    <i><?php echo $single_detail_value['currency']; ?></i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-center" style="width: 150px;">
        <span>
            <abbr>
                <em class="kt-font-bolder">
                    <i><?php echo $single_detail_value['payment_terms']; ?></i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-left" style="width: 200px;">
        <span>
            <abbr>
                <em class="kt-font-bolder"> Total recd : 
                    <i><?php echo number_format(array_sum($single_detail_value['part_payment_details']),2,'.',','); ?></i>
                </em>
                <em class="kt-font-bolder"> part 1: 
                    <i class=""><?php echo $single_detail_value['part_payment_details']['part_1']; ?></i>
                </em>
                <em class="kt-font-bolder"> part 2: 
                    <i class=""><?php echo $single_detail_value['part_payment_details']['part_2']; ?></i>
                </em>
                <em class="kt-font-bolder"> part 3: 
                    <i class=""><?php echo $single_detail_value['part_payment_details']['part_3']; ?></i>
                </em>
                <em class="kt-font-bolder"> part 4: 
                    <i class=""><?php echo $single_detail_value['part_payment_details']['part_4']; ?></i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-center" style="width: 150px;"><?php echo$single_detail_value['cha_name']; ?></td>
    <td class="kt-align-left" style="width: 350px;">
        <span>
            <abbr>
                <em class="kt-font-bolder"> ADV NO no: 
                    <i class=""><?php echo$single_detail_value['advisery_no'][0]; ?></i>
                </em>
                <em class="kt-font-bolder"> ADV AMT: 
                    <i class=""><?php echo $single_detail_value['advisery_amount'][0]; ?></i>
                </em>
                <em class="kt-font-bolder"> UTILISED AMT: 
                    <i class=""><?php echo $single_detail_value['utilised_amount'][0]; ?></i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-left" style="width: 350px;">
        <span>
            <abbr>
                <em class="kt-font-bolder"> ADV NO no: 
                    <i class=""><?php echo$single_detail_value['advisery_no'][1]; ?></i>
                </em>
                <em class="kt-font-bolder"> ADV AMT: 
                    <i class=""><?php echo $single_detail_value['advisery_amount'][1]; ?></i>
                </em>
                <em class="kt-font-bolder"> UTILISED AMT: 
                    <i class=""><?php echo $single_detail_value['utilised_amount'][1]; ?></i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-left" style="width: 350px;">
        <span>
            <abbr>
                <em class="kt-font-bolder"> ADV NO no: 
                    <i class=""><?php echo$single_detail_value['advisery_no'][2]; ?></i>
                </em>
                <em class="kt-font-bolder"> ADV AMT: 
                    <i class=""><?php echo $single_detail_value['advisery_amount'][2]; ?></i>
                </em>
                <em class="kt-font-bolder"> UTILISED AMT: 
                    <i class=""><?php echo $single_detail_value['utilised_amount'][2]; ?></i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-left" style="width: 350px;">
        <span>
            <abbr>
                <em class="kt-font-bolder"> ADV NO no: 
                    <i class=""><?php echo$single_detail_value['advisery_no'][3]; ?></i>
                </em>
                <em class="kt-font-bolder"> ADV AMT: 
                    <i class=""><?php echo $single_detail_value['advisery_amount'][3]; ?></i>
                </em>
                <em class="kt-font-bolder"> UTILISED AMT: 
                    <i class=""><?php echo $single_detail_value['utilised_amount'][3]; ?></i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-center" style="width: 150px;">
        <span>
            <abbr>
                <em class="kt-font-bolder">
                    <i class=""><?php echo $single_detail_value['outstanding_amount']; ?></i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-center" style="width: 150px;">
        <span>
            <abbr>
                <em class="kt-font-bolder">
                    <i class=""><?php echo $single_detail_value['po_no']; ?></i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-center" style="width: 150px;">
        <span>
            <abbr>
                <em class="kt-font-bolder">
                    <i class=""><?php echo $single_detail_value['extra']; ?></i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-left" style="width: 200px;">
        <span>
            <abbr>
                <em class="kt-font-bolder"> NO: 
                    <i class=""><?php echo $single_detail_value['shipping_bill_details']['shipping_bill_no']; ?></i>
                </em>
                <em class="kt-font-bolder"> DATE: 
                    <i class=""><?php echo $single_detail_value['shipping_bill_details']['shipping_bill_date']; ?></i>
                </em>
                <em class="kt-font-bolder"> LEO DATE: 
                    <i class=""><?php echo $single_detail_value['shipping_bill_details']['shipping_bill_leo_date']; ?></i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-center" style="width: 100px;">
        <span>
            <abbr>
                <em class="kt-font-bolder">
                    <i class=""><?php echo $single_detail_value['port_code']; ?></i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-left" style="width: 200px;">
        <span>
            <abbr>
                <em class="kt-font-bolder"> NO: 
                    <i class=""><?php echo$single_detail_value['egm']['egm_no']; ?></i>
                </em>
                <em class="kt-font-bolder"> DATE: 
                    <i class=""><?php echo$single_detail_value['egm']['egm_date']; ?></i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-center" style="width: 100px;">
        <span>
            <abbr>
                <em class="kt-font-bolder">
                    <i class=""><?php echo $single_detail_value['bill_given_to_ca']; ?></i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-center" style="width: 100px;">
        <span>
            <abbr>
                <em class="kt-font-bolder">
                    <i class=""><?php echo $single_detail_value['e_brc_remark']; ?></i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-center" style="width: 100px;">
        <span>
            <abbr>
                <em class="kt-font-bolder">
                    <i class=""><?php echo $single_detail_value['bank_sub_date'];?></i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-center" style="width: 100px;">
        <span>
            <abbr>
                <em class="kt-font-bolder">
                    <i class=""><?php echo $single_detail_value['bill_id_no']; ?></i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-center" style="width: 100px;">
        <span>
            <abbr>
                <em class="kt-font-bolder">
                    <i class=""><?php echo $single_detail_value['brc_no']; ?></i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-center" style="width: 100px;">
        <span>
            <abbr>
                <em class="kt-font-bolder">
                    <i class=""><?php echo $single_detail_value['rodtep_scheme']; ?></i>
                </em>
            </abbr>
        </span>
    </td>
    <td class="kt-align-center" style="width: 100px;">
        <span>
            <abbr>
                <em class="kt-font-bolder">
                    <i class=""><?php echo $single_detail_value['final_remark']; ?></i>
                </em>
            </abbr>
        </span>
    </td>
</tr>
<?php }?>


