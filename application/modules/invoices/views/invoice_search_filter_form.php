<!--begin: Search Form -->
<form class="kt-form kt-form--fit kt-margin-b-20 invoice_font" id="invoice_search_filter_form">
	<div id="invoice_search_filter_form_div">
		<div class="row kt-margin-b-20">
			<div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
		      	<label>Invoice #</label>
		      	<input type="text" class="form-control" value="<?php echo (!empty($search_filter_form_data['invoice_no'])) ? $search_filter_form_data['invoice_no'] : '';?>" name="invoice_no" placeholder="Enter Invoice No" />
		   	</div>
			<div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
		      	<label>financial year</label>
				<select class="form-control financial_year_select_picker" name="financial_year" multiple>
			      	<option value="">Select</option>
					<?php foreach($year_list as $single_year) {?>
				        <option value="<?php echo $single_year['years'];?>"
			         	<?php echo 
				         	(in_array($single_year['years'], $search_filter_form_data['financial_year']))
				         	? 'selected'
				         	:'';
			         	?>
			         	>
				        	<?php echo $single_year['years'];?>
				        </option>
		         	<?php } ?>     
				</select>
		   	</div>
			<div class="col-lg-3 col-md-9 col-sm-12">
		      	<label>invoice Date</label>
  				<div class="form-group-sub">
					<div class="kt-portlet__head-toolbar">
						<input type="text" class="form-control" value="<?php echo !empty($search_filter_form_data['invoice_date'])? $search_filter_form_data['invoice_date']: '';?>" name="invoice_date" id="invoice_date" hidden/>
	                    <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
							<a href="javascript:void(0);" class="btn kt-subheader__btn-daterange" id="kt_proforma_daterangepicker" data-toggle="kt-tooltip" title="Select Destrack Date" data-placement="left" style="background: rgb(0 0 0 / 10%); display: inline-flex; height: 32px !important; padding-top: 0; padding-bottom: 0; margin-top: 0.25rem; margin-bottom: 0.25rem; margin-left: 0.25rem;">
								<span class="kt-subheader__btn-daterange-title" id="kt_proforma_daterangepicker_title" style="display: inline-block;color: #959cb6;font-weight: 500;">Today</span>&nbsp;
								<span class="kt-subheader__btn-daterange-date" id="kt_proforma_daterangepicker_date" style="display: inline-block;color: #5d78ff;margin-right: 0.75rem;font-weight: 500;">Aug 16</span>
								<i class="flaticon2-calendar-1" style="color: #5d78ff; font-size: 1rem !important;"></i>
							</a>
	                    </ul>
	                </div>
				</div>
			</div>
			<div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
				<label>Company Name :</label>
				<div class="input-group">
					<input type="text" class="form-control" name="customer_name" value="<?php echo $search_filter_form_data['customer_name']; ?>" placeholder="min 4 character">
				</div>
				<select class="form-control select_picker" name="client_id" data-live-search = "true">
					<option value="">Select Name</option>
					<?php foreach($search_filter_form_data['client_id'] as $single_details){
						
						echo '<option value="'.$single_details['id'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
					}  ?>
				</select>                
			</div>
		</div>
		<div class="row kt-margin-b-20">
			<div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
		      	<label>Country</label>
		      	<select class="form-control country_name_select_picker"  name="country_name" multiple> 
		      		<option value="">Select</option>
					<?php foreach($country_list as $single_country) {?>
						<option value="<?php echo $single_country['id'];?>"
						<?php echo
						(in_array($single_country['id'], $search_filter_form_data['country']))
				         	? 'selected'
				         	:'';
			         	?>
	               		>
						<?php echo $single_country['name'];?>
	               		</option>
	           		<?php } ?> 
				</select>
		    </div>
			<div class="col-lg-3 kt-margin-b-10-tablet-and-mobile kt-hidden">
		      	<label>Member Name</label>
				<input type="text" class="form-control" value="<?php echo (!empty($search_filter_form_data['member_name'])) ? $search_filter_form_data['member_name'] : '';?>" name="member_name" placeholder="Enter Member Name" />
			</div>
			<div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
				<label>Invoice Status</label>
				<select class="form-control country_name_select_picker" name="invoice_status">
					<option value="" <?php echo ($search_filter_form_data['invoice_status'] == '') ? 'selected': ''; ?>>Select</option>
					<option value="dispatched" <?php echo ($search_filter_form_data['invoice_status'] == 'dispatched') ? 'selected': ''; ?>>Dispatched</option>
					<option value="not_dispatched" <?php echo ($search_filter_form_data['invoice_status'] == 'not_dispatched') ? 'selected': ''; ?>>Not Dispatched</option>
				</select>
			</div>
			<div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
				<label>Sales Person</label>
				<select class="form-control country_name_select_picker"  name="assigned_to" multiple>
					<option value="">Select</option>
					<?php foreach($sales_person as $single_sales_person) {?>
						<option value="<?php echo $single_sales_person['user_id'];?>"
						<?php echo
							(in_array($single_sales_person['user_id'], $search_filter_form_data['sales_person']))
							? 'selected'
							:'';
						?>
						>
							<?php echo $single_sales_person['name'];?>
						</option>
					<?php } ?>
				</select>
	      	</div>
		</div>	      
		<div class="kt-separator kt-separator--md kt-separator--dashed"></div>
		<div class="row">
		    <div class="col-lg-12">
		        <button class="btn btn-primary btn-brand--icon invoice_search_filter_form_submit" type="reset">
		            <span>
		                <i class="la la-search"></i>
		                <span>Search</span>
		            </span>
		        </button>
		        &nbsp;&nbsp;
		        <button class="btn btn-secondary btn-secondary--icon invoice_search_filter_form_reset" type="reset">
		            <span>
		                <i class="la la-close"></i>
		                <span>Reset</span>
		            </span>
		        </button>
		    </div>
		</div>
	</div>	
</form>