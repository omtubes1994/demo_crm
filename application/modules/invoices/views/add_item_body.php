<tr class="item_body_<?php echo $count_no;?>">
	
	<td><input type="text" class="form-control" name="invoice_dtl_id_<?php echo $count_no;?>" value="<?php echo $invoice_dtl_id;?>" hidden><?php echo $count_no;?></td>
	<td style="width: 40%;">
		<input type="text" class="form-control description" name="description_<?php echo $count_no;?>" value="<?php echo htmlspecialchars($description);?>">
	</td>
	<td>
		<input type="number" class="form-control  hscode" maxlength="6" name="hscode_<?php echo $count_no;?>" value="<?php echo $hscode;?>">
	</td>
	<td><input type="number" class="form-control quantity" count_no="<?php echo $count_no;?>" name="quantity_<?php echo $count_no;?>"value="<?php echo $quantity;?>"></td>
	<td><input type="number" class="form-control  rate" count_no="<?php echo $count_no;?>" name="rate_<?php echo $count_no;?>"value="<?php echo $rate;?>"></td>
	<td><input type="number" class="form-control" name="price_<?php echo $count_no;?>"value="<?php echo $price;?>" readonly></td>
	<td>
		<select class="form-control invoice_select_picker"  name="product_id_<?php echo $count_no;?>" data-live-search="true">
	      	<option value="">Select Product</option>
	     	<?php foreach($product_list as $single_product) {?>
				<option 
					value="<?php echo $single_product['id'];?>"
					<?php echo ($product_id == $single_product['id'])? 'selected':'';?>
					data-tokens="<?php echo $single_product['name'];?>">
					<?php echo $single_product['name'];?>
				</option>
	     	<?php } ?>     
		</select>
	</td>
	<td>
		<select class="form-control invoice_select_picker"  name="material_id_<?php echo $count_no;?>" data-live-search="true">
	      	<option value="">Select Material</option>
	     	<?php foreach($material_list as $single_material) {?>
				<option 
					value="<?php echo $single_material['id'];?>"
					<?php echo ($material_id == $single_material['id'])? 'selected':'';?>
					data-tokens="<?php echo $single_material['name'];?>">
					<?php echo $single_material['name'];?>
				</option>
	     	<?php } ?>     
		</select>
	</td>
	<td>
		<?php if(!empty($invoice_dtl_id)) {?>
			<a href="javascript:;" class="btn-sm btn btn-label-danger btn-bold delete_invoice_order_details" invoice_dtl_id="<?php echo $invoice_dtl_id;?>">
				<i class="la la-trash-o"></i>
			</a>
		<?php }?>
	</td>
</tr>