<style type="text/css">
    .kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
        padding-top: 4% !important;
    }
    .layer-white{
        display: none;
        position: absolute;
        top: 0em !important;
        left: 0em !important;
        width: 100%;
        height: 100%;
        text-align: center;
        vertical-align: middle;
        background-color: rgba(255, 255, 255, 0.55);
        opacity: 1;
        line-height: 1;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
        -webkit-animation-duration: 0.5s;
        animation-duration: 0.5s;
        -webkit-transition: background-color 0.5s linear;
        transition: background-color 0.5s linear;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        will-change: opacity;
        z-index: 9;
    }
    .div-loader{
        position: absolute;
        top: 50%;
        left: 50%;
        margin: 0px;
        text-align: center;
        z-index: 1000;
        -webkit-transform: translateX(-50%) translateY(-50%);
        transform: translateX(-50%) translateY(-50%);
    }
    .kt-spinner:before {
        width: 50px;
        height: 50px;
        margin-top: -10px;
    }
    tbody#export_invoice_body tr:hover {
        background-color: gainsboro;
    }
    tbody#export_invoice_body tr:hover .first_div {
        border-left: 0.25rem solid #898989 !important;
    }
    tbody#export_invoice_body td {
        font-style: normal;
        font-size: 15px;
        border: 0.05rem solid gainsboro;
    }
    tbody#export_invoice_body td span{
        /*width:200px !important;*/
        display: inline-flex;
        width: 100%;
    }
    tbody#export_invoice_body td span i{
        cursor: pointer;
    }
    tbody#export_invoice_body td span abbr{
        width: 100%;
    }    
    tbody#export_invoice_body td span abbr em{
        display: block;
        color: #898989;
        margin: 3px 0 3px 0;
        font-style: normal;
        font-family: Helvetica Neue,Helvetica,Arial,sans-serif
    }
    tbody#export_invoice_body td span abbr em i{
        font-weight: 500;
        color: #898989;
        margin: 3px 0 3px 0;
        font-style: normal;
        font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
    }
    thead#invoice_export_header tr th{
        background: gainsboro;
        color: #767676;
        font-size: 14px;
        font-family: 'latomedium';
        padding: 10px 20px;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    }
    ul.menu-tab {
        margin: 0px;
        padding: 0px;
        border: 1px solid #E7E7E7;
        /*border-radius: 50px;*/
        overflow: hidden;
        font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
        font-size: 14px;
        line-height: 20px;
        color: #333;
    }
    ul.menu-tab li{
        width: calc(100%/1) !important;
        display: inline;
        text-align: center;
        float: left;
    }
    ul.menu-tab li.active_list{
        /*border-bottom: 0.25rem solid #767676 !important;*/
    }
    ul.menu-tab li a{
        cursor: pointer;
        display: inline-block;
        outline: none;
        text-align: center;
        width: 100%;
        background: #F5F5F5;
        /*border-right: 2px solid #fff;*/
        color: #767676;
        font-size: 15px;
        font-family: 'latomedium';
        background-color: gainsboro;
        padding: 3px 3px 12px 3px;
    }
    ul.menu-tab li a i{
        display: inline-block;
        width: 35px;
        height: 28px;
        font-style: normal;
        background-size: 100%;
        position: relative;
        top: 6px;
    }
    .form_name{
        color: #767676 !important;
        font-size: 14px !important;
        font-family: 'latomedium' !important;
        padding: 10px 10px !important; 
        font-weight: bold !important;
    }
    .kt-badge--gainsboro, .kt-font-gainsboro{
        background-color: gainsboro;
    }
</style>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					Product Lists
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
		        <div class="kt-portlet__head-wrapper">
		            <div class="kt-portlet__head-actions">
		                <a href="javascript:;" class="btn btn-brand btn-elevate btn-icon-sm invoice_export_search_filter_action" action_value="show" style="background-color: gainsboro; border-color: gainsboro; color: #767676; font-weight: 800;">
		                    Add Search Filter
		                </a>
		            </div>
		        </div>
	    	</div>
		</div>
		<div class="kt-portlet__body invoice_export_search_filter" style="display: none; padding: 1% 1% 0% 1%;">
        	<form class="kt-form kt-form--fit kt-margin-b-20" id="invoice_export_search_filter_form">
                <div id="invoice_export_search_filter_div" style="position: relative; width: 100%; padding-right: 10px; padding-left: 10px;">
                    <?php $this->load->view('invoices/invoice_export_search_filter');?>
                </div>
            </form>
		</div>
		<div class="kt-portlet__body" style="padding: 25px 25px 0px 25px;">
			<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
				<div class="row">
					<!-- <div class="col-sm-1"></div> -->
					<div class="col-sm-12">
						<ul class="menu-tab kt-font-bolder"
							style="">
							<li class="tab_name_click pending_order active_list" tab-name="pending_order">
								<a>
									<i class="custom_heading_icon active_issues_tabs_icon">
										<img src="assets/media/icons/svg/Tools/Angle Grinder.svg"/>
									</i>
									<span style="top: 6px;position: relative;">
										Export Invoice Listing
									</span>
								</a>
							</li>
						</ul>
					</div>
					<div class="col-sm-12">
                        <div class="kt-scroll" data-scroll="true" data-scroll-x="true" style="height: auto;">
                            <div style="width: 4200px;">
                                <table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 100%; border: 0.25rem solid gainsboro;">
                                    <thead id="invoice_export_header">
                                        <tr role="row">
                                            <th class="kt-font-bolder kt-align-left" style="padding: 0px 0px 0px 0px;text-align: center;">Sr. No.</th>
                                            <th class="kt-font-bolder kt-align-left" style="padding: 0px 0px 0px 10px;">Consignee Name</th>
                                            <th class="sorting_disabled kt-font-bolder kt-align-center" style="">Actions</th>
                                            <th 
                                                class=" sorting_search kt-font-bolder kt-align-left" 
                                                sorting_name="invoice_mst.invoice_date"
                                                sorting_value=""
                                                style=" padding: 0% 0% 0% 15px;">
                                                Invoice Details
                                            </th>
                                            <th 
                                                class=" sorting_search kt-font-bolder kt-align-center" 
                                                sorting_name="invoice_mst.invoice_date"
                                                sorting_value=""
                                                style="">
                                                PORT OF LOADING
                                            </th>
                                            <th 
                                                class=" sorting_search kt-font-bolder kt-align-center" 
                                                sorting_name="invoice_mst.invoice_date"
                                                sorting_value=""
                                                style="">
                                                PORT OF DISCHARGE
                                            </th>
                                            <th 
                                                class=" sorting_search kt-font-bolder kt-align-center" 
                                                sorting_name="invoice_mst.invoice_date"
                                                sorting_value=""
                                                style="">
                                                FINAL DESTINATION
                                            </th>
                                            <th 
                                                class=" sorting_search kt-font-bolder kt-align-center" 
                                                sorting_name="invoice_mst.invoice_date"
                                                sorting_value=""
                                                style="">
                                                BUYER OTHER THEN CONSIGNEE
                                            </th>
                                            <th 
                                                class=" sorting_search kt-font-bolder kt-align-center" 
                                                sorting_name="invoice_mst.invoice_date"
                                                sorting_value=""
                                                style="">
                                                CURRENCY
                                            </th>
                                            <th 
                                                class=" sorting_search kt-font-bolder kt-align-center" 
                                                sorting_name="invoice_mst.invoice_date"
                                                sorting_value=""
                                                style="">
                                                PAYMENT TERMS
                                            </th>
                                            <th 
                                                class=" sorting_search kt-font-bolder kt-align-left" 
                                                sorting_name="invoice_mst.invoice_date"
                                                sorting_value=""
                                                style="padding: 0% 0% 0% 15px;">
                                                PAYMENT Recd
                                            </th>
                                            <th 
                                                class=" sorting_search kt-font-bolder kt-align-center" 
                                                sorting_name="invoice_mst.invoice_date"
                                                sorting_value=""
                                                style="">
                                                CHA Name
                                            </th>
                                            <th 
                                                class=" sorting_search kt-font-bolder kt-align-left" 
                                                sorting_name="invoice_mst.invoice_date"
                                                sorting_value=""
                                                colspan="4"
                                                style="padding: 0% 0% 0% 15px;">
                                                ADVISDE DETAILS
                                            </th>
                                            <th 
                                                class=" sorting_search kt-font-bolder kt-align-center" 
                                                sorting_name="invoice_mst.invoice_date"
                                                sorting_value=""
                                                style="">
                                                outstanding amt
                                            </th>
                                            <th 
                                                class=" sorting_search kt-font-bolder kt-align-center" 
                                                sorting_name="invoice_mst.invoice_date"
                                                sorting_value=""
                                                style="">
                                                extra
                                            </th>
                                            <th 
                                                class=" sorting_search kt-font-bolder kt-align-center" 
                                                sorting_name="invoice_mst.invoice_date"
                                                sorting_value=""
                                                style="">
                                                PO Number
                                            </th>
                                            <th 
                                                class=" sorting_search kt-font-bolder kt-align-left" 
                                                sorting_name="invoice_mst.invoice_date"
                                                sorting_value=""
                                                style="padding: 0% 0% 0% 15px;">
                                                SHIPPING BILL DETAILS
                                            </th>   
                                            <th 
                                                class=" sorting_search kt-font-bolder kt-align-center" 
                                                sorting_name="invoice_mst.invoice_date"
                                                sorting_value=""
                                                style="">
                                                PORT CODE
                                            </th>
                                            <th 
                                                class=" sorting_search kt-font-bolder kt-align-left" 
                                                sorting_name="invoice_mst.invoice_date"
                                                sorting_value=""
                                                style="padding: 0% 0% 0% 15px;">
                                                EGM
                                            </th>
                                            <th 
                                                class=" sorting_search kt-font-bolder kt-align-center" 
                                                sorting_name="invoice_mst.invoice_date"
                                                sorting_value=""
                                                style="">
                                                S/BILL GIVEN TO CA
                                            </th>
                                            <th 
                                                class=" sorting_search kt-font-bolder kt-align-center" 
                                                sorting_name="invoice_mst.invoice_date"
                                                sorting_value=""
                                                style="">
                                                e-BRC REMARK
                                            </th>
                                            <th 
                                                class=" sorting_search kt-font-bolder kt-align-center" 
                                                sorting_name="invoice_mst.invoice_date"
                                                sorting_value=""
                                                style="">
                                                BANK SUB DATE
                                            </th><th 
                                                class=" sorting_search kt-font-bolder kt-align-center" 
                                                sorting_name="invoice_mst.invoice_date"
                                                sorting_value=""
                                                style="">
                                                BILL ID NO
                                            </th>
                                            <th 
                                                class=" sorting_search kt-font-bolder kt-align-center" 
                                                sorting_name="invoice_mst.invoice_date"
                                                sorting_value=""
                                                style="">
                                                BRC No
                                            </th>
                                            <th 
                                                class=" sorting_search kt-font-bolder kt-align-center" 
                                                sorting_name="invoice_mst.invoice_date"
                                                sorting_value=""
                                                style="">
                                                RODTEP SCHEME
                                            </th><th 
                                                class=" sorting_search kt-font-bolder kt-align-center" 
                                                sorting_name="invoice_mst.invoice_date"
                                                sorting_value=""
                                                style="">
                                                FINAL REMARK
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="export_invoice_body">
                                		
          								<?php $this->load->view('invoices/invoice_export_body');?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div id="export_invoice_paggination" class="layer-white">
                            <div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
                        </div>
                    </div>
				</div>
				<div class="row" id="export_invoice_paggination" style="padding: 25px 1px 1px 1px;">
          			<?php $this->load->view('common/common_paggination');?>
				</div>
			</div>
		</div>
	</div>
</div>