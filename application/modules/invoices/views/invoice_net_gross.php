<tr>
	<?php
		$charge_value = ''; 
		if(!empty($this->session->userdata('invoice_order_details_'.$id)['different_charges'])) {
			$charge_value = implode(' , ', array_column($this->session->userdata('invoice_order_details_'.$id)['different_charges'], 'charges_name'));
			// echo "<pre>";print_r($charge_value);echo"</pre><hr>";exit;
		}	 
	?>
	<td class="kt-font-danger kt-font-lg"><?php echo $charge_value;?></td>
	<td class="kt-font-success kt-font-xl kt-font-boldest purchase_order_gross_amount">
		<?php echo number_format($this->session->userdata('invoice_order_details_'.$id)['net_total'],2,'.',',');?>
	</td>
	<td class="kt-font-success kt-font-xl kt-font-boldest purchase_order_gross_amount">
		<?php echo number_format($this->session->userdata('invoice_order_details_'.$id)['gross_total'],2,'.',',');?>
	</td>
</tr>