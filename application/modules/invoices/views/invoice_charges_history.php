<thead>
   <tr>
   	<th class="kt-font-dark kt-font-md kt-font-bold">Charge Name</th>
   	<th class="kt-font-dark kt-font-md kt-font-bold">Charge Added Or Deducted</th>
   	<th class="kt-font-dark kt-font-md kt-font-bold">Charge Value</th>
   	<th class="kt-font-dark kt-font-md kt-font-bold">Net Total: <span  class="kt-font-brand kt-font-lg kt-font-bolder" style="float: right;"><?php echo number_format($this->session->userdata('invoice_order_details_'.$id)['net_total'],2,'.',','); ?></span></th>
      <th class="kt-font-dark kt-font-md kt-font-bold">Action</th>
   </tr>
</thead>
<tbody>
   <?php if(!empty($this->session->userdata('invoice_order_details_'.$id)['different_charges'])) {?>
      <?php foreach($this->session->userdata('invoice_order_details_'.$id)['different_charges'] as $charge_key =>  $charges_details) {?>
         <tr>
         	<td class="kt-font-dark kt-font-md kt-font-bold kt-font-transform-c"><?php echo $charges_details['charges_name'];?></td>
         	<td class="kt-font-dark kt-font-md kt-font-bold"><?php echo $charges_details['add_minus_charges_value'];?></td>
         	<td class="kt-font-dark kt-font-md kt-font-bold"><?php echo $charges_details['charge_value'];?></td>
         	<td class="kt-font-brand kt-font-lg kt-font-bold"><span style="float: right;"><?php echo number_format($charges_details['gross_total'],2,'.',',');?></span></td>
            <td>
               <a href="javascript:;" class="btn-sm btn btn-label-danger btn-bold delete_invoice_charge_details" charge_id="<?php echo $charge_key;?>">
                  <i class="la la-trash-o"></i>
               </a>
            </td>
         </tr>
      <?php }?>
   <?php }?>
   <tr>
   	<td class="kt-font-dark kt-font-md kt-font-bold" colspan="3" style="text-align: center;">GROSS TOTAL</td>
   	<td class="kt-font-brand kt-font-lg kt-font-bolder">
         <span style="float: right;">
            
            <?php  echo number_format($this->session->userdata('invoice_order_details_'.$id)['gross_total'],2,'.',','); ?>  
         </span>
      </td>
      <th class="kt-font-dark kt-font-md kt-font-bold">Action</th>
   </tr>	
</tbody>