<style type="text/css">
	.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
		padding-top: 4% !important;
	}
	.layer-white{
	    display: none;
	    position: absolute;
	    top: 0em !important;
	    left: 0em !important;
	    width: 100%;
	    height: 100%;
	    text-align: center;
	    vertical-align: middle;
	    background-color: rgba(255, 255, 255, 0.55);
	    opacity: 1;
	    line-height: 1;
	    -webkit-animation-fill-mode: both;
	    animation-fill-mode: both;
	    -webkit-animation-duration: 0.5s;
	    animation-duration: 0.5s;
	    -webkit-transition: background-color 0.5s linear;
	    transition: background-color 0.5s linear;
	    -webkit-user-select: none;
	    -moz-user-select: none;
	    -ms-user-select: none;
	    user-select: none;
	    will-change: opacity;
	    z-index: 9;
	}
	.div-loader{
	    position: absolute;
		top: 50%;
		left: 50%;
		margin: 0px;
		text-align: center;
		z-index: 1000;
		-webkit-transform: translateX(-50%) translateY(-50%);
		transform: translateX(-50%) translateY(-50%);
	}
	.kt-spinner:before {
	    width: 50px;
	    height: 50px;
	    margin-top: -10px;
	}
	.invoice_font {
        font-size: 1rem;
        font-weight: 500;
        line-height: 1.5rem;
        -webkit-transition: color 0.3s ease;
        transition: color 0.3s ease;
        color: #000;
    }
</style>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<?php if(in_array(18,$this->session->userdata('graph_data_access'))) { ?>
	<div class="row">
		<div class="col-xl-12">
			<!--Begin::Portlet-->
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Invoice 
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
					</div>
				</div>
				<div class="kt-portlet__body">
					<!--Begin::Timeline 3 -->	
					<div class="kt-scroll" data-scroll="true" style="height: 400px">
						<div id="invoice_total_highchart"></div>
					</div>

					<!--End::Timeline 3 -->
				</div>
			</div>

			<!--End::Portlet-->
		</div>
	</div>
	<?php }?>
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					LIST OF INVOICE
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
		        <div class="kt-portlet__head-wrapper">
		            <div class="kt-portlet__head-actions">
		                <a href="javascript:;" class="btn btn-brand btn-elevate btn-icon-sm add_invoice_search_filter_form">
		                    Add Search Filter
		                </a>
		            </div>
		        </div>
	    	</div>
		</div>
		<div class="kt-portlet__body invoice_search_filter" style="display: none;" id="invoice_search_filter_form_div">

        	<?php $this->load->view('invoices/invoice_search_filter_form'); ?>
		</div>
		<div class="kt-portlet__body">
			<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
				<div class="row">
					<div class="col-sm-12">
						<input type="text" id="column_name" value="<?php echo $sorting['column_name'];?>" hidden>
    					<input type="text" id="column_value" value="<?php echo $sorting['column_value'];?>" hidden>
						<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 100%;">
							<thead id="invoice_header">
          						<?php $this->load->view('invoices/invoice_header');?>
							</thead>
							<tbody id="invoice_body">
      							<?php $this->load->view('invoices/invoice_body');?>
							</tbody>
						</table>
						<div id="invoice_table_loader" class="layer-white">
							<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
						</div>
					</div>
				</div>
				<div class="row" id="invoice_paggination">
          			<?php $this->load->view('invoices/invoice_paggination');?>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- end:: Content -->