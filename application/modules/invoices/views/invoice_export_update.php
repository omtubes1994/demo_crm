<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col">
			<div class="alert alert-light alert-elevate fade show" role="alert" style="padding: 10px 20px 0px 20px;">
				<div class="alert-icon"><i class="flaticon-edit kt-font-brand"></i></div>
				<div class="alert-text">
					<h2 class="kt-font-info">Export Register</h2>
				</div>
				<div class="update_export">
					<button type="button" class="btn btn-primary btn-wide invoice_export_save_data">Save Details</button>
				</div>
			</div>
		</div>
	</div>
	<div class="kt-portlet" style="background: #F5F5F5;">
		<div class="kt-portlet__body">
			<div class="kt-form__section kt-form__section--first">
				<!--begin::Form-->
				<form class="kt-form" id="invoice_export_update_form">
						  
					<div class="form-group row"> 
						<div class="col-lg-3">
							<label class="">Invoice #</label>
							<input type="text" class="form-control" value="<?php echo $export_invoice_details['invoice_no'];?>" readonly>
							<input type="text" class="form-control" name="invoice_id" value="<?php echo $export_invoice_details['invoice_mst_id'];?>" hidden>
						</div>
						<div class="col-lg-3">
							<label class=""> Invoice Date</label>
							<input type="text" class="form-control" value="<?php echo $export_invoice_details['invoice_date'];?>" readonly>
						</div>
						<div class="col-lg-3">
							<label class=""> Invoice Value</label>
							<input type="text" class="form-control" value="<?php echo $export_invoice_details['grand_total'];?>" readonly>
						</div>
						<div class="col-lg-3">
							<label class="">Consignee Name</label>
							<input type="text" class="form-control" name="consignee_name" value="<?php echo $export_invoice_details['consignee_name'];?>">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-3">
							<label class=""> Port Of Loading</label>
							<input type="text" class="form-control" name="port_of_loading" value="<?php echo $export_invoice_details['port_of_loading'];?>">
						</div>
				        <div class="col-lg-3">
							<label class=""> Port Of Discharge</label>
							<input type="text" class="form-control" name="port_of_discharge" value="<?php echo $export_invoice_details['port_of_discharge'];?>">
						</div>		
						<div class="col-lg-3">
							<label class=""> Final Destination</label>
							<input type="text" class="form-control" name="final_destination" value="<?php echo $export_invoice_details['final_destination'];?>">
						</div>
						<div class="col-lg-3">
							<label class="">Buyers Others Then Consignee</label>
							<input type="text" class="form-control" name="buyer_other_then_consignee" value="<?php echo $export_invoice_details['buyer_other_then_consignee'];?>">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-3">
							<label class="">Currerncy</label>
					        <select class="form-control currency_select_picker" name="currency">
					            <option value="">Select</option>
					            <?php foreach($currency_list as $single_currency_list) {?>
					                <option value="<?php echo $single_currency_list['currency'];?>"
					                	<?php echo ($export_invoice_details['currency'] == $single_currency_list['currency']) ? 'selected':'';?>
					                	>
					                    <?php echo $single_currency_list['currency'];?>
					                </option>
					            <?php } ?>     
					        </select>
						</div>
						<div class="col-lg-3">
							<label class=""> payment Terms</label>
					        <select class="form-control payment_terms_select_picker" name="payment_terms">
					            <option value="">Select</option>
					            <?php foreach($payment_terms_list as $single_payment_terms) {?>
					                <option value="<?php echo $single_payment_terms['term_value'];?>"
					                	<?php echo ($export_invoice_details['payment_terms'] == $single_payment_terms['term_value']) ? 'selected':'';?>
					                	>
					                    <?php echo $single_payment_terms['term_value'];?>
					                </option>
					            <?php } ?>     
					        </select>
						</div>
						<div class="col-lg-3">
							<label class="">CHA Name</label>
							<input type="text" class="form-control" name="cha_name" value="<?php echo $export_invoice_details['cha_name'];?>">
						</div>
						<div class="col-lg-3">
							<label class="">Outstanding AMT</label>
							<input type="text" class="form-control" name="outstanding_amount" value="<?php echo $export_invoice_details['outstanding_amount'];?>">
						</div>
					</div>
					<div class="form-group row">		
						<div class="col-lg-3">
							<label class="">Extra</label>
							<input type="text" class="form-control" name="extra" value="<?php echo $export_invoice_details['extra'];?>">
						</div>
						<div class="col-lg-3">
							<label class="">PO NO</label>
							<input type="text" class="form-control" name="po_no" value="<?php echo $export_invoice_details['po_no'];?>">
						</div>
						<div class="col-lg-3">	
							<label class="">PORT Code</label>
							<input type="text" class="form-control" name="port_code" value="<?php echo $export_invoice_details['port_code'];?>">
						</div>
						<div class="col-lg-3">
							<label class="">S/BILL Given TO CA </label>
							<input type="text" class="form-control" name="bill_given_to_ca" value="<?php echo $export_invoice_details['bill_given_to_ca'];?>">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-3">
							<label class="">E brc Remark </label>
							<input type="text" class="form-control" name="e_brc_remark" value="<?php echo $export_invoice_details['e_brc_remark'];?>">
						</div>
						<div class="col-lg-3">
							<label class="">BANK SUB DATE</label>
							<input type="text" class="form-control invoice_export_banK_sub_date" name="bank_sub_date" value="<?php echo $export_invoice_details['bank_sub_date'];?>">
						</div>	
						<div class="col-lg-3">
							<label class="">BILL ID NO</label>
							<input type="text" class="form-control" name="bill_id_no" value="<?php echo $export_invoice_details['bill_id_no'];?>">
						</div>
						<div class="col-lg-3">
							<label class="">BRC No</label>
							<input type="text" class="form-control" name="brc_no" value="<?php echo $export_invoice_details['brc_no'];?>">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-3">
							<label class="">RODTEP Scheme</label>
							<input type="text" class="form-control" name="rodtep_scheme" value="<?php echo $export_invoice_details['rodtep_scheme'];?>">
						</div>
						<div class="col-lg-3">
							<label class="">FINAl REMARK</label>
							<input type="text" class="form-control" name="final_remark" value="<?php echo $export_invoice_details['final_remark'];?>">
						</div>
                    </div>
					<div class="form-group row">
						<div class="col-lg-12">
							<h5>
								<label class="kt-font-dark kt-font-lg kt-font-bold">part payment</label>
							</h5>
						</div>
						<div class="col-lg-3">
							<label class=""> Payment Received</label> 
							<input type="text" class="form-control" readonly value="<?php echo number_format(array_sum($export_invoice_details['part_payment_details']),2,'.',',');?>">
						</div>		
						<div class="col-lg-2">
							<label class="">Part 1</label>
							<input type="text" class="form-control" name="part_1" value="<?php echo $export_invoice_details['part_payment_details']['part_1'];?>">
						</div>
						<div class="col-lg-2">
							<label class="">Part 2</label>
							<input type="text" class="form-control" name="part_2" value="<?php echo $export_invoice_details['part_payment_details']['part_2'];?>">
						</div>
                    	<div class="col-lg-2">
							<label class="">Part 3</label>
							<input type="text" class="form-control" name="part_3" value="<?php echo $export_invoice_details['part_payment_details']['part_3'];?>">
						</div>
						<div class="col-lg-2">
							<label class="">Part 4</label>
							<input type="text" class="form-control" name="part_4" value="<?php echo $export_invoice_details['part_payment_details']['part_4'];?>">
						</div>
					</div>
		            <div class="form-group row">
                        <div class="col-lg-12">
							<h5>
								<label class="kt-font-dark kt-font-lg kt-font-bold">Advisery Details </label>
							</h5>		
						</div>
                        <div class="col-lg-3">
							<label class="">ADV NO</label>
							<input type="text" class="form-control" name="advisery_no[0]" value="<?php echo $export_invoice_details['advisery_no'][0];?>">
						</div>
                      
                        <div class="col-lg-3">
							<label class="">ADV AMT</label>
							<input type="text" class="form-control" name="advisery_amount[0]" value="<?php echo $export_invoice_details['advisery_amount'][0];?>">
					    </div>
                         <div class="col-lg-3">
							<label class="">Utilised Amt</label>
							<input type="text" class="form-control" name="utilised_amount[0]" value="<?php echo $export_invoice_details['utilised_amount'][0];?>">
					    </div>
					</div>    
					<div class="form-group row">
                         <div class="col-lg-3">
							<label class="">ADV NO no</label>
							<input type="text" class="form-control" name="advisery_no[1]" value="<?php echo $export_invoice_details['advisery_no'][1];?>">
				        </div>
						<div class="col-lg-3">
							<label class="">ADV AMT</label>
							<input type="text" class="form-control" name="advisery_amount[1]" value="<?php echo $export_invoice_details['advisery_amount'][1];?>">
						</div>
						
						<div class="col-lg-3">
							<label class="">Utilised Amt</label>
							<input type="text" class="form-control" name="utilised_amount[1]" value="<?php echo $export_invoice_details['utilised_amount'][1];?>">
						</div>
					</div>    
					<div class="form-group row">
						<div class="col-lg-3">
							<label class="">ADV NO no</label>
							<input type="text" class="form-control" name="advisery_no[2]" value="<?php echo $export_invoice_details['advisery_no'][2];?>">
						</div>
						<div class="col-lg-3">
							<label class="">ADV AMT</label>
							<input type="text" class="form-control" name="advisery_amount[2]" value="<?php echo $export_invoice_details['advisery_amount'][2];?>">
						</div>
						<div class="col-lg-3">
							<label class="">Utilised Amt</label>
							<input type="text" class="form-control" name="utilised_amount[2]" value="<?php echo $export_invoice_details['utilised_amount'][2];?>">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-3">
							<label class="">ADV NO no</label>
							<input type="text" class="form-control" name="advisery_no[3]" value="<?php echo $export_invoice_details['advisery_no'][3];?>">
						</div>
						<div class="col-lg-3">
							<label class="">ADV AMT</label>
							<input type="text" class="form-control" name="advisery_amount[3]" value="<?php echo $export_invoice_details['advisery_amount'][3];?>">
						</div>
						<div class="col-lg-3">
							<label class="">Utilised Amt</label>
							<input type="text" class="form-control" name="utilised_amount[3]" value="<?php echo $export_invoice_details['utilised_amount'][3];?>">
						</div>
					</div>
					<div class="form-group row">	
						<div class="col-lg-12">
							<h5>
								<label class="kt-font-dark kt-font-lg kt-font-bold">SHIPPING BILL DETAILS </label>
							</h5>		
						</div>
						<div class="col-lg-3">
							<label class="">SHIPPING BILL DETAILS NO</label>
							<input type="text" class="form-control" name="shipping_bill_no" value="<?php echo $export_invoice_details['shipping_bill_details']['shipping_bill_no'];?>">
						</div>
						<div class="col-lg-3">
							<label class="">DATE</label>
							<input type="text" class="form-control hasdatepicker " name="shipping_bill_date" value="<?php echo $export_invoice_details['shipping_bill_details']['shipping_bill_date'];?>">
						</div>
						<div class="col-lg-3">
							<label class="">LEO DATE</label>
							<input type="text" class="form-control hasdatepicker " name="shipping_bill_leo_date" value="<?php echo $export_invoice_details['shipping_bill_details']['shipping_bill_leo_date'];?>">
						</div>
				   	</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<h5>
								<label class="kt-font-dark kt-font-lg kt-font-bold">EGM</label>
							</h5>		
						</div>
						<div class="col-lg-3">
							<label class="">NO</label>
							<input type="text" class="form-control" name="egm_no" value="<?php echo $export_invoice_details['egm']['egm_no'];?>">
						</div>
						<div class="col-lg-3">
							<label class="">DATE</label>
							<input type="input" class="form-control hasdatepicker " name="egm_date" value="<?php echo $export_invoice_details['egm']['egm_date'];?>">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>