jQuery(document).ready(function() {
	Invoice_Bootstrap_Select.init();
	Invoice_Bootstrap_Datepicker.init();
	KTAutosize.init();
	KTBootstrapMaxlength.init();
	InvoiceFormControls.init();
	$('button.add_invoice').click(function(){

		if($(this).hasClass('kt-spinner') == false) {

			set_reset_spinner(this);
			ajax_call_function( {call_type: 'add_new_invoice', invoice_form_data: $('form#invoice_form').serializeArray()}, 'add_new_invoice');
		}
	});
	$('div#invoice_paggination').on('click', 'li.invoice_paggination_number', function(){

		ajax_call_function({
							call_type: 'paggination_filter',
							limit: $(this).attr('limit'),
							offset: $(this).attr('offset'),
							search_form_data: $('form#invoice_search_filter_form').serializeArray(),
							invoice_sort_name: $('#column_name').val(),
							invoice_sort_value: $('#column_value').val(),
						}, 'paggination_filter');
	});
	$('thead#invoice_header').on('click', 'th.sorting_search', function(){
       
      	ajax_call_function({
						call_type: 'sort_filter',
						invoice_sort_name: $(this).attr('sorting_name'),
						invoice_sort_value: $(this).attr('sorting_value'),
						search_form_data: $('form#invoice_search_filter_form').serializeArray(),
						limit:$('#invoice_limit').val(),
						offset:$('#invoice_offset').val()
					},'sort_filter');
  	});
	$('a.add_invoice_search_filter_form').click(function(){

		$('div.invoice_search_filter').show();
	});
    $('div#invoice_search_filter_form_div').on('click', 'button.invoice_search_filter_form_submit', function(){

		set_reset_spinner(this);

		search_filter();
	});
	$('div#invoice_search_filter_form_div').on('click', 'button.invoice_search_filter_form_reset', function(){

		ajax_call_function({
						call_type: 'search_filter',
						search_form_data: []
					},
					'search_filter'
				);
		set_reset_spinner(this);
    });
    $('div#invoice_paggination').on('change', 'select#set_limit', function(){

    	ajax_call_function({
							call_type: 'paggination_filter',
							limit: $('select#set_limit').val(),
							offset: 0,
							search_form_data: $('form#invoice_search_filter_form').serializeArray(),
							invoice_sort_name: $('#column_name').val(),
							invoice_sort_value: $('#column_value').val(),
						}, 'paggination_filter');
    });
    $('tbody#invoice_list_body').on('click', 'a.delete_invoice_details', function(){

		$('tr.count_'+$(this).attr('count_no')).remove();
		ajax_call_function({call_type: 'delete_invoice_details', count_no: $(this).attr('count_no')}, 'delete_invoice_details');
	});
	call_invoice_total_highchart();
	$('a.add_item_to_invoice_order').click(function(){
		var count_number = $(this).attr('next_count_number');
		ajax_call_function({call_type: 'add_invoice_order', count_no: count_number}, 'add_invoice_order');
		$(this).attr('next_count_number',++count_number);
	});
	$('button.save_invoice').click(function(){

		$('form#invoice_details_form').submit();
	}); 
	$('tbody#product_list_body').on('input', '.quantity', function(){

		add_total($(this).attr('count_no'));
	});
	$('tbody#product_list_body').on('input', '.rate', function(){
		add_total($(this).attr('count_no'));
	});
	$('tbody#product_list_body').on('click', '.delete_invoice_order_details', function(){

		$('div#invoice_table_loader').show();
		swal({
			  title: "Are you sure?",
			  text: "Once deleted, you will not be able to recover this Procurement Details!",
			  icon: "warning",
			  buttons:  ["Cancel", "Delete"],
			  dangerMode: true,	
			})
		.then((willDelete) => {
		  	if (willDelete) {
				ajax_call_function({call_type: 'delete_invoice_order', invoice_dtl_id: $(this).attr('invoice_dtl_id')}, 'delete_invoice_order');
	  		} else {
			    swal({
		    		title: "Invoice Order is not deleted",
		      		icon: "info",
		    	});
		  	}
	    	$('div#invoice_table_loader').hide();
		});
	});
	ajax_call_function({call_type: 'get_invoice_order_body',invoice_id: '<?php echo $this->uri->segment("3",'');?>'}, 'get_invoice_order_body');
	ajax_call_function({call_type: 'get_invoice_net_total',invoice_id: '<?php echo $this->uri->segment("3",0);?>'}, 'get_invoice_net_total');
	$('a.add_gst_or_discount_modal').click(function(){

		ajax_call_function({call_type: 'add_gst_or_discount',invoice_id: '<?php echo $this->uri->segment("3",0);?>'}, 'add_gst_or_discount');
	});
	$('button.add_charges_in_invoice_order').click(function(){

		ajax_call_function({call_type: 'add_charges_in_invoice_order', invoice_charges_details: $('form#add_charges_in_invoice_order_form').serializeArray()}, 'add_charges_in_invoice_order');
		ajax_call_function({call_type: 'add_gst_or_discount',invoice_id: '<?php echo $this->uri->segment("3",0);?>'}, 'add_gst_or_discount');
		ajax_call_function({call_type: 'get_invoice_net_total',invoice_id: '<?php echo $this->uri->segment("3",0);?>'}, 'get_invoice_net_total');
	});
	$('select[name="company_id"]').change(function(){

		ajax_call_function({call_type: 'get_client_name',client_id: $('select[name="company_id"]').val()}, 'get_client_name');
	});
	$('table#charge_history_table').on('click', 'a.delete_invoice_charge_details', function(){

		ajax_call_function({call_type: 'delete_charge_details',charge_key: $(this).attr('charge_id')}, 'delete_charge_details');
		ajax_call_function({call_type: 'add_gst_or_discount',invoice_id: '<?php echo $this->uri->segment("3",0);?>'}, 'add_gst_or_discount');
	});
	$('tbody#invoice_body').on('click', 'button.delete_invoice', function(){

		swal({
			  title: "Are you sure?",
			  text: "Once deleted, you will not be able to recover this Invoice Details!",
			  icon: "warning",
			  buttons:  ["Cancel", "Delete"],
			  dangerMode: true,	
			})
		.then((willDelete) => {
		  	if (willDelete) {
		  		set_reset_spinner($(this), true, 'kt-spinner--success');
				ajax_call_function({call_type: 'delete_invoice',invoice_mst_id: $(this).attr('invoice_id')}, 'delete_invoice');
	  		} else {
			    swal({
		    		title: "Invoice is not deleted",
		      		icon: "info",
		    	});
		  	}
		});
	});
	$('i.add_attention').click(function(){
		var company_id = $('select[name="company_id"]').val();
		if(company_id == ''){
	    	swal({
				  title: "Company Name is not selected!!!",
				  text: "",
				  icon: "error",
				  buttons:  ["Cancel","Okay"],
				  dangerMode: true,	
				})
			.then((willDelete) => {
	    		$('#add_attention').modal('hide');
			});	
		} else {

			ajax_call_function({call_type: 'get_member_details',company_id: company_id}, 'get_member_details');
		}

	});
	$('button.add_attention_form_submit').click(function(){

		ajax_call_function({call_type: 'add_attention_form', form_data: $('form#add_attention_form_submit').serializeArray()}, 'add_attention_form');
	});

	$('button.invoice_export_save_data').click(function(){
        ajax_call_function({
        					call_type: 'invoice_export_update_data', 
        					update_form: $('form#invoice_export_update_form').serializeArray()
        				}, 'invoice_export_update_form'); 
    });
    $('a.invoice_export_search_filter_action').click(function(){

    	if($(this).attr('action_value') == 'show') {

			$('div.invoice_export_search_filter').show();
			$('a.invoice_export_search_filter_action').html('').html('Remove Search Filter');
			$(this).attr('action_value', 'hide');
    	} else {

			$('div.invoice_export_search_filter').hide();
			$('a.invoice_export_search_filter_action').html('').html('Add Search Filter');
			$(this).attr('action_value', 'show');
    	}
	});
	$('div#invoice_export_search_filter_div').on('click', 'button.invoice_export_search_form_submit', function(){
		ajax_call_function({
        					call_type: 'invoice_export_search_filter', 
        					search_filter_form: $('form#invoice_export_search_filter_form').serializeArray(),
        					limit: $('div#export_invoice_paggination input#limit').val(),
        					offset: $('div#export_invoice_paggination input#offset').val()
        				}, 'invoice_export_search_filter'); 
	});
	$('div#export_invoice_paggination').on('click', 'li.listing_paggination_number', function(){
		ajax_call_function({
        					call_type: 'invoice_export_paggination_filter', 
        					search_filter_form: $('form#invoice_export_search_filter_form').serializeArray(),
        					limit: $(this).attr('limit'),
        					offset: $(this).attr('offset')
        				}, 'invoice_export_paggination_filter');
	});
	$('div#export_invoice_paggination').on('change', 'select#set_limit', function(){
		ajax_call_function({
        					call_type: 'invoice_export_paggination_filter', 
        					search_filter_form: $('form#invoice_export_search_filter_form').serializeArray(),
        					limit: $('select[name="paggination_limit"]').val(),
        					offset: 0
        				}, 'invoice_export_paggination_filter');
	});
	$('div.invoice_search_filter').on('input', 'input[name="customer_name"]', function () {

        var search_text = $('input[name="customer_name"]').val();
        if (search_text.length <= 3) {

            toastr.info('Please Enter more than 3 character to update company name!');
            return false;
        }
        $('select[name="client_id"]').html('');
        // $('div#table_loader').show();
        ajax_call_function({
            call_type: 'search_invoice_client_name',
            client_name: search_text,
        }, 'search_invoice_client_name');
    });

	

	document.addEventListener('keydown', function (event) {
		if (event.shiftKey) {

			if (event.key === 'I') {

				ajax_call_function({ call_type: 'change_type', type: 'in', invoice_search_form: $('form#invoice_search_filter_form').serializeArray(), invoice_sort_name: $('#column_name').val(), invoice_sort_value: $('#column_value').val(), limit: $('#invoice_limit').val(), offset: $('#invoice_offset').val() }, 'change_type');

			} else if (event.key === 'Z') {

				ajax_call_function({ call_type: 'change_type', type: 'zen', invoice_search_form: $('form#invoice_search_filter_form').serializeArray(), invoice_sort_name: $('#column_name').val(), invoice_sort_value: $('#column_value').val(), limit: $('#invoice_limit').val(), offset: $('#invoice_offset').val() }, 'change_type');
			} else if (event.key === 'O') {

				ajax_call_function({ call_type: 'change_type', type: 'om', invoice_search_form: $('form#invoice_search_filter_form').serializeArray(), invoice_sort_name: $('#column_name').val(), invoice_sort_value: $('#column_value').val(), limit: $('#invoice_limit').val(), offset: $('#invoice_offset').val() }, 'change_type');
			} else if (event.key === 'A') {

				ajax_call_function({ call_type: 'change_type', type: '', invoice_search_form: $('form#invoice_search_filter_form').serializeArray(), invoice_sort_name: $('#column_name').val(), invoice_sort_value: $('#column_value').val(), limit: $('#invoice_limit').val(), offset: $('#invoice_offset').val() }, 'change_type');
			}
		}
	});

	$('input[name="radio_company_type"]').click(function () {

		ajax_call_function({ call_type: 'change_type', type: $("input[type='radio'][name='radio_company_type']:checked").val(), invoice_search_form: $('form#invoice_search_filter_form').serializeArray(), invoice_sort_name: $('#column_name').val(), invoice_sort_value: $('#column_value').val(), limit: $('#invoice_limit').val(), offset: $('#invoice_offset').val() }, 'change_type');
	});
});

function call_invoice_total_highchart(){

	ajax_call_function({ call_type: 'invoice_total_highchart' }, 'invoice_total_highchart');
}
function add_total(count_number) {
	
	var quantity = $('input[name="quantity_'+count_number+'"]').val();
	var rate = $('input[name="rate_'+count_number+'"]').val();
	var net_total = rate * quantity;
	$('input[name="price_'+count_number+'"]').val(net_total);
	if(net_total > 0) {

		set_invoice_order_details(net_total, count_number);
	}
}

function set_invoice_order_details(net_total, count_number) {

	ajax_call_function({call_type: 'set_invoice_order_details', net_total: net_total, count_number: count_number}, 'set_invoice_order_details');
	ajax_call_function({call_type: 'get_invoice_net_total',invoice_id: '<?php echo $this->uri->segment("3",0);?>'}, 'get_invoice_net_total');
}
function ajax_call_function(data, callType, url = "<?php echo base_url('invoices/ajax_function'),'/',$this->uri->segment(3); ?>") {

	$.ajax({
		type: 'POST',
		data: data,
		url: url,
		dataType: 'JSON',
		success: function(res){
			if(res.status == 'successful') {
				
				if(callType == 'search_filter' || callType == 'sort_filter' || callType == 'paggination_filter') {
					$('tbody#invoice_body').html('').html(res.invoice_body);
					$('div#invoice_search_filter_form_div').html('').html(res.invoice_search_filter);
					$('thead#invoice_header').html('').html(res.invoice_sorting);
					$('div#invoice_paggination').html('').html(res.invoice_paggination);
					$('input#column_name').val(res.invoice_sorting_column_details.column_name);
					$('input#column_value').val(res.invoice_sorting_column_details.column_value);
					Invoice_Bootstrap_Select.init();
					Invoice_Bootstrap_Datepicker.init();
					$('div#invoice_table_loader').hide();
					if(callType == 'search_filter') {

						toastr.info("Search Filter is Applied");
					} else if (callType == 'sort_filter') {

						toastr.info("Sorting Filter is Applied");
					} else if (callType == 'paggination_filter') {

						toastr.info("Paggination Filter is Applied");
					}
				} else if(callType == 'add_new_invoice') {

					swal({
                        "title": "",
                        "text": res.message,
                        "type": "success",
                        "confirmButtonClass": "btn btn-secondary"
                    });	
					set_reset_spinner('add_invoice', 'button.add_invoice', false);
					setTimeout(function() { 
						location.reload();
				    }, 2000);
				} else if(callType == 'invoice_total_highchart') {
					invoice_total_highchart(res.invoice_total_highchart_data);
				} else if(callType == 'add_invoice_order') {
					$('tbody#product_list_body').append(res.invoice_body);
					KTBootstrapMaxlength.init();
					Invoice_Bootstrap_Select.init();
				} else if(callType == 'delete_invoice_order') {
					swal({
			    		title: "Invoice Order is deleted",
			      		icon: "success",
			    	});
					ajax_call_function({call_type: 'get_invoice_order_body',invoice_id: '<?php echo $this->uri->segment("3",'');?>'}, 'get_invoice_order_body');
				} else if(callType == 'get_invoice_order_body') {
					
					$('tbody#product_list_body').html(res.invoice_order_body);
					$('a.add_item_to_invoice_order').attr('next_count_number',res.count_no);
					Invoice_Bootstrap_Select.init();
				} else if(callType == 'get_invoice_net_total') {
					$('tbody#net_total_gross_total_body').html(res.invoice_net_total);
				} else if(callType == 'add_gst_or_discount') {
					if(res.message == 'net_total_not_found') {
						swal({
		                    "title": "",
		                    "text": "",
		  					"icon": "error",
		                    "confirmButtonClass": "btn btn-secondary"
		                });	
		                swal({
							  title: "Quantity or Price is not added!!!",
							  text: "",
							  icon: "error",
							  buttons:  ["Cancel", "Okay"],
							  dangerMode: true,	
							})
						.then((willDelete) => {
							$('div#add_gst_or_discount').modal('hide');
						  	if (willDelete) {
					  		} else {
						  	}
						});		                
					}else{
						$('table#charge_history_table').html(res.discount_history);
					}
				} else if(callType == 'get_client_name') {
					$('select[name="member_id"]').html(res.member_name);
				} else if(callType == 'save_invoice') {
					swal({
						  title: "Invoice is Added",
						  text: "",
						  icon: "success",
						  buttons:  ["View Invoice Listing", "View Previous Invoice"],
						  successMode: true,	
						})
					.then((willDelete) => {
					  	if (willDelete) {
					  		location.href = "<?php echo base_url('invoices/add_invoice/');?>"+res.invoice_id;
				  		} else {
					  		location.href = "<?php echo base_url('invoices/invoice_listing_new');?>";
					  	}
					});
				} else if(callType == 'delete_invoice') {
					swal({
			    		title: "Invoice is delete successfully",
			      		icon: "success",
			    	});
			    	set_reset_spinner($('button.delete_invoice'), false, 'kt-spinner--success');
				} else if(callType == 'get_member_details') {
					$('table#attention_list_table').html('').html(res.member_list_html);
					$('input#company_name').val(res.company_name_html);
					$('input#company_id').val(res.company_id_html);
				} else if(callType == 'add_attention_form') {
					swal({
			    		title: res.message,
			      		icon: res.icon,
			    	});
		    	} else if(callType=='invoice_export_search_filter' || callType=='invoice_export_paggination_filter') {
					$('div#invoice_export_search_filter_div').html('').html(res.search_filter_html);
					$('tbody#export_invoice_body').html('').html(res.table_body_html);
					$('div#export_invoice_paggination').html('').html(res.paggination_html);
		    	} else if(callType=='invoice_export_update_form') {
		    		toastr.info("information is updated");
				} else if (callType == 'change_type') {
					
					$('tbody#invoice_body').html('').html(res.table_body_html);
					$('div#invoice_paggination').html('').html(res.total_amount);
					$('input#column_name').val(res.limit);
					$('input#column_value').val(res.offset);
					$('input#column_name').val(res.invoice_sorting_column_details.column_name);
					$('input#column_value').val(res.invoice_sorting_column_details.column_value);
					call_invoice_total_highchart();
					$('div#invoice_table_loader').hide();
				} else if (callType == 'search_invoice_client_name') {

					$('div.invoice_search_filter select[name="client_id"]').html(res.client_list_html).selectpicker('refresh');
					$('div#table_loader').hide();
					toastr.info('Company Name is Updated!');
				}
			}
		},
		beforeSend: function(response){
			if(callType == 'search_filter' || callType == 'sort_filter' || callType == 'paggination_filter') {
				$('div#invoice_table_loader').show();
			}
		}
	});
};

function search_filter() {


	ajax_call_function(
					{
						call_type: 'search_filter',
						search_form_data: $('form#invoice_search_filter_form').serializeArray(),
						limit: $('#invoice_limit').val(),
						offset:$('#invoice_offset').val(),
						invoice_sort_name: $('#column_name').val(),
						invoice_sort_value: $('#column_value').val(),
					},
					'search_filter'
				);
}

function invoice_export_search_filter() {
	ajax_call_function(
					{
						call_type: 'export_search_filter',
						search_form_data: $('form#export_search_filter_form').serializeArray(),
						invoice_id:$(this).attr('invoice_id'),
						limit: $('#invoice_limit').val(),
						offset:$('#invoice_offset').val()
					},
					'search_filter'
				);
}

function set_reset_spinner(obj, set_unset_flag = true, spinner_color = 'kt-spinner--light') {

	if(set_unset_flag) {

		$(obj).addClass('kt-spinner');
    	$(obj).addClass('kt-spinner--right');
    	$(obj).addClass('kt-spinner--sm');
    	$(obj).addClass(spinner_color);	
	} else{

		$(obj).removeClass('kt-spinner');
    	$(obj).removeClass('kt-spinner--right');
    	$(obj).removeClass('kt-spinner--sm');
    	$(obj).removeClass(spinner_color);
	}
}
// Class definition

var Invoice_Bootstrap_Datepicker = function () {

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }
    
    // Private functions
    var demos = function () {   
        // enable clear button 
        $('#invoice_date_picker, #invoice_date_picker_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: true,
            templates: arrows,
            format: 'yyyy-mm-dd',
        });

    };
    var proformadaterangepickerInit = function() {
    
        if ($('#kt_proforma_daterangepicker').length == 0) {
            return;
        }

        var picker = $('#kt_proforma_daterangepicker');
        var start = moment().subtract(1, 'days');
        var end = moment().subtract(1, 'days');

        function cb(start, end, label) {
            var title = '';
            var range = '';
            if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D Y');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D Y');
            } else {                
                range = start.format('MMM D Y') + ' - ' + end.format('MMM D Y');
            }

            if(label == 'first_time') {

                $('#kt_proforma_daterangepicker_title').html('Select');
                $('#kt_proforma_daterangepicker_date').html('Date');
                $('input#invoice_date').val('');
            } else {

                $('#kt_proforma_daterangepicker_title').html(title);
                $('#kt_proforma_daterangepicker_date').html(range);
                $('input#invoice_date').val(range);
            }
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end, 'first_time');
    };

    return {
        // public functions
        init: function() {
            demos(); 
            proformadaterangepickerInit(); 
        }
    };
}();

var Invoice_Bootstrap_Select = function () {
					    
    // Private functions
    var demos = function () {
        // minimum setup
        $('.financial_year_select_picker').selectpicker();
        $('.country_name_select_picker').selectpicker();
        $('.invoice_select_picker').selectpicker();
        $('.payment_terms_select_picker').selectpicker();
    }

    return {
        // public functions
        init: function() {
            demos(); 
        }
    };
}();
var KTAutosize = function () {

    // Private functions
    var demos = function () {
        // basic demo
        var demo1 = $('#remarks');
        autosize(demo1);
    }

    return {
        // public functions
        init: function() {
            demos(); 
        }
    };
}();
// Class definition

var KTBootstrapMaxlength = function () {
    
    // Private functions
    var demos = function () {
        // always show
        $('.hscode').maxlength({
            alwaysShow: true,
            threshold: 5,
            placement: 'top-right',
            warningClass: "kt-badge kt-badge--info kt-badge--rounded kt-badge--inline",
            limitReachedClass: "kt-badge kt-badge--info kt-badge--rounded kt-badge--inline"
        });
    }

    return {
        // public functions
        init: function() {
            demos();  
        }
    };
}();
toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": true,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": true,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
};

function invoice_total_highchart(highchart_data) {
	console.log(highchart_data);
	Highcharts.chart('invoice_total_highchart', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: 'Total Invoice'
	    },
	    subtitle: {
	        text: ''
	    },
	    xAxis: {
	        categories: [
	            'Jan',
	            'Feb',
	            'Mar',
	            'Apr',
	            'May',
	            'Jun',
	            'Jul',
	            'Aug',
	            'Sep',
	            'Oct',
	            'Nov',
	            'Dec'
	        ],
	        crosshair: true
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Total Invoice is US Dollar'
	        }
	    },
	    tooltip: {
	        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	            '<td style="padding:0"><b>${point.y:.1f}</b></td></tr>',
	        footerFormat: '</table>',
	        shared: true,
	        useHTML: true
	    },
	    plotOptions: {
	        column: {
	            pointPadding: 0.2,
	            borderWidth: 0
	        }
	    },
	    // series: [{"name":2018,"data":[95421,81447,264768,153695,131686,189921,330266,516395,80345,185983,89970,282280]}]
	    series: highchart_data
	});
	// Highcharts.chart('invoice_total_highchart', {
	// 	// Create the chart
	//     chart: {
	//         type: 'column'
	//     },
	//     title: {
	//         text: 'Total Invoice for Year: <b>2021</b>'
	//     },
	//     subtitle: {
	//         text: ''
	//     },
	//     accessibility: {
	//         announceNewData: {
	//             enabled: true
	//         }
	//     },
	//     xAxis: {
	//         type: 'category'
	//     },
	//     yAxis: {
	//         title: {
	//             text: 'Total Invoice is Rupees'
	//         }

	//     },
	//     legend: {
	//         enabled: false
	//     },
	//     plotOptions: {
	//         series: {
	//             borderWidth: 0,
	//             dataLabels: {
	//                 enabled: true,
	//                 format: '{point.y}'
	//             }
	//         }
	//     },

	//     tooltip: {
	//         headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	//         pointFormat: '<span style="color:{point.color}">{point.name}</span>: Total Revenue is <b>{point.y}</b><br/>'
	//     },

	//     series: [
	//         {
	//             name: "Total Invoice",
	//             colorByPoint: true,
	//             data: highchart_data
	//         }
	//     ]
	// });
}
// Class definition

var InvoiceFormControls = function () {
    // Private functions

    var demo1 = function () {
        $( "#invoice_details_form" ).validate({
            // define validation rules
            rules: {
                invoice_no: {
                    required: true
                },
                invoice_date: {
                    required: true
                },
                company_id: {
                    required: true
                },
				invoice_status: {
                    required: true
                },
				assigned_to: {
                    required: true
                },
			    currency_id: {
                    required: true
                },
                //member_id: {
                //    required: true
                //},
                mode: {
                    required: true
                }
                // options: {
                //     required: true,
                //     minlength: 2,
                //     maxlength: 4
                // }
            },

            errorPlacement: function(error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function(event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                ajax_call_function({ 
									call_type: 'save_invoice',
									invoice_order_details: $('form#invoice_order_form').serializeArray(),
									invoice_details: $('form#invoice_details_form').serializeArray()
									},
									'save_invoice'
								);
				set_reset_spinner($(this));
            }
        });
    }

    return {
        // public functions
        init: function() {
            demo1();
        }
    };
}();