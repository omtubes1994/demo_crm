<?php 
if(!empty($invoice_list)) {
	foreach ($invoice_list as $invoice_list_key => $invoice_list_value) {
?>
	<tr class="invoice_font">
        <td style="text-align:center;"><?php echo $invoice_list_key+1; ?></br>	
			<?php if($invoice_list_value['type'] == 'om'){ ?>
				<span class="kt-badge kt-badge--success  kt-badge--inline kt-badge--pill" style="color: #000; font-weight: 800; font-size: 14px;">OM</span>
			<?php }else if($invoice_list_value['type'] == 'zen'){ ?>
				<span class="kt-badge kt-badge--primary  kt-badge--inline kt-badge--pill" style="color: #000; font-weight: 800; font-size: 14px;">ZEN</span>
			<?php }else if($invoice_list_value['type'] == 'in'){ ?>
				<span class="kt-badge kt-badge--warning  kt-badge--inline kt-badge--pill" style="color: #000; font-weight: 800; font-size: 14px;"> IN </span>
			<?php }?>
		</td>
		<td><?php echo $invoice_list_value['invoice_no']; ?></td>
		<td>
			<?php if($invoice_list_value['invoice_status'] == 'dispatched'){ ?>

				<span class="kt-badge kt-badge--unified-success kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline" style="font-size: 14px;">Dispatched</span>
			<?php }else { ?>

				<span class="kt-badge kt-badge--unified-danger kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline" style="font-size: 14px;">Not Dispatched</span>
			<?php } ?>
		</td>
		<td><?php echo $user_list[$invoice_list_value['assigned_to']]; ?></td>
		<td><?php echo date('F, j Y', strtotime($invoice_list_value['invoice_date'])); ?></td>
		<td><?php echo $invoice_list_value['customer_name']; ?></td>
		<!-- <td><?php echo $invoice_list_value['member_name']; ?></td> -->
		<td><?php echo $invoice_list_value['country']; ?></td>
		<td class="highlight" title="Grand Total" style="font-weight: bold; color: rgb(113, 106, 202); float: right;">
			<?php echo $currency_list[$invoice_list_value['currency_id']], number_format($invoice_list_value['grand_total'],2,'.',','); ?>		
		</td>
		<td>
			<a href="<?php echo base_url('pdf_management/invoice_pdf/'.$invoice_list_value['invoice_mst_id']);?>" target="_blank" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View Invoice" >
				<i class="la la-eye"></i>
			</a>
			<?php if(!in_array($this->session->userdata('role'), array('5', '16'))){?>
            <a href="<?php echo base_url('invoices/add_invoice/'.$invoice_list_value['invoice_mst_id']);?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit" target="_blank">
                <i class="la la-edit"></i>
            </a>
            <button class="btn btn-sm btn-clean btn-icon btn-icon-md delete_invoice" title="Delete" invoice_id="<?php echo $invoice_list_value['invoice_mst_id'];?>">
            	<i class="la la-trash"></i>
            </button>
			<?php } ?>
		</td>  
    </tr>
<?php 
	}	
} 
?>
<tr>  
	<td colspan="7" class="kt-font-boldest kt-font-info kt-font-xl kt-align-center">Total</td>
	<td colspan="2" class="kt-font-boldest kt-font-info kt-font-xl kt-align-center"><?php echo '$'.number_format(array_sum(array_column($invoice_list, 'grand_total')),2,'.',','); ?></td>
</tr>