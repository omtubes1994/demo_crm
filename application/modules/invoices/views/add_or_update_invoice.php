<style type="text/css">
	.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
		padding-top: 4% !important;
	}
	.layer-white{
	    display: none;
	    position: absolute;
	    top: 0em !important;
	    left: 0em !important;
	    width: 100%;
	    height: 100%;
	    text-align: center;
	    vertical-align: middle;
	    background-color: rgba(255, 255, 255, 0.55);
	    opacity: 1;
	    line-height: 1;
	    -webkit-animation-fill-mode: both;
	    animation-fill-mode: both;
	    -webkit-animation-duration: 0.5s;
	    animation-duration: 0.5s;
	    -webkit-transition: background-color 0.5s linear;
	    transition: background-color 0.5s linear;
	    -webkit-user-select: none;
	    -moz-user-select: none;
	    -ms-user-select: none;
	    user-select: none;
	    will-change: opacity;
	    z-index: 9;
	}
	.div-loader{
	    position: absolute;
		top: 50%;
		left: 50%;
		margin: 0px;
		text-align: center;
		z-index: 1000;
		-webkit-transform: translateX(-50%) translateY(-50%);
		transform: translateX(-50%) translateY(-50%);
	}
	.kt-spinner:before {
	    width: 50px;
	    height: 50px;
	    margin-top: -10px;
	}
</style>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col">
			<div class="alert alert-light alert-elevate fade show" role="alert">
				<div class="alert-icon"><i class="flaticon-edit kt-font-brand"></i></div>
				<div class="alert-text">
					ADD Invoice.
				</div>
				<div>
					<button type="reset" class="btn btn-success save_invoice">Save</button>
				</div>
			</div>
		</div>
	</div>
	<!--begin::Portlet-->
	<div class="kt-portlet">
		<div class="kt-portlet__body">
			<div class="kt-form__section kt-form__section--first">
				<!--begin::Form-->
				<form class="kt-form" id="invoice_details_form">
					<input type="text" name="invoice_mst_id" value="<?php echo $invoice_details['invoice_mst_id'];?>" hidden>
					<div class="form-group row validate is-invalid">
						<div class="col-lg-3 col-md-9 col-sm-12">
							<div class="form-group form-group-last kt-hide">
								<div class="alert alert-danger" role="alert" id="kt_form_1_msg">
									<div class="alert-icon"><i class="flaticon-warning"></i></div>
									<div class="alert-text">
										Oh snap! Change a few things up and try submitting again.
									</div>
									<div class="alert-close">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true"><i class="la la-close"></i></span>
										</button>
									</div>
								</div>
							</div>
							<label class="col-form-label" style="font-weight: 500;color: #212529;">Invoice #</label>
							<div class="input-group date">
								<input type="text" class="form-control" placeholder="Please enter invoice #" name="invoice_no" value="<?php echo $invoice_details['invoice_no'];?>">
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="la la-calendar"></i>
									</span>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-9 col-sm-12">
					      	<label class="col-form-label" style="font-weight: 500;color: #212529;">invoice Date</label>
							<div class="input-group date">
								<input type="text" class="form-control" readonly value="<?php echo $invoice_details['invoice_date'];?>" id="invoice_date_picker" name="invoice_date" />
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="la la-calendar"></i>
									</span>
								</div>
							</div>
						</div>
						<div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
					      	<label class="col-form-label" style="font-weight: 500;color: #212529;">Company</label>
							<select class="form-control invoice_select_picker"  data-live-search="true" name="company_id">
						      	<option value="">Select Company</option>
					         	<?php foreach($customer as $single_customer) {?>
									<option 
										value="<?php echo $single_customer['id'];?>"
										<?php echo ($invoice_details['company_id'] == $single_customer['id'])?'selected':'';?>
										data-tokens="<?php echo $single_customer['name'];?>">
										<?php echo $single_customer['name']; ?>
										<?php //echo $single_client['client_name'], '(', $country[$single_client['country']] ,')' ;?>
									</option>
					         	<?php } ?>     
							</select>
					   	</div>
					   	<div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
					      	<label class="col-form-label kt-hidden" style="font-weight: 500;color: #212529;">Attention</label>
					      	<div class="input-group date kt-hidden">
								<select class="form-control" name="member_id">
							      	<option value="">Select Member</option>
						      		<?php
					      			if(!empty($member_details)) {
					      				foreach ($member_details as $single_member) { ?>
					      					<option 
												value="<?php echo $single_member['comp_dtl_id'];?>"
												<?php echo ($invoice_details['member_id'] == $single_member['comp_dtl_id'])?'selected':'';?>
												data-tokens="<?php echo $single_member['member_name'];?>">
													<?php echo $single_member['member_name']; ?>
					      					</option>
					      				<?php 
					      				}
					      			}
						      		?>   
								</select>
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="kt-font-info kt-font-bolder la la-plus add_attention" data-toggle="modal" data-target="#add_attention"></i>
									</span>
								</div>
							</div>	
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-10 col-md-9 col-sm-12">
							<label class="col-form-label" style="font-weight: 500;color: #212529;">Purchase Order</label>
							<div class="input-group date">
								<div class="col-lg-12 col-md-9 col-sm-12">
									<textarea class="form-control" name="remarks" rows="1"><?php echo $invoice_details['remarks'];?></textarea>
								</div>
							</div>
						</div>
						<div class="col-lg-2 col-md-9 col-sm-12">
					      	<label class="col-form-label" style="font-weight: 500;color: #212529;">Currency</label>
							<select class="form-control" name="currency_id">
						      	<option value="">Select Currency</option>
						      	<?php foreach ($currency_list as $currency_details) { ?>
						      		<option value="<?php echo $currency_details['currency_id'];?>" <?php echo ($invoice_details['currency_id'] == $currency_details['currency_id'])?'selected':'';?>><?php echo $currency_details['currency'];?></option>
						      	<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-3 col-md-9 col-sm-12">
							<label class="col-form-label" style="font-weight: 500;color: #212529;">Transport Mode</label>
							<select class="form-control" name="mode">
						      	<option value="">Select Company</option>
								<option value="air" <?php echo ($invoice_details['mode'] == 'air')?'selected':'';?>>Airways</option>
								<option value="sea" <?php echo ($invoice_details['mode'] == 'sea')?'selected':'';?>>Seaways</option>
							</select>
						</div>
						<div class="col-lg-3 col-md-9 col-sm-12">
							<label class="col-form-label" style="font-weight: 500;color: #212529;">Type</label>
							<select class="form-control" name="type">
								<option value="">Select Type</option>
								<option value="om" <?php echo ($invoice_details['type'] == 'om')?'selected':'';?>>OM</option>
								<option value="zen" <?php echo ($invoice_details['type'] == 'zen')?'selected':'';?>>ZEN</option>
								<option value="in" <?php echo ($invoice_details['type'] == 'in')?'selected':'';?>>IN</option>
							</select>
						</div>
						<div class="col-lg-3 col-md-9 col-sm-12">
							<label class="col-form-label" style="font-weight: 500;color: #212529;">Status</label>
							<select class="form-control" name="invoice_status">
								<option value="">Select Status</option>
								<option value="dispatched" <?php echo ($invoice_details['invoice_status'] == 'dispatched')?'selected':'';?>>Dispatched</option>
								<option value="not_dispatched" <?php echo ($invoice_details['invoice_status'] == 'not_dispatched')?'selected':'';?>>Not Dispatched</option>
							</select>
						</div>
						<div class="col-lg-3 col-md-9 col-sm-12">
							<label class="col-form-label" style="font-weight: 500;color: #212529;">Status</label>
							<select class="form-control invoice_select_picker"  data-live-search="true" name="assigned_to">
								<option value="">Select sales person</option>
								<?php foreach($sales_person_list as $single_sales_person) {?>
									<option
										value="<?php echo $single_sales_person['user_id'];?>"
										<?php echo ($invoice_details['assigned_to'] == $single_sales_person['user_id'])?'selected':'';?>
										data-tokens="<?php echo $single_sales_person['name'];?>">
										<?php echo $single_sales_person['name']; ?>
									</option>
								<?php } ?>
							</select>
						</div>
					</div>
				</form>

				<!--end::Form-->
				<!--begin::Form-->
				<form class="kt-form" id="invoice_order_form">
					<div class="row">
						<div class="col-lg-12" style="padding: 0px 0px 0px 0px !important;">
							<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline">
								<thead>
									<tr role="row">
										<th colspan="8" style="text-align: center;">ADD ORDER ITEMS</th>		
										<th style="text-align: center;">
											<a href="javascript:;" class="btn-sm btn btn-label-success btn-bold add_item_to_invoice_order" next_count_number="">
												<i class="la la-plus">Add</i>
											</a>
										</th>		
									</tr>
									<tr role="row">
										<th class="sorting_disabled" style="">Sr #</th>
										<th class="sorting_disabled" style="">Description</th>
										<th class="sorting_disabled" style="">Hscode</th>
										<th class="sorting_disabled" style="">Quantity</th>
										<th class="sorting_disabled" style="">Rate</th>
										<th class="sorting_disabled" style="">Total</th>
										<th class="sorting_disabled" style="">Product</th>
										<th class="sorting_disabled" style="">Material</th>
										<th class="sorting_disabled" style="">Actions</th>
									</tr>
								</thead>
								<tbody id="product_list_body" style="height: 150px;">
								</tbody>
							</table>
							<div id="invoice_table_loader" class="layer-white">
								<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
							</div>
						</div>
					</div>
					
				</form>

				<!--end::Form-->
				<div class="kt-invoice__container" style="">
					<div class="table table-striped- table-bordered table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th>
										Different Charges or Discount
										<a href="#" class="btn btn-icon add_gst_or_discount_modal" data-toggle="modal" data-target="#add_gst_or_discount">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--md">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<polygon points="0 0 24 0 24 24 0 24"></polygon>
													<path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
													<path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" fill="#000000"></path>
												</g>
											</svg>
										</a>
									</th>
									<th style="width: 20%;">NET AMOUNT</th>
									<th style="width: 20%;">GROSS AMOUNT</th>
								</tr>
							</thead>
							<tbody id="net_total_gross_total_body">
								
							</tbody>
						</table>
					</div>
				</div>
			</div>	
		</div>
	</div>

	<!--end::Portlet-->
</div>

<!-- end:: Content -->
<div class="modal fade" id="add_gst_or_discount" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Other Charges</h5>
				<button type="reset" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
       	<form id="add_charges_in_invoice_order_form">
       		<div class="row">
       			<div class="col-md-6 form-group">
       				<label for="contact_date">Charge Name</label>
       				<select class="form-control" name="charges_name">
       					<option value="">Select Charge Name</option>
       					<option value="discount">Discount</option>
       					<option value="freight_charge">Freight</option>
       					<option value="other_charge">Other Charges</option>
       				</select>
       			</div>
       			<div class="col-md-6 form-group">
       				<label for="contact_date">Charge Should Be Added Or Deducted From Net Total</label>
       				<select class="form-control" name="add_minus_charges">
       					<option value="">Select</option>
       					<option value="+">Add To Net Total</option>
       					<option value="-">Subtract To Net Total</option>
       				</select>
       			</div>
       		</div>
       		<div class="row">
       			
       			<div class="col-md-6 form-group">
       				<label for="contact_date">Charge Value</label>
       				<input type="text" name="charge_value" class="form-control" value="">
       			</div>
       			<div class="col-md-6 form-group" style="padding: 3% 2% 0% 0%;">
       				<button class="btn btn-success add_charges_in_invoice_order" type="reset" style="float: right;">Submit</button>
       			</div>
       		</div>
       		<div class="clearfix"></div>
       	</form>
       	<hr/>
       	<h4>Charges History</h4>
       <div>
        	<table class="table table-bordered" id="charge_history_table">
        		
        	</table>
       </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="add_attention" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Other Charges</h5>
				<button type="reset" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form id="add_attention_form_submit">
		       		<div class="row">	       			
		       			<div class="col-md-4 form-group">
		       				<label for="contact_date">Company Name</label>
		       				<input type="text" id="company_name" class="form-control" value="" readonly>
		       				<input type="text" id="company_id" name="client_id" class="form-control" value="" hidden>
		       			</div>
		       			<div class="col-md-4 form-group">
		       				<label for="contact_date">Attention</label>
		       				<input type="text" name="name" class="form-control" value="">
		       			</div>
		       			<div class="col-md-4 form-group">
		       				<label for="contact_date">Email</label>
		       				<input type="text" name="email" class="form-control" value="">
		       			</div>
		       		</div>
		       		<div class="row">	       			
		       			<div class="col-md-4 form-group">
		       				<label for="contact_date">Mobile</label>
		       				<input type="text" name="mobile" class="form-control" value="">
		       			</div>
		       			<div class="col-md-4 form-group">
		       				<label for="contact_date">WhatsApp</label>
		       				<input type="text" name="is_whatsapp" class="form-control" value="">
		       			</div>
		       			<div class="col-md-4 form-group">
		       				<label for="contact_date">Skype</label>
		       				<input type="text" name="skype" class="form-control" value="">
		       			</div>
		       		</div>
		       		<div class="row">	       			
		       			<div class="col-md-4 form-group">
		       				<label for="contact_date">Telephone</label>
		       				<input type="text" name="telephone" class="form-control" value="">
		       				<input type="text" name="status" class="form-control" value="Y" hidden>
		       				<input type="text" name="entered_by" class="form-control" value="<?php echo $this->session->userdata('user_id');?>" hidden>
		       				<input type="text" name="entered_on" class="form-control" value="<?php echo date('Y-m-d h:i:s');?>" hidden>
		       			</div>
		       			<div class="col-md-6 form-group">
		       			</div>
		       			<div class="col-md-2 form-group" style="padding: 3% 2% 0% 0%;">
		       				<button class="btn btn-success add_attention_form_submit" type="button" style="float: right;">Submit</button>
		       			</div>
		       		</div>
		       		<div class="clearfix"></div>
		       	</form>
	       		<hr/>
	       		<h4>Attention List</h4>
		       	<div>
		        	<table class="table table-bordered" id="attention_list_table">
		        		
		        	</table>
	       		</div>
	       	</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
			</div>
		</div>
	</div>
</div>