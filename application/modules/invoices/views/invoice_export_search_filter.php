<div class="row kt-margin-b-20">
    <label class="col-lg-1 col-form-label form_name">Invoice. No.</label>
    <div class="col-lg-2 form-group-sub">
        <input type="text" class="form-control" name="invoice_mst.invoice_no" value="">
    </div>
    <label class="col-lg-1 col-form-label form_name">Invoice.Date.</label>
    <div class="col-lg-2 input-group date">
        <input type="text" class="form-control" readonly id="invoice_date_picker" name="invoice_mst.invoice_date" />
        <div class="input-group-append">
            <span class="input-group-text">
                <i class="la la-calendar"></i>
            </span>
        </div>
    </div>
    <label class="col-lg-1 col-form-label form_name">Invoice.Value.</label>
    <div class="col-lg-2 form-group-sub">
        <input type="text" class="form-control" name="invoice_mst.grand_total" value="">
    </div>
    <label class="col-lg-1 col-form-label form_name">Consignee Name</label>
    <div class="col-lg-2 form-group-sub">
        <input type="text" class="form-control" name="invoice_export_info.consignee_name" value="">
    </div>
</div>
<div class="row kt-margin-b-20">            
    <label class="col-lg-1 col-form-label form_name">Port of loading</label>
    <div class="col-lg-2 form-group-sub">
        <input type="text" class="form-control" name="invoice_export_info.port_of_loading" value="">
    </div>
    <label class="col-lg-1 col-form-label form_name">Port of discharge</label>
    <div class="col-lg-2 form-group-sub">
        <input type="text" class="form-control" name="invoice_export_info.port_of_discharge" value="">
    </div>
    <label class="col-lg-1 col-form-label form_name">Final destination</label>
    <div class="col-lg-2 form-group-sub">
        <input type="text" class="form-control" name="invoice_export_info.final_destination" value="">
    </div>  
    <label class="col-lg-1 col-form-label form_name">Buyers other then consinee</label>
    <div class="col-lg-2 form-group-sub">
        <input type="text" class="form-control" name="invoice_export_info.buyer_other_then_consignee" value="">
    </div>
</div>
<div class="row kt-margin-b-20">
    <label class="col-lg-1 col-form-label form_name">Currency</label>
    <div class="col-lg-2 form-group-sub">
        <select class="form-control currency_select_picker" name="invoice_export_info.currency" multiple>
            <option value="">Select</option>
            <?php foreach($payment_terms_list as $single_payment_terms) {?>
                <option value="<?php echo $single_payment_terms['term_value'];?>">
                    <?php echo $single_payment_terms['term_value'];?>
                </option>
            <?php } ?>     
        </select>
    </div>
    <label class="col-lg-1 col-form-label form_name">Payment Terms</label>
    <div class="col-lg-2 form-group-sub">
        <select class="form-control payment_terms_select_picker" name="invoice_export_info.payment_terms" multiple>
            <option value="">Select</option>
            <?php foreach($payment_terms_list as $single_payment_terms) {?>
                <option value="<?php echo $single_payment_terms['term_value'];?>">
                    <?php echo $single_payment_terms['term_value'];?>
                </option>
            <?php } ?>     
        </select>
    </div>
    <label class="col-lg-1 col-form-label form_name">Cha name</label>
    <div class="col-lg-2 form-group-sub">
        <input type="text" class="form-control" name="invoice_export_info.cha_name" value="">
    </div>
    <label class="col-lg-1 col-form-label form_name">Outstanding amount</label>
    <div class="col-lg-2 form-group-sub">
        <input type="text" class="form-control" name="invoice_export_info.outstanding_amount" value="">
    </div>  
</div>              
<div class="row kt-margin-b-20">
    <label class="col-lg-1 col-form-label form_name">Extra</label>
    <div class="col-lg-2 form-group-sub">
        <input type="text" class="form-control" name="invoice_export_info.extra" value="">
    </div>
    <label class="col-lg-1 col-form-label form_name">PO NO</label>
    <div class="col-lg-2 form-group-sub">
        <input type="text" class="form-control" name="invoice_export_info.po_no" value="">
    </div>
    <label class="col-lg-1 col-form-label form_name">Port code</label>
    <div class="col-lg-2 form-group-sub">
        <input type="text" class="form-control" name="invoice_export_info.port_code" value="">
    </div>
    <label class="col-lg-1 col-form-label form_name">Bill given to ca</label>
    <div class="col-lg-2 form-group-sub">
        <input type="text" class="form-control" name="invoice_export_info.bill_given_to_ca" value="">
    </div>  
</div>  
<div class="row kt-margin-b-20">
    <label class="col-lg-1 col-form-label form_name">E-brc remark</label>
    <div class="col-lg-2 form-group-sub">
        <input type="text" class="form-control" name="invoice_export_info.e_brc_remark" value="">
    </div>
    <label class="col-lg-1 col-form-label form_name">Bank sub date</label>
    <div class="col-lg-2 input-group date">
        <input type="text" class="form-control" id="export_banK_sub_date" name="invoice_export_info.bank_sub_date">
        <div class="input-group-append">
            <span class="input-group-text">
                <i class="la la-calendar"></i>
            </span>
        </div>
    </div>
    <label class="col-lg-1 col-form-label form_name">Bill id no</label>
    <div class="col-lg-2 form-group-sub">
        <input type="text" class="form-control" name="invoice_export_info.bill_id_no" value="">
    </div>
    <label class="col-lg-1 col-form-label form_name">Brc no</label>
    <div class="col-lg-2 form-group-sub">
        <input type="text" class="form-control" name="invoice_export_info.brc_no" value="">
    </div>
</div>          
<div class="row kt-margin-b-20">
    <label class="col-lg-1 col-form-label form_name">Rodtep scheme</label>
    <div class="col-lg-2 form-group-sub">
        <input type="text" class="form-control" name="invoice_export_info.rodtep_scheme" value="">  
    </div>
    <label class="col-lg-1 col-form-label form_name">Final remark</label>
    <div class="col-lg-2 form-group-sub">
        <input type="text" class="form-control" name="invoice_export_info.final_remark" value="">
    </div>
</div>
<div class="row kt-margin-b-20">
    <div class="col-lg-2">
        <button class="btn btn-primary btn-brand-icon invoice_export_search_form_submit" type="reset">
            <span>
                <i class="la la-search"></i>
                <span>Search</span>
            </span>
        </button>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button class="btn btn-secondary btn-secondary-icon invoice_export_search_form_reset" type="reset">
            <span>
                <i class="la la-close"></i>
                <span>Reset</span>
            </span>
        </button>
    </div>  
</div>