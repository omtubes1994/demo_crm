<tr role="row">
	<th class="sorting_disabled" style="width: 7%;"> Record ID </th>
	<th 
		class="<?php echo $sorting['invoice_mst.invoice_no']['class_name'];?> sorting_search" 
		sorting_name="invoice_mst.invoice_no"
		sorting_value="<?php echo $sorting['invoice_mst.invoice_no']['value'];?>"
		style="width: 10%;">
		Invoice #
	</th>
	<th
		class="<?php echo $sorting['invoice_mst.invoice_status']['class_name'];?> sorting_search"
		sorting_name="invoice_mst.invoice_status"
		sorting_value="<?php echo $sorting['invoice_mst.invoice_status']['value'];?>"
		style="width: 10%;">
		Status
	</th>
	<th
		class="<?php echo $sorting['users.name']['class_name'];?> sorting_search"
		sorting_name="users.name"
		sorting_value="<?php echo $sorting['users.name']['value'];?>"
		style="width: 10%;">
		Sales Person
	</th>
	<th 
		class="<?php echo $sorting['invoice_mst.invoice_date']['class_name'];?> sorting_search" 
		sorting_name="invoice_mst.invoice_date"
		sorting_value="<?php echo $sorting['invoice_mst.invoice_date']['value'];?>"
		style="width: 10%;">
		Invoice Date
	</th>
	<th 
		class="<?php echo $sorting['customer_mst.name']['class_name'];?> sorting_search" 
		sorting_name="customer_mst.name"
		sorting_value="<?php echo $sorting['customer_mst.name']['value'];?>"
		style="width: 25%;">
		Company Name
	</th>
	<!-- <th class="sorting_disabled" style="width: 15%;"> Attention </th> -->
	<th 
		class="<?php echo $sorting['country_mst.name']['class_name'];?> sorting_search" 
		sorting_name="country_mst.name"
		sorting_value="<?php echo $sorting['country_mst.name']['value'];?>"
		style="width: 10%;">
		Country
	</th>    
	<th 
		class="<?php echo $sorting['invoice_mst.grand_total']['class_name'];?> sorting_search" 
		sorting_name="invoice_mst.grand_total"
		sorting_value="<?php echo $sorting['invoice_mst.grand_total']['value'];?>"
		style="width: 10%;">
		Amount
	</th>
	<th class="sorting_disabled" style="width: 8%;">Actions</th>
</tr>

