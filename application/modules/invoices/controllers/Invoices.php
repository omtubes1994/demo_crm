<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoices extends MX_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('user_id')){
			redirect('login', 'refresh');
			exit;
		}else{
			$access_count = 0;
			if(!empty($this->session->userdata('main_module_access')) && in_array(3, array_column($this->session->userdata('main_module_access'), 'module_id'))) {

				foreach ($this->session->userdata('sub_module_access') as $key => $value) {
					
					if($value['module_id'] == 3 && $value['status'] == 'Active') {
						$access_count++;
					}
				}
			}
			if($access_count == 0){
				redirect($this->session->userdata('home/dashboard'));
				exit;
			}
		}
		error_reporting(0);
		$this->load->model('client/client_model');
		$this->load->model('invoice_model');
		$this->load->model('quotations/quotation_model');
		$this->load->model('common/common_model');
	}

	public function transer_data() {
		$this->invoice_model->transer_data();
	}

	public function index(){
		redirect('invoices/new');
	}

	public function new($invoice_id=0){
		if(!empty($this->input->post())){
			$invoice_mst_arr = array(
				'invoice_no' => $this->input->post('invoice_no'),
				'invoice_date' => date('Y-m-d', strtotime($this->input->post('invoice_date'))),
				'company_id' => $this->input->post('company'),
				'member_id' => $this->input->post('attention'),
				'remarks' => $this->input->post('remarks'),
				'freight_charge' => $this->input->post('freight_charges'),
				'other_charge' => $this->input->post('other_charges'),
				'discount' => $this->input->post('discount'),
				'mode' => $this->input->post('mode'),
			);

			if($this->input->post('invoice_id')){
				$invoice_mst_arr['modified_on'] = date('Y-m-d H:i:s');
				$invoice_mst_arr['modified_by'] = $this->session->userdata('user_id');
				$invoice_id = $this->input->post('invoice_id');
				$this->invoice_model->updateInvoice('invoice_mst', $invoice_mst_arr, array('invoice_mst_id' => $invoice_id));
				$this->invoice_model->deleteInvoice('invoice_dtl', array('invoice_mst_id' => $invoice_id));
				$msg = 'Invoice updated successfully';
			}else{
				$invoice_mst_arr['entered_on'] = date('Y-m-d H:i:s');
				$invoice_mst_arr['entered_by'] = $this->session->userdata('user_id');
				$invoice_id = $this->invoice_model->insertInvoice('invoice_mst', $invoice_mst_arr);
				$msg = 'Invoice created successfully';
			}

			$net_total = 0;
			// foreach ($this->input->post('description') as $key => $value) {
			// 	$invoice_dtl_arr = array(
			// 		'invoice_mst_id' => $invoice_id,
			// 		'product_id' => $this->input->post('product')[$key],
			// 		'material_id' => $this->input->post('material')[$key],
			// 		'description' => $this->input->post('description')[$key],
			// 		'quantity' => $this->input->post('quantity')[$key],
			// 		'rate' => $this->input->post('rate')[$key],
			// 		'price' => $this->input->post('quantity')[$key] * $this->input->post('rate')[$key]
			// 	);
			// 	$net_total += $this->input->post('quantity')[$key] * $this->input->post('rate')[$key];
			// 	$invoice_dtl_id = $this->invoice_model->insertInvoice('invoice_dtl', $invoice_dtl_arr);
			// }
			foreach ($this->input->post('description') as $key => $value) {
				$invoice_dtl_arr[] = array(
					'invoice_mst_id' => $invoice_id,
					'product_id' => $this->input->post('product')[$key],
					'material_id' => $this->input->post('material')[$key],
					'description' => $this->input->post('description')[$key],
					'quantity' => $this->input->post('quantity')[$key],
					'rate' => $this->input->post('rate')[$key],
					'hscode' => $this->input->post('hscode')[$key],
					'price' => $this->input->post('quantity')[$key] * $this->input->post('rate')[$key]
				);
				$net_total += $this->input->post('quantity')[$key] * $this->input->post('rate')[$key];
			}
			// echo "<pre>";print_r($invoice_dtl_arr);echo"</pre><hr>";exit;
			$this->invoice_model->batch_insert_data('invoice_dtl', $invoice_dtl_arr);
			$update_arr = array(
				'net_total' => $net_total,
				'grand_total' => $net_total+$this->input->post('freight_charges')+$this->input->post('other_charges')-$this->input->post('discount'),
			);
			$this->invoice_model->updateInvoice('invoice_mst', $update_arr, array('invoice_mst_id' => $invoice_id));
			$this->session->set_flashdata('success', $msg);
			redirect('invoices/list');
		}else{
			if($invoice_id != 0){
				$data['invoice_details'] = $this->invoice_model->getInvoiceDetails($invoice_id);
				$data['invoice_id'] = $invoice_id;
			}
			$data['clients'] = $this->client_model->getClients();
			$data['region'] = $this->quotation_model->getLookup(1);
			$data['country'] = $this->quotation_model->getLookup(2);
			$data['product'] = $product = $this->quotation_model->getLookup(259);
			$data['prd_str'] = $data['mat_str'] = ""; 
			foreach($product as $prod){ 
				$data['prd_str'] .= '<option value="'.$prod['lookup_id'].'">'.ucwords(strtolower($prod['lookup_value'])).'</option>';
			}

			$data['material'] = $material = $this->quotation_model->getLookup(272);
			foreach($material as $mat){ 
				$data['mat_str'] .= '<option value="'.$mat['lookup_id'].'">'.ucwords(strtolower($mat['lookup_value'])).'</option>';
			}
			$this->load->view('header', array('title' => 'Add / Edit Invoice'));
			$this->load->view('sidebar', array('title' => 'Add / Edit Invoice'));
			$this->load->view('add_invoice', $data);
			$this->load->view('footer');
		}
	}

	public function new_2() {

		$invoice_id = $this->uri->segment(3,0);
		if($invoice_id != 0){
			$data['invoice_details'] = $this->invoice_model->getInvoiceDetails($invoice_id);
			$data['invoice_id'] = $invoice_id;
		}
		$data['clients'] = $this->client_model->getClients();
		$data['region'] = $this->quotation_model->getLookup(1);
		$data['country'] = $this->quotation_model->getLookup(2);
		$data['product'] = $product = $this->quotation_model->getLookup(259);
		$data['prd_str'] = $data['mat_str'] = ""; 
		foreach($product as $prod){ 
			$data['prd_str'] .= '<option value="'.$prod['lookup_id'].'">'.ucwords(strtolower($prod['lookup_value'])).'</option>';
		}

		$data['material'] = $material = $this->quotation_model->getLookup(272);
		foreach($material as $mat){ 
			$data['mat_str'] .= '<option value="'.$mat['lookup_id'].'">'.ucwords(strtolower($mat['lookup_value'])).'</option>';
		}
		$this->load->view('header', array('title' => 'Add / Edit Invoice'));
		$this->load->view('sidebar', array('title' => 'Add / Edit Invoice'));
		$this->load->view('add_invoice_new', $data);
		$this->load->view('footer');	
	}	

	function list(){
		/*$data['records'] = $this->list_data();*/
		$finYears = $this->invoice_model->getFinancialYears();
		$this->load->view('header', array('title' => 'Invoice List'));
		$this->load->view('sidebar', array('title' => 'Invoice List'));
		$this->load->view('invoice_list_view', array('finYears' => $finYears));
		$this->load->view('footer');
	}

	function list_data(){
		$order_by = $this->input->get('columns')[$this->input->get('order')[0]['column']]['data'];
		if($order_by == 'record_id' || $order_by == 'company_name'){
			$order_by = 'client_name';
		}
		else if($order_by =='invoice_date')
		{
			$order_by = 'date(i.invoice_date)';
		}
		$searchByYear = $this->input->get('searchByFinYear');
		$dir = $this->input->get('order')[0]['dir'];
		$records = $this->invoice_model->getInvoiceList($this->input->get('start'), $this->input->get('length'), $this->input->get('search')['value'], $order_by, $dir, $searchByYear);
		$data['iTotalDisplayRecords'] = $data['iTotalRecords'] = $this->invoice_model->getInvoiceListCount($this->input->get('search')['value'], $searchByYear);
		$data['aaData'] = $records;
		echo json_encode($data);
	}

	function invoice_pdf($invoice_id){

		$data['invoice_details'] = $this->invoice_model->getInvoiceDetails($invoice_id);
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->library('Pdf');
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		/*$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Nicola Asuni');
		$pdf->SetTitle('TCPDF Example 061');
		$pdf->SetSubject('TCPDF Tutorial');
		$pdf->SetKeywords('TCPDF, PDF, example, test, guide');*/

		// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, NULL, '', array(0,0,0), array(255,255,255));
		$pdf->SetFooterData(array(0,0,0), array(255,255,255));

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(10, PDF_MARGIN_TOP-10, 10);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		    require_once(dirname(__FILE__).'/lang/eng.php');
		    $pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('helvetica', '', 10);

		// add a page
		$pdf->AddPage();

		/* NOTE:
		 * *********************************************************
		 * You can load external XHTML using :
		 *
		 * $html = file_get_contents('/path/to/your/file.html');
		 *
		 * External CSS files will be automatically loaded.
		 * Sometimes you need to fix the path of the external CSS.
		 * *********************************************************
		 */

		// define some HTML content with style


		$table_str = '';

		$i=0;
		// echo "<pre>";print_r($data['invoice_details']);echo"</pre><hr>";exit;
        foreach ($data['invoice_details'] as $key => $value) { 
			$bg_color = '';
			if($i%2 != 0){
				$bg_color = '#e4e1e1;';
			}
			$table_str .= '<tr style="background-color: '.$bg_color.'; font-size: 11px; font-family: courier;">
				<td style="text-align: right;">'.++$i.'</td>
				<td>'.$value['description'].'</td>
				<td style="text-align: right;"> '.$value['quantity'].'</td>
				<td>-</td>
				<td style="text-align: right;">'.$value['rate'].'</td>
				<td>-</td>
				<td style="text-align: right;">'.$value['price'].'</td>
			</tr>';
        }


        $total_table = '<table cellspacing="0" cellpadding="10">
		<tr >
		<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;" width="50%" >Net Total</td>
		<td style="font-size: 12px; font-family: courier; border: solid 1px grey;" width="50%">'.$data['invoice_details'][0]['net_total'].'</td>
		</tr>';

		if($data['invoice_details'][0]['freight_charge'] > 0){
			$total_table .= '<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Freight Charges</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['invoice_details'][0]['freight_charge'].'</td>
			</tr>';	
		}
		
		if($data['invoice_details'][0]['other_charge'] > 0){
			$total_table .= '<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Other Charges</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['invoice_details'][0]['other_charge'].'</td>
			</tr>';
		}

		if($data['invoice_details'][0]['discount'] > 0){
			$total_table .= '<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Discount</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['invoice_details'][0]['discount'].'</td>
			</tr>';
		}

		$total_table .= '<tr>
			<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Grand Charges</td>
			<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['invoice_details'][0]['grand_total'].'</td>
		</tr>
		</table>';
		$grand_total = $data['invoice_details'][0]['grand_total'];
        if(strpos($grand_total, '.') === false){
        	$grand_total = $grand_total.'.00';
        }
		$dec_text = 'cents';
		// $grand_total_words = $this->numberTowords($data['invoice_details'][0]['grand_total']);
		$grand_total_words = $this->numberTowords($grand_total, $dec_text);
		$date = date('Y-m-d', strtotime($data['invoice_details'][0]['invoice_date']));

		$html = '<table cellpadding="5" cellspacing="0">
		<tr style="background-color: #fff;">
		<td width="50%" style="padding:5px;vertical-align: text-top; ">
		<img src="/crm/assets/media/client-logos/logo.png" width="180" height="50" style="padding-left: 10px;"><br/>
		<strong style="font-size: 16px;">OM TUBES & FITTINGS INDUSTRIES</strong>
		<div class="left_addrs" style="font-size: 14px;color: #484545;">10 Bordi Bunglow, 1st Panjarapole Lane, CP Tank, Mumbai, Maharashtra, India <br/>GSTIN 27AFRPM5323E1ZC
		</div>
		<table style="margin-top: -15px;">
		<tr style="background-color: #fff;">
		<td style="font-size: 14px;color: #484545;" width="50%">+91 (22) 6743 7634</td>
		</tr>
		<tr style="background-color: #fff;">
		<td style="font-size: 14px;color: #484545;" align="left">www.omtubes.com</td>
		</tr>
		</table>
		</td>
		<td width="50%">
		<strong style="font-size: 25px; text-align: right; line-height: 35px;">Quotation</strong><br/>
		<table style="line-height: 22px;">
		<tr style="background-color: #e4e1e1; border-bottom: 1px solid;">
		<td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;"> Quote: </strong></td>
		<td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;">Date: </strong></td>
		</tr>
		<tr>
		<td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;">'.$data['invoice_details'][0]['invoice_no'].'</td>
		<td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;">'.$date.'</td>
		</tr>
		<tr style="background-color: #e4e1e1;">
		<td style="padding:5px;vertical-align: text-top;" width="75%"><strong style="font-size: 12px;"> Customer:</strong></td>
		<td style="padding:5px;vertical-align: text-top;" width="25%"><strong style="font-size: 12px;">Reference:</strong></td>
		</tr>
		<tr>
		<td style="padding:5px;vertical-align: text-top; font-family: courier;">
		<strong>'.$data['invoice_details'][0]['customer_name'].'</strong>
		<div class="left_addrs" style="font-size: 11px;color: #484545; line-height: 17px; font-family: courier;">'.$data['invoice_details'][0]['country'].'<br/>
		</div>
		</td>
		<td>Refxxxx</td>
		</tr>
		</table>
		</td>
		</tr>
		<tr>
		<td colspan="2"></td>
		</tr>
		<tr>
		<td colspan="2">
		<table cellspacing="0" cellpadding="10" border="0">
		<thead>
		<tr style="background-color: #e4e1e1;">
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="7%;">#</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="46%">Item Description</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="9%">Qty</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="8%">Unit</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="11%">Price</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="8%">Unit</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="11%">Total</td>
		</tr>
		</thead>
		<tbody>'.$table_str.'</tbody>
		</table>
		</td>
		</tr>
		<tr>
		<td>
		<strong>Total in words:</strong><br/><span style="font-family: courier; font-size: 11px;">'.$grand_total_words.'</span><br/>
		<hr/>
		<table cellspacing="0" cellpadding="3" border="0">
			<tr>
				<td><strong>Additional Notes</strong></td>
			</tr>
			<tr>
				<td style="font-family: courier; font-size: 11px;">
					We reserve the right to correct the pricing offered due to any typographical errors.<br/>
					This offer is not valid for end users from Iran, Iraq, North Korea, Cuba, Sudan & Syria.
				</td>
			</tr>
		</table>
		</td>
		<td>'.$total_table.'</td>
		</tr>
		<tr>
		<td colspan="2"></td>
		</tr>
		<tr>
		<td colspan="2"></td>
		</tr>
		<tr>
		<td></td>
		<td align="center">
		Thank you for your business<br/>
		For Om Tubes & Fittings Industries<br/><br/>
		<img src="/assets/media/stamp.png" /><br/><br/>
		<table>
		<tr>
			<td width="30%" rowspan="3"></td>
		 	<td align="left" width="70%">Name : <span style="font-family: courier;">ABC XYZ</span></td>
		</tr>
		<tr>
			<td align="left">Email : <span style="font-family: courier;">abc@xyz.com</span></td>
		</tr>
		<tr>
			<td align="left">Mobile : <span style="font-family: courier;">+91 22 2222 2222</span></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>';

		// output the HTML content
		$pdf->writeHTML($html, true, false, true, false, '');

		//Close and output PDF document
		$pdf->Output('example_061.pdf', 'I');
	}

	function deleteInvoice(){
		$invoice_id = $this->input->post('invoice_id');
		$this->invoice_model->deleteInvoice('invoice_dtl', array('invoice_mst_id' => $invoice_id));
		$this->invoice_model->deleteInvoice('invoice_mst', array('invoice_mst_id' => $invoice_id));
	}

	function numberTowords($num, $dec_text)
	{

		$ones = array(
		'' =>"ZERO",
		0 =>"ZERO",
		1 => "ONE",
		2 => "TWO",
		3 => "THREE",
		4 => "FOUR",
		5 => "FIVE",
		6 => "SIX",
		7 => "SEVEN",
		8 => "EIGHT",
		9 => "NINE",
		10 => "TEN",
		11 => "ELEVEN",
		12 => "TWELVE",
		13 => "THIRTEEN",
		14 => "FOURTEEN",
		15 => "FIFTEEN",
		16 => "SIXTEEN",
		17 => "SEVENTEEN",
		18 => "EIGHTEEN",
		19 => "NINETEEN",
		"014" => "FOURTEEN"
		);
		$tens = array( 
		0 => "ZERO",
		1 => "TEN",
		2 => "TWENTY",
		3 => "THIRTY", 
		4 => "FORTY", 
		5 => "FIFTY", 
		6 => "SIXTY", 
		7 => "SEVENTY", 
		8 => "EIGHTY", 
		9 => "NINETY" 
		); 
		$hundreds = array( 
		"HUNDRED", 
		"THOUSAND", 
		"MILLION", 
		"BILLION", 
		"TRILLION", 
		"QUARDRILLION" 
		); /*limit t quadrillion */
		$num = number_format($num,2,".",","); 
		$num_arr = explode(".",$num); 
		$wholenum = $num_arr[0]; 
		$decnum = $num_arr[1]; 
		$whole_arr = array_reverse(explode(",",$wholenum)); 
		krsort($whole_arr,1); 
		$rettxt = ""; 
		foreach($whole_arr as $key => $i){
			
		while(substr($i,0,1)=="0")
				$i=substr($i,1,5);
		if($i < 20){ 
		/* echo "getting:".$i; */
		$rettxt .= $ones[$i]; 
		}elseif($i < 100){ 
		if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)]; 
		if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)]; 
		}else{ 
		if(substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
		if(substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)]; 
		if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)]; 
		} 
		if($key > 0){ 
		$rettxt .= " ".$hundreds[$key]." "; 
		}
		} 
		if($decnum > 0){
			$rettxt .= " and ";
			if($decnum < 20){
				if(!empty($ones[$decnum])) {
					$rettxt .= $ones[$decnum];
				}
			}elseif($decnum < 100){
				$rettxt .= $tens[substr($decnum,0,1)];
				$rettxt .= " ".$ones[substr($decnum,1,1)];
			}

			$rettxt .= " ".$dec_text;
		}
		return $rettxt;
	}

	
	// public function numberTowords($num){
		
	// 	// echo "<pre>";print_r($num);echo"</pre><hr>";exit;
	// 	$f = new NumberFormatter("en-ind", NumberFormatter::DIGIT_SYMBOL);
	// 	$formated_number = $f->format($num);
		
	// 	// echo  $f->format($num),"<hr>";die('debug');
	// 	return ucwords(strtolower($formated_number));
	// }


	public function invoice_listing_new() {	
		$this->session->set_userdata(array('invoice_type'=> 'om'));
		// echo "<pre>";print_r($this->session->userdata());echo"</pre><hr>";die('debug');

		$where_array = array(
								'invoice_mst.is_deleted' => NULL,
								'invoice_mst.invoice_date >=' => '"'.date('Y').'-01-01 00:00:00"',
								'invoice_mst.invoice_date <=' => '"'.((int)(date('Y')) +1).'-12-31 23:59:59"',
								'invoice_mst.type IN'=> '("'.$this->session->userdata('invoice_type').'")',
		);
		if($this->session->userdata('role') == 5){

			$where_array['invoice_mst.assigned_to IN'] = "('".implode("', '", $this->session->userdata('invoice_access')['sales_user_id'])."')";
		}

		$data = $this->create_invoice_data($where_array, array('column_name'=>'invoice_mst.invoice_date','column_value'=>'desc'), 10, 0);
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Invoice List'));
		$this->load->view('sidebar', array('title' => 'Invoice List'));
		$this->load->view('invoices/list', $data);
		$this->load->view('footer');
	}	

	public function add_invoice() {

		$data = array();
		$data['invoice_details'] = array('invoice_mst_id'=>'','invoice_no'=>'','invoice_date'=>'','company_id'=>'','member_id'=>'','remarks'=>'','mode'=>'', 'currency_id'=>'', 'type'=>'', 'invoice_status'=>'', 'assigned_to'=>'');

		$invoice_id = $this->uri->segment('3',0);
		// echo "<pre>";print_r($invoice_id);echo"</pre><hr>";exit;
		$this->session->unset_userdata('invoice_order_details_'.$invoice_id);
		if(!empty($invoice_id)) {
			
			$data['invoice_details'] = $this->invoice_model->get_data('*', array('is_deleted'=>NULL,'invoice_mst_id'=> $invoice_id), 'invoice_mst', 'row_array');
			
			$data['invoice_product_details'] = $this->invoice_model->get_data('*', array('status'=>'Active','invoice_mst_id'=> $invoice_id), 'invoice_dtl', 'result_array');

			$data['invoice_charge_details'] = $this->invoice_model->get_data('*', array('status'=>'Active','invoice_id'=> $invoice_id), 'invoice_order_charges_information', 'result_array');

			$data['member_details'] =$this->invoice_model->get_data('comp_dtl_id, member_name',array('status'=>'Active','comp_mst_id'=>$data['invoice_details']['company_id']),'customer_dtl');
			
			$this->generate_session($data['invoice_details'], $data['invoice_product_details'], $data['invoice_charge_details']);
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		}

		$data['customer'] = $this->invoice_model->get_data('id, name, country_id', array('status' => 'Active'), 'customer_mst');

		$data['country'] = array_column($this->invoice_model->get_data('id, name', array('status' => 'Active'), 'country_mst'), 'name', 'id');

		$data['region'] = $this->invoice_model->get_data('id, name', array('status' => 'Active'), 'region_mst');

		$data['transport_mode'] = $this->invoice_model->get_data('*', array('status'=> 'Active'), 'transport_mode');

		$data['currency_list'] = $this->invoice_model->get_data('currency_id, currency', array('status'=> 'Active'), 'currency');

		$data['sales_person_list'] = $this->common_model->get_dynamic_data_sales_db_null_false('user_id, name',"status=1 AND role IN (5, 16)",'users');

		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Add Invoice'));
		$this->load->view('sidebar', array('title' => 'Add Invoice'));
		$this->load->view('invoices/add_or_update_invoice', $data);
		$this->load->view('footer');
	}

	public function invoice_export_list() {

		$data = array();
		$data =$this->create_export_invoice_data(array('invoice_mst.is_deleted is' => 'NULL', 'invoice_mst.invoice_date >=' => '"2021-04-01"', 'invoice_mst.invoice_date <=' => '"2022-03-31"', 'invoice_mst.invoice_no NOT LIKE' => '"%INSTINOX"'));
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Export Invoice List'));
		$this->load->view('sidebar', array('title' => 'Export Invoice List'));
		$this->load->view('invoices/invoice_export_list', $data);
		$this->load->view('footer');
	}

	public function invoice_export_update(){

 		$data = array();
    	$invoice_mst_id= $this->uri->segment(3, 0);
    	if(!empty($invoice_mst_id)) {

    		$data['export_invoice_details'] = $this->invoice_model->get_invoice_export_update_data(array('invoice_mst_id' => $invoice_mst_id));
    		if(empty($data['export_invoice_details'])) {

	    		redirect('invoices/invoice_export_list');
    		}
    		$data['export_invoice_details']['part_payment_details'] = $this->create_static_response_for_json_value($data['export_invoice_details'], 'part_payment_details');
    		$data['export_invoice_details']['advisery_no'] = $this->create_static_response_for_json_value($data['export_invoice_details'], 'advisery_no');
    		$data['export_invoice_details']['advisery_amount'] = $this->create_static_response_for_json_value($data['export_invoice_details'], 'advisery_amount');
    		$data['export_invoice_details']['utilised_amount'] = $this->create_static_response_for_json_value($data['export_invoice_details'], 'utilised_amount');
    		$data['export_invoice_details']['shipping_bill_details'] = $this->create_static_response_for_json_value($data['export_invoice_details'], 'shipping_bill_details');
    		$data['export_invoice_details']['egm'] = $this->create_static_response_for_json_value($data['export_invoice_details'], 'egm');
    		$data['currency_list'] = $this->invoice_model->get_data('*',array('status'=>'Active'),'currency');
			$data['payment_terms_list'] = $this->invoice_model->get_data('*',array('status'=>'Active'),'payment_terms');
    		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		 	$this->load->view('header', array('title' => 'Update Invoice'));
			$this->load->view('sidebar', array('title' => 'Update Invoice'));
			$this->load->view('invoices/invoice_export_update',$data);
			$this->load->view('footer');	
	    }else {
	    	redirect('invoices/invoice_export_list');
	    }
	}
	
 	public function ajax_function() {

		if($this->input->is_ajax_request()){
			$post_details = $this->input->post();
			$call_type = $this->input->post('call_type');
			$response['status'] = 'successful';	
			switch ($call_type) {

				case 'add_new_invoice':
					// echo "<pre>";print_r(array_column($this->input->post('invoice_form_data'), 'value', 'name'));echo"</pre><hr>";die;
					$form_data = array_column($this->input->post('invoice_form_data'), 'value', 'name');
					$invoice_mst_arr = array(
						'invoice_no' => $form_data['invoice_no'],
						'invoice_date' => date('Y-m-d', strtotime($form_data['invoice_date'])),
						'company_id' => $form_data['company'],
						'member_id' => $form_data['attention'],
						'remarks' => $form_data['remarks'],
						'freight_charge' => $form_data['freight_charges'],
						'other_charge' => $form_data['other_charges'],
						'discount' => $form_data['discount'],
						'mode' => $form_data['mode'],
					);

					if(!empty($form_data['invoice_id'])){
						$invoice_mst_arr['modified_on'] = date('Y-m-d H:i:s');
						$invoice_mst_arr['modified_by'] = $this->session->userdata('user_id');
						$invoice_id = $form_data['invoice_id'];
						$this->invoice_model->updateInvoice('invoice_mst', $invoice_mst_arr, array('invoice_mst_id' => $invoice_id));
						$this->invoice_model->deleteInvoice('invoice_dtl', array('invoice_mst_id' => $invoice_id));
						$response['message'] = "Invoice updated successfully";

					}else{
						$invoice_mst_arr['entered_on'] = date('Y-m-d H:i:s');
						$invoice_mst_arr['entered_by'] = $this->session->userdata('user_id');
						$invoice_id = $this->invoice_model->insertInvoice('invoice_mst', $invoice_mst_arr);
						$response['message'] = "Invoice created successfully";
					}
					$invoice_dtl_arr = array();
					$net_total = 0;
					foreach ($this->input->post('invoice_form_data') as $key => $value) {
						if($value['name'] == 'description[]') {
							$invoice_dtl_arr[] = array(
								'invoice_mst_id' => $invoice_id,
								'description' => $value['value'],
								'quantity' => $this->input->post('invoice_form_data')[$key+1]['value'],
								'rate' => $this->input->post('invoice_form_data')[$key+2]['value'],
								'product_id' => $this->input->post('invoice_form_data')[$key+3]['value'],
								'material_id' => $this->input->post('invoice_form_data')[$key+4]['value'],
								'price' => $this->input->post('invoice_form_data')[$key+2]['value'] * $this->input->post('invoice_form_data')[$key+2]['value']
							);
							$net_total += $this->input->post('invoice_form_data')[$key+1]['value'] * $this->input->post('invoice_form_data')[$key+2]['value'];
						}
					}
					// echo "<pre>";print_r($invoice_dtl_arr);echo"</pre><hr>";exit;
					// die('done one process');
					if(!empty($invoice_dtl_arr)) {

						$this->invoice_model->batch_insert_data('invoice_dtl', $invoice_dtl_arr);
					}
					$update_arr = array(
						'net_total' => $net_total,
						'grand_total' => $net_total+$form_data['freight_charges']+$form_data['other_charges']-$form_data['discount'],
					);
					$this->invoice_model->updateInvoice('invoice_mst', $update_arr, array('invoice_mst_id' => $invoice_id));
				break;

				case 'search_filter':
				case 'paggination_filter':
				case 'sort_filter':
					
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					$response_html = $this->all_filter_data(
											$this->input->post('search_form_data'),
											array(
												'column_name'=> $this->input->post('invoice_sort_name'), 
												'column_value'=>$this->input->post('invoice_sort_value')
											),
											$this->input->post('limit'),
											$this->input->post('offset')
										);
					// echo "<pre>";print_r($response_html);echo"</pre><hr>";exit;
					$response['invoice_body'] = $response_html['invoice_body']; 
					$response['invoice_search_filter'] = $response_html['invoice_search_filter'];
					$response['invoice_sorting'] = $response_html['invoice_sorting'];
					$response['invoice_paggination'] = $response_html['invoice_paggination'];
					$response['invoice_sorting_column_details']['column_name'] = $this->input->post('invoice_sort_name');
					$response['invoice_sorting_column_details']['column_value'] = $this->input->post('invoice_sort_value');
				break;
	
				case 'invoice_total_highchart':
					
					$where_array = array(
										'is_deleted' => NULL,
										'currency_id !='=> 0
									);
					if(!empty($this->session->userdata('invoice_type'))){

						$where_array['type ='] = '"'.$this->session->userdata('invoice_type').'"';
					}
					$all_invoice_details = $this->invoice_model->get_all_invoice_details($where_array);
					// echo "<pre>";print_r($all_invoice_details);echo"</pre><hr>";
					$unique_year = array_unique(array_column($all_invoice_details, 'year'));
					$data = array();
					foreach ($unique_year as $year_name) {
						
						for ($i=1; $i < 13; $i++) { 
							
							$data[$year_name][$i] = 0;
						}
					}
					// echo "<pre>";print_r($unique_year);echo"</pre><hr>";
					$currency_details =  array_column($this->common_model->get_dynamic_data_sales_db('currency_id, currency_rate', array('status'=>'Active'), 'currency'), 'currency_rate', 'currency_id');
					$currency_value = $currency_details[1];
					foreach ($all_invoice_details as $single_invoice_details) {
						// Apr to March 
						// $month = ($single_invoice_details['month'] >= 4) ? $single_invoice_details['month'] - 3 : $single_invoice_details['month'] + 9;
						// $data[$single_invoice_details['year']][$month] += round((((int)$single_invoice_details['grand_total']) * ($currency_details[$single_invoice_details['currency_id']] / $currency_value)));

						// Jan to Dec 
						$month = $single_invoice_details['month'];
						if (!isset($data[$single_invoice_details['year']][$month])) {
							$data[$single_invoice_details['year']][$month] = 0;
						}
						$data[$single_invoice_details['year']][$month] += round(
							((int)$single_invoice_details['grand_total']) * 
							($currency_details[$single_invoice_details['currency_id']] / $currency_value)
						);
					}
					// echo "<pre>";print_r($data);echo"</pre><hr>";
					$response['invoice_total_highchart_data'] = array();
					foreach ($data as $year_name => $year_invoice) {
						ksort($year_invoice);
						// echo "<pre>";var_dump($year_invoice);echo"</pre><hr>";exit;
						$highchart_data = array();
						foreach ($year_invoice as $single_total) {
							
							$highchart_data[] = (int)$single_total;
						}
						$response['invoice_total_highchart_data'][] = array(
																	'name' => $year_name,
																	'data' => $highchart_data
																);
					}
					// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
				break;

				case 'add_invoice_order':

					$counter_number =  $this->input->post('count_no');
					if(!empty($counter_number)) {

						$response['invoice_body'] = $this->load->view(
																	'invoices/add_item_body',
																	$this->prepare_invoice_order_body_data($counter_number),
																	true
																);
					}
				break;

				case 'save_invoice':
					
					// echo "<pre>";print_r($this->input->post('invoice_order_details'));echo"</pre><hr>";
					// echo "<pre>";print_r($this->input->post('invoice_details'));echo"</pre><hr>";die;
					$invoice_form_data = array();
					$invoice_form_data = array_column($this->input->post('invoice_details'),'value','name');
					$invoice_form_data['freight_charge'] = 0;
					$invoice_form_data['other_charge'] = 0;
					$invoice_form_data['discount'] = 0;
					$invoice_form_data['net_total'] = 0;
					$invoice_form_data['grand_total'] = 0;
					$invoice_form_data['entered_on'] = date('Y-m-d H:i:s');
					$invoice_form_data['entered_by'] = $this->session->userdata('user_id');
					$invoice_form_data['modified_by'] = $this->session->userdata('user_id');
					$invoice_charges_data = array();
					$id = $this->uri->segment('3',0);
					if(!empty($this->session->userdata('invoice_order_details_'.$id))) {

						$invoice_form_data['net_total'] = $this->session->userdata('invoice_order_details_'.$id)['net_total'];
						$invoice_form_data['grand_total'] = $this->session->userdata('invoice_order_details_'.$id)['gross_total'];
						if(!empty($this->session->userdata('invoice_order_details_'.$id)['different_charges'])){

							foreach ($this->session->userdata('invoice_order_details_'.$id)['different_charges'] as $charge_key => $single_charge_details) {
								
								$invoice_charges_data[] = array(
																'charge_name' => $single_charge_details['charges_name'], 
																'charge_value' => $single_charge_details['charge_value'], 
																'charge_deducted_or_added' => $single_charge_details['add_minus_charges_value'], 
																'gross_total_after_charge' => $single_charge_details['gross_total'], 
														);
								$invoice_form_data[$single_charge_details['charges_name']] += $single_charge_details['charge_value'];
							}
						}
					}

						// echo "<pre>";print_r($invoice_form_data);echo"</pre><hr>";die;
					$invoice_id = $invoice_form_data['invoice_mst_id'];
					unset($invoice_form_data['invoice_mst_id']);
					if(empty($invoice_id)) {

						// echo "<pre>";print_r($invoice_form_data);echo"</pre><hr>";die;
						$invoice_id = $this->common_model->insert_data_sales_db('invoice_mst', $invoice_form_data, 'single_insert');
					} else {

						$this->common_model->update_data_sales_db('invoice_mst', $invoice_form_data, array('invoice_mst_id'=>$invoice_id), 'single_update');
					}
					// die('debug');
					$this->common_model->update_data_sales_db('invoice_order_charges_information', array('status'=>'Inactive'),array('invoice_id'=>$invoice_id));
					if(!empty($invoice_charges_data)) {
						foreach ($invoice_charges_data as $invoice_charge_key => $invoice_charge_detail) {
							
							$invoice_charges_data[$invoice_charge_key]['invoice_id'] = $invoice_id;
						}
						$this->common_model->insert_data_sales_db('invoice_order_charges_information', $invoice_charges_data, 'batch');
					}
					// echo "<pre>";print_r($invoice_charges_data);echo"</pre><hr>";
					if(!empty($this->input->post('invoice_order_details'))) {

						$invoice_order_form_data = array_column($this->input->post('invoice_order_details'),'value','name');
						$form_data = $insert_array = $update_array =  array();
						foreach ($invoice_order_form_data as $key => $value) {
							$explode_key = explode('_', $key);
							$insert_array_key_name = $explode_key[(count($explode_key)-1)]-1;
							unset($explode_key[count($explode_key)-1]);
							$insert_array_column_name = implode('_',$explode_key);
							$form_data[$insert_array_key_name][$insert_array_column_name] = $value; 
							$form_data[$insert_array_key_name]['invoice_mst_id'] = $invoice_id; 

						}
						if(!empty($form_data)) {
							foreach ($form_data as $form_data_value) {
								
								if($form_data_value['invoice_dtl_id'] == ''){

									$insert_array[] = $form_data_value;
								} else {

									$update_array[] = $form_data_value;
								}
							}
							if(!empty($insert_array)) {

								// echo "<pre>";print_r($insert_array);echo"</pre><hr>";
								$this->common_model->insert_data_sales_db('invoice_dtl', $insert_array, 'batch');
							}
							if(!empty($update_array)) {

								// echo "<pre>";print_r($update_array);echo"</pre><hr>";
								$this->common_model->update_data_sales_db('invoice_dtl', $update_array, 'invoice_dtl_id', 'batch');
							}
						}
					}

					$response['invoice_id'] = $invoice_id;
					// echo "<pre>";print_r($invoice_order_form_data);echo"</pre><hr>";exit;
				break;

				case 'delete_invoice_order':
					
					if(!empty($this->input->post('invoice_dtl_id'))) {
						$get_product_details =  $this->invoice_model->get_data('*', array('status'=>'Active', 'invoice_dtl_id'=>$this->input->post('invoice_dtl_id')), 'invoice_dtl', 'row_array');
						if(!empty($get_product_details)) {
							$invoice_mst_details =  $this->invoice_model->get_data('*', array('is_deleted'=>NULL, 'invoice_mst_id'=>$get_product_details['invoice_mst_id']), 'invoice_mst', 'row_array');
							$invoice_order_charges_details =  $this->invoice_model->get_data('*', array('status'=>'Active', 'invoice_id'=>$get_product_details['invoice_mst_id']), 'invoice_order_charges_information');
							$this->invoice_model->updateInvoice('invoice_mst', array('net_total'=> ($invoice_mst_details['net_total'] - $get_product_details['price']), 'grand_total'=> ($invoice_mst_details['grand_total'] - $get_product_details['price'])), array('invoice_mst_id'=> $get_product_details['invoice_mst_id']));
							foreach($invoice_order_charges_details as $order_details) {

								$this->invoice_model->updateInvoice('invoice_order_charges_information', array('gross_total_after_charge'=> ($order_details['gross_total_after_charge'] - $get_product_details['price'])), array('id'=> $order_details['id']));
							}
							$this->invoice_model->updateInvoice('invoice_dtl', array('status'=> 'Inactive'), array('invoice_dtl_id'=> $this->input->post('invoice_dtl_id')));
						}
					}
				break;

				case 'get_invoice_order_body':
					
					$invoice_id = $this->input->post('invoice_id');
					$response['invoice_order_body'] = '';
					$response['count_no'] = 1;
					if($invoice_id != '') {
						$product_details = $this->invoice_model->get_data('*', array('status' => 'Active', 'invoice_mst_id' => $invoice_id), 'invoice_dtl');
						if(!empty($product_details)) {
							foreach ($product_details as $product_key => $product_value) { 

								$response['invoice_order_body'] .= $this->load->view(
																					'invoices/add_item_body',
																					$this->prepare_invoice_order_body_data($product_key+1, $product_value),
																					true
																				);
							}
							$response['count_no'] = count($product_details)+1;
						}
					}
				break;

				case 'get_invoice_net_total':
	
					$response['invoice_net_total'] = $this->load->view('invoices/invoice_net_gross', array('id' => $this->input->post('invoice_id')), true);
				break;

				case 'set_invoice_order_details':
					
					$this->set_invoice_order_session_value($this->input->post('net_total'), $this->input->post('count_number'));
				break;	

				case 'add_gst_or_discount':
					
					$response['message'] = 'net_total_not_found';
					$id = $this->input->post('invoice_id');
					if(!empty($this->session->userdata('invoice_order_details_'.$id)['net_total'])){

						$response['discount_history'] = $this->load->view('invoices/invoice_charges_history', array('id' =>$id), true);
						$response['message'] = 'net_total_found';
					}
				break;
					
				case 'add_charges_in_invoice_order':
					
					$charge_name_array = array('+'=>'Added', '-'=>'Deducted');
					$form_data = array_column($this->input->post('invoice_charges_details'), 'value', 'name');
					$form_data['add_minus_charges_value'] = $charge_name_array[$form_data['add_minus_charges']]; 
					$find_percentage_in_string = strpos($form_data['charge_value'], '%');
					$id = $this->uri->segment('3',0);
					$order_details = $this->session->userdata('invoice_order_details_'.$id);
					$gross_total = $order_details['gross_total'];
					if($find_percentage_in_string) {
						$percentage_value = $gross_total*(((int) $form_data['charge_value'] ) / 100 );
						if($form_data['add_minus_charges'] == '+') {

							$form_data['gross_total'] = $gross_total + $percentage_value; 
						} else if ($form_data['add_minus_charges'] == '-'){

							$form_data['gross_total'] = $gross_total - $percentage_value; 
						}
					} else {

						if($form_data['add_minus_charges'] == '+') {

							$form_data['gross_total'] = $gross_total + $form_data['charge_value']; 
						} else if ($form_data['add_minus_charges'] == '-'){

							$form_data['gross_total'] = $gross_total - $form_data['charge_value']; 
						}
					}
					$order_details['different_charges'][count($order_details['different_charges'])] = $form_data;
					$order_details['gross_total'] = $form_data['gross_total'];
					$this->session->set_userdata('invoice_order_details_'.$id, $order_details);
				break;	

				case 'get_client_name':
					
					$client_id = $this->input->post('client_id');
					if(!empty($client_id)) {
						$response['member_name'] = $this->load->view(
																	'common/option_tag',
																	array(
																		'option_value' => 'member_id',
																		'option_name' => 'name',
																		'option_array' => $this->invoice_model->get_data(
																											'member_id, name',
																											array(
																												'status'=>'Y',
																												'client_id'=>$client_id
																											),
																											'members'
																										)
																	),
																	true
																	);
					}
				break;

				case 'delete_charge_details':
					
					$charge_key = $this->input->post('charge_key');
					$id = $this->uri->segment('3',0);
					$session_data = $this->session->userdata('invoice_order_details_'.$id);
					if(array_key_exists($charge_key, $session_data['different_charges'])) {

 						// echo "<pre>";print_r($session_data);echo"</pre><hr>";
 						$charge_data_of_delete = $session_data['different_charges'][$charge_key];
 						$gross_total = 0;
						$current_charge_value = 0;
						$gross_total_changed_flag = false;
 						foreach ($session_data['different_charges'] as $different_charges_key => $different_charges_value) {
 							
 							if($different_charges_key == $charge_key) {
								// echo $gross_total;die;
								if($charge_data_of_delete['add_minus_charges_value'] == 'Added') {

									$current_charge_value = ($charge_data_of_delete['gross_total']- ($gross_total + $session_data['net_total']));
									$session_data['gross_total'] = $session_data['gross_total'] - $current_charge_value;
								}else if($charge_data_of_delete['add_minus_charges_value'] == 'Deducted') {
									
									$current_charge_value = (($gross_total + $session_data['net_total']) - $charge_data_of_delete['gross_total']);
									$session_data['gross_total'] = $session_data['gross_total'] + $current_charge_value;
								}
								// echo $current_charge_value;die;
 							}else{
 								if($current_charge_value == 0) {

									if($different_charges_value['add_minus_charges_value'] == 'Added') {

		 								$gross_total += ($different_charges_value['gross_total']- ($gross_total + $session_data['net_total']));
									}else if($different_charges_value['add_minus_charges_value'] == 'Deducted') {

										// echo $gross_total,"<hr>";
		 								$gross_total -= (($gross_total + $session_data['net_total']) -$different_charges_value['gross_total']);
										// echo $gross_total;die;
									}
 								} else {

 									if($charge_data_of_delete['add_minus_charges_value'] == 'Added') {

	 									$session_data['different_charges'][$different_charges_key]['gross_total'] -= $current_charge_value;		
									}else if($charge_data_of_delete['add_minus_charges_value'] == 'Deducted') {

	 									$session_data['different_charges'][$different_charges_key]['gross_total'] += $current_charge_value;	
									}
 								}
 							}
 						}
						// $get_charge_data = $session_data['different_charges'][$charge_key];
 						// echo "<pre>";print_r($session_data);echo"</pre><hr>";exit;
						unset($session_data['different_charges'][$charge_key]);
						$this->session->set_userdata('invoice_order_details_'.$id,$session_data);
					}
				break;

				case 'delete_invoice':

					if(!empty($this->input->post('invoice_mst_id'))) {
						// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
						$this->invoice_model->updateInvoice('invoice_mst', array('is_deleted'=> 'Inactive'), array('invoice_mst_id'=> $this->input->post('invoice_mst_id')));
						$this->invoice_model->updateInvoice('invoice_dtl', array('status'=> 'Inactive'), array('invoice_mst_id'=> $this->input->post('invoice_mst_id')));
						$this->invoice_model->updateInvoice('invoice_order_charges_information', array('status'=> 'Inactive'), array('invoice_id'=> $this->input->post('invoice_mst_id')));
					}
				break;
					
				case 'get_member_details':
					if(!empty($this->input->post('company_id'))) {

						$data['member_list'] = $this->invoice_model->get_data('member_id, name', array('status'=>'Y', 'client_id'=>$this->input->post('company_id')), 'members');
						$response['member_list_html'] = $this->load->view('company_attention_list', $data, true);

						$company_details = $this->invoice_model->get_data('name', array('status' => 'Active', 'id' => $this->input->post('company_id')), 'customer_mst', 'row_array');
						$response['company_name_html'] = $company_details['name'];
						$response['company_id_html'] = $this->input->post('company_id');
					}
				break;

				case 'add_attention_form':

					// echo "<pre>";print_r($this->input->post('form_data'));echo"</pre><hr>";exit;
					$response['icon'] = "warning";
					if(empty($this->input->post('form_data')[1]['value'])) {

						$response['message'] = "Attention Name is empty!!!";
					} elseif(empty($this->input->post('form_data')[2]['value'])) {

						$response['message'] = "Email is empty!!!";
					} else {
						$company_details = $this->invoice_model->get_data('name', array('status' => 'Y', 'client_id' => $this->input->post('form_data')[0]['value'], 'name' => trim($this->input->post('form_data')[1]['value'])), 'members', 'row_array');
						$response['message'] = "Attention Name already exists!!!";	
						$response['icon'] = "error";
						if(empty($company_details)) {
							$insert_array = array();
							foreach ($this->input->post('form_data') as $form_details) {
								$insert_array[$form_details['name']] = trim($form_details['value']);
							}
							$id = $this->invoice_model->insertInvoice('members', $insert_array);
							$response['message'] = "Attention Name is added!!!";	
							$response['icon'] = "success";
						}
					}
				break;

				case 'invoice_export_update_data':

					$update_data = array();
					$part_payment_details = array();
					$advisery_no = array();
					$advisery_amount = array();
					$shipping_bill_details = array();
					$egm_details = array();
				    // echo "<pre>";print_r($this->input->post('update_form'));echo"</pre><hr>";exit;
				    foreach ($this->input->post('update_form') as $form_key => $form_details) {
			    		if(!in_array($form_details['name'], array('part_1', 'part_2', 'part_3', 'part_4', 'advisery_no[0]', 'advisery_amount[0]', 'utilised_amount[0]', 'advisery_no[1]', 'advisery_amount[1]', 'utilised_amount[1]', 'advisery_no[2]', 'advisery_amount[2]', 'utilised_amount[2]', 'advisery_no[3]', 'advisery_amount[3]', 'utilised_amount[3]', 'shipping_bill_no', 'shipping_bill_date', 'shipping_bill_leo_date', 'egm_no', 'egm_date', 'bank_sub_date'))) {

			    			$update_data[$form_details['name']] = $form_details['value'];
			    		} else {
							if(in_array($form_details['name'], array('part_1', 'part_2', 'part_3', 'part_4'))) {
								
								$part_payment_details[$form_details['name']] = $form_details['value'];
							} else if(in_array($form_details['name'], array('advisery_no[0]', 'advisery_no[1]', 'advisery_no[2]', 'advisery_no[3]'))) {

								$advisery_no[] = $form_details['value'];
							}  else if(in_array($form_details['name'], array('advisery_amount[0]', 'advisery_amount[1]', 'advisery_amount[2]', 'advisery_amount[3]'))) {

								$advisery_amount[] = $form_details['value'];
							}  else if(in_array($form_details['name'], array('utilised_amount[0]', 'utilised_amount[1]', 'utilised_amount[2]', 'utilised_amount[3]'))) {

								$utilised_amount[] = $form_details['value'];
							} else if(in_array($form_details['name'], array('shipping_bill_no', 'shipping_bill_date', 'shipping_bill_leo_date'))) {

								$shipping_bill_details[$form_details['name']] = $form_details['value'];
							} else if(in_array($form_details['name'], array('egm_no', 'egm_date'))) {

								$egm_details[$form_details['name']] = $form_details['value'];
							} else if(in_array($form_details['name'], array('bank_sub_date'))) {
								$update_data[$form_details['name']] = null;
								if(!empty($form_details['value'])){
									$update_data[$form_details['name']] = date('Y-m-d',strtotime($form_details['value']));
								}
							}			
			    		}
				    }
	    			$update_data['part_payment_details'] = json_encode($part_payment_details);
	    			$update_data['advisery_no'] = json_encode($advisery_no);
	    			$update_data['advisery_amount'] = json_encode($advisery_amount);
	    			$update_data['utilised_amount'] = json_encode($utilised_amount);
	    			$update_data['shipping_bill_details'] = json_encode($shipping_bill_details);
	    			$update_data['egm'] = json_encode($egm_details);

				    // echo "<pre>";print_r($update_data);echo"</pre><hr>";die('debug');
	    			$check_record_exist = $this->invoice_model->get_data('*',array('status' => 'Active', 'invoice_id'=>$update_data['invoice_id']),'invoice_export_info', 'row_array');
	    			if(empty($check_record_exist)) {

	    				$this->common_model->insert_data_sales_db('invoice_export_info', $update_data);
	    			} else {

	    				$this->common_model->update_data_sales_db('invoice_export_info', $update_data, array('id'=>$check_record_exist['id']));
	    			}
				break;

				case'invoice_export_search_filter':

					$where_array = $this->invoice_export_search_filter_value($this->input->post('search_filter_form'));
                    $data =$this->create_export_invoice_data($where_array, $this->input->post('limit'), $this->input->post('offset'));
					// echo "<pre>";print_r($data);echo"</pre><hr>";die('debug');
					$response['search_filter_html'] = $this->load->view('invoices/invoice_export_search_filter', $data, true);
					$response['table_body_html'] = $this->load->view('invoices/invoice_export_body', $data, true);
					$response['paggination_html'] = $this->load->view('common/common_paggination', $data, true);
					// echo "<pre>";print_r($response);echo"</pre><hr>";die('debug');

				break;

				case'invoice_export_paggination_filter':

					$where_array = $this->invoice_export_search_filter_value($this->input->post('search_filter_form'));
                    $data =$this->create_export_invoice_data($where_array, $this->input->post('limit'), $this->input->post('offset'));
					$response['search_filter_html'] = $this->load->view('invoices/invoice_export_search_filter', $data, true);
					$response['table_body_html'] = $this->load->view('invoices/invoice_export_body', $data, true);
					$response['paggination_html'] = $this->load->view('common/common_paggination', $data, true);
					// echo "<pre>";print_r($response);echo"</pre><hr>";die('debug');

				break;

				case 'change_type':

					//  echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					$this->session->set_userdata(array('invoice_type'=>$this->input->post('type')));
					$where_array = array(
							'invoice_mst.is_deleted' => NULL,
							'invoice_mst.invoice_date >=' => '"'.date('Y').'-01-01 00:00:00"',
							'invoice_mst.invoice_date <=' => '"'.((int)(date('Y')) +1).'-12-31 23:59:59"'
						);
					if(!empty($this->session->userdata('invoice_type'))){

						$where_array['invoice_mst.type IN'] = '("'.$this->session->userdata('invoice_type').'")';
					}
					if($this->session->userdata('role') == 5){

						$where_array['invoice_mst.assigned_to IN'] = "('".implode("', '", $this->session->userdata('invoice_access')['sales_user_id'])."')";
					}
					$data = $this->create_invoice_data(
						$where_array,//where array
						array(
							'column_name'=> $this->input->post('invoice_sort_name'),
							'column_value'=>$this->input->post('invoice_sort_value')
						),//order array
						$this->input->post('limit'),
						$this->input->post('offset')
					);
					
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
					$response['table_body_html'] = $this->load->view('invoices/invoice_body', $data, true);
					$response['total_amount'] = $this->load->view('invoices/invoice_paggination', $data, true);
					$response['limit'] = $this->input->post('limit');
					$response['offset'] = $this->input->post('offset');
					$response['invoice_sorting_column_details']['column_name'] = $this->input->post('invoice_sort_name');
					$response['invoice_sorting_column_details']['column_value'] = $this->input->post('invoice_sort_value');
				break;
				
				case 'search_invoice_client_name':

					$search_client_name = $post_details['client_name'];
					$where_string = "status = 'Active' AND name LIKE '%".$search_client_name."%'";
					if($this->session->userdata('role') == 5){

						$where_string .= "AND assigned_to IN (".implode(', ', $this->session->userdata('lead_access')['sales_user_id']).")";
					}
					$client_details = $this->common_model->get_dynamic_data_sales_db_null_false('id, name', $where_string, 'customer_mst');
					$response['client_list_html'] = '<option value="">Select Name</option>';
					foreach($client_details as $single_details){

                        $response['client_list_html'] .= '<option value="'.$single_details['id'].'">'.$single_details['name'].'</option>';
                    }
				break;

				default:
					$response['status'] = 'failed';	
					$response['message'] = "call type not found";
				break;
			}
			echo json_encode($response);

		} else{
			die('access is not allowed to this function');
		}
	}


	private function change_key_as_per_value($data, $changing_key_column_name = 'value'){

		$return_data = array();
		foreach ($data as $single_data) {

			$return_data[$single_data[$changing_key_column_name]] = $single_data;
		}

		return $return_data;
	}

	//invoice listing code start
	private function all_filter_data($search_form_data, $order_by_array, $limit, $offset) {

		$where_array = array('invoice_mst.is_deleted' => NULL);
		if(!empty($this->session->userdata('invoice_type'))){
			
			$where_array['invoice_mst.type IN'] = '("'.$this->session->userdata('invoice_type').'")';
		}
		if($this->session->userdata('role') == 5){

			$where_array['invoice_mst.assigned_to IN'] = "('".implode("', '", $this->session->userdata('invoice_access')['sales_user_id'])."')";
		}
		$return_response = array();
		
		$country_id_in_array = $assigned_to_in_array = array();
		if(!empty($search_form_data)) {
			
			foreach ($search_form_data as $form_data_value) {
				
				if(!empty($form_data_value['value'])) {

					if($form_data_value['name'] == 'invoice_no') {

						$where_array['invoice_mst.invoice_no LIKE'] = '"%'.trim($form_data_value['value']).'%"';

					} else if($form_data_value['name'] == 'financial_year') {

						$invoice_date_explode = explode('-', $form_data_value['value']);
						if(empty($where_array['invoice_mst.invoice_date >='])){

							$where_array['invoice_mst.invoice_date >='] = '"'.$invoice_date_explode[0].'-04-01 00:00:00'.'"';
							$where_array['invoice_mst.invoice_date <='] = '"'.$invoice_date_explode[1].'-03-31 23:59:59'.'"';
						} else {
							// echo date('Y', strtotime($where_array['invoice_mst.invoice_date >='])),"<hr>",$invoice_date_explode[0],"<hr>";
							// echo "<pre>";print_r($where_array);echo"</pre><hr>";
							// echo "<pre>";print_r($invoice_date_explode);echo"</pre><hr>";die('debug');
							if(date('Y', strtotime(trim(str_replace('"', '', $where_array['invoice_mst.invoice_date >='])))) > $invoice_date_explode[0]) {

								$where_array['invoice_mst.invoice_date >='] = '"'.$invoice_date_explode[0].'-04-01 00:00:00'.'"';
							}
							if(date('Y', strtotime(trim(str_replace('"', '', $where_array['invoice_mst.invoice_date <='])))) < $invoice_date_explode[1]) {

								$where_array['invoice_mst.invoice_date <='] = '"'.$invoice_date_explode[1].'-03-31 23:59:59'.'"';
							}
						}
						
					} else if($form_data_value['name'] == 'invoice_date') {

						$explode_date = explode('-', $form_data_value['value']);
						if(count($explode_date) == 1) {

							$where_array['invoice_mst.invoice_date'] = '"'.date('Y-m-d', strtotime($explode_date[0])).'"';
						}else {
							if(date('Y-m-d', strtotime($explode_date[0])) == date('Y-m-d', strtotime($explode_date[1]))) {

								$where_array['invoice_mst.invoice_date'] = '"'.date('Y-m-d', strtotime($explode_date[0])).'"';
							} else {

								$where_array['invoice_mst.invoice_date >='] = '"'.date('Y-m-d', strtotime($explode_date[0])).'"';
								$where_array['invoice_mst.invoice_date <='] = '"'.date('Y-m-d', strtotime($explode_date[1])).'"';
							}
						}
					
					} else if($form_data_value['name'] == 'customer_name') {

						$where_array['customer_mst.name LIKE'] = '"%'.trim($form_data_value['value']).'%"';

					} else if($form_data_value['name'] == 'country_name') {

						$country_id_in_array[] = $form_data_value['value'];

					} else if($form_data_value['name'] == 'member_name') {

						$where_array['members.name LIKE'] = '"%'.trim($form_data_value['value']).'%"';

					} else if($form_data_value['name'] == 'type') {

						$where_array['invoice_mst.type IN'] = '"'.trim($form_data_value['value']).'"';

					} else if($form_data_value['name'] == 'invoice_status') {

						$where_array['invoice_mst.invoice_status'] = '"'.trim($form_data_value['value']).'"';

					} else if($form_data_value['name'] == 'assigned_to') {

						$assigned_to_in_array[] = $form_data_value['value'];

					} else if($form_data_value['name'] == 'client_id') {

						$where_array['invoice_mst.company_id'] = trim($form_data_value['value']);
					}
				}
				if(!empty($country_id_in_array)) {

					$where_array['country_mst.id IN'] = '("'.implode('", "', $country_id_in_array).'")';
				}
				if(!empty($assigned_to_in_array)) {

					$where_array['invoice_mst.assigned_to IN'] = '("'.implode('", "', $assigned_to_in_array).'")';
				}
			}
		}
		// echo "<pre>";print_r($where_array);echo"</pre><hr>";exit;
		$data = $this->create_invoice_data($where_array, $order_by_array, $limit, $offset);
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$return_response['invoice_body'] = $this->load->view('invoice_body', $data, true);
		$return_response['invoice_search_filter'] = $this->load->view('invoice_search_filter_form', $data, true);
		$return_response['invoice_sorting'] = $this->load->view('invoice_header', $data, true);
	  	// echo "<pre>";print_r($return_response['invoice_sorting']);echo"</pre><hr>";exit;
		$return_response['invoice_paggination'] = $this->load->view('invoice_paggination', $data, true);
		return $return_response;
	}

	private function create_invoice_data($where_array, $order_by, $limit, $offset) {

		// echo "<pre>";print_r($where_array);echo"</pre><hr>";exit;
		$data = $this->invoice_model->get_invoice_data($where_array, $order_by,$limit, $offset);
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;

		$data['user_list'] = array_column($this->common_model->get_dynamic_data_sales_db('user_id, name', array(), 'users'), 'name', 'user_id');
		$data['user_list']['']='';
		$data['user_list'][0]='';

		$data['currency_list'] = array_column($this->common_model->get_dynamic_data_sales_db('currency_id, decimal_number', array('status' => 'Active'), 'currency'), 'decimal_number', 'currency_id');
		$data['currency_list']['']='';
		$data['currency_list'][0]='';

		//search filter data
		$data['year_list'] = $this->invoice_model->get_financial_years();
		$data['type'] = $this->common_model->get_dynamic_data_sales_db('type',array('is_deleted' => NULL),'invoice_mst');
		$data['country_list'] = $this->common_model->get_dynamic_data_sales_db('name, id', array('status'=> "Active"), 'country_mst');
		$data['sales_person'] = $this->common_model->get_sales_person('name, user_id');

		$data['search_filter_form_data'] = $this->set_search_filter_value($where_array);
		//sorting
		$data['sorting'] = $this->set_sorting_value($order_by);

		//total
		$data['total_amount'] = $this->invoice_model->get_total($where_array);

		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$data['paggination_data']['limit'] = $limit;
		$data['paggination_data']['offset'] = $offset;
		return $data;
	}
	private function set_search_filter_value($where_array) {
		
		// echo "<pre>";print_r($where_array);echo"</pre><hr>";

		foreach ($where_array as $where_key => $where_value) {
			$where_array[$where_key] = trim(str_replace('"', '', $where_value));
		}
		$return_array['invoice_no'] = (!empty($where_array['invoice_mst.invoice_no LIKE'])) ? trim(str_replace('%', '', $where_array['invoice_mst.invoice_no LIKE'])) : ''; 
		$return_array['financial_year'] = array();
		if(!empty($where_array['invoice_mst.invoice_date >='])) {

			for (
				$i = date('Y', strtotime($where_array['invoice_mst.invoice_date >=']));
				$i < date('Y', strtotime($where_array['invoice_mst.invoice_date <=']));
				$i++
			) { 
				
				$return_array['financial_year'][] = $i.'-'.($i+1);
			}
		}
		$return_array['invoice_date'] = (!empty($where_array['invoice_mst.invoice_date'])) ? $where_array['invoice_mst.invoice_date']: ''; 
		$return_array['customer_name'] = (!empty($where_array['customer_mst.name LIKE'])) ? trim(str_replace('%', '', $where_array['customer_mst.name LIKE'])): ''; 
		$return_array['member_name'] = (!empty($where_array['customer_dtl.member_name LIKE'])) ? trim(str_replace('%', '', $where_array['customer_dtl.member_name LIKE'])): '';
		$return_array['country'] = array();
		if(!empty($where_array['country_mst.id IN'])) {

			$country_id_in = trim(str_replace('(', '', $where_array['country_mst.id IN']));
			$country_id_in = trim(str_replace(')', '', $country_id_in));
			$return_array['country'] = explode(', ', $country_id_in);
		
		}
		$return_array['type'] = array();
		if(!empty($where_array['invoice_mst.type IN'])) {

			$type_id_in = trim(str_replace('(', '', $where_array['invoice_mst.type IN']));
			$type_id_in = trim(str_replace(')', '', $type_id_in));
			$return_array['type'] = explode(', ', $type_id_in);
		
		}
		$return_array['invoice_status'] = (!empty($where_array['invoice_mst.invoice_status'])) ? $where_array['invoice_mst.invoice_status']: '';
		$return_array['sales_person'] = array();
		if(!empty($where_array['invoice_mst.assigned_to IN'])) {

			$assigned_to_in = trim(str_replace('(', '', $where_array['invoice_mst.assigned_to IN']));
			$assigned_to_in = trim(str_replace(')', '', $assigned_to_in));
			$return_array['sales_person'] = explode(', ', $assigned_to_in);
		}
		$return_array['client_id'] = array();
		if(!empty($where_array['customer_mst.name LIKE'])){

			$search_filter_where_string = "status = 'Active' AND name LIKE '%".$where_array['customer_mst.name LIKE']."%'";
			if($this->session->userdata('role') == 5){

				$search_filter_where_string .= "AND assigned_to IN (".implode(', ', $this->session->userdata('lead_access')['sales_user_id']).")";
			}
			$return_array['client_id'] = $this->change_key_as_per_value($this->common_model->get_dynamic_data_sales_db_null_false('id, name, "" as selected', $search_filter_where_string, 'customer_mst'), 'id');
		}
		if(!empty($where_array['invoice_mst.company_id'])){

			$return_array['client_id'][$where_array['invoice_mst.company_id']]['selected'] = 'selected';
		}
		
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		return $return_array;
	}

	private function set_sorting_value($sort_array) {

		// echo "<pre>";print_r($sort_array);echo"</pre><hr>";
		$sorting_static_array = array('invoice_mst.invoice_no', 'invoice_mst.invoice_date', 'customer_mst.name', 'country_mst.name','invoice_mst.grand_total', 'invoice_mst.invoice_status', 'users.name');
		$sorting = array();
		foreach ($sorting_static_array as $sorting_static_value) {
			
			$sorting[strtolower($sorting_static_value)]['class_name'] = 'sorting';
			$sorting[strtolower($sorting_static_value)]['value'] = 'asc';
			if($sort_array['column_name'] == $sorting_static_value) {
				if($sort_array['column_value'] == 'asc') {

					$sorting[strtolower($sorting_static_value)]['class_name'] = 'sorting_asc';
					$sorting[strtolower($sorting_static_value)]['value'] = 'desc';
				} else {
					$sorting[strtolower($sorting_static_value)]['class_name'] = 'sorting_desc';
				}			
			}
		}
		$sorting['column_name'] = $sort_array['column_name'];
		$sorting['column_value'] = $sort_array['column_value'];
		// echo "<pre>";print_r($sorting);echo"</pre><hr>";die('debug');
		return $sorting;
	}

	private function prepare_invoice_order_body_data($count_no = 1, $product_details = array('invoice_dtl_id' => '', 'product_id' => '', 'material_id' => '', 'description' => '', 'quantity' => '', 'rate' => '', 'price' => '', 'hscode' => '')) { 

		$return_array = $product_details;
		$return_array['count_no'] = $count_no;
		$return_array['product_list']= $this->invoice_model->get_data(
																	'id, name',
																	array(
																		'status' => 'Active'
																	),
																	'product_mst'
																);
		$return_array['material_list'] = $this->invoice_model->get_data(
																	'id, name',
																	array(
																		'status' => 'Active'
																	),
																	'material_mst'
																);
		return $return_array;
	}

	private function set_invoice_order_session_value($net_total, $count_no) {

		$invoice_order_details = array();
		$id = $this->uri->segment('3',0);
		$session_invoice_order_details = $this->session->userdata('invoice_order_details_'.$id);
		if(empty($session_invoice_order_details)) {

			$invoice_order_details[$count_no-1] = $net_total;
			$session_data['net_total'] = array_sum($invoice_order_details);
			$session_data['gross_total'] = array_sum($invoice_order_details);
			$session_data['order_wise_total'] = $invoice_order_details;
			$session_data['different_charges'] = array();
		} else {

			$invoice_order_details = $session_invoice_order_details['order_wise_total'];
			$invoice_order_details[$count_no] = $net_total;
			$session_data['net_total'] = array_sum($invoice_order_details);
			$session_data['gross_total'] = array_sum($invoice_order_details);
			foreach ($session_invoice_order_details['different_charges'] as $charge_details) {
				
				if($charge_details['add_minus_charges_value'] == 'Added') {
					
					$session_data['gross_total'] = $session_data['gross_total'] + $charge_details['charge_value'];
				}else if($charge_details['add_minus_charges_value'] == 'Deducted') {

					$session_data['gross_total'] = $session_data['gross_total'] - $charge_details['charge_value'];
				}
			}
			$session_data['order_wise_total'] = $invoice_order_details;
			$session_data['different_charges'] = $session_invoice_order_details['different_charges'];
		}
		$this->session->set_userdata('invoice_order_details_'.$id,$session_data);
	}

	private function generate_session($invoice_details, $product_details, $charges_details) {

		$id = $this->uri->segment('3',0);
		$charge_name_array = array('Added'=>'+', 'Deducted'=>'-');
		$order_details['net_total'] = $invoice_details['net_total'];
		$order_details['gross_total'] = $invoice_details['grand_total'];
		foreach ($product_details as $product_details_key => $product_details_value) {
			$order_details['order_wise_total'][$product_details_key+1] = $product_details_value['price'];
		}	
		foreach ($charges_details as $charges_details_key => $charges_details_value) {
			
			$order_details['different_charges'][$charges_details_key]['charges_name'] = $charges_details_value['charge_name'];
			$order_details['different_charges'][$charges_details_key]['add_minus_charges'] = $charge_name_array[$charges_details_value['charge_deducted_or_added']];
			$order_details['different_charges'][$charges_details_key]['charge_value'] = $charges_details_value['charge_value'];
			$order_details['different_charges'][$charges_details_key]['add_minus_charges_value'] = $charges_details_value['charge_deducted_or_added'];
			$order_details['different_charges'][$charges_details_key]['gross_total'] = $charges_details_value['gross_total_after_charge'];
		}
		$this->session->set_userdata('invoice_order_details_'.$id, $order_details);
	}

	//invoice listing code End
	//invoice export code start
	
    private function create_export_invoice_data($where_array,$limit =10, $offset=0) {

		$invoice_export_details = $this->invoice_model->get_export_invoice_listing_data($where_array,$limit, $offset,'result_array');

		// echo "<pre>";print_r($invoice_export_details);echo"</pre><hr>";exit;
		$data['export_list'] = array();
	  	foreach($invoice_export_details['invoice_export_list'] as $key => $single_details) {

            $data['export_list'][$key]['invoice_id'] =$single_details['invoice_mst_id'];
	  		$data['export_list'][$key]['invoice_no'] =$single_details['invoice_no'];
	  		$data['export_list'][$key]['invoice_date'] =$single_details['invoice_date'];
	  		$data['export_list'][$key]['consignee_name'] =$single_details['consignee_name'];
	  		$data['export_list'][$key]['port_of_loading'] =$single_details['port_of_loading'];
	  		$data['export_list'][$key]['port_of_discharge'] =$single_details['port_of_discharge'];
	  		$data['export_list'][$key]['final_destination'] =$single_details['final_destination'];
	  		$data['export_list'][$key]['buyer_other_then_consignee'] =$single_details['buyer_other_then_consignee'];
	  		$data['export_list'][$key]['currency'] =$single_details['currency'];
	  		$data['export_list'][$key]['payment_terms'] =$single_details['payment_terms'];
	  		$data['export_list'][$key]['grand_total'] =$single_details['grand_total'];
	  		$data['export_list'][$key]['cha_name'] =$single_details['cha_name'];
	  		$data['export_list'][$key]['outstanding_amount'] =$single_details['outstanding_amount'];
	  		$data['export_list'][$key]['extra'] =$single_details['extra'];
	  		$data['export_list'][$key]['po_no'] =$single_details['po_no'];
	  		$data['export_list'][$key]['port_code'] =$single_details['port_code'];
	  		$data['export_list'][$key]['bill_given_to_ca'] =$single_details['bill_given_to_ca'];
	  		$data['export_list'][$key]['e_brc_remark'] =$single_details['e_brc_remark'];
	  		$data['export_list'][$key]['bank_sub_date'] =$single_details['bank_sub_date'];
	  		$data['export_list'][$key]['bill_id_no'] =$single_details['bill_id_no'];
 	  		$data['export_list'][$key]['brc_no'] =$single_details['brc_no'];
	  		$data['export_list'][$key]['rodtep_scheme'] =$single_details['rodtep_scheme'];
	  		$data['export_list'][$key]['final_remark'] =$single_details['final_remark'];
	  		$data['export_list'][$key]['part_payment_details'] = $this->create_static_response_for_json_value($single_details, 'part_payment_details');
    		$data['export_list'][$key]['advisery_no'] = $this->create_static_response_for_json_value($single_details, 'advisery_no');
    		$data['export_list'][$key]['advisery_amount'] = $this->create_static_response_for_json_value($single_details, 'advisery_amount');
    		$data['export_list'][$key]['utilised_amount'] = $this->create_static_response_for_json_value($single_details, 'utilised_amount');
    		$data['export_list'][$key]['shipping_bill_details'] = $this->create_static_response_for_json_value($single_details, 'shipping_bill_details');
    		$data['export_list'][$key]['egm'] = $this->create_static_response_for_json_value($single_details, 'egm');
	  	}
		$data['paggination_data'] = $invoice_export_details['paggination_data'];
		$data['paggination_data']['limit'] = $limit;
		$data['paggination_data']['offset'] = $offset;
		$data['currency_list'] = $this->invoice_model->get_data('*',array('status'=>'Active'),'currency');
		$data['payment_terms_list'] = $this->invoice_model->get_data('*',array('status'=>'Active'),'payment_terms');
		$data['search_filter'] = array();
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		return $data;
	}
	
	private function invoice_export_search_filter_value($search_form_data){
      	
		$where_array = array();
		$where_array['invoice_mst.is_deleted is'] = 'NULL';
		$where_array['invoice_mst.invoice_date >='] = '"2021-04-01"';
		$where_array['invoice_mst.invoice_date <='] = '"2022-03-31"';
		$where_array['invoice_mst.invoice_no NOT LIKE'] = '"%INSTINOX"';
		if(!empty($search_form_data)) {

			$form_data = array_column($search_form_data, 'value', 'name');
			foreach ($form_data as $column_name => $column_value) {

				if(!empty($column_value)){
					if(in_array($column_name, array('invoice_mst.invoice_no', 'invoice_export_info.consignee_name', 'invoice_export_info.port_of_loading', 'invoice_export_info.port_of_discharge', 'invoice_export_info.final_destination', 'invoice_export_info.buyer_other_then_consignee', 'invoice_export_info.cha_name', 'invoice_export_info.po_no', 'invoice_export_info.port_code', 'invoice_export_info.bill_given_to_ca', 'invoice_export_info.e_brc_remark', 'invoice_export_info.bill_id_no', 'invoice_export_info.brc_no', 'invoice_export_info.rodtep_scheme', 'invoice_export_info.final_remark'))) {

						$where_array[$column_name.' LIKE '] = "'%".$column_value."%'";

					} else if(in_array($column_name, array('invoice_mst.invoice_date', 'invoice_mst.grand_total', 'invoice_export_info.outstanding_amount', 'invoice_export_info.extra', 'invoice_export_info.bank_sub_date'))) {
						
						$where_array[$column_name] = "'".trim($column_value)."'";

					} else if(in_array($column_name, array('invoice_export_info.currency', 'invoice_export_info.payment_terms'))) {

						$where_array[$column_name.' IN '] = "'".implode("', '", $column_value)."'";

					}
				}
			}
		}
      	// echo "<pre>";print_r($where_array);echo"</pre><hr>";die('debug');
		return $where_array;
    }

    private function create_static_response_for_json_value($export_invoice_details, $details_name){

    	$return_array = array();
    	if(!empty($export_invoice_details[$details_name])) {

			$return_array = json_decode($export_invoice_details[$details_name], true); 
		} else {

			switch ($details_name) {
				case 'part_payment_details':
					
					$return_array = array('part_1'=>0, 'part_2'=>0, 'part_3'=>0, 'part_4'=>0);
					break;

				case 'advisery_no':
				case 'advisery_amount':
				case 'utilised_amount':
					
					$return_array = array('', '', '', '');
					break;

				case 'shipping_bill_details':
					
					$return_array = array('shipping_bill_no'=>'', 'shipping_bill_date'=>'', 'shipping_bill_leo_date'=>'');
					break;	
		
				case 'egm':
					
					$return_array = array('egm_no'=>'', 'egm_date'=>'');
					break;

				default:
					
					break;
			}
		}
		return $return_array;
    }

	//invoice export code end
}?>