<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search_engine extends MX_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('user_id')){
			redirect('login', 'refresh');
			exit;
		}else{
			// $access_count = 0;
			// if(!empty($this->session->userdata('main_module_access')) && in_array(17, array_column($this->session->userdata('main_module_access'), 'module_id'))) {

			// 	foreach ($this->session->userdata('sub_module_access') as $key => $value) {
					
			// 		if($value['module_id'] == 17 && $value['status'] == 'Active') {
			// 			$access_count++;
			// 		}
			// 	}
			// }
			// if($access_count == 0){
			// 	redirect($this->session->userdata('home/dashboard'));
			// 	exit;
			// }
		}
		error_reporting(E_ALL);
		$this->load->model('Search_engine_model');
		$this->load->model('common/common_model');
	}

	public function index(){
		redirect('search_engine/data_show');
	}

	public function data_show() {

		$data = array();
		$data['user_access_details'] = $this->Search_engine_model->get_dynamic_data('*', array('status'=>'Active', 'user_id'=>$this->session->userdata('user_id')), 'search_engine_primary_data', 'row_array');
		$version_details = $this->common_model->get_all_conditional_data_sales_db('id', array('status'=> 'Active'), 'version_information', 'row_array', array(), array('column_name'=> 'id','column_value'=>'desc'));

		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		if(!empty($data['user_access_details'])) {
			
			$search_engine_access_url_details = array_column($this->Search_engine_model->get_dynamic_data('*', array('status'=>'Active', 'version_id'=> $version_details['id'], 'access_type'=> $data['user_access_details']['access_type']), 'search_engine_access_url'), 'url', 'lead_name');
			if($data['user_access_details']['piping'] == 'Yes') {
					
				$data['user_access_details']['piping_url'] = $search_engine_access_url_details['piping'];
			}
			if($data['user_access_details']['tube_fitting_and_valves'] == 'Yes') {

				$data['user_access_details']['tube_fitting_and_valves_url'] = $search_engine_access_url_details['tube_fitting_and_valves'];
			}
			if($data['user_access_details']['tubing'] == 'Yes') {

				$data['user_access_details']['tubing_url'] = $search_engine_access_url_details['tubing'];
			}
			if($data['user_access_details']['hammer'] == 'Yes') {

				$data['user_access_details']['hammer_url'] = $search_engine_access_url_details['hammer'];
			}
			// $data['user_access_details']['piping_url'] = 'https://datastudio.google.com/embed/reporting/f220a62c-c163-453b-bff2-f6c0a79938b3/page/sA6NC';
			// $data['user_access_details']['tube_fitting_and_valves_url'] = 'https://datastudio.google.com/embed/reporting/f367183a-dc82-4628-9de7-afcb3eec4311/page/TF6NC';
			// $data['user_access_details']['tubing_url'] = 'https://datastudio.google.com/embed/reporting/85f646d3-574d-493b-8b6f-fcbba726dafd/page/7F6NC';
			// $data['user_access_details']['hammer_url'] = 'https://datastudio.google.com/embed/reporting/d24a6436-15c0-49a0-943d-8cd49cea8fd5/page/LG6NC';
			// if($data['user_access_details']['access_type'] == 'Admin') {

			// 	$data['user_access_details']['piping_url'] = 'https://datastudio.google.com/embed/reporting/2515ae09-5706-4229-aee0-52837df7e92c/page/sA6NC';
			// 	$data['user_access_details']['tube_fitting_and_valves_url'] = 'https://datastudio.google.com/embed/reporting/e078e98a-8a98-4985-afa2-718861272bc3/page/TF6NC';
			// 	$data['user_access_details']['tubing_url'] = 'https://datastudio.google.com/embed/reporting/48704ba3-3749-4340-90db-97537f631a64/page/7F6NC';
			// 	$data['user_access_details']['hammer_url'] = 'https://datastudio.google.com/embed/reporting/cfa2acda-a503-42b7-92bb-6aa7f65a3357/page/LG6NC';
			// }
		}

		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Search Engine Primary'));
		$this->load->view('sidebar', array('title' => 'Search Engine Primary'));
		$this->load->view('search_engine/data_show', $data);
		$this->load->view('footer');	
	}

	public function data_show_without_extract() {

		$data = array();
		// Without extract:
		$data['pipes'] = 'https://datastudio.google.com/u/1/reporting/4905238e-7cf0-4c67-a805-08664035ca86/page/sA6NC';
		$data['tf'] = 'https://datastudio.google.com/u/1/reporting/4905238e-7cf0-4c67-a805-08664035ca86/page/TF6NC';
		$data['tubing'] = 'https://datastudio.google.com/u/1/reporting/4905238e-7cf0-4c67-a805-08664035ca86/page/7F6NC';
		$data['hammer'] = 'https://datastudio.google.com/u/1/reporting/4905238e-7cf0-4c67-a805-08664035ca86/page/LG6NC';
		$this->load->view('header', array('title' => 'Hr Add Employee Details'));
		$this->load->view('sidebar', array('title' => 'Hr Add Employee Details'));
		$this->load->view('search_engine/data_show_without_extract', $data);
		$this->load->view('footer');	
	}

	public function manage_data_show() {

		$data = array();
		$this->load->view('header', array('title' => ''));
		$this->load->view('sidebar', array('title' => ''));
		$this->load->view('search_engine/manage_data_show', $data);
		$this->load->view('footer');	
	}

	public function search_engine_access() {

		$data = array();
		$data['search_engine_list'] = $this->Search_engine_model->get_dynamic_data('*', array('status'=>'Active'), 'search_engine_primary_data');
		$data['user_list'] = array_column($this->Search_engine_model->get_dynamic_data('*', array(), 'users'), 'name', 'user_id');
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => ''));
		$this->load->view('sidebar', array('title' => 'Search Engine Access'));
		$this->load->view('search_engine/search_engine_access', $data);
		$this->load->view('footer');	
	}

	public function search_engine_access_update() {

		$id = $this->uri->segment('3', 0);
		if(!empty($id)) {

			$data = array();
			$data['single_user_details'] = $this->Search_engine_model->get_dynamic_data('*', array('status'=>'Active', 'id'=> $id), 'search_engine_primary_data', 'row_array');
			$data['user_list'] = array_column($this->Search_engine_model->get_dynamic_data('*', array(), 'users'), 'name', 'user_id');
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
			$this->load->view('header', array('title' => ''));
			$this->load->view('sidebar', array('title' => 'Search Engine Access'));
			$this->load->view('search_engine/search_engine_access_update', $data);
			$this->load->view('footer');	
		} else {
			$user_list = $this->Search_engine_model->get_dynamic_data('*', array('status'=>1), 'users');
			$role_list = array_column($this->Search_engine_model->get_dynamic_data('*', array(), 'role'), 'role_name', 'role_id');
			foreach ($user_list as $value) {
				if(!empty($role_list[$value['role']])) {

					$data['user_list'][$role_list[$value['role']]][]= $value;
				}
			}
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
			$this->load->view('header', array('title' => ''));
			$this->load->view('sidebar', array('title' => 'Search Engine Access'));
			$this->load->view('search_engine/search_engine_access_update', $data);
			$this->load->view('footer');
		}
	}

	public function ajax_function() {

		if($this->input->is_ajax_request()){
			$post_details = $this->input->post();
			$response['status'] = 'successful';	
			switch ($post_details['call_type']) {

				case 'search_engine_access_update_form':

					$form_data = array_column($post_details['form_data'], 'value', 'name');
					if(!empty($form_data['id'])) {
						$this->common_model->update_data_sales_db('search_engine_primary_data', array($form_data), 'id', 'batch');
					}else{
						//check user id exist or not
						$user_exist_or_not = $this->Search_engine_model->get_dynamic_data('*', array('status'=> 'Active', 'user_id'=> $form_data['user_id']), 'search_engine_primary_data', 'row_array');
						if(empty($user_exist_or_not)) {
							$this->common_model->insert_data_sales_db('search_engine_primary_data', $form_data);
						}else{
							$this->common_model->update_data_sales_db('search_engine_primary_data', $form_data, array('id'=> $user_exist_or_not['id']));
						}
					}
				break;
				case 'delete_search_engine_access':
					if(!empty($post_details['id'])) {

						$this->common_model->update_data_sales_db('search_engine_primary_data', array('status'=>'Inactive'), array('id'=>$post_details['id']));
					}
				break;
				default:
					$response['status'] = 'failed';	
					$response['message'] = "call type not found";
				break;
			}
			echo json_encode($response);
		} else {
			die('access is not allowed to this function');
		}
	}
}