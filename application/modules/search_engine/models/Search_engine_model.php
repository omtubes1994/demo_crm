<?php 
Class Search_engine_model extends CI_Model{

	function __construct(){
		parent::__construct();
		error_reporting(E_ALL);
	}

	public function insert_data($table, $data){
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	public function update_data($table, $data, $where){
		return $this->db->update($table, $data, $where);
	}

	public function get_dynamic_data($select = '*', $where, $table_name, $return_type = 'result_array', $limit = 0, $offset = 0) {

		$this->db->select($select);
		$this->db->where($where);
		return $this->db->get($table_name, $limit, $offset)->$return_type();
	}

	public function insert_data_batch($table_name, $insert_data) {

		$this->db->insert_batch($table_name, $insert_data);
	}

	public function update_data_batch($table_name, $update_data, $where_string) {

		$this->db->update_batch($table_name, $update_data, $where_string);	
	}
}