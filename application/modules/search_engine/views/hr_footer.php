<!-- begin::Global Config(global config for global JS sciprts) -->
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": [
							"#c5cbe3",
							"#a1a8c3",
							"#3d4465",
							"#3e4466"
						],
						"shape": [
							"#f0f3ff",
							"#d9dffa",
							"#afb4d4",
							"#646c9a"
						]
					}
				}
			};
		

		</script>

		<!-- end::Global Config -->

		<!--begin::Global Theme Bundle(used by all pages) -->
		<script src="assets/plugins/global/plugins.bundle.js" type="text/javascript"></script>
		<script src="assets/js/scripts.bundle.js" type="text/javascript"></script>


		<script src="assets/js/pages/crud/metronic-datatable/base/html-table.js" type="text/javascript"></script>

		<!--end::Global Theme Bundle -->
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<script type="text/javascript">
			var KTBootstrapDatepicker = function () {

			    var arrows;
			    var arrows = {
			            leftArrow: '<i class="la la-angle-right"></i>',
			            rightArrow: '<i class="la la-angle-left"></i>'
			        }
			    // Private functions
			    var demos = function () {
			       
			        // enable clear button 
			        $('#birth_date, #kt_datepicker_3_validate').datepicker({
			            rtl: KTUtil.isRTL(),
			            todayBtn: "linked",
			            clearBtn: true,
			            todayHighlight: true,
			            templates: arrows,
			            dateFormat: 'yy-mm-dd',
			            autoclose: true
			        });
			    };

			    return {
			        // public functions
			        init: function() {
			            demos(); 
			        }
			    };
			}();

			var KTDropzoneDemo = function () {

			    var demo3 = function () {
			       
			        // set the dropzone container id
			        var id = '#kt_dropzone_5';

			        // set the preview element template
			        var previewNode = $(id + " .dropzone-item");
			        previewNode.id = "";
			        var previewTemplate = previewNode.parent('.dropzone-items').html();
			        previewNode.remove();

			        var myDropzone5 = new Dropzone(id, { // Make the whole body a dropzone
			            url: "<?php echo base_url('hr/upload_document');?>", // Set the url for your upload script location
			            paramName: "file", // The name that will be used to transfer the file
			            params: {'doc_type': 'aadhar_card', 'aadhar_number': $('input#aadhar_number').val()},
			            maxFiles: 1,
			            maxFilesize: 1, // Max filesize in MB
			            previewTemplate: previewTemplate,
			            previewsContainer: id + " .dropzone-items", // Define the container to display the previews
			            acceptedFiles: ".jpeg, .jpg, .png, .pdf",
			            clickable: id + " .dropzone-select" // Define the element that should be used as click trigger to select files.
			        });

			        myDropzone5.on("addedfile", function(file) {
			            // Hookup the start button
			            $(document).find( id + ' .dropzone-item').css('display', '');
			        });

			        // Update the total progress bar
			        myDropzone5.on("totaluploadprogress", function(progress) {
			            $( id + " .progress-bar").css('width', progress + "%");
			        });

			        myDropzone5.on("sending", function(file) {
			            // Show the total progress bar when upload starts
			            $( id + " .progress-bar").css('opacity', "1");
			        });

			        // Hide the total progress bar when nothing's uploading anymore
			        myDropzone5.on("complete", function(progress) {
			           	var thisProgressBar = id + " .dz-complete";
			            setTimeout(function(){
			                $( thisProgressBar + " .progress-bar, " + thisProgressBar + " .progress").css('opacity', '0');
			            }, 300);
			            swal({
				    		title: "File is Uploaded Successfully",
				      		icon: "success",
				    	});
			        });

		  			//  myDropzone5.on("removedfile", function(file) {
					// 	$.ajax({
					// 		url: "<?php echo site_url('hr/remove_document'); ?>",
					// 		type: "POST",
					// 		data: {'mtc_for_id': '<?php if(isset($mtc_details)){echo $mtc_details['mtc_for_id'];} ?>'},
					// 	});
					// });
			    }
			    var pan_card = function () {
			        
			        // set the dropzone container id
			        var id = '#pan_card_dropzone';

			        // set the preview element template
			        var previewNode = $(id + " .dropzone-item");
			        previewNode.id = "";
			        var previewTemplate = previewNode.parent('.dropzone-items').html();
			        previewNode.remove();

			        var pan_card = new Dropzone(id, { // Make the whole body a dropzone
			            url: "<?php echo base_url('hr/upload_document');?>", // Set the url for your upload script location
			            paramName: "file", // The name that will be used to transfer the file
			            params: {'doc_type': 'pan_card', 'pan_number': $('input#pan_number').val()},
			            maxFiles: 1,
			            maxFilesize: 1, // Max filesize in MB
			            previewTemplate: previewTemplate,
			            previewsContainer: id + " .dropzone-items", // Define the container to display the previews
			            acceptedFiles: ".jpeg, .jpg, .png, .pdf",
			            clickable: id + " .dropzone-select" // Define the element that should be used as click trigger to select files.
			        });

			        pan_card.on("addedfile", function(file) {
			             // Hookup the start button
			             $(document).find( id + ' .dropzone-item').css('display', '');
			        });

			        // Update the total progress bar
			        pan_card.on("totaluploadprogress", function(progress) {
			            $( id + " .progress-bar").css('width', progress + "%");
			        });

			        pan_card.on("sending", function(file) {
			            // Show the total progress bar when upload starts
			            $( id + " .progress-bar").css('opacity', "1");
			        });

			        // Hide the total progress bar when nothing's uploading anymore
			        pan_card.on("complete", function(progress) {
			            var thisProgressBar = id + " .dz-complete";
			            setTimeout(function(){
			                $( thisProgressBar + " .progress-bar, " + thisProgressBar + " .progress").css('opacity', '0');
			            }, 300);
			            swal({
				    		title: "File is Uploaded Successfully",
				      		icon: "success",
				    	});
			        });

		  			//  pan_card.on("removedfile", function(file) {
					// 	$.ajax({
					// 		url: "<?php echo site_url('hr/remove_document'); ?>",
					// 		type: "POST",
					// 		data: {'mtc_for_id': '<?php if(isset($mtc_details)){echo $mtc_details['mtc_for_id'];} ?>'},
					// 	});
					// });
			    }

			    var profile_pic = function () {
			        
			        // set the dropzone container id
			         	var id = '#profile_pic_dropzone';

			         // set the preview element template
			         var previewNode = $(id + " .dropzone-item");
			         previewNode.id = "";
			         var previewTemplate = previewNode.parent('.dropzone-items').html();
			         previewNode.remove();

			         var profile_pic = new Dropzone(id, { // Make the whole body a dropzone
			            url: "<?php echo base_url('hr/upload_document');?>", // Set the url for your upload script location
			            paramName: "file", // The name that will be used to transfer the file
			            params: {'doc_type': 'profile_pic'},
			            maxFiles: 1,
			            maxFilesize: 1, // Max filesize in MB
			            previewTemplate: previewTemplate,
			            previewsContainer: id + " .dropzone-items", // Define the container to display the previews
			            acceptedFiles: ".jpeg, .jpg, .png",
			            clickable: id + " .dropzone-select" // Define the element that should be used as click trigger to select files.
			         });

			         profile_pic.on("addedfile", function(file) {
			             // Hookup the start button
			             $(document).find( id + ' .dropzone-item').css('display', '');
			         });

			         // Update the total progress bar
			         profile_pic.on("totaluploadprogress", function(progress) {
			             $( id + " .progress-bar").css('width', progress + "%");
			         });

			         profile_pic.on("sending", function(file) {
			             // Show the total progress bar when upload starts
			             $( id + " .progress-bar").css('opacity', "1");
			         });

			         // Hide the total progress bar when nothing's uploading anymore
			         profile_pic.on("complete", function(progress) {
			             var thisProgressBar = id + " .dz-complete";
			             setTimeout(function(){
			                 $( thisProgressBar + " .progress-bar, " + thisProgressBar + " .progress").css('opacity', '0');
			             }, 300);
			            swal({
				    		title: "File is Uploaded Successfully",
				      		icon: "success",
				    	});
			         });

		  			//  profile_pic.on("removedfile", function(file) {
					// 	$.ajax({
					// 		url: "<?php echo site_url('hr/remove_document'); ?>",
					// 		type: "POST",
					// 		data: {'mtc_for_id': '<?php if(isset($mtc_details)){echo $mtc_details['mtc_for_id'];} ?>'},
					// 	});
					// });
			    }

			    var salary_slip = function () {
			       
			        // set the dropzone container id
			        var id = '#salary_slip_upload';

			        // set the preview element template
			        var previewNode = $(id + " .dropzone-item");
			        previewNode.id = "";
			        var previewTemplate = previewNode.parent('.dropzone-items').html();
			        previewNode.remove();
			        setTimeout(function(){
		                
				        var salary_slip_upload = new Dropzone(id, { // Make the whole body a dropzone
				            url: "<?php echo base_url('hr/upload_document');?>", // Set the url for your upload script location
				            paramName: "file", // The name that will be used to transfer the file
				            params: {'doc_type': 'salary_slip', 'people_id': $('select#employee_name').val(), 'year': $('select#salary_slip_year').val(), 'month': $('select#salary_slip_month').val()},
				            maxFiles: 1,
				            maxFilesize: 1, // Max filesize in MB
				            previewTemplate: previewTemplate,
				            previewsContainer: id + " .dropzone-items", // Define the container to display the previews
				            acceptedFiles: ".jpeg, .jpg, .png, .pdf",
				            clickable: id + " .dropzone-select" // Define the element that should be used as click trigger to select files.
				        });

				        salary_slip_upload.on("addedfile", function(file) {
				            // Hookup the start button
				            $(document).find( id + ' .dropzone-item').css('display', '');
				        });

				        // Update the total progress bar
				        salary_slip_upload.on("totaluploadprogress", function(progress) {
				            $( id + " .progress-bar").css('width', progress + "%");
				        });

				        salary_slip_upload.on("sending", function(file) {
				            // Show the total progress bar when upload starts
				            $( id + " .progress-bar").css('opacity', "1");
				        });

				        // Hide the total progress bar when nothing's uploading anymore
				        salary_slip_upload.on("complete", function(progress) {
				           	var thisProgressBar = id + " .dz-complete";
				            setTimeout(function(){
				                $( thisProgressBar + " .progress-bar, " + thisProgressBar + " .progress").css('opacity', '0');
				            }, 300);
				            swal({
					    		title: "File is Uploaded Successfully",
					      		icon: "success",
					    	});
		            }, 300);
		        });

		  			//  salary_slip_upload.on("removedfile", function(file) {
					// 	$.ajax({
					// 		url: "<?php echo site_url('hr/remove_document'); ?>",
					// 		type: "POST",
					// 		data: {'mtc_for_id': '<?php if(isset($mtc_details)){echo $mtc_details['mtc_for_id'];} ?>'},
					// 	});
					// });
			    }

			    return {
			        // public functions
			        init: function() {
						<?php if($this->uri->segment(2) == 'add_employee') {?>
				            demo3();
				            pan_card();
				            profile_pic();
	    				<?php } else if($this->uri->segment(2) == 'add_salary_slip') { ?>
				            // demo3();
			            	salary_slip();
	    				<?php } ?>
			        }
			    };
			}();

			var KTWizard2 = function () {
			    // Base elements
			    var wizardEl;
			    var formEl;
			    var validator;
			    var wizard;

			    // Private functions
			    var initWizard = function () {
			        // Initialize form wizard
			        wizard = new KTWizard('kt_wizard_v2', {
			            startStep: 1, // initial active step number
						clickableSteps: true  // allow step clicking
			        });

			        // Validation before going to next page
			        wizard.on('beforeNext', function(wizardObj) {
			            if (validator.form() !== true) {
			                wizardObj.stop();  // don't go to the next step
			            }
			            if(wizardObj.currentStep == 1){

			            	// check name already present or not
		            		$.ajax({
								type: 'POST',
								data: {call_type: 'check_user_exist_or_not', first_name: $('input#first_name').val(), last_name: $('input#last_name').val()},
								url: "<?php echo base_url('hr/ajax_function'); ?>",
								dataType: 'JSON',
								success: function(res){

									if(res.status == 'successful') {

										if(res.user_exists) {

							                swal({
					                            title: "",
									    		text: "First name and last name already exist!",
									      		icon: "warning"
									    	});
											setTimeout(function() { 
												location.reload();
										    }, 2000);
										}
									}
								},
								beforeSend: function(response){
								}
							});
			            }
			            if(wizardObj.currentStep == 3){
			            	if($('div.actual_address').attr('style') == 'display:none') {
			            		// $('button[data-ktwizard-type="action-next"]').trigger("click");
			            		$('div.actual_address_main').attr('style','display:none');
			            	}
			            }
			        });

			        wizard.on('beforePrev', function(wizardObj) {
						if (validator.form() !== true) {
							wizardObj.stop();  // don't go to the next step
						}
					});

			        // Change event
			        wizard.on('change', function(wizard) {
			            KTUtil.scrollTop();
			        });
			    }

			    var initValidation = function() {
			        validator = formEl.validate({
			            // Validate only visible fields
			            ignore: ":hidden",

			            // Validation rules
			            rules: {
			               	//= Step 1
							first_name: {
								required: true
							},
							last_name: {
								required: true
							},
							gender: {
								required: true
							},
							blood_group: {
								required: true
							},
							birth_date: {
								required: true
							},
							age: {
								required: true
							},

							//= Step 2
							personal_number: {
								required: true,
						        minlength:10,
						        maxlength:10
							},
							personal_email: {
								required: true,
								email: true
							},
							marriage_status: {
								required: true
							},
							qualification: {
								required: true
							},
							aadhar_number: {
								required: true,
								
								number: true,
								minlength:12,
						        maxlength:12
							},
							pan_number: {
								required: true,
								minlength:10,
						        maxlength:10
							},

							//= Step 3
							resident_address_1: {
								required: true
							},
							resident_address_2: {
								required: true
							},
							resident_postcode: {
								required: true,
								number: true,
								minlength: 6,
								maxlength:6
							},	
							resident_state: {
								required: true
							},
							resident_city: {
								required: true
							},

							//= Step 4
							actual_address_1: {
								required: true
							},
							actual_address_2: {
								required: true
							},
							actual_postcode: {
								required: true,
								number: true,
								minlength: 6,
								maxlength:6
							},	
							actual_state: {
								required: true
							},
							actual_city: {
								required: true
							},

							//= Step 5
							official_phone: {
								required: true,
								minlength:10,
						        maxlength:10
							},
							official_email: {
								required: true,
								email: true
							},
							job_location: {
								required: true
							},
							department: {
								required: true
							},
							// designation: {
							// 	required: true
							// },

							//= Step 6
							bank_name: {
								required: true
							},
							branch_name: {
								required: true
							},
							beneficiary_name: {
								required: true
							},
							account_number: {
								required: true
							},
							ifsc_code: {
								required: true
							},
							//= Step 6

							number_1: {
								// required: true,
								minlength:10,
						        maxlength:10
							},
							alternate_number_1: {
								// required: true,
								minlength:10,
						        maxlength:10
							},
							email_1: {
								// required: true,
								// email: true
							},
							number_2: {
								// required: true,
								minlength:10,
						        maxlength:10
							},
							alternate_number_2: {
								// required: true,
								minlength:10,
						        maxlength:10
							},
							email_2: {
								// required: true,
								// email: true
							},
			            },

			            // Display error
			            invalidHandler: function(event, validator) {
			                KTUtil.scrollTop();

			                swal({
					    		title: "",
					    		text: "There are some errors in your submission. Please correct them.",
					      		icon: "warning"
					    	});
			                // swal({
			                //     "title": "",
			                //     "text": "There are some errors in your submission. Please correct them.",
			                //     "type": "error",
			                //     "confirmButtonClass": "btn btn-secondary"
			                // });
			            },

			            // Submit valid form
			            submitHandler: function (form) {
			                
			            }
			        });
			    }

			    var initSubmit = function() {
			        var btn = formEl.find('[data-ktwizard-type="action-submit"]');

			        btn.on('click', function(e) {

			        	if (!btn.hasClass('kt-spinner')){

				            e.preventDefault();

				            if (validator.form()) {
				                var data = {
					    			call_type: 'submit_add_employee_form',
					    			add_employee_form_data: $('form#add_employee_form').serializeArray()
					    		};

				                $.ajax({
									type: 'POST',
									data: data,
									url: "<?php echo base_url('hr/ajax_function'); ?>",
									dataType: 'JSON',
									success: function(res){

										if(res.status == 'successful') {

					                        KTApp.unprogress(btn);
											
							                swal({
					                            title: "",
									    		text: "The application has been successfully submitted!",
									      		icon: "success"
									    	});
										}
									},
									beforeSend: function(response){
				                		KTApp.progress(btn);
									}
								});
				            }
			        	}

			        });
			    }

			    return {
			        // public functions
			        init: function() {
			            wizardEl = KTUtil.get('kt_wizard_v2');
			            formEl = $('#add_employee_form');

			            initWizard();
			            initValidation();
			            initSubmit();
			        }
			    };
			}();

			jQuery(document).ready(function() {
				
				KTBootstrapDatepicker.init();
			    KTDropzoneDemo.init();
				<?php if($this->uri->segment(2) == 'add_employee') {?>
	    			KTWizard2.init();
	    		<?php } ?>

    			$('button.next_button').click(function(){

			    	var data = {
		    			call_type: 'get_review_of_employee',
		    			add_employee_form_data: $('form#add_employee_form').serializeArray()
		    		};
	    			ajax_call_function(data, 'get_review_of_employee');
    			});

    			$('label.mark_address_permanent').click(function(){

    				if ($('input#address_permanent').is(':checked')) {
    					$('div.actual_address').attr('style','display:none');
    				} else {
    					$('div.actual_address').attr('style','display:block');
    				}
    			});

			    $('input#birth_date').change(function(){

			    	ajax_call_function({call_type: 'get_age_on_birth_date', birth_date: $('input#birth_date').val()}, 'get_age_on_birth_date');
			    });

			    $('div.last_step').click(function(){

			    	var data = {
		    			call_type: 'get_review_of_employee',
		    			add_employee_form_data: $('form#add_employee_form').serializeArray()
		    		};
	    			ajax_call_function(data, 'get_review_of_employee');
			    });

			    $('div#hr_user_list_paggination').on('click', 'li.hr_user_list_paggination', function(){

			    	paggination_filter_data_call($(this).attr('limit'), $(this).attr('offset'));
			    });

			    $('div#hr_user_list_paggination').on('change', 'select.hr_user_list_paggination', function(){

			    	paggination_filter_data_call($('select.hr_user_list_paggination').val());
			    });

			    $('a.add_user_search_filter_form').click(function(){

			    	$('div.user_search_filter').slideDown();
			    });

			    $('div#user_search_filter_form_div').on('click', 'button.user_search_filter_form_submit', function(){

			    	var user_id = $('select[name=employee_id]').val();
			    	$('select[name=employee_id]').val(user_id);
			    	set_reset_spinner('user_search_form', this);
			    	paggination_filter_data_call($('input#user_paggination_limit').val(), $('input#user_paggination_offset').val());
			    });
			    	
			    $('div#user_search_filter_form_div').on('click', 'button.user_search_filter_form_reset', function(){
			    	
			    	paggination_filter_data_call({call_type: 'paggination_filter_data'});
			    });

			    $('a.update_information_tab').click(function(){

			    	$('a.update_information_tab').removeClass('kt-widget__item--active');
			    	$(this).addClass('kt-widget__item--active');
			    });

			    $('button.employee_update_account_information, button.employee_update_personal_information, button.employee_update_address_information, button.employee_update_official_information, button.employee_update_bank_information, button.employee_update_reference_information').click(function(){

			    	var form_id = $(this).attr('form_id');
			    	var data = {
		    			call_type: form_id,
		    			update_form_data: $('form#'+form_id).serializeArray(),
		    			people_id: '<?php echo $this->uri->segment(4);?>'
		    		};
	    			ajax_call_function(data, 'employee_form_updated');
			    });


			    $('select#employee_name').change(function(){

		    		$('div.salary_slip_year').slideDown();
			    	if($(this).val() == ''){
		    			$('div.salary_slip_year').slideUp();
			    		$('select#salary_slip_year').val('');
	            		$('select#salary_slip_year').trigger("change");
			    		$('select#salary_slip_month').val('');
	            		$('select#salary_slip_month').trigger("change");
			    	}
			    });

			    $('select#salary_slip_year').change(function(){

		    		$('div.salary_slip_month').slideDown();
			    	if($(this).val() == ''){

		    			$('div.salary_slip_month').slideUp();
			    		$('select#salary_slip_month').val('');
	            		$('select#salary_slip_month').trigger("change");
			    	}
			    });

			    $('select#salary_slip_month').change(function(){

		    		$('div.salary_slip_upload').slideDown();
			    	if($(this).val() == ''){

		    			$('div.salary_slip_upload').slideUp();
			    	}
			    });

			    function set_reset_spinner(call_type, obj, set_unset_flag = true) {

			    	if(call_type == 'user_search_form') {
			    		if(set_unset_flag) {

				    		$(obj).addClass('kt-spinner');
					    	$(obj).addClass('kt-spinner--right');
					    	$(obj).addClass('kt-spinner--sm');
					    	$(obj).addClass('kt-spinner--light');	
			    		} else{

			    			$(obj).removeClass('kt-spinner');
					    	$(obj).removeClass('kt-spinner--right');
					    	$(obj).removeClass('kt-spinner--sm');
					    	$(obj).removeClass('kt-spinner--light');
			    		}
			    	}
			    }

			    function paggination_filter_data_call(limit = 0, offset = 0) {

			    	var data = {
			    		call_type: 'paggination_filter_data',
			    		limit: limit,
			    		offset: offset,
			    		search_form_data: $('form#user_search_filter_form').serializeArray()
			    	}
			    	ajax_call_function(data, 'paggination_filter_data');
			    }

			    function ajax_call_function(data, callType, url = "<?php echo base_url('hr/ajax_function'); ?>") {

					$.ajax({
						type: 'POST',
						data: data,
						url: url,
						dataType: 'JSON',
						success: function(res){

							if(res.status == 'successful') {

								switch (callType) {

									case 'get_age_on_birth_date':
										
		    							$('input#age').val(res.age);
										break;

									case 'get_review_of_employee':
										
										$('div#employee_review_page').html('').html(res.html_body);		
										break;

									case 'submit_add_employee_form':
										swal({
				                            "title": "",
				                            "text": "The application has been successfully submitted!",
				                            "type": "success",
				                            "confirmButtonClass": "btn btn-secondary"
				                        });	
										break;

									case 'paggination_filter_data':
										$('div#hr_user_list').html('').html(res.html_user_list);
										$('div#hr_user_list_paggination').html('').html(res.html_paggination);
										$('div#user_search_filter_form_div').html('').html(res.html_user_search_form);
										set_reset_spinner('user_search_form', $('button.user_search_filter_form_submit'), false);
										break;

									case 'employee_form_updated':
										swal({
				                            "title": "",
				                            "text": "Inforation is updated successfully!",
				                            "type": "success",
				                            "confirmButtonClass": "btn btn-secondary"
				                        });
				                        setTimeout(function(){
				                        	location.reload();
				                        }, 2000);

									default:
										break;
								}
							}
						},
						beforeSend: function(response){
						}
					});
				};
			});
		</script>

	</body>

	<!-- end::Body -->
</html>