<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col-lg-12">

			<!--begin::Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Update Search Engine Access
						</h3>
					</div>
				</div>

				<!--begin::Form-->
				<form class="kt-form kt-form--label-right" id="search_engine_access_update_form">
					<div class="kt-portlet__body">
						<div class="form-group row form-group-marginless kt-margin-t-20">
							<label class="col-lg-1 col-form-label">Username:</label>
							<div class="col-lg-4">
							<?php if(!empty($single_user_details['user_id'])){?>
								<div class="input-group">
									<div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
									<input type="text" class="form-control" placeholder="<?php echo $user_list[$single_user_details['user_id']]; ?>" readonly>
									<input type="text" name="id" class="form-control" value="<?php echo $single_user_details['id'];?>" hidden>
								</div>
							<?php }else {?>
								<select class="form-control kt-selectpicker" name="user_id">
									<?php foreach ($user_list as $user_department => $user_details) { ?>
									<optgroup label="<?php echo $user_department; ?>" data-max-options="2">
										<?php foreach ($user_details as $single_user_details) { ?>
										<option value="<?php echo $single_user_details['user_id']; ?>"><?php echo $single_user_details['name']; ?></option>
										<?php } ?>
									</optgroup>
									<?php } ?>
								</select>
							<?php }?>
							</div>
							<label class="col-lg-2 col-form-label">Access Type:</label>
							<div class="col-lg-4">
							<?php if(!empty($single_user_details['access_type'])){?>
								<div class="kt-radio-inline">
									<label class="kt-radio kt-radio--solid">
										<input type="radio" name="access_type" 
										<?php echo ($single_user_details['access_type'] == 'Admin') ?'checked':''?>
										value="Admin"> Admin
										<span></span>
									</label>
									<label class="kt-radio kt-radio--solid">
										<input type="radio" name="access_type" 
										<?php echo ($single_user_details['access_type'] == 'Sales')?'checked':''?>
										value="Sales"> Sales
										<span></span>
									</label>
									<label class="kt-radio kt-radio--solid">
										<input type="radio" name="access_type" 
										<?php echo ($single_user_details['access_type'] == 'Procurement')?'checked':''?>
										value="Procurement"> Procurement
										<span></span>
									</label>
									<label class="kt-radio kt-radio--solid">
										<input type="radio" name="access_type" 
										<?php echo ($single_user_details['access_type'] == 'Quality')?'checked':''?>
										value="Quality"> Quality
										<span></span>
									</label>
								</div>
							<?php } else {?>
								<div class="kt-radio-inline">
									<label class="kt-radio kt-radio--solid">
										<input type="radio" name="access_type" value="Admin"> Admin
										<span></span>
									</label>
									<label class="kt-radio kt-radio--solid">
										<input type="radio" name="access_type" value="Sales" checked> Sales
										<span></span>
									</label>
									<label class="kt-radio kt-radio--solid">
										<input type="radio" name="access_type" value="Procurement"> Procurement
										<span></span>
									</label>
									<label class="kt-radio kt-radio--solid">
										<input type="radio" name="access_type" value="Quality"> Quality
										<span></span>
									</label>
								</div>
							<?php }?>
							</div>
						</div>	
						<div class="kt-separator kt-separator--border-dashed kt-separator--space-lg kt-separator--portlet-fit"></div>
						<div class="form-group row form-group-marginless">
							<label class="col-lg-1 col-form-label">Piping:</label>
							<div class="col-lg-2">
							<?php if(!empty($single_user_details['piping'])){?>
								<div class="kt-radio-inline">
									<label class="kt-radio kt-radio--solid">
										<input type="radio" name="piping" 
										<?php echo ($single_user_details['piping'] == 'Yes') ?'checked':''?>
										value="Yes"> Yes
										<span></span>
									</label>
									<label class="kt-radio kt-radio--solid">
										<input type="radio" name="piping" 
										<?php echo ($single_user_details['piping'] == 'No')?'checked':''?>
										value="No"> No
										<span></span>
									</label>
								</div>
							<?php } else {?>
								<div class="kt-radio-inline">
									<label class="kt-radio kt-radio--solid">
										<input type="radio" name="piping" value="Yes"> Yes
										<span></span>
									</label>
									<label class="kt-radio kt-radio--solid">
										<input type="radio" name="piping" value="No" checked> No
										<span></span>
									</label>
								</div>
							<?php }?>
							</div>
							<label class="col-lg-2 col-form-label">Tube Fitting & Valves:</label>
							<div class="col-lg-2">
							<?php if(!empty($single_user_details['tube_fitting_and_valves'])){?>
								<div class="kt-radio-inline">
									<label class="kt-radio kt-radio--solid">
										<input type="radio" name="tube_fitting_and_valves" 
										<?php echo ($single_user_details['tube_fitting_and_valves'] == 'Yes')?'checked':''?>
										value="Yes"> Yes
										<span></span>
									</label>
									<label class="kt-radio kt-radio--solid">
										<input type="radio" name="tube_fitting_and_valves" 
										<?php echo ($single_user_details['tube_fitting_and_valves'] == 'No')?'checked':''?>
										value="No"> No
										<span></span>
									</label>
								</div>
							<?php } else {?>
								<div class="kt-radio-inline">
									<label class="kt-radio kt-radio--solid">
										<input type="radio" name="tube_fitting_and_valves" value="Yes"> Yes
										<span></span>
									</label>
									<label class="kt-radio kt-radio--solid">
										<input type="radio" name="tube_fitting_and_valves" value="No" checked> No
										<span></span>
									</label>
								</div>
							<?php }?>
							</div>
							<label class="col-lg-2 col-form-label">Tubing:</label>
							<div class="col-lg-2">
							<?php if(!empty($single_user_details['tubing'])){?>
								<div class="kt-radio-inline">
									<label class="kt-radio kt-radio--solid">
										<input type="radio" name="tubing" 
										<?php echo ($single_user_details['tubing'] == 'Yes')?'checked':''?>
										value="Yes"> Yes
										<span></span>
									</label>
									<label class="kt-radio kt-radio--solid">
										<input type="radio" name="tubing" 
										<?php echo ($single_user_details['tubing'] == 'No')?'checked':''?>
										value="No"> No
										<span></span>
									</label>
								</div>
							<?php } else {?>
								<div class="kt-radio-inline">
									<label class="kt-radio kt-radio--solid">
										<input type="radio" name="tubing" value="Yes"> Yes
										<span></span>
									</label>
									<label class="kt-radio kt-radio--solid">
										<input type="radio" name="tubing" value="No" checked> No
										<span></span>
									</label>
								</div>
							<?php }?>
							</div>
						</div>
						<div class="kt-separator kt-separator--border-dashed kt-separator--space-lg kt-separator--portlet-fit"></div>
						<div class="form-group row">
							<label class="col-lg-1 col-form-label">Hammer:</label>
							<div class="col-lg-2">
							<?php if(!empty($single_user_details['access_type'])){?>
								<div class="kt-radio-inline">
									<label class="kt-radio kt-radio--solid">
										<input type="radio" name="hammer" 
										<?php echo ($single_user_details['hammer'] == 'Yes')?'checked':''?>
										value="Yes"> Yes
										<span></span>
									</label>
									<label class="kt-radio kt-radio--solid">
										<input type="radio" name="hammer" 
										<?php echo ($single_user_details['hammer'] == 'No')?'checked':''?>
										value="No"> No
										<span></span>
									</label>
								</div>
							<?php } else {?>
								<div class="kt-radio-inline">
									<label class="kt-radio kt-radio--solid">
										<input type="radio" name="hammer" value="Yes"> Yes
										<span></span>
									</label>
									<label class="kt-radio kt-radio--solid">
										<input type="radio" name="hammer" value="No" checked> No
										<span></span>
									</label>
								</div>
							<?php }?>
							</div>
						</div>
					</div>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<div class="row">
								<div class="col-lg-5"></div>
								<div class="col-lg-7">
									<button type="reset" class="btn btn-brand search_engine_access_update_form_button">Submit</button>
								</div>
							</div>
						</div>
					</div>
				</form>

				<!--end::Form-->
			</div>

			<!--end::Portlet-->
		</div>
	</div>
</div>

<!-- end:: Content -->
