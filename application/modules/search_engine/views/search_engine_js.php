jQuery(document).ready(function() {
	$('button.search_engine_access_update_form_button').click(function(){
		ajax_call_function({call_type: 'search_engine_access_update_form', form_data: $('form#search_engine_access_update_form').serializeArray()}, 'search_engine_access_update_form');

	});
	$('button.delete_search_engine_access').click(function(){
		swal({
	          title: "Are you sure?",
	          icon: "warning",
	          buttons:  ["Cancel", "Delete"],
	          dangerMode: true, 
	        })
	    .then((willDelete) => {
	        if (willDelete) {
	            ajax_call_function({call_type: 'delete_search_engine_access', id: $(this).attr('id')},'delete_search_engine_access');
	        } else {
	            swal({
	                title: "Access is not deleted",
	                icon: "info",
	            });
	        }
	    });
	});
	$('.kt-selectpicker').selectpicker();
});
function ajax_call_function(data, callType, url = "<?php echo base_url('search_engine/ajax_function'); ?>") {

	$.ajax({
		type: 'POST',
		data: data,
		url: url,
		dataType: 'JSON',
		success: function(res){
			if(res.status == 'successful') {
				
				switch(callType){
					case 'search_engine_access_update_form':
						swal({
			                title: "Data is updated",
			                icon: "info",
			            });			
					break;
					case 'delete_search_engine_access':
						swal({
			                title: "Access is deleted",
			                icon: "info",
			            });			
					break;
					default:
						$response['status'] = 'failed';	
						$response['message'] = "call type not found";
					break;
				}
			}
		},
		beforeSend: function(response){
		}
	});
}