<style type="text/css">
    .kt-aside--fixed .kt-wrapper {
        padding-left: 130px !important;
    }
    .kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper {
        padding-top: 60px !important;
    }
    .nav-pills .nav-item .nav-link:active, .nav-pills .nav-item .nav-link.active, .nav-pills .nav-item .nav-link.active:hover {

        border-bottom: 0.25rem solid #5d78ff!important;
        color: #1a73e8;
        opacity: 1;
        font-family: Google Sans,Helvetica Neue,sans-serif;
        font-size: 14px;
        font-weight: 600;
        background-color: #fff;
    }
</style>
<!-- end:: Header Mobile -->
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <!-- begin:: Content -->
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Data
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <?php if(!empty($user_access_details)) {?>
                            <?php $create_tab_active_1 = true; $create_tab_active_2 = true;?>
                            <ul class="nav nav-pills nav-fill" role="tablist" style="padding: 0px 400px 0px 400px;">
                                <?php if($user_access_details['piping'] == 'Yes') {?>
                                <li class="nav-item">
                                    <!-- <a class="nav-link <?php echo ($create_tab_active_1)? 'active' : '';?>" data-toggle="tab" href="#kt_tabs_5_1">Piping</a> -->
                                    <a class="nav-link <?php echo ($create_tab_active_1)? 'active' : '';?>"  target="_blank" href="<?php echo $user_access_details['piping_url'];?>">Piping</a>
                                </li>
                                <?php $create_tab_active_1 =false;}?>
                                <?php if($user_access_details['tube_fitting_and_valves'] == 'Yes') {?>
                                <li class="nav-item">
                                    <a class="nav-link <?php echo ($create_tab_active_1)? 'active' : '';?>"  target="_blank" href="<?php echo $user_access_details['tube_fitting_and_valves_url'];?>">Tube Fitting Valves</a>
                                    <!-- <a class="nav-link <?php echo ($create_tab_active_1)? 'active' : '';?>" data-toggle="tab" href="#kt_tabs_5_2">Tube Fitting Valves</a> -->
                                </li>
                                <?php $create_tab_active_1 =false;}?>
                                <?php if($user_access_details['tubing'] == 'Yes') {?>
                                <li class="nav-item">
                                    <a class="nav-link <?php echo ($create_tab_active_1)? 'active' : '';?>"  target="_blank" href="<?php echo $user_access_details['tubing_url'];?>">Tubing</a>
                                    <!-- <a class="nav-link <?php echo ($create_tab_active_1)? 'active' : '';?>" data-toggle="tab" href="#kt_tabs_5_3">Tubing</a> -->
                                </li>
                                <?php $create_tab_active_1 =false;}?>
                                <?php if($user_access_details['hammer'] == 'Yes') {?>
                                <li class="nav-item">
                                    <a class="nav-link <?php echo ($create_tab_active_1)? 'active' : '';?>"  target="_blank" href="<?php echo $user_access_details['hammer_url'];?>">Hammer Union</a>
                                    <!-- <a class="nav-link <?php echo ($create_tab_active_1)? 'active' : '';?>" data-toggle="tab" href="#kt_tabs_5_4">Hammer Union</a> -->
                                </li>
                                <?php $create_tab_active_1 =false;}?>
                                <?php if($user_access_details['invoice'] == 'Yes') {?>
                                <li class="nav-item">
                                    <a class="nav-link <?php echo ($create_tab_active_1)? 'active' : '';?>"  target="_blank" href="https://datastudio.google.com/embed/reporting/47ed5617-1bcb-43fe-ae00-93d0e964a065/page/fuVOC">Invoice</a>
                                    <!-- <a class="nav-link <?php echo ($create_tab_active_1)? 'active' : '';?>" data-toggle="tab" href="#kt_tabs_5_4">Hammer Union</a> -->
                                </li>
                                <?php $create_tab_active_1 =false;}?>
                                <?php if($user_access_details['quotation'] == 'Yes') {?>
                                <li class="nav-item">
                                    <a class="nav-link <?php echo ($create_tab_active_1)? 'active' : '';?>"  target="_blank" href="https://datastudio.google.com/embed/reporting/bd6708d9-80e9-4e44-a697-38e38f51991c/page/LfcOC">Quotation</a>
                                    <!-- <a class="nav-link <?php echo ($create_tab_active_1)? 'active' : '';?>" data-toggle="tab" href="#kt_tabs_5_4">Hammer Union</a> -->
                                </li>
                                <?php }?>
                            </ul>
                            <!-- <div class="tab-content">
                                <?php if($user_access_details['piping'] == 'Yes') {?>
                                <div class="tab-pane <?php echo ($create_tab_active_2)? 'active' : '';?>" id="kt_tabs_5_1" role="tabpanel">
                                    <iframe width="100%" height="1500" src="<?php echo $user_access_details['piping_url'];?>" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                                <?php $create_tab_active_2 =false;}?>
                                <?php if($user_access_details['tube_fitting_and_valves'] == 'Yes') {?>
                                <div class="tab-pane <?php echo ($create_tab_active_2)? 'active' : '';?>" id="kt_tabs_5_2" role="tabpanel">
                                    <iframe width="100%" height="1500" src="<?php echo $user_access_details['tube_fitting_and_valves'];?>" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                                <?php $create_tab_active_2 =false;}?>
                                <?php if($user_access_details['tubing'] == 'Yes') {?>
                                <div class="tab-pane <?php echo ($create_tab_active_2)? 'active' : '';?>" id="kt_tabs_5_3" role="tabpanel">
                                    <iframe width="100%" height="1500" src="<?php echo $user_access_details['tubing_url'];?>7F6NC" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                                <?php $create_tab_active_2 =false;}?>
                                <?php if($user_access_details['hammer'] == 'Yes') {?>
                                <div class="tab-pane <?php echo ($create_tab_active_2)? 'active' : '';?>" id="kt_tabs_5_4" role="tabpanel">
                                    <iframe width="100%" height="1500" src="<?php echo $user_access_details['hammer_url'];?>" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                                <?php }?>
                            </div> -->
                            <?php }?>
                        </div>
                    </div>

                    <!--end::Portlet-->            
                </div>

                <!-- end:: Content -->
            </div>

            <!-- begin:: Footer -->
            <div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
                <div class="kt-container  kt-container--fluid ">
                    <div class="kt-footer__copyright">
                        2020&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Keenthemes</a>
                    </div>
                    <div class="kt-footer__menu">
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">About</a>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Team</a>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Contact</a>
                    </div>
                </div>
            </div>

            <!-- end:: Footer -->
        </div>
    </div>
</div>

<!-- end:: Page -->

<!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="fa fa-arrow-up"></i>
</div>

<!-- end::Scrolltop -->