<style type="text/css">
	.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
		padding-top: 4% !important;
	}
	.layer-white{
	    display: none;
	    position: absolute;
	    top: 0em !important;
	    left: 0em !important;
	    width: 100%;
	    height: 100%;
	    text-align: center;
	    vertical-align: middle;
	    background-color: rgba(255, 255, 255, 0.55);
	    opacity: 1;
	    line-height: 1;
	    -webkit-animation-fill-mode: both;
	    animation-fill-mode: both;
	    -webkit-animation-duration: 0.5s;
	    animation-duration: 0.5s;
	    -webkit-transition: background-color 0.5s linear;
	    transition: background-color 0.5s linear;
	    -webkit-user-select: none;
	    -moz-user-select: none;
	    -ms-user-select: none;
	    user-select: none;
	    will-change: opacity;
	    z-index: 9;
	}
	.div-loader{
	    position: absolute;
		top: 50%;
		left: 50%;
		margin: 0px;
		text-align: center;
		z-index: 1000;
		-webkit-transform: translateX(-50%) translateY(-50%);
		transform: translateX(-50%) translateY(-50%);
	}
	.kt-spinner:before {
	    width: 50px;
	    height: 50px;
	    margin-top: -10px;
	}
</style>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					Search Engine
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
		            <div class="kt-portlet__head-actions">
		                <a href="<?php echo base_url('search_engine/search_engine_access_update/');?>" class="btn btn-brand btn-elevate btn-icon-sm add_invoice_search_filter_form" target="_blank">
		                    Add New User
		                </a>
		            </div>
		        </div>
	    	</div>
		</div>
		<div class="kt-portlet__body">
			<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
				<div class="row">
					<div class="col-sm-12">
						<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 100%;">
							<thead id="invoice_header">
          						<tr role="row">
									<th 
										class="sorting_search" 
										sorting_name=""
										sorting_value=""
										style="width: 10%;">
										id
									</th>
									<th 
										class="sorting_search" 
										sorting_name=""
										sorting_value=""
										style="width: 30%;">
										Username
									</th>
									<th 
										class="sorting_search" 
										sorting_name="clients.client_name"
										sorting_value=""
										style="width: 10%;">
										Piping
									</th>
									<th 
										class="sorting_search" 
										sorting_name="lookup.lookup_value"
										sorting_value=""
										style="width: 10%;">
										Tube Fitting & Valves
									</th>    
									<th 
										class="sorting_search" 
										sorting_name=""
										sorting_value=""
										style="width: 10%;">
										Tubing
									</th>
									<th 
										class="sorting_search" 
										sorting_name=""
										sorting_value=""
										style="width: 10%;">
										Hammer
									</th>
									<th 
										class="sorting_search" 
										sorting_name=""
										sorting_value=""
										style="width: 10%;">
										Access Type
									</th>
									<th class="sorting_disabled" style="width: 10%;">Actions</th>
								</tr>
							</thead>
							<tbody id="invoice_body">
							<?php foreach ($search_engine_list as $key => $single_user_details) { ?>
								<tr>  
							        <td><?php echo $key+1;?></td>
									<td><?php echo $user_list[$single_user_details['user_id']]; ?></td>
									<td><?php echo $single_user_details['piping']; ?></td>
									<td><?php echo $single_user_details['tube_fitting_and_valves']; ?></td>
									<td><?php echo $single_user_details['tubing']; ?></td>
									<td><?php echo $single_user_details['hammer']; ?></td>
									<td class="kt-font-primary kt-font-bolder kt-font-lg kt-align-center" title="Access Type" style="">
										<?php echo $single_user_details['access_type']; ?>
									</td>
									<td>
							            <a href="<?php echo base_url('search_engine/search_engine_access_update/').$single_user_details['id'];?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit" target="_blank">
							                <i class="la la-edit"></i>
							            </a>
							            <button class="btn btn-sm btn-clean btn-icon btn-icon-md delete_search_engine_access" title="Delete" id="<?php echo $single_user_details['id'];?>">
							            	<i class="la la-trash"></i>
							            </button>
									</td>  
							    </tr>
							<?php } ?>
							</tbody>
						</table>
						<div id="invoice_table_loader" class="layer-white">
							<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- end:: Content -->