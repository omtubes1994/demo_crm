<style type="text/css">
    .kt-aside--fixed .kt-wrapper {
        padding-left: 130px !important;
    }
    .kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper {
        padding-top: 60px !important;
    }
    .nav-pills .nav-item .nav-link:active, .nav-pills .nav-item .nav-link.active, .nav-pills .nav-item .nav-link.active:hover {

        border-bottom: 0.25rem solid #5d78ff!important;
        color: #1a73e8;
        opacity: 1;
        font-family: Google Sans,Helvetica Neue,sans-serif;
        font-size: 14px;
        font-weight: 600;
        background-color: #fff;
    }
</style>
<!-- end:: Header Mobile -->
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Content -->
            <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div class="kt-portlet kt-portlet--mobile">
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="kt-font-brand flaticon2-line-chart"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                HTML Table
                                <small>Datatable initialized from HTML table</small>
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">

                        <!--begin: Search Form -->
                        <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <div class="row align-items-center">
                                        <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                            <div class="kt-input-icon kt-input-icon--left">
                                                <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                                <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                                    <span><i class="la la-search"></i></span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                            <div class="kt-form__group kt-form__group--inline">
                                                <div class="kt-form__label">
                                                    <label>Status:</label>
                                                </div>
                                                <div class="kt-form__control">
                                                    <select class="form-control bootstrap-select" id="kt_form_status">
                                                        <option value="">All</option>
                                                        <option value="1">Pending</option>
                                                        <option value="2">Delivered</option>
                                                        <option value="3">Canceled</option>
                                                        <option value="4">Success</option>
                                                        <option value="5">Info</option>
                                                        <option value="6">Danger</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                            <div class="kt-form__group kt-form__group--inline">
                                                <div class="kt-form__label">
                                                    <label>Type:</label>
                                                </div>
                                                <div class="kt-form__control">
                                                    <select class="form-control bootstrap-select" id="kt_form_type">
                                                        <option value="">All</option>
                                                        <option value="1">Online</option>
                                                        <option value="2">Retail</option>
                                                        <option value="3">Direct</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 order-1 order-xl-2 kt-align-right">
                                    <a href="#" class="btn btn-default kt-hidden">
                                        <i class="la la-cart-plus"></i> New Order
                                    </a>
                                    <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-xl-none"></div>
                                </div>
                            </div>
                        </div>

                        <!--end: Search Form -->
                    </div>
                    <div class="kt-portlet__body kt-portlet__body--fit">

                        <!--begin: Datatable -->
                        <table class="kt-datatable" id="html_table" width="100%">
                            <thead>
                                <tr>
                                    <th title="Field #1">Order ID</th>
                                    <th title="Field #2">Car Make</th>
                                    <th title="Field #3">Car Model</th>
                                    <th title="Field #4">Color</th>
                                    <th title="Field #5">Deposit Paid</th>
                                    <th title="Field #6">Order Date</th>
                                    <th title="Field #7">Status</th>
                                    <th title="Field #8">Type</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>0006-3629</td>
                                    <td>Land Rover</td>
                                    <td>Range Rover</td>
                                    <td>Orange</td>
                                    <td>$22672.60</td>
                                    <td>2016-11-28</td>
                                    <td align="right">3</td>
                                    <td align="right">3</td>
                                </tr>
                                <tr>
                                    <td>50458-602</td>
                                    <td>Buick</td>
                                    <td>Skylark</td>
                                    <td>Indigo</td>
                                    <td>$85442.74</td>
                                    <td>2017-02-27</td>
                                    <td align="right">3</td>
                                    <td align="right">2</td>
                                </tr>
                                <tr>
                                    <td>51785-424</td>
                                    <td>Bentley</td>
                                    <td>Continental</td>
                                    <td>Khaki</td>
                                    <td>$35290.47</td>
                                    <td>2017-12-04</td>
                                    <td align="right">1</td>
                                    <td align="right">3</td>
                                </tr>
                                <tr>
                                    <td>55648-771</td>
                                    <td>Buick</td>
                                    <td>LeSabre</td>
                                    <td>Violet</td>
                                    <td>$56243.46</td>
                                    <td>2016-02-04</td>
                                    <td align="right">3</td>
                                    <td align="right">3</td>
                                </tr>
                                <tr>
                                    <td>0187-0063</td>
                                    <td>Mercedes-Benz</td>
                                    <td>S-Class</td>
                                    <td>Goldenrod</td>
                                    <td>$97306.72</td>
                                    <td>2017-11-06</td>
                                    <td align="right">5</td>
                                    <td align="right">3</td>
                                </tr>
                            </tbody>
                        </table>

                        <!--end: Datatable -->
                    </div>
                </div>
            </div>

        <!-- end:: Content -->
    </div>

    <!-- begin:: Footer -->
    <div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-footer__copyright">
                2020&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Keenthemes</a>
            </div>
            <div class="kt-footer__menu">
                <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">About</a>
                <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Team</a>
                <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Contact</a>
            </div>
        </div>
    </div>

    <!-- end:: Footer -->
</div>

<!-- end:: Page -->

<!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="fa fa-arrow-up"></i>
</div>

<!-- end::Scrolltop -->