<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends MX_Controller {
	function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('user_id')){
			redirect('login', 'refresh');
			exit;
		}else{
			// $access_count = 0;
			// if(!empty($this->session->userdata('main_module_access')) && in_array(4, array_column($this->session->userdata('main_module_access'), 'module_id'))) {

			// 	foreach ($this->session->userdata('sub_module_access') as $key => $value) {
					
			// 		if($value['module_id'] == 4 && $value['status'] == 'Active') {
			// 			$access_count++;
			// 		}
			// 	}
			// }
			// if($access_count == 0){
			// 	redirect($this->session->userdata('home/dashboard'));
			// 	exit;
			// }
		}
		error_reporting(0);
		$this->load->model('client_model');
		$this->load->model('quotations/quotation_model');
	}

	function index(){
		$this->client_list();
	}

	public function add_client(){

		$data = array();
		$data['customer_data'] = array('id'=>'', 'name'=>'', 'region_id'=>'', 'country_id'=>'');
		$data['company_data'] = array('product_category_id'=>'');

		$data['assigned_to'] = $this->common_model->get_dynamic_data_sales_db_null_false('user_id,name',"status=1 AND role IN (5, 16)", 'users');

		$data['region_list'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'region_mst');

		$data['country_list'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'country_mst');

		$data['product_category'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'product_category');
		
		$data['client_list'] = array();

		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Client List'));
		$this->load->view('sidebar', array('title' => 'Client List'));
		$this->load->view('client/add_client_form',$data);
		$this->load->view('footer');
	}

	public function update_client(){

		$comp_mst_id = $this->uri->segment(3, 0);
		if(!empty($comp_mst_id)){
			
			$data['customer_data'] = $this->client_model->get_all_data('*', array('id'=> $comp_mst_id), 'customer_mst','row_array');

			if(!empty($data['customer_data'])){
				
				$data['company_data'] = $this->client_model->get_all_data('*', array('customer_mst_id'=> $comp_mst_id), 'customer_data','row_array');	

				$data['company_member'] = $this->client_model->get_all_data('*', array('comp_mst_id'=> $comp_mst_id), 'customer_dtl');	
		
			}
		}else{

			redirect('client/add_client/', 'refresh');
		}

		$data['assigned_to'] = $this->common_model->get_dynamic_data_sales_db_null_false('user_id,name',"status=1 AND role IN (5, 16)", 'users');

		$data['region_list'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'region_mst');

		$data['country_list'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'country_mst');

		$data['product_category'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'product_category');	

		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Update Client'));
		$this->load->view('sidebar', array('title' => 'Update Client'));
		$this->load->view('client/update_client_form',$data);
		$this->load->view('footer');
	}

	public function ajax_function() {

		if($this->input->is_ajax_request()){
			$post_details = $this->input->post();
			$call_type = $this->input->post('call_type');
			$response['status'] = 'successful';	
			switch ($call_type) {
				
				case 'get_client_details':
					
					$client_details = $this->client_model->get_client_name($this->input->post('search_data'));

					$response['client_option_tag'] = '<option value="">Select</option>';
					foreach ($client_details as $single_client_details) {
						
						$response['client_option_tag'] .= '<option value="'.$single_client_details['id'].'">'.$single_client_details['name'].'</option>';
					}
				break;

				case 'add_client_form':

					$response['message'] = "client Data are Added";	

					$insert_array = array();
					$insert_array = array_column($this->input->post('client_mst_details'), 'value', 'name');
					$insert_data_array = array_column($this->input->post('client_data_details'), 'value', 'name');
					
					if(empty($this->input->post('comp_mst_id'))){
						
						$insert_array['entered_on'] = date('Y-m-d H:i:s');
						$insert_array['entered_by'] = $this->session->userdata('user_id');

						$comp_mst_id = $this->common_model->insert_data_sales_db('customer_mst', $insert_array);
						$insert_data_array['customer_mst_id'] = $comp_mst_id;
						$this->common_model->insert_data_sales_db('customer_data', $insert_data_array);
					}
				break;

				case 'update_client_form':

					// echo "<pre>";print_r($this->input->post('comp_mst_id'));echo"</pre><hr>";die;
					$response['message'] = "client Data are Uppdate";	

					$update_array = array();
					$update_array = array_column($this->input->post('client_mst_details'), 'value', 'name');
					$update_data_array = array_column($this->input->post('client_data_details'), 'value', 'name');
					
					if(!empty($this->input->post('comp_mst_id'))){
						$update_array['modified_on'] = date('Y-m-d H:i:s');
						$update_array['modified_by'] = $this->session->userdata('user_id');

						$this->common_model->update_data_sales_db('customer_mst', $update_array, array('id' =>$this->input->post('comp_mst_id')));
						$this->common_model->update_data_sales_db('customer_data', $update_data_array, array('customer_mst_id' =>$this->input->post('comp_mst_id')));
					}
				break;

				case 'get_member_model_body':

					$response['add_member_html'] = "";
					$response['comp_mst_id'] = $this->input->post('comp_mst_id');
					if(!empty($this->input->post('comp_mst_id'))){
							
						$data['comp_mst_id'] = $this->input->post('comp_mst_id');
						$response['add_member_html'] = $this->load->view('client/add_member_form', $data, true);
					}
				break;

				case 'add_member_form':

					$response['message'] = "Member are Added";

					$insert_array = array();
					$insert_array = array_column($this->input->post('add_member_form'), 'value', 'name');
					if(empty($this->input->post('comp_dtl_id'))){
						
						$insert_array['entered_on'] = date('Y-m-d H:i:s');
						$insert_array['main_buyer'] = "Yes";
						$insert_array['other_member'] = "No";
						$this->common_model->insert_data_sales_db('customer_dtl', $insert_array);
					}
				break;

				default:
					$response['status'] = 'failed';	
					$response['message'] = "call type not found";
				break;
			}
			echo json_encode($response);
		} else{
			die('access is not allowed to this function');
		}
	}

	public function addClients($client_id=0){
		if(!empty($this->input->post())){			
			
			$client_data = array(
				'name' => $this->input->post('name'),
				'country_id' => $this->input->post('country_id'),
				'region_id' => $this->input->post('region_id'),
				'website' => $this->input->post('website'),
				'modified_on' => date('Y-m-d H:i:s'),
				'modified_by' => $this->session->userdata('user_id'),
				'status' => 'Active',	
			);
			if($this->input->post('id')){
				
				$client_id = $this->input->post('id');
				$this->client_model->updateClient($client_data, array('id' => $client_id));
				$this->session->set_flashdata("success","Client updated successfully!!");
			}else{
				$client_data['entered_on'] = date('Y-m-d H:i:s');
				$client_data['entered_by'] = $this->session->userdata('user_id');
				// echo "<pre>";print_r($client_data);echo"</pre><hr>";die;
				$client_id = $this->client_model->addClient($client_data);
				$this->session->set_flashdata("success","Client added successfully!!");
			}

			if($this->input->post('name')){
				foreach ($this->input->post('name') as $key => $value) {
					$member_data = array(
						'comp_mst_id' => $client_id,
						'name' => $value,
						'email' => $this->input->post('email')[$key],
						'mobile' => $this->input->post('mobile')[$key],
						'is_whatsapp' => $this->input->post('is_whatsapp')[$key],
						'skype' => $this->input->post('skype')[$key],
						'telephone' => $this->input->post('telephone')[$key],
						'status' => 'Active',
						'modified_on' => date('Y-m-d H:i:s'),
						// 'modified_by' => $this->session->userdata('user_id'),
					);

					if(isset($this->input->post('member_id')[$key])){
						$member_id = $this->input->post('member_id')[$key];
						$this->client_model->updateMember($member_data, array('member_id' => $member_id));
					}else{
						$member_data['entered_on'] = date('Y-m-d H:i:s');
						$member_data['entered_by'] = $this->session->userdata('user_id');
						$member_id = $this->client_model->addClientMember($member_data);
					}
				}
			}
			redirect('client/client_list/');
		}else{
			if($client_id > 0){
				$data['client_details'] = $this->client_model->getClientDetails($client_id);
				$data['client_members'] = $this->client_model->getClientMembers($client_id);
				$data['client_id'] = $client_id;
				// echo "<pre>";print_r($data);echo"</pre><hr>";die;
			}
			$data['region'] = $this->client_model->get_all_data('*', array('status'=>'Active'), 'region_mst');
			$data['country'] = $this->client_model->get_all_data('*', array('status'=>'Active'), 'country_mst');
			// echo "<pre>";print_r($data);echo"</pre><hr>";die;
			$this->load->view('header', array('title' => 'Add Client'));
			$this->load->view('sidebar', array('title' => 'Add Client'));
			$this->load->view('add_client',$data);
			$this->load->view('footer');
		}
	}

	public function addMembers(){
		if(!empty($this->input->post())){

		}else{
			// $data['users'] = $this->quotation_model->getUsers();
			// $data['clients'] = $this->client_model->getClients();
			// $data['region'] = $this->quotation_model->getLookup(1);
			// $data['country'] = $this->quotation_model->getLookup(2);
			$this->load->view('header', array('title' => 'Add Member'));
			$this->load->view('sidebar', array('title' => 'Add Member'));
			$this->load->view('add_member');
			$this->load->view('footer');
		}
	}

	function addClientAjax($call=''){
		$client_data = array(
			"name" => $this->input->post('name'),
			"country_id" => $this->input->post('country_id'),
			"region_id" => $this->input->post('region_id'),
			"website" => $this->input->post('website'),
			"entered_on" => date('Y-m-d H:i:s'),
			"entered_by" => $this->session->userdata('user_id'),
			"status" => 'Active'
			// 'origin' => 'client'
		);

		$comp_mst_id = $this->client_model->addClient($client_data);
		// echo "<pre>";print_r($earch);echo"</pre><hr>";die;
		if($call==''){
			$customer = $this->client_model->getClients();
			echo json_encode(array("comp_mst_id" => $comp_mst_id, "customer" => $customer));
		}
		else{
			$this->session->set_flashdata("success","Client added successfully!!");
			redirect('client/addClients');
		}
	}

	function addClientMemberAjax(){
		$member_data = array(
			"comp_mst_id" => $this->input->post('id'),
			"member_name" => $this->input->post('member_name'),
			"email" => $this->input->post('email'),
			"mobile" => $this->input->post('mobile'),
			"is_whatsapp" => $this->input->post('is_whatsapp'),
			"skype" => $this->input->post('skype'),
			"telephone" => $this->input->post('telephone'),
			"entered_on" => date('Y-m-d H:i:s'),
			// "main_buyer" => $this->input->post('main_buyer'),
			"other_member" => 'Yes',
			"status" => 'Active'
		);

		$comp_dtl_id = $this->client_model->addClientMember($member_data);
		$customer_member = $this->client_model->getMembers($this->input->post('id'));
		echo json_encode(array("comp_dtl_id" => $comp_dtl_id, "customer_member" => $customer_member));
	}

	function client_list(){
		if($this->session->userdata('role') != 1){
			redirect('client/addClients');
			exit;
		}
		$this->load->view('header', array('title' => 'Client List'));
		$this->load->view('sidebar', array('title' => 'Client List'));
		$this->load->view('client_list_view');
		$this->load->view('footer');
	}

	function client_data(){
		
		// echo "<pre>";print_r($this->input->get());echo"</pre><hr>";die;		
		$order_by = $this->input->get('columns')[$this->input->get('order')[0]['column']]['data'];
		
		if($order_by == 'record_id' || $order_by == 'company_name'){
			$order_by = 'c.name';
		}else if($order_by == 'country'){
			$order_by = 'l1.name';
		}else if($order_by == 'region'){
			$order_by = 'l2.name';
		}
		$dir = $this->input->get('order')[0]['dir'];
		$records = $this->client_model->getClientList($this->input->get('start'), $this->input->get('length'), $this->input->get('search')['value'], $order_by, $dir);
		// echo "<pre>";print_r($dir);echo"</pre><hr>";die;
		$data['iTotalDisplayRecords'] = $data['iTotalRecords'] = $this->client_model->getClientListCount($this->input->get('search')['value']);
		$data['aaData'] = $records;
		// echo "<pre>";print_r($data);echo"</pre><hr>";die;
		echo json_encode($data);
	}

	function member_list(){
		if($this->session->userdata('role') != 1){
			redirect('client/addClients');
			exit;
		}
		$this->load->view('header', array('title' => 'Member List'));
		$this->load->view('sidebar', array('title' => 'Member List'));
		$this->load->view('member_list_view');
		$this->load->view('footer');
	}

	function member_data(){

		$order_by = $this->input->get('columns')[$this->input->get('order')[0]['column']]['data'];
		if($order_by == 'record_id' || $order_by == 'company_name'){
			$order_by = 'c.name';
		}else if($order_by == 'email'){
			$order_by = 'm.email';
		}else if($order_by == 'mobile'){
			$order_by = 'm.mobile';
		}else if($order_by == 'telepone'){
			$order_by = 'm.telepone';
		}else if($order_by == 'member_name'){
			$order_by = 'm.member_name';
		}
		$dir = $this->input->get('order')[0]['dir'];
		$records = $this->client_model->getMemberList($this->input->get('start'), $this->input->get('length'), $this->input->get('search')['value'], $order_by, $dir);
		$data['iTotalDisplayRecords'] = $data['iTotalRecords'] = $this->client_model->getMemberListCount($this->input->get('search')['value']);
		$data['aaData'] = $records;
		echo json_encode($data);
	}

	function deleteClient(){
		$this->client_model->deleteClient($this->input->post('comp_mst_id'));
		$this->session->set_flashdata("success","Client deleted successfully!!");
	}

	function deleteMember(){
		$this->client_model->deleteMember($this->input->post('comp_dtl_id'));
		$this->session->set_flashdata("success","Member deleted successfully!!");
	}

	function getClientMembers(){
		$customer_member = $this->client_model->getMembers($this->input->post('client_id'));
		echo json_encode(array('customer_member' => $customer_member));
	}

	function clientListJson(){
		if($this->session->userdata('role') == 5){
			$clients = $this->client_model->getClients();
			$region = $this->quotation_model->getLookup(1);
			$country = $this->quotation_model->getLookup(2);
			$connect_list = $this->getConnectList(); //date("Y-m-d", strtotime('monday this week'))
			
			$counts = $this->client_model->getPerformanceCount($this->session->userdata('user_id'));
			$daywise = $this->client_model->getDayWiseCount();
			$reasons = $this->client_model->getDayWiseReason();
			$array = array();
		    $Date2 = date('Y-m-d'); 
			$date = new DateTime($Date2);
			$date->modify('-29 day');
			$Date1 = $date->format('Y-m-d');
			$array = array(); 
			$Variable1 = strtotime($Date1); 
			$Variable2 = strtotime($Date2);
			$i=1;
			for ($currentDate = $Variable1; $currentDate <= $Variable2; $currentDate += (86400)) { 
				$Store = date('Y-m-d', $currentDate); 
				//$array[] = $Store;
				$array[$i]['key'] = date('d M', strtotime($Store));
				$array[$i]['value'] = 0.5;
				$array[$i]['reason'] = '';
				foreach ($daywise as $dw) {
					if($dw['contacted_on'] == $Store){
						$array[$i]['value'] = $dw['count'];
						break;
					}
				}

				foreach ($reasons as $res) {
					if($res['contacted_on'] == $Store){
						$array[$i]['reason'] = $res['comments'];
						break;
					}
				}
				$i++;
			}

			$dates = $this->client_model->getEntryDates($this->session->userdata('user_id'));
			$Variable1 = strtotime('2020-04-01'); 
			$Variable2 = strtotime(date('Y-m-d'));
			$no_entry = array();
			for ($currentDate = $Variable1; $currentDate <= $Variable2; $currentDate += (86400)) {
				$cur_date = date('Y-m-d', $currentDate);
				if(!in_array($cur_date, $dates)){
					$no_entry[] = $cur_date;
				}
			}

			echo json_encode(array('clients' => $clients, 'connect_list' => $connect_list, 'region' => $region, 'country' => $country, 'monthly_count' => $array, 'counts' => $counts, 'no_entry' => $no_entry));
		}
	}
	
	function clientConnect(){
		if(!empty($this->input->post()) && $this->session->userdata('role') == 5){
			$arr = array(
				'user_id' => $this->session->userdata('user_id'),
				'client_id' => $this->input->post('qc_client'),
				'member_id' => $this->input->post('qc_member'),
				'contact_mode' => $this->input->post('qc_contact_via'),
				'email_sent' => $this->input->post('qc_email_sent'),
				'comments' => $this->input->post('qc_comments'),
				'contacted_on' => date('Y-m-d', strtotime($this->input->post('qc_contacted_on')))
			);
			if($this->input->post('qc_connect_id') > 0){
				$id = $this->input->post('qc_connect_id');
				$this->client_model->updateData('client_connect', $arr, array('connect_id' => $id));
			}else{
				$id = $this->client_model->insertData('client_connect', $arr);
			}
			if($id > 0){
				$connect_list = $this->getConnectList(); //date("Y-m-d", strtotime('monday this week'))
				echo json_encode(array('status' => 'cc_success', 'connect_list' => $connect_list));
			}else{
				echo json_encode(array('status' => 'cc_failed'));
			}
		}
	}

	function getConnectList($start = '', $end = ''){
		if($this->session->userdata('role') == 5){
			$client_data = $this->client_model->getConnectList($start, $end);
			return $client_data;
		}
	}

	function getConnectDetails(){
		if($this->session->userdata('role') == 5){
			$connectDetails = $this->client_model->getConnectDetails($this->input->post('connect_id'));
			echo json_encode($connectDetails);
		}
	}

	function deleteConnect(){
		if($this->session->userdata('role') == 5){
			$this->client_model->deleteConnect($this->input->post('connect_id'));
			$connect_list = $this->getConnectList(); //date("Y-m-d", strtotime('monday this week'))
			echo json_encode(array('status' => 'cc_success', 'connect_list' => $connect_list));
		}
	}

	function updateNoReason(){
		if($this->session->userdata('role') == 5){
			$array = array(
				'user_id' => $this->session->userdata('user_id'),
				'client_id' => 0,
				'member_id' => 0,
				'contact_mode' => 'No Touch Point',
				'comments' => $this->input->post('qc_no_reason'),
				'contacted_on' => date('Y-m-d', strtotime($this->input->post('qc_no_date'))),
				'entered_on' => date('Y-m-d H:i:s')
			);
			$res = $this->client_model->insertData('client_connect', $array);
			
			$dates = $this->client_model->getEntryDates($this->session->userdata('user_id'));
			$Variable1 = strtotime('2020-04-01'); 
			$Variable2 = strtotime(date('Y-m-d'));
			$no_entry = array();
			for ($currentDate = $Variable1; $currentDate <= $Variable2; $currentDate += (86400)) {
				$cur_date = date('Y-m-d', $currentDate);
				if(!in_array($cur_date, $dates)){
					$no_entry[] = $cur_date;
				}
			}

			echo json_encode(array('no_entry' => $no_entry));
		}
	}

	function touchpoint_list(){
		if($this->session->userdata('role') != 1){
			redirect('client/addClients');
			exit;
		}
		$data['sales_person'] = $this->client_model->getSalesPerson();
		$this->load->view('header', array('title' => 'Touch Points List'));
		$this->load->view('sidebar', array('title' => 'Touch Points List'));
		$this->load->view('tpoints_list_view', $data);
		$this->load->view('footer');
	}

	function touchpoint_list_data(){
		$order_by = $this->input->get('columns')[$this->input->get('order')[0]['column']]['data'];
		if($order_by == 'record_id' || $order_by == 'username'){
			$order_by = 'u.name';
		}
		if($order_by == 'member_name'){
			$order_by = 'm.name';
		}
		$searchBySalesPerson = $this->input->get('searchBySalesPerson');
		$dir = $this->input->get('order')[0]['dir'];
		$records = $this->client_model->getPointList($this->input->get('start'), $this->input->get('length'), $this->input->get('search')['value'], $order_by, $dir, $searchBySalesPerson);
		$data['iTotalDisplayRecords'] = $data['iTotalRecords'] = $this->client_model->getPointListCount($this->input->get('search')['value'], $searchBySalesPerson);
		$data['aaData'] = $records;
		echo json_encode($data);
	}

	function getTPPerformance(){
		$user_id = $this->input->post('user_id');
		$counts = $this->client_model->getPerformanceCount($user_id);
		$daywise = $this->client_model->getDayWiseCount($user_id);
		$reasons = $this->client_model->getDayWiseReason($user_id);
		$Date2 = date('Y-m-d'); 
		$date = new DateTime($Date2);
		$date->modify('-29 day');
		$Date1 = $date->format('Y-m-d');
		$array = array(); 
		$Variable1 = strtotime($Date1); 
		$Variable2 = strtotime($Date2);

		if(isset($_POST['start_date'])){
			$Variable1 = strtotime($this->input->post('start_date'));
			$date = new DateTime(date('Y-m-d', $Variable1));
			$date->modify('+29 day');
			$Date2 = $date->format('Y-m-d');
			$Variable2 = strtotime($Date2);
		}

		$i=1;
		for ($currentDate = $Variable1; $currentDate <= $Variable2; $currentDate += (86400)) { 
			$Store = date('Y-m-d', $currentDate); 
			//$array[] = $Store;
			$array[$i]['key'] = date('d M', strtotime($Store));
			$array[$i]['value'] = 0.5;
			$array[$i]['reason'] = '';
			foreach ($daywise as $dw) {
				if($dw['contacted_on'] == $Store){
					$array[$i]['value'] = $dw['count'];
					break;
				}
			}

			foreach ($reasons as $res) {
				if($res['contacted_on'] == $Store){
					$array[$i]['reason'] = $res['comments'];
					break;
				}
			}
			$i++;
		}
		echo json_encode(array('monthly_count' => $array, 'counts' => $counts));
	}

	function getTouchPointCounts(){
		$res = $this->client_model->getTouchPointCounts();
		$ret_arr = array(
			'call' => 0,
			'whatsapp' => 0,
			'linkedIn' => 0
		);

		foreach ($res as $key => $value) {
			if($value['contact_mode'] == 'call'){
				$ret_arr['call'] = $value['count'];
			}

			else if($value['contact_mode'] == 'whatsapp'){
				$ret_arr['whatsapp'] = $value['count'];
			}

			else if($value['contact_mode'] == 'linkedIn'){
				$ret_arr['linkedIn'] = $value['count'];
			}
		}

		echo json_encode($ret_arr);
	}


	function searchClients(){

		$search = $this->input->post('search');
		$leads = $this->client_model->searchClients($search);
		// echo "<pre>";print_r($leads);echo"</pre><hr>";die;
		echo json_encode($leads);
	}

	
}