<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12 col-xl-12 order-lg-1 order-xl-1">
                <div class="kt-portlet">

                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="kt-font-brand flaticon2-line-chart"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                Update Client
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-wrapper">
                                <div class="kt-portlet__head-actions">
                                    <button type="submit" class="btn btn-primary update_client_form" id="comp_mst_id" comp_mst_id="<?php echo $customer_data['id'];?>">Update Clients</button>
                                </div>
                            </div>
                        </div>          
                    </div>
                    <div class="kt-portlet__body">
                        <form class="kt-form" id="update_client_form">
                            <div class="form-group row">
								<div class="col-lg-3 form-group-sub">
								    <label class="form-control-label">Client Name</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="client_name" name="name" value="<?php echo $customer_data['name'];?>">
                                    </div>
                                    <small class="form-text text-danger" id="update_client_error"></small>                                   
								</div>
                                <div class="col-lg-3 form-group-sub">
                                    <label class="form-control-label">Country</label>
                                    <select class="form-control client_select_picker" data-live-search="true" id="country_id" name="country_id">
                                        <option value="">Select</option>
                                        <?php foreach ($country_list as $single_country) { ?>
                                            <option value="<?php echo $single_country['id'];?>"
                                                    region_id="<?php echo $single_country['region_id'];?>"
                                            <?php 
                                                echo ($customer_data['country_id'] == $single_country['id']) ? 'selected': ''; ?>>
                                            <?php echo $single_country['name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                    <small class="form-text text-danger" id="update_country_error"></small> 
								</div>
                                <div class="col-lg-3 form-group-sub">
                                    <label class="form-control-label">Region</label>
                                    <select class="form-control client_select_picker" data-live-search="true" id="region_id" name="region_id">
                                        <?php foreach ($region_list as $single_region) { ?>
                                            <option value="<?php echo $single_region['id'];?>">
                                                <?php echo $single_region['name'];?>
                                            </option>
                                        <?php } ?>
								    </select>
								</div>
                                <div class="col-lg-3 form-group-sub">
								    <label class="form-control-label">Assign To:</label>
									<select class="form-control client_select_picker" data-live-search="true" name="assigned_to">
										<option value="">Select</option>
                                        <?php foreach ($assigned_to as $single_person) { ?>
                                            <option value="<?php echo $single_person['user_id']; ?>"
                                                <?php 
                                                echo ($customer_data['assigned_to'] == $single_person['user_id']) ? 'selected': '';
                                                ?>>
                                                <?php echo $single_person['name']; ?>
                                            </option>;
                                        <?php } ?>
                                    </select>
								</div>                              
                            </div>
                        </form>

                        <form class="kt-form" id="update_client_data_form">
                            <div class="form-group row"> 
                                <div class="col-lg-3 form-group-sub">
                                    <label class="form-control-label">Client Category</label>
                                    <select class="form-control client_select_picker" data-live-search="true" id="product_category_id"  name="product_category_id">
                                        <option value="">Select</option>
                                        <?php foreach ($product_category as $single_product_category) { ?>
                                            <option value="<?php echo $single_product_category['id']; ?>" 
                                                <?php 
                                                    echo ($company_data['product_category_id'] == $single_product_category['id']) ? 'selected': ''; ?>>
                                                <?php echo $single_product_category['product_name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                    <small class="form-text text-danger" id="update_category_error"></small> 
                                </div>
                            </div>
                        </form>

                        <form id="add_member_form">
                            <div class="form-group row">
                                <div class="col-lg-12" style="padding: 0px 0px 0px 0px !important;">
                                    <table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info">
                                        <thead>
                                            <tr role="row">
                                                <th colspan="3" style="text-align: Center;">Member List</th>
                                                <th style="text-align: center;">
                                                    <button type="button" class="btn btn-sm btn-primary pull-right add_member" id="add_member" data-toggle="modal" data-target="#member_model" comp_mst_id="<?php echo $customer_data['id'];?>">Add Member</button>
                                                </th>     
                                            </tr>       
                                            <tr role="row">
                                                <th>Sr</th>
                                                <th>Name</th>
												<th>Designation</th>
												<th>Email</th>
                                            </tr>
                                        </thead>
                                        <tbody id="member_body">
                                            <?php if(!empty($company_member)){ ?>
                                                <?php foreach ($company_member as $company_member_key => $single_member) { ?>
                                                <tr>
                                                    <td><?php echo $company_member_key+1;?>
                                                        <?php $single_member['comp_dtl_id']; ?>
                                                    </td>
                                                    <td><?php echo $single_member['member_name']; ?></td>
                                                    <td><?php echo $single_member['designation']; ?></td>
                                                    <td><?php echo $single_member['email']; ?></td>
                                                </tr>
                                                <?php } ?>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                    
                    </div> 
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="member_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      	<div class="modal-dialog modal-xl" role="document">
         	<div class="modal-content">
	            <div class="modal-header">
	               <h5 class="modal-title" id="exampleModalLabel">Add New Member</h5>
	               <button type="reset" class="close" data-dismiss="modal" aria-label="Close">
	               </button>
	            </div>
	            <div class="modal-body">
		            <form id="client_member_form">	            	

						
		            </form>
	            </div>
	            <div class="modal-footer">
	               	<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
	            </div>
        	</div>
    	</div>
   	</div>

</div>
<!-- end:: Content -->