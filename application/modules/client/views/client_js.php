jQuery(document).ready(function () {

    Client_Js.init();


    $('form#add_client_form').on('change', 'select.country_id', function () {

        $('div.selected_region').hide();
        var val = $(this).val();
        if (val != '') {
            $('div.selected_region').show();
            $("#region_id").val($("#country_id option:selected").attr('region_id'));
        }
    });

    $('div.client_name_search').click(function () {

        var search_value = $("input#client_search_value").val();
        toastr.success(search_value);
        ajax_call_function({

            call_type: 'get_client_details',
            search_data: search_value
        }, 'get_client_details');
    });

    $('div.client_name').on('change', 'select.client_details', function () {
        
        window.location.href = '<?php echo base_url("client/update_client/"); ?>' + $("select#name").val();
    });

    $('button.save_client_form').click(function () {

        var client_name = $('#client_search_value').val();
        var country_id = $('#country_id').val();
        var category_id = $('#product_category_id').val();

        var status = false;
        if (client_name == "") {

            $("#client_error").html("<span class='text-denger'>*Please Enter Customer Name*</span>");
            status = false;
        } else {
            $("#client_error").html("");
            status = true;
        }

        var status_1 = false;
        if (country_id == "") {

            $("#country_error").html("<span class='text-denger'>*Please Select Country*</span>");
            status_1 = false;
        } else {
            $("#country_error").html("");
            status_1 = true;
        }

        var status_2 = false;
        if (category_id == "") {

            $("#category_error").html("<span class='text-denger'>*Please Select Product Category*</span>");
            status_2 = false;
        } else {
            $("#category_error").html("");
            status_2 = true;
        }

        if ((status == true) && (status_1 == true) && (status_2 == true)) {
            ajax_call_function({
                call_type: 'add_client_form',
                comp_mst_id: $(this).val(),
                client_mst_details: $('form#add_client_form').serializeArray(),
                client_data_details: $('form#add_client_data_form').serializeArray(),
            }, 'add_client_form');
        }
    });
    
    $('button.update_client_form').click(function () {

        var client_name = $('#client_name').val();
        var country_id = $('#country_id').val();
        var category_id = $('#product_category_id').val();

        var status = false;
        if (client_name == "") {

            $("#update_client_error").html("<span class='text-denger'>*Please Enter Customer Name*</span>");
            status = false;
        } else {
            $("#update_client_error").html("");
            status = true;
        }

        var status_1 = false;
        if(country_id == "") {

            $("#update_country_error").html("<span class='text-denger'>*Please Select Country*</span>");
            status_1 = false;
        } else {
            $("#update_country_error").html("");
            status_1 = true;
        }

        var status_2 = false;
        if (category_id == "") {

            $("#update_category_error").html("<span class='text-denger'>*Please Select Product Category*</span>");
            status_2 = false;
        } else {
            $("#update_category_error").html("");
            status_2 = true;
        }

        if ((status == true) && (status_1 == true) && (status_1 == true)) {
            ajax_call_function({
                call_type: 'update_client_form',
                comp_mst_id: $(this).attr('comp_mst_id'),
                client_mst_details: $('form#update_client_form').serializeArray(),
                client_data_details: $('form#update_client_data_form').serializeArray(),
            }, 'update_client_form');
        }
    });

    $('button.add_member').click(function () {

        var comp_mst_id = $(this).attr('comp_mst_id');
        ajax_call_function({
            call_type: 'get_member_model_body',
            comp_mst_id: comp_mst_id,
        }, 'get_member_model_body');
    });

    $('div#member_model').on('click', 'button.save_member', function () {

        var member_name = $('#member_name').val();
        var status = false;
        if (member_name == "") {

            $("#member_error").html("<span class='text-denger'>*Please Enter Member Name*</span>");
            status = false;
        } else {
            $("#member_error").html("");
            status = true;
        }

        if (status == true) {
            ajax_call_function({
                call_type: 'add_member_form',
                comp_dtl_id: $(this).val(),
                add_member_form: $('form#client_member_form').serializeArray()
            }, 'add_member_form');
        }
    });
});


function ajax_call_function(data, call_type, url = "<?php echo base_url('client/ajax_function'); ?>") {
    $.ajax({
        type: 'POST',
        data: data,
        url: url,
        dataType: 'JSON',
        success: function (res) {
            toastr.clear();
            if (res.status == 'successful') {
                switch (call_type) {

                    case 'get_client_details':
                        // console.log(res.client_option_tag);
                        $('select.client_details').html('').html(res.client_option_tag);
                        Client_Js.init();
                    break;

                    case 'get_member_model_body':
                        $('form#client_member_form').html('').html(res.add_member_html);
                        Client_Js.init();
                    break;

                    case 'add_client_form':
                        swal({
                            title: "New Client Added",
                            icon: "success",
                        });
                        setTimeout(function () {

                            location.reload();
                        }, 1500);
                    break;

                    case 'update_client_form':
                        swal({
                            title: "Client Update",
                            icon: "success",
                        });
                        setTimeout(function () {

                            location.reload();
                        }, 1500);
                    break;

                    case 'add_member_form':
                        swal({
                            title: "New Member Added",
                            icon: "success",
                        });
                        setTimeout(function () {

                            location.reload();
                        }, 1500);
                    break;
                }
            }
        },
        beforeSend: function (response) {

        }
    })
}

var Client_Js = function () {

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }

    // Private functions
    var datepicker = function () {
        // enable clear button 
        $('.client_date_picker').datepicker({
            rtl: KTUtil.isRTL(),
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: true,
            templates: arrows,
            format: 'yyyy-mm-dd',
        });
    };

    return {
        // public functions
        init: function () {
            datepicker();
            $('.client_select_picker').selectpicker();
            $('.client_search_filter_form').selectpicker();
        }
    };
}();

toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};
"use strict";
// Class definition