<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12 col-xl-12 order-lg-1 order-xl-1">
                <div class="kt-portlet">

                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="kt-font-brand flaticon2-line-chart"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                NEW Client
                            </h3>
                        </div>               
                    </div>
                    <div class="kt-portlet__body">
                        <form class="kt-form" id="add_client_form">
                            <div class="form-group row">
								
                                <div class="col-lg-3">
									<label class="">Company</label>
									<div class="input-group client_name">
										<div class="input-group">
											<input type="text" id="client_search_value" class="form-control validate[required]"  placeholder="Enter Client Name" name="name" value="<?php echo $customer_data['name'];?>">
											<div class="input-group-prepend client_name_search">
												<span class="input-group-text">
													<i class="la la-search"></i>
												</span>
											</div>
										</div>
										<select class="form-control kt-selectpicker client_details" data-size="7" data-live-search="true" tabindex="-98" id="name">
											<option value="">Select</option>
											<?php if(!empty($client_list)){
												foreach ($client_list as $key => $value) { ?>

                                                    <option value="<?php echo $value['id']; ?>"
                                                        <?php 
                                                        echo ($customer_data['id'] == $value['id']) ? 'selected': '';
                                                        ?>>
                                                        <?php echo $value['name']; ?>
                                                    </option>;
                                                    <?php }
											} ?>
										</select>                                        
									</div>
                                    <small class="form-text text-danger" id="client_error"></small>
								</div>
                                <div class="col-lg-3 form-group-sub">
								    <label class="form-control-label">Assign To:</label>
									<select class="form-control client_select_picker" name="assigned_to">
										<option value="125">Unassigned sales</option>
                                        <?php foreach ($assigned_to as $single_person) { ?>
                                            <option value="<?php echo $single_person['user_id']; ?>"
                                                <?php 
                                                echo ($customer_data['assigned_to'] == $single_person['user_id']) ? 'selected': '';
                                                ?>>
                                                <?php echo $single_person['name']; ?>
                                            </option>;
                                        <?php } ?>
                                    </select>
								</div>
                                <div class="col-lg-3 form-group-sub">
                                    <label class="form-control-label">Country</label>
                                    <select class="form-control client_select_picker country_id" data-live-search="true" id="country_id" name="country_id">
                                        <option value="">Select</option>
                                        <?php foreach ($country_list as $single_country) { ?>
                                            <option value="<?php echo $single_country['id'];?>"
                                                    region_id="<?php echo $single_country['region_id'];?>"
                                            <?php 
                                                echo ($customer_data['country_id'] == $single_country['id']) ? 'selected': ''; ?>>
                                            <?php echo $single_country['name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                    <small class="form-text text-danger" id="country_error"></small> 
								</div>
                                <div class="col-lg-3 form-group-sub  selected_region" style="display:<?php echo ($customer_data['country_id'] == 'selected') ? 'block' : 'none';?>">
                                    <label class="form-control-label">Region</label>
                                    <select class="form-control" id="region_id" name="region_id">
                                        <?php foreach ($region_list as $single_region) { ?>
                                            <option value="<?php echo $single_region['id'];?>">
                                                <?php echo $single_region['name'];?>
                                            </option>
                                        <?php } ?>
								    </select>
								</div>                       
                            </div>
                        </form>

                        <form class="kt-form" id="add_client_data_form">
                            <div class="form-group row"> 
                                <div class="col-lg-3 form-group-sub">
                                    <label class="form-control-label">Client Category</label>
                                    <select class="form-control client_select_picker" id="product_category_id"  name="product_category_id">
                                        <option value="17">Miscellaneous Leads</option>
                                        <!-- <option value="">select</option> -->
                                        <!-- <?php foreach ($product_category as $single_product_category) { ?>
                                            <option value="<?php echo $single_product_category['id']; ?>" 
                                                <?php 
                                                    echo ($company_data['product_category_id'] == $single_product_category['id']) ? 'selected': ''; ?>>
                                                <?php echo $single_product_category['product_name']; ?>
                                            </option>
                                        <?php } ?> -->
                                    </select>
                                    <small class="form-text text-danger" id="category_error"></small> 
                                </div>
                            </div>
                        </form>
                    </div> 
                    
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-12" style="text-align:center;">
                                <button type="submit" class="btn btn-primary save_client_form" id="comp_mst_id" >Save Clients</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- end:: Content -->