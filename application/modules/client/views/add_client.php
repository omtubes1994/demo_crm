<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
			<div class="col-lg-12 col-xl-12 order-lg-1 order-xl-1">
				<div class="kt-portlet">
					<!-- form -->
				<?php echo form_open('', array('id' => 'addClient', 'class' => 'kt-form kt-form--label-right', 'autocomplete' => 'off'));?>
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Add Client</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						</button>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-6 col-sm-12">
								<div class="form-group">
									<label for="recipient-name" class="form-control-label">Company Name:</label>
									<input type="text" class="form-control validate[required] name" id="name" name="name" value=
									"<?php if(isset($client_details)) echo $client_details['name']; ?>" autocomplete="off">
								</div>
								<div class="row">
									<div class="col-12">
										<div id="company_result_client" style="background-color: #fff; z-index: 100; position: absolute; border: 1px solid; width: 80%; max-height: 100px; height: 100px; overflow-y: scroll; display: none; margin-top: -25px;">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-12">
								<div class="form-group">
									<label for="message-text" class="form-control-label">Country:</label>
									<select class="form-control validate[required]" id="country_id" name="country_id">
										<option value="" disbaled>Select</option>
										<?php 
											foreach ($country as $key => $value) {
												$selected = '';
												if(isset($client_details) && $client_details['country_id'] == $value['id']){
													$selected = 'selected="selected"';
												}
												echo '<option value="'.$value['id'].'" region_id="'.$value['region_id'].'" '.$selected.' readonly>'.ucwords(strtolower($value['name'])).'</option>';
												
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-12">
								<div class="form-group">
									<label for="message-text" class="form-control-label">Region:</label>
									<select class="form-control validate[required]" id="region_id" name="region_id">
										<option value="" disbaled>Select</option>
										<?php 
											foreach ($region as $key => $value) {
												$selected = '';
												if(isset($client_details) && $client_details['region_id'] == $value['id']){
													$selected = 'selected="selected"';
												}
												echo '<option value="'.$value['id'].'" '.$selected.'>'.ucwords(strtolower($value['name'])).'</option>';
											}
										?>
									</select>
								</div>
							</div>
							<div class="col-md-6 col-sm-12">
								<div class="form-group">
									<label for="message-text" class="form-control-label">Website:</label>
									<input type="text" class="form-control validate[custom[url]]" id="website" name="website" value="<?php if(isset($client_details)) echo $client_details['website']; ?>">
								</div>
							</div>

							<div class="col-md-12">
								<h4>Members</h4>
								<button type="button" class="btn btn-sm btn-primary pull-right" id="add_member" >Add New</button>
								<div class="form-group">
									<table class="table table-bordered" id="member_grid">
										<thead>
											<tr>
												<th>Name</th>
												<th>Email</th>
												<th>Mobile</th>
												<th>Is Whatsapp</th>
												<th>Skype</th>
												<th>Telephone</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
										<?php 
										if(isset($client_members) && !empty($client_members)){

											// echo "<pre>";print_r($client_members);echo"</pre><hr>";die;
											$i=1;
											foreach($client_members as $cm){ ?>
												<tr>
													<td><input class="form-control" type="text" name="member_name[]" value="<?php echo $cm['member_name']; ?>"></td>
													<td><input class="form-control" type="text" name="email[]" value="<?php echo $cm['email']; ?>"></td>
													<td><input class="form-control" type="text" name="mobile[]" value="<?php echo $cm['mobile']; ?>"></td>
													<td>
														<select class="form-control" name="is_whatsapp">
															<option value="Yes" <?php if($cm['is_whatsapp'] == 'Yes') echo 'selected="selected"';?>>Yes</option>
															<option value="No" <?php if($cm['is_whatsapp'] == 'No') echo 'selected="selected"';?>>No</option>
														</select>
													</td>
													<td><input class="form-control" type="text" name="skype[]" value="<?php echo $cm['skype']; ?>"></td>
													<td><input class="form-control" type="text" name="telephone[]" value="<?php echo $cm['telephone']; ?>"></td>
													<td><input type="hidden" name="comp_dtl_id[]" value="<?php echo $cm['comp_dtl_id']; ?>" class="comp_dtl_id"><button type="button" class="btn btn-danger btn-sm removeBtn">Remove</button></td>
												</tr>
										<?php $i++; 
											} 
										} ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<?php if(isset($client_details)){ ?>
							<input type="hidden" value="<?php echo $client_details['id'];?>" name="id">
						<?php } ?>
						<button type="submit" class="btn btn-primary"><?php if(isset($client_details)) echo 'Update Client'; else echo 'Add Client'; ?></button>
					</div>
				<?php echo form_close(); ?>
					<!-- form -->

				</div>
			</div>
		</div>
	</div>
</div>

<!--begin::Client form-->
<div class="modal fade" id="add_company" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
		
		</div>
	</div>
</div>


<!--end::Client form-->