<div class="form-group row">
    <input type="text" class="form-control" name="comp_mst_id" id="comp_mst_id" value="<?php echo $comp_mst_id; ?>" hidden/>
    <div class="col-lg-3 form-group-sub">
        <label class="form-control-label">Member Name</label>
        <input type="text" class="form-control" name="member_name" id="member_name" value="">
        <small class="form-text text-danger" id="member_error"></small> 
    </div>
    <div class="col-lg-3 form-group-sub">
        <label class="form-control-label">Designation</label>
        <input type="text" class="form-control" name="designation" value="">
    </div>
    <div class="col-lg-3 form-group-sub">
        <label class="form-control-label">Email</label>
        <input type="text" class="form-control" name="email" value="">
    </div>
    <div class="col-lg-3 form-group-sub">
        <label class="form-control-label">Mobile</label>
        <input type="text" class="form-control" name="mobile" value="">
    </div>
</div>
<div class="form-group row">
    <div class="col-lg-3 form-group-sub">
        <label class="form-control-label client_select_picker">Is Whatsapp</label>
        <select class="form-control" name="is_whatsapp">
            <option value="No">No</option>
            <option value="Yes">Yes</option>                                                      
        </select>
    </div>
    <div class="col-lg-3 form-group-sub">
        <label class="form-control-label">Skype</label>
        <input type="text" class="form-control" name="skype" value="">
    </div>
    <div class="col-lg-3 form-group-sub">
        <label class="form-control-label">Telephone</label>
        <input type="text" class="form-control" name="telephone" value="">
    </div>
    <!-- <div class="col-lg-3 form-group-sub">
        <label class="form-control-label client_select_picker">Main Buyer</label>
        <select class="form-control" name="main_buyer">
            <option value="No">No</option>
            <option value="Yes">Yes</option>                                                      
        </select>
    </div> -->
</div>
<div class="row">
	<div class="col-md-12 form-group" style="text-align:center;">
		<button class="btn btn-success save_member" type="reset" id="comp_dtl_id" style="text-align:center;">Submit</button>
	</div>
</div>

