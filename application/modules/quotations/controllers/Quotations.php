<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quotations extends MX_Controller {
	public function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('user_id') && $this->router->fetch_method() != 'daily_followup'){
			redirect('login', 'refresh');
			exit;
		}else{
			// $access_count = 0;
			// if(!empty($this->session->userdata('main_module_access')) && in_array(2, array_column($this->session->userdata('main_module_access'), 'module_id'))) {

			// 	foreach ($this->session->userdata('sub_module_access') as $key => $value) {
					
			// 		if($value['module_id'] == 2 && $value['status'] == 'Active') {
			// 			$access_count++;
			// 		}
			// 	}
			// }
			// if($access_count == 0){
			// 	redirect($this->session->userdata('home/dashboard'));
			// 	exit;
			// }
		}
		error_reporting(0);
		$this->load->model('quotation_model');
		$this->load->model('client/client_model');
		$this->load->model('common/common_model');
	}

	public function index(){
		$this->add();
	}

	public function add($quote_id=0, $rfq_id=0){
	
		
		// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";die;
		$this->session->set_userdata('quotation_priority', 0);
		if(!empty($this->input->post())){

			$insert_arr = array(
								
				'made_by' => $this->input->post('made_by'),
				'assigned_to' => $this->input->post('assigned_to'),
				'stage' => $this->input->post('stage'),
				'is_new' => 'Y',
				'client_id' => $this->input->post('client_id'),
				'member_id' => $this->input->post('member_id'),
				'reference' => $this->input->post('reference'),
				'delivered_through' => $this->input->post('delivered_through'),
				'delivery_time' => $this->input->post('delivery_time'),
				'payment_term' => $this->input->post('payment_term'),
				'validity' => $this->input->post('validity'),
				'currency' => $this->input->post('currency'),
				'currency_rate' => $this->input->post('currency_rate'),
				'origin_country' => $this->input->post('origin_country'),
				'mtc_type' => $this->input->post('mtc_type'),
				'transport_mode' => $this->input->post('transport_mode'),
				'client_type' => $this->input->post('client_type'),
				'importance' => $this->input->post('importance'),
				'followup_date' => date('Y-m-d', strtotime('+1 day', time())),
				'modified_on' => date('Y-m-d H:i:s'),
				'modified_by' => $this->session->userdata('user_id'),
				'rfq_id' => $this->input->post('rfq_id'),
				'additional_comment' => $this->input->post('additional_comment'),
				'comments' => $this->input->post('terms_conditions'),
				'quotation_priority' => $this->input->post('quotation_priority'),
				'priority_reason' => $this->input->post('priority_reason')
			);

			if($this->input->post('quote_date')){

				if($this->input->post('quote_date') == date('d-m-Y')){

					$insert_arr['entered_on'] = date('Y-m-d H:i:s');
				}else{

					$insert_arr['entered_on'] = date('Y-m-d 2:30:0', strtotime($this->input->post('quote_date')));
				}
				
			}
			// echo "<pre>";print_r($insert_arr);echo"</pre><hr>";die;
			$currency_arr = $this->quotation_model->getData('currency', array('status'=> 'Active'));

			if($this->input->post('stage') == 'draft' || $this->input->post('stage') == 'publish'){
				$insert_arr['status'] = 'Open';
			}else if($this->input->post('stage') == 'proforma'){
				// die('came in proforma!!');
				$insert_arr['status'] = 'Won';
				$insert_arr['order_no'] = $this->input->post('order_no');
				if(!isset($_POST['proforma_no']) || $this->input->post('proforma_no') == ''){
					// $logic = $this->quotation_model->getData('number_logic', 'logic_id = 2');
					// $proforma_value = $logic[0]['logic_value'] +1;
					// $proforma_no = 'OM/'.$proforma_value.'/21-22';
					// $insert_arr['proforma_no'] = $proforma_no;
					$insert_arr['proforma_no'] = $this->get_quote_no($this->input->post('client_type'), 'proforma_no');
					$insert_arr['confirmed_on'] = date('Y-m-d H:i:s');
					// $this->quotation_model->updateData('number_logic', array('logic_value' => $proforma_value), array('logic_id' => 2));
				}
			}

			$rfq_priority = $this->common_model->get_dynamic_data_sales_db('priority, priority_reason', array('rfq_mst_id' => $rfq_id), 'rfq_mst','row_array');
			if(!empty($rfq_priority)){

				$insert_arr['quotation_priority'] = $rfq_priority['priority'];
				$insert_arr['priority_reason'] = $rfq_priority['priority_reason'];
			}
			if($this->input->post('quote_id') != ''){
				// die('came in update');
				$quotation_id = $this->input->post('quote_id');
				if($this->input->post('stage') == 'publish' && $this->input->post('quote_no') == ''){
					
					$insert_arr['quote_no'] = $this->get_quote_no($this->input->post('client_type'), 'quote_no');
					// $logic = $this->quotation_model->getData('number_logic', 'logic_id = 1');
					// $quotation_value = $logic[0]['logic_value'] +1;
					// $quote_no = 'OM/'.$quotation_value.'/21-22';
					// $insert_arr['quote_no'] = $quote_no;
					$insert_arr['entered_on'] = date('Y-m-d H:i:s');
				}

				$this->quotation_model->updateData('quotation_mst', $insert_arr, array('quotation_mst_id' => $quotation_id));
				$this->quotation_model->deleteData('quotation_dtl', array('quotation_mst_id' => $quotation_id));
			}else{
				$insert_arr['quote_no'] = '';
				if($this->input->post('stage') == 'draft'){

					$insert_arr['quote_no'] = '';
				}else if($this->input->post('stage') == 'publish'){

					// $logic = $this->quotation_model->getData('number_logic', 'logic_id = 1');
					// $quotation_value = $logic[0]['logic_value'] +1;
					// $quote_no = 'OM/'.$quotation_value.'/21-22';
					$insert_arr['quote_no'] = $this->get_quote_no($this->input->post('client_type'), 'quote_no');
				}
				$insert_arr['entered_on'] = date('Y-m-d H:i:s');
				$quotation_id = $this->quotation_model->insertData('quotation_mst', $insert_arr);
			}

			if(in_array($this->input->post('stage'), array('publish', 'proforma'))){

				$this->quotation_model->insertData('quotation_logs', array(
													'user_id'=> $this->session->userdata('user_id'),
													'user_name'=> $this->session->userdata('name'),
													'stage'=> $this->input->post('stage')
												));
			}
			// if(isset($quotation_value)){
			// 	$this->quotation_model->updateData('number_logic', array('logic_value' => $quotation_value), array('logic_id' => 1));
			// }

			$net_total = 0;
			foreach ($this->input->post('description') as $key => $value) {
				if($this->input->post('currency_rate') != ''){
					$base_price = round($this->input->post('unit_rate')[$key] / $this->input->post('currency_rate'), 2);
				} else if($this->input->post('currency') == 1){
					$base_price = round($this->input->post('unit_rate')[$key] / $currency_arr[0]['currency_rate'], 2);
				} else if($this->input->post('currency') == 2){
					$base_price = round($this->input->post('unit_rate')[$key] / $currency_arr[1]['currency_rate'], 2);
				} else if($this->input->post('currency') == 3){
					$base_price = round($this->input->post('unit_rate')[$key] / $currency_arr[2]['currency_rate'], 2);
				}else if($this->input->post('currency') == 4){
					$base_price = round($this->input->post('unit_rate')[$key] / $currency_arr[3]['currency_rate'], 2);
				}

				if($this->input->post('unit_price')[$key] != ''){
					$unit_price = $this->input->post('unit_price')[$key];
					$detail_arr = array(
						'quotation_mst_id' => $quotation_id,
						'product_id' => $this->input->post('product_id')[$key],
						'material_id' => $this->input->post('material_id')[$key],
						'description' => $this->input->post('description')[$key],
						'quantity' => $this->input->post('quantity')[$key],
						'unit' => $this->input->post('unit')[$key],
						'unit_price' => $unit_price
					);
				}else{
					$unit_price = ($base_price + ($base_price * $this->input->post('margin')[$key] / 100) + ($base_price * $this->input->post('packing_charge')[$key] / 100));
					$detail_arr = array(
						'quotation_mst_id' => $quotation_id,
						'product_id' => $this->input->post('product_id')[$key],
						'material_id' => $this->input->post('material_id')[$key],
						'description' => $this->input->post('description')[$key],
						'quantity' => $this->input->post('quantity')[$key],
						'unit' => $this->input->post('unit')[$key],
						'unit_rate' => $this->input->post('unit_rate')[$key],
						'margin' => $this->input->post('margin')[$key],
						'packing_charge' => $this->input->post('packing_charge')[$key],
						'unit_price' => $unit_price,
					);
				}
				$row_price = $this->input->post('quantity')[$key] * $unit_price;
				$net_total += $row_price;
				$detail_arr['row_price'] = $row_price;

				$this->quotation_model->insertData('quotation_dtl', $detail_arr);
			}

			$discount_type = $this->input->post('discount_type');
			$other_charges = $discount = 0;

			if($this->input->post('discount') != ''){
				$discount = $this->input->post('discount');
				if($discount_type == 'percent'){
					$discount = round(($net_total * $discount) / 100, 2);
				}
			}

			$gst = 0;
			if($this->input->post('currency') == 3){
				$gst = round(($net_total * 18) / 100, 2);
			}

			if($this->input->post('other_charges') != ''){
				$other_charges = $this->input->post('other_charges');
			}

			$update_arr = array(
				'net_total' => $net_total,
				'freight' => $this->input->post('freight'),
				'bank_charges' => $this->input->post('bank_charges'),
				'gst' => $gst,
				'discount_type' => $discount_type,
				'discount' => $discount,
				'other_charges' => $other_charges,
				'grand_total' => $net_total + $this->input->post('freight') + $this->input->post('bank_charges') + $gst + $other_charges - $discount,
			);

			$this->quotation_model->updateData('quotation_mst', $update_arr, array('quotation_mst_id' => $quotation_id));


			if(!empty($_FILES['purchase_order'])){
				$config['upload_path']          = './assets/purchase_orders/';
	            $config['allowed_types']        = 'pdf';//'jpeg|gif|jpg|png';
	            $config['max_size']             = 5242880;
	            $config['file_name']            = 'PO-'.$quotation_id;
	            $config['overwrite']            = TRUE;
	            $this->load->library('upload', $config);
	            if ( ! $this->upload->do_upload('purchase_order'))
	            {
					$error = array('error' => $this->upload->display_errors());
					$data = array('status' => 'failed', 'msg' => $error['error']);
	            }
	            else
	            {
	            	$file_dtls = $this->upload->data();
		            $data = array('status' => 'success', 'msg' => 'Image uploaded successfully!', 'file_name' => $file_dtls['file_name']);
		            $this->quotation_model->updateData('quotation_mst', array('purchase_order' => $file_dtls['file_name']), array('quotation_mst_id' => $quotation_id));
	            }
			}

			if($this->input->post('stage') == 'publish'){
				
				$sms_details = $this->quotation_model->getSMSDetails('quotation', $quotation_id);
				$this->send_sms('quotation_ready', $sms_details);
				$this->send_whatsapp('quotation_ready', $sms_details);

			}else if($this->input->post('stage') == 'proforma'){

				$sms_details = $this->quotation_model->getSMSDetails('proforma', $quotation_id);
				if($sms_details['purchase_user'] != ''){

					$this->send_sms('proforma_ready', $sms_details);
					$this->send_whatsapp('proforma_ready', $sms_details);
				}else{
					
					$this->send_sms('proforma_ready_2', $sms_details);
					$this->send_whatsapp('proforma_ready_2', $sms_details);
				}
			}
			
			$this->session->set_flashdata('success', 'Quotation saved successfully');
			redirect('quotations/list');
		}else{

			$client_id = $member_id = 0;
			if($quote_id > 0){
				// echo "<pre>";print_r($quote_id);echo"</pre><hr>";die;
				$data['quote_details'] = $this->quotation_model->getQuotationDetails($quote_id);
				// echo "<pre>";print_r($data['quote_details']);echo"</pre><hr>";die;

				if(!empty($data['quote_details'])){
					$this->session->set_userdata('quotation_priority', $data['quote_details'][0]['quotation_priority']);
					$client_id = $data['quote_details'][0]['client_id'];
					$member_id = $data['quote_details'][0]['member_id'];
				}
				$data['quote_id'] = $quote_id;
			}
			if($rfq_id > 0){
				$data['rfq_details'] = $this->quotation_model->getData('rfq_mst', 'rfq_mst_id = '.$rfq_id);
				$data['rfq_notes'] = $this->quotation_model->getData('rfq_note_query', "type='notes' and rfq_id = ".$rfq_id);
			}else{
				$data['rfq_details'] = $this->quotation_model->getData('rfq_mst', 'rfq_mst_id = '.$data['quote_details'][0]['rfq_id']);
				$data['rfq_notes'] = $this->quotation_model->getData('rfq_note_query', "type='notes' and rfq_id = ".$data['quote_details'][0]['rfq_id']);
			}
			if($client_id === 0){

				$client_id = $data['rfq_details'][0]['rfq_company'];
				$member_id = $data['rfq_details'][0]['rfq_buyer'];
			}
			$data['clients'] = $this->quotation_model->get_client_name_on_id($client_id);
			$data['members'] = $this->common_model->get_dynamic_data_sales_db('comp_dtl_id, member_name', array('status'=>'Active', 'comp_mst_id'=> $client_id), 'customer_dtl');
			// $data['users'] = $this->quotation_model->getUsers();
			// echo "<pre>";print_r($data['rfq_details']);echo"</pre><hr>";die;
			$data['users'] = $this->common_model->get_dynamic_data_sales_db('*', array('status' => 1, 'role' => 4), 'users');
			array_push($data['users'] ,(['user_id'=>73,'name'=>'Ankita Parmar']));
			// $data['assignee'] = $this->quotation_model->getAssignee();
			$data['assignee'] = $this->quotation_model->getData('users', '(role = 5  OR role = 16) and status = 1');
			
			// $data['members'] = $this->quotation_model->getData('customer_dtl',array('status'=>'Active'));

			// echo "<pre>";print_r($data['members']);echo"</pre><hr>";die;
			
			
			$data['region'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=> 'Active'), 'region_mst');
			$data['country'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=> 'Active'), 'country_mst');
			$data['transport_mode'] = $this->quotation_model->getData('transport_mode',array('status'=>'Active'));
			$data['delivery'] = $this->quotation_model->getData('delivery', array('status'=> 'Active'));
			$data['delivery_time'] = $this->quotation_model->getData('delivery_time', array('status'=> 'Active'));
			$data['payment_terms'] = $this->quotation_model->getData('payment_terms', array('status'=> 'Active'));
			$data['origin_country'] = $this->quotation_model->getData('origin_country', array('status'=> 'Active'));
			$data['currency'] = $this->quotation_model->getData('currency', array('status'=> 'Active'));
			$data['validity'] = $this->quotation_model->getData('validity', array('status'=> 'Active'));
			$data['mtc_type'] = $this->quotation_model->getData('mtc_type', array('status'=> 'Active'));
			$data['ports'] = $this->quotation_model->getData('ports');
			$data['zones'] = $this->quotation_model->getData('zone_to_country');
			$data['unit_str'] = $data['prd_str'] = $data['mat_str'] = ""; 

			$data['product'] = $product = $this->quotation_model->getData('product_mst',array('status'=>'Active'));
			foreach($product as $prod){
				$data['prd_str'] .= '<option value="'.$prod['id'].'">'.ucwords(strtolower($prod['name'])).'</option>';
			}

			$data['material'] = $material = $this->quotation_model->getData('material_mst',array('status'=>'Active'));
			foreach($material as $mat){ 
				$data['mat_str'] .= '<option value="'.$mat['id'].'">'.ucwords(strtolower($mat['name'])).'</option>';
			}

			$data['units'] = $units = $this->quotation_model->getData('units',array('status'=>'Active'));
			foreach($units as $uts){ 
				$data['unit_str'] .= '<option value="'.$uts['unit_id'].'">'.ucwords(strtolower($uts['unit_value'])).'</option>';
			}
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
			$this->load->view('header', array('title' => 'Add Quotation'));
			$this->load->view('sidebar', array('title' => 'Add Quotation'));
			$this->load->view('quotation_form', $data);
			$this->load->view('footer');
		}
	}

	private function get_quote_no($client_type = 'omtubes', $quote_type = 'quote_no'){
		
		$client_static_array = array('omtubes'=> 'om', 'zengineer'=> 'zen', 'instinox'=> 'in');
		$client_type = $client_static_array[$client_type];
		$year = date('y').'-'.(date('y') + 1);
		if(date('m') <= 3){

			$year = (date('y')-1)."-".date('y');
		}
		$last_query_id = $this->common_model->get_dynamic_data_sales_db('last_quote_id', array('status'=> "Active", 'module_name'=> 'quotation', 'sub_module_name'=> $quote_type, 'sub_module_type'=> 'om'), 'last_quote_created_number', 'row_array');
		$no = (((int)$last_query_id['last_quote_id'])+1);
		$this->common_model->update_data_sales_db('last_quote_created_number', array('last_quote_id'=>(((int)$last_query_id['last_quote_id'])+1)), array('status'=> "Active", 'module_name'=> 'quotation', 'sub_module_name'=> $quote_type, 'sub_module_type'=> 'om'));
		
		return strtoupper($client_type).'/'.$no.'/'.$year;
	}
	public function quotation_status_sms_test() {
		$quotation_id =  $this->uri->segment('3', 0);
		// echo $quotation_id, "<hr>";die('debug');
		if(!empty($quotation_id)) {

			// $sms_details = $this->quotation_model->getSMSDetails('quotation', $quotation_id);
			// $this->send_sms('quotation_ready', $sms_details);
			// echo "<pre>";print_r($sms_details);echo"</pre><hr>";exit;

			// $sms_details = $this->quotation_model->getSMSDetails('proforma', $quotation_id);
			// $this->send_sms('proforma_ready', $sms_details);
			// $this->send_sms('proforma_ready_2', $sms_details);

			// $query_data = $this->quotation_model->getData('query_mst', 'query_id = 178');
			// $query_details = $this->quotation_model->getQueryQuote('178');
			// $sender = $this->quotation_model->getData('users', 'user_id = '.$query_data[0]['raised_by']);
			// $rece = $this->quotation_model->getData('users', 'user_id = '.$query_data[0]['query_recepient']);
			// $this->send_sms('query_raised', array('name'=> $rece[0]['name'], 'sender'=> $sender[0]['name'], 'quote_str'=> $query_details['quote_str'], 'quote'=> $query_details['quote'], 'query_text'=> 'Query raised from proforma screen', 'for_id'=> $query_data[0]['query_recepient']));
			// $sender = $this->quotation_model->getData('users', 'user_id = '.$query_data[0]['query_recepient']);
			// $rece = $this->quotation_model->getData('users', 'user_id = '.$query_data[0]['raised_by']);
			// $this->send_sms('query_answered', array('name'=> $rece[0]['name'], 'sender'=> $sender[0]['name'], 'quote_str'=> $query_details['quote_str'], 'quote'=> $query_details['quote'], 'query_text'=> 'desai', 'for_id'=> $query_data[0]['query_recepient']));

			$users = $this->quotation_model->getData('users', '(role = 5  OR role = 16) and status = 1');
			foreach ($users as $user) {
				$count = $this->quotation_model->getFollowupCount($user['user_id']);
				if($count > 0){

					$this->send_sms('daily_follow_up', array('name'=> $user['name'], 'count'=> $count, 'mobile'=> $user['mobile']));
					$this->send_whatsapp('daily_follow_up', array('name'=> $user['name'], 'count'=> $count, 'mobile'=> $user['mobile']));
				}
			}
			
		}
	}

	function zen_pdf($quote_id){

		redirect('pdf_management/quotation_pdf/'.$quote_id, 'refresh');
		// $data['quote_details'] = $this->quotation_model->getQuotationDetails($quote_id);
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$data = array();
		// error_reporting(E_ALL);
		$quotation_details = $this->quotation_model->get_dynamic_data('*', array('quotation_mst_id' => $quote_id), 'quotation_mst', 'row_array');
		if(!empty($quotation_details)) {

			$data['rfq_details'] = $this->quotation_model->get_dynamic_data('*', array('rfq_mst_id' => $quotation_details['rfq_id']), 'rfq_mst', 'row_array');

			// INNER JOIN `quotation_dtl` `d` ON `m`.`quotation_mst_id` = `d`.`quotation_mst_id`
			$data['quotation_dtl'] = $this->quotation_model->get_dynamic_data('*', array('quotation_mst_id' => $quotation_details['quotation_mst_id']), 'quotation_dtl', 'result_array'); 
			if(!empty($quotation_details['client_id'])) {

				// INNER JOIN `clients` `c` ON `c`.`client_id` = `m`.`client_id`
				$data['clients'] = $this->quotation_model->get_dynamic_data('*', array('id' => $quotation_details['client_id']), 'customer_mst', 'row_array'); 
			}
			$data['members']['member_name'] = '';
			if(!empty($quotation_details['member_id'])) {

				// INNER JOIN `members` `mb` ON `mb`.`member_id` = `m`.`member_id` 
				// $data['members'] = $this->quotation_model->get_dynamic_data('*', array('member_id' => $quotation_details['member_id']), 'members', 'row_array'); 
				$data['members'] = $this->quotation_model->get_dynamic_data('*', array('comp_dtl_id' => $quotation_details['member_id']), 'customer_dtl', 'row_array'); 
			}
			if(!empty($quotation_details['transport_mode'])) {

				// INNER JOIN `transport_mode` `t` ON `m`.`transport_mode` = `t`.`mode_id`
				$data['transport_mode'] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'mode_id' => $quotation_details['transport_mode']), 'transport_mode', 'row_array'); 
			}
			if(!empty($quotation_details['payment_term'])) {

				//  INNER JOIN `payment_terms` `pt` ON `m`.`payment_term` = `pt`.`term_id` 
				$data['payment_terms'] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'term_id' => $quotation_details['payment_term']), 'payment_terms', 'row_array'); 
			}
			if(!empty($quotation_details['delivered_through'])) {

				//  INNER JOIN `delivery` `dl` ON `m`.`delivered_through` = `dl`.`delivery_id` 
				$data['delivery'] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'delivery_id' => $quotation_details['delivered_through']), 'delivery', 'row_array'); 
			}
			if(!empty($quotation_details['delivery_time'])) {

				//  INNER JOIN `delivery_time` `dt` ON `m`.`delivery_time` = `dt`.`dt_id` 
				$data['delivery_time'] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'dt_id' => $quotation_details['delivery_time']), 'delivery_time', 'row_array'); 
			}
			if(!empty($quotation_details['origin_country'])) {

				//  INNER JOIN `origin_country` `oc` ON `m`.`origin_country` = `oc`.`country_id`
				$data['origin_country'] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'country_id' => $quotation_details['origin_country']), 'origin_country', 'row_array'); 
			}
			if(!empty($quotation_details['currency'])) {

				//   INNER JOIN `currency` `cr` ON `m`.`currency` = `cr`.`currency_id` 
				$data['currency'] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'currency_id' => $quotation_details['currency']), 'currency', 'row_array'); 
			}
			if(!empty($quotation_details['validity'])) {

				//   INNER JOIN `validity` `v` ON `m`.`validity` = `v`.`validity_id`
				$data['validity'] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'validity_id' => $quotation_details['validity']), 'validity', 'row_array'); 
			}
			if(!empty($quotation_details['mtc_type'])) {

				//    INNER JOIN `mtc_type` `mt` ON `m`.`mtc_type` = `mt`.`mtc_id` 
				$data['mtc_type'] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'mtc_id' => $quotation_details['mtc_type']), 'mtc_type', 'row_array'); 
			}
			if(!empty($data['quotation_dtl'])) {

				//    INNER JOIN `units` `u` ON `d`.`unit` = `u`.`unit_id` 
				foreach (array_column($data['quotation_dtl'], 'unit','unit') as $key => $value) {
					$data['units'][$key] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'unit_id ' => $value), 'units', 'row_array');
				}	
			}
			if(!empty($quotation_details['assigned_to'])) {

				//    INNER JOIN `users` `us` ON `m`.`assigned_to` = `us`.`user_id` 
				$data['users'] = $this->quotation_model->get_dynamic_data('*', array('user_id' => $quotation_details['assigned_to']), 'users', 'row_array'); 
			}
			if(!empty($data['clients']['country_id'])) {

				//    LEFT JOIN `lookup` `lc` ON `lc`.`lookup_id` = `c`.`country` 
				// $data['lookup_country'] = $this->quotation_model->get_dynamic_data('*', array('lookup_id' => $data['clients']['country']), 'lookup', 'row_array');
				$data['lookup_country'] = $this->quotation_model->get_dynamic_data('*', array('id' => $data['clients']['country_id']), 'country_mst', 'row_array'); 
			}
			if(!empty($data['clients']['region_id'])) {

				//    LEFT JOIN `lookup` `lr` ON `lr`.`lookup_id` = `c`.`region`
				// $data['lookup_region'] = $this->quotation_model->get_dynamic_data('*', array('lookup_id' => $data['clients']['region']), 'lookup', 'row_array'); 
				$data['lookup_region'] = $this->quotation_model->get_dynamic_data('*', array('id' => $data['clients']['region_id']), 'region_mst', 'row_array'); 
			}
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		}
		// echo "<pre>";print_r($quotation_details);echo"</pre><hr>";exit;
		if($this->session->userdata('role') == 5 || $this->session->userdata('role') == 16){
			$this->quotation_model->updateData('quotation_mst', array('is_new' => NULL), array('quotation_mst_id' => $quote_id));
		}
		if(($data['delivery']['delivery_name'] == 'CFR Port Name' || $data['delivery']['delivery_name'] == 'CIF Port Name') && $data['transport_mode']['mode'] == 'Sea Worthy'){
			$port_name = $this->quotation_model->getPortName('CIF', 'sea', $data['lookup_country']['name']);
			if($data['delivery']['delivery_name'] == 'CFR Port Name'){
				$data['delivery']['delivery_name'] = 'CFR '.$port_name;
			} else if($data['delivery']['delivery_name'] == 'CIF Port Name'){
				$data['delivery']['delivery_name'] = 'CIF '.$port_name;
			}
		}
		$this->load->library('Pdf');
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

			// set document information
			

			// set default header data
			$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, NULL, '', array(0,0,0), array(255,255,255));
			$pdf->SetFooterData(array(0,0,0), array(255,255,255));

			// set header and footer fonts
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			// set margins
			$pdf->SetMargins(10, PDF_MARGIN_TOP-10, 10);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER-5);

			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM-15);

			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			// set some language-dependent strings (optional)
			if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			    require_once(dirname(__FILE__).'/lang/eng.php');
			    $pdf->setLanguageArray($l);
			}

			// ---------------------------------------------------------

			// set font
			$pdf->SetFont('helvetica', '', 10);

		// add a page
		$pdf->AddPage();

		

		// define some HTML content with style


		$table_str = '';

		$i=0;
        foreach ($data['quotation_dtl'] as $key => $value) { 
        	$bg_color = '';
			if($i%2 != 0){
				$bg_color = '#e4e1e1;';
			}

			if(strpos($value['quantity'], '.') === false){
				$value['quantity'] = $value['quantity'].".00";
			}

			if(strpos($value['unit_price'], '.') === false){
				$value['unit_price'] = $value['unit_price'].".00";
			}

			if(strpos($value['row_price'], '.') === false){
				$value['row_price'] = $value['row_price'].".00";
			}
			$table_str .= '<tr style="background-color: '.$bg_color.'; font-size: 11px; font-family: courier;">
				<td style="text-align: right;">'.++$i.'</td>
				<td>'.htmlentities($value['description']).'</td>
				<td style="text-align: right;"> '.$value['quantity'].'</td>
				<td>'.ucwords(strtolower($data['units'][$value['unit']]['unit_value'])).'.</td>';
				// if((int)$value['unit_price'] > 0){

				// 	$table_str .= '<td style="text-align: right;">'.$data['currency']['currency_icon'].$value['unit_price'].'</td>';
				// } else {

				// 	$table_str .= '<td style="text-align: right;">REGRET</td>';
				// }
				// $table_str .= '<td>P.'.ucwords(strtolower($data['units'][$value['unit']]['unit_value'])).'.</td>';
				// if((int)$value['row_price'] > 0){

				// 	$table_str .= '<td style="text-align: right;">'.$data['currency']['currency_icon'].$value['row_price'].'</td>';
				// } else {

				// 	$table_str .= '<td style="text-align: right;">REGRET</td>';
				// }
				if($value['unit_price'] > 0 || $value['row_price'] > 0) {

					if(!in_array($this->session->userdata('user_id'), array(191))){
						$table_str .= '<td style="text-align: right;">'.$data['currency']['currency_icon'].$value['unit_price'].'</td>';
						$table_str .= '<td>P.'.ucwords(strtolower($data['units'][$value['unit']]['unit_value'])).'.</td>';
						$table_str .= '<td style="text-align: right;">'.$data['currency']['currency_icon'].$value['row_price'].'</td>';
					}
					
				}else {

					if(!in_array($this->session->userdata('user_id'), array(191))){
						$table_str .= '<td style="text-align: right;">REGRET</td>';
						$table_str .= '<td>P.'.ucwords(strtolower($data['units'][$value['unit']]['unit_value'])).'.</td>';
						$table_str .= '<td style="text-align: right;">REGRET</td>';
					}
				}
			$table_str .= '</tr>';
        }

        $net_total = (!empty($quotation_details)) ? $quotation_details['net_total'] : 00;
        if(strpos($net_total, '.') === false){
        	$net_total = $net_total.'.00';
        }
		if(in_array($this->session->userdata('user_id'), array(191))){

			$net_total = 0;
		}

        $total_table = '<table cellspacing="0" cellpadding="10">
		<tr >
		<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;" width="50%" >Net Total</td>
		<td style="font-size: 12px; font-family: courier; border: solid 1px grey;" width="50%">'.$data['currency']['currency_icon'].$net_total.'</td>
		</tr>';

		if($quotation_details['freight'] > 0){
			$freight = $quotation_details['freight'];
	        if(strpos($freight, '.') === false){
	        	$freight = $freight.'.00';
	        }
			$total_table .= '<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Freight Charges</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['currency']['currency_icon'].$freight.'</td>
			</tr>';	
		}
		
		if($quotation_details['bank_charges'] > 0){
			$bank_charges = $quotation_details['bank_charges'];
	        if(strpos($bank_charges, '.') === false){
	        	$bank_charges = $bank_charges.'.00';
	        }
			$total_table .= '<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Bank Charges</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['currency']['currency_icon'].$bank_charges.'</td>
			</tr>';
		}

		if($quotation_details['gst'] > 0 && $data['currency']['currency'] == 'INR'){
			$gst = $quotation_details['gst'];
	        if(strpos($gst, '.') === false){
	        	$gst = $gst.'.00';
	        }
			$total_table .= '<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">GST</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['currency']['currency_icon'].$gst.'</td>
			</tr>';
		}

		if($quotation_details['other_charges'] > 0){
			$other_charges = $quotation_details['other_charges'];
	        if(strpos($other_charges, '.') === false){
	        	$other_charges = $other_charges.'.00';
	        }
			$total_table .= '<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Other Charges</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['currency']['currency_icon'].$other_charges.'</td>
			</tr>';
		}

		if($quotation_details['discount'] > 0){
			$discount = $quotation_details['discount'];
	        if(strpos($discount, '.') === false){
	        	$discount = $discount.'.00';
	        }
			$total_table .= '<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Discount</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['currency']['currency_icon'].$discount.'</td>
			</tr>';
		}

		$grand_total = $quotation_details['grand_total'];
        if(strpos($grand_total, '.') === false){
        	$grand_total = $grand_total.'.00';
        }
		if(in_array($this->session->userdata('user_id'), array(191))){

			$grand_total = 0;
		}
		$total_table .= '<tr>
			<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Grand Total</td>
			<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['currency']['currency_icon'].$grand_total.'</td>
		</tr>
		</table>';

		$dec_text = 'paise';
		if($data['currency']['currency'] != 'INR'){
			$dec_text = 'cents';
		}
		$grand_total_words = $data['currency']['currency']." : ".$this->numberTowords($grand_total, $dec_text);
		$date = date('d-m-Y', strtotime($quotation_details['entered_on']));
		$client_name = trim($data['clients']['name']);

		if(!empty($quotation_details['quote_no'])){
			$type_name = "Quote #";
			$type_value = $quotation_details['quote_no'];
		}else{
			$type_name = "RFQ #";
			$type_value = $data['rfq_details']['rfq_no'];
		}

		$additional_comment = '';

		if($quotation_details['additional_comment'] != ''){
			$additional_comment = '<br/><br/>'.$quotation_details['additional_comment'];
		}

		$display = "block";
		if(in_array($this->session->userdata('user_id'), array(79,85,133,141,179,180,189,193, 205))){ 
			
			$display = "none";			
		}
		$html = '<!-- EXAMPLE OF CSS STYLE -->
		<table cellpadding="5" cellspacing="0">
		<tr style="background-color: #fff;">
		<td width="50%" style="padding:5px;vertical-align: text-top; ">
		<img src="/assets/media/client-logos/logo_zengineer.PNG" width="180" height="50" style="padding-left: 10px;"><br/>
		<strong style="font-size: 17px;">Zengineer Manufacturing LLP</strong>
		<div class="left_addrs" style="font-size: 14px;color: #484545; line-height: 20px;">3rd Floor, 7/Plot 457, Phoenix Building, S.V.P Road, Mumbai 400004<br/>GSTIN 27AADFZ2705J1Z1
		</div>
		<table style="margin-top: -15px;">
		<tr style="background-color: #fff;">
		<td style="font-size: 14px; color: #484545; line-height: 20px;" align="left">info@zengineermfg.com</td>
		</tr>
		</table>
		</td>
		<td width="50%">
		<strong style="font-size: 25px; text-align: right; line-height: 35px;">Quotation</strong><br/>
		<table style="line-height: 22px;">
		<tr style="background-color: #e4e1e1; border-bottom: 1px solid;">
		<td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;"> '.$type_name.': </strong></td>
		<td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;">Date : </strong></td>
		</tr>
		<tr>
		<td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;">'.$type_value.'</td>
		<td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;">'.$date.'</td>
		</tr>
		<tr style="background-color: #e4e1e1;">
		<td colspan="2" style="padding:5px;vertical-align: text-top;"><strong style="font-size: 12px;"> Customer:</strong></td>
		</tr>
		<tr>
		<td colspan="2" style="padding:5px;vertical-align: text-top; font-family: courier;">
		<strong style="display:'.$display.';">'.$client_name.'</strong>
		<div class="left_addrs" style="font-size: 11px;color: #484545; line-height: 17px; font-family: courier; display: '.$display.';">'.
			$data['lookup_country']['name'].'<br/>'.
			$data['members']['member_name'].'
		</div>
		</td>
		</tr>
		<tr style="background-color: #e4e1e1;">
		<td colspan="2" style="padding:5px;vertical-align: text-top;"><strong style="font-size: 12px;">Reference :</strong></td>
		</tr>
		<tr>
		<td colspan="2" style="font-family: courier; font-size: 11px;color: #484545;">'.$quotation_details['reference'].'</td>
		</tr>
		</table>
		</td>
		</tr>
		<tr>
		<td colspan="2"></td>
		</tr>
		<tr>
		<td colspan="2">
		<table cellspacing="0" cellpadding="10" border="0">
		<thead>
		<tr style="background-color: #e4e1e1;">
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="7%;">#</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="36%">Item Description</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="12%">Qty</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="8%">Unit</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="13%">Price</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="9%">Unit</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="15%">Total</td>
		</tr>
		</thead>
		<tbody>'.$table_str.'</tbody>
		</table>
		</td>
		</tr>
		<tr>
		<td>
		<strong>Total in words:</strong><br/><span style="font-family: courier; font-size: 11px;">'.$grand_total_words.'</span><br/>
		<hr/>
		<table cellspacing="0" cellpadding="3" border="0">
		<tr>
		<td><strong>Additional Notes</strong></td>
		</tr>
		<tr>
		<td style="font-family: courier; font-size: 11px;">
		We reserve the right to correct the pricing offered due to any typographical errors.<br/>
		This offer is not valid for end users from Iran, Iraq, North Korea, Cuba, Sudan & Syria.'.$additional_comment.'
		</td>
		</tr>
		</table>
		</td>
		<td>'.$total_table.'</td>
		</tr>
		<tr>
		<td colspan="2"></td>
		</tr>
		<tr>
		<td colspan="2"></td>
		</tr>
		<tr>
		<td>
		<table>
		<tr>
		<td colspan="2"><strong>Terms and Conditions</strong></td>
		</tr>
		<tr>
		<td width="35%">Delivered To : </td> <td width="65%"><span style="font-family: courier;">'.$data['delivery']['delivery_name'].'</span></td>
		</tr>
		<tr>
		<td>Delivery Time : </td> <td><span style="font-family: courier;">'.$data['delivery_time']['dt_value'].'</span></td>
		</tr>
		<tr>
		<td>Validity : </td> <td><span style="font-family: courier;">'.$data['validity']['validity_value'].'</span></td>
		</tr>
		<tr>
		<td>Currency : </td> <td><span style="font-family: courier;">'.$data['currency']['currency'].'</span></td>
		</tr>
		<tr>
		<td>Country of Origin : </td> <td><span style="font-family: courier;">'.$data['origin_country']['country'].'</span></td>
		</tr>
		<tr>
		<td>MTC Type : </td> <td><span style="font-family: courier;">'.$data['mtc_type']['mtc_value'].'</span></td>
		</tr>
		<tr>
		<td>Packing Type : </td> <td><span style="font-family: courier;">'.$data['transport_mode']['mode'].'</span></td>
		</tr>
		<tr>
		<td>Payment : </td> <td><span style="font-family: courier;">'.$data['payment_terms']['term_value'].'</span></td>
		</tr>
		</table>
		</td>
		<td align="center">
		Thank you for your business<br/>
		For Zengineer Manufacturing LLP<br/><br/>
		<img src="/assets/media/client-logos/zengineer_stamp.png" / height="100px" weight="100px"><br/><br/>
		<table>
		<tr>
		<td width="26%" rowspan="3"></td>
		<td align="left" width="74%">Name : <span style="font-family: courier;">'.$data['users']['name'].'</span></td>
		</tr>
		<tr>
		<td align="left">Email : <span style="font-family: courier;">'.$data['users']['email'].'</span></td>
		</tr>
		<tr>
		<td align="left">Mobile : <span style="font-family: courier;">'.$data['users']['mobile'].'</span></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>';
		if(!empty($data['lookup_country']['name']) && $data['lookup_country']['name'] == 'Saudi Arabia') {

			$html .= '<div style="width:100%">
				<img src="/assets/media/saudi_extra_banner.png" style="padding: 2% 0% 0% 20%;"/>
			</div>';
		}else if (!empty($data['lookup_country']['name']) && $data['lookup_country']['name'] == 'Qatar'){

			$html .= '<div style="width:70%">
				<img src="/assets/media/qatar_extra_banner.png" style="padding: 2% 10% 10% 20%;"/>
			</div>';
		}

		/*$html = <<<HTMLEND
		blah blah blah
		HTMLEND;*/

		// output the HTML content
		$pdf->writeHTML($html, true, false, true, false, '');

		//Close and output PDF document
		$pdf->Output(str_replace('/', '-', $quotation_details['quote_no']).'.pdf', 'I');
	}

	function new_pdf($quote_id){

		redirect('pdf_management/quotation_pdf/'.$quote_id, 'refresh');
		// $data['quote_details'] = $this->quotation_model->getQuotationDetails($quote_id);
		$data = array();
		// error_reporting(E_ALL);
		$quotation_details = $this->quotation_model->get_dynamic_data('*', array('quotation_mst_id' => $quote_id), 'quotation_mst', 'row_array');
		// echo "<pre>";print_r($quotation_details);echo"</pre><hr>";exit;
		if(!empty($quotation_details)) {

			$data['rfq_details'] = $this->quotation_model->get_dynamic_data('*', array('rfq_mst_id' => $quotation_details['rfq_id']), 'rfq_mst', 'row_array');
			// INNER JOIN `quotation_dtl` `d` ON `m`.`quotation_mst_id` = `d`.`quotation_mst_id`
			$data['quotation_dtl'] = $this->quotation_model->get_dynamic_data('*', array('quotation_mst_id' => $quotation_details['quotation_mst_id']), 'quotation_dtl', 'result_array'); 
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
			if(!empty($quotation_details['client_id'])) {

				// INNER JOIN `clients` `c` ON `c`.`client_id` = `m`.`client_id`  
				$data['clients'] = $this->quotation_model->get_dynamic_data('*', array('id' => $quotation_details['client_id']), 'customer_mst', 'row_array');
			}
			$data['members']['member_name'] = '';
			if(!empty($quotation_details['member_id'])) {

				// INNER JOIN `members` `mb` ON `mb`.`member_id` = `m`.`member_id` 
				$data['members'] = $this->quotation_model->get_dynamic_data('*', array('comp_dtl_id' => $quotation_details['member_id']), 'customer_dtl', 'row_array'); 
			}
			if(!empty($quotation_details['transport_mode'])) {

				// INNER JOIN `transport_mode` `t` ON `m`.`transport_mode` = `t`.`mode_id`
				$data['transport_mode'] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'mode_id' => $quotation_details['transport_mode']), 'transport_mode', 'row_array'); 
			}
			if(!empty($quotation_details['payment_term'])) {

				//  INNER JOIN `payment_terms` `pt` ON `m`.`payment_term` = `pt`.`term_id` 
				$data['payment_terms'] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'term_id' => $quotation_details['payment_term']), 'payment_terms', 'row_array'); 
			}
			if(!empty($quotation_details['delivered_through'])) {

				//  INNER JOIN `delivery` `dl` ON `m`.`delivered_through` = `dl`.`delivery_id` 
				$data['delivery'] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'delivery_id' => $quotation_details['delivered_through']), 'delivery', 'row_array'); 
			}
			if(!empty($quotation_details['delivery_time'])) {

				//  INNER JOIN `delivery_time` `dt` ON `m`.`delivery_time` = `dt`.`dt_id` 
				$data['delivery_time'] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'dt_id' => $quotation_details['delivery_time']), 'delivery_time', 'row_array'); 
			}
			if(!empty($quotation_details['origin_country'])) {

				//  INNER JOIN `origin_country` `oc` ON `m`.`origin_country` = `oc`.`country_id`
				$data['origin_country'] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'country_id' => $quotation_details['origin_country']), 'origin_country', 'row_array'); 
			}
			if(!empty($quotation_details['currency'])) {

				//   INNER JOIN `currency` `cr` ON `m`.`currency` = `cr`.`currency_id` 
				$data['currency'] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'currency_id' => $quotation_details['currency']), 'currency', 'row_array'); 
			}
			if(!empty($quotation_details['validity'])) {

				//   INNER JOIN `validity` `v` ON `m`.`validity` = `v`.`validity_id`
				$data['validity'] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'validity_id' => $quotation_details['validity']), 'validity', 'row_array'); 
			}
			if(!empty($quotation_details['mtc_type'])) {

				//    INNER JOIN `mtc_type` `mt` ON `m`.`mtc_type` = `mt`.`mtc_id` 
				$data['mtc_type'] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'mtc_id' => $quotation_details['mtc_type']), 'mtc_type', 'row_array'); 
			}
			if(!empty($data['quotation_dtl'])) {

				//    INNER JOIN `units` `u` ON `d`.`unit` = `u`.`unit_id` 
				foreach (array_column($data['quotation_dtl'], 'unit','unit') as $key => $value) {
					$data['units'][$key] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'unit_id' => $value), 'units', 'row_array');
				}	
			}
			if(!empty($quotation_details['assigned_to'])) {

				//    INNER JOIN `users` `us` ON `m`.`assigned_to` = `us`.`user_id` 
				$data['users'] = $this->quotation_model->get_dynamic_data('*', array('user_id' => $quotation_details['assigned_to']), 'users', 'row_array'); 
			}
			if(!empty($data['clients']['country_id'])) {

				//    LEFT JOIN `lookup` `lc` ON `lc`.`lookup_id` = `c`.`country` 
				$data['lookup_country'] = $this->quotation_model->get_dynamic_data('*', array('id' => $data['clients']['country_id']), 'country_mst', 'row_array'); 
			}
			if(!empty($data['clients']['region_id'])) {

				//    LEFT JOIN `lookup` `lr` ON `lr`.`lookup_id` = `c`.`region`
				$data['lookup_region'] = $this->quotation_model->get_dynamic_data('*', array('id' => $data['clients']['region_id']), 'region_mst', 'row_array'); 
			}
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		}
		// echo "<pre>";print_r($quotation_details);echo"</pre><hr>";exit;

		if($this->session->userdata('role') == 5 || $this->session->userdata('role') == 16){
			$this->quotation_model->updateData('quotation_mst', array('is_new' => NULL), array('quotation_mst_id' => $quote_id));
		}
		if(($data['delivery']['delivery_name'] == 'CFR Port Name' || $data['delivery']['delivery_name'] == 'CIF Port Name') && $data['transport_mode']['mode'] == 'Sea Worthy'){
			$port_name = $this->quotation_model->getPortName('CIF', 'sea', $data['lookup_country']['name']);
			if($data['delivery']['delivery_name'] == 'CFR Port Name'){
				$data['delivery']['delivery_name'] = 'CFR '.$port_name;
			} else if($data['delivery']['delivery_name'] == 'CIF Port Name'){
				$data['delivery']['delivery_name'] = 'CIF '.$port_name;
			}
		}
		$this->load->library('Pdf');
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

			// set document information
			

			// set default header data
			$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, NULL, '', array(0,0,0), array(255,255,255));
			$pdf->SetFooterData(array(0,0,0), array(255,255,255));

			// set header and footer fonts
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			// set margins
			$pdf->SetMargins(10, PDF_MARGIN_TOP-10, 10);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER-5);

			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM-15);

			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			// set some language-dependent strings (optional)
			if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			    require_once(dirname(__FILE__).'/lang/eng.php');
			    $pdf->setLanguageArray($l);
			}

			// ---------------------------------------------------------

			// set font
			$pdf->SetFont('helvetica', '', 10);

		// add a page
		$pdf->AddPage();

		

		// define some HTML content with style


		$table_str = '';

		$i=0;
        foreach ($data['quotation_dtl'] as $key => $value) { 
        	$bg_color = '';
			if($i%2 != 0){
				$bg_color = '#e4e1e1;';
			}

			if(strpos($value['quantity'], '.') === false){
				$value['quantity'] = $value['quantity'].".00";
			}

			if(strpos($value['unit_price'], '.') === false){
				$value['unit_price'] = $value['unit_price'].".00";
			}

			if(strpos($value['row_price'], '.') === false){
				$value['row_price'] = $value['row_price'].".00";
			}
			$table_str .= '<tr style="background-color: '.$bg_color.'; font-size: 11px; font-family: courier;">
				<td style="text-align: right;">'.++$i.'</td>
				<td>'.htmlentities($value['description']).'</td>
				<td style="text-align: right;"> '.$value['quantity'].'</td>
				<td>'.ucwords(strtolower($data['units'][$value['unit']]['unit_value'])).'.</td>';
				// if((int)$value['unit_price'] > 0){

				// 	$table_str .= '<td style="text-align: right;">'.$data['currency']['currency_icon'].$value['unit_price'].'</td>';
				// } else {

				// 	$table_str .= '<td style="text-align: right;">REGRET</td>';
				// }
				// $table_str .= '<td>P.'.ucwords(strtolower($data['units'][$value['unit']]['unit_value'])).'.</td>';
				// if((int)$value['row_price'] > 0){

				// 	$table_str .= '<td style="text-align: right;">'.$data['currency']['currency_icon'].$value['row_price'].'</td>';
				// } else {

				// 	$table_str .= '<td style="text-align: right;">REGRET</td>';
				// }
				if(!in_array($this->session->userdata('user_id'), array(209, 216))){

					if($value['unit_price'] > 0 || $value['row_price'] > 0) {
						
						if(!in_array($this->session->userdata('user_id'), array(191))){
							$table_str .= '<td style="text-align: right;">'.$data['currency']['currency_icon'].$value['unit_price'].'</td>';
							$table_str .= '<td>P.'.ucwords(strtolower($data['units'][$value['unit']]['unit_value'])).'.</td>';
							$table_str .= '<td style="text-align: right;">'.$data['currency']['currency_icon'].$value['row_price'].'</td>';
						}
					}else {
						
						if(!in_array($this->session->userdata('user_id'), array(191))){
							$table_str .= '<td style="text-align: right;">REGRET</td>';
							$table_str .= '<td>P.'.ucwords(strtolower($data['units'][$value['unit']]['unit_value'])).'.</td>';
							$table_str .= '<td style="text-align: right;">REGRET</td>';
						}
					}
				}
			$table_str .= '</tr>';
        }

        $net_total = (!empty($quotation_details)) ? $quotation_details['net_total'] : 00;
        if(strpos($net_total, '.') === false){
        	$net_total = $net_total.'.00';
        }
		if(in_array($this->session->userdata('user_id'), array(191))){

			$net_total = 0;
		}
        $total_table = '<table cellspacing="0" cellpadding="10">
		<tr >
		<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;" width="50%" >Net Total</td>
		<td style="font-size: 12px; font-family: courier; border: solid 1px grey;" width="50%">'.$data['currency']['currency_icon'].$net_total.'</td>
		</tr>';

		if($quotation_details['freight'] > 0){
			$freight = $quotation_details['freight'];
	        if(strpos($freight, '.') === false){
	        	$freight = $freight.'.00';
	        }
			$total_table .= '<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Freight Charges</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['currency']['currency_icon'].$freight.'</td>
			</tr>';	
		}
		
		if($quotation_details['bank_charges'] > 0){
			$bank_charges = $quotation_details['bank_charges'];
	        if(strpos($bank_charges, '.') === false){
	        	$bank_charges = $bank_charges.'.00';
	        }
			$total_table .= '<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Bank Charges</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['currency']['currency_icon'].$bank_charges.'</td>
			</tr>';
		}

		if($quotation_details['gst'] > 0 && $data['currency']['currency'] == 'INR'){
			$gst = $quotation_details['gst'];
	        if(strpos($gst, '.') === false){
	        	$gst = $gst.'.00';
	        }
			$total_table .= '<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">GST</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['currency']['currency_icon'].$gst.'</td>
			</tr>';
		}

		if($quotation_details['other_charges'] > 0){
			$other_charges = $quotation_details['other_charges'];
	        if(strpos($other_charges, '.') === false){
	        	$other_charges = $other_charges.'.00';
	        }
			$total_table .= '<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Other Charges</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['currency']['currency_icon'].$other_charges.'</td>
			</tr>';
		}

		if($quotation_details['discount'] > 0){
			$discount = $quotation_details['discount'];
	        if(strpos($discount, '.') === false){
	        	$discount = $discount.'.00';
	        }
			$total_table .= '<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Discount</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['currency']['currency_icon'].$discount.'</td>
			</tr>';
		}

		$grand_total = $quotation_details['grand_total'];
        if(strpos($grand_total, '.') === false){
        	$grand_total = $grand_total.'.00';
        }
		if(in_array($this->session->userdata('user_id'), array(191))){

			$grand_total = 0;
		}
		$total_table .= '<tr>
			<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Grand Total</td>
			<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['currency']['currency_icon'].$grand_total.'</td>
		</tr>
		</table>';

		$dec_text = 'paise';
		if($data['currency']['currency'] != 'INR'){
			$dec_text = 'cents';
		}
		$grand_total_words = $data['currency']['currency']." : ".$this->numberTowords($grand_total, $dec_text);
		$date = date('d-m-Y', strtotime($quotation_details['entered_on']));
		$client_name = trim($data['clients']['name']);

		$additional_comment = '';

		if($quotation_details['additional_comment'] != ''){
			$additional_comment = '<br/><br/>'.$quotation_details['additional_comment'];
		}

		if(!empty($quotation_details['quote_no'])){
			$type_name = "Quote #";
			$type_value = $quotation_details['quote_no'];
		}else{
			$type_name = "RFQ #";
			$type_value = $data['rfq_details']['rfq_no'];
		}

		$comment = $quotation_details['additional_notes'];

		$display = "block";
		if(in_array($this->session->userdata('user_id'), array(79,85,133,141,179,180,189,205))){ 
			
			$display = "none";			
		}
		$html = '<!-- EXAMPLE OF CSS STYLE -->
		<table cellpadding="5" cellspacing="0">
		<tr style="background-color: #fff;">
		<td width="50%" style="padding:5px;vertical-align: text-top; ">
		<img src="/assets/media/client-logos/logo.png" width="180" height="50" style="padding-left: 10px;"><br/>
		<strong style="font-size: 17px;">OM TUBES & FITTINGS INDUSTRIES</strong>
		<div class="left_addrs" style="font-size: 14px;color: #484545; line-height: 20px;">10 Bordi Bunglow, 1st Panjarapole Lane, CP Tank, Mumbai, Maharashtra, India <br/>GSTIN 27AFRPM5323E1ZC
		</div>
		<table style="margin-top: -15px;">
		<tr style="background-color: #fff;">
		<td style="font-size: 14px; color: #484545; line-height: 20px;" width="50%">+91 (22) 6743 7634</td>
		</tr>
		<tr style="background-color: #fff;">
		<td style="font-size: 14px; color: #484545; line-height: 20px;" align="left">www.omtubes.com</td>
		</tr>
		</table>
		</td>
		<td width="50%">
		<strong style="font-size: 25px; text-align: right; line-height: 35px;">Quotation</strong><br/>
		<table style="line-height: 22px;">
		<tr style="background-color: #e4e1e1; border-bottom: 1px solid;">
		<td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;">'.$type_name.' : </strong></td>
		<td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;">Date : </strong></td>
		</tr>
		<tr>
		<td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;">'.$type_value.'</td>
		<td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;">'.$date.'</td>
		</tr>
		<tr style="background-color: #e4e1e1;">
		<td colspan="2" style="padding:5px;vertical-align: text-top;"><strong style="font-size: 12px;"> Customer:</strong></td>
		</tr>
		<tr>
		<td colspan="2" style="padding:5px;vertical-align: text-top; font-family: courier;">
		<strong style="display:'.$display.';">'.$client_name.'</strong>
		<div class="left_addrs" style="font-size: 11px;color: #484545; line-height: 17px; font-family: courier; display: '.$display.';">'.
			$data['lookup_country']['name'].'<br/>'.
			$data['members']['member_name'].'
		</div>
		</td>
		</tr>
		<tr style="background-color: #e4e1e1;">
		<td colspan="2" style="padding:5px;vertical-align: text-top;"><strong style="font-size: 12px;">Reference :</strong></td>
		</tr>
		<tr>
		<td colspan="2" style="font-family: courier; font-size: 11px;color: #484545;">'.$quotation_details['reference'].'</td>
		</tr>
		</table>
		</td>
		</tr>
		<tr>
		<td colspan="2"></td>
		</tr>
		<tr>
		<td colspan="2">
		<table cellspacing="0" cellpadding="10" border="0">
		<thead>
		<tr style="background-color: #e4e1e1;">
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="7%;">#</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="36%">Item Description</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="12%">Qty</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="8%">Unit</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="13%">Price</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="9%">Unit</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="15%">Total</td>
		</tr>
		</thead>
		<tbody>'.$table_str.'</tbody>
		</table>
		</td>
		</tr>
		<tr>
		<td>
		<strong>Total in words:</strong><br/><span style="font-family: courier; font-size: 11px;">'.$grand_total_words.'</span><br/>
		<hr/>
		<table cellspacing="0" cellpadding="3" border="0">
		<tr>
		<td><strong>Additional Notes</strong></td>
		</tr>
		<tr>
		<td style="font-family: courier; font-size: 11px;">
		We reserve the right to correct the pricing offered due to any typographical errors.<br/>
		This offer is not valid for end users from Iran, Iraq, North Korea, Cuba, Sudan & Syria.'.$quotation_details['comments'].$additional_comment.'
		</td>
		</tr>
		</table>
		</td>
		<td>'.$total_table.'</td>
		</tr>
		<tr>
		<td colspan="2"></td>
		</tr>
		<tr>
		<td colspan="2"></td>
		</tr>
		<tr>
		<td>
		<table>
		<tr>
		<td colspan="2"><strong>Terms and Conditions</strong></td>
		</tr>
		<tr>
		<td width="35%">Delivered To : </td> <td width="65%"><span style="font-family: courier;">'.$data['delivery']['delivery_name'].'</span></td>
		</tr>
		<tr>
		<td>Delivery Time : </td> <td><span style="font-family: courier;">'.$data['delivery_time']['dt_value'].'</span></td>
		</tr>
		<tr>
		<td>Validity : </td> <td><span style="font-family: courier;">'.$data['validity']['validity_value'].'</span></td>
		</tr>
		<tr>
		<td>Currency : </td> <td><span style="font-family: courier;">'.$data['currency']['currency'].'</span></td>
		</tr>
		<tr>
		<td>Country of Origin : </td> <td><span style="font-family: courier;">'.$data['origin_country']['country'].'</span></td>
		</tr>
		<tr>
		<td>MTC Type : </td> <td><span style="font-family: courier;">'.$data['mtc_type']['mtc_value'].'</span></td>
		</tr>
		<tr>
		<td>Packing Type : </td> <td><span style="font-family: courier;">'.$data['transport_mode']['mode'].'</span></td>
		</tr>
		<tr>
		<td>Payment : </td> <td><span style="font-family: courier;">'.$data['payment_terms']['term_value'].'</span></td>
		</tr>
		</table>
		</td>
		<td align="center">
		Thank you for your business<br/>
		For Om Tubes & Fittings Industries<br/><br/>
		<img src="/assets/media/stamp.png" /><br/><br/>
		<table>
		<tr>
		<td width="26%" rowspan="3"></td>
		<td align="left" width="74%">Name : <span style="font-family: courier;">'.$data['users']['name'].'</span></td>
		</tr>
		<tr>
		<td align="left">Email : <span style="font-family: courier;">'.$data['users']['email'].'</span></td>
		</tr>
		<tr>
		<td align="left">Mobile : <span style="font-family: courier;">'.$data['users']['mobile'].'</span></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>';
		if(!empty($data['lookup_country']['name']) && $data['lookup_country']['name'] == 'Saudi Arabia') {

			$html .= '<div style="width:100%">
				<img src="/assets/media/saudi_extra_banner.png" style="padding: 2% 0% 0% 20%;"/>
			</div>';
		}else if (!empty($data['lookup_country']['name']) && $data['lookup_country']['name'] == 'Qatar'){

			$html .= '<div style="width:70%">
				<img src="/assets/media/qatar_extra_banner.png" style="padding: 2% 10% 10% 20%;"/>
			</div>';
		}

		/*$html = <<<HTMLEND
		blah blah blah
		HTMLEND;*/

		// output the HTML content
		$pdf->writeHTML($html, true, false, true, false, '');

		//Close and output PDF document
		$pdf->Output(str_replace('/', '-', $quotation_details['quote_no']).'.pdf', 'I');
	}

	function in_pdf($quote_id){

		redirect('pdf_management/quotation_pdf/'.$quote_id, 'refresh');
		// $data['quote_details'] = $this->quotation_model->getQuotationDetails($quote_id);
		$data = array();
		$quotation_details = $this->quotation_model->get_dynamic_data('*', array('quotation_mst_id' => $quote_id), 'quotation_mst', 'row_array');
		// echo "<pre>";print_r($quotation_details);echo"</pre><hr>";exit;
		if(!empty($quotation_details)) {

			$data['rfq_details'] = $this->quotation_model->get_dynamic_data('*', array('rfq_mst_id' => $quotation_details['rfq_id']), 'rfq_mst', 'row_array');

			// INNER JOIN `quotation_dtl` `d` ON `m`.`quotation_mst_id` = `d`.`quotation_mst_id`
			$data['quotation_dtl'] = $this->quotation_model->get_dynamic_data('*', array('quotation_mst_id' => $quotation_details['quotation_mst_id']), 'quotation_dtl', 'result_array'); 
			if(!empty($quotation_details['client_id'])) {

				// INNER JOIN `clients` `c` ON `c`.`client_id` = `m`.`client_id`  
				$data['clients'] = $this->quotation_model->get_dynamic_data('*', array('id' => $quotation_details['client_id']), 'customer_mst', 'row_array');
			}
			$data['members']['member_name'] = '';
			if(!empty($quotation_details['member_id'])) {

				// INNER JOIN `members` `mb` ON `mb`.`member_id` = `m`.`member_id` 
				$data['members'] = $this->quotation_model->get_dynamic_data('*', array('comp_dtl_id' => $quotation_details['member_id']), 'customer_dtl', 'row_array'); 
			}
			if(!empty($quotation_details['transport_mode'])) {

				// INNER JOIN `transport_mode` `t` ON `m`.`transport_mode` = `t`.`mode_id`
				$data['transport_mode'] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'mode_id' => $quotation_details['transport_mode']), 'transport_mode', 'row_array'); 
			}
			if(!empty($quotation_details['payment_term'])) {

				//  INNER JOIN `payment_terms` `pt` ON `m`.`payment_term` = `pt`.`term_id` 
				$data['payment_terms'] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'term_id' => $quotation_details['payment_term']), 'payment_terms', 'row_array'); 
			}
			if(!empty($quotation_details['delivered_through'])) {

				//  INNER JOIN `delivery` `dl` ON `m`.`delivered_through` = `dl`.`delivery_id` 
				$data['delivery'] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'delivery_id' => $quotation_details['delivered_through']), 'delivery', 'row_array'); 
			}
			if(!empty($quotation_details['delivery_time'])) {

				//  INNER JOIN `delivery_time` `dt` ON `m`.`delivery_time` = `dt`.`dt_id` 
				$data['delivery_time'] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'dt_id' => $quotation_details['delivery_time']), 'delivery_time', 'row_array'); 
			}
			if(!empty($quotation_details['origin_country'])) {

				//  INNER JOIN `origin_country` `oc` ON `m`.`origin_country` = `oc`.`country_id`
				$data['origin_country'] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'country_id' => $quotation_details['origin_country']), 'origin_country', 'row_array'); 
			}
			if(!empty($quotation_details['currency'])) {

				//   INNER JOIN `currency` `cr` ON `m`.`currency` = `cr`.`currency_id` 
				$data['currency'] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'currency_id' => $quotation_details['currency']), 'currency', 'row_array'); 
			}
			if(!empty($quotation_details['validity'])) {

				//   INNER JOIN `validity` `v` ON `m`.`validity` = `v`.`validity_id`
				$data['validity'] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'validity_id' => $quotation_details['validity']), 'validity', 'row_array'); 
			}
			if(!empty($quotation_details['mtc_type'])) {

				//    INNER JOIN `mtc_type` `mt` ON `m`.`mtc_type` = `mt`.`mtc_id` 
				$data['mtc_type'] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'mtc_id' => $quotation_details['mtc_type']), 'mtc_type', 'row_array'); 
			}
			if(!empty($data['quotation_dtl'])) {

				//    INNER JOIN `units` `u` ON `d`.`unit` = `u`.`unit_id` 
				foreach (array_column($data['quotation_dtl'], 'unit','unit') as $key => $value) {
					$data['units'][$key] = $this->quotation_model->get_dynamic_data('*', array('status'=> 'Active', 'unit_id ' => $value), 'units', 'row_array');
				}	
			}
			if(!empty($quotation_details['assigned_to'])) {

				//    INNER JOIN `users` `us` ON `m`.`assigned_to` = `us`.`user_id` 
				$data['users'] = $this->quotation_model->get_dynamic_data('*', array('user_id' => $quotation_details['assigned_to']), 'users', 'row_array'); 
			}
			if(!empty($data['clients']['country_id'])) {

				//    LEFT JOIN `lookup` `lc` ON `lc`.`lookup_id` = `c`.`country` 
				$data['lookup_country'] = $this->quotation_model->get_dynamic_data('*', array('id' => $data['clients']['country_id']), 'country_mst', 'row_array'); 
			}
			if(!empty($data['clients']['region_id'])) {

				//    LEFT JOIN `lookup` `lr` ON `lr`.`lookup_id` = `c`.`region`
				$data['lookup_region'] = $this->quotation_model->get_dynamic_data('*', array('id' => $data['clients']['region_id']), 'region_mst', 'row_array'); 
			}
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		}
		// echo "<pre>";print_r($quotation_details);echo"</pre><hr>";exit;

		if($this->session->userdata('role') == 5 || $this->session->userdata('role') == 16){
			$this->quotation_model->updateData('quotation_mst', array('is_new' => NULL), array('quotation_mst_id' => $quote_id));
		}
		if(($data['delivery']['delivery_name'] == 'CFR Port Name' || $data['delivery']['delivery_name'] == 'CIF Port Name') && $data['transport_mode']['mode'] == 'Sea Worthy'){
			$port_name = $this->quotation_model->getPortName('CIF', 'sea', $data['lookup_country']['name']);
			if($data['delivery']['delivery_name'] == 'CFR Port Name'){
				$data['delivery']['delivery_name'] = 'CFR '.$port_name;
			} else if($data['delivery']['delivery_name'] == 'CIF Port Name'){
				$data['delivery']['delivery_name'] = 'CIF '.$port_name;
			}
		}
		$this->load->library('Pdf');
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

			// set document information
			

			// set default header data
			$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, NULL, '', array(0,0,0), array(255,255,255));
			$pdf->SetFooterData(array(0,0,0), array(255,255,255));

			// set header and footer fonts
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			// set margins
			$pdf->SetMargins(10, PDF_MARGIN_TOP-10, 10);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER-5);

			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM-15);

			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			// set some language-dependent strings (optional)
			if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			    require_once(dirname(__FILE__).'/lang/eng.php');
			    $pdf->setLanguageArray($l);
			}

			// ---------------------------------------------------------

			// set font
			$pdf->SetFont('helvetica', '', 10);

		// add a page
		$pdf->AddPage();

		

		// define some HTML content with style


		$table_str = '';

		$i=0;
        foreach ($data['quotation_dtl'] as $key => $value) { 
        	$bg_color = '';
			if($i%2 != 0){
				$bg_color = '#e4e1e1;';
			}

			if(strpos($value['quantity'], '.') === false){
				$value['quantity'] = $value['quantity'].".00";
			}

			if(strpos($value['unit_price'], '.') === false){
				$value['unit_price'] = $value['unit_price'].".00";
			}

			if(strpos($value['row_price'], '.') === false){
				$value['row_price'] = $value['row_price'].".00";
			}
			$table_str .= '<tr style="background-color: '.$bg_color.'; font-size: 11px; font-family: courier;">
				<td style="text-align: right;">'.++$i.'</td>
				<td>'.htmlentities($value['description']).'</td>
				<td style="text-align: right;"> '.$value['quantity'].'</td>
				<td>'.ucwords(strtolower($data['units'][$value['unit']]['unit_value'])).'.</td>';
				// if((int)$value['unit_price'] > 0){

				// 	$table_str .= '<td style="text-align: right;">'.$data['currency']['currency_icon'].$value['unit_price'].'</td>';
				// } else {

				// 	$table_str .= '<td style="text-align: right;">REGRET</td>';
				// }
				// $table_str .= '<td>P.'.ucwords(strtolower($data['units'][$value['unit']]['unit_value'])).'.</td>';
				// if((int)$value['row_price'] > 0){

				// 	$table_str .= '<td style="text-align: right;">'.$data['currency']['currency_icon'].$value['row_price'].'</td>';
				// } else {

				// 	$table_str .= '<td style="text-align: right;">REGRET</td>';
				// }
				if(!in_array($this->session->userdata('user_id'), array(209, 216))){

					if($value['unit_price'] > 0 || $value['row_price'] > 0) {
						
						if(!in_array($this->session->userdata('user_id'), array(191))){
							$table_str .= '<td style="text-align: right;">'.$data['currency']['currency_icon'].$value['unit_price'].'</td>';
							$table_str .= '<td>P.'.ucwords(strtolower($data['units'][$value['unit']]['unit_value'])).'.</td>';
							$table_str .= '<td style="text-align: right;">'.$data['currency']['currency_icon'].$value['row_price'].'</td>';
						}
					}else {

						if(!in_array($this->session->userdata('user_id'), array(191))){
							$table_str .= '<td style="text-align: right;">REGRET</td>';
							$table_str .= '<td>P.'.ucwords(strtolower($data['units'][$value['unit']]['unit_value'])).'.</td>';
							$table_str .= '<td style="text-align: right;">REGRET</td>';
						}
					}
				}
			$table_str .= '</tr>';
        }

        $net_total = (!empty($quotation_details)) ? $quotation_details['net_total'] : 00;
        if(strpos($net_total, '.') === false){
        	$net_total = $net_total.'.00';
        }
		if(in_array($this->session->userdata('user_id'), array(191))){

			$net_total = 0;
		}
        $total_table = '<table cellspacing="0" cellpadding="10">
		<tr >
		<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;" width="50%" >Net Total</td>
		<td style="font-size: 12px; font-family: courier; border: solid 1px grey;" width="50%">'.$data['currency']['currency_icon'].$net_total.'</td>
		</tr>';

		if($quotation_details['freight'] > 0){
			$freight = $quotation_details['freight'];
	        if(strpos($freight, '.') === false){
	        	$freight = $freight.'.00';
	        }
			$total_table .= '<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Freight Charges</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['currency']['currency_icon'].$freight.'</td>
			</tr>';	
		}
		
		if($quotation_details['bank_charges'] > 0){
			$bank_charges = $quotation_details['bank_charges'];
	        if(strpos($bank_charges, '.') === false){
	        	$bank_charges = $bank_charges.'.00';
	        }
			$total_table .= '<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Bank Charges</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['currency']['currency_icon'].$bank_charges.'</td>
			</tr>';
		}

		if($quotation_details['gst'] > 0 && $data['currency']['currency'] == 'INR'){
			$gst = $quotation_details['gst'];
	        if(strpos($gst, '.') === false){
	        	$gst = $gst.'.00';
	        }
			$total_table .= '<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">GST</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['currency']['currency_icon'].$gst.'</td>
			</tr>';
		}

		if($quotation_details['other_charges'] > 0){
			$other_charges = $quotation_details['other_charges'];
	        if(strpos($other_charges, '.') === false){
	        	$other_charges = $other_charges.'.00';
	        }
			$total_table .= '<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Other Charges</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['currency']['currency_icon'].$other_charges.'</td>
			</tr>';
		}

		if($quotation_details['discount'] > 0){
			$discount = $quotation_details['discount'];
	        if(strpos($discount, '.') === false){
	        	$discount = $discount.'.00';
	        }
			$total_table .= '<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Discount</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['currency']['currency_icon'].$discount.'</td>
			</tr>';
		}

		$grand_total = $quotation_details['grand_total'];
        if(strpos($grand_total, '.') === false){
        	$grand_total = $grand_total.'.00';
        }
		if(in_array($this->session->userdata('user_id'), array(191))){

			$grand_total = 0;
		}
		$total_table .= '<tr>
			<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Grand Total</td>
			<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['currency']['currency_icon'].$grand_total.'</td>
		</tr>
		</table>';
		

		$dec_text = 'paise';
		if($data['currency']['currency'] != 'INR'){
			$dec_text = 'cents';
		}
		$grand_total_words = $data['currency']['currency']." : ".$this->numberTowords($grand_total, $dec_text);
		$date = date('d-m-Y', strtotime($quotation_details['entered_on']));
		$client_name = trim($data['clients']['name']);

		$additional_comment = '';

		if($quotation_details['additional_comment'] != ''){
			$additional_comment = '<br/><br/>'.$quotation_details['additional_comment'];
		}

		if(!empty($quotation_details['quote_no'])){
			$type_name = "Quote #";
			$type_value = $quotation_details['quote_no'];
		}else{
			$type_name = "RFQ #";
			$type_value = $data['rfq_details']['rfq_no'];
		}

		$comment = $quotation_details['additional_notes'];

		$display = "block";
		if(in_array($this->session->userdata('user_id'), array(79,85,133,141,179,180,189,205))){ 
			
			$display = "none";			
		}
		$html = '<!-- EXAMPLE OF CSS STYLE -->
		<table cellpadding="5" cellspacing="0">
		<tr style="background-color: #fff;">
		<td width="50%" style="padding:5px;vertical-align: text-top; ">
		<img src="/assets/media/client-logos/Instinox_logo.png" width="180" height="50" style="padding-left: 10px;"><br/>
		<strong style="font-size: 17px;">INSTINOX LLP</strong>
		<div class="left_addrs" style="font-size: 14px;color: #484545; line-height: 20px;">Gr,3,32, Carpenter 2nd street, CP Tank, Mumbai 400004, India <br/>GSTIN 27AAIFI3989G1ZW
		</div>
		<table style="margin-top: -15px;">
		<tr style="background-color: #fff;">
		<td style="font-size: 14px; color: #484545; line-height: 20px;" width="50%">+91 (22) 6743 9101</td>
		</tr>
		<tr style="background-color: #fff;">
		<td style="font-size: 14px; color: #484545; line-height: 20px;" align="left">sales@instinox.com </td>
		</tr>
		</table>
		</td>
		<td width="50%">
		<strong style="font-size: 25px; text-align: right; line-height: 35px;">Quotation</strong><br/>
		<table style="line-height: 22px;">
		<tr style="background-color: #e4e1e1; border-bottom: 1px solid;">
		<td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;">'.$type_name.' : </strong></td>
		<td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;">Date : </strong></td>
		</tr>
		<tr>
		<td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;">'.$type_value.'</td>
		<td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;">'.$date.'</td>
		</tr>
		<tr style="background-color: #e4e1e1;">
		<td colspan="2" style="padding:5px;vertical-align: text-top;"><strong style="font-size: 12px;"> Customer:</strong></td>
		</tr>
		<tr>
		<td colspan="2" style="padding:5px;vertical-align: text-top; font-family: courier;">
		<strong style="display:'.$display.';">'.$client_name.'</strong>
		<div class="left_addrs" style="font-size: 11px;color: #484545; line-height: 17px; font-family: courier; display: '.$display.';">'.
			$data['lookup_country']['name'].'<br/>'.
			$data['members']['member_name'].'
		</div>
		</td>
		</tr>
		<tr style="background-color: #e4e1e1;">
		<td colspan="2" style="padding:5px;vertical-align: text-top;"><strong style="font-size: 12px;">Reference :</strong></td>
		</tr>
		<tr>
		<td colspan="2" style="font-family: courier; font-size: 11px;color: #484545;">'.$quotation_details['reference'].'</td>
		</tr>
		</table>
		</td>
		</tr>
		<tr>
		<td colspan="2"></td>
		</tr>
		<tr>
		<td colspan="2">
		<table cellspacing="0" cellpadding="10" border="0">
		<thead>
		<tr style="background-color: #e4e1e1;">
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="7%;">#</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="36%">Item Description</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="12%">Qty</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="8%">Unit</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="13%">Price</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="9%">Unit</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="15%">Total</td>
		</tr>
		</thead>
		<tbody>'.$table_str.'</tbody>
		</table>
		</td>
		</tr>
		<tr>
		<td>
		<strong>Total in words:</strong><br/><span style="font-family: courier; font-size: 11px;">'.$grand_total_words.'</span><br/>
		<hr/>
		<table cellspacing="0" cellpadding="3" border="0">
		<tr>
		<td><strong>Additional Notes</strong></td>
		</tr>
		<tr>
		<td style="font-family: courier; font-size: 11px;">
		We reserve the right to correct the pricing offered due to any typographical errors.<br/>
		This offer is not valid for end users from Iran, Iraq, North Korea, Cuba, Sudan & Syria.'.$quotation_details['comments'].$additional_comment.'
		</td>
		</tr>
		</table>
		</td>
		<td>'.$total_table.'</td>
		</tr>
		<tr>
		<td colspan="2"></td>
		</tr>
		<tr>
		<td colspan="2"></td>
		</tr>
		<tr>
		<td>
		<table>
		<tr>
		<td colspan="2"><strong>Terms and Conditions</strong></td>
		</tr>
		<tr>
		<td width="35%">Delivered To : </td> <td width="65%"><span style="font-family: courier;">'.$data['delivery']['delivery_name'].'</span></td>
		</tr>
		<tr>
		<td>Delivery Time : </td> <td><span style="font-family: courier;">'.$data['delivery_time']['dt_value'].'</span></td>
		</tr>
		<tr>
		<td>Validity : </td> <td><span style="font-family: courier;">'.$data['validity']['validity_value'].'</span></td>
		</tr>
		<tr>
		<td>Currency : </td> <td><span style="font-family: courier;">'.$data['currency']['currency'].'</span></td>
		</tr>
		<tr>
		<td>Country of Origin : </td> <td><span style="font-family: courier;">'.$data['origin_country']['country'].'</span></td>
		</tr>
		<tr>
		<td>MTC Type : </td> <td><span style="font-family: courier;">'.$data['mtc_type']['mtc_value'].'</span></td>
		</tr>
		<tr>
		<td>Packing Type : </td> <td><span style="font-family: courier;">'.$data['transport_mode']['mode'].'</span></td>
		</tr>
		<tr>
		<td>Payment : </td> <td><span style="font-family: courier;">'.$data['payment_terms']['term_value'].'</span></td>
		</tr>
		</table>
		</td>
		<td align="center">
		Thank you for your business<br/>
		For INSTINOX LLP<br/><br/>
		<img src="/assets/media/client-logos/instinox_stamp.jpeg" /><br/><br/>
		<table>
		<tr>
		<td width="26%" rowspan="3"></td>
		<td align="left" width="74%">Name : <span style="font-family: courier;">'.$data['users']['name'].'</span></td>
		</tr>
		<tr>
		<td align="left">Email : <span style="font-family: courier;">'.$data['users']['email'].'</span></td>
		</tr>
		<tr>
		<td align="left">Mobile : <span style="font-family: courier;">'.$data['users']['mobile'].'</span></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>';
		if(!empty($data['lookup_country']['name']) && $data['lookup_country']['name'] == 'Saudi Arabia') {

			$html .= '<div style="width:100%">
				<img src="/assets/media/saudi_extra_banner.png" style="padding: 2% 0% 0% 20%;"/>
			</div>';
		}else if (!empty($data['lookup_country']['name']) && $data['lookup_country']['name'] == 'Qatar'){

			$html .= '<div style="width:70%">
				<img src="/assets/media/qatar_extra_banner.png" style="padding: 2% 10% 10% 20%;"/>
			</div>';
		}

		/*$html = <<<HTMLEND
		blah blah blah
		HTMLEND;*/

		// output the HTML content
		$pdf->writeHTML($html, true, false, true, false, '');

		//Close and output PDF document
		$pdf->Output(str_replace('/', '-', $quotation_details['quote_no']).'.pdf', 'I');
	}

	function pdf($quote_id){

		redirect('pdf_management/quotation_pdf/'.$quote_id, 'refresh');
		$data['quote_details'] = $this->quotation_model->getQuotationDetails($quote_id);
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		if($this->session->userdata('role') == 5){
			$this->quotation_model->updateData('quotation_mst', array('is_new' => NULL), array('quotation_mst_id' => $quote_id));
		}
		if(!empty($data['quote_details'])) {

			if(($data['quote_details'][0]['delivery_name'] == 'CFR Port Name' || $data['quote_details'][0]['delivery_name'] == 'CIF Port Name') && $data['quote_details'][0]['mode'] == 'Sea Worthy'){
				$port_name = $this->quotation_model->getPortName('CIF', 'sea', $data['quote_details'][0]['country']);
				if($data['quote_details'][0]['delivery_name'] == 'CFR Port Name'){
					$data['quote_details'][0]['delivery_name'] = 'CFR '.$port_name;
				} else if($data['quote_details'][0]['delivery_name'] == 'CIF Port Name'){
					$data['quote_details'][0]['delivery_name'] = 'CIF '.$port_name;
				}
			}
		}
		$this->load->library('Pdf');
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

			// set document information
			

			// set default header data
			$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, NULL, '', array(0,0,0), array(255,255,255));
			$pdf->SetFooterData(array(0,0,0), array(255,255,255));

			// set header and footer fonts
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			// set margins
			$pdf->SetMargins(10, PDF_MARGIN_TOP-10, 10);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER-5);

			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM-15);

			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			// set some language-dependent strings (optional)
			if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			    require_once(dirname(__FILE__).'/lang/eng.php');
			    $pdf->setLanguageArray($l);
			}

			// ---------------------------------------------------------

			// set font
			$pdf->SetFont('helvetica', '', 10);

		// add a page
		$pdf->AddPage();

		

		// define some HTML content with style


		$table_str = '';

		$i=0;
        foreach ($data['quote_details'] as $key => $value) { 
        	$bg_color = '';
			if($i%2 != 0){
				$bg_color = '#e4e1e1;';
			}

			if(strpos($value['quantity'], '.') === false){
				$value['quantity'] = $value['quantity'].".00";
			}

			if(strpos($value['unit_price'], '.') === false){
				$value['unit_price'] = $value['unit_price'].".00";
			}

			if(strpos($value['row_price'], '.') === false){
				$value['row_price'] = $value['row_price'].".00";
			}

			$table_str .= '<tr style="background-color: '.$bg_color.'; font-size: 11px; font-family: courier;">
				<td style="text-align: right;">'.++$i.'</td>
				<td>'.htmlentities($value['description']).'</td>
				<td style="text-align: right;"> '.$value['quantity'].'</td>
				<td>'.ucwords(strtolower($value['unit_value'])).'.</td>
				<td style="text-align: right;">'.$data['quote_details'][0]['currency_icon'].$value['unit_price'].'</td>
				<td>P.'.ucwords(strtolower($value['unit_value'])).'.</td>
				<td style="text-align: right;">'.$data['quote_details'][0]['currency_icon'].$value['row_price'].'</td>
			</tr>';
        }

        $net_total = (!empty($data['quote_details'])) ? $data['quote_details'][0]['net_total'] : 00;
        if(strpos($net_total, '.') === false){
        	$net_total = $net_total.'.00';
        }

        $total_table = '<table cellspacing="0" cellpadding="10">
		<tr >
		<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;" width="50%" >Net Total</td>
		<td style="font-size: 12px; font-family: courier; border: solid 1px grey;" width="50%">'.$data['quote_details'][0]['currency_icon'].$net_total.'</td>
		</tr>';

		if($data['quote_details'][0]['freight'] > 0){
			$freight = $data['quote_details'][0]['freight'];
	        if(strpos($freight, '.') === false){
	        	$freight = $freight.'.00';
	        }
			$total_table .= '<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Freight Charges</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['quote_details'][0]['currency_icon'].$freight.'</td>
			</tr>';	
		}
		
		if($data['quote_details'][0]['bank_charges'] > 0){
			$bank_charges = $data['quote_details'][0]['bank_charges'];
	        if(strpos($bank_charges, '.') === false){
	        	$bank_charges = $bank_charges.'.00';
	        }
			$total_table .= '<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Bank Charges</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['quote_details'][0]['currency_icon'].$bank_charges.'</td>
			</tr>';
		}

		if($data['quote_details'][0]['gst'] > 0 && $data['quote_details'][0]['currency'] == 'INR'){
			$gst = $data['quote_details'][0]['gst'];
	        if(strpos($gst, '.') === false){
	        	$gst = $gst.'.00';
	        }
			$total_table .= '<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">GST</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['quote_details'][0]['currency_icon'].$gst.'</td>
			</tr>';
		}

		if($data['quote_details'][0]['other_charges'] > 0){
			$other_charges = $data['quote_details'][0]['other_charges'];
	        if(strpos($other_charges, '.') === false){
	        	$other_charges = $other_charges.'.00';
	        }
			$total_table .= '<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Other Charges</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['quote_details'][0]['currency_icon'].$other_charges.'</td>
			</tr>';
		}

		if($data['quote_details'][0]['discount'] > 0){
			$discount = $data['quote_details'][0]['discount'];
	        if(strpos($discount, '.') === false){
	        	$discount = $discount.'.00';
	        }
			$total_table .= '<tr>
				<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Discount</td>
				<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['quote_details'][0]['currency_icon'].$discount.'</td>
			</tr>';
		}

		$grand_total = $data['quote_details'][0]['grand_total'];
        if(strpos($grand_total, '.') === false){
        	$grand_total = $grand_total.'.00';
        }
		$total_table .= '<tr>
			<td style="background-color: #e4e1e1; font-size: 12px; border: solid 1px grey;">Grand Total</td>
			<td style="font-size: 12px; font-family: courier; border: solid 1px grey;">'.$data['quote_details'][0]['currency_icon'].$grand_total.'</td>
		</tr>
		</table>';

		$dec_text = 'paise';
		if($data['quote_details'][0]['currency'] != 'INR'){
			$dec_text = 'cents';
		}
		$grand_total_words = $data['quote_details'][0]['currency']." : ".$this->numberTowords($grand_total, $dec_text);
		$date = date('d-m-Y', strtotime($data['quote_details'][0]['entered_on']));
		$client_name = trim($data['quote_details'][0]['client_name']);

		$additional_comment = '';

		if($data['quote_details'][0]['additional_comment'] != ''){
			$additional_comment = '<br/><br/>'.$data['quote_details'][0]['additional_comment'];
		}

		$display = "block";
		if(in_array($this->session->userdata('user_id'), array(79,85,133,141,179,180,189, 205, 210))){ 
			
			$display = "none";			
		}

		$html = '<!-- EXAMPLE OF CSS STYLE -->
		<table cellpadding="5" cellspacing="0">
		<tr style="background-color: #fff;">
		<td width="50%" style="padding:5px;vertical-align: text-top; ">
		<img src="/assets/media/client-logos/logo.png" width="180" height="50" style="padding-left: 10px;"><br/>
		<strong style="font-size: 17px;">OM TUBES & FITTINGS INDUSTRIES</strong>
		<div class="left_addrs" style="font-size: 14px;color: #484545; line-height: 20px;">10 Bordi Bunglow, 1st Panjarapole Lane, CP Tank, Mumbai, Maharashtra, India <br/>GSTIN 27AFRPM5323E1ZC
		</div>
		<table style="margin-top: -15px;">
		<tr style="background-color: #fff;">
		<td style="font-size: 14px; color: #484545; line-height: 20px;" width="50%">+91 (22) 6743 7634</td>
		</tr>
		<tr style="background-color: #fff;">
		<td style="font-size: 14px; color: #484545; line-height: 20px;" align="left">www.omtubes.com</td>
		</tr>
		</table>
		</td>
		<td width="50%">
		<strong style="font-size: 25px; text-align: right; line-height: 35px;">Quotation</strong><br/>
		<table style="line-height: 22px;">
		<tr style="background-color: #e4e1e1; border-bottom: 1px solid;">
		<td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;"> Quote # : </strong></td>
		<td style="padding:5px;vertical-align: text-top;" width="50%"><strong style="font-size: 12px;">Date : </strong></td>
		</tr>
		<tr>
		<td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;">'.$data['quote_details'][0]['quote_no'].'</td>
		<td style="padding:5px;vertical-align: text-top; font-size: 11px; font-family: courier;">'.$date.'</td>
		</tr>
		<tr style="background-color: #e4e1e1;">
		<td colspan="2" style="padding:5px;vertical-align: text-top;"><strong style="font-size: 12px;"> Customer:</strong></td>
		</tr>
		<tr>
		<td colspan="2" style="padding:5px;vertical-align: text-top; font-family: courier;">
		<strong style="display:'.$display.';">'.$client_name.'</strong>
		<div class="left_addrs" style="font-size: 11px;color: #484545; line-height: 17px; font-family: courier;" style="display:'.$display.';">'.$data['quote_details'][0]['country'].'<br/>'.$data['quote_details'][0]['name'].'
		</div>
		</td>
		</tr>
		<tr style="background-color: #e4e1e1;">
		<td colspan="2" style="padding:5px;vertical-align: text-top;"><strong style="font-size: 12px;">Reference :</strong></td>
		</tr>
		<tr>
		<td colspan="2" style="font-family: courier; font-size: 11px;color: #484545;">'.$data['quote_details'][0]['reference'].'</td>
		</tr>
		</table>
		</td>
		</tr>
		<tr>
		<td colspan="2"></td>
		</tr>
		<tr>
		<td colspan="2">
		<table cellspacing="0" cellpadding="10" border="0">
		<thead>
		<tr style="background-color: #e4e1e1;">
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="7%;">#</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="36%">Item Description</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="12%">Qty</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="8%">Unit</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="13%">Price</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="9%">Unit</td>
		<td class="table_heading" style="font-weight: bold;padding:5px;vertical-align: text-top; font-size: 12px;" width="15%">Total</td>
		</tr>
		</thead>
		<tbody>'.$table_str.'</tbody>
		</table>
		</td>
		</tr>
		<tr>
		<td>
		<strong>Total in words:</strong><br/><span style="font-family: courier; font-size: 11px;">'.$grand_total_words.'</span><br/>
		<hr/>
		<table cellspacing="0" cellpadding="3" border="0">
		<tr>
		<td><strong>Additional Notes</strong></td>
		</tr>
		<tr>
		<td style="font-family: courier; font-size: 11px;">
		We reserve the right to correct the pricing offered due to any typographical errors.<br/>
		This offer is not valid for end users from Iran, Iraq, North Korea, Cuba, Sudan & Syria.'.$additional_comment.'
		</td>
		</tr>
		</table>
		</td>
		<td>'.$total_table.'</td>
		</tr>
		<tr>
		<td colspan="2"></td>
		</tr>
		<tr>
		<td colspan="2"></td>
		</tr>
		<tr>
		<td>
		<table>
		<tr>
		<td colspan="2"><strong>Terms and Conditions</strong></td>
		</tr>
		<tr>
		<td width="35%">Delivered To : </td> <td width="65%"><span style="font-family: courier;">'.$data['quote_details'][0]['delivery_name'].'</span></td>
		</tr>
		<tr>
		<td>Delivery Time : </td> <td><span style="font-family: courier;">'.$data['quote_details'][0]['dt_value'].'</span></td>
		</tr>
		<tr>
		<td>Validity : </td> <td><span style="font-family: courier;">'.$data['quote_details'][0]['validity_value'].'</span></td>
		</tr>
		<tr>
		<td>Currency : </td> <td><span style="font-family: courier;">'.$data['quote_details'][0]['currency'].'</span></td>
		</tr>
		<tr>
		<td>Country of Origin : </td> <td><span style="font-family: courier;">'.$data['quote_details'][0]['origin'].'</span></td>
		</tr>
		<tr>
		<td>MTC Type : </td> <td><span style="font-family: courier;">'.$data['quote_details'][0]['mtc_value'].'</span></td>
		</tr>
		<tr>
		<td>Packing Type : </td> <td><span style="font-family: courier;">'.$data['quote_details'][0]['mode'].'</span></td>
		</tr>
		<tr>
		<td>Payment : </td> <td><span style="font-family: courier;">'.$data['quote_details'][0]['term_value'].'</span></td>
		</tr>
		</table>
		</td>
		<td align="center">
		Thank you for your business<br/>
		For Om Tubes & Fittings Industries<br/><br/>
		<img src="/assets/media/stamp.png" /><br/><br/>
		<table>
		<tr>
		<td width="26%" rowspan="3"></td>
		<td align="left" width="74%">Name : <span style="font-family: courier;">'.$data['quote_details'][0]['uname'].'</span></td>
		</tr>
		<tr>
		<td align="left">Email : <span style="font-family: courier;">'.$data['quote_details'][0]['uemail'].'</span></td>
		</tr>
		<tr>
		<td align="left">Mobile : <span style="font-family: courier;">'.$data['quote_details'][0]['umobile'].'</span></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>';

		/*$html = <<<HTMLEND
		blah blah blah
		HTMLEND;*/

		// output the HTML content
		$pdf->writeHTML($html, true, false, true, false, '');

		//Close and output PDF document
		$pdf->Output(str_replace('/', '-', $data['quote_details'][0]['quote_no']).'.pdf', 'I');
	}	

	function numberTowords($num, $dec_text)
	{

		$ones = array(
		'' =>"ZERO",
		0 =>"ZERO",
		1 => "ONE",
		2 => "TWO",
		3 => "THREE",
		4 => "FOUR",
		5 => "FIVE",
		6 => "SIX",
		7 => "SEVEN",
		8 => "EIGHT",
		9 => "NINE",
		10 => "TEN",
		11 => "ELEVEN",
		12 => "TWELVE",
		13 => "THIRTEEN",
		14 => "FOURTEEN",
		15 => "FIFTEEN",
		16 => "SIXTEEN",
		17 => "SEVENTEEN",
		18 => "EIGHTEEN",
		19 => "NINETEEN",
		"014" => "FOURTEEN"
		);
		$tens = array( 
		0 => "ZERO",
		1 => "TEN",
		2 => "TWENTY",
		3 => "THIRTY", 
		4 => "FORTY", 
		5 => "FIFTY", 
		6 => "SIXTY", 
		7 => "SEVENTY", 
		8 => "EIGHTY", 
		9 => "NINETY" 
		); 
		$hundreds = array( 
		"HUNDRED", 
		"THOUSAND", 
		"MILLION", 
		"BILLION", 
		"TRILLION", 
		"QUARDRILLION" 
		); /*limit t quadrillion */
		$num = number_format($num,2,".",","); 
		$num_arr = explode(".",$num); 
		$wholenum = $num_arr[0]; 
		$decnum = $num_arr[1]; 
		$whole_arr = array_reverse(explode(",",$wholenum)); 
		krsort($whole_arr,1); 
		$rettxt = ""; 
		foreach($whole_arr as $key => $i){
			
		while(substr($i,0,1)=="0")
				$i=substr($i,1,5);
		if($i < 20){ 
		/* echo "getting:".$i; */
		$rettxt .= $ones[$i]; 
		}elseif($i < 100){ 
		if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)]; 
		if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)]; 
		}else{ 
		if(substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
		if(substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)]; 
		if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)]; 
		} 
		if($key > 0){ 
		$rettxt .= " ".$hundreds[$key]." "; 
		}
		} 
		if($decnum > 0){
			$rettxt .= " and ";
			if($decnum < 20){
				if(!empty($ones[$decnum])) {
					$rettxt .= $ones[$decnum];
				}
			}elseif($decnum < 100){
				$rettxt .= $tens[substr($decnum,0,1)];
				$rettxt .= " ".$ones[substr($decnum,1,1)];
			}

			$rettxt .= " ".$dec_text;
		}
		return $rettxt;
	}

	function list($type = ''){
		/*$data['records'] = $this->list_data();*/
		$finYears = $this->quotation_model->getFinancialYears();
		$this->load->view('header', array('title' => 'Quotations List'));
		$this->load->view('sidebar', array('title' => 'Quotations List'));
		$users = $this->quotation_model->getData('users', '(role = 5  OR role = 16) and status = 1');
		$user_str = '';
		foreach ($users as $key => $value) {
			$user_str .= '<option value="'.$value['user_id'].'">'.$value['name'].'</option>';
		};

		$countries = $this->quotation_model->getData('country_mst', 'status = "Active"');
		// $countries = $this->quotation_model->getData('lookup', 'lookup_group = 2');
		$country_str = '';
		foreach ($countries as $key => $value) {
			$country_str .= '<option value="'.$value['id'].'">'.$value['name'].'</option>';
		};

		$region = $this->quotation_model->getData('region_mst', 'status = "Active"');
		// $region = $this->quotation_model->getData('lookup', 'lookup_group = 1');
		$region_str = '';
		foreach ($region as $key => $value) {
			$region_str .= '<option value="'.$value['id'].'">'.$value['name'].'</option>';
		};
		// unset($finYears[0]);
		// echo "<pre>";print_r($finYears);echo"</pre><hr>";exit;
		$this->load->view('quotation_list_view', array('type' => $type, 'finYears' => $finYears, 'user_str'=>$user_str, 'country_str'=>$country_str, 'region_str'=>$region_str, 'sales_person' => $users));
		$this->load->view('footer');
	}

	function list_data($type=''){

		foreach ($this->input->post('columns') as $key => $value) {
			$search_key = '';
			
			if(in_array($this->session->userdata('role'), array(1, 5, 16))){
					if($key == 1){
						$search_key = 'quote_no';
					}else if($key == 2){
						$search_key = 'assigned_to';
					}else if($key == 3){
						$search_key = 'entered_on';
					}else if($key == 4){
						$search_key = 'client_name';
					}else if($key == 5){
						$search_key = 'grand_total';
					}else if($key == 6){
						$search_key = 'country';
					}else if($key == 7){
						$search_key = 'region';
					}else if($key == 8){
						$search_key = 'followup_date';
					}else if($key == 9){
						$search_key = 'importance';
					}else if($key == 10){
						$search_key = 'status';
					}
				}else{
					if($key == 1){
						$search_key = 'quote_no';
					}else if($key == 2){
						$search_key = 'entered_on';
					}else if($key == 3){
						$search_key = 'client_name';
					}else if($key == 4){
						$search_key = 'grand_total';
					}else if($key == 5){
						$search_key = 'country';
					}else if($key == 6){
						$search_key = 'region';
					}else if($key == 7){
						$search_key = 'followup_date';
					}else if($key == 8){
						$search_key = 'importance';
					}else if($key == 9){
						$search_key = 'status';
					}
				}

			// echo $search_key;		

			if($search_key == 'date' || $search_key == 'fdate'){
				if($this->input->post('columns')[$key]['search']['value'] != ''){
					$search[$search_key] = date('Y-m-d', strtotime($this->input->post('columns')[$key]['search']['value']));	
				}else{
					$search[$search_key] = '';
				}
			}else{
				$search[$search_key] = $this->input->post('columns')[$key]['search']['value'];
			}
		}

		$order_by = $this->input->post('columns')[$this->input->post('order')[0]['column']]['data'];
		$dir = $this->input->post('order')[0]['dir'];
		if($type == ''){
			if($order_by == 'record_id'){
				
				
			}
		}else if($type == 'draft'){
			if($order_by == 'record_id'){
				$order_by = 'entered_on';
				$dir = 'desc';
			}
		}
		
		// $order_by = 'entered_on';
		// $dir = 'desc';		

		$searchByYear = $this->input->post('searchByFinYear');
		$where_array = array();
		if(!empty($this->input->post('quotation_sales_person'))) {

			$where_array['quotation_sales_person'] = $this->input->post('quotation_sales_person');
		}
		if(!empty($this->input->post('quotation_country'))) {

			$where_array['quotation_country'] = $this->input->post('quotation_country');
		}
		if(!empty($this->input->post('quotation_region'))) {

			$where_array['quotation_region'] = $this->input->post('quotation_region');
		}
		if(!empty($this->input->post('quotation_priority'))) {

			$where_array['quotation_priority'] = $this->input->post('quotation_priority');
		}
		if(!empty($this->input->post('quotation_is_new'))) {

			$where_array['quotation_is_new'] = $this->input->post('quotation_is_new');
		}
		if(!empty($this->input->post('product_family'))) {

			$where_array['product_family'] = $this->input->post('product_family');
		}
		// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
		// echo "<pre>";print_r($search);echo"</pre><hr>";
		// echo "<pre>";print_r($dir);echo"</pre><hr>";
		// echo "<pre>";print_r($type);echo"</pre><hr>";
		// echo "<pre>";print_r($searchByYear);echo"</pre><hr>";
		// echo "<pre>";print_r($where_array);echo"</pre><hr>";exit;
		$records = $this->quotation_model->getQuotationList($this->input->post('start'), $this->input->post('length'), $search,'quote_no', $dir, $type, $searchByYear, $where_array);
		
		// echo "<pre>";print_r($records);echo"</pre><hr>";exit;
		
		$data['iTotalDisplayRecords'] = $data['iTotalRecords'] = $this->quotation_model->getQuotationListCount($search, $type, $searchByYear);
		$data['aaData'] = $records;
		echo json_encode($data);
	}

	function followup(){
		$this->load->view('header', array('title' => 'Follow Up List'));
		$this->load->view('sidebar', array('title' => 'Follow Up List'));
		//$this->load->view('followup_list_view');
		$users = $this->quotation_model->getData('users', '(role = 5  OR role = 16) and status = 1');
		$user_str = '';
		foreach ($users as $key => $value) {
			$user_str .= '<option value="'.$value['user_id'].'">'.$value['name'].'</option>';
		};

		// $countries = $this->quotation_model->getData('lookup', 'lookup_group = 2');
		// $country_str = '';
		// foreach ($countries as $key => $value) {
		// 	$country_str .= '<option value="'.$value['lookup_id'].'">'.$value['lookup_value'].'</option>';
		// };

		// $region = $this->quotation_model->getData('lookup', 'lookup_group = 1');
		// $region_str = '';
		// foreach ($region as $key => $value) {
		// 	$region_str .= '<option value="'.$value['lookup_id'].'">'.$value['lookup_value'].'</option>';
		// };

		$countries = $this->quotation_model->getData('country_mst', 'status = "Active"');
		$country_str = '';
		foreach ($countries as $key => $value) {
			$country_str .= '<option value="'.$value['id'].'">'.$value['name'].'</option>';
		};

		$region = $this->quotation_model->getData('region_mst', 'status = "Active"');
		$region_str = '';
		foreach ($region as $key => $value) {
			$region_str .= '<option value="'.$value['id'].'">'.$value['name'].'</option>';
		};

		$finYears = $this->quotation_model->getFinancialYears();
		$this->load->view('followup_list_new', array('user_str'=>$user_str, 'country_str'=>$country_str, 'region_str'=>$region_str, 'finYears' => $finYears));
		$this->load->view('footer');
	}

	function followup_data(){
		foreach ($this->input->post('columns') as $key => $value) {
			$search_key = '';
			
			if(in_array($this->session->userdata('role'), array(1, 5, 16))){
					if($key == 1){
						$search_key = 'quote_no';
					}else if($key == 2){
						$search_key = 'assigned_to';
					}else if($key == 3){
						$search_key = 'entered_on';
					}else if($key == 4){
						$search_key = 'client_name';
					}else if($key == 5){
						$search_key = 'grand_total';
					}else if($key == 6){
						$search_key = 'country';
					}else if($key == 7){
						$search_key = 'region';
					}else if($key == 8){
						$search_key = 'followup_date';
					}else if($key == 9){
						$search_key = 'importance';
					}else if($key == 10){
						$search_key = 'status';
					}
				}else{
					if($key == 1){
						$search_key = 'quote_no';
					}else if($key == 2){
						$search_key = 'entered_on';
					}else if($key == 3){
						$search_key = 'client_name';
					}else if($key == 4){
						$search_key = 'grand_total';
					}else if($key == 5){
						$search_key = 'country';
					}else if($key == 6){
						$search_key = 'region';
					}else if($key == 7){
						$search_key = 'followup_date';
					}else if($key == 8){
						$search_key = 'importance';
					}else if($key == 9){
						$search_key = 'status';
					}
				}

			// echo $search_key;		

			if($search_key == 'date' || $search_key == 'fdate'){
				if($this->input->post('columns')[$key]['search']['value'] != ''){
					$search[$search_key] = date('Y-m-d', strtotime($this->input->post('columns')[$key]['search']['value']));	
				}else{
					$search[$search_key] = '';
				}
			}else{
				$search[$search_key] = $this->input->post('columns')[$key]['search']['value'];
			}
		}
		$searchByYear = $this->input->post('searchByFinYear');
		$order_by = $this->input->post('columns')[$this->input->post('order')[0]['column']]['data'];
		if($order_by == 'record_id'){
			$order_by = 'client_name';
		}
		$dir = $this->input->post('order')[0]['dir'];
		$records = $this->quotation_model->getFollowUpList($this->input->post('start'), $this->input->post('length'), $search, $order_by, $dir, $searchByYear);
		
		
		$data['iTotalDisplayRecords'] = $data['iTotalRecords'] = $this->quotation_model->getFollowUpListCount($search);
		$data['aaData'] = $records;
		// echo "<pre>";print_r($data['aaData']);echo"</pre><hr>";exit;
		echo json_encode($data);
	}

	function getFollowUpHistory(){
		$res = $this->quotation_model->getFollowUpHistory($this->input->post('quote_id'));
		echo json_encode($res);
	}

	function getQueryHistory(){
		$res = $this->quotation_model->getQueryHistory($this->input->post('quote_id'), $this->input->post('query_type'));
		echo json_encode($res);
	}

	function addQuery(){
		// echo $this->input->post('query_text').'<br>'; echo $this->input->post('quote_id'); exit();
		
		$res = $this->quotation_model->getData('query_mst', "query_for_id = ".$this->input->post('quote_id')." and query_type = '".$this->input->post('query_type')."'");
		$is_new = false;		
		if(empty($res)){
			// die('came in');
			$query_recepient = $this->quotation_model->getQueryRecepient($this->input->post('quote_id'), $this->input->post('query_type'));
			// echo "<pre>";print_r($query_recepient);echo"</pre><hr>";exit;
			$insert_arr = array(
				'query_for_id' => $this->input->post('quote_id'),
				'query_type' => $this->input->post('query_type'),
				'raised_by' => $this->session->userdata('user_id'),
				'raised_on' => date('Y-m-d H:i:s'),
				'query_status' => 'open',
				'query_recepient' => $query_recepient
			);
			$query_id = $this->quotation_model->insertData('query_mst', $insert_arr);
			$is_new = true;
		}else{
			$query_id = $this->input->post('query_id');
			$this->quotation_model->updateData('query_mst', array('query_status' => $this->input->post('query_status')), array('query_id' => $this->input->post('query_id')));
		}

		$text_arr = array(
			'query_id' => $query_id,
			'query_text' => $this->input->post('query_text'),
			'entered_by' => $this->session->userdata('user_id'),
			'entered_on' => date('Y-m-d H:i:s')
		);
		$this->quotation_model->insertData('query_texts', $text_arr);

		$query_data = $this->quotation_model->getData('query_mst', 'query_id = '.$query_id);
		$query_details = $this->quotation_model->getQueryQuote($query_id);
		if($query_data[0]['raised_by'] == $this->session->userdata('user_id')){
			$sender = $this->quotation_model->getData('users', 'user_id = '.$query_data[0]['raised_by']);
			$rece = $this->quotation_model->getData('users', 'user_id = '.$query_data[0]['query_recepient']);
			$this->send_sms('query_raised', array('name'=> $rece[0]['name'], 'sender'=> $sender[0]['name'], 'quote_str'=> $query_details['quote_str'], 'quote'=> $query_details['quote'], 'query_text'=> $this->input->post('query_text'), 'for_id'=> $query_data[0]['query_recepient'], 'mobile'=> $rece[0]['mobile']));

		}else{
			$sender = $this->quotation_model->getData('users', 'user_id = '.$query_data[0]['query_recepient']);
			$rece = $this->quotation_model->getData('users', 'user_id = '.$query_data[0]['raised_by']);
			$this->send_sms('query_answered', array('name'=> $rece[0]['name'], 'sender'=> $sender[0]['name'], 'quote_str'=> $query_details['quote_str'], 'quote'=> $query_details['quote'], 'query_text'=> $this->input->post('query_text'), 'for_id'=> $query_data[0]['query_recepient'], 'mobile'=> $rece[0]['mobile']));
		}
	}

	function addFollowUp(){
		$insert_arr = array(
			'followedup_on' => date('Y-m-d', strtotime($this->input->post('followedup_on'))),
			'follow_up_text' => $this->input->post('follow_up_text'),
			'quotation_mst_id' => $this->input->post('quote_id'),
			'member_id' => $this->input->post('member_name'),
			'connect_mode' => $this->input->post('connect_mode'),
			'entered_on' => date('Y-m-d H:i:s'),
			'entered_by' => $this->session->userdata('user_id'),
			'email' => $this->input->post('email'),
		);
		// echo "<pre>";print_r($insert_arr);echo"</pre><hr>";exit;
		$this->insert_into_daily_work_sales_on_quotations_data($insert_arr);
		// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";
		$this->quotation_model->insertData('follow_up', $insert_arr);
		
		$this->quotation_model->updateData('quotation_mst', array('followup_date' => date('Y-m-d', strtotime($this->input->post('followup_date')))), array('quotation_mst_id' => $this->input->post('quote_id')));
		if($this->input->post('redirect') == 'quote_page'){
			redirect('quotations/viewQuotation/'.$this->input->post('quote_id'));
		}else if($this->input->post('redirect') == 'list'){
			redirect('quotations/list', 'refresh');
		}else{
			redirect('quotations/followup');
		}
	}
	
	function viewQuotation($quote_id){
		$data['quote_details'] = $this->quotation_model->getQuotationDetails($quote_id);
		$data['follow_up'] = $this->quotation_model->getFollowUpHistory($quote_id);
		$data['siblings'] = $this->quotation_model->getSiblingQuotation($quote_id);
		$data['client_details'] = $this->quotation_model->getClientDetails($quote_id);
		$data['reason'] = $this->quotation_model->getData('close_reason', array('status'=> 'Active'));
		$data['quote_id'] = $quote_id;
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Quotations Details'));
		$this->load->view('sidebar', array('title' => 'Quotations Details'));
		$this->load->view('quotation_view', $data);
		$this->load->view('footer');
	}

	function makeChanges(){
		$update_arr = array(
			'importance' => $this->input->post('importance'),
			'status' => $this->input->post('status'),
		);

		if($this->input->post('reason') && $this->input->post('status') == 'Closed'){
			$update_arr['close_reason'] = $this->input->post('reason');
		}

		$this->quotation_model->updateData('quotation_mst', $update_arr, array('quotation_mst_id' => $this->input->post('quote_id')));
		$this->session->set_flashdata('success', 'Quotation updated successfully');
		redirect('quotations/viewQuotation/'.$this->input->post('quote_id'));
	}

	function deleteQuote(){
		$this->quotation_model->deleteData('quotation_dtl', array('quotation_mst_id' => $this->input->post('quote_id')));
		$this->quotation_model->deleteData('quotation_mst', array('quotation_mst_id' => $this->input->post('quote_id')));
	}

	function addDetails(){
		$type = $this->input->post('type');
		$data = $this->input->post('data');

		switch($type){
			case "delivery_time":
				$new_record_id = $this->quotation_model->insertData('delivery_time', array('dt_value' => $data));
				$all_data = $this->quotation_model->getData('delivery_time');
				$arr = array();
				foreach ($all_data as $key => $value) {
					$arr[$key]['id'] = $value['dt_id'];
					$arr[$key]['value'] = $value['dt_value'];
				}
				break;

			case "payment_terms":
				$new_record_id = $this->quotation_model->insertData('payment_terms', array('term_value' => $data));
				$all_data = $this->quotation_model->getData('payment_terms');
				$arr = array();
				foreach ($all_data as $key => $value) {
					$arr[$key]['id'] = $value['term_id'];
					$arr[$key]['value'] = $value['term_value'];
				}
				break;

			case "validity":
				$new_record_id = $this->quotation_model->insertData('validity', array('validity_value' => $data));
				$all_data = $this->quotation_model->getData('validity');
				$arr = array();
				foreach ($all_data as $key => $value) {
					$arr[$key]['id'] = $value['validity_id'];
					$arr[$key]['value'] = $value['validity_value'];
				}
				break;

			case "origin":
				$new_record_id = $this->quotation_model->insertData('origin_country', array('country' => $data));
				$all_data = $this->quotation_model->getData('origin_country');
				$arr = array();
				foreach ($all_data as $key => $value) {
					$arr[$key]['id'] = $value['country_id'];
					$arr[$key]['value'] = $value['country'];
				}
				break;

			case "unit":
				$new_record_id = $this->quotation_model->insertData('units', array('unit_value' => $data));
				$all_data = $this->quotation_model->getData('units');
				$arr = array();
				foreach ($all_data as $key => $value) {
					$arr[$key]['id'] = $value['unit_id'];
					$arr[$key]['value'] = $value['unit_value'];
				}
				break;

			case "product":
				// $new_record_id = $this->quotation_model->insertData('lookup', array('lookup_value' => $data, 'lookup_group' => 259));
				$new_record_id = $this->quotation_model->insertData('product_mst', array('name' => $data, 'status'=> 'Active'));
				// $all_data = $this->quotation_model->getData('lookup', 'lookup_group = 259');
				$all_data = $this->quotation_model->getData('product_mst', 'status = "Active"');
				$arr = array();
				foreach ($all_data as $key => $value) {
					// $arr[$key]['id'] = $value['lookup_id'];
					// $arr[$key]['value'] = $value['lookup_value'];
					$arr[$key]['id'] = $value['id'];
					$arr[$key]['value'] = $value['name'];
				}
				break;
		}
		echo json_encode(array('new_record_id' => $new_record_id, "records" => $arr));
	}

	function calculateCharge(){
		$weight = $this->input->post('weight');
		$zone_id = $this->input->post('zone_id');

		
		if($weight < 71){
			$mul = 1;
		}else if($weight < 100){
			$mul = $weight;
			$weight = 71; 
		}else if($weight < 300){
			$mul = $weight;
			$weight = 100; 
		}else if($weight < 500){
			$mul = $weight;
			$weight = 300; 
		}else if($weight < 1000){
			$mul = $weight;
			$weight = 500; 
		}else if($weight > 1000){
			$mul = $weight;
			$weight = 1000;
		}
		$res = $this->quotation_model->getData('ddu_charges', 'weight = '.$weight);
		$currency_arr = $this->quotation_model->getData('currency', array('status'=> 'Active'));
		$base_charge_rs = $res[0]['zone_'.$zone_id] * $mul;
		$fuel_surcharge = 0.25 * $base_charge_rs;
		$custom_clearance = 2000;
		$base_total = $base_charge_rs + $fuel_surcharge + $custom_clearance;
		$tax = 0.28 * $base_total;
		$total = $base_total + $tax;
		echo $charge = round($total / $currency_arr[0]['currency_rate'], 2);
	}

	public function sendSms($mobile,$sms_txt, $dlt_id){ 
		// $mobile='9821850733';
		// $mobile='9082159156';
		// $dlt_id='1307161789797960476';
		$curl = curl_init();
		// echo $sms_txt, "<hr>"; die('debug');
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.msg91.com/api/sendhttp.php?authkey=345434ALgG4fZD72t35f941f44P1&mobiles=91".$mobile."&message=".$sms_txt."&sender=OMTUBE&route=4&DLT_TE_ID=".$dlt_id,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_SSL_VERIFYHOST => 0,
		  CURLOPT_SSL_VERIFYPEER => 0,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  echo $response,"<hr>";
		}
	}

	function testSms(){
		// $this->sendSms('919821850733', 'Dear Abc Your quotation Pqr is ready. Submit it to client a.s.a.p.');
		$this->sendSms('9082159156', 'Dear%20Parth%20Brid%20Good%20Morning.%20You%20have%2012%20quotations%20due%20for%20follow%20today.%20Kindly%20check%20with%20clients%20and%20try%20to%20convert%20these%20orders.%20Also%20do%20not%20forget%20to%20update%20status%20in%20CRM%20Have%20a%20great%20day%20ahead.', '1307161789797960476');
		// $this->sendSms($sms_details['mobile'], str_replace(' ', '%20', $sms_text), $dlt_id);
	}

	function daily_followup(){
		$users = $this->quotation_model->getData('users', '(role = 5  OR role = 16) and status = 1');
		foreach ($users as $user) {
			$count = $this->quotation_model->getFollowupCount($user['user_id']);
			if($count > 0){

				$this->send_sms('daily_follow_up', array('name'=> $user['name'], 'count'=> $count, 'mobile'=> $user['mobile']));
				$this->send_whatsapp('daily_follow_up', array('name'=> $user['name'], 'count'=> $count, 'mobile'=> $user['mobile']));
			}
		}
	}

	function query_list($query_type, $query_status){
		$this->load->view('header', array('title' => 'Query List - '.ucfirst(strtolower($query_type)).' - '.ucfirst(strtolower($query_status))));
		$this->load->view('sidebar', array('title' => 'Query List - '.ucfirst(strtolower($query_type)).' - '.ucfirst(strtolower($query_status))));
		/*$purchase_user = $this->quotation_model->getData('users', 'role in (6, 8) and status = 1');
		$user_str = '';
		foreach ($purchase_user as $key => $value) {
			$user_str .= '<option value="'.$value['user_id'].'">'.$value['name'].'</option>';
		};*/
		$this->load->view('query_list_view', array('query_type' => $query_type, 'query_status' => $query_status));
		$this->load->view('footer');
	}

	function query_list_data($query_type, $query_status){
		foreach ($this->input->post('columns') as $key => $value) {
			$search_key = '';
			if($search_key == 'raised_on'){
				if($this->input->post('columns')[$key]['search']['value'] != ''){
					$search[$search_key] = date('Y-m-d', strtotime($this->input->post('columns')[$key]['search']['value']));	
				}else{
					$search[$search_key] = '';
				}
			}else{
				$search[$search_key] = $this->input->post('columns')[$key]['search']['value'];
			}
		}
		$search['query_type'] = $query_type;
		$search['query_status'] = $query_status;

		$order_by = $this->input->post('columns')[$this->input->post('order')[0]['column']]['data'];
		$dir = $this->input->post('order')[0]['dir'];
		if($order_by == 'record_id'){
			$order_by = 'raised_on';
			$dir = 'desc';
		}

		$records = $this->quotation_model->getQueryList($this->input->post('start'), $this->input->post('length'), $search, $order_by, $dir);
		$data['iTotalDisplayRecords'] = $data['iTotalRecords'] = $this->quotation_model->getQueryListCount($search);
		$data['aaData'] = $records;
		echo json_encode($data);
	}

	function uploadPO(){
		if(!empty($_FILES['po_file'])){
			$config['upload_path']          = './assets/purchase_orders/';
            $config['allowed_types']        = 'pdf';//'jpeg|gif|jpg|png';
            $config['max_size']             = 5242880;
            $config['file_name']            = 'PO-'.$this->input->post('quote_id').'-'.time();
            $config['overwrite']            = TRUE;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('po_file'))
            {
				$error = array('error' => $this->upload->display_errors());
				$data = array('status' => 'failed', 'msg' => $error['error']);
            }
            else
            {
            	$file_dtls = $this->upload->data();
	            $data = array('status' => 'success', 'msg' => 'Image uploaded successfully!', 'file_name' => $file_dtls['file_name']);
	            $this->quotation_model->updateData('quotation_mst', array('purchase_order' => $file_dtls['file_name'], 'po_add_time'=>date('Y-m-d')), array('quotation_mst_id' => $this->input->post('quote_id')));
            }
            echo '<script>window.close();</script>';
		}
	}

	public function ajax_function() {

		if($this->input->is_ajax_request()){
			$call_type = $this->input->post('call_type');
			$response['status'] = 'successful';	
			switch ($call_type) {

				case 'quotation_list_update_follow_up':

					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";
					$form_data = array_column($this->input->post('form_data'), 'value', 'name');
					$insert_arr = array(
						'followedup_on' => date('Y-m-d', strtotime($form_data['followedup_on'])),
						'follow_up_text' => $form_data['follow_up_text'],
						'quotation_mst_id' => $form_data['quote_id'],
						'member_id' =>$form_data['member_name'],
						'connect_mode' =>$form_data['connect_mode'],
						'entered_on' => date('Y-m-d H:i:s'),
						'entered_by' => $this->session->userdata('user_id'),
						'email' =>$form_data['email'],
					);
					// echo "<pre>";print_r($insert_arr);echo"</pre><hr>";
					$this->insert_into_daily_work_sales_on_quotations_data($insert_arr);
					$this->quotation_model->insertData('follow_up', $insert_arr);
					$this->quotation_model->updateData('quotation_mst', array('followup_date' => date('Y-m-d', strtotime($form_data['followup_date']))), array('quotation_mst_id' => $form_data['quote_id']));
				break;

				case 'quotation_follow_up_highchart':
					
					$users = $this->quotation_model->getData('users', '(role = 5  OR role = 16) and status = 1');
					// echo "<pre>";print_r($users);echo"</pre><hr>";exit;
					$user_name_array = array_column($users, 'name', 'user_id');
					$filter_type = $this->input->post('filter_date');
					// current time filter
					$date = date('Y-m-d', strtotime('-1 days'));
					if( $filter_type == 'month') {
						$date = date('Y-m-d', strtotime('-31 days'));

					} else if( $filter_type == 'week') {
						$date = date('Y-m-d', strtotime('-7 days'));

					} else if( $filter_type == 'all') {
						$date = '';

					}
					$follow_up_highchart_data = $this->quotation_model->get_follow_list_pending_data_for_highchart($date);
					foreach ($follow_up_highchart_data as $highchart_data) {
						$data['follow_up_highchart_data'][$highchart_data['assigned_to']][] = $highchart_data;
					}
					$i = 0;
					foreach ($data['follow_up_highchart_data'] as $user_id => $highchart_data) {

						$data[$i]['name'] = $user_name_array[$user_id];
						$data[$i]['y'] = (int)count($highchart_data);
						$data[$i]['drilldown'] = $user_name_array[$user_id];
						$i++;
					}
					$sort_array_column = array_column($data, 'y');
					arsort($sort_array_column);
					foreach ($sort_array_column as $sort_array_column_key => $sort_array_column_value) {
						
						$response['highchart_data'][] = $data[$sort_array_column_key]; 						
					}
				break;

				case 'quotation_list_highchart':

					$filter_type = (!empty($this->input->post('filter_date'))) ? $this->input->post('filter_date'):'day';
					$filter_year = (!empty($this->input->post('filter_year'))) ? $this->input->post('filter_year'):'2021';
					$filter_sales_person = (!empty($this->input->post('filter_sales_person'))) ? $this->input->post('filter_sales_person'):'All';
					$response['highchart_data']['category'] = array();
					$response['highchart_data']['series'] = array();
					$start_day = '';
					$end_day = date('Y-m-d 23:59:59');
					$years_list = $this->quotation_model->get_year_list();
					$sales_person = array_column($this->quotation_model->get_sales_user_name('users', '(role = 5  OR role = 16) and status = 1'), 'name','user_id');
					if( $filter_type == 'day') {

						$filter_year = '2021';
						// $start_day = date('Y-m-d 00:00:00', strtotime('last sunday'));
						$start_day = date('Y-m-d 00:00:00', strtotime('- 7 days'));
						$quotation_list = $this->quotation_model->get_quotation_list_data_day_wise($start_day, $end_day, $filter_sales_person);
					}	
					if( $filter_type == 'week') {

						$filter_year = '2021';
						$start_day = date('Y-m-d 00:00:00', strtotime('- 77 days'));
						$quotation_list = $this->quotation_model->get_quotation_list_data_week_wise($start_day, $end_day, $filter_sales_person);
					}	
					if( $filter_type == 'month') {
						
						$quotation_list = $this->quotation_model->get_quotation_list_data_month_wise($filter_year, $filter_sales_person);
					}

					// echo "<pre>";print_r($quotation_list);echo"</pre><hr>";exit;
					foreach ($quotation_list as $single_data) {
						if(!empty($sales_person[$single_data['assigned_to']])){

							$quotation_list_highchart_data[trim($sales_person[$single_data['assigned_to']])][$single_data[$filter_type]][] = $single_data; 
						}
					}
					$week_increment=2;
					foreach (array_column($quotation_list, $filter_type, $filter_type) as $category_value) {
						
						$category = $category_value;
						if( $filter_type == 'week') {

							$week = ($category_value-$week_increment);
							$category = date('d, M', strtotime('-'.$week.' weeks'));
							$week_increment = $week_increment+2;
						}
						$response['highchart_data']['category'][] = $category;
					}
					$i=1;
					$response['highchart_data']['series'][0]['name'] = 'Total';
					foreach ($quotation_list_highchart_data as $sales_person_name => $sales_person_details) {
						
						$date_name_handling_in_increment = 0;
						$response['highchart_data']['series'][$i]['name'] = $sales_person_name;
						foreach ($sales_person_details as $date_name => $single_person_details) {
							
							$response['highchart_data']['series'][0]['data'][$date_name_handling_in_increment] += (int)count($single_person_details);
							$response['highchart_data']['series'][$i]['data'][] = (int)count($single_person_details);
							$date_name_handling_in_increment++;
						}
						$i++;
					}
					$response['html_body']['filter_year'] = $this->load->view('quotations/filter_year', array('filter_year'=>$filter_year, 'years_list'=> $years_list), TRUE);
					
					$response['html_body']['filter_sales_person'] = $this->load->view(
																				'quotations/filter_sales_person',
																				array(
																					'sales_person' => $sales_person,
																					'filter_sales_person' => $filter_sales_person
																				),
																				TRUE);
					$response['html_body']['filter_sales_person_id'] = $filter_sales_person;
					$response['html_body']['filter_sales_person_name'] = ($filter_sales_person != 'All') ? $sales_person[$filter_sales_person] : $filter_sales_person;
					$response['html_body']['filter_date_name'] = $filter_type;
					$response['html_body']['filter_year_name'] = $filter_year;
				break;

				case 'update_rating':
					
					$form_data = array_column($this->input->post('rating_value'), 'value', 'name');
					// echo "<pre>";print_r($form_data);echo"</pre><hr>";
					foreach ($form_data as $form_key => $form_value) {
						
						$explode = explode('_', $form_key);
						if(count($explode) == 4) {
							
							if(empty($form_data['update_quotation_priority_reason_'.$explode[3]])){

								$response['status'] = 'failed';
							} else {
								
								$this->common_model->update_data_sales_db('quotation_mst', array('quotation_priority' => (int)$form_value), array('quotation_mst_id'=>$explode[3]));
							}
							
						} elseif(count($explode) == 5) {

							$this->common_model->update_data_sales_db('quotation_mst', array('priority_reason' => $form_value), array('quotation_mst_id'=>$explode[4]));
						}
					}
				break;

				case 'quotation_close_reason_highchart':

					$response['highchart_data'] = array();
					$where_array = array();
					if(!empty($this->input->post('filter_date'))) {

						$data_explode = explode(' - ', $this->input->post('filter_date'));
						if(count($data_explode) == 1){

							$where_array['entered_on >='] = date('Y-m-d 00:00:00', strtotime($data_explode[0]));
							$where_array['entered_on <='] = date('Y-m-d 23:59:59', strtotime($data_explode[0]));
						}else if(count($data_explode) == 2){

							$where_array['entered_on >='] = date('Y-m-d 00:00:00', strtotime($data_explode[0]));
							$where_array['entered_on <='] = date('Y-m-d 23:59:59', strtotime($data_explode[1]));
						}
					}

					if(!in_array($this->session->userdata('role'), array(1, 16, 17))){

						$where_array['assigned_to'] = $this->session->userdata('user_id');
					}
					$highchart_data = $this->quotation_model->get_quotation_close_reason_piechart_data($where_array, 0);
					$total_quote = array_sum(array_column($highchart_data, 'count'));
					$reason_name = array_column($this->common_model->get_dynamic_data_sales_db('*', array(), 'close_reason'), 'reason_text', 'reason_id');
					foreach ($highchart_data as $single_highchart_data) {
						
						if(!empty($reason_name[$single_highchart_data['close_reason']])) {

				  			$response['highchart_data'][] = array(
				  											'name'=> $reason_name[$single_highchart_data['close_reason']].' ('.$single_highchart_data['count'].')',
				  											'y'=> ((int)round(((int)$single_highchart_data['count']/(int)$total_quote)*100))
				  										);
						}
					}
					if(!empty($response['highchart_data'])) {
						$response['highchart_data'][0]['sliced'] = true;
						$response['highchart_data'][0]['selected'] = true;
					}
				break;

	            case 'delete_po':

	               $this->common_model->update_data_sales_db('quotation_mst', array('purchase_order'=>''), array('quotation_mst_id'=>$this->input->post('quotation_mst_id')));
	            break;

	            case 'get_member_details':
            		
            		// $member_details = $this->common_model->get_all_conditional_data_sales_db('*', array('client_id'=> $this->input->post('client_id')), 'members', 'result_array', array(), array(), array('name'));
            		$member_details = $this->common_model->get_all_conditional_data_sales_db('*', array('comp_mst_id'=> $this->input->post('client_id')), 'customer_dtl', 'result_array', array(), array(), array('member_name'));
            		$response['member_name_html'] = '<option value=""></option>';
            		if(!empty($member_details)) {
            			foreach ($member_details as $key => $value) {
            				
            				$response['member_name_html'] .= '<option value="'.$value['comp_dtl_id'].'">'.$value['member_name'].'</option>';
            			}
            		}
            	break;

				case 'get_client_details':

					
					$client_details = $this->quotation_model->get_client_name($this->input->post('search_data'));
					// echo "<pre>";print_r($client_details);echo"</pre><hr>";exit;
					$response['client_option_tag'] = '<option value="">Select</option>';
					foreach ($client_details as $single_client_details) {
						
						$response['client_option_tag'] .= '<option value="'.$single_client_details['id'].'">'.$single_client_details['name'].'</option>';
					}
					// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
					break;
					
				case 'get_client_member_details':
						
					$members_details = $this->common_model->get_dynamic_data_sales_db('comp_dtl_id, member_name', array('status'=>'Active', 'comp_mst_id'=> $this->input->post('client_id')), 'customer_dtl');
					$response['client_member_option_tag'] = '<option value="">Select</option>';
					foreach ($members_details as $single_client_details) {
						
						$response['client_member_option_tag'] .= '<option value="'.$single_client_details['comp_dtl_id'].'">'.$single_client_details['member_name'].'</option>';
					}
				break;

				default:
					$response['status'] = 'failed';	
					$response['message'] = "call type not found";
					break;
			}
			echo json_encode($response);

		} else{
			die('access is not allowed to this function');
		}
	}
	private function send_sms($sms_type, $sms_details) {

		$date_time = $this->get_indian_time();
		if($date_time >= "2023-08-05 21:00:00" && $date_time <= "2023-08-06 23:59:59"){

			return false;
		}
		$mobile = $sms_txt = $dlt_id = '';
		// $mobile = '9082159156';
		$mobile = $sms_details['mobile'];
		if($sms_type == 'quotation_ready') {

			$sms_txt = 'Dear '.$sms_details['sales_user'].' Your quotation '.$sms_details['quote_no'].' is ready. Submit it to client a.s.a.p.';
			$dlt_id = '1307161789565342668';
			$this->quotation_model->insertData('notifications', array('for_id' => $sms_details['user_id'], 'notify_str' => $sms_txt, 'notify_date' => date('Y-m-d H:i:s'), 'notify_viewed' => 0));

		} else if ($sms_type == 'proforma_ready') {

			$sms_txt = 'Dear '.$sms_details['sales_user'].' Your Proforma '.$sms_details['proforma_no'].' is ready against PO '.$sms_details['rfq_no'].' made by '.$sms_details['purchase_user'].' Submit it to the client a.s.a.p';
			$dlt_id = '1307161789697481288';
			$this->quotation_model->insertData('notifications', array('for_id' => $sms_details['user_id'], 'notify_str' => $sms_txt, 'notify_date' => date('Y-m-d H:i:s'), 'notify_viewed' => 0));

		} else if ($sms_type == 'proforma_ready_2') {

			$sms_txt = 'Dear '.$sms_details['sales_user'].' Your proforma '.$sms_details['proforma_no'].' is ready. Submit it to client a.s.a.p.';
			$dlt_id = '1307161789719855112';
			$this->quotation_model->insertData('notifications', array('for_id' => $sms_details['user_id'], 'notify_str' => $sms_txt, 'notify_date' => date('Y-m-d H:i:s'), 'notify_viewed' => 0));

		} else if($sms_type == 'query_raised'){

			$sms_txt = 'Dear '.$sms_details['name'].' '.$sms_details['sender'].' has raised a query against '.substr($sms_details['quote_str'],0, 25).'.. '.$sms_details['quote'].' Query: ('.substr($sms_details['query_text'], 0, 20).')';

			$dlt_id = '1307161789767950941';
			$this->quotation_model->insertData('notifications', array('for_id' => $sms_details['query_recepient'], 'notify_str' => $sms_text, 'notify_date' => date('Y-m-d H:i:s'), 'notify_viewed' => 0));

		}else if($sms_type == 'query_answered'){

			$sms_txt = 'Dear '.$sms_details['name'].'%0a'.$sms_details['sender'].' has answered to your query against '.substr($sms_details['quote_str'], 0, 25).'.. '.$sms_details['quote'].'%0aQuery: ('.substr($sms_details['query_text'], 0, 20).')';

			$dlt_id = '1307161789779790647';
			$this->quotation_model->insertData('notifications', array('for_id' => $sms_details['query_recepient'], 'notify_str' => $sms_text, 'notify_date' => date('Y-m-d H:i:s'), 'notify_viewed' => 0));

		} else if($sms_type == 'daily_follow_up') {

			$sms_txt = 'Dear '.$sms_details['name'].' Good Morning. You have '.$sms_details['count'].' quotations due for follow today. Kindly check with clients and try to convert these orders. Also do not forget to update status in CRM Have a great day ahead.';
			$dlt_id = '1307161789797960476';
		}

		if(!empty($mobile) && !empty($sms_txt) && !empty($dlt_id)) {

			// echo $mobile,"<hr>", $sms_txt,"<hr>", $dlt_id,"<hr>";
			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api.msg91.com/api/sendhttp.php?authkey=345359A1zGdmFe5f930cf3P1&mobiles=91".$mobile."&message=".urlencode($sms_txt)."&sender=OMTUBE&route=4&DLT_TE_ID=".$dlt_id,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_SSL_VERIFYHOST => 0,
			  CURLOPT_SSL_VERIFYPEER => 0,
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);
			
			if ($err) {
			  echo "cURL Error #:" . $err;
			} else {
			  // echo $response,"<hr>";
			}
		}
	}
	public function send_whatsapp($whatsapp_type, $whatsapp_details){

		$data = array();
		if($whatsapp_type == 'quotation_ready') {

			$data = json_encode(array(
				'broadcast_name' => 'crm7',
				'template_name' => 'crm7',
				'parameters' => array(
					array(
						'name' => 'name',
						'value' => $whatsapp_details['sales_user']
					),
					array(
						'name' => 'quotation',
						'value' => $whatsapp_details['quote_no']
					)
				)
			));

		} else if ($whatsapp_type == 'proforma_ready') {

			$data = json_encode(array(
				'broadcast_name' => 'proforma_po',
				'template_name' => 'proforma_po',
				'parameters' => array(
					array(
						'name' => 'name',
						'value' => $whatsapp_details['sales_user']
					),
					array(
						'name' => 'proforma_no',
						'value' => $whatsapp_details['proforma_no']
					),
					array(
						'name' => 'po_no',
						'value' => $whatsapp_details['rfq_no']
					),
					array(
						'name' => 'name_2',
						'value' => $whatsapp_details['purchase_user']
					),
				)
			));

		} else if ($whatsapp_type == 'proforma_ready_2') {

			$data = json_encode(array(
				'broadcast_name' => 'proforma',
				'template_name' => 'proforma',
				'parameters' => array(
					array(
						'name' => 'name',
						'value' => $whatsapp_details['sales_user']
					),
					array(
						'name' => 'proforma_no',
						'value' => $whatsapp_details['proforma_no']
					)
				)
			));

		} else if($whatsapp_type == 'daily_follow_up') {

			$data = json_encode(array(
				'broadcast_name' => 'follow_up_alert',
				'template_name' => 'follow_up_alert',
				'parameters' => array(
					array(
						'name' => 'name',
						'value' => $whatsapp_details['name']
					),
					array(
						'name' => 'quotation_count',
						'value' => $whatsapp_details['count']
					)
				)
			));
		}

		if(!empty($whatsapp_detail['mobile']) && !empty($data)) {

			// URL and data
			$url = 'https://live-mt-server.wati.io/200110/api/v1/sendTemplateMessage?whatsappNumber=%2B'.$whatsapp_detail['mobile'];

			// Headers
			$headers = array(
				'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0NzVhNGE4OC03ZDQ5LTRjMmItOGM0ZS01N2RlYzNkMGZiZWMiLCJ1bmlxdWVfbmFtZSI6ImNybUBvbXR1YmVzLmNvbSIsIm5hbWVpZCI6ImNybUBvbXR1YmVzLmNvbSIsImVtYWlsIjoiY3JtQG9tdHViZXMuY29tIiwiYXV0aF90aW1lIjoiMTAvMDQvMjAyMyAxMjoyNTowNyIsImRiX25hbWUiOiJtdC1wcm9kLVRlbmFudHMiLCJ0ZW5hbnRfaWQiOiIyMDAxMTAiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJBRE1JTklTVFJBVE9SIiwiZXhwIjoyNTM0MDIzMDA4MDAsImlzcyI6IkNsYXJlX0FJIiwiYXVkIjoiQ2xhcmVfQUkifQ.cFanSeCHT9pYMdMtdjERT-qu3waG7KGE6TMc1N03l1s',
				'Content-Type: application/json'
			);

			// cURL initialization
			$ch = curl_init($url);

			// Set cURL options
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			// Execute cURL and get the response
			$response = curl_exec($ch);

			// Check for cURL errors
			if (curl_errno($ch)) {
				echo 'cURL Error: ' . curl_error($ch);
			}

			// Close cURL
			curl_close($ch);

			// Output the response
			// echo $response;
		}
    }
	private function insert_into_daily_work_sales_on_quotations_data($data) {

		$insert_array_for_daily_report['user_id'] = $this->session->userdata('user_id');
		$insert_array_for_daily_report['quotation_id'] = $data['quotation_mst_id'];
		$quotation_details = $this->common_model->get_dynamic_data_sales_db('client_id, quote_no, grand_total', array('quotation_mst_id' => $data['quotation_mst_id']), 'quotation_mst','row_array');
		$insert_array_for_daily_report['quotation_no'] = '';
		$insert_array_for_daily_report['client_id'] = '';
		$insert_array_for_daily_report['value'] = '';
		$insert_array_for_daily_report['client_name'] = '';
		$insert_array_for_daily_report['country'] = '';
		$insert_array_for_daily_report['member_name'] = '';
		$insert_array_for_daily_report['email_sent'] = '';
		if(!empty($quotation_details)) {

			$insert_array_for_daily_report['quotation_no'] = $quotation_details['quote_no'];
			$insert_array_for_daily_report['client_id'] = $quotation_details['client_id'];
			$insert_array_for_daily_report['value'] = $quotation_details['grand_total'];
			
			$client_details = $this->common_model->get_dynamic_data_sales_db('name, country_id', array('id' => $quotation_details['client_id']), 'customer_mst','row_array');
			if(!empty($client_details)){

				$insert_array_for_daily_report['client_name'] = $client_details['name'];
				if(!empty($client_details['country_id'])){

					$country_details = $this->common_model->get_dynamic_data_sales_db('name', array('id' => $client_details['country_id']), 'country_mst','row_array');
					if(!empty($country_details)){

						$insert_array_for_daily_report['country'] = $country_details['name'];
					}
				}
			}			
		}
		$insert_array_for_daily_report['member_id'] = $data['member_id'];
		// $member_details = $this->common_model->get_dynamic_data_sales_db('name', array('member_id' => $data['member_id']), 'members','row_array');
		$member_details = $this->common_model->get_dynamic_data_sales_db('member_name', array('comp_dtl_id' => $data['member_id']), 'customer_dtl','row_array');
		if(!empty($member_details)){

			$insert_array_for_daily_report['member_name'] = $member_details['member_name'];
		}
		$insert_array_for_daily_report['contact_date'] = date('Y-m-d h:i:s');
		$insert_array_for_daily_report['contact_mode'] = $data['connect_mode'];
		$insert_array_for_daily_report['comments'] = $data['follow_up_text'];
		$insert_array_for_daily_report['email_sent'] = $data['email'];

		// echo "<pre>";print_r($insert_array_for_daily_report);echo"</pre><hr>";exit;
		$this->common_model->insert_data_sales_db('daily_work_sales_on_quotation_data', $insert_array_for_daily_report);
	}
	private function get_indian_time($date_format = 'Y-m-d H:i:s'){

		date_default_timezone_set('Asia/Kolkata');
		return date($date_format);
	}
}