<?php 
class Quotation_model extends CI_Model{

	function __construct(){
		parent::__construct();
	}
	
	function getUsers(){
		$this->db->where('role', 4);
		$this->db->or_where('user_id', 19);
		return $this->db->get_where('users', array('status' => 1))->result_array();
	}

	function getAssignee(){
		$this->db->where('role', 5);
		return $this->db->get_where('users', array('status' => 1))->result_array();
	}

	function getLookup($lookup_id){
		$this->db->order_by('lookup_value');
		return $this->db->get_where('lookup', array('lookup_group' => $lookup_id))->result_array();
	}

	function getData($table, $where = ''){
		if($where != ''){
			$this->db->where($where);
		}
		return $this->db->get($table)->result_array();
	} 

	function insertData($table, $data){
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	function updateData($table, $data, $where){
		$this->db->update($table, $data, $where);
	}

	function deleteData($table, $where){
		$this->db->delete($table, $where);
	}

	function getQuotationDetails($quote_id){
		$this->db->select('m.*, d.*, m.entered_on quote_date, c.client_name, lc.lookup_value country, lr.lookup_value region, mb.name, mb.email, mb.mobile, mb.telephone, mb.skype, dl.delivery_name, dt.dt_value, pt.term_value, oc.country origin, mt.mtc_value, v.validity_value, cr.currency, cr.currency_icon, t.mode, u.unit_value, us.name uname, us.email uemail, us.mobile umobile');
		$this->db->join('quotation_dtl d', 'm.quotation_mst_id = d.quotation_mst_id', 'inner');
		$this->db->join('clients c', 'c.client_id = m.client_id', 'inner');
		$this->db->join('members mb', 'mb.member_id = m.member_id', 'inner');
		$this->db->join('transport_mode t', 'm.transport_mode = t.mode_id', 'inner');
		$this->db->join('payment_terms pt', 'm.payment_term = pt.term_id', 'inner');
		$this->db->join('delivery dl', 'm.delivered_through = dl.delivery_id', 'inner');
		$this->db->join('delivery_time dt', 'm.delivery_time = dt.dt_id', 'inner');
		$this->db->join('origin_country oc', 'm.origin_country = oc.country_id', 'inner');
		$this->db->join('currency cr', 'm.currency = cr.currency_id', 'inner');
		$this->db->join('validity v', 'm.validity = v.validity_id', 'inner');
		$this->db->join('mtc_type mt', 'm.mtc_type = mt.mtc_id', 'inner');
		$this->db->join('units u', 'd.unit = u.unit_id', 'inner');
		$this->db->join('users us', 'm.assigned_to = us.user_id', 'inner');
		$this->db->join('lookup lc', 'lc.lookup_id = c.country', 'left');
		$this->db->join('lookup lr', 'lr.lookup_id = c.region', 'left');
		return $this->db->get_where('quotation_mst m', array('m.quotation_mst_id' => $quote_id))->result_array();
	}

	function getQuotationList($start, $length, $where, $order, $dir, $type, $searchByYear='all', $where_array){
		$this->db->select('m.*, c.client_name, lc.lookup_value country, lr.lookup_value region, MONTH(m.entered_on) month, WEEK(m.entered_on) week, DATE_FORMAT(m.entered_on, "%d-%b") date, DATE_FORMAT(m.followup_date, "%d-%b") fdate, m.importance, m.status, mb.mobile, mb.is_whatsapp, u.name username ,d.name made_by, cr.currency_icon, close_reason.reason_text');
		$this->db->join('clients c', 'c.client_id = m.client_id', 'inner');
		$this->db->join('users u', 'u.user_id = m.assigned_to', 'inner');
		$this->db->join('users d', 'd.user_id = m.made_by', 'inner');
		$this->db->join('members mb', 'mb.member_id = m.member_id', 'left');
		$this->db->join('lookup lc', 'lc.lookup_id = c.country', 'left');
		$this->db->join('lookup lr', 'lr.lookup_id = c.region', 'left');
		$this->db->join('currency cr', 'cr.currency_id = m.currency', 'left');
		$this->db->join('close_reason', 'close_reason.reason_id = m.close_reason', 'left');
		if(!empty($where)){
			
			foreach ($where as $key => $value) {
				// $this->db->group_start();
				if($key == 'quote_no' && $value != ''){
					$this->db->where("m.quote_no like '%".$value."%'");
				}
				else if($key == 'assigned_to' && $value != ''){
					$this->db->where("m.assigned_to = ".$value);
				}
				else if($key == 'date' && $value != ''){
					$this->db->where("m.entered_on like '%".$value."%'");
				}
				else if($key == 'client_name' && $value != ''){
					$this->db->where("c.client_name like '%".$value."%'");
				}
				else if($key == 'grand_total' && $value != ''){

					$grand_total_explode = explode('-', $value);
					
					if(!empty($grand_total_explode) && count($grand_total_explode) == 2) {
						if(trim($grand_total_explode[0]) != trim($grand_total_explode[1])) {
							$this->db->where("CASE 
										WHEN m.currency = 1 
										THEN grand_total >= ".$grand_total_explode[0]." AND grand_total <= ".$grand_total_explode[1]."
							            WHEN m.currency = 2 
							            THEN round(grand_total * 1.09002) >= ".$grand_total_explode[0]." AND round(grand_total * 1.09002) <= ".$grand_total_explode[1]."
							            WHEN m.currency = 3 
							            THEN round(grand_total / 77.1379) >= ".$grand_total_explode[0]." AND round(grand_total / 77.1379) <= ".$grand_total_explode[1]."
							            WHEN m.currency = 4 
							            THEN round(grand_total * 1.31013) >= ".$grand_total_explode[0]." AND round(grand_total * 1.31013) <= ".$grand_total_explode[1]."
							            WHEN m.currency = 5 
							            THEN round(grand_total * 0.7278) >= ".$grand_total_explode[0]." AND round(grand_total * 0.7278) <= ".$grand_total_explode[1]."
							            WHEN m.currency = 6 
							            THEN round(grand_total * 0.2722) >= ".$grand_total_explode[0]." AND round(grand_total * 0.2722) <= ".$grand_total_explode[1]."
									END ", null, false);
						}else {
							$this->db->where("CASE 
										WHEN m.currency = 1 THEN grand_total >= ".$grand_total_explode[0]."
							            WHEN m.currency = 2 THEN round(grand_total * 1.09002) >= ".$grand_total_explode[0]."
							            WHEN m.currency = 3 THEN round(grand_total / 77.1379) >= ".$grand_total_explode[0]."
							            WHEN m.currency = 4 THEN round(grand_total * 1.31013) >= ".$grand_total_explode[0]."
							            WHEN m.currency = 5 THEN round(grand_total * 0.7278) >= ".$grand_total_explode[0]."
							            WHEN m.currency = 6 THEN round(grand_total * 0.2722) >= ".$grand_total_explode[0]."
									END ", null, false);
						}
					}

				} 
				else if($key == 'country' && $value != ''){
					$this->db->where("lc.lookup_id =".$value);
				} 
				else if($key == 'region' && $value != ''){
					$this->db->where("lr.lookup_id = ".$value);
				} 
				else if($key == 'fdate' && $value != ''){
					$this->db->where("m.followup_date like '%".$value."%'");
				} 
				else if($key == 'importance' && $value != ''){
					$this->db->where("m.importance like '%".$value."%'");
				} 
				else if($key == 'status' && $value != ''){
					$this->db->where("m.status like '%".$value."%'");
				} 
			}

			/*foreach ($where as $key => $value){
				$this->db->where('m.quote_no like ', '%'.$value.'%');
				$this->db->or_where('m.entered_on like ', '%'.$value.'%');
				$this->db->or_where('c.client_name like ', '%'.$value.'%');
				$this->db->or_where('mb.name like ', '%'.$value.'%');
				$this->db->or_where('m.grand_total like ', '%'.$value.'%');
				$this->db->or_where('lc.lookup_value like ', '%'.$value.'%');
				$this->db->or_where('lr.lookup_value like ', '%'.$value.'%');
				$this->db->or_where('m.followup_date like ', '%'.$value.'%');
				$this->db->or_where('m.importance like ', '%'.$value.'%');
				$this->db->or_where('m.status like ', '%'.$value.'%');
				$this->db->or_where('u.name like ', '%'.$value.'%');
			}*/
			// $this->db->group_end();
		}
		if($this->session->userdata('role') == 5){
			$this->db->where('m.assigned_to', $this->session->userdata('user_id'));
		}
		if($type != ''){
			$this->db->where('stage', $type);
		}else{
			$this->db->where_in('stage', array('publish', 'proforma'));
		}

		if($searchByYear != 'all'){
			$years = explode('-', $searchByYear);
			$this->db->where('m.entered_on >= ', date($years[0].'-04-01 00:00:00'));
			$this->db->where('m.entered_on <= ', date($years[1].'-03-31 23:59:59'));
		}

		if(!empty($where_array)) {
			if(!empty($where_array['quotation_sales_person'])) {

				$this->db->where_in('m.assigned_to', $where_array['quotation_sales_person']);
			}
			if(!empty($where_array['quotation_country'])) {

				$this->db->where_in('lc.lookup_id', $where_array['quotation_country']);
			}
			if(!empty($where_array['quotation_region'])) {

				$this->db->where_in('lr.lookup_id', $where_array['quotation_region']);
			}
			if(!empty($where_array['quotation_priority'])) {

				$this->db->where_in('m.quotation_priority', $where_array['quotation_priority']);
			}
			if(!empty($where_array['quotation_is_new'])) {

				$this->db->where_in('m.is_new', $where_array['quotation_is_new']);
			}
		}
		$this->db->limit($length, $start);
		$this->db->order_by($order, $dir);
		$res = $this->db->get('quotation_mst m')->result_array();
		// echo $this->db->last_query();
		// echo "<pre>";print_r($res);echo"</pre><hr>";exit;
		$k=0;
		$result = array();
		foreach ($res as $key => $value) {
			$result[$key] = $value;
			$result[$key]['record_id'] = ++$k;
			$this->db->order_by('followedup_on', 'desc');
			$follow_up_res = $this->db->get_where('follow_up', array('quotation_mst_id' => $value['quotation_mst_id']))->row_array();
			if(!empty($follow_up_res)){
				$date1 = date_create($follow_up_res['followedup_on']);
				$date2 = date_create(date('Y-m-d'));
				$diff_obj = date_diff($date1, $date2);
				$diff = $diff_obj->format("%a");

				if($diff < 8){
					$result[$key]['last_followed'] = $diff.' days ago';
				}else if($diff < 30){
					$weeks = round($diff / 7);
					$result[$key]['last_followed'] = $weeks.' weeks ago';
				}else if($diff < 365){
					$months = round($diff / 30);
					$result[$key]['last_followed'] = $months.' months ago';
				}else if($diff > 365){
					$years = round($diff / 365);
					$result[$key]['last_followed'] = $years.' years ago';
				}

				$result[$key]['follow_up_text'] = $follow_up_res['follow_up_text'];

			}else{
				$result[$key]['last_followed'] = '';
				$result[$key]['follow_up_text'] = '';
			}

			$result[$key]['procurement_person'] = '';
			if($value['rfq_id'] > 0){
				$this->db->join('rfq_mst r', 'r.rfq_mst_id = q.query_for_id', 'inner');
				$this->db->join('quotation_mst m', 'r.rfq_mst_id = m.rfq_id', 'inner');
				$query_res = $this->db->get_where('query_mst q', array('r.rfq_mst_id' => $value['rfq_id'], 'query_type' => 'purchase'))->row_array();
				if(!empty($query_res)){
					$result[$key]['has_query'] = true;
					$result[$key]['rfq_id'] = $query_res['rfq_id'];
					$result[$key]['query_id'] = $query_res['query_id'];
					$result[$key]['query_type'] = $query_res['query_type'];
				}else{
					$result[$key]['has_query'] = false;
					$result[$key]['rfq_id'] = '';
					$result[$key]['query_id'] = '';
					$result[$key]['query_type'] = '';
				}
				$this->db->select('users.name');
				$this->db->join('users', 'users.user_id = rfq_mst.assigned_to', 'left');
				$res = $this->db->get_where('rfq_mst', array('rfq_mst_id'=>$value['rfq_id']))->row_array();
				$result[$key]['procurement_person'] = $res['name'];
			}else{
				$result[$key]['has_query'] = false;
				$result[$key]['rfq_id'] = '';
				$result[$key]['query_id'] = '';
				$result[$key]['query_type'] = '';
			}
			$result[$key]['priority_div'] = '<div class="kt-font-warning kt-demo-icon__preview" data-toggle="kt-tooltip" data-placement="left" title="'.$value["priority_reason"].'" data-original-title="Tooltip title">';
			for ($i=0; $i < 5; $i++) { 
			
				if($i < $value['quotation_priority']) {
					$result[$key]['priority_div'] .= '<i class="la la-star" style="line-height: 0;vertical-align: middle;font-size: 1.5rem !important;"></i>';
				}
			}
				
			$result[$key]['priority_div'] .= '</div>';
			
			// echo "<pre>";print_r($result);echo"</pre><hr>";exit;
		}
		return $result;
	}

	function getQuotationListCount($where, $type, $searchByYear='all'){
		$this->db->select('m.*, c.client_name, lc.lookup_value country, lr.lookup_value region, MONTH(m.entered_on) month, WEEK(m.entered_on) week, DATE_FORMAT(m.entered_on, "%d-%b") date, DATE_FORMAT(m.followup_date, "%d-%b") fdate, m.importance, m.status, mb.mobile, mb.is_whatsapp, u.name username');
		$this->db->join('clients c', 'c.client_id = m.client_id', 'inner');
		$this->db->join('users u', 'u.user_id = m.assigned_to', 'inner');
		$this->db->join('members mb', 'mb.member_id = m.member_id', 'left');
		$this->db->join('lookup lc', 'lc.lookup_id = c.country', 'left');
		$this->db->join('lookup lr', 'lr.lookup_id = c.region', 'left');
		$this->db->join('currency cr', 'cr.currency_id = m.currency', 'left');
		if(!empty($where)){
			
			foreach ($where as $key => $value) {
				// $this->db->group_start();
				if($key == 'quote_no' && $value != ''){
					$this->db->where("m.quote_no like '%".$value."%'");
				}
				else if($key == 'assigned_to' && $value != ''){
					$this->db->where("m.assigned_to = ".$value);
				}
				else if($key == 'date' && $value != ''){
					$this->db->where("m.entered_on like '%".$value."%'");
				}
				else if($key == 'client_name' && $value != ''){
					$this->db->where("c.client_name like '%".$value."%'");
				}
				else if($key == 'grand_total' && $value != ''){
					$grand_total_explode = explode('-', $value);
					if(!empty($grand_total_explode) && count($grand_total_explode) == 2) {
						if($grand_total_explode[0] != $grand_total_explode[1]) {

							$this->db->where("CASE 
										WHEN m.currency = 1 
										THEN grand_total >= ".$grand_total_explode[0]." AND grand_total <= ".$grand_total_explode[1]."
							            WHEN m.currency = 2 
							            THEN round(grand_total * 1.09002) >= ".$grand_total_explode[0]." AND round(grand_total * 1.09002) <= ".$grand_total_explode[1]."
							            WHEN m.currency = 3 
							            THEN round(grand_total / 77.1379) >= ".$grand_total_explode[0]." AND round(grand_total / 77.1379) <= ".$grand_total_explode[1]."
							            WHEN m.currency = 4 
							            THEN round(grand_total * 1.31013) >= ".$grand_total_explode[0]." AND round(grand_total * 1.31013) <= ".$grand_total_explode[1]."
							            WHEN m.currency = 5 
							            THEN round(grand_total * 0.7278) >= ".$grand_total_explode[0]." AND round(grand_total * 0.7278) <= ".$grand_total_explode[1]."
							            WHEN m.currency = 6 
							            THEN round(grand_total * 0.2722) >= ".$grand_total_explode[0]." AND round(grand_total * 0.2722) <= ".$grand_total_explode[1]."
									END ", null, false);
						}else {
							$this->db->where("CASE 
										WHEN m.currency = 1 THEN grand_total >= ".$grand_total_explode[0]."
							            WHEN m.currency = 2 THEN round(grand_total * 1.09002) >= ".$grand_total_explode[0]."
							            WHEN m.currency = 3 THEN round(grand_total / 77.1379) >= ".$grand_total_explode[0]."
							            WHEN m.currency = 4 THEN round(grand_total * 1.31013) >= ".$grand_total_explode[0]."
							            WHEN m.currency = 5 THEN round(grand_total * 0.7278) >= ".$grand_total_explode[0]."
							            WHEN m.currency = 6 THEN round(grand_total * 0.2722) >= ".$grand_total_explode[0]."
									END ", null, false);
						}
					}
				} 
				else if($key == 'country' && $value != ''){
					$this->db->where("lc.lookup_id = ".$value);
				} 
				else if($key == 'region' && $value != ''){
					$this->db->where("lr.lookup_id = ".$value);
				}
				else if($key == 'fdate' && $value != ''){
					$this->db->where("m.followup_date like '%".$value."%'");
				} 
				else if($key == 'importance' && $value != ''){
					$this->db->where("m.importance like '%".$value."%'");
				} 
				else if($key == 'status' && $value != ''){
					$this->db->where("m.status like '%".$value."%'");
				} 
			}
		}

		/*if($where != ''){
			$this->db->group_start();
			$this->db->where('m.quote_no like ', '%'.$where.'%');
			$this->db->or_where('m.entered_on like ', '%'.$where.'%');
			$this->db->or_where('c.client_name like ', '%'.$where.'%');
			$this->db->or_where('mb.name like ', '%'.$where.'%');
			$this->db->or_where('m.grand_total like ', '%'.$where.'%');
			$this->db->or_where('lc.lookup_value like ', '%'.$where.'%');
			$this->db->or_where('lr.lookup_value like ', '%'.$where.'%');
			$this->db->or_where('m.followup_date like ', '%'.$where.'%');
			$this->db->or_where('m.importance like ', '%'.$where.'%');
			$this->db->or_where('m.status like ', '%'.$where.'%');
			$this->db->or_where('u.name like ', '%'.$where.'%');
			$this->db->group_end();
		}*/
		if($this->session->userdata('role') == 5){
			$this->db->where('m.assigned_to', $this->session->userdata('user_id'));
		}
		if($type != ''){
			$this->db->where('stage', $type);
		}else{
			$this->db->where_in('stage', array('publish', 'proforma'));
		}
		if($searchByYear != 'all'){
			$years = explode('-', $searchByYear);
			$this->db->where('m.entered_on >= ', date($years[0].'-04-01 00:00:00'));
			$this->db->where('m.entered_on <= ', date($years[1].'-03-31 23:59:59'));
		}
		$res = $this->db->get('quotation_mst m')->result_array();
		//echo $this->db->last_query();
		return sizeof($res);
	}

	function getFollowUpList($start, $length, $search, $order_by, $dir, $searchByYear){
		$this->db->select('m.*, c.client_name, lc.lookup_value country, lr.lookup_value region, MONTH(m.entered_on) month, WEEK(m.entered_on) week, DATE_FORMAT(m.entered_on, "%d-%b") date, DATE_FORMAT(m.followup_date, "%d-%b") fdate, m.importance, m.status, mb.mobile, mb.is_whatsapp, u.name username, cr.currency_icon');
		$this->db->join('clients c', 'c.client_id = m.client_id', 'inner');
		$this->db->join('users u', 'u.user_id = m.assigned_to', 'inner');
		$this->db->join('members mb', 'mb.member_id = m.member_id', 'left');
		$this->db->join('lookup lc', 'lc.lookup_id = c.country', 'left');
		$this->db->join('lookup lr', 'lr.lookup_id = c.region', 'left');
		$this->db->join('currency cr', 'cr.currency_id = m.currency', 'left');
		$this->db->where('m.followup_date <=', date('Y-m-d'));
		$this->db->where("m.quote_no != '' and m.quote_no is not null");
		$this->db->where("m.status", "open");
		if($this->session->userdata('role') == 5){
			$this->db->where('m.assigned_to', $this->session->userdata('user_id'));
		}

		/*if($search != ''){
			$this->db->group_start();
			$this->db->where('m.quote_no like ', '%'.$search.'%');
			$this->db->or_where('m.entered_on like ', '%'.$search.'%');
			$this->db->or_where('c.client_name like ', '%'.$search.'%');
			$this->db->or_where('m.grand_total like ', '%'.$search.'%');
			$this->db->or_where('lc.lookup_value like ', '%'.$search.'%');
			$this->db->or_where('lr.lookup_value like ', '%'.$search.'%');
			$this->db->or_where('m.followup_date like ', '%'.$search.'%');
			$this->db->or_where('u.name like ', '%'.$search.'%');
			$this->db->group_end();
		}*/

		foreach ($search as $key => $value) {
			// $this->db->group_start();
			if($key == 'quote_no' && $value != ''){
				$this->db->where("m.quote_no like '%".$value."%'");
			}
			else if($key == 'assigned_to' && $value != ''){
					$this->db->where("m.assigned_to = ".$value);
				}
			else if($key == 'date' && $value != ''){
				$this->db->where("m.entered_on like '%".$value."%'");
			}
			else if($key == 'client_name' && $value != ''){
				$this->db->where("c.client_name like '%".$value."%'");
			}
			else if($key == 'grand_total' && $value != ''){
				$this->db->where("m.grand_total like '%".$value."%'");
			} 
			else if($key == 'country' && $value != ''){
				$this->db->where("lc.lookup_id =".$value);
			} 
			else if($key == 'region' && $value != ''){
				$this->db->where("lr.lookup_id = ".$value);
			}
			else if($key == 'fdate' && $value != ''){
				$this->db->where("m.followup_date like '%".$value."%'");
			} 
			else if($key == 'importance' && $value != ''){
				$this->db->where("m.importance like '%".$value."%'");
			} 
			else if($key == 'status' && $value != ''){
				$this->db->where("m.status like '%".$value."%'");
			} 
		}

		if($searchByYear != 'all'){
			$years = explode('-', $searchByYear);
			$this->db->where('m.entered_on >= ', date($years[0].'-04-01 00:00:00'));
			$this->db->where('m.entered_on <= ', date($years[1].'-03-31 23:59:59'));
		}

		$this->db->limit($length, $start);
		$this->db->order_by($order_by, $dir);
		$res = $this->db->get('quotation_mst m')->result_array();
		//echo $this->db->last_query();
		$k=0;
		$result = array();
		foreach ($res as $key => $value) {
			$result[$key] = $value;
			$result[$key]['record_id'] = ++$k;
			$follow_up_res = $this->db->get_where('follow_up', array('quotation_mst_id' => $value['quotation_mst_id']))->row_array();
			if(!empty($follow_up_res)){
				$date1 = date_create($follow_up_res['followedup_on']);
				$date2 = date_create(date('Y-m-d'));
				$diff_obj = date_diff($date1, $date2);
				$diff = $diff_obj->format("%a");

				if($diff < 8){
					$result[$key]['last_followed'] = $diff.' days ago';
				}else if($diff < 30){
					$weeks = round($diff / 7);
					$result[$key]['last_followed'] = $weeks.' weeks ago';
				}else if($diff < 365){
					$months = round($diff / 30);
					$result[$key]['last_followed'] = $months.' months ago';
				}else if($diff > 365){
					$years = round($diff / 365);
					$result[$key]['last_followed'] = $years.' years ago';
				}

				$result[$key]['follow_up_text'] = $follow_up_res['follow_up_text'];

			}else{
				$result[$key]['last_followed'] = '';
				$result[$key]['follow_up_text'] = '';
			}
		}
		return $result;
	}

	function getFollowUpListCount($search){
		$this->db->select('m.*, c.client_name, lc.lookup_value country, lr.lookup_value region, MONTH(m.entered_on) month, WEEK(m.entered_on) week, DATE(m.entered_on) date');
		$this->db->join('clients c', 'c.client_id = m.client_id', 'inner');
		$this->db->join('lookup lc', 'lc.lookup_id = c.country', 'left');
		$this->db->join('lookup lr', 'lr.lookup_id = c.region', 'left');
		$this->db->join('users u', 'u.user_id = m.assigned_to', 'inner');
		$this->db->where('m.followup_date <=', date('Y-m-d'));
		$this->db->where("m.quote_no != '' and m.quote_no is not null");
		$this->db->where("m.status", "open");
		if($this->session->userdata('role') == 5){
			$this->db->where('m.assigned_to', $this->session->userdata('user_id'));
		}

		/*if($search != ''){
			$this->db->group_start();
			$this->db->where('m.quote_no like ', '%'.$search.'%');
			$this->db->or_where('m.entered_on like ', '%'.$search.'%');
			$this->db->or_where('c.client_name like ', '%'.$search.'%');
			$this->db->or_where('m.grand_total like ', '%'.$search.'%');
			$this->db->or_where('lc.lookup_value like ', '%'.$search.'%');
			$this->db->or_where('lr.lookup_value like ', '%'.$search.'%');
			$this->db->or_where('m.followup_date like ', '%'.$search.'%');
			$this->db->or_where('u.name like ', '%'.$search.'%');
			$this->db->group_end();
		}*/

		foreach ($search as $key => $value) {
			// $this->db->group_start();
			if($key == 'quote_no' && $value != ''){
				$this->db->where("m.quote_no like '%".$value."%'");
			}
			else if($key == 'assigned_to' && $value != ''){
					$this->db->where("m.assigned_to = ".$value);
				}
			else if($key == 'date' && $value != ''){
				$this->db->where("m.entered_on like '%".$value."%'");
			}
			else if($key == 'client_name' && $value != ''){
				$this->db->where("c.client_name like '%".$value."%'");
			}
			else if($key == 'grand_total' && $value != ''){
				$this->db->where("m.grand_total like '%".$value."%'");
			} 
			else if($key == 'country' && $value != ''){
				$this->db->where("lc.lookup_id =".$value);
			} 
			else if($key == 'region' && $value != ''){
				$this->db->where("lr.lookup_id = ".$value);
			}
			else if($key == 'fdate' && $value != ''){
				$this->db->where("m.followup_date like '%".$value."%'");
			} 
			else if($key == 'importance' && $value != ''){
				$this->db->where("m.importance like '%".$value."%'");
			} 
			else if($key == 'status' && $value != ''){
				$this->db->where("m.status like '%".$value."%'");
			} 
		}

		$res = $this->db->get('quotation_mst m')->result_array();
		$k=0;
		$result = array();
		foreach ($res as $key => $value) {
			$result[$key] = $value;
			$result[$key]['record_id'] = ++$k;
		}
		return sizeof($result);
	}


	function getFollowUpHistory($quote_id){
		$this->db->select('q.quote_no, f.*, c.client_name');
		$this->db->join('quotation_mst q', 'q.quotation_mst_id = f.quotation_mst_id', 'inner');
		$this->db->join('clients c', 'c.client_id = q.client_id', 'inner');
		$this->db->where_in('q.stage', array('publish', 'proforma'));
		return $this->db->get_where('follow_up f', array('f.quotation_mst_id' => $quote_id))->result_array();
	}

	function getQueryHistory($quote_id, $query_type){
		$this->db->join('query_texts qt', 'qt.query_id = q.query_id', 'inner');
		return $this->db->get_where('query_mst q', array('q.query_for_id' => $quote_id, 'q.query_type' => $query_type))->result_array();
	}

	function getSiblingQuotation($quote_id){
		$this->db->select('client_id');
		$res = $this->db->get_where('quotation_mst', array('quotation_mst_id' => $quote_id))->row_array();

		$this->db->where('quotation_mst_id !=', $quote_id);
		return $this->db->get_where('quotation_mst', array('client_id' => $res['client_id'], 'stage' => 'publish'))->result_array();
	}

	function getPortName($port_type, $delivery_type, $country){
		$res = $this->db->get_where('ports', array('port_type' => $port_type, 'delivery_type' => $delivery_type, 'country' => $country))->row_array();
		return $res['port_name'];
	}

	function getClientDetails($quote_id){
		$this->db->select('client_id');
		$res = $this->db->get_where('quotation_mst', array('quotation_mst_id' => $quote_id))->row_array();

		$this->db->select('m.*, c.*, lc.lookup_value country, lr.lookup_value region');
		$this->db->join('clients c', 'c.client_id = m.client_id', 'inner');		
		$this->db->join('lookup lc', 'lc.lookup_id = c.country', 'left');		
		$this->db->join('lookup lr', 'lr.lookup_id = c.region', 'left');		
		return $this->db->get_where('members m', array('m.client_id' => $res['client_id']))->result_array();
	}

	function getFinancialYears(){

		$res = $this->db->query("
						SELECT
						   CASE WHEN MONTH(entered_on)>=3 THEN
						          concat(YEAR(entered_on), '-',YEAR(entered_on)+1)
						   ELSE concat(YEAR(entered_on)-1,'-', YEAR(entered_on)) END AS years
						FROM quotation_mst
						WHERE Year(entered_on) != 1970
						GROUP BY years
						order by years desc;
					")->result_array();
		// echo "<pre>";print_r($res);echo"</pre><hr>";exit;
		return $res;
		
		// $this->db->select('Year(entered_on) as year');
		// $this->db->where(array('Year(entered_on) !=' => '1970'));
		// $this->db->group_by('year');
		// $this->db->order_by('year', 'desc');
		// $res = $this->db->get('quotation_mst')->result_array();
		// return $this->create_year_list($res);
		// $this->db->select('case when month(entered_on) > 3 then concat(year(entered_on),"-",year(entered_on)+1) 
    	// else concat(year(entered_on)-1,"-",year(entered_on)) end as years');
    	// $this->db->distinct();
    	// $this->db->order_by('years', 'desc');
    	// $res =$this->db->get('quotation_mst')->result_array();
    	// return $res;
	}

	private function create_year_list($all_years){

		$return_array = array();

		if(!empty($all_years)) {

			foreach ($all_years as $key => $single_year) {
				
				$return_array[]['years'] = $single_year['year'].'-'.( ((int)$single_year['year']) +1);				
			}
			$return_array[]['years'] = ( ((int)$all_years[count($all_years)-1]['year']) -1).'-'.$all_years[count($all_years)-1]['year'];				
		}

		return $return_array;
	}

	function getSMSDetails($type, $quote_id){
		if($type == 'quotation'){
			$this->db->select('q.quote_no, qu.name sales_user, qu.mobile, r.rfq_subject, ru.name purchase_user, qu.user_id user_id');
			$this->db->join('users qu', 'qu.user_id = q.assigned_to', 'inner');
			$this->db->join('rfq_mst r', 'r.rfq_mst_id = q.rfq_id', 'left');
			$this->db->join('users ru', 'ru.user_id = r.assigned_to', 'left');
			$res = $this->db->get_where('quotation_mst q', array('q.quotation_mst_id' => $quote_id))->row_array();
			return $res;
		}else if($type == 'proforma'){
			$this->db->select('q.proforma_no, qu.name sales_user, qu.mobile, r.rfq_no, ru.name purchase_user, qu.user_id user_id');
			$this->db->join('users qu', 'qu.user_id = q.assigned_to', 'inner');
			$this->db->join('rfq_mst r', 'r.rfq_mst_id = q.rfq_id', 'left');
			$this->db->join('users ru', 'ru.user_id = r.assigned_to', 'left');
			$res = $this->db->get_where('quotation_mst q', array('q.quotation_mst_id' => $quote_id))->row_array();
			return $res;
		}
	}

	function getFollowupCount($user_id){
		$this->db->where("quote_no != '' and quote_no is not null");
		$this->db->where("status", "open");
		$this->db->where('followup_date <=', date('Y-m-d'));
		$res = $this->db->get_where('quotation_mst', array('assigned_to' => $user_id))->result_array();
		return sizeof($res);
	}

	function getQueryRecepient($quote_id, $query_type){
		if($query_type == 'sales' || $query_type == 'proforma'){
			$this->db->select('r.assigned_to');
			$this->db->join('rfq_mst r', 'q.rfq_id = r.rfq_mst_id', 'inner');
			$res = $this->db->get_where('quotation_mst q', array('q.quotation_mst_id' => $quote_id))->row_array();
			return $res['assigned_to'];	
		}else if($query_type == 'purchase'){
			$this->db->select('q.assigned_to');
			$this->db->join('quotation_mst q', 'q.rfq_id = r.rfq_mst_id', 'inner');
			$res = $this->db->get_where('rfq_mst r', array('r.rfq_mst_id' => $quote_id))->row_array();
			return $res['assigned_to'];
		}else if($query_type == 'production') {
			$this->db->select('q.assigned_to');
			$res = $this->db->get_where('quotation_mst q', array('q.quotation_mst_id' => $quote_id))->row_array();
			return $res['assigned_to'];
		}
		
	}

	function getQueryList($start, $length, $search, $order_by, $dir){
		$this->db->select('q.*, t.name recepient, f.name sender, DATE_FORMAT(q.raised_on, "%d-%b") raised_on');
		$this->db->join('users f', 'q.raised_by = f.user_id', 'inner');
		$this->db->join('users t', 'q.query_recepient = t.user_id', 'inner');
		if($this->session->userdata('role') == 5 && in_array($search['query_type'], array('sales', 'proforma'))){
			$this->db->where('q.raised_by', $this->session->userdata('user_id'));
		}
		if($this->session->userdata('role') == 5 && in_array($search['query_type'], array('purchase'))){
			$this->db->where('q.query_recepient', $this->session->userdata('user_id'));
		}
		$this->db->limit($length, $start);
		$this->db->order_by($order_by, $dir);
		$res = $this->db->get_where('query_mst q', array('query_type' => $search['query_type'], 'query_status' => $search['query_status']))->result_array();
		//echo $this->db->last_query();
		
		$k=0;
		$result = array();
		foreach ($res as $key => $value) {
			$result[$key] = $value;
			$result[$key]['record_id'] = ++$k;
			switch ($value['query_type']) {
				case 'sales':
					$for = $this->db->get_where('quotation_mst', array('quotation_mst_id' => $value['query_for_id']))->row_array();
					$result[$key]['query_for'] = $for['quote_no'];
					break;

				case 'proforma':
					$for = $this->db->get_where('quotation_mst', array('quotation_mst_id' => $value['query_for_id']))->row_array();
					$result[$key]['query_for'] = $for['proforma_no'];
					break;

				case 'purchase':
					$for = $this->db->get_where('rfq_mst', array('rfq_mst_id' => $value['query_for_id']))->row_array();
					$result[$key]['query_for'] = $for['rfq_no'];
					break;
			}
		}
		return $result;
	}

	function getQueryListCount($search){
		$this->db->select('q.*, t.name recepient, f.name sender');
		$this->db->join('users f', 'q.raised_by = f.user_id', 'inner');
		$this->db->join('users t', 'q.query_recepient = t.user_id', 'inner');
		if($this->session->userdata('role') == 5){
			$this->db->where('q.raised_by', $this->session->userdata('user_id'));
		}
		$res = $this->db->get_where('query_mst q', array('query_type' => $search['query_type'], 'query_status' => $search['query_status']))->result_array();
		return sizeof($res);
	}

	function getQueryQuote($query_id){
		$res = $this->db->get_where('query_mst', array('query_id' => $query_id))->row_array();
		if($res['query_type'] == 'sales'){
			$res1 = $this->db->get_where('quotation_mst', array('quotation_mst_id' => $res['query_for_id']))->row_array();
			$quote = $res1['quote_no'];
			$quote_str = 'Quote';
		} else if($res['query_type'] == 'proforma'){
			$res1 = $this->db->get_where('quotation_mst', array('quotation_mst_id' => $res['query_for_id']))->row_array();
			$quote = $res1['proforma_no'];
			$quote_str = 'Quote';
		} else if($res['query_type'] == 'purchase'){
			$res1 = $this->db->get_where('rfq_mst', array('rfq_mst_id' => $res['query_for_id']))->row_array();
			$quote = $res1['rfq_no'];
			$quote_str = 'RFQ';
		}
		return array('quote_str' => $quote_str, 'quote' => $quote);
	}

	public function get_dynamic_data($select = '*', $where, $table_name, $return_type = 'result_array', $limit = 0, $offset = 0) {
		
		$this->db->select($select);
		$this->db->where($where);
		return $this->db->get($table_name, $limit, $offset)->$return_type();
	}

	public function get_follow_list_pending_data_for_highchart($date) {

		$this->db->where('followup_date >', $date);
		$this->db->where('followup_date <=', date('Y-m-d'));
		$this->db->where("quote_no != '' and quote_no is not null");
		$this->db->where("status", "open");
		// $this->db->where('m.assigned_to', $assigned_to);
		$this->db->order_by('followup_date', 'DESC');
		return $this->db->get('quotation_mst')->result_array();
	}

	public function get_quotation_list_data_month_wise($year, $sales_person) {

		$where_string = "YEAR(entered_on) != '1970' AND assigned_to != 0";
		if($year != 'All') {
			$where_string = $where_string." AND YEAR(entered_on) ='".$year."'"; 
		}
		if($sales_person != 'All') {
			$where_string = $where_string." AND assigned_to ='".$sales_person."'"; 
		}
		$res = $this->db->query("SELECT MONTHNAME(entered_on) month, assigned_to FROM `quotation_mst` WHERE {$where_string} order by entered_on ASC")->result_array();
		return $res;
	}

	public function get_year_list() {

		$res = $this->db->query("SELECT YEAR(entered_on) year FROM quotation_mst WHERE YEAR(entered_on) != '1970' group by year order by entered_on DESC")->result_array();
		return $res;
	}

	public function get_quotation_list_data_day_wise($start_date, $end_date, $sales_person) {

		$where_string = "entered_on >= '{$start_date}' AND entered_on <= '{$end_date}' AND assigned_to != 0";
		if($sales_person != 'All') {
			$where_string = $where_string." AND assigned_to ='".$sales_person."'"; 
		}
		$res = $this->db->query("SELECT DAYNAME(entered_on) day, assigned_to FROM `quotation_mst` WHERE {$where_string} order by entered_on ASC")->result_array();
		return $res;
	}

	public function get_quotation_list_data_week_wise($start_date, $end_date, $sales_person) {

		$where_string = "entered_on >= '{$start_date}' AND entered_on <= '{$end_date}' AND assigned_to != 0";
		if($sales_person != 'All') {
			$where_string = $where_string." AND assigned_to ='".$sales_person."'"; 
		}
		$res = $this->db->query("SELECT week(entered_on) week, assigned_to FROM `quotation_mst` WHERE {$where_string} order by entered_on ASC")->result_array();
		return $res;
	}

	public function get_sales_user_name() {

		return $this->db->get_where('users', "(role = 5  OR role = 16) and status = 1")->result_array();
	}

	public function get_quotation_close_reason_piechart_data($where_array, $limit) {
		
		$this->db->select('close_reason, count(*) as count');
		$this->db->where("status = 'Closed' AND close_reason is not null", null, false);
		$this->db->where($where_array);
		$this->db->group_by('close_reason');
		$this->db->order_by('count', 'desc');
		$res = $this->db->get('quotation_mst', $limit)->result_array();
		return $res;
	}
} 
?>