jQuery(document).ready(function() {
	KTAutosize.init();

	$("div.span_notes").click(function(){

		$("div.span_notes").hide();
		$("div.textarea_notes").show();
		autosize($('#terms_conditions'));
	});
	// ajax_call_function({call_type: 'quotation_follow_up_highchart', filter_date: ''}, 'quotation_follow_up_highchart');
	<?php if(in_array($this->session->userdata('role'), array(1, 16))) { ?>
		<?php if($this->uri->segment(2) == 'add') {?>
			KTIONRangeSlider.init('<?php echo $this->session->userdata("quotation_priority");?>');
		<?php }?>
	<?php }?>
	<?php if($this->uri->segment(2) == 'list') {?>
		KTIONRangeSlider.init('0');
	<?php }?>
	$('a.change_quotation_pending_for_follow_up_date_for_highchart').click(function(){

		ajax_call_function({call_type: 'quotation_follow_up_highchart', filter_date: $(this).attr('filter-date')}, 'quotation_follow_up_highchart');
	});
	$('a.change_quotation_list_date_for_highchart').click(function(){

		ajax_call_function({call_type: 'quotation_list_highchart', filter_date: $(this).attr('filter-date'), filter_year: $('input#filter_year_value').val(), filter_sales_person: $('input#filter_sales_person_value').val()}, 'quotation_list_highchart');

	});
	$('div.select_year_dropdown').on('click', 'a.select_year', function(){

		ajax_call_function({call_type: 'quotation_list_highchart', filter_date: 'month', filter_year: $(this).attr('year-value'), filter_sales_person: $('input#filter_sales_person_value').val()}, 'quotation_list_highchart');
		$('a.change_quotation_list_date_for_highchart').removeClass('active');
		$('a.change_quotation_list_date_for_highchart[filter-date = month]').addClass('active');
	});

	$('div.select_sales_person_dropdown').on('click', 'a.select_sales_person', function(){

		ajax_call_function({call_type: 'quotation_list_highchart', filter_date: $('input#filter_date_value').val(), filter_year: $('input#filter_year_value').val(), filter_sales_person: $(this).attr('sales-person')}, 'quotation_list_highchart');
	});

	$('form#quotation_form').submit(function() {
		var role = "<?php echo $this->session->userdata('role');?>";
		if(role == 6 || role == 8 || role == 1 || role == 17) {
			if($('button.quotation_list_save_button').attr('submit_count') == 0){

				var stage_value = $('select#stage').val();
				if(stage_value == 'publish') {
					swal({
		                  title: "Are you sure?",
		                  text: "Please check, margin is added or not",
		                  icon: "warning",
		                  buttons:  ["Publish", "Cancel"],
		                  dangerMode: true, 
		                })
		            .then((willDelete) => {
		                if (willDelete) {

							return false;
		                } else {

							var submit_form = true;
							// Loop through each row
							$('tr.line_item').each(function () {

								var cost = parseFloat($(this).find('.rate').val());
								var margin = parseFloat($(this).find('.margin').val());
								var unit_price = parseFloat($(this).find('.unit_price').html());
								var unit_price_direct = parseFloat($(this).find('.unit_price_txt').val());
								console.log(cost, margin, unit_price, unit_price_direct);
								if ((isNaN(cost) || cost <= 0)){

									if ((isNaN(unit_price_direct) || unit_price_direct <= 0)) {

										submit_form = false;
										swal({
											title: "SORRY!",
											text: "CANNOT BE PUBLISHED AS Cost And Direct Price CANNOT BE 0",
											})
										return false;
									}
								}else{

									if ((isNaN(margin) || margin <= 0) && (isNaN(unit_price_direct) || unit_price_direct <= 0)) {

										submit_form = false;
										swal({
											title: "SORRY!",
											text: "CANNOT BE PUBLISHED AS Margin OR Direct Price CANNOT BE 0",
											})
										return false;
									}
								}
							});
				
							// If all margins are valid, submit the form
							if (submit_form) {

								$('button.quotation_list_save_button').attr('submit_count', 1);
								$('form#quotation_form').submit();
							}
		                	return true;
		                }
		            });
				}else{
					$('button.quotation_list_save_button').attr('submit_count', 1);
					$('form#quotation_form').submit();
					return true;
				}
				return false;
			}
		}
	});

	$('div.client_name_search').click(function(){

		var search_value = $("input#client_search_value").val();
		alert(search_value);
		ajax_call_function({ call_type: 'get_client_details', search_data: search_value }, 'get_client_details');
		
	});
	
	$('select[name="client_id"]').change(function(){

		ajax_call_function({ call_type: 'get_client_member_details', client_id: $(this).val() }, 'get_client_member_details');
	});

	$("form#quotation_form").on('click', 'a.add_sample_query', function () {

		var quotation_id = $(this).attr('quotation_mst_id');
		var query_type = $(this).attr('query_type');

		$.ajax({
			type: "POST",
			data: { "call_type": "get_sample_query_history", 'sample_id': quotation_id, 'sample_type': query_type },
			url: "<?php echo site_url('query/ajax_function'); ?>",
			dataType: 'JSON',
			success: function (res) {

				$('div#add_query').modal('show');
				$('input[name="query_type"]').val(res.sample_type);
				$('input[name="query_reference"]').val(res.reference);
				$('input[name="query_reference_id"]').val(res.reference_id);
				$('input[name="query_creator_id"]').val(res.sales_person_id);
				$('textarea[name="query_text"]').val('');
				$('div.query_type').html(res.query_type_option_tag);
				$('div.quotation_sample_query_history').html(res.query_history_html);
				$('.kt-selectpicker').selectpicker();
			}
		});
	});

	$('button.save_sample_query').click(function () {

		$.ajax({
			type: 'POST',
			data: {
				'call_type': 'save_sample_query',
				form_data: $('form#sample_query_form').serializeArray()
			},
			url: "<?php echo base_url('query/ajax_function'); ?>",
			dataType: 'JSON',
			success: function (res) {
				toastr.clear();
				if (res.status == 'successful') {
					$('div#add_query').modal('hide');
					toastr.success(res.message);
				} else {

					toastr.error(res.message);
				}
			},
			beforeSend: function (response) {
				toastr.clear();
				toastr.warning('Saving of the query has begun.');
			}
		});
	});
});


function ajax_call_function(data, callType, url = "<?php echo base_url('quotations/ajax_function'); ?>") {

	$.ajax({
		type: 'POST',
		data: data,
		url: url,
		dataType: 'JSON',
		success: function(res){
			if(res.status == 'successful') {
				
				if(callType == 'quotation_follow_up_highchart') {
					if(data.filter_date != ''){

						swal({
				    		title: "Follup Data Updated ",
				      		icon: "success",
				    	});
					}
			    	quotation_follow_up_highchart(res.highchart_data);
				} else if(callType == 'quotation_list_highchart') {
					if(data.filter_date != ''){
						sweet_alert('list Data Updated');
					}
					quotation_list_highchart(res.highchart_data);
				} else if (callType == 'get_client_details') {
					
					$('select.client_details').html('').html(res.client_option_tag);
					$('.kt-selectpicker').selectpicker();
				} else if (callType == 'get_client_member_details') {

					$('select.mem_details').html('').html(res.client_member_option_tag);
				} else if(callType == 'quotation_close_reason_highchart') {

					quotation_reject_reason_piechart(res.highchart_data);
				}
			}
		},
		beforeSend: function(response){
			
		}
	});
};


function sweet_alert(title, icon = 'success') {

	swal({
			title: title,
			icon: icon,
    	});
}

// create user wise quotation follup list pending highchart
function quotation_follow_up_highchart(highchart_data) {

	Highcharts.chart('quotation_pending_highchart', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: 'Employee Wise Quotation Follow Up Pending Count'
	    },
	    subtitle: {
	        text: ''
	    },
	    accessibility: {
	        announceNewData: {
	            enabled: true
	        }
	    },
	    xAxis: {
	        type: 'category'
	    },
	    yAxis: {
	        title: {
	            text: 'Total Number of Follow Up Pending'
	        }

	    },
	    legend: {
	        enabled: false
	    },
	    plotOptions: {
	        series: {
	            borderWidth: 0,
	            dataLabels: {
	                enabled: true,
	                format: '{point.y:f}'
	            }
	        }
	    },

	    tooltip: {
	        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> Follow Up Pending<br/>'
	    },

	    series: [
	        {
	            name: "OM Tubes",
	            colorByPoint: true,
	            data: highchart_data
	        }
	    ]

	});
}

// create user wise quotation list pending highchart
function quotation_list_highchart(highchart_data) {

	Highcharts.chart('quotation_list_highchart', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: ''
	    },
	    xAxis: {
	        categories: highchart_data.category
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Total fruit consumption'
	        },
	        stackLabels: {
	            enabled: true,
	            style: {
	                fontWeight: 'bold',
	                color: ( // theme
	                    Highcharts.defaultOptions.title.style &&
	                    Highcharts.defaultOptions.title.style.color
	                ) || 'gray'
	            }
	        }
	    },
	    legend: {
	        align: 'right',
	        x: -30,
	        verticalAlign: 'top',
	        y: 25,
	        floating: true,
	        backgroundColor:
	            Highcharts.defaultOptions.legend.backgroundColor || 'white',
	        borderColor: '#CCC',
	        borderWidth: 1,
	        shadow: false
	    },
	    tooltip: {
	        headerFormat: '<b>{point.x}</b><br/>',
	        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
	    },
	    plotOptions: {
	        column: {
	            stacking: 'normal',
	            dataLabels: {
	                enabled: true
	            }
	        }
	    },
	    series: [{
	        name: 'won',
	        data: highchart_data.won,
	        color: '#28a745'
	    },{
	        name: 'Open',
	        data: highchart_data.open,
	        color: '#007bff'
	    },{
	        name: 'Closed',
	        data: highchart_data.closed,
	        color: '#dc3545'
	    }]
	});
}

// create user wise quotation reject reason PieChart
function quotation_reject_reason_piechart(highchart_data) {

	Highcharts.chart('quotation_reject_reason_piechart', {
	    chart: {
	        plotBackgroundColor: null,
	        plotBorderWidth: null,
	        plotShadow: false,
	        type: 'pie'
	    },
	    title: {
	        text: 'Browser market shares in January, 2018'
	    },
	    tooltip: {
	        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	    },
	    accessibility: {
	        point: {
	            valueSuffix: '%'
	        }
	    },
	    plotOptions: {
	        pie: {
	            allowPointSelect: true,
	            cursor: 'pointer',
	            dataLabels: {
	                enabled: false
	            },
	            showInLegend: true
	        }
	    },
	    series: [{
	        name: 'Brands',
	        colorByPoint: true,
	        data: highchart_data
	    }]
	});
}

// Class definition

var KTIONRangeSlider = function () {
    
    // Private functions
    var demos = function (lead_start) {
    	
        // min & max values
        $('#quotation_priority').ionRangeSlider({
            min: 0,
            max: 5,
            from: lead_start
        });
    }

    return {
        // public functions
        init: function(lead_start) {
            demos(lead_start); 
        }
    };
}();

var KTAutosize = function () {
    
    // Private functions

    var quotation_created_date_range_picker_Init = function() {

        if ($('#quotation_created_date_range_picker').length == 0) {
            return;
        }

        var picker = $('#quotation_created_date_range_picker');
        var start = moment().startOf('year');
        var end = moment().endOf('year');

        function cb(start, end, label) {
            var title = '';
            var range = '';

            if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D');
            } else {
                range = start.format('MMM D') + ' - ' + end.format('MMM D');
            }

            $('#quotation_created_date_range_picker_date').html(range);
            $('#quotation_created_date_range_picker_title').html(title);
            ajax_call_function({call_type: 'quotation_list_highchart', filter_date: range}, 'quotation_list_highchart', "<?php echo base_url('home/ajax_function'); ?>");
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Today': 		[moment(), moment()],
                'Yesterday': 	[moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'This Week': 	[moment().startOf('week'), moment().endOf('week')],
                'This Month': 	[moment().startOf('month'), moment().endOf('month')],
                'This Year': 	[moment().startOf('year'), moment().endOf('year')],
                'Last Month': 	[moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end, 'This Year');
    }
    var quotation_reject_reason_date_picker_Init = function() {
        if ($('#quotation_close_reason_range_picker').length == 0) {
            return;
        }

        var picker = $('#quotation_close_reason_range_picker');
        var start = moment().startOf('year');
        var end = moment().endOf('year');

        function cb(start, end, label) {
            var title = '';
            var range = '';

            if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D Y');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D Y');
            } else {
                range = start.format('MMM D Y') + ' - ' + end.format('MMM D Y');
            }

            $('#quotation_close_reason_range_picker_date').html(range);
            $('#quotation_close_reason_range_picker_title').html(title);
            ajax_call_function({call_type: 'quotation_close_reason_highchart', filter_date: range}, 'quotation_close_reason_highchart');
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'This Week': [moment().startOf('week'), moment().endOf('week')],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end, 'This Month');
    }

    return {
        // public functions
        init: function() {
            quotation_created_date_range_picker_Init();
            quotation_reject_reason_date_picker_Init();
        }
    };
}();