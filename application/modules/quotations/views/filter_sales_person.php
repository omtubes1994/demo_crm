<h6 class="dropdown-header">Select Sales Person</h6>
<a class="dropdown-item select_sales_person <?php echo ($filter_sales_person == 'All')? 'disabled':' '?>" sales-person="All">All</a>
<?php foreach($sales_person as $sales_person_id => $sales_person_name) {?>
	<a class="dropdown-item select_sales_person <?php echo ($filter_sales_person == $sales_person_id)? 'disabled':' '?>" sales-person="<?php echo $sales_person_id;?>"><?php echo $sales_person_name;?></a>
<?php }?>