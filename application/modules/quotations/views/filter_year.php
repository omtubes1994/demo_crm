<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="border: 0px solid #e2e5ec;">
	Years :<span style="font-weight: 800; color: #5d78ff; padding: 1px 1px 1px 10px;"><?php echo $filter_year;?></span>
</button>
<div class="dropdown-menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -215px, 0px);">
	<h6 class="dropdown-header">Select Years</h6>
	<a class="dropdown-item select_year <?php echo ($filter_year == 'All')? 'disabled':' '?>" year-value="All">All</a>
	<?php foreach($years_list as $year_details) {?>
		<a class="dropdown-item select_year <?php echo ($filter_year == $year_details['year'])? 'disabled':' '?>" year-value="<?php echo $year_details['year'];?>"><?php echo $year_details['year'];?></a>
	<?php }?>
</div>
