jQuery(document).ready(function() {

	<?php if(in_array($this->session->userdata('role'), array(1))) { ?>
		<?php if($this->uri->segment(2) == 'followup') {?>
			ajax_call_function({call_type: 'quotation_follow_up_highchart', filter_date: ''}, 'quotation_follow_up_highchart');
		<?php } else if($this->uri->segment(2) == 'list') {?>
			ajax_call_function({call_type: 'quotation_list_highchart', filter_date: ''}, 'quotation_list_highchart');
		<?php } else if($this->uri->segment(2) == 'add') {?>
			KTIONRangeSlider.init('<?php echo $this->session->userdata("quotation_priority");?>');
		<?php }?>
	<?php }?>
	<?php if($this->uri->segment(2) == 'list') {?>
		KTIONRangeSlider.init('0');
	<?php }?>
	$('a.change_quotation_pending_for_follow_up_date_for_highchart').click(function(){

		ajax_call_function({call_type: 'quotation_follow_up_highchart', filter_date: $(this).attr('filter-date')}, 'quotation_follow_up_highchart');
	});
	$('a.change_quotation_list_date_for_highchart').click(function(){

		ajax_call_function({call_type: 'quotation_list_highchart', filter_date: $(this).attr('filter-date'), filter_year: $('input#filter_year_value').val(), filter_sales_person: $('input#filter_sales_person_value').val()}, 'quotation_list_highchart');

	});
	$('div.select_year_dropdown').on('click', 'a.select_year', function(){

		ajax_call_function({call_type: 'quotation_list_highchart', filter_date: 'month', filter_year: $(this).attr('year-value'), filter_sales_person: $('input#filter_sales_person_value').val()}, 'quotation_list_highchart');
		$('a.change_quotation_list_date_for_highchart').removeClass('active');
		$('a.change_quotation_list_date_for_highchart[filter-date = month]').addClass('active');
	});

	$('div.select_sales_person_dropdown').on('click', 'a.select_sales_person', function(){

		ajax_call_function({call_type: 'quotation_list_highchart', filter_date: $('input#filter_date_value').val(), filter_year: $('input#filter_year_value').val(), filter_sales_person: $(this).attr('sales-person')}, 'quotation_list_highchart');
	});
});


function ajax_call_function(data, callType, url = "<?php echo base_url('quotations/ajax_function'); ?>") {

	$.ajax({
		type: 'POST',
		data: data,
		url: url,
		dataType: 'JSON',
		success: function(res){
			if(res.status == 'successful') {
				
				if(callType == 'quotation_follow_up_highchart') {
					if(data.filter_date != ''){

						swal({
				    		title: "Follup Data Updated ",
				      		icon: "success",
				    	});
					}
			    	quotation_follow_up_highchart(res.highchart_data);
				}else if(callType == 'quotation_list_highchart') {
					if(data.filter_date != ''){

						swal({
				    		title: "list Data Updated ",
				      		icon: "success",
				    	});
					}
					quotation_list_highchart(res.highchart_data);
					$('input#filter_date_value').val(res.html_body.filter_date_name);
					$('input#filter_year_value').val(res.html_body.filter_year_name);
					$('input#filter_sales_person_value').val(res.html_body.filter_sales_person_id);
					$('span#sales_person_name').html('').html(res.html_body.filter_sales_person_name);
					$('div.select_year_dropdown').html('').html(res.html_body.filter_year);
					$('div.select_sales_person_dropdown').html('').html(res.html_body.filter_sales_person);
				}
			}
		},
		beforeSend: function(response){
			
		}
	});
};

// create user wise quotation follup list pending highchart
function quotation_follow_up_highchart(highchart_data) {

	Highcharts.chart('quotation_pending_highchart', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: 'Employee Wise Quotation Follow Up Pending Count'
	    },
	    subtitle: {
	        text: ''
	    },
	    accessibility: {
	        announceNewData: {
	            enabled: true
	        }
	    },
	    xAxis: {
	        type: 'category'
	    },
	    yAxis: {
	        title: {
	            text: 'Total Number of Follow Up Pending'
	        }

	    },
	    legend: {
	        enabled: false
	    },
	    plotOptions: {
	        series: {
	            borderWidth: 0,
	            dataLabels: {
	                enabled: true,
	                format: '{point.y:f}'
	            }
	        }
	    },

	    tooltip: {
	        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> Follow Up Pending<br/>'
	    },

	    series: [
	        {
	            name: "OM Tubes",
	            colorByPoint: true,
	            data: highchart_data
	        }
	    ]

	});
}

// create user wise quotation list pending highchart
function quotation_list_highchart(highchart_data) {

	Highcharts.chart('quotation_list_highchart', {
		chart: {
	        type: 'line'
	    },
	    title: {
	        text: 'Quotation created Daily, Weekly and Monthly'
	    },
	    subtitle: {
	        text: 'All Employees'
	    },
	    xAxis: {
	        // categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
	        categories: highchart_data.category
	    },
	    yAxis: {
	        title: {
	            text: 'Number of Quotation'
	        }
	    },
	    plotOptions: {
	        line: {
	            dataLabels: {
	                enabled: true
	            },
	            enableMouseTracking: false
	        }
	    },
	    series: highchart_data.series
	    // series: [
	    // 	{
		   //      name: 'Quotation Created',
		   //      data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
		   //  }
	    // ]
	});
}

// Class definition

var KTIONRangeSlider = function () {
    
    // Private functions
    var demos = function (lead_start) {
    	
        // min & max values
        $('#quotation_priority').ionRangeSlider({
            min: 0,
            max: 5,
            from: lead_start
        });
    }

    return {
        // public functions
        init: function(lead_start) {
            demos(lead_start); 
        }
    };
}();