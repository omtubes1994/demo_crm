<style type="text/css">
	div.span_notes:hover{
		background-color: #13151c1c;
	}
	.btn-default {
		margin-right: 5px;
		border: 2px solid #000;
		background-color: #333;
		color: #fff;
	}

	.tinymce-container {
        resize: both;
        overflow: auto;
        height: 60px;
        border: 1px solid #ddd;
    }

    div.tox-tinymce {
        width: 100%;
        border: none;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
			<div class="col-lg-12 col-xl-12 order-lg-1 order-xl-1">
				<div class="kt-portlet">
					<?php echo form_open('', array("class" => "kt-form kt-form--label-right", "id" => "quotation_form", "enctype" => "multipart/form-data")); ?>
						<div class="kt-portlet__body">
							<div class="form-group row">
								<div class="col-lg-4">
									<label>Made By</label>
									<div class="input-group">
										<div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
										<select class="form-control validate[required]" name="made_by">
											<option value="">Select User</option>
											<?php
												if(!isset($quote_details) && $this->session->userdata('role') == 5){
													echo '<option value="'.$this->session->userdata('user_id').'" selected="selected">'.ucwords(strtolower($this->session->userdata('name'))).'</option>';	
												}
												else if(!empty($users)){
													foreach ($users as $key => $value) {
														$selected = '';
														if($this->session->userdata('user_id') == $value['user_id'] && !isset($quote_details)){
															$selected = 'selected="selected"';
														}

														if(isset($quote_details) && $quote_details[0]['made_by'] == $value['user_id']){
															$selected = 'selected="selected"';
														}
														echo '<option value="'.$value['user_id'].'" '.$selected.'>'.ucwords(strtolower($value['name'])).'</option>';
													}
												}
											?>
										</select>
									</div>
								</div>
								<div class="col-lg-4">
									<label>Assign To</label>
									<div class="input-group">
										<div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
										<select class="form-control validate[required]" name="assigned_to">
											<option value="">Select User</option>
											<?php 
												if(!isset($quote_details) && $this->session->userdata('role') == 5){
													echo '<option value="'.$this->session->userdata('user_id').'" selected="selected">'.ucwords(strtolower($this->session->userdata('name'))).'</option>';	
												}
												else if(!empty($assignee)){
													foreach ($assignee as $key => $value) {
														$selected = '';
														if(isset($quote_details) && $quote_details[0]['assigned_to'] == $value['user_id']){
															$selected = 'selected="selected"';
														}else if(!isset($quote_details) && isset($rfq_details) && $rfq_details[0]['rfq_sentby'] == $value['user_id']){
															$selected = 'selected="selected"';
														}
														echo '<option value="'.$value['user_id'].'" '.$selected.'>'.ucwords(strtolower($value['name'])).'</option>';
													}
												}
											?>
										</select>
									</div>
								</div>
								<div class="col-lg-4 kt-hidden">
									<label>Company Type</label>
									<div class="input-group">
										<div class="input-group-prepend"><span class="input-group-text"><i class="la la-contao"></i></span></div>
										<select class="form-control validate[required]" name="client_type">
											<option value="">Select Type</option>
											
											<?php
												$omtubes_option = "selected";
												$zengineer_option = $instinox_option = "";
												if(isset($rfq_details)){

													if($rfq_details[0]['type'] == 'zen'){

														$zengineer_option = "selected";
													}else if($rfq_details[0]['type'] == 'in'){
														
														$instinox_option = "selected";
													}
												}
												echo '<option value="omtubes" '.$omtubes_option.'>Om Tubes</option>';
												echo '<option value="zengineer" '.$zengineer_option.'>Zengineer</option>';
												echo '<option value="instinox" '.$instinox_option.'>Instinox</option>';
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-lg-3">
									<label class="">Quote #</label>
									<input type="hidden" name="quote_no" value="<?php if(isset($quote_details)) echo $quote_details[0]['quote_no']; ?>">
									<input type="text" class="form-control" placeholder="Quote #" value="<?php if(isset($quote_details)) echo $quote_details[0]['quote_no']; ?>" disabled>
								</div>
								<div class="col-lg-3">
									<label class="">Quote Date</label>
									<input type="text" name="quote_date" id="quote_date" class="form-control hasdatepicker" <?php if(isset($quote_details)) { echo 'value="'.date('d-m-Y', strtotime($quote_details[0]['quote_date'])).'"'; } else { echo 'disabled'; } ?>>
								</div>


								<div class="col-lg-3">
									<label>Stage</label>
									
									<?php if($this->session->userdata('quotation_access')['quotation_form_stage_access']){ ?>
										<select class="form-control validate[required]" name="stage" id="stage">
											<option value="publish" <?php if(isset($quote_details) && $quote_details[0]['stage'] == 'publish') echo 'selected="selected"'; ?>>Publish</option>
											<option value="draft" <?php if(isset($quote_details) && $quote_details[0]['stage'] == 'draft') echo 'selected="selected"'; ?>>Draft</option>
										<?php if(isset($quote_details) && ($quote_details[0]['stage'] == 'publish' || $quote_details[0]['stage'] == 'proforma')){ ?>
											<option value="proforma" <?php if(isset($quote_details) && $quote_details[0]['stage'] == 'proforma') echo 'selected="selected"'; ?>>Convert to Proforma</option>
										<?php } ?>
										</select>
									<?php }else{ ?>	
										<select class="form-control validate[required]" name="stage" id="stage" readonly>
										<?php if(isset($quote_details)) {?>
											<option value="<?php echo $quote_details[0]['stage']; ?>" >
											<?php if($quote_details[0]['stage'] == 'publish') {?>
												Publish
											<?php } elseif($quote_details[0]['stage'] == 'draft') {?>
												Draft
											<?php } elseif($quote_details[0]['stage'] == 'proforma') {?>
												Convert to Proforma
											<?php } ?>
											</option>
										<?php }else{ ?>
											<option value="draft">Draft</option>
										<?php } ?>
										</select>
									<?php } ?>
								</div>
								<?php if($this->session->userdata('role') == 5 || $this->session->userdata('role') == 1){ ?>
								<div class="col-lg-3">
									<label class="">Purchase Order(PDF file)</label>
									<input type="file" name="purchase_order" id="purchase_order" class="form-control" accept="application/pdf">
									<?php 
										if(isset($quote_details) && $quote_details[0]['purchase_order'] != ''){
											echo '<a href="'.site_url('assets/purchase_orders/'.$quote_details[0]['purchase_order']).'" target="_blank">View PO</a>';
										}
									?>
								</div>
								<?php } ?>
							</div>
							<div class="form-group row">
								<div class="col-lg-3">
									<label class="">Company</label>
									<?php
									$company_display = "none";
									if($this->session->userdata('quotation_access')['quotation_add_client_name_access']){
										$company_display = "block";
									}
									?>
									<div class="input-group desai" style="display:<?php echo $company_display; ?>">
										<div class="input-group">
											<input type="text" id="client_search_value" class="form-control" placeholder="Enter Client Name">
											<div class="input-group-prepend client_name_search">
												<span class="input-group-text">
													<i class="la la-search"></i>
												</span>
											</div>
										</div>
										<select class="form-control kt-selectpicker client_details validate[required]" data-size="7" data-live-search="true" tabindex="-98" name="client_id">
											<option value="">Select</option>
											<?php 
												if(!empty($clients)){
													foreach ($clients as $key => $value) {
														$selected = '';
														if(isset($quote_details) && $quote_details[0]['client_id'] == $value['id']) {
															$selected='selected="selected"';
															
														}else if(!isset($quote_details) && isset($rfq_details) && $rfq_details[0]['rfq_company'] == $value['id']){
															$selected='selected="selected"';
														}
														echo '<option '.$selected.' value="'.$value['id'].'">'.ucwords(strtolower($value['name'])).' ('.$value['country'].')</option>';
													}
												}
											?>
										</select>

										
										<div class="input-group-append">
											<button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#add_company">+</button>
										</div>
									</div>
								</div>
								<div class="col-lg-3">
									<label class="">Attention</label>
									<?php
									$member_display = "none";
									if($this->session->userdata('quotation_access')['quotation_add_member_name_access']){
										$member_display = "block";
									}
									?>
									<div class="input-group" style="display:<?php echo $member_display; ?>">
										<select class="form-control mem_details" data-size="7" data-live-search="true" tabindex="-98" name="member_id">
											<option value="">Choose Attention</option>
											<?php 
												if(!empty($members)){
													foreach ($members as $key => $value) {
														$selected = '';
														if(isset($quote_details) && $quote_details[0]['member_id'] == $value['comp_dtl_id']) {
															$selected='selected="selected"';
														}else if(!isset($quote_details) && isset($rfq_details) && $rfq_details[0]['rfq_buyer'] == $value['comp_dtl_id']){
															$selected='selected="selected"';
														}
														echo '<option '.$selected.' value="'.$value['comp_dtl_id'].'">'.ucwords(strtolower($value['member_name'])).'</option>';
													}
												}
											?>
										</select>
										<div class="input-group-append">
											<button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#add_member" id="addNewMember" disabled="disabled">+</button>
										</div>
									</div>
								</div>
								<div class="col-lg-3" id="ref_div" <?php if((isset($quote_details) && $quote_details['0']['stage'] != 'proforma') || !isset($quote_details)){ echo 'style="display: block;"'; }else{ echo 'style="display: none;"'; } ?>>
									<label class="">Reference</label>
									<div class="input-group">
										<input type="text" class="form-control" placeholder="Reference" name="reference" value="<?php if(isset($quote_details)) echo $quote_details[0]['reference']; ?>">
									</div>
								</div>

								<div class="col-lg-3">
									<label class="">Importance</label>
									<select class="form-control validate[]" name="importance" id="importance">
										<option value="">Select</option>

										<option value="L" <?php if(isset($rfq_details) && $rfq_details[0]['rfq_importance'] == 'L' && !isset($quote_details)){ echo 'selected="selected"'; } else if(isset($quote_details) && $quote_details[0]['importance'] == 'L'){ echo 'selected="selected"'; } ?>>Low</option>

										<option value="M" <?php if(isset($rfq_details[0]) && $rfq_details[0]['rfq_importance'] == 'M'){ echo 'selected="selected"'; } else if(isset($quote_details) && $quote_details[0]['importance'] == 'M'){ echo 'selected="selected"'; } ?>>Medium</option>

										<option value="H" <?php if(isset($rfq_details) && $rfq_details[0]['rfq_importance'] == 'H'){ echo 'selected="selected"'; } else if(isset($quote_details) && $quote_details[0]['importance'] == 'H'){ echo 'selected="selected"'; } ?>>High</option>

										<option value="V" <?php if(isset($rfq_details) && $rfq_details[0]['rfq_importance'] == 'V'){ echo 'selected="selected"'; } else if(isset($quote_details) && $quote_details[0]['importance'] == 'V'){ echo 'selected="selected"'; } ?>>Very High</option>
									</select>
								</div>

								<div class="col-lg-3" id="order_div" <?php if(isset($quote_details) && $quote_details['0']['stage'] == 'proforma'){ echo 'style="display: block;"'; }else{ echo 'style="display: none;"'; }?>>
									<label class="">Order #</label>
									<div class="input-group">
										<input type="text" class="form-control" placeholder="Order #" name="order_no" value="<?php if(isset($quote_details)) echo $quote_details[0]['order_no']; ?>">
									</div>
								</div>
								<div class="col-lg-3" id="proforma_div" <?php if(isset($quote_details) && $quote_details['0']['stage'] == 'proforma'){ echo 'style="display: block;"'; }else{ echo 'style="display: none;"'; }?>>
									<label class="">Proforma #</label>
									<div class="input-group">
										<input type="text" class="form-control" placeholder="Proforma #" name="proforma_no" value="<?php if(isset($quote_details)) echo $quote_details[0]['proforma_no']; ?>" readonly="readonly">
									</div>
								</div>
								<?php if(!empty($rfq_notes)){?>
									<div class="col-lg-3" >
										<button type="button" data-toggle="modal" data-target="#notes-modal" class="btn btn-xl btn-primary notes" title="Add Notes" style="margin-top: 25px;">RFQ Notes</button>
									</div>
								<?php } ?>
								<input type="hidden" name="rfq_id" value="<?php if(isset($quote_details)){echo $quote_details[0]['rfq_id']; }else if(isset($rfq_details)){echo $rfq_details[0]['rfq_mst_id'];} ?>">
							</div>
							<?php if($this->session->userdata('role') == 1){?>
							<div class="form-group row kt-hidden">
								<div class="col-md-6 form-group row">
									<label class="col-form-label col-lg-2 col-sm-12">Quotation Priority</label>
									<div class="kt-font-info col-lg-4 col-md-9 col-sm-12">
										<input type="hidden" id="quotation_priority" name="quotation_priority" />
									</div>
								</div>	
							</div>
							<?php } ?>
							<div class="form-group row">
								<div class="col-lg-3">
									<label class="">Product Family</label>
									<input type="text" class="form-control" placeholder="Product Family" value="<?php if(isset($rfq_details)) echo $rfq_details[0]['product_family']; ?>" disabled>
								</div>
								<div>
									<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="javascript:void(0)" class="btn-sm btn btn-label-primary btn-bold add_sample_query" data-toggle="modal" data-target="#add_query" quotation_mst_id="<?php echo $quote_id;?>" query_type="quotation_sample_query" title="Sample">
                                        <i class="la la-comment"> Sample Request</i>
                                    </a>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-lg-12">
									<table class="table table-bordered">
										<thead>
											<tr>
												<?php if(in_array($this->session->userdata('role'), array(5))){ ?>
													<th colspan="8">Quotation Preview<input type="button" class="btn btn-primary btn-sm" id="add_row" style="float:right;" value="Add Row"/>&nbsp;&nbsp;&nbsp;
														<button class="btn-default btn-sm btn" type="button" title="Bold" tabindex="-1" id="bold_button">
															<span class="fa fa-bold"></span>
														</button>
														<button class="btn-default btn-sm btn" type="button" title="Italic" tabindex="-1" id="italic_button">
															<span class="fa fa-italic"></span>
														</button>
														<button class="btn-default btn-sm btn" type="button" title="Underline" tabindex="-1" id="underline_button">
															<span class="fa fa-underline"></span>
														</button>
														<button class="btn-default btn-sm btn" type="button" title="Align Left" tabindex="-1" id="alignleft_button">
															<span class="fa fa-align-left"></span>
														</button>
													</th>
												<?php }else{ ?>
													<th colspan="11">Quotation Preview <a href="javascript:void(0)" class="btn btn-label-success btn-sm btn-bold covert_to_csv"  style="float:left;" aria-expanded="true">
													Export to csv </a> &nbsp;&nbsp;&nbsp;&nbsp;
														<button class="btn-default btn-sm btn" type="button" title="Bold" tabindex="-1" id="bold_button">
															<span class="fa fa-bold"></span>
														</button>
														<button class="btn-default btn-sm btn" type="button" title="Italic" tabindex="-1" id="italic_button">
															<span class="fa fa-italic"></span>
														</button>
														<button class="btn-default btn-sm btn" type="button" title="Underline" tabindex="-1" id="underline_button">
															<span class="fa fa-underline"></span>
														</button>
														<button class="btn-default btn-sm btn" type="button" title="Align Left" tabindex="-1" id="alignleft_button">
															<span class="fa fa-align-left"></span>
														</button>
														<input type="button" class="btn btn-primary btn-sm" id="add_row" style="float:right;" value="Add Row"/>
													</th>
												<?php } ?>
											</tr>
											<tr>
												<th rowspan="2" width="4%">Sr #</th>
												<th width="10%">Product<?php if($this->session->userdata('quotation_access')['quotation_form_add_new_product_access']){?><br/> <button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#add_product">+</button><?php }?></th>
												<th rowspan="2" width="20%">Description</th>
												<th rowspan="2" width="8%">Quantity</th>
												<th width="10%">Unit<?php if($this->session->userdata('quotation_access')['quotation_form_add_new_unit_access']){ ?><br/> <button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#add_unit">+</button><?php }?>
												</th>
												<?php if($this->session->userdata('quotation_access')['quotation_form_margin_and_unit_price_access']){ ?>
													<th rowspan="2" width="8%">Cost</th>
													<th width="8%">Margin(%)</th>
													<th width="8%">Packaging Charges(%)</th>
													<th rowspan="2" width="9%">Unit Price</th>
												<?php }else if(in_array($this->session->userdata('role'), array(5))){ ?>
													<th rowspan="2" width="9%">Unit Price</th>
												<?php }else if(in_array($this->session->userdata('role'), array(8))){ ?>
													<th rowspan="2" width="8%">Cost</th>
												<?php } ?>
												<th rowspan="2" width="9%">Total Price</th>
												<th rowspan="2" width="6%">Action</th>
											</tr>
											<tr>
												<td>
													<select class="form-control" id="parent_product"><option value="">Select Product</option><?php echo $prd_str; ?></select>
													<select class="form-control" id="parent_material"><option value="">Select Material</option><?php echo $mat_str; ?></select>
												</td>
												<td>
													<select class="form-control" id="parent_unit"><?php echo $unit_str; ?></select>
												</td>
												<?php if($this->session->userdata('quotation_access')['quotation_form_margin_and_unit_price_access']){ ?>
													<td>
														<input type="text" class="form-control" id="parent_margin" placeholder="Margin (in percent)">
													</td>
													<td>
														<input type="text" class="form-control" id="parent_packaging" placeholder="Packaging (in percent)">
													</td>
												<?php }else{ ?>
													<td style="display:none;">
														<input type="text" class="form-control" id="parent_margin" placeholder="Margin (in percent)">
													</td>
													<td style="display:none;">
														<input type="text" class="form-control" id="parent_packaging" placeholder="Packaging (in percent)">
													</td>
												<?php } ?>
											</tr>
										</thead>
										<?php
											$cost_display = $margin_display = $packaging_charges_display = $unit_price_display = 'display:none;';
											if($this->session->userdata('quotation_access')['quotation_form_margin_and_unit_price_access']){

												$cost_display = $margin_display = $packaging_charges_display = $unit_price_display = 'block;';
											}else if(in_array($this->session->userdata('role'), array(5))){

												$unit_price_display = 'block;';
											}else if(in_array($this->session->userdata('role'), array(8))){

												$cost_display = 'block;';
											}
										?>
										<tbody id="tbody">
											<?php 
												$k=0;
												if(isset($quote_details)){
													foreach ($quote_details as $key => $value) {
											?>
												<tr class="line_item">
													<td><?php echo ++$k;?></td>
													<td><select class="form-control products" name="product_id[]">
															<option value="">Select Product</option>
															<?php
																foreach ($product as $prod) {
																	$selected = '';
																	if($prod['id'] == $value['product_id']){
																		$selected = 'selected = "selected"';
																	}
																	echo '<option '.$selected.' value="'.$prod['id'].'">'.ucwords(strtolower($prod['name'])).'</option>';
																}
															?>
														</select>
														<select class="form-control materials" name="material_id[]">
															<option value="">Select Material</option>
															<?php
																foreach ($material as $mat) {
																	$selected = '';
																	if($mat['id'] == $value['material_id']){
																		$selected = 'selected = "selected"';
																	}
																	echo '<option '.$selected.' value="'.$mat['id'].'">'.ucwords(strtolower($mat['name'])).'</option>';
																}
															?>
														</select>
													</td>
													<td>
														<div class="tinymce-container">
														<textarea class="form-control validate[required] description" name="description[]" id="description_<?php echo $key; ?>">
															<?php echo htmlspecialchars($value['description']); ?>
														</textarea>
														</div>
													</td>
													<td>
														<input type="text" class="form-control validate[required,custom[onlyNumberSp]] quantity" name="quantity[]" value="<?php echo $value['quantity']; ?>">
													</td>
													<td>
														<select class="form-control units" name="unit[]">
															<?php foreach($units as $uts){
																$selected = '';
																if($uts['unit_id'] == $value['unit']){
																	$selected = 'selected = "selected"';
																}
																echo '<option '.$selected.' value="'.$uts['unit_id'].'">'.ucwords(strtolower($uts['unit_value'])).'</option>';
															}?>
														</select>
													</td>
													<td style="<?php echo $cost_display; ?>">
														<input type="text" class="form-control <?php if($value['unit_rate'] != '') echo 'validate[required,custom[onlyNumberSp]]'; ?> rate" name="unit_rate[]" <?php if($value['unit_rate'] != '') echo 'value="'.$value['unit_rate'].'"'; else echo 'readonly="readonly"'; ?>>
													</td>
													<td style="<?php echo $margin_display; ?>">
														<input type="text" class="form-control <?php if($value['unit_rate'] != '') echo 'validate[required,custom[onlyNumberSp]]'; ?> margin" name="margin[]" <?php if($value['margin'] != '') echo 'value="'.$value['margin'].'"'; else echo 'readonly="readonly"'; ?>>
													</td>
													<td style="<?php echo $packaging_charges_display; ?>">
														<input type="text" class="form-control <?php if($value['unit_rate'] != '') echo 'validate[required,custom[onlyNumberSp]]'; ?> packingCharge" name="packing_charge[]" <?php if($value['unit_rate'] != '') echo 'value="'.$value['packing_charge'].'"'; else echo 'readonly="readonly"'; ?>>
													</td>
													<td style="<?php echo $unit_price_display; ?>">
														<label class="unit_price"><?php if($value['unit_rate'] != '') echo $value['unit_price']; ?></label>
														<input type="text" class="form-control validate[custom[onlyNumberSp]] unit_price_txt" name="unit_price[]" <?php if($value['unit_rate'] == '') echo 'value="'.$value['unit_price'].'"'; ?>>
													</td>
													<td>
														<label class="total_price"><?php echo $value['row_price']; ?></label>
													</td>
													<td>
														<button class="btn btn-sm btn-danger delRow">Delete</button>
													</td>
												</tr>
											<?php
													}
												}
											?>
										</tbody>
										<tfoot>
											<?php if(in_array($this->session->userdata('role'), array(5))){ ?>
											<tr>
												<td colspan="3" rowspan="10">
													<div class="kt-portlet__head">
														<div class="kt-portlet__head-label">
															<span class="kt-portlet__head-icon">
																<i class="flaticon-edit-1" style="color: #343a40;font-size: 3.3rem;"></i>
															</span>
															<h3 class="kt-portlet__head-title">
																Additional Notes
															</h3>
														</div>
													</div>
													<div class="alert alert-light alert-elevate fade show" role="alert" style="padding: 10px;">
														<div class="alert-text span_notes" style="padding: 10px;">
															<span class="span_notes" style="font-size: 1.0rem; color: #495057; font-weight: 400;">
																We reserve the right to correct the pricing offered due to any typographical errors. This offer is not valid for end users from Iran, Iraq, North Korea, Cuba, Sudan & Syria.
															</span>
														</div>
														<div class="alert-text textarea_notes" style="padding: 10px; display:none;">
															<textarea class="form-control textarea_notes" id="terms_conditions" name="terms_conditions" rows="3" style="font-size: 1.0rem; color: #495057; font-weight: 400;">We reserve the right to correct the pricing offered due to any typographical errors. This offer is not valid for end users from Iran, Iraq, North Korea, Cuba, Sudan & Syria.</textarea>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" rowspan="10"></td>
												<td colspan="2">Net Total</td>
												<td colspan="2" id="net_total"><?php if(isset($quote_details)) echo $quote_details[0]['net_total']; ?></td>
											</tr>
											<tr>
												<td colspan="2">Discount Type</td>
												<td colspan="2">
													<select id="discount_type" class="form-control" name="discount_type">
														<option value="percent" <?php if(isset($quote_details) && $quote_details[0]['discount_type'] == 'percent') echo 'selected="selected";';?>>Percent</option>
														<option value="value" <?php if(isset($quote_details) && $quote_details[0]['discount_type'] == 'value') echo 'selected="selected";';?>>Value</option>
													</select>
												</td>
											</tr>
											<tr>
												<td colspan="2">Discount</td>
												<td colspan="2"><input type="text" name="discount" id="discount" class="form-control validate[custom[onlyNumberSp]]" value="<?php if(isset($quote_details)) echo $quote_details[0]['discount']; ?>"></td>
											</tr>
											<tr>
												<td colspan="2">Freight <button class="btn btn-sm btn-warning" type="button" data-toggle="modal" data-target="#freight_calc_modal"><i class="fa fa-calculator" aria-hidden="true"></i></button></td>
												<td colspan="2"><input type="text" name="freight" id="freight" class="form-control" name="freight" value="<?php if(isset($quote_details)) echo $quote_details[0]['freight']; ?>"></td>
											</tr>
											<tr>
												<td colspan="2">Bank Charges</td>
												<td colspan="2"><input type="text" name="bank_charges" id="bank_charges" class="form-control" value="<?php if(isset($quote_details)) echo $quote_details[0]['bank_charges']; ?>"></td>
											</tr>
											<tr id="gst_tr" <?php if(isset($quote_details) && $quote_details[0]['gst'] > 0) echo 'style="display: contents;"'; else echo 'style="display: none;"';?>>
												<td colspan="2">GST (18%)</td>
												<td colspan="2"><span id="gst_span"><?php if(isset($quote_details) && $quote_details[0]['gst'] > 0) echo $quote_details[0]['gst']; ?></span><input type="hidden" name="gst" id="gst" value="<?php if(isset($quote_details) && $quote_details[0]['gst'] > 0) echo $quote_details[0]['gst']; ?>"></td>
											</tr>
											<tr>
												<td colspan="4">
													<div class="accordion" id="accordionExample">
														<div class="card">
															<div class="card-header" id="headingOne">
																<button class="btn btn-primary btn-xs pull-right" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
																	( + )
																</button>
															</div>

															<div id="collapseOne" class="collapse <?php if(isset($quote_details) && $quote_details[0]['other_charges'] > 0) echo 'show';?>" aria-labelledby="headingOne" data-parent="#accordionExample">
																<div class="card-body">
																	<table class="table table-bordered">
																		<tr>
																			<td width="50%">Other Charges</td>
																			<td width="50%"><input type="text" name="other_charges" id="other_charges" class="form-control validate[custom[onlyNumberSp]]" value="<?php if(isset($quote_details)) echo $quote_details[0]['other_charges']; ?>"></td>
																		</tr>
																	</table>
																</div>
															</div>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" style="font-weight: bold">Grand Total</td>
												<td colspan="2" id="grand_total"><?php if(isset($quote_details)) echo $quote_details[0]['grand_total']; ?></td>
											</tr>
											<tr style="display: none;" id="additional_comment_tr">
												<td colspan="4">
													<label>Additional Comment</label>
													<textarea class="form-control validate[required]" name="additional_comment" id="additional_comment"></textarea>
												</td>
											</tr>
											<?php }else{ ?>
												<tr>
													<?php
														if($this->session->userdata('role') == 8){

															echo '<td colspan="3" rowspan="10">';		
														}else{
															
															echo '<td colspan="4" rowspan="10">';		
														}
													?>
														<div class="kt-portlet__head">
															<div class="kt-portlet__head-label">
																<span class="kt-portlet__head-icon">
																	<i class="flaticon-edit-1" style="color: #343a40;font-size: 3.3rem;"></i>
																</span>
																<h3 class="kt-portlet__head-title">
																	Additional Notes
																</h3>
															</div>
														</div>
														<div class="alert alert-light alert-elevate fade show" role="alert" style="padding: 10px;">
															<div class="alert-text span_notes" style="padding: 10px;">
																<span class="span_notes" style="font-size: 1.0rem; color: #495057; font-weight: 400;">
																	<?php 
																		echo (isset($quote_details)) ? $quote_details[0]['comments'] : '';
																	?>
																</span>
															</div>
															<div class="alert-text textarea_notes" style="padding: 10px; display:none;">
																<textarea class="form-control textarea_notes" id="terms_conditions" name="terms_conditions" rows="3" style="font-size: 1.0rem; color: #495057; font-weight: 400;"><?php echo (isset($quote_details)) ? $quote_details[0]['comments'] : ''; ?></textarea>
															</div>
														</div>
													</td>
												</tr>
												<tr>
													<?php
														if($this->session->userdata('role') == 8){

															echo '<td colspan="2" rowspan="10"></td>';		
														}else{
															
															echo '<td colspan="3" rowspan="10"></td>';		
														}
													?>
													
													<td colspan="2">Net Total</td>
													<td colspan="2" id="net_total"><?php if(isset($quote_details)) echo $quote_details[0]['net_total']; ?></td>
												</tr>
												<tr>
													<td colspan="2">Discount Type</td>
													<td colspan="2">
														<select id="discount_type" class="form-control" name="discount_type">
															<option value="percent">Percent</option>
															<option value="value" selected="selected">Value</option>
														</select>
													</td>
												</tr>
												<tr>
													<td colspan="2">Discount</td>
													<td colspan="2"><input type="text" name="discount" id="discount" class="form-control validate[custom[onlyNumberSp]]" value="<?php if(isset($quote_details)) echo $quote_details[0]['discount']; ?>"></td>
												</tr>
												<tr>
													<td colspan="2">Freight <button class="btn btn-sm btn-warning" type="button" data-toggle="modal" data-target="#freight_calc_modal"><i class="fa fa-calculator" aria-hidden="true"></i></button></td>
													<td colspan="2"><input type="text" name="freight" id="freight" class="form-control" name="freight" value="<?php if(isset($quote_details)) echo $quote_details[0]['freight']; ?>"></td>
												</tr>
												<tr>
													<td colspan="2">Bank Charges</td>
													<td colspan="2"><input type="text" name="bank_charges" id="bank_charges" class="form-control" value="<?php if(isset($quote_details)) echo $quote_details[0]['bank_charges']; ?>"></td>
												</tr>
												<tr id="gst_tr" <?php if(isset($quote_details) && $quote_details[0]['gst'] > 0) echo 'style="display: contents;"'; else echo 'style="display: none;"';?>>
													<td colspan="2">GST (18%)</td>
													<td colspan="2"><span id="gst_span"><?php if(isset($quote_details) && $quote_details[0]['gst'] > 0) echo $quote_details[0]['gst']; ?></span><input type="hidden" name="gst" id="gst" value="<?php if(isset($quote_details) && $quote_details[0]['gst'] > 0) echo $quote_details[0]['gst']; ?>"></td>
												</tr>
												<tr>
													<td colspan="4">
														<div class="accordion" id="accordionExample">
															<div class="card">
																<div class="card-header" id="headingOne">
																	<button class="btn btn-primary btn-xs pull-right" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
																		( + )
																	</button>
																</div>

																<div id="collapseOne" class="collapse <?php if(isset($quote_details) && $quote_details[0]['other_charges'] > 0) echo 'show';?>" aria-labelledby="headingOne" data-parent="#accordionExample">
																	<div class="card-body">
																		<table class="table table-bordered">
																			<tr>
																				<td width="50%">Other Charges</td>
																				<td width="50%"><input type="text" name="other_charges" id="other_charges" class="form-control validate[custom[onlyNumberSp]]" value="<?php if(isset($quote_details)) echo $quote_details[0]['other_charges']; ?>"></td>
																			</tr>
																		</table>
																	</div>
																</div>
															</div>
														</div>
													</td>
												</tr>
												<tr>
													<td colspan="2" style="font-weight: bold">Grand Total</td>
													<td colspan="2" id="grand_total"><?php if(isset($quote_details)) echo $quote_details[0]['grand_total']; ?></td>
												</tr>
												<tr style="display: none;" id="additional_comment_tr">
													<td colspan="4">
														<label>Additional Comment</label>
														<textarea class="form-control validate[required]" name="additional_comment" id="additional_comment"></textarea>
													</td>
												</tr>
											<?php } ?>
										</tfoot>
									</table>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-lg-12">
									<h5>Terms & Conditions</h5>
								</div>

								<div class="col-lg-12">
									<table class="table">
										<tr>
											<td width="50%">
												<table class="table table-bordered">
													<tr>
														<td>Delivered To</td>
														<td id="delivered_to_td">
															<select id="delivered_through" class="form-control" name="delivered_through">
																<?php foreach($delivery as $option){
																	$selected = '';
																	if(isset($quote_details) && $option['delivery_id'] == $quote_details[0]['delivered_through']){
																		$selected = 'selected = "selected"';
																	}
																?>
																	<option <?php echo $selected; ?> value="<?php echo $option['delivery_id']; ?>" trans_id = "<?php echo $option['transport_id']; ?>"><?php echo $option['delivery_name']; ?></option>
																<?php } ?>
															</select>
														</td>
													</tr>
													<tr>
														<td>Delivery Time</td>
														<td>
															<div class="input-group">
																<select class="form-control" id="delivery_time" name="delivery_time">
																	<?php foreach($delivery_time as $option){
																		$selected = '';
																		if(isset($quote_details) && $option['dt_id'] == $quote_details[0]['delivery_time']){
																			$selected = 'selected = "selected"';
																		}
																	?>
																		<option <?php echo $selected; ?> value="<?php echo $option['dt_id']; ?>"><?php echo $option['dt_value']; ?></option>
																	<?php } ?>
																</select>
																<!-- <div class="input-group-append">
																	<button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#add_delivery_time">+</button>
																</div> -->
															</div>
														</td>
													</tr>
													<tr>
														<td>Payment</td>
														<td>
															<div class="input-group">
																<select class="form-control" id="payment_term" name="payment_term">
																	<?php foreach($payment_terms as $option){ 
																		$selected = '';
																		if(isset($quote_details) && $option['term_id'] == $quote_details[0]['payment_term']){
																			$selected = 'selected = "selected"';
																		}
																	?>
																		<option <?php echo $selected; ?> value="<?php echo $option['term_id']; ?>"><?php echo $option['term_value']; ?></option>
																	<?php } ?>
																</select>
																<!-- <div class="input-group-append">
																	<button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#add_payment_terms">+</button>
																</div> -->
															</div>
														</td>
													</tr>
													<tr>
														<td>Validity</td>
														<td>
															<div class="input-group">
																<select class="form-control" id="validity" name="validity">
																	<?php foreach($validity as $option){ 
																		$selected = '';
																		if(isset($quote_details) && $option['validity_id'] == $quote_details[0]['validity']){
																			$selected = 'selected = "selected"';
																		}
																	?>
																		<option <?php echo $selected; ?> value="<?php echo $option['validity_id']; ?>"><?php echo $option['validity_value']; ?></option>
																	<?php } ?>
																</select>
																<!-- <div class="input-group-append">
																	<button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#add_validity">+</button>
																</div> -->
															</div>
														</td>
													</tr>
													<!-- <tr>
														<td>Make</td>
														<td>
															<div class="input-group">
																<select id="make" class="form-control">
																	<option>OM Tubes</option>
																</select>
																<div class="input-group-append">
																	<button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#add_delivery_time">+</button>
																</div>
															</div>
														</td>
													</tr> -->
												</table>
											</td>
											<td width="50%">
												<table class="table table-bordered">
													<tr>
														<td>Currency</td>
														<td>
															<select id="currency" class="form-control" name="currency">
																<?php foreach($currency as $option){ 
																	$selected = '';
																	if(isset($quote_details) && $option['currency'] == $quote_details[0]['currency']){
																		$selected = 'selected = "selected"';
																		if($quote_details[0]['currency_rate'] > 0){
																			$option['currency_rate'] = $quote_details[0]['currency_rate'];
																		}
																	}
																?>
																	<option <?php echo $selected; ?> rate="<?php echo $option['currency_rate']; ?>" value="<?php echo $option['currency_id']; ?>"><?php echo $option['currency']; ?></option>
																<?php } ?>
															</select>
															<input type="hidden" name="currency_rate" id="currency_rate" <?php if(isset($quote_details)) { echo 'value="'.$quote_details[0]['currency_rate'].'"'; } ?>>
														</td>
													</tr>
													<tr>
														<td>Country of Origin</td>
														<td>
															<div class="input-group">
																<select id="origin_country" class="form-control" name="origin_country">
																	<?php foreach($origin_country as $option){ 
																		$selected = '';
																		if(isset($quote_details) && $option['country_id'] == $quote_details[0]['origin_country']){
																			$selected = 'selected = "selected"';
																		}
																	?>
																		<option <?php echo $selected; ?> value="<?php echo $option['country_id']; ?>"><?php echo $option['country']; ?></option>
																	<?php } ?>
																</select>
																<!-- <div class="input-group-append">
																	<button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#add_origin">+</button>
																</div> -->
															</div>
														</td>
													</tr>
													<tr>
														<td>MTC Type</td>
														<td>
															<select class="form-control" id="mtc_type" name="mtc_type">
																<?php foreach($mtc_type as $option){ 
																	$selected = '';
																	if(isset($quote_details) && $option['mtc_id'] == $quote_details[0]['mtc_type']){
																		$selected = 'selected = "selected"';
																	}
																?>
																	<option <?php echo $selected; ?> value="<?php echo $option['mtc_id']; ?>"><?php echo $option['mtc_value']; ?></option>
																<?php } ?>
															</select>
														</td>
													</tr>
													<tr>
														<td>Packing Type</td>
														<td>
															<select class="form-control" id="transport_mode" name="transport_mode">
																<?php foreach($transport_mode as $option){ 
																	$selected = '';
																	if(isset($quote_details) && $option['mode_id'] == $quote_details[0]['transport_mode']){
																		$selected = 'selected = "selected"';
																	}
																?>
																	<option <?php echo $selected; ?> value="<?php echo $option['mode_id']; ?>"><?php echo $option['mode']; ?></option>
																<?php } ?>
															</select>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<div class="kt-portlet__foot">
							<div class="kt-form__actions">
								<div class="row">
									<div class="col-lg-4"></div>
									<div class="col-lg-8">
										<?php if(isset($quote_details)) echo '<input type="hidden" name="quote_id" value="'.$quote_id.'">'; ?>
										<button type="submit" class="btn btn-primary quotation_list_save_button" submit_count=0 name="save_quotation">Save Quotation</button>
									</div>
								</div>
							</div>
						</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<!--begin::Modal-->

<div class="modal fade" id="add_query" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<form id="sample_query_form">
					<div class="row">

						<!--Begin:: App Content-->
						<div class="kt-grid__item kt-grid__item--fluid kt-app__content" id="kt_chat_content" style="width:100%; padding: 0px 24px 0px 24px;">
							<div class="kt-chat">
								<div class="kt-portlet kt-portlet--head-lg kt-portlet--last">
									<div class="kt-portlet__head">
										<div class="kt-chat__head ">
											<div class="kt-chat__left">

												<!--begin:: Aside Mobile Toggle -->
												<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md kt-hidden-desktop" id="kt_chat_aside_mobile_toggle">
													<i class="flaticon2-open-text-book"></i>
												</button>

												<!--end:: Aside Mobile Toggle-->
												<div class="dropdown dropdown-inline">
													<div class="kt-chat__label">
														<a href="#" class="kt-chat__title">Sales Query Informations</a>
													</div>
												</div>
											</div>
											<div class="kt-chat__center">
												<div class="kt-chat__label">

												</div>
												<div class="kt-chat__pic kt-hidden">
													<span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Jason Muller">
														<img src="assets/media/users/300_12.jpg" alt="image">
													</span>
													<span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Nick Bold">
														<img src="assets/media/users/300_11.jpg" alt="image">
													</span>
													<span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Milano Esco">
														<img src="assets/media/users/100_14.jpg" alt="image">
													</span>
													<span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Teresa Fox">
														<img src="assets/media/users/100_4.jpg" alt="image">
													</span>
												</div>
											</div>
											<div class="kt-chat__right">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
											</div>
										</div>
									</div>
									<div class="kt-portlet__body">
										<div class="kt-scroll quotation_sample_query_history" data-scroll="true" data-height="400">

										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-chat__input">
											<div class="kt-chat__editor">
												<input type="text" class="form-control" name="query_type" value="" hidden />
												<input type="text" class="form-control" name="query_reference" value="" hidden />
												<input type="text" class="form-control" name="query_reference_id" value="" hidden />
												<textarea style="height: 50px" placeholder="Type here..." name="query_text"></textarea>
												<input type="text" class="form-control" name="query_creator_id" value="" hidden />
												<input type="text" class="form-control" name="query_status" value="open" hidden />
											</div>
											<div class="kt-chat__toolbar">
												<div class="kt_chat__tools">
													<div class="query_type">

													</div>
												</div>
												<div class="kt_chat__actions">
													<button type="button" class="btn btn-brand btn-md btn-upper btn-bold kt-chat__reply save_sample_query">reply</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<!--End:: App Content-->
					</div>
				</form>
			</div>
			<div class="modal-footer">

			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="add_company" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<?php echo form_open('', array('id' => 'addCompany', 'class' => 'kt-form kt-form--label-right', 'autocomplete' => 'off'));?>
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add Client</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="recipient-name" class="form-control-label">Company Name:</label>
								<input type="text" class="form-control validate[required] name" id="name" name="name">
							</div>
							<div class="row">
								<div class="col-12">
									<div id="company_result_client" style="background-color: #fff; z-index: 100; position: absolute; border: 1px solid; width: 80%; max-height: 100px; height: 100px; overflow-y: scroll; display: none; margin-top: -25px;">
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="message-text" class="form-control-label">Country:</label>
								<select class="form-control validate[required]" id="country_id" name="country_id">
									<option value="" disbaled>Select</option>
									<?php 
										foreach ($country as $key => $value) {
											echo '<option value="'.$value['id'].'" region_id="'.$value['region_id'].'" readonly>'.ucwords(strtolower($value['name'])).'</option>';
										}
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="message-text" class="form-control-label">Region:</label>
								<select class="form-control validate[required]" id="region_id" name="region_id">
									<option value="" disbaled>Select</option>
									<?php 
										foreach ($region as $key => $value) {
											echo '<option value="'.$value['id'].'" >'.ucwords(strtolower($value['name'])).'</option>';
										}
									?>
								</select>
							</div>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="message-text" class="form-control-label">Website:</label>
								<input type="text" class="form-control validate[custom[url]]" id="website" name="website">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Add Client</button>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>

<div class="modal fade" id="add_member" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<?php echo form_open('', array('id' => 'addMember', 'class' => 'kt-form kt-form--label-right'));?>
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add Company Member</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-3 col-sm-12">
							<label for="message-text" class="form-control-label">Name:</label>
							<input type="text" class="form-control validate[required]" id="member_name" name="member_name">
						</div>

						<div class="col-md-3 col-sm-12">
							<label for="message-text" class="form-control-label">Email:</label>
							<input type="email" class="form-control validate[required,custom[email]]" id="email" name="email">
						</div>

						<div class="col-md-3 col-sm-12">
							<label for="message-text" class="form-control-label">Mobile #:</label>
							<input type="number" class="form-control validate[custom[onlyNumberSp]]" id="mobile" name="mobile">
						</div>

						<div class="col-md-3 col-sm-12">
							<label for="message-text" class="form-control-label">Telephone:</label>
							<input type="number" class="form-control validate[custom[onlyNumberSp]]" id="telephone" name="telephone">
						</div>
					</div>
					<div class="row">	
						<div class="col-md-3 col-sm-12">
							<label for="message-text" class="form-control-label">Whatsapp #:</label>
							<!-- <input type="number" class="form-control validate[custom[onlyNumberSp]]" id="is_whatsapp" name="is_whatsapp"> -->
							<select class="form-control" id="is_whatsapp" name="is_whatsapp">
								<!-- <option value="">Select</option> -->
								<option value="Yes">Yes</option>
								<option value="No">No</option>
							</select>
						</div>

						<div class="col-md-3 col-sm-12">
							<label for="message-text" class="form-control-label">Skype ID:</label>
							<input type="text" class="form-control" id="skype" name="skype">
						</div>

						<div class="col-md-3 col-sm-12">
							<label for="message-text" class="form-control-label">Main Buyer  #:</label>
							<!-- <input type="number" class="form-control validate[custom[onlyNumberSp]]" id="is_whatsapp" name="is_whatsapp"> -->
							<select class="form-control" id="main_buyer" name="main_buyer">
								<!-- <option value="">Select</option> -->
								<option value="Yes">Yes</option>
								<option value="No">No</option>
							</select>
						</div>

						<div class="col-md-3 col-sm-12">
							<label for="message-text" class="form-control-label">Other Member #:</label>
							<!-- <input type="number" class="form-control validate[custom[onlyNumberSp]]" id="is_whatsapp" name="is_whatsapp"> -->
							<select class="form-control" id="other_member" name="other_member">
								<option value="">Select</option>
								<option value="Yes">Yes</option>
								<option value="No">No</option>
							</select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" id="addMemberBtn" class="btn btn-primary">Add Member</button>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
<!--end::Modal-->


<div class="modal fade" id="notes-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Notes</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<div id="tab_history">
		        	<table class="table table-bordered" id="notes_table">
		        		<thead>
		            		<tr>
		            			<th width="70%">Notes</th>
		            			<th>Added On</th>
		            		</tr>
		            	</thead>
		            	<tbody>
		        		<?php 
		        			foreach ($rfq_notes as $key => $value) {
		        				echo '<tr connect_id = "'.$value['connect_id'].'" type="'.$value['type'].'">
		        					<td>'.$value['note'].'</td>
		        					<td>'.date('d M', strtotime($value['entered_on'])).'</td>
		        				</tr>';
		        			}
		        		?>
		        		</tbody>
		        	</table>
		        </div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="add_delivery_time" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<?php echo form_open('', array('id' => 'delivery_time_frm')); ?>
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add Delivery Time</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<input type="text" id="new_delivery_time" class="form-control validate[required]" placeholder="Add new value">
					</div>
				</div>
				<div class="modal-footer">
					<button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" id="addDeliveryBtn" class="btn btn-primary">Submit</button>
				</div>
			</div>
		<?php echo form_close(); ?>
	</div>
</div>


<div class="modal fade" id="add_payment_terms" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<?php echo form_open('', array('id' => 'payment_term_frm')); ?>
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add Payment Term</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<input type="text" id="new_payment_term" class="form-control validate[required]" placeholder="Add new value">
					</div>
				</div>
				<div class="modal-footer">
					<button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" id="addPaymentBtn" class="btn btn-primary">Submit</button>
				</div>
			</div>
		<?php echo form_close(); ?>
	</div>
</div>

<div class="modal fade" id="add_validity" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<?php echo form_open('', array('id' => 'validity_frm')); ?>
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add Validity</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<input type="text" id="new_validity" class="form-control validate[required]" placeholder="Add new value">
					</div>
				</div>
				<div class="modal-footer">
					<button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" id="addValidtyBtn" class="btn btn-primary">Submit</button>
				</div>
			</div>
		<?php echo form_close(); ?>
	</div>
</div>

<div class="modal fade" id="add_origin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<?php echo form_open('', array('id' => 'origin_frm')); ?>
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add Origin Country</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<input type="text" id="new_origin" class="form-control validate[required]" placeholder="Add new value">
					</div>
				</div>
				<div class="modal-footer">
					<button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" id="addOriginBtn" class="btn btn-primary">Submit</button>
				</div>
			</div>
		<?php echo form_close(); ?>
	</div>
</div>

<div class="modal fade" id="add_unit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<?php echo form_open('', array('id' => 'unit_frm')); ?>
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add Unit</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<input type="text" id="new_unit" class="form-control validate[required]" placeholder="Add new value">
					</div>
				</div>
				<div class="modal-footer">
					<button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" id="addUnitBtn" class="btn btn-primary">Submit</button>
				</div>
			</div>
		<?php echo form_close(); ?>
	</div>
</div>

<div class="modal fade" id="add_product" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<?php echo form_open('', array('id' => 'product_frm')); ?>
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add Product</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<input type="text" id="new_product" class="form-control validate[required]" placeholder="Add new value">
					</div>
				</div>
				<div class="modal-footer">
					<button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" id="addProductBtn" class="btn btn-primary">Submit</button>
				</div>
			</div>
		<?php echo form_close(); ?>
	</div>
</div>

<div class="modal fade" id="freight_calc_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Freight Calculator</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<?php echo form_open('', array('id' => 'freight_calc')); ?>
					<div class="row">
						<div class="col-md-6 form-group">
							<label for="fc_trans_mode">Transport Mode</label>
							<select class="form-control validate[required]" id="fc_trans_mode">
								<option value="sea">Sea Worthy</option>
								<option value="air">Air Worthy</option>
							</select>
						</div>
						<div class="col-md-6 form-group">
							<label for="fc_terms">Terms</label>
							<select class="form-control validate[required]" id="fc_terms">
								<option value="">Select</option>
								<option value="CIF" type="sea">CIF</option>
								<option value="FOB" type="sea">FOB</option>
								<option value="DDU" type="air" style="display: none;">DDU</option>
								<option value="FOB" type="air" style="display: none;">FOB</option>
							</select>
						</div>
						<div class="col-md-6 form-group">
							<label for="fc_weight">Weight</label>
							<input type="text" id="fc_weight" class="form-control validate[required,custom[onlyNumberSp]]" placeholder="in kilograms">
						</div>
						<div class="col-md-6 form-group">
							<label for="fc_overlength">Overlength Pipes</label>
							<select id="fc_overlength" class="form-control validate[required]">
								<option value="no">No</option>
								<option value="yes">Yes</option>
							</select>
						</div>
						<div class="col-md-6 form-group" id="sea_country">
							<label for="fc_country">Country</label>
							<select id="fc_country" class="form-control validate[required]">
								<option value="">Select Country</option>
								<?php foreach($ports as $port){?>
									<option value="<?php echo $port['country']; ?>" port="<?php echo $port['port_name']; ?>" per_ton="<?php echo $port['per_ton']; ?>"><?php echo $port['country']; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-md-6 form-group" id="air_country" style="display: none;" >
							<label for="fc_country_air">Country</label>
							<select id="fc_country_air" class="form-control validate[required]">
								<option value="">Select Country</option>
								<?php foreach($zones as $zone){?>
									<option value="<?php echo $zone['country']; ?>" zone="<?php echo $zone['zone_id']; ?>"><?php echo $zone['country']; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-md-6 form-group">
							<label for="fc_port">Port</label>
							<input type="text" disabled="disabled" id="fc_port" class="form-control">
						</div>
						<div class="col-md-12 form-group pull-right">
							<button type="submit" id="fc_submit" class="btn btn-success pull-right">Caclculate</button>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 form-group">
							<label for="fc_value">Freight Value (in USD)</label>
							<input type="text" disabled="disabled" id="fc_value" class="form-control">
						</div>
					</div>
				<?php echo form_close(); ?>
				<style>
					#freight_calc_modal .select2-container{
						display: block;
					}
				</style>
			</div>
			<div class="modal-footer">
				<button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<style>
	thead th{
		text-align: center;
	}
</style>