<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket_management extends MX_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('common/common_model');
		$this->load->model('ticket_management/ticket_model');
	}

	public function ticket_list() {

		$data =  $this->prepare_listing_data_for_ticket($this->create_where_for_ticket_listing());
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => ''));
		$this->load->view('sidebar', array('title' => 'User Data'));
		$this->load->view('ticket_management/ticket_index', $data);
		$this->load->view('footer');
	}

	public function ticket_template_master() {

		$data['template_list'] =  $this->common_model->get_dynamic_data_sales_db('*', array('status'=> 'Active', 'type'=>'system_admin'), 'ticket');

		$this->load->view('header', array('title' => ''));
		$this->load->view('sidebar', array('title' => 'User Data'));
		$this->load->view('ticket_management/ticket_template_index', $data);
		$this->load->view('footer');
	}


    public function ajax_function() {

        if($this->input->is_ajax_request()) {
        	$post_details = $this->input->post();
            $response['status'] = 'successful';
            switch ($post_details['call_type']) {
            	case 'save_ticket_details':

            	    if(!empty($post_details['form_data'])){

            	    	$form_data =  array_column($post_details['form_data'], 'value', 'name');
            	    	$insert_data['ticket_id'] = $form_data['ticket_id'];
            	    	$insert_data['user_comment'] = $form_data['user_comment'];
            	    	$insert_data['ticket_close_time'] = date('Y-m-d 23:59:59', strtotime($form_data['ticket_close_time']));
            	    	$insert_data['user_id']=$this->session->userdata('user_id');
            	    	$insert_data['user_department_id']=$this->session->userdata('role');
            	    	$insert_data['add_time'] = date('Y-m-d H:i:s');
            	    	$insert_data['update_time'] = date('Y-m-d H:i:s');
                        $this->common_model->insert_data_sales_db('ticket_management_details',$insert_data);
						$sms_txt = "Dear System Admin,".$this->session->userdata('name')."  raised a ticket for you. - Om Tubes.";
                		$this->sendSms('8591412252', str_replace(' ', '%20', $sms_txt), '1307164551233434491');
                    }
	            	   
        	   	break;

                case'ticket_action':

                	if(!empty($post_details['update_data']) && !empty($post_details['where_data'])){

             			if($post_details['update_data']['ticket_status'] == "Done") {
							$activity_information = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'id'=>$post_details['where_data']['id']), 'ticket_management_details', 'row_array');
							// echo "<pre>";print_r($activity_information);echo"</pre><hr>";exit;
							$user_information = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'1', 'user_id'=>$activity_information['user_id']), 'users', 'row_array');
							// echo "<pre>";print_r($user_information);echo"</pre><hr>";exit;
							if(!empty($user_information['name']) && !empty($user_information['mobile'])) {
								
	             				$sms_txt = "Hello ".$user_information['name'].",system admin has solved your ticket. - Om Tubes.";
	                			$this->sendSms($user_information['mobile'], str_replace(' ', '%20', $sms_txt), '1307164561780551241');	
							}
             			}
         				$this->common_model->update_data_sales_db('ticket_management_details',$post_details['update_data'],$post_details['where_data']);
                	}
                break;

                case 'submit_search_filter':
					
					// echo "<pre>";print_r($post_details['form_data']);echo"</pre><hr>";exit;
					if(!empty($post_details['form_data'])) {

						$data =  $this->prepare_listing_data_for_ticket($this->create_where_for_ticket_listing($post_details['ticket_status'], $post_details['form_data']));
					}else{

						$data =  $this->prepare_listing_data_for_ticket($this->create_where_for_ticket_listing($post_details['ticket_status']));
					}
					$response['ticket_body_html'] = $this->load->view('ticket_management/ticket_list_body', $data, true);

				break;

				case 'add_edit_ticket_template_activity_form_data':
				
					$data = array();
					$data['ticket_template_search_filter']['ticket_repeat_day'] = array('First Day', 'Last Day', 'Every Monday', 'Every Tuesday', 'Every Wednesday', 'Every Thursday', 'Every Friday', 'Every Saturday', 'First Monday', 'Second Monday', 'Third Monday', 'Fourth Monday', 'Fifth Monday', 'First Tuesday', 'Second Tuesday', 'Third Tuesday', 'Fourth Tuesday', 'Fifth Tuesday', 'First Wednesday', 'Second Wednesday', 'Third Wednesday', 'Fourth Wednesday', 'Fifth Wednesday', 'First Thursday', 'Second Thursday', 'Third Thursday', 'Fourth Thursday', 'Fifth Thursday', 'First Friday', 'Second Friday', 'Third Friday', 'Fourth Friday', 'Fifth Friday', 'First Saturday', 'Second Saturday', 'Third Saturday', 'Fourth Saturday', 'Fifth Saturday');
					$data['activity_information'] = array();
					if(!empty($post_details['id'])) {

						$data['activity_information'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'id'=>$post_details['id']), 'ticket', 'row_array');
						// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
					}	

					$response['ticket_template_activity_form'] = $this->load->view('ticket_management/ticket_activity_add_edit_form', $data, true);
				break;

				case 'add_edit_ticket_template_activity':
					$form_data = array_column($post_details['form_data'], 'value', 'name');
					if(!empty($form_data['id'])) {

						$form_data['update_time'] = date('Y-m-d H:i:s');
						$this->common_model->update_data_sales_db('ticket', array($form_data), 'id', 'batch');
					}else{

						$form_data['type'] = "system_admin";
						$form_data['add_time'] = date('Y-m-d H:i:s');
						$form_data['update_time'] = date('Y-m-d H:i:s');
						$this->common_model->insert_data_sales_db('ticket', $form_data);
					}
					// echo "<pre>";print_r($form_data);echo"</pre><hr>";exit;

				break;

				case 'delete_ticket_template_activity':

					if(!empty($post_details['id'])) {

						$this->common_model->update_data_sales_db('ticket', array('status'=> 'Inactive'), array('id'=>$post_details['id']));
					}						
				break;
				case 'ticket_management_graph':

	                $where_array = array();
	                $explode_filter_date=array();
					$where_array['status']='"Active"';
	                if(!empty($post_details['filter_date'])){
						$explode_filter_date = explode(' - ', $post_details['filter_date']);
						
						if(count($explode_filter_date) == 1){

							$where_array['add_time >='] = "'".date('Y-m-d 00:00:00', strtotime($explode_filter_date[0]))."'";
							$where_array['add_time <='] = "'".date('Y-m-d 23:59:59', strtotime($explode_filter_date[0]))."'";
						} else {

							$where_array['add_time >='] = "'".date('Y-m-d 00:00:00', strtotime($explode_filter_date[0]))."'";
							$where_array['add_time <='] = "'".date('Y-m-d 23:59:59', strtotime($explode_filter_date[1]))."'";
	                    }
	                }    
                	$pending_data = $accepted_data = $completed_data = $overdue_data = $rejected_data =  array();
					$ticket =$this->ticket_model->get_ticket_listing_graph_data($where_array);
				 	$category_array = $pending_count = $accept_count = $completed_count = $overdue_count = $reject_count = array();
				 	
				 	foreach ($ticket as $data_value) {
				 		
				 		$category_key = '';	
				 		if(in_array(date('Y-m-d',strtotime($data_value['add_time'])), $category_array)){

				 			$category_key = array_search(date('Y-m-d',strtotime($data_value['add_time'])), $category_array);
				 		}else{

				 			$category_array[] = date('Y-m-d',strtotime($data_value['add_time']));
			 				$category_key = array_search(date('Y-m-d',strtotime($data_value['add_time'])), $category_array);
			 				$pending_count[$category_key] = 0;
			 				$accept_count[$category_key] = 0;
			 				$completed_count[$category_key] = 0;
			 				$overdue_count[$category_key] = 0;
			 				$reject_count[$category_key] = 0;
				 		}
				 		if($category_key >= 0) {

					 		if($data_value['ticket_status'] == 'Pending'){

								$pending_count[$category_key] += 1;
							}else if($data_value['ticket_status'] == 'Accept'){

								$accept_count[$category_key] += 1;
							}else if($data_value['ticket_status'] == 'Done'){

								$completed_count[$category_key] += 1;
							}else if($data_value['ticket_status'] == 'Overdue'){

								$overdue_count[$category_key] += 1;
							}else if($data_value['ticket_status'] == 'Reject'){

								$reject_count[$category_key] += 1;
							}
				 		}
				 	}
				 	if(!empty($category_array)){
				 		foreach ($category_array as $category_key => $category_date) {
				 			$category_array[$category_key] = date('j,F', strtotime($category_date));
				 		}
				 	}
				 	$response['ticket_management_series_data'] =  array(
																	array(	
															            'name' => 'Pending',
															            'data' => $pending_count
															        ), array(
															            'name' => 'Accepted',
															            'data' => $accept_count
															        ), array(
															            'name' => 'Completed',
															            'data' => $completed_count
															        ), array(
															            'name' => 'Overdue',
															            'data' => $overdue_count
															        ), array(
															            'name' => 'Rejected',
															            'data' => $accept_count
															        )
																);
				 	$response['ticket_management_category'] = $category_array;
					// echo "<pre>";print_r($category_array);echo"</pre><hr>";
					// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
				break;

		 		default:
		 			$response['status'] = 'failed';	
		 			$response['message'] = "call type not found";
	 			break;
		 	}
		 	echo json_encode($response);

       	}else{
		 	die('access is not allowed to this function');
		}
	}

   	private function prepare_listing_data_for_ticket(
											$where_array,
											$order_by_array = array('column_name'=>'ticket_close_time', 'column_value'=>'asc'),
											$limit = 0,
											$offset = 0) {

		$return_array = array();
		$ticket_type = array_column($this->common_model->get_dynamic_data_sales_db('id, name', array('status'=>'Active'), 'ticket'), 'name', 'id');
		$user_details = array_column($this->common_model->get_dynamic_data_sales_db('user_id, name', array('status'=>'1'), 'users'), 'name', 'user_id');
		$user_role_details = array_column($this->common_model->get_dynamic_data_sales_db('role_id, role_name', array(), 'role'), 'role_name', 'role_id');
		$return_array = $this->ticket_model->get_ticket_listing_data($where_array, $order_by_array, $limit, $offset);
		foreach ($return_array['ticket_list'] as $ticket_key => $ticket_details) {
			
			$return_array['ticket_list'][$ticket_key]['ticket_name'] = $ticket_type[$ticket_details['ticket_id']];
			$return_array['ticket_list'][$ticket_key]['user_name'] = $user_details[$ticket_details['user_id']];
			$return_array['ticket_list'][$ticket_key]['start_date'] = date('y-M-d H:i:s', strtotime($ticket_details['add_time']));
			$return_array['ticket_list'][$ticket_key]['end_date'] = date('y-M-d H:i:s', strtotime($ticket_details['ticket_close_time']));
			$return_array['ticket_list'][$ticket_key]['access_ticket_active'] = $return_array['ticket_list'][$ticket_key]['access_ticket_completed'] = $return_array['ticket_list'][$ticket_key]['access_ticket_reject'] = $return_array['ticket_list'][$ticket_key]['access_ticket_delete'] = false;
			// if(!empty($this->common_model->get_dynamic_data_sales_db('user_id', array('status'=>'Active', 'user_id'=>$ticket_details['user_id'],'reporting_manager'=>$this->session->userdata('user_id')), 'organization_chart'))){

			// 	if($ticket_details['ticket_status'] == 'Pending') {
					
			// 	}
			// }
			if($ticket_details['user_id'] == $this->session->userdata('user_id')) {
				$return_array['ticket_list'][$ticket_key]['access_ticket_delete'] = true;
			}
			if($this->session->userdata('user_id') == 67) {

				$return_array['ticket_list'][$ticket_key]['access_ticket_completed'] = true;
				$return_array['ticket_list'][$ticket_key]['access_ticket_active'] = true;
				$return_array['ticket_list'][$ticket_key]['access_ticket_reject'] = true;
			}

		}
		// preparing search filter data
		$return_array['ticket_search_filter']['ticket_type'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'type'=>'user'), 'ticket');
		$ticket_status_column_details = $this->ticket_model->get_column_details('ticket_management_details', 'ticket_status'); 
		$return_array['ticket_search_filter']['ticket_status'] = array();
		if(!empty($ticket_status_column_details)) {
			
			$ticket_status_column_details['Type'] = str_replace("enum(", "", $ticket_status_column_details['Type']);
			$ticket_status_column_details['Type'] = str_replace(")", "", $ticket_status_column_details['Type']);
			$ticket_status_column_details['Type'] = str_replace("'", "", $ticket_status_column_details['Type']);
			$return_array['ticket_search_filter']['ticket_status'] =  explode(',', $ticket_status_column_details['Type']);
			// echo "<pre>";print_r($ticket_status_column_details);echo"</pre><hr>";exit;

		}
		$ticket_user_list = $this->common_model->get_all_conditional_data_sales_db('user_id', array('status'=>'Active'), 'ticket_management_details', 'result_array', array(), array(), 'user_id');
		if(!empty($ticket_user_list)) {

			foreach ($ticket_user_list as $single_user_details) {
				$return_array['ticket_search_filter']['ticket_user_list'][] = array('user_id'=>$single_user_details['user_id'],'user_name'=>$user_details[$single_user_details['user_id']]);  
			}
		}
		$ticket_user_department = $this->common_model->get_all_conditional_data_sales_db('user_department_id', array('status'=>'Active'), 'ticket_management_details', 'result_array', array(), array(), 'user_department_id');
		if(!empty($ticket_user_department)) {

			foreach ($ticket_user_department as $single_user_details) {
				$return_array['ticket_search_filter']['ticket_user_department'][] = array('department_id'=>$single_user_details['user_department_id'],'department_name'=>$user_role_details[$single_user_details['user_department_id']]);  
			}
		}

		// End

		// preparing overview count
		unset($where_array['ticket_status IN ']);
		$return_array['ticket_overall'] = array_column($this->ticket_model->get_ticket_listing_overview_count($where_array),'count', 'ticket_status');

		// End

		$return_array['ticket_status_color'] = array('Pending'=>'kt-font-warning', 'Accept'=> 'kt-font-dark', 'Reject'=> 'kt-font-primary', 'Overdue'=> 'kt-font-danger', 'Done'=> 'kt-font-success');

		$ticket_list_array = array();
		foreach ($return_array['ticket_list'] as $ticket_key => $single_ticket_details) {
			
			$ticket_list_array[date('Y-m-d', strtotime($single_ticket_details['add_time']))][$ticket_key] = $single_ticket_details;
		}
		$return_array['ticket_list'] = $ticket_list_array;
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		return $return_array;
	}

	private function create_where_for_ticket_listing($ticket_status_string = "Pending", $search_filter_form = array()) {

		$where_array = $ticket_status_array = $user_id_array = $ticket_id_array = $user_department_array = array();
		$where_array['status'] = "'Active'";
		$where_array['add_time <='] = "'".date('Y-m-d 23:59:59', mktime(0, 0, 0, date('n'), date('d'), date('Y')))."'";
		
		if(!empty($search_filter_form)) {
			
			foreach ($search_filter_form as $search_form_details) {
				if($search_form_details['name'] == 'ticket_status') {
					$ticket_status_array[] = $search_form_details['value'];
				}else if($search_form_details['name'] == 'ticket_id') {
					$ticket_id_array[] = $search_form_details['value'];
				}else if($search_form_details['name'] == 'user_id') {
					$user_id_array[] = $search_form_details['value'];
				}else if($search_form_details['name'] == 'user_department_id') {
					$user_department_array[] = $search_form_details['value'];
				}
			}	
		}

		if(!empty($ticket_id_array)){

			$where_array['ticket_id IN '] = "(";
			$where_array['ticket_id IN '] .= "'".implode("', '", $ticket_id_array)."'";
			$where_array['ticket_id IN '] .= ")";
		}

		if(!empty($user_department_array)){

			$where_array['user_department_id IN '] = "(";
			$where_array['user_department_id IN '] .= "'".implode("', '", $user_department_array)."'";
			$where_array['user_department_id IN '] .= ")";
		}

		$where_array['ticket_status IN '] = "(";
		if(empty($ticket_status_array)) {
			$where_array['ticket_status IN '] .= "'".$ticket_status_string."'";
		}else{

			$where_array['ticket_status IN '] .= "'".implode("', '", $ticket_status_array)."'";
		}
		$where_array['ticket_status IN '] .= ")";

		if(!in_array($this->session->userdata('role'), array( 1, 15, 17))) {

			$where_array['user_id IN '] = "(";
			if(empty($user_id_array)) {

				$where_array['user_id IN '] .= "'".$this->session->userdata('user_id')."'";
				$get_reporting_person_user_id = $this->common_model->get_dynamic_data_sales_db('user_id', array('status'=>'Active', 'reporting_manager'=>$this->session->userdata('user_id')), 'organization_chart');
				if(!empty($get_reporting_person_user_id)){
					foreach ($get_reporting_person_user_id as $single_user_details) {
						$where_array['user_id IN '] .= ",'".$single_user_details['user_id']."'";
					}
				}
			}else{

				$where_array['user_id IN '] .= "'".implode("', '", $user_id_array)."'";
			}
			$where_array['user_id IN '] .= ")";
		}

		// echo "<pre>";print_r($search_filter_form);echo"</pre><hr>";exit;
		// echo "<pre>";print_r($where_array);echo"</pre><hr>";exit;
		return $where_array;
	}

	public function create_default_activity_system_admin() {

		//getting starting day of month and ending day
		$start_day = date('j', mktime(0, 0, 0, date('n'), 1, date('Y')));
		$end_day = date('j', mktime(0, 0, 0, date('n')+1, 0, date('Y')));
		// echo $start_day, "<hr>", $end_day, "<hr>";
		$monday_array = array();
		$tuesday_array = array();
		$wednesday_array = array();
		$thursday_array = array();
		$friday_array = array();
		$saturday_array = array();
		$sunday_array = array();
		for($i = $start_day; $i <= $end_day; $i++) {

			$day_name = date('l', mktime(0, 0, 0, date('n'), $i, date('Y')));
			switch ($day_name) {
				case 'Monday':
				
					$monday_array[] = date('Y-m-d 23:59:59', mktime(0, 0, 0, date('n'), $i, date('Y')));
				break;
				case 'Tuesday':
				
					$tuesday_array[] = date('Y-m-d 23:59:59', mktime(0, 0, 0, date('n'), $i, date('Y')));
				break;
				case 'Wednesday':
				
					$wednesday_array[] = date('Y-m-d 23:59:59', mktime(0, 0, 0, date('n'), $i, date('Y')));
				break;
				case 'Thursday':
				
					$thursday_array[] = date('Y-m-d 23:59:59', mktime(0, 0, 0, date('n'), $i, date('Y')));
				break;
				case 'Friday':
				
					$friday_array[] = date('Y-m-d 23:59:59', mktime(0, 0, 0, date('n'), $i, date('Y')));
				break;
				case 'Saturday':
				
					$saturday_array[] = date('Y-m-d 23:59:59', mktime(0, 0, 0, date('n'), $i, date('Y')));
				break;
				case 'Sunday':
				
					$sunday_array[] = date('Y-m-d 23:59:59', mktime(0, 0, 0, date('n'), $i, date('Y')));
				break;
			}
		}
		
		$static_array_every_month_work = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'type' => 'system_admin', 'ticket_cron_status'=> 'pending'), 'ticket');
		$day_array = array();
		$date = '';
		foreach ($static_array_every_month_work as $ticket_details) {
			
			$this->common_model->update_data_sales_db('ticket', array('ticket_cron_status'=> 'in_progress'), array('id'=>$ticket_details['id']));
			$insert_array = array();
        	$ticket_repeat_day=explode(" ",$ticket_details['ticket_repeat_day']);
	        switch ($ticket_repeat_day[1]) {
				case 'Monday':
                	$day_array = $monday_array;
                break;
                case 'Tuesday':
                	$day_array = $tuesday_array;
                break;
                case 'Wednesday':
                	$day_array = $wednesday_array;
                break;
                case 'Thursday':
                	$day_array = $thursday_array;
                break;
                case 'Friday':
                	$day_array = $friday_array;
                break;
                case 'Saturday':
                	$day_array = $saturday_array;
                break;
                case 'Sunday':
                	$day_array = $sunday_array;
                break;
                case 'Day':
                	$date = $start_day;
                	if($ticket_details['ticket_repeat_day'] == 'Last Day') {

                		$date = $end_day;
                	}
                break;
            }
            if($ticket_repeat_day[0] == 'Every'){

            	if(!empty($day_array)) {

					foreach ($day_array as $single_date) {
						$insert_array[] = array(
												'ticket_id' => $ticket_details['id'],
												'user_id' => 109,
												'user_department_id' => 1,
												'user_comment'=>$ticket_details['name'],
												'ticket_status'=>'Pending',
												'ticket_close_time'=>$single_date,
												'add_time'=>date('Y-m-d 00:00:00', strtotime($single_date)),
												'update_time'=>date('Y-m-d 00:00:00', strtotime($single_date))
											);
					}
            	}
			
			}else if(in_array($ticket_details['ticket_repeat_day'], array('First Day', 'Last Day'))){
					
				if(!empty($date)) {

					$insert_array[] = array(
											'ticket_id' => $ticket_details['id'],
											'user_id' => 109,
											'user_department_id' => 1,
											'user_comment'=>$ticket_details['name'],
											'ticket_status'=>'Pending',
											'ticket_close_time'=> date('Y-m-d 23:59:59', mktime(0, 0, 0, date('n'), $date, date('Y'))),
											'add_time'=>date('Y-m-d 00:00:00', mktime(0, 0, 0, date('n'), $date, date('Y'))),
											'update_time'=>date('Y-m-d 00:00:00', mktime(0, 0, 0, date('n'), $date, date('Y')))
										);
				}	
				
			}else if(in_array($ticket_repeat_day[0], array('First', 'Second', 'Third', 'Fourth', 'Fifth'))){

				$static_day_key_name = array('First'=>0, 'Second'=>1, 'Third'=>2, 'Fourth'=>3, 'Fifth'=>4);
				if(!empty($static_day_key_name[$ticket_repeat_day[0]])) {

					if(!empty($day_array[$static_day_key_name[$ticket_repeat_day[0]]])) {
				
						$insert_array[] = array(
												'ticket_id' => $ticket_details['id'],
												'user_id' => 109,
												'user_department_id' => 1,
												'user_comment'=>$ticket_details['name'],
												'ticket_status'=>'Pending',
												'ticket_close_time'=>$day_array[$static_day_key_name[$ticket_repeat_day[0]]],
												'add_time'=>date('Y-m-d 00:00:00', strtotime($day_array[$static_day_key_name[$ticket_repeat_day[0]]])),
												'update_time'=>date('Y-m-d 00:00:00', strtotime($day_array[$static_day_key_name[$ticket_repeat_day[0]]]))
											);
					}
				}

			}

			if(!empty($insert_array)) {

				echo "<pre>";print_r($insert_array);echo"</pre><hr>";
				$this->common_model->insert_data_sales_db('ticket_management_details', $insert_array, 'batch');
				$this->common_model->update_data_sales_db('ticket', array('ticket_cron_status'=> 'completed'), array('id'=>$ticket_details['id']));
			}
		}
		die('debug');
	}

	public function set_ticket_as_overdue() {

		$ticket_details =  $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'ticket_status'=>'Pending'), 'ticket_management_details');
		foreach ($ticket_details as $single_ticket_details) {
			
			if(date('Y') == date('Y', strtotime($single_ticket_details['ticket_close_time']))) {

				if(date('n') == date('n', strtotime($single_ticket_details['ticket_close_time']))) {

					if(date('d') > date('d', strtotime($single_ticket_details['ticket_close_time']))) {

						$this->common_model->update_data_sales_db('ticket_management_details', array('ticket_status'=>'Overdue'), array('id'=>$single_ticket_details['id']));
					}
				}
			}

		}
		echo "<pre>";print_r($ticket_details);echo"</pre><hr>";exit;
	}

	// sms
	public function sendSms($mobile,$sms_txt, $dlt_id){ 

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.msg91.com/api/sendhttp.php?authkey=345359A1zGdmFe5f930cf3P1&mobiles=91".$mobile."&message=".$sms_txt."&sender=OMTUBE&route=4&DLT_TE_ID=".$dlt_id,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        
        if ($err) {
          // echo "cURL Error #:" . $err;
        } else {
          // echo $response,"<hr>";
        }
    }
    //END
}