<?php if(!empty($ticket_list)){ ?>
    <tr class="table-full-block">
        <th width="5%">
            <span class="">
                <label for="check_all">Sr. No</label>
            </span>
        </th>
        <th class="table_data_left">Project Activities (<span id="projActCnt"><?php echo count($ticket_list)?></span>)</th>
        <th class="table_data_center">System Admin Comment</th>
        <th class="table_data_center">Current Status</th>
        <th class="table_data_center">Action</th>
    </tr>
    <?php $first_tr = true; ?>
    <?php foreach ($ticket_list as $ticket_date => $all_ticket_on_same_date) { ?>
        <tr class="table-full-block open_data" tr_class_name="<?php echo $ticket_date; ?>" action="show">
            <th width="5%">
                <span class="">
                    
                </span>
            </th>
            <th class="table_data_left">
                <span id="projActCnt"><?php echo date('F j', strtotime($ticket_date)); ?></span>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polygon points="0 0 24 0 24 24 0 24" /></g></svg>
            </th>
            <th class="table_data_center"></th>
            <th class="table_data_center"></th>
            <th class="table_data_center"></th>
        </tr>

        <?php foreach ($all_ticket_on_same_date as $ticket_key => $ticket_details) { ?>
            <?php if($first_tr){ ?>
            <tr id="" class="table-full-block task <?php echo $ticket_date; ?>">

            <?php }else{ ?>
            <tr id="" class="table-full-block task <?php echo $ticket_date; ?>" style="display: none;">
            <?php }?>
                <td>
                    <span class="">
                        <label for=""><?php echo $ticket_key+1; ?></label>
                    </span>
                </td>
                <td class="table_data_left">
                    <abbr><?php echo $ticket_details['ticket_name']; ?>
                        <ul class="project_mng_name_list">
                            <li>
                                <abbr>
                                    <a href="javascript:void(0)"><?php echo $ticket_details['user_name']; ?></a>
                                </abbr>
                            </li>
                            <li>
                                <abbr><i>Comment:</i><?php echo $ticket_details['user_comment']; ?></abbr>
                            </li>
                        </ul>
                        <ul class="project_mng_start_date">
                            <li>
                                <span>Create Date: </span><?php echo $ticket_details['start_date']; ?>
                            </li>
                            <li>
                            <?php if($ticket_details['ticket_status'] == 'Pending'){ ?>
                                <span>Due Date: </span><?php echo $ticket_details['end_date']; ?>
                            <?php }else if($ticket_details['ticket_status'] == 'Accept'){  ?>
                                <span>Accepted Date: </span><?php echo $ticket_details['accept_reject_time']; ?>
                            <?php }else if($ticket_details['ticket_status'] == 'Reject'){  ?>
                                <span>Rejected Date: </span><?php echo $ticket_details['accept_reject_time']; ?>
                            <?php }else if($ticket_details['ticket_status'] == 'Overdue'){  ?>
                                <span>Due Date: </span><?php echo $ticket_details['end_date']; ?>
                            <?php }else if($ticket_details['ticket_status'] == 'Done'){  ?>
                                <span>Completed Date: </span><?php echo $ticket_details['done_time']; ?>
                            <?php } ?>
                            </li>
                        </ul>
                    </abbr>
                </td>
                <td>
                        <?php echo $ticket_details['system_admin_comment']; ?>
                </td>
                <td>
                    <em class="pending-activity-color <?php echo $ticket_status_color[$ticket_details['ticket_status']]; ?>"><?php echo $ticket_details['ticket_status']; ?></em>
                </td>
                <td class="activity_action">
                    <ul>
                        <?php if($ticket_details['access_ticket_active']){ ?>
                        <li>
                            <a href="javascript:void(0);" class="action" title="Mark Ticket as Active" id="<?php echo $ticket_details['id']; ?>" action_type = "accept"></a>
                        </li>
                        <?php } ?>
                        <?php if($ticket_details['access_ticket_completed']){ ?>
                        <li>
                            <a href="javascript:void(0);" class="prject_mng_active_task_active action" title="Mark Ticket as Completed" id="<?php echo $ticket_details['id']; ?>" action_type = "completed"></a>
                        </li>
                        <?php } ?>
                        <?php if($ticket_details['access_ticket_reject']){ ?>
                        <li>
                            <a href="javascript:void(0);" class="action" title="Mark Ticket as N/A" id="<?php echo $ticket_details['id']; ?>" action_type = "reject"></a>
                        </li>
                        <?php } ?>
                        <?php if($ticket_details['access_ticket_delete']){ ?>
                        <li>
                            <a href="javascript:void(0);" class="flaticon-delete-1 action" data-toggle="tooltip" data-placement="top" title="Delete Ticket" id="<?php echo $ticket_details['id']; ?>" action_type = "delete"></a>
                        </li>
                        <?php } ?>
                    </ul>
                </td>
            </tr>   
        <?php } $first_tr = false; ?>
    <?php } ?>
<?php }else{ ?> 
    <tr class="table-full-block">
        <th width="5%">
            <span class="">
                <label for="check_all">Sr. No</label>
            </span>
        </th>
        <th class="table_data_left">Project Activities (<span id="projActCnt"><?php echo 0; ?></span>)</th>
        <th class="table_data_center">System Admin Comment</th>
        <th class="table_data_center">Current Status</th>
        <th class="table_data_center">Action</th>
    </tr>
    <tr id="" class="table-full-block task ">
        <td><span class=""></span></td>
        <td class="table_data_left"><abbr>No Record Found</abbr></td>
        <td></td>
        <td></td>
        <td class="activity_action"></td>
    </tr>
<?php } ?>