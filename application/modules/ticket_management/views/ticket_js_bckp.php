jQuery(document).ready(function() {
  KTBootstrapSelect.init();
  KTFormControls.init();
  KTAutosize.init();
  $('.kt-selectpicker').selectpicker();
  var role = "<?php echo $this->session->userdata('role'); ?>"
  if(role == 1 || role == 15 ||role == 17) {

    ajax_call_function({call_type: 'ticket_management_graph', filter_year: ''}, 'ticket_management_graph');
  }
});

$('span.get_employee_data').click(function(){
  ajax_call_function({call_type:'get_employee_data',user_id:$(this).attr('user_id')},'get_employee_data');
});


$('button.create_ticket').click(function(){
        
    set_date_picker();
});

$('a.add_filter_by').click(function(){

    $('div.project_management_comment_box').show();
    set_search_filter_select();
});

$('a.close_filter_by').click(function(){

    $('div.project_management_comment_box').hide();
});

$('a.submit_search_filter').click(function(){

    ajax_call_function({call_type: 'submit_search_filter', form_data: $('form#search_filter_form').serializeArray(), ticket_status: 'Pending'},'submit_search_filter');
});

$('a.reset_search_filter').click(function(){

    ajax_call_function({call_type: 'submit_search_filter', ticket_status: 'Pending'},'submit_search_filter');
    $('div.project_management_comment_box').hide();
});

$('span.ticket_status_change').click(function(){

    ajax_call_function({call_type: 'submit_search_filter', ticket_status: $(this).attr('status_name')},'submit_search_filter');
});

$('tbody#ticket_list').on('click', 'a.action', function(){

    var action_type = $(this).attr('action_type');
    var id = $(this).attr('id');
    $('div#add_comment').modal('show');
    $('button.add_comment').attr('comment_action_type', action_type);
    $('button.add_comment').attr('id', id);
});
$('button.add_comment').click(function(){

    var textarea_text = $('textarea#system_admin_comment').val();
    if(textarea_text == '') {
        return false
    }
    var action_type = $(this).attr('comment_action_type');
    if(action_type == 'accept') {

        ajax_call_function({call_type:'ticket_action',update_data:{'ticket_status':'Accept', 'accept_reject_time': "<?php echo date('Y-m-d H:i:s'); ?>", 'system_admin_comment':textarea_text, 'system_admin_comment_date': "<?php echo date('Y-m-d H:i:s'); ?>"},where_data:{id:$(this).attr('id')}},'ticket_action');
    }else if(action_type == 'completed') {

        ajax_call_function({call_type:'ticket_action',update_data:{'ticket_status':'Done', 'done_time': "<?php echo date('Y-m-d H:i:s'); ?>", 'system_admin_comment':textarea_text, 'system_admin_comment_date': "<?php echo date('Y-m-d H:i:s'); ?>"},where_data:{id:$(this).attr('id')}},'ticket_action');
    }else if(action_type == 'reject') {

        ajax_call_function({call_type:'ticket_action',update_data:{'ticket_status':'Reject', 'accept_reject_time': "<?php echo date('Y-m-d H:i:s'); ?>", 'system_admin_comment':textarea_text, 'system_admin_comment_date': "<?php echo date('Y-m-d H:i:s'); ?>"},where_data:{id:$(this).attr('id')}},'ticket_action');
    }else if(action_type == 'delete') {

        ajax_call_function({call_type:'ticket_action',update_data:{'status':'Inactive'},where_data:{id:$(this).attr('id')}},'ticket_action');
    }
    swal({
        title: "Ticket is updated Successfully !!!",
        icon: "success",
    });
});

$('tbody#template_list').on('click', 'a.edit_tempate_activity', function(){

    ajax_call_function({call_type:'add_edit_ticket_template_activity_form_data',id:$(this).attr('id')},'add_edit_ticket_template_activity_form_data');
});

$('button.add_template_activity').click(function(){

    ajax_call_function({call_type:'add_edit_ticket_template_activity',form_data:$('form#add_ticket_activity').serializeArray()},'add_edit_ticket_template_activity');
    
});

$('button.edit_tempate_activity').click(function(){

    ajax_call_function({call_type:'add_edit_ticket_template_activity_form_data'},'add_edit_ticket_template_activity_form_data');
});
$('tbody#template_list').on('click', 'a.delete_tempate_activity', function(){

    ajax_call_function({call_type:'delete_ticket_template_activity', id:$(this).attr('id')},'delete_ticket_template_activity');
});

$('tbody#ticket_list').on('click', 'tr.open_data', function(){
    var class_name = $(this).attr('tr_class_name');
    var action = $(this).attr('action');
    if(action == "show") {

        $(this).attr('action','hide');
        $('tr.'+class_name).show();
    }else{

        $(this).attr('action','show');
        $('tr.'+class_name).hide();
    }
});

var KTAutosize = function () {
    var daterangepickerInit = function() {
        if ($('#ticket_date').length == 0) {
            return;
        }

        var picker = $('#ticket_date');
        var start = moment().subtract(1, 'days');
        var end = moment().subtract(1, 'days');

        function cb(start, end, label) {
            var title = '';
            var range = '';
            if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D Y');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D Y');
            } else {
                range = start.format('MMM D Y') + ' - ' + end.format('MMM D Y');
            }
            $('#ticket_date_daterangepicker_date').html(range);
            $('#ticket_date_daterangepicker_title').html(title);
            
            ajax_call_function({call_type: 'ticket_management_graph', filter_date: range}, 'ticket_management_graph');
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end, 'Yesterday');
    }
    return {
        // public functions
        init: function() {
            
            daterangepickerInit();
        }
    };
}();

function ajax_call_function(data, callType, url = "<?php echo base_url('ticket_management/ajax_function'); ?>") {
    
    $.ajax({

        type: 'POST',
        data: data,
        url: url,
        dataType: 'JSON',
        success: function(res){
            if(res.status == 'successful') {
                switch (callType) {
                    case 'submit_search_filter':
                        $('div.loader_class').hide();
                        $('tbody#ticket_list').html('').html(res.ticket_body_html);
                    break;

                    case 'add_edit_ticket_template_activity_form_data':
                        $('div.ticket_template_form').html('').html(res.ticket_template_activity_form);
                    break;

                    case 'add_edit_ticket_template_activity':

                         swal({
                                title: "Ticket is added successfully!!!",
                                icon: "success"
                            });
                    break;

                    case 'delete_ticket_template_activity':

                        swal({
                            title: "Ticket is added successfully!!!",
                            icon: "success"
                        });
                        setTimeout(function() { 
                             location.reload();
                         }, 1000);
                    break;
                    case 'ticket_management_graph':

                    ticket_raise_graph(res.ticket_management_category, res.ticket_management_series_data);
                    break;

                    default:
                    break;
                }
            }
        },
        beforeSend: function(response){
            switch (callType) {
                case 'submit_search_filter':
                    $('div.loader_class').show();
                break;
                
                default:
                break;
            }
        }
    });
};

function set_date_picker() {
    
    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }
    
    // enable clear button 
    $('#kt_datepicker_3, #kt_datepicker_3_validate').datepicker({
        rtl: KTUtil.isRTL(),
        todayBtn: "linked",
        clearBtn: true,
        todayHighlight: true,
        templates: arrows,
        startDate: '-0d',
    });
}


// Class definition

var KTFormControls = function () {
    // Private functions
    var validator;

    var demo1 = function () {
       validator= $( "#kt_form_1" ).validate({
            // define validation rules
            rules: {
                
                ticket_id: {
                    required: true
                },
                user_comment: {
                    required: true
                },
                ticket_close_time: {
                    required: true
                }
            },

            errorPlacement: function(error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function(event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                //console.log(form);
                
            }
        });
    }
    var initSubmit = function() {
        var btn =  $("#kt_form_1").find('.submit');

        btn.on('click', function(e) {

            if (!btn.hasClass('kt-spinner')){

                e.preventDefault();

                if (validator.form()) {
                    var data = {
                        call_type:'save_ticket_details',
                        form_data: $('#kt_form_1').serializeArray()
                    };

                    $.ajax({
                        type: 'POST',
                        data: data,
                        url: "<?php echo base_url('ticket_management/ajax_function'); ?>",
                        dataType: 'JSON',
                        success: function(res){

                            if(res.status == 'successful') {

                                KTApp.unprogress(btn);
                                
                                swal({
                                    title: "Ticket is added successfully!",
                                    icon: "success"
                                });
                                setTimeout(function() { 
                                     location.reload();
                                 }, 1000);
                            }
                                            
                        },
                        beforeSend: function(response){
                            KTApp.progress(btn);
                        }
                    });
                }
            }

        });
    }

    return {
        // public functions
        init: function() {
            demo1();
            initSubmit();
        }
    };
}();


function set_search_filter_select() {

    $('#ticket_status').select2({
        placeholder: "Select Ticket Status",
    });
    $('#ticket_type').select2({
        placeholder: "Select Ticket Type",
    });
    $('#ticket_user').select2({
        placeholder: "Select Ticket User",
    });
    $('#ticket_department').select2({
        placeholder: "Select Ticket Department",
    });
}

function ticket_raise_graph(category, highchart_data) {

    Highcharts.chart('ticket_management_graph', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Ticket Status Count'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: category,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Count'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: highchart_data
    });
}