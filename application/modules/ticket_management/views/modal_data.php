<form class="kt-form kt-form--label-right" id="kt_form_1">
    <div class="kt-portlet__body" style="padding: 10px 25px;">
        <div class="form-group form-group-last kt-hide">
            <div class="alert alert-danger" role="alert" id="kt_form_1_msg">
                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                <div class="alert-text">
                    Oh snap! Change a few things up and try submitting again.
                </div>
                <div class="alert-close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="la la-close"></i></span>
                    </button>
                </div>
            </div>
        </div>
        
        <div class="form-group row">
            <!-- <input class="form-control" style="font-size: 1.2rem" type="text" value="<?php //if (!empty($ticket_data['id'])){ echo $ticket_data['id'];}?>" name="id" hidden> --> 
            <input class="form-control" style="font-size: 1.2rem" type="text" value="<?php if (!empty($reporting_manager)){ echo $reporting_manager ; }?>" name="reporting_id" hidden>
            <label class="col-form-label col-lg-2 col-sm-12 ticket_add_form_label" style="text-align: left;">Ticket Type</label>
            <div class="col-lg-4 col-md-9 col-sm-12 form-group-sub ticket_add_form_label">
                <select class="form-control kt-selectpicker" name="ticket_id" data-size="7" data-live-search="true">
                   <option class="role" value="">select Reason</option>
                   <?php foreach($ticket_resons as $key=>$data){?>
                        <option class="role"  value="<?php echo $key;?>">
                            <?php echo $data; ?></option>
                    <?php }?>
                </select>
                <span class="form-text text-muted">Please select an option.</span>
            </div>
        </div>
        
        <div class="form-group row">
            <label class="col-form-label col-lg-2 col-sm-12 ticket_add_form_label" style="text-align: left;">Comment</label>
            <div class="col-lg-9 col-md-9 col-sm-12 ticket_add_form_label">
                <textarea class="form-control" name="user_comment" placeholder="Enter a menu" rows="8"></textarea>
                <span class="form-text text-muted">Please enter a menu within text length range 10 and 100.</span>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-form-label col-lg-2 col-sm-12 ticket_add_form_label" style="text-align: left;">Closing Date</label>
            <div class="col-lg-4 col-md-9 col-sm-12 ticket_add_form_label">
                <div class="input-group date">
                    <input type="text" class="form-control" name="ticket_close_time" readonly value="" id="kt_datepicker_3" />
                </div>
                <span class="form-text text-muted">Enable clear and today helper buttons</span>
            </div>
        </div>
        <div class="kt-separator kt-separator--border-dashed kt-separator--space-xl"></div>
    </div>
    <div class="kt-portlet__foot">
        <div class="kt-form__actions">
            <div class="row">
                <div class="col-lg-9 ml-lg-auto">
                    <button type="submit" class="btn btn-brand submit" >Submit</button>
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</form>


