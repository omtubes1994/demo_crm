<table class="pending_active_issues_table_data project_management_table_data">
	<tbody id="templet_body">
	<?php if(!empty($template_list)){ ?>
	    <tr class="table-full-block">
	        <th width="5%">
	            <span class="">
	                <label for="check_all">Sr. No</label>
	            </span>
	        </th>
	        <th class="table_data_left">Activities (<span id="projActCnt"><?php echo count($template_list)?></span>)</th>
	        <th class="table_data_center">Repeat Day</th>
	        <th class="table_data_center">Action</th>
	    </tr>
	    <?php foreach ($template_list as $template_key => $template_details) { ?>
        <tr id="" class="table-full-block task ">
            <td>
                <span class="">
                    <label for=""><?php echo $template_key+1; ?></label>
                </span>
            </td>
            <td class="table_data_left">
                <abbr>
                	<?php echo $template_details['name']; ?>
                </abbr>
            </td>
            <td>
                <em class="pending-activity-color"><?php echo $template_details['ticket_repeat_day']; ?></em>
            </td>
            <td class="activity_action">
                <ul>
                    <li>
                        <a href="javascript:void(0);" class="flaticon-delete-1 delete_tempate_activity" data-toggle="tooltip" data-placement="top" title="Delete Activities" id="<?php echo $template_details['id']; ?>" action_type = "delete"></a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="flaticon-edit-1 edit_tempate_activity" data-toggle="modal" data-target="#kt_modal_6" title="Edit Ticket" id="<?php echo $template_details['id']; ?>" action_type = "edit"></a>
                    </li>
                </ul>
            </td>
        </tr>
	    <?php } ?>
	<?php }else{ ?> 
	    <tr class="table-full-block">
	        <th width="5%">
	            <span class="">
	                <label for="check_all">Sr. No</label>
	            </span>
	        </th>
	        <th class="table_data_left">Activities (<span id="projActCnt"><?php echo 0; ?></span>)</th>
	        <th class="table_data_center">Repeat Day</th>
	        <th class="table_data_center">Action</th>
	    </tr>
	    <tr id="" class="table-full-block task ">
	        <td><span class=""></span></td>
	        <td class="table_data_left"><abbr>No Record Found</abbr></td>
	        <td></td>
	        <td class="activity_action"></td>
	    </tr>
	<?php } ?>
	</tbody>
</table>