<?php if(!empty($ticket_list)){ ?>
    <tr class="table-full-block">
        <th width="5%">
            <span class="">
                <label for="check_all">Sr. No</label>
            </span>
        </th>
        <th class="table_data_left">Project Activities (<span id="projActCnt"><?php echo count($ticket_list)?></span>)</th>
        <th class="table_data_left">System Admin Comment</th>
        <th class="table_data_center">Current Status</th>
        <th class="table_data_center">Action</th>
    </tr>
    <?php $first_tr = true; ?>
    <?php foreach ($ticket_list as $ticket_date => $all_ticket_on_same_date) { ?>
        <tr class="table-full-block open_data" tr_class_name="<?php echo $ticket_date; ?>" action="show">
            <th width="5%">
                <span class="">
                    
                </span>
            </th>
            <th class="table_data_left">
                <span id="projActCnt"><?php echo date('F j', strtotime($ticket_date)); ?></span>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polygon points="0 0 24 0 24 24 0 24" /></g></svg>
            </th>
            <th class="table_data_center"></th>
            <th class="table_data_center"></th>
            <th class="table_data_center"></th>
        </tr>

        <?php foreach ($all_ticket_on_same_date as $ticket_key => $ticket_details) { ?>
            <?php if($first_tr){ ?>
            <tr id="" class="table-full-block task <?php echo $ticket_date; ?>">

            <?php }else{ ?>
            <tr id="" class="table-full-block task <?php echo $ticket_date; ?>" style="display: none;">
            <?php }?>
                <td>
                    <span class="">
                        <label for=""><?php echo $ticket_key+1; ?></label>
                    </span>
                </td>
                <td class="table_data_left">
                    <abbr>
                        <span class="kt-font-bolder"><?php echo $ticket_details['ticket_name'];?></span>
                        &nbsp;&nbsp;
                        <span>Issue / Bug: [ <?php echo $ticket_details['user_comment'];?> ]</span>
                        <span class="col-xl-4 kt-font-bolder kt-badge <?php echo $ticket_priority_color[$ticket_details['ticket_priority']];?> kt-badge--sm kt-badge--rounded" style="width: auto; padding: 10px;">
                            <?php echo $ticket_details['ticket_priority'];?>
                        </span>
                        <ul class="project_mng_name_list">
                            <li><span class="kt-font-bolder">Creator Name :</span>
                                <abbr>
                                    <a href="javascript:void(0)"><?php echo $ticket_details['user_name']; ?></a>
                                </abbr>
                            </li>
                            <?php if(!empty($ticket_details['reassigned_to_user_name'])){ ?>
                                <span class="kt-font-bolder">Assigned Name :</span>
                                <abbr>
                                    <a href="javascript:void(0)"><?php echo $ticket_details['reassigned_to_user_name'];?></a>
                                </abbr>
                            <?php }?>
                        </ul>
                        <ul class="project_mng_file_list">
                            <?php if($ticket_details['department'] == 'crm'){ ?>
                            <?php if(!empty($ticket_details['issue_document'])){ ?>
                                <li>Issue Pic / File :
                                    <abbr><?php echo $ticket_details['issue_document']; ?></abbr>
                                    <a class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View <?php echo $ticket_details['issue_document']; ?>" href="<?php echo base_url('assets/crm_ticket/'.$ticket_details['issue_document']);?>" style="color: #212529;" target="_blank">
                                        <i class="fa fa-eye kt-font-bolder" style="color: #212529;"></i>
                                    </a>
                                </li>
                            <?php }?>
                            <?php }?>
                        </ul>
                        <ul class="project_mng_start_date">
                            <li>
                                <span>Create Date: </span><?php echo $ticket_details['start_date']; ?>
                            </li>
                            <li>
                            <?php if($ticket_details['ticket_status'] == 'Pending'){ ?>
                                <span>Due Date: </span><?php echo $ticket_details['end_date']; ?>
                            <?php }else if($ticket_details['ticket_status'] == 'Accept'){  ?>
                                <span>Accepted Date: </span><?php echo  date('j, M', strtotime($ticket_details['accept_reject_time'])); ?>
                            <?php }else if($ticket_details['ticket_status'] == 'Reject'){  ?>
                                <span>Rejected Date: </span><?php echo  date('j, M', strtotime($ticket_details['accept_reject_time']));?>
                            <?php }else if($ticket_details['ticket_status'] == 'Overdue'){  ?>
                                <span>Due Date: </span><?php echo $ticket_details['end_date']; ?>
                            <?php }else if($ticket_details['ticket_status'] == 'Done'){  ?>
                                <span>Completed Date: </span><?php echo date('j, M', strtotime($ticket_details['done_time'])); ?>
                            <?php } ?>
                            </li>
                        </ul>
                    </abbr>
                </td>
                <td class="table_data_left">
                    <?php if(!empty($ticket_details['system_admin_comment'])){ ?>
                    <div class="comment_scrollbar">
                        <?php foreach(json_decode($ticket_details['system_admin_comment'], true) as $single_details) {?>
                            <ul class="project_mng_start_date">
                                <li>
                                    <span>Date: </span><?php echo  date('j, M', strtotime($single_details['date'])); ?> &nbsp;&nbsp;
                                    <span>Comment: </span><?php echo $single_details['comment']; ?>
                                </li>
                            </ul><hr style="margin: 2px;">
                        <?php } ?>
                    </div>
                    <?php } ?>
                </td>
                <td>
                    <em class="pending-activity-color <?php echo $ticket_status_color[$ticket_details['ticket_status']]; ?>"><?php echo $ticket_details['ticket_status']; ?></em>
                </td>
                <td class="activity_action">
                    <ul>
                        <?php if($ticket_details['access_ticket_reassign']){ ?>
                            <div class="dropdown">                         
                                <a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md " data-toggle="dropdown">
                                    <i class=" flaticon-user-ok" title="Reassign Ticket">
                                    </i>                            
                                </a>                           
                                <div class="dropdown-menu dropdown-menu-right ">
                                    <?php foreach($ticket_details['reassigned_drop_down'] as $key=>$value){?>
                                     <a class="dropdown-item reassign" value="<?php echo $key ;?>" id="<?php echo $ticket_details['id'];?>"><?php echo $value ;?></a>
                                    <?php }?>                                          
                                </div>                      
                            </div>
                        <?php } ?>
                        <?php if($ticket_details['access_ticket_active']){ ?>
                        <li>
                            <a href="javascript:void(0);" class="action" id="<?php echo $ticket_details['id']; ?>" action_type = "accept">
                                <i class="fa fa-comments" title="Mark Ticket as Active"></i>
                            </a>
                        </li>
                        <?php } ?>
                        <?php if($ticket_details['access_ticket_completed']){ ?>
                        <li>
                            <a href="javascript:void(0);" class="prject_mng_active_task_active action" title="Mark Ticket as Completed" id="<?php echo $ticket_details['id']; ?>" action_type = "completed"></a>
                        </li>
                        <?php } ?>
                        <?php if($ticket_details['access_ticket_reject']){ ?>
                        <li>
                            <a href="javascript:void(0);" class="action" title="Mark Ticket as N/A" id="<?php echo $ticket_details['id']; ?>" action_type = "reject"></a>
                        </li>
                        <?php } ?>
                        <?php if($ticket_details['access_ticket_delete']){ ?>
                        <li>
                            <a href="javascript:void(0);" class="flaticon-delete-1 action" data-toggle="tooltip" data-placement="top" title="Delete Ticket" id="<?php echo $ticket_details['id']; ?>" action_type = "delete"></a>
                        </li>
                        <?php } ?>
                    </ul>
                </td>
            </tr>   
        <?php } $first_tr = false; ?>
    <?php } ?>
<?php }else{ ?> 
    <tr class="table-full-block">
        <th width="5%">
            <span class="">
                <label for="check_all">Sr. No</label>
            </span>
        </th>
        <th class="table_data_left">Project Activities (<span id="projActCnt"><?php echo 0; ?></span>)</th>
        <th class="table_data_center">System Admin Comment</th>
        <th class="table_data_center">Current Status</th>
        <th class="table_data_center">Action</th>
    </tr>
    <tr id="" class="table-full-block task ">
        <td><span class=""></span></td>
        <td class="table_data_left"><abbr>No Record Found</abbr></td>
        <td></td>
        <td></td>
        <td class="activity_action"></td>
    </tr>
<?php } ?>