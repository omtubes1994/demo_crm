<!-- begin:: Content -->
<link href="<?php echo base_url('assets/css/ticket/ticket_css.css');?>" rel='stylesheet' type='text/css'  media='all'>
<link href="<?php echo base_url('assets/css/ticket/ticket_css_2.css');?>" rel='stylesheet' type='text/css'  media='all'>
<style type="text/css">
	.ticket_add_form_label{
	 	color: #898989 !important;
	 	font-size: 17px !important;
	 	font-weight: bold !important;
	 	padding: 0 7px 0 4px !important;
	 	font-family: 'latomedium' !important;
	}
	.project_mng_sort_filter > a::before {
		top: 3px;
	}
	.project_management_table_data > tbody > tr > td > abbr {
	    display: inline-block;
	    width: 100% !important;
	}
</style>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

	<!--Begin::Dashboard 6-->
	<div class="wrapper contennmenu_wrap" style="margin-top:0px !important;">
	    <div class="row">
	    	<!--Begin::Portlet-->
	    	<?php if(in_array($this->session->userdata('role'), array(1, 15, 17))){?>
			<div class=" col-md-12 kt-portlet kt-portlet--height-fluid">
				 <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                           Tiket management
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar select_date_filter_div">
                        <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
							<a href="javascript:void(0);" class="btn kt-subheader__btn-daterange" id="ticket_date" data-toggle="kt-tooltip" title="Select  Date" data-placement="left" style="background: rgb(0 0 0 / 10%); display: inline-flex; height: 32px !important; padding-top: 0; padding-bottom: 0; margin-top: 0.25rem; margin-bottom: 0.25rem; margin-left: 0.25rem;">
								<span class="kt-subheader__btn-daterange-title" id="ticket_date_daterangepicker_title" style="display: inline-block;color: #959cb6;font-weight: 500;">Today</span>&nbsp;
								<span class="kt-subheader__btn-daterange-date" id="ticket_date_daterangepicker_date" style="display: inline-block;color: #5d78ff;margin-right: 0.75rem;font-weight: 500;">Aug 16</span>
								<i class="flaticon2-calendar-1" style="color: #5d78ff; font-size: 1rem !important;"></i>
							</a>
                        </ul>
                    </div>
                </div>
                 <div class="kt-portlet__body">
                    <!--Begin::Timeline 3 -->   
                    <div class="kt-scroll" data-scroll="true" style="height: 400px">
                        <div id="ticket_management_graph"></div>
                    </div>

                    <!--End::Timeline 3 -->
                </div>
	        </div>
	        <?php }?>
	    	<div class="col-md-12" id="activity_top_numbers">
	    		<div class="white_grid wrapper">
				    <div class="common_heading"><i class="custom_heading_icon overview_data_icon"></i> Overview </div>
				    <ul class="keyword_ranking_legend project_mng_legend wrapper">
				        <li>
				            <div class="keyword_ranking_legend">
				                <i class="lead_tracking_legend_icon active_tasks_legend_icon"></i>
				                <span class="ticket_status_change" status_name="Accept" style="cursor:pointer;">
				                    <abbr><?php echo (!empty($ticket_overall['Accept'])) ? $ticket_overall['Accept']:0; ?></abbr>
				                    <em>Accepted Tickets</em>
				                </span>
				            </div>
				        </li>
				        <li>
				            <div class="keyword_ranking_legend">
				                <i class="lead_tracking_legend_icon pending_complete_icon"></i>
				                <span class="ticket_status_change" status_name="Done" style="cursor:pointer;">
				                    <abbr><?php echo (!empty($ticket_overall['Done'])) ? $ticket_overall['Done']:0; ?></abbr>
				                    <em>Completed Tickets</em>
				                </span>
				            </div>
				        </li>
				        <li>
				            <div class="keyword_ranking_legend">
				                <i class="lead_tracking_legend_icon pending_all_issue_icon"></i>
				                <span class="ticket_status_change" status_name="Pending" style="cursor:pointer;">
				                    <abbr><?php echo (!empty($ticket_overall['Pending'])) ? $ticket_overall['Pending']:0; ?></abbr>
				                    <em>Pending Tickets</em>
				                </span>
				            </div>
				        </li>
				        <li>
				            <div class="keyword_ranking_legend">
				                <i class="lead_tracking_legend_icon project_overdue_tasks_icon"></i>
				                <span class="ticket_status_change" status_name="Overdue" style="cursor:pointer;">
				                    <abbr><?php echo (!empty($ticket_overall['Overdue'])) ? $ticket_overall['Overdue']:0; ?></abbr>
				                    <em>Overdue Tickets</em>
				                </span>
				            </div>
				        </li>
				        <li>
				            <div class="keyword_ranking_legend">
				                <i class="lead_tracking_legend_icon non_active_legend_icon"></i>
				                <span class="ticket_status_change" status_name="Reject" style="cursor:pointer;">
				                    <abbr><?php echo (!empty($ticket_overall['Reject'])) ? $ticket_overall['Reject']:0; ?></abbr>
				                    <em>Rejected Tickets</em>
				                </span>
				            </div>
				        </li>
				    </ul>
				</div>
			</div>
	        <div class="col-md-12">
	            <div class="white_grid wrapper">
	            	<div class="wrapper" id="pmgmt_top_filter_bar">
						<div class="col-md-12">
						    <div class="pending_issue_search_bar_wrapper project_mng_search_section ">
						        <div class="new_lead_form_inner_search new_lead_form_inner_search12 project-mng-search-bar">
						        	<abbr>
										<button type="button" class="btn btn-bold btn-label-brand btn-sm create_ticket" data-toggle="modal" data-target="#add_new_ticket">Create Ticket</button>
						        	</abbr>
						    		<div class="pending_create_new_issue project_mng_main_wrapper">
										<div class="new_conversation_sort12 project_mng_sort_filter">
						                    <a href="javascript:void(0);" class="idattrlink-new add-overlay add_filter_by" icon=""><em>Filter By</em></a>
						                    <div class="project_management_comment_box" style="display:none;">
												<div class="project_management_comment_inner">
													<span>Filter By</span>
													<div class="wrapper scroll-remove-margin" style="height:auto;" id="filter_elements_div">
														<!--begin::Form-->
														<form class="kt-form kt-form--label-right" id="search_filter_form">
															<ul class="project_filter_element">
																<li>
																<?php if(!empty($ticket_search_filter['ticket_status'])){ ?>
																	<span>Ticket Status</span>
																	<ul class="project_mng_services_filter">
																		<select class="form-control" id="ticket_status" name="ticket_status" multiple="multiple" style="min-width: 100px!important;">
																			<optgroup label="Select Ticket Status">
																			<?php foreach ($ticket_search_filter['ticket_status'] as $single_ticket_status) { ?>
																				<option value="<?php echo $single_ticket_status; ?>">
																					<?php echo $single_ticket_status; ?>
																				</option>
																			<?php } ?>
																			</optgroup>
																		</select>
																	</ul>
																<?php } ?>
																</li>
																<li>
																<?php if(!empty($ticket_search_filter['ticket_type'])){ ?>
																	<span>Ticket Type</span>
																	<ul class="project_mng_services_filter">
																		<select class="form-control" id="ticket_type" name="ticket_id" multiple="multiple">
																			<optgroup label="Select Ticket Type">
																			<?php foreach ($ticket_search_filter['ticket_type'] as $single_ticket_type_details) { ?>
																				<option value="<?php echo $single_ticket_type_details['id']; ?>">
																					<?php echo $single_ticket_type_details['name']; ?>
																				</option>
																			<?php } ?>
																			</optgroup>
																		</select>
																	</ul>
																<?php } ?>
																</li>
																<?php if(in_array($this->session->userdata('role'), array(15, 17))) { ?>
																	<li>
																	<?php if(!empty($ticket_search_filter['ticket_user_list'])){ ?>
																		<span>User</span>
																		<ul class="project_mng_services_filter">
																			<select class="form-control" id="ticket_user" name="user_id" multiple="multiple">
																				<optgroup label="Select Ticket User">
																				<?php foreach ($ticket_search_filter['ticket_user_list'] as $single_user_details) { ?>
																					<option value="<?php echo $single_user_details['user_id']; ?>">
																						<?php echo $single_user_details['user_name']; ?>
																					</option>
																				<?php } ?>
																				</optgroup>
																			</select>
																		</ul>
																	<?php } ?>
																	</li>
																	<li>
																	<?php if(!empty($ticket_search_filter['ticket_user_department'])){ ?>
																		<span>Department</span>
																		<ul class="project_mng_services_filter">
																			<select class="form-control" id="ticket_department" name="user_department_id" multiple="multiple">
																				<optgroup label="Select Ticket Department">
																				<?php foreach ($ticket_search_filter['ticket_user_department'] as $single_user_details) { ?>
																					<option value="<?php echo $single_user_details['department_id']; ?>">
																						<?php echo $single_user_details['department_name']; ?>
																					</option>
																				<?php } ?>
																				</optgroup>
																			</select>
																		</ul>
																	<?php } ?>
																	</li>
																<?php } ?>
															</ul>
														</form>

														<!--end::Form-->
													</div>
													<div class="project_management_comment_footer">
														<a href="javascript:void(0);" id="" class="lead-save-btn submit_search_filter">Filter</a>
														<a href="javascript:void(0);" id="" class="lead-save-btn reset_search_filter">Reset Filter</a>
							                            <a class="lead-close-btn close_filter_by" style="color: #898989 !important;">Cancel</a>
													</div>
												</div>
											</div>
						                </div>
						    		</div>
						        </div>
						    </div>
						</div>
					</div>
					<div class="wrapper conversation_main_wrapper">
						<div class="keyword_ranking_tab_content content_scroller mCustomScrollbar _mCS_1 mCS_no_scrollbar" style="height:auto;">
							<div id="activity_list_container" class="wrapper">
								<div class="glo_app layer white loader_class" style="display: none;">
									<div class="glo_app apploader large"></div>
								</div>
								<div class="pending_issues_table_data_section project_mng_data_section">
									<table class="pending_active_issues_table_data project_management_table_data">
										<tbody id="ticket_list">
											<?php $this->load->view('ticket_management/ticket_list_body'); ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
	            </div>
	        </div>
	    </div>
	</div>
	<!--End::Dashboard 6-->
</div>

<!-- end:: Content -->


<!--begin::Modal-->
<div class="modal fade" id="add_new_ticket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header" style="padding: 15px 25px;">
				<h5 class="modal-title" id="exampleModalLabel ">New message</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">

				<!--begin::Form-->
				<form class="kt-form kt-form--label-right" id="kt_form_1">
				    <div class="kt-portlet__body" style="padding: 10px 25px;">
				        <div class="form-group form-group-last kt-hide">
				            <div class="alert alert-danger" role="alert" id="kt_form_1_msg">
				                <div class="alert-icon"><i class="flaticon-warning"></i></div>
				                <div class="alert-text">
				                    Oh snap! Change a few things up and try submitting again.
				                </div>
				                <div class="alert-close">
				                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				                        <span aria-hidden="true"><i class="la la-close"></i></span>
				                    </button>
				                </div>
				            </div>
				        </div>
				        <div class="form-group row">
				            <label class="col-form-label col-lg-2 col-sm-12 ticket_add_form_label" style="text-align: left;">Ticket Type</label>
				            <div class="col-lg-4 col-md-9 col-sm-12 form-group-sub ticket_add_form_label">
				            	<?php if(!empty($ticket_search_filter['ticket_type'])){ ?>
									<select class="form-control  kt-selectpicker" name="ticket_id" data-size="4" data-live-search="true">
										<option class="role" value="">Select Ticket Type</option>
										<?php foreach ($ticket_search_filter['ticket_type'] as $single_ticket_type_details) { ?>
											<option class="role"  value="<?php echo $single_ticket_type_details['id']; ?>">
					                            <?php echo $single_ticket_type_details['name']; ?>
				                            </option>
										<?php } ?>
									</select>
								<?php } ?>
				                <span class="form-text text-muted">Please select an option.</span>
				            </div>
				        </div>
				        
				        <div class="form-group row">
				            <label class="col-form-label col-lg-2 col-sm-12 ticket_add_form_label" style="text-align: left;">Comment</label>
				            <div class="col-lg-9 col-md-9 col-sm-12 ticket_add_form_label">
				                <textarea class="form-control" name="user_comment" placeholder="Enter a menu" rows="8"></textarea>
				                <span class="form-text text-muted">Please enter a menu within text length range 10 and 100.</span>
				            </div>
				        </div>
				        <div class="form-group row">
				            <label class="col-form-label col-lg-2 col-sm-12 ticket_add_form_label" style="text-align: left;">Closing Date</label>
				            <div class="col-lg-4 col-md-9 col-sm-12 ticket_add_form_label">
				                <div class="input-group date">
				                    <input type="text" class="form-control" name="ticket_close_time" readonly value="" id="kt_datepicker_3" />
				                </div>
				                <span class="form-text text-muted">Enable clear and today helper buttons</span>
				            </div>
				        </div>
				        <div class="kt-separator kt-separator--border-dashed kt-separator--space-xl"></div>
				    </div>
				    <div class="kt-portlet__foot">
				        <div class="kt-form__actions">
				            <div class="row">
				                <div class="col-lg-9 ml-lg-auto">
				                    <button type="submit" class="btn btn-brand submit">Submit</button>
				                    <button type="reset" class="btn btn-secondary">Cancel</button>
				                </div>
				            </div>
				        </div>
				    </div>
				</form>

				<!--end::Form-->
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->

<!-- start model -->
<div class="modal fade" id="add_comment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding: 15px 25px;">
                <h5 class="modal-title" id="exampleModalLabel ">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">

                <!--begin::Form-->
                <!-- <form class="kt-form kt-form--label-right" id="ticket_comment_form"> -->
                    <div class="kt-portlet__body" style="padding: 10px 25px;">
                        <div class="form-group form-group-last kt-hide">
                            <div class="alert alert-danger" role="alert" id="kt_form_1_msg">
                                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                <div class="alert-text">
                                    Oh snap! Change a few things up and try submitting again.
                                </div>
                                <div class="alert-close">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true"><i class="la la-close"></i></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2 col-sm-12 ticket_add_form_label" style="text-align: left;">Comment</label>
                            <div class="col-lg-9 col-md-9 col-sm-12 ticket_add_form_label">
                                <textarea class="form-control" id="system_admin_comment" placeholder="Enter Comment" rows="8"></textarea>
                            </div>
                        </div>
                    </div>
                <!-- </form> -->

                <!--end::Form-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-brand add_comment" comment_action_type = "" id = "">Submit</button>
                <button type="reset" class="btn btn-secondary">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- end model -->
