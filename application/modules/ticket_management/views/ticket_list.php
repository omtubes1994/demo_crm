<style type="text/css">
    .layer-white{
        display: none;
        position: absolute;
        top: 0em !important;
        left: 0em !important;
        width: 100%;
        height: 100%;
        text-align: center;
        vertical-align: middle;
        background-color: rgba(255, 255, 255, 0.55);
        opacity: 1;
        line-height: 1;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
        -webkit-animation-duration: 0.5s;
        animation-duration: 0.5s;
        -webkit-transition: background-color 0.5s linear;
        transition: background-color 0.5s linear;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        will-change: opacity;
        z-index: 9;
    }
    .div-loader{
        position: absolute;
        top: 50%;
        left: 50%;
        margin: 0px;
        text-align: center;
        z-index: 1000;
        -webkit-transform: translateX(-50%) translateY(-50%);
        transform: translateX(-50%) translateY(-50%);
    }
    .kt-spinner:before {
        width: 50px;
        height: 50px;
        margin-top: -10px;
    }
</style>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <!-- <i class="kt-font-brand flaticon2-line-chart"></i> -->
                </span>
                <h3 class="btn btn-bold btn-sm btn-font-sm btn-label-info"style="height: 3rem ;width: 10rem;font-size: 1.3rem;">
                Ticket List
                </h3>
            </div>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <div class="kt-portlet__head-toolbar">
           
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions ticket_listing">
                        <a href="javascript:;" class="btn btn-bold btn-sm btn-font-sm btn-label-brand kt-font-bold add_search_filter" data-toggle="modal" data-target="#exampleModal">
                            Search Filter
                        </a>
                    </div>
                </div>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions ticket_listing">
                        <a href="javascript:;" class="btn btn-bold btn-sm btn-font-sm btn-label-brand kt-font-bold edit_ticket"  data-toggle="modal" data-target="#kt_modal_4">
                            Add New  
                        </a>
                    </div>
                </div>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 <div class="kt-portlet__head-toolbar">
                    <a href="#" class="btn btn-label-success btn-sm btn-bold dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        Employee
                    </a>
                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right" style="">
                        <ul class="kt-nav">
                            <?php if(!empty($get_employee_id_for_rm)){
                                  foreach($get_employee_id_for_rm as $single_id){ ?>
                            <li class="kt-nav__item">
                                <a href="javascript:;" class="kt-nav__link">
                                    <i class="kt-nav__link-icon flaticon-users-1"></i>
                                    <span class="kt-nav__link-text get_employee_data" user_id="<?php echo $single_id;?>"><?php echo $users[$single_id];?></span>
                                </a>
                            </li>
                            <?php } }?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body ticket_search_filter" style="display: none;">

            <!--begin: Search Form -->
            <form class="kt-form kt-form--fit kt-margin-b-20" id="ticket_search_filter_form">
                <div id="ticket_search_filter_form_div">
                  <?php// $this->load->view('user/ticket_search_filter_form');?>
                </div>
            </form>
        </div>
        <div class="kt-portlet__body">
            <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" style="width:1536px">
                            <thead>
                                <tr>
                                    <th style="text-align: center;width:2%"><span class="btn btn-bold btn-sm btn-font-sm  btn-label-brand" style="height: 3rem ;width: 5rem;font-size: 1.2rem;text-align: center;">SR.NO</span></th>

                                     <th style="text-align: center;width:calc(100%/9)"><span class="btn btn-bold btn-sm btn-font-sm  btn-label-brand" style="height: 3rem ;width: 7rem;font-size: 1.2rem;text-align: center;">Name</span></th>

                                     <th style="text-align: center;width: calc(100%/6)"><span class="btn btn-bold btn-sm btn-font-sm  btn-label-brand" style="height: 3rem ;width: 10rem;font-size: 1.2rem;text-align: center;">Ticket reason</span></th>

                                     <th style="text-align: center;width: calc(100%/6)"><span class="btn btn-bold btn-sm btn-font-sm  btn-label-brand" style="height: 3rem ;width: 10rem;font-size: 1.2rem;text-align: center;">User comment</span></th>

                                     <th style="text-align: center;width: calc(100%/8)"><span class="btn btn-bold btn-sm btn-font-sm  btn-label-brand" style="height: 3rem ;width: 15rem;font-size: 1.2rem;text-align: center;">rm status</span></th>
                                     <th style="text-align: center;width: calc(100%/8)"><span class="btn btn-bold btn-sm btn-font-sm  btn-label-brand" style="height: 3rem ;width: 15rem;font-size: 1.2rem;text-align: center;">rm remark</span></th>
                                     <th style="text-align: center;width: calc(100%/8)"><span class="btn btn-bold btn-sm btn-font-sm  btn-label-brand" style="height: 3rem ;width: 15rem;font-size: 1.2rem;text-align: center;">work on issue  stauts</span></th>
                                     <th style="text-align: center;width: 10%"><span class="btn btn-bold btn-sm btn-font-sm  btn-label-brand" style="height: 3rem ;width: 10rem;font-size: 1.2rem;text-align: center;">Action</span></th>
                                </tr>
                            </thead>
                            <tbody id="ticket_listing">
                                  <?php $this->load->view('ticket_management/ticket_list_body');?>
                            </tbody>
                        </table>
                        <div id="ticket_loader" class="layer-white">
                            <div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
                        </div>
                    </div>
                </div>
                <div class="row" id="ticket_paggination">
                    <?php $this->load->view('common/common_paggination')?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade show" id="kt_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display:none; padding-right: 21px;" aria-modal="true">
    <div class="modal-dialog modal-lg border border-info" role="document">
        <div class="kt-scroll ps ps--active-y " data-scroll="true" style="height:auto ; overflow: hidden;">
            <div class="modal-content">
                <div class="modal-header model_form">
                    <h5 class="modal-title" id="exampleModalLabel">Assing To user</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="kt-scrol form_body" data-scroll="true" data-height="180" style="padding-left: 30px;padding-right: 30px;padding-top: 30px;">
                     
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary submit_ticket_form">SUBMIT</button>
                </div>
            </div>
        </div>
    </div>
</div>
