<!-- begin:: Content -->
<link href="<?php echo base_url('assets/css/ticket/ticket_css.css');?>" rel='stylesheet' type='text/css'  media='all'>
<link href="<?php echo base_url('assets/css/ticket/ticket_css_2.css');?>" rel='stylesheet' type='text/css'  media='all'>
<style type="text/css">
	.ticket_add_form_label{
	 	color: #898989 !important;
	 	font-size: 17px !important;
	 	font-weight: bold !important;
	 	padding: 0 7px 0 4px !important;
	 	font-family: 'latomedium' !important;
	}
	.project_mng_sort_filter > a::before {
		top: 3px;
	}
</style>


<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">


	<!--Begin::Dashboard 6-->
	<div class="wrapper contennmenu_wrap" style="margin-top:0px !important;">
		<div class="col-md-12" id="activity_top_numbers">
    		<div class="white_grid wrapper">
			    <div class="common_heading"><i class="custom_heading_icon overview_data_icon"></i> Overview </div>
			    <ul  class="keyword_ranking_legend project_mng_legend wrapper" >
			        <li style="width: calc(100%/3) !important;">
			            <div class="keyword_ranking_legend col-md-6">
			                <i class="lead_tracking_legend_icon active_tasks_legend_icon"></i>
			                <span class="ticket_department_change" department_name="system_it" style="cursor:pointer;">
			                    <abbr></abbr>
			                    <em>System IT</em>
			                </span>
			            </div>
			        </li>
			        <li style="width: calc(100%/3) !important;">
			            <div class="keyword_ranking_legend">
			                <i class="lead_tracking_legend_icon pending_complete_icon"></i>
			                <span class="ticket_department_change" department_name="system_crm" style="cursor:pointer;">
			                    <abbr></abbr>
			                    <em>System CRM</em>
			                </span>
			            </div>
			        </li>
			        <li style="width: calc(100%/3) !important;">
			            <div class="keyword_ranking_legend">
			                <i class="lead_tracking_legend_icon pending_all_issue_icon"></i>
			                <span class="ticket_department_change" department_name="system_admin" style="cursor:pointer;">
			                    <abbr></abbr>
			                    <em>System Admin</em>
			                </span>
			            </div>
			        </li>
			    </ul>
			</div>
		</div>
	    <div class="row">
	        <div class="col-md-12">
	            <div class="white_grid wrapper">
	            	<div class="wrapper" id="pmgmt_top_filter_bar">
						<div class="col-md-12">
						    <div class="pending_issue_search_bar_wrapper project_mng_search_section ">
						        <div class="new_lead_form_inner_search new_lead_form_inner_search12 project-mng-search-bar">
						        	<abbr>
										<button type="button" class="btn btn-bold btn-label-brand btn-sm edit_tempate_activity" data-toggle="modal" data-target="#kt_modal_6">Create Ticket</button>
						        	</abbr>
						        </div>
						    </div>
						</div>
					</div>
					<div class="wrapper conversation_main_wrapper">
						<div class="keyword_ranking_tab_content content_scroller mCustomScrollbar _mCS_1 mCS_no_scrollbar" style="height:auto;">
							<div id="activity_list_container" class="wrapper">
								<div class="glo_app layer white loader_class" style="display: none;">
									<div class="glo_app apploader large"></div>
								</div>
								<div class="pending_issues_table_data_section project_mng_data_section "  id="template_list">
									<?php $this->load->view('ticket_management/template_body');?>
								</div>
							</div>
						</div>
					</div>
	            </div>
	        </div>
	    </div>
	</div>
	<!--End::Dashboard 6-->
</div>

<!-- end:: Content -->

<!-- Modal -->
	<div class="modal fade" id="kt_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<!--begin::Form-->
					<form class="kt-form kt-form--label-right" id="add_ticket_activity">
					    <div class="kt-portlet__body ticket_template_form" style="padding: 10px 25px;">
					    </div>
					    <div class="kt-portlet__foot">
					        <div class="kt-form__actions">
					            <div class="row">
					                <div class="col-lg-9 ml-lg-auto">
					                    <button type="button" class="btn btn-brand add_template_activity">Submit</button>
					                    <button type="reset" class="btn btn-secondary">Cancel</button>
					                </div>
					            </div>
					        </div>
					    </div>
					</form>

					<!--end::Form-->
				</div>
			</div>
		</div>
	</div>

	<!--begin::Modal-->