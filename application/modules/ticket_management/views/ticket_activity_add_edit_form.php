<?php if(!empty($activity_information)) { ?>
	<div class="form-group row">
		<label class="col-form-label col-lg-2 col-sm-12 ticket_add_form_label" style="text-align: left;">Activity Name</label>
		<div class="col-lg-9 col-md-9 col-sm-12 ticket_add_form_label">
		    <input type="text" class="form-control" name="id" value="<?php echo $activity_information['id'];?>" hidden>
		    <input type="text" class="form-control" name="name" placeholder="Enter activity name" value="<?php echo $activity_information['name'];?>">
		    <span class="form-text text-muted">Please select an option.</span>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-form-label col-lg-2 col-sm-12 ticket_add_form_label" style="text-align: left;">Ticket Repeat Name</label>
		<div class="col-lg-4 col-md-9 col-sm-12 form-group-sub ticket_add_form_label">
			<?php if(!empty($ticket_template_search_filter['ticket_repeat_day'])){ ?>
				<select class="form-control  kt-selectpicker" name="ticket_repeat_day" data-size="4" data-live-search="true">
					<option class="role" value="">Select Ticket Repeat Name</option>
					<?php foreach ($ticket_template_search_filter['ticket_repeat_day'] as $single_ticket_template_name) { ?>
						<option class="role"  value="<?php echo $single_ticket_template_name; ?>"
							<?php echo ($activity_information['ticket_repeat_day'] == $single_ticket_template_name) ? 'selected':'';?>
							>
		                    <?php echo $single_ticket_template_name; ?>
		                </option>
					<?php } ?>
				</select>
			<?php } ?>
		    <span class="form-text text-muted">Please select an option.</span>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-form-label col-lg-3 col-sm-12 ticket_add_form_label" style="text-align: left;">Ticket Department Name</label>
		<div class="col-lg-4 col-md-9 col-sm-12 form-group-sub ticket_add_form_label">
			<?php if(!empty($department)){ ?>
				<select class="form-control  kt-selectpicker" name="type" data-size="4" data-live-search="true">
					<option class="role" value="">Select Ticket Department Name</option>
					<?php foreach ($department as $single_ticket_type) { ?>
						<option class="role"  value="<?php echo $single_ticket_type; ?>"
							<?php echo ($activity_information['type'] == $single_ticket_type) ? 'selected':'';?>
							>
		                    <?php echo $single_ticket_type; ?>
		                </option>
					<?php } ?>
				</select>
			<?php } ?>
		    <span class="form-text text-muted">Please select an option.</span>
		</div>
	</div>
<?php }else {?>
	<div class="form-group row">
		<label class="col-form-label col-lg-2 col-sm-12 ticket_add_form_label" style="text-align: left;">Activity Name</label>
		<div class="col-lg-9 col-md-9 col-sm-12 ticket_add_form_label">
	    <input type="text" class="form-control" name="name" placeholder="Enter activity name">
		    <span class="form-text text-muted">Please select an option.</span>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-form-label col-lg-2 col-sm-12 ticket_add_form_label" style="text-align: left;">Ticket Repeat Name</label>
		<div class="col-lg-4 col-md-9 col-sm-12 form-group-sub ticket_add_form_label">
			<?php if(!empty($ticket_template_search_filter['ticket_repeat_day'])){ ?>
				<select class="form-control  kt-selectpicker" name="ticket_repeat_day" data-size="4" data-live-search="true">
					<option class="role" value="">Select Ticket Repeat Name</option>
					<?php foreach ($ticket_template_search_filter['ticket_repeat_day'] as $single_ticket_template_name) { ?>
						<option class="role"  value="<?php echo $single_ticket_template_name; ?>">
		                    <?php echo $single_ticket_template_name; ?>
		                </option>
					<?php } ?>
				</select>
			<?php } ?>
		    <span class="form-text text-muted">Please select an option.</span>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-form-label col-lg-3 col-sm-12 ticket_add_form_label" style="text-align: left;">Ticket Department Name</label>
		<div class="col-lg-4 col-md-9 col-sm-12 form-group-sub ticket_add_form_label">
			<?php if(!empty($department)){ ?>
				<select class="form-control  kt-selectpicker" name="type" data-size="4" data-live-search="true">
					<option class="role" value="">Select Ticket Department Name</option>
					<?php foreach ($department as $single_ticket_type) { ?>
						<option class="role"  value="<?php echo $single_ticket_type; ?>">
		                    <?php echo $single_ticket_type;?>
		                </option>
					<?php } ?>
				</select>
			<?php } ?>
		    <span class="form-text text-muted">Please select an option.</span>
		</div>
	</div>
<?php } ?>
<div class="kt-separator kt-separator--border-dashed kt-separator--space-xl"></div>