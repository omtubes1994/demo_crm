<?php 
Class Ticket_model extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	public function get_ticket_listing_data($where_array, $order_by_array, $limit, $offset) {

		$return_array = array();
		$this->db->select('SQL_CALC_FOUND_ROWS *', FALSE);
		if(!empty($where_array)) {
			$this->db->where($where_array, NULL, FALSE);
		}
		if(!empty($order_by_array)) {
			$this->db->order_by($order_by_array['column_name'], $order_by_array['column_value']);
		}
		$return_array['ticket_list'] = $this->db->get('ticket_management_details', $limit, $offset)->result_array();
		// echo $this->db->last_query(),"<hr>";
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		$return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows;")->row_array();
		return $return_array;
	}

	public function get_column_details($table_name, $column_name) {

		return $this->db->query("SHOW COLUMNS FROM ".$table_name." LIKE '".$column_name."';")->row_array();
	}

	public function get_ticket_listing_overview_count($where_array) {

		$this->db->select('count(*) count, ticket_status');
		$this->db->where($where_array, NULL, FALSE);
		$this->db->group_by('ticket_status');
		return $this->db->get('ticket_management_details')->result_array();
	}

	public function get_ticket_listing_graph_data($where_array) {

		$this->db->select('*');
		$this->db->where($where_array, NULL, FALSE);
		$this->db->order_by('add_time', 'asc');
		return $this->db->get('ticket_management_details')->result_array();
	}
}