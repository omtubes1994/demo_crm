<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quality extends MX_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('user_id')){
			redirect('login', 'refresh');
			exit;
		}else{
			$access_count = 0;
			if(!empty($this->session->userdata('main_module_access')) && in_array(10, array_column($this->session->userdata('main_module_access'), 'module_id'))) {

				foreach ($this->session->userdata('sub_module_access') as $key => $value) {
					
					if($value['module_id'] == 10 && $value['status'] == 'Active') {
						$access_count++;
					}
				}
			}
			if($access_count == 0){
				redirect($this->session->userdata('home/dashboard'));
				exit;
			}
		}
		$this->load->model('quality_model');
		$this->load->model('common/common_model');
		error_reporting(0);
	}

	function index(){
		redirect('quality/mtc_list');
	}

	function add_mtc($mtc_id = 0){
		if(!empty($this->input->post())){
			$insert_arr = array(
				'mtc_type' => $this->input->post('mtc_type'),
				'mtc_for' => $this->input->post('mtc_for'),
				'mtc_for_id' => $this->input->post('mtc_for_hidden'),
				'mtc_company' => $this->input->post('mtc_company'),
				'mtc_company_id' => $this->input->post('mtc_company_id'),
				'assigned_to' => $this->input->post('assigned_to'),
			);
			if($this->input->post('mtc_mst_id') > 0){
				$insert_arr['modified_on'] = date('Y-m-d H:i:s');
				$insert_arr['modified_by'] = $this->session->userdata('user_id');
				$insert_arr['made_flag'] = $this->input->post('made_flag');
				if($this->input->post('made_flag') == 'Y'){
					$insert_arr['made_flag_on'] = date('Y-m-d H:i:s');
				}
				
				$this->quality_model->updateData('mtc_mst', $insert_arr, array('mtc_mst_id' => $this->input->post('mtc_mst_id')));
				/*if(!empty($_FILES)){
					$config['upload_path']          = './assets/mtc-document/';
		            $config['allowed_types']        = 'pdf';//'jpeg|gif|jpg|png';
		            $config['max_size']             = 5242880;
		            //$config['max_width']            = 2000;
		            //$config['max_height']           = 2000;
		            //$config['min_width']            = 1000;
		            //$config['min_height']           = 1000;
		            $config['file_name']            = 'mtc-'.$this->input->post('mtc_mst_id');
		            $config['overwrite']            = TRUE;

		            $this->load->library('upload', $config);

		            if ( ! $this->upload->do_upload('mtc_file'))
		            {
						$error = array('error' => $this->upload->display_errors());
						$data = array('status' => 'failed', 'msg' => $error['error']);
		            }
		            else
		            {
		            	$file_dtls = $this->upload->data();
			        	$insert_arr['mtc_file_name'] = $file_dtls['file_name'];
		            }

		            $this->quality_model->updateData('mtc_mst', $insert_arr, array('mtc_mst_id' => $this->input->post('mtc_mst_id')));
		            
				}*/
				redirect('quality/mtc_list');
			}else{
				$insert_arr['created_by'] = $this->session->userdata('user_id');
				$insert_arr['entered_on'] = date('Y-m-d H:i:s');
				$this->quality_model->insertData('mtc_mst', $insert_arr);
				if($this->input->post('is_ajax') == 1){
					echo 'submitted';
				}else{
					redirect('quality/mtc_list');
				}
			}
		}else{
			if($mtc_id > 0){
				$data['mtc_details'] = $this->quality_model->getMTCDetails($mtc_id);
				$data['mtc_files'] = $this->quality_model->getData('mtc_files', 'mtc_mst_id = '.$mtc_id);
			}
			$data['quality_users'] = $this->quality_model->getData('users', 'role in (9, 10)');
			$this->load->view('header', array('title' => 'Add Mtc'));
			$this->load->view('sidebar');
			$this->load->view('add_mtc', $data);
			$this->load->view('footer');
		}
	}

	function getSuggestions(){
		$search = $this->input->post('search');
		$mtc_for = $this->input->post('mtc_for');
		$result = $this->quality_model->getSuggestions($search, $mtc_for);
		echo json_encode($result);
	}	

	function mtc_list(){
		$data['quality_users'] = $this->quality_model->getData('users', 'role in (9, 10)');
		$data['sales_users'] = $this->quality_model->getData('users', 'role in (5)');
		$this->load->view('header', array('title' => 'MTC List'));
		$this->load->view('sidebar', array('title' => 'MTC List'));
		$this->load->view('mtc_list', $data);
		$this->load->view('footer');
	}

	function mtc_list_data(){

		$search = array();
		// echo "<pre>";print_r($search);echo"</pre><hr>";exit;
		/*foreach ($this->input->post('columns') as $key => $value) {
			$search_key = '';
			if($key == 1){
				$search_key = 'pq.company_name';
			}else if($key == 2){
				$search_key = 'pq.country';
			}else if($key == 3){
				$search_key = 'pq.region';
			}else if($key == 4){
				$search_key = 'pq.client_category';
			}else if($key == 5){
				$search_key = 'pq.client_stage';
			}else if($key == 6){
				$search_key = 'pq.registration_method';
			}else if($key == 7){
				$search_key = 'ld.member_name';
			}else if($key == 0){
				$search_key = 'assigned_to';
			}

			$search[$search_key] = $this->input->post('columns')[$key]['search']['value'];
		}*/
		//print_r($search);
		$order_by = $this->input->post('columns')[$this->input->post('order')[0]['column']]['data'];
		if($order_by == 'record_id'){
			$order_by = 'mtc_mst_id';
		}
		$dir = $this->input->post('order')[0]['dir'];
		$records = $this->quality_model->getMTCList($this->input->post('start'), $this->input->post('length'), $search, $order_by, $dir);
		$data['iTotalDisplayRecords'] = $data['iTotalRecords'] = $this->quality_model->getMTCListCount($search);
		// echo "<pre>";print_r($records);echo"</pre><hr>";exit;
		$data['aaData'] = $records;

		echo json_encode($data);
	}

	function getSampleMTCStatus(){
		$mtc = $this->quality_model->getSampleMTCStatus($this->input->post('quote_id'));
		if($mtc['made_flag'] == 'Y'){
			$mtc_files = $this->quality_model->getData('mtc_files', 'mtc_mst_id = '.$mtc['mtc_mst_id']);
		}
		if(!empty($mtc)){
			echo json_encode(array('mtc' => $mtc, 'files' => $mtc_files));
		}else{
			echo json_encode(array());
		}
	}

	function updateMTC(){
		if($this->input->post('made_flag')){
			$update_arr['made_flag'] = $this->input->post('made_flag');
			$update_arr['made_flag_on'] = date('Y-m-d H:i:s');
		}

		if($this->input->post('checked_by_quality_admin')){
			$update_arr['checked_by_quality_admin'] = $this->input->post('checked_by_quality_admin');
			$update_arr['checked_by_quality_admin_on'] = date('Y-m-d H:i:s');
			$update_arr['qa_comment'] = $this->input->post('qa_comment');
		}

		if($this->input->post('checked_by_super_admin')){
			$update_arr['checked_by_super_admin'] = $this->input->post('checked_by_super_admin');
			$update_arr['checked_by_super_admin_on'] = date('Y-m-d H:i:s');
			$update_arr['sa_comment'] = $this->input->post('sa_comment');
		}
		$update_arr['modified_on'] = date('Y-m-d H:i:s');
		$update_arr['modified_by'] = $this->session->userdata('user_id');

		$this->quality_model->updateData('mtc_mst', $update_arr, array('mtc_mst_id' => $this->input->post('mtc_mst_id')));
	}

	function uploadDocument(){
		if($this->input->post('mtc_mst_id') > 0){
			$mtc_mst_id = $this->input->post('mtc_mst_id');
			//$res = $this->quality_model->getData('mtc_files', 'mtc_file_id = '.$mtc_mst_id);
			$config['upload_path']          = './assets/mtc-document/';
            $config['allowed_types']        = 'pdf';//'jpeg|gif|jpg|png';
            $config['max_size']             = 10485760;
            //$config['max_width']            = 2000;
            //$config['max_height']           = 2000;
            //$config['min_width']            = 1000;
            //$config['min_height']           = 1000;
            $config['file_name']            = time().'-MTC-DOCUMENT-'.$mtc_mst_id;
            $config['overwrite']            = TRUE;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('file'))
            {
				$error = array('error' => $this->upload->display_errors());
				$data = array('status' => 'failed', 'msg' => $error['error']);
            }
            else
            {
            	$file_dtls = $this->upload->data();
	            $data = array('status' => 'success', 'msg' => 'Image uploaded successfully!', 'file_name' => $file_dtls['file_name']);
	            $this->quality_model->insertData('mtc_files', array('file_name' => $file_dtls['file_name'], 'mtc_mst_id' => $this->input->post('mtc_mst_id')));
            }
		}else{
			$data = array('status' => 'failed', 'msg' => '');
		}
		echo json_encode($data);
	}

	function deleteDocument(){
		/*$mtc_for_id = $this->input->post('mtc_for_id');
		$img_dtls = $this->mtc-document->getData('mtc_files', "pq_client_id = ".$mtc_for_id);
		$this->quality_model->updateData('pq_client', array('upload_document' => NULL), array("pq_client_id = ".$mtc_for_id));
		unlink('./assets/property-images/main/'.$img_dtls[0]['upload_document']);*/
	}

	function add_marking($marking_id=0){
		if(!empty($this->input->post())){
			$master_arr = array(
				'marking_type' => $this->input->post('marking_type'),
				'marking_for' => $this->input->post('marking_for'),
				'marking_for_id' => $this->input->post('marking_for_hidden'),
				'made_by' => $this->input->post('made_by'),
				'assigned_to' => $this->input->post('assigned_to'),
				'reference' => $this->input->post('reference'),
				'comments' => $this->input->post('comments'),
				'modified_on' => date('Y-m-d H:i:s'),
				'modified_by' => $this->session->userdata('user_id')
			);
			if($this->input->post('marking_mst_id') > 0){
				$marking_id = $this->input->post('marking_mst_id');
				$this->quality_model->deleteData('marking_dtl', array('marking_mst_id' => $marking_id));
			}else{
				$master_arr['entered_on'] = date('Y-m-d H:i:s');
				$master_arr['entered_by'] = $this->session->userdata('user_id');
				$master_arr['made_status'] = 'Yes';
				$marking_id = $this->quality_model->insertData('marking_mst', $master_arr);
			}
			foreach ($this->input->post('quote_dtl_id') as $key => $value) {
				$detail_arr = array(
					'marking_mst_id' => $marking_id,
					'quotation_dtl_id' => $value,
					'product_id' => $this->input->post('product')[$key],
					'material_id' => $this->input->post('material')[$key],
					'heat_no' => $this->input->post('heat_number')[$key],
					'specification' => $this->input->post('specification')[$key],
					'marking' => $this->input->post('marking')[$key],
				);
				$this->quality_model->insertData('marking_dtl', $detail_arr);

				$update_arr = array(
					'product_id' => $this->input->post('product')[$key],
					'material_id' => $this->input->post('material')[$key],
				);
				$this->quality_model->updateData('quotation_dtl', $update_arr, array('quotation_dtl_id' => $value));
			}
			redirect('quality/marking_list/'.$this->input->post('marking_type'));
		}else{
			if($marking_id > 0){
				$data['marking_id'] = $marking_id;
				$data['marking_details'] = $this->quality_model->getMarkingDetails($marking_id);
			}
			$data['quality_users'] = $this->quality_model->getData('users', 'role in (9, 10)');
			$data['sales_users'] = $this->quality_model->getData('users', 'role in (5)');
			$data['product'] = $this->quality_model->getData('lookup', 'lookup_group = 259');
			$data['material'] = $this->quality_model->getData('lookup', 'lookup_group = 272');
			// $data['product'] = $this->quality_model->getData('product_mst', 'status="Active"');
			// $data['material'] = $this->quality_model->getData('material_mst', 'status="Active"');
			$this->load->view('header', array('title' => 'Add / Edit Marking'));
			$this->load->view('sidebar', array('title' => 'Add / Edit Marking'));
			$this->load->view('add_marking', $data);
			$this->load->view('footer');
		}
	}

	function getLineItems(){
		$details = $this->quality_model->getLineItems($this->input->post('quotation_mst_id'));
		echo json_encode($details);
	}

	function generateHeat(){
		$product_id = $this->input->post('product_id');
		$heat_arr = $this->quality_model->getHeatNumber($product_id);
		if(!empty($heat_arr)){
			$prefix = $heat_arr['prefix'];
			switch ($product_id) {
				case 289:
					$heat_no = $heat_arr['logic_value'];
					$heat_number = $prefix.$heat_no;
					$new_heat_no = $heat_no + 1;
					if($new_heat_no == 100){
						$new_heat_no = 1;
						$prefix++;
					}
					$new_prefix = $prefix;
					$this->quality_model->updateData('number_logic', array('logic_value' => $new_heat_no, 'prefix' => $new_prefix), array('logic_for' => $product_id));
					break;

				case 264:
				case 263:
				case 295: 
				case 265: 
				case 267:
				case 266:
				case 300:
				case 262:
				case 268:
				case 302:
					$heat_no = $heat_arr['logic_value'];
					$new_heat_no = $heat_no + 1;
					switch (strlen($heat_no)) {
						case 1:
							$heat_no = '000'.$heat_no;
							break;

						case 2:
							$heat_no = '00'.$heat_no;
							break;

						case 3:
							$heat_no = '0'.$heat_no;
							break;
						
						default:
							$heat_no = $heat_no;
							break;
					}
					$heat_number = $prefix.$heat_no;
					$this->quality_model->updateData('number_logic', array('logic_value' => $new_heat_no), array('logic_for' => $product_id));
					break;

				case 260:
					$heat_no = $heat_arr['logic_value'];
					$heat_number = $heat_no.$prefix;
					$new_heat_no = $heat_no + 1;
					$this->quality_model->updateData('number_logic', array('logic_value' => $new_heat_no), array('logic_for' => $product_id));
					break;
			}
			echo $heat_number;
		}
	}

	function marking_list($marking_type){
		$data['marking_type'] = $marking_type;
		$data['quality_users'] = $this->quality_model->getData('users', 'role in (9, 10)');
		$data['sales_users'] = $this->quality_model->getData('users', 'role in (5)');
		$this->load->view('header', array('title' => 'Marking List'));
		$this->load->view('sidebar', array('title' => 'Marking List'));
		$this->load->view($marking_type.'_marking_list', $data);
		$this->load->view('footer');
	}

	function marking_list_data($type){
		$search = array('marking_type' => $type);
		/*foreach ($this->input->post('columns') as $key => $value) {
			$search_key = '';
			if($key == 1){
				$search_key = 'pq.company_name';
			}else if($key == 2){
				$search_key = 'pq.country';
			}else if($key == 3){
				$search_key = 'pq.region';
			}else if($key == 4){
				$search_key = 'pq.client_category';
			}else if($key == 5){
				$search_key = 'pq.client_stage';
			}else if($key == 6){
				$search_key = 'pq.registration_method';
			}else if($key == 7){
				$search_key = 'ld.member_name';
			}else if($key == 0){
				$search_key = 'assigned_to';
			}

			$search[$search_key] = $this->input->post('columns')[$key]['search']['value'];
		}*/
		//print_r($search);
		$order_by = $this->input->post('columns')[$this->input->post('order')[0]['column']]['data'];
		if($order_by == 'record_id'){
			$order_by = 'marking_mst_id';
		}
		$dir = $this->input->post('order')[0]['dir'];
		$records = $this->quality_model->getMarkingList($this->input->post('start'), $this->input->post('length'), $search, $order_by, $dir);
		$data['iTotalDisplayRecords'] = $data['iTotalRecords'] = $this->quality_model->getMarkingListCount($search);
		$data['aaData'] = $records;

		echo json_encode($data);
	}

	function updateMarking(){
		$marking_detail = $this->quality_model->getData('marking_mst', 'marking_mst_id = '.$this->input->post('marking_mst_id'));
		if($this->input->post('made_status')){
			$update_arr['made_status'] = $this->input->post('made_status');
		}

		if($this->input->post('quality_admin_status')){
			$update_arr['quality_admin_status'] = $this->input->post('quality_admin_status');
			$update_arr['qa_comment'] = $this->input->post('qa_comment');
			if($this->input->post('quality_admin_status_on') == '' || $update_arr['quality_admin_status'] != $marking_detail['quality_admin_status']){
				$update_arr['quality_admin_status_on'] = date('Y-m-d H:i:s');
			}
		}

		if($this->input->post('super_admin_status')){
			$update_arr['super_admin_status'] = $this->input->post('super_admin_status');
			$update_arr['super_admin_status_on'] = date('Y-m-d H:i:s');
			$update_arr['sa_comment'] = $this->input->post('sa_comment');
			if($this->input->post('super_admin_status_on') == '' || $update_arr['super_admin_status'] != $marking_detail['super_admin_status']){
				$update_arr['super_admin_status_on'] = date('Y-m-d H:i:s');
			}
		}
		$update_arr['modified_on'] = date('Y-m-d H:i:s');
		$update_arr['modified_by'] = $this->session->userdata('user_id');

		$this->quality_model->updateData('marking_mst', $update_arr, array('marking_mst_id' => $this->input->post('marking_mst_id')));
		echo $this->db->last_query();
	}

	function viewMarking($marking_id){
		$marking_details = $this->quality_model->getMarkingDetails($marking_id);

		$this->load->library('Pdf');
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		

		// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, NULL, '', array(0,0,0), array(255,255,255));
		$pdf->SetFooterData(array(0,0,0), array(255,255,255));

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(10, PDF_MARGIN_TOP-10, 10);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER-5);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM-15);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		    require_once(dirname(__FILE__).'/lang/eng.php');
		    $pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('helvetica', '', 10);

		// add a page
		$pdf->AddPage();

		$tbl = ''; $i = 1;

		foreach ($marking_details as $key => $value) {
			$tbl .= '<tr>
				<td colspan="3" style="font-family: courier; font-size: 12px;">Line Item # '.$i++.'</td>
			</tr>
			<tr>
				<td style="font-family:courier;font-size: 11px;" align="left">'.addslashes($marking_details[$key]['description']).'</td>
				<td style="font-family: courier; font-size: 11px;" align="right">'.$value['quantity'].'</td>
				<td style="font-family: courier; font-size: 11px;" align="left">'.$value['unit_value'].'</td>
			</tr>
			<tr>
				<td style="font-family: courier; font-size: 16px;" colspan="3">'.nl2br($value['marking']).'</td>
			</tr>
			<tr><td colspan="3"></td></tr>
			';
		}


		$html = '
			<table cellpadding="5" cellspacing="0">
				<tr style="background-color: #fff;">
					<td style="vertical-align: text-top;" align="center">
						<img src="/assets/media/client-logos/logo.png" width="180" height="50">
					</td>
				</tr>
				<tr>
					<td>
						<table cellpadding="5" cellspacing="0" border="1">
							<tr>
								<td>Order #: -</td>
								<td>Proforma #: '.$marking_details[0]['proforma_no'].'</td>
								<td>Date: '.date('d M, Y', strtotime($marking_details[0]['confirmed_on'])).'</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table cellspacing="10" cellpadding="0" border="0" style="border: solid 1px grey;">
							<tr>
								<th align="center" colspan="3"><h3>Marking Details</h3></th>
							</tr>
							<tr>
								<th width="80%">Description</th>
								<th width="10%">Quantity</th>
								<th width="10%">Unit</th>
							</tr>
							'.$tbl.'
						</table>
					</td>
				</tr>
			</table>
		';

		// output the HTML content
		$pdf->writeHTML($html, true, false, true, false, '');

		//Close and output PDF document
		$pdf->Output(str_replace('/', '-', $marking_details[0]['proforma_no']).'.pdf', 'I');
	}

	public function quality_inspection_documentation() {

		$data = $this->prepare_listing_data(implode(' AND ', $this->default_where_for_all_tab('documentation', $this->session->userdata('tab_name'))),array(),10,0);
		if(in_array($this->session->userdata('user_id'), array(65, 120, 158, 167, 169, 179, 233, 238, 239, 255, 272, 273, 274, 276, 282, 299))) {

			$data = $this->prepare_listing_data(implode(' AND ', $this->default_where_for_all_tab('inspection', $this->session->userdata('tab_name'))),array(),10,0);
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Qualtity Listing'));
		$this->load->view('sidebar', array('title' => 'Qualtity Listing'));
		$this->load->view('quality/quality_document_inspection_list', $data);
		$this->load->view('footer');
	}

	public function update_documentation() {

		if(!empty($this->uri->segment('3'))) {

			$data = array();
			$data['quality_information'] = $this->common_model->get_dynamic_data_sales_db('*',array('id'=>$this->uri->segment('3')),'production_process_information','row_array');

			$data['user_quality_admin'] = $this->common_model->get_dynamic_data_sales_db('*',array('status'=>1, 'role'=>11), 'users');

			$data['user_quality_user'] = $this->common_model->get_dynamic_data_sales_db('*',array('status'=>1, 'role'=>10), 'users');
			
			$production_details = $this->common_model->get_dynamic_data_sales_db('*',array('id'=>$this->uri->segment('3')), 'production_process_information', 'row_array');
			
			$data['production_status'] =  $this->create_production_status_data();
			$this->common_model->update_data_sales_db('production_process_information', array('view_count'=>$production_details['view_count']+1), array('id'=>$this->uri->segment('3')));
			
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
			$this->load->view('header', array('title' => 'Update Documentation'));
			$this->load->view('sidebar', array('title' => 'Update Documentation'));
			$this->load->view('quality/quality_document_update', $data);
			$this->load->view('footer');
		} else {
			redirect('quality/quality_inspection_documentation');
		}
	}

	public function update_inspection() {

		if(!empty($this->uri->segment('3'))) {
			
			$data = array();
			$data['quality_information'] = $this->common_model->get_dynamic_data_sales_db('*',array('id'=>$this->uri->segment('3')),'production_process_information','row_array');
			$data['user_quality_admin'] = $this->common_model->get_dynamic_data_sales_db('*',array('status'=>1, 'role'=>11), 'users');
			$data['user_quality_user'] = $this->common_model->get_dynamic_data_sales_db('*',array('status'=>1, 'role'=>10), 'users');
			$data['production_status'] =  $this->create_production_status_data();
			$this->common_model->update_data_sales_db('production_process_information', array('view_count'=>$data['quality_information']['view_count']+1), array('id'=>$this->uri->segment('3')));
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;	
			$this->load->view('header', array('title' => 'Update Inspection'));
			$this->load->view('sidebar', array('title' => 'Update Inspection'));
			$this->load->view('quality/quality_inspection_update', $data);
			$this->load->view('footer');
		} else {
			redirect('quality/quality_inspection_documentation');
		}
	}

	public function ajax_function() {

		if($this->input->is_ajax_request()){
			$call_type = $this->input->post('call_type');
			$response['status'] = 'successful';	
			switch ($call_type) {
				
				case 'change_main_tab':
					$data = $this->prepare_listing_data(implode(' AND ', $this->default_where_for_all_tab($this->input->post('main_tab_name'), $this->session->userdata('tab_name'))),array(),10,0);
					$response['quality_list_body'] = $this->load->view('quality/quality_document_inspection_list_body', $data, true);
				break;

				case 'change_tab' :
					if(!empty($this->session->userdata('main_tab_name')) &&  !empty($this->input->post('tab_name'))) {

						$data = $this->prepare_listing_data(implode(' AND ', $this->default_where_for_all_tab($this->session->userdata('main_tab_name'), $this->input->post('tab_name'))),array(),10,0);
						$response['search_filter_body'] = $this->load->view('quality/quality_document_inspection_list_search_filter_form', $data, true);
						$response['quality_list_body'] = $this->load->view('quality/quality_document_inspection_list_body', $data, true);
						// $response['paggination_filter_body'] = $this->load->view('quality/quality_list_paggination', $data, true);
					}
				break;

				case 'search_filter':
					
					if(!empty($this->input->post('tab_name'))) {

						$where_response = $this->create_where_response_on_search_filter($this->input->post('quality_search_form'), $this->input->post('tab_name'));				
						// echo "<pre>";print_r($where_response);echo"</pre><hr>";exit;		
						$data = $this->prepare_listing_data(implode(' AND ', $where_response['where_string_array']),array(),10,0, $where_response['where_array']);
						$response['search_filter_body'] = $this->load->view('quality/quality_document_inspection_list_search_filter_form', $data, true);
						$response['list_body'] = $this->load->view('quality_document_inspection_list_body', $data, true);
						$response['paggination_filter_body'] = $this->load->view('quality/quality_document_inspection_list_paggination', $data, true);
					}
				break;

				case 'paggination_filter':

					if(!empty($this->input->post('tab_name'))) {

						$where_response = $this->create_where_response_on_search_filter($this->input->post('production_search_form'), $this->input->post('tab_name'));
						$data = $this->prepare_listing_data(implode(' AND ', $where_response['where_string_array']),array(),$this->input->post('limit'),$this->input->post('offset'), $where_response['where_array']);
						$response['search_filter_body'] = $this->load->view('quality/quality_document_inspection_list_search_filter_form', $data, true);
						$response['list_body'] = $this->load->view('quality_document_inspection_list_body', $data, true);
						$response['paggination_filter_body'] = $this->load->view('quality/quality_document_inspection_list_paggination', $data, true);
					}
				break;

				case 'save_quality_update_form':
					
					$update_array = array();
					foreach(array_column($this->input->post('form_data'), 'value', 'name') as $column_name => $column_value) {
						$update_array[$column_name] = $column_value;
					}
					$this->common_model->update_data_sales_db('production_process_information',array($update_array), 'id', 'batch');
				break;
				
				case 'delete_quality_list_data':
					$production_id = $this->input->post('delete_id');
					$response['status'] = 'failed';	
					if(!empty($production_id)) {
						$this->common_model->update_data_sales_db('production_process_information', array('status'=>'Inactive'), array('id'=>$production_id));
						$response['status'] = 'successful';
					}
                break;

                case 'save_email_name':
            		if(!empty($this->input->post('id'))) {

            			$this->common_model->update_data_sales_db('production_process_information', array('mtc_email_name'=> $this->input->post('mtc_email_name'), 'marking_email_name'=> $this->input->post('marking_email_name')), array('id'=>$this->input->post('id')));
            		}
            	break;
				
				default:
					$response['status'] = 'failed';	
					$response['message'] = "call type not found";
				break;
			}
			echo json_encode($response);

		} else{
			die('access is not allowed to this function');
		}
	}

	private function prepare_listing_data($where, $order_by, $limit, $offset, $search_filter_array = array()) {

		// echo "<pre>";print_r($where);echo"</pre><hr>";exit;
		$data = $this->quality_model->get_production_listingz_data($where, $order_by, $limit, $offset);
		// all product list
		$data['product_list'] = $this->get_common_data('product_list');
		// all material list
		$data['material_list'] = $this->get_common_data('material_list');
		// all currency list
		$data['currency_list'] = $this->get_common_data('currency_details');

		foreach ($data['production_list'] as $production_list_key => $production_list_value) {
			
			//getting client name
			$data['production_list'][$production_list_key]['client_details'] = $this->common_model->get_dynamic_data_sales_db('name',array('id'=>$production_list_value['client_id']), 'customer_mst', 'row_array');

			// getting currency symbol
			$data['production_list'][$production_list_key]['currency_details']['currency_icon'] = $data['currency_list'][$production_list_value['currency']];

			//getting product details
			$product_details = $this->common_model->get_dynamic_data_sales_db('*',array('quotation_mst_id'=>$production_list_value['quotation_mst_id']), 'quotation_dtl');

			// production status name
			$data['production_list'][$production_list_key]['production_status_name'] = $this->create_production_status_data('single', $production_list_value['production_status']);
			// echo "<pre>";print_r($product_details);echo"</pre><hr>";
			if(!empty($product_details)) {
				
				$data['production_list'][$production_list_key]['product_details'] = array();
				$product_array = array();
				$material_array = array();
				foreach ($product_details as $product_key => $single_product_detail) {
					
					if(!empty($data['product_list'][$single_product_detail['product_id']])) {

						if(!in_array($data['product_list'][$single_product_detail['product_id']], $product_array)) {

							$product_array[] = $data['product_list'][$single_product_detail['product_id']];
						}
					}
					if(!empty($data['material_list'][$single_product_detail['material_id']])) {

						if(!in_array($data['material_list'][$single_product_detail['material_id']], $material_array)) {

							$material_array[] = $data['material_list'][$single_product_detail['material_id']];
						}
					}
				}
				//finding maximum count
				$higher_count = (count($product_array)>count($material_array)) ? count($product_array) : count($material_array);
				for ($i=0; $i < $higher_count; $i++) { 
					$data['production_list'][$production_list_key]['product_details'][$i]['product_name'] = '';
					$data['production_list'][$production_list_key]['product_details'][$i]['material_name'] = '';
					if(!empty($product_array[$i])) {

						$data['production_list'][$production_list_key]['product_details'][$i]['product_name'] = $product_array[$i];
					}
					if(!empty($material_array[$i])) {

						$data['production_list'][$production_list_key]['product_details'][$i]['material_name'] = $material_array[$i];
					}
				}
			}

			$data['production_list'][$production_list_key]['pdf_button'] = false;
			
			$work_order_sheet_details = $this->common_model->get_dynamic_data_sales_db('*',array('work_order_no'=>$production_list_value['work_order_no']), 'work_order_sheet', 'row_array');
			// echo "<pre>";print_r($work_order_sheet_details);echo"</pre><hr>";exit;
			if(!empty($work_order_sheet_details)) {
				if(!empty($work_order_sheet_details['approved_by'])) {
				
					$data['production_list'][$production_list_key]['pdf_button'] = true;
				}
			}

			$po_details = $this->common_model->get_dynamic_data_sales_db('id, vendor_name, gross_total, delivery_time',array('work_order_no'=>$production_list_value['work_order_no']), 'procurement');
			$data['production_list'][$production_list_key]['po_details'] = array();
			if(!empty($po_details)){

				$data['production_list'][$production_list_key]['po_details'] = $po_details;
			}

			if(!empty($production_list_value['latest_update'])){

				$last_comment_array = json_decode($production_list_value['latest_update'], true);

				$last_update = $last_comment_array[count($last_comment_array)-1];
				$data['production_list'][$production_list_key]['last_latest_update'] = $last_update['message'];
			}
		}

		// all client name list
		$data['client_list'] = $this->quality_model->get_production_listingz_client_list_data();

		//admin data
		$data['user_quality_admin'] = array_column($this->common_model->get_dynamic_data_sales_db('user_id,name',array('role'=>11, 'status'=>1), 'users'),'name','user_id');
		//admin user
		$data['user_quality_user'] =  array_column($this->common_model->get_dynamic_data_sales_db('user_id,name',array('role'=>10, 'status'=>1), 'users'),'name','user_id');
		// all handle by list
		$data['handle_by_users'] = $this->get_common_data('handle_by');
		// all production status list
		$data['production_status'] = $this->create_production_status_data();
		// all user list
		$data['users_details'] = array_merge(array('0'=>''),$this->get_common_data('users_details'));
		// all user Sales Person list
		$data['sales_person_details'] = $this->get_common_data('sales_person');
		$data['payment_term_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*',array('status'=>'Active'), 'payment_terms'),'term_value', 'term_id');
		$data['paggination_data']['limit'] = $limit;
		$data['paggination_data']['offset'] = $offset;
		$data['search_filter'] = $this->create_search_filter_data($search_filter_array);
		$data['total_revenue_list'] = $this->quality_model->production_listingz_total_revenue($where);
		//  echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		return $data;
	}

	private function default_where_for_all_tab($main_tab_name, $tab_name){

		$this->session->set_userdata('main_tab_name',$main_tab_name);
		$this->session->set_userdata('tab_name',$tab_name);
		$return_where = array(
							0=> "(production_process_information.production_status is NULL OR production_process_information.production_status != 'dispatched' AND production_process_information.production_status != 'on_hold' AND production_process_information.production_status != 'ready_for_dispatch' AND production_process_information.production_status != 'semi_ready' AND production_process_information.production_status != 'order_cancelled')",
							1=> "(production_process_information.status is NULL or production_process_information.status = 'Active')");

		if($tab_name == 'dispatch_order') {
			$return_where[0] = "production_process_information.production_status = 'dispatched'";
		
		}elseif ($tab_name == 'on_hold_order') {
			$return_where[0] = "production_process_information.production_status = 'on_hold'";
		
		}elseif ($tab_name == 'ready_to_dispatch_order') {
			$return_where[0] = "production_process_information.production_status = 'ready_for_dispatch'";
		
		}elseif ($tab_name == 'semi_ready') {
			$return_where[0] = "production_process_information.production_status = 'semi_ready'";
		
		}elseif ($tab_name == 'order_cancelled') {
			$return_where[0] = "production_process_information.production_status = 'order_cancelled'";
		
		}

		if($this->session->userdata('quality_access')['quality_product_family_access']) {
			$return_where[] = " production_process_information.product_family IN ('".implode("', '", $this->session->userdata('quality_access')['quality_product_family_access'])."')";
		}
		return $return_where;
	}

	private function get_common_data($data_type = '') {

		$return_array = array();
		if(!empty($data_type)) {

			switch ($data_type) {
				case 'product_list':	
					$return_array = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status' => 'Active'), 'product_mst'), 'name', 'id');	
				break;

				case 'material_list':
					$return_array = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status' => 'Active'), 'material_mst'), 'name', 'id');
				break;
				
				case 'handle_by':
					$return_array = $this->common_model->get_procurement_person('user_id, name');
				break;

				case 'users_details':
					$return_array = array_column($this->common_model->get_dynamic_data_sales_db('user_id, name',array(), 'users'),'name','user_id');
				break;

				case 'currency_details':
					$return_array = array_column($this->common_model->get_dynamic_data_sales_db('currency_id, decimal_number',array('status'=>'Active'), 'currency'),'decimal_number','currency_id');
				break;

				case 'sales_person':
					$return_array = array_column($this->common_model->get_sales_person(),'name','user_id');
				break;

				default:
					
					echo "Data Type does not matched!!!";die('debug');
				break;
			}
		} else {
			echo "Data Type is provided!!!";die('debug');
		}
		return $return_array;
	}

	private function create_production_status_data($call_type='all', $production_status_name = ''){

		$static_production_status =  array(
										'yet_to_start_production'=> array('name'=>'Yet To Start Production','color'=>'dark'),
										'in_production'=> array('name'=>'In Production', 'color'=>'warning'),
										'ready_for_dispatch'=>array('name'=>'Ready For Dispatch', 'color'=>'primary'),
										'delay_in_delivery'=>array('name'=>'Delay in Delivery', 'color'=>'danger'),
										'on_hold'=>array('name'=>'Order On Hold', 'color'=>'gainsboro'),
										'dispatched'=>array('name'=>'Dispatched', 'color'=>'success'),
										'semi_ready'=>array('name'=>'Semi Ready', 'color'=>'primary'),
										'order_cancelled'=>array('name'=>'Order Cancelled', 'color'=>'danger'),
										'merchant_trade'=>array('name'=>'Merchant Trade(MTT)', 'color'=>'danger'),
										'query'=>array('name'=>'Query', 'color'=>'gainsboro'),
										'mtt_rfd'=>array('name'=>'MTT/RFD', 'color'=>'dark')
									);
		if($call_type == 'all') {

			return $static_production_status;
		} else {

			return (!empty($production_status_name)) ? $static_production_status[$production_status_name] : array('name'=>'','color'=>'');
		}
	}

	private function create_search_filter_data($filter_data = '') {

		$return_array = array(
			'work_order_no' => array(0=>''), 'client_name' => array(), 'assigned_to' => array(), 'product_family' => array(), 
			'proforma_no' => array(0=>''), 'order_no' => array(0=>''), 'proforma_date' => array(0=>''), 'payment_term' => array(), 
			'payment_status' => array(0=>''), 'vendor_po' => array(0=>''), 'delivery_date' => array(0=>''), 'qc_clearance' => array(),
			'documentation_marking_assign_by' => array(), 'documentation_marking_assign_to' => array(), 'documentation_marking_checked_by' => array(),
			'documentation_mtc_assign_by' => array(), 'documentation_mtc_assign_to' => array(), 'documentation_mtc_checked_by' => array(),
			'soft_copy' => array(), 'hard_copy' => array(),
			'inspection_assign_by' => array(), 'inspection_assign_to' => array(), 'inspection_dimension' => array(),
			'inspection_photo' => array(), 'mtc_sent' => array(), 'latest_update' => array(0=>''), 'special_comment' => array(0=>''), 'handled_by' => array(),
			'production_status' => array()
		);
		if(!empty($filter_data)) {
			foreach ($filter_data as $key => $value) {
				$return_array[$key] = $value;
			}
		}
		return $return_array; 
	}

	private function create_where_response_on_search_filter($search_form_data, $tab_name) {

		// echo "<pre>";print_r($search_form_data);echo"</pre><hr>";exit;
		$return_response = array();
		$return_response['where_array'] = array();
		$return_response['where_string_array'] = $this->default_where_for_all_tab($this->session->userdata('main_tab_name'), $tab_name);
		if(!empty($search_form_data)) {

			foreach ($search_form_data as $form_key => $form_data) {

				if(!empty($form_data['value'])){

					if($form_data['value'] != 'blank') {

						$return_response['where_array'][$form_data['name']][] = $form_data['value'];
					} else {

						$return_response['where_array'][$form_data['name']][] = 0;
					}
				}
			}
		}
		// echo "<pre>";print_r($return_response['where_array']);echo"</pre><hr>";die;
		foreach ($return_response['where_array'] as $column_name => $column_details) {

			$current_count_number = count($return_response['where_string_array']);
			if($column_name == 'work_order_no'){

				$return_response['where_string_array'][$current_count_number] = "production_process_information.work_order_no LIKE '%".$column_details[0]."%'";
			} elseif($column_name == 'client_name') {

				$return_response['where_string_array'][$current_count_number] = "(quotation_mst.client_id = '".$column_details[0]."'";
				unset($column_details[0]);
				foreach ($column_details as $client_id) {
					$return_response['where_string_array'][$current_count_number] .= " OR quotation_mst.client_id = '".$client_id."'";
				}
				$return_response['where_string_array'][$current_count_number] .= ")";
			} elseif($column_name == 'assigned_to') {

				$return_response['where_string_array'][$current_count_number] = "(quotation_mst.assigned_to = '".$column_details[0]."'";
				unset($column_details[0]);
				foreach ($column_details as $sales_person_id) {
					$return_response['where_string_array'][$current_count_number] .= " OR quotation_mst.assigned_to = '".$sales_person_id."'";
				}
				$return_response['where_string_array'][$current_count_number] .= ")";
			} elseif($column_name == 'product_family') {

				$return_response['where_string_array'][$current_count_number] = "(production_process_information.product_family = '".$column_details[0]."'";
				unset($column_details[0]);
				foreach ($column_details as $product_family_name) {
					$return_response['where_string_array'][$current_count_number] .= " OR production_process_information.product_family = '".$product_family_name."'";
				}
				$return_response['where_string_array'][$current_count_number] .= ")";
			} elseif($column_name == 'proforma_no') {

				$return_response['where_string_array'][$current_count_number] = "quotation_mst.proforma_no LIKE '%".$column_details[0]."%'";
			} elseif($column_name == 'order_no') {

				$return_response['where_string_array'][$current_count_number] = "quotation_mst.order_no LIKE '%".$column_details[0]."%'";
			} elseif($column_name == 'proforma_date') {
					
				$date_explode = explode(' - ',$column_details[0]);
				if(count($date_explode) == 1) {
					$return_response['where_string_array'][$current_count_number] = "(quotation_mst.confirmed_on >= '".date('Y-m-d 00:00:00', strtotime($date_explode[0]))."' AND quotation_mst.confirmed_on <= '".date('Y-m-d 23:59:59',strtotime($date_explode[0]))."')";	
				} else {
					$return_response['where_string_array'][$current_count_number] = "(quotation_mst.confirmed_on >= '".date('Y-m-d 00:00:00', strtotime($date_explode[0]))."' AND quotation_mst.confirmed_on <= '".date('Y-m-d 23:59:59',strtotime($date_explode[1]))."')";	
				}
				$this->session->unset_userdata('search_filter_proforma_date');
				$this->session->set_userdata('search_filter_proforma_date', $column_details[0]);
			} else if($column_name == 'payment_term') {

				$return_response['where_string_array'][$current_count_number] = "(quotation_mst.payment_term = '".$column_details[0]."'";
				unset($column_details[0]);
				foreach ($column_details as $payment_terms_name) {
					$return_response['where_string_array'][$current_count_number] .= " OR quotation_mst.payment_term = '".$product_family_name."'";
				}
				$return_response['where_string_array'][$current_count_number] .= ")";
			} elseif($column_name == 'payment_status') {

				$return_response['where_string_array'][$current_count_number] = "production_process_information.payment_status LIKE '%".$column_details[0]."%'";
			} elseif($column_name == 'vendor_po') {

				$return_response['where_string_array'][$current_count_number] = "production_process_information.vendor_po LIKE '%".$column_details[0]."%'";
			} elseif($column_name == 'delivery_date') {
				
				$this->session->unset_userdata('search_filter_delivery_date');
				$this->session->set_userdata('search_filter_delivery_date', $column_details[0]);
				$date_explode = explode(' - ',$column_details[0]);
				if(count($date_explode) == 1) {
					$return_response['where_string_array'][$current_count_number] = "production_process_information.delivery_date = '".date('Y-m-d', strtotime($date_explode[0]))."'";
				} else {
					if($date_explode[0] == $date_explode[1]) {
						$return_response['where_string_array'][$current_count_number] = "production_process_information.delivery_date = '".date('Y-m-d', strtotime($date_explode[0]))."'";
					} else{ 
						$return_response['where_string_array'][$current_count_number] = "(production_process_information.delivery_date >= '".date('Y-m-d', strtotime($date_explode[0]))."' AND production_process_information.delivery_date <= '".date('Y-m-d',strtotime($date_explode[1]))."')";
					}
				}
			} else if($column_name == 'qc_clearance') {

				$return_response['where_string_array'][$current_count_number] = "(production_process_information.qc_clearance = '".$column_details[0]."'";
				unset($column_details[0]);
				foreach ($column_details as $payment_terms_name) {
					$return_response['where_string_array'][$current_count_number] .= " OR production_process_information.qc_clearance = '".$product_family_name."'";
				}
				$return_response['where_string_array'][$current_count_number] .= ")";
			}elseif($column_name == 'documentation_marking_assign_by') {

				$return_response['where_string_array'][$current_count_number] = "(production_process_information.documentation_marking_assign_by = '".$column_details[0]."'";
				unset($column_details[0]);
				foreach ($column_details as $sales_person_id) {
					$return_response['where_string_array'][$current_count_number] .= " OR production_process_information.documentation_marking_assign_by = '".$sales_person_id."'";
				}
				$return_response['where_string_array'][$current_count_number] .= ")"; 
			}elseif($column_name == 'documentation_marking_assign_to') {

				$return_response['where_string_array'][$current_count_number] = "(production_process_information.documentation_marking_assign_to = '".$column_details[0]."'";
				unset($column_details[0]);
				foreach ($column_details as $sales_person_id) {
					$return_response['where_string_array'][$current_count_number] .= " OR production_process_information.documentation_marking_assign_to = '".$sales_person_id."'";
				}
				$return_response['where_string_array'][$current_count_number] .= ")";
			}elseif($column_name == 'documentation_marking_checked_by') {

				$return_response['where_string_array'][$current_count_number] = "(production_process_information.documentation_marking_checked_by = '".$column_details[0]."'";
				unset($column_details[0]);
				foreach ($column_details as $sales_person_id) {
					$return_response['where_string_array'][$current_count_number] .= " OR production_process_information.documentation_marking_checked_by = '".$sales_person_id."'";
				}
				$return_response['where_string_array'][$current_count_number] .= ")";
			}elseif($column_name == 'documentation_mtc_assign_by') {

				$return_response['where_string_array'][$current_count_number] = "(production_process_information.documentation_mtc_assign_by = '".$column_details[0]."'";
				unset($column_details[0]);
				foreach ($column_details as $sales_person_id) {
					$return_response['where_string_array'][$current_count_number] .= " OR production_process_information.documentation_mtc_assign_by = '".$sales_person_id."'";
				}
				$return_response['where_string_array'][$current_count_number] .= ")"; 
			}elseif($column_name == 'documentation_mtc_assign_to') {

				$return_response['where_string_array'][$current_count_number] = "(production_process_information.documentation_mtc_assign_to = '".$column_details[0]."'";
				unset($column_details[0]);
				foreach ($column_details as $sales_person_id) {
					$return_response['where_string_array'][$current_count_number] .= " OR production_process_information.documentation_mtc_assign_to = '".$sales_person_id."'";
				}
				$return_response['where_string_array'][$current_count_number] .= ")";
			}elseif($column_name == 'documentation_mtc_checked_by') {

				$return_response['where_string_array'][$current_count_number] = "(production_process_information.documentation_mtc_checked_by = '".$column_details[0]."'";
				unset($column_details[0]);
				foreach ($column_details as $sales_person_id) {
					$return_response['where_string_array'][$current_count_number] .= " OR production_process_information.documentation_mtc_checked_by = '".$sales_person_id."'";
				}
				$return_response['where_string_array'][$current_count_number] .= ")";
			}elseif($column_name == 'soft_copy') {

				$return_response['where_string_array'][$current_count_number] = "(production_process_information.soft_copy = '".$column_details[0]."'";
				unset($column_details[0]);
				foreach ($column_details as $sales_person_id) {
					$return_response['where_string_array'][$current_count_number] .= " OR production_process_information.soft_copy = '".$sales_person_id."'";
				}
				$return_response['where_string_array'][$current_count_number] .= ")";
			}elseif($column_name == 'hard_copy') {

				$return_response['where_string_array'][$current_count_number] = "(production_process_information.hard_copy = '".$column_details[0]."'";
				unset($column_details[0]);
				foreach ($column_details as $sales_person_id) {
					$return_response['where_string_array'][$current_count_number] .= " OR production_process_information.hard_copy = '".$sales_person_id."'";
				}
				$return_response['where_string_array'][$current_count_number] .= ")";
			}elseif($column_name == 'inspection_assign_by') {

				$return_response['where_string_array'][$current_count_number] = "(production_process_information.inspection_assign_by = '".$column_details[0]."'";
				unset($column_details[0]);
				foreach ($column_details as $sales_person_id) {
					$return_response['where_string_array'][$current_count_number] .= " OR production_process_information.inspection_assign_by = '".$sales_person_id."'";
				}
				$return_response['where_string_array'][$current_count_number] .= ")";
			}elseif($column_name == 'inspection_assign_to') {

				$return_response['where_string_array'][$current_count_number] = "(production_process_information.inspection_assign_to = '".$column_details[0]."'";
				unset($column_details[0]);
				foreach ($column_details as $sales_person_id) {
					$return_response['where_string_array'][$current_count_number] .= " OR production_process_information.inspection_assign_to = '".$sales_person_id."'";
				}
				$return_response['where_string_array'][$current_count_number] .= ")";
			}elseif($column_name == 'inspection_dimension') {

				$return_response['where_string_array'][$current_count_number] = "(production_process_information.inspection_dimension = '".$column_details[0]."'";
				unset($column_details[0]);
				foreach ($column_details as $sales_person_id) {
					$return_response['where_string_array'][$current_count_number] .= " OR production_process_information.inspection_dimension = '".$sales_person_id."'";
				}
				$return_response['where_string_array'][$current_count_number] .= ")";
			}elseif($column_name == 'inspection_photo') {

				$return_response['where_string_array'][$current_count_number] = "(production_process_information.inspection_photo = '".$column_details[0]."'";
				unset($column_details[0]);
				foreach ($column_details as $sales_person_id) {
					$return_response['where_string_array'][$current_count_number] .= " OR production_process_information.inspection_photo = '".$sales_person_id."'";
				}
				$return_response['where_string_array'][$current_count_number] .= ")";
			}elseif($column_name == 'latest_update') {

				$return_response['where_string_array'][$current_count_number] = "production_process_information.latest_update LIKE '%".$column_details[0]."%'";
			} elseif($column_name == 'special_comment') {

				$return_response['where_string_array'][$current_count_number] = "production_process_information.special_comment LIKE '%".$column_details[0]."%'";
			} elseif($column_name == 'handled_by') {
				
				$return_response['where_string_array'][$current_count_number] = "(production_process_information.handled_by LIKE '%".$column_details[0]."%'";
				unset($column_details[0]);
				foreach ($column_details as $handle_user_name) {
					$return_response['where_string_array'][$current_count_number] .= " OR production_process_information.handled_by LIKE '%".$handle_user_name."%'";
				}
				$return_response['where_string_array'][$current_count_number] .= ")";
			} elseif($column_name == 'production_status') {
				unset($return_response['where_string_array'][2]);
				$return_response['where_string_array'][2] = "(production_process_information.production_status = '".$column_details[0]."'";
				unset($column_details[0]);
				foreach ($column_details as $production_status) {
					$return_response['where_string_array'][2] .= " OR production_process_information.production_status = '".$production_status."'";
				}
				$return_response['where_string_array'][2] .= ")";
			}
		}
		// echo "<pre>";print_r($return_response);echo"</pre><hr>";exit;
		return $return_response;
	}
}