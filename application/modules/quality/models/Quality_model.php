<?php 
Class Quality_model extends CI_Model{


	function getSuggestions($search, $mtc_for){
		if($mtc_for == 'quotation_no'){
			$this->db->select('c.id, c.name, m.comp_dtl_id, m.member_name, q.assigned_to, q.quote_no mtc_str, q.quotation_mst_id mtc_str_id');
			$this->db->join('customer_mst c', 'c.id = q.client_id', 'inner');
			$this->db->join('customer_dtl m', 'm.comp_dtl_id = q.member_id', 'inner');
			$this->db->where("q.quote_no like '%".$search."%'");
			return $this->db->get('quotation_mst q')->result_array();
		} else if($mtc_for == 'proforma_no'){
			$this->db->select('c.id, c.name, m.comp_dtl_id, m.member_name, q.assigned_to, q.proforma_no mtc_str, q.quotation_mst_id mtc_str_id');
			$this->db->join('customer_mst c', 'c.id = q.client_id', 'inner');
			$this->db->join('customer_dtl m', 'm.comp_dtl_id = q.member_id', 'inner');
			$this->db->where("q.proforma_no like '%".$search."%'");
			return $this->db->get('quotation_mst q')->result_array();
		} else if($mtc_for == 'invoice_no'){
			$this->db->select('c.id, c.name, i.invoice_no mtc_str, i.invoice_mst_id mtc_str_id');
			$this->db->join('customer_mst c', 'c.id = i.company_id', 'inner');
			//$this->db->join('clients c', 'c.client_id = q.client_id', 'inner');
			$this->db->where("i.invoice_no like '%".$search."%'");
			return $this->db->get('invoice_mst i')->result_array();
		}
	}

	function insertData($table, $data){
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	function updateData($table, $data, $where){
		$this->db->update($table, $data, $where);
	}

	function deleteData($table, $where){
		$this->db->delete($table, $where);
	}

	function getData($table, $where=''){
		if($where != ''){
			$this->db->where($where);
		}
		return $this->db->get($table)->result_array();
	}

	function getMTCList($start, $length, $search, $order, $dir){
		$this->db->select('m.*, uq.name assigned_to, us.name created_by');
		$this->db->join('users us', 'us.user_id = m.created_by', 'inner');
		$this->db->join('users uq', 'uq.user_id = m.assigned_to', 'left');
		$this->db->join('customer_mst c', 'c.id = m.mtc_company_id', 'left');
		if(!empty($search)){
			foreach ($search as $key => $value) {
				if($value != ''){
					if($key == 'company_name' || $key == 'member_name'){
						$this->db->where($key." like '%".$value."%'");
					}else{
						$this->db->where($key, $value);
					}
				}
			}
		}
		if($this->session->userdata('role') == 5){
			$this->db->where('created_by', $this->session->userdata('user_id'));
		}
		if($this->session->userdata('role') == 10){
			$this->db->where('assigned_to', $this->session->userdata('user_id'));
		}
		$this->db->limit($length, $start);
		$this->db->order_by($order, $dir);
		$res = $this->db->get('mtc_mst m')->result_array();

		$k=0;
		$result = array();
		foreach ($res as $key => $value) {
			$result[$key] = $value;
			$result[$key]['record_id'] = ++$k;
			$result[$key]['files'] = $this->getData('mtc_files', 'mtc_mst_id = '.$value['mtc_mst_id']);
		}
		return $result;
	}

	function getMTCListCount($search){
		$this->db->select('m.*, uq.name assigned_to, us.name created_by');
		$this->db->join('users us', 'us.user_id = m.created_by', 'inner');
		$this->db->join('users uq', 'uq.user_id = m.assigned_to', 'left');
		$this->db->join('customer_mst c', 'c.id = m.mtc_company_id', 'left');
		if(!empty($search)){
			foreach ($search as $key => $value) {
				if($value != ''){
					if($key == 'company_name' || $key == 'member_name'){
						$this->db->where($key." like '%".$value."%'");
					}else{
						$this->db->where($key, $value);
					}
				}
			}
		}
		if($this->session->userdata('role') == 5){
			$this->db->where('created_by', $this->session->userdata('user_id'));
		}
		if($this->session->userdata('role') == 10){
			$this->db->where('assigned_to', $this->session->userdata('user_id'));
		}
		$res = $this->db->get('mtc_mst m')->result_array();
		return sizeof($res);
	}


	// function getSampleMTCStatus($quote_id){
		// 	return $this->db->get_where('mtc_mst', array('mtc_type' => 'sample', 'mtc_for_id' => $quote_id))->row_array();
	// }

	function getMTCDetails($mtc_id){
		return $this->db->get_where('mtc_mst', array('mtc_mst_id' => $mtc_id))->row_array();
	}

	// function getLineItems($quote_id){
		// 	$this->db->select('qd.*, u.unit_value unit, p.lookup_value product, m.lookup_value material');
		// 	$this->db->join('units u', 'u.unit_id = qd.unit', 'inner');
		// 	$this->db->join('lookup p', 'p.lookup_id = qd.product_id', 'left');
		// 	$this->db->join('lookup m', 'm.lookup_id = qd.material_id', 'left');
		// 	return $this->db->get_where('quotation_dtl qd', array('quotation_mst_id' => $quote_id))->result_array();
	// }

	// function getHeatNumber($product_id){
		// 	$res = $this->db->get_where('number_logic', array('logic_for' => $product_id))->row_array();
		// 	return $res;
	// }
	
	// function getMarkingDetails($marking_id){
		// 	$this->db->select('m.*, md.*, q.quote_no, q.quotation_mst_id, q.proforma_no, q.confirmed_on, q.client_id, q.member_id, qd.*, c.client_name, me.name, u.unit_value');
		// 	$this->db->join('marking_dtl md', 'm.marking_mst_id = md.marking_mst_id', 'inner');
		// 	$this->db->join('quotation_mst q', 'q.quotation_mst_id = m.marking_for_id', 'inner');
		// 	$this->db->join('quotation_dtl qd', 'qd.quotation_dtl_id = md.quotation_dtl_id', 'inner');
		// 	$this->db->join('clients c', 'c.client_id = q.client_id', 'inner');
		// 	$this->db->join('members me', 'me.member_id = q.member_id', 'inner');
		// 	$this->db->join('units u', 'u.unit_id = qd.unit', 'inner');
		// 	return $this->db->get_where('marking_mst m', array('m.marking_mst_id' => $marking_id))->result_array();
	// }

	// function getMarkingList($start, $length, $search, $order, $dir){
		// 	$this->db->select('m.*, uq.name assigned_to, us.name created_by, DATE_FORMAT(q.entered_on, "%d-%b") quote_date, c.client_name');
		// 	$this->db->join('quotation_mst q', 'q.quotation_mst_id = m.marking_for_id', 'inner');
		// 	$this->db->join('clients c', 'c.client_id = q.client_id', 'inner');
		// 	$this->db->join('users us', 'us.user_id = m.made_by', 'inner');
		// 	$this->db->join('users uq', 'uq.user_id = m.assigned_to', 'left');
		// 	$this->db->limit($length, $start);
		// 	$this->db->order_by($order, $dir);
		// 	if(!empty($search)){
		// 		foreach ($search as $key => $value) {
		// 			if($value != ''){
		// 				if($key == 'client_name'){
		// 					$this->db->where($key." like '%".$value."%'");
		// 				}else{
		// 					$this->db->where($key, $value);
		// 				}
		// 			}
		// 		}
		// 	}
		// 	$res = $this->db->get('marking_mst m')->result_array();
		// 	$k=0;
		// 	$result = array();
		// 	foreach ($res as $key => $value) {
		// 		$result[$key] = $value;
		// 		$result[$key]['record_id'] = ++$k;
		// 	}
		// 	return $result;
	// }

	// function getMarkingListCount($search){
		// 	$this->db->select('m.*, uq.name assigned_to, us.name created_by, DATE_FORMAT(q.entered_on, "%d-%b") quote_date, c.client_name');
		// 	$this->db->join('quotation_mst q', 'q.quotation_mst_id = m.marking_for_id', 'inner');
		// 	$this->db->join('clients c', 'c.client_id = q.client_id', 'inner');
		// 	$this->db->join('users us', 'us.user_id = m.made_by', 'inner');
		// 	$this->db->join('users uq', 'uq.user_id = m.assigned_to', 'left');
		// 	if(!empty($search)){
		// 		foreach ($search as $key => $value) {
		// 			if($value != ''){
		// 				if($key == 'client_name'){
		// 					$this->db->where($key." like '%".$value."%'");
		// 				}else{
		// 					$this->db->where($key, $value);
		// 				}
		// 			}
		// 		}
		// 	}
		// 	$res = $this->db->get('marking_mst m')->result_array();
		// 	return sizeof($res);
	// }

	public function get_production_listingz_data($where_array, $order_by_array, $limit, $offset) {

		$this->db->select('SQL_CALC_FOUND_ROWS quotation_mst.client_id, quotation_mst.quotation_mst_id, quotation_mst.proforma_no, quotation_mst.confirmed_on, quotation_mst.currency, quotation_mst.grand_total, quotation_mst.assigned_to, quotation_mst.order_no, quotation_mst.payment_term, quotation_mst.purchase_order, quotation_mst.po_add_time,
		production_process_information.handled_by, production_process_information.vendor_po, production_process_information.handled_by, production_process_information.production_status, production_process_information.product_family, production_process_information.payment_status, production_process_information.delivery_date, production_process_information.qc_clearance, production_process_information.latest_update, production_process_information.special_comment, production_process_information.work_order_no, production_process_information.id,
		production_process_information.mtc_sent, production_process_information.documentation_marking_assign_by,
		production_process_information.documentation_marking_assign_to, production_process_information.documentation_marking_checked_by, production_process_information.documentation_marking_comment, production_process_information.documentation_mtc_assign_by, production_process_information.documentation_mtc_assign_to, production_process_information.documentation_mtc_checked_by,
		production_process_information.documentation_mtc_comment, production_process_information.soft_copy, 
		production_process_information.hard_copy, production_process_information.inspection_assign_by, 
		production_process_information.inspection_assign_to, production_process_information.inspection_dimension, production_process_information.inspection_photo, production_process_information.inspection_marking,  production_process_information.inspection_comment, production_process_information.view_count, production_process_information.documentation_marking_status,
		production_process_information.handled_by_new, production_process_information.documentation_mtc_status,  production_process_information.marking_pdf, production_process_information.mtc_pdf', FALSE);
		$this->db->join('quotation_mst', 'quotation_mst.quotation_mst_id = production_process_information.quotation_mst_id', 'left');
		$this->db->where($where_array, null, false);
		$this->db->order_by('production_process_information.id','desc');
		$this->db->where(array('quotation_mst.currency != '=>''));
		$return_array['production_list'] = $this->db->get('production_process_information', $limit, $offset)->result_array();
	    $return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows;")->row_array();
		return $return_array;
	}

	public function get_production_listingz_client_list_data() {

		$this->db->select('quotation_mst.client_id, customer_mst.name');
		$this->db->join('customer_mst', 'customer_mst.id = quotation_mst.client_id', 'left');
		$this->db->where(array('quotation_mst.stage'=>'Proforma', 'quotation_mst.status'=>'Won'));
		$this->db->group_by('quotation_mst.client_id');
		$res = $this->db->get('quotation_mst')->result_array();
		return $res;
	}

	public function production_listingz_total_revenue($where_string) {

		$this->db->select('quotation_mst.currency, sum(quotation_mst.grand_total) as total');
		$this->db->join('production_process_information', 'production_process_information.quotation_mst_id = quotation_mst.quotation_mst_id', 'left');
		$this->db->where($where_string, null, false);
		$this->db->where(array('quotation_mst.currency !='=> ''));
		$this->db->group_by('quotation_mst.currency');
		$result = $this->db->get('quotation_mst')->result_array();
		return $result;
	}
	
}