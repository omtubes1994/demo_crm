jQuery(document).ready(function() {
    var method = "<?php echo $this->uri->segment('2',0)?>";
    if(method == 'quality_inspection_documentation') {
        Listing_js.init();
    }
    
    $('li.main_tab_name_click').click(function(){

        get_main_tab_name();
        ajax_call_function({call_type: 'change_main_tab', main_tab_name: $(this).attr('tab-name')},'change_main_tab');
        $('li.main_tab_name_click').removeClass('active_list');
        $(this).addClass('active_list');
    });
     $('li.tab_name_click').click(function(){
        
        var tab_name = $(this).attr('tab-name');
        if(!$('li.'+tab_name).hasClass('active_list')){
            $('li.active_list').removeClass('active_list');
            $('li.'+tab_name).addClass('active_list');
            ajax_call_function({call_type: 'change_tab',tab_name: tab_name},'change_tab');  
        }
    });
    $('a.add_quality_listing_search_filter_form').click(function(){
        
        $('.quality_listing_search_filter').show();
    });
    $('div.quality_listing_search_filter').on('click', 'button.quality_listing_search_filter_form_submit', function(){
        ajax_call_function({call_type: 'search_filter',tab_name:get_tab_name() , quality_search_form: $('form#quality_listing_search_filter_form').serializeArray()},'search_filter');  
    });
    $('select.handled_by_select_picker').selectpicker();
    $('button.save_quality_documentation_data').click(function(){

        ajax_call_function({call_type: 'save_quality_update_form', form_data: $('form#quality_documentation_form').serializeArray()},'save_quality_update_form');   
    });
    $('button.save_quality_inspection_data').click(function(){

        ajax_call_function({call_type: 'save_quality_update_form', form_data: $('form#quality_inspection_form').serializeArray()},'save_quality_update_form');  
    });
    $('div#quality_list_paggination').on('click', '.quality_list_paggination_number', function(){
        ajax_call_function({call_type: 'paggination_filter', tab_name: get_tab_name(), quality_inspection_search_form: $('form#quality_inspection_search_filter_form').serializeArray(), limit: $(this).attr('limit'), offset: $(this).attr('offset')},'paggination_filter');
    });
    $('div#quality_list_paggination').on('change', 'select#set_limit', function(){
        ajax_call_function({call_type: 'paggination_filter', tab_name: get_tab_name(),  quality_search_form: $('form#quality_listing_search_filter_form').serializeArray(), limit: $('select#set_limit').val(), offset: 0},'paggination_filter');
    });
    $('tbody#quality_listing').on('click', '.delete_quality_list_data', function(){
        swal({
              title: "Are you sure?",
              text: "Once deleted, you will not be able to recover this data!",
              icon: "warning",
              buttons:  ["Cancel", "Delete"],
              dangerMode: true, 
            })
        .then((willDelete) => {
            if (willDelete) {
                ajax_call_function({call_type: 'delete_quality_list_data', delete_id: $(this).attr('quality_id')},'delete_quality_list_data');
            } else {
                swal({
                    title: "Quality data is not deleted",
                    icon: "info",
                });
            }
        });
    });
    $('#quality_listing').on('click', 'a.mtc_upload', function(){
         $('button.save_email_name').hide();
        ajax_call_function({call_type: 'get_mtc_upload_history', production_id: $(this).attr('production_id')},'get_mtc_upload_history', "<?php echo base_url('production/ajax_function');?>");        
    });
    $('button.mtc_upload_close').click(function(){

        ajax_call_function({call_type: 'unset_mtc_production_id'},'unset_mtc_production_id', "<?php echo base_url('production/ajax_function');?>");
    });
    $('div#production_mtc_pdf_history').on('click', '.delete_mtc_pdf_production', function(){
        swal({
              title: "Are you sure?",
              text: "Once deleted, you will not be able to recover this MTC PDF!",
              icon: "warning",
              buttons:  ["Cancel", "Delete"],
              dangerMode: true, 
            })
        .then((willDelete) => {
            if (willDelete) {
                ajax_call_function({call_type: 'delete_mtc_pdf', pdf_key: $(this).attr('pdf_key')},'delete_mtc_pdf', "<?php echo base_url('production/ajax_function');?>");
            } else {
                swal({
                    title: "MTC Pdf is not deleted",
                    icon: "info",
                });
            }
        });
    });
    $('button.save_email_name').click(function(){
        ajax_call_function({call_type: 'save_email_name', id: $(this).attr('id'), mtc_email_name: $('textarea[name="mtc_email_name"]').val(), marking_email_name: $('textarea[name="marking_email_name"]').val()},'save_email_name');

    });
});
function get_tab_name() {
    if($('li.pending_order').hasClass('active_list')) {
        return 'pending_order';
    } else if($('li.on_hold_order').hasClass('active_list')) {
        return 'on_hold_order';
    } else if($('li.dispatch_order').hasClass('active_list')) {
        return 'dispatch_order';
    } else if($('li.ready_to_dispatch_order').hasClass('active_list')) {
        return 'ready_to_dispatch_order';
    } else if($('li.semi_ready').hasClass('active_list')) {
        return 'semi_ready';
    } else if($('li.order_cancelled').hasClass('active_list')) {
        return 'order_cancelled';
    }
    return '';
}
 
function get_main_tab_name() {
    
    $('th.all').hide();
    if($('li.documentation').hasClass('active_list')) {
        $('th.inspection').show();
        return 'documentation';
    } else if($('li.inspection').hasClass('active_list')) {
        $('th.documentation').show();
        return 'inspection';
    }
    return '';
}



function ajax_call_function(data, callType, url = "<?php echo base_url('quality/ajax_function'); ?>") {

    $.ajax({
        type: 'POST',
        data: data,
        url: url,
        dataType: 'JSON',
        success: function(res){
            if(res.status == 'successful') {
                $('.layer-white').hide();
                switch (callType) {
                    case 'save_quality_update_form':
                        setTimeout(function(){

                            toastr.info("Details Are Updated!!!"); 
                            setTimeout(function(){
                                location.reload();
                            }, 2000);
                        }, 1000);
                    break;
                    case 'change_main_tab':
                        $('tbody#quality_listing').html('').html(res.quality_list_body);
                    break;
                    case 'change_tab':
                        $('tbody#quality_listing').html('').html(res.quality_list_body);
                    break;
                    case 'search_filter':
                    case 'paggination_filter':
                        
                        $('div.quality_listing_search_filter').html('').html(res.search_filter_body);
                        $('tbody#quality_listing').html('').html(res.list_body);
                        $('div#quality_list_paggination').html('').html(res.paggination_filter_body);
                           Listing_js.init(); 
                        //$('input#delivery_date').val("<?php echo $this->session->userdata('search_filter_delivery_date');?>");
                    break;
                    case 'get_mtc_upload_history':
                        $('div#production_mtc_pdf_history').html('').html(res.mtc_upload_table);
                        $('div#production_marking_pdf_history').html('').html(res.marking_upload_table);
                        $('textarea[name="mtc_email_name"]').html('').html(res.mtc_email_name);
                        $('textarea[name="marking_email_name"]').html('').html(res.marking_email_name);
                        $('button.save_email_name').attr('id', res.id);
                        $('button.save_email_name').show();
                        autosize($('textarea[name="mtc_email_name"]'));
                        autosize($('textarea[name="marking_email_name"]'));
                    break;
                    case 'delete_mtc_pdf':
                        swal({
                            title: "MTC Pdf is Deleted successfully",
                            icon: "success",
                        });
                        ajax_call_function({call_type: 'get_mtc_upload_history'},'get_mtc_upload_history');
                    break;
                    default:
                    
                    break;  
                }
            }
        },
        beforeSend: function(response){
            $('.layer-white').show();
        }
    });
};

function set_reset_spinner(obj, set_unset_flag = true, spinner_color = 'kt-spinner--light') {

    if(set_unset_flag) {

        $(obj).addClass('kt-spinner');
        $(obj).addClass('kt-spinner--right');
        $(obj).addClass('kt-spinner--sm');
        $(obj).addClass(spinner_color); 
    } else{

        $(obj).removeClass('kt-spinner');
        $(obj).removeClass('kt-spinner--right');
        $(obj).removeClass('kt-spinner--sm');
        $(obj).removeClass(spinner_color);
    }

}
var Listing_js = function () {

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }
    
    // Private functions
    var demos = function () {   
        // enable clear button 
        $('#listing_date, #listing_delivery_date').datepicker({
            rtl: KTUtil.isRTL(),
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: true,
            templates: arrows,
            format: 'yyyy-mm-dd',
        });
        $('.listing_select_picker').selectpicker();

    };

    var daterangepickerInit = function() {
    
        if ($('#kt_dashboard_daterangepicker').length == 0) {
            return;
        }

        var picker = $('#kt_dashboard_daterangepicker');
        var start = moment().subtract(1, 'days');
        var end = moment().subtract(1, 'days');

        function cb(start, end, label) {
            var title = '';
            var range = '';
            if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D Y');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D Y');
            } else {                
                range = start.format('MMM D Y') + ' - ' + end.format('MMM D Y');
            }

            if(label == 'first_time') {
                $('#kt_dashboard_daterangepicker_title').html('Select');
                $('#kt_dashboard_daterangepicker_date').html('Date');
                $('input#delivery_date').val('');
            } else {

                $('#kt_dashboard_daterangepicker_title').html(title);
                $('#kt_dashboard_daterangepicker_date').html(range);
                $('input#delivery_date').val(range);
            }
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end, 'first_time');
    };

    var proformadaterangepickerInit = function() {
    
        if ($('#kt_proforma_daterangepicker').length == 0) {
            return;
        }

        var picker = $('#kt_proforma_daterangepicker');
        var start = moment().subtract(1, 'days');
        var end = moment().subtract(1, 'days');

        function cb(start, end, label) {
            var title = '';
            var range = '';
            if (label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D Y');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D Y');
            } else {                
                range = start.format('MMM D Y') + ' - ' + end.format('MMM D Y');
            }

            if(label == 'first_time') {

                $('#kt_proforma_daterangepicker_title').html('Select');
                $('#kt_proforma_daterangepicker_date').html('Date');
                $('input#proforma_date').val('');
            } else {

                $('#kt_proforma_daterangepicker_title').html(title);
                $('#kt_proforma_daterangepicker_date').html(range);
                $('input#proforma_date').val(range);
            }
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end, 'first_time');
    };
    var demo2 = function () {
        // set the dropzone container id
        var id = '#kt_dropzone_4';

        // set the preview element template
        var previewNode = $(id + " .dropzone-item");
        previewNode.id = "";
        var previewTemplate = previewNode.parent('.dropzone-items').html();
        previewNode.remove();

        var myDropzone4 = new Dropzone(id, { // Make the whole body a dropzone
            url: "<?php echo base_url('production/ajax_function');?>", // Set the url for your upload script location
            params: {'call_type': 'upload_mtc'},
            parallelUploads: 3,
            previewTemplate: previewTemplate,
            maxFilesize: 5, // Max filesize in MB
            autoQueue: false, // Make sure the files aren't queued until manually added
            previewsContainer: id + " .dropzone-items", // Define the container to display the previews
            clickable: id + " .dropzone-select" // Define the element that should be used as click trigger to select files.
        });

        myDropzone4.on("addedfile", function(file) {
            // Hookup the start button
            file.previewElement.querySelector(id + " .dropzone-start").onclick = function() { myDropzone4.enqueueFile(file); };
            $(document).find( id + ' .dropzone-item').css('display', '');
            $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'inline-block');
        });

        // Update the total progress bar
        myDropzone4.on("totaluploadprogress", function(progress) {
            $(this).find( id + " .progress-bar").css('width', progress + "%");
        });

        myDropzone4.on("sending", function(file) {
            // Show the total progress bar when upload starts
            $( id + " .progress-bar").css('opacity', '1');
            // And disable the start button
            file.previewElement.querySelector(id + " .dropzone-start").setAttribute("disabled", "disabled");
        });

        // Hide the total progress bar when nothing's uploading anymore
        myDropzone4.on("complete", function(progress) {
            var thisProgressBar = id + " .dz-complete";
            setTimeout(function(){
                $( thisProgressBar + " .progress-bar, " + thisProgressBar + " .progress, " + thisProgressBar + " .dropzone-start").css('opacity', '0');
            }, 300)
            toastr.success("MTC Pdf is Uploaded!!!.");
            ajax_call_function({call_type: 'get_mtc_upload_history'},'get_mtc_upload_history', "<?php echo base_url('production/ajax_function');?>");
        });

        // Setup the buttons for all transfers
        document.querySelector( id + " .dropzone-upload").onclick = function() {
            myDropzone4.enqueueFiles(myDropzone4.getFilesWithStatus(Dropzone.ADDED));
        };

        // Setup the button for remove all files
        document.querySelector(id + " .dropzone-remove-all").onclick = function() {
            $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
            myDropzone4.removeAllFiles(true);
        };

        // On all files completed upload
        myDropzone4.on("queuecomplete", function(progress){
            $( id + " .dropzone-upload").css('display', 'none');
        });

        // On all files removed
        myDropzone4.on("removedfile", function(file){
            if(myDropzone4.files.length < 1){
                $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
            }
        });
    }

    var marking_pdf = function () {
        // set the dropzone container id
        var id = '#kt_dropzone_marking_pdf';

        // set the preview element template
        var previewNode = $(id + " .dropzone-item");
        previewNode.id = "";
        var previewTemplate = previewNode.parent('.dropzone-items').html();
        previewNode.remove();

        var myDropzone4 = new Dropzone(id, { // Make the whole body a dropzone
            url: "<?php echo base_url('production/ajax_function');?>", // Set the url for your upload script location
            params: {'call_type': 'upload_marking'},
            parallelUploads: 3,
            previewTemplate: previewTemplate,
            maxFilesize: 5, // Max filesize in MB
            autoQueue: false, // Make sure the files aren't queued until manually added
            previewsContainer: id + " .dropzone-items", // Define the container to display the previews
            clickable: id + " .dropzone-select" // Define the element that should be used as click trigger to select files.
        });

        myDropzone4.on("addedfile", function(file) {
            // Hookup the start button
            file.previewElement.querySelector(id + " .dropzone-start").onclick = function() { myDropzone4.enqueueFile(file); };
            $(document).find( id + ' .dropzone-item').css('display', '');
            $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'inline-block');
        });

        // Update the total progress bar
        myDropzone4.on("totaluploadprogress", function(progress) {
            $(this).find( id + " .progress-bar").css('width', progress + "%");
        });

        myDropzone4.on("sending", function(file) {
            // Show the total progress bar when upload starts
            $( id + " .progress-bar").css('opacity', '1');
            // And disable the start button
            file.previewElement.querySelector(id + " .dropzone-start").setAttribute("disabled", "disabled");
        });

        // Hide the total progress bar when nothing's uploading anymore
        myDropzone4.on("complete", function(progress) {
            var thisProgressBar = id + " .dz-complete";
            setTimeout(function(){
                $( thisProgressBar + " .progress-bar, " + thisProgressBar + " .progress, " + thisProgressBar + " .dropzone-start").css('opacity', '0');
            }, 300)
            toastr.success("MTC Pdf is Uploaded!!!.");
            ajax_call_function({call_type: 'get_mtc_upload_history'},'get_mtc_upload_history', "<?php echo base_url('production/ajax_function');?>");
        });

        // Setup the buttons for all transfers
        document.querySelector( id + " .dropzone-upload").onclick = function() {
            myDropzone4.enqueueFiles(myDropzone4.getFilesWithStatus(Dropzone.ADDED));
        };

        // Setup the button for remove all files
        document.querySelector(id + " .dropzone-remove-all").onclick = function() {
            $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
            myDropzone4.removeAllFiles(true);
        };

        // On all files completed upload
        myDropzone4.on("queuecomplete", function(progress){
            $( id + " .dropzone-upload").css('display', 'none');
        });

        // On all files removed
        myDropzone4.on("removedfile", function(file){
            if(myDropzone4.files.length < 1){
                $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
            }
        });
    }

    return {
        // public functions
        init: function() {
            demos(); 
            daterangepickerInit(); 
            proformadaterangepickerInit(); 
            demo2();
            marking_pdf();
        }
    };
}();