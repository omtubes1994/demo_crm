<!--begin: Search Form -->
<form class="kt-form kt-form--fit kt-margin-b-20" id="quality_listing_search_filter_form">
	<div style="position: relative; width: 100%; padding-right: 10px; padding-left: 10px;">
		<div class="row kt-margin-b-20">
			<label class="col-lg-1 col-form-label form_name">WO #:</label>
			<div class="col-lg-2 form-group-sub">
				<input type="text" class="form-control" name="work_order_no" value="">
			</div>
		   	<label class="col-lg-1 col-form-label form_name">Client Name:</label>
			<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="client_name" multiple data-live-search="true">
					<?php foreach ($client_list as $client_details) { ?>
						<option value="<?php echo $client_details['client_id'];?>"
						<?php echo (in_array($client_details['client_id'], $search_filter['client_name']))?'selected':''?>>
							<?php echo $client_details['client_name'];?>
						</option>
					<?php } ?>
				</select>
			</div>
			<label class="col-lg-1 col-form-label form_name">Sales Person:</label>
			<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="assigned_to" multiple data-live-search="true">
					<?php foreach ($sales_person_details as $sales_person_id => $sales_person_name) { ?>
						<option value="<?php echo $sales_person_id;?>"
						<?php echo (in_array($sales_person_id, $search_filter['assigned_to']))?'selected':''?>>
							<?php echo $sales_person_name;?>
						</option>
					<?php } ?>
				</select>
			</div>
			<label class="col-lg-1 col-form-label form_name">Product Family:</label>
			<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="product_family" multiple data-live-search="true">
					<option value="piping" 
					<?php echo (in_array('piping', $search_filter['product_family']))?'selected':''?>>
						Piping
					</option>
					<option value="instrumentation"
					<?php echo (in_array('instrumentation', $search_filter['product_family']))?'selected':''?>>
						Instrumentation
					</option>
					<option value="precision"
					<?php echo (in_array('precision', $search_filter['product_family']))?'selected':''?>>
						Precision
					</option>
				</select>
			</div>
		</div>
		<div class="row kt-margin-b-20">
			<label class="col-lg-1 col-form-label form_name">Proforma #:</label>
			<div class="col-lg-2 form-group-sub">
				<input type="text" class="form-control" name="proforma_no" value="<?php echo $search_filter['proforma_no'][0];?>">
			</div>
			<label class="col-lg-1 col-form-label form_name">PO #:</label>
			<div class="col-lg-2 form-group-sub">
				<input type="text" class="form-control" name="order_no" value="<?php echo $search_filter['order_no'][0];?>">
			</div>
			<label class="col-lg-1 col-form-label form_name">proforma Date:</label>
			<div class="col-lg-2 form-group-sub">
				<div class="kt-portlet__head-toolbar">
					<input type="text" class="form-control" value="<?php echo $search_filter['proforma_date'][0];?>" name="proforma_date" id="proforma_date" hidden/>
                    <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
						<a href="javascript:void(0);" class="btn kt-subheader__btn-daterange" id="kt_proforma_daterangepicker" data-toggle="kt-tooltip" title="Select Destrack Date" data-placement="left" style="background: rgb(0 0 0 / 10%); display: inline-flex; height: 32px !important; padding-top: 0; padding-bottom: 0; margin-top: 0.25rem; margin-bottom: 0.25rem; margin-left: 0.25rem;">
							<span class="kt-subheader__btn-daterange-title" id="kt_proforma_daterangepicker_title" style="display: inline-block;color: #959cb6;font-weight: 500;">Today</span>&nbsp;
							<span class="kt-subheader__btn-daterange-date" id="kt_proforma_daterangepicker_date" style="display: inline-block;color: #5d78ff;margin-right: 0.75rem;font-weight: 500;">Aug 16</span>
							<i class="flaticon2-calendar-1" style="color: #5d78ff; font-size: 1rem !important;"></i>
						</a>
                    </ul>
                </div>
			</div>
			<label class="col-lg-1 col-form-label form_name">Vendor PO #:</label>
			<div class="col-lg-2 form-group-sub">
				<input type="text" class="form-control" name="vendor_po" value="<?php echo $search_filter['vendor_po'][0];?>">
			</div>
		</div>
		<div class="row kt-margin-b-20">	
			<label class="col-lg-1 col-form-label form_name">Delivery Date:</label>
			<div class="col-lg-2 form-group-sub">
				<div class="kt-portlet__head-toolbar">
					<input type="text" class="form-control" value="<?php  echo $search_filter['delivery_date'][0];?>" name="delivery_date" id="delivery_date" hidden/>
                    <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
						<a href="javascript:void(0);" class="btn kt-subheader__btn-daterange" id="kt_dashboard_daterangepicker" data-toggle="kt-tooltip" title="Select Destrack Date" data-placement="left" style="background: rgb(0 0 0 / 10%); display: inline-flex; height: 32px !important; padding-top: 0; padding-bottom: 0; margin-top: 0.25rem; margin-bottom: 0.25rem; margin-left: 0.25rem;">
							<span class="kt-subheader__btn-daterange-title" id="kt_dashboard_daterangepicker_title" style="display: inline-block;color: #959cb6;font-weight: 500;">Today</span>&nbsp;
							<span class="kt-subheader__btn-daterange-date" id="kt_dashboard_daterangepicker_date" style="display: inline-block;color: #5d78ff;margin-right: 0.75rem;font-weight: 500;">Aug 16</span>
							<i class="flaticon2-calendar-1" style="color: #5d78ff; font-size: 1rem !important;"></i>
						</a>
                    </ul>
                </div>
			</div>
			<label class="col-lg-1 col-form-label form_name">QC Clearance:</label>
			<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="qc_clearance" multiple data-live-search="true">
					<option value="Yes"
					<?php echo (in_array('Yes', $search_filter['qc_clearance']))?'selected':''?>>Yes</option>
					<option value="No"
					<?php echo (in_array('No', $search_filter['qc_clearance']))?'selected':''?>>No</option>
				</select>
			</div>
			<label class="col-lg-1 col-form-label form_name">Handled By:</label>
			<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="handled_by" multiple>
					<?php foreach ($handle_by_users as $handle_by_users_details) { ?>
						<?php $explode_name = explode(' ', $handle_by_users_details['name']); ?>
						<option value="<?php echo $explode_name[0];?>"
						<?php echo (in_array($explode_name[0], $search_filter['handled_by']))?'selected':''?>>
							<?php echo $explode_name[0];?>
						</option>
					<?php } ?>
				</select>
			</div>
			<?php if($this->session->userdata('production_tab_name') == 'pending_order') {?>
			<label class="col-lg-1 col-form-label form_name">Status:</label>
			<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="production_status" multiple>
					<?php foreach ($production_status as $production_key => $production_details) { ?>
						<option 
							value="<?php echo $production_key;?>"
							data-content="<span class='kt-badge kt-badge--<?php echo $production_details['color'];?> kt-badge--inline kt-badge--rounded kt-font-bolder'><?php echo $production_details['name'];?></span>"
							<?php echo (in_array($production_key, $search_filter['production_status']))?'selected':''?>>
								<?php echo $production_details['name'];?>
						</option>
					<?php } ?>
				</select>
			</div>
			<?php }?>
		    </div>
		    <div class=" all row kt-margin-b-20">
		    	<label class =" col-lg-1 col-form-label form_name">Documentation Assign By:</label>
		    	<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="documentation_marking_assign_by" multiple>
					<?php foreach ($user_quality_admin as $documentation_marking_assign_by_users_id => $documentation_marking_assign_by_users_name) {
					echo $documentation_marking_assign_by_users_name ; ?>
						<option value="<?php echo $documentation_marking_assign_by_users_id;?>"
						<?php echo $documentation_marking_assign_by_users_id, $search_filter['documentation_marking_assign_by']?'selected':''?>>
							<?php echo $documentation_marking_assign_by_users_name;?>
						</option>
					<?php } ?>
				</select>
			    </div>
			    <label class =" col-lg-1 col-form-label form_name">Documentation Assign to:</label>
		    	<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="documentation_marking_assign_to" multiple>
					<?php foreach ($user_quality_user as $documentation_marking_assign_to_users_id => $documentation_marking_assign_to_users_name) {
					echo $documentation_marking_assign_to_users_name ; ?>
						<option value="<?php echo $documentation_marking_assign_to_users_id;?>"
						<?php echo $documentation_marking_assign_to_users_id, $search_filter['documentation_marking_assign_to']?'selected':''?>>
							<?php echo $documentation_marking_assign_to_users_name;?>
						</option>
					<?php } ?>
				</select>
				</div>
				<label class =" col-lg-1 col-form-label form_name">MTC Assign By:</label>
		    	<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="documentation_mtc_assign_by" multiple>
					<?php foreach ($user_quality_admin as $documentation_mtc_assign_by_users_id => $documentation_mtc_assign_by_users_name) {
					echo $documentation_mtc_assign_by_users_name ; ?>
						<option value="<?php echo $documentation_mtc_assign_by_users_id;?>"
						<?php echo $documentation_mtc_assign_by_users_id, $search_filter['documentation_mtc_assign_by']?'selected':''?>>
							<?php echo $documentation_mtc_assign_by_users_name;?>
						</option>
					<?php } ?>
				</select>
				</div>
				<label class =" col-lg-1 col-form-label form_name">MTC Assign To:</label>
		    	<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="documentation_mtc_assign_to" multiple>
					<?php foreach ($user_quality_user as $documentation_mtc_assign_to_users_id => $documentation_mtc_assign_to_users_name) {
					echo $documentation_mtc_assign_to_users_name ; ?>
						<option value="<?php echo $documentation_mtc_assign_to_users_id;?>"
						<?php echo $documentation_mtc_assign_to_users_id, $search_filter['documentation_mtc_assign_to']?'selected':''?>>
							<?php echo $documentation_mtc_assign_to_users_name;?>
						</option>
					<?php } ?>
				</select>
			    </div>
			</div>
			 <div class="all row kt-margin-b-20">
			    <label class =" col-lg-1 col-form-label form_name">Checked By:</label>
		    	<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="documentation_mtc_checked_by" multiple>
					<?php foreach ($user_quality_admin as $documentation_mtc_checked_by_users_id => $documentation_mtc_checked_by_users_name) {
					echo $documentation_mtc_checked_by_users_name ; ?>
						<option value="<?php echo $documentation_mtc_checked_by_users_id;?>"
						<?php echo $documentation_mtc_checked_by_users_id, $search_filter['documentation_mtc_checked_by']?'selected':''?>>
							<?php echo $documentation_mtc_checked_by_users_name;?>
						</option>
					<?php } ?>
				</select>
				</div>
				<label class =" col-lg-1 col-form-label form_name">Soft copy:</label>
		    	<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="soft_copy" multiple>
					<option value="Yes"
					<?php echo (in_array('Yes', $search_filter['soft_copy']))?'selected':''?>>Yes</option>
					<option value="No"
					<?php echo (in_array('No', $search_filter['soft_copy']))?'selected':''?>>No</option>	
				</select>
				</div>
				<label class =" col-lg-1 col-form-label form_name">Hard copy:</label>
		    	<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="hard_copy" multiple>
					<option value="Yes"
					<?php echo (in_array('Yes', $search_filter['hard_copy']))?'selected':''?>>Yes</option>
					<option value="No"
					<?php echo (in_array('No', $search_filter['hard_copy']))?'selected':''?>>No</option>	
				</select>
			    </div>
			</div>
			<div class=" all row kt-margin-b-20">
		    	<label class =" col-lg-1 col-form-label form_name">Dimension:</label>
		    	<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="inspection_dimension" multiple>
					<option value="Yes"
					<?php echo (in_array('Yes', $search_filter['inspection_dimension']))?'selected':''?>>Yes</option>
					<option value="No"
					<?php echo (in_array('No', $search_filter['inspection_dimension']))?'selected':''?>>No</option>	
				</select>
			    </div>
			    <label class =" col-lg-1 col-form-label form_name">photo:</label>
		    	<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="inspection_photo" multiple>
					<option value="Yes"
					<?php echo (in_array('Yes', $search_filter['inspection_photo']))?'selected':''?>>Yes</option>
					<option value="No"
					<?php echo (in_array('No', $search_filter['inspection_photo']))?'selected':''?>>No</option>	
				</select>
				</div>
				<label class =" col-lg-1 col-form-label form_name">Inspection Assign By:</label>
		    	<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="inspection_assign_by" multiple>
					<?php foreach ($user_quality_admin as $inspection_assign_by_users_id => $inspection_assign_by_users_name) {
					echo $inspection_assign_by_users_name ; ?>
						<option value="<?php echo $inspection_assign_by_users_id;?>"
						<?php echo $inspection_assign_by_users_id, $search_filter['inspection_assign_by']?'selected':''?>>
							<?php echo $inspection_assign_by_users_name;?>
						</option>
					<?php } ?>
				</select>
				</div>
				<label class =" col-lg-1 col-form-label form_name">Inspection Assign To:</label>
		    	<div class="col-lg-2 form-group-sub">
				<select class="form-control listing_select_picker" name="inspection_assign_to" multiple>
					<?php foreach ($user_quality_users as $inspection_assign_to_users_id => $inspection_assign_to_users_name) {
					echo $inspection_assign_to_users_name ; ?>
						<option value="<?php echo $inspection_assign_to_users_id;?>"
						<?php echo $inspection_assign_to_users_id, $search_filter['inspection_assign_to']?'selected':''?>>
							<?php echo $inspection_assign_to_users_name;?>
						</option>
					<?php } ?>
				</select>
				</div>
			</div>
			<div class="col-lg-2">
		        <button class="btn btn-primary btn-brand--icon quality_listing_search_filter_form_submit" type="reset">
		            <span>
		                <i class="la la-search"></i>
		                <span>Search</span>
		            </span>
		        </button>
		        &nbsp;&nbsp;
		        <button class="btn btn-secondary btn-secondary--icon quality_listing_search_filter_form_reset" type="reset">
		            <span>
		                <i class="la la-close"></i>
		                <span>Reset</span>
		            </span>
		        </button>
		    </div>	
		</div>
	</div>	
</form>
