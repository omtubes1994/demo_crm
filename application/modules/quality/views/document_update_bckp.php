<style type="text/css">
	.form_name{
	    color: #767676 !important;
	    font-size: 14px !important;
	    font-family: 'latomedium' !important;
	    padding: 10px 10px !important; 
	    font-weight: bold !important;
	}
	.kt-badge--gainsboro{
		background-color: gainsboro;
	}
</style>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col">
			<div class="alert alert-light alert-elevate fade show" role="alert">
				<div class="alert-icon"><i class="flaticon-edit kt-font-brand"></i></div>
				<div class="alert-text">
					<h2 class="kt-font-info">Quality Details</h2>
				</div>
				<div>
					<button type="button" class="btn btn-primary btn-wide save_quality_documentation_data">Save Details</button>
				</div>
			</div>
		</div>
	</div>
	<!--begin::Portlet-->
	<div class="kt-portlet" style="background: #F5F5F5;">
		<div class="kt-portlet__body">
			<div class="kt-form__section kt-form__section--first">
				<!--begin::Form-->
				<form class="kt-form" id="quality_documentation_form">
					<input type="text" name="id" value="<?php echo $this->uri->segment('3');?>" hidden>
					<div class="form-group row">
						<label class="col-lg-1 col-form-label form_name">Work Order No:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" value="<?php echo $quality_information['work_order_no'];?>" readonly>
						</div>
						<label class="col-lg-1 col-form-label form_name">Marking Assing BY:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control handled_by_select_picker" name="documentation_marking_assign_by" multiple>
								<option value="">Select</option>
								<?php foreach($user_quality_admin as $quality_admin_details) {?>
									<option 
									value="<?php echo $quality_admin_details['user_id'];?>" 
									<?php 
										echo ($quality_information['documentation_marking_assign_by'] == $quality_admin_details['user_id']) ? 'selected': '';
									?>>
									<?php echo ucfirst(strtolower($quality_admin_details['name']));?>
									</option>
								<?php } ?>
							</select>
						</div>
						<label class="col-lg-1 col-form-label form_name">Marking Assing to:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control handled_by_select_picker" name="documentation_marking_assign_to" multiple>
								<option value="">Select</option>
								<?php foreach($user_quality_user as $quality_user_details) {?>
									<option 
									value="<?php echo $quality_user_details['user_id'];?>" 
									<?php 
										echo ($quality_information['documentation_marking_assign_to'] == $quality_user_details['user_id']) ? 'selected': '';
									?>>
									<?php echo ucfirst(strtolower($quality_user_details['name']));?>
									</option>
								<?php } ?>
							</select>
						</div>
						<label class="col-lg-1 col-form-label form_name">Marking Checked BY:</label>
						<div class="col-lg-2 form-group-sub">
								<select class="form-control handled_by_select_picker" name="documentation_marking_checked_by" multiple>
								<option value="">Select</option>
								<?php foreach(array_merge($user_quality_admin, $user_quality_user) as $quality_admin_details) {?>
									<option 
									value="<?php echo $quality_admin_details['user_id'];?>" 
									<?php 
										echo ($quality_information['documentation_marking_checked_by'] == $quality_admin_details['user_id']) ? 'selected': '';
									?>>
									<?php echo ucfirst(strtolower($quality_admin_details['name']));?>
									</option>
								<?php } ?>
								</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-1 col-form-label form_name">Marking Comments:</label>
						<div class="col-lg-5 form-group-sub">
							<textarea class="form-control" id="kt_autosize_2" name="documentation_marking_comment" rows="2" style="overflow: hidden; overflow-wrap: break-word; resize: none; height: 54px;"></textarea>
						</div>
						<label class="col-lg-1 col-form-label form_name">MTC Assing BY:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control handled_by_select_picker" name="documentation_mtc_assign_by" multiple>
								<option value="">Select</option>
								<?php foreach($user_quality_admin as $quality_admin_details) {?>
									<option 
									value="<?php echo $quality_admin_details['user_id'];?>" 
									<?php 
										echo ($quality_information['documentation_mtc_assign_by'] == $quality_admin_details['user_id']) ? 'selected': '';
									?>>
									<?php echo ucfirst(strtolower($quality_admin_details['name']));?>
									</option>
								<?php } ?>
							</select>
						</div>
						<label class="col-lg-1 col-form-label form_name">MTC Assing to:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control handled_by_select_picker" name="documentation_mtc_assign_to" multiple>
								<option value="">Select</option>
								<?php foreach($user_quality_user as $quality_user_details) {?>
									<option 
									value="<?php echo $quality_user_details['user_id'];?>" 
									<?php 
										echo ($quality_information['documentation_mtc_assign_to'] == $quality_user_details['user_id']) ? 'selected': '';
									?>>
									<?php echo ucfirst(strtolower($quality_user_details['name']));?>
									</option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group row">	
						<label class="col-lg-1 col-form-label form_name">MTC Checked BY:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control handled_by_select_picker" name="documentation_mtc_checked_by" multiple>
							<option value="">Select</option>
							<?php foreach($user_quality_admin as $quality_admin_details) {?>
								<option 
								value="<?php echo $quality_admin_details['user_id'];?>" 
								<?php 
									echo ($quality_information['documentation_mtc_checked_by'] == $quality_admin_details['user_id']) ? 'selected': '';
								?>>
								<?php echo ucfirst(strtolower($quality_admin_details['name']));?>
								</option>
							<?php } ?>
							</select>
						</div>
						<label class="col-lg-1 col-form-label form_name">MTC Comments:</label>
						<div class="col-lg-5 form-group-sub">
							<textarea class="form-control" id="kt_autosize_2" name="documentation_mtc_comment" rows="2" style="overflow: hidden; overflow-wrap: break-word; resize: none; height: 54px;"></textarea>
						</div>
			        	<label class="col-lg-1 col-form-label form_name">soft_copy:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control" name="soft_copy">
								<option value="">Select </option>
								<option value="Yes"
								<?php echo ($quality_information['soft_copy'] == 'Yes')?'selected':''?>>
									Yes
								</option>
								<option value="No"
								<?php echo ($quality_information['soft_copy'] == 'No')?'selected':''?>>
									No
								</option>
							</select>
						</div>
					</div>
					<div class="form-group row">	
						<label class="col-lg-1 col-form-label form_name">hard_copy:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control" name="hard_copy">
								<option value="">Select</option>
								<option value="Yes"
								<?php echo ($quality_information['hard_copy'] == 'Yes')?'selected':''?>>
									Yes
								</option>
								<option value="No"
								<?php echo ($quality_information['hard_copy'] == 'No')?'selected':''?>>
									No
								</option>
							</select>
						</div>
						<?php if(in_array($this->session->userdata('user_id'), array(59, 33, 35))) {?>
						<label class="col-lg-1 col-form-label form_name">Current Status:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control handled_by_select_picker" name="production_status">
								<option value="">Select Current Status</option>
								<?php foreach ($production_status as $production_key => $production_details) { ?>
									<option 
										value="<?php echo $production_key;?>"
										data-content="<span class='kt-badge kt-badge--<?php echo $production_details['color'];?> kt-badge--inline kt-badge--rounded kt-font-bolder'><?php echo $production_details['name'];?></span>"
										<?php echo ($quality_information['production_status']== $production_key)?'selected':''?>>
											<?php echo $production_details['name'];?>
									</option>
								<?php }?>
							</select>
						</div>
						<?php } ?>
					</div>
				</form>
    
				<!--end::Form-->
			</div>	
		</div>
	</div>
	
	<!--end::Portlet-->
</div>

<!-- end:: Content -->