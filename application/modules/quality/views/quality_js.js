jQuery(document).ready(function() {
	
    $('li.main_tab_name_click').click(function(){

    	ajax_call_function({call_type: 'change_main_tab', tab_name: $(this).attr('tab-name')},'change_main_tab');
    });
});

function get_main_tab_name() {
    if($('li.documentation').hasClass('active_list')) {
        return 'documentation';
    } else if($('li.inspection').hasClass('active_list')) {
        return 'inspection';
    }
    return '';
}

function ajax_call_function(data, callType, url = "<?php echo base_url('invoices/ajax_function'); ?>") {

	$.ajax({
		type: 'POST',
		data: data,
		url: url,
		dataType: 'JSON',
		success: function(res){
			if(res.status == 'successful') {
				switch (callType) {
					case 'change_main_tab':
						$('tbody#quality_listing').html('').html(res.quality_list_body);
					break;
					
					default:
					
					break;	
				}
			}
		},
		beforeSend: function(response){
			
		}
	});
};

function set_reset_spinner(obj, set_unset_flag = true, spinner_color = 'kt-spinner--light') {

	if(set_unset_flag) {

		$(obj).addClass('kt-spinner');
    	$(obj).addClass('kt-spinner--right');
    	$(obj).addClass('kt-spinner--sm');
    	$(obj).addClass(spinner_color);	
	} else{

		$(obj).removeClass('kt-spinner');
    	$(obj).removeClass('kt-spinner--right');
    	$(obj).removeClass('kt-spinner--sm');
    	$(obj).removeClass(spinner_color);
	}
}