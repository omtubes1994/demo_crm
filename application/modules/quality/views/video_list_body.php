<div class="row">
   <div class="col-xl-12">
       <div class="kt-portlet " style="height: 900px;">
           <div class="kt-portlet__head">
               <div class="kt-portlet__head-label">
                   <h3 class="kt-portlet__head-title">VIDEO SECTION</h3>
               </div>
           </div>
          <form class="kt-form kt-form--label-right" id="employee_update_account_information">
               <div class="kt-portlet__body">
                <div class="kt-scroll ps ps--active-y" data-scroll="true" style="height:auto ; overflow: hidden;">
                   <div class="row">
                     <?php if(!empty($video_details)){
                                foreach($video_details as $single_video_data){
                                    if(in_array($single_video_data['id'],$video_access_details)){
                                ?>
                        <div class="col-xl-6">
                            <!--Begin::Portlet-->
                            <div class="kt-portlet kt-portlet--height-fluid">
                                <div class="kt-portlet__body">

                                    <!--begin::Widget -->
                                    <div class="kt-widget kt-widget--user-profile-2">
                                        
                                        <div class="kt-widget__body">
                                            <div class="col-md-12">
                                                <div class="embed-responsive embed-responsive-16by9 ">
                                                    <iframe   width="514" height="279" class="play_video" id="myVideo" style="border-radius: 9px;" src="<?php echo $single_video_data['video_url'].'?modestbranding=1&start=10&controls=0&rel=0&disablekb=0'?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""
                                                    	sandbox="allow-forms allow-scripts allow-pointer-lock allow-same-origin allow-top-navigation"></iframe>
                                                </div>
                                            </div>
                                            <p>Playback position: <span id="demo"></span></p>
                                        </div>
                                    </div>

                                    <!--end::Widget -->
                                </div>
                            </div>

                            <!--End::Portlet-->
                        </div>
                        
                    <?php } } } ?>
                    </div>
                </div>
               </div>
           </form>
       </div>
   </div>
	</div>
<div id="hr_edit_listing_table_loader" class="layer-white" style="display:none;">
    <div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
</div>