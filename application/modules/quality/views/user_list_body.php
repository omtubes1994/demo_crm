<?php 
	if(!empty($user_list)) {
		foreach ($user_list as $user_key => $user_list_value) {
?>	
			<tr>
				<td style="">
			        <span><?php echo $user_key+1;?></span>
			    </td>
			    <td>
			       <a href="javascript:void(0)" class="chat_transcript_on_click" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><?php echo ucwords(strtolower($user_list_value['name'])); ?></a>
			    </td>
			    <td class="user_single_view1" style=""><span><?php echo (!empty($role[$user_list_value['role']])) ? $role[$user_list_value['role']] : 'No Role Present'; ?></span></td>
			    <td class="" style="">
			        <span><?php echo $user_list_value['email']; ?></span>
				</td>                        
			    <td class="user_single_view1" style="">
			        <span><?php echo $user_list_value['mobile']; ?></span>
			    </td>
				<!-- <td class="user_single_view_hide">
					<span><?php echo 8; ?></span>
			        </td> -->
			    <td>
			        <abbr class="single_user_details_view" user_id="<?php echo $user_list_value['user_id'];?>">
			            <a href="javascript:void(0)" class="chat_transcript_on_click" data-toggle="tooltip" data-placement="top" title="Edit user details" data-original-title="View"></a>
			        </abbr>
			    </td>
			</tr>
<?php
		}
	}
	?>			