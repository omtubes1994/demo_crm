<tr class="subject" tabindex="0">
    <th style="width: calc(100%/5); text-align: center;">Heat no</th>
    <th style="width: calc(100%/5); text-align: center;">Client Name</th>
    <th style="width: calc(100%/5); text-align: center;">Product</th>
    <th style="width: calc(100%/5); text-align: center;">Material</th>
</tr>
<tr class="search_tr">
	<th style="width: calc(100%/5); text-align: center;">
		<input type="text" class="form-control" id="heat_number" placeholder="Heat number" style="border-radius: 10px; width: 206px; padding: 0px 50px; text-align: center;">
	</th>
	<th style="width: calc(100%/5); text-align: center;">
		<input type="text" class="form-control" id="client_name" placeholder="Client Name" style="border-radius: 10px; width: 206px; padding: 0px 50px; text-align: center;">
	</th>
    <th style="width: calc(100%/5); text-align: center;">
        <select class="form-control" id="product" name="product" style="border-radius: 10px; width: 206px; padding: 0px 30px; text-align: center;">
            <option value="">Select Product</option>
            <?php 
            	foreach ($product as $product_key => $product_value) {
            		echo "<option value='".$product_key."'>".$product_value."</option>";
            	}
            ?>
        </select>
    </th>
    <th style="width: calc(100%/5); text-align: center;">
        <select class="form-control" id="material" name="material" style="border-radius: 10px; width: 206px; padding: 0px 30px; text-align: center;">
            <option value="">Select Material</option>
            <?php 
            	foreach ($material as $material_key => $material_value) {
            		echo "<option value='".$material_key."'>".$material_value."</option>";
            	}
            ?>
        </select>
    </th>
    <th style="width: calc(100%/5); text-align: center;">
        <input type="button" class="btn btn-success" id="heat_search_filter" value="Apply" style="border-radius: 10px; width: 45%;">
        <input type="button" class="btn btn-warning" id="heat_reset_filter" value="Reset" style="border-radius: 10px; width: 45%;">
    </th>
</tr>
<?php 
if(!empty($heat_details)) {
	foreach ($heat_details as $heat_details_key => $heat_details_value) {
?>
		<tr>
		    <td style="text-align: center;">
		    	<span>
		        	<i style="cursor: pointer;" data-value="Public" class=""><?php echo $heat_details_key+1;?></i>
		        	<abbr>
			            <em> Heat No: 
			              	<em class="close_activity_status">
			              		<?php echo $heat_details_value['heat_no'];?>
			          		</em>
			      		</em>
			      	</abbr>	
		    	</span>
		    </td>
		    <td><?php echo $heat_details_value['client_name'];?></td>
		    <td><?php echo $product[$heat_details_value['product_id']];?></td>
		    <td><?php echo $material[$heat_details_value['material_id']];?></td>
		</tr>
<?php 
	}
}	 
?>