<?php foreach ($production_list as $production_list_key => $single_production_details) {
	
	if(empty($single_production_details['marking_pdf']) || empty($single_production_details['mtc_pdf'])){

		// continue;
	}
	$view_count = $background = $marking = $mtc = '';
	if($single_production_details['view_count'] == 0){
		
		$view_count = 'new';
	}
	if(!empty($single_production_details['marking_pdf'])){

		$marking = '<span class="kt-badge kt-badge--primary kt-badge--md kt-badge--rounded" style="padding:5px;">Marking</span>';
	}
	if(!empty($single_production_details['mtc_pdf'])){

		$mtc = '<span class="kt-badge kt-badge--success kt-badge--md kt-badge--rounded">MTC</span>';
	}	
	
?>
<tr class="<?php echo $view_count, $background;?>">
	<td class="first_div" style="width:30px; padding:0px 0px 0px 15px;">
		
		<?php echo $production_list_key+1;?>		
	</td>
	<td style="width:30px; padding:0px 5px 0px 5px;">
		<?php echo $single_production_details['work_order_no'];?>
		<?php if($single_production_details['pdf_button'] && $this->session->userdata('production_access')['production_list_work_order_sheet_view_access']){?>
		<a href="<?php echo base_url('production/pdf/'.$single_production_details['work_order_no']);?>" target="_blank">
			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--dark kt-svg-icon--md">
			    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
			        <rect x="0" y="0" width="24" height="24"/>
			        <path d="M3,12 C3,12 5.45454545,6 12,6 C16.9090909,6 21,12 21,12 C21,12 16.9090909,18 12,18 C5.45454545,18 3,12 3,12 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
			        <path d="M12,15 C10.3431458,15 9,13.6568542 9,12 C9,10.3431458 10.3431458,9 12,9 C13.6568542,9 15,10.3431458 15,12 C15,13.6568542 13.6568542,15 12,15 Z" fill="#000000" opacity="0.3"/>
			    </g>
			</svg>
		</a>
		<?php } ?>
		<?php echo $marking,"<hr style='margin-top: 5px; margin-bottom: 5px;'>", $mtc; ?>
	</td>
	<td class="kt-align-right" style="width:100px; padding:0px 10px 0px 10px;">
		<em class="kt-align-center kt-font-bolder kt-font-<?php echo $single_production_details['production_status_name']['color'];?>" style="width: 200px;padding: 0px 0px 0px 0px;">
    		<?php echo $single_production_details['production_status_name']['name'];?>
		</em>
		<hr>
		<?php if(in_array($this->session->userdata('role'), array(1, 11, 17))){ ?>
			<a class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View Proforma" href="<?php echo base_url('pdf_management/proforma_pdf/'.$single_production_details['quotation_mst_id']);?>" style="color: #ACACAC;" target="_blank">
    		<i class="la la-eye kt-font-bolder" style="color: #ACACAC;"></i>
	    </a>
	    <a href="<?php echo base_url('quality/update_'.$this->session->userdata('main_tab_name').'/'.$single_production_details['id']);?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit" target="_blank">
	        <i class="la la-edit kt-font-bolder" style="color: #ACACAC;"></i>
	    </a>
	    <?php if(!empty($single_production_details['id'])) {?>
	    <button class="btn btn-sm btn-clean btn-icon btn-icon-md delete_quality_list_data kt-hidden" title="Delete" quality_id="<?php echo $single_production_details['id'];?>">
	    	<i class="la la-trash kt-font-bolder" style="color: #ACACAC;"></i>
	    </button>
	    <?php } ?>
    	<a class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View Purchase Order" href="<?php echo base_url('assets/purchase_orders/'.$single_production_details['purchase_order']);?>"  style="color: #ACACAC;" target="_blank">
    		<i class="fa fa-file-pdf-o kt-font-bolder" style="color: #ACACAC;">
    			<?php if(strtotime($single_production_details['po_add_time']) > strtotime(date('y-m-d', strtotime('-2 days')))){?>
    				<span class="kt-badge kt-badge--unified-success  kt-badge--inline kt-badge--bolder">new</span>
    			<?php } ?>
    		</i>
    	</a>
    	<a class="btn btn-sm btn-clean btn-icon btn-icon-md add_query kt-hidden" title="Add Query" href="javascript:;" data-toggle="modal" data-target="#production_query" quotation_for="<?php echo $single_production_details['quotation_mst_id'];?>" style="color: #ACACAC;">
    		<i class="fa fa-question kt-font-bolder" style="color: #ACACAC;"></i>
    	</a>
		<?php if($this->session->userdata('quality_access')['quality_mtc_and_marking_upload_access']){?>
			<a class="btn btn-sm btn-clean btn-icon btn-icon-md mtc_upload" title="MTC Upload" href="javascript:;" data-toggle="modal" data-target="#mtc_upload" production_id="<?php echo $single_production_details['id'];?>" style="color: #ACACAC;">
				<i class="fa fa-upload kt-font-bolder" style="color: #ACACAC;"></i>
			</a>
		<?php }?>
    	<?php } elseif ($this->session->userdata('main_tab_name') == "documentation") { ?>
    		<a href="<?php echo base_url('quality/update_'.$this->session->userdata('main_tab_name').'/'.$single_production_details['id']);?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit" target="_blank">
		        <i class="la la-edit kt-font-bolder" style="color: #ACACAC;"></i>
		    </a>
    		<a class="btn btn-sm btn-clean btn-icon btn-icon-md add_query" title="Add Query" href="javascript:;" data-toggle="modal" data-target="#production_query" quotation_for="<?php echo $single_production_details['quotation_mst_id'];?>" style="color: #ACACAC;">
	    		<i class="fa fa-question kt-font-bolder" style="color: #ACACAC;"></i>
	    	</a>
	    	<a class="btn btn-sm btn-clean btn-icon btn-icon-md mtc_upload" title="MTC Upload" href="javascript:;" data-toggle="modal" data-target="#mtc_upload" production_id="<?php echo $single_production_details['id'];?>" style="color: #ACACAC;">
	    		<i class="fa fa-upload kt-font-bolder" style="color: #ACACAC;"></i> 
	    	</a>
    	<?php } ?>
	</td>
	<td style="width:300px; padding:0px 0px 0px 15px;">
		<span>
	        <abbr>
				<?php if($this->session->userdata('quality_access')['client_name_access']){?>
					<em class="kt-font-bolder"> Client Name:
						<i><?php echo $single_production_details['client_details']['name'];?></i>
					</em>
				<?php }?>
	            <em class="kt-font-bolder kt-hidden"> Sales Person: 
	            	<i class="">
	            		<?php echo $users_details[$single_production_details['assigned_to']];?>
	        		</i>
	        	</em>
			</abbr>
		</span>
		<span>
	        <abbr>
				<em class="kt-font-bolder"> Product Family:
					<i class=""><?php echo $single_production_details['product_family'];?></i>
				</em>
			</abbr>
		</span>
	</td>
	<td style="width:230px; padding:0px 0px 0px 15px;">
		<span>    
	        <abbr>
	            <em class="kt-font-bolder"> Proforma #: 
	              	<i><?php echo $single_production_details['proforma_no'];?></i>
	      		</em>
	      		<em class="kt-font-bolder"> PO #: 
	              	<i><?php echo $single_production_details['order_no'];?></i>
	      		</em>
	            <em class="kt-font-bolder"> Proforma Date: 
	            	<i>
	            		<?php 
	            			if(!empty($single_production_details['confirmed_on'])) {
	            				echo date('F j, Y',strtotime($single_production_details['confirmed_on']));
	            			} else {
	            				echo "Proforma Date Not Found";
	            			}
	            		?>
	        		</i>
	        	</em> 
	    	</abbr>
		</span>
	</td>
	<td style="width:250px; padding:0px 0px 0px 15px;">
		<span>
	        <abbr>
	            <em class="kt-font-bolder"> Delivery Date: 
	            	<i>
	            		<?php 
	            			if(!empty($single_production_details['delivery_date'])) {
	            				echo date('F j, Y',strtotime($single_production_details['delivery_date']));
	            			} else {
	            				echo "Delivery Date Not Found";
	            			}
	            		?>
	        		</i>
	        	</em>
				<?php if(in_array($this->session->userdata('role'), array(1, 8, 11, 17))){ ?>
	            <em class="kt-font-bolder kt-hidden"> Vendor PO: 
	              	<i><?php echo $single_production_details['vendor_po'];?></i>
	      		</em>
	        	<em class="kt-font-bolder kt-hidden"> MTC Sent: 
	              	<i class=""><?php echo $single_production_details['mtc_sent'];?></i>
	      		</em> 
				<?php } ?>
	    	</abbr>
		</span>
	</td>
	<td style="width:250px; padding:0px 0px 0px 15px;">
		<span>
	        <abbr>
				<?php if(in_array($this->session->userdata('role'), array(1, 8, 11, 17))){ ?>
	            <em class="kt-font-bolder"> QC Clearance: 
	              	<i><?php echo $single_production_details['qc_clearance'];?></i>
	      		</em>
	      		<em class="kt-font-bolder"> Latest Update: 
	              	<i>
						<?php if(strlen($single_production_details['last_latest_update']) > 50) {
							echo substr($single_production_details['last_latest_update'], 0, 50), '...';
						} else {
							echo $single_production_details['last_latest_update'];
						} ?>
              		</i>
	      		</em>
	            <em class="kt-font-bolder kt-hidden"> QC Comments: 
	            	<i>
	            		<?php
	            			if(strlen($single_production_details['special_comment']) > 50) {

	            				echo substr($single_production_details['special_comment'], 0, 50), '...';

	            			} else {
		
	            				echo $single_production_details['special_comment'];	

	            			}
	            		?>
	        		</i>
	        	</em> 
				<?php }else if($this->session->userdata('main_tab_name') == 'inspection'){ ?>
					<em class="kt-font-bolder"> QC Clearance: 
		              	<i><?php echo $single_production_details['qc_clearance'];?></i>
		      		</em>
				<?php } ?>
	    	</abbr>
		</span>
	</td>
	<?php if($this->session->userdata('main_tab_name') != 'inspection') {?>
		<td style="width:230px; padding:0px 0px 0px 15px;">
			<span>    
		        <abbr>
		            <em class="kt-font-bolder kt-hidden"> Assign By: 
		              	<i><?php echo $users_details[$single_production_details['documentation_marking_assign_by']];?></i>
		      		</em>
		      		<em class="kt-font-bolder"> Assign to: 
		              	<i><?php echo $users_details[$single_production_details['documentation_marking_assign_to']];?></i>
		      		</em>
		      		<em class="kt-font-bolder"> Status:
		              	<i><?php echo $single_production_details['documentation_marking_status'];?></i>
		      		</em>
		      		<em class="kt-font-bolder"> Checked By: 
		              	<i><?php echo $users_details[$single_production_details['documentation_marking_checked_by']];?></i>
		      		</em>
		    	</abbr>
			</span>
		</td>
		<td style="width:230px; padding:0px 0px 0px 15px;">
			<span>    
		        <abbr>
		      		<em class="kt-font-bolder"> Comments: 
		              	<i><?php echo $single_production_details['documentation_marking_comment'];?></i>
		      		</em>
		    	</abbr>
			</span>
		</td>
		<td style="width:230px; padding:0px 0px 0px 15px;">
			<span>    
		        <abbr>
		            <em class="kt-font-bolder kt-hidden"> Assign By: 
		              	<i><?php echo $users_details[$single_production_details['documentation_mtc_assign_by']];?></i>
		      		</em>
		      		<em class="kt-font-bolder"> Assign to: 
		              	<i><?php echo $users_details[$single_production_details['documentation_mtc_assign_to']];?></i>
		      		</em>
		      		<em class="kt-font-bolder"> Status:
		              	<i><?php echo $single_production_details['documentation_mtc_status'];?></i>
		      		</em>
		      		<em class="kt-font-bolder"> Checked By: 
		              	<i><?php echo $users_details[$single_production_details['documentation_mtc_checked_by']];?></i>
		      		</em>
		    	</abbr>
			</span>
		</td>
		<td style="width:230px; padding:0px 0px 0px 15px;">
			<span>    
		        <abbr>
		      		<em class="kt-font-bolder"> Comments: 
		              	<i><?php echo $single_production_details['documentation_mtc_comment'];?></i>
		      		</em>
		    	</abbr>
			</span>
		</td>
		<td style="width:230px; padding:0px 0px 0px 15px;">
			<span>    
		        <abbr>
		            <em class="kt-font-bolder"> Soft Copy: 
		              	<i><?php echo $single_production_details['soft_copy'];?></i>
		      		</em>
		      		<em class="kt-font-bolder"> Hard Copy: 
		              	<i><?php echo $single_production_details['hard_copy'];?></i>
		      		</em>
		    	</abbr>
			</span>
		</td>
	<?php } else if($this->session->userdata('main_tab_name') != 'documentation') {?>
		<td style="width:230px; padding:0px 0px 0px 15px;">
			<span>    
		        <abbr>
		            <em class="kt-font-bolder"> Assign By: 
		              	<i><?php echo $users_details[$single_production_details['inspection_assign_by']];?></i>
		      		</em>
		      		<em class="kt-font-bolder"> Assign to: 
		              	<i><?php echo $users_details[$single_production_details['inspection_assign_to']];?></i>
		      		</em>
		    	</abbr>
			</span>
		</td>
		<td style="width:230px; padding:0px 0px 0px 15px;">
			<span>    
		        <abbr>
		            <em class="kt-font-bolder"> Dimension: 
		              	<i><?php echo $single_production_details['inspection_dimension'];?></i>
		      		</em>
		      		<em class="kt-font-bolder"> Photo: 
		              	<i><?php echo $single_production_details['inspection_photo'];?></i>
		      		</em>
		      		<em class="kt-font-bolder"> Marking: 
		              	<i><?php echo $single_production_details['inspection_marking'];?></i>
		      		</em>
		    	</abbr>
			</span>
		</td>
		<td style="width:230px; padding:0px 0px 0px 15px;">
			<span>    
		        <abbr>
		            <em class="kt-font-bolder"> Comment: 
		              	<i><?php echo $single_production_details['inspection_comment'];?></i>
		      		</em>
		    	</abbr>
			</span>
		</td>
	<?php }?>
	<td class="kt-align-center" style="width:130px; padding:0px 0px 0px 15px;">
		<span>
	        <abbr>
	    	<?php foreach (explode(',', $single_production_details['handled_by']) as $handled_by_name) { ?>
	        	<em> <?php echo $handled_by_name;?> </em>
	    	<?php }?>
	    	</abbr>
		</span>
	</td>
	<td class="kt-align-center" style="width:130px; padding:0px 0px 0px 15px;">
		<span>
	        <abbr>
	    	<?php foreach (explode(',', $single_production_details['handled_by_new']) as $handled_by_name) { ?>
	        	<em> <?php echo $handled_by_name;?> </em>
	    	<?php }?>
	    	</abbr>
		</span>
	</td>
	<?php if(in_array($this->session->userdata('main_tab_name'), array('inspection'))){?>
	<td class="kt-align-center" style="width:500px;padding:0px 0px 0px 15px;">
		<span>
			<abbr>
				<em class="kt-font-bolder row"> 
					<span style="width: 2%;padding: 0% 0% 0% 0%;">
			    		#
			    	</span> 
			    	|
			    	<span style="width: 45%;padding: 0% 0% 0% 0%;">
			    		Vendor Name
			    	</span> 
			    	|
			    	<span style="width: 15%;padding: 0% 0% 0% 0%";>
			    		PO Value
			    	</span>
			    	|
	            	<span style="width: 20%;padding: 0% 0% 0% 0%";>
	            		Delivery Time
	            	</span>
	            	|
	            	<span style="width: 8%;padding: 0% 0% 0% 0%";>
	            		Action
	            	</span>
			    </em>
			    <hr>
			    <?php 
			    if(!empty($single_production_details['po_details'])){
			    	foreach ($single_production_details['po_details'] as $single_po_key => $single_po_details) {
	    		?>
					    <em class="kt-font-bolder row"> 
							<span style="width: 2%;padding: 0% 0% 0% 0%;">
					    		<?php echo $single_po_key+1;?>
					    	</span> 
					    	|
					    	<span style="width: 45%;padding: 0% 0% 0% 0%;">
					    		<?php echo $single_po_details['vendor_name'];?>
					    	</span> 
					    	|
					    	<span style="width: 15%;padding: 0% 0% 0% 0%";>
					    		<?php echo $single_po_details['gross_total'];?>
					    	</span>
					    	|
			            	<span style="width: 20%;padding: 0% 0% 0% 0%";>
								<?php echo $single_po_details['delivery_time'];?>
			            	</span>
			            	|
			            	<span style="width: 8%;padding: 0% 0% 0% 0%";>
								<a href="<?php echo base_url('procurement/pdf/'.$single_po_details['id']);?>" target="_blank">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--dark kt-svg-icon--md">
									    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									        <rect x="0" y="0" width="24" height="24"/>
									        <path d="M3,12 C3,12 5.45454545,6 12,6 C16.9090909,6 21,12 21,12 C21,12 16.9090909,18 12,18 C5.45454545,18 3,12 3,12 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
									        <path d="M12,15 C10.3431458,15 9,13.6568542 9,12 C9,10.3431458 10.3431458,9 12,9 C13.6568542,9 15,10.3431458 15,12 C15,13.6568542 13.6568542,15 12,15 Z" fill="#000000" opacity="0.3"/>
									    </g>
									</svg>
								</a>
			            	</span>
					    </em>
					    <hr>
				<?php 
					}
				}
				?>
			</abbr>
		</span>
	</td>
	<?php } ?>
</tr>
<?php }?>