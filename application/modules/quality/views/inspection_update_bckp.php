<style type="text/css">
	.form_name{
	    color: #767676 !important;
	    font-size: 14px !important;
	    font-family: 'latomedium' !important;
	    padding: 10px 10px !important; 
	    font-weight: bold !important;
	}
	.kt-badge--gainsboro{
		background-color: gainsboro;
	}
</style>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col">
			<div class="alert alert-light alert-elevate fade show" role="alert">
				<div class="alert-icon"><i class="flaticon-edit kt-font-brand"></i></div>
				<div class="alert-text">
					<h2 class="kt-font-info">Quality Inspection Details</h2>
				</div>
				<div>
					<button type="button" class="btn btn-primary btn-wide save_quality_inspection_data">Save Details</button>
				</div>
			</div>
		</div>
	</div>
	<!--begin::Portlet-->
	<div class="kt-portlet" style="background: #F5F5F5;">
		<div class="kt-portlet__body">
			<div class="kt-form__section kt-form__section--first">
				<!--begin::Form-->
				<form class="kt-form" id="quality_inspection_form">
					<input type="text" name="id" value="<?php echo $this->uri->segment('3');?>" hidden>
					<div class="form-group row">
						<label class="col-lg-1 col-form-label form_name">Work Order No:</label>
						<div class="col-lg-2 form-group-sub">
							<input type="text" class="form-control" value="<?php echo $quality_information['work_order_no'];?>" readonly>
						</div>
						<label class="col-lg-1 col-form-label form_name">Assing BY:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control handled_by_select_picker" name="inspection_assign_by" >
								<option value="">Select</option>
						        <?php foreach($user_quality_admin as $quality_admin_details) {?>
									<option 
									value="<?php echo $quality_admin_details['user_id'];?>" 
									<?php 
										echo ($quality_information['inspection_assign_by'] == $quality_admin_details['user_id']) ? 'selected': '';
									?>>
									<?php echo ucfirst(strtolower($quality_admin_details['name']));?>
									</option>
								<?php } ?>
							</select>
						</div>
						<label class="col-lg-1 col-form-label form_name">Assing to:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control handled_by_select_picker" name="inspection_assign_to" >
								<option value="">Select</option>
								<?php foreach($user_quality_user as $quality_user_details) {?>
									<option 
									value="<?php echo $quality_user_details['user_id'];?>" 
									<?php 
										echo ($quality_information['inspection_assign_to'] == $quality_user_details['user_id']) ? 'selected': '';
									?>>
									<?php echo ucfirst(strtolower($quality_user_details['name']));?>
									</option>
								<?php } ?>
							</select>
						</div>							
						<label class="col-lg-1 col-form-label form_name">Diameter:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control" name="inspection_dimension">
								<option value="">Select</option>
								<option value="Yes"
								<?php echo ($quality_information['inspection_dimension'] == 'Yes')?'selected':''?>>
									Yes
								</option>
								<option value="No"
								<?php echo ($quality_information['inspection_dimension'] == 'No')?'selected':''?>>
									No
								</option>
							</select>
						</div>	
					</div>
					<div class="form-group row">	
						<label class="col-lg-1 col-form-label form_name">Photo:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control" name="inspection_photo">
								<option value="">Select</option>
								<option value="Yes"
								<?php echo ($quality_information['inspection_photo'] == 'Yes')?'selected':''?>>
									Yes
								</option>
								<option value="No"
								<?php echo ($quality_information['inspection_photo'] == 'No')?'selected':''?>>
									No
								</option>
							</select>
						</div>
					    <label class="col-lg-1 col-form-label form_name">Marking:</label>
					    <div class="col-lg-2 form-group-sub">
							<select class="form-control" name="inspection_marking">
								<option value="">Select</option>
								<option value="Yes"
								<?php echo ($quality_information['inspection_marking'] == 'Yes')?'selected':''?>>
									Yes
								</option>
								<option value="No"
								<?php echo ($quality_information['inspection_marking'] == 'No')?'selected':''?>>
									No
								</option>
							</select>
						</div>
						<label class="col-lg-1 col-form-label form_name">comment:</label>
						<div class="col-lg-5 form-group-sub">
							<textarea class="form-control" id="kt_autosize_1" name="inspection_comment" rows="1" style="overflow: hidden; overflow-wrap: break-word; resize: none; height: 36px;"><?php echo trim($quality_information['inspection_comment']);?></textarea>
						</div>	
					</div>
					<div class="form-group row">	
						<label class="col-lg-1 col-form-label form_name">QC Clearance:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control" name="qc_clearance">
								<option value="">Select</option>
								<option value="Yes"
								<?php echo ($quality_information['qc_clearance'] == 'Yes')?'selected':''?>>
									Yes
								</option>
								<option value="No"
								<?php echo ($quality_information['qc_clearance'] == 'No')?'selected':''?>>
									No
								</option>
							</select>
						</div>
						<?php if(in_array($this->session->userdata('user_id'), array(59, 33, 35))) {?>
						<label class="col-lg-1 col-form-label form_name">Current Status:</label>
						<div class="col-lg-2 form-group-sub">
							<select class="form-control handled_by_select_picker" name="production_status">
								<option value="">Select Current Status</option>
								<?php foreach ($production_status as $production_key => $production_details) { ?>
									<option 
										value="<?php echo $production_key;?>"
										data-content="<span class='kt-badge kt-badge--<?php echo $production_details['color'];?> kt-badge--inline kt-badge--rounded kt-font-bolder'><?php echo $production_details['name'];?></span>"
										<?php echo ($quality_information['production_status']== $production_key)?'selected':''?>>
											<?php echo $production_details['name'];?>
									</option>
								<?php }?>
							</select>
						</div>
						<?php } ?>
					</div>		
				</form>
    
				<!--end::Form-->
			</div>	
		</div>
	</div>
	
	<!--end::Portlet-->
</div>

<!-- end:: Content -->