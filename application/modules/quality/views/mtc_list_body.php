<tr class="subject" tabindex="0">
    <th>Approved Status</th>
    <th class="list_type_add_remove">Mtc Type</th>
    <th>Quotation For</th>
    <th>Company Name</th>
    <th>Assigned</th>
    <th>Creator</th>
    <th>Action</th>
</tr>
<tr class="subject">
    <th>
        <select class="form-control" id="status_search" name="status_search" style="border-radius: 10px;">
            <option value="">Select</option>
            <option value="Y">Yes/Approved</option>';
            <option value="P">Pending</option>';
            <option value="N">No/Disapproved</option>';
        </select>
    </th>
    <th class="list_type_add_remove">
        <select class="form-control" id="list_type" name="list_type" style="border-radius: 10px;">
            <option value="">Select</option>
            <option value="sample">Sample MTC</option>';
            <option value="pending">Pending Order MTC</option>';
            <option value="dispatch">Dispatch Order MTC</option>';
            <option value="proforma" style="display: none;">Proforma Order MTC</option>';
        </select>
    </th>
    <th><input type="text" class="form-control" id="quotation_for" placeholder="Quotation number" style="border-radius: 10px;"></th>
    <th><input type="text" class="form-control" id="company_name" placeholder="Company Name" style="border-radius: 10px;"></th>
    <th>
        <?php if(!empty($assigned)) {?>
        <select class="form-control" id="assigned" name="assigned" style="border-radius: 10px;">
            <option value="">Select</option>
            <?php foreach($assigned as $assigned_value) {?>
                <option value="<?php echo $assigned_value;?>"><?php echo $users[$assigned_value];?></option>';
            <?php }?>
        </select>
        <?php }?>
    </th>
    <th>
        <?php if(!empty($creater)) {?>
        <select class="form-control" id="creater" name="creater" style="border-radius: 10px;">
            <option value="">Select</option>
            <?php foreach($creater as $creator_value) {?>
                <option value="<?php echo $creator_value;?>"><?php echo $users[$creator_value];?></option>';
            <?php }?>
        </select>
        <?php }?>
        </th>
    <th>
        <input type="button" class="btn btn-success" id="mst_search_filter" value="Apply" style="border-radius: 10px; width: 45%;">
        <input type="button" class="btn btn-warning" id="mst_reset_filter" value="Reset" style="border-radius: 10px; width: 45%;">
    </th>
</tr>
<?php  
if(!empty($list)) {
	foreach ($list as $list_key => $list_value) { 
?>	
<tr>
    <td>
    	<span style="width:200px !important;">
        <i style="cursor: pointer;" data-value="Public" class=""><?php echo $list_key+1; ?></i>
        <abbr>
            <em> Made Status: 
              	<em class="close_activity_status">
              		<?php echo $list_value['made_status']; ?>
          		</em>
      		</em>
            <?php if($call_type != 'marking_sample_list') { ?> 
        	  	<em> Quality Status: 
                  	<em class="notpossible_activity_status">
                  		<?php echo $list_value['admin_status']; ?>
                  	</em>
              	</em> 
            	<em> Admin Status: 
            		<em class="notpossible_activity_status">
            			<?php echo $list_value['super_admin_status']; ?>
            		</em>
            	</em>
            <?php } ?>    
            <em> Date: <i><?php echo $list_value['date']; ?></i></em> 
    	</abbr>
    	</span>
    </td>
    <td class="list_type_add_remove"><?php echo $list_value['list_type']; ?></td>
    <td><?php echo $list_value['quotation_name']; ?></td>
    <td><?php echo $list_value['client_name']; ?></td>
    <td><?php echo $list_value['assigned_to']; ?></td>
    <td><?php echo $list_value['created_by']; ?></td>
    <td class="activity_action">
    	<ul>
    	<?php if($call_type != 'marking_sample_list' && (in_array($this->session->userdata('role'), array(1, 10)) || $this->session->userdata('user_id') == 33) ){ ?>
      		<li class="update_quality_status"  update_status_id="<?php echo $list_value['id']; ?>">
    			<a class="mark-completed update_status" data-target="#mtc_status" data-toggle="modal" data-placement="top" title="Update Status" data-original-title="Update Status"></a>
    		</li>
    	<?php }?>
        <li>
        	<a href="<?php echo $list_value['edit_url'];?>" data-toggle="tooltip" data-placement="top" title="Edit" data-original-title="Edit"></a>
        </li>
        <?php if(!empty($list_value['purchase_order'])) {?>
            <li>
              	<a href="<?php echo base_url('/assets/purchase_orders/'.$list_value['purchase_order'])?>" class="idattrlink-new" data-toggle="tooltip" data-placement="top" title="Purchase Order" data-original-title="View Purchase Order"></a>
            </li>
        <?php } ?>
        <li class="delete_mtc_marking_listing" delete_list_id="<?php echo $list_value['id']; ?>" call_type="<?php echo $call_type; ?>">
          	<a data-toggle="tooltip" class="delete_mtc_marking_listing template-delete-icon custom-template-delete"  data-placement="top" title="Delete" data-original-title="Delete"></a>
        </li>
    	</ul>
    </td>
</tr>
<?php  
    } 
} 
?>