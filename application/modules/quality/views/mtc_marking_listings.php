<!-- begin:: Content -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.css" rel="stylesheet">
<style type="text/css">
	.tr_active{
		display: block !important;
	}
	.tr_inactive{
		display: none !important;
	}
	li.active_list{
		border-bottom: 0.25rem solid #1cc88a!important;
	}
	#kt_wrapper{
		padding-top: 3% !important;
	}
	.my-scroll{
      border:1px solid #e1e1e1;
      height: 670px;
      width:auto;
      overflow-y: auto;
    }
</style>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid container_24 data-panel">
	<div class="wrapper pt20">
		<div class="row">
			<div class="col-md-12">
				<div class="white_grid wrapper">
					<div class="common_heading"><i class="custom_heading_icon overview_data_icon"></i> Marking Listing</div>
					<div class="col-md-12" style="display: none;">
						<div class="pending_issue_search_bar_wrapper">
							<input type="text" id="current_tab_name" value="<?php echo $call_type; ?>" hidden="hidden">
							<div class="new_lead_form_inner_search new_lead_form_inner_search12">
								<div class="pending_create_new_issue">
										<a href="javascript:void(0);" id="create_new_pending_issue" class="modal-show">+ Create New Marking</a> 
								</div>
							</div>
						</div>
					</div>
					<div class="wrapper conversation_main_wrapper">
						<ul class="tabular menu keyword_ranking_tab_menu pending_issue_main_tab">
							<?php 
								$mtc_list_class = $sample_marking_list_class = $proforma_marking_list_class = '';
								if($call_type == 'mtc_list') {
									$mtc_list_class = 'active_list';
								} elseif ($call_type == 'marking_sample_list') {
									$sample_marking_list_class = 'active_list';
								} elseif ($call_type == 'marking_proforma_list') {
									$proforma_marking_list_class = 'active_list';
								}
							?>
							<li class="item <?php echo $mtc_list_class;?>" header_call_type ="mtc_list" data-tab="mtc_list" style="width: calc(100%/3) !important;">
								<a style="cursor: pointer;">
									<i class="custom_heading_icon active_issues_tabs_icon"></i>
									MTC List
								</a>
							</li>
							<li class="item  <?php echo $sample_marking_list_class;?>" header_call_type ="marking_sample_list" data-tab="sample_marking_list" style="width: calc(100%/3) !important;">
								<a style="cursor: pointer;">
									<i class="custom_heading_icon active_issues_tabs_icon"></i>
									Sample Marking List
								</a>
							</li>
							<li class="item  <?php echo $proforma_marking_list_class;?>" header_call_type ="marking_proforma_list" data-tab="proforma_marking_list" style="width: calc(100%/3) !important;">
								<a style="cursor: pointer;">
									<i class="custom_heading_icon complete_issues_tabs_icon"></i>
									Proforma Marking List
								</a>
							</li>
						</ul>
						<div class="keyword_ranking_tab_content">
							<div id="mCSB_1" class="my-scroll mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" style="max-height: none;" tabindex="0">
								<div id="mCSB_1_container" class="mCSB_container" style="position:relative; top:0; left:0;" dir="ltr">
									<div class="wrapper ui tab active" data-tab="sample_marking_list">
										<div class="pending_issues_table_data_section">
											<table class="pending_active_issues_table_data">
												<div class="glo_app layer white" style="display: none;"><div class="glo_app apploader large"></div></div>
												<tbody id="pending_issues_data">
													<?php $this->load->view('quality/mtc_list_body');?>
												</tbody>
											</table>
										</div>
										<div class="kt-portlet__body" id="paggination_data">
								            <?php $this->load->view('quality/paggination');?>
								        </div>		
									</div>
								</div>	
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</div>
</div>		
<div class="modal fade" id="test_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- <form id="updateMTC" name="updateMTC" autocomplete="off" enctype="multipart/form-data" method="post"> -->
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">MTC Status Update</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body" id="mtc_status_body">
				<?php //$this->load->view('quality/update_status_model');?>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="update_mst_id" id="update_mst_id">
				<button type="submit" class="btn btn-success" id="mtc_status_update">Update MTC</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close" onclick="">Close</button>
			</div>
			<!-- </form> -->
		</div>
	</div>
</div>