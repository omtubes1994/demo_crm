<!-- begin:: Content -->
<link href="https://v7push-5900.kxcdn.com/css/styles.css" rel="stylesheet" type="text/css" media="all">
<link href='https://v7push-5900.kxcdn.com/css/styles_v2.css?version=1' rel='stylesheet' type='text/css'  media='all'>
<style type="text/css">
    .my-scroll{
        border:1px solid #e1e1e1;
        height: 65vh;
        width:auto;
        overflow-y: auto;
    }
</style>
<div class="row">
    <div id="user_left_view" class="col-lg-6 col-xl-12 order-lg-1 order-xl-1">
        <div class="white_grid wrapper">
            <div class="glo_app layer white" style="display: none;">
                <div class="glo_app apploader large"></div>
            </div>
            <div class="new_lead_form_search">
                <div class="new_lead_form_inner_search new_lead_form_inner_search12 res_apex_search_box res_apex_search_box_menu">
                    <input type="text" id="current_tab_name" value="<?php echo $call_type; ?>" hidden="hidden">
                    <abbr>
                        <input class="form-control" type="text" placeholder="Search members" class="form-textbox" id="user_search" style="border-radius: 10px;">
                    </abbr>
                    <abbr>
                        <select class="form-control" id="user_status" name="user_status" style="border-radius: 10px;">
                            <option value="1">Select Status</option>
                            <option value="1" selected>Active</option>';
                            <option value="0">Deleted</option>';
                        </select>
                    </abbr>
                    <abbr>
                        <select class="form-control" id="user_role" name="user_role" style="border-radius: 10px;">
                            <option value="">Select Role</option>
                            <?php foreach($role as $role_id => $role_name) {?>
                                <option value="<?php echo $role_id; ?>"><?php echo ucfirst($role_name); ?></option>';
                            <?php }?>
                        </select>
                    </abbr>
                    <button type="button" class="btn btn-primary" id="add_user">Add User</button>
                    <button type="button" class="btn btn-primary" id="add_module">Add Module</button>
                    <button type="button" class="btn btn-primary" id="show_department_graph" data-toggle="modal" data-target="#department_modal">Show Department Graph</button>
                </div>
            </div>
            <div class="col-md-12 scroll-remove-margin"> 
                <div class="wrapper">
                    <div class="res_apex_chat_height content_scroller mCustomScrollbar _mCS_2" style="height:65vh;">
                        <div id="mCSB_2" class="my-scroll mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" style="max-height: none;" tabindex="0">
                            <div id="mCSB_2_container" class="mCSB_container" style="position:relative; top:0; left:0;" dir="ltr">
                                <div class="team_hevily_table_data">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th style="width:5%;">id</th>
                                                <th style="width: 25%;">Username</th>
                                                <th class="user_single_view1" style="width:15%;"><i></i>Role</th>                         
                                                <th style="width:15%;"><i></i>Email ID</th>                          
                                                <th class="user_single_view1" style="width:15%;"><i></i>Mobile Number</th>                          
                                                <!-- <th class="user_single_view_hide"><i></i>Modules</th> -->
                                                <th style="width: 25%;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="user_details">
                                            <?php $this->load->view('user/user_list_body');?>
                                        </tbody>
                                    </table> 
                                </div>
                            </div>      
                        </div>
                    </div>
                </div> 
            </div>
            <div class="pagination-wrapper"  id= "paggination_data" style="padding: 10px 10px 10px 10px;"> 
            <?php $this->load->view('user/paggination');?>
            </div>
        </div>
    </div>
    <div id="user_right_view" class="col-lg-6 col-xl-6 order-lg-1 order-xl-1"  style="display: none;">
        <div class="glo_app layer white" style="display: none;">
            <div class="glo_app apploader layerge"></div>
        </div><i></i>
        <div class="white_grid wrapper scroll-remove-margin">
            <div id="single_user_data">
            <?php $this->load->view('user/single_user_details');?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Assign User Data To Other User</h4>
        <input type="text" name="delete_user_id" id="delete_user_id" value="" hidden>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <form>
            <div class="glo_app add_delete_user_select_user_details layer white" style="display: none;">
                <div class="glo_app apploader layerge"></div>
            </div>
          <div class="form-group">
            <label for="recipient-name" class="control-label">Select Role:</label>
            <select class="form-control" id="delete_user_select_role" name="delete_user_select_role" style="border-radius: 10px;">
                <option value="">Select User Role</option>
                <?php foreach($role as $role_id => $role_name) {?>
                    <option value="<?php echo $role_id;?>"><?php echo $role_name;?></option>';
                <?php }?>
            </select>
          </div>
          <div class="form-group">
            <label for="message-text" class="control-label">Select User:</label>
            <select class="form-control" id="delete_user_select_user" name="delete_user_select_user" style="border-radius: 10px;">
                <option value="">Select User</option>
            </select>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary  add_delete_user_user_id">Assign data</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="department_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div id="department_highchart"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Send message</button>
            </div>
        </div>
    </div>
</div>
