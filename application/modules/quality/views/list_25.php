<style type="text/css">
	.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
		padding-top: 4% !important;
	}
	.layer-white{
	    display: none;
	    position: absolute;
	    top: 0em !important;
	    left: 0em !important;
	    width: 100%;
	    height: 100%;
	    text-align: center;
	    vertical-align: middle;
	    background-color: rgba(255, 255, 255, 0.55);
	    opacity: 1;
	    line-height: 1;
	    -webkit-animation-fill-mode: both;
	    animation-fill-mode: both;
	    -webkit-animation-duration: 0.5s;
	    animation-duration: 0.5s;
	    -webkit-transition: background-color 0.5s linear;
	    transition: background-color 0.5s linear;
	    -webkit-user-select: none;
	    -moz-user-select: none;
	    -ms-user-select: none;
	    user-select: none;
	    will-change: opacity;
	    z-index: 9;
	}
	.div-loader{
	    position: absolute;
		top: 50%;
		left: 50%;
		margin: 0px;
		text-align: center;
		z-index: 1000;
		-webkit-transform: translateX(-50%) translateY(-50%);
		transform: translateX(-50%) translateY(-50%);
	}
	.kt-spinner:before {
	    width: 50px;
	    height: 50px;
	    margin-top: -10px;
	}
	tbody#quality_listing tr:hover {
		background-color: gainsboro;
	}
	tbody#quality_listing tr:hover .first_div {
	    border-left: 0.25rem solid #5578eb !important;
	}
	tbody#quality_listing td {
		font-style: normal;
		font-size: 15px;
	    border: 0.05rem solid gainsboro;
	}
	tbody#quality_listing td span{
		/*width:200px !important;*/
		display: inline-flex;
	    width: 100%;
	}
	tbody#quality_listing td span i{
		cursor: pointer;
	}
	tbody#quality_listing td span abbr{
	    width: 100%;
	}    
	tbody#quality_listing td span abbr em{
		display: block;
		color: #898989;
		margin: 3px 0 3px 0;
		font-style: normal;
		font-family: Helvetica Neue,Helvetica,Arial,sans-serif
	}
	tbody#quality_listing td span abbr em i{
		font-weight: 500;
	    color: #898989;
	    margin: 3px 0 3px 0;
	    font-style: normal;
	    font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
	}
	thead#production_header tr th{
	    background: gainsboro;
	    color: #767676;
	    font-size: 14px;
	    font-family: 'latomedium';
	    padding: 10px 20px;
	    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
	}
	ul.menu-tab {
		margin: 0px;
	    padding: 0px;
	    border: 1px solid #E7E7E7;
	    /*border-radius: 50px;*/
	    overflow: hidden;
	    font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
	    font-size: 14px;
	    line-height: 20px;
	    color: #333;
	}
	ul.menu-tab li{
		width: calc(100%/6) !important;
	    display: inline;
	    text-align: center;
	    float: left;
	}
	ul.menu-tab li.active_list{
	    border-bottom: 0.25rem solid #767676 !important;
	}
	ul.menu-tab li a{
		cursor: pointer;
	    display: inline-block;
	    outline: none;
	    text-align: center;
	    width: 100%;
	    background: #F5F5F5;
	    /*border-right: 2px solid #fff;*/
	    color: #767676;
	    font-size: 15px;
	    font-family: 'latomedium';
	    background-color: gainsboro;
	    padding: 3px 3px 12px 3px;
	}
	ul.menu-tab li a i{
		display: inline-block;
	    width: 35px;
	    height: 28px;
	    font-style: normal;
	    background-size: 100%;
	    position: relative;
	    top: 6px;
	}
	.form_name{
	    color: #767676 !important;
	    font-size: 14px !important;
	    font-family: 'latomedium' !important;
	    padding: 10px 10px !important; 
	    font-weight: bold !important;
	}
	.kt-badge--gainsboro, .kt-font-gainsboro{
		background-color: gainsboro;
	}
</style>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					Product Lists
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
		        <div class="kt-portlet__head-wrapper">
		            <div class="kt-portlet__head-actions">
		                <a href="javascript:;" class="btn btn-brand btn-elevate btn-icon-sm add_quality_listing_search_filter_form" style="background-color: gainsboro; border-color: gainsboro; color: #767676; font-weight: 800;">
		                    Add Search Filter
		                </a>
		            </div>
		        </div>
	    	</div>
		</div>
		<div class="kt-portlet__body quality_listing_search_filter" style="display: none; padding: 1% 1% 0% 1%;">
			
        	<?php $this->load->view('quality/quality_document_inspection_list_search_filter_form'); ?>
		</div>
		<div class="kt-portlet__body" style="padding: 25px 25px 0px 25px;">
			<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
				<div class="row">
					<div class="col-sm-12">
						<ul class="menu-tab kt-font-bolder"
							style="">
							<li class="main_tab_name_click documentation active_list" tab-name="documentation" style="width: calc(100%/2) !important;">
								<a>
									<i class="custom_heading_icon active_issues_tabs_icon">
										<img src="assets/media/icons/svg/Tools/Angle Grinder.svg"/>
									</i>
									<span style="top: 6px;position: relative;">
										Documentation
									</span>
								</a>
							</li>
							<li class="main_tab_name_click inspection" tab-name="inspection" style="padding: 0px 0px 0px 10px;width: calc(100%/2) !important;">
								<a>
									<i class="custom_heading_icon active_issues_tabs_icon">
									    <img src="assets/media/icons/svg/Tools/Tools.svg"/>
									</i>
									<span style="top: 6px;position: relative;">
										Inspection
									</span>
								</a>
							</li>
						</ul>
					</div>
					<div class="col-sm-1"></div>
					<div class="col-sm-12">
						<ul class="menu-tab kt-font-bolder"
							style="">
							<li class="tab_name_click pending_order active_list" tab-name="pending_order">
								<a>
									<i class="custom_heading_icon active_issues_tabs_icon">
										<img src="assets/media/icons/svg/Tools/Angle Grinder.svg"/>
									</i>
									<span style="top: 6px;position: relative;">
										Pending Order
									</span>
								</a>
							</li>
							<li class="tab_name_click semi_ready" tab-name="semi_ready" style="padding: 0px 0px 0px 10px;">
								<a>
									<i class="custom_heading_icon active_issues_tabs_icon">
									    <img src="assets/media/icons/svg/Tools/Tools.svg"/>
									</i>
									<span style="top: 6px;position: relative;">
										Semi Ready
									</span>
								</a>
							</li>
							<li class="tab_name_click ready_to_dispatch_order" tab-name="ready_to_dispatch_order" style="padding: 0px 0px 0px 10px;">
								<a>
									<i class="custom_heading_icon active_issues_tabs_icon">
									    <img src="assets/media/icons/svg/Tools/Tools.svg"/>
									</i>
									<span style="top: 6px;position: relative;">
										Ready To Dispatch
									</span>
								</a>
							</li>
							<li class="tab_name_click dispatch_order" tab-name="dispatch_order" style="padding: 0px 0px 0px 10px;">
								<a>
									<i class="custom_heading_icon active_issues_tabs_icon">
									    <img src="assets/media/icons/svg/Tools/Tools.svg"/>
									</i>
									<span style="top: 6px;position: relative;">
										Dispatch Order
									</span>
								</a>
							</li>
							<li class="tab_name_click on_hold_order" tab-name="on_hold_order" style="padding: 0px 0px 0px 10px;">
								<a>
									<i class="custom_heading_icon active_issues_tabs_icon">
									    <img src="assets/media/icons/svg/Tools/Tools.svg"/>
									</i>
									<span style="top: 6px;position: relative;">
										On Hold Order
									</span>
								</a>
							</li>
							<li class="tab_name_click order_cancelled" tab-name="order_cancelled" style="padding: 0px 0px 0px 10px;">
								<a>
									<i class="custom_heading_icon active_issues_tabs_icon">
									    <img src="assets/media/icons/svg/Tools/Tools.svg"/>
									</i>
									<span style="top: 6px;position: relative;">
										Order Cancelled
									</span>
								</a>
							</li>
						</ul>
					</div>
					<div class="col-sm-1"></div>
					<div class="col-sm-12">
						<div class="kt-scroll" data-scroll="true" data-scroll-x="true" style="height: auto;">
							<div style="width: 2500px;">
								<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 100%; border: 0.25rem solid gainsboro;">
									<thead id="production_header">
		          						<tr role="row">
		          							<th style="
												    padding: 0px 0px 0px 0px;
												    text-align: center;">Sr. No.</th>
		          							<th style="
												    padding: 0px 0px 0px 0px;
												    text-align: center;">Wo. No.</th>
											<th class="sorting_disabled kt-font-bolder kt-align-right" style="">Actions</th>
											<th 
												class=" sorting_search kt-font-bolder kt-align-left" 
												sorting_name="invoice_mst.invoice_date"
												sorting_value=""
												colspan="2" 
												style=" padding: 0% 0% 0% 15px;">
												Proforma Details<span class="kt-font-bolder kt-align-right"></span>
											</th>
											<th 
												class=" sorting_search kt-font-bolder kt-align-center" 
												sorting_name="invoice_mst.invoice_date"
												sorting_value=""
												colspan="2" 
												style="">
												Other Details
											</th>
											<th 
												class="all documentation sorting_disabled kt-font-bolder kt-align-left"
												colspan="2" 
												style=" padding: 0% 0% 0% 15px;">
												Marking<span class="kt-font-bolder kt-align-right"></span>
											</th>
											<th 
												class="all documentation sorting_disabled kt-font-bolder kt-align-left"
												colspan="3" 
												style=" padding: 0% 0% 0% 15px;">
												MTC<span class="kt-font-bolder kt-align-right"></span>
											</th>
											<th 
												class="all inspection sorting_disabled kt-font-bolder kt-align-left"
												style=" padding: 0% 0% 0% 15px;display: none;">
												Inspection Person<span class="kt-font-bolder kt-align-right"></span>
											</th>
											<th 
												class="all inspection sorting_disabled kt-font-bolder kt-align-left"
												colspan="2" 
												style=" padding: 0% 0% 0% 15px;display: none;">
												Parameters Checked<span class="kt-font-bolder kt-align-right"></span>
											</th>
											
											<th 
												class=" sorting_search kt-font-bolder kt-align-center" 
												sorting_name="invoice_mst.invoice_date"
												sorting_value=""
												style="">
												Handled By
											</th>
											<th 
												class=" sorting_search kt-font-bolder kt-align-center" 
												sorting_name="invoice_mst.invoice_date"
												sorting_value=""
												style="">
												Status
											</th>
										</tr>
									</thead>
									<tbody id="quality_listing">
										<?php $this->load->view('quality/quality_document_inspection_list_body'); ?>
									</tbody>
								</table>
							</div>
						</div>
						<div id="invoice_table_loader" class="layer-white">
							<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
						</div>
					</div>
				</div>
				<div class="row" id="quality_list_paggination" style="padding: 25px 1px 1px 1px;">
          			<?php $this->load->view('quality/quality_document_inspection_list_paggination');?>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- end:: Content -->
<div class="modal fade" id="production_query" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Query</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
           		<div class="row">
           			<div class="col-md-6">
           				<label for="query_text">Query Details</label>
           				<textarea id="query_text" name="query_text" class="form-control validate[required]"></textarea>
           			</div>
           		</div>
           		<div class="clearfix"></div>
           		<div class="row">
           			<div class="col-md-6 align-self-center">
           				<input type="hidden" id="add_query_quotation_id" name="quote_id">
           				<button class="btn btn-success add_query_submit" type="reset">Add Query</button>
           			</div>
           		</div>
               	<hr/>
               	<h4>Query History</h4>
                <div id="production_query_history"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
			</div>
		</div>
	</div>
</div>
<!--begin::Modal-->
	<div class="modal modal-stick-to-bottom fade" id="mtc_upload" role="dialog" data-backdrop="false">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">MTC PDF</h5>
				</div>
				<div class="modal-body">
	           		<div class="form-group form-group-last row">
						<label class="col-lg-3 col-form-label">Upload Files:</label>
						<div class="col-lg-9">
							<div class="dropzone dropzone-multi" id="kt_dropzone_4">
								<div class="dropzone-panel">
									<a class="dropzone-select btn btn-label-brand btn-bold btn-sm">Attach files</a>
									<a class="dropzone-upload btn btn-label-brand btn-bold btn-sm">Upload All</a>
									<a class="dropzone-remove-all btn btn-label-brand btn-bold btn-sm">Remove All</a>
								</div>
								<div class="dropzone-items">
									<div class="dropzone-item" style="display:none">
										<div class="dropzone-file">
											<div class="dropzone-filename" title="some_image_file_name.jpg"><span data-dz-name>some_image_file_name.jpg</span> <strong>(<span  data-dz-size>340kb</span>)</strong></div>
											<div class="dropzone-error" data-dz-errormessage></div>
										</div>
										<div class="dropzone-progress">
											<div class="progress">
												<div class="progress-bar kt-bg-brand" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress></div>
											</div>
										</div>
										<div class="dropzone-toolbar">
											<span class="dropzone-start"><i class="flaticon2-arrow"></i></span>
											<span class="dropzone-cancel" data-dz-remove style="display: none;"><i class="flaticon2-cross"></i></span>
											<span class="dropzone-delete" data-dz-remove><i class="flaticon2-cross"></i></span>
										</div>
									</div>
								</div>
							</div>
							<span class="form-text text-muted">Max file size is 1MB and max number of files is 5.</span>
						</div>
					</div>
	               	<hr/>
	               	<h4>MTC PDF</h4>
	                <div id="production_mtc_pdf_history" style="padding: 0% 10% 0% 10%;">
	                	
	                </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary mtc_upload_close" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<!--end::Modal-->
</div>