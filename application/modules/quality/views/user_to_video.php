<?php if(!empty($single_user_details)) { ?>
	<style type="text/css">
		.my-scroll{
			  border:1px solid #e1e1e1;
			  height: 65vh;
			  width:auto;
			  overflow-y: auto;
		}
	</style>
	<div class="common_heading apex_common_heading_float team_hively_heading_block"> <?php echo ucwords(strtolower($single_user_details['name']));?> (<?php echo ($single_user_details['status']) ? 'Active':'Inactive'; ?>)</div>
    <ul class="con_right_name apex_chat_screen_action">
        <!-- <li><a href="javascript:void(0)" class="con_right_side_full_screen" data-toggle="tooltip" data-placement="bottom" title="FullScreen" data-original-title="FullScreen"></a></li> -->
	    <li class="close_right_side_details">
	        <a href="javascript:void(0)" class="con_right_block_close full_screen_close1 " data-toggle="tooltip" data-placement="left" title="Close"></a>
	    </li>
    </ul>
	<div class="wrapper">
	    <div class="common_heading apex_common_heading_float team_hively_heading_block"> Update Users Details </div>
	    <div class="rating_messages_table_wrapper content_scroller teamhively_right_side_section mCustomScrollbar _mCS_2 mCS_no_scrollbar res_rating_msg_height" style="height:65vh;">
			<div id="mCSB_1" class="my-scroll mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" style="max-height: none;" tabindex="0">
				<div id="mCSB_1_container" class="mCSB_container" style="position:relative; top:0; left:0;" dir="ltr">
			    	<div id="rating_message">
			    		<div class="team_hevily_right_side_section" id="user_viw">
			    			
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
						        <thead>
						            <tr>
						                <th colspan="2">
						                    <label class="col-lg-6">DEPARTMENT</label>
						                   <div class="btn-group show col-lg-6 department">
												<select class="form-control" id="department_name"  style="border-radius: 20px;">
											      
											            <?php foreach ($department_name as $value) {?>
											            <option value="<?php echo $value?>"<?php echo ($value=='sales') ? 'selected': ''; ?>><?php echo ucfirst($value);?></option>
											        <?php }?>
											</div>
										</th> 
						                <th  colspan="1"><button  class="btn btn-info update_video_details"  user_id="<?php echo $single_user_details['user_id'];?>">Update</button></th>
						            </tr>
						        </thead>
						        <tbody id='video_body'>	
						       <?php $this->load->view('user/video_body');?>    
						        </tbody>
							</table>
						</div>
					</div>
		    	</div>
			</div>
		</div>
	</div>
<?php } ?>