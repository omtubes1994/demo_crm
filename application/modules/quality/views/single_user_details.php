<?php if(!empty($single_user_details)) { ?>
	<style type="text/css">
	.my-scroll{
      border:1px solid #e1e1e1;
      height: 330px;
      width:auto;
      overflow-y: auto;
    }
</style>
	<div class="common_heading apex_common_heading_float team_hively_heading_block"> <?php echo ucwords(strtolower($single_user_details['name']));?> (<?php echo ($single_user_details['status']) ? 'Active':'Inactive'; ?>)</div>
	    <ul class="con_right_name apex_chat_screen_action">
	        <li><a href="javascript:void(0)" class="con_right_side_full_screen" data-toggle="tooltip" data-placement="bottom" title="FullScreen" data-original-title="FullScreen"></a></li>
		    <li>
		        <a href="javascript:void(0)" class="con_right_block_close full_screen_close1 " data-toggle="tooltip" data-placement="left" title="Close"></a>
		    </li>
	    </ul>
	    <div class="team_right_user_block">
	        <ul class="keyword_ranking_legend wrapper team_hively_legend team_hively_user_legend" id="hively_view">
	            <li class="total_user_score_right_border1" style="padding: 10px 5px 18px 2px;">
	                <div class="keyword_ranking_legend teamhively_legend_section">
	                <i class="lead_tracking_legend_icon teamhively_user_score_icon1"></i>
	                    <span>
	                        <abbr><?php echo count($module_access); ?><div class="team_hively_img"></div>   
	                        </abbr>
	                        <em>Module
	                            <a href="#" class="team_hevily_score_view">
	                                <i></i>
	                                <ul class="team_hevily_score_view_content">
	                                	<?php 
	                                		foreach ($modules as $modules_details) {
	                                	?>
		                                    <li>
		                                        <abbr><?php echo $modules_details['module_name']; ?></abbr>
		                                        <i><?php echo in_array($modules_details['module_id'], $module_access)? 'Active' : 'Inactive'; ?></i>
		                                    </li>
	                                	<?php		
	                                		}
	                                	?>
	                                </ul>
	                            </a>
	                        </em>
	                    </span>
	                </div>
	            </li>
	            <li style="width: calc(100%/3);">
	                <div class="keyword_ranking_legend teamhively_legend_section">
	                	<i class="lead_tracking_legend_icon teamhively_user_score_icon"></i>
	                    <span>
	                        <abbr style="font-size: 24px;"><?php echo $single_user_details['username']; ?></abbr>
	                        <em>UserName</em>
	                    </span>
	                </div>
	            </li>
	            <li style="width: calc(100%/3);">
	                <div class="keyword_ranking_legend teamhively_legend_section">
	                	<i class="lead_tracking_legend_icon lead_mng_mobile_icon"></i>
	                    <span>
	                        <abbr style="font-size: 24px;"><?php echo $single_user_details['mobile']; ?></abbr>
	                        <em>Mobile Number</em>
	                    </span>
	                </div>
	            </li>
	        </ul>
	        <ul class="keyword_ranking_legend wrapper team_hively_legend team_hively_user_legend" id="hively_view">
	            <li style="width: calc(100%/2);">
	                <div class="keyword_ranking_legend teamhively_legend_section">
	                	<i class="lead_tracking_legend_icon teamhively_user_icon"></i>
	                    <span>
	                        <abbr style="font-size: 24px;"><?php echo $single_user_details['email']; ?></abbr>
	                        <em>Email ID</em>
	                    </span>
	                </div>
	            </li>
	            <li style="width: calc(100%/2);">
	                <div class="keyword_ranking_legend teamhively_legend_section">
	                	<i class="lead_tracking_legend_icon teamhively_user_score_icon"></i>
	                    <span>
	                        <abbr style="font-size: 24px;"><?php echo $role[$single_user_details['role']]; ?></abbr>
	                        <em>Role</em>
	                    </span>
	                </div>
	            </li>

	        </ul>
	    </div>
	</div>
	<div class="wrapper">
	    <div class="common_heading apex_common_heading_float team_hively_heading_block"> Update Users Details </div>
	    <div class="rating_messages_table_wrapper content_scroller teamhively_right_side_section mCustomScrollbar _mCS_2 mCS_no_scrollbar res_rating_msg_height" style="height:320px;">
			<div id="mCSB_1" class="my-scroll mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" style="max-height: none;" tabindex="0">
				<div id="mCSB_1_container" class="mCSB_container" style="position:relative; top:0; left:0;" dir="ltr">
			    	<div id="rating_message"><div class="team_hevily_right_side_section" id="user_viw">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
						        <thead>
						            <tr>
						                <th>Field</th>
						                <th>Value</th>
						                <th><button  class="btn btn-info update_user_details"  user_id="<?php echo $single_user_details['user_id'];?>">Update</button></th>
						            </tr>
						        </thead>
						        <tbody id="">
			                        <tr>
					                    <td>
					                        <span>Employee Name</span>
					                        <abbr></abbr>
					                    </td>
					                    <td>
					                        <em>
					                            <i></i>
					                            <span><input type="text" name="<?php echo $single_user_details['user_id'].'_employee_name'; ?>" id="<?php echo $single_user_details['user_id'].'_employee_name'; ?>" value=""></span>
					                        </em>
					                    </td>
					                </tr>
					                <tr>
					                    <td>
					                        <span>Email ID</span>
					                        <abbr></abbr>
					                    </td>
					                    <td>
					                        <em>
					                            <i></i>
					                            <span><input type="text" name="<?php echo $single_user_details['user_id'].'_email'; ?>new_email" id="<?php echo $single_user_details['user_id'].'_email'; ?>" value=""></span>
					                        </em>
					                    </td>
					                </tr>
					                <tr>
					                    <td>
					                        <span>User Name</span>
					                        <abbr></abbr>
					                    </td>
					                    <td>
					                        <em>
					                            <i></i>
					                            <span><input type="text" name="<?php echo $single_user_details['user_id'].'_username'; ?>" id="<?php echo $single_user_details['user_id'].'_username'; ?>" value=""></span>
					                        </em>
					                    </td>
					                </tr>
					                <tr>
					                    <td>
					                        <span>Password</span>
					                        <abbr></abbr>
					                    </td>
					                    <td>
					                        <em>
					                            <i></i>
					                            <span><input type="text" name="<?php echo $single_user_details['user_id'].'_password'; ?>" id="<?php echo $single_user_details['user_id'].'_password'; ?>" value=""></span>
					                        </em>
					                    </td>
					                </tr>
					                <tr>
					                    <td>
					                        <span>Mobile Number</span>
					                        <abbr></abbr>
					                    </td>
					                    <td>
					                        <em>
					                            <i></i>
					                            <span><input type="text" name="<?php echo $single_user_details['user_id'].'_mobile'; ?>" id="<?php echo $single_user_details['user_id'].'_mobile'; ?>" value=""></span>
					                        </em>
					                    </td>
					                </tr>
					                <tr>
					                    <td>
					                        <span>Role</span>
					                        <abbr></abbr>
					                    </td>
					                    <td>
					                        <em>
					                            <i></i>
					                            <span><select class="form-control" id="<?php echo $single_user_details['user_id'].'_role'; ?>" name="<?php echo $single_user_details['user_id'].'_role'; ?>" style="border-radius: 10px;">
											            <option value="">Select</option>
											            <?php
											            	if(!empty($role)) {
											            		foreach ($role as $role_key => $role_value) { 
											            			$selected= "";
											            			if(!empty($single_user_details['role']) & $single_user_details['role'] == $role_key) {

											            				$selected= "selected";
											            			}
											            ?>
											            			<option value="<?php echo $role_key?>"<?php echo $selected;?>><?php echo $role_value; ?></option>';

											            <?php			
											            		}
											            	}
											            ?>
											        </select></span>
					                        </em>
					                    </td>
					                </tr>
					                <tr>
					                    <td>
					                        <span>Status</span>
					                        <abbr></abbr>
					                    </td>
					                    <td>
					                        <em>
					                            <i></i>
					                            <span>
					                            	<select class="form-control" id="<?php echo $single_user_details['user_id'].'_status'; ?>" name="<?php echo $single_user_details['user_id'].'_status'; ?>" style="border-radius: 10px;">
											            <option value="">Select Status</option>
											            <option value="1" <?php echo ($single_user_details['status'] == '1') ? 'selected': ''; ?>>Active</option>';
											            <option value="0" <?php echo ($single_user_details['status'] == '0') ? 'selected': ''; ?>>Inactive</option>';
											        </select>
											    </span>
					                        </em>
					                    </td>
					                </tr>
					                <?php foreach($modules as $modules_key => $module_value) {
					                	// echo "<pre>";print_r($module_value);echo"</pre><hr>";exit;
					                	?>
						               	<tr>
						                    <td style="width: 200px;">
						                        <span><?php echo $module_value['module_name']; ?></span>
						                        <abbr><span>
						                            	<select class="form-control" id="<?php echo 'module_id_'.$module_value['module_id']; ?>" name="<?php echo 'module_id_'.$module_value['module_id']; ?>" style="border-radius: 10px;">
												            <option value="">Select <?php echo $module_value['module_name']; ?></option>
												            <option value="1" <?php echo (in_array($module_value['module_id'], $module_access)) ? 'selected': ''; ?>>Yes</option>';
												            <option value="0" <?php echo (!in_array($module_value['module_id'], $module_access)) ? 'selected': ''; ?>>No</option>';
												        </select>
						                            </span></abbr>
						                    </td>
						                    <td>
						                        <em>
						                            <i></i>
						                            <?php if(!empty($sub_module[$module_value['module_id']])) {?>
							                            <?php foreach($sub_module[$module_value['module_id']] as $sub_module_key => $sub_module_value) {?>
							                            	<?php 
							                            		$r1_checked = $r2_checked = '';
							                            		if(in_array($sub_module_value['sub_module_id'], $sub_module_access)) {
							                            			
							                            			$r1_checked = 'checked';
							                            		} else {

							                            			$r2_checked = 'checked';
							                            		}
							                            	?>
								                            <span style="width: calc(100%/<?php echo count($sub_module[$module_value['module_id']]);?>);">
								                            	<span class="title"><?php echo $sub_module_value['sub_module_name'];?> : </span>
						                                        <input name="<?php echo 'radio_'.$sub_module_value['sub_module_id'];?>" id="<?php echo 'r'.$sub_module_value['sub_module_id'];?>" type="radio" value="Yes" <?php echo $r1_checked;?>>
						                                        <label for="<?php echo 'r'.$sub_module_value['sub_module_id'];?>"><span></span>Yes</label>
						                                        <input name="<?php echo 'radio_'.$sub_module_value['sub_module_id'];?>" id="<?php echo 'rr'.$sub_module_value['sub_module_id'];?>" type="radio" value="No" <?php echo $r2_checked;?>>
						                                        <label for="<?php echo 'rr'.$sub_module_value['sub_module_id'];?>"><span></span>No</label>
								                            </span>
							                        	<?php }?>
						                        	<?php }?>
						                        </em>
						                    </td>
						                </tr>
						            <?php } ?>    
			                    </tbody>
							</table>
						</div>
					</div>
		    </div>
		</div>
	</div>
</div>
<?php } ?>	