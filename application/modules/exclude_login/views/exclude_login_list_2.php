<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					Exclude Login
				</h3>
			</div>
            <div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="kt-portlet__head-actions exclude__login">
		                <a href="javascript:;" class="btn btn-brand btn-elevate btn-icon-sm " data-toggle="modal" data-target="#exampleModal" >
		                    Add New 
		                </a>
	                </div>
                </div>
			</div>
		</div>
		
		<div class="kt-portlet__body">

			<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
				<div class="row">
					<div class="col-sm-12">
						<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" style="width:1536px">
							<thead>
          			            <th>No</th>
          			            <th>Name</th>
          			            <th>Action</th>
							</thead>
							<tbody id='exclude_login' >
								<?php $i=0 ; foreach($exclude_details as $key=>$single_exclude_details){
									//echo "<pre>";print_r($single_exclude_details);echo"</pre><hr>";exit;?>
	          			           <tr>
	          			           	   <td>
	          			           	      <?php echo $i+1 ;?>
	          			               </td>
	          			               <td>
	          			           	     <?php echo $single_exclude_details ;?>
	          			               </td>
	          			               <td>
		          			               	<a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_exclude_login_data" delete_id="<?php echo$key; ?>" title="Delete">
						                     	<i class="la la-trash"></i>
					                        </a>
	          			               </td>
	          			           </tr>
						        <?php $i++;}?>
							</tbody>
						</table>
						<div id="kt_table_1_processing" class="dataTables_processing card" style="display: none; top: 10% !important;">Processing...</div>
					</div>
				</div>
			</div>
		</div>

	</div>

</div>

<!-- end:: Content -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel"> ADD USER TO EXCLUDE LOGIN</h4>
        <input type="text" name="user_id" id="user_id" value="" hidden>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="message-text" class="control-label">Select User:</label>
            <select class="form-control" id="Add_user" name="Add_user" style="border-radius: 10px;">
            	<option value="">Select User</option>
            	<?php foreach($users as $key=>$single_users){?> 
                <option value="<?php echo $key;?>"><?php echo $single_users;?></option>
                <?php }?>
            </select>
          </div>
        </form>
      </div>
      <div class="modal-footer add_user">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary  add_selected_user_id">Exclude login</button>
      </div>
    </div>
  </div>
</div>