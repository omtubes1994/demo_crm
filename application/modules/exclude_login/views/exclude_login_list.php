<div class="col-xl-4 col-lg-6 order-lg-3 order-xl-1">
   <!--begin:: Widgets/New Users-->
	<div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
				Exclude Login
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="kt-portlet__head-actions exclude__login">
		                <a href="javascript:;" class="btn btn-brand btn-elevate btn-icon-sm " data-toggle="modal" data-target="#exampleModal" >
		                    Add New 
		                </a>
	                </div>
                </div>
			</div>
		</div>
		<div class="kt-portlet__body">
			<div class="tab-content">
				<div class="tab-pane active" id="kt_widget4_tab1_content">
					<div class="kt-widget4">
						<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid">
							<thead>
          			            <th>No</th>
          			            <th>Name</th>
          			            <th>Action</th>
							</thead>
							<tbody id='exclude_login' >
								<?php $i=0 ; foreach($exclude_details as $key=>$single_exclude_details){
									//echo "<pre>";print_r($single_exclude_details);echo"</pre><hr>";exit;?>
	          			           <tr>
	          			           	   <td>
	          			           	      <?php echo $i+1 ;?>
	          			               </td>
	          			               <td>
	          			           	     <?php echo $single_exclude_details ;?>
	          			               </td>
	          			               <td>
		          			               	<a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_exclude_login_data" delete_id="<?php echo$key; ?>" title="Delete">
						                     	<i class="la la-trash"></i>
					                        </a>
	          			               </td>
	          			           </tr>
						        <?php $i++;}?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--end:: Widgets/New Users-->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel"> ADD USER TO EXCLUDE LOGIN</h4>
        <input type="text" name="user_id" id="user_id" value="" hidden>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="message-text" class="control-label">Select User:</label>
            <select class="form-control" id="Add_user" name="Add_user" style="border-radius: 10px;">
            	<option value="">Select User</option>
            	<?php foreach($users as $key=>$single_users){?> 
                <option value="<?php echo $key;?>"><?php echo $single_users;?></option>
                <?php }?>
            </select>
          </div>
        </form>
      </div>
      <div class="modal-footer add_user">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary  add_selected_user_id">Exclude login</button>
      </div>
    </div>
  </div>
</div>