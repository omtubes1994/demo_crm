function ajax_call_function(data, callType, url = "<?php echo base_url('Exclude_login/ajax_function'); ?>") {

		$.ajax({
			type: 'POST',
			data: data,
			url: url,
			dataType: 'JSON',
			success: function(res){
				if(res.status == 'successful') {	
					if(callType == 'delete_form_exclude' || callType == 'add_user_to_exclude') {
						location.reload()
					}
			    }
        	},
        	beforeSend: function(response){
        		
        	}
	});
};



jQuery(document).ready(function() {})

	$('tbody#exclude_login').on('click', 'a.delete_exclude_login_data', function(){
  swal({
              title: "Are you sure?",
              icon: "warning",
              buttons:  ["Cancel", "remove exclude"],
              dangerMode: true, 
            })
            .then((willDelete) => {
                if (willDelete) {
                    ajax_call_function({call_type:'delete_form_exclude',delete_id:$(this).attr('delete_id')},'delete_form_exclude');
                    swal({
                        title: "removed exclude successfully",
                        icon: "success",
                    });
                } else {
                    swal({
                        title: "removed exclude failed ",
                        icon: "info",
                    });
                }
            });

    });
    


    $('div.add_user').on('click','button.add_selected_user_id',function(){
    	 ajax_call_function({call_type:'add_user_to_exclude',user_id: $('select#Add_user').val(),},'add_user_to_exclude');

    });