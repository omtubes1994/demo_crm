<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exclude_login extends MX_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('user_id')){
			redirect('login', 'refresh');
			exit;
		}else{
			$access_count = 0;
			if(!empty($this->session->userdata('main_module_access')) && in_array(3, array_column($this->session->userdata('main_module_access'), 'module_id'))) {

				foreach ($this->session->userdata('sub_module_access') as $key => $value) {
					
					if($value['module_id'] == 3 && $value['status'] == 'Active') {
						$access_count++;
					}
				}
			}
			if($access_count == 0){
				redirect($this->session->userdata('home/dashboard'));
				exit;
			}
		}
		$this->load->model('common/common_model');
		$this->load->model('exclude_login/exclude_login_model');
	}
    public function exclude_listingz(){
        
        $data=$this->exclude_list_data();
       	echo "<pre>";print_r($data);echo"</pre><hr>";exit;
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('exclude_login_list',$data);
        // $this->load->view('exclude_login_list_2',$data);
        $this->load->view('footer');

    }
    public function ajax_function() {

		if($this->input->is_ajax_request()){
			$call_type = $this->input->post('call_type');
			$response['status'] = 'successful';	
			switch ($call_type) {
				case'delete_form_exclude':
					if(!empty($this->input->post())){
	                        $update_array=array('status'=>'Inactive');
	                        } 
	                    $this->common_model->update_data_sales_db('login_verification_ignore',$update_array,array('user_id'=>$this->input->post('delete_id'),));
	               
				break;
				case'add_user_to_exclude':
					if(!empty($this->input->post())){
	                        $insert_array=array('user_id'=>$this->input->post('user_id'));
	                        } 
	                    $this->common_model->insert_data_sales_db('login_verification_ignore',$insert_array);
	               
				break;

				default:
					$response['status'] = 'failed';	
					$response['message'] = "call type not found";
					break;
			}
			echo json_encode($response);

		} else{
			die('access is not allowed to this function');
		}
	}


	private function exclude_list_data() {
		$data=array();

		$login_exclude_list = $this->exclude_login_model->get_login_verification_exclude('*',array('status'=>'Active'),'login_verification_ignore');

		echo "<pre>";print_r($login_exclude_list);echo"</pre><hr>";exit;
		//echo "<pre>";print_r($data['exclude_details']);echo"</pre><hr>";exit;
        return $data;		
	}
}
?>