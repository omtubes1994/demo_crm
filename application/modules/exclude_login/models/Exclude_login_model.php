<?php 
Class Exclude_login_model extends CI_Model{

	function __construct(){
		parent::__construct();
	}
	
	public function get_login_verification_exclude() {

		$this->db->select('users.name as user_name, login_verification_ignore.id as id');
		$this->db->join('users', 'users.user_id = login_verification_ignore.user_id', 'left');
		$this->db->where(array('login_verification_ignore.status' => 'Active'));
		return $this->db->get('login_verification_ignore')->result_array();
	}
}
?>