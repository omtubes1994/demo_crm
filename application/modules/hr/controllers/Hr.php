<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hr extends MX_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('user_id')){
			redirect('login', 'refresh');
			exit;
		}else{
			// $access_count = 0;
			// if(!empty($this->session->userdata('main_module_access')) && in_array(17, array_column($this->session->userdata('main_module_access'), 'module_id'))) {

			// 	foreach ($this->session->userdata('sub_module_access') as $key => $value) {
					
			// 		if($value['module_id'] == 17 && $value['status'] == 'Active') {
			// 			$access_count++;
			// 		}
			// 	}
			// }
			// if($access_count == 0){
			// 	redirect($this->session->userdata('home/dashboard'));
			// 	exit;
			// }
		}
		// error_reporting(E_ALL);
		$this->load->model('hr_model');
	}

	public function index(){
		redirect('hr/dashboard');
	}
	public function add_employee() {

		if($this->session->userdata('user_id') != 2) {

			//check wheather user exist or not
			// if yes then redirect it to view its details page 
			// else let him add details
			$people_information_details = $this->hr_model->get_dynamic_data('*', array('status' => 'Active', 'user_id'=>$this->session->userdata('user_id')), 'people_information', 'row_array');
			if(!empty($people_information_details)) {

				redirect('hr/edit_listing/account_setting/'.$people_information_details['id']);
			} else {

				$data = array();
				$data['gender'] = $this->hr_model->get_dynamic_data('*', array('status' => 'Active'), 'gender');
				$data['blood_group'] = $this->hr_model->get_dynamic_data('*', array('status' => 'Active'), 'blood_group');
				$data['marriage_info'] = $this->hr_model->get_dynamic_data('*', array('status' => 'Active'), 'marriage_info');
				$data['qualification'] = $this->hr_model->get_dynamic_data('*', array('status' => 'Active'), 'qualification');
				$data['department'] = $this->hr_model->get_dynamic_data('*', array(), 'role');
				// $data['designation'] = $this->hr_model->get_dynamic_data('*', array('status' => 'Active'), 'designation');
				$data['designation'] = array();
				$data['job_location'] = $this->hr_model->get_dynamic_data('*', array('status' => 'Active'), 'job_location');
				// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
				$this->load->view('hr/hr_header', array('title' => 'Hr Add Employee Details'));
				$this->load->view('sidebar', array('title' => 'Hr Add Employee Details'));
				$this->load->view('hr/add_employee', $data);
				$this->load->view('hr/hr_footer');
			}
		} else {
			redirect('hr/edit_listing/account_setting/1');
		}
	}

	public function dashboard() {

		$data = array();
		$this->load->view('hr/hr_header', array('title' => 'Hr Add Employee Details'));
		$this->load->view('sidebar', array('title' => 'Hr Dashboard'));
		$this->load->view('hr/hr_dashboard', $data);
		$this->load->view('hr/hr_footer');
	} 
	
	public function listing() {

		$data = array();
		$data = $this->create_employee_listing_data(array('status' => "'Active'"), array(), 10, 0, array('employee_id' => 0, 'role_name' => ''));
		
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('hr/hr_header', array('title' => 'Hr Listing'));
		$this->load->view('sidebar', array('title' => 'Hr Listing'));
		$this->load->view('hr/hr_listing', $data);
		$this->load->view('hr/hr_footer');
	} 

	public function edit_listing_html() {

		$data = array();
		$this->load->view('hr/hr_header', array('title' => 'Hr Listing'));
		$this->load->view('sidebar', array('title' => 'Hr Listing'));
		$this->load->view('hr/hr_edit_listing_html', $data);
		$this->load->view('hr/hr_footer');
	}

	public function edit_listing() {

		$data = array();
		$people_id = $this->uri->segment(4, 0);
		if(!empty($people_id)) {
			
			switch ($this->uri->segment(3)) {

				case 'address':
					$data['address_details'] = $this->hr_model->get_dynamic_data('', array('status'=>'Active', 'people_id' => $people_id, 'people_address.address_type' => 'resident'), 'people_address', 'row_array');
				break;

				case 'bank_details':
					$data['bank_details'] = $this->hr_model->get_dynamic_data('', array('status'=>'Active', 'people_id' => $people_id), 'bank_details', 'row_array');
				break;
				
				case 'reference':
					$data['reference_details'] = $this->hr_model->get_dynamic_data('', array('status'=>'Active', 'people_id' => $people_id), 'people_reference', 'result_array');
					if(empty($data['reference_details'])) {
						$data['reference_details'][0] = array('id' => 0, 'reference_name' => '', 'relation' => '', 'address' => '', 'number' => '', 'alternate_number' => '', 'email' => '');
						$data['reference_details'][1] = array('id' => 0, 'reference_name' => '', 'relation' => '', 'address' => '', 'number' => '', 'alternate_number' => '', 'email' => '');
					} else if(count($data['reference_details']) == 1) {
						
						$data['reference_details'][1] = array('id' => 0, 'reference_name' => '', 'relation' => '', 'address' => '', 'number' => '', 'alternate_number' => '', 'email' => '');
					}
				break;

				case 'leave_management':
					$data = $this->prepare_leave_data($this->session->userdata('user_id'));
				break;

				case 'salary_slip':
					$data = $this->prepare_salary_slip_data($this->session->userdata('user_id'));
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
				break;

				default:
				break;
			}
			$data['user_details'] = $this->hr_model->get_dynamic_data('', array('status'=>'Active', 'id' => $people_id), 'people_information', 'row_array');
			$data['bank_details'] = $this->hr_model->get_dynamic_data('', array('status'=>'Active', 'people_id' => $people_id), 'bank_details', 'row_array');
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
			$this->load->view('hr/hr_header', array('title' => 'Hr Listing'));
			$this->load->view('sidebar', array('title' => 'Hr Listing'));
			$this->load->view('hr/hr_edit_listing', $data);
			$this->load->view('hr/hr_footer');
		}
	}

	public function upload_document() {

		$response_json['status'] = 'success'; 
		$doc_type =  $this->input->post('doc_type');
		switch ($doc_type) {
			case 'aadhar_card':

				$uploaded_file_status = $this->upload_doc_config($doc_type);
				if($uploaded_file_status['status'] == 'failed') {

					$response_json['status'] = 'error';
					$response_json['msg'] = $uploaded_file_status['msg'];
				} else {

					$set_session_data = $this->session->userdata();
		        	$set_session_data['add_employee_data']['aadhar_card_number'] = $this->input->post('aadhar_number');
		        	$set_session_data['add_employee_data']['aadhar_card_file_name'] = $uploaded_file_status['file_name'];
					$this->session->set_userdata($set_session_data); 
				}
			break;
			
			case 'pan_card':

				$uploaded_file_status = $this->upload_doc_config($doc_type);
				if($uploaded_file_status['status'] == 'failed') {

					$response_json['status'] = 'error';
					$response_json['msg'] = $uploaded_file_status['msg'];
				} else {
					
					$set_session_data = $this->session->userdata();
		        	$set_session_data['add_employee_data']['pan_card_number'] = $this->input->post('pan_number');
		        	$set_session_data['add_employee_data']['pan_card_file_name'] = $uploaded_file_status['file_name'];
					$this->session->set_userdata($set_session_data); 
				}
			break;	

			case 'profile_pic':
				
				$uploaded_file_status = $this->upload_doc_config($doc_type);
				if($uploaded_file_status['status'] == 'failed') {

					$response_json['status'] = 'error';
					$response_json['msg'] = $uploaded_file_status['msg'];
				} else {
					
					if(!empty($this->input->post('people_id'))) {
						$this->hr_model->update_data('people_information', array('profile_pic_file_path'=> $uploaded_file_status['file_name']), array('id'=>$this->input->post('people_id')));
					} else {

						$set_session_data = $this->session->userdata();
			        	$set_session_data['add_employee_data']['profile_pic_file_name'] = $uploaded_file_status['file_name'];
						$this->session->set_userdata($set_session_data); 
					}
				}
			break;	

			default:
				
			break;
		}
		json_encode($response_json);

	}

	public function check_session() {

		echo "<pre>";print_r($this->session->userdata('add_employee_data'));echo"</pre><hr>";exit;
	}

	public function destroy_session() {

		$this->session->unset_userdata('add_employee_data');
	}

	public function list_salary_slip_default_details(){
    	$data = array();
    	$data['column_list']=$this->hr_model->db->list_fields('salary_slip_default');
    	$data['salary_slip_details'] = $this->hr_model->get_dynamic_data('*', array('status'=>'Active'), 'salary_slip_default');
    	$this->load->view('hr/hr_header', array('title' => 'Hr Listing'));
		$this->load->view('sidebar', array('title' => 'Hr Listing'));
		$this->load->view('hr/list_sal', $data);
		$this->load->view('hr/hr_footer');
    }

	public function ajax_function() {

		if($this->input->is_ajax_request()){
			$call_type = $this->input->post('call_type');
			$response['status'] = 'successful';	
			switch ($call_type) {

				case 'get_age_on_birth_date':

					$birth_date = strtotime($this->input->post('birth_date'));
					if(!empty($birth_date)) {

						$current_date = time();
						$diff = abs($birth_date - $current_date);
						// $years = floor($diff / (365*60*60*24));
						// $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
						// $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
						$response['age'] = floor($diff / (365*60*60*24));
					}

					break;

				case 'get_review_of_employee':
				
					$data['form_data'] = array_column($this->input->post('add_employee_form_data'), 'value', 'name');
					// echo "<pre>";print_r($data['form_data']);echo"</pre><hr>";exit;
					$response['html_body'] = $this->load->view('hr/add_employee_review_page', $data, TRUE);
					break;	

				case 'submit_add_employee_form':
					
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					$form_data = array_column($this->input->post('add_employee_form_data'), 'value', 'name');

					// preparing insert array for inserting into people information table
						$people_information_insert_array = array();

						$people_information_insert_array['first_name'] = $form_data['first_name'];
						$people_information_insert_array['last_name'] = $form_data['last_name'];
						$people_information_insert_array['gender'] = $form_data['gender'];
						$people_information_insert_array['blood_group'] = $form_data['blood_group'];
						$people_information_insert_array['birth_date'] = date('Y-m-d',strtotime($form_data['birth_date']));
						$people_information_insert_array['age'] = $form_data['age'];
						$people_information_insert_array['personal_number'] = $form_data['personal_number'];
						$people_information_insert_array['personal_email'] = $form_data['personal_email'];
						$people_information_insert_array['marriage_status'] = $form_data['marriage_status'];
						$people_information_insert_array['qualification'] = $form_data['qualification'];
						$people_information_insert_array['aadhar_number'] = $form_data['aadhar_number'];
						$people_information_insert_array['pan_number'] = $form_data['pan_number'];
						$people_information_insert_array['department'] = $form_data['department'];
						$people_information_insert_array['designation'] = '';
						$people_information_insert_array['official_number'] = $form_data['official_number'];
						$people_information_insert_array['official_email'] = $form_data['official_email'];
						$people_information_insert_array['job_location'] = $form_data['job_location'];
						$people_information_insert_array['user_id'] = $this->session->userdata('user_id');
						
						$session_data = $this->session->userdata('add_employee_data');
						$people_information_insert_array['aadhar_card_file_path'] = $session_data['aadhar_card_file_name'];
						$people_information_insert_array['pan_card_file_path'] = $session_data['pan_card_file_name'];
						$people_information_insert_array['profile_pic_file_path'] = $session_data['profile_pic_file_name'];
						// echo "<pre>";print_r($people_information_insert_array);echo"</pre><hr>";die;
						$people_id = $this->hr_model->insert_data('people_information',$people_information_insert_array);
					// END	

					// preparing insert array for inserting into people information table
						$people_reference_insert_array = array();
						$reference_static_array = array('reference_name','relation', 'address', 'number', 'alternate_number', 'email');
						for ($i=1; $i < 6; $i++) { 
							
							if(!empty($form_data['reference_name_'.$i]) || !empty($form_data['relation_'.$i]) || !empty($form_data['address_'.$i])
								|| !empty($form_data['number_'.$i]) || !empty($form_data['alternate_number_'.$i]) || !empty($form_data['email_'.$i])) {

								$people_reference_insert_array[$i]['people_id'] = $people_id;
								foreach ($reference_static_array as $reference_name) {
									
									$people_reference_insert_array[$i][$reference_name] = $form_data[$reference_name.'_'.$i];
								}

							}
						}
						if(!empty($people_reference_insert_array)) {
							// echo "<pre>";print_r($people_reference_insert_array);echo"</pre><hr>";
							$this->hr_model->insert_data_batch('people_reference',$people_reference_insert_array);
						}

					// END	

					// preparing insert array for inserting into people address table
						
						if(!empty($form_data['resident_address_1']) || !empty($form_data['resident_address_2']) || !empty($form_data['resident_postcode'])
							|| !empty($form_data['resident_city']) || !empty($form_data['resident_state']) || !empty($form_data['resident_country'])) {

							$people_address_insert_array = array();
							$people_address_insert_array['people_id'] = $people_id;
							$people_address_insert_array['address_type'] = 'resident';
							$people_address_insert_array['address'] = $form_data['resident_address_1'].', '.$form_data['resident_address_2'];
							$people_address_insert_array['pin_code'] = $form_data['resident_postcode'];
							$people_address_insert_array['city'] = $form_data['resident_city'];
							$people_address_insert_array['state'] = $form_data['resident_state'];
							$people_address_insert_array['country'] = $form_data['resident_country'];
							// echo "<pre>";print_r($people_address_insert_array);echo"</pre><hr>";
							$this->hr_model->insert_data('people_address',$people_address_insert_array);
						}
						if(!empty($form_data['actual_address_1']) || !empty($form_data['actual_address_2']) || !empty($form_data['actual_postcode'])
							|| !empty($form_data['actual_city']) || !empty($form_data['actual_state']) || !empty($form_data['actual_country'])) {

							$people_address_insert_array = array();
							$people_address_insert_array['people_id'] = $people_id;
							$people_address_insert_array['address_type'] = 'actual';
							$people_address_insert_array['address'] = $form_data['actual_address_1'].', '.$form_data['actual_address_2'];
							$people_address_insert_array['pin_code'] = $form_data['actual_postcode'];
							$people_address_insert_array['city'] = $form_data['actual_city'];
							$people_address_insert_array['state'] = $form_data['actual_state'];
							$people_address_insert_array['country'] = $form_data['actual_country'];
							// echo "<pre>";print_r($people_address_insert_array);echo"</pre><hr>";
							$this->hr_model->insert_data('people_address',$people_address_insert_array);
						}
					// END		

					// preparing insert array for inserting into bank details table

						$bank_details_insert_array = array();
						$bank_details_insert_array['people_id'] = $people_id;
						$bank_details_insert_array['bank_name'] = $form_data['bank_name'];
						$bank_details_insert_array['branch_name'] = $form_data['branch_name'];
						$bank_details_insert_array['beneficiary_name'] = $form_data['beneficiary_name'];
						$bank_details_insert_array['account_number'] = $form_data['account_number'];
						$bank_details_insert_array['ifsc_code'] = $form_data['ifsc_code'];
						// echo "<pre>";print_r($bank_details_insert_array);echo"</pre><hr>";
						$this->hr_model->insert_data('bank_details',$bank_details_insert_array);
					// END		

					break;	

				case 'check_user_exist_or_not':
					
					$first_name =  $this->input->post('first_name');
					$last_name =  $this->input->post('last_name');
					$get_count = $this->hr_model->get_dynamic_data('id', array('status' => 'Active', 'first_name' => $first_name, 'last_name' => $last_name), 'people_information', 'row_array');
					$response['user_exists'] = false;
					if(!empty($get_count)) {

						$response['user_exists'] = true;
					}
					break;

				case 'paggination_filter_data':
					
					$where = array('status' => "'Active'");
					$limit = 10;
					$offset = 0;
					$employee_id = 0;
					$role_name = '';
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					if(!empty($this->input->post('search_form_data'))) {
						$form_data = array_column($this->input->post('search_form_data'), 'value', 'name');
						if(!empty($form_data['employee_id'])) {
							$employee_id = $where['id'] = $form_data['employee_id'];		
						}
						if(!empty($form_data['role_name'])) {
							$role_name = $form_data['role_name'];		
							$where['department'] = "'".$form_data['role_name']."'";		
						}
					}
					if(!empty($this->input->post('limit'))) {

						$limit = $this->input->post('limit');
					}
					if(!empty($this->input->post('offset'))) {

						$offset = $this->input->post('offset');
					}
					// echo "<pre>";print_r($where);echo"</pre><hr>";exit;
					$data = $this->create_employee_listing_data($where, array(), $limit, $offset, array('employee_id' => $employee_id, 'role_name' => $role_name));
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
					$response['html_user_list'] = $this->load->view('hr/hr_user_list', $data, true);
					$response['html_paggination'] = $this->load->view('hr/hr_paggination', $data, true);
					$response['html_user_search_form'] = $this->load->view('hr/hr_search_filter_form', $data, true);
					break;	

				case 'employee_update_account_information':
				case 'employee_update_personal_information':
				case 'employee_update_address_information':
				case 'employee_update_official_information':
				case 'employee_update_bank_information':
				case 'employee_update_reference_information':
					
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					$people_id = $this->input->post('people_id');
					$update_array = array_column($this->input->post('update_form_data'), 'value', 'name');
					if(!empty($update_array)) {
						if(!empty($update_array['birth_date'])) {
							$update_array['birth_date'] = date('Y-m-d',strtotime($update_array['birth_date']));
						}
						if(in_array($call_type, array('employee_update_account_information', 'employee_update_personal_information', 'employee_update_official_information'))) {
							$this->hr_model->update_data('people_information', $update_array, array('id'=>$people_id));

						} elseif ($call_type == 'employee_update_address_information') {
							$this->hr_model->update_data('people_address', $update_array, array('people_id'=>$people_id, 'address_type' => 'resident'));

						} elseif ($call_type == 'employee_update_bank_information') {
							$this->hr_model->update_data('bank_details', $update_array, array('people_id'=>$people_id));

						} elseif ($call_type == 'employee_update_reference_information') {
							if(!empty($update_array['id_1'])) {
								$this->hr_model->update_data(
															'people_reference',
															array(
																'reference_name' => $update_array['reference_name_1'],
																'relation' => $update_array['relation_1'],
																'address' => $update_array['address_1'],
																'number' => $update_array['number_1'],
																'alternate_number' => $update_array['alternate_number_1'],
																'email' => $update_array['email_1']
															),
															array(
																'id' => $update_array['id_1'],
																'people_id'=>$people_id
															)
														);
							} else {
								$this->hr_model->insert_data('people_reference',array('people_id' => $people_id, 'reference_name' => $update_array['reference_name_1'], 'relation' => $update_array['relation_1'], 'address' => $update_array['address_1'], 'number' => $update_array['number_1'], 'alternate_number' => $update_array['alternate_number_1'], 'email' => $update_array['email_1']));
							}
							if(!empty($update_array['id_2'])) {

								$this->hr_model->update_data(
															'people_reference',
															array(
																'reference_name' => $update_array['reference_name_2'],
																'relation' => $update_array['relation_2'],
																'address' => $update_array['address_2'],
																'number' => $update_array['number_2'],
																'alternate_number' => $update_array['alternate_number_2'],
																'email' => $update_array['email_2']
															),
															array(
																'id' => $update_array['id_2'],
																'people_id'=>$people_id
															)
														);
							} else {
								$this->hr_model->insert_data('people_reference',array('people_id' => $people_id, 'reference_name' => $update_array['reference_name_2'], 'relation' => $update_array['relation_2'], 'address' => $update_array['address_2'], 'number' => $update_array['number_2'], 'alternate_number' => $update_array['alternate_number_2'], 'email' => $update_array['email_2']));
							}
						}
					}
					break;
				
				case 'update_leave_request':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					if(!empty($this->input->post('id')) && !empty($this->input->post('request_status'))) {
						$this->hr_model->update_data('people_request_info', array('request_status'=>$this->input->post('request_status')), array('id'=> $this->input->post('id')));
						if($this->input->post('request_status') == 'Accepted') {
							$this->add_leave_month_wise($this->input->post('id'));
						}
						$response['request_status'] = $this->input->post('request_status');
					}
				break;

				case 'change_tab':
				case 'change_user_tab':

					$data =  $this->prepare_leave_data($this->input->post('user_id'), $this->input->post('request_status'));
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
					$response['leave_list_html'] = $this->load->view('hr/leave_request_list', $data, true);
					// echo "<pre>";print_r($response['leave_list_html']);echo"</pre><hr>";exit;
				break;

				case 'people_leaves_info_id':

					$people_leaves_info_id=  $this->hr_model->get_dynamic_data('id', array('people_id'=>$this->session->userdata('user_id')), 'people_leaves_info', 'row_array');
					$response['people_leaves_info_id_value_html']='<input type="hidden" class="form-control people_leaves_info_id" name="people_leaves_info_id" value="'.$people_leaves_info_id['id'].'"></input>';

				break;

				case 'leave_count':
                      $people_info = $this->hr_model->get_dynamic_data('*', array('people_id'=>$this->session->userdata('user_id')), 'people_leaves_info', 'row_array');
                      $people_info['leave'] = $people_info['total_leave_available']-$people_info['total_leave_taken'];
                      if($people_info['leave']<00.00){
                                     $response['leave_count_html'] = '00.00' ;
                                }else{
                                 $response['leave_count_html'] = $people_info['leave'];

                      }

				          //echo "<pre>";print_r($response);echo"</pre>";

				break;

				case 'add_request':
					
					// echo "<pre>";print_r($this->input->post('form_data'));echo"</pre><hr>";
					
					$insert_array = array();
					if(!empty($this->input->post('form_data'))) {

						foreach ($this->input->post('form_data') as $single_field_details) {
							
							if($single_field_details['name'] != 'date') {

								$insert_array[$single_field_details['name']] = trim($single_field_details['value']);
							}else{

								$date = explode(' / ',$single_field_details['value']);
								$insert_array['start_date'] = date('Y-m-d 00:00:00', strtotime($date[0]));
								$insert_array['end_date'] = date('Y-m-d 00:00:00', strtotime($date[1]));
							}
						}
					}
                  	$people_info = $this->hr_model->get_dynamic_data('id', array('status'=>'Active', 'year'=>date('Y'), 'people_id'=>$this->session->userdata('user_id')), 'people_leaves_info', 'row_array');
                  	if(!empty($people_info)) {

						$insert_array['people_leaves_info_id'] = $people_info['id'];
						// echo "<pre>";print_r($insert_array);echo"</pre><hr>";exit;
						if(!empty($insert_array)) {
							$this->hr_model->insert_data('people_request_info', $insert_array);
                  		}
					}			
				break;
				
				case 'salary_slip_default_form_submit':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					$form_data = array_column($this->input->post('form_data'), 'value', 'name');
					if(!empty($form_data['people_id'])) {
						
						// echo "<pre>";print_r($form_data);echo"</pre><hr>";exit;
						// checking record exist or not
						$salary_slip_default_details =  $this->hr_model->get_dynamic_data('salary_slip_default_id', array('status' => 'Active', 'people_id'=>$form_data['people_id']), 'salary_slip_default');

						// echo "<pre>";print_r($salary_slip_default_details);echo"</pre><hr>";exit;
						// if record is not found then insert new
						// else update the records
						if(empty($salary_slip_default_details)) {
							// die('came in');

							$this->hr_model->insert_data('salary_slip_default', $form_data);
						} else {

							$this->hr_model->update_data_batch('salary_slip_default', array(0=>$form_data), 'people_id');
						}
					}
				break;

				case 'get_salary_slip_default_data_on_people_id':
					
					$data = array();
					if(!empty($this->input->post('people_id'))) {

						$salary_slip_default_column = $this->hr_model->get_column_name('salary_slip_default');
						$data['salary_slip_default_details'] = array();
						$data['salary_slip_default_details'] = $this->hr_model->get_dynamic_data('*', array('status' => 'Active', 'people_id'=>$this->input->post('people_id')), 'salary_slip_default', 'row_array');
						// echo "<pre>";print_r($salary_slip_default_column);echo"</pre><hr>";
						$data['salary_slip_form_column'][] = $salary_slip_default_column[2];
						for ($i=3; $i < count($salary_slip_default_column)-4 ; $i=$i+2) { 
							
							$data['salary_slip_form_column'][] = $salary_slip_default_column[$i];
						}
						// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
						$response['salary_slip_default_column_div'] = $this->load->view('hr_salary_slip_default_column_div', $data, true);
					}

				break;

				case 'update_salary_slip':
				  $data = array_column($this->input->post('form_data'), 'value', 'name');
				   $insert_array = array();
				    foreach( $this->input->post('form_data') as $single_feild_details){
				    	$insert_array[$single_feild_details['name']]=trim($single_feild_details['value']);
				    }
					//$salary_slip_default_id = $this->uri->segment(3, 0);
			    	if(!empty($data['people_id'])) {
			    		$this->hr_model->update_data('salary_slip_default',$insert_array,array('people_id'=>$data['people_id']));
			    	}else{
			    	$this->hr_model->insert_data('salary_slip_default',$insert_array);
                         }
                break;

                case'salary_slip_default_list_delete':

                    $this->hr_model->update_data('salary_slip_default',array('status'=>'inactive'),array('salary_slip_id'=>$this->input->post('salary_slip_id')));
				break;

				default:
					$response['status'] = 'failed';	
					$response['message'] = "call type not found";
					break;
			}
			echo json_encode($response);

		} else{
			die('access is not allowed to this function');
		}
	}

	// private function
	
	private function upload_doc_config($doc_type) {

		$config['upload_path']          = './assets/hr_document/'.$doc_type.'/';
        $config['allowed_types']        = 'pdf|jpeg|gif|jpg|png';//'jpeg|gif|jpg|png';
        $config['file_name']            = $doc_type.'_'.$this->session->userdata('user_id');
        $config['overwrite']            = TRUE;
        
        $this->load->library('upload', $config);

		$data = array('status' => 'success', 'msg' => '', 'file_name' => '');
        if ( ! $this->upload->do_upload('file'))
        {
			$error = array('error' => $this->upload->display_errors());
			$data['status'] = 'failed';
			$data['msg'] = $error['error'];
        }
        else
        {
        	$file_dtls = $this->upload->data();
			$data['file_name'] = $file_dtls['file_name'];
        }
        // echo "<pre>";print_r($data);echo"</pre><hr>";exit;
        return $data;
	}	

	private function create_employee_listing_data($where, $order_by, $limit, $offset, $search_filter_array = array()) {

		$return_array = array();
		$return_array = $this->hr_model->get_user_listing($where, $order_by, $limit, $offset);
		$return_array['paggination_data']['limit'] = $limit;
		$return_array['paggination_data']['offset'] = $offset;
		$return_array['employee_list'] = $this->hr_model->get_dynamic_data('id, first_name, last_name', array('status' => 'Active'), 'people_information');
		$return_array['department_list'] = $this->hr_model->get_dynamic_data('*', array(), 'role');
		$return_array['search_filter']['employee_id'] = (int) $search_filter_array['employee_id'];
		$return_array['search_filter']['role_name'] = $search_filter_array['role_name'];
		return $return_array;
	}

	private function prepare_leave_data($people_id, $request_status = 'leave_request') {

		$return_array = array();
		$leave_info = $this->hr_model->get_people_leave_information($people_id);
		// echo "<pre>";print_r($leave_info);echo"</pre><hr>";exit;
		if(!empty($leave_info)) {

			$leave_user_id_array = array();
			$leave_info_id_array = array();
			$total_leave_info_array = array();
			foreach($leave_info as $info_key => $info_details) {
				$leave_user_id_array[$info_details['id']] = $info_details['people_id'];
				$leave_info_id_array[$info_key] = $info_details['id'];
				$total_leave_info_array[$info_details['id']]['total_leave_taken'] = $info_details['total_leave_taken'];
				$total_leave_info_array[$info_details['id']]['total_leave_available'] = $info_details['total_leave_available'];
			}		
			$where_string = "status = 'Active'";
			$where_string .= " AND leave_request_type ='".$request_status."'";
			if(!empty($leave_info_id_array)) {

				$where_string .= " AND people_leaves_info_id IN (".implode(',', $leave_info_id_array).")";
			}
			$request_info = $this->hr_model->get_leave_request_info($where_string);
			$user_details = array_column($this->hr_model->get_dynamic_data('*', array('status'=>1), 'users'), 'name', 'user_id');
			foreach($request_info as $request_info_key => $request_info_details) {
				$return_array[$request_info_key]['request_info_id'] = $request_info_details['id'];
				$return_array[$request_info_key]['request_type'] = ucwords(str_replace("_", " ", $request_info_details['leave_request_type']));
				$return_array[$request_info_key]['profile_pic'] = "profile_pic_".$leave_user_id_array[$request_info_details['people_leaves_info_id']].".jpeg";
				$return_array[$request_info_key]['profile_name'] = $user_details[$leave_user_id_array[$request_info_details['people_leaves_info_id']]];
				$return_array[$request_info_key]['leave_count'] = $this->create_leave_count($request_info_details['start_date'], $request_info_details['end_date']);
				$return_array[$request_info_key]['leave_add_day'] = $this->leave_added_before_days($request_info_details['add_time'], date('Y-m-d'));
				$return_array[$request_info_key]['leave_start_date_end_date'] = date('F d', strtotime($request_info_details['start_date'])).' - '.date('F d Y', strtotime($request_info_details['end_date']));
				$return_array[$request_info_key]['total_leave_taken'] = $total_leave_info_array[$request_info_details['people_leaves_info_id']]['total_leave_taken'];
				$return_array[$request_info_key]['total_leave_available'] = $total_leave_info_array[$request_info_details['people_leaves_info_id']]['total_leave_available'];
				$return_array[$request_info_key]['reason'] = $request_info_details['reason'];
				$return_array[$request_info_key]['request_status'] = $request_info_details['request_status'];
				$return_array[$request_info_key]['request_status_color'] = $this->create_request_status_color($request_info_details['request_status']);
				$return_array[$request_info_key]['action_button_visible'] = false;
				if($request_info_details['request_status'] == 'Pending' && $request_info_details['reporting_manager'] == $this->session->userdata('user_id')) {
					if(date('M', strtotime($request_info_details['start_date'])) == date('m') || true) {

						$return_array[$request_info_key]['action_button_visible'] = true;
					}
				}
			}
		}
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		return array(
				'leaves_details'=>$return_array,
				'reporting_manager_details' => $this->hr_model->get_reporting_manager_details($this->session->userdata('user_id')),
				// adding current person id and all id which report to the current user id
				'all_reporing_id' => array_merge(array(array('user_id'=>$this->session->userdata('user_id'),'name'=>$this->session->userdata('name'))), $this->hr_model->get_reporting_person_id($this->session->userdata('user_id'))),
				'leave_balance_details' => $this->hr_model->get_dynamic_data('*', array('status'=>'Active', 'year'=>date('Y'), 'people_id'=>$this->session->userdata('user_id')), 'people_leaves_info', 'row_array')
			);
	}

	private function leave_added_before_days($start_date, $end_date) {

		$diff = abs(strtotime($end_date) - strtotime($start_date.' - 1 day'));
		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
		if(!empty($months)) {
			return $months." Months and ".$days." Day's Ago";
		}
		return $days." Day's Ago";
	}

	private function create_leave_count($start_date, $end_date) {

		if($start_date != $end_date) {

			$holiday_count = $this->get_sunday_second_count_of_given_month($start_date, $end_date);
			// echo 'start date:-->',$start_date, 'end date:-->', $end_date, 'holiday_count:-->', $holiday_count,"<hr>";
			$diff = abs(strtotime($end_date) - strtotime($start_date.' - 1 day'));
			$years = floor($diff / (365*60*60*24));
			$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
			return $days-$holiday_count;
		} 
		return 1;
	}

	private function get_sunday_second_count_of_given_month($start_date, $end_date) {

		$holiday_count = 0;
		$second_sat_month_string = 'second sat of '.date('F Y', strtotime($start_date));
		$second_sat_date =  date('d', strtotime($second_sat_month_string));
		if($second_sat_date > date('d', strtotime($start_date)) && $second_sat_date < date('d', strtotime($end_date))) {
 
			$holiday_count++;
		}
		foreach(array('first','second','third','fourth','fifth') as $sunday_number) {

			$sunday_date = date('d', strtotime($sunday_number.' sun of '.date('F Y', strtotime($start_date))));
			// echo $sunday_number.' Sunday :->'.$sunday_date,"<hr>";
			if($sunday_date >= date('d', strtotime($start_date)) && $sunday_date <= date('d', strtotime($end_date))) {

				$holiday_count++;
			}
			if($sunday_date+7 > 31){
				break;
			}
		}
		// get paid holiday count
		$holiday_detail = $this->hr_model->get_dynamic_data('*', array('date >='=> $start_date, 'date <='=> $end_date), 'holiday_list');
		if(!empty($holiday_detail)) {
			// echo "<pre>";print_r($holiday_detail);echo"</pre><hr>";
			$holiday_count = $holiday_count + (count($holiday_detail));	
		}
		return $holiday_count;
	}

	private function create_request_status_color($request_status) {

		$return_color_string = 'bg-warning';
		if($request_status == 'Accepted') {

			$return_color_string = 'bg-success';
		}else if ($request_status == 'Not Accepted') {
			
			$return_color_string = 'bg-danger';
			
		}else if ($request_status == 'cancel') {

			$return_color_string = 'bg-primary';
		}
		return $return_color_string;
	}

	private function add_leave_month_wise($request_info_id) {

		$update_array = array();
		$request_info_details = $this->hr_model->get_dynamic_data('*', array('status'=>'Active', 'id'=>$request_info_id), 'people_request_info', 'row_array');
		//getting people leave info details
		$people_leave_info_details =  $this->hr_model->get_dynamic_data("*", array('status'=>'Active', 'id'=>$request_info_details['people_leaves_info_id']), 'people_leaves_info', 'row_array');
		if($request_info_details['leave_request_type'] == 'leave_request') {

			if(date('m', strtotime($request_info_details['start_date'])) == date('m', strtotime($request_info_details['end_date']))) {
				$column_name = lcfirst(date('F', strtotime($request_info_details['start_date'])).'_total_leave');
				$no_of_days = $this->create_leave_count($request_info_details['start_date'], $request_info_details['end_date']);
				$update_array = array(
									'total_leave_taken'=> (
										$people_leave_info_details['total_leave_taken']+$no_of_days
									),
									$column_name => (
										$people_leave_info_details[$column_name]+$no_of_days
									)
								);
			}else {

				$start_column_name = lcfirst(date('F', strtotime($request_info_details['start_date'])).'_total_leave');
				$start_no_of_days = $this->create_leave_count($request_info_details['start_date'], date('Y-m-t', strtotime($request_info_details['start_date'])));

				$end_column_name = lcfirst(date('F', strtotime($request_info_details['end_date'])).'_total_leave');
				$end_no_of_days = $this->create_leave_count(date('Y-m-1', strtotime($request_info_details['end_date'])), $request_info_details['end_date']);
				$update_array = array(
									'total_leave_taken'=> (
										$people_leave_info_details['total_leave_taken']+$start_no_of_days+$end_no_of_days
									),
									$start_column_name => (
										$people_leave_info_details[$start_column_name]+$start_no_of_days
									),	
									$end_column_name => (
										$people_leave_info_details[$end_column_name]+$end_no_of_days
									)
								);
			}
		} else if ($request_info_details['leave_request_type'] == 'compoff_request') {

			$no_of_days = $this->create_leave_count($request_info_details['start_date'], $request_info_details['end_date']);
			$update_array = array(
								'total_leave_available'=> (
									$people_leave_info_details['total_leave_available']+$no_of_days
								)
							);
		} else if (in_array($request_info_details['leave_request_type'], array('late_mark', 'early_leave'))) {
			
			//get total number of late mark
			$total_late_mark = $this->hr_model->get_dynamic_data('count(*) as total', array('status'=> 'Active', 'year'=> date('Y'), 'leave_request_type'=> 'late_mark', 'request_status'=> 'Accepted'), 'people_request_info','row_array');	
			//get total number of early leave
			$total_early_leave = $this->hr_model->get_dynamic_data('count(*) as total', array('status'=> 'Active', 'year'=> date('Y'), 'leave_request_type'=> 'early_leave', 'request_status'=> 'Accepted'), 'people_request_info','row_array');
			if( ($total_late_mark['total'] + $total_early_leave['total']) > 3) {
				$column_name = lcfirst(date('F', strtotime($request_info_details['start_date'])).'_total_leave');
				$update_array = array(
								'total_leave_taken'=> (
									$people_leave_info_details['total_leave_taken']+0.5
								),
								$column_name => (
									$people_leave_info_details[$column_name]+0.5
								)
							);
			}
			// echo "<pre>";print_r($total_late_mark);echo"</pre><hr>";	
			// echo "<pre>";print_r($total_early_leave);echo"</pre><hr>";
		}

		// echo "<pre>";print_r($update_array);echo"</pre><hr>";exit;
		if(!empty($update_array)) {

			$this->hr_model->update_data(
										'people_leaves_info',
										$update_array,
										array(
											'id'=> $request_info_details['people_leaves_info_id']
										)
									);
		}
	}

	private function prepare_salary_slip_data($user_id){

		$data = array();
		$month = '';
		$data['all_user_list'] = array_column($this->hr_model->get_dynamic_data('name, user_id', array('status'=>'1'), 'users'),'name', 'user_id');
		// getting users details
		$data['user_details'] = $this->hr_model->get_dynamic_data('user_id, name, joining_date', array('status'=>'1', 'user_id'=> $user_id), 'users', 'row_array');
		// getting people_information
		$data['people_information_details'] = $this->hr_model->get_dynamic_data('*', array('status'=>'Active', 'user_id'=> $user_id), 'people_information', 'row_array');
		// getting salary slip info details
		$salary_slip_info = $this->hr_model->get_dynamic_data('*', array('status'=>'Active', 'people_id'=> $user_id), 'salary_slip_info', 'result_array');
    	// $data['salary_income']=array();
    	// $data['salary_expences']=array();
    	$data['salary_year']=$this->hr_model->get_salary_year();
   		$data['salary_month'] = array();
        // echo "<pre>";print_r($salary_slip_info);echo"</pre><hr>";exit;
        if(!empty($salary_slip_info)) {
        	
		    foreach($salary_slip_info as $salary_key => $salary_details){
		    	
		    	if(empty($data['salary_month'][$salary_details['salary_month']]['total']['income'])) {

	        		$data['salary_month'][$salary_details['salary_month']]['total']['income'] = 0;
	        		$data['salary_month'][$salary_details['salary_month']]['total']['expense'] = 0;
		    	}
		    	if($salary_details['salary_slip_income_or_expense'] == 'income'){

		    		$data['salary_month'][$salary_details['salary_month']]['salary_income'][$salary_key]['name'] = $salary_details['salary_slip_name'];
		    		$data['salary_month'][$salary_details['salary_month']]['salary_income'][$salary_key]['value']=$salary_details['salary_slip_value'];
		    		$data['salary_month'][$salary_details['salary_month']]['total']['income'] += $salary_details['salary_slip_value']; 
		    	} else {

		    		$data['salary_month'][$salary_details['salary_month']]['salary_expences'][$salary_key]['name'] = $salary_details['salary_slip_name'];
		    		$data['salary_month'][$salary_details['salary_month']]['salary_expences'][$salary_key]['value']=$salary_details['salary_slip_value'];
		    		$data['salary_month'][$salary_details['salary_month']]['total']['expense'] += $salary_details['salary_slip_value']; 
		    	}
		    }       
        }

    	//echo "<pre>";print_r($data);echo"</pre><hr>";exit;

        return $data;
    }

    //cron function starts here

    public function insert_people_leave_info_data(){

    	$user_where['status'] = 1;
    	$user_where['role !='] = 1;
    	if($this->uri->segment('3') != '') {
    		$user_where['user_id'] = $this->uri->segment('3');
    	}
		$users = $this->hr_model->get_dynamic_data('user_id, joining_date', $user_where, 'users');
		// echo "<pre>";print_r($users);echo"</pre><hr>";exit;
	  	// starting month for jan is 1
        foreach ($users as $users_details) {

        	$insert_array =  array();	
			$total_leave_available = 0.0;
			// echo $users_details['joining_date'], "<hr>";
			if(date('Y', strtotime($users_details['joining_date'])) == date('Y')) {

				if(date('m', strtotime($users_details['joining_date'])) != date('m')) {
					$starting_month_leave = 1.5;
					$starting_month = date('m', strtotime($users_details['joining_date']));
					$day_of_joining = date('d', strtotime($users_details['joining_date']));
					if($day_of_joining > 10){

						$starting_month_leave = 1;
						if($day_of_joining > 20){
						
							$starting_month_leave = 0.5;
						}
					}
		        	$insert_array['people_id'] = $users_details['user_id'];
				  	// here Janurary month or first month of joining date is calculated 
		    		$insert_array[date('F', mktime(0, 0, 0, $starting_month, 1, date('y')))] = $starting_month_leave;
		    		$total_leave_available = $starting_month_leave;
		        	// here all month calculation is done
				 	for($i = $starting_month+1; $i < date('m');$i++){

		        		$insert_array[date('F', mktime(0, 0, 0, $i, 1, date('y')))] = 1.5;
		        		$total_leave_available = $total_leave_available + 1.5;
				  	}
		    		$insert_array['total_leave_available'] = $total_leave_available;
	    		}
			}else {

				$insert_array['people_id'] = $users_details['user_id'];
	        	// here all month calculation is done
			 	for($i = 1; $i < date('m');$i++){

	        		$insert_array[date('F', mktime(0, 0, 0, $i, 1, date('y')))] = 1.5;
	        		$total_leave_available = $total_leave_available + 1.5;
			  	}
	    		$insert_array['total_leave_available'] = $total_leave_available;
			}
		  	// echo "<pre>";print_r($insert_array);echo"</pre><hr>";
		  	if(!empty($insert_array)) {

		  		$this->hr_model->insert_data('people_leaves_info', $insert_array);
		  	}
        }
	  	die('done processs!!!');
	}

	public function insert_salary_slip_info_data($start_month = 1, $end_month = 1) {

		if($start_month != $end_month){
			die('Start Month should be equal to end month !!!');
		}
		$salary_slip_default = $this->hr_model->get_dynamic_data('*', array('status'=>'Active', 'year'=>date('Y')), 'salary_slip_default');
		$salary_slip_column = $this->hr_model->db->list_fields('salary_slip_default');
		$start_month = date('m', mktime(0, 0, 0, $start_month, 1, date('y')));
		$end_month = date('m', mktime(0, 0, 0, $end_month, 1, date('y')));
        foreach ($salary_slip_default as $salary_slip_default_key => $salary_slip_default_details) {
        
			// echo "<pre>";print_r($salary_slip_default_details);echo"</pre><hr>";
			$people_leave_info = $this->hr_model->get_dynamic_data('*', array('status'=>'Active', 'year'=>date('Y'), 'people_id' => $salary_slip_default_details['people_id']), 'people_leaves_info', 'row_array');
			// echo "<pre>";print_r($people_leave_info);echo"</pre><hr>";
			$total_pay_to_cut = 0;
			if($people_leave_info['total_leave_taken'] > $people_leave_info['total_leave_available']) {

				$total_number_of_paid_days_in_month = 26;
				if($start_month == 2){
					
					$total_number_of_paid_days_in_month = (date('t', mktime(0, 0, 0, $start_month, 1, date('y')))) - 4;
				}
				$no_of_day_pay_cut = $people_leave_info['total_leave_taken'] - $people_leave_info['total_leave_available'];
				$total_pay_to_cut = ceil(	
											(
												$salary_slip_default_details['total_salary'] 
												/ 
												$total_number_of_paid_days_in_month
											)
											*$no_of_day_pay_cut
										);
				// echo $total_pay_to_cut,"<hr>";die('debug');
			}
        	$insert_array =  array();
        	$j = 0;
        	for($month_number = $start_month; $month_number <= $end_month;$month_number++) {

        		$deducted_salary_slip_amount = 0;
	        	for ($i=3; $i<count($salary_slip_column)-4; $i = $i+2) { 
	        			
		        	$insert_array[$j] =  array();
		        	$insert_array[$j]['people_id'] = $salary_slip_default_details['people_id'];
		        	$insert_array[$j]['salary_month'] = date('F', mktime(0, 0, 0, $month_number, 1, date('y')));
		        	$insert_array[$j]['salary_slip_name'] = $salary_slip_column[$i];
	        		$insert_array[$j]['salary_slip_value'] = $salary_slip_default_details[$salary_slip_column[$i]];
		        	if($total_pay_to_cut > 0) {

		        		$currenct_amount_to_deduct = $this->prepare_salary_slip_amount($salary_slip_column[$i], $total_pay_to_cut, $deducted_salary_slip_amount);
	        			if(!empty($currenct_amount_to_deduct)){

	        				$insert_array[$j]['salary_slip_value'] = $salary_slip_default_details[$salary_slip_column[$i]] - $currenct_amount_to_deduct;
	        				$deducted_salary_slip_amount += $currenct_amount_to_deduct;
	        			}
		        	} 
		        	$insert_array[$j]['salary_slip_income_or_expense'] = $salary_slip_default_details[$salary_slip_column[$i].'_income_or_expense'];
		        	$insert_array[$j]['year'] = date('Y');
		        	$j++;
	        	}
	        }
	        $this->hr_model->insert_data_batch('salary_slip_info', $insert_array);
	    }
        die('done processs!!!');
	}

	private function prepare_salary_slip_amount($salary_slip_name, $total_pay_to_cut, $deducted_salary_slip_amount) {

		switch ($salary_slip_name) {
			case 'basic':
				$return_amount = (floor($total_pay_to_cut * (50/100)));
			break;

			case 'hra':
				$return_amount = (floor($total_pay_to_cut * (25/100)));
			break;

			case 'other_allowance':
				$return_amount = ($total_pay_to_cut - $deducted_salary_slip_amount);
			break;

			default:
				$return_amount = 0;
			break;
		}
		return $return_amount;
	}

	// per person salary slip defaut information is added
    public function add_salary_slip_default_details($salary_slip_id) {

    	$data = array();
    	$salary_slip_default_id = $this->uri->segment(3, 0);
    	if(!empty($salary_slip_default_id)) {
         $data['salary_slip_details'] = $this->hr_model->get_dynamic_data('*', array('people_id'=>$salary_slip_default_id), 'salary_slip_default');
    	//echo "<pre>";print_r($data['salary_slip_details']);echo"</pre><hr>";exit;
    	}
    	$data['user_details'] = $this->hr_model->get_dynamic_data('user_id, name', array('status'=>1), 'users');
    	 //echo "<pre>";print_r($data['salary_slip_details']);echo"</pre><hr>";exit;
    	$this->load->view('hr/hr_header', array('title' => 'Hr Listing'));
		$this->load->view('sidebar', array('title' => 'Hr Listing'));
		$this->load->view('hr/add_sal', $data);
		$this->load->view('hr/hr_footer');
    }

    public function create_people_information_user_id() {
    	$this->hr_model->create_people_information_user_id();
    }
	//cron function ends here
}