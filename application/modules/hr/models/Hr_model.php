<?php 
Class Hr_model extends CI_Model{

	function __construct(){
		parent::__construct();
		$CI = &get_instance();
		// $this->crm_sales_db = $CI->load->database('crm_sales', true);
		// echo "<pre>";print_r($this->crm_sales_db);echo"</pre><hr>";exit;
	}


	public function live_server_data($select, $where_array, $table_name) {

		// $this->crm_sales_db->select($select);
		// $this->crm_sales_db->where($where_array);
		// return $this->crm_sales_db->get($table_name)->result_array();
	}

	public function insert_data($table, $data){
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	public function update_data($table, $data, $where){
		return $this->db->update($table, $data, $where);
	}

	public function get_dynamic_data($select = '*', $where, $table_name, $return_type = 'result_array', $limit = 0, $offset = 0) {

		$this->db->select($select);
		$this->db->where($where);
		return $this->db->get($table_name, $limit, $offset)->$return_type();
	}

	public function insert_data_batch($table_name, $insert_data) {

		$this->db->insert_batch($table_name, $insert_data);
	}

	public function update_data_batch($table_name, $update_data, $where_string) {

		$this->db->update_batch($table_name, $update_data, $where_string);	
	}

	public function get_user_listing($where_array, $order_by_array, $limit, $offset) {

		$return_array = array();
		$this->db->select('SQL_CALC_FOUND_ROWS *', FALSE);
		if(!empty($where_array)) {
			$this->db->where($where_array, NULL, FALSE);
		}
		if(!empty($order_by_array)) {
			$this->db->order_by($order_by_array['column_name'], $order_by_array['column_value']);
		}
		$return_array['user_list'] = $this->db->get('people_information', $limit, $offset)->result_array();
		// echo $this->db->last_query(),"<hr>";die;
		$return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows;")->row_array();
		return $return_array;
	}

	public function get_single_user_details($people_id) {

		$this->db->select('*');
		$this->db->join('people_address', 'people_address.people_id = people_information.id', 'LEFT');
		$this->db->join('people_reference', 'people_reference.people_id = people_information.id', 'LEFT');
		$this->db->join('bank_details', 'bank_details.people_id = people_information.id', 'LEFT');
		$this->db->where(array('people_information.status' => 'Active', 'people_address.address_type' => 'resident', 'people_information.id'=>$people_id));
		return $this->db->get('people_information')->result_array();
	}
	public function get_leave_info() {

		return $this->db->get_where('people_leaves_info', array('status'=>'Active'))->result_array();
	}

	public function get_leave_request_info($where_string) {

		// $this->db->where_in('people_leaves_info_id',$people_id_array);
		$this->db->where($where_string, null, false);
		$res =  $this->db->get('people_request_info')->result_array();
		// echo $this->db->last_query(),"<hr>";
		// echo "<pre>";print_r($res);echo"</pre><hr>";exit;
		return $res;
	}

	public function get_reporting_user_details($role_ids = array()) {

		if(!empty($role_ids)) {

			$this->db->where_in('role', $role_ids);
			return $this->db->get_where('users', array('status'=> 1))->result_array();
		}
	}

	public function get_column_name($table_name) {

		return $this->db->list_fields($table_name);	
	}

	public function get_people_leave_information($reporting_id) {

		$this->db->where_in('people_id', $reporting_id);	
		$res = $this->db->get_where('people_leaves_info', array('status'=>'Active', 'year'=>date('Y')))->result_array();
		return $res;
	}

	public function get_reporting_person_id($user_id) {

		$this->db->select('users.user_id, users.name');
		$this->db->join('users', 'users.user_id = leave_request_reporting_manager.people_id', 'left');
		$result = $this->db->get_where('leave_request_reporting_manager', array('leave_request_reporting_manager.status'=>'Active', 'leave_request_reporting_manager.reporting_id' => $user_id))->result_array();
		return $result;
	}
	
	public function get_reporting_manager_details($user_id) {

		$this->db->select('users.user_id, users.name');
		$this->db->join('users', 'users.user_id = leave_request_reporting_manager.reporting_id', 'left');
		$result = $this->db->get_where('leave_request_reporting_manager', array('leave_request_reporting_manager.status'=>'Active', 'leave_request_reporting_manager.people_id' => $user_id))->row_array();
		return $result;
	}
	public function get_salary_year(){
    	$this->db->select('year');
    	$this->db->group_by('salary_slip_info.year','ASC');
    	return $res= $this->db->get('salary_slip_info')->result_array();   	
    	//echo "<pre>";print_r($res);echo"</pre><hr>";exit;
    }
	//cron function start
	public function create_people_information_user_id() {

		$res = $this->db->get_where('people_information', array('status'=>'Active'))->result_array();
		// echo "<pre>";print_r($res);echo"</pre><hr>";
		foreach ($res as $key => $value) {
			$update_array = $this->getting_people_id_from_string($value['aadhar_card_file_path'], $value['pan_card_file_path'], $value['profile_pic_file_path']);
			echo "<pre>";print_r($update_array);echo"</pre><hr>";
			if(!empty($update_array)) {

				$this->db->update('people_information', $update_array, array('id'=>$value['id']));
			}
		}
		die('done process!!!');
	}

	private function getting_people_id_from_string($aadhar_card_path, $pan_card_path, $profile_pic_path) {

		$return_array = array();
	
		if(!empty($aadhar_card_path)) {
		
			$string_without_dot = explode('.', $aadhar_card_path);
			$string_without_underscore = explode('_', $string_without_dot[0]);
			unset($string_without_underscore[3]);
			$return_array['aadhar_card_file_path'] = implode('_', $string_without_underscore).'.'.$string_without_dot[1];
		}

		if(!empty($pan_card_path)) {
		
			$string_without_dot = explode('.', $pan_card_path);
			$string_without_underscore = explode('_', $string_without_dot[0]);
			unset($string_without_underscore[3]);
			$return_array['pan_card_file_path'] = implode('_', $string_without_underscore).'.'.$string_without_dot[1];
		}

		if(!empty($profile_pic_path)) {

			$string_without_dot = explode('.', $profile_pic_path);
			$string_without_underscore = explode('_', $string_without_dot[0]);
			unset($string_without_underscore[3]);
			$return_array['profile_pic_file_path'] = implode('_', $string_without_underscore).'.'.$string_without_dot[1];
		}
		if(!empty($string_without_underscore)) {

			$return_array['user_id'] = $string_without_underscore[2];
		}
		
		return $return_array;
	}

	//cron function end

}