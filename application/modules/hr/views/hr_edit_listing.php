<style type="text/css">
    .kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
        padding-top: 4% !important;
    }
    .layer-white{
        display: none;
        position: absolute;
        top: 0em !important;
        left: 0em !important;
        width: 100%;
        height: 100%;
        text-align: center;
        vertical-align: middle;
        background-color: rgba(255, 255, 255, 0.55);
        opacity: 1;
        line-height: 1;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
        -webkit-animation-duration: 0.5s;
        animation-duration: 0.5s;
        -webkit-transition: background-color 0.5s linear;
        transition: background-color 0.5s linear;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        will-change: opacity;
        z-index: 9;
    }
    .div-loader{
        position: absolute;
        top: 50%;
        left: 50%;
        margin: 0px;
        text-align: center;
        z-index: 1000;
        -webkit-transform: translateX(-50%) translateY(-50%);
        transform: translateX(-50%) translateY(-50%);
    }
    .kt-spinner:before {
        width: 50px;
        height: 50px;
        margin-top: -10px;
    }
    .kt-aside--fixed .kt-wrapper {
        padding-left: 130px !important;
    }
    .kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper {
        padding-top: 30px !important;
    }
    .salary_slip thead tr th{
        background-color: transparent;
        padding: 1rem 0 0.5rem 0;
        color: #74788d;
        border-top: 0;
        border-bottom: 1px solid #ebedf2;
    }
    .salary_slip tbody tr td{
        background-color: transparent;
        padding: 1rem 0 1rem 0;
        border-top: none;
        font-weight: 700;
        font-size: 1.1rem;
        color: #595d6e;
        padding-top: 1.8rem;
    }
    .grand_total thead tr th{
        font-size: 1.1rem;
        text-transform: capitalize;
        font-weight: 500;
        color: #74788d;
        border-top: 0;
        border-bottom: 1px solid #ebedf2;
        padding: 10px 10px 10px 0;
        background-color: transparent;
    }
    .grand_total tbody tr td{

        font-size: 1.1rem;
        text-transform: capitalize;
        background-color: transparent;
        font-weight: 500;
        color: #595d6e;
        padding: 10px 10px 10px 0;
    }
</style>
<!-- end:: Header Mobile -->
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

               <!-- begin:: Content -->
               <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

                   <!--Begin::App-->
                   <div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">

                       <!--Begin:: App Aside Mobile Toggle-->
                       <button class="kt-app__aside-close" id="kt_user_profile_aside_close">
                           <i class="la la-close"></i>
                       </button>

                       <!--End:: App Aside Mobile Toggle-->

                       <!--Begin:: App Aside-->
                       <div class="kt-grid__item kt-app__toggle kt-app__aside" id="kt_user_profile_aside">

                           <!--begin:: Widgets/Applications/User/Profile1-->
                           <div class="kt-portlet " style="height: 680px;">
                               <div class="kt-portlet__head  kt-portlet__head--noborder"></div>
                               <div class="kt-portlet__body kt-portlet__body--fit-y">

                                   <!--begin::Widget -->
                                   <div class="kt-widget kt-widget--user-profile-1">
                                       <div class="kt-widget__head">
                                           <div class="kt-widget__media">
                                               <img src="<?php echo base_url('assets/hr_document/profile_pic/'.$user_details['profile_pic_file_path']);?>" alt="image">
                                           </div>
                                           <div class="kt-widget__content">
                                               <div class="kt-widget__section">
                                                   <a href="javascript:void(0);" class="kt-widget__username">
                                                       <?php echo $user_details['first_name'], ' ', $user_details['last_name']; ?>
                                                       <i class="flaticon2-correct kt-font-success"></i>
                                                   </a>
                                                   <span class="kt-widget__subtitle">
                                                       <?php echo $user_details['department']; ?>
                                                   </span>
                                               </div>
                                               <div class="kt-widget__action" style="display: none;">
                                                   <button type="button" class="btn btn-info btn-sm">chat</button>&nbsp;
                                                   <button type="button" class="btn btn-success btn-sm">follow</button>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="kt-widget__body">
                                           <div class="kt-widget__content">
                                               <div class="kt-widget__info">
                                                   <span class="kt-widget__label">Email:</span>
                                                   <a href="javascript:void(0);" class="kt-widget__data"><?php echo $user_details['official_email']; ?></a>
                                               </div>
                                               <div class="kt-widget__info">
                                                   <span class="kt-widget__label">Phone:</span>
                                                   <a href="javascript:void(0);" class="kt-widget__data"><?php echo $user_details['official_number']; ?></a>
                                               </div>
                                               <div class="kt-widget__info">
                                                   <span class="kt-widget__label">Location:</span>
                                                   <span class="kt-widget__data"><?php echo $user_details['job_location']; ?></span>
                                               </div>
                                           </div>
                                           <div class="kt-widget__items">
                                               <a href="<?php echo base_url('hr/edit_listing/account_setting/'.$this->uri->segment(4, 0));?>" class="kt-widget__item update_information_tab <?php echo ($this->uri->segment(3) == 'account_setting') ? 'kt-widget__item--active':'';?>">
                                                   <span class="kt-widget__section">
                                                       <span class="kt-widget__icon">
                                                           <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                               <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                   <polygon points="0 0 24 0 24 24 0 24" />
                                                                   <path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero" />
                                                                   <path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3" />
                                                               </g>
                                                           </svg> </span>
                                                       <span class="kt-widget__desc">
                                                           Account Setting
                                                       </span>
                                                   </span>
                                               </a>
                                               <a href="<?php echo base_url('hr/edit_listing/personal_information/'.$this->uri->segment(4, 0));?>" class="kt-widget__item update_information_tab <?php echo ($this->uri->segment(3) == 'personal_information') ? 'kt-widget__item--active':'';?>">
                                                   <span class="kt-widget__section">
                                                       <span class="kt-widget__icon">
                                                           <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                               <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                   <polygon points="0 0 24 0 24 24 0 24" />
                                                                   <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                                   <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                                                               </g>
                                                           </svg> </span>
                                                       <span class="kt-widget__desc">
                                                           Personal Information
                                                       </span>
                                                   </span>
                                               </a>
                                               <a href="<?php echo base_url('hr/edit_listing/address/'.$this->uri->segment(4, 0));?>" class="kt-widget__item update_information_tab  <?php echo ($this->uri->segment(3) == 'address') ? 'kt-widget__item--active':'';?>">
                                                   <span class="kt-widget__section">
                                                       <span class="kt-widget__icon">
                                                           <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                               <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                   <rect x="0" y="0" width="24" height="24" />
                                                                   <path d="M2.56066017,10.6819805 L4.68198052,8.56066017 C5.26776695,7.97487373 6.21751442,7.97487373 6.80330086,8.56066017 L8.9246212,10.6819805 C9.51040764,11.267767 9.51040764,12.2175144 8.9246212,12.8033009 L6.80330086,14.9246212 C6.21751442,15.5104076 5.26776695,15.5104076 4.68198052,14.9246212 L2.56066017,12.8033009 C1.97487373,12.2175144 1.97487373,11.267767 2.56066017,10.6819805 Z M14.5606602,10.6819805 L16.6819805,8.56066017 C17.267767,7.97487373 18.2175144,7.97487373 18.8033009,8.56066017 L20.9246212,10.6819805 C21.5104076,11.267767 21.5104076,12.2175144 20.9246212,12.8033009 L18.8033009,14.9246212 C18.2175144,15.5104076 17.267767,15.5104076 16.6819805,14.9246212 L14.5606602,12.8033009 C13.9748737,12.2175144 13.9748737,11.267767 14.5606602,10.6819805 Z" fill="#000000" opacity="0.3" />
                                                                   <path d="M8.56066017,16.6819805 L10.6819805,14.5606602 C11.267767,13.9748737 12.2175144,13.9748737 12.8033009,14.5606602 L14.9246212,16.6819805 C15.5104076,17.267767 15.5104076,18.2175144 14.9246212,18.8033009 L12.8033009,20.9246212 C12.2175144,21.5104076 11.267767,21.5104076 10.6819805,20.9246212 L8.56066017,18.8033009 C7.97487373,18.2175144 7.97487373,17.267767 8.56066017,16.6819805 Z M8.56066017,4.68198052 L10.6819805,2.56066017 C11.267767,1.97487373 12.2175144,1.97487373 12.8033009,2.56066017 L14.9246212,4.68198052 C15.5104076,5.26776695 15.5104076,6.21751442 14.9246212,6.80330086 L12.8033009,8.9246212 C12.2175144,9.51040764 11.267767,9.51040764 10.6819805,8.9246212 L8.56066017,6.80330086 C7.97487373,6.21751442 7.97487373,5.26776695 8.56066017,4.68198052 Z" fill="#000000" />
                                                               </g>
                                                           </svg> </span>
                                                       <span class="kt-widget__desc">
                                                           Resident Information
                                                       </span>
                                                       </spandiv>
                                               </a>
                                               <a href="<?php echo base_url('hr/edit_listing/official_information/'.$this->uri->segment(4, 0));?>" class="kt-widget__item update_information_tab <?php echo ($this->uri->segment(3) == 'official_information') ? 'kt-widget__item--active':'';?>">
                                                   <span class="kt-widget__section">
                                                       <span class="kt-widget__icon">
                                                           <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                               <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                   <rect x="0" y="0" width="24" height="24" />
                                                                   <path d="M4,4 L11.6314229,2.5691082 C11.8750185,2.52343403 12.1249815,2.52343403 12.3685771,2.5691082 L20,4 L20,13.2830094 C20,16.2173861 18.4883464,18.9447835 16,20.5 L12.5299989,22.6687507 C12.2057287,22.8714196 11.7942713,22.8714196 11.4700011,22.6687507 L8,20.5 C5.51165358,18.9447835 4,16.2173861 4,13.2830094 L4,4 Z" fill="#000000" opacity="0.3" />
                                                                   <path d="M12,11 C10.8954305,11 10,10.1045695 10,9 C10,7.8954305 10.8954305,7 12,7 C13.1045695,7 14,7.8954305 14,9 C14,10.1045695 13.1045695,11 12,11 Z" fill="#000000" opacity="0.3" />
                                                                   <path d="M7.00036205,16.4995035 C7.21569918,13.5165724 9.36772908,12 11.9907452,12 C14.6506758,12 16.8360465,13.4332455 16.9988413,16.5 C17.0053266,16.6221713 16.9988413,17 16.5815,17 C14.5228466,17 11.463736,17 7.4041679,17 C7.26484009,17 6.98863236,16.6619875 7.00036205,16.4995035 Z" fill="#000000" opacity="0.3" />
                                                               </g>
                                                           </svg> </span>
                                                       <span class="kt-widget__desc">
                                                           Employee Details
                                                       </span>
                                                   </span>
                                                   <span class="kt-badge kt-badge--unified-danger kt-badge--sm kt-badge--rounded kt-badge--bolder">5</span>
                                               </a>
                                               <a href="<?php echo base_url('hr/edit_listing/bank_details/'.$this->uri->segment(4, 0));?>" class="kt-widget__item update_information_tab <?php echo ($this->uri->segment(3) == 'bank_details') ? 'kt-widget__item--active':'';?>">
                                                   <span class="kt-widget__section">
                                                       <span class="kt-widget__icon">
                                                           <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                               <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                   <rect x="0" y="0" width="24" height="24" />
                                                                   <path d="M6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,12 C19,12.5522847 18.5522847,13 18,13 L6,13 C5.44771525,13 5,12.5522847 5,12 L5,3 C5,2.44771525 5.44771525,2 6,2 Z M7.5,5 C7.22385763,5 7,5.22385763 7,5.5 C7,5.77614237 7.22385763,6 7.5,6 L13.5,6 C13.7761424,6 14,5.77614237 14,5.5 C14,5.22385763 13.7761424,5 13.5,5 L7.5,5 Z M7.5,7 C7.22385763,7 7,7.22385763 7,7.5 C7,7.77614237 7.22385763,8 7.5,8 L10.5,8 C10.7761424,8 11,7.77614237 11,7.5 C11,7.22385763 10.7761424,7 10.5,7 L7.5,7 Z" fill="#000000" opacity="0.3" />
                                                                   <path d="M3.79274528,6.57253826 L12,12.5 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 Z" fill="#000000" />
                                                               </g>
                                                           </svg> </span>
                                                       <span class="kt-widget__desc">
                                                           Bank Setting
                                                       </span>
                                                   </span>
                                               </a>
                                               <a href="<?php echo base_url('hr/edit_listing/reference/'.$this->uri->segment(4, 0));?>" class="kt-widget__item update_information_tab <?php echo ($this->uri->segment(3) == 'reference') ? 'kt-widget__item--active':'';?>">
                                                   <span class="kt-widget__section">
                                                       <span class="kt-widget__icon">
                                                           <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                               <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                   <rect x="0" y="0" width="24" height="24" />
                                                                   <rect fill="#000000" x="2" y="5" width="19" height="4" rx="1" />
                                                                   <rect fill="#000000" opacity="0.3" x="2" y="11" width="19" height="10" rx="1" />
                                                               </g>
                                                           </svg> </span>
                                                       <span class="kt-widget__desc">
                                                           Reference
                                                       </span>
                                                   </span>
                                               </a>
                                               <a href="<?php echo base_url('hr/edit_listing/leave_management/'.$this->uri->segment(4, 0));?>" class="kt-widget__item update_information_tab <?php echo ($this->uri->segment(3) == 'leave_management') ? 'kt-widget__item--active':'';?>">
                                                   <span class="kt-widget__section">
                                                       <span class="kt-widget__icon">
                                                           <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                               <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                   <rect x="0" y="0" width="24" height="24" />
                                                                   <rect fill="#000000" x="2" y="5" width="19" height="4" rx="1" />
                                                                   <rect fill="#000000" opacity="0.3" x="2" y="11" width="19" height="10" rx="1" />
                                                               </g>
                                                           </svg> </span>
                                                       <span class="kt-widget__desc">
                                                           Leave Management
                                                       </span>
                                                   </span>
                                               </a>
                                                <a href="<?php echo base_url('hr/edit_listing/salary_slip/'.$this->uri->segment(4, 0));?>" class="kt-widget__item update_information_tab <?php echo ($this->uri->segment(3) == 'salary_slip') ? 'kt-widget__item--active':'';?>">
                                                   <span class="kt-widget__section">
                                                       <span class="kt-widget__icon">
                                                           <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                               <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                   <rect x="0" y="0" width="24" height="24" />
                                                                   <rect fill="#000000" x="2" y="5" width="19" height="4" rx="1" />
                                                                   <rect fill="#000000" opacity="0.3" x="2" y="11" width="19" height="10" rx="1" />
                                                               </g>
                                                           </svg> </span>
                                                       <span class="kt-widget__desc">
                                                           Salary Slip
                                                       </span>
                                                   </span>
                                                </a>
                                           </div>
                                       </div>
                                   </div>

                                   <!--end::Widget -->
                               </div>
                           </div>

                           <!--end:: Widgets/Applications/User/Profile1-->
                       </div>

                       <!--End:: App Aside-->

                       <!--Begin:: App Content-->
                       <div class="kt-grid__item kt-grid__item--fluid kt-app__content listing">
                       <?php if($this->uri->segment(3) == 'account_setting') {?>
                           <div class="row">
                               <div class="col-xl-12">
                                   <div class="kt-portlet" style="height: 680px;">
                                       <div class="kt-portlet__head">
                                           <div class="kt-portlet__head-label">
                                               <h3 class="kt-portlet__head-title">Account Details <small>update your account details</small></h3>
                                           </div>
                                           <div class="kt-portlet__head-toolbar">
                                               <div class="kt-portlet__head-wrapper">
                                                   <div class="dropdown dropdown-inline">
                                                       <button type="reset" class="btn btn-success employee_update_account_information" form_id="employee_update_account_information">Update</button>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                       <form class="kt-form kt-form--label-right" id="employee_update_account_information">
                                           <div class="kt-portlet__body">
                                               <div class="kt-section kt-section--first">
                                                   <div class="kt-section__body">
                                                        <div class="form-group row">
                                                           <label class="col-xl-3 col-form-label">Profie Photo:</label>
                                                           <div class="col-xl-3">
                                                               <div class="kt-avatar kt-avatar--outline" id="kt_user_avatar">
                                                                   <div class="kt-avatar__holder" style="background-image: url(<?php echo base_url('assets/hr_document/profile_pic/'.$user_details['profile_pic_file_path']); ?>)"></div>
                                                               </div>
                                                            </div>
                                                            <div class="col-xl-6">
                                                                <div class="dropzone dropzone-multi" id="profile_pic_dropzone">
                                                                    <div class="dropzone-panel">
                                                                        <a class="dropzone-select btn btn-label-brand btn-bold btn-sm">Upload</a>
                                                                    </div>
                                                                    <div class="dropzone-items">
                                                                        <div class="dropzone-item" style="display:none">
                                                                            <div class="dropzone-file">
                                                                                <div class="dropzone-filename" title="some_image_file_name.jpg">
                                                                                    <span data-dz-name>some_image_file_name.jpg</span> <strong>(<span  data-dz-size>340kb</span>)</strong>
                                                                                </div>
                                                                                <div class="dropzone-error" data-dz-errormessage></div>
                                                                            </div>
                                                                            <div class="dropzone-progress">
                                                                                <div class="progress">
                                                                                    <div class="progress-bar kt-bg-brand" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress></div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="dropzone-toolbar">
                                                                                <span class="dropzone-delete" data-dz-remove><i class="flaticon2-cross"></i></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <span class="form-text text-muted">Max file size is 1MB and max number of files is 1.</br>Only JPG, AND PNG File Accepted.</span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">First Name</label>
                                                           <div class="col-lg-9 col-xl-6">
                                                               <input class="form-control" type="text" name="first_name" value="<?php echo $user_details['first_name']; ?>">
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Last Name</label>
                                                           <div class="col-lg-9 col-xl-6">
                                                               <input class="form-control" type="text" name="last_name" value="<?php echo $user_details['last_name']; ?>">
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Gender</label>
                                                           <div class="col-lg-9 col-xl-6">
                                                               <input class="form-control" type="text" name="gender" value="<?php echo $user_details['gender']; ?>">
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Blood Group</label>
                                                           <div class="col-lg-9 col-xl-6">
                                                               <input class="form-control" type="text" name="blood_group" value="<?php echo $user_details['blood_group']; ?>">
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Date Of Birth</label>
                                                           <div class="col-lg-9 col-xl-6 input-group date">
                                                               <input type="text" class="form-control" readonly value="<?php echo date('m/d/Y',strtotime($user_details['birth_date'])); ?>" id="birth_date" name="birth_date" />
                                                               <div class="input-group-append">
                                                                   <span class="input-group-text">
                                                                       <i class="la la-calendar"></i>
                                                                   </span>
                                                               </div>
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Age</label>
                                                           <div class="col-lg-9 col-xl-6">
                                                               <input class="form-control" type="text" readonly name="age" id="age" value="<?php echo $user_details['age']; ?>">
                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </form>
                                   </div>
                               </div>
                           </div>
                       <?php } else if($this->uri->segment(3) == 'personal_information') {?>
                           <div class="row">
                               <div class="col-xl-12">
                                   <div class="kt-portlet" style="height: 680px;">
                                       <div class="kt-portlet__head">
                                           <div class="kt-portlet__head-label">
                                               <h3 class="kt-portlet__head-title">Personal Information <small>update your personal details</small></h3>
                                           </div>
                                           <div class="kt-portlet__head-toolbar">
                                               <div class="kt-portlet__head-wrapper">
                                                   <div class="dropdown dropdown-inline">
                                                       <button type="reset" class="btn btn-success employee_update_personal_information" form_id="employee_update_personal_information">Update</button>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                       <form class="kt-form kt-form--label-right" id="employee_update_personal_information">
                                           <div class="kt-portlet__body">
                                               <div class="kt-section kt-section--first">
                                                   <div class="kt-section__body">
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Personal Mobile</label>
                                                           <div class="col-lg-9 col-xl-6">
                                                               <input class="form-control" type="text" name="personal_number" value="<?php echo $user_details['personal_number']; ?>">
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Personal Email</label>
                                                           <div class="col-lg-9 col-xl-6">
                                                               <input class="form-control" type="text" name="personal_email" value="<?php echo $user_details['personal_email']; ?>">
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Marital Status</label>
                                                           <div class="col-lg-9 col-xl-6">
                                                               <input class="form-control" type="text" name="marriage_status" value="<?php echo $user_details['marriage_status']; ?>">
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Qualifaction</label>
                                                           <div class="col-lg-9 col-xl-6">
                                                               <input class="form-control" type="text" name="qualification" value="<?php echo $user_details['qualification']; ?>">
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-form-label">Aadhar Card</label>
                                                           <div class="col-xl-2">
                                                               <input class="form-control" type="text" name="aadhar_number" value="<?php echo $user_details['aadhar_number']; ?>">
                                                           </div>
                                                           <label class="col-xl-2 col-form-label">Pan Card</label>
                                                           <div class="col-xl-2">
                                                               <input class="form-control" type="text" name="pan_number" value="<?php echo $user_details['pan_number']; ?>">
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-form-label">Aadhar photo</label>
                                                           <div class="col-xl-2">
                                                               <div class="kt-avatar kt-avatar--outline" id="kt_user_avatar">
                                                                   <div class="kt-avatar__holder" style="background-image: url(<?php echo base_url('assets/hr_document/aadhar_card/'.$user_details['aadhar_card_file_path']); ?>)"></div>
                                                               </div>
                                                           </div>
                                                           <label class="col-xl-2 col-form-label">Pan card Photo</label>
                                                           <div class="col-xl-3">
                                                               <div class="kt-avatar kt-avatar--outline" id="kt_user_avatar">
                                                                   <div class="kt-avatar__holder" style="background-image: url(<?php echo base_url('assets/hr_document/pan_card/'.$user_details['pan_card_file_path']); ?>)"></div>
                                                               </div>
                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </form>
                                   </div>
                               </div>
                           </div>
                       <?php } else if($this->uri->segment(3) == 'address') {?>
                           <div class="row">
                               <div class="col-xl-12">
                                   <div class="kt-portlet" style="height: 680px;">
                                       <div class="kt-portlet__head">
                                           <div class="kt-portlet__head-label">
                                               <h3 class="kt-portlet__head-title">Address <small>update your address</small></h3>
                                           </div>
                                           <div class="kt-portlet__head-toolbar">
                                               <div class="kt-portlet__head-wrapper">
                                                   <div class="dropdown dropdown-inline">
                                                       <button type="reset" class="btn btn-success employee_update_address_information" form_id="employee_update_address_information">Update</button>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                       <form class="kt-form kt-form--label-right" id="employee_update_address_information">
                                           <div class="kt-portlet__body">
                                               <div class="kt-section kt-section--first">
                                                   <div class="kt-section__body">
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Address</label>
                                                           <div class="col-lg-9 col-xl-6">
                                                               <input class="form-control" type="text" name="address" value="<?php echo $address_details['address']; ?>">
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Postcode</label>
                                                           <div class="col-lg-9 col-xl-6">
                                                               <input class="form-control" type="text" name="pin_code" value="<?php echo $address_details['pin_code']; ?>">
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">State</label>
                                                           <div class="col-lg-9 col-xl-6">
                                                               <input class="form-control" type="text" name="state" value="<?php echo $address_details['state']; ?>">
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">City</label>
                                                           <div class="col-lg-9 col-xl-6">
                                                               <input class="form-control" type="text" name="city" value="<?php echo $address_details['city']; ?>">
                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </form>
                                   </div>
                               </div>
                           </div>
                       <?php } else if($this->uri->segment(3) == 'official_information') {?>
                           <div class="row">
                               <div class="col-xl-12">
                                   <div class="kt-portlet" style="height: 680px;">
                                       <div class="kt-portlet__head">
                                           <div class="kt-portlet__head-label">
                                               <h3 class="kt-portlet__head-title">Official Information <small>update your official details</small></h3>
                                           </div>
                                           <div class="kt-portlet__head-toolbar">
                                               <div class="kt-portlet__head-wrapper">
                                                   <div class="dropdown dropdown-inline">
                                                       <button type="reset" class="btn btn-success employee_update_official_information" form_id="employee_update_official_information">Update</button>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                       <form class="kt-form kt-form--label-right" id="employee_update_official_information">
                                           <div class="kt-portlet__body">
                                               <div class="kt-section kt-section--first">
                                                   <div class="kt-section__body">
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Official Mobile</label>
                                                           <div class="col-lg-9 col-xl-6">
                                                               <input class="form-control" type="text" name="official_number" value="<?php echo $user_details['official_number']; ?>">
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Official Email</label>
                                                           <div class="col-lg-9 col-xl-6">
                                                               <input class="form-control" type="text" name="official_email" value="<?php echo $user_details['official_email']; ?>">
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Job Location</label>
                                                           <div class="col-lg-9 col-xl-6">
                                                               <input class="form-control" type="text" name="job_location" value="<?php echo $user_details['job_location']; ?>">
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Department</label>
                                                           <div class="col-lg-9 col-xl-6">
                                                               <input class="form-control" type="text" name="department" value="<?php echo $user_details['department']; ?>">
                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </form>
                                   </div>
                               </div>
                           </div>
                       <?php } else if($this->uri->segment(3) == 'bank_details') {?>
                           <div class="row">
                               <div class="col-xl-12">
                                   <div class="kt-portlet" style="height: 680px;">
                                       <div class="kt-portlet__head">
                                           <div class="kt-portlet__head-label">
                                               <h3 class="kt-portlet__head-title">Payment Information <small>update your payment details</small></h3>
                                           </div>
                                           <div class="kt-portlet__head-toolbar">
                                               <div class="kt-portlet__head-wrapper">
                                                   <div class="dropdown dropdown-inline">
                                                       <button type="reset" class="btn btn-success employee_update_bank_information" form_id="employee_update_bank_information">Update</button>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                       <form class="kt-form kt-form--label-right" id="employee_update_bank_information">
                                           <div class="kt-portlet__body">
                                               <div class="kt-section kt-section--first">
                                                   <div class="kt-section__body">
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Bank Name</label>
                                                           <div class="col-lg-9 col-xl-6">
                                                               <input class="form-control" type="text" name="bank_name" value="<?php echo $bank_details['bank_name']; ?>">
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Branch Name</label>
                                                           <div class="col-lg-9 col-xl-6">
                                                               <input class="form-control" type="text" name="branch_name" value="<?php echo $bank_details['branch_name']; ?>">
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Benificary Name</label>
                                                           <div class="col-lg-9 col-xl-6">
                                                               <input class="form-control" type="text"name="beneficiary_name" value="<?php echo $bank_details['beneficiary_name'];?>">
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Account Number</label>
                                                           <div class="col-lg-9 col-xl-6">
                                                               <input class="form-control" type="text" name="account_number" value="<?php echo $bank_details['account_number']; ?>">
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">IFSC Code</label>
                                                           <div class="col-lg-9 col-xl-6">
                                                               <input class="form-control" type="text" name="ifsc_code" value="<?php echo $bank_details['ifsc_code']; ?>">
                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </form>
                                   </div>
                               </div>
                           </div>
                       <?php } else if($this->uri->segment(3) == 'reference') {?>
                           <div class="row">
                               <div class="col-xl-12">
                                   <div class="kt-portlet" style="height: 680px;">
                                       <div class="kt-portlet__head">
                                           <div class="kt-portlet__head-label">
                                               <h3 class="kt-portlet__head-title">Reference Information <small>update your reference details</small></h3>
                                           </div>
                                           <div class="kt-portlet__head-toolbar">
                                               <div class="kt-portlet__head-wrapper">
                                                   <div class="dropdown dropdown-inline">
                                                       <button type="reset" class="btn btn-success employee_update_reference_information" form_id="employee_update_reference_information">Update</button>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                       <form class="kt-form kt-form--label-right" id="employee_update_reference_information">
                                           <div class="kt-portlet__body">
                                               <div class="kt-section kt-section--first">
                                                   <div class="kt-section__body">
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label"></label>
                                                           <label class="col-xl-4 col-lg-4 col-form-label" style="text-align: left;">Reference 1</label>
                                                           <input class="form-control" type="text" name="id_1" value="<?php echo $reference_details[0]['id']; ?>" hidden>
                                                           <label class="col-xl-4 col-lg-4 col-form-label" style="text-align: left;">Reference 2</label>
                                                           <input class="form-control" type="text" name="id_2" value="<?php echo $reference_details[1]['id']; ?>" hidden>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Reference Name</label>
                                                           <div class="col-lg-4 col-xl-4">
                                                               <input class="form-control" type="text" name="reference_name_1" value="<?php echo $reference_details[0]['reference_name']; ?>">
                                                           </div>
                                                           <div class="col-lg-4 col-xl-4">
                                                               <input class="form-control" type="text" name="reference_name_2" value="<?php echo $reference_details[1]['reference_name']; ?>">
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Relation</label>
                                                           <div class="col-lg-4 col-xl-4">
                                                               <input class="form-control" type="text" name="relation_1" value="<?php echo $reference_details[0]['relation']; ?>">
                                                           </div>
                                                           <div class="col-lg-4 col-xl-4">
                                                               <input class="form-control" type="text" name="relation_2" value="<?php echo $reference_details[1]['relation']; ?>">
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Address</label>
                                                           <div class="col-lg-4 col-xl-4">
                                                               <input class="form-control" type="text" name="address_1" value="<?php echo $reference_details[0]['address']; ?>">
                                                           </div>
                                                           <div class="col-lg-4 col-xl-4">
                                                               <input class="form-control" type="text" name="address_2" value="<?php echo $reference_details[1]['address']; ?>">
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Number</label>
                                                           <div class="col-lg-4 col-xl-4">
                                                               <input class="form-control" type="text" name="number_1" value="<?php echo $reference_details[0]['number']; ?>">
                                                           </div>
                                                           <div class="col-lg-4 col-xl-4">
                                                               <input class="form-control" type="text" name="number_2" value="<?php echo $reference_details[1]['number']; ?>">
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Alternate Number</label>
                                                           <div class="col-lg-4 col-xl-4">
                                                               <input class="form-control" type="text" name="alternate_number_1" value="<?php echo $reference_details[0]['alternate_number']; ?>">
                                                           </div>
                                                           <div class="col-lg-4 col-xl-4">
                                                               <input class="form-control" type="text" name="alternate_number_2" value="<?php echo $reference_details[1]['alternate_number']; ?>">
                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                           <label class="col-xl-3 col-lg-3 col-form-label">Email</label>
                                                           <div class="col-lg-4 col-xl-4">
                                                               <input class="form-control" type="text" name="email_1" value="<?php echo $reference_details[0]['email']; ?>">
                                                           </div>
                                                           <div class="col-lg-4 col-xl-4">
                                                               <input class="form-control" type="text" name="email_2" value="<?php echo $reference_details[1]['email']; ?>">
                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </form>
                                   </div>
                               </div>
                           </div>
                       <?php } else if($this->uri->segment(3) == 'leave_management') {?>
                            <?php if(count($all_reporing_id) > 1) {?>
                            <div class="kt-portlet">
                                <ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand" role="tablist" style="padding: 1% 2% 1% 2%;margin-bottom: 0px;">
                                    <?php foreach ($all_reporing_id as $single_reporing_id_details) { ?>
                                        <li class="nav-item" style="width: calc(100%/<?php echo count($all_reporing_id);?>) !important;margin-right: 0px;">
                                            <a class="
                                                    nav-link 
                                                    change_user_tab
                                                    <?php echo ($single_reporing_id_details['user_id'] == $this->session->userdata('user_id')) 
                                                                ? 'active':'';
                                                    ?> 
                                                    "
                                                    user_id = "<?php echo $single_reporing_id_details['user_id']; ?>"
                                                    data-toggle="tab" href="#kt_apps_contacts_view_tab_1" role="tab" style="text-align: center;">
                                                <i class="flaticon2-note"></i>
                                                <?php 
                                                    $explode_string = explode(' ', $single_reporing_id_details['name']);
                                                    echo ucfirst($explode_string[0]); 
                                                ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <?php } ?>
                            <input type="text" id="change_user_tab_user_id" value="<?php echo $this->session->userdata('user_id'); ?>" hidden>
                            <input type="text" id="change_tab_request_type" value="leave_request" hidden>
                            <div class="kt-portlet kt-portlet--height-fluid" style="height: 60px !important;">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            Leave Available:<?php echo $leave_balance_details['total_leave_available'];?>
                                        </h3>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <h3 class="kt-portlet__head-title">
                                            Leave Taken:<?php echo $leave_balance_details['total_leave_taken'];?>
                                        </h3>
                                        
                                    </div>
                                    <div class="kt-portlet__head-toolbar">
                                        <ul class="nav nav-pills nav-pills-sm nav-pills-label nav-pills-bold" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link change_tab active" data-toggle="tab" href="#kt_widget5_tab1_content" role="tab" aria-selected="false" request-type="leave_request">
                                                    All Purpose Leave
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link change_tab" data-toggle="tab" href="#kt_widget5_tab2_content" role="tab" aria-selected="false" request-type="compoff_request">
                                                    CompOff
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link change_tab" data-toggle="tab" href="#kt_widget5_tab1_content" role="tab" aria-selected="false" request-type="early_leave">
                                                    Early Leave
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link change_tab" data-toggle="tab" href="#kt_widget5_tab2_content" role="tab" aria-selected="false" request-type="late_mark">
                                                    Late Mark
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="dropdown dropdown-inline" data-toggle-="kt-tooltip" title="Quick actions" data-placement="left">
                                            <?php if(!empty($reporting_manager_details) && false) {?>
                                            <a href="#" class="btn btn-icon get_reporting_manager" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--md">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <polygon points="0 0 24 0 24 24 0 24" />
                                                        <path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                        <path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" fill="#000000" />
                                                    </g>
                                                </svg>
                                            </a>
                                            <?php }?>
                                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">

                                                <!--begin::Nav-->
                                                <ul class="kt-nav">
                                                    <li class="kt-nav__head">
                                                        Add Any Request:
                                                    </li>
                                                    <li class="kt-nav__separator"></li>
                                                    <li class="kt-nav__item">
                                                        <a href="#" class="kt-nav__link" data-toggle="modal" data-target="#add_leave_form" id="leave_btn">
                                                            <i class="kt-nav__link-icon flaticon2-telegram-logo"></i>
                                                            <span class="kt-nav__link-text">Leave</span>
                                                        </a>
                                                    </li>
                                                    <li class="kt-nav__item">
                                                        <a href="#" class="kt-nav__link" data-toggle="modal" data-target="#compoff_form" id="leave_btn">
                                                            <i class="kt-nav__link-icon flaticon2-new-email"></i>
                                                            <span class="kt-nav__link-text">Comp Off</span>
                                                            <!-- <span class="kt-nav__link-badge">
                                                                <span class="kt-badge kt-badge--brand kt-badge--rounded">5</span>
                                                            </span> -->
                                                        </a>
                                                    </li>
                                                    <li class="kt-nav__item">
                                                        <a href="#" class="kt-nav__link "data-toggle="modal" data-target="#late_mark" id="late_mark_btn">
                                                            <i class="kt-nav__link-icon flaticon2-drop"></i>
                                                            <span class="kt-nav__link-text">Late Mark</span>
                                                        </a>
                                                    </li>
                                                    <li class="kt-nav__item">
                                                        <a href="#" class="kt-nav__link"
                                                        data-toggle="modal" data-target="#early_leave" id="leave_btn">
                                                            <i class="kt-nav__link-icon flaticon2-calendar-8"></i>
                                                            <span class="kt-nav__link-text">Early Leave</span>
                                                        </a>
                                                    </li>
                                                    
                                                    <li class="kt-nav__separator"></li>
                                                </ul>

                                                <!--end::Nav-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="leave_request_list">
                                <?php $this->load->view('hr/leave_request_list');?>
                            </div>

                        <?php } else if($this->uri->segment(3) == 'salary_slip') {?>
                            <div class="row" style="display:none;">
                                <div class="kt-portlet">
                                    <div class="kt-portlet__head">
                                        <div class="kt-portlet__head-label">
                                            <span class="kt-portlet__head-icon kt-hidden">
                                                <i class="la la-gear"></i>
                                            </span>
                                            <h3 class="kt-portlet__head-title">
                                                Add Salary Slip Amount
                                            </h3>
                                        </div>
                                    </div>

                                    <div class="kt-portlet__body">
                                        <div class="kt-form__section kt-form__section--first">
                                            <!--begin::Form-->
                                            <form class="kt-form" id="salary_slip_default_form">
                                                <div class="form-group row salary_slip_row">
                                                    <div class="col-lg-4">
                                                        <div class="row">
                                                            <div class="col-lg-8">
                                                                <label>Person:</label>
                                                                <select class="form-control kt-selectpicker" name="people_id" data-size="7" data-live-search="true">
                                                                    <option value="">Select</option>
                                                                    <?php foreach ($all_user_list as $single_user_id => $single_user_name) { ?>
                                                                        <option value="<?php echo $single_user_id; ?>"><?php echo $single_user_name;?></option>
                                                                    <?php } ?>
                                                                </select>
                                                                <input type="text" class="form-control" name="year" value="<?php echo date('Y');?>" hidden>
                                                            </div>
                                                        </div>
                                                    </div>                                                 
                                                </div>
                                            </form>

                                            <!--end::Form-->
                                        </div>
                                    </div>
                                    <div class="kt-portlet__foot">
                                        <div class="kt-form__actions">
                                            <div class="row">
                                                <div class="col-lg-2"></div>
                                                <div class="col-lg-2">
                                                    <button type="reset" class="btn btn-success salary_slip_default_form_submit">Submit</button>
                                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="display:block;">
                                <div class="kt-portlet ">
                                    <div class="kt-portlet__head">
                                        <div class="kt-portlet__head-label">
                                            <h3 class="kt-portlet__head-title">
                                                Salary Slip
                                            </h3>
                                        </div>
                                        <div class="kt-portlet__head-toolbar">
                                            <?php foreach($salary_year as $year_value) {?>
                                                <ul class="nav nav-pills nav-pills-sm nav-pills-label nav-pills-bold" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link change_tab active year_filter" data-toggle="tab" href="#kt_widget5_tab1_content" role="tab" aria-selected="true" year-value=" ">
                                                          <?php echo $year_value['year'] ;?>  
                                                        </a>
                                                    </li>
                                                </ul>
                                            <?php }?>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__body">

                                        <!--begin::Accordion-->
                                        <?php foreach($salary_month as $month_name => $month_details){?>
                                        <div class="accordion accordion-solid accordion-toggle-plus" id="accordionExample6">
                                            <div class="card">
                                                <div class="card-header" id="headingOne6">
                                                    <div class="card-title" data-toggle="collapse" data-target="#collapseOne6" aria-expanded="true" aria-controls="collapseOne6">
                                                     <?php echo ucfirst($month_name);?>
                                                    </div>
                                                </div>
                                                <div id="collapseOne6" class="collapse show" aria-labelledby="headingOne6" data-parent="#accordionExample6" style="">
                                                    <div class="card-body">
                                                        <div class="row">   
                                                            <div class="col-xl-6">
                                                                <div class="table-responsive">
                                                                    <table class="table salary_slip grand_total">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Earning</th>
                                                                                <th>AMOUNT</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <?php foreach($month_details['salary_income'] as $salary_slip_value){ ?>
                                                                            <tr>
                                                                                <td><?php echo $salary_slip_value['name'];?></td>
                                                                                <td class="kt-font-primary kt-font-xl kt-font-boldest"><?php echo $salary_slip_value['value'] ;?></td>
                                                                            </tr>
                                                                            <?php } ?>
                                                                            <tr>
                                                                                <td>Total Income</td>
                                                                                <td class="kt-font-primary kt-font-xl kt-font-boldest"><?php echo $month_details['total']['income'] ;?></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="col-xl-6">
                                                                <div class="table-responsive">
                                                                    <table class="table salary_slip grand_total">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Deduction</th>
                                                                                <th>AMOUNT</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <?php foreach($month_details['salary_expences'] as $salary_slip_value){ ?>
                                                                            <tr>
                                                                                <td><?php echo $salary_slip_value['name'] ;?></td>
                                                                                <td class="kt-font-primary kt-font-xl kt-font-boldest"><?php echo $salary_slip_value['value'] ;?></td>
                                                                            </tr>
                                                                            <?php } ?>
                                                                            <tr>
                                                                                <td>Total Expense</td>
                                                                                <td class="kt-font-primary kt-font-xl kt-font-boldest"><?php echo $month_details['total']['expense'] ;?></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>    
                                                        </div>
                                                        <div class="row" style="padding: 30px 200px 0px 10px;">
                                                            <div class="col-xl-12">
                                                                <div class="table-responsive">
                                                                    <table class="table grand_total">
                                                                        <thead> 
                                                                            <tr>
                                                                                <th>BANK</th>
                                                                                <th>ACC.NO.</th>
                                                                                <th>DUE DATE</th>
                                                                                <th>TOTAL AMOUNT</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td><?php echo $bank_details['bank_name'] ;?></td>
                                                                                <td><?php echo $bank_details['account_number'] ;?></td>
                                                                                <!-- <td>Jan 07, <?php echo date('Y');?></td> -->
                                                                                <td><?php 
                                                                                echo date('M 07, Y', 
                                                                                        strtotime(
                                                                                            date('F', 
                                                                                                mktime(
                                                                                                    0, 0, 0, 
                                                                                                    (date('m',strtotime($month_name))+1),
                                                                                                    1,
                                                                                                    date('y')
                                                                                                )
                                                                                            )
                                                                                        )
                                                                                    );?></td>
                                                                                <td class="kt-font-primary kt-font-xl kt-font-boldest"><?php echo $month_details['total']['income'] - $month_details['total']['expense'];?></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                         <?php }?>
                                        <!--end::Accordion-->
                                    </div>
                                </div>
                            </div>
                        <?php }?>
                            <div id="hr_edit_listing_table_loader" class="layer-white">
                                <div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
                            </div>
                        </div>

                       <!--End:: App Content-->
                   </div>

                   <!--End::App-->
               </div>

               <!-- end:: Content -->
            </div>

            <!-- begin:: Footer -->
            <div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
                <div class="kt-container  kt-container--fluid ">
                    <div class="kt-footer__copyright">
                        2020&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Keenthemes</a>
                    </div>
                    <div class="kt-footer__menu">
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">About</a>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Team</a>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Contact</a>
                    </div>
                </div>
            </div>

            <!-- end:: Footer -->
        </div>
    </div>
</div>

<!-- end:: Page -->

<!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="fa fa-arrow-up"></i>
</div>

<!-- end::Scrolltop -->

<div class="modal fade" id="add_leave_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Request Leave Form</h5>
                <button type="reset" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form id="add_leave_form">
                    <input type="text" class="form-control" name="leave_request_type" value="leave_request" hidden/>
                    <input type="text" class="form-control" name="leave_request_creator_id" value="<?php echo $this->session->userdata('user_id');?>" hidden/>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label class="col-lg-12 col-sm-12">Start Date & End Date</label>
                            <div class="col-lg-12 col-md-9 col-sm-12">
                                <div class='input-group pull-right' id='kt_daterangepicker_4'>
                                    <input type="text" class="form-control" name="date" readonly placeholder="Select date & time range" />
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="col-lg-12 col-sm-12">Approvel from</label>
                            <div class="col-lg-12 col-md-9 col-sm-12">
                                <div class="input-daterange input-group">
                                    <input type="text" class="form-control" readonly value="<?php echo $reporting_manager_details['name'];?>" />
                                    <input type="text" class="form-control" name="reporting_manager" hidden value="<?php echo $reporting_manager_details['user_id'];?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label for="col-lg-12 col-sm-12">Reason</label>
                            <div class="col-lg-12 col-md-9 col-sm-12">
                                <div class="input-daterange input-group">
                                    <textarea class="form-control" id="reason" rows="3" name="reason"></textarea>
                                </div>
                            </div>
                        </div>    
                    </div>
                    <div class="clearfix"></div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success add_leave_form_submit" form-name="add_leave_form">Add</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- compoff form -->
<div class="modal fade" id="compoff_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">CompOff Form</h5>
                <button type="reset" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form id="comp_off_form">
                    <input type="text" class="form-control" name="leave_request_type" value="compoff_request" hidden/>
                    <input type="text" class="form-control" name="leave_request_creator_id" value="<?php echo $this->session->userdata('user_id');?>" hidden/>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label class="col-lg-12 col-sm-12">Start Date & End Date</label>
                            <div class="col-lg-12 col-md-9 col-sm-12">
                                <div class='input-group pull-right' id='comp_off_datepicker'>
                                    <input type="text" class="form-control" name="date" readonly placeholder="Select date & time range" />
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="col-lg-12 col-sm-12">Approvel from</label>
                            <div class="col-lg-12 col-md-9 col-sm-12">
                                <div class="input-daterange input-group">
                                    <input type="text" class="form-control" readonly value="<?php echo $reporting_manager_details['name'];?>" />
                                    <input type="text" class="form-control" name="reporting_manager" hidden value="<?php echo $reporting_manager_details['user_id'];?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label for="col-lg-12 col-sm-12">Reason</label>
                            <div class="col-lg-12 col-md-9 col-sm-12">
                                <div class="input-daterange input-group">
                                    <textarea class="form-control" id="reason" rows="3" name="reason"></textarea>
                                </div>
                            </div>
                        </div>    
                    </div>
                    <div class="clearfix"></div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success add_leave_form_submit" form-name="comp_off_form">Add</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- early leave -->
<div class="modal fade" id="early_leave" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Early Leave Form</h5>
                <button type="reset" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form id="early_leave_form">
                    <input type="text" class="form-control" name="leave_request_type" value="early_leave" hidden/>
                    <input type="text" class="form-control" name="leave_request_creator_id" value="<?php echo $this->session->userdata('user_id');?>" hidden/>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label class="col-lg-12 col-sm-12">Start Date & End Date</label>
                            <div class="col-lg-12 col-md-9 col-sm-12">
                                <div class='input-group pull-right' id='early_leave_datepicker'>
                                    <input type="text" class="form-control" name="date" readonly placeholder="Select date & time range" />
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="col-lg-12 col-sm-12">Approvel from</label>
                            <div class="col-lg-12 col-md-9 col-sm-12">
                                <div class="input-daterange input-group">
                                    <input type="text" class="form-control" readonly value="<?php echo $reporting_manager_details['name'];?>" />
                                    <input type="text" class="form-control" name="reporting_manager" hidden value="<?php echo $reporting_manager_details['user_id'];?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label for="col-lg-12 col-sm-12">Reason</label>
                            <div class="col-lg-12 col-md-9 col-sm-12">
                                <div class="input-daterange input-group">
                                    <textarea class="form-control" id="reason" rows="3" name="reason"></textarea>
                                </div>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>    
                    </div>
                    <div class="clearfix"></div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success add_leave_form_submit" form-name="early_leave_form">Add</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- late mark -->
<div class="modal fade" id="late_mark" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Late Mark Form</h5>
                <button type="reset" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form id="late_mark_form">
                    <input type="text" class="form-control" name="leave_request_type" value="late_mark" hidden/>
                    <input type="text" class="form-control" name="leave_request_creator_id" value="<?php echo $this->session->userdata('user_id');?>" hidden/>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label class="col-lg-12 col-sm-12">Start Date & End Date</label>
                            <div class="col-lg-12 col-md-9 col-sm-12">
                                <div class='input-group pull-right' id='late_mark_datepicker'>
                                    <input type="text" class="form-control" name="date" readonly placeholder="Select date & time range" />
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="col-lg-12 col-sm-12">Approvel from</label>
                            <div class="col-lg-12 col-md-9 col-sm-12">
                                <div class="input-daterange input-group">
                                    <input type="text" class="form-control" readonly value="<?php echo $reporting_manager_details['name'];?>" />
                                    <input type="text" class="form-control" name="reporting_manager" hidden value="<?php echo $reporting_manager_details['user_id'];?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label for="col-lg-12 col-sm-12">Reason</label>
                            <div class="col-lg-12 col-md-9 col-sm-12">
                                <div class="input-daterange input-group">
                                    <textarea class="form-control" id="reason" rows="3" name="reason"></textarea>
                                </div>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>    
                    </div>
                    <div class="clearfix"></div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success add_leave_form_submit" form-name="late_mark_form">Add</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>