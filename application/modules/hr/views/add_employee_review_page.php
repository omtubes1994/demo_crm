<?php if(!empty($form_data)) {?>
	<?php 
	// echo "<pre>";print_r($form_data);echo"</pre><hr>";exit;
	?>
	<div class="kt-wizard-v2__review-item">
		<div class="kt-wizard-v2__review-title">
			Account Details
		</div>
		<div class="kt-wizard-v2__review-content">
			Name: <?php echo $form_data['first_name'].' ', $form_data['last_name']; ?><br />
			Gender: <?php echo $form_data['gender']; ?><br />
			Blood Group: <?php echo $form_data['blood_group']; ?><br />
			Birth Date: <?php echo date('d F, Y', strtotime($form_data['birth_date'])); ?><br />
			Age: <?php echo $form_data['age']; ?>
		</div>
	</div>
	<div class="kt-wizard-v2__review-item">
		<div class="kt-wizard-v2__review-title">
			Personal Information
		</div>
		<div class="kt-wizard-v2__review-content">
			Personal Mobile: <?php echo $form_data['personal_number']; ?><br />
			Personal Email: <?php echo $form_data['personal_email']; ?><br />
			Marriage : <?php echo $form_data['marriage_status']; ?><br />
			Qualification: <?php echo $form_data['qualification']; ?><br />
			Aadhar Card Number: <?php echo $form_data['aadhar_number']; ?><br />
			Pan Card Number: <?php echo $form_data['pan_number']; ?>
		</div>
	</div>
	<div class="kt-wizard-v2__review-item">
		<div class="kt-wizard-v2__review-title">
			Resident Address
		</div>
		<div class="kt-wizard-v2__review-content">
			Address: <?php echo $form_data['resident_address_1'], ' , ',$form_data['resident_address_2'] ; ?><br />
			Pin Code: <?php echo $form_data['resident_postcode']; ?><br />
			State: <?php echo $form_data['resident_state']; ?><br />
			City: <?php echo $form_data['resident_city']; ?>
		</div>
	</div>

	<?php if(!empty($form_data['actual_address_1'])) {?>
	<div class="kt-wizard-v2__review-item">
		<div class="kt-wizard-v2__review-title">
			Actual Address
		</div>
		<div class="kt-wizard-v2__review-content">
			Address: <?php echo $form_data['actual_address_1'], ' , ',$form_data['actual_address_2'] ; ?><br />
			Pin Code: <?php echo $form_data['actual_postcode']; ?><br />
			State: <?php echo $form_data['actual_state']; ?><br />
			City: <?php echo $form_data['actual_city']; ?>
		</div>
	</div>
	<?php }?>
	<div class="kt-wizard-v2__review-item">
		<div class="kt-wizard-v2__review-title">
			Official Details
		</div>
		<div class="kt-wizard-v2__review-content">
			Official Mobile: <?php echo $form_data['official_number']; ?><br />
			Official Email: <?php echo $form_data['official_email']; ?><br />
			<!-- Designation : <?php echo $form_data['designation']; ?><br /> -->
			Department: <?php echo $form_data['department']; ?><br />
			Job Location: <?php echo $form_data['job_location']; ?><br />
			<?php 

				$session_data = $this->session->userdata('add_employee_data');
				$profile_pic_url = 'assets/hr_document/profile_pic/'.$session_data['profile_pic_file_name'];
			?>
			<div class="kt-widget__media kt-hidden-">
				Profile Photo: <img src="<?php echo $profile_pic_url;?>" alt="image" style="height: 100px; width: 100px;">
			</div>
		</div>
	</div>
	<div class="kt-wizard-v2__review-item">
		<div class="kt-wizard-v2__review-title">
			Payment Details
		</div>
		<div class="kt-wizard-v2__review-content">
			Bank Name: <?php echo $form_data['bank_name']; ?><br />
			Branch Name: <?php echo $form_data['branch_name']; ?><br />
			Beneficiary Name: <?php echo $form_data['beneficiary_name']; ?><br />
			Account Number: <?php echo $form_data['account_number']; ?><br />
			IFSC Code: <?php echo $form_data['ifsc_code']; ?>
		</div>
	</div>
	<div class="kt-wizard-v2__review-item">
		<div class="kt-wizard-v2__review-title">
			Reference
		</div>
		<div class="kt-wizard-v2__review-content">
			Reference Number: 1<br />
			Name: <?php echo $form_data['reference_name_1']; ?><br />
			Relation: <?php echo $form_data['relation_1']; ?><br />
			Address: <?php echo $form_data['address_1']; ?><br />
			Number: <?php echo $form_data['number_1']; ?><br />
			Alternate Number: <?php echo $form_data['alternate_number_1']; ?><br />
			Email: <?php echo $form_data['email_1']; ?><br />

			Reference Number: 2<br />
			Name: <?php echo $form_data['reference_name_2']; ?><br />
			Relation: <?php echo $form_data['relation_2']; ?><br />
			Address: <?php echo $form_data['address_2']; ?><br />
			Number: <?php echo $form_data['number_2']; ?><br />
			Alternate Number: <?php echo $form_data['alternate_number_2']; ?><br />
			Email: <?php echo $form_data['email_2']; ?>
		</div>
	</div>
<?php }?>