<style type="text/css">
    .kt-aside--fixed .kt-wrapper {
        padding-left: 130px !important;
    }
    .kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper {
        padding-top: 60px !important;
    }
    .nav-pills .nav-item .nav-link:active, .nav-pills .nav-item .nav-link.active, .nav-pills .nav-item .nav-link.active:hover {

        border-bottom: 0.25rem solid #5d78ff!important;
        color: #1a73e8;
        opacity: 1;
        font-family: Google Sans,Helvetica Neue,sans-serif;
        font-size: 14px;
        font-weight: 600;
        background-color: #fff;
    }
</style>
<!-- end:: Header Mobile -->
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <!-- begin:: Content -->
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Data
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <ul class="nav nav-pills nav-fill" role="tablist" style="padding: 0px 500px 0px 500px;">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#kt_tabs_5_1">Piping</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_tabs_5_2">Tube Fitting Valves</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_tabs_5_3">Tubing</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_tabs_5_4">Hammer Union</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="kt_tabs_5_1" role="tabpanel">
                                    <iframe width="100%" height="1500" src="https://datastudio.google.com/embed/reporting/eed992f1-d38c-4475-a675-ec2b4a29d47f/page/dkKMC" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                                <div class="tab-pane" id="kt_tabs_5_2" role="tabpanel">
                                    <iframe width="100%" height="1500" src="https://datastudio.google.com/embed/reporting/eed992f1-d38c-4475-a675-ec2b4a29d47f/page/CPLMC" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                                <div class="tab-pane" id="kt_tabs_5_3" role="tabpanel">
                                    <iframe width="100%" height="1500" src="https://datastudio.google.com/embed/reporting/eed992f1-d38c-4475-a675-ec2b4a29d47f/page/OULMC" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                                <div class="tab-pane" id="kt_tabs_5_4" role="tabpanel">
                                    <iframe width="100%" height="1500" src="https://datastudio.google.com/embed/reporting/eed992f1-d38c-4475-a675-ec2b4a29d47f/page/TULMC" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--end::Portlet-->            
                </div>

                <!-- end:: Content -->
            </div>

            <!-- begin:: Footer -->
            <div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
                <div class="kt-container  kt-container--fluid ">
                    <div class="kt-footer__copyright">
                        2020&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Keenthemes</a>
                    </div>
                    <div class="kt-footer__menu">
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">About</a>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Team</a>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Contact</a>
                    </div>
                </div>
            </div>

            <!-- end:: Footer -->
        </div>
    </div>
</div>

<!-- end:: Page -->

<!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="fa fa-arrow-up"></i>
</div>

<!-- end::Scrolltop -->