<?php 
if(!empty($leaves_details)) {
    //creating array chuck of 3
    $array_chunk =  array_chunk($leaves_details, 3);
    foreach ($array_chunk as $array_chunk_details) { 
?>
        <div class="row" style="height:auto;">
            <?php  foreach ($array_chunk_details as $leave_list_details) {  ?>
            <div class="col-xl-4 col-lg-6 order-lg-1 order-xl-1">
                <div class="kt-portlet">
                   <!--begin:: Widgets/Blog-->
                   <div class="kt-portlet kt-portlet--height-fluid kt-widget19" style="font-style: normal;font-family: Helvetica Neue,Helvetica,Arial,sans-serif;">
                       <div class="kt-portlet__body" style="padding: 25px 0px 0px 0px;">
                           <div class="kt-widget19__wrapper">
                               <div style="padding: 0px 25px 10px 25px;">
                                    <div class="row">
                                        <div class="col-xl-8">
                                            
                                           <h3 class="kt-portlet__head-title" style="
                                               margin: 0;
                                               padding: 0;
                                               font-size: 1.2rem;
                                               font-weight: 600;
                                               color: #48465b;
                                           "><?php echo $leave_list_details['request_type']; ?></h3>
                                        </div>
                                        <div class="col-xl-4" style="text-align: right;padding-right: 0%;">
                                            <button type="button" class="btn btn-outline-hover-info btn-elevate btn-icon" style="background-color: white;border-color: white; height:1rem;"><i class="la la-close"></i></button>
                                        </div>
                                    </div>
                               </div>
                               <div class="kt-widget19__content" style="padding: 0px 25px;">
                                   <div class="kt-widget19__userpic">
                                       <img src="<?php echo base_url('assets/hr_document/profile_pic/'.$leave_list_details['profile_pic']);?>" alt="" style="height: 50px;width: 50px; border-radius: 10px !important;">
                                   </div>
                                   <div class="kt-widget19__info">
                                       <a href="#" class="kt-widget19__username">
                                           <?php echo $leave_list_details['profile_name']; ?>
                                       </a>
                                       <span class="kt-widget19__time">
                                           <?php echo $leave_list_details['leave_add_day']; ?>
                                       </span>
                                   </div>
                                   <div class="kt-widget19__stats">
                                       <span class="kt-widget19__number kt-font-brand">
                                           <?php echo $leave_list_details['leave_count']; ?>             
                                        </span>
                                       <a href="#" class="kt-widget19__comment">
                                           Days
                                       </a>
                                   </div>
                               </div>
                               <div style="
                               height: 140px;
                               background-color: #007bff40;
                               ">
                                   <div style="padding: 25px;">
                                       
                                       <h3 class="kt-portlet__head-title kt-font-center" style="
                                                                           margin: 0;
                                                                           padding: 0;
                                                                           font-size: 1.2rem;
                                                                           font-weight: 600;
                                                                           color: #48465b;
                                                                           text-align: center;padding: 0px 0px 20px 0px;
                                                                       "> <?php echo $leave_list_details['leave_start_date_end_date']; ?></h3>  
                                       <div class="progress"style="height: 1.20rem;">
                                           <div class="progress-bar bg-info" role="progressbar" style="width: <?php echo ($leave_list_details['total_leave_taken']*100)/$leave_list_details['total_leave_available']; ?>%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                       </div>
                                       <h3 class="kt-portlet__head-title kt-font-center" style="
                                                                           margin: 0;
                                                                           padding: 0;
                                                                           font-size: 1.2rem;
                                                                           font-weight: 600;
                                                                           color: #48465b;
                                                                           text-align: center;padding: 20px 0px 20px 0px;
                                                                       "><?php echo max(0, $leave_list_details['total_leave_available']-$leave_list_details['total_leave_taken']); ?> Leaves Remaning</h3>
                                   </div>
                               </div>
                               <div style="
                                   padding: 25px 0px 15px 25px;
                               ">
                                   <span class="kt-font-left" style="color: gray;
                                               font-weight: 900;">Reason</span>
                               </div>                                    
                               <div class="kt-widget19__text" style="padding: 0px 25px;
                                   font-size: 1.2rem;
                                   font-weight: 600;
                                   color: #48465b;">
                                   <?php echo $leave_list_details['reason']; ?>
                               </div>
                               <div style="
                                   padding: 25px 25px 15px 25px;
                               ">
                                   <span class="kt-font-left" style="color: gray;
                                   font-weight: 900;">Approval Process</span>
                               </div>
                               <div style="padding: 0px 25px;">
                                   
                                   <div class="progress"style="height: 1.5rem;">
                                       <div class="progress-bar <?php echo $leave_list_details['request_status_color']; ?>" role="progressbar" style="width: 100%;color: black;font-weight: 600;font-size: 1.2rem !important;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"><?php echo $leave_list_details['request_status'];?></div>
                                   </div>
                                </div>
                                <?php if($leave_list_details['action_button_visible']) {?>
                                <div class="kt-widget19__content" style="padding: 25px 25px 0px 25px;margin: 0px 0px 0px 0px;">
                                    <div class="kt-widget19__userpic">
                                       <img src="<?php echo base_url('assets/hr_document/profile_pic/'.$leave_list_details['profile_pic']);?>" alt=""style="height: 50px;width: 50px; border-radius: 10px !important;">
                                    </div>
                                    <div class="kt-widget19__info">
                                        <div class="row">
                                            <div class="col-xl-2"></div>
                                            <div class="col-xl-10 kt-font-right" style="padding: 0px 0px 0px 0px;">
                                               
                                                <button type="button" class="btn btn-success approve_leave" style="width:45%" leave-request-id="<?php echo $leave_list_details['request_info_id'];?>" request-type="Accepted">Approve</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <button type="button" class="btn reject_leave" style="width:40%;background-color: darkgray;border-color: darkgray;"leave-request-id="<?php echo $leave_list_details['request_info_id'];?>" request-type="Not Accepted">Reject</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-widget19__stats">
                                        <button type="button" class="btn btn-outline-hover-info btn-elevate btn-icon cancel_leave" style="background-color: white;border-color: white;"leave-request-id="<?php echo $leave_list_details['request_info_id'];?>" request-type="cancel"><i class="la la-close"></i></button>
                                    </div>
                                </div>
                                <?php }?>
                           </div>     
                       </div>
                   </div>
                   <!--end:: Widgets/Blog-->
                </div> 
            </div>
            <?php } ?>
        </div>
<?php   
    }
} else {
?>        
    <div class="row" style="height:auto;">
        <div class="col-xl-4 col-lg-6 order-lg-1 order-xl-1">
            <div class="kt-portlet">
                <!--begin:: Widgets/Blog-->
                <div class="kt-portlet kt-portlet--height-fluid kt-widget19" style="font-style: normal;font-family: Helvetica Neue,Helvetica,Arial,sans-serif;">
                    <div class="kt-portlet__body" style="padding: 25px 0px 0px 0px;">
                        <div class="kt-widget19__wrapper">
                            <div style="padding: 0px 0px 10px 25px;">
                                <h3 class="kt-portlet__head-title" style="
                                   margin: 0;
                                   padding: 0;
                                   font-size: 1.2rem;
                                   font-weight: 600;
                                   color: #48465b;
                                "><?php echo 'Leaves Not Found!!!'; ?></h3>
                            </div>
                        </div>     
                    </div>
                </div>
                <!--end:: Widgets/Blog-->
            </div> 
        </div>
    </div>
<?php } ?>