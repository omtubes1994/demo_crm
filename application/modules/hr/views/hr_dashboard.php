<style type="text/css">
    .kt-aside--fixed .kt-wrapper {
        padding-left: 130px !important;
    }
    .kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper {
        padding-top: 60px !important;
    }
</style>
        <!-- end:: Header Mobile -->
        <div class="kt-grid kt-grid--hor kt-grid--root">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                        <!-- begin:: Content Head -->
                        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                            <div class="kt-container  kt-container--fluid ">
                                <div class="kt-subheader__main">
                                    <h3 class="kt-subheader__title">Dashboard</h3>
                                    <span class="kt-subheader__separator kt-hidden"></span>
                                    <div class="kt-subheader__breadcrumbs">
                                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                                        <span class="kt-subheader__breadcrumbs-separator"></span>
                                        <a href="" class="kt-subheader__breadcrumbs-link">Hr</a>
                                        <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Dashboard</span>
                                    </div>
                                </div>
                                <div class="kt-subheader__toolbar">
                                    <div class="kt-subheader__wrapper">
                                        <a href="javascript:void(0)" class="btn kt-subheader__btn-daterange" id="kt_dashboard_daterangepicker" data-toggle="kt-tooltip" title="Select dashboard daterange" data-placement="left">
                                            <span class="kt-subheader__btn-daterange-title" id="kt_dashboard_daterangepicker_title">Today</span>&nbsp;
                                            <span class="kt-subheader__btn-daterange-date" id="kt_dashboard_daterangepicker_date">Aug 16</span>
                                            <i class="flaticon2-calendar-1"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- end:: Content Head -->

                        <!-- begin:: Content -->
                        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

                            <!--Begin::Dashboard 6-->

                            <!--begin:: Widgets/Stats-->
                            <div class="kt-portlet">
                                <div class="kt-portlet__body  kt-portlet__body--fit">
                                    <div class="row row-no-padding row-col-separator-lg">
                                        <div class="col-md-12 col-lg-6 col-xl-2">

                                            <!--begin::Total Profit-->
                                            <div class="kt-widget24">
                                                <div class="kt-widget24__details">
                                                    <div class="kt-widget24__info">
                                                        <h4 class="kt-widget24__title">
                                                            Total Employees
                                                        </h4>
                                                        <!-- <span class="kt-widget24__desc">
                                                            Employees Added till 2021
                                                        </span> -->
                                                    </div>
                                                    <span class="kt-widget24__stats kt-font-brand">
                                                        50
                                                    </span>
                                                </div>
                                                <div class="progress" style="height: 0.7rem;">
                                                    <div class="progress-bar bg-warning" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                                    <div class="progress-bar bg-success" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                                                    <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                                <div class="kt-widget24__action">
                                                    <span class="kt-widget24__change" style="color: #5578eb !important">
                                                        Emp Added 2021
                                                    </span>
                                                    <span class="kt-widget24__number" style="color: #5578eb !important">
                                                        25
                                                    </span>
                                                </div>
                                                <div class="kt-widget24__action">
                                                    <span class="kt-widget24__change" style="color: #0abb87 !important">
                                                        Emp Added 2020
                                                    </span>
                                                    <span class="kt-widget24__number" style="color: #0abb87 !important">
                                                        15
                                                    </span>
                                                </div>
                                                <div class="kt-widget24__action">
                                                    <span class="kt-widget24__change" style="color: #ffb822 !important">
                                                        Emp Added 2019
                                                    </span>
                                                    <span class="kt-widget24__number" style="color: #ffb822 !important">
                                                        10
                                                    </span>
                                                </div>
                                            </div>

                                            <!--end::Total Profit-->
                                        </div>
                                        <div class="col-md-12 col-lg-6 col-xl-2">

                                            <!--begin::New Feedbacks-->
                                            <div class="kt-widget24">
                                                <div class="kt-widget24__details">
                                                    <div class="kt-widget24__info">
                                                        <h4 class="kt-widget24__title">
                                                            Effective Growth Rate
                                                        </h4>
                                                        <!-- <span class="kt-widget24__desc">
                                                            Customer Review
                                                        </span> -->
                                                    </div>
                                                    <span class="kt-widget24__stats kt-font-warning">
                                                        120%
                                                    </span>
                                                </div>
                                                <div class="progress" style="height: 0.7rem;">
                                                    <div class="progress-bar bg-warning" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                                    <div class="progress-bar bg-success" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                                                    <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                                <div class="kt-widget24__action">
                                                    <span class="kt-widget24__change" style="color: #5578eb !important">
                                                        Growth rate 2021
                                                    </span>
                                                    <span class="kt-widget24__number" style="color: #5578eb !important">
                                                        50%
                                                    </span>
                                                </div>
                                                <div class="kt-widget24__action">
                                                    <span class="kt-widget24__change" style="color: #0abb87 !important">
                                                        Growth rate 2020
                                                    </span>
                                                    <span class="kt-widget24__number" style="color: #0abb87 !important">
                                                        40%
                                                    </span>
                                                </div>
                                                <div class="kt-widget24__action">
                                                    <span class="kt-widget24__change" style="color: #ffb822 !important">
                                                        Growth rate 2019
                                                    </span>
                                                    <span class="kt-widget24__number" style="color: #ffb822 !important">
                                                        10
                                                    </span>
                                                </div>
                                            </div>

                                            <!--end::New Feedbacks-->
                                        </div>
                                        <div class="col-md-12 col-lg-6 col-xl-2">

                                            <!--begin::New Orders-->
                                            <div class="kt-widget24">
                                                <div class="kt-widget24__details">
                                                    <div class="kt-widget24__info">
                                                        <h4 class="kt-widget24__title">
                                                            Hirings
                                                        </h4>
                                                        <!-- <span class="kt-widget24__desc">
                                                            Customer Review
                                                        </span> -->
                                                    </div>
                                                    <span class="kt-widget24__stats kt-font-danger">
                                                        60%
                                                    </span>
                                                </div>
                                                <div class="progress" style="height: 0.7rem;">
                                                    <div class="progress-bar bg-warning" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                                    <div class="progress-bar bg-success" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                                                    <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                                <div class="kt-widget24__action">
                                                    <span class="kt-widget24__change" style="color: #5578eb !important">
                                                        Hiring 2021
                                                    </span>
                                                    <span class="kt-widget24__number" style="color: #5578eb !important">
                                                        50%
                                                    </span>
                                                </div>
                                                <div class="kt-widget24__action">
                                                    <span class="kt-widget24__change" style="color: #0abb87 !important">
                                                        Hiring 2020
                                                    </span>
                                                    <span class="kt-widget24__number" style="color: #0abb87 !important">
                                                        40%
                                                    </span>
                                                </div>
                                                <div class="kt-widget24__action">
                                                    <span class="kt-widget24__change" style="color: #ffb822 !important">
                                                        Hiring 2019
                                                    </span>
                                                    <span class="kt-widget24__number" style="color: #ffb822 !important">
                                                        10%
                                                    </span>
                                                </div>
                                            </div>

                                            <!--end::New Orders-->
                                        </div>
                                        <div class="col-md-12 col-lg-6 col-xl-2">

                                            <!--begin::New Users-->
                                            <div class="kt-widget24">
                                                <div class="kt-widget24__details">
                                                    <div class="kt-widget24__info">
                                                        <h4 class="kt-widget24__title">
                                                            Month Hirings
                                                        </h4>
                                                        <!-- <span class="kt-widget24__desc">
                                                            Customer Review
                                                        </span> -->
                                                    </div>
                                                    <span class="kt-widget24__stats kt-font-success">
                                                        60%
                                                    </span>
                                                </div>
                                                <div class="progress" style="height: 0.7rem;">
                                                    <div class="progress-bar bg-warning" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                                    <div class="progress-bar bg-success" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                                                    <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                                <div class="kt-widget24__action">
                                                    <span class="kt-widget24__change" style="color: #5578eb !important">
                                                        March 2021
                                                    </span>
                                                    <span class="kt-widget24__number" style="color: #5578eb !important">
                                                        50%
                                                    </span>
                                                </div>
                                                <div class="kt-widget24__action">
                                                    <span class="kt-widget24__change" style="color: #0abb87 !important">
                                                        March 2020
                                                    </span>
                                                    <span class="kt-widget24__number" style="color: #0abb87 !important">
                                                        40%
                                                    </span>
                                                </div>
                                                <div class="kt-widget24__action">
                                                    <span class="kt-widget24__change" style="color: #ffb822 !important">
                                                        March 2019
                                                    </span>
                                                    <span class="kt-widget24__number" style="color: #ffb822 !important">
                                                        10%
                                                    </span>
                                                </div>
                                            </div>

                                            <!--end::New Users-->
                                        </div>
                                        <div class="col-md-12 col-lg-6 col-xl-2">

                                            <!--begin::New Users-->
                                            <div class="kt-widget24">
                                                <div class="kt-widget24__details">
                                                    <div class="kt-widget24__info">
                                                        <h4 class="kt-widget24__title">
                                                            Attrition
                                                        </h4>
                                                        <!-- <span class="kt-widget24__desc">
                                                            Customer Review
                                                        </span> -->
                                                    </div>
                                                    <span class="kt-widget24__stats kt-font-primary">
                                                        60%
                                                    </span>
                                                </div>
                                                <div class="progress" style="height: 0.7rem;">
                                                    <div class="progress-bar bg-warning" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                                    <div class="progress-bar bg-success" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                                                    <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                                <div class="kt-widget24__action">
                                                    <span class="kt-widget24__change" style="color: #5578eb !important">
                                                        Attrition 2021
                                                    </span>
                                                    <span class="kt-widget24__number" style="color: #5578eb !important">
                                                        50%
                                                    </span>
                                                </div>
                                                <div class="kt-widget24__action">
                                                    <span class="kt-widget24__change" style="color: #0abb87 !important">
                                                        Attrition 2020
                                                    </span>
                                                    <span class="kt-widget24__number" style="color: #0abb87 !important">
                                                        40%
                                                    </span>
                                                </div>
                                                <div class="kt-widget24__action">
                                                    <span class="kt-widget24__change" style="color: #ffb822 !important">
                                                        Attrition 2019
                                                    </span>
                                                    <span class="kt-widget24__number" style="color: #ffb822 !important">
                                                        10%
                                                    </span>
                                                </div>
                                            </div>

                                            <!--end::New Users-->
                                        </div>
                                        <div class="col-md-12 col-lg-6 col-xl-2">

                                            <!--begin::New Users-->
                                            <div class="kt-widget24">
                                                <div class="kt-widget24__details">
                                                    <div class="kt-widget24__info">
                                                        <h4 class="kt-widget24__title">
                                                            Monthly Attrition
                                                        </h4>
                                                        <!-- <span class="kt-widget24__desc">
                                                            Customer Review
                                                        </span> -->
                                                    </div>
                                                    <span class="kt-widget24__stats kt-font-brand">
                                                        60%
                                                    </span>
                                                </div>
                                                <div class="progress" style="height: 0.7rem;">
                                                    <div class="progress-bar bg-warning" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                                    <div class="progress-bar bg-success" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                                                    <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                                <div class="kt-widget24__action">
                                                    <span class="kt-widget24__change" style="color: #5578eb !important">
                                                        March 2021
                                                    </span>
                                                    <span class="kt-widget24__number" style="color: #5578eb !important">
                                                        50%
                                                    </span>
                                                </div>
                                                <div class="kt-widget24__action">
                                                    <span class="kt-widget24__change" style="color: #0abb87 !important">
                                                        March 2020
                                                    </span>
                                                    <span class="kt-widget24__number" style="color: #0abb87 !important">
                                                        40%
                                                    </span>
                                                </div>
                                                <div class="kt-widget24__action">
                                                    <span class="kt-widget24__change" style="color: #ffb822 !important">
                                                        March 2019
                                                    </span>
                                                    <span class="kt-widget24__number" style="color: #ffb822 !important">
                                                        10%
                                                    </span>
                                                </div>
                                            </div>

                                            <!--end::New Users-->
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--end:: Widgets/Stats-->

                            <!--Begin::Row-->
                            <div class="row">
                                <div class="col-xl-6">

                                   <!--begin:: Widgets/Inbound Bandwidth-->
                                    
                                    <div class="kt-portlet kt-portlet--tab">
                                        <div class="kt-portlet__body">
                                            <div id="container" style="height:500px;"></div>
                                        </div>
                                    </div>
                                    <!--end:: Widgets/Inbound Bandwidth-->
                                </div>
                                <div class="col-xl-6">

                                    <!--begin:: Widgets/Order Statistics-->
                                    <div class="kt-portlet kt-portlet--height-fluid">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Order Statistics
                                                </h3>
                                            </div>
                                            <div class="kt-portlet__head-toolbar">
                                                <a href="#" class="btn btn-label-brand btn-bold btn-sm dropdown-toggle" data-toggle="dropdown">
                                                    Export
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">

                                                    <!--begin::Nav-->
                                                    <ul class="kt-nav">
                                                        <li class="kt-nav__head">
                                                            Export Options
                                                            <span data-toggle="kt-tooltip" data-placement="right" title="Click to learn more...">
                                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand kt-svg-icon--md1">
                                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                        <rect x="0" y="0" width="24" height="24" />
                                                                        <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                                                                        <rect fill="#000000" x="11" y="10" width="2" height="7" rx="1" />
                                                                        <rect fill="#000000" x="11" y="7" width="2" height="2" rx="1" />
                                                                    </g>
                                                                </svg> </span>
                                                        </li>
                                                        <li class="kt-nav__separator"></li>
                                                        <li class="kt-nav__item">
                                                            <a href="#" class="kt-nav__link">
                                                                <i class="kt-nav__link-icon flaticon2-drop"></i>
                                                                <span class="kt-nav__link-text">Activity</span>
                                                            </a>
                                                        </li>
                                                        <li class="kt-nav__item">
                                                            <a href="#" class="kt-nav__link">
                                                                <i class="kt-nav__link-icon flaticon2-calendar-8"></i>
                                                                <span class="kt-nav__link-text">FAQ</span>
                                                            </a>
                                                        </li>
                                                        <li class="kt-nav__item">
                                                            <a href="#" class="kt-nav__link">
                                                                <i class="kt-nav__link-icon flaticon2-telegram-logo"></i>
                                                                <span class="kt-nav__link-text">Settings</span>
                                                            </a>
                                                        </li>
                                                        <li class="kt-nav__item">
                                                            <a href="#" class="kt-nav__link">
                                                                <i class="kt-nav__link-icon flaticon2-new-email"></i>
                                                                <span class="kt-nav__link-text">Support</span>
                                                                <span class="kt-nav__link-badge">
                                                                    <span class="kt-badge kt-badge--success kt-badge--rounded">5</span>
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <li class="kt-nav__separator"></li>
                                                        <li class="kt-nav__foot">
                                                            <a class="btn btn-label-danger btn-bold btn-sm" href="#">Upgrade plan</a>
                                                            <a class="btn btn-clean btn-bold btn-sm" href="#" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more...">Learn more</a>
                                                        </li>
                                                    </ul>

                                                    <!--end::Nav-->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body kt-portlet__body--fluid">
                                            <div class="kt-widget12">
                                                <div class="kt-widget12__content">
                                                    <div class="kt-widget12__item">
                                                        <div class="kt-widget12__info">
                                                            <span class="kt-widget12__desc">Annual Taxes EMS</span>
                                                            <span class="kt-widget12__value">$400,000</span>
                                                        </div>
                                                        <div class="kt-widget12__info">
                                                            <span class="kt-widget12__desc">Finance Review Date</span>
                                                            <span class="kt-widget12__value">July 24,2019</span>
                                                        </div>
                                                    </div>
                                                    <div class="kt-widget12__item">
                                                        <div class="kt-widget12__info">
                                                            <span class="kt-widget12__desc">Avarage Revenue</span>
                                                            <span class="kt-widget12__value">$60M</span>
                                                        </div>
                                                        <div class="kt-widget12__info">
                                                            <span class="kt-widget12__desc">Revenue Margin</span>
                                                            <div class="kt-widget12__progress">
                                                                <div class="progress kt-progress--sm">
                                                                    <div class="progress-bar kt-bg-brand" role="progressbar" style="width: 40%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                                                </div>
                                                                <span class="kt-widget12__stat">
                                                                    40%
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="kt-widget12__chart" style="height:250px;">
                                                    <canvas id="kt_chart_order_statistics"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!--end:: Widgets/Order Statistics-->
                                </div>
                            </div>
                            <!--End::Row-->

                            <!--End::Dashboard 6-->
                        </div>

                        <!-- end:: Content -->
                    </div>

                    <!-- begin:: Footer -->
                    <div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
                        <div class="kt-container  kt-container--fluid ">
                            <div class="kt-footer__copyright">
                                2020&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Keenthemes</a>
                            </div>
                            <div class="kt-footer__menu">
                                <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">About</a>
                                <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Team</a>
                                <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Contact</a>
                            </div>
                        </div>
                    </div>

                    <!-- end:: Footer -->
                </div>
            </div>
        </div>

        <!-- end:: Page -->

        <!-- begin::Scrolltop -->
        <div id="kt_scrolltop" class="kt-scrolltop">
            <i class="fa fa-arrow-up"></i>
        </div>

        <!-- end::Scrolltop -->