<?php if(!empty($user_list)) {?>
    <?php 
        $image_not_found_array = array('success', 'danger', 'warning', 'info', 'dark', 'brand');
        $image_not_found_array_key = 0;
        $user_list_2_chunk = array_chunk($user_list, 2);
    ?>
    <?php foreach($user_list_2_chunk as $user_list_2_data) {?>
        <div class="row">
        <?php foreach($user_list_2_data as $single_user_details) {?>
            <div class="col-xl-6">
                <!--begin:: Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__body">
                        <div class="kt-widget kt-widget--user-profile-3">
                            <div class="kt-widget__top">
                                <?php if(!empty($single_user_details['profile_pic_file_path'])) {?>
                                    <div class="kt-widget__media kt-hidden-">
                                        <!-- <img src="<?php echo base_url('assets/hr_document/profile_pic/'.$single_user_details['profile_pic_file_path']);?>" alt="image"> -->
                                        <img src="<?php echo 'https://crm.omtubes.com/assets/hr_document/profile_pic/'.$single_user_details['profile_pic_file_path'];?>" alt="image"  style="max-width: 110px;max-height: 110px;">
                                    </div>
                                <?php } else {?>    
                                    <div class="kt-widget__pic kt-widget__pic--<?php echo $image_not_found_array[$image_not_found_array_key%5];?> kt-font-<?php echo $image_not_found_array[$image_not_found_array_key%5];?> kt-font-boldest kt-font-light kt-hidden-">
                                        <?php echo ucfirst($single_user_details['first_name'][0]), ucfirst($single_user_details['last_name'][0]);?>
                                    </div>
                                <?php $image_not_found_array_key++;}?>
                                <div class="kt-widget__content">
                                    <div class="kt-widget__head">
                                        <a href="#" class="kt-widget__username">
                                            <?php echo trim($single_user_details['first_name']),' ', trim($single_user_details['last_name']);?>
                                            <i class="flaticon2-correct kt-font-success"></i>
                                        </a>
                                        <div class="kt-widget__action">
                                            <a href="<?php echo base_url('hr/edit_listing/account_setting/'.$single_user_details['id']);?>" target="_blank" class="btn btn-brand btn-sm btn-upper">
                                                <i class="la la-edit"></i>
                                                Detail
                                            </a>
                                        </div>
                                    </div>
                                    <div class="kt-widget__subhead">
                                        <a href="#"><i class="flaticon2-new-email"></i><?php echo $single_user_details['official_email']; ?></a>
                                        <a href="#"><i class="flaticon2-calendar-3"></i><?php echo $single_user_details['department']; ?></a>
                                        <a href="#"><i class="flaticon2-placeholder"></i><?php echo $single_user_details['official_number']; ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end:: Portlet-->
            </div>
        <?php } ?>
        </div>
    <?php } ?>
<?php } ?>