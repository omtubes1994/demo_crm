
<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
				HTML Table
				<small>Datatable initialized from HTML table</small>
			</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">
					<div class="dropdown dropdown-inline">
						<button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="la la-download"></i> Export
						</button>
						<div class="dropdown-menu dropdown-menu-right">
							<ul class="kt-nav">
								<li class="kt-nav__section kt-nav__section--first">
									<span class="kt-nav__section-text">Choose an option</span>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon la la-print"></i>
										<span class="kt-nav__link-text">Print</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon la la-copy"></i>
										<span class="kt-nav__link-text">Copy</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon la la-file-excel-o"></i>
										<span class="kt-nav__link-text">Excel</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon la la-file-text-o"></i>
										<span class="kt-nav__link-text">CSV</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon la la-file-pdf-o"></i>
										<span class="kt-nav__link-text">PDF</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
					&nbsp;
					<a href="#" class="btn btn-brand btn-elevate btn-icon-sm">
						<i class="la la-plus"></i>
						New Record
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="kt-portlet__body">
	</div>
	<div class="kt-portlet__body kt-portlet__body--fit">

		<!--begin: Datatable -->
		<div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded"><table class="kt-datatable__table" id="html_table" width="100%" style="display: block;">
			<thead class="kt-datatable__head">
				<tr class="kt-datatable__row" style="left: 0px;">
					 <th data-field="Order ID" class="kt-datatable__cell kt-datatable__cell--sort">
							<span style="width: 168px;">salary ID</span>
					</th>
					<th data-field="Order ID" class="kt-datatable__cell kt-datatable__cell--sort">
							<span style="width: 168px;">people ID</span>
					</th>
			            <?php 
			     	 	for ($i=2;$i<count($column_list)-4;$i=$i+2) {
					      $sum = $column_list[$i]; 
					      ?>
					      <th data-field="Order ID" class="kt-datatable__cell kt-datatable__cell--sort">
				             <span style="width: 156px;"><?php echo $sum ;?>
				             </span>
			             </th>
					   <?php
					  }					 
			     	 	?>
			     	<th data-field="Order ID" class="kt-datatable__cell kt-datatable__cell--sort">
							<span style="width: 156px;">Action</span>
					</th>
		        </tr>
			</thead>
			<tbody style="" class="kt-datatable__body salary_slip_default_list">
				<?php 
				//echo "<pre>";print_r($salary_slip_details);echo"</pre><hr>";
				if(!empty($salary_slip_details)) {
					foreach ($salary_slip_details as $salary_slip_details_key => $salary_slip_details_value) {
						$val=$salary_slip_details_value;
						$arr = array_values($val);
						//echo "<pre>";print_r($val);echo"</pre><hr>";
					?>
				<tr data-row="0" class="kt-datatable__row" style="left: 0px;">
					<td class="kt-datatable__cell kt-datatable__toggle-detail">
						<a class="kt-datatable__toggle-detail" href="">
							<i class="fa fa-caret-right">
								
							</i>
						</a>
				    </td>
				     <td data-field="Order ID" class="kt-datatable__cell">
				    	<span style="width: 156px;"><?php echo $salary_slip_details_key+1;?>
				        </span>
				   </td>
				       <?php 
				     
					    for ($i=0;$i<count($arr)-4;$i=$i+2) {
						    $sum = $arr[$i];	      
                  	 //echo "<pre>";print_r($sum);echo"</pre><hr>";?>
					 <td data-field="Order ID" class="kt-datatable__cell"><span style="width: 156px;"><?php echo $sum ;
					?></span>
					 </td>

					 <?php  }?>
					 <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell">
						<span style="overflow: visible; position: relative; width: 110px;">	
							</div>						
								<a href="<?php echo base_url('hr/add_salary_slip_default_details/'.$salary_slip_details_value['salary_slip_id'])?>;" class="btn btn-sm btn-clean btn-icon btn-icon-md salary_slip_default_list_data_edit"
								 title="Edit details">		
									<i class="la la-edit"></i>						
								</a>						
								<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md salary_slip_default_list_data"  salary_slip_id="<?php echo $salary_slip_details_value['salary_slip_id']; ?>"  title="Delete">				
									<i class="la la-trash"></i>						
								</a>					
						</span>
					</td>
					</tr>
					<?php }
				    }
					?>  
				</tbody>
		</table>
		<div class="kt-datatable__pager kt-datatable--paging-loaded">
			<ul class="kt-datatable__pager-nav">
				<li>
					<a title="First" class="kt-datatable__pager-link kt-datatable__pager-link--first kt-datatable__pager-link--disabled" data-page="1" disabled="disabled"><i class="flaticon2-fast-back"></i></a></li><li><a title="Previous" class="kt-datatable__pager-link kt-datatable__pager-link--prev kt-datatable__pager-link--disabled" data-page="1" disabled="disabled"><i class="flaticon2-back"></i></a></li><li style=""></li><li style="display: none;"><input type="text" class="kt-pager-input form-control" title="Page number"></li><li><a class="kt-datatable__pager-link kt-datatable__pager-link-number kt-datatable__pager-link--active" data-page="1" title="1">1</a></li><li><a class="kt-datatable__pager-link kt-datatable__pager-link-number" data-page="2" title="2">2</a></li><li><a class="kt-datatable__pager-link kt-datatable__pager-link-number" data-page="3" title="3">3</a></li><li><a class="kt-datatable__pager-link kt-datatable__pager-link-number" data-page="4" title="4">4</a></li><li><a class="kt-datatable__pager-link kt-datatable__pager-link-number" data-page="5" title="5">5</a></li><li></li><li><a title="Next" class="kt-datatable__pager-link kt-datatable__pager-link--next" data-page="2"><i class="flaticon2-next"></i></a></li><li><a title="Last" class="kt-datatable__pager-link kt-datatable__pager-link--last" data-page="15"><i class="flaticon2-fast-next"></i></a></li></ul><div class="kt-datatable__pager-info"><div class="dropdown bootstrap-select kt-datatable__pager-size" style="width: 60px;"><select class="selectpicker kt-datatable__pager-size" title="Select page size" data-width="60px" data-container="body" data-selected="10" tabindex="-98"><option class="bs-title-option" value=""></option><option value="5">5</option><option value="10">10</option><option value="20">20</option><option value="30">30</option><option value="50">50</option><option value="100">100</option></select><button type="button" class="btn dropdown-toggle btn-light" data-toggle="dropdown" role="combobox" aria-owns="bs-select-3" aria-haspopup="listbox" aria-expanded="false" title="Select page size"><div class="filter-option"><div class="filter-option-inner"><div class="filter-option-inner-inner">10</div></div> </div></button><div class="dropdown-menu "><div class="inner show" role="listbox" id="bs-select-3" tabindex="-1"><ul class="dropdown-menu inner show" role="presentation"></ul></div></div></div><span class="kt-datatable__pager-detail">Showing 1 - 10 of 143</span></div></div></div>

		<!--end: Datatable -->
	</div>
							
</div>