<style type="text/css">
    .kt-aside--fixed .kt-wrapper {
        padding-left: 130px !important;
    }
    .kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper {
        padding-top: 60px !important;
    }
</style>
		<div class="kt-grid kt-grid--hor kt-grid--root">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

					<!-- end:: Header -->
					<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

						<!-- begin:: Content -->
						<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<div class="kt-portlet">
								<div class="kt-portlet__body kt-portlet__body--fit">
									<div class="kt-grid  kt-wizard-v2 kt-wizard-v2--white" id="kt_wizard_v2" data-ktwizard-state="step-first">
										<div class="kt-grid__item kt-wizard-v2__aside">

											<!--begin: Form Wizard Nav -->
											<div class="kt-wizard-v2__nav">

												<!--doc: Remove "kt-wizard-v2__nav-items--clickable" class and also set 'clickableSteps: false' in the JS init to disable manually clicking step titles -->
												<div class="kt-wizard-v2__nav-items kt-wizard-v2__nav-items--clickable">
													<div class="kt-wizard-v2__nav-item" data-ktwizard-type="step" data-ktwizard-state="current">
														<div class="kt-wizard-v2__nav-body">
															<div class="kt-wizard-v2__nav-icon">
																<i class="flaticon-globe"></i>
															</div>
															<div class="kt-wizard-v2__nav-label">
																<div class="kt-wizard-v2__nav-label-title">
																	Account Settings
																</div>
																<div class="kt-wizard-v2__nav-label-desc">
																	Setup Your Account Details
																</div>
															</div>
														</div>
													</div>
													<div class="kt-wizard-v2__nav-item" data-ktwizard-type="step">
														<div class="kt-wizard-v2__nav-body">
															<div class="kt-wizard-v2__nav-icon">
																<i class="flaticon-globe"></i>
															</div>
															<div class="kt-wizard-v2__nav-label">
																<div class="kt-wizard-v2__nav-label-title">
																	Personal Information
																</div>
																<div class="kt-wizard-v2__nav-label-desc">
																	Setup Your Personal Details
																</div>
															</div>
														</div>
													</div>
													<div class="kt-wizard-v2__nav-item" data-ktwizard-type="step">
														<div class="kt-wizard-v2__nav-body">
															<div class="kt-wizard-v2__nav-icon">
																<i class="flaticon-bus-stop"></i>
															</div>
															<div class="kt-wizard-v2__nav-label">
																<div class="kt-wizard-v2__nav-label-title">
																	Residential Address
																</div>
																<div class="kt-wizard-v2__nav-label-desc">
																	Enter Your residention address
																</div>
															</div>
														</div>
													</div>
													<div class="kt-wizard-v2__nav-item actual_address" data-ktwizard-type="step">
														<div class="kt-wizard-v2__nav-body">
															<div class="kt-wizard-v2__nav-icon">
																<i class="flaticon-trophy"></i>
															</div>
															<div class="kt-wizard-v2__nav-label">
																<div class="kt-wizard-v2__nav-label-title">
																	Actual Address
																</div>
																<div class="kt-wizard-v2__nav-label-desc">
																	If staying Out of Mumbai
																</div>
															</div>
														</div>
													</div>
													<div class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
														<div class="kt-wizard-v2__nav-body">
															<div class="kt-wizard-v2__nav-icon">
																<i class="flaticon-responsive"></i>
															</div>
															<div class="kt-wizard-v2__nav-label">
																<div class="kt-wizard-v2__nav-label-title">
																	Employee Details
																</div>
																<div class="kt-wizard-v2__nav-label-desc">
																	Department, Designation, Location, Qualification
																</div>
															</div>
														</div>
													</div>
													<div class="kt-wizard-v2__nav-item" data-ktwizard-type="step">
														<div class="kt-wizard-v2__nav-body">
															<div class="kt-wizard-v2__nav-icon">
																<i class="flaticon-truck"></i>
															</div>
															<div class="kt-wizard-v2__nav-label">
																<div class="kt-wizard-v2__nav-label-title">
																	Bank Details
																</div>
																<div class="kt-wizard-v2__nav-label-desc">
																	Bank Name, IFSC Code, Account Number
																</div>
															</div>
														</div>
													</div>
													<div class="kt-wizard-v2__nav-item" data-ktwizard-type="step">
														<div class="kt-wizard-v2__nav-body">
															<div class="kt-wizard-v2__nav-icon">
																<i class="flaticon-truck"></i>
															</div>
															<div class="kt-wizard-v2__nav-label">
																<div class="kt-wizard-v2__nav-label-title">
																	References
																</div>
																<div class="kt-wizard-v2__nav-label-desc">
																	Reference Name, Number, Email
																</div>
															</div>
														</div>
													</div>
													<div class="kt-wizard-v2__nav-item last_step" data-ktwizard-type="step">
														<div class="kt-wizard-v2__nav-body">
															<div class="kt-wizard-v2__nav-icon">
																<i class="flaticon-confetti"></i>
															</div>
															<div class="kt-wizard-v2__nav-label">
																<div class="kt-wizard-v2__nav-label-title">
																	Completed!
																</div>
																<div class="kt-wizard-v2__nav-label-desc">
																	Review and Submit
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>

											<!--end: Form Wizard Nav -->
										</div>
										<div class="kt-grid__item kt-grid__item--fluid kt-wizard-v2__wrapper">

											<!--begin: Form Wizard Form-->
											<form class="kt-form" id="add_employee_form">

												<!--begin: Form Wizard Step 1-->
												<div class="kt-wizard-v2__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
													<div class="kt-heading kt-heading--md">Enter your Account Details</div>
													<div class="kt-form__section kt-form__section--first">
														<div class="kt-wizard-v2__form">
															<div class="row">
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>First Name</label>
																		<input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name">
																		<span class="form-text text-muted">Please enter your first name.</span>
																	</div>
																</div>
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>Last Name</label>
																		<input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
																		<span class="form-text text-muted">Please enter your last name.</span>
																	</div>
																</div>
																	
															</div>
															<div class="row">
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>Gender:</label>
																		<select name="gender" id="gender" class="form-control">
																			<option value="">Select</option>
																			<?php foreach($gender as $gender_value) {?>
																				<option value="<?php echo $gender_value['name'];?>"><?php echo ucfirst($gender_value['name']);?></option>
																			<?php }?>
																		</select>
																	</div>
																</div>
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>Blood Group:</label>
																		<select name="blood_group" id="blood_group" class="form-control">
																			<option value="">Select</option>
																			<?php foreach($blood_group as $blood_group_value) {?>
																				<option value="<?php echo $blood_group_value['name'];?>"><?php echo ucfirst($blood_group_value['name']);?></option>
																			<?php }?>
																		</select>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>Date Of Birth:</label>
																		<div class="input-group date">
																			<input type="text" class="form-control" readonly value="" id="birth_date" name="birth_date" />
																			<div class="input-group-append">
																				<span class="input-group-text">
																					<i class="la la-calendar"></i>
																				</span>
																			</div>
																		</div>
																		<span class="form-text text-muted"></span>
																	</div>
																</div>
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>Age:</label>
																		<input type="text" class="form-control" id="age" name="age" value="" readonly>
																	</div>
																</div>
															</div>
															
														</div>
													</div>
												</div>
												<!--end: Form Wizard Step 1-->

												<!--begin: Form Wizard Step 2-->
												<div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
													<div class="kt-heading kt-heading--md">Enter your Personal Details</div>
													<div class="kt-form__section kt-form__section--first">
														<div class="kt-wizard-v2__form">
															<div class="row">
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>Personal Mobile</label>
																		<input type="tel" class="form-control" id="personal_number" name="personal_number" placeholder="phone">
																		<span class="form-text text-muted">Please enter your phone number.</span>
																	</div>
																</div>
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>Personal Email</label>
																		<input type="email" class="form-control" id="personal_email" name="personal_email" placeholder="Email">
																		<span class="form-text text-muted">Please enter your email address.</span>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>Marital Status:</label>
																		<select id="marriage_status" name="marriage_status" class="form-control">
																			<option value="">Select</option>
																			<?php foreach($marriage_info as $marriage_value) { ?>
																				<option value="<?php echo $marriage_value['name'];?>"><?php echo ucfirst($marriage_value['name']);?></option>
																			<?php }?>
																		</select>
																	</div>
																</div>
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>Qualification:</label>
																		<select id="qualification" name="qualification" class="form-control">
																			<option value="">Select</option>
																			<?php foreach($qualification as $qualification_value) {?>
																				<option value="<?php echo $qualification_value['name'];?>"><?php echo ucfirst($qualification_value['name']);?></option>
																			<?php }?>
																		</select>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>Aadhar Card Number</label>
																		<input type="tel" class="form-control" id="aadhar_number" name="aadhar_number" placeholder="phone">
																		<span class="form-text text-muted">Please enter your Aadhar card number.</span>
																	</div>
																</div>
																<div class="col-xl-6">
																	<div class="form-group form-group-last row">
																		<label class="col-lg-3 col-form-label">Upload:</label>
																		<div class="col-lg-9">
																			<div class="dropzone dropzone-multi" id="kt_dropzone_5">
																				<div class="dropzone-panel">
																					<a class="dropzone-select btn btn-label-brand btn-bold btn-sm">Attach files</a>
																				</div>
																				<div class="dropzone-items">
																					<div class="dropzone-item" style="display:none">
																						<div class="dropzone-file">
																							<div class="dropzone-filename" title="some_image_file_name.jpg">
																								<span data-dz-name>some_image_file_name.jpg</span> <strong>(<span  data-dz-size>340kb</span>)</strong>
																							</div>
																							<div class="dropzone-error" data-dz-errormessage></div>
																						</div>
																						<div class="dropzone-progress">
																							<div class="progress">
																								<div class="progress-bar kt-bg-brand" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress></div>
																							</div>
																						</div>
																						<div class="dropzone-toolbar">
																							<span class="dropzone-delete" data-dz-remove><i class="flaticon2-cross"></i></span>
																						</div>
																					</div>
																				</div>
																			</div>
																			<span class="form-text text-muted">Max file size is 1MB and max number of files is 1.</br>Only JPG, PNG and PDF File Accepted.</span>
																		</div>
																	</div>
																</div>

															</div>
															<div class="row">
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>Pan Card Number</label>
																		<input type="tel" class="form-control" id="pan_number" name="pan_number" placeholder="phone">
																		<span class="form-text text-muted">Please enter your Pan card number.</span>
																	</div>
																</div>																
																<div class="col-xl-6">
																	<div class="form-group form-group-last row">
																		<label class="col-lg-3 col-form-label">Upload:</label>
																		<div class="col-lg-9">
																			<div class="dropzone dropzone-multi" id="pan_card_dropzone">
																				<div class="dropzone-panel">
																					<a class="dropzone-select btn btn-label-brand btn-bold btn-sm">Attach files</a>
																				</div>
																				<div class="dropzone-items">
																					<div class="dropzone-item" style="display:none">
																						<div class="dropzone-file">
																							<div class="dropzone-filename" title="some_image_file_name.jpg">
																								<span data-dz-name>some_image_file_name.jpg</span> <strong>(<span  data-dz-size>340kb</span>)</strong>
																							</div>
																							<div class="dropzone-error" data-dz-errormessage></div>
																						</div>
																						<div class="dropzone-progress">
																							<div class="progress">
																								<div class="progress-bar kt-bg-brand" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress></div>
																							</div>
																						</div>
																						<div class="dropzone-toolbar">
																							<span class="dropzone-delete" data-dz-remove><i class="flaticon2-cross"></i></span>
																						</div>
																					</div>
																				</div>
																			</div>
																			<span class="form-text text-muted">Max file size is 1MB and max number of files is 1.</br>Only JPG, PNG and PDF File Accepted.</span>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!--end: Form Wizard Step 2-->
												
												<!--begin: Form Wizard Step 3-->
												<div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
													<div class="kt-heading kt-heading--md">Setup Your Resident Address</div>
													<div class="kt-form__section kt-form__section--first">
														<div class="kt-wizard-v2__form">
															<div class="row">
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>Address Line 1</label>
																		<input type="text" class="form-control" name="resident_address_1">
																		<span class="form-text text-muted">Please enter your Address.</span>
																	</div>
																</div>
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>Address Line 2</label>
																		<input type="text" class="form-control" name="resident_address_2">
																		<span class="form-text text-muted">Please enter your Address.</span>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>Postcode</label>
																		<input type="text" class="form-control" name="resident_postcode">
																		<span class="form-text text-muted">Please enter your Postcode.</span>
																	</div>
																</div>
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>State</label>
																		<select name="resident_state" class="form-control">
																			<option value="">Select</option>
																			<option value="Maharashtra">Maharashtra</option>
																		</select>
																		<span class="form-text text-muted">Please enter your State.</span>							
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>City</label>
																		<input type="text" class="form-control" name="resident_city">
																		<span class="form-text text-muted">Please enter your City.</span>
																	</div>
																</div>
																<div class="col-xl-6">
																	<div class="form-group" style="padding-top: 40px;">
																		<label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand mark_address_permanent">
																			<input type="checkbox" name="address_permanent" id="address_permanent"> Mark This Address As permanent Address
																			<span></span>
																		</label>
																		<label style="display: none;">Country:</label>
																		<select name="resident_country" class="form-control"  style="display: none;">
																			<option value="">Select</option>
																			<option value="India" selected>India</option>
																		</select>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!--end: Form Wizard Step 3-->

												<!--begin: Form Wizard Step 4-->
												<div class="kt-wizard-v2__content actual_address_main" data-ktwizard-type="step-content">
													<div class="kt-heading kt-heading--md">Setup Your Actual Address</div>
													<div class="kt-form__section kt-form__section--first">
														<div class="kt-wizard-v2__form">
															<div class="row">
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>Address Line 1</label>
																		<input type="text" class="form-control" name="actual_address_1">
																		<span class="form-text text-muted">Please enter your Address.</span>
																	</div>
																</div>
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>Address Line 2</label>
																		<input type="text" class="form-control" name="actual_address_2">
																		<span class="form-text text-muted">Please enter your Address.</span>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>Postcode</label>
																		<input type="text" class="form-control" name="actual_postcode">
																		<span class="form-text text-muted">Please enter your Postcode.</span>
																	</div>
																</div>
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>State</label>
																		<select name="actual_state" class="form-control">
																			<option value="">Select</option>
																			<option value="Maharashtra">Maharashtra</option>
																		</select>
																		<span class="form-text text-muted">Please enter your State.</span>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>City</label>
																		<input type="text" class="form-control" name="actual_city">
																		<span class="form-text text-muted">Please enter your City.</span>
																	</div>
																</div>
																<div class="col-xl-6" style="display: none;">
																	<div class="form-group">
																		<label>Country:</label>
																		<select name="actual_country" class="form-control">
																			<option value="">Select</option>
																			<option value="India">India</option>
																		</select>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!--end: Form Wizard Step 4-->

												<!--begin: Form Wizard Step 5-->
												<div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
													<div class="kt-heading kt-heading--md">Add your Official Details</div>
													<div class="kt-form__section kt-form__section--first">
														<div class="kt-wizard-v2__form">
															<div class="row">
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>Official Number</label>
																		<input type="tel" class="form-control" id="official_number" name="official_number">
																		<span class="form-text text-muted">Please enter your phone number.</span>
																	</div>
																</div>
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>Official Email</label>
																		<input type="email" class="form-control" id="official_email" name="official_email">
																		<span class="form-text text-muted">Please enter your email address.</span>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>Job Location:</label>
																		<select id="job_location" name="job_location" class="form-control">
																			<option value="">Select</option>
																			<?php foreach($job_location as $job_location_value) { ?>
																				<option value="<?php echo $job_location_value['name'];?>"><?php echo ucfirst($job_location_value['name']);?></option>
																			<?php }?>
																		</select>
																	</div>
																</div>
																<div class="col-xl-6">
																	<div class="form-group form-group-last row">
																		<label class="col-lg-3 col-form-label">Profie Photo:</label>
																		<div class="col-lg-9">
																			<div class="dropzone dropzone-multi" id="profile_pic_dropzone">
																				<div class="dropzone-panel">
																					<a class="dropzone-select btn btn-label-brand btn-bold btn-sm">Upload</a>
																				</div>
																				<div class="dropzone-items">
																					<div class="dropzone-item" style="display:none">
																						<div class="dropzone-file">
																							<div class="dropzone-filename" title="some_image_file_name.jpg">
																								<span data-dz-name>some_image_file_name.jpg</span> <strong>(<span  data-dz-size>340kb</span>)</strong>
																							</div>
																							<div class="dropzone-error" data-dz-errormessage></div>
																						</div>
																						<div class="dropzone-progress">
																							<div class="progress">
																								<div class="progress-bar kt-bg-brand" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress></div>
																							</div>
																						</div>
																						<div class="dropzone-toolbar">
																							<span class="dropzone-delete" data-dz-remove><i class="flaticon2-cross"></i></span>
																						</div>
																					</div>
																				</div>
																			</div>
																			<span class="form-text text-muted">Max file size is 1MB and max number of files is 1.</br>Only JPG, AND PNG File Accepted.</span>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>Department:</label>
																		<select id="department" name="department" class="form-control">
																			<option value="">Select</option>
																			<?php foreach($department as $department_value) { ?>
																				<option value="<?php echo $department_value['role_name'];?>"><?php echo ucfirst($department_value['role_name']);?></option>
																			<?php }?>
																		</select>
																	</div>
																</div>
																<div class="col-xl-6" style="display: none;">
																	<div class="form-group">
																		<?php if(!empty($designation)) { ?>
																		<label>Designation:</label>
																		<select id="designation" name="designation" class="form-control">
																			<option value="">Select</option>
																			<?php foreach($designation as $designation_value) { ?>
																				<option value="<?php echo $designation_value['name'];?>"><?php echo ucfirst($designation_value['name']);?></option>
																			<?php } ?>
																		</select>
																		<?php }?>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!--end: Form Wizard Step 5-->

												<!--begin: Form Wizard Step 6-->
												<div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
													<div class="kt-heading kt-heading--md">Enter your Payment Details</div>
													<div class="kt-form__section kt-form__section--first">
														<div class="kt-wizard-v2__form">
															<div class="row">
																<div class="col-xl-4">
																	<div class="form-group">
																		<label>Bank Name</label>
																		<input type="text" class="form-control" name="bank_name" id="bank_name">
																		<span class="form-text text-muted">Please enter your Bank Name.</span>
																	</div>
																</div>
																<div class="col-xl-4">
																	<div class="form-group">
																		<label>Branch Name</label>
																		<input type="text" class="form-control" name="branch_name" id="branch_name">
																		<span class="form-text text-muted">Please enter your Branch Name.</span>
																	</div>
																</div>
																<div class="col-xl-4">
																	<div class="form-group">
																		<label>Beneficiary Name</label>
																		<input type="text" class="form-control" name="beneficiary_name" id="beneficiary_name">
																		<span class="form-text text-muted">Please enter your Beneficiary Name.</span>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>Account Number</label>
																		<input type="text" class="form-control" name="account_number" id="account_number">
																		<span class="form-text text-muted">Please enter your Account Number.</span>
																	</div>
																</div>
																<div class="col-xl-6">
																	<div class="form-group">
																		<label>IFSC Code</label>
																		<input type="text" class="form-control" name="ifsc_code" id="ifsc_code">
																		<span class="form-text text-muted">Please enter your IFSC code.</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>

												<!--end: Form Wizard Step 6-->

												<!--begin: Form Wizard Step 7-->
												<div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
													<div class="kt-heading kt-heading--md">References Details</div>
													<div class="kt-form__section kt-form__section--first">
														<div class="kt-wizard-v2__form">
															<div>
																<table class="table">
																  	<thead>
																    	<tr>
																      		<th scope="col">#</th>
																      		<th scope="col">Reference Name</th>
																      		<th scope="col">Relation</th>
																      		<th scope="col">Address</th>
																	      	<th scope="col">Number</th>
																	      	<th scope="col">Alternate Number</th>
																	      	<th scope="col">Email</th>
																    	</tr>
																  	</thead>
																  	<tbody>
																    	<tr>
																      		<th scope="row">1</th>
																      		<td><input class="form-control" type="text" name="reference_name_1"></td>
																      		<td><input class="form-control" type="text" name="relation_1"></td>
																      		<td><input class="form-control" type="text" name="address_1"></td>
																      		<td><input class="form-control" type="text" name="number_1"></td>
																      		<td><input class="form-control" type="text" name="alternate_number_1"></td>
																      		<td><input class="form-control" type="text" name="email_1"></td>
																    	</tr>
																    	<tr>
																      		<th scope="row">2</th>
																      		<td><input class="form-control" type="text" name="reference_name_2"></td>
																      		<td><input class="form-control" type="text" name="relation_2"></td>
																      		<td><input class="form-control" type="text" name="address_2"></td>
																      		<td><input class="form-control" type="text" name="number_2"></td>
																      		<td><input class="form-control" type="text" name="alternate_number_2"></td>
																      		<td><input class="form-control" type="text" name="email_2"></td>
																    	</tr>
																  	</tbody>
																</table>
															</div>
														</div>
													</div>
												</div>

												<!--end: Form Wizard Step 7-->

												<!--begin: Form Wizard Step 7-->
												<div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
													<div class="kt-heading kt-heading--md">Review your Details and Submit</div>
													<div class="kt-form__section kt-form__section--first">
														<div class="kt-wizard-v2__review" id="employee_review_page">
														</div>
													</div>
												</div>

												<!--end: Form Wizard Step 7-->

												<!--begin: Form Actions -->
												<div class="kt-form__actions">
													<button class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-prev">
														Previous
													</button>
													<button class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-submit">
														Submit
													</button>
													<button class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u next_button" data-ktwizard-type="action-next">
														Next Step
													</button>
												</div>

												<!--end: Form Actions -->
											</form>

											<!--end: Form Wizard Form-->
										</div>
									</div>
								</div>
							</div>
						</div>

						<!-- end:: Content -->
					</div>

					<!-- begin:: Footer -->
					<div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
						<div class="kt-container  kt-container--fluid ">
							<div class="kt-footer__copyright">
								2020&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Keenthemes</a>
							</div>
							<div class="kt-footer__menu">
								<a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">About</a>
								<a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Team</a>
								<a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Contact</a>
							</div>
						</div>
					</div>

					<!-- end:: Footer -->
				</div>
			</div>
		</div>

		<!-- end:: Page -->

		<!-- begin::Scrolltop -->
		<div id="kt_scrolltop" class="kt-scrolltop">
			<i class="fa fa-arrow-up"></i>
		</div>

		<!-- end::Scrolltop -->

		