<style type="text/css">
    .kt-aside--fixed .kt-wrapper {
        padding-left: 130px !important;
    }
    .kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper {
        padding-top: 30px !important;
    }
</style>
<!-- end:: Header Mobile -->
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <!-- begin:: Content -->
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">Add Salary Slip</h3>
                            </div>
                        </div>

                        <!--begin::Form-->
                        <form class="kt-form kt-form--label-right" id="kt_form_1">
                            <div class="kt-portlet__body">
                                <div class="kt-form__content">
                                    <div class="kt-alert m-alert--icon alert alert-danger kt-hidden" role="alert" id="kt_form_1_msg">
                                        <div class="kt-alert__icon">
                                            <i class="la la-warning"></i>
                                        </div>
                                        <div class="kt-alert__text">
                                            Oh snap! Change a few things up and try submitting again.
                                        </div>
                                        <div class="kt-alert__close">
                                            <button type="button" class="close" data-close="alert" aria-label="Close">
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
                                <div class="form-group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Employee Name *</label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <select class="form-control kt-bootstrap-select" id="employee_name" name="select">
                                            <option value="">Select Employee</option>
                                            <?php foreach($employee_list as $employee_details) {?>
                                                <option value="<?php echo $employee_details['id'];?>"><?php echo trim(strtoupper($employee_details['first_name'])), ' ',trim(strtoupper($employee_details['last_name']))  ?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row salary_slip_year">
                                    <label class="col-form-label col-lg-3 col-sm-12">Year *</label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <select class="form-control kt-bootstrap-select" id="salary_slip_year">
                                            <option value="">Select Year</option>
                                            <option value="2021">2021</option>
                                            <option value="2020">2020</option>
                                            <option value="2019">2019</option>
                                            <option value="2018">2018</option>
                                            <option value="2017">2017</option>
                                            <option value="2016">2016</option>
                                            <option value="2015">2015</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row salary_slip_month">
                                    <label class="col-form-label col-lg-3 col-sm-12">Month *</label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <select class="form-control kt-bootstrap-select" id="salary_slip_month">
                                            <option value="">Select Month</option>
                                            <option value="jan">January</option>
                                            <option value="feb">February</option>
                                            <option value="mar">March</option>
                                            <option value="apr">April</option>
                                            <option value="may">May</option>
                                            <option value="june">June</option>
                                            <option value="july">July</option>
                                            <option value="aug">August</option>
                                            <option value="aug">August</option>
                                            <option value="sep">September</option>
                                            <option value="oct">October</option>
                                            <option value="dec">December</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row salary_slip_upload" style="display: none;">
                                    <label class="col-form-label col-lg-3 col-sm-12">Upload Salary Slip*</label>
                                    <div class="col-lg-9">
                                        <div class="dropzone dropzone-multi" id="salary_slip_upload">
                                            <div class="dropzone-panel">
                                                <a class="dropzone-select btn btn-label-brand btn-bold btn-sm">Attach files</a>
                                            </div>
                                            <div class="dropzone-items">
                                                <div class="dropzone-item" style="display:none">
                                                    <div class="dropzone-file">
                                                        <div class="dropzone-filename" title="some_image_file_name.jpg">
                                                            <span data-dz-name>some_image_file_name.jpg</span> <strong>(<span  data-dz-size>340kb</span>)</strong>
                                                        </div>
                                                        <div class="dropzone-error" data-dz-errormessage></div>
                                                    </div>
                                                    <div class="dropzone-progress">
                                                        <div class="progress">
                                                            <div class="progress-bar kt-bg-brand" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress></div>
                                                        </div>
                                                    </div>
                                                    <div class="dropzone-toolbar">
                                                        <span class="dropzone-delete" data-dz-remove><i class="flaticon2-cross"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <span class="form-text text-muted">Max file size is 1MB and max number of files is 1.</br>Only JPG, PNG and PDF File Accepted.</span>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class=" ">
                                    <div class="row">
                                        <div class="col-lg-9 ml-lg-auto">
                                            <button type="submit" class="btn btn-success">Validate</button>
                                            <button type="reset" class="btn btn-secondary">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <!--end::Form-->
                    </div>

                    <!--end::Portlet-->
                </div>

                <!-- end:: Content -->
            </div>

            <!-- begin:: Footer -->
            <div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
                <div class="kt-container  kt-container--fluid ">
                    <div class="kt-footer__copyright">
                        2020&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Keenthemes</a>
                    </div>
                    <div class="kt-footer__menu">
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">About</a>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Team</a>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Contact</a>
                    </div>
                </div>
            </div>

            <!-- end:: Footer -->
        </div>
    </div>
</div>

<!-- end:: Page -->

<!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="fa fa-arrow-up"></i>
</div>

<!-- end::Scrolltop -->