<style type="text/css">
    .kt-aside--fixed .kt-wrapper {
        padding-left: 130px !important;
    }
    .kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper {
        padding-top: 30px !important;
    }
</style>
    
<!-- end:: Header Mobile -->
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

               <!-- begin:: Content -->
               <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

                            <!--begin::Portlet-->
                            <div class="kt-portlet">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            Salary slip update
                                        </h3>
                                    </div>
                                </div>

                                <!--begin::Form-->
                                
                                <form class="kt-form kt-form--label-right"
                                id="update_salary_slip" novalidate="novalidate">
                                
                                    <div class="kt-portlet__body">
                                        <div class="kt-form__content">
                                            <div class="kt-alert m-alert--icon alert alert-danger kt-hidden" role="alert" id="kt_form_1_msg">
                                                <div class="kt-alert__icon">
                                                    <i class="la la-warning"></i>
                                                </div>
                                                <div class="kt-alert__text">
                                                    Oh snap! Change a few things up and try submitting again.
                                                </div>
                                                <div class="kt-alert__close">
                                                    <button type="button" class="close" data-close="alert" aria-label="Close">
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
                                        
                                                <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">user names</label>
                                                 <select class="form-control col-lg-6 col-xl-3" id="people_id" name="people_id"><option value="">Select user</option>
                                                    <?php 
                                                       foreach($user_details as $single_user_details){
                                                            $selected= '';
                                                            if($single_user_details['user_id'] == $salary_slip_details[0]['people_id']){
                                                                $selected = 'selected = "selected"';
                                                            }
                                                echo '<option value="'.$single_user_details['user_id'].'" '.$selected.'>'.ucwords(strtolower($single_user_details['name'])).'</option>';
                                               }
                                                echo '</select>';
                                                ?>

                                               </div>

                                        <div class="form-group row">
                                               <label class="col-form-label col-lg-3 col-sm-12">Basic</label>
                                               <div class="col-lg-6 col-xl-3">
                                                   <input class="form-control" type="text" name="basic"  value="<?php if(isset($salary_slip_details)) echo $salary_slip_details[0]['basic']; ?>">
                                               </div>
                                        </div>
                                       
                                        <div class="form-group row">
                                            <label class="col-form-label col-lg-3 col-sm-12">Effect</label>
                                             <select class="form-control col-lg-6 col-xl-3" id="basic_income_or_expense" name="basic_income_or_expense">
                                                 <?php
                                                if(isset($salary_slip_details)) ?><option value="<?php echo( $salary_slip_details[0]["basic_income_or_expense"])?>"><?php echo($salary_slip_details[0]["basic_income_or_expense"]);?>
                                                    
                                                </option>
                                            <option value="expense">expense</option>
                                             <option value="income">income</option>
                                            </select>  
                                        </div>
                                        <div class="form-group row">
                                               <label class="col-form-label col-lg-3 col-sm-12">Hra</label>
                                               <div class="col-lg-6 col-xl-3">
                                                   <input class="form-control" type="text" name="hra"  value="<?php if(isset($salary_slip_details)) echo $salary_slip_details[0]['hra']; ?>">
                                               </div>
                                        </div>
                                        <div class="form-group row">
                                               <label class="col-form-label col-lg-3 col-sm-12">Effect</label>
                                            <select class="form-control col-lg-6 col-xl-3" id="hra_income_or_expense" name="hra_income_or_expense">
                                                  <?php
                                                if(isset($salary_slip_details)) ?><option value="<?php echo( $salary_slip_details[0]["hra_income_or_expense"])?>"><?php echo($salary_slip_details[0]["hra_income_or_expense"]);?></option>
                                            <option value="expense">expense</option>
                                             <option value="income">income</option>
                                            </select>
                                        </div>
                                        <div class="form-group row">
                                               <label class="col-form-label col-lg-3 col-sm-12">Conveyance</label>
                                               <div class="col-lg-6 col-xl-3">
                                                   <input class="form-control" type="text" name="conveyance"  value="<?php if(isset($salary_slip_details)) echo $salary_slip_details[0]['conveyance']; ?>">
                                               </div>
                                        </div>
                                        <div class="form-group row">
                                               <label class="col-form-label col-lg-3 col-sm-12">Effect</label>
                                            <select class="form-control col-lg-6 col-xl-3" id="conveyance_income_or_expense" name="conveyance_income_or_expense">
                                                  <?php
                                                if(isset($salary_slip_details)) ?><option value="<?php echo( $salary_slip_details[0]["conveyance_income_or_expense"])?>"><?php echo($salary_slip_details[0]["conveyance_income_or_expense"]);?></option>
                                            <option value="expense">expense</option>
                                             <option value="income">income</option>
                                            </select>
                                        </div>
                                        <div class="form-group row">
                                               <label class="col-form-label col-lg-3 col-sm-12">Other allowance</label>
                                               <div class="col-lg-6 col-xl-3">
                                                   <input class="form-control" type="text" name="other_allowance"  value="<?php if(isset($salary_slip_details)) echo $salary_slip_details[0]['other_allowance']; ?>">
                                               </div>
                                        </div>
                                        <div class="form-group row">
                                               <label class="col-form-label col-lg-3 col-sm-12">Effect</label>
                                            <select class="form-control col-lg-6 col-xl-3" id="other_allowance_income_or_expense" name="other_allowance_income_or_expense">
                                                  <?php
                                                if(isset($salary_slip_details)) ?><option value="<?php echo( $salary_slip_details[0]["other_allowance_income_or_expense"])?>"><?php echo($salary_slip_details[0]["other_allowance_income_or_expense"]);?></option>
                                            <option value="expense">expense</option>
                                             <option value="income">income</option>
                                            </select>
                                        </div>
                                        <div class="form-group row">
                                               <label class="col-form-label col-lg-3 col-sm-12">Pt</label>
                                               <div class="col-lg-6 col-xl-3">
                                                   <input class="form-control" type="text" name="pt"  value="<?php if(isset($salary_slip_details)) echo $salary_slip_details[0]['pt']; ?>">
                                               </div>
                                        </div>
                                        <div class="form-group row">
                                               <label class="col-form-label col-lg-3 col-sm-12">Effect</label>
                                            <select class="form-control col-lg-6 col-xl-3" id="pt_income_or_expense" name="pt_income_or_expense">
                                                 <?php
                                                if(isset($salary_slip_details)) ?><option value="<?php echo( $salary_slip_details[0]["pt_income_or_expense"])?>"><?php echo($salary_slip_details[0]["pt_income_or_expense"]);?></option>
                                            <option value="expense">expense</option>
                                             <option value="income">income</option>
                                            </select>
                                        </div>
                                        <div class="form-group row">
                                               <label class="col-form-label col-lg-3 col-sm-12">Loan</label>
                                               <div class="col-lg-6 col-xl-3">
                                                   <input class="form-control" type="text" name="loan"  value="<?php if(isset($salary_slip_details)) echo $salary_slip_details[0]['loan']; ?>">
                                               </div>
                                        </div>
                                        <div class="form-group row">
                                               <label class="col-form-label col-lg-3 col-sm-12">Effect</label>
                                            <select class="form-control col-lg-6 col-xl-3" id="loan_income_or_expense" name="loan_income_or_expense">
                                                 <?php
                                                if(isset($salary_slip_details)) ?><option value="<?php echo( $salary_slip_details[0]["loan_income_or_expense"])?>"><?php echo($salary_slip_details[0]["loan_income_or_expense"]);?></option>
                                            <option value="expense">expense</option>
                                             <option value="income">income</option>
                                            </select>
                                        </div>
                                        <div class="form-group row">
                                               <label class="col-form-label col-lg-3 col-sm-12">Advance</label>
                                               <div class="col-lg-6 col-xl-3">
                                                   <input class="form-control" type="text" name="advance"  value="<?php if(isset($salary_slip_details)) echo $salary_slip_details[0]['advance']; ?>">
                                               </div>
                                        </div>
                                        <div class="form-group row">
                                               <label class="col-form-label col-lg-3 col-sm-12">Effect</label>
                                            <select class="form-control col-lg-6 col-xl-3" id="advance_income_or_expense" name="advance_income_or_expense">
                                                 <?php
                                                if(isset($salary_slip_details)) ?><option value="<?php echo( $salary_slip_details[0]["advance_income_or_expense"])?>"><?php echo($salary_slip_details[0]["advance_income_or_expense"]);?></option>
                                            <option value="expense">expense</option>
                                             <option value="income">income</option>
                                            </select>
                                        </div>
                                        <div class="form-group row">
                                               <label class="col-form-label col-lg-3 col-sm-12">Other</label>
                                               <div class="col-lg-6 col-xl-3">
                                                   <input class="form-control" type="text" name="other"  value="<?php if(isset($salary_slip_details)) echo $salary_slip_details[0]['other']; ?>">
                                               </div>
                                        </div>
                                        <div class="form-group row">
                                               <label class="col-form-label col-lg-3 col-sm-12">Effect</label>
                                            <select class="form-control col-lg-6 col-xl-3" id="other_income_or_expense" name="other_income_or_expense">
                                                  <?php
                                                if(isset($salary_slip_details)) ?><option value="<?php echo( $salary_slip_details[0]["other_income_or_expense"])?>"><?php echo($salary_slip_details[0]["other_income_or_expense"]);?></option>
                                            <option value="expense">expense</option>
                                             <option value="income">income</option>
                                            </select>
                                        </div>
                                        
                                       <div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
                                    </div>
                                    <div class="kt-portlet__foot">
                                        <div class=" ">
                                            <div class="row">
                                                <div class="col-lg-9 ml-lg-auto">
                                                    <button type="reset" class="btn btn-success update_salary_slip">Submit</button>
                                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                                </div>
                                            </div>
                                        </div>    
                                    </div>
                                    
                                </form>
                               
                                
                                <!--end::Form-->
                            </div>

                            <!--end::Portlet-->
                        </div>

               <!-- end:: Content -->
            </div>

            <!-- begin:: Footer -->
            <div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
                <div class="kt-container  kt-container--fluid ">
                    <div class="kt-footer__copyright">
                        2020&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Keenthemes</a>
                    </div>
                    <div class="kt-footer__menu">
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">About</a>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Team</a>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Contact</a>
                    </div>
                </div>
            </div>

            <!-- end:: Footer -->
        </div>
    </div>
</div>

<!-- end:: Page -->











                                           

                                           
