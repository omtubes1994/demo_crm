<style type="text/css">
    .kt-aside--fixed .kt-wrapper {
        padding-left: 130px !important;
    }
    .kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper {
        padding-top: 60px !important;
    }
</style>
<!-- end:: Header Mobile -->
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <!-- begin:: Content Head -->
                <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                    <div class="kt-container  kt-container--fluid ">
                        <div class="kt-subheader__main">
                            <h3 class="kt-subheader__title">Employee Listing</h3>
                            <span class="kt-subheader__separator kt-hidden"></span>
                            <div class="kt-subheader__breadcrumbs">
                                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                                <span class="kt-subheader__breadcrumbs-separator"></span>
                                <a href="" class="kt-subheader__breadcrumbs-link">Hr</a>
                                <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Employee Listing</span>
                            </div>
                        </div>
                        <div class="kt-subheader__toolbar" style="display: none;">
                            <div class="kt-subheader__wrapper">
                                <a href="javascript:void(0)" class="btn kt-subheader__btn-daterange" id="kt_dashboard_daterangepicker" data-toggle="kt-tooltip" title="Select dashboard daterange" data-placement="left">
                                    <span class="kt-subheader__btn-daterange-title" id="kt_dashboard_daterangepicker_title">Today</span>&nbsp;
                                    <span class="kt-subheader__btn-daterange-date" id="kt_dashboard_daterangepicker_date">Aug 16</span>
                                    <i class="flaticon2-calendar-1"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- end:: Content Head -->

                <!-- begin:: Content -->
                        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

                            <div class="row">
                                <div class="col-xl-6">
                                    <!--begin:: Portlet-->
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__body">
                                            <div class="kt-widget kt-widget--user-profile-3">
                                                <div class="kt-widget__top">
                                                    <div class="kt-widget__media kt-hidden-">
                                                        <img src="assets/media/users/100_1.jpg" alt="image">
                                                    </div>
                                                    <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
                                                        JM
                                                    </div>
                                                    <div class="kt-widget__content">
                                                        <div class="kt-widget__head">
                                                            <a href="#" class="kt-widget__username">
                                                                Jason Muller
                                                                <i class="flaticon2-correct kt-font-success"></i>
                                                            </a>
                                                            <div class="kt-widget__action">
                                                                <!-- <button type="button" class="btn btn-label-success btn-sm btn-upper">ask</button>&nbsp; -->
                                                                <button type="button" class="btn btn-brand btn-sm btn-upper">Detail</button>
                                                            </div>
                                                        </div>
                                                        <div class="kt-widget__subhead">
                                                            <a href="#"><i class="flaticon2-new-email"></i>jason@siastudio.com</a>
                                                            <a href="#"><i class="flaticon2-calendar-3"></i>PR Manager </a>
                                                            <a href="#"><i class="flaticon2-placeholder"></i>Melbourne</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!--end:: Portlet-->
                                </div>
                                <div class="col-xl-6">
                                    
                                    <!--begin:: Portlet-->
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__body">
                                            <div class="kt-widget kt-widget--user-profile-3">
                                                <div class="kt-widget__top">
                                                    <div class="kt-widget__media kt-hidden-">
                                                        <img src="assets/media/users/100_1.jpg" alt="image">
                                                    </div>
                                                    <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
                                                        JM
                                                    </div>
                                                    <div class="kt-widget__content">
                                                        <div class="kt-widget__head">
                                                            <a href="#" class="kt-widget__username">
                                                                Jason Muller
                                                                <i class="flaticon2-correct kt-font-success"></i>
                                                            </a>
                                                            <div class="kt-widget__action">
                                                                <!-- <button type="button" class="btn btn-label-success btn-sm btn-upper">ask</button>&nbsp; -->
                                                                <button type="button" class="btn btn-brand btn-sm btn-upper">Detail</button>
                                                            </div>
                                                        </div>
                                                        <div class="kt-widget__subhead">
                                                            <a href="#"><i class="flaticon2-new-email"></i>jason@siastudio.com</a>
                                                            <a href="#"><i class="flaticon2-calendar-3"></i>PR Manager </a>
                                                            <a href="#"><i class="flaticon2-placeholder"></i>Melbourne</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!--end:: Portlet-->
                                </div>
                            </div>        

                            <!--Begin::Pagination-->
                            <div class="row">
                                <div class="col-xl-12">

                                    <!--begin:: Components/Pagination/Default-->
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__body">

                                            <!--begin: Pagination-->
                                            <div class="kt-pagination kt-pagination--brand">
                                                <ul class="kt-pagination__links">
                                                    <li class="kt-pagination__link--first">
                                                        <a href="#"><i class="fa fa-angle-double-left kt-font-brand"></i></a>
                                                    </li>
                                                    <li class="kt-pagination__link--next">
                                                        <a href="#"><i class="fa fa-angle-left kt-font-brand"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="#">...</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">29</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">30</a>
                                                    </li>
                                                    <li class="kt-pagination__link--active">
                                                        <a href="#">31</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">32</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">33</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">34</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">...</a>
                                                    </li>
                                                    <li class="kt-pagination__link--prev">
                                                        <a href="#"><i class="fa fa-angle-right kt-font-brand"></i></a>
                                                    </li>
                                                    <li class="kt-pagination__link--last">
                                                        <a href="#"><i class="fa fa-angle-double-right kt-font-brand"></i></a>
                                                    </li>
                                                </ul>
                                                <div class="kt-pagination__toolbar">
                                                    <select class="form-control kt-font-brand" style="width: 60px">
                                                        <option value="10">10</option>
                                                        <option value="20">20</option>
                                                        <option value="30">30</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                    <span class="pagination__desc">
                                                        Displaying 10 of 230 records
                                                    </span>
                                                </div>
                                            </div>

                                            <!--end: Pagination-->
                                        </div>
                                    </div>

                                    <!--end:: Components/Pagination/Default-->
                                </div>
                            </div>

                            <!--End::Pagination-->
                        </div>

                        <!-- end:: Content -->
            </div>

            <!-- begin:: Footer -->
            <div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
                <div class="kt-container  kt-container--fluid ">
                    <div class="kt-footer__copyright">
                        2020&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Keenthemes</a>
                    </div>
                    <div class="kt-footer__menu">
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">About</a>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Team</a>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Contact</a>
                    </div>
                </div>
            </div>

            <!-- end:: Footer -->
        </div>
    </div>
</div>

<!-- end:: Page -->

<!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="fa fa-arrow-up"></i>
</div>

<!-- end::Scrolltop -->