<style type="text/css">
    .kt-aside--fixed .kt-wrapper {
        padding-left: 130px !important;
    }
    .kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper {
        padding-top: 30px !important;
    }
    .salary_slip thead tr th{
        background-color: transparent;
        padding: 1rem 0 0.5rem 0;
        color: #74788d;
        border-top: 0;
        border-bottom: 1px solid #ebedf2;
    }
    .salary_slip tbody tr td{
        background-color: transparent;
        padding: 1rem 0 1rem 0;
        border-top: none;
        font-weight: 700;
        font-size: 1.1rem;
        color: #595d6e;
        padding-top: 1.8rem;
    }
    .grand_total thead tr th{
        font-size: 1.1rem;
        text-transform: capitalize;
        font-weight: 500;
        color: #74788d;
        border-top: 0;
        border-bottom: 1px solid #ebedf2;
        padding: 10px 10px 10px 0;
        background-color: transparent;
    }
    .grand_total tbody tr td{

        font-size: 1.1rem;
        text-transform: capitalize;
        background-color: transparent;
        font-weight: 500;
        color: #595d6e;
        padding: 10px 10px 10px 0;
    }
</style>
<!-- end:: Header Mobile -->
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

               <!-- begin:: Content -->
                        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

                            <!--Begin::App-->
                            <div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">

                                <!--Begin:: App Aside Mobile Toggle-->
                                <button class="kt-app__aside-close" id="kt_user_profile_aside_close">
                                    <i class="la la-close"></i>
                                </button>

                                <!--End:: App Aside Mobile Toggle-->

                                <!--Begin:: App Aside-->
                                <div class="kt-grid__item kt-app__toggle kt-app__aside" id="kt_user_profile_aside">

                                    <!--begin:: Widgets/Applications/User/Profile1-->
                                    <div class="kt-portlet " style="height: 857px;">
                                        <div class="kt-portlet__head  kt-portlet__head--noborder">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                </h3>
                                            </div>
                                            <div class="kt-portlet__head-toolbar">
                                                <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                                                    <i class="flaticon-more-1"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-fit dropdown-menu-md">

                                                    <!--begin::Nav-->
                                                    <ul class="kt-nav">
                                                        <li class="kt-nav__head">
                                                            Export Options
                                                            <span data-toggle="kt-tooltip" data-placement="right" title="Click to learn more...">
                                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand kt-svg-icon--md1">
                                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                        <rect x="0" y="0" width="24" height="24" />
                                                                        <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                                                                        <rect fill="#000000" x="11" y="10" width="2" height="7" rx="1" />
                                                                        <rect fill="#000000" x="11" y="7" width="2" height="2" rx="1" />
                                                                    </g>
                                                                </svg> </span>
                                                        </li>
                                                        <li class="kt-nav__separator"></li>
                                                        <li class="kt-nav__item">
                                                            <a href="#" class="kt-nav__link">
                                                                <i class="kt-nav__link-icon flaticon2-drop"></i>
                                                                <span class="kt-nav__link-text">Activity</span>
                                                            </a>
                                                        </li>
                                                        <li class="kt-nav__item">
                                                            <a href="#" class="kt-nav__link">
                                                                <i class="kt-nav__link-icon flaticon2-calendar-8"></i>
                                                                <span class="kt-nav__link-text">FAQ</span>
                                                            </a>
                                                        </li>
                                                        <li class="kt-nav__item">
                                                            <a href="#" class="kt-nav__link">
                                                                <i class="kt-nav__link-icon flaticon2-telegram-logo"></i>
                                                                <span class="kt-nav__link-text">Settings</span>
                                                            </a>
                                                        </li>
                                                        <li class="kt-nav__item">
                                                            <a href="#" class="kt-nav__link">
                                                                <i class="kt-nav__link-icon flaticon2-new-email"></i>
                                                                <span class="kt-nav__link-text">Support</span>
                                                                <span class="kt-nav__link-badge">
                                                                    <span class="kt-badge kt-badge--success kt-badge--rounded">5</span>
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <li class="kt-nav__separator"></li>
                                                        <li class="kt-nav__foot">
                                                            <a class="btn btn-label-danger btn-bold btn-sm" href="#">Upgrade plan</a>
                                                            <a class="btn btn-clean btn-bold btn-sm" href="#" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more...">Learn more</a>
                                                        </li>
                                                    </ul>

                                                    <!--end::Nav-->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body kt-portlet__body--fit-y">

                                            <!--begin::Widget -->
                                            <div class="kt-widget kt-widget--user-profile-1">
                                                <div class="kt-widget__head">
                                                    <div class="kt-widget__media">
                                                        <img src="assets/media/users/100_13.jpg" alt="image">
                                                    </div>
                                                    <div class="kt-widget__content">
                                                        <div class="kt-widget__section">
                                                            <a href="#" class="kt-widget__username">
                                                                Jason Muller
                                                                <i class="flaticon2-correct kt-font-success"></i>
                                                            </a>
                                                            <span class="kt-widget__subtitle">
                                                                Head of Development
                                                            </span>
                                                        </div>
                                                        <div class="kt-widget__action">
                                                            <button type="button" class="btn btn-info btn-sm">chat</button>&nbsp;
                                                            <button type="button" class="btn btn-success btn-sm">follow</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="kt-widget__body">
                                                    <div class="kt-widget__content">
                                                        <div class="kt-widget__info">
                                                            <span class="kt-widget__label">Email:</span>
                                                            <a href="#" class="kt-widget__data">matt@fifestudios.com</a>
                                                        </div>
                                                        <div class="kt-widget__info">
                                                            <span class="kt-widget__label">Phone:</span>
                                                            <a href="#" class="kt-widget__data">44(76)34254578</a>
                                                        </div>
                                                        <div class="kt-widget__info">
                                                            <span class="kt-widget__label">Location:</span>
                                                            <span class="kt-widget__data">Melbourne</span>
                                                        </div>
                                                    </div>
                                                    <div class="kt-widget__items">
                                                        <a href="<?php echo base_url('hr/edit_listing/account_setting');?>" class="kt-widget__item kt-widget__item--active">
                                                            <span class="kt-widget__section">
                                                                <span class="kt-widget__icon">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                            <polygon points="0 0 24 0 24 24 0 24" />
                                                                            <path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero" />
                                                                            <path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3" />
                                                                        </g>
                                                                    </svg> </span>
                                                                <span class="kt-widget__desc">
                                                                    Account Setting
                                                                </span>
                                                            </span>
                                                        </a>
                                                        <a href="<?php echo base_url('hr/edit_listing/personal_information');?>" class="kt-widget__item ">
                                                            <span class="kt-widget__section">
                                                                <span class="kt-widget__icon">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                            <polygon points="0 0 24 0 24 24 0 24" />
                                                                            <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                                            <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                                                                        </g>
                                                                    </svg> </span>
                                                                <span class="kt-widget__desc">
                                                                    Personal Information
                                                                </span>
                                                            </span>
                                                        </a>
                                                        <a href="<?php echo base_url('hr/edit_listing/address');?>" class="kt-widget__item ">
                                                            <span class="kt-widget__section">
                                                                <span class="kt-widget__icon">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24" height="24" />
                                                                            <path d="M2.56066017,10.6819805 L4.68198052,8.56066017 C5.26776695,7.97487373 6.21751442,7.97487373 6.80330086,8.56066017 L8.9246212,10.6819805 C9.51040764,11.267767 9.51040764,12.2175144 8.9246212,12.8033009 L6.80330086,14.9246212 C6.21751442,15.5104076 5.26776695,15.5104076 4.68198052,14.9246212 L2.56066017,12.8033009 C1.97487373,12.2175144 1.97487373,11.267767 2.56066017,10.6819805 Z M14.5606602,10.6819805 L16.6819805,8.56066017 C17.267767,7.97487373 18.2175144,7.97487373 18.8033009,8.56066017 L20.9246212,10.6819805 C21.5104076,11.267767 21.5104076,12.2175144 20.9246212,12.8033009 L18.8033009,14.9246212 C18.2175144,15.5104076 17.267767,15.5104076 16.6819805,14.9246212 L14.5606602,12.8033009 C13.9748737,12.2175144 13.9748737,11.267767 14.5606602,10.6819805 Z" fill="#000000" opacity="0.3" />
                                                                            <path d="M8.56066017,16.6819805 L10.6819805,14.5606602 C11.267767,13.9748737 12.2175144,13.9748737 12.8033009,14.5606602 L14.9246212,16.6819805 C15.5104076,17.267767 15.5104076,18.2175144 14.9246212,18.8033009 L12.8033009,20.9246212 C12.2175144,21.5104076 11.267767,21.5104076 10.6819805,20.9246212 L8.56066017,18.8033009 C7.97487373,18.2175144 7.97487373,17.267767 8.56066017,16.6819805 Z M8.56066017,4.68198052 L10.6819805,2.56066017 C11.267767,1.97487373 12.2175144,1.97487373 12.8033009,2.56066017 L14.9246212,4.68198052 C15.5104076,5.26776695 15.5104076,6.21751442 14.9246212,6.80330086 L12.8033009,8.9246212 C12.2175144,9.51040764 11.267767,9.51040764 10.6819805,8.9246212 L8.56066017,6.80330086 C7.97487373,6.21751442 7.97487373,5.26776695 8.56066017,4.68198052 Z" fill="#000000" />
                                                                        </g>
                                                                    </svg> </span>
                                                                <span class="kt-widget__desc">
                                                                    Resident Information
                                                                </span>
                                                                </spandiv>
                                                        </a>
                                                        <a href="<?php echo base_url('hr/edit_listing/official_information');?>" class="kt-widget__item ">
                                                            <span class="kt-widget__section">
                                                                <span class="kt-widget__icon">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24" height="24" />
                                                                            <path d="M4,4 L11.6314229,2.5691082 C11.8750185,2.52343403 12.1249815,2.52343403 12.3685771,2.5691082 L20,4 L20,13.2830094 C20,16.2173861 18.4883464,18.9447835 16,20.5 L12.5299989,22.6687507 C12.2057287,22.8714196 11.7942713,22.8714196 11.4700011,22.6687507 L8,20.5 C5.51165358,18.9447835 4,16.2173861 4,13.2830094 L4,4 Z" fill="#000000" opacity="0.3" />
                                                                            <path d="M12,11 C10.8954305,11 10,10.1045695 10,9 C10,7.8954305 10.8954305,7 12,7 C13.1045695,7 14,7.8954305 14,9 C14,10.1045695 13.1045695,11 12,11 Z" fill="#000000" opacity="0.3" />
                                                                            <path d="M7.00036205,16.4995035 C7.21569918,13.5165724 9.36772908,12 11.9907452,12 C14.6506758,12 16.8360465,13.4332455 16.9988413,16.5 C17.0053266,16.6221713 16.9988413,17 16.5815,17 C14.5228466,17 11.463736,17 7.4041679,17 C7.26484009,17 6.98863236,16.6619875 7.00036205,16.4995035 Z" fill="#000000" opacity="0.3" />
                                                                        </g>
                                                                    </svg> </span>
                                                                <span class="kt-widget__desc">
                                                                    Employee Details
                                                                </span>
                                                            </span>
                                                            <span class="kt-badge kt-badge--unified-danger kt-badge--sm kt-badge--rounded kt-badge--bolder">5</span>
                                                        </a>
                                                        <a href="<?php echo base_url('hr/edit_listing/bank_details');?>" class="kt-widget__item ">
                                                            <span class="kt-widget__section">
                                                                <span class="kt-widget__icon">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24" height="24" />
                                                                            <path d="M6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,12 C19,12.5522847 18.5522847,13 18,13 L6,13 C5.44771525,13 5,12.5522847 5,12 L5,3 C5,2.44771525 5.44771525,2 6,2 Z M7.5,5 C7.22385763,5 7,5.22385763 7,5.5 C7,5.77614237 7.22385763,6 7.5,6 L13.5,6 C13.7761424,6 14,5.77614237 14,5.5 C14,5.22385763 13.7761424,5 13.5,5 L7.5,5 Z M7.5,7 C7.22385763,7 7,7.22385763 7,7.5 C7,7.77614237 7.22385763,8 7.5,8 L10.5,8 C10.7761424,8 11,7.77614237 11,7.5 C11,7.22385763 10.7761424,7 10.5,7 L7.5,7 Z" fill="#000000" opacity="0.3" />
                                                                            <path d="M3.79274528,6.57253826 L12,12.5 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 Z" fill="#000000" />
                                                                        </g>
                                                                    </svg> </span>
                                                                <span class="kt-widget__desc">
                                                                    Bank Setting
                                                                </span>
                                                            </span>
                                                        </a>
                                                        <a href="<?php echo base_url('hr/edit_listing/reference');?>" class="kt-widget__item" data-toggle="kt-tooltip" title="Coming soon..." data-placement="right"> <span class="kt-widget__section">
                                                                <span class="kt-widget__icon">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24" height="24" />
                                                                            <rect fill="#000000" x="2" y="5" width="19" height="4" rx="1" />
                                                                            <rect fill="#000000" opacity="0.3" x="2" y="11" width="19" height="10" rx="1" />
                                                                        </g>
                                                                    </svg> </span>
                                                                <span class="kt-widget__desc">
                                                                    Reference
                                                                </span>
                                                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--end::Widget -->
                                        </div>
                                    </div>

                                    <!--end:: Widgets/Applications/User/Profile1-->
                                </div>

                                <!--End:: App Aside-->

                                <!--Begin:: App Content-->
                                <div class="kt-grid__item kt-grid__item--fluid kt-app__content">
                                    <!-- <div class="row"> -->
                                        <!-- <div class="col-xl-12"> -->
                                            <!-- <div class="kt-portlet"> -->
                                                <!-- <div class="kt-portlet__head">
                                                    <div class="kt-portlet__head-label">
                                                        <h3 class="kt-portlet__head-title">Leave Details <small>update your account details</small></h3>
                                                    </div>
                                                </div> -->
                                                <div class="row">
                                                    <div class="kt-portlet">
                                                        <div class="kt-portlet__head">
                                                            <div class="kt-portlet__head-label">
                                                                <span class="kt-portlet__head-icon kt-hidden">
                                                                    <i class="la la-gear"></i>
                                                                </span>
                                                                <h3 class="kt-portlet__head-title">
                                                                    Form Repeater Example
                                                                </h3>
                                                            </div>
                                                        </div>

                                                        <!--begin::Form-->
                                                        <form class="kt-form">
                                                            <div class="kt-portlet__body">
                                                                <div class="kt-form__section kt-form__section--first">
                                                                    <div class="form-group row">
                                                                        <div class="col-lg-4">
                                                                            <label>Basic:</label>
                                                                            <input type="email" class="form-control" placeholder="Enter Basic amount">
                                                                            <label>Income/Expense:</label>
                                                                            <div class="kt-radio-inline">
                                                                                <label class="kt-radio kt-radio--bold kt-radio--brand">
                                                                                    <input type="radio" name="radio6"> Income
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="kt-radio kt-radio--bold kt-radio--brand">
                                                                                    <input type="radio" name="radio6"> Expense
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-4">
                                                                            <label>Hra:</label>
                                                                            <input type="email" class="form-control" placeholder="Enter Hra amount">
                                                                            <label>Income/Expense:</label>
                                                                            <div class="kt-radio-inline">
                                                                                <label class="kt-radio kt-radio--bold kt-radio--brand">
                                                                                    <input type="radio" name="radio6"> Income
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="kt-radio kt-radio--bold kt-radio--brand">
                                                                                    <input type="radio" name="radio6"> Expense
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-4">
                                                                            <label>Conveyance:</label>
                                                                            <input type="email" class="form-control" placeholder="Enter Conveyance amount">
                                                                            <label>Income/Expense:</label>
                                                                            <div class="kt-radio-inline">
                                                                                <label class="kt-radio kt-radio--bold kt-radio--brand">
                                                                                    <input type="radio" name="radio6"> Income
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="kt-radio kt-radio--bold kt-radio--brand">
                                                                                    <input type="radio" name="radio6"> Expense
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </div>                  
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <div class="col-lg-4">
                                                                            <label>Other Allowance:</label>
                                                                            <input type="email" class="form-control" placeholder="Enter Other Allowance amount">
                                                                            <label>Income/Expense:</label>
                                                                            <div class="kt-radio-inline">
                                                                                <label class="kt-radio kt-radio--bold kt-radio--brand">
                                                                                    <input type="radio" name="radio6"> Income
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="kt-radio kt-radio--bold kt-radio--brand">
                                                                                    <input type="radio" name="radio6"> Expense
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-4">
                                                                            <label>Professional Tax:</label>
                                                                            <input type="email" class="form-control" placeholder="Enter Professional Tax amount">
                                                                            <label>Income/Expense:</label>
                                                                            <div class="kt-radio-inline">
                                                                                <label class="kt-radio kt-radio--bold kt-radio--brand">
                                                                                    <input type="radio" name="radio6"> Income
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="kt-radio kt-radio--bold kt-radio--brand">
                                                                                    <input type="radio" name="radio6"> Expense
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-4">
                                                                            <label>Loan:</label>
                                                                            <input type="email" class="form-control" placeholder="Enter Loan amount">
                                                                            <label>Income/Expense:</label>
                                                                            <div class="kt-radio-inline">
                                                                                <label class="kt-radio kt-radio--bold kt-radio--brand">
                                                                                    <input type="radio" name="radio6"> Income
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="kt-radio kt-radio--bold kt-radio--brand">
                                                                                    <input type="radio" name="radio6"> Expense
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </div>                  
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <div class="col-lg-4">
                                                                            <label>Advance:</label>
                                                                            <input type="email" class="form-control" placeholder="Enter Advance amount">
                                                                            <label>Income/Expense:</label>
                                                                            <div class="kt-radio-inline">
                                                                                <label class="kt-radio kt-radio--bold kt-radio--brand">
                                                                                    <input type="radio" name="radio6"> Income
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="kt-radio kt-radio--bold kt-radio--brand">
                                                                                    <input type="radio" name="radio6"> Expense
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-4">
                                                                            <label>Other:</label>
                                                                            <input type="email" class="form-control" placeholder="Enter Other amount">
                                                                            <label>Income/Expense:</label>
                                                                            <div class="kt-radio-inline">
                                                                                <label class="kt-radio kt-radio--bold kt-radio--brand">
                                                                                    <input type="radio" name="radio6"> Income
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="kt-radio kt-radio--bold kt-radio--brand">
                                                                                    <input type="radio" name="radio6"> Expense
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>
                                                                    <div id="kt_repeater_1">
                                                                        <div class="form-group form-group-last row" id="kt_repeater_1">
                                                                            <label class="col-lg-2 col-form-label">Contacts:</label>
                                                                            <div data-repeater-list="" class="col-lg-10">
                                                                                <div data-repeater-item class="form-group row align-items-center">
                                                                                    <div class="col-md-3" style="height:70px;">
                                                                                        <div class="kt-form__group--inline">
                                                                                            <div class="kt-form__label">
                                                                                                <label>Salary Slip Name:</label>
                                                                                            </div>
                                                                                            <div class="kt-form__control">
                                                                                                <input type="email" class="form-control" placeholder="Enter Salary Slip Name">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="d-md-none kt-margin-b-10"></div>
                                                                                    </div>
                                                                                    <div class="col-md-3" style="height:70px;">
                                                                                        <div class="kt-form__group--inline">
                                                                                            <div class="kt-form__label">
                                                                                                <label>Amount:</label>
                                                                                            </div>
                                                                                            <div class="kt-form__control">
                                                                                                <input type="email" class="form-control" placeholder="Enter Amount">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="d-md-none kt-margin-b-10"></div>
                                                                                    </div>
                                                                                    <div class="col-md-3" style="height:70px;">
                                                                                        <div class="kt-form__group--inline">
                                                                                            <div class="kt-form__label">
                                                                                                <label class="kt-label m-label--single">Income/Expense:</label>
                                                                                            </div>
                                                                                            <div class="kt-form__control">
                                                                                                <div class="kt-radio-inline">
                                                                                                    <label class="kt-radio kt-radio--bold kt-radio--brand">
                                                                                                        <input type="radio" name="radio6"> Income
                                                                                                        <span></span>
                                                                                                    </label>
                                                                                                    <label class="kt-radio kt-radio--bold kt-radio--brand">
                                                                                                        <input type="radio" name="radio6"> Expense
                                                                                                        <span></span>
                                                                                                    </label>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="d-md-none kt-margin-b-10"></div>
                                                                                    </div>
                                                                                    <div class="col-md-3" style="height:70px;">
                                                                                        <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                                                                            <i class="la la-trash-o"></i>
                                                                                            Delete
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group form-group-last row">
                                                                            <label class="col-lg-2 col-form-label"></label>
                                                                            <div class="col-lg-4">
                                                                                <a href="javascript:;" data-repeater-create="" class="btn btn-bold btn-sm btn-label-brand">
                                                                                    <i class="la la-plus"></i> Add
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="kt-portlet__foot">
                                                                <div class="kt-form__actions">
                                                                    <div class="row">
                                                                        <div class="col-lg-2"></div>
                                                                        <div class="col-lg-2">
                                                                            <button type="reset" class="btn btn-success">Submit</button>
                                                                            <button type="reset" class="btn btn-secondary">Cancel</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>

                                                        <!--end::Form-->
                                                    </div>

                                                </div>

                                                <div class="row" style="display:none;">
                                                    <div class="kt-portlet ">
                                                        <div class="kt-portlet__head">
                                                            <div class="kt-portlet__head-label">
                                                                <h3 class="kt-portlet__head-title">
                                                                    Salary Slip
                                                                </h3>
                                                            </div>
                                                        </div>
                                                        <div class="kt-portlet__body">

                                                            <!--begin::Accordion-->
                                                            <div class="accordion accordion-solid accordion-toggle-plus" id="accordionExample6">
                                                                <div class="card">
                                                                    <div class="card-header" id="headingOne6">
                                                                        <div class="card-title" data-toggle="collapse" data-target="#collapseOne6" aria-expanded="true" aria-controls="collapseOne6">
                                                                            Januaray
                                                                        </div>
                                                                    </div>
                                                                    <div id="collapseOne6" class="collapse show" aria-labelledby="headingOne6" data-parent="#accordionExample6" style="">
                                                                        <div class="card-body">
                                                                            <div class="row">   
                                                                                <div class="col-xl-6">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table salary_slip">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th>INCOME</th>
                                                                                                    <th>AMOUNT</th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>Creative Design</td>
                                                                                                    <td class="kt-font-danger kt-font-lg">$3200.00</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>Front-End Development</td>
                                                                                                    <td class="kt-font-danger kt-font-lg">$4800.00</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>Back-End Development</td>
                                                                                                    <td class="kt-font-danger kt-font-lg">$12600.00</td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xl-6">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table salary_slip">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th>EXPENSE</th>
                                                                                                    <th>AMOUNT</th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>Creative Design</td>
                                                                                                    <td class="kt-font-danger kt-font-lg">$3200.00</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>Front-End Development</td>
                                                                                                    <td class="kt-font-danger kt-font-lg">$4800.00</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>Back-End Development</td>
                                                                                                    <td class="kt-font-danger kt-font-lg">$12600.00</td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>    
                                                                            </div>
                                                                            <div class="row" style="padding: 30px 200px 0px 10px;">
                                                                                <div class="col-xl-12">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table grand_total">
                                                                                            <thead> 
                                                                                                <tr>
                                                                                                    <th>BANK</th>
                                                                                                    <th>ACC.NO.</th>
                                                                                                    <th>DUE DATE</th>
                                                                                                    <th>TOTAL AMOUNT</th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>BARCLAYS UK</td>
                                                                                                    <td>12345678909</td>
                                                                                                    <td>Jan 07, 2018</td>
                                                                                                    <td class="kt-font-danger kt-font-xl kt-font-boldest">20,600.00</td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="card">
                                                                    <div class="card-header" id="headingTwo6">
                                                                        <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseTwo6" aria-expanded="false" aria-controls="collapseTwo6">
                                                                            February
                                                                        </div>
                                                                    </div>
                                                                    <div id="collapseTwo6" class="collapse" aria-labelledby="headingTwo6" data-parent="#accordionExample6" style="">
                                                                        <div class="card-body">
                                                                            <div class="row">   
                                                                                <div class="col-xl-6">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table salary_slip">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th>INCOME</th>
                                                                                                    <th>AMOUNT</th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>Creative Design</td>
                                                                                                    <td class="kt-font-danger kt-font-lg">$3200.00</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>Front-End Development</td>
                                                                                                    <td class="kt-font-danger kt-font-lg">$4800.00</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>Back-End Development</td>
                                                                                                    <td class="kt-font-danger kt-font-lg">$12600.00</td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xl-6">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table salary_slip">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th>EXPENSE</th>
                                                                                                    <th>AMOUNT</th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>Creative Design</td>
                                                                                                    <td class="kt-font-danger kt-font-lg">$3200.00</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>Front-End Development</td>
                                                                                                    <td class="kt-font-danger kt-font-lg">$4800.00</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>Back-End Development</td>
                                                                                                    <td class="kt-font-danger kt-font-lg">$12600.00</td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>    
                                                                            </div>
                                                                            <div class="row" style="padding: 30px 200px 0px 10px;">
                                                                                <div class="col-xl-12">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table grand_total">
                                                                                            <thead> 
                                                                                                <tr>
                                                                                                    <th>BANK</th>
                                                                                                    <th>ACC.NO.</th>
                                                                                                    <th>DUE DATE</th>
                                                                                                    <th>TOTAL AMOUNT</th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>BARCLAYS UK</td>
                                                                                                    <td>12345678909</td>
                                                                                                    <td>Jan 07, 2018</td>
                                                                                                    <td class="kt-font-danger kt-font-xl kt-font-boldest">20,600.00</td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="card">
                                                                    <div class="card-header" id="headingThree6">
                                                                        <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseThree6" aria-expanded="false" aria-controls="collapseThree6">
                                                                            March
                                                                        </div>
                                                                    </div>
                                                                    <div id="collapseThree6" class="collapse" aria-labelledby="headingThree6" data-parent="#accordionExample6">
                                                                        <div class="card-body">
                                                                            <div class="row">   
                                                                                <div class="col-xl-6">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table salary_slip">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th>INCOME</th>
                                                                                                    <th>AMOUNT</th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>Creative Design</td>
                                                                                                    <td class="kt-font-danger kt-font-lg">$3200.00</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>Front-End Development</td>
                                                                                                    <td class="kt-font-danger kt-font-lg">$4800.00</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>Back-End Development</td>
                                                                                                    <td class="kt-font-danger kt-font-lg">$12600.00</td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xl-6">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table salary_slip">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th>EXPENSE</th>
                                                                                                    <th>AMOUNT</th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>Creative Design</td>
                                                                                                    <td class="kt-font-danger kt-font-lg">$3200.00</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>Front-End Development</td>
                                                                                                    <td class="kt-font-danger kt-font-lg">$4800.00</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>Back-End Development</td>
                                                                                                    <td class="kt-font-danger kt-font-lg">$12600.00</td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>    
                                                                            </div>
                                                                            <div class="row" style="padding: 30px 200px 0px 10px;">
                                                                                <div class="col-xl-12">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table grand_total">
                                                                                            <thead> 
                                                                                                <tr>
                                                                                                    <th>BANK</th>
                                                                                                    <th>ACC.NO.</th>
                                                                                                    <th>DUE DATE</th>
                                                                                                    <th>TOTAL AMOUNT</th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>BARCLAYS UK</td>
                                                                                                    <td>12345678909</td>
                                                                                                    <td>Jan 07, 2018</td>
                                                                                                    <td class="kt-font-danger kt-font-xl kt-font-boldest">20,600.00</td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <!--end::Accordion-->
                                                        </div>
                                                    </div>
                                                                </div>

                                                        <div class="row" style="display:none;">
                                                                    <div class="col-xl-4 col-lg-6 order-lg-1 order-xl-1">
                                                                        <div class="kt-portlet">
                                                                             <div class="kt-portlet__head">
                                                                <div class="kt-portlet__head-label">
                                                                    <h3 class="kt-portlet__head-title">Leave Details <small>update your account details</small></h3>
                                                                </div>
                                                            </div>
                                                            <!--begin:: Widgets/Blog-->
                                                            <div class="kt-portlet kt-portlet--height-fluid kt-widget19">
                                                                <div class="kt-portlet__body">
                                                                    <div class="kt-widget19__wrapper">
                                                                        <div class="kt-widget19__content">
                                                                            <div class="kt-widget19__userpic">
                                                                                <img src="assets/media/users/user1.jpg" alt="">
                                                                            </div>
                                                                            <div class="kt-widget19__info">
                                                                                <a href="#" class="kt-widget19__username">
                                                                                    Anna Krox
                                                                                </a>
                                                                                <span class="kt-widget19__time">
                                                                                    UX/UI Designer, Google
                                                                                </span>
                                                                            </div>
                                                                            <div class="kt-widget19__stats">
                                                                                <span class="kt-widget19__number kt-font-brand">
                                                                                    18
                                                                                </span>
                                                                                <a href="#" class="kt-widget19__comment">
                                                                                    Comments
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="kt-widget19__text">
                                                                            Lorem Ipsum is simply dummy text of the printing and typesetting scrambled a type specimen book text of the dummy text of the printing printing and typesetting industry scrambled dummy text of the printing.
                                                                        </div>
                                                                    </div>
                                                                    <div class="kt-widget19__action">
                                                                        <a href="#" class="btn btn-sm btn-label-brand btn-bold">Read More...</a>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <!--end:: Widgets/Blog-->
                                                        </div>    
                                                    </div>    
                                                    <div class="col-xl-4 col-lg-6 order-lg-1 order-xl-1">
                                                        <!--Begin::Portlet-->
                                                        <div class="kt-portlet kt-portlet--height-fluid">
                                                            <div class="kt-portlet__head kt-portlet__head--noborder">
                                                                <div class="kt-portlet__head-label">
                                                                    <h3 class="kt-portlet__head-title">
                                                                    </h3>
                                                                </div>
                                                                <div class="kt-portlet__head-toolbar">
                                                                    <a href="#" class="btn btn-icon" data-toggle="dropdown">
                                                                        <i class="flaticon-more-1 kt-font-brand"></i>
                                                                    </a>
                                                                    <div class="dropdown-menu dropdown-menu-right">
                                                                        <ul class="kt-nav">
                                                                            <li class="kt-nav__item">
                                                                                <a href="#" class="kt-nav__link">
                                                                                    <i class="kt-nav__link-icon flaticon2-line-chart"></i>
                                                                                    <span class="kt-nav__link-text">Reports</span>
                                                                                </a>
                                                                            </li>
                                                                            <li class="kt-nav__item">
                                                                                <a href="#" class="kt-nav__link">
                                                                                    <i class="kt-nav__link-icon flaticon2-send"></i>
                                                                                    <span class="kt-nav__link-text">Messages</span>
                                                                                </a>
                                                                            </li>
                                                                            <li class="kt-nav__item">
                                                                                <a href="#" class="kt-nav__link">
                                                                                    <i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
                                                                                    <span class="kt-nav__link-text">Charts</span>
                                                                                </a>
                                                                            </li>
                                                                            <li class="kt-nav__item">
                                                                                <a href="#" class="kt-nav__link">
                                                                                    <i class="kt-nav__link-icon flaticon2-avatar"></i>
                                                                                    <span class="kt-nav__link-text">Members</span>
                                                                                </a>
                                                                            </li>
                                                                            <li class="kt-nav__item">
                                                                                <a href="#" class="kt-nav__link">
                                                                                    <i class="kt-nav__link-icon flaticon2-settings"></i>
                                                                                    <span class="kt-nav__link-text">Settings</span>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="kt-portlet__body">

                                                                <!--begin::Widget -->
                                                                <div class="kt-widget kt-widget--user-profile-2">
                                                                    <div class="kt-widget__head">
                                                                        <div class="kt-widget__media">
                                                                            <img class="kt-widget__img kt-hidden-" src="assets/media/users/300_21.jpg" alt="image">
                                                                            <div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-hidden">
                                                                                ChS
                                                                            </div>
                                                                        </div>
                                                                        <div class="kt-widget__info">
                                                                            <a href="#" class="kt-widget__username">
                                                                                Luca Doncic
                                                                            </a>
                                                                            <span class="kt-widget__desc">
                                                                                Head of Development
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="kt-widget__body">
                                                                        <div class="kt-widget__section">
                                                                            I distinguish three <a href="#" class="kt-font-brand kt-link kt-font-transform-u kt-font-bold">#xrs-54pq</a> objectsves First
                                                                            merely firsr <b>USD249/Annual</b> your been to giant
                                                                            esetablished and nice coocked rice
                                                                        </div>
                                                                        <div class="kt-widget__item">
                                                                            <div class="kt-widget__contact">
                                                                                <span class="kt-widget__label">Email:</span>
                                                                                <a href="#" class="kt-widget__data">luca@festudios.com</a>
                                                                            </div>
                                                                            <div class="kt-widget__contact">
                                                                                <span class="kt-widget__label">Phone:</span>
                                                                                <a href="#" class="kt-widget__data">44(76)34254578</a>
                                                                            </div>
                                                                            <div class="kt-widget__contact">
                                                                                <span class="kt-widget__label">Location:</span>
                                                                                <span class="kt-widget__data">Barcelona</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="kt-widget__footer">
                                                                        <button type="button" class="btn btn-label-primary btn-lg btn-upper">write message</button>
                                                                    </div>
                                                                </div>

                                                                <!--end::Widget -->
                                                            </div>
                                                        </div>

                                                        <!--End::Portlet-->
                                                    </div>
                                                     <div class="col-xl-4 col-lg-6 order-lg-1 order-xl-1" style="height:400px;">
                                                        <div class="kt-portlet">
                                                            <!--begin:: Widgets/Blog-->
                                                            <div class="kt-portlet kt-portlet--height-fluid kt-widget19" style="    font-style: normal;font-family: Helvetica Neue,Helvetica,Arial,sans-serif;">
                                                                <div class="kt-portlet__body" style="padding: 25px 0px 0px 0px;">
                                                                    <div class="kt-widget19__wrapper">
                                                                        <div style="padding: 0px 0px 10px 25px;">
                                                                            <h3 class="kt-portlet__head-title" style="
                                                                                margin: 0;
                                                                                padding: 0;
                                                                                font-size: 1.2rem;
                                                                                font-weight: 600;
                                                                                color: #48465b;
                                                                            ">Sick Leave Request</h3>
                                                                        </div>
                                                                        <div class="kt-widget19__content" style="padding: 0px 25px;">
                                                                            <div class="kt-widget19__userpic">
                                                                                <img src="assets/media/users/user1.jpg" alt="">
                                                                            </div>
                                                                            <div class="kt-widget19__info">
                                                                                <a href="#" class="kt-widget19__username">
                                                                                    Anna Krox
                                                                                </a>
                                                                                <span class="kt-widget19__time">
                                                                                    3 Day's Ago
                                                                                </span>
                                                                            </div>
                                                                            <div class="kt-widget19__stats">
                                                                                <span class="kt-widget19__number kt-font-brand">
                                                                                    2
                                                                                </span>
                                                                                <a href="#" class="kt-widget19__comment">
                                                                                    Days
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                        <div style="
                                                                        height: 100px;
                                                                        background-color: #007bff40;
                                                                        ">
                                                                            <h3 class="kt-portlet__head-title kt-font-center" style="
                                                                                                                margin: 0;
                                                                                                                padding: 0;
                                                                                                                font-size: 1.2rem;
                                                                                                                font-weight: 600;
                                                                                                                color: #48465b;
                                                                                                                text-align: center;padding: 20px 0px 20px 0px;
                                                                                                            ">March 27 - March 28 2020</h3>
                                                                            
                                                                        </div>
                                                                        <div style="
                                                                            padding: 25px 0px 15px 25px;
                                                                        ">
                                                                            <span class="kt-font-left kt-font-bold" style="color: gray;
                                                                                        font-weight: 900;">Reason</span>
                                                                        </div>                                    
                                                                        <div class="kt-widget19__text" style="padding: 0px 25px;
                                                                            font-size: 1.2rem;
                                                                            font-weight: 600;
                                                                            color: #48465b;">
                                                                            Lorem Ipsum is simply dummy text of the printing and typesetting scrambled a type specimen book text of the dummy text of the printing.
                                                                        </div>
                                                                        <div style="
                                                                            padding: 25px 25px 15px 25px;
                                                                        ">
                                                                            <span class="kt-font-left kt-font-bold" style="color: gray;
                                                                            font-weight: 900;">Approval Process</span>
                                                                            <div class="progress"style="height: 1.20rem;">
                                                                                <div class="progress-bar bg-warning" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">Pending</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="kt-widget19__content" style="padding: 0px 25px;margin: 0px 0px 0px 0px;">
                                                                            <div class="kt-widget19__userpic">
                                                                                <img src="assets/media/users/user1.jpg" alt="">
                                                                            </div>
                                                                            <div class="kt-widget19__info">
                                                                                <div class="row">
                                                                                    <div class="col-xl-2"></div>
                                                                                    <div class="col-xl-10 kt-font-right" style="padding: 0px 0px 0px 0px;">
                                                                                        
                                                                                        <button type="button" class="btn btn-success" style="width:45%">Approve</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                        <button type="button" class="btn" style="width:40%;background-color: darkgray;
    border-color: darkgray;">Reject</button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="kt-widget19__stats">
                                                                                <button type="button" class="btn btn-outline-hover-info btn-elevate btn-icon" style="background-color: white;
    border-color: white;"><i class="la la-close"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>     
                                                                </div>
                                                            </div>
                                                            <!--end:: Widgets/Blog-->
                                                        </div> 
                                                    </div>    
                                                </div>    
                                            <?php if($this->uri->segment(3) == 'account_setting') {?>
                                                <div class="kt-portlet__head">
                                                    <div class="kt-portlet__head-label">
                                                        <h3 class="kt-portlet__head-title">Account Details <small>update your account details</small></h3>
                                                    </div>
                                                    <div class="kt-portlet__head-toolbar">
                                                        <div class="kt-portlet__head-wrapper">
                                                            <div class="dropdown dropdown-inline">
                                                                <button type="button" class="btn btn-label-brand btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="flaticon2-gear"></i>
                                                                </button>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <ul class="kt-nav">
                                                                        <li class="kt-nav__section kt-nav__section--first">
                                                                            <span class="kt-nav__section-text">Export Tools</span>
                                                                        </li>
                                                                        <li class="kt-nav__item">
                                                                            <a href="#" class="kt-nav__link">
                                                                                <i class="kt-nav__link-icon la la-print"></i>
                                                                                <span class="kt-nav__link-text">Print</span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="kt-nav__item">
                                                                            <a href="#" class="kt-nav__link">
                                                                                <i class="kt-nav__link-icon la la-copy"></i>
                                                                                <span class="kt-nav__link-text">Copy</span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="kt-nav__item">
                                                                            <a href="#" class="kt-nav__link">
                                                                                <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                                                                <span class="kt-nav__link-text">Excel</span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="kt-nav__item">
                                                                            <a href="#" class="kt-nav__link">
                                                                                <i class="kt-nav__link-icon la la-file-text-o"></i>
                                                                                <span class="kt-nav__link-text">CSV</span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="kt-nav__item">
                                                                            <a href="#" class="kt-nav__link">
                                                                                <i class="kt-nav__link-icon la la-file-pdf-o"></i>
                                                                                <span class="kt-nav__link-text">PDF</span>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <form class="kt-form kt-form--label-right">
                                                    <div class="kt-portlet__body">
                                                        <div class="kt-section kt-section--first">
                                                            <div class="kt-section__body">
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Avatar</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <div class="kt-avatar kt-avatar--outline" id="kt_user_avatar">
                                                                            <div class="kt-avatar__holder" style="background-image: url(assets/media/users/100_13.jpg)"></div>
                                                                            <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
                                                                                <i class="fa fa-pen"></i>
                                                                                <input type="file" name="profile_avatar" accept=".png, .jpg, .jpeg">
                                                                            </label>
                                                                            <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                                                                <i class="fa fa-times"></i>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">First Name</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Nick">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Last Name</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Bold">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Gender</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Nick">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Blood Group</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Bold">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Date Of Birth</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Nick">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Age</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Bold">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="kt-portlet__foot" style="display: none;">
                                                        <div class="kt-form__actions">
                                                            <div class="row">
                                                                <div class="col-lg-3 col-xl-3">
                                                                </div>
                                                                <div class="col-lg-9 col-xl-9">
                                                                    <button type="reset" class="btn btn-success">Submit</button>&nbsp;
                                                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            <?php } else if($this->uri->segment(3) == 'personal_information') {?>
                                                <div class="kt-portlet__head">
                                                    <div class="kt-portlet__head-label">
                                                        <h3 class="kt-portlet__head-title">Personal Information <small>update your personal details</small></h3>
                                                    </div>
                                                     <div class="kt-portlet__head-toolbar">
                                                        <div class="kt-portlet__head-wrapper">
                                                            <div class="dropdown dropdown-inline">
                                                                <button type="button" class="btn btn-label-brand btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="flaticon2-gear"></i>
                                                                </button>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <ul class="kt-nav">
                                                                        <li class="kt-nav__section kt-nav__section--first">
                                                                            <span class="kt-nav__section-text">Export Tools</span>
                                                                        </li>
                                                                        <li class="kt-nav__item">
                                                                            <a href="#" class="kt-nav__link">
                                                                                <i class="kt-nav__link-icon la la-print"></i>
                                                                                <span class="kt-nav__link-text">Print</span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="kt-nav__item">
                                                                            <a href="#" class="kt-nav__link">
                                                                                <i class="kt-nav__link-icon la la-copy"></i>
                                                                                <span class="kt-nav__link-text">Copy</span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="kt-nav__item">
                                                                            <a href="#" class="kt-nav__link">
                                                                                <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                                                                <span class="kt-nav__link-text">Excel</span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="kt-nav__item">
                                                                            <a href="#" class="kt-nav__link">
                                                                                <i class="kt-nav__link-icon la la-file-text-o"></i>
                                                                                <span class="kt-nav__link-text">CSV</span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="kt-nav__item">
                                                                            <a href="#" class="kt-nav__link">
                                                                                <i class="kt-nav__link-icon la la-file-pdf-o"></i>
                                                                                <span class="kt-nav__link-text">PDF</span>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <form class="kt-form kt-form--label-right">
                                                    <div class="kt-portlet__body">
                                                        <div class="kt-section kt-section--first">
                                                            <div class="kt-section__body">
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Personal Mobile</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Nick">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Personal Email</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Bold">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Marital Status</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Nick">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Qualifaction</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Bold">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Aadhar Card</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Nick">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Pan Card</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Bold">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            <?php } else if($this->uri->segment(3) == 'address') {?>
                                                <div class="kt-portlet__head">
                                                    <div class="kt-portlet__head-label">
                                                        <h3 class="kt-portlet__head-title">Address <small>update your address</small></h3>
                                                    </div>
                                                </div>
                                                <form class="kt-form kt-form--label-right">
                                                    <div class="kt-portlet__body">
                                                        <div class="kt-section kt-section--first">
                                                            <div class="kt-section__body">
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Address</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Nick">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Postcode</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Bold">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">State</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Nick">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">City</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Bold">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            <?php } else if($this->uri->segment(3) == 'official_information') {?>
                                                <div class="kt-portlet__head">
                                                    <div class="kt-portlet__head-label">
                                                        <h3 class="kt-portlet__head-title">Official Information <small>update your official details</small></h3>
                                                    </div>
                                                </div>
                                                <form class="kt-form kt-form--label-right">
                                                    <div class="kt-portlet__body">
                                                        <div class="kt-section kt-section--first">
                                                            <div class="kt-section__body">
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Official Mobile</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Nick">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Official Email</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Bold">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Job Location</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Nick">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Department</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Bold">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            <?php } else if($this->uri->segment(3) == 'bank_details') {?>
                                                <div class="kt-portlet__head">
                                                    <div class="kt-portlet__head-label">
                                                        <h3 class="kt-portlet__head-title">Payment Information <small>update your payment details</small></h3>
                                                    </div>
                                                </div>
                                                <form class="kt-form kt-form--label-right">
                                                    <div class="kt-portlet__body">
                                                        <div class="kt-section kt-section--first">
                                                            <div class="kt-section__body">
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Bank Name</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Nick">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Branch Name</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Bold">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Benificary Name</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Nick">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Account Number</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Bold">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">IFSC Code</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Nick">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            <?php } else if($this->uri->segment(3) == 'reference') {?>
                                                <div class="kt-portlet__head">
                                                    <div class="kt-portlet__head-label">
                                                        <h3 class="kt-portlet__head-title">Reference Information <small>update your reference details</small></h3>
                                                    </div>
                                                </div>
                                                <form class="kt-form kt-form--label-right">
                                                    <div class="kt-portlet__body">
                                                        <div class="kt-section kt-section--first">
                                                            <div class="kt-section__body">
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Reference Name</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Nick">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Relation</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Bold">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Address</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Nick">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Number</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Bold">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Alternate Number</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Nick">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">Email</label>
                                                                    <div class="col-lg-9 col-xl-6">
                                                                        <input class="form-control" type="text" value="Bold">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            <?php }?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--End:: App Content-->
                            </div>

                            <!--End::App-->
                        </div>

                        <!-- end:: Content -->
            </div>

            <!-- begin:: Footer -->
            <div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
                <div class="kt-container  kt-container--fluid ">
                    <div class="kt-footer__copyright">
                        2020&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Keenthemes</a>
                    </div>
                    <div class="kt-footer__menu">
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">About</a>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Team</a>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Contact</a>
                    </div>
                </div>
            </div>

            <!-- end:: Footer -->
        </div>
    </div>
</div>

<!-- end:: Page -->

<!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="fa fa-arrow-up"></i>
</div>

<!-- end::Scrolltop -->