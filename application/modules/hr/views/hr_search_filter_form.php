<div class="row kt-margin-b-20">
    <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
        <label>Employee Name:</label>
        <select class="form-control" name="employee_id">
            <option value="">Select</option>
            <?php foreach($employee_list as $employee_details) {?>
                <option value="<?php echo $employee_details['id'];?>" <?php echo ($search_filter['employee_id'] == $employee_details['id']) ? 'selected': ''; ?>><?php echo trim(ucfirst(strtolower($employee_details['first_name']))), ' ', trim(ucfirst(strtolower($employee_details['last_name'])));?></option>
            <?php } ?>     
        </select>
    </div>
    <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
        <label>Department:</label>
        <select class="form-control" name="role_name">
            <option value="">Select</option>
            <?php foreach($department_list as $department_details) {?>
                <option value="<?php echo $department_details['role_name'];?>" <?php echo ($search_filter['role_name'] == $department_details['role_name']) ? 'selected': ''; ?>><?php echo $department_details['role_name'];?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="kt-separator kt-separator--md kt-separator--dashed"></div>
<div class="row">
    <div class="col-lg-12">
        <button class="btn btn-primary btn-brand--icon user_search_filter_form_submit" type="reset">
            <span>
                <i class="la la-search"></i>
                <span>Search</span>
            </span>
        </button>
        &nbsp;&nbsp;
        <button class="btn btn-secondary btn-secondary--icon user_search_filter_form_reset" type="reset">
            <span>
                <i class="la la-close"></i>
                <span>Reset</span>
            </span>
        </button>
    </div>
</div>