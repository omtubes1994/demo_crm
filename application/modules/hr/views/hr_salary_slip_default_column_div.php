<?php foreach ($salary_slip_form_column as $salary_slip_key => $salary_slip_name) { ?>
    <div class="col-lg-4 salary_slip_default_form_field" style="display:none;padding-top: 10px;">
        <label><?php echo ucfirst($salary_slip_name);?>:</label>
        <div class="row">
            <div class="col-lg-8">
                <input type="text" class="form-control" name="<?php echo $salary_slip_name;?>" value="<?php echo $salary_slip_default_details[$salary_slip_name];?>">
            </div>
            <!-- <div class="col-lg-3" style="padding-top: 0.5rem;">
                <label class="kt-radio kt-radio--bold kt-radio--brand">
                    <input type="radio" name="<?php echo $salary_slip_name, '_income_or_expense';?>" value="income" checked> Income
                    <span></span>
                </label>
            </div>
            <div class="col-lg-3" style="padding-top: 0.5rem;">
                <label class="kt-radio kt-radio--bold kt-radio--brand">
                    <input type="radio" name="<?php echo $salary_slip_name, '_income_or_expense';?>" value="expense"> Expense
                    <span></span>
                </label>
            </div> -->
        </div>
    </div>
<?php } ?>