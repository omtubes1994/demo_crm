<?php 
Class Lead_management_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$CI = &get_instance();
	}

	public function	get_listing_data_experiment($select_string, $join_string, $where_string, $having_string, $order_by_string, $limit, $offset){
		
		$return_array = array();
		$query = " 	SELECT
						{$select_string}
					FROM
						`customer_mst`
						{$join_string}
					WHERE
						{$where_string}
					GROUP BY
						customer_mst.id";
		if(!empty($having_string)){

			$query .= " 
					HAVING
						{$having_string}";
		}
		$query .= "	
					ORDER BY
						{$order_by_string}
					LIMIT 
						{$limit}
					OFFSET
						{$offset}";


		$return_array['list_data'] = $this->db->query($query)->result_array();
					// WHERE 
					// last_contacted is TRUE
		// echo "<pre>";print_r($this->db->last_query());echo"</pre><hr>";exit;
		$return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows")->row_array();
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";
		return $return_array;		

	}	

	public function get_listing_count_data($join_string, $where_string, $having_string){
		$query = " 
			SELECT COUNT(*) as total_count
			FROM (
				SELECT DISTINCT customer_mst.id, MAX(customer_connect.connected_on)  AS last_contact_date
				FROM `customer_mst`
				{$join_string}
				WHERE {$where_string}
				GROUP BY customer_mst.id";

		if (!empty($having_string)) {
			$query .= " HAVING {$having_string}";
		}

		$query .= ") as filtered_customers";
		$result = $this->db->query($query)->row_array();
		// echo "<pre>";print_r($this->db->last_query());echo"</pre><hr>";exit;

		return $result['total_count'] ?? 0;
	}

	public function	get_listing_data($select_string, $where_array, $order_by_string, $limit, $offset){
		
		$return_array = array();
		$this->db->select($select_string, FALSE);
		$this->db->join('customer_data', 'customer_data.customer_mst_id = customer_mst.id', 'left');
		$this->db->join('product_category', 'product_category.id = customer_data.product_category_id', 'left');
		$this->db->join('country_mst', 'country_mst.id = customer_mst.country_id', 'left');
		$this->db->join('country_flags', 'country_flags.country = country_mst.name', 'left');
		$this->db->join('customer_connect', 'customer_connect.comp_mst_id = country_mst.id', 'left');
		$this->db->where($where_array, null, false);
		$this->db->order_by($order_by_string);
		// $this->db->order_by('customer_mst.lead_stage desc, customer_mst.name asc');
		// $this->db->order_by($order_by_array['column_name'], $order_by_array['column_value']);
		$return_array['list_data'] = $this->db->get('customer_mst', $limit, $offset)->result_array();
		echo "<pre>";print_r($this->db->last_query());echo"</pre><hr>";exit;
		$return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows")->row_array();
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		return $return_array;		

		

	}	
	public function	get_primary_listing_data($where_array, $order_by_array, $limit, $offset){
		
		// $return_array = array();
		$this->db->select('SQL_CALC_FOUND_ROWS customer_mst.id, customer_mst.name customer_name, customer_mst.region_id, customer_mst.country_id, customer_mst.assigned_to, customer_mst.website, customer_mst.no_of_employees, customer_mst.lead_type, customer_mst.lead_stage, customer_mst.stage_reason, customer_mst.box, customer_mst.box_comment, customer_mst.sample, customer_mst.sample_comment, customer_mst.lead_priority, customer_mst.priority_reason, customer_mst.entered_by, customer_mst.modified_by, customer_mst.deleted, customer_mst.purchase_factor_1, customer_mst.purchase_factor_2, customer_mst.purchase_factor_3, customer_mst.sales_notes, customer_mst.product_pitch, customer_mst.purchase_comments, customer_mst.margins, customer_mst.delete_reason, customer_mst.brand, customer_mst.status, customer_mst.entered_on, customer_mst.modified_on,
		customer_data.source,  customer_data.rank, DATE_FORMAT(customer_data.last_purchased, "%b-%y") last_purchased, customer_data.product_category_id, product_category.product_name, country_mst.name country_name, country_flags.flag_name', FALSE);
		
		$this->db->join('customer_data', 'customer_data.customer_mst_id = customer_mst.id', 'left');
		$this->db->join('product_category', 'product_category.id = customer_data.product_category_id', 'left');
		// $this->db->join('country_mst', 'country_mst.id = customer_data.cod_mst_id', 'left');
		$this->db->join('country_mst', 'country_mst.id = customer_mst.country_id', 'left');
		$this->db->join('country_flags', 'country_flags.country = country_mst.name', 'left');
					
		$this->db->where($where_array, null, false);
		$this->db->order_by($order_by_array['column_name'], $order_by_array['column_value']);
		$return_array['primary_list'] = $this->db->get('customer_mst', $limit, $offset)->result_array();
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		$return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows")->row_array();
		return $return_array;		
	}

	public function get_hetro_listing_data($where_array, $limit, $offset){

		$return_array = array();
		$this->db->select('SQL_CALC_FOUND_ROWS customer_mst.id, customer_mst.name customer_name, customer_mst.region_id, customer_mst.country_id, customer_mst.website, customer_mst.entered_on, customer_mst.entered_by, customer_mst.modified_on, customer_mst.modified_by, customer_mst.assigned_to,  customer_mst.no_of_employees, customer_mst.lead_type, customer_mst.lead_stage, customer_mst.stage_reason, customer_mst.box, customer_mst.box_comment, customer_mst.sample, customer_mst.sample_comment, customer_mst.lead_priority, customer_mst.priority_reason, customer_mst.deleted, customer_data.source, customer_data.last_purchased, customer_data.product_category_id, product_category.product_name, country_mst.name country_name,
		country_flags.flag_name', FALSE);

		$this->db->join('customer_data', 'customer_data.customer_mst_id = customer_mst.id', 'left');
		$this->db->join('product_category', 'product_category.id = customer_data.product_category_id', 'left');
		$this->db->join('country_mst', 'country_mst.id = customer_mst.country_id', 'left');
		$this->db->join('country_flags', 'country_flags.country = country_mst.name', 'left');

		$this->db->where($where_array, null, false);
		$this->db->order_by('customer_mst.lead_stage desc, customer_mst.name asc');
		$return_array['hetro_list'] = $this->db->get('customer_mst', $limit, $offset)->result_array();		
		// echo "<pre>";print_r($this->db->last_query());echo"</pre><hr>";exit;
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		$return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows;")->row_array();
		return $return_array;
	}


	public function get_dynamic_data_sales_db($select_string = '*', $where_array, $table_name, $return_type = 'result_array', $limit = 0, $offset = 0) {
		
		$this->db->select($select_string);
		$this->db->where($where_array);
		return $this->db->get($table_name, $limit, $offset)->$return_type();
	}

	public function get_all_conditional_data_sales_db($select_string = '*', $where_array, $table_name, $return_type = 'result_array', $join_array = array(), $order_by_array = array(), $group_by_array = array(), $limit = 0, $offset = 0) {

		$this->db->select($select_string);
		$this->db->where($where_array);
		if(!empty($group_by_array)) {
			$this->db->group_by($group_by_array);
		}
		if(!empty($order_by_array)) {
			$this->db->order_by($order_by_array['column_name'], $order_by_array['column_value']);
		}
		return $this->db->get($table_name, $limit, $offset)->$return_type();
	}

	public function getAllYearName() {

		$res = $this->db->query("	SELECT
							   			CASE WHEN MONTH(last_purchased)>=3 
							   			THEN concat(YEAR(last_purchased), '-',YEAR(last_purchased)+1)
							   			ELSE concat(YEAR(last_purchased)-1,'-', YEAR(last_purchased)) 
							   			END AS year
									FROM customer_data
									WHERE Year(last_purchased) != 0
									GROUP BY year
									order by last_purchased desc;"

								)->result_array();
		// echo "<pre>";print_r($res);	echo"</pre><hr>";exit;
		return $res;
	}

	public function get_dynamic_data_sales_db_null_false($select_string = '*', $where_array, $table_name, $return_type = 'result_array', $limit = 0, $offset = 0) {

		$this->db->select($select_string);
		$this->db->where($where_array, null, false);
		return $this->db->get($table_name, $limit, $offset)->$return_type();
	}

	public function get_all_data($select, $where_array, $table_name, $result_type = 'result_array') {

		$this->db->select($select);
		$this->db->where($where_array);
		return $this->db->get($table_name)->$result_type();		
	}

	public function delete_Data($table, $where){
		$this->db->delete($table, $where);
	}

	public function insert_data($table_name, $insert_data, $insert_type = 'single_insert'){

		if($insert_type == 'single_insert') {

			$this->db->insert($table_name, $insert_data);
			return $this->db->insert_id();
		} else if ($insert_type == 'batch') {

			$this->db->insert_batch($table_name, $insert_data);
		}
	}

	public function update_data($table_name, $update_data, $where_array, $update_type = 'single_update'){
		
		if($update_type == 'single_update') {

			return $this->db->update($table_name, $update_data, $where_array);
		} else if ($update_type == 'batch') {

			$this->db->update_batch($table_name, $update_data, $where_array);

			// echo "<pre>";print_r($this->db->last_query());echo"</pre><hr>";
		}
	}

	public function get_last_purchase_date() {

		$res = $this->db->query("	SELECT
							   			(
											CASE WHEN MONTH(last_purchased)>=3 
											THEN concat(YEAR(last_purchased), '-',YEAR(last_purchased)+1)
											ELSE concat(YEAR(last_purchased)-1,'-', YEAR(last_purchased)) 
											END 
										)AS value,
										(
											CASE WHEN MONTH(last_purchased) >= 3
											THEN YEAR(last_purchased)
											ELSE YEAR(last_purchased) - 1
											END
										) AS name, '' as selected
									FROM customer_data
									WHERE Year(last_purchased) != 0
									GROUP BY value, name
									order by last_purchased desc;"

								)->result_array();
		// echo "<pre>";print_r($res);	echo"</pre><hr>";exit;
		return $res;
	}

	public function get_lead_name_count($product_category_id){

		$res = $this->db->query("SELECT
									customer_mst.id
								FROM
									`customer_mst` 
								INNER JOIN
									`customer_data` 
										ON `customer_data`.`customer_mst_id` = `customer_mst`.`id` 
								WHERE
									customer_mst.deleted is null
									AND customer_data.rank is not null
									AND customer_data.product_category_id = {$product_category_id}
								"
							)->result_array();
			// echo $this->db->last_query(),"<hr>";
		return $res;
	}
	public function get_lead_name_count_hetro($product_category_id){

		$res = $this->db->query("SELECT
									customer_mst.id
								FROM
									`customer_mst` 
								INNER JOIN
									`customer_data` 
										ON `customer_data`.`customer_mst_id` = `customer_mst`.`id` 
								WHERE
									customer_mst.deleted is null
									AND customer_data.product_category_id = {$product_category_id}
								"
							)->result_array();
			// echo $this->db->last_query(),"<hr>";
		return $res;
	}
	public function get_lead_name_count_internal(){

		$res = $this->db->query("SELECT
									customer_mst.id
								FROM
									`customer_mst` 
								INNER JOIN
									`customer_data` 
										ON `customer_data`.`customer_mst_id` = `customer_mst`.`id` 
								WHERE
									customer_data.source = 'internal'
								"
							)->result_array();
			// echo $this->db->last_query(),"<hr>";
		return $res;
	}
	public function get_url_data($product_name, $access_type){

		$res = $this->db->query("SELECT
									`url`
								FROM
									`search_engine_access_url`
								WHERE
									`status` = 'Active'
									AND`lead_name` = '".$product_name."'
									AND`access_type` = '".$access_type."'
								"
							)->row_array();
			// echo $this->db->last_query(),"<hr>";
		return $res;
	}
	public function get_quotation_vs_proforma_highchart_list($company_id) {

		$where_string = "stage IN ('publish','proforma') AND YEAR(entered_on) = ".date('Y')."  AND client_id = ".$company_id;
		$res = $this->db->query("
							SELECT
							MONTHNAME(entered_on) month,
							count('*') count,
							count(IF(status = 'won', 1, NULL)) won,
							count(IF(status = 'open', 1, NULL)) open,
							count(IF(status = 'closed', 1, NULL)) closed
							FROM quotation_mst
							WHERE {$where_string}
							group by month
							order by entered_on ASC
						")->result_array();
		// echo $this->db->last_query(),"<hr>";die;
		return $res;
	}
}
