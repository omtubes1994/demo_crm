<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lead_management extends MX_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('user_id')){
			redirect('login', 'refresh');
			exit;
		}
		else{
			// $access_count = $main_module_count = 0;
			// if(!empty($this->session->userdata('main_module_access'))) {

			// 	foreach ($this->session->userdata('main_module_access') as $key => $value) {
					
			// 		if(in_array($value['module_id'], array(24, 25, 26)) && $value['status'] == 'Active') {
			// 			$main_module_count++;
			// 		}
			// 	}
			// 	if($main_module_count > 0) {

			// 		if(!empty($this->session->userdata('lead_count'))) {
						
			// 			foreach ($this->session->userdata('lead_count') as $key => $value) {
							
			// 				if($value > 0) {
			// 					$access_count++;
			// 				}
			// 			}
			// 		}
			// 	}
			// }
			// if($access_count == 0  && $this->session->userdata('role') != 1){
			// 	redirect($this->session->userdata('home/dashboard'));
			// 	exit;
			// }
		}
		error_reporting(0);
		$this->load->model('lead_management_model');
		$this->load->model('common/common_model');
		$this->load->model('home/home_model');
	}

	public function show_session(){

		echo "<pre>"; print_r($this->session->userdata()); "</prer>"; exit;
	}

	public function index(){

		$data = array();

		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Leads List'));
		$this->load->view('sidebar', array('title' => 'Leads List'));
		$this->load->view('lead_management/index',$data);
		$this->load->view('footer');
	}

	public function primary_lead(){

		$category = $this->uri->segment(3, 0);
		// echo "<pre>";print_r($category);echo"</pre><hr>";exit;
		if(!empty($category)){	

			if($category == 'PROCESS_CONTROL'){
				$category = 'PROCESS_CONTROL';
			}else if($category == 'HAMMER%20UNION'){
				$category = 'HAMMER UNION';				
			}
		}

		$data = $this->create_primary_lead_data(array('customer_data.rank !=' => null, 'customer_mst.deleted is null', 'product_category.product_name LIKE'=> "'%".$category."%'"), array('column_name'=>'customer_data.rank', 'column_value'=>'asc'), 10, 0);
		$data['category_name'] = $category;

		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Leads List'));
		$this->load->view('sidebar', array('title' => 'Leads List'));
		$this->load->view('lead_management/primary_list',$data);
		$this->load->view('footer');
	}

	public function hetro_lead(){
		
		$category = $this->uri->segment(3, 0);
		// echo "<pre>";print_r($category);echo"</pre><hr>";exit;
		if(!empty($category)){
			
			if($category == 'Water%20Companies'){
				$category = 'Water Companies';
			}else if($category == 'Chemical%20Companies'){
				$category = 'Chemical Companies';				
			}else if($category == 'EPC%20companies'){
				$category = 'EPC Companies';				
			}else if($category == 'sugar%20companies'){
				$category = 'sugar Companies';				
			}else if($category == 'Heteregenous%20Tubes%20India'){
				$category = 'Heteregenous Tubes India';				
			}else if($category == 'pvf%20companies'){
				$category = 'pvf companies';				
			}else if($category == 'Miscellaneous%20Leads'){
				$category = 'Miscellaneous Leads';				
			}else if($category == 'Forged%20Fittings'){
				$category = 'Forged Fittings';				
			}
		}
		
		$data = $this->create_hetro_lead_data(array('customer_mst.id !='=> null, 'customer_mst.deleted is null', 'product_category.product_name LIKE'=> "'%".$category."%'"), 10, 0);		
		$data['category_name'] = $category;

		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Leads List'));
		$this->load->view('sidebar', array('title' => 'Leads List'));
		$this->load->view('lead_management/hetro_list',$data);
		$this->load->view('footer');
	}

	public function update_primary_lead(){

		$data = array();
		$data['customer_data'] = array('id'=>'', 'name'=>'', 'country_id'=>'', 'region_id'=>'', 'assigned_to'=>'', 'website'=>'', 'no_of_employees'=>'', 'lead_type'=>'', 'lead_stage'=>'', 'stage_reason'=>'', 'purchase_factor_1'=>'', 'purchase_factor_2'=>'', 'purchase_factor_3'=>'', 'product_pitch'=>'', 'box'=>'', 'box_comment'=>'', 'purchase_comments'=>'', 'sample'=>'', 'sample_comment'=>'', 'sales_notes'=>'', 'margins'=>'', 'lead_priority'=>'', 'priority_reason'=>'');
		

		$data['next_count_number'] = 1;
		$data['next_number'] = 1;
		
		$comp_mst_id = $this->uri->segment(3, 0);
		// echo "<pre>";print_r($comp_mst_id);echo"</pre><hr>";exit;
		if(!empty($comp_mst_id)){
			
			$data['customer_data'] = $this->lead_management_model->get_all_data('*', array('id'=> $comp_mst_id), 'customer_mst','row_array');
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
			
			if(!empty($data['customer_data'])){
				
				$data['company_data'] = $this->lead_management_model->get_all_data('*', array('customer_mst_id'=> $comp_mst_id), 'customer_data','row_array');	

				$data['member_last_contact'] = $this->lead_management_model->get_all_conditional_data_sales_db('*', array('comp_mst_id'=>$comp_mst_id), 'customer_connect','row_array', array(),array('column_name'=>'connected_on', 'column_value'=>'desc'),array(),0,0);			
			}
			
			$data['member_detail'] = $this->get_member_detail($comp_mst_id);
			$data['other_member_detail'] = $this->get_other_member_detail($comp_mst_id);			
			
			$data['next_count_number'] = count($data['member_detail'])+1;
			$data['next_number'] = count($data['other_member_detail'])+1;
			
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		}
		
		$data['assigned_to'] = $this->lead_management_model->get_dynamic_data_sales_db_null_false('user_id,name',"status=1 AND role IN (5, 16)",'users');

		$data['region_list'] = $this->lead_management_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'region_mst');

		$data['country_list'] = $this->lead_management_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'country_mst');

		$data['lead_type'] = $this->lead_management_model->get_dynamic_data_sales_db('lead_type_id, type_name', array(),'lead_type');

		$data['lead_stage'] = $this->lead_management_model->get_dynamic_data_sales_db('lead_stage_id, stage_name', array(),'lead_stages');

		$data['stage_reasons'] = $this->lead_management_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'lead_stage_reasons');

		$data['purchase_factor'] = $this->lead_management_model->get_dynamic_data_sales_db('*', array(),'purchase_factors');		

		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Leads List'));
		$this->load->view('sidebar', array('title' => 'Leads List'));
		$this->load->view('lead_management/update_primary_lead_form',$data);
		$this->load->view('footer');
	}

	public function update_hetro_lead(){

		$data = array();
		$data['customer_data'] = array('id'=>'', 'name'=>'', 'region_id'=>'', 'country_id'=>'', 'website'=>'', 'no_of_employees'=>'','assigned_to'=>'', 'lead_type'=>'', 'lead_stage'=>'', 'stage_reason'=>'', 'box'=>'', 'sample'=>'', 'box_comment' =>'', 'sample_comment'=>'', 'lead_priority'=>'', 'priority_reason'=>'', 'brand'=>'');

		$data['next_count_number'] = 1;
		$data['next_number'] = 1;
		$comp_mst_id = $this->uri->segment(3, 0);
		// echo "<pre>";print_r($comp_mst_id);echo"</pre><hr>";exit;
		
		if(!empty($comp_mst_id)){
			
			$data['customer_data'] = $this->lead_management_model->get_all_data('*', array('id'=> $comp_mst_id), 'customer_mst','row_array');
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
			
			if(!empty($data['customer_data'])){
				
				$data['company_data'] = $this->lead_management_model->get_all_data('*', array('customer_mst_id'=> $comp_mst_id, 'source'=>'hetro'), 'customer_data','row_array');	

				$data['member_last_contact'] = $this->lead_management_model->get_all_conditional_data_sales_db('*', array('comp_mst_id'=>$comp_mst_id), 'customer_connect','row_array', array(),array('column_name'=>'connected_on', 'column_value'=>'desc'),array(),0,0);			
			}

			$data['member_detail'] = $this->get_member_detail($comp_mst_id);
			$data['other_member_detail'] = $this->get_other_member_detail($comp_mst_id);			
			// echo "<pre>";print_r($data['other_member_detail']);echo"</pre><hr>";exit;
			
			$data['next_count_number'] = count($data['member_detail'])+1;
			$data['next_number'] = count($data['other_member_detail'])+1;			
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		
		$data['assigned_to'] = $this->lead_management_model->get_dynamic_data_sales_db_null_false('user_id,name',"status=1 AND role IN (5, 16)",'users');

		$data['region_list'] = $this->lead_management_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'region_mst');

		$data['country_list'] = $this->lead_management_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'country_mst');

		$data['lead_type'] = $this->lead_management_model->get_dynamic_data_sales_db('lead_type_id, type_name', array(),'lead_type');

		$data['lead_stage'] = $this->lead_management_model->get_dynamic_data_sales_db('lead_stage_id, stage_name', array(),'lead_stages');

		$data['stage_reasons'] = $this->lead_management_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'lead_stage_reasons');

		$data['purchase_factor'] = $this->lead_management_model->get_dynamic_data_sales_db('*', array(),'purchase_factors');		

		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Leads List'));
		$this->load->view('sidebar', array('title' => 'Leads List'));
		$this->load->view('lead_management/update_hetro_lead_form',$data);
		$this->load->view('footer');
	}

	// Add New Lead
	public function add_lead(){

		$data = array();
		$data['customer_data'] = array('id'=>'', 'name'=>'', 'region_id'=>'', 'country_id'=>'', 'website'=>'', 'no_of_employees'=>'','assigned_to'=>'', 'lead_type'=>'', 'lead_stage'=>'', 'stage_reason'=>'', 'box'=>'', 'sample'=>'', 'box_comment' =>'', 'sample_comment'=>'', 'lead_priority'=>'', 'priority_reason'=>'', 'brand'=>'');

		$data['next_count_number'] = 1;
		$data['next_number'] = 1;
		
		$data['assigned_to'] = $this->lead_management_model->get_dynamic_data_sales_db_null_false('user_id,name',"status=1 AND role IN (5, 16)", 'users');

		$data['region_list'] = $this->lead_management_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'region_mst');

		$data['country_list'] = $this->lead_management_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'country_mst');

		$data['lead_type'] = $this->lead_management_model->get_dynamic_data_sales_db('lead_type_id, type_name', array(),'lead_type');

		$data['lead_stage'] = $this->lead_management_model->get_dynamic_data_sales_db('lead_stage_id, stage_name', array(),'lead_stages');

		$data['stage_reasons'] = $this->lead_management_model->get_dynamic_data_sales_db('*', array('status'=>'Active'),'lead_stage_reasons');

		$data['lead_category'] = $this->lead_management_model->get_dynamic_data_sales_db('*', array('source'=>'Hetro'),'product_category');		

		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Leads List'));
		$this->load->view('sidebar', array('title' => 'Leads List'));
		$this->load->view('lead_management/add_new_lead',$data);
		$this->load->view('footer');
	}

	public function ajax_function() {

		if($this->input->is_ajax_request()){
			$post_details = $this->input->post();
			$call_type = $this->input->post('call_type');
			$response['status'] = 'successful';	
			switch ($call_type) {

				case 'get_search_filter_div':
						
					// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;
					$data['category'] = $post_details['category'];
					$data['all_year'] = $this->lead_management_model->getAllYearName();
					// $data['last_contacted'] = $this->get_last_contact()
					// $data['connect_mode'] = $this->lead_management_model->get_all_conditional_data_sales_db('comp_connect_id, connect_mode',array(),'customer_connect');
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
					$data['lead_type'] = $this->lead_management_model->get_all_conditional_data_sales_db('lead_type_id, type_name',array(),'lead_type');
					$data['lead_stage'] = $this->lead_management_model->get_all_conditional_data_sales_db('lead_stage_id, stage_name' ,array(),'lead_stages');
					$data['region_list'] = $this->lead_management_model->get_all_conditional_data_sales_db('id, name',array('status'=>'active'),'region_mst');
					$data['country_list'] = $this->lead_management_model->get_all_conditional_data_sales_db('id, name',array('status'=>'active'),'country_mst');
					$data['company_list'] = $this->lead_management_model->get_dynamic_data_sales_db('id, name', array('status'=>'Active'), 'customer_mst');						
					$data['user_list'] = $this->lead_management_model->get_dynamic_data_sales_db_null_false('user_id,name',"status=1 AND role IN (5, 16)",'users');
						
					$data['search_filter'] = array('year'=> '', 'box' => '', 'sample'=>'', 'lead_type' => '', 'lead_stage' => '', 'company_name' => '', 'region_id' => '', 'country_id' => '', 'assigned_to' => '', 'lead_priority'=>'', 'connect_mode'=>'');
						
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
					$response['search_filter_data'] = $this->load->view('lead_management/search_filter_form', $data, true);
				break;

				case 'primary_search_filter':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;					
					$search_filter_data = $this->create_search_filter_data($post_details['search_form_data'], $post_details['category_name'], $post_details['category_source']);
					
					$data = $this->create_primary_lead_data($search_filter_data['where'] , array('column_name'=>'customer_data.rank', 'column_value'=>'asc'), $this->input->post('limit'), $this->input->post('offset'));
					
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;					
					$data['search_filter'] = $search_filter_data['search_filter_data'];	
					$data['category'] = 'primary_lead';
					// echo "<pre>";print_r($search_filter_data);echo"</pre><hr>";exit;
					$data['all_year'] = $this->lead_management_model->getAllYearName();				
					$data['lead_type'] = $this->lead_management_model->get_all_conditional_data_sales_db('lead_type_id, type_name',array(),'lead_type');
					$data['lead_stage'] = $this->lead_management_model->get_all_conditional_data_sales_db('lead_stage_id, stage_name',array(),'lead_stages');
					$data['region_list'] = $this->lead_management_model->get_all_conditional_data_sales_db('id, name',array('status'=>'active'),'region_mst');
					$data['country_list'] = $this->lead_management_model->get_all_conditional_data_sales_db('id, name',array('status'=>'active'),'country_mst');						
					$data['company_list'] = $this->lead_management_model->get_dynamic_data_sales_db('id, name', array('status'=>'Active'), 'customer_mst');						
					$data['user_list'] = $this->lead_management_model->get_dynamic_data_sales_db_null_false('user_id,name',"status=1 AND role IN (5, 16)",'users');
					
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
					$response['primary_list_body'] = $this->load->view('lead_management/primary_list_body', $data, true);
					$response['primary_search_filter'] = $this->load->view('lead_management/search_filter_form', $data, true);
					$response['primary_paggination'] = $this->load->view('lead_management/paggination', $data, true);					
				break;
					
				case 'hetro_search_filter':
					// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;					
					$search_filter_data = $this->create_search_filter_data($post_details['search_form_data'], $post_details['category_name'],$post_details['category_source']);					
						
					$data = $this->create_hetro_lead_data($search_filter_data['where'], $this->input->post('limit'), $this->input->post('offset'));
						
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
					$data['search_filter'] = $search_filter_data['search_filter_data'];					
					$data['lead_type'] = $this->lead_management_model->get_all_conditional_data_sales_db('lead_type_id, type_name',array(),'lead_type');
					$data['lead_stage'] = $this->lead_management_model->get_all_conditional_data_sales_db('lead_stage_id, stage_name',array(),'lead_stages');
					$data['region_list'] = $this->lead_management_model->get_all_conditional_data_sales_db('id, name',array('status'=>'active'),'region_mst');
					$data['country_list'] = $this->lead_management_model->get_all_conditional_data_sales_db('id, name',array('status'=>'active'),'country_mst');
					$data['company_list'] = $this->lead_management_model->get_dynamic_data_sales_db('id, name', array('status'=>'Active'), 'customer_mst');						
					$data['user_list'] = $this->lead_management_model->get_dynamic_data_sales_db_null_false('user_id,name',"status=1 AND role IN (5, 16)",'users');
					
					$response['hetro_list_body'] = $this->load->view('lead_management/hetro_list_body', $data, true);
					$response['hetro_search_filter'] = $this->load->view('lead_management/search_filter_form', $data, true);
					$response['hetro_paggination'] = $this->load->view('lead_management/paggination', $data, true);					
				break;


				// common case Primary + Hetro
				case 'primary_box_sample_comment_modal':
				case 'hetro_box_sample_comment_modal':
					// echo "<pre>";print_r($post_details['comp_mst_id']);echo"</pre><hr>";exit;
					if(!empty($post_details['comp_mst_id'])){
							
						$data['comment_detail'] = $this->lead_management_model->get_all_conditional_data_sales_db('*',array('id'=>$post_details['comp_mst_id']),'customer_mst','row_array');
							
						// echo "<pre>";print_r($data['comment_detail']);echo"</pre><hr>";exit;
						$response['box_sample_comment_model_body'] = $this->load->view('lead_management/box_sample_comment_model_body', $data, true);
					}
				break;

				case 'primary_member_count_modal':
				case 'hetro_member_count_modal':
					// echo "<pre>";print_r($post_details['comp_mst_id']);echo"</pre><hr>";exit;
					if(!empty($post_details['comp_mst_id'])){
							
						$data['buyer_member'] = $this->lead_management_model->get_all_conditional_data_sales_db('*',array('comp_mst_id'=>$post_details['comp_mst_id'], 'other_member !='=>'Yes', 'status'=>'Active'),'customer_dtl');

						$data['other_member'] = $this->lead_management_model->get_all_conditional_data_sales_db('*',array('comp_mst_id'=>$post_details['comp_mst_id'], 'other_member'=>'Yes', 'status'=>'Active'),'customer_dtl');
							// echo "<pre>";print_r($other_member);echo"</pre><hr>";exit;
						$response['member_count_model_body'] = $this->load->view('lead_management/member_count_model_body', $data, true);
					}
				break;

				case 'update_primary_lead_stage':
				case 'update_hetro_lead_stage':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					if(!empty($post_details['comp_mst_id'])){

						$update_data['lead_stage'] = $post_details['lead_stage'];
						$update_data['modified_on'] = date('Y-m-d H:i:s');
						$update_data['modified_by'] = $this->session->userdata('user_id');

						$this->lead_management_model->update_data('customer_mst', $update_data, array('id' =>$post_details['comp_mst_id']));
					}
				break;

				case 'primary_update_rating_model':
				case 'hetro_update_rating_model':
				
					// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;
					$response['update_rating_html'] = "";
					$response['comp_mst_id'] = $post_details['comp_mst_id'];
						
					if(!empty($post_details['comp_mst_id'])){
							
						$data['comp_mst_id'] = $post_details['comp_mst_id'];
						$data['lead_priority'] = $post_details['lead_priority'];
						$data['priority_reason'] = $post_details['priority_reason'];
							
						// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
						$response['update_rating_html'] = $this->load->view('lead_management/update_rating_model_body', $data, true);
						$response['comp_mst_id'] = $post_details['comp_mst_id'];
					}				
				break;

				case 'update_rating_model':

					// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;
					if(!empty($post_details['comp_mst_id'])){
						$form_data = array();
						$form_data = array_column($post_details['update_rating_data'], 'value', 'name');
						
						$form_data['modified_on'] = date('Y-m-d H:i:s');
						$form_data['modified_by'] = $this->session->userdata('user_id');
						
						// echo "<pre>";print_r($form_data['lead_priority']);echo"</pre><hr>";exit;
						$this->lead_management_model->update_data('customer_mst', $form_data, array('id' =>$post_details['comp_mst_id']));
					}	
				break;

				case 'get_primary_member_body':
				case 'get_hetro_member_body':

					// echo "<pre>"; print_r($this->input->post()); "</pre>";exit;
					if(!empty($this->input->post('next_count_number'))){
						
						$data['next_count_number'] = $this->input->post('next_count_number');
						$data['connected_on'] = '-';
						$data['connect_mode'] = '-';
						$response['member_body_detail']	= $this->load->view('lead_management/member_body', $data, true);
						// echo "<pre>"; print_r($response); "</pre>";exit;
					}
				break;

				case 'get_primary_other_member_body':
				case 'get_hetro_other_member_body':

					// echo "<pre>"; print_r($this->input->post()); "</pre>";exit;
					if(!empty($this->input->post('next_number'))){
						
						$data['next_number'] = $this->input->post('next_number');
						$data['connected_on'] = '-';
						$data['connect_mode'] = '-';
						// echo "<pre>"; print_r($data); "</pre>";exit;
						$response['other_member_body_detail']	= $this->load->view('lead_management/other_member_body', $data, true);
					}
				break;				

				case 'primary_member_followup_modal':
				case 'primary_other_member_followup_modal':
				case 'hetro_member_followup_modal':
				case 'hetro_other_member_followup_modal':
					
					// echo "<pre>";print_r($post_details);echo"</pre><hr>";exit;
					$response['follow_up_html'] = "";
					$response['comp_dtl_id'] = $post_details['comp_dtl_id'];
					if(!empty($post_details['comp_dtl_id'])){
							
						$data['connected_on'] = date('d-m-Y');
						$data['comp_mst_id'] = $post_details['comp_mst_id'];
						$data['category_id'] = $post_details['category_id'];
						$data['country_id'] = $post_details['country_id'];
							
						//getting follow up history
						$data['follow_up_details'] = $this->lead_management_model->get_all_conditional_data_sales_db('*',array('comp_detail_id'=>$post_details['comp_dtl_id'], 'comp_mst_id'=>$post_details['comp_mst_id'], ),'customer_connect','result_array',array(), array('column_name'=>'connected_on', 'column_value'=>'desc'), array(), 0,0);

						$user_list = array_column($this->lead_management_model->get_all_data('user_id, name', array(), 'users'), 'name', 'user_id');
						$user_list['']='';
						$user_list[0]='';

						foreach ($data['follow_up_details'] as $follow_up_details_key => $single_follow_up) {

							$data['follow_up_details'][$follow_up_details_key]['user_name'] = $user_list[$single_follow_up['entered_by']]; 
						}
												
						// echo "<pre>";print_r($data);echo"</pre><hr>";
						$response['follow_up_html'] = $this->load->view('lead_management/follow_up_model_body', $data, true);
						$response['comp_dtl_id'] = $post_details['comp_dtl_id'];
					}
				break;

				case 'save_primary_followup':
				case 'save_hetro_followup':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					if(!empty($post_details['comp_dtl_id'])){

						$form_data = array_column($post_details['add_followup_data_form'], 'value', 'name');
						$comp_dtl_id = $post_details['comp_dtl_id'];
						
						$insert_array = array(
												'comp_detail_id' => $comp_dtl_id,
												'comp_mst_id' => $form_data['comp_mst_id'],
												'connect_mode' => $form_data['connect_mode'],
												'email_sent' => $form_data['email'],
												'comments' => $form_data['comments'],
												'connected_on' => $form_data['connected_on'],
												'entered_on' => date('Y-m-d H:i:s'),
												'entered_by' => $this->session->userdata('user_id'),
						); 
						// echo "<pre>";print_r($insert_array);echo"</pre><hr>";
						if(!empty($insert_array)){

							$this->lead_management_model->insert_data('customer_connect', $insert_array);
						}


						$insert_array = array();

						$insert_array['member_id'] = $comp_dtl_id;
						$insert_array['member_name'] = $this->common_model->get_dynamic_data_sales_db('member_name', array('comp_dtl_id' => $comp_dtl_id), 'customer_dtl','row_array')['member_name'];
						$insert_array['contact_date'] = date('Y-m-d', strtotime($form_data['connected_on'])).' '.date('H:i:s');
						$insert_array['contact_mode'] = $form_data['connect_mode'];
						$insert_array['comments'] = $form_data['comments'];
						$insert_array['email_sent'] = $form_data['email'];
						$insert_array['user_id'] = $this->session->userdata('user_id');
						$insert_array['client_id'] = $form_data['comp_mst_id'];
						$insert_array['country'] = $this->common_model->get_dynamic_data_sales_db('name', array('id' => $form_data['country_id']), 'country_mst','row_array')['name'];
						$insert_array['lead_name'] =  $this->common_model->get_dynamic_data_sales_db('product_name', array('id' => $form_data['category_id']), 'product_category','row_array')['product_name'];
						$insert_array['client_name'] = $this->common_model->get_dynamic_data_sales_db('name', array('id' => $form_data['comp_mst_id']), 'customer_mst','row_array')['name'];
						$insert_array['date_indian_timezone'] = $this->get_indian_time();
						if(!empty($insert_array)){

							$this->common_model->insert_data_sales_db('daily_work_sales_on_lead_data', $insert_array);
						}

						$response['comp_mst_id'] = $form_data['comp_mst_id'];
						$response['country_id'] = $form_data['country_id'];
						$response['category_id'] = $form_data['category_id'];
					}
				break;

				case 'delete_member':
				case 'delete_other_member':	
					// echo "<pre>";print_r($post_details['comp_dtl_id']);echo"</pre><hr>";exit;
					if(!empty($post_details['comp_dtl_id'])){

						$this->lead_management_model->delete_Data('customer_dtl', array('comp_mst_id' => $post_details['comp_mst_id'], 'comp_dtl_id'=> $post_details['comp_dtl_id']));
					}
				break;
					
				case 'update_primary_lead_data':
				case 'update_hetro_lead_data':
							
					// echo "<pre>";print_r($this->input->post('client_member_details'));echo"</pre><hr>";exit;
	
					$response['message'] = "client Data are Updated";					
					$update_array = array();
					$update_array = array_column($this->input->post('client_details'), 'value', 'name');
					$update_member_array = array_column($this->input->post('client_member_details'), 'value', 'name');
					$update_other_member_array = array_column($this->input->post('client_other_member_details'), 'value', 'name');
					
					$update_array['modified_on'] = date('Y-m-d H:i:s');
					$update_array['modified_by'] = $this->session->userdata('user_id');
					$comp_mst_id = $this->input->post('comp_mst_id');
					
					$this->lead_management_model->update_data('customer_mst', $update_array, array('id'=>$comp_mst_id));
					$this->lead_management_model->delete_Data('customer_dtl', array('comp_mst_id'=>$comp_mst_id));
					
					// echo "<pre>";print_r($this->input->post('client_member_details'));echo"</pre><hr>";exit;
					for($i=1; $i <= (count($this->input->post('client_member_details'))/10); $i++){
						
						$member_array[] = array(
							
							'comp_mst_id' => $comp_mst_id,
							'comp_dtl_id' => $update_member_array['comp_dtl_id_'.$i],
							'member_name'=>$update_member_array['member_name_'.$i],
							'designation'=>$update_member_array['designation_'.$i],
							'email'=>$update_member_array['email_'.$i],
							'mobile'=>$update_member_array['mobile_'.$i],
							'is_whatsapp'=>$update_member_array['is_whatsapp_'.$i],
							'skype'=>$update_member_array['skype_'.$i],
							'telephone'=>$update_member_array['telephone_'.$i],
							'main_buyer'=>$update_member_array['main_buyer_'.$i],
							'decision_maker'=>$update_member_array['decision_maker_'.$i],
							'other_member' => $update_member_array['other_member'.$i]='No',
						);
						
					}
					// echo "<pre>";print_r($member_array);echo"</pre><hr>";exit;
					if (!empty($member_array)) {
						$this->lead_management_model->insert_data('customer_dtl', $member_array, 'batch');
						// echo "<pre>";print_r($member_array);echo"</pre><hr>";exit;
					}

					// update other member detail
					for ($i=1; $i <= (count($this->input->post('client_other_member_details'))/8); $i++) {
						
						$other_member_array[] = array(
							
							'comp_mst_id' => $comp_mst_id,
							'comp_dtl_id' => $update_other_member_array['comp_dtl_id_'.$i],
							'member_name' => $update_other_member_array['member_name_'.$i],
							'designation' => $update_other_member_array['designation_'.$i],
							'email' => $update_other_member_array['email_'.$i],
							'mobile' => $update_other_member_array['mobile_'.$i],
							'is_whatsapp' => $update_other_member_array['is_whatsapp_'.$i],
							'skype' => $update_other_member_array['skype_'.$i],
							'telephone' => $update_other_member_array['telephone_'.$i],
							'other_member' => $update_other_member_array['other_member'.$i]='Yes',
						);						
					}
					if (!empty($other_member_array)) {
						$this->lead_management_model->insert_data('customer_dtl', $other_member_array, 'batch');
						// echo "<pre>";print_r($other_member_array);echo"</pre><hr>";exit;
					}
				break;

				// ADD NEW LEAD
				case 'save_lead_details':

					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					$response['message'] = "client Data are Updated";	

					$update_array = array();
					$update_array = array_column($this->input->post('client_mst_details'), 'value', 'name');
					$update_data_array = array_column($this->input->post('client_data_details'), 'value', 'name');
					$update_member_array = array_column($this->input->post('client_member_details'), 'value', 'name');
					$update_other_member_array = array_column($this->input->post('client_other_member_details'), 'value', 'name');

					if(empty($this->input->post('comp_mst_id'))){
						
						$update_array['entered_on'] = date('Y-m-d H:i:s');
						$update_array['entered_by'] = $this->session->userdata('user_id');

						$comp_mst_id = $this->lead_management_model->insert_data('customer_mst', $update_array);

						$update_data_array['customer_mst_id'] = $comp_mst_id;
						$this->lead_management_model->insert_data('customer_data', $update_data_array);

					}else{

						$comp_mst_id = $this->input->post('comp_mst_id');
						// echo "<pre>";print_r($comp_mst_id);echo"</pre><hr>";exit;
						$update_array['modified_on'] = date('Y-m-d H:i:s');
						$update_array['modified_by'] = $this->session->userdata('user_id');

						$this->lead_management_model->update_data('customer_mst', $update_array, array('id'=>$comp_mst_id));
						
						$update_data_array['customer_mst_id'] = $comp_mst_id;
						$this->lead_management_model->insert_data('customer_data', $update_data_array);

					}
					// echo "<pre>";print_r($this->input->post('client_member_details'));echo"</pre><hr>";exit;
					for($i=1; $i <= (count($this->input->post('client_member_details'))/10); $i++){
						
						$member_array[] = array(
							
							'comp_mst_id' => $comp_mst_id,
							'comp_dtl_id' => $update_member_array['comp_dtl_id_'.$i],
							'member_name'=>$update_member_array['member_name_'.$i],
							'designation'=>$update_member_array['designation_'.$i],
							'email'=>$update_member_array['email_'.$i],
							'mobile'=>$update_member_array['mobile_'.$i],
							'is_whatsapp'=>$update_member_array['is_whatsapp_'.$i],
							'skype'=>$update_member_array['skype_'.$i],
							'telephone'=>$update_member_array['telephone_'.$i],
							'main_buyer'=>$update_member_array['main_buyer_'.$i],
							'decision_maker'=>$update_member_array['decision_maker_'.$i],
							'other_member' => $update_member_array['other_member'.$i]='No',
						);
						
					}
					// echo "<pre>";print_r($member_array);echo"</pre><hr>";exit;
					if (!empty($member_array)) {
						$this->lead_management_model->insert_data('customer_dtl', $member_array, 'batch');
						// echo "<pre>";print_r($member_array);echo"</pre><hr>";exit;
					}

					// update other member detail
					for ($i=1; $i <= (count($this->input->post('client_other_member_details'))/8); $i++) {
						
						$other_member_array[] = array(
							
							'comp_mst_id' => $comp_mst_id,
							'comp_dtl_id' => $update_other_member_array['comp_dtl_id_'.$i],
							'member_name' => $update_other_member_array['member_name_'.$i],
							'designation' => $update_other_member_array['designation_'.$i],
							'email' => $update_other_member_array['email_'.$i],
							'mobile' => $update_other_member_array['mobile_'.$i],
							'is_whatsapp' => $update_other_member_array['is_whatsapp_'.$i],
							'skype' => $update_other_member_array['skype_'.$i],
							'telephone' => $update_other_member_array['telephone_'.$i],
							'other_member' => $update_other_member_array['other_member'.$i]='Yes',
						);						
					}
					if (!empty($other_member_array)) {
						$this->lead_management_model->insert_data('customer_dtl', $other_member_array, 'batch');
						// echo "<pre>";print_r($other_member_array);echo"</pre><hr>";exit;
					}
				break;

				case 'get_member_body':

					// echo "<pre>"; print_r($this->input->post()); "</pre>";exit;
					if(!empty($this->input->post('next_count_number'))){
						
						$data['next_count_number'] = $this->input->post('next_count_number');
						$data['connected_on'] = '-';
						$data['connect_mode'] = '-';
						$response['member_body_detail']	= $this->load->view('lead_management/member_body', $data, true);
						// echo "<pre>"; print_r($response); "</pre>";exit;
					}
				break;

				case 'get_other_member_body':

					// echo "<pre>"; print_r($this->input->post()); "</pre>";exit;
					if(!empty($this->input->post('next_number'))){
						
						$data['next_number'] = $this->input->post('next_number');
						$data['connected_on'] = '-';
						$data['connect_mode'] = '-';
						// echo "<pre>"; print_r($data); "</pre>";exit;
						$response['other_member_body_detail']	= $this->load->view('lead_management/other_member_body', $data, true);
					}
				break;

				//// DESAI CODE START////////
				case 'lead_tab_data':

					if(!empty($this->input->post('product_name'))){

						$data = $this->prepare_all_data($this->input->post());

						$lead_count = $this->update_lead_tab_counts($this->input->post('search_form_data'));

						// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
						$response['table_head'] = $this->load->view('lead_management/index_table_header', $data, true);
						$response['table_body'] = $this->load->view('lead_management/index_table_body', $data, true);
						$response['search_filter'] = $this->load->view('lead_management/search_filter_form', $data, true);
						$response['paggination'] = $this->load->view('lead_management/paggination', $data, true);
						$response['lead_tab_wise_count'] = $lead_count;
					}
				break;

				case 'member_details':
					
					if(!empty($post_details['comp_mst_id'])){
							
						$data['buyer_member'] = $this->lead_management_model->get_all_conditional_data_sales_db('*',array('comp_mst_id'=>$post_details['comp_mst_id'], 'other_member !='=>'Yes', 'status'=>'Active'),'customer_dtl');

						$data['other_member'] = $this->lead_management_model->get_all_conditional_data_sales_db('*',array('comp_mst_id'=>$post_details['comp_mst_id'], 'other_member'=>'Yes', 'status'=>'Active'),'customer_dtl');
						$data['country_name'] = $post_details['country_name'];
						// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
						$response['member_count_model_body'] = $this->load->view('lead_management/member_count_model_body', $data, true);
					}
				break;

				case 'get_box_sample_comment_details':

					// echo "<pre>";print_r($post_details['comp_mst_id']);echo"</pre><hr>";exit;
					if(!empty($post_details['comp_mst_id'])){
							
						$data['comment_detail'] = $this->lead_management_model->get_all_conditional_data_sales_db('*',array('id'=>$post_details['comp_mst_id']),'customer_mst','row_array');
							
						// echo "<pre>";print_r($data['comment_detail']);echo"</pre><hr>";exit;
						$response['box_sample_comment_model_body'] = $this->load->view('lead_management/box_sample_comment_model_body', $data, true);
					}
				break;
				
				case 'update_lead_stage':
					
					if(!empty($post_details['comp_mst_id'])){

						$update_data['lead_stage'] = $post_details['lead_stage'];
						$update_data['modified_on'] = date('Y-m-d H:i:s');
						$update_data['modified_by'] = $this->session->userdata('user_id');

						$this->lead_management_model->update_data('customer_mst', $update_data, array('id' =>$post_details['comp_mst_id']));
					}
				break;

				case 'save_contact_info_with_member':
					
					// echo "<pre>";print_r($post_details['form_data']);echo"</pre><hr>";die('debug');
					$form_data = array_column($post_details['form_data'], 'value', 'name');
					$insert_array = array();				
					$insert_array = array(
										'comp_mst_id' => $form_data['comp_mst_id'],
										'comp_detail_id' => $form_data['comp_detail_id'],
										'comments' => $form_data['contact_details'],
										'connect_mode' => $form_data['connect_mode'],
										'email_sent' => $form_data['email_sent'],
										'connected_on' => date('Y-m-d', strtotime($form_data['contact_date'])),
										'entered_by' => $this->session->userdata('user_id'),
										'added_on' => date('Y-m-d H:i:s')
					);
					// echo "<pre>";print_r($insert_array);echo"</pre><hr>";die;
					if(!empty($insert_array)){

						$this->lead_management_model->insert_data('customer_connect', $insert_array);
					}

					$insert_array = array();

					$insert_array['member_id'] = $form_data['comp_detail_id'];
					$insert_array['member_name'] = $form_data['member_name'];
					$insert_array['contact_date'] = date('Y-m-d', strtotime($form_data['contact_date'])).' '.date('H:i:s');
					$insert_array['contact_mode'] = $form_data['connect_mode'];
					$insert_array['comments'] = $form_data['contact_details'];
					$insert_array['email_sent'] = $form_data['email_sent'];
					$insert_array['user_id'] = $this->session->userdata('user_id');
					$insert_array['client_id'] = $form_data['comp_mst_id'];
					$insert_array['country'] = $form_data['country_name'];
					$insert_array['lead_name'] = $form_data['lead_name'];
					$insert_array['client_name'] = $this->common_model->get_dynamic_data_sales_db('name', array('id' => 	$form_data['comp_mst_id']), 'customer_mst','row_array')['name'];
					$insert_array['date_indian_timezone'] = $this->get_indian_time();
					if(!empty($insert_array)){

						$this->common_model->insert_data_sales_db('daily_work_sales_on_lead_data', $insert_array);
					}
					// echo "<pre>";print_r($insert_array);echo"</pre><hr>";die('debug');
					
				break;

				case 'get_member_contact_history':
					
					$data = array();
					$data['member_history'] = $this->lead_management_model->get_all_conditional_data_sales_db('*',array('comp_detail_id'=>$post_details['comp_detail_id']),'customer_connect', 'result_array', array(), array('column_name'=>'connected_on', 'column_value'=>'desc'));
					$data['member_list'] = array_column($this->common_model->get_dynamic_data_sales_db('comp_dtl_id, member_name', array('comp_mst_id'=> $post_details['comp_mst_id']), 'customer_dtl'), 'member_name', 'comp_dtl_id');
					$response['tbody'] = $this->load->view('lead_management/member_history_body', $data, true);
				break;

				case 'get_last_10_contact_history':

					$data = array();
					$data['member_history'] = $this->lead_management_model->get_all_conditional_data_sales_db('*',array('comp_mst_id'=>$post_details['comp_mst_id']),'customer_connect', 'result_array', array(), array('column_name'=>'connected_on', 'column_value'=>'desc'), array(), 10);
					$data['member_list'] = array_column($this->common_model->get_dynamic_data_sales_db('comp_dtl_id, member_name', array('comp_mst_id'=> $post_details['comp_mst_id']), 'customer_dtl'), 'member_name', 'comp_dtl_id');
					$response['tbody'] = $this->load->view('lead_management/member_history_body', $data, true);
				break;

				//// DESAI CODE END////////
				case 'get_graph_details':
					
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;

					###############################################################################
					######################     DEFAULT DATA    ####################################
					###############################################################################
					// $response['quotation_vs_proforma'] = array();
					###############################################################################
					$response['export_stats_yearly'] = array();
					###############################################################################
					$response['export_stats_yearly_internal'] = array();
					###############################################################################
					$response['import_stats_data'] = array();
					###############################################################################
					$response['production_status_wise'] = array();
					###############################################################################
					$response['exporters_data'] = array();
					###############################################################################
					$response['decision_maker_data'] = array();
					###############################################################################
					$response['additional_information'] = "";
					###############################################################################
					$response['company_name'] = "";
					###############################################################################
					###############################################################################
					###############################################################################
					if(!empty($this->input->post('company_name')) && !empty($this->input->post('product_category')) && !empty($this->input->post('company_id'))){
						
						$company_name = $this->input->post('company_name');
						$product_category = $this->input->post('product_category');
						$company_id = $this->input->post('company_id');
						$response['company_name'] = $company_name;
						#################  QUOTATION VS PROFORMA  #################################
							
							// $quotation_list_details = $this->lead_management_model->get_quotation_vs_proforma_highchart_list($company_id);
							// // echo "<pre>";print_r($quotation_list_details);echo"</pre><hr>";exit;
							// if(!empty($quotation_list_details)){

							// 	$response['quotation_vs_proforma']['series'][0] = array();
							// 	$response['quotation_vs_proforma']['series'][0]['name'] = 'WON';
							// 	$response['quotation_vs_proforma']['series'][0]['color'] = '#28a745';
							// 	$response['quotation_vs_proforma']['series'][1] = array();
							// 	$response['quotation_vs_proforma']['series'][1]['name'] = 'OPEN';
							// 	$response['quotation_vs_proforma']['series'][1]['color'] = '#007bff';
							// 	$response['quotation_vs_proforma']['series'][2] = array();
							// 	$response['quotation_vs_proforma']['series'][2]['name'] = 'CLOSED';
							// 	$response['quotation_vs_proforma']['series'][2]['color'] = '#dc3545';
							// 	$response['quotation_vs_proforma_highchart_category'] = array();
							// 	foreach ($quotation_list_details as $key => $quotation_details) {

							// 		$response['quotation_vs_proforma']['category'][] = $quotation_details['month'];
							// 		$response['quotation_vs_proforma']['series'][0]['data'][] = (int)$quotation_details['won'];
							// 		$response['quotation_vs_proforma']['series'][1]['data'][] = (int)$quotation_details['open'];
							// 		$response['quotation_vs_proforma']['series'][2]['data'][] = (int)$quotation_details['closed'];
							// 	}
							// }

						##########################  END  ##########################################
						#################  EXPORT STATS YEARLY  #################################

							if(in_array($product_category, array('PIPES', 'TUBES', 'TUBING', 'HAMMER UNION', 'internal'))){
								
								$response['export_stats_yearly']['category'] = array();
								$response['export_stats_yearly']['series'] = array();
								$response['export_stats_yearly']['total'] = 0;
								$export_stats_yearly_select = "SUM(fob) as value, '' as exporter_name, year";
								$export_stats_yearly_where = "NEW_IMPORTER_NAME LIKE '%".strtoupper($company_name)."%'";
								if(in_array($product_category, array('PIPES', 'TUBES', 'TUBING', 'HAMMER UNION'))){

									$export_stats_yearly_where .= " AND type = '".strtolower(str_replace(" ", "_", $product_category))."'";
								}
								$export_stats_yearly_group_by = array('year');

								if(in_array($this->session->userdata('role'), array(1))){

									$export_stats_yearly_select = 'SUM(fob) as value, exporter_name, year';
									$export_stats_yearly_group_by = array('exporter_name', 'year');
								}
								$export_stats_yearly_data = $this->common_model->get_all_conditional_data_marketing_db($export_stats_yearly_select, $export_stats_yearly_where, 'product_details', 'result_array', array(), array('column_name'=> 'year', 'column_value'=> 'ASC'), $export_stats_yearly_group_by);

								$series_name = array();
								foreach ($export_stats_yearly_data as $single_details) {
									
									if(!in_array($single_details['year'], $response['export_stats_yearly']['category'])){

										$response['export_stats_yearly']['category'][] = $single_details['year'];
									}
									if(!in_array($single_details['exporter_name'], $series_name)){
									
										$series_name[] = $single_details['exporter_name'];
									}
									$response['export_stats_yearly']['total'] += $single_details['value'];
								}
								$static_year_value = array();
								foreach ($response['export_stats_yearly']['category'] as $year_key => $year_name) {
									
									$static_year_value[] = 0; 
								}
								foreach ($export_stats_yearly_data as $single_details) {

									$response['export_stats_yearly']['series'][array_search($single_details['exporter_name'], $series_name)]['name'] = $single_details['exporter_name'];
									$response['export_stats_yearly']['series'][array_search($single_details['exporter_name'], $series_name)]['data'] = $static_year_value;
								}
								// echo "<pre>";print_r($response['export_stats_yearly']);echo"</pre><hr>";
								foreach ($export_stats_yearly_data as $single_details) {


									$response['export_stats_yearly']['series'][array_search($single_details['exporter_name'], $series_name)]['data'][array_search($single_details['year'], $response['export_stats_yearly']['category'])] = (int) $single_details['value'];
								}
								// echo "<pre>";print_r($response['export_stats_yearly']);echo"</pre><hr>";exit;
								$response['export_stats_yearly']['total'] = $this->convert_number_into_indian_currency($response['export_stats_yearly']['total']);
							}
						
						##########################  END  ##########################################
						###############  EXPORT STATS YEARLY INTERNAL  ############################	

							$response['export_stats_yearly_internal'] = $this->export_stats_yearly_internal($company_name);

						##########################  END  ##########################################
						#######################  IMPORT STATS  ####################################	

							$import_stats_data_select = "description as name, fob";
							$import_stats_data_where = "importer_name LIKE '%".strtoupper($company_name)."%'";
							if(in_array($product_category, array('internal'))){

								$import_stats_data_select = "product as name, fob";
								$import_stats_data_where = " AND type = '".strtolower(str_replace(" ", "_", $product_category))."'";
							}

							$import_stats_data = $this->common_model->get_all_conditional_data_marketing_db($import_stats_data_select, $import_stats_data_where, 'import_details', 'result_array', array(), array('column_name'=>'fob', 'column_value'=>'desc'));
							// echo "<pre>";print_r($import_stats_data);echo"</pre><hr>";exit;
							if(!empty($import_stats_data)){
								
								foreach ($import_stats_data as $import_stats_data_key => $import_stats_data_value) {
									if(round($import_stats_data_value['fob']) > 0){
										
										$response['import_stats_data'][$import_stats_data_key]['name'] = $import_stats_data_value['name'];
										$response['import_stats_data'][$import_stats_data_key]['y'] = round($import_stats_data_value['fob']);
									}
								}
							}

						##########################  END  ##########################################
						###############  Production Status Wise Graph  ############################	
							if(in_array($this->session->userdata('role'), array(1))){
								$response['production_status_wise'] = $this->get_order_wise_total_new($company_id);
								$response['production_status_wise']['category'] = array($company_name);
							}

						##########################  END  ##########################################
						######################  EXPORTER DATA  ####################################
							if(in_array($this->session->userdata('role'), array(1))){

								if(in_array($product_category, array('PIPES', 'TUBES', 'TUBING', 'HAMMER UNION', 'internal'))){
									
									$exporter_data_select = "exporter_name as name, exporter_fob as value";
									$exporter_data_where = "importer_name LIKE '%".strtoupper($company_name)."%'";
									if(in_array($product_category, array('PIPES', 'TUBES', 'TUBING', 'HAMMER UNION'))){

										$exporter_data_where .= "AND type = '".strtolower(str_replace(" ", "_", $product_category))."'";
									}
									$exporter_data = $this->common_model->get_dynamic_data_marketing_db($exporter_data_select, $exporter_data_where, 'competitor_ranks_details');
									
									if(!empty($exporter_data)){
										
										// echo $this->common_model->db2->last_query(),"</hr>";
										// echo "<pre>";print_r($exporter_data);echo"</pre><hr>";exit;
										foreach ($exporter_data as $ex_key => $ex_value) {
											
											$response['exporters_data'][$ex_key]['name'] = $ex_value['name'];
											$response['exporters_data'][$ex_key]['y'] = (int)$ex_value['value'];
										}
									}
								}
							}

						##########################  END  ##########################################
						######################  DECISION MAKER  ###################################	
							$decision_maker = $this->common_model->get_all_conditional_data_sales_db("member_name, decision_maker", array('comp_mst_id'=> $company_id, 'decision_maker > '=> 0), 'customer_dtl', 'result_array', array(), array('column_name'=>'decision_maker', 'column_value'=>'desc'));
							if(!empty($decision_maker)){

								foreach ($decision_maker as $dmkey => $dmvalue) {

									$response['decision_maker_data'][$dmkey]['name'] = $dmvalue['member_name'];
									$response['decision_maker_data'][$dmkey]['y'] = round($dmvalue['decision_maker']);
								}
							}

						##########################  END  ##########################################
						########################  LEAD TABLE  #####################################	
							$client_details = $this->common_model->get_dynamic_data_sales_db('*', array('id'=>$company_id), 'customer_mst', 'row_array');
							$response['additional_information'] = 
								"
													<table class='table table-bordered'>
														<tr>
															<th>Purchase Decision Factors</th>
															<td><b>Rank 1</b> - ".$client_details['purchase_factor_1']."</td>
															<td><b>Rank 2</b> - ".$client_details['purchase_factor_2']."</td>
															<td><b>Rank 3</b> - ".$client_details['purchase_factor_3']."</td>
														</tr>
														<tr>
															<th>No. of employees</th>
															<td>".$client_details['no_of_employees']."</td>
															<th>Last Buy</th>
															<td>".$client_details['last_purchased']."</td>
														</tr>
														<tr>
															<th>Specific Product to Pitch</th>
															<td>".$client_details['product_pitch']."</td>
															<th>Margins</th>
															<td>".$client_details['margins']."</td>
														</tr>
														<tr>
															<th>Sales Notes</th>
															<td colspan='3'>".$client_details['sales_notes']."</td>
														</tr>
														<tr>
															<th>Purchase Comments</th>
															<td colspan='3'>".$client_details['purchase_comments']."</td>
														</tr>
													</table>
								";

						##########################  END  ##########################################
					}
					// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
				break;

				case 'get_graph_details_bckp':

					$response['hs_buy_graph_data'] = $response['exporters_data'] = array();
					$response['decision_maker_data'] = $response['additional_information'] = array();
					$response['export_stats_yearly']['category'] = array();
					$response['export_stats_yearly']['series'] = array();
					$response['export_stats_yearly']['total'] = 0;
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					$company_name = $this->input->post('company_name');
					$product_category = $this->input->post('product_category');
					$company_id = $this->input->post('company_id');
					$response['company_name'] = $company_name;
					if(!empty($company_name) && !empty($product_category) && !empty($company_id)){
						
						if(in_array($product_category, array('PIPES', 'TUBES', 'TUBING', 'HAMMER UNION'))){
						
							//////HS BUY DATA
							$hs_buy_data = $this->common_model->get_all_conditional_data_marketing_db('*',"NEW_IMPORTER_NAME LIKE '%".strtoupper($company_name)."%' AND product ='".$product_category."'",'hs_buy_percent', 'result_array', array(), array('column_name'=>'FOB_VALUE_INR', 'column_value'=>'desc'));
							if(!empty($hs_buy_data)){
								
								$colors = array('#660033', '#CC6633', '#CCFFFF', '#996600', '#DCEDC8', '#CFD8DC', '#E91E63');
								foreach ($hs_buy_data as $hs_key => $hs_value) {
									if(round($hs_value['FOB_VALUE_INR']) > 0){
										$bg_color = '';
										if(strtolower(trim($hs_value['hs_desc'])) == 'smls pipe/tube in ss/cs/as'){
											$bg_color = '#00FF00';
										}else if(strtolower(trim($hs_value['hs_desc'])) == 'welded pipe/tube in ss/cs/as'){
											$bg_color = '#FF7F00';
										}else if(strtolower(trim($hs_value['hs_desc'])) == 'fittings flange ss/cs/as'){
											$bg_color = '#FF0000';
										}else if(strtolower(trim($hs_value['hs_desc'])) == 'fasteners'){
											$bg_color = '#0000FF';
										}else if(strtolower(trim($hs_value['hs_desc'])) == 'nickel alloy pipe/tube/fittings'){
											$bg_color = '#4B0082';
										}else{
											$col_key = $hs_key;
											if($hs_key > 6){
												$col_key = $hs_key - $col_key;
											}
											$bg_color = $colors[$col_key];
										}
				
										$response['hs_buy_graph_data'][$hs_key]['name'] = $hs_value['hs_desc'];
										$response['hs_buy_graph_data'][$hs_key]['y'] = round($hs_value['FOB_VALUE_INR']);
									}
								}
							}
						}else if(in_array($product_category, array('internal'))){
							
							//////HS BUY DATA INTERNAL////////////////
							$hs_buy_data = $this->common_model->get_all_conditional_data_sales_db('*',"NEW_IMPORTER_NAME LIKE '%".strtoupper($company_name)."%'",'hs_buy_percent_invoice', 'result_array', array(), array('column_name'=>'FOB_VALUE_INR', 'column_value'=>'desc'));
				
							if(!empty($hs_buy_data)){
							
								foreach ($hs_buy_data as $hs_key => $hs_value) {
									if(round($hs_value['FOB_VALUE_INR']) > 0){
										
										$response['hs_buy_graph_data'][$hs_key]['name'] = $hs_value['PRODUCT_NAME'];
										$response['hs_buy_graph_data'][$hs_key]['y'] = round($hs_value['FOB_VALUE_INR']);
									}
								}
							}
						}
				
						//////////EXPORTER DATA/////////////////
						if($this->session->userdata('role') == 1){
							if(in_array($product_category, array('PIPES', 'TUBES', 'TUBING', 'HAMMER UNION'))){
				
								$table_name = "competitor_ranks_pipe";
								switch ($product_category) {
									case 'TUBES':
										
										$table_name = "competitor_ranks_tube";
									break;
									case 'TUBING':
										
										$table_name = "competitor_ranks_tubing";
									break;
									case 'HAMMER UNION':
										
										$table_name = "competitor_ranks_hammer_union";
									break;
									
									default:
									break;
								}
								$export_data = $this->common_model->get_dynamic_data_marketing_db('*',"NEW_IMPORTER_NAME LIKE '%".strtoupper($company_name)."%'", $table_name);
								if(!empty($export_data)){
								
									foreach ($export_data as $ex_key => $ex_value) {
										
										$response['exporters_data'][$ex_key]['name'] = $ex_value['EXPORTER_NAME'];
										$response['exporters_data'][$ex_key]['y'] = round($ex_value['EXPORTER_CONTRIBUTION'], 2);
									
									}
								}
							}
						}
				
						//////////DECISION MAKER////////////////
						$decision_maker = $this->common_model->get_all_conditional_data_sales_db("member_name, decision_maker", array('comp_mst_id'=> $company_id, 'decision_maker > '=> 0), 'customer_dtl', 'result_array', array(), array('column_name'=>'decision_maker', 'column_value'=>'desc'));
						if(!empty($decision_maker)){
				
							foreach ($decision_maker as $dmkey => $dmvalue) {
				
								$response['decision_maker_data'][$dmkey]['name'] = $dmvalue['member_name'];
								$response['decision_maker_data'][$dmkey]['y'] = round($dmvalue['decision_maker']);
							}
						}
				
						///////////LEAD TABLE/////////////////
						$client_details = $this->common_model->get_dynamic_data_sales_db('*', array('id'=>$company_id), 'customer_mst', 'row_array');
						$response['additional_information'] = 
							"
												<h4>Additional Information</h4>
												<table class='table table-bordered'>
													<tr>
														<th>Purchase Decision Factors</th>
														<td><b>Rank 1</b> - ".$client_details['purchase_factor_1']."</td>
														<td><b>Rank 2</b> - ".$client_details['purchase_factor_2']."</td>
														<td><b>Rank 3</b> - ".$client_details['purchase_factor_3']."</td>
													</tr>
													<tr>
														<th>No. of employees</th>
														<td>".$client_details['no_of_employees']."</td>
														<th>Last Buy</th>
														<td>".$client_details['last_purchased']."</td>
													</tr>
													<tr>
														<th>Specific Product to Pitch</th>
														<td>".$client_details['product_pitch']."</td>
														<th>Margins</th>
														<td>".$client_details['margins']."</td>
													</tr>
													<tr>
														<th>Sales Notes</th>
														<td colspan='3'>".$client_details['sales_notes']."</td>
													</tr>
													<tr>
														<th>Purchase Comments</th>
														<td colspan='3'>".$client_details['purchase_comments']."</td>
													</tr>
												</table>
							";
				
						//////////EXPORT STATS YEARLY///////////
						if(in_array($product_category, array('PIPES', 'TUBES', 'TUBING', 'HAMMER UNION'))){
				
							$table_name = "trending_pipes";
							switch ($product_category) {
								case 'TUBES':
								
									$table_name = "trending_tubes";
								break;
								case 'TUBING':
								
									$table_name = "trending_tubing";
								break;
								case 'HAMMER UNION':
								
									$table_name = "trending_hammer_union";
								break;
								default:
								break;
							}
							if(in_array($this->session->userdata('role'), array(1, 5, 16))){
				
								if(in_array($this->session->userdata('role'), array(1))){
				
									$export_stats_yearly_data = $this->common_model->get_all_conditional_data_marketing_db("SUM(SUM_FOB_VALUE_IN) as value, EXPORTER_NAME, SB_YEAR","NEW_IMPORTER_NAME LIKE '%".strtoupper($company_name)."%'", $table_name, 'result_array', array(), array('column_name'=> 'SB_YEAR', 'column_value'=> 'ASC'), array('EXPORTER_NAME', 'SB_YEAR'));
								}else if(in_array($this->session->userdata('role'), array(5, 16))){
				
									$export_stats_yearly_data = $this->common_model->get_all_conditional_data_marketing_db("SUM(SUM_FOB_VALUE_IN) as value, '' as EXPORTER_NAME, SB_YEAR", "NEW_IMPORTER_NAME LIKE '%".strtoupper($company_name)."%'", $table_name, 'result_array', array(), array('column_name'=> 'SB_YEAR', 'column_value'=> 'ASC'), array('SB_YEAR'));
								}
								
								$response['export_stats_yearly']['category'] = array();
								$response['export_stats_yearly']['series'] = array();
								$response['export_stats_yearly']['total'] = 0;
								$series_name = $series_data = array();
								foreach ($export_stats_yearly_data as $single_details) {
									
									if(!in_array($single_details['SB_YEAR'], $response['export_stats_yearly']['category'])){
				
										$response['export_stats_yearly']['category'][] = $single_details['SB_YEAR'];
									}
									if(!in_array($single_details['EXPORTER_NAME'], $series_name)){
									
										$series_name[] = $single_details['EXPORTER_NAME'];
									}
									$response['export_stats_yearly']['total'] += $single_details['value'];
								}
								$static_year_value = array();
								foreach ($response['export_stats_yearly']['category'] as $year_key => $year_name) {
									
									$static_year_value[] = 0; 
								}
								// echo "<pre>";print_r($static_year_value);echo"</pre><hr>";
								$i = 0;
				
								foreach ($export_stats_yearly_data as $single_details) {
				
									// if($i <= 9){
				
										$response['export_stats_yearly']['series'][array_search($single_details['EXPORTER_NAME'], $series_name)]['name'] = $single_details['EXPORTER_NAME'];
										$response['export_stats_yearly']['series'][array_search($single_details['EXPORTER_NAME'], $series_name)]['data'] = $static_year_value;
									// }
									// $i++;
								}
								// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
								$i = 0;
								foreach ($export_stats_yearly_data as $single_details) {
				
									// if($i <= 9){
				
										$response['export_stats_yearly']['series'][array_search($single_details['EXPORTER_NAME'], $series_name)]['data'][array_search($single_details['SB_YEAR'], $response['export_stats_yearly']['category'])] = (int) $single_details['value'];
									// }
									// $i++;
								}
								// echo "<pre>";print_r($response['export_stats_yearly']);echo"</pre><hr>";exit;
							}
							$response['export_stats_yearly']['total'] = $this->convert_number_into_indian_currency($response['export_stats_yearly']['total']);
							$response['export_stats_yearly_internal'] = $this->export_stats_yearly_internal($company_name);
						}else if(in_array($product_category, array('internal'))){
				
							$response['export_stats_yearly_internal'] = $this->export_stats_yearly_internal($company_name);
						}
				
						////////////////Production Status Wise Graph///////////
						$response['production_status_wise'] = $this->get_order_wise_total_new($company_id);
						$response['production_status_wise']['category'] = array($company_name);
					}
				break;

				// client name search filter
				case 'get_client_name_lead':
					
					$search_client_name = $post_details['client_name'];
					$where_string = "status = 'Active' AND name LIKE '%".$search_client_name."%'";
					if($this->session->userdata('role') == 5){

						$where_string .= "AND assigned_to IN (".implode(', ', $this->session->userdata('lead_access')['lead_sales_user_id']).")";
					}
					// echo "<pre>";print_r($where_array);echo"</pre><hr>";exit;
					$client_details = $this->common_model->get_dynamic_data_sales_db_null_false('id, name', $where_string, 'customer_mst');
					$response['client_list_html'] = '<option value="">Select Name</option>';
					foreach($client_details as $single_details){

                        $response['client_list_html'] .= '<option value="'.$single_details['id'].'">'.$single_details['name'].'</option>';
                    }
				break;

				case 'get_member_name_lead':

					$client_id = $post_details['client_id'];
					$client_details = $this->common_model->get_dynamic_data_sales_db_null_false('comp_dtl_id, member_name', "status = 'Active' AND comp_mst_id = ".$client_id."", 'customer_dtl');
					// echo "<pre>";print_r($client_details);echo"</pre><hr>";exit;
					$response['client_member_list_html'] = '<option value="">Select Name</option>';
					foreach($client_details as $single_details){

                        $response['client_member_list_html'] .= '<option value="'.$single_details['comp_dtl_id'].'">'.$single_details['member_name'].'</option>';
                    }
				break;

				case 'get_document_upload_history':

					$client_id = (!empty($this->input->post('client_id'))) ? $this->input->post('client_id') : $this->session->userdata('client_id_for_document_upload');
					if(!empty($client_id)) {

						$documents_details = $this->common_model->get_dynamic_data_sales_db('id, document_file',array('id'=>$client_id), 'customer_mst', 'row_array');

						if(!empty($documents_details)){

							$response['client_document_history'] = $this->load->view('lead_management/document_file', array('document_details'=> $documents_details), true);
						}
						$this->session->set_userdata('client_id_for_document_upload', $client_id);
					}
				break;

				case 'client_document_upload':

					$response['msg'] = 'File is uploaded successfully!!!';
					$return_response['status'] = '';
					$client_id = $this->session->userdata('client_id_for_document_upload');
					if(!empty($client_id)){

						$return_response = $this->upload_doc_config();

					}
					$response['status'] = $return_response['status'];
					if($return_response['status'] == 'successful') {

						$document_path = array();
						$document_details = $this->common_model->get_dynamic_data_sales_db('id, document_file',array('id'=> $client_id,'status'=>'Active'), 'customer_mst', 'row_array');

						if(!empty($document_details)){

							$document_path = json_decode($document_details['document_file'], true);
							$document_path[] = array('file_type' => $return_response['file_type'], 'file_name' => $return_response['file_name'], 'file_add_date' => $return_response['file_add_date']);

							$this->common_model->update_data_sales_db('customer_mst', array('document_file'=> json_encode($document_path)), array('id'=>$client_id));
						}
					}else {

						$response['msg'] = 'File is not uploaded.';
					}
				break;

				case 'client_document_delete':

					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";die;
					if(!empty($this->input->post('id')) && !empty($this->input->post('file_name'))){

						$file_details = $this->common_model->get_dynamic_data_sales_db('document_file', array('id'=> $this->input->post('id')), 'customer_mst', 'row_array');

						if(!empty($file_details)){

							$arrayData = json_decode($file_details['document_file'], true);
							foreach($arrayData as $single_array){

								if($single_array['file_name'] !== $this->input->post('file_name')){

									$filter_array[] = $single_array;
								}
							}

							if(empty($filter_array)){

								$this->common_model->update_data_sales_db('customer_mst', array('document_file' => ''), array('id'=> $this->input->post('id')));
							}else{

								$this->common_model->update_data_sales_db('customer_mst', array('document_file' => json_encode($filter_array)), array('id'=> $this->input->post('id')));
							}
						}
					}
				break;

				case 'change_pq_status':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";die;
					if(!empty($this->input->post('id')) && !empty($this->input->post('pq_status'))){

						if(empty($this->input->post('is_pq'))){
							$update_data['is_pq'] = "Yes";
						}else{
							$update_data['is_pq'] = $this->input->post('is_pq');
						}
						$update_data['pq_client_status'] = $this->input->post('pq_status');
						$update_data['modified_on'] = date('Y-m-d H:i:s');
						$update_data['modified_by'] = $this->session->userdata('user_id');

						$this->common_model->update_data_sales_db('customer_mst', $update_data, array('id'=> $this->input->post('id')));
					}
				break;

				case 'get_pq_details':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";die;
					if(!empty($post_details['comp_mst_id'])){

						$data['pq_details'] = $this->common_model->get_all_conditional_data_sales_db('*', array('comp_mst_id' => $post_details['comp_mst_id'], 'status' => 'Active'),'pq_details','row_array');

						$response['pq_details_model_body'] = $this->load->view('lead_management/pq_details_model_body', $data, true);
					}
				break;

				case 'save_pq_details':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";die;
					$form_data = array_column($this->input->post('form_data'), 'value', 'name');
					if(!empty($post_details['comp_mst_id'])){

						$pq_details = $this->common_model->get_dynamic_data_sales_db('*', array('comp_mst_id' => $post_details['comp_mst_id']), 'pq_details', 'row_array');

						if(!empty($pq_details['comp_mst_id'])){

							$form_data['modified_by'] =  $this->session->userdata('user_id');
							$this->common_model->update_data_sales_db('pq_details', $form_data, array('comp_mst_id'=> $post_details['comp_mst_id']));
						}else{

							$form_data['comp_mst_id'] =  $post_details['comp_mst_id'];
							$form_data['entered_by'] =  $this->session->userdata('user_id');
							$this->common_model->insert_data_sales_db('pq_details', $form_data);
						}
					}
				break;

				case 'add_pq_comment_details':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";die;
					if(!empty($post_details['id'])){

						$comment_details = array();
						$old_comment_details = $this->common_model->get_dynamic_data_sales_db('comp_mst_id, comment', array('id'=> $post_details['id'], 'status'=> 'Active'), 'pq_details', 'row_array');
						if(!empty($old_comment_details)){

							$comment_details = json_decode($old_comment_details['comment'], true);
						}
						$comment_details[] = array(
							'message'=> $post_details['comment'],
							'user_id'=> $this->session->userdata('user_id'),
							'date_time'=> $this->get_indian_time(),
						);
						if(!empty($comment_details)){

							$this->common_model->update_data_sales_db('pq_details', array('comment'=> json_encode($comment_details)), array('id'=> $post_details['id']));
						}
						$response['comp_mst_id'] = $old_comment_details['comp_mst_id'];
					}
				break;

				case 'get_special_comment':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";die;
					if(!empty($post_details['comp_mst_id'])){

						$data['comment_details'] = $this->common_model->get_all_conditional_data_sales_db('id, special_comment', array('id' => $post_details['comp_mst_id'], 'status' => 'Active'),'customer_mst','row_array');

						$response['special_comment_history'] = $this->load->view('lead_management/special_comment_model_body', $data, true);
					}
				break;

				case 'save_special_comment':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";die;
					if(!empty($post_details['comp_mst_id'])){

						$comment_details = array();
						$old_comment_details = $this->common_model->get_dynamic_data_sales_db('id, special_comment', array('id'=> $post_details['comp_mst_id'], 'status'=> 'Active'), 'customer_mst', 'row_array');
						if(!empty($old_comment_details)){

							$comment_details = json_decode($old_comment_details['special_comment'], true);
						}
						$comment_details[] = array(
							'message'=> $post_details['comment'],
							'date_time'=> $this->get_indian_time(),
						);
						if(!empty($comment_details)){

							$this->common_model->update_data_sales_db('customer_mst', array('special_comment'=> json_encode($comment_details)), array('id'=> $post_details['comp_mst_id']));
						}
					}
				break;

				default:
					$response['status'] = 'failed';	
					$response['message'] = "call type not found";
				break;
			}
			echo json_encode($response);
		} else{
			die('access is not allowed to this function');
		}
	}

	private function export_stats_yearly_internal($company_name){

		$return_array = array();
		$return_array['category'] = array();
		$return_array['series'] = array();
		$return_array['total'] = 0;

		if(in_array($this->session->userdata('role'), array(1))){

			$export_stats_yearly_data = $this->common_model->get_all_conditional_data_sales_db("SUM(FOB_VALUE_INR) as value, EXPORTER_NAME, NEW_IMPORTER_NAME, INVOICE_YEAR","NEW_IMPORTER_NAME LIKE '%".strtoupper($company_name)."%'", "trending_invoice", 'result_array', array(), array('column_name'=> 'INVOICE_YEAR', 'column_value'=> 'ASC'), array('EXPORTER_NAME', 'INVOICE_YEAR'));
		}else{

			$export_stats_yearly_data = $this->common_model->get_all_conditional_data_sales_db("SUM(FOB_VALUE_INR) as value, '' as EXPORTER_NAME, '' as NEW_IMPORTER_NAME, INVOICE_YEAR", "NEW_IMPORTER_NAME LIKE '%".strtoupper($company_name)."%'", "trending_invoice", 'result_array', array(), array('column_name'=> 'INVOICE_YEAR', 'column_value'=> 'ASC'), array('INVOICE_YEAR'));
		}
		$series_name = array();
		foreach ($export_stats_yearly_data as $single_details) {
			
			if(!in_array($single_details['EXPORTER_NAME'], $return_array['category'])){
				
				$return_array['category'][] = $single_details['EXPORTER_NAME'];
			}
			if(!in_array($single_details['INVOICE_YEAR'], $series_name)){
				
				$series_name[] = $single_details['INVOICE_YEAR'];
			}
			$return_array['total'] += $single_details['value'];
		}
		$static_year_value = array();
		foreach ($return_array['category'] as $year_key => $year_name) {
			
			$static_year_value[] = 0;
		}
		foreach ($export_stats_yearly_data as $single_details) {
			
			$return_array['series'][array_search($single_details['INVOICE_YEAR'], $series_name)]['name'] = $single_details['INVOICE_YEAR'];
			$return_array['series'][array_search($single_details['INVOICE_YEAR'], $series_name)]['data'] = $static_year_value;
		}
		foreach ($export_stats_yearly_data as $single_details) {

			$return_array['series'][array_search($single_details['INVOICE_YEAR'], $series_name)]['data'][array_search($single_details['EXPORTER_NAME'], $return_array['category'])] = (int) $single_details['value'];
		}
		$return_array['total'] = $this->convert_number_into_indian_currency($return_array['total']);

		// echo "<pre>";print_r($return_array);echo"</pre><hr>";die('done');
		return (!empty($return_array['category']) && !empty($return_array['series']) && !empty($return_array['total'])) ? $return_array : array();
	}
	private function prepare_listing_data_new($where, $order_by, $limit, $offset, $search_filter_array = array()) {

		$data = $this->home_model->get_production_listingz_data($where, $order_by,$limit, $offset);
		
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$return_data['total_revenue_list'] = array();
		$return_data['paggination_data'] = $data['paggination_data'];
		foreach ($data['production_list'] as $production_list_key => $production_list_value) {
			
			$temp_grand_total = 0;
			// echo "<pre>";print_r($production_list_value);echo"</pre><hr>";
			if(!isset($return_data['total_revenue_list'][$production_list_value['currency']])){

				$return_data['total_revenue_list'][$production_list_value['currency']]['currency'] = $production_list_value['currency'];
				$return_data['total_revenue_list'][$production_list_value['currency']]['total'] = 0;
			}

			if(($production_list_value['semi_ready_count'] + $production_list_value['ready_for_dispatch_count'] + $production_list_value['dispatch_count'] + $production_list_value['on_hold_count'] + $production_list_value['query_count'] + $production_list_value['mtt_rfd_count'] + $production_list_value['mtt_count'] + $production_list_value['cancel_count']) > 0){
				
				if($this->session->userdata('production_tab_name') == 'pending_order') {

					$temp_grand_total = $this->home_model->get_pending_count($production_list_value['quotation_mst_id']);
				}else{

					$temp_grand_total = $this->home_model->get_other_count($production_list_value['quotation_mst_id'], $this->session->userdata('production_tab_name'));
					
				}
				if($temp_grand_total > 0){

					$return_data['total_revenue_list'][$production_list_value['currency']]['total'] += $temp_grand_total;
					$return_data['total_revenue_list'][$production_list_value['currency']]['total'] += ($production_list_value['freight'] + $production_list_value['bank_charges'] + $production_list_value['gst']);
					$return_data['total_revenue_list'][$production_list_value['currency']]['total'] -= $production_list_value['discount'];
				}else{

					$return_data['paggination_data']['total_rows'] = $return_data['paggination_data']['total_rows']-1;
					if($return_data['paggination_data']['total_rows'] == 0){

						$return_data['total_revenue_list'] = array();
					}
				}
				
			}else{

				$return_data['total_revenue_list'][$production_list_value['currency']]['total'] += $production_list_value['grand_total'];
			}
			// echo "<pre>";print_r($return_data['total_revenue_list']);echo"</pre><hr>";
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";die('came in');
		return $return_data;
	}
	private function default_where_for_all_tab($tab_name, $company_id){

		$this->session->set_userdata('production_tab_name',$tab_name);
		$return_where = array(
							0=> "quotation_mst.stage = 'proforma'",
							1=> "quotation_mst.status = 'Won'",
							2=> "quotation_mst.work_order_no != 0",
							3=> "(production_process_information.production_status is NULL OR production_process_information.production_status != 'dispatched' AND production_process_information.production_status != 'on_hold' AND production_process_information.production_status != 'ready_for_dispatch' AND production_process_information.production_status != 'semi_ready' AND
							production_process_information.production_status != 'merchant_trade' AND 
							production_process_information.production_status != 'query' AND
							production_process_information.production_status != 'mtt_rfd' AND
							production_process_information.production_status != 'order_cancelled')",
							4=> "(production_process_information.status is NULL or production_process_information.status = 'Active')");
		if($tab_name == 'dispatch_order') {
			$return_where[3] = "(production_process_information.production_status = 'dispatched' OR production_process_information.dispatch_count > 0)";
		
		}elseif ($tab_name == 'on_hold_order') {
			$return_where[3] = "(production_process_information.production_status = 'on_hold' OR production_process_information.on_hold_count > 0)";
		
		}elseif ($tab_name == 'ready_to_dispatch_order') {
			$return_where[3] = "(production_process_information.production_status = 'ready_for_dispatch' OR production_process_information.ready_for_dispatch_count > 0)";
		
		}elseif ($tab_name == 'semi_ready') {
			$return_where[3] = "(production_process_information.production_status = 'semi_ready' OR production_process_information.semi_ready_count > 0)";
		
		}elseif ($tab_name == 'order_cancelled') {
			$return_where[3] = "(production_process_information.production_status = 'order_cancelled' OR production_process_information.cancel_count > 0)";
		
		}elseif ($tab_name == 'mtt') {
			$return_where[3] = "(production_process_information.production_status = 'merchant_trade' OR production_process_information.mtt_count > 0)";
		
		}elseif ($tab_name == 'query') {
			$return_where[3] = "(production_process_information.production_status = 'query' OR production_process_information.query_count > 0)";
		
		}elseif ($tab_name == 'mtt_rfd') {
			$return_where[3] = "(production_process_information.production_status = 'mtt_rfd' OR production_process_information.mtt_rfd_count > 0)";
		
		}
		if(in_array($this->session->userdata('role'), array(5, 8))) {
			$return_where[5] = "quotation_mst.assigned_to = ".$this->session->userdata('user_id');
			if($this->session->userdata('role') == 8) {
				$procurement_user_name = $this->common_model->get_dynamic_data_sales_db('name',array('user_id'=>$this->session->userdata('user_id')), 'users', 'row_array');
				$user_explode = explode(' ',$procurement_user_name['name']);
				$return_where[5] = "production_process_information.handled_by LIKE '%".$user_explode['0']."%'";
			}

		}else if(in_array($this->session->userdata('role'), array(18))) {
			
			$procurement_user_name = $this->common_model->get_dynamic_data_sales_db('name',array('user_id'=>$this->session->userdata('user_id')), 'users', 'row_array');
			$user_explode = explode(' ',$procurement_user_name['name']);
			$return_where[5] = "production_process_information.handled_by LIKE '%".$user_explode['0']."%'";
		}

		$return_where[] = "quotation_mst.client_id = ".$company_id;
		// echo "<pre>";print_r($return_where);echo"</pre><hr>";exit;
		return $return_where;
	}
	private function get_total_in_indian_currency($total_details) {

		// getting currency details
		$currency_details = array_column($this->common_model->get_dynamic_data_sales_db('*',array('status'=>'Active'), 'currency'), 'currency_rate', 'currency_id');
		$currency_value = $currency_details[1];
		$num = 0;
		foreach ($total_details as $single_details) {

			$num += round((((int)$single_details['total']) * ($currency_details[$single_details['currency']] / $currency_value)));
			// $num +=  $single_details['total'] * $currency_details[$single_details['currency']];
		}
		return array('total'=>$num);
	}
	private function convert_number_into_indian_currency($num){

		$num = number_format($num,0,'','');
		$new_num = '';
		if(strlen($num) < 4) {
			$new_num = $num;
		}else{
			if(strlen($num) % 2 == 0) {
				for ($i=0; $i < strlen($num)-3; $i++) { 
					
					if(($i % 2) == 0) {

						$new_num .= $num[$i];
						$new_num .= ',';
					}else {

						$new_num .= $num[$i];
					}
				}
				$new_num .= $num[strlen($num)-3];
				$new_num .= $num[strlen($num)-2];
				$new_num .= $num[strlen($num)-1];
			}else {
				for ($i=0; $i < strlen($num)-3; $i++) { 
					
					if(($i % 2) != 0) {

						$new_num .= $num[$i];
						$new_num .= ',';
					}else {

						$new_num .= $num[$i];
					}
				}
				$new_num .= $num[strlen($num)-3];
				$new_num .= $num[strlen($num)-2];
				$new_num .= $num[strlen($num)-1];
			}
		}
		return $new_num;
	}
	private function get_order_wise_total_new($company_id){

		$return_data['series'] = array(
										array('name'=>'Pending Order', 'data'=> array(0)),
										array('name'=>'Semi Ready', 'data'=> array(0)),
										array('name'=>'MTT', 'data'=> array(0)),
										array('name'=>'Query', 'data'=> array(0)),
										array('name'=>'MTT/RFD', 'data'=> array(0)),
										array('name'=>'Ready To Dispatch', 'data'=> array(0))
									);
		$return_data['total'] = 0;
        foreach (array('pending_order', 'semi_ready', 'mtt', 'query', 'mtt_rfd', 'ready_to_dispatch_order') as $key => $status_name) {
			
			$order_total = $this->prepare_listing_data_new(implode(' AND ', $this->default_where_for_all_tab($status_name, $company_id)),array(),0,0);
			// echo "<pre>";print_r($order_total);echo"</pre><hr>";die('done');
			$total_in_rupees = $this->get_total_in_indian_currency($order_total['total_revenue_list']);
			// echo "<pre>";print_r($total_in_rupees);echo"</pre><hr>";die('done');
			$return_data['series'][$key]['data'] = array(round($total_in_rupees['total']));
			$return_data['total'] += $total_in_rupees['total'];
		}
		$return_data['total'] = $this->convert_number_into_indian_currency($return_data['total']);

		// echo "<pre>";print_r($return_data);echo"</pre><hr>";die('done');
		return $return_data;
	}
	private function prepare_where_and_orderby_for_table($product_name, $search_filter_form_data, $limit, $offset){
		
		$return_array = array();
		$return_array['select'] = " SQL_CALC_FOUND_ROWS
									customer_mst.id,
									customer_mst.name customer_name,
									customer_mst.region_id,
									customer_mst.country_id,
									customer_mst.website,
									customer_mst.entered_on,
									customer_mst.entered_by,
									customer_mst.modified_on,
									customer_mst.modified_by,
									customer_mst.assigned_to,
									customer_mst.no_of_employees,
									customer_mst.lead_type,
									customer_mst.lead_stage,
									customer_mst.stage_reason,
									customer_mst.box,
									customer_mst.box_comment,
									customer_mst.sample,
									customer_mst.sample_comment,
									customer_mst.lead_priority,
									customer_mst.priority_reason,
									customer_mst.deleted,
									customer_data.source,
									DATE_FORMAT(customer_data.last_purchased, '%b-%y') last_purchased,
									customer_data.product_category_id,
									country_mst.name country_name,
									country_flags.flag_name,
									customer_data.rank,
									customer_mst.status,
									customer_mst.brand,
									customer_mst.delete_reason,
									customer_mst.margins,
									customer_mst.purchase_comments,
									customer_mst.purchase_factor_1,
									customer_mst.purchase_factor_2,
									customer_mst.purchase_factor_3,
									customer_mst.sales_notes,
									customer_mst.document_file,
									customer_mst.pq_client_status,
									customer_mst.is_pq,
									customer_mst.lead_status,
									customer_mst.special_comment,
									customer_mst.product_pitch";
		$return_array['join'] = 'INNER JOIN
									customer_data
										ON customer_data.customer_mst_id = customer_mst.id
								LEFT JOIN
									country_mst
										ON country_mst.id = customer_mst.country_id
								LEFT JOIN
									country_flags
										ON country_flags.country = country_mst.name
								LEFT JOIN
									customer_connect
										ON customer_mst.id = customer_connect.comp_mst_id';
		$return_array['where'] = '';
		$return_array['order_by'] = '';
		$return_array['limit'] = $limit; 
		$return_array['offset'] = $offset;
		$return_array['having'] = '';
		$static_product_category_array = array(
			'TUBES'=>1, 'PIPES'=>2, 'HAMMER UNION'=>5, 'TUBING'=>6, 'PROCESS_CONTROL'=>7, 'Chemical Companies'=>8,
			'EPC Companies'=>9, 'distributors'=>10,	'Shipyards'=>11, 'Water Companies'=>12, 'Forged Fittings'=>13, 'Heteregenous Tubes India'=>14, 'sugar Companies'=>15, 'pvf companies'=>16, 'Miscellaneous Leads'=>17, 'Hydraulic fitting'=>18, 'Fasteners'=>19
		);
		
		if(in_array($product_name, array('PIPES', 'TUBES', 'PROCESS_CONTROL', 'TUBING', 'HAMMER UNION'))){

			$return_array['where'] = "customer_mst.deleted is null AND customer_mst.status = 'Active' AND customer_data.rank is not null AND customer_data.product_category_id = ".$static_product_category_array[$product_name]."";
			$return_array['order_by'] = 'customer_data.rank asc';
			
		}else if(in_array($product_name, array('Shipyards', 'Water Companies', 'Chemical Companies', 'EPC Companies', 'sugar Companies', 'Heteregenous Tubes India', 'pvf companies', 'distributors', 'Forged Fittings', 'Miscellaneous Leads', 'Hydraulic fitting', 'Fasteners'))){

			$return_array['where'] = "customer_mst.deleted is null AND customer_mst.status = 'Active' AND customer_data.product_category_id = ".$static_product_category_array[$product_name]."";
			$return_array['order_by'] = 'customer_mst.lead_stage desc, customer_mst.name asc';
		}else if(in_array($product_name, array('internal'))){
			
			$return_array['where'] = "customer_data.source = 'internal'";
			$return_array['order_by'] = "customer_data.rank asc";
		}else if(in_array($product_name, array('instrumentation'))){
			
			$return_array['where'] = "customer_mst.deleted is null AND customer_mst.status = 'Active' AND customer_data.product_category_id IN(1, 7, 10, 14, 16, 18)";
			$return_array['order_by'] = 'customer_mst.lead_stage desc, customer_mst.name asc';
		}else if(in_array($product_name, array('pq'))){

			$return_array['where'] = "customer_mst.deleted is null AND customer_mst.status = 'Active' AND customer_mst.is_pq = 'Yes' AND (( customer_data.product_category_id IN (1, 2, 3, 4, 5, 6, 7) AND customer_data.rank is not null) OR  (customer_data.product_category_id not IN (1, 2, 3, 4, 5, 6, 7) AND customer_data.rank is null))";
			$return_array['order_by'] = 'customer_mst.lead_stage desc, customer_data.rank asc';
		}
		if($this->session->userdata('role') == 5){
			
			$return_array['where'] .= " AND customer_mst.assigned_to IN (".implode(', ', $this->session->userdata('lead_access')['lead_sales_user_id']).")";
		}

		if(!empty($search_filter_form_data)){

			$sample_box = $lead_type = $lead_stage = array();
			$country_id = $region_id = $assigned_to = array();
			$client_id = $last_purchased = $last_purchased_month = $connect_mode = array();
			$brand_list= $pq_client_status = array();

			// echo "<pre>";print_r(array_column($search_filter_form_data, 'value', 'name'));echo"</pre><hr>";
			//  echo "<pre>";print_r($search_filter_form_data);echo"</pre><hr>";exit;
			foreach ($search_filter_form_data as $form_data) {
				
				$name = $form_data['name'];
				if(!empty($form_data['value'])){

					switch ($name) {

						case 'sample_box':

							$explode_sample_box = explode("_", $form_data['value']);
							$sample_box[] = array(
													'name' => $explode_sample_box[0],	
													'value' => $explode_sample_box[1],	
												);
						break;
						case 'lead_type':
						case 'lead_stage':
						case 'country_id':
						case 'region_id':
						case 'assigned_to':
						case 'client_id':
						case 'last_purchased':
						case 'last_purchased_month':
						case 'connect_mode':
						case 'brand_list':
						case 'pq_client_status':

							$$name[] = $form_data['value'];
						break;
						case 'last_contacted':

							$return_array['select'] .= ', MAX(customer_connect.connected_on)  AS last_contact_date';
							switch ($form_data['value']) {
								case '0-7':

									$return_array['having'] = 'last_contact_date >= DATE_SUB(NOW(), INTERVAL 7 day)';
								break;
								case '7-28':

									$return_array['having'] = 'last_contact_date >= DATE_SUB(NOW(), INTERVAL 28 day) AND last_contact_date <= DATE_SUB(NOW(), INTERVAL 8 day)';
								break;	
								case '28-0':
									
									$return_array['having'] = 'last_contact_date <= DATE_SUB(NOW(), INTERVAL 29 day)';
								break;
								case '0-0':
									
									$return_array['having'] = 'last_contact_date is null';
								break;
							}
						break;
						case 'member':

							$return_array['join'] .= '
													LEFT JOIN
														customer_dtl
															ON customer_mst.id = customer_dtl.comp_mst_id';

							$return_array['where'] .= " AND customer_dtl.comp_dtl_id = ".$form_data['value'];
						break;
						case 'document_file':
							switch ($form_data['value']) {
								case 'uploded':

									$return_array['where'] .= " AND customer_mst.document_file != ''";
								break;
								case 'not-uploded':

									$return_array['where'] .= " AND customer_mst.document_file = ''";
								break;
							}
						break;
					}
				}
			}
			if(!empty($sample_box)){

				$return_array['where'] .= " AND (";
				for ($i=0; $i < count($sample_box); $i++) { 
					
					if($i > 0){
						
						$return_array['where'] .= " OR ";
					}
					$return_array['where'] .= "customer_mst.".$sample_box[$i]['name']." = '".$sample_box[$i]['value']."'";

				}					
				$return_array['where'] .= ")";
			}
			if(!empty($lead_type)){

				$return_array['where'] .= " AND customer_mst.lead_type IN ('".implode("', '", $lead_type)."')";
			}
			if(!empty($lead_stage)){

				$return_array['where'] .= " AND (";
				foreach ($lead_stage as $key => $stage_value) {

					if($key > 0){

						$return_array['where'] .= " OR ";
					}
					$return_array['where'] .= "customer_mst.lead_stage {$stage_value}";
				}
				$return_array['where'] .= ")";
			}
			if(!empty($country_id)){

				$return_array['where'] .= " AND customer_mst.country_id IN ('".implode("', '", $country_id)."')";
			}
			if(!empty($region_id)){

				$return_array['where'] .= " AND customer_mst.region_id IN ('".implode("', '", $region_id)."')";
			}
			if(!empty($assigned_to)){

				$return_array['where'] .= " AND customer_mst.assigned_to IN ('".implode("', '", $assigned_to)."')";
			}
			if(!empty($client_id)){

				$return_array['where'] .= " AND customer_mst.id IN (".implode(", ", $client_id).")";
			}
			if(!empty($last_purchased)){
				
				$return_array['where'] .= " AND (";
				for ($i=0; $i < count($last_purchased); $i++) { 
					
					if($i > 0){
						
						$return_array['where'] .= " OR ";
					}
					$explode_years = explode("-", $last_purchased[$i]);
					// if(count($explode_years) == 2){
						// echo "<pre>";print_r(count($explode_years));echo"</pre><hr>";exit;
						$return_array['where'] .= "customer_data.last_purchased >= '".$explode_years[0].'-04-01'."' AND customer_data.last_purchased <= '".$explode_years[1].'-03-31'."'";
					// }
					// if(count($explode_years) == 3){

					// 	$return_array['where'] .= "Year(customer_data.last_purchased) = '".$explode_years[0]."' AND Month(customer_data.last_purchased) = '".$explode_years[2]."'";
					// }

				}
				$return_array['where'] .= ")";
			}
			if(!empty($last_purchased_month)) {
				$return_array['where'] .= " AND (";
				for ($i = 0; $i < count($last_purchased_month); $i++) {
					if ($i > 0) {
						$return_array['where'] .= " OR ";
					}
					$month = date("m", strtotime($last_purchased_month[$i]));
					$return_array['where'] .= "MONTH(customer_data.last_purchased) = '$month'";

				}
				$return_array['where'] .= ")";
			}
			if(!empty($connect_mode)){

				$return_array['where'] .= " AND customer_connect.connect_mode IN ('".implode("', '", $connect_mode)."')";
			}
			if(!empty($brand_list)){
				
				$return_array['where'] .= " AND (";
				for ($i=0; $i < count($brand_list); $i++) {
					
					if($i > 0){
						$return_array['where'] .= " OR ";
					}
					$return_array['where'] .= "customer_mst.brand = '".$brand_list[$i]."'";
				}					
				$return_array['where'] .= ")";
			}			
			if(!empty($pq_client_status)){

				$return_array['where'] .= " AND customer_mst.pq_client_status IN ('".implode("', '", $pq_client_status)."')";
			}
			// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		}

		return $return_array;
	}

	private function prepare_search_filter_data($search_filter_form_data){

		// echo "<pre>";print_r($search_filter_form_data);echo"</pre><hr>";exit;
		$data['sample_box'] = array(
							'box_Yes' => array(
								'name' => 'Box - Yes',
								'value' => 'box_Yes',
								'selected' => ''
							),
							'box_No' => array(
								'name' => 'Box - No',
								'value' => 'box_No',
								'selected' => ''
							),
							'sample_Yes' => array(
								'name' => 'Sample - Yes',
								'value' => 'sample_Yes',
								'selected' => ''
							),
							'sample_No' => array(
								'name' => 'Sample - No',
								'value' => 'sample_No',
								'selected' => ''
							)
		);
		$data['lead_type']    = $this->change_key_as_per_value(
									$this->lead_management_model->get_all_conditional_data_sales_db(
										'type_name as name, lead_type_id as value, "" as selected' ,
										array(),
										'lead_type'
									)
		);
		$data['lead_stage']   =	$this->change_key_as_per_value(
									$this->lead_management_model->get_all_conditional_data_sales_db(
										'stage_name as name, concat("= ", lead_stage_id) as value, "" as selected' ,
										array(),
										'lead_stages'
									)
		);
		$data['lead_stage']['is null'] =  array(
											'name' => 'Blank',
											'value' => 'is null',
											'selected' => ''
		);
		$data['country_id']   =	$this->change_key_as_per_value(
									$this->lead_management_model->get_all_conditional_data_sales_db(
										'name, id as value, "" as selected',
										array('status'=>'active'),
										'country_mst'
									)
		);
		$data['region_id']    =	$this->change_key_as_per_value(
									$this->lead_management_model->get_all_conditional_data_sales_db(
										'name, id as value, "" as selected',
										array('status'=>'active'),
										'region_mst'
									)
		);
		$data['assigned_to']  =	$this->change_key_as_per_value(
									$this->lead_management_model->get_dynamic_data_sales_db_null_false(
										'name, user_id as value, "" as selected',
										"status=1 AND role IN (5, 16)",
										'users'
									)
		);
		$data['last_contacted'] = array(
							'0-7' => array(
								'name' => 'Less Than 1 Week',
								'value' => '0-7',
								'selected' => ''
							),
							'7-28' => array(
								'name' => '1 Week To 4 Week',
								'value' => '7-28',
								'selected' => ''
							),
							'28-0' => array(
								'name' => '1 Month +',
								'value' => '28-0',
								'selected' => ''
							),
							'0-0' => array(
								'name' => 'blank',
								'value' => '0-0',
								'selected' => ''
							)
		);
		$data['last_purchased']  =	$this->change_key_as_per_value($this->lead_management_model->get_last_purchase_date());
		$data['last_purchased_month']  =	$this->change_key_as_per_value($this->lead_management_model->get_last_purchase_date());
		$data['connect_mode'] = array(
							'call' => array(
								'name' => 'Call',
								'value' => 'call',
								'selected' => ''
							),
							'whatsapp' => array(
								'name' => 'Whatsapp',
								'value' => 'whatsapp',
								'selected' => ''
							),
							'email' => array(
								'name' => 'Email',
								'value' => 'email',
								'selected' => ''
							),
							'linkedin' => array(
								'name' => 'LinkedIn',
								'value' => 'linkedin',
								'selected' => ''
							)
		);
		$data['brand_list'] =	$this->change_key_as_per_value(
									$this->lead_management_model->get_all_conditional_data_sales_db(
										'brand as name, brand as value, "" as selected',
										array('status'=>'Active', 'brand !='=> null),
										'customer_mst',
										'result_array',
										array(),
										array(),
										array('brand')
									),
									'name'
		);
		$data['document_file'] = array(
							'uploded' => array(
								'name' => 'Uploded',
								'value' => 'uploded',
								'selected' => ''
							),
							'not-uploded' => array(
								'name' => 'Not-Uploded',
								'value' => 'not-uploded',
								'selected' => ''
							)
		);
		$data['pq_client_status'] = array(
							'Approved' => array(
								'name' => 'Approved',
								'value' => 'Approved',
								'selected' => ''
							),
							'Pending' => array(
								'name' => 'Pending',
								'value' => 'Pending',
								'selected' => ''
							)
		);
		$data['search_lead_client_name'] = '';
		$data['member'] = array();
		if(!empty($search_filter_form_data)){

			foreach ($search_filter_form_data as $form_data) {
				
				if(!empty($form_data['value'])){

					switch ($form_data['name']) {

						case 'search_lead_client_name':

							$data['search_lead_client_name'] = $form_data['value'];
							$where_string = "status = 'Active' AND name LIKE '%".$form_data['value']."%'";
							if($this->session->userdata('role') == 5){

								$where_string .= " AND assigned_to IN ('".implode("', '", $this->session->userdata('lead_access')['lead_sales_user_id'])."')";
							}
							$data['client_id'] = $this->change_key_as_per_value(
								$this->lead_management_model->get_dynamic_data_sales_db_null_false(
									'id as value, name, "" as selected',
									$where_string,
									'customer_mst'
								)
							);
							$data['search_lead_client_name'] = $form_data['value'];
						break;
						case 'client_id':

							$data[$form_data['name']][$form_data['value']]['selected'] = 'selected';
							$data['member'] = $this->change_key_as_per_value(
								$this->lead_management_model->get_dynamic_data_sales_db_null_false(
									'comp_dtl_id as value, member_name, "" as selected',
									"status = 'Active' AND comp_mst_id = ".$form_data['value'],
									'customer_dtl'
								)
							);
						break;

						case 'sample_box':
						case 'lead_type':
						case 'lead_stage':
						case 'country_id':
						case 'region_id':
						case 'assigned_to':
						case 'last_contacted':
						case 'last_purchased':
						case 'last_purchased_month':
						case 'connect_mode':
						case 'brand_list':
						case 'member':
						case 'document_file':
						case 'pq_client_status':

							$data[$form_data['name']][$form_data['value']]['selected'] = 'selected';
						break;

						default:
						break;
					}
				}
			}
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		return $data;
	}

	private function change_key_as_per_value($data, $changing_key_column_name = 'value'){

		$return_data = array();
		foreach ($data as $single_data) {
			
			$return_data[$single_data[$changing_key_column_name]] = $single_data;
		}

		return $return_data;
	}
	private function prepare_table_body_data($select_string, $join_string, $where_array, $having_string, $order_by, $limit, $offset){
		
		$data = $this->lead_management_model->get_listing_data_experiment($select_string, $join_string, $where_array, $having_string, $order_by, $limit, $offset);
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;

		$lead_type_details = array_column($this->lead_management_model->get_all_data('lead_type_id, type_name', array(), 'lead_type'), 'type_name', 'lead_type_id');
		$lead_type_details['']='';
		$lead_type_details[0]='';

		$user_list = array_column($this->lead_management_model->get_all_data('user_id, name', array(), 'users'), 'name', 'user_id');
		$user_list['']='';
		$user_list[0]='';

		$reason_list = array_column($this->lead_management_model->get_all_data('lead_reason_id, reason', array(), 'lead_stage_reasons'), 'reason', 'lead_reason_id');
		$reason_list['']='';
		$reason_list[0]='';
		
		foreach($data['list_data'] as $list_data_key => $list_data_details){			
			// echo "<pre>";print_r($list_data_details);echo"</pre><hr>";exit;
			
			$data['list_data'][$list_data_key]['lead_name'] = $lead_type_details[$list_data_details['lead_type']];
			$data['list_data'][$list_data_key]['user_name'] = $user_list[$list_data_details['assigned_to']];
			$data['list_data'][$list_data_key]['stage_reason'] = $reason_list[$list_data_details['stage_reason']];
			// $data['list_data'][$list_data_key]['last_purchase_date'] = date('F, j Y', strtotime($list_data_details['last_purchased']));
			
			$client_member_details = $this->lead_management_model->get_all_conditional_data_sales_db('*', array('comp_mst_id'=>$list_data_details['id'],'status'=>'Active'),'customer_dtl','result_array', array(),array(),array(),0,0);			
			
			$data['list_data'][$list_data_key]['comp_dtl_id'] = null;
			$data['list_data'][$list_data_key]['member_name'] = null;
			$data['list_data'][$list_data_key]['designation'] = null;
			$data['list_data'][$list_data_key]['email'] = null; 
			$data['list_data'][$list_data_key]['mobile'] = null;
			$member_count = $non_member_count = 0;
			
			if(!empty($client_member_details)){
				
				foreach($client_member_details as $single_member_details){
					// echo "<pre>";print_r($single_member_details);echo"</pre><hr>";exit;
					
					if($data['list_data'][$list_data_key]['comp_dtl_id'] === null && strtolower($single_member_details['main_buyer']) == 'yes'){
						
						$data['list_data'][$list_data_key]['comp_dtl_id'] = $single_member_details['comp_dtl_id'];
						$data['list_data'][$list_data_key]['member_name'] = $single_member_details['member_name'];
						$data['list_data'][$list_data_key]['designation'] = $single_member_details['designation'];
						$data['list_data'][$list_data_key]['email'] = $single_member_details['email']; 
						$data['list_data'][$list_data_key]['mobile'] = $single_member_details['mobile'];
						$member_count++;
					}else{
						if($single_member_details['member_name'] = '' && $single_member_details['email'] = '' && $single_member_details['mobile'] = ''){}else{
								
							if(strtolower($single_member_details['other_member']) == 'yes'){
								$non_member_count++;
							}else{
								$member_count++;
							}
						}
							
					}
					
				}
			}

			$data['list_data'][$list_data_key]['member_count'] = $member_count;
			$data['list_data'][$list_data_key]['non_member_count'] = $non_member_count;
				
			$client_last_comment_details = $this->lead_management_model->get_all_conditional_data_sales_db('connected_on, connect_mode, comments', array('comp_mst_id'=>$list_data_details['id'], ),'customer_connect','row_array', array(),array('column_name'=>'connected_on', 'column_value'=>'desc'),array(),0,0);			
			$last_contacted_data = $this->get_last_contacted_data($client_last_comment_details);

			$data['list_data'][$list_data_key]['last_contacted'] = $last_contacted_data['last_contacted'];
			$data['list_data'][$list_data_key]['comments'] = $last_contacted_data['comments'];
			$data['list_data'][$list_data_key]['connect_mode'] = $last_contacted_data['connect_mode'];

			$approved_class = $pending_class = '';
			if($list_data_details['pq_client_status'] == 'Approved'){

				$data['list_data'][$list_data_key]['pq_status_class'] = 'kt-align-left kt-font-bolder kt-badge kt-badge--success kt-badge--md kt-badge--rounded';
				$approved_class = 'active';
			}else{

				$data['list_data'][$list_data_key]['pq_status_class'] = 'kt-align-left kt-font-bolder kt-badge kt-badge--brand kt-badge--md kt-badge--rounded';
				$pending_class = 'active';
			}
			$data['list_data'][$list_data_key]['pq_status_array'] = array(
				'Pending'   =>	array('value'=> 'Pending',    'display_name'=> 'Pending',    'class'=> $pending_class),
				'Approved'  =>	array('value'=> 'Approved',   'display_name'=> 'Approved',   'class'=> $approved_class),
			);

			$pq_details = $this->lead_management_model->get_all_conditional_data_sales_db('*', array('comp_mst_id'=>$list_data_details['id'], 'status'=>'Active'),'pq_details','row_array');
			if(!empty($pq_details['comment'])){
				foreach (json_decode($pq_details['comment'], true) as $comment_details) {

					$data['list_data'][$list_data_key]['last_pq_comment'] = $comment_details['message'];
				}
			}

			if(!empty($list_data_details['special_comment'])){
				foreach (json_decode($list_data_details['special_comment'], true) as $comment_details) {

					$data['list_data'][$list_data_key]['last_special_comment'] = $comment_details['message'];
				}
			}
		}
		$data['paggination_data']['limit'] = $limit;
		$data['paggination_data']['offset'] = $offset;
		
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		return $data;
	}

	private function update_lead_tab_counts($search_filter_form_data) {

		$lead_tab_count = [];
		$static_product_category_array = [
			'TUBES' => 1, 'PIPES' => 2, 'HAMMER UNION' => 5, 'TUBING' => 6, 'PROCESS_CONTROL' => 7,
			'Chemical Companies' => 8, 'EPC Companies' => 9, 'distributors' => 10, 'Shipyards' => 11,
			'Water Companies' => 12, 'Forged Fittings' => 13, 'Heteregenous Tubes India' => 14,
			'sugar Companies' => 15, 'pvf companies' => 16, 'Miscellaneous Leads' => 17,
			'Hydraulic fitting' => 18, 'Fasteners' => 19, 'internal' => 20, 'instrumentation' => 21, 'pq' => 22
		];

		// echo "<pre>";print_r($search_filter_form_data);echo"</pre><hr>";exit;
		$filters   = $this->processSearchFilters($search_filter_form_data);
		$where_conditions = $filters['where'];
		$having_conditions = $filters['having'];
		$join_conditions = $filters['join'];

		$join_string = "INNER JOIN customer_data ON customer_data.customer_mst_id = customer_mst.id
						LEFT JOIN country_mst ON country_mst.id = customer_mst.country_id
						LEFT JOIN country_flags ON country_flags.country = country_mst.name
						LEFT JOIN customer_connect ON customer_mst.id = customer_connect.comp_mst_id";

		if (!empty($join_conditions)) {
			$join_string .= " " . implode(" ", $join_conditions);
		}

		$base_where = "customer_mst.deleted IS NULL AND customer_mst.status = 'Active'";
		foreach ($static_product_category_array as $product_name => $product_id) {
			$where_string = $base_where;

			if (in_array($product_id, [1, 2, 5, 6, 7])) {

				$where_string .= " AND customer_data.rank IS NOT NULL AND customer_data.product_category_id = {$product_id}";
			} else if (in_array($product_id, [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19])) {

				$where_string .= " AND customer_data.product_category_id = {$product_id}";
			} else if ($product_id == 20) {

				$where_string .= " AND customer_data.source = 'internal'";
			} else if ($product_id == 21) {

				$where_string .= " AND customer_data.product_category_id IN (1, 7, 10, 14, 16, 18)";
			} else if ($product_id == 22) {

				$where_string .= " AND customer_mst.is_pq = 'Yes'";
			}
			if (!empty($where_conditions)) {
				$where_string .= " AND " . implode(" AND ", $where_conditions);
			}
			if($this->session->userdata('role') == 5){

				$where_string .= " AND customer_mst.assigned_to IN (".implode(', ', $this->session->userdata('lead_access')['lead_sales_user_id']).")";
			}

			$having_string = !empty($having_conditions) ? implode(" AND ", $having_conditions) : '';
			$result = $this->lead_management_model->get_listing_count_data($join_string, $where_string, $having_string);

			$lead_tab_count[$product_name] = $result ?? 0;
		}
		return $lead_tab_count;
	}

	private function processSearchFilters($search_filter_form_data) {

		$processed_filters = $having_conditions = $join_conditions = [];

		$last_purchased_conditions = $last_purchased_month_conditions = $sample_box_conditions = [];
		$brand_list_conditions = $document_file_conditions = $pq_client_status_conditions = [];
		$lead_stage_conditions = $region_conditions = $country_conditions = [];

		// echo "<pre>";print_r($search_filter_form_data);echo"</pre><hr>";exit;
		foreach ($search_filter_form_data as $form_data) {
			$name = trim($form_data['name']);
			$value = trim($form_data['value']);

			if ($name === 'search_lead_client_name' || $name === 'last_contact_date' || empty($value)) {
				continue;
			}
			if ($name === 'last_contacted') {
				switch ($value) {
					case '0-7':
						$having_conditions[] = "MAX(customer_connect.connected_on) >= DATE_SUB(NOW(), INTERVAL 7 DAY)";
					break;
					case '7-28':
						$having_conditions[] = "MAX(customer_connect.connected_on) >= DATE_SUB(NOW(), INTERVAL 28 DAY) AND MAX(customer_connect.connected_on) <= DATE_SUB(NOW(), INTERVAL 8 DAY)";
					break;
					case '28-0':
						$having_conditions[] = "MAX(customer_connect.connected_on) <= DATE_SUB(NOW(), INTERVAL 29 DAY)";
					break;
					case '0-0':
						$having_conditions[] = "MAX(customer_connect.connected_on) IS NULL";
					break;
				}
				continue;
			}
			if ($name === 'lead_stage') {
				if ($value === 'is null') {
					$lead_stage_conditions[] = "customer_mst.lead_stage IS NULL";
				} else {
					$value = ltrim($value, '= ');
					$lead_stage_conditions[] = $this->db->escape($value);
				}
				continue;
			}
			if ($name === 'sample_box') {

				$explode_sample_box = explode("_", $value);
				if (count($explode_sample_box) === 2) {
					$column_name = $explode_sample_box[0];
					$column_value = $explode_sample_box[1];
					$sample_box_conditions[] = "customer_mst." . $this->db->escape_str($column_name) . " = " . $this->db->escape($column_value);
				}
				continue;
			}
			if ($name === 'last_purchased') {

				$explode_years = explode("-", $value);
				if (count($explode_years) === 2) {
					$start_date = $explode_years[0] . '-04-01';
					$end_date = $explode_years[1] . '-03-31';
					$last_purchased_conditions[] = "(customer_data.last_purchased >= '$start_date' AND customer_data.last_purchased <= '$end_date')";
				}
				continue;
			}
			if ($name === 'last_purchased_month') {

				$month_number = date("m", strtotime("1 $value"));
				$last_purchased_month_conditions[] = "MONTH(customer_data.last_purchased) = '$month_number'";
				continue;
			}
			if ($name === 'member') {

				if (!empty($value)) {

					if (!in_array("LEFT JOIN customer_dtl ON customer_mst.id = customer_dtl.comp_mst_id", $join_conditions)) {
						$join_conditions[] = "LEFT JOIN customer_dtl ON customer_mst.id = customer_dtl.comp_mst_id";
					}
					$processed_filters['customer_dtl.comp_dtl_id'][] = $value;
				}
				continue;
			}
			if ($name === 'brand_list') {

				$brand_list_conditions[] = "customer_mst.brand = " . $this->db->escape($value);
				continue;
			}
			if ($name === 'pq_client_status') {

				$pq_client_status_conditions[] = "customer_mst.pq_client_status = " . $this->db->escape($value);
				continue;
			}
			if ($name === 'document_file') {
				if ($value === 'uploded') {
					$document_file_conditions[] = "customer_mst.document_file != ''";
				} elseif ($value === 'not-uploded') {
					$document_file_conditions[] = "customer_mst.document_file = ''";
				}
				continue;
			}
			if ($name === 'region_id') {

				$region_conditions[] = $this->db->escape($value);
				continue;
			}
			if ($name === 'country_id') {
				$country_conditions[] = $this->db->escape($value);
				continue;
			}

			if (!empty($value)) {
				if (!isset($processed_filters[$name])) {
					$processed_filters[$name] = [];
				}
				$processed_filters[$name][] = $value;
			}
		}

		// Generate SQL conditions
		$where_conditions = [];
		foreach ($processed_filters as $key => $values) {
			$escaped_values = array_map([$this->db, 'escape'], $values);

			if ($key === 'client_id') {

				$where_conditions[] = "customer_mst.id IN (" . implode(", ", $escaped_values) . ")";
			}elseif ($key === 'lead_type') {

				if (count($escaped_values) > 1) {
					$where_conditions[] = "customer_mst.lead_type IN (" . implode(", ", $escaped_values) . ")";
				} else {
					$where_conditions[] = "customer_mst.lead_type = " . $escaped_values[0];
				}
			} elseif (count($values) > 1) {

				$where_conditions[] = "$key IN (" . implode(", ", $escaped_values) . ")";
			} else {

				$where_conditions[] = "$key = " . $escaped_values[0];
			}
		}

		if (!empty($lead_stage_conditions)) {

			$where_conditions[] = "customer_mst.lead_stage IN (" . implode(", ", $lead_stage_conditions) . ")";
		}
		if (!empty($region_conditions)) {

			$where_conditions[] = "customer_mst.region_id IN (" . implode(", ", $region_conditions) . ")";
		}
		if (!empty($country_conditions)) {

			$where_conditions[] = "customer_mst.country_id IN (" . implode(", ", $country_conditions) . ")";
		}
		if (!empty($pq_client_status_conditions)) {

			$where_conditions[] = "(" . implode(" OR ", $pq_client_status_conditions) . ")";
		}
		if (!empty($document_file_conditions)) {

			$where_conditions[] = "(" . implode(" OR ", $document_file_conditions) . ")";
		}
		if (!empty($brand_list_conditions)) {

			$where_conditions[] = "(" . implode(" OR ", $brand_list_conditions) . ")";
    	}
		if (!empty($sample_box_conditions)) {

			$where_conditions[] = "(" . implode(" OR ", $sample_box_conditions) . ")";
		}
		if (!empty($last_purchased_conditions)) {

			$where_conditions[] = "(" . implode(" OR ", $last_purchased_conditions) . ")";
		}
		if (!empty($last_purchased_month_conditions)) {

			$where_conditions[] = "(" . implode(" OR ", $last_purchased_month_conditions) . ")";
		}
		// echo "<pre>";print_r($join_conditions);echo"</pre><hr>";exit;

		return [
			'join'  => $join_conditions,
			'where'  => $where_conditions,
			'having' => $having_conditions
		];
	}

	private function prepare_all_data($req_details){

		// echo "<pre>";print_r($req_details);echo"</pre><hr>";die;
		$data = array();
		///////////////Preparing WHERE & Order BY START///////////
		$prepare_table_body_data_params = $this->prepare_where_and_orderby_for_table($req_details['product_name'], $req_details['search_form_data'], $req_details['limit'], $req_details['offset']);
		// echo "<pre>";print_r($prepare_table_body_data_params);echo"</pre><hr>";exit;

		///////////////Preparing WHERE & Order by END/////////////
		////////////////////TABLE BODY START//////////////////////
		
		$data = $this->prepare_table_body_data(
								$prepare_table_body_data_params['select'],
								$prepare_table_body_data_params['join'],
								$prepare_table_body_data_params['where'],
								$prepare_table_body_data_params['having'],
								$prepare_table_body_data_params['order_by'],
								$prepare_table_body_data_params['limit'],
								$prepare_table_body_data_params['offset']
							);
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		//////////////////////TABLE BODY END//////////////////////
		////////////////////PAGGINATION START/////////////////////

		////////////////////PAGGINATION END///////////////////////
		////////////////////SEARCH FILTER START////////////////////
		$data['search_filter_form_data'] = $this->prepare_search_filter_data($req_details['search_form_data']);
		

		////////////////////SEARCH FILTER END/////////////////////
		$data['lead_name'] = $req_details['product_name'];

		if(!in_array($req_details['product_name'], array('PIPES', 'TUBES', 'PROCESS_CONTROL', 'TUBING', 'HAMMER UNION', 'internal', 'pq'))){
			$data['rank_colum_hidden'] = "kt-hidden";
		}
		
		//////////////////// Get URL ///////////////////////
		$data['search_engine_url_details']['url'] = '';

		if(!empty($req_details['product_name'])){
			
			$search_engine_data = $this->common_model->get_dynamic_data_sales_db('*', array('status'=> 'Active', 'user_id'=> $this->session->userdata('user_id')),'search_engine_primary_data','row_array');
			if(!empty($search_engine_data)){

				if(in_array($req_details['product_name'], array('PIPES', 'TUBES', 'TUBING', 'HAMMER UNION', 'internal'))){

					$category_matching_to_where_array = array('PIPES'=> 'piping', 'TUBES'=> 'tube_fitting_and_valves', 'TUBING'=> 'tubing', 'HAMMER UNION'=> 'hammer', 'internal'=> 'internal_data');
					
					// $data['search_engine_url_details'] = $this->lead_management_model->get_url_data($category_matching_to_where_array[$req_details['product_name']], $search_engine_data['access_type']);

					$version_details = $this->common_model->get_all_conditional_data_sales_db('id', array('status'=> 'Active'), 'version_information', 'row_array', array(), array('column_name'=> 'id','column_value'=>'desc'));
					
					if(!empty($version_details)){

						$data['search_engine_url_details'] = $this->common_model->get_dynamic_data_sales_db('url', array('status'=> 'Active', 'lead_name'=> $category_matching_to_where_array[$req_details['product_name']], 'access_type'=>  $search_engine_data['access_type'], 'version_id'=> $version_details['id']),'search_engine_access_url','row_array');
					}
				}
			}
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		return $data;		
	}

	private function upload_doc_config() {

		// echo "<pre>";print_r($_FILES);echo"</pre><hr>";exit;
		$config['upload_path']          = './assets/client_document/';
        $config['allowed_types']        = 'jpeg|gif|jpg|png';
        $config['overwrite']            = TRUE;

        $this->load->library('upload', $config);

		$data = array('status' => 'successful', 'msg' => '', 'file_name' => '');
        if ( ! $this->upload->do_upload('file'))
        {
			$error = array('error' => $this->upload->display_errors());
			$data['status'] = 'failed';
			$data['msg'] = $error['error'];
        }
        else
        {
			$file_dtls = $this->upload->data();
			$data['file_type'] = $file_dtls['file_type'];
			$data['file_name'] = $file_dtls['file_name'];
			$data['file_add_date'] = date('Y-m-d H:i:s');
        }
        // echo "<pre>";print_r($data);echo"</pre><hr>";exit;
        return $data;
	}







	private function create_primary_lead_data($where_array, $order_by, $limit, $offset){

		$data = $this->lead_management_model->get_primary_listing_data($where_array, $order_by, $limit, $offset);
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;

		$lead_type_details = array_column($this->lead_management_model->get_all_data('lead_type_id, type_name', array(), 'lead_type'), 'type_name', 'lead_type_id');
		$lead_type_details['']='';
		$lead_type_details[0]='';

		$user_list = array_column($this->lead_management_model->get_all_data('user_id, name', array(), 'users'), 'name', 'user_id');
		$user_list['']='';
		$user_list[0]='';

		$reason_list = array_column($this->lead_management_model->get_all_data('lead_reason_id, reason', array('status'=>'Active'), 'lead_stage_reasons'), 'reason', 'lead_reason_id');
		$reason_list['']='';
		$reason_list[0]='';
		
		foreach($data['primary_list'] as $primary_list_key => $primary_list_details){			
			// echo "<pre>";print_r($primary_list_details);echo"</pre><hr>";exit;
			
			$data['primary_list'][$primary_list_key]['lead_name'] = $lead_type_details[$primary_list_details['lead_type']];
			$data['primary_list'][$primary_list_key]['user_name'] = $user_list[$primary_list_details['assigned_to']];
			$data['primary_list'][$primary_list_key]['stage_reason'] = $reason_list[$primary_list_details['stage_reason']];
			// $data['primary_list'][$primary_list_key]['last_purchase_date'] = date('F, j Y', strtotime($primary_list_details['last_purchased']));
			
			$client_member_details = $this->lead_management_model->get_all_conditional_data_sales_db('*', array('comp_mst_id'=>$primary_list_details['id'],'status'=>'Active'),'customer_dtl','result_array', array(),array(),array(),0,0);			
			// echo "<pre>";print_r($client_member_details);echo"</pre><hr>";exit;
			
			$data['primary_list'][$primary_list_key]['comp_dtl_id'] = null;
			$data['primary_list'][$primary_list_key]['member_name'] = null;
			$data['primary_list'][$primary_list_key]['designation'] = null;
			$data['primary_list'][$primary_list_key]['email'] = null; 
			$data['primary_list'][$primary_list_key]['mobile'] = null;
			$member_count = $non_member_count = 0;
			
			if(!empty($client_member_details)){
				
				foreach($client_member_details as $single_member_details){
					// echo "<pre>";print_r($single_member_details);echo"</pre><hr>";exit;
					
					if(strtolower($single_member_details['main_buyer']) == 'yes'){
						
						$data['primary_list'][$primary_list_key]['comp_dtl_id'] = $single_member_details['comp_dtl_id'];
						$data['primary_list'][$primary_list_key]['member_name'] = $single_member_details['member_name'];
						$data['primary_list'][$primary_list_key]['designation'] = $single_member_details['designation'];
						$data['primary_list'][$primary_list_key]['email'] = $single_member_details['email']; 
						$data['primary_list'][$primary_list_key]['mobile'] = $single_member_details['mobile'];
						
					}else{
						if($single_member_details['member_name'] = '' && $single_member_details['email'] = '' && $single_member_details['mobile'] = ''){}else{
								
							if(strtolower($single_member_details['other_member']) == 'yes'){
								$non_member_count++;
							}else{
								$member_count++;
							}
						}
							
					}
				}
			}

			$data['primary_list'][$primary_list_key]['member_count'] = $member_count;
			$data['primary_list'][$primary_list_key]['non_member_count'] = $non_member_count;
				
			$client_last_comment_details = $this->lead_management_model->get_all_conditional_data_sales_db('connected_on, connect_mode, comments', array('comp_mst_id'=>$primary_list_details['id'], ),'customer_connect','row_array', array(),array('column_name'=>'connected_on', 'column_value'=>'desc'),array(),0,0);			
				// echo "<pre>";print_r($client_last_comment_details);echo"</pre><hr>";exit;	
				
			$last_contacted_data = $this->get_last_contacted_data($client_last_comment_details);

			$data['primary_list'][$primary_list_key]['last_contacted'] = $last_contacted_data['last_contacted'];
			$data['primary_list'][$primary_list_key]['comments'] = $last_contacted_data['comments'];
			$data['primary_list'][$primary_list_key]['connect_mode'] = $last_contacted_data['connect_mode'];
		}
		
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;	
		$data['paggination_data']['limit'] = $limit;
		$data['paggination_data']['offset'] = $offset;
		return $data;	
	}

	private function create_hetro_lead_data($where_array, $limit, $offset){

		$data = $this->lead_management_model->get_hetro_listing_data($where_array, $limit, $offset);
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		
		$lead_type_details = array_column($this->lead_management_model->get_all_data('lead_type_id, type_name', array(), 'lead_type'), 'type_name', 'lead_type_id');
		$lead_type_details['']='';
		$lead_type_details[0]='';

		$user_list = array_column($this->lead_management_model->get_all_data('user_id, name', array(), 'users'), 'name', 'user_id');
		$user_list['']='';
		$user_list[0]='';

		$reason_list = array_column($this->lead_management_model->get_all_data('lead_reason_id, reason', array('status'=>'Active'), 'lead_stage_reasons'), 'reason', 'lead_reason_id');
		$reason_list['']='';
		$reason_list[0]='';
				
		foreach($data['hetro_list'] as $hetro_list_key => $single_hetro_list_detail){
			
			$data['hetro_list'][$hetro_list_key]['lead_name'] = $lead_type_details[$single_hetro_list_detail['lead_type']];
			$data['hetro_list'][$hetro_list_key]['user_name'] = $user_list[$single_hetro_list_detail['assigned_to']];
			$data['hetro_list'][$hetro_list_key]['stage_reason'] = $reason_list[$single_hetro_list_detail['stage_reason']];

			$client_member_details = $this->lead_management_model->get_all_conditional_data_sales_db('*', array('comp_mst_id'=>$single_hetro_list_detail['id'],'status'=>'Active'),'customer_dtl','result_array', array(),array(),array(),0,0);			
			// echo "<pre>";print_r($client_member_details);echo"</pre><hr>";exit;

			$data['hetro_list'][$hetro_list_key]['comp_dtl_id'] = null;
			$data['hetro_list'][$hetro_list_key]['member_name'] = null;
			$data['hetro_list'][$hetro_list_key]['designation'] = null;
			$data['hetro_list'][$hetro_list_key]['email'] = null; 
			$data['hetro_list'][$hetro_list_key]['mobile'] = null;
			$member_count = $non_member_count = 0;
			
			if(!empty($client_member_details)){

				foreach($client_member_details as $single_member_details){

					if(strtolower($single_member_details['main_buyer']) == 'yes'){
						// echo "<pre>";print_r($single_member_details);echo"</pre><hr>";exit;

						$data['hetro_list'][$hetro_list_key]['comp_dtl_id'] = $single_member_details['comp_dtl_id'];
						$data['hetro_list'][$hetro_list_key]['member_name'] = $single_member_details['member_name'];
						$data['hetro_list'][$hetro_list_key]['designation'] = $single_member_details['designation'];
						$data['hetro_list'][$hetro_list_key]['email'] = $single_member_details['email']; 
						$data['hetro_list'][$hetro_list_key]['mobile'] = $single_member_details['mobile'];

					}else{
						if(
							$single_member_details['member_name'] = '' && $single_member_details['email'] = '' && $single_member_details['mobile'] = ''){}else{

							if(strtolower($single_member_details['other_member']) == 'yes'){
								$non_member_count++;
							}else{
								$member_count++;
							}
						}
					}
				}
			}
			$data['hetro_list'][$hetro_list_key]['member_count'] = $member_count;
			$data['hetro_list'][$hetro_list_key]['non_member_count'] = $non_member_count;

			$client_last_comment_details = $this->lead_management_model->get_all_conditional_data_sales_db('connected_on, connect_mode, comments', array('comp_mst_id'=>$single_hetro_list_detail['id'], ),'customer_connect','row_array', array(),array('column_name'=>'connected_on', 'column_value'=>'desc'),array(),0,0);			

			$last_contacted_data = $this->get_last_contacted_data($client_last_comment_details);

			$data['hetro_list'][$hetro_list_key]['last_contacted'] = $last_contacted_data['last_contacted'];
			$data['hetro_list'][$hetro_list_key]['comments'] = $last_contacted_data['comments'];
			$data['hetro_list'][$hetro_list_key]['connect_mode'] = $last_contacted_data['connect_mode'];
			
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$data['paggination_data']['limit'] = $limit;
		$data['paggination_data']['offset'] = $offset;
		return $data;
	}

	private function create_search_filter_data($search_filter_form_data, $category_name, $category_source){
			
		$return_array['where'] = array();
		if($category_source == "primary_lead"){

			$return_array['where'] = "customer_data.rank is not null AND product_category.product_name LIKE '%".$category_name."%'";
		}else{
			
			$return_array['where'] = "product_category.product_name LIKE '%".$category_name."%'";
		}

			
		$return_array['search_filter_data'] = array('box' => '', 'sample'=>'', 'lead_type' => '', 'lead_stage' => '', 'company_name' => '', 'country_id' => '', 'region_id' => '', 'assigned_to' => '', 'year' => '', 'lead_priority'=>'');
	
		if(!empty($search_filter_form_data)){
							
			foreach($search_filter_form_data as $form_data){
				
				if(!empty($form_data['value']) || $form_data['value'] === '0'){
						
					switch($form_data['name']){

						case 'lead_type':
							// $return_array['where']['lead_type'] = $form_data['value'];
							$return_array['where'] .= " AND customer_mst.lead_type = '".$form_data['value']."'";$return_array['search_filter_data']['lead_type'] = $form_data['value'];
						break;

						case 'lead_stage':
							// $return_array['where']['lead_stage'] = $form_data['value'];
							$return_array['where'] .= " AND customer_mst.lead_stage = '".$form_data['value']."'";
							$return_array['search_filter_data']['lead_stage'] = $form_data['value'];
						break;

						case 'company_name':
							// $return_array['where']['client_mst_id'] = $form_data['value'];
							$return_array['where'] .= " AND customer_mst.name LIKE '%".$form_data['value']."%'";
							$return_array['search_filter_data']['company_name'] = $form_data['value'];
						break;

						case 'region_name':
							// $return_array['where']['region_id'] = $form_data['value'];
							$return_array['where'] .= " AND customer_mst.region_id = '".$form_data['value']."'";
							$return_array['search_filter_data']['region_id'] = $form_data['value'];
						break;
								
						case 'country_id':
							// $return_array['where']['country_id'] = $form_data['value'];
							$return_array['where'] .= " AND customer_mst.country_id = '".$form_data['value']."'";
							$return_array['search_filter_data']['country_id'] = $form_data['value'];
						break;
									
						case 'assigned_to':
							// $return_array['where']['assigned_to'] = $form_data['value'];
							$return_array['where'] .= " AND customer_mst.assigned_to = '".$form_data['value']."'";
							$return_array['search_filter_data']['assigned_to'] = $form_data['value'];
						break;
								
						case 'box':
							// $return_array['where']['box'] = $form_data['value'];
							$return_array['where'] .= " AND customer_mst.box = '".$form_data['value']."'";
							$return_array['search_filter_data']['box'] = $form_data['value'];
						break;
								
						case 'sample':
							// $return_array['where']['sample'] = $form_data['value'];
							$return_array['where'] .= " AND customer_mst.sample = '".$form_data['value']."'";
							$return_array['search_filter_data']['sample'] = $form_data['value'];
						break;

						case 'year':
							$explode_years = explode("-", $form_data['value']);							
							// $return_array['where']['lead_mst.LAST_PURCHASED >='] = $explode_years[0]'-04-01'; 
							// $return_array['where']['lead_mst.LAST_PURCHASED <='] = $explode_years[1].'-03-31';
							$return_array['where'] .= " AND customer_data.last_purchased >= '".$explode_years[0].'-04-01'."' AND customer_data.last_purchased <= '".$explode_years[1].'-03-31'."'"; 
							$return_array['search_filter_data']['year'] = $form_data['value'];
						break;

						// case 'connect_mode':
						// 	// $return_array['where']['sample'] = $form_data['value'];
						// 	$return_array['where'] .= " AND connect_mode = '".$form_data['value']."'";
						// 	$return_array['search_filter_data']['connect_mode'] = $form_data['value'];
						// break;

						// case 'lead_priority':

						// 		// $return_array['where']['lead_priority'] = $form_data['value'];
						// 		$return_array['where'] .= " AND customer_mst.lead_priority = '".$form_data['value']."'";
						// 		$return_array['search_filter_data']['lead_priority'] = $form_data['value'];
						// break;

						default:
								
						break;		
					}						
				}					
			}				
		}
		// echo "<pre>";print_r($return_array['where']);echo"</pre><hr>";exit;
		return $return_array;
	}

	private function get_last_contacted_data($data) {
		
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$return_array = array('last_contacted' => '', 'comments' => '', 'connect_mode' => '');
		if(!empty($data)){
			
			$date1 = date_create($data['connected_on']);
			$date2 = date_create(date('Y-m-d'));
			$diff_obj = date_diff($date1, $date2);
			$diff = $diff_obj->format("%a");
			if($diff < 8){
				$return_array['last_contacted'] = $diff.' days ago';
			}else if($diff < 30){
				$weeks = round($diff / 7);
				$return_array['last_contacted'] = $weeks.' weeks ago';
			}else if($diff < 365){
				$months = round($diff / 30);
				$return_array['last_contacted'] = $months.' months ago';
			}else if($diff > 365){
				$years = round($diff / 365);
				$return_array['last_contacted'] = $years.' years ago';
			}
			$return_array['comments'] = $data['comments'];
			$return_array['connect_mode'] = $data['connect_mode'];
		}
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		return $return_array;
	}

	private function get_member_detail($comp_mst_id){
		
		$member_data= $this->lead_management_model->get_all_conditional_data_sales_db('*', array('comp_mst_id'=> $comp_mst_id, 'other_member'=>'No', 'status'=>'Active'), 'customer_dtl','result_array', array(),array('column_name'=>'comp_dtl_id', 'column_value'=>'asc'),array(),0,0);

		if(!empty($member_data)){
				
			foreach($member_data as $member_key => $single_member_detail){				
					
				$member_lead_connect = $this->lead_management_model->get_all_conditional_data_sales_db('*', array('comp_detail_id'=>$single_member_detail['comp_dtl_id']), 'customer_connect','row_array', array(),array('column_name'=>'connected_on', 'column_value'=>'desc'),array(),0,0);
					
				if(!empty($member_lead_connect)){
						
					$member_data[$member_key]['connected_on'] = date('d-m-Y', strtotime($member_lead_connect['connected_on']));
						
					$member_data[$member_key]['connect_mode'] = $member_lead_connect['connect_mode'];				
				}else{
						
					$member_data[$member_key]['connected_on'] = '-';
					$member_data[$member_key]['connect_mode'] = '-';
				}					
			}
		}			
		// echo "<pre>";print_r($member_data);echo"</pre><hr>";exit;
		return $member_data;
	}

	private function get_other_member_detail($comp_mst_id){

		$other_member = $this->lead_management_model->get_all_conditional_data_sales_db('*', array('comp_mst_id'=> $comp_mst_id, 'other_member'=>'Yes', 'main_buyer'=> 'No', 'status'=>'Active'), 'customer_dtl','result_array', array(),array('column_name'=>'comp_dtl_id', 'column_value'=>'asc'),array(),0,0);
		
		if(!empty($other_member)){
			
			foreach($other_member as $other_member_key => $other_member_detail){	
					// echo "<pre>";print_r($other_member_detail);echo"</pre><hr>";exit;

					$other_member_lead_connect = $this->lead_management_model->get_all_conditional_data_sales_db('*', array('comp_mst_id'=> $comp_mst_id, 'comp_detail_id'=>$other_member_detail['comp_dtl_id']), 'customer_connect','row_array', array(),array('column_name'=>'connected_on', 'column_value'=>'desc'),array(),0,0);
					// echo "<pre>";print_r($other_member_lead_connect);echo"</pre><hr>";exit;

					if(!empty($other_member_lead_connect)){

						$other_member[$other_member_key]['connected_on'] = date('d-m-y', strtotime($other_member_lead_connect['connected_on']));

						$other_member[$other_member_key]['connect_mode'] = $other_member_lead_connect['connect_mode'];
					}else{

						$other_member[$other_member_key]['connected_on'] = '-';	
						$other_member[$other_member_key]['connect_mode'] = '-';
					}
					
				}
			}
		// echo "<pre>";print_r($other_member);echo"</pre><hr>";exit;
		return $other_member;
	}

	private function get_indian_time($date_format = 'Y-m-d H:i:s'){

		date_default_timezone_set('Asia/Kolkata');
		return date($date_format);
	}
}