<?php foreach($member_history as $history_key => $history_details) {?>
<tr>
    <th scope="row"><?php echo $history_key+1; ?></th>
    <td style="font-weight: 500;">
        <?php echo (!empty($member_list[$history_details['comp_detail_id']])) ? $member_list[$history_details['comp_detail_id']] : 'No Name Found'; ?>
    </td>
    <td style="font-weight: 500;">
    <?php 
        if(!empty($history_details['connected_on'])){
            echo date('j, F Y', strtotime($history_details['connected_on']));
        }
    ?>
    </td>
    <td style="font-weight: 500;"><?php echo $history_details['connect_mode']; ?></td>
    <td style="font-weight: 500;"><?php echo $history_details['email_sent']; ?></td>
    <td style="font-weight: 500;"><?php echo $history_details['comments']; ?></td>
</tr>
<?php } ?>