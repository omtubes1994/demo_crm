<table class="table table-hover" id="buyer_table">
	<thead>
		<tr>
			<th colspan="4" style="text-align:center">Buyers</th>
		</tr>
	</thead>
	<thead class="thead-light">
		<tr>
			<th>No</th>
			<th>Name</th>
			<th>Email</th>
			<th>Whatsapp</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php if(!empty($buyer_member)){ ?>
			<?php foreach($buyer_member as $buyer_member_key => $single_buyer_member){ ?>
				<tr class="all_tr comp_dtl_id_<?php echo $single_buyer_member['comp_dtl_id'];?>">
					<th scope="row"><?php echo $buyer_member_key+1; ?></th>
					<td style="font-weight: 500;"><?php echo $single_buyer_member['member_name'];?></td>
					<td style="font-weight: 500;"><?php echo $single_buyer_member['email'];?></td>
					<td style="font-weight: 500;"><?php echo $single_buyer_member['mobile'];?></td>
					<td style="font-weight: 500;">
						<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md open_member_contact_model pull-right" title="Contact" comp_dtl_id="<?php echo $single_buyer_member['comp_dtl_id'];?>" comp_mst_id="<?php echo $single_buyer_member['comp_mst_id'];?>" member_name="<?php echo $single_buyer_member['member_name'];?>" country_name="<?php echo $country_name; ?>">
							<i class="la la-comment"></i>
						</button>
						&nbsp
						<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md open_member_contact_history_model pull-right" title="history" comp_dtl_id="<?php echo $single_buyer_member['comp_dtl_id'];?>" comp_mst_id="<?php echo $single_buyer_member['comp_mst_id'];?>" member_name="<?php echo $single_buyer_member['member_name'];?>" country_name="<?php echo $country_name; ?>">
							<i class="la la-eye"></i>
						</button>
					</td>
				</tr>
			<?php } ?>
		<?php } ?>
    </tbody>
</table>

<table class="table table-hover" id="member_table">
	<thead>
		<tr>
			<th colspan="4" style="text-align:center">Other Members</th>
		</tr>
	</thead>
	<thead class="thead-light">
		<tr>
			<th>No</th>
			<th>Name</th>
			<th>Email</th>
			<th>Whatsapp</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php if(!empty($other_member)){ ?>
			<?php foreach($other_member as $other_member_key => $single_other_member){ ?>
				<tr class="all_tr comp_dtl_id_<?php echo $single_other_member['comp_dtl_id'];?>">
					<th scope="row"><?php echo $other_member_key+1; ?></th>
					<td style="font-weight: 500;"><?php echo $single_other_member['member_name'];?></td>
					<td style="font-weight: 500;"><?php echo $single_other_member['email'];?></td>
					<td style="font-weight: 500;"><?php echo $single_other_member['mobile'];?></td>
					<td style="font-weight: 500;">
						<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md open_member_contact_model pull-right" title="Contact" comp_dtl_id="<?php echo $single_other_member['comp_dtl_id'];?>" comp_mst_id="<?php echo $single_other_member['comp_mst_id'];?>" member_name="<?php echo $single_other_member['member_name'];?>" country_name="<?php echo $country_name; ?>">
							<i class="la la-comment"></i>
						</button>
						&nbsp
						<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md open_member_contact_history_model pull-right" title="history" comp_dtl_id="<?php echo $single_other_member['comp_dtl_id'];?>" comp_mst_id="<?php echo $single_other_member['comp_mst_id'];?>" member_name="<?php echo $single_other_member['member_name'];?>" country_name="<?php echo $country_name; ?>">
							<i class="la la-eye"></i>
						</button>
					</td>
				</tr>
			<?php } ?>
		<?php } ?>
    </tbody>
</table>