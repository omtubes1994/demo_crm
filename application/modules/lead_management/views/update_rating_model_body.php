<div class="row">
    <input type="text" class="form-control" name="id" value="<?php echo $comp_mst_id; ?>" hidden/>			
	<div class="col-md-12 form-group row">
	    <label class="col-form-label col-lg-3 col-sm-12">Priority range</label>
		<div class="col-lg-8 col-md-9 col-sm-12">
            <div class="kt-ion-range-slider">
			    <input type="hidden" id="lead_priority_value" name="lead_priority" value="<?php echo $lead_priority;?>">
			</div>
		</div>
	</div>
    
    <div class="col-md-12 form-group row lead_priority_reason" style="display:<?php echo ($lead_priority != 0) ? 'block' : 'none';?>">
	    <label class="col-form-label col-lg-4 col-sm-12">Priority Reason</label>
		<div class="kt-font-info col-lg-6 col-md-9 col-sm-12" id="priority_reason">
		    <input type="text" class="form-control" id="lead_priority_reason" name="priority_reason" value="<?php echo $priority_reason;?>">
			<small class="form-text text-danger" id="reason_error"></small>
		</div>
	</div>
</div>