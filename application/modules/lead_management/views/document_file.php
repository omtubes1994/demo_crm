<table class="table table-striped" style="margin: 0% 0% 0% 0%;">
	<thead>
		<tr>
			<th scope="col" class="kt-align-center" style="width:5%;">#</th>
			<th scope="col" class="kt-align-center" style="width:55%;">File Name</th>
			<th scope="col" class="kt-align-center" style="width:15%;">Date</th>
			<th scope="col" class="kt-align-center" style="width:25%;">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php if(!empty($document_details['document_file'])) {?>
                <?php foreach(json_decode($document_details['document_file'], true) as $single_document_key => $single_document_details) {?>
				<tr>
					<th scope="row" class="kt-align-center"><?php echo $single_document_key+1;?></th>
					<td class="kt-font-bold kt-align-center"><?php echo $single_document_details['file_name']; ?></td>
					<td class="kt-font-bold kt-align-center"><?php echo date('j F, Y', strtotime($single_document_details['file_add_date'])); ?></td>
					<td class="kt-font-bold kt-align-center">
						<div class="row">
							<div class="col-md-12">

								<a class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View <?php echo $single_document_details['file_name']; ?>" href="<?php echo base_url('assets/client_document/'.$single_document_details['file_name']);?>" style="color: #212529;" target="_blank">
						    		<i class="la la-eye kt-font-bolder" style="color: #212529;"></i>
							    </a>	
								<a class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View <?php echo $single_document_details['file_name']; ?>" href="<?php echo base_url('assets/client_document/'.$single_document_details['file_name']);?>" style="color: #212529;" target="_blank" download>
									<i class="fa fa-download kt-font-bolder" style="color: #212529;"></i>
								</a>
								<button class="btn btn-sm btn-clean btn-icon btn-icon-md delete_file" title="Delete" id="<?php echo $document_details['id'];?>" file_name="<?php echo $single_document_details['file_name'];?>" style="color: #212529;">
									<i class="la la-trash kt-font-bolder" style="color: #212529;"></i>
								</button>
							</div>
						</div>
					</td>
				</tr>
                <?php } ?>
			<?php } else{ ?>

				<th class="row"></th>
				<td class="kt-font-bold kt-align-right">Data Not Found !!</td>
				<td></td>
				<td></td>
			<?php }?>
	</tbody>
</table>