<div class="form-group lead_font row">
    <label class="col-xl-1" style="text-align:right;"> Comment :</label>
    <div class="col-xl-10">
        <textarea class="form-control" id="special_comment" placeholder="Type here..."></textarea>
    </div>
</div>
<br>
<h4>Comment History</h4>
<div class="comment_history">
	<table class="table table-bordered">
	    <thead>
		    <tr>
			   	<th>Sr</th>
			   	<th>Comment</th>
			   	<th>Date</th>
		    </tr>
		</thead>
		<tbody>
            <?php if(!empty($comment_details['special_comment'])) {?>
                <?php foreach(json_decode($comment_details['special_comment'], true) as $single_comment_key => $single_comment) {?>
                    <tr>
                        <td><?php echo $single_comment_key+1; ?></td>
                        <td><?php echo $single_comment['message']; ?></td>
                        <td><?php echo date('j F, Y', strtotime($single_comment['date_time'])); ?></td>
                    </tr>
                <?php } ?>
            <?php } ?>
		</tbody>
	</table>
</div>