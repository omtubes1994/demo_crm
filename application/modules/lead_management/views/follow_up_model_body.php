<div class="row">
	<input type="text" class="form-control" name="comp_mst_id" value="<?php echo $comp_mst_id; ?>" hidden/>
	<input type="text" class="form-control" name="category_id" value="<?php echo $category_id; ?>" hidden/>
	<input type="text" class="form-control" name="country_id" value="<?php echo $country_id; ?>" hidden/>

	<div class="col-md-4 form-group">
    	<label for="contact_date">Connect Date</label>
		<input type="text" class="form-control" name="connected_on" value="<?php echo date('Y-m-d', strtotime($connected_on));?>" readonly>
  	</div>
	<div class="col-md-4 form-group">
        <label for="contact_date">Connect Mode</label>
        <select class="form-control" name="connect_mode">
        	<option value="">Select</option>
        	<option value="whatsapp">Whatsapp</option>
            <option value="call">Call Connected</option>
            <option value="call_attempted">Call Attempted</option>
            <option value="linkedln">Linkedln</option>
            <option value="email">Email</option>
        </select>
    </div>
	<div class="col-md-4 form-group">
        <label for="contact_date">Email Sent</label>
        <select class="form-control" name="email">
        	<option value="">Select</option>
        	<option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>   		               
</div>
<div class="row">	                  
	<div class="col-md-6 form-group">
    	<label for="contact_date">Comments</label>
		<textarea class="form-control" name="comments"></textarea> 
  	</div>
</div>
<div class="row">
	<div class="col-md-12 form-group" style="justify-content: right;">
		<button class="btn btn-success save_followup" type="reset" id="comp_dtl_id" style="float: right;">Submit</button>
	</div>
</div>

<hr/>
<h4>Follow Up History</h4>
<div>
	<table class="table table-bordered">
	   <thead>
		   <tr>
			   	<th>Sr</th>
			   	<th>Connected On</th>
			   	<th>Connect Mode</th>
			   	<th>Email Sent</th>
			   	<th>Comments</th>
			   	<th>Entered By</th>
		   </tr>
		</thead>
		<tbody>
		<?php if(!empty($follow_up_details)){ ?>
			<?php foreach ($follow_up_details as $follow_up_details_key => $single_follow_up) { ?>
			  	<tr>
				   	<td><?php echo $follow_up_details_key+1; ?></td>
				   	<td><?php echo date('d-m-Y', strtotime($single_follow_up['connected_on'])); ?></td>
				   	<td><?php echo $single_follow_up['connect_mode']; ?></td>
				   	<td><?php echo $single_follow_up['email_sent']; ?></td>
				   	<td><?php echo $single_follow_up['comments']; ?></td>
				   	<td><?php echo $single_follow_up['user_name']; ?></td>
			   </tr>
			<?php } ?>
		<?php } ?>
		</tbody>
	</table>
</div>

