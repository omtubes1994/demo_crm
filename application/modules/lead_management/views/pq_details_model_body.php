<div class="form-group lead_font row">
    <div class="col-md-3">
        <label>Registration ID</label>
        <input class="form-control" type="text" name="registration_no" value="<?php echo $pq_details['registration_no'];?>">
    </div>
    <div class="col-md-3">
        <label>Registration Method</label>
        <select class="form-control" name="registration_method">
            <option value="">Select</option>
            <option value="online" <?php echo ($pq_details["registration_method"] == 'online') ? 'selected': '';?>>Online</option>
            <option value="offline" <?php echo ($pq_details["registration_method"] == 'offline') ? 'selected': '';?>>Offline</option>
        </select>
    </div>
    <div class="col-md-3">
        <label>ID</label>
        <textarea class="form-control" name="portal_id"><?php echo $pq_details['portal_id'];?></textarea>
    </div>
    <div class="col-md-3">
        <label>Password</label>
        <textarea class="form-control" name="portal_password"><?php echo $pq_details['portal_password'];?></textarea>
    </div>
</div>
<div class="form-group lead_font row">
    <div class="col-md-6">
        <label>Important Links</label>
        <textarea class="form-control" name="important_links"><?php echo $pq_details['important_links'];?></textarea>
    </div>
    <div class="col-md-3"></div>
    <?php if(!empty($pq_details['id'])){ ?>
    <div class="col-md-3">
        <button type="button" class="btn-sm btn btn-label-primary btn-bold pq_comment" style="margin-top: 40px;" title="PQ Comment" comp_mst_id="<?php echo $pq_details['comp_mst_id'];?>" pq_id="<?php echo $pq_details['id'];?>">
            <i class="la la-comment"> Comments </i>
        </button>
    </div>
    <?php } ?>
</div>
<br>


<h4>Comment History</h4>
<div class="comment_history">
	<table class="table table-bordered">
	    <thead>
		    <tr>
			   	<th>Sr</th>
			   	<th>Comment</th>
			   	<th>Date</th>
		    </tr>
		</thead>
		<tbody>
            <?php if(!empty($pq_details['comment'])) {?>
                <?php foreach(json_decode($pq_details['comment'], true) as $single_comment_key => $single_comment) {?>
                    <tr>
                        <td><?php echo $single_comment_key+1; ?></td>
                        <td><?php echo $single_comment['message']; ?></td>
                        <td><?php echo date('j F, Y', strtotime($single_comment['date_time'])); ?></td>
                    </tr>
                <?php } ?>
            <?php } ?>
		</tbody>
	</table>
</div>