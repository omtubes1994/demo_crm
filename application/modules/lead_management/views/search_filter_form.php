<form class="kt-form kt-form--fit kt-margin-b-20" id="search_lead_list_form">
    <div class="lead_font" id="search_filter_div">
        <div class="row kt-margin-b-20"> 
            <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                <label>Last Contact:</label>
                <select class="form-control lead_select_picker" name="last_contacted">
                    <option value="">Last Contacted</option>
					<?php 
                    foreach($search_filter_form_data['last_contacted'] as $single_details){
                        
                        echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
                    }
                    ?>                
                </select>
            </div>                  
            <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                <label>Last Purchased:</label>
                <select class="form-control lead_select_picker" name="last_purchased" multiple>
                    <option value="">Last Purchased</option>
					<?php 
                    foreach($search_filter_form_data['last_purchased'] as $single_details){
                        
                        echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
                    }
                    ?>                
                </select>
            </div>
            <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                <label>Last Purchased Month:</label>
                <select class="form-control lead_select_picker" name="last_purchased_month" id="last_purchased_month" multiple>
                    <option value="">Last Purchased Month</option>
                    <option value="Apr" <?php echo (array_key_exists('Apr', $search_filter_form_data['last_purchased_month'])) ? 'selected' : ''; ?>>April</option>
                    <option value="May" <?php echo (array_key_exists('May', $search_filter_form_data['last_purchased_month'])) ? 'selected' : ''; ?>>May</option>
                    <option value="Jun" <?php echo (array_key_exists('Jun', $search_filter_form_data['last_purchased_month'])) ? 'selected' : ''; ?>>June</option>
                    <option value="Jul" <?php echo (array_key_exists('Jul', $search_filter_form_data['last_purchased_month'])) ? 'selected' : ''; ?>>July</option>
                    <option value="Aug" <?php echo (array_key_exists('Aug', $search_filter_form_data['last_purchased_month'])) ? 'selected' : ''; ?>>August</option>
                    <option value="Sep" <?php echo (array_key_exists('Sep', $search_filter_form_data['last_purchased_month'])) ? 'selected' : ''; ?>>September</option>
                    <option value="Oct" <?php echo (array_key_exists('Oct', $search_filter_form_data['last_purchased_month'])) ? 'selected' : ''; ?>>October</option>
                    <option value="Nov" <?php echo (array_key_exists('Nov', $search_filter_form_data['last_purchased_month'])) ? 'selected' : ''; ?>>November</option>
                    <option value="Dec" <?php echo (array_key_exists('Dec', $search_filter_form_data['last_purchased_month'])) ? 'selected' : ''; ?>>December</option>
                    <option value="Jan" <?php echo (array_key_exists('Jan', $search_filter_form_data['last_purchased_month'])) ? 'selected' : ''; ?>>January</option>
                    <option value="Jan" <?php echo (array_key_exists('Jan', $search_filter_form_data['last_purchased_month'])) ? 'selected' : ''; ?>>February</option>
                    <option value="Mar" <?php echo (array_key_exists('Mar', $search_filter_form_data['last_purchased_month'])) ? 'selected' : ''; ?>>March</option>
                </select>
            </div>
            <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                <label>Box/Sample:</label>
                <select class="form-control lead_select_picker" name="sample_box" multiple>
                    <option value="">Select</option>
                    <?php 
                    foreach($search_filter_form_data['sample_box'] as $single_details){
                        
                        echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="row kt-margin-b-20">
            <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                <label>Lead Type :</label>
                <select class="form-control lead_select_picker" name="lead_type" multiple>
                    <option value="">Select Type</option>
                    <?php 
                    foreach($search_filter_form_data['lead_type'] as $single_details){
                        
                        echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                <label>Lead Stage :</label>
                <select class="form-control lead_select_picker" name="lead_stage" multiple>
                    <option value="">Select Stage</option>                    
                    <?php 
                    foreach($search_filter_form_data['lead_stage'] as $single_details){
                        
                        echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
                    }
                    ?>     
                </select>
            </div>
            <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                <label>Company Name :</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="client_name" name="search_lead_client_name" value="<?php echo $search_filter_form_data['search_lead_client_name']; ?>" placeholder="min 4 character">
                </div>
                <select class="form-control lead_select_picker" name="client_id" data-live-search = "true">
                    <option value="">Select Name</option>
                    <?php foreach($search_filter_form_data['client_id'] as $single_details){

                        echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
                    }  ?>
                </select>
            </div>
            <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile member_div <?php (count($search_filter_form_data['member']) == 0)?'kt-hidden':''; ?>">
                <label>Member:</label>
                <select class="form-control lead_select_picker" name="member" data-live-search = "true">
                    <option value="">Select Name</option>
                    <?php foreach($search_filter_form_data['member'] as $single_details){

                        echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['member_name'].'</option>';
                    }  ?>
                </select>
            </div>
        </div>
        <div class="row kt-margin-b-20">
            <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                <label>Country :</label>
                <select class="form-control lead_select_picker" name="country_id" data-live-search = "true"  multiple>
                    <option value="">Select Country</option>
                    <?php 
                    foreach($search_filter_form_data['country_id'] as $single_details){
                        
                        echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
                    }
                    ?>                          
                </select>
            </div>
            <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                <label>Region :</label>
                <select class="form-control lead_select_picker" name="region_id" data-live-search = "true" multiple>
                    <option value="">Select region</option>
                    <?php 
                    foreach($search_filter_form_data['region_id'] as $single_details){
                        
                        echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-3 kt-margin-b-10-tablet-and-mobile">
                <label>Sales Person :</label>
                <select class="form-control lead_select_picker" name="assigned_to" data-live-search = "true" multiple>
                    <option value="">Select</option>
                    <?php 
                    foreach($search_filter_form_data['assigned_to'] as $single_details){
                        
                        echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                <label>Connect Mode :</label>
                <select class="form-control lead_select_picker" name="connect_mode" data-live-search = "true" multiple>
                    <option value="">Select Connect Mode</option>
                    <?php 
                    foreach($search_filter_form_data['connect_mode'] as $single_details){
                        
                        echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="row kt-margin-b-20">
            <div class="col-md-3 kt-margin-b-10-tablet-and-mobile">
                <label>Brand :</label>
                <select class="form-control lead_select_picker" name="brand_list" data-live-search = "true" multiple>
                    <option value="">Select</option>
                    <?php foreach($search_filter_form_data['brand_list'] as $single_details){
                        
                        echo '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
                    } ?>
                </select>
            </div>
            <div class="col-md-3 kt-margin-b-10-tablet-and-mobile">
                <label>Document Upload :</label>
                <select class="form-control lead_select_picker" name="document_file" data-live-search = "true" multiple>
                    <option value="">Select</option>
                    <?php foreach($search_filter_form_data['document_file'] as $single_details){

                        echo  '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
                    } ?>
                </select>
            </div>
            <div class="col-md-3 kt-margin-b-10-tablet-and-mobile">
                <label>PQ Status :</label>
                <select class="form-control lead_select_picker" name="pq_client_status" data-live-search = "true" multiple>
                    <option value="">Select</option>
                    <?php foreach($search_filter_form_data['pq_client_status'] as $single_details){

                        echo  '<option value="'.$single_details['value'].'" '.$single_details['selected'].'>'.$single_details['name'].'</option>';
                    } ?>
                </select>
            </div>
        </div>
        <div class="kt-separator kt-separator--md kt-separator--dashed"></div>
        <div class="row">
            <div class="col-lg-12">
                <button class="btn btn-primary btn-brand--icon search_lead_list_form_submit" type="reset">
                    <span>
                        <i class="la la-search"></i>
                        <span>Search</span>
                    </span>
                </button>
                &nbsp;&nbsp;
                <button class="btn btn-secondary btn-secondary--icon search_lead_list_form_reset" type="reset">
                    <span>
                        <i class="la la-close"></i>
                        <span>Reset</span>
                    </span>
                </button>
            </div>
        </div>
    </div>
</form>