jQuery(document).ready(function(){

    // alert("hii");
    Leads_Js.init();

    //Common Primary + Hetro
    $('a.add_search_filter').click(function () {

        var button_value = $(this).attr('button_value');
        if (button_value == 'show') {

            $('div.search_filter_data').show();
            $(this).attr('button_value', 'hide');
            $(this).html('Hide Search Filter');
        } else {
            $('div.search_filter_data').hide();
            $(this).attr('button_value', 'show');
            $(this).html('Add Search Filter');
        }
    });

    $('div.search_filter_data').on('click', 'button.search_filter_submit', function () {

        var source = "<?php echo $this->uri->segment(2); ?>";
        if (source == 'primary_lead') {

            set_reset_spinner($('button.search_filter_submit'));
            primary_search_filter();
        } else if (source == 'hetro_lead') {

            set_reset_spinner($('button.search_filter_submit'));
            hetro_search_filter();
        }
    });

    $('div.search_filter_data').on('click', 'button.search_filter_reset', function () {

        var source = "<?php echo $this->uri->segment(2); ?>";
        if (source == 'primary_lead') {

            var category_name = "<?php echo $this->uri->segment(3); ?>";

            if (category_name == 'PROCESS_CONTROL') {
                category_name = 'PROCESS_CONTROL';
            } else if (category_name == 'HAMMER%20UNION') {
                category_name = 'HAMMER UNION';
            }

            ajax_call_function({
                                call_type: 'primary_search_filter',
                                limit: $('input#limit').val(),
                                offset: $('input#offset').val(),
                                category_name: category_name,
                                reset_search_form: [],
            }, 'primary_search_filter');

        } else if (source == 'hetro_lead') {

            var category_name = "<?php echo $this->uri->segment(3); ?>";

            if (category_name == 'Water%20Companies') {
                category_name = 'Water Companies';
            } else if (category_name == 'Chemical%20Companies') {
                category_name = 'Chemical Companies';
            } else if (category_name == 'EPC%20companies') {
                category_name = 'EPC Companies';
            } else if (category_name == 'sugar%20companies') {
                category_name = 'sugar Companies';
            } else if (category_name == 'Heteregenous%20Tubes%20India') {
                category_name = 'Heteregenous Tubes India';
            } else if (category_name == 'pvf%20companies') {
                category_name = 'pvf companies';
            } else if (category_name == 'Miscellaneous%20Leads') {
                category_name = 'Miscellaneous Leads';
            } else if (category_name == 'Forged%20Fittings') {
                category_name = 'Forged Fittings';
            }

            ajax_call_function({
                                call_type: 'hetro_search_filter',
                                limit: $('input#limit').val(),
                                offset: $('input#offset').val(),
                                category_name: category_name,
                                reset_search_form: [],
            }, 'hetro_search_filter');
        }
    });

    $('div.primary_paggination').on('click', 'li.paggination_number', function () {

        primary_search_filter($(this).attr('limit'), $(this).attr('offset'));
    });

    $('div.hetro_paggination').on('change', 'select.limit_change', function () {

        hetro_search_filter($('select.limit_change').val());
    });

    $('div.hetro_paggination').on('click', 'li.paggination_number', function () {

        hetro_search_filter($(this).attr('limit'), $(this).attr('offset'));
    });

    $('div.primary_paggination').on('change', 'select.limit_change', function () {

        primary_search_filter($('select.limit_change').val());
    });

    $('form#update_rating_form').on('change', '#lead_priority_value', function () {

        $('div.lead_priority_reason').hide();
        var val = $(this).val();
        if (val > 0) {
            $('div.lead_priority_reason').show();
        }
    });

    /* $('div#update_rating').on('click', 'button.save_update_rating', function () {

        var priority_reason = $('#lead_priority_reason').val();
        var status = false;
        if (priority_reason == "") {

            $("#reason_error").html("<span class='text-denger'>Please Enter Priority reason</span>");
            status = false;
        } else {
            $("#reason_error").html("");
            status = true;
        }
        // alert(status); 
        if (status == true) {
            ajax_call_function({
                call_type: 'update_rating_model',
                comp_mst_id: $(this).val(),
                update_rating_data: $('form#update_rating_form').serializeArray()
            }, 'update_rating_model');
        }
    }); */


    //primary lead
    $('tbody#primary_list_body').on('click', 'span.box_sample_comment_model', function () {

        ajax_call_function({
            call_type: 'primary_box_sample_comment_modal',
            comp_mst_id: $(this).attr('comp_mst_id'),
        }, 'primary_box_sample_comment_modal');
    });

    $('tbody#primary_list_body').on('click', 'div.member_count_list_model', function () {

        ajax_call_function({
            call_type: 'primary_member_count_modal',
            comp_mst_id: $(this).attr('comp_mst_id'),
        }, 'primary_member_count_modal');
    });

    $('tbody#primary_list_body').on('change', 'select.primary_dropdown', function () {
     
        ajax_call_function({
            call_type: 'update_primary_lead_stage',
            comp_mst_id: $(this).attr('comp_mst_id'),
            lead_stage: $(this).val(),
        }, 'update_primary_lead_stage');
    });

    $('tbody#primary_list_body').on('click', 'a.primary_update_rating', function () {

        ajax_call_function({
            call_type: 'primary_update_rating_model',
            comp_mst_id: $(this).attr('comp_mst_id'),
            lead_priority: $(this).attr('lead_priority'),
            priority_reason: $(this).attr('priority_reason'),
        }, 'primary_update_rating_model');
    });

    // $('form#add_primary_lead').on('change', 'select.lead_type', function () {

    //     $('div.lead_industry').hide();
    //     var val = $(this).val();
    //     // alert(val);
    //     if (val == 3) {
    //         $('div.lead_industry').show();
    //     }
    // });

    $('form#add_primary_lead').on('change', 'select.lead_stage', function () {

        $('div.stage_reason').hide();
        var val = $(this).val();
        if (val == 0) {
            $('div.stage_reason').show();
        }
    });

    $('form#add_primary_lead').on('change', 'select.box_change', function () {

        $('div.box_comment').hide();
        var val = $(this).val();
        if (val == 'Yes') {
            $('div.box_comment').show();
        }
    });

    $('form#add_primary_lead').on('change', 'select.sample_change', function () {

        $('div.sample_comment').hide();
        var val = $(this).val();
        if (val == 'Yes') {
            $('div.sample_comment').show();
        }
    });

    $('form#add_primary_lead').on('change', '.lead_priority', function () {

        $('div.primary_priority_reason').hide();
        var val = $(this).val();
        if (val > 0) {
            $('div.primary_priority_reason').show();
        }
    });

    // $('form#primary_member_details').on('click', 'a.add_primary_member', function () { 
    //     var next_count_number = $(this).attr('next_count_number'); 
    //     // alert(next_count_number);
    //     ajax_call_function({

    //         call_type: 'get_primary_member_body',
    //         next_count_number: next_count_number,
    //     }, 'get_primary_member_body');
    //     $(this).attr('next_count_number', ++next_count_number);
    // });

    // $('form#primary_other_member_details').on('click', 'a.add_primary_other_member', function () { 
    //     var next_number = $(this).attr('next_number'); 
    //     // alert(next_number);
    //     ajax_call_function({

    //         call_type: 'get_primary_other_member_body',
    //         next_number: next_number,
    //     }, 'get_primary_other_member_body');
    //     $(this).attr('next_number', ++next_number);
    // });

    $('a.add_primary_member').click(function () {

        var next_count_number = $(this).attr('next_count_number');
        // alert(next_count_number);
        ajax_call_function({
            call_type: 'get_primary_member_body',
            next_count_number: next_count_number,
        }, 'get_primary_member_body');
        $(this).attr('next_count_number', ++next_count_number);
    });

    $('a.add_primary_other_member').click(function () {

        var next_number = $(this).attr('next_number');
        ajax_call_function({
            call_type: 'get_primary_other_member_body',
            next_number: next_number,
        }, 'get_primary_other_member_body');
        $(this).attr('next_number', ++next_number);
    });

    $('tbody#add_primary_member_body').on('click', 'a.delete_member_details', function () {
        $(this).closest('tr').remove();
    });

    $('tbody#add_primary_other_member_body').on('click', 'a.delete_other_member_details', function () {
        $(this).closest('tr').remove();
    });

    $('tbody#add_primary_member_body').on('click', 'a.primary_member_followup_modal', function () {

        ajax_call_function({
            call_type: 'primary_member_followup_modal',
            comp_dtl_id: $(this).attr('comp_dtl_id'),
            comp_mst_id: $(this).attr('comp_mst_id'),
        }, 'primary_member_followup_modal');
    });

    $('tbody#add_primary_other_member_body').on('click', 'a.primary_other_member_followup_modal', function () {

        ajax_call_function({
            call_type: 'primary_other_member_followup_modal',
            comp_dtl_id: $(this).attr('comp_dtl_id'),
            comp_mst_id: $(this).attr('comp_mst_id'),
        }, 'primary_other_member_followup_modal');
    });

    $('div#primary_followup_model').on('click', 'button.save_followup', function () {

        ajax_call_function({
            call_type: 'save_primary_followup',
            comp_dtl_id: $(this).val(),
            add_followup_data_form: $('form#primary_followup_form').serializeArray()
        }, 'save_primary_followup');
    });

    $('tbody#add_primary_member_body').on('click', 'a.delete_member', function () {
        // $('a.delete_member').click( function () {

        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover Member Details",
            icon: "warning",
            buttons: ["Cancel", "Delete"],
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    ajax_call_function({
                        call_type: 'delete_member',
                        comp_dtl_id: $(this).attr('comp_dtl_id'),
                        comp_mst_id: $(this).attr('comp_mst_id'),
                    }, 'delete_member');
                    swal({
                        title: "Member Deleted",
                        icon: "success",
                    });
                } else {
                    swal({
                        title: "Member Not Delete",
                        icon: "info",
                    });
                }
            });
    });

    $('tbody#add_primary_other_member_body').on('click', 'a.delete_other_member', function () {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover Member Details",
            icon: "warning",
            buttons: ["Cancel", "Delete"],
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    ajax_call_function({
                        call_type: 'delete_other_member',
                        comp_dtl_id: $(this).attr('comp_dtl_id'),
                        comp_mst_id: $(this).attr('comp_mst_id'),
                    }, 'delete_other_member');
                    swal({
                        title: "Member Deleted",
                        icon: "success",
                    });
                } else {
                    swal({
                        title: "Member Not Delete",
                        icon: "info",
                    });
                }
            });
    });

    $('button.save_primary_form').click(function () {

        var comp_mst_id = $(this).attr('comp_mst_id');
        // alert(comp_mst_id);
        ajax_call_function({
            call_type: 'update_primary_lead_data',
            comp_mst_id: comp_mst_id,
            client_details: $('form#add_primary_lead').serializeArray(),
            client_member_details: $('form#primary_member_details').serializeArray(),
            client_other_member_details: $('form#primary_other_member_details').serializeArray(),
        }, 'update_primary_lead_data');
    });

    
    //hetro lead
    $('tbody#hetro_list_body').on('click', 'span.box_sample_comment_model', function () {

        ajax_call_function({
            call_type: 'hetro_box_sample_comment_modal',
            comp_mst_id: $(this).attr('comp_mst_id'),
        }, 'hetro_box_sample_comment_modal');
    });

    $('tbody#hetro_list_body').on('click', 'div.member_count_list_model', function () {

        ajax_call_function({
            call_type: 'hetro_member_count_modal',
            comp_mst_id: $(this).attr('comp_mst_id'),
        }, 'hetro_member_count_modal');
    });

    $('tbody#hetro_list_body').on('change', 'select.hetro_dropdown', function () {

        ajax_call_function({
            call_type: 'update_hetro_lead_stage',
            comp_mst_id: $(this).attr('comp_mst_id'),
            lead_stage: $(this).val(),
        }, 'update_hetro_lead_stage');
    });

    $('tbody#hetro_list_body').on('click', 'a.hetro_update_rating', function () {

        ajax_call_function({
            call_type: 'hetro_update_rating_model',
            comp_mst_id: $(this).attr('comp_mst_id'),
            lead_priority: $(this).attr('lead_priority'),
            priority_reason: $(this).attr('priority_reason'),
        }, 'hetro_update_rating_model');
    });
    

    // $('form#add_hetro_lead_form').on('change', 'select.lead_type', function () {

    //     $('div.lead_industry').hide();
    //     var val = $(this).val();
    //     // alert(val);
    //     if (val == 3) {
    //         $('div.lead_industry').show();
    //     }
    // });

    $('form#add_hetro_lead_form').on('change', 'select.lead_stage', function () {

        $('div.stage_reason').hide();
        var val = $(this).val();
        if (val == 0) {
            $('div.stage_reason').show();
        }
    });

    $('form#add_hetro_lead_form').on('change', 'select.box_change', function () {

        $('div.box_comment').hide();
        var val = $(this).val();
        if (val == 'Yes') {
            $('div.box_comment').show();
        }
    });

    $('form#add_hetro_lead_form').on('change', 'select.sample_change', function () {

        $('div.sample_comment').hide();
        var val = $(this).val();
        if (val == 'Yes') {
            $('div.sample_comment').show();
        }
    });

    $('form#add_hetro_lead_form').on('change', '.lead_priority', function () {

        $('div.primary_priority_reason').hide();
        var val = $(this).val();
        if (val > 0) {

            $('div.primary_priority_reason').show();
        }
    });

    $('a.add_hetro_member').click(function () {

        var next_count_number = $(this).attr('next_count_number');
        ajax_call_function({
            call_type: 'get_hetro_member_body',
            next_count_number: next_count_number,
        }, 'get_hetro_member_body');
        $(this).attr('next_count_number', ++next_count_number);
    });

    $('a.add_hetro_other_member').click(function () {

        var next_number = $(this).attr('next_number');
        ajax_call_function({
            call_type: 'get_hetro_other_member_body',
            next_number: next_number,
        }, 'get_hetro_other_member_body');
        $(this).attr('next_number', ++next_number);
    });

    $('tbody#add_hetro_member_body').on('click', 'a.delete_member_details', function () {
        $(this).closest('tr').remove();
    });

    $('tbody#add_hetro_other_member_body').on('click', 'a.delete_other_member_details', function () {
        $(this).closest('tr').remove();
    });

    $('tbody#add_hetro_member_body').on('click', 'a.hetro_member_followup_modal', function () {

        ajax_call_function({
            call_type: 'hetro_member_followup_modal',
            comp_dtl_id: $(this).attr('comp_dtl_id'),
            comp_mst_id: $(this).attr('comp_mst_id'),
        }, 'hetro_member_followup_modal');
    });

    $('tbody#add_hetro_other_member_body').on('click', 'a.hetro_other_member_followup_modal', function () {

        ajax_call_function({
            call_type: 'hetro_other_member_followup_modal',
            comp_dtl_id: $(this).attr('comp_dtl_id'),
            comp_mst_id: $(this).attr('comp_mst_id'),
        }, 'hetro_other_member_followup_modal');
    });

    $('div#hetro_followup_model').on('click', 'button.save_followup', function () {

        ajax_call_function({
            call_type: 'save_hetro_followup',
            comp_dtl_id: $(this).val(),
            add_followup_data_form: $('form#hetro_followup_form').serializeArray()
        }, 'save_hetro_followup');
    });

    $('tbody#add_hetro_member_body').on('click', 'a.delete_member', function () {
        // $('a.delete_member').click( function () {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover Member Details",
            icon: "warning",
            buttons: ["Cancel", "Delete"],
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    ajax_call_function({
                        call_type: 'delete_member',
                        comp_dtl_id: $(this).attr('comp_dtl_id'),
                        comp_mst_id: $(this).attr('comp_mst_id'),
                    }, 'delete_member');
                    swal({
                        title: "Member Deleted",
                        icon: "success",
                    });
                } else {
                    swal({
                        title: "Member Not Delete",
                        icon: "info",
                    });
                }
            });
    });

    $('tbody#add_hetro_other_member_body').on('click', 'a.delete_other_member', function () {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover Member Details",
            icon: "warning",
            buttons: ["Cancel", "Delete"],
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    ajax_call_function({
                        call_type: 'delete_other_member',
                        comp_dtl_id: $(this).attr('comp_dtl_id'),
                        comp_mst_id: $(this).attr('comp_mst_id'),
                    }, 'delete_other_member');
                    swal({
                        title: "Member Deleted",
                        icon: "success",
                    });
                } else {
                    swal({
                        title: "Member Not Delete",
                        icon: "info",
                    });
                }
            });
    });

    $('button.save_hetro_lead_form').click(function () {

        var comp_mst_id = $(this).attr('comp_mst_id');
        ajax_call_function({
            call_type: 'update_hetro_lead_data',
            comp_mst_id: comp_mst_id,
            client_details: $('form#add_hetro_lead_form').serializeArray(),
            client_member_details: $('form#hetro_member_form').serializeArray(),
            client_other_member_details: $('form#hetro_other_member_form').serializeArray(),
        }, 'update_hetro_lead_data');
    });


    // Add New Lead
    $('a.add_member').click(function () {

        var next_count_number = $(this).attr('next_count_number');
        ajax_call_function({
            call_type: 'get_member_body',
            next_count_number: next_count_number,
        }, 'get_member_body');
        $(this).attr('next_count_number', ++next_count_number);
    });

    $('a.add_other_member').click(function () {

        var next_number = $(this).attr('next_number');
        ajax_call_function({
            call_type: 'get_other_member_body',
            next_number: next_number,
        }, 'get_other_member_body');
        $(this).attr('next_number', ++next_number);
    });

    $('button.save_lead_form').click(function () {

        var comp_mst_id = $(this).attr('comp_mst_id');
        ajax_call_function({
            call_type: 'save_lead_details',
            comp_mst_id: comp_mst_id,
            client_mst_details: $('form#add_lead_form').serializeArray(),
            client_data_details: $('form#add_lead_data_form').serializeArray(),
            client_member_details: $('form#member_form').serializeArray(),
            client_other_member_details: $('form#other_member_form').serializeArray(),
        }, 'save_lead_details');
    });

    $(".customer_name").keyup(function () {
        var search = $(this).val();
        // alert(search);
        if (search != '') {
            $.ajax({
                type: 'POST',
                data: { search: search },
                url: "<?php echo base_url('client/searchClients');?>",
                success: function (res) {
                    var resp = $.parseJSON(res);
                    var leads = '';
                    Object.keys(resp).forEach(function (key) {

                        // leads += '<div style="padding: 5px; width: 90%; cursor: pointer"><a href="<?php echo base_url("lead_management/add_lead/"); ?>' + resp[key].id + '" target="_blank">' + resp[key].name + '</div></a>';                       

                        leads += '<div style="width: 90%;"><a href="<?php echo base_url("lead_management/update_hetro_lead/");?>' + resp[key].id + '" target="_blank">' + resp[key].name + '</a><div>';

                    });

                    if (leads != '') {

                        $("#customer_result_client").html(leads).show();
                    } else {

                        $("#customer_result_client").html(leads).hide();
                    }
                }
            });
        }
    });

    $('form#add_lead_form').on('change', 'select.box_change', function () {

        $('div.box_comment').hide();
        var val = $(this).val();
        if (val == 'Yes') {
            $('div.box_comment').show();
        }
    });

    $('form#add_lead_form').on('change', 'select.sample_change', function () {

        $('div.sample_comment').hide();
        var val = $(this).val();
        if (val == 'Yes') {
            $('div.sample_comment').show();
        }
    });

    $('form#add_lead_form').on('change', '.lead_priority', function () {

        $('div.primary_priority_reason').hide();
        var val = $(this).val();
        if (val > 0) {
            $('div.primary_priority_reason').show();
        }
    });

    $('form#add_lead_form').on('change', 'select.lead_stage', function () {

        $('div.stage_reason').hide();
        var val = $(this).val();
        if (val == 0) {
            $('div.stage_reason').show();
        }
    });

    $('form#add_lead_form').on('change', 'select.country_id', function () {

        $('div.selected_region').hide();
        var val = $(this).val();
        if (val != '') {
            $('div.selected_region').show();
            $("#region_id").val($("#country_id option:selected").attr('region_id'));
        }
    });

    $('tbody#add_member_body').on('click', 'a.delete_member_details', function () {
        $(this).closest('tr').remove();
    });

    $('tbody#add_other_member_body').on('click', 'a.delete_other_member_details', function () {
        $(this).closest('tr').remove();
    });




    $('div.main_tab_name_click').click(function () {

        $('div.' + $('div.main_tab_name_click.active_list').attr('tab-name')).slideUp();
        setTimeout(() => {
            
            $('div.' + $(this).attr('tab-name')).slideDown();
        }, 1000);
        $('div.main_tab_name_click').removeClass('active_list');
        $(this).addClass('active_list');
        $('div.sub_tab_name_click').removeClass('active_list');
        $('tbody#table_body').html('');
    });

    $('li.sub_tab_name_click').click(function(){

        $('li.sub_tab_name_click').attr('data-ktwizard-state', 'pending');
        $(this).attr('data-ktwizard-state','current');
        $('li.sub_tab_name_click').removeClass('active_list');
        $(this).addClass('active_list');
        get_lead_management_tab_details();
    });
    $('a.add_search_filter_form').click(function () {

        var button_value = $(this).attr('button_value');
        if (button_value == 'show') {

            $('div.search_filter').show();
            $(this).attr('button_value', 'hide');
            // $(this).html('Hide Search Filter');
        } else {
            $('div.search_filter').hide();
            $(this).attr('button_value', 'show');
            // $(this).html('Add Search Filter');
        }
    });
    $('div.search_filter').on('click', 'button.search_lead_list_form_submit', function () {


        get_lead_management_tab_details();
    });
        
    $('tbody#table_body').on('click', 'div.member_count_list_model', function () {

        ajax_call_function({
            call_type: 'primary_member_count_modal',
            comp_mst_id: $(this).attr('comp_mst_id'),
        }, 'primary_member_count_modal');
    });

    $('tbody#table_body').on('click', 'a.primary_update_rating', function () {

        ajax_call_function({
            call_type: 'get_lead_rating_details',
            comp_mst_id: $(this).attr('comp_mst_id'),
            lead_priority: $(this).attr('lead_priority'),
            priority_reason: $(this).attr('priority_reason'),
        }, 'get_lead_rating_details');
    });

    $('div#update_rating').on('click', 'button.save_update_rating', function () {

        var priority_reason = $('#lead_priority_reason').val();
        var status = false;
        if (priority_reason == "") {

            $("#reason_error").html("<span class='text-denger'>Please Enter Priority reason</span>");
            status = false;
        } else {
            $("#reason_error").html("");
            status = true;
        }
        // alert(status); 
        if (status == true) {
            ajax_call_function({
                call_type: 'update_rating_model',
                comp_mst_id: $(this).val(),
                update_rating_data: $('form#update_rating_form').serializeArray()
            }, 'update_rating_model');
        }
    });
    // get_lead_management_tab_details('PIPES');
});

function ajax_call_function(data, call_type, url = "<?php echo base_url('lead_management/ajax_function'); ?>") {
    $.ajax({
        type: 'POST',
        data: data,
        url: url,
        dataType: 'JSON',
        success: function (res) {
            if (res.status == 'successful') {
                switch (call_type) {

                    // common response Primary + Hetro
                    case 'primary_box_sample_comment_modal':
                    case 'hetro_box_sample_comment_modal':
                        $('form#box_sample_comment').html('').html(res.box_sample_comment_model_body);
                    break;

                    case 'primary_member_count_modal':
                    case 'hetro_member_count_modal':
                        $('form#member_list_form').html('').html(res.member_count_model_body);
                    break;

                    case 'get_search_filter_div':
                        $('div.search_filter_data').html('').html(res.search_filter_data);
                        Leads_Js.init();
                    break;

                    case 'primary_update_rating_model':
                    case 'hetro_update_rating_model':
                        // console.log(res.comp_mst_id);
                        $('form#update_rating_form').html('').html(res.update_rating_html);
                        $('button[id=comp_mst_id]').val(res.comp_mst_id);
                        Leads_Js.init();
                    break;

                    case 'save_hetro_followup':
                    case 'save_primary_followup':
                        swal({
                            title: "Followup added",
                            icon: "success",
                        });
                    break;

                    case 'update_rating_model':
                        swal({
                            title: "Priority updated",
                            icon: "success",
                        });
                    break;

                    case 'update_hetro_lead_data':
                    case 'update_primary_lead_data':
                        swal({
                            title: "Update added",
                            icon: "success",
                        });
                    break;

                    //primary response
                    case 'primary_search_filter':
                        $('tbody#primary_list_body').html('').html(res.primary_list_body);
                        $('div#search_filter_div').html('').html(res.primary_search_filter);
                        $('div.primary_paggination').html('').html(res.primary_paggination);
                        Leads_Js.init();
                        $('div#primary_list_process').hide();
                    break;

                    case 'get_primary_member_body':
                        $('tbody#add_primary_member_body').append(res.member_body_detail);
                    break;

                    case 'get_primary_other_member_body':
                        // console.log(res.next_number);
                        $('tbody#add_primary_other_member_body').append(res.other_member_body_detail);
                    break;

                    case 'primary_member_followup_modal':
                    case 'primary_other_member_followup_modal':
                        $('form#primary_followup_form').html('').html(res.follow_up_html);
                        $('button[id=comp_dtl_id]').val(res.comp_dtl_id);
                        Leads_Js.init();
                    break;

                    //Hetro response
                    case 'hetro_search_filter':
                        $('tbody#hetro_list_body').html('').html(res.hetro_list_body);
                        $('div#search_filter_div').html('').html(res.hetro_search_filter);
                        $('div.hetro_paggination').html('').html(res.hetro_paggination);
                        Leads_Js.init();
                        $('div#hetro_list_process').hide();
                    break;

                    case 'get_hetro_member_body':
                        // console.log(res.member_body_detail);   
                        $('tbody#add_hetro_member_body').append(res.member_body_detail);
                    break;

                    case 'get_hetro_other_member_body':
                        $('tbody#add_hetro_other_member_body').append(res.other_member_body_detail);
                    break;

                    case 'hetro_member_followup_modal':
                    case 'hetro_other_member_followup_modal':
                        $('form#hetro_followup_form').html('').html(res.follow_up_html);
                        $('button[id=comp_dtl_id]').val(res.comp_dtl_id);
                        Leads_Js.init();
                    break;

                    // Add New Member 
                    case 'get_member_body':
                        $('tbody#add_member_body').append(res.member_body_detail);
                    break;

                    case 'get_other_member_body':
                        // console.log(res.next_number);
                        $('tbody#add_other_member_body').append(res.other_member_body_detail);
                    break;

                    case 'save_lead_details':
                        swal({
                            title: "New Lead Added",
                            icon: "success",
                        });
                        setTimeout(function () {

                            location.reload();
                        }, 1500);
                    break;

                    case 'lead_tab_data':

                        $('tbody#table_body').html('').html(res.table_body);
                        $('div.search_filter').html('').html(res.search_filter);
                        $('div.paggination').html('').html(res.paggination);
                        Leads_Js.init();
                        $('div#table_loader').hide();
                    break;
                    
                    case 'member_details':

                        $('form#member_list_form').html('').html(res.member_count_model_body);
                    break;
                    
                    case 'get_lead_rating_details':
                        // console.log(res.comp_mst_id);
                        $('form#update_rating_form').html('').html(res.update_rating_html);
                        $('button[id=comp_mst_id]').val(res.comp_mst_id);
                        Leads_Js.init();
                    break;
                }
            }
        },
        beforesend: function (response) {
            switch (callType) {
                default:
                break;
            }
        }
    })
}


function primary_search_filter(limit = $('input#limit').val(), offset = $('input#offset').val()) {

    $('div#primary_list_process').show();

    var category_source = "<?php echo $this->uri->segment(2);?>";
    var category_name = "<?php echo $this->uri->segment(3);?>";

    if (category_name == 'PROCESS_CONTROL') {
        category_name = 'PROCESS_CONTROL';
    } else if (category_name == 'HAMMER%20UNION') {
        category_name = 'HAMMER UNION';
    }

    var primary_search_filter = $('form#search_filter_form').serializeArray();
    if (primary_search_filter == '') {

        primary_search_filter = '';
    }
    ajax_call_function(
        {
            call_type: 'primary_search_filter',
            search_form_data: primary_search_filter,
            category_name: category_name,
            category_source: category_source,
            limit: limit,
            offset: offset
        },
        'primary_search_filter'
    );
}

function hetro_search_filter(limit = $('input#limit').val(), offset = $('input#offset').val()) {

    $('div#hetro_list_process').show();

    var category_source = "<?php echo $this->uri->segment(2);?>";
    var category_name = "<?php echo $this->uri->segment(3); ?>";

    if (category_name == 'Water%20Companies') {
        category_name = 'Water Companies';
    } else if (category_name == 'Chemical%20Companies') {
        category_name = 'Chemical Companies';
    } else if (category_name == 'EPC%20companies') {
        category_name = 'EPC Companies';
    } else if (category_name == 'sugar%20companies') {
        category_name = 'sugar Companies';
    } else if (category_name == 'Heteregenous%20Tubes%20India') {
        category_name = 'Heteregenous Tubes India';
    } else if (category_name == 'pvf%20companies') {
        category_name = 'pvf companies';
    } else if (category_name == 'Miscellaneous%20Leads') {
        category_name = 'Miscellaneous Leads';
    } else if (category_name == 'Forged%20Fittings') {
        category_name = 'Forged Fittings';
    }

    var hetro_search_filter = $('form#search_filter_form').serializeArray();
    if (hetro_search_filter == '') {

        hetro_search_filter = '';
    }
    ajax_call_function(
        {
            call_type: 'hetro_search_filter',
            search_form_data: hetro_search_filter,
            category_name: category_name,
            category_source: category_source,
            limit: limit,
            offset: offset
        },
        'hetro_search_filter'
    );
}

function set_reset_spinner(obj, set_unset_flag = true) {
    if (set_unset_flag) {

        $(obj).addClass('kt-spinner');
        $(obj).addClass('kt-spinner--right');
        $(obj).addClass('kt-spinner--sm');
        $(obj).addClass('kt-spinner--light');
    } else {

        $(obj).removeClass('kt-spinner');
        $(obj).removeClass('kt-spinner--right');
        $(obj).removeClass('kt-spinner--sm');
        $(obj).removeClass('kt-spinner--light');
    }
}

//// DESAI CODE START////////

function get_lead_management_tab_details() {

    $('div#table_loader').show();
    ajax_call_function(
        {
            call_type: 'lead_tab_data',
            product_name: $('li.sub_tab_name_click.active_list').attr('tab-name'),
            search_form_data: $('form#search_lead_list_form').serializeArray(),
            // category_name: category_name,
            // category_source: category_source,
            // limit: limit,
            // offset: offset
        },
        'lead_tab_data'
    );
}

//// DESAI CODE END////////

var Leads_Js = function () {

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }

    // Private functions
    var datepicker = function () {
        // enable clear button 
        $('.lead_date_picker').datepicker({
            rtl: KTUtil.isRTL(),
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: true,
            templates: arrows,
            format: 'yyyy-mm-dd',
        });        
    };


    // Private functions
    var demos = function () {

        // min & max values
        $('#lead_priority_value').ionRangeSlider({
            min: 0,
            max: 5,
            // from: lead_start
        });
    }

    return {
        // public functions
        init: function () {
            datepicker();
            demos();
            $('.lead_select_picker').selectpicker();
            $('.lead_search_filter_form').selectpicker();

        }
    };
}();