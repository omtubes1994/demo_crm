$(document).ready(function(){

    Leads_Js.init();
    dropdown_set_click_event();


   
    $('form#add_primary_lead').on('change', 'select.lead_stage', function () {

        $('div.stage_reason').hide();
        var val = $(this).val();
        if (val == 0) {
            $('div.stage_reason').show();
        }
    });

    $('form#add_primary_lead').on('change', 'select.box_change', function () {

        $('div.box_comment').hide();
        var val = $(this).val();
        if (val == 'Yes') {
            $('div.box_comment').show();
        }
    });

    $('form#add_primary_lead').on('change', 'select.sample_change', function () {

        $('div.sample_comment').hide();
        var val = $(this).val();
        if (val == 'Yes') {
            $('div.sample_comment').show();
        }
    });

    $('form#add_primary_lead').on('change', '.lead_priority', function () {

        $('div.primary_priority_reason').hide();
        var val = $(this).val();
        if (val > 0) {
            $('div.primary_priority_reason').show();
        }
    });

    // $('form#primary_member_details').on('click', 'a.add_primary_member', function () { 
    //     var next_count_number = $(this).attr('next_count_number'); 
    //     // alert(next_count_number);
    //     ajax_call_function({

    //         call_type: 'get_primary_member_body',
    //         next_count_number: next_count_number,
    //     }, 'get_primary_member_body');
    //     $(this).attr('next_count_number', ++next_count_number);
    // });

    // $('form#primary_other_member_details').on('click', 'a.add_primary_other_member', function () { 
    //     var next_number = $(this).attr('next_number'); 
    //     // alert(next_number);
    //     ajax_call_function({

    //         call_type: 'get_primary_other_member_body',
    //         next_number: next_number,
    //     }, 'get_primary_other_member_body');
    //     $(this).attr('next_number', ++next_number);
    // });

    $('a.add_primary_member').click(function () {

        var next_count_number = $(this).attr('next_count_number');
        // alert(next_count_number);
        ajax_call_function({
            call_type: 'get_primary_member_body',
            next_count_number: next_count_number,
        }, 'get_primary_member_body');
        $(this).attr('next_count_number', ++next_count_number);
    });

    $('a.add_primary_other_member').click(function () {

        var next_number = $(this).attr('next_number');
        ajax_call_function({
            call_type: 'get_primary_other_member_body',
            next_number: next_number,
        }, 'get_primary_other_member_body');
        $(this).attr('next_number', ++next_number);
    });

    $('tbody#add_primary_member_body').on('click', 'a.delete_member_details', function () {
        $(this).closest('tr').remove();
    });

    $('tbody#add_primary_other_member_body').on('click', 'a.delete_other_member_details', function () {
        $(this).closest('tr').remove();
    });

    $('tbody#add_primary_member_body').on('click', 'a.primary_member_followup_modal', function () {

        ajax_call_function({
            call_type: 'primary_member_followup_modal',
            comp_dtl_id: $(this).attr('comp_dtl_id'),
            comp_mst_id: $(this).attr('comp_mst_id'),
            country_id: $(this).attr('country_id'),
            category_id: $(this).attr('category_id'),
        }, 'primary_member_followup_modal');
    });

    $('tbody#add_primary_other_member_body').on('click', 'a.primary_other_member_followup_modal', function () {

        ajax_call_function({
            call_type: 'primary_member_followup_modal',
            comp_dtl_id: $(this).attr('comp_dtl_id'),
            comp_mst_id: $(this).attr('comp_mst_id'),
            country_id: $(this).attr('country_id'),
            category_id: $(this).attr('category_id'),
        }, 'primary_member_followup_modal');
    });

    $('div#primary_followup_model').on('click', 'button.save_followup', function () {

        ajax_call_function({
            call_type: 'save_primary_followup',
            comp_dtl_id: $(this).val(),
            add_followup_data_form: $('form#primary_followup_form').serializeArray()
        }, 'save_primary_followup');
    });

    $('tbody#add_primary_member_body').on('click', 'a.delete_member', function () {
        // $('a.delete_member').click( function () {

        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover Member Details",
            icon: "warning",
            buttons: ["Cancel", "Delete"],
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    ajax_call_function({
                        call_type: 'delete_member',
                        comp_dtl_id: $(this).attr('comp_dtl_id'),
                        comp_mst_id: $(this).attr('comp_mst_id'),
                    }, 'delete_member');
                    swal({
                        title: "Member Deleted",
                        icon: "success",
                    });
                    setTimeout(function () {

                        location.reload();
                    }, 600);
                } else {
                    swal({
                        title: "Member Not Delete",
                        icon: "info",
                    });
                }
            });
    });

    $('tbody#add_primary_other_member_body').on('click', 'a.delete_other_member', function () {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover Member Details",
            icon: "warning",
            buttons: ["Cancel", "Delete"],
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    ajax_call_function({
                        call_type: 'delete_other_member',
                        comp_dtl_id: $(this).attr('comp_dtl_id'),
                        comp_mst_id: $(this).attr('comp_mst_id'),
                    }, 'delete_other_member');
                    swal({
                        title: "Member Deleted",
                        icon: "success",
                    });
                    setTimeout(function () {

                        location.reload();
                    }, 600);
                } else {
                    swal({
                        title: "Member Not Delete",
                        icon: "info",
                    });
                }
            });
    });

    $('button.save_primary_form').click(function () {

        var comp_mst_id = $(this).attr('comp_mst_id');
        // alert(comp_mst_id);
        ajax_call_function({
            call_type: 'update_primary_lead_data',
            comp_mst_id: comp_mst_id,
            client_details: $('form#add_primary_lead').serializeArray(),
            client_member_details: $('form#primary_member_details').serializeArray(),
            client_other_member_details: $('form#primary_other_member_details').serializeArray(),
        }, 'update_primary_lead_data');
    });

    
    //hetro lead
    
    $('form#add_hetro_lead_form').on('change', 'select.lead_stage', function () {

        $('div.stage_reason').hide();
        var val = $(this).val();
        if (val == 0) {
            $('div.stage_reason').show();
        }
    });

    $('form#add_hetro_lead_form').on('change', 'select.box_change', function () {

        $('div.box_comment').hide();
        var val = $(this).val();
        if (val == 'Yes') {
            $('div.box_comment').show();
        }
    });

    $('form#add_hetro_lead_form').on('change', 'select.sample_change', function () {

        $('div.sample_comment').hide();
        var val = $(this).val();
        if (val == 'Yes') {
            $('div.sample_comment').show();
        }
    });

    $('form#add_hetro_lead_form').on('change', '.lead_priority', function () {

        $('div.primary_priority_reason').hide();
        var val = $(this).val();
        if (val > 0) {

            $('div.primary_priority_reason').show();
        }
    });

    $('a.add_hetro_member').click(function () {

        var next_count_number = $(this).attr('next_count_number');
        ajax_call_function({
            call_type: 'get_hetro_member_body',
            next_count_number: next_count_number,
        }, 'get_hetro_member_body');
        $(this).attr('next_count_number', ++next_count_number);
    });

    $('a.add_hetro_other_member').click(function () {

        var next_number = $(this).attr('next_number');
        ajax_call_function({
            call_type: 'get_hetro_other_member_body',
            next_number: next_number,
        }, 'get_hetro_other_member_body');
        $(this).attr('next_number', ++next_number);
    });

    $('tbody#add_hetro_member_body').on('click', 'a.delete_member_details', function () {
        $(this).closest('tr').remove();
    });

    $('tbody#add_hetro_other_member_body').on('click', 'a.delete_other_member_details', function () {
        $(this).closest('tr').remove();
    });

    $('tbody#add_hetro_member_body').on('click', 'a.hetro_member_followup_modal', function () {

        ajax_call_function({
            call_type: 'hetro_member_followup_modal',
            comp_dtl_id: $(this).attr('comp_dtl_id'),
            comp_mst_id: $(this).attr('comp_mst_id'),
        }, 'hetro_member_followup_modal');
    });

    $('tbody#add_hetro_other_member_body').on('click', 'a.hetro_other_member_followup_modal', function () {

        ajax_call_function({
            call_type: 'hetro_other_member_followup_modal',
            comp_dtl_id: $(this).attr('comp_dtl_id'),
            comp_mst_id: $(this).attr('comp_mst_id'),
        }, 'hetro_other_member_followup_modal');
    });

    $('div#hetro_followup_model').on('click', 'button.save_followup', function () {

        ajax_call_function({
            call_type: 'save_hetro_followup',
            comp_dtl_id: $(this).val(),
            add_followup_data_form: $('form#hetro_followup_form').serializeArray()
        }, 'save_hetro_followup');
    });

    $('tbody#add_hetro_member_body').on('click', 'a.delete_member', function () {
        // $('a.delete_member').click( function () {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover Member Details",
            icon: "warning",
            buttons: ["Cancel", "Delete"],
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    ajax_call_function({
                        call_type: 'delete_member',
                        comp_dtl_id: $(this).attr('comp_dtl_id'),
                        comp_mst_id: $(this).attr('comp_mst_id'),
                    }, 'delete_member');
                    swal({
                        title: "Member Deleted",
                        icon: "success",
                    });
                } else {
                    swal({
                        title: "Member Not Delete",
                        icon: "info",
                    });
                }
            });
    });

    $('tbody#add_hetro_other_member_body').on('click', 'a.delete_other_member', function () {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover Member Details",
            icon: "warning",
            buttons: ["Cancel", "Delete"],
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    ajax_call_function({
                        call_type: 'delete_other_member',
                        comp_dtl_id: $(this).attr('comp_dtl_id'),
                        comp_mst_id: $(this).attr('comp_mst_id'),
                    }, 'delete_other_member');
                    swal({
                        title: "Member Deleted",
                        icon: "success",
                    });
                } else {
                    swal({
                        title: "Member Not Delete",
                        icon: "info",
                    });
                }
            });
    });

    $('button.save_hetro_lead_form').click(function () {

        var comp_mst_id = $(this).attr('comp_mst_id');
        ajax_call_function({
            call_type: 'update_hetro_lead_data',
            comp_mst_id: comp_mst_id,
            client_details: $('form#add_hetro_lead_form').serializeArray(),
            client_member_details: $('form#hetro_member_form').serializeArray(),
            client_other_member_details: $('form#hetro_other_member_form').serializeArray(),
        }, 'update_hetro_lead_data');
    });


    // Add New Lead
    $('a.add_member').click(function () {

        var next_count_number = $(this).attr('next_count_number');
        ajax_call_function({
            call_type: 'get_member_body',
            next_count_number: next_count_number,
        }, 'get_member_body');
        $(this).attr('next_count_number', ++next_count_number);
    });

    $('a.add_other_member').click(function () {

        var next_number = $(this).attr('next_number');
        ajax_call_function({
            call_type: 'get_other_member_body',
            next_number: next_number,
        }, 'get_other_member_body');
        $(this).attr('next_number', ++next_number);
    });

    $('button.save_lead_form').click(function () {

        var comp_mst_id = $(this).attr('comp_mst_id');
        ajax_call_function({
            call_type: 'save_lead_details',
            comp_mst_id: comp_mst_id,
            client_mst_details: $('form#add_lead_form').serializeArray(),
            client_data_details: $('form#add_lead_data_form').serializeArray(),
            client_member_details: $('form#member_form').serializeArray(),
            client_other_member_details: $('form#other_member_form').serializeArray(),
        }, 'save_lead_details');
    });

    $(".customer_name").keyup(function () {
        var search = $(this).val();
        // alert(search);
        if (search != '') {
            $.ajax({
                type: 'POST',
                data: { search: search },
                url: "<?php echo base_url('client/searchClients');?>",
                success: function (res) {
                    var resp = $.parseJSON(res);
                    var leads = '';
                    Object.keys(resp).forEach(function (key) {

                        // leads += '<div style="padding: 5px; width: 90%; cursor: pointer"><a href="<?php echo base_url("lead_management/add_lead/"); ?>' + resp[key].id + '" target="_blank">' + resp[key].name + '</div></a>';                       

                        leads += '<div style="width: 90%;"><a href="<?php echo base_url("lead_management/update_hetro_lead/");?>' + resp[key].id + '" target="_blank">' + resp[key].name + '</a><div>';

                    });

                    if (leads != '') {

                        $("#customer_result_client").html(leads).show();
                    } else {

                        $("#customer_result_client").html(leads).hide();
                    }
                }
            });
        }
    });

    $('form#add_lead_form').on('change', 'select.box_change', function () {

        $('div.box_comment').hide();
        var val = $(this).val();
        if (val == 'Yes') {
            $('div.box_comment').show();
        }
    });

    $('form#add_lead_form').on('change', 'select.sample_change', function () {

        $('div.sample_comment').hide();
        var val = $(this).val();
        if (val == 'Yes') {
            $('div.sample_comment').show();
        }
    });

    $('form#add_lead_form').on('change', '.lead_priority', function () {

        $('div.primary_priority_reason').hide();
        var val = $(this).val();
        if (val > 0) {
            $('div.primary_priority_reason').show();
        }
    });

    $('form#add_lead_form').on('change', 'select.lead_stage', function () {

        $('div.stage_reason').hide();
        var val = $(this).val();
        if (val == 0) {
            $('div.stage_reason').show();
        }
    });

    $('form#add_lead_form').on('change', 'select.country_id', function () {

        $('div.selected_region').hide();
        var val = $(this).val();
        if (val != '') {
            $('div.selected_region').show();
            $("#region_id").val($("#country_id option:selected").attr('region_id'));
        }
    });

    $('tbody#add_member_body').on('click', 'a.delete_member_details', function () {
        $(this).closest('tr').remove();
    });

    $('tbody#add_other_member_body').on('click', 'a.delete_other_member_details', function () {
        $(this).closest('tr').remove();
    });




    $('div.main_tab_name_click').click(function () {

        $('div.' + $('div.main_tab_name_click.active_list').attr('tab-name')).slideUp();
        setTimeout(() => {
            
            $('div.' + $(this).attr('tab-name')).slideDown();
        }, 1000);
        $('div.main_tab_name_click').removeClass('active_list');
        $(this).addClass('active_list');
        $('div.sub_tab_name_click').removeClass('active_list');
        $('tbody#table_body').html('');
    });

    $('li.sub_tab_name_click').click(function(){

        $('li.sub_tab_name_click').attr('data-ktwizard-state', 'pending');
        $(this).attr('data-ktwizard-state','current');
        $('li.sub_tab_name_click').removeClass('active_list');
        $(this).addClass('active_list');
        $('input#lead_limit').val(10);
        $('input#lead_offset').val(0);
        get_lead_management_tab_details();
    });
    $('a.add_search_filter_form').click(function () {

        var button_value = $(this).attr('button_value');
        if (button_value == 'show') {

            $('div.search_filter').show();
            $(this).attr('button_value', 'hide');
        } else {
            $('div.search_filter').hide();
            $(this).attr('button_value', 'show');
        }
    });
    $('div.search_filter').on('click', 'button.search_lead_list_form_submit', function () {

        get_lead_management_tab_details();
    });

    $('div.search_filter').on('click', 'button.search_lead_list_form_reset', function () {

        $('div#table_loader').show();
        ajax_call_function(
            {
                call_type: 'lead_tab_data',
                product_name: $('li.sub_tab_name_click.active_list').attr('tab-name'),
                search_form_data: [],
                limit: 10,
                offset: 0
            },
            'lead_tab_data'
        );
    });
        
    $('tbody#table_body').on('click', 'div.member_count_list_model', function () {

        ajax_call_function({
            call_type: 'member_details',
            comp_mst_id: $(this).attr('comp_mst_id'),
            country_name: $(this).attr('country_name'),
        }, 'member_details');
    });

    $('div.paggination').on('click', 'li.paggination_number', function () {

        $('input#lead_limit').val($(this).attr('limit'));
        $('input#lead_offset').val($(this).attr('offset'));
        get_lead_management_tab_details();
    });

    $('div.paggination').on('change', 'select.limit_change', function () {

        $('input#lead_limit').val($(this).val());
        get_lead_management_tab_details();
    });
    $('tbody#table_body').on('click', 'span.box_sample_comment_model', function () {

        ajax_call_function({
            call_type: 'get_box_sample_comment_details',
            comp_mst_id: $(this).attr('comp_mst_id'),
        }, 'get_box_sample_comment_details');
    });

    // get_lead_management_tab_details('PIPES');
    $('form#member_list_form').on('click', 'button.open_member_contact_model', function () {

        if ($(this).attr('comp_mst_id') != '' && $(this).attr('comp_dtl_id') != ''){

            $('input[name="comp_mst_id"]').val($(this).attr('comp_mst_id'));
            $('input[name="comp_detail_id"]').val($(this).attr('comp_dtl_id'));
            $('input[name="member_name"]').val($(this).attr('member_name'));
            $('input[name="country_name"]').val($(this).attr('country_name'));
            $('input[name="lead_name"]').val($('li.sub_tab_name_click.active_list').attr('tab-name'));
            $('#member-contact-popup').modal('show');
        }
    });
    $('button.form_member_contact_save').click(function(){

        toastr.info('Data is getting saved. This might take some seconds.');
        ajax_call_function({
            call_type: 'save_contact_info_with_member',
            form_data: $('form#form_member_contact').serializeArray(),
        }, 'save_contact_info_with_member');
    });
    $('form#member_list_form').on('click', 'button.open_member_contact_history_model', function () {

        var comp_mst_id = $(this).attr('comp_mst_id');
        var comp_dtl_id = $(this).attr('comp_dtl_id');
        if (comp_mst_id != '' && comp_dtl_id != '') {
            $('tr.all_tr').css('background-color','');
            $('tr.comp_dtl_id_'+comp_dtl_id).css('background-color','blanchedalmond');
            $('div#member_contact_history_tbody_loader').show();
            $('#member_contact_history').modal('show');
            ajax_call_function({
                call_type: 'get_member_contact_history',
                comp_detail_id: comp_dtl_id,
                comp_mst_id: comp_mst_id,
            }, 'get_member_contact_history');
        }
    });
    $('a.view_last_10_conversation').click(function(){

        var comp_mst_id = $(this).attr('comp_mst_id');
        if(comp_mst_id != 0){
            $('div#member_contact_history_tbody_loader').show();
            $('#member_contact_history').modal('show');
            ajax_call_function({
                call_type: 'get_last_10_contact_history',
                comp_mst_id: $(this).attr('comp_mst_id'),
            }, 'get_last_10_contact_history');
        }
       
    });
    $('tbody#table_body').on('click', 'button.action_comment', function () {

        ajax_call_function({
            call_type: 'member_details',
            comp_mst_id: $(this).attr('comp_mst_id'),
            country_name: $(this).attr('country_name'),
        }, 'member_details');
    });
    
    $('tbody#table_body').on('click', 'button.graph_details', function () { 

        ajax_call_function({
            call_type: 'get_lead_graph_details',
            company_name: $(this).attr('company_name'),
            product_category: $(this).attr('product_category'),
            company_id: $(this).attr('comp_mst_id'),
            module_type: 'lead',
        }, 'get_lead_graph_details',"<?php echo base_url('graph_management/ajax_function'); ?>");
    });

    $('div.search_filter').on('input', 'input#client_name', function () {

        var search_text = $('input#client_name').val();
        if (search_text.length <= 3) {

            toastr.info('Please Enter more than 3 character to update company name!');
            return false;
        }
        $('select[name="client_id"]').html('');
        $('div#table_loader').show();
        $('div.member_div').addClass('kt-hidden');
        ajax_call_function({
            call_type: 'get_client_name_lead',
            client_name: search_text,
        }, 'get_client_name_lead');
    });

    $('div.search_filter').on('change', 'select[name="client_id"]', function () {

        $('div.search_filter_data select[name="member"]').html('').selectpicker('refresh');
        $('div#table_loader').show();
        ajax_call_function({
            call_type: 'get_member_name_lead',
            client_id: $('select[name="client_id"]').val(),
        }, 'get_member_name_lead');
    });
    $("form#add_primary_lead").on('click', '.add_sample_query', function () {

        var lead_id = $(this).attr('comp_mst_id');
        var query_type = $(this).attr('query_type');

        // alert(quote_id);
        $.ajax({
            type: "POST",
            data: { "call_type": "get_sample_query_history", 'sample_id': lead_id, 'sample_type': query_type },
            url: "<?php echo site_url('query/ajax_function'); ?>",
            dataType: 'JSON',
            success: function (res) {

                $('div#add_query').modal('show');
                $('input[name="query_type"]').val(res.sample_type);
                $('input[name="query_reference"]').val(res.reference);
                $('input[name="query_reference_id"]').val(res.reference_id);
                $('input[name="query_creator_id"]').val(res.sales_person_id);
                $('textarea[name="query_text"]').val('');
                $('div.query_type').html(res.query_type_option_tag);
                $('div.lead_sample_query_history').html(res.query_history_html);
                $('.kt-selectpicker').selectpicker();
            }
        });
    });

    $('button.save_sample_query').click(function () {

        $.ajax({
            type: 'POST',
            data: {
                'call_type': 'save_sample_query',
                form_data: $('form#sample_query_form').serializeArray()
            },
            url: "<?php echo base_url('query/ajax_function'); ?>",
            dataType: 'JSON',
            success: function (res) {
                toastr.clear();
                if (res.status == 'successful') {
                    $('div#add_query').modal('hide');
                    toastr.success(res.message);
                } else {

                    toastr.error(res.message);
                }
            },
            beforeSend: function (response) {
                toastr.clear();
                toastr.warning('Saving of the query has begun.');
            }
        });
    });

    $("tbody#table_body").on('click', '.add_pq_and_lead_query', function () {

        $.ajax({
            type: "POST",
            data: { "call_type": "get_pq_and_lead_query_history", 'client_id': $(this).attr('client_id') },
            url: "<?php echo site_url('query/ajax_function'); ?>",
            dataType: 'JSON',
            success: function (res) {

                $('div#add_pq_and_lead_query').modal('show');
                $('input[name="query_type"]').val(res.query_type);
                $('input[name="query_reference"]').val(res.reference);
                $('input[name="query_reference_id"]').val(res.reference_id);
                $('input[name="query_creator_id"]').val(res.sales_person_id);
                $('input[name="query_assigned_id"]').val(res.query_assigned_id);
                $('textarea[name="query_text"]').val('');
                $('div.query_type').html(res.query_type_option_tag);
                $('div.pq_and_lead_query_history').html(res.query_history_html);
                $('.kt-selectpicker').selectpicker();
            }
        });
    });

    $('button.save_pq_and_lead_query').click(function () {

        $.ajax({
            type: 'POST',
            data: {
                'call_type': 'save_pq_and_lead_query',
                form_data: $('form#pq_and_lead_query_form').serializeArray()
            },
            url: "<?php echo base_url('query/ajax_function'); ?>",
            dataType: 'JSON',
            success: function (res) {
                toastr.clear();
                if (res.status == 'successful') {
                    $('div#add_pq_and_lead_query').modal('hide');
                    toastr.success(res.message);
                } else {

                    toastr.error(res.message);
                }
            },
            beforeSend: function (response) {
                toastr.clear();
                toastr.warning('Saving of the query has begun.');
            }
        });
    });

    $('tbody#table_body').on('click', 'button.add_document', function () {

	    ajax_call_function({ call_type: 'get_document_upload_history', client_id: $(this).attr('comp_mst_id') }, 'get_document_upload_history');
    });
    $("div#document_upload_history").on('click', "button.delete_file", function () {

        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this File!",
            icon: "warning",
            buttons: ["Cancel", "Delete"],
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {

                ajax_call_function({ call_type: 'client_document_delete', id: $(this).attr('id'), file_name: $(this).attr('file_name') }, 'client_document_delete');
            } else {
                swal({
                    title: "File is not deleted",
                    icon: "info",
                });
            }
        });
    });

    $('tbody#table_body').on('click', 'button.pq_details', function () {
        ajax_call_function({
            call_type: 'get_pq_details',
            comp_mst_id: $(this).attr('comp_mst_id'),
        }, 'get_pq_details');
    });

    $('button.save_pq_details').click(function () {
        ajax_call_function({
            call_type: 'save_pq_details',
            comp_mst_id: $(this).val(),
            form_data: $('form#form_pq_details').serializeArray(),
        }, 'save_pq_details');
    });

    $('form#form_pq_details').on('click', 'button.pq_comment', function () {

        $('#pq_comment_modal').modal('show');
        var pq_id = $(this).attr('pq_id');
        $('button[id=pq_id]').val(pq_id);
    });

    $('button.save_pq_comment').click(function (event) {

        event.preventDefault();
        const id = $(this).val();
        const comment = $('textarea#pq_comment').val();
        if (typeof id !== 'undefined' && id.length !== 0 && comment.length !== 0) {

            ajax_call_function({
                call_type: 'add_pq_comment_details',
                id: id,
                comment: comment
            }, 'add_pq_comment_details');
        } else {

            swal({
                title: "Comment cannot be empty!",
                text: 'Try again to reply, once you enter comment',
                icon: "info",
            });
        }
    });

    $('tbody#table_body').on('click', 'button.special_comment', function () {
        ajax_call_function({
            call_type: 'get_special_comment',
            comp_mst_id: $(this).attr('comp_mst_id'),
        }, 'get_special_comment');
    });

    $('button.save_special_comment').click(function () {
        ajax_call_function({
            call_type: 'save_special_comment',
            comp_mst_id: $(this).val(),
            comment: $('textarea#special_comment').val(),
        }, 'save_special_comment');
    });

    $("form#pq_and_lead_query_form").on('change', 'select[name="sub_query_type"]', function () {

        var type = $(this).val();
        var assigned_id = $("input[name='query_assigned_id']").val();

        if (type == 'Registration') {

            $("input[name='query_assigned_id']").val(assigned_id);
            $("input[name='query_type']").val("pq_query");
        } else if (type == 'Other') {

            $("input[name='query_assigned_id']").val(2);
            $("input[name='query_type']").val("lead_query");
        }
    });

});

function ajax_call_function(data, call_type, url = "<?php echo base_url('lead_management/ajax_function'); ?>") {
    $.ajax({
        type: 'POST',
        data: data,
        url: url,
        dataType: 'JSON',
        success: function (res) {
            toastr.clear();
            if (res.status == 'successful') {
                switch (call_type) {

                    // common response Primary + Hetro
                    case 'primary_box_sample_comment_modal':
                    case 'hetro_box_sample_comment_modal':
                        $('form#box_sample_comment').html('').html(res.box_sample_comment_model_body);
                    break;

                    case 'primary_member_count_modal':
                    case 'hetro_member_count_modal':
                        $('form#member_list_form').html('').html(res.member_count_model_body);
                    break;

                    case 'get_search_filter_div':
                        $('div.search_filter_data').html('').html(res.search_filter_data);
                        Leads_Js.init();
                    break;

                    case 'primary_update_rating_model':
                    case 'hetro_update_rating_model':
                        // console.log(res.comp_mst_id);
                        $('form#update_rating_form').html('').html(res.update_rating_html);
                        $('button[id=comp_mst_id]').val(res.comp_mst_id);
                        Leads_Js.init();
                    break;

                    case 'save_hetro_followup':
                    case 'save_primary_followup':
                        swal({
                            title: "Followup added",
                            icon: "success",
                        });
                        ajax_call_function({ call_type: 'primary_member_followup_modal', comp_dtl_id: data.comp_dtl_id, comp_mst_id: res.comp_mst_id, country_id: res.country_id, category_id: res.category_id }, 'primary_member_followup_modal');
                    break;

                    case 'update_rating_model':
                        swal({
                            title: "Priority updated",
                            icon: "success",
                        });
                    break;

                    case 'update_hetro_lead_data':
                    case 'update_primary_lead_data':
                        swal({
                            title: "Update added",
                            icon: "success",
                        });
                        setTimeout(function () {

                            location.href = "<?php echo base_url('lead_management/update_primary_lead/'); ?>" + data.comp_mst_id;
                        }, 600);
                    break;

                    //primary response
                    case 'primary_search_filter':
                        $('tbody#primary_list_body').html('').html(res.primary_list_body);
                        $('div#search_filter_div').html('').html(res.primary_search_filter);
                        $('div.primary_paggination').html('').html(res.primary_paggination);
                        Leads_Js.init();
                        $('div#primary_list_process').hide();
                    break;

                    case 'get_primary_member_body':
                        $('tbody#add_primary_member_body').append(res.member_body_detail);
                    break;

                    case 'get_primary_other_member_body':
                        // console.log(res.next_number);
                        $('tbody#add_primary_other_member_body').append(res.other_member_body_detail);
                    break;

                    case 'primary_member_followup_modal':
                    case 'primary_other_member_followup_modal':
                        $('form#primary_followup_form').html('').html(res.follow_up_html);
                        $('button[id=comp_dtl_id]').val(res.comp_dtl_id);
                        Leads_Js.init();
                    break;

                    //Hetro response
                    case 'hetro_search_filter':
                        $('tbody#hetro_list_body').html('').html(res.hetro_list_body);
                        $('div#search_filter_div').html('').html(res.hetro_search_filter);
                        $('div.hetro_paggination').html('').html(res.hetro_paggination);
                        Leads_Js.init();
                        $('div#hetro_list_process').hide();
                    break;

                    case 'get_hetro_member_body':
                        // console.log(res.member_body_detail);   
                        $('tbody#add_hetro_member_body').append(res.member_body_detail);
                    break;

                    case 'get_hetro_other_member_body':
                        $('tbody#add_hetro_other_member_body').append(res.other_member_body_detail);
                    break;

                    case 'hetro_member_followup_modal':
                    case 'hetro_other_member_followup_modal':
                        $('form#hetro_followup_form').html('').html(res.follow_up_html);
                        $('button[id=comp_dtl_id]').val(res.comp_dtl_id);
                        Leads_Js.init();
                    break;

                    // Add New Member 
                    case 'get_member_body':
                        $('tbody#add_member_body').append(res.member_body_detail);
                    break;

                    case 'get_other_member_body':
                        // console.log(res.next_number);
                        $('tbody#add_other_member_body').append(res.other_member_body_detail);
                    break;

                    case 'save_lead_details':
                        swal({
                            title: "New Lead Added",
                            icon: "success",
                        });
                        setTimeout(function () {

                            location.reload();
                        }, 1500);
                    break;




                    case 'lead_tab_data':

                        $('thead#table_head').html('').html(res.table_head);
                        $('tbody#table_body').html('').html(res.table_body);
                        $('div.search_filter').html('').html(res.search_filter);
                        $('div.paggination').html('').html(res.paggination);

                        if (res.lead_tab_wise_count) {
                            updateLeadTabCount(res.lead_tab_wise_count);
                        }

                        Leads_Js.init();
                        dropdown_set_click_event();
                        $('div#table_loader').hide();
                    break;
                    
                    case 'member_details':

                        $('form#member_list_form').html('').html(res.member_count_model_body);
                        $('a.view_last_10_conversation').attr('comp_mst_id', data.comp_mst_id);
                    break;
            
                    case 'get_box_sample_comment_details':

                        $('form#box_sample_comment').html('').html(res.box_sample_comment_model_body);
                    break;

                    case 'save_contact_info_with_member':

                        setTimeout(() => {
            
                            $('#form_member_contact')[0].reset();
                            $('#member-contact-popup').modal('hide');
                            toastr.success('Data is saved successfully!');
                        }, 1000);
                        
                    break;

                    case 'get_member_contact_history':
                    case 'get_last_10_contact_history':
                            
                        $('tbody#member_contact_history_tbody').html('').html(res.tbody);
                        $('div#member_contact_history_tbody_loader').hide();
                    break;

                    case 'get_client_name_lead':

                        $('div.search_filter select[name="client_id"]').html(res.client_list_html).selectpicker('refresh');
                        $('div#table_loader').hide();
                        toastr.info('Company Name is Updated!');
                    break;

                    case 'get_member_name_lead':

                        $('div.member_div').removeClass('kt-hidden');
                        $('div.search_filter select[name="member"]').html(res.client_member_list_html).selectpicker('refresh');
                        $('div#table_loader').hide();
                    break;

                    case 'get_lead_graph_details':

                        set_all_graph_data(res);
                    break;

                    case 'get_document_upload_history':

                        $('div#document_upload_history').html('').html(res.client_document_history);
                        KTDropzoneDemo.init();
                    break;
                    case 'client_document_delete':
                        swal({
                            title: "Document is Deleted successfully",
                            icon: "success",
                        });
                        ajax_call_function({ call_type: 'get_document_upload_history' }, 'get_document_upload_history');
                    break;
                    case 'change_pq_status':
                        swal({
                            title: "PQ Status Changes",
                            icon: "success",
                        });
                        get_lead_management_tab_details();
                    break;
                    case 'get_pq_details':

                        $('form#form_pq_details').html('').html(res.pq_details_model_body);
                        $('button[id=comp_mst_id]').val(data.comp_mst_id);
                    break;
                    case 'save_pq_details':
                        ajax_call_function({ call_type: 'get_pq_details', comp_mst_id: (data.comp_mst_id) }, 'get_pq_details');
                        swal({
                            title: "Data Updated",
                            icon: "success",
                        });
                    break;
                    case 'add_pq_comment_details':

                        ajax_call_function({ call_type: 'get_pq_details', comp_mst_id: res.comp_mst_id }, 'get_pq_details');
                        swal({
                            title: "Comment Updated",
                            icon: "success",
                        });
                    break;
                    case 'get_special_comment':
                        $('form#special_comment_details_form').html('').html(res.special_comment_history);
                        $('button[id=customer_id]').val(data.comp_mst_id);
                    break;
                    case 'save_special_comment':
                        ajax_call_function({ call_type: 'get_special_comment', comp_mst_id: (data.comp_mst_id) }, 'get_special_comment');
                        swal({
                            title: "Data Updated",
                            icon: "success",
                        });
                    break;
                }
            }
        },
        beforeSend: function (response) {
            
            switch (call_type) {
                case 'get_graph_details':
                case 'get_lead_graph_details':
                    $('div#table_loader').show();
                break;
            }
        }
    })
}

function updateLeadTabCount(leadTabCount) {
    Object.keys(leadTabCount).forEach(function (leadType) {
        var count = leadTabCount[leadType] || 0;

        $("li.sub_tab_name_click[tab-name='" + leadType + "'] span").each(function () {
            var existingText = $(this).text();
            var updatedText = existingText.replace(/\(\d+\)/, "(" + count + ")"); // Replace only the number inside parentheses
            $(this).text(updatedText);
        });
    });
}

function set_all_graph_data(response){

    $('h2.graph_company_name').html('').html(response.company_name);
    // quotation_vs_proforma_highchart(response.quotation_vs_proforma);
    export_stats_yearly(response.export_stats_yearly);
    export_stats_yearly_internal(response.export_stats_yearly_internal);
    import_stats(response.import_stats_data);
    production_status_wise(response.production_status_wise);
    exporters_stats(response.exporters_data);
    decision_maker(response.decision_maker_data);
    $("div#leadtable").html('').html(response.additional_information);
    $("div#stats-modal").modal('show');
    $('div#table_loader').hide();
}
function primary_search_filter(limit = $('input#limit').val(), offset = $('input#offset').val()) {

    $('div#primary_list_process').show();

    var category_source = "<?php echo $this->uri->segment(2);?>";
    var category_name = "<?php echo $this->uri->segment(3);?>";

    if (category_name == 'PROCESS_CONTROL') {
        category_name = 'PROCESS_CONTROL';
    } else if (category_name == 'HAMMER%20UNION') {
        category_name = 'HAMMER UNION';
    }

    var primary_search_filter = $('form#search_filter_form').serializeArray();
    if (primary_search_filter == '') {

        primary_search_filter = '';
    }
    ajax_call_function(
        {
            call_type: 'primary_search_filter',
            search_form_data: primary_search_filter,
            category_name: category_name,
            category_source: category_source,
            limit: limit,
            offset: offset
        },
        'primary_search_filter'
    );
}

function hetro_search_filter(limit = $('input#limit').val(), offset = $('input#offset').val()) {

    $('div#hetro_list_process').show();

    var category_source = "<?php echo $this->uri->segment(2);?>";
    var category_name = "<?php echo $this->uri->segment(3); ?>";

    if (category_name == 'Water%20Companies') {
        category_name = 'Water Companies';
    } else if (category_name == 'Chemical%20Companies') {
        category_name = 'Chemical Companies';
    } else if (category_name == 'EPC%20companies') {
        category_name = 'EPC Companies';
    } else if (category_name == 'sugar%20companies') {
        category_name = 'sugar Companies';
    } else if (category_name == 'Heteregenous%20Tubes%20India') {
        category_name = 'Heteregenous Tubes India';
    } else if (category_name == 'pvf%20companies') {
        category_name = 'pvf companies';
    } else if (category_name == 'Miscellaneous%20Leads') {
        category_name = 'Miscellaneous Leads';
    } else if (category_name == 'Forged%20Fittings') {
        category_name = 'Forged Fittings';
    }

    var hetro_search_filter = $('form#search_filter_form').serializeArray();
    if (hetro_search_filter == '') {

        hetro_search_filter = '';
    }
    ajax_call_function(
        {
            call_type: 'hetro_search_filter',
            search_form_data: hetro_search_filter,
            category_name: category_name,
            category_source: category_source,
            limit: limit,
            offset: offset
        },
        'hetro_search_filter'
    );
}

function set_reset_spinner(obj, set_unset_flag = true) {
    if (set_unset_flag) {

        $(obj).addClass('kt-spinner');
        $(obj).addClass('kt-spinner--right');
        $(obj).addClass('kt-spinner--sm');
        $(obj).addClass('kt-spinner--light');
    } else {

        $(obj).removeClass('kt-spinner');
        $(obj).removeClass('kt-spinner--right');
        $(obj).removeClass('kt-spinner--sm');
        $(obj).removeClass('kt-spinner--light');
    }
}

function dropdown_set_click_event() {

    const dropdownItems = document.querySelectorAll("tbody#table_body .dropdown-item.change_status");
    dropdownItems.forEach(item => {
        item.addEventListener("click", function (event) {
            event.preventDefault();
            const value = this.getAttribute("value");
            const id = this.getAttribute("id");
            console.log(id);
            if (typeof id !== 'undefined' && id.length !== 0 && typeof value !== 'undefined' && value.length !== 0) {
                ajax_call_function({
                    call_type: 'change_pq_status',
                    id: id,
                    pq_status: value,
                    is_pq: this.getAttribute("is_pq"),
                }, 'change_pq_status');
            }
        });
    });
}
//// DESAI CODE START////////

function get_lead_management_tab_details() {

    
    $('div#table_loader').show();
    ajax_call_function(
        {
            call_type         :  'lead_tab_data',
            product_name      :   $('li.sub_tab_name_click.active_list').attr('tab-name'),
            search_form_data  :   $('form#search_lead_list_form').serializeArray(),
            limit             :   $('input#lead_limit').val(),
            offset            :   $('input#lead_offset').val()
        },
        'lead_tab_data'
    );
}

//// DESAI CODE END////////

var Leads_Js = function () {

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }

    // Private functions
    var datepicker = function () {
        // enable clear button 
        $('.lead_date_picker').datepicker({
            rtl: KTUtil.isRTL(),
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: true,
            templates: arrows,
            format: 'yyyy-mm-dd',
        });        
    };


    // Private functions
    var demos = function () {

        // min & max values
        $('#lead_priority_value').ionRangeSlider({
            min: 0,
            max: 5,
            // from: lead_start
        });
    }

    return {
        // public functions
        init: function () {
            datepicker();
            demos();
            $('.lead_select_picker').selectpicker();
            $('.lead_search_filter_form').selectpicker();
        }
    };
}();


var KTDropzoneDemo = function () {

    var client_document = function () {
        // set the dropzone container id
        var id = '#kt_dropzone_client_document';

        // set the preview element template
        var previewNode = $(id + " .dropzone-item");
        previewNode.id = "";
        var previewTemplate = previewNode.parent('.dropzone-items').html();
        previewNode.remove();

        var myDropzone4 = new Dropzone(id, { // Make the whole body a dropzone
            url: "<?php echo base_url('lead_management/ajax_function');?>", // Set the url for your upload script location
            params: { 'call_type': 'client_document_upload' },
            parallelUploads: 3,
            previewTemplate: previewTemplate,
            maxFilesize: 5, // Max filesize in MB
            autoQueue: false, // Make sure the files aren't queued until manually added
            previewsContainer: id + " .dropzone-items", // Define the container to display the previews
            clickable: id + " .dropzone-select" // Define the element that should be used as click trigger to select files.
        });

        myDropzone4.on("addedfile", function (file) {
            // Hookup the start button
            file.previewElement.querySelector(id + " .dropzone-start").onclick = function () { myDropzone4.enqueueFile(file); };
            $(document).find(id + ' .dropzone-item').css('display', '');
            $(id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'inline-block');
        });

        // Update the total progress bar
        myDropzone4.on("totaluploadprogress", function (progress) {
            $(this).find(id + " .progress-bar").css('width', progress + "%");
        });

        myDropzone4.on("sending", function (file) {
            // Show the total progress bar when upload starts
            $(id + " .progress-bar").css('opacity', '1');
            // And disable the start button
            file.previewElement.querySelector(id + " .dropzone-start").setAttribute("disabled", "disabled");
        });

        // Hide the total progress bar when nothing's uploading anymore
        myDropzone4.on("complete", function (progress) {
            var thisProgressBar = id + " .dz-complete";
            setTimeout(function () {
                $(thisProgressBar + " .progress-bar, " + thisProgressBar + " .progress, " + thisProgressBar + " .dropzone-start").css('opacity', '0');
            }, 300)
            toastr.success("Document is Uploaded!!!.");
            ajax_call_function({ call_type: 'get_document_upload_history' }, 'get_document_upload_history',);
        });

        // Setup the buttons for all transfers
        document.querySelector(id + " .dropzone-upload").onclick = function () {
            myDropzone4.enqueueFiles(myDropzone4.getFilesWithStatus(Dropzone.ADDED));
        };

        // Setup the button for remove all files
        document.querySelector(id + " .dropzone-remove-all").onclick = function () {
            $(id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
            myDropzone4.removeAllFiles(true);
        };

        // On all files completed upload
        myDropzone4.on("queuecomplete", function (progress) {
            $(id + " .dropzone-upload").css('display', 'none');
        });

        // On all files removed
        myDropzone4.on("removedfile", function (file) {
            if (myDropzone4.files.length < 1) {
                $(id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
            }
        });
    }

    return {
        // public functions
        init: function () {
            client_document();
        }
    };
}();

function quotation_vs_proforma_highchart(highchart_data) {

    if(highchart_data.length === 0){

        $('div.quotation_vs_proforma_body').css('display','hide');
        $('h3.quotation_vs_proforma_title').html('').html('Quotation Vs Proforma Won <span class="kt-font-danger">No Data Found</span>');
        return false;
    }
    $('div.quotation_vs_proforma_body').css('display','block');
    $('h3.quotation_vs_proforma_title').html('').html('Quotation Vs Proforma Won');
    Highcharts.chart('quotation_vs_proforma_highchart', {
        // Create the chart
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            // categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
            categories: highchart_data.category
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total Quotation'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: highchart_data.series
    });
}
function export_stats_yearly(highchart_data) {

    if(highchart_data.length === 0){

        $('div.export_stats_yearly_body').css('display','none');
        $('h3.export_stats_yearly_title').html('').html('Yearly Buying Trends Total <span class="kt-font-danger">No Data Found</span>');
        return false;
    }
    $('div.export_stats_yearly_body').css('display','block');
    $('h3.export_stats_yearly_title').html('').html('Yearly Buying Trends Total: Rupees '+ highchart_data.total);

    highchart_data.series.forEach(serie => {
        serie.data = serie.data.map(value => value === 0 ? null : value);
    });
    Highcharts.chart('export_stats_yearly_container', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: highchart_data.category
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Exporter wise import values'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray'
                }
            }
        },
        legend: {
            enabled: false,
            /*align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false*/
        },
        credits: {
            enabled: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: highchart_data.series
    });
}
function export_stats_yearly_internal(highchart_data) {

    if(highchart_data.length === 0){

        $('div.export_stats_yearly_internal_body').css('display','none');
        $('h3.export_stats_yearly_internal_title').html('').html('Yearly Buying Trends <span class="kt-font-danger">No Data Found</span>');
        return false;
    }
    $('div.export_stats_yearly_internal_body').css('display','block');
    $('h3.export_stats_yearly_internal_title').html('').html('Yearly Buying Trends Total: $'+highchart_data.total);
    Highcharts.chart('export_stats_yearly_internal_container', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: highchart_data.category,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Exporter Wise import values'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>${point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            series: {
                pointPadding: 0.2,
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '${point.y}'
                }
            }
        },
        series: highchart_data.series
    });
}
function import_stats(highchart_data) {

    if(highchart_data.length === 0){

        $('div.import_stats_body').css('display','none');
        $('h3.import_stats_title').html('').html('Top Products <span class="kt-font-danger">No Data Found</span>');
        return false;
    }
    $('div.import_stats_body').css('display','block');
    $('h3.import_stats_title').html('').html('Top Products');
    Highcharts.chart('import_stats_container', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    connectorColor: 'silver'
                }
            }
        },
        series: [{
            name: 'Share',
            data: highchart_data
        }],
        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        },
    });
}
function production_status_wise(highchart_data) {
    
    if(highchart_data.length === 0){

        $('div.production_status_wise_div').css('display','none');
        return false;
    }
    $('div.production_status_wise_div').css('display','block');
    $('h3.production_status_wise_title').html('').html('Total: $'+highchart_data.total);
    Highcharts.chart('production_status_wise', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: highchart_data.category,
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Production Status Wise Total'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>${point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        series: {
            pointPadding: 0.2,
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '${point.y}'
            }
        }
    },
    series: highchart_data.series
    });
}
function exporters_stats(highchart_data){

    if(highchart_data.length === 0){

        $('div.exporter_stats_div').css('display','none');
        return false;
    }
    $('div.exporter_stats_div').css('display','block');
    $('h3.exporter_stats_title').html('').html('Top Exporter');
    Highcharts.chart('exporter_stats_container', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y}',
                    connectorColor: 'silver'
                }
            }
        },
        series: [{
            name: 'Share',
            data: highchart_data
        }],
        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        },
    });
}
function decision_maker(highchart_data) {

    if(highchart_data.length === 0){

        $('div.decision_maker_body').css('display','none');
        $('h3.decision_maker_title').html('').html('Decision Making Person <span class="kt-font-danger">No Data Found</span>');
        return false;
    }
    $('div.decision_maker_body').css('display','block');
    $('h3.decision_maker_title').html('').html('Decision Making Person');
    Highcharts.chart('decision_maker_container', {

        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    connectorColor: 'silver'
                }
            }
        },
        series: [{
            name: 'Decision Making',
            data: highchart_data
        }],
        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        },
    });
}