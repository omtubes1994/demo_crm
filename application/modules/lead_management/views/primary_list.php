<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <style type="text/css">
		.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
			padding-top: 3% !important;
		}
		.layer-white{
		    display: none;
		    position: fixed;
		    top: 0em !important;
		    left: 0em !important;
		    width: 100%;
		    height: 100%;
		    text-align: center;
		    vertical-align: middle;
		    background-color: rgba(255, 255, 255, 0.55);
		    opacity: 1;
		    line-height: 1;
		    -webkit-animation-fill-mode: both;
		    animation-fill-mode: both;
		    -webkit-animation-duration: 0.5s;
		    animation-duration: 0.5s;
		    -webkit-transition: background-color 0.5s linear;
		    transition: background-color 0.5s linear;
		    -webkit-user-select: none;
		    -moz-user-select: none;
		    -ms-user-select: none;
		    user-select: none;
		    will-change: opacity;
		    z-index: 9;
		}
		.div-loader{
		    position: absolute;
			top: 50%;
			left: 50%;
			margin: 0px;
			text-align: center;
			z-index: 1000;
			-webkit-transform: translateX(-50%) translateY(-50%);
			transform: translateX(-50%) translateY(-50%);
		}
		.kt-spinner:before {
		    width: 50px;
		    height: 50px;
		    margin-top: -10px;
		}

		#primary_list_body .primary_dropdown{
		
			background-position: center right;
			background-repeat: no-repeat;
			border: 0.5px solid #d6d4d4;
			border-radius: 0.5px;
			color: #555;
			line-height: 2.5px;
			font-size: inherit;
			margin: 0;
			overflow: hidden;
			padding-top: 8px;
			padding-bottom: 8px;
			/* font-weight: 100px; */
    		cursor: pointer;
			text-overflow: ellipsis;
			white-space: nowrap;
		}

    </style>

    <!-- begin:: Container -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        PRIMARY <?php echo $category_name;?> LIST                       
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">
                            <a href="javascript:;" class="btn btn-brand btn-elevate btn-icon-sm add_search_filter" button_value = "show">
                                Add Search Filter
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body search_filter_data" style="display: none;">
               
            </div>            
        </div>
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body">
                <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <div class="row">                        
                        <div class="col-sm-12">
                            <table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 100%;">
                            
                                <thead>
                                    <tr>
                                        <th width="3%">RANK.</th>
                                        <th width="20%">Lead Details</th>
                                        <th width="10%">Lead Stage</th>
                                        <th width="13%">Name</th>
                                        <th width="15%">Contact</th>						
                                        <th width="3%"></th>
                                        <th width="5%">Assigned To</th>						
                                        <th width="20%">Comments</th>						
                                        <th width="5%">Connect Mode</th>						
                                        <th width="5%">Actions</th>
                                    </tr>
                                </thead>
                                <tbody id="primary_list_body">
                                    <?php $this->load->view('lead_management/primary_list_body');?>
                                </tbody>
                            </table> 
                            <div id="primary_list_process" class="layer-white">
								<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
							</div>                            
                        </div>
                    </div>
                     <div class="row primary_paggination">
						<?php $this->load->view('lead_management/paggination');?>
					</div>
                </div>		
            </div>
        </div>
    </div>
    <!-- end:: Container -->

    <!-- stard:: model -->
	<div class="modal fade" id="member_count_list_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Member Details</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<!-- <style>
						#buyer_table.table-bordered th, #buyer_table.table-bordered td, #member_table.table-bordered th, #member_table.table-bordered td{
							border: 1px solid #ebedf2 !important;
						}
					</style> -->
					<form id="member_list_form">

					</form>
					
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="box_comment_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Comments</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<form id="box_sample_comment">
						
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="update_rating" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Update Rating</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<form id="update_rating_form" onsubmit="return false">
						
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-success save_update_rating" id="comp_mst_id">Save</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="task_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lm" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">New Task</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<form id="add_task_form">

					</form>
				</div>
				<div class="modal-footer">					
					<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label= "Close">Close</button>
	            	<button class="btn btn-success save_task" type="reset" id="client_id" style="float: right;">Save Task</button>
				</div>				
			</div>
		</div>
	</div>
    <!--end::model-->

</div>
<!--end::Content-->
