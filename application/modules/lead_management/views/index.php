<style type="text/css">
    .kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
		padding-top: 3% !important;
	}
	.layer-white{
	    display: none;
	    position: fixed;
	    top: 0em !important;
	    left: 0em !important;
	    width: 100%;
	    height: 100%;
	    text-align: center;
	    vertical-align: middle;
	    background-color: rgba(255, 255, 255, 0.55);
	    opacity: 1;
	    line-height: 1;
	    -webkit-animation-fill-mode: both;
	    animation-fill-mode: both;
	    -webkit-animation-duration: 0.5s;
	    animation-duration: 0.5s;
	    -webkit-transition: background-color 0.5s linear;
	    transition: background-color 0.5s linear;
	    -webkit-user-select: none;
	    -moz-user-select: none;
	    -ms-user-select: none;
	    user-select: none;
	    will-change: opacity;
	    z-index: 9;
	}
	.div-loader{
	    position: absolute;
		top: 50%;
		left: 50%;
		margin: 0px;
		text-align: center;
		z-index: 1000;
		-webkit-transform: translateX(-50%) translateY(-50%);
		transform: translateX(-50%) translateY(-50%);
	}
	.kt-spinner:before {
	    width: 50px;
	    height: 50px;
	    margin-top: -10px;
	}
	ul.menu-tab {
		margin: 0px;
	    padding: 0px;
	    border: 1px solid #E7E7E7;
	    /*border-radius: 50px;*/
	    overflow: hidden;
	    font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
	    font-size: 14px;
	    line-height: 20px;
	    color: #333;
	}
	ul.menu-tab li{
	    display: inline;
	    text-align: center;
	    float: left;
	}
	ul.menu-tab li.active_list a{

        background-color: #343a40 !important;
        border-color: #5578eb !important;
        color: #ffffff !important;
	}
	ul.menu-tab li a{
        cursor: pointer;
	    display: inline-block;
	    outline: none;
	    text-align: center;
	    width: 100%;
	    background: #F5F5F5;
	    /*border-right: 2px solid #fff;*/
	    color: #767676;
	    font-size: 15px;
	    font-family: 'latomedium';
	    background-color: gainsboro;
	    padding: 3px 3px 12px 3px;
	}
    ul.menu-tab li a span{

        top: 6px;
        position: relative;
        font-size: 1.5rem;
        font-weight: 600;
    }
	.form_name{
	    color: #767676 !important;
	    font-size: 14px !important;
	    font-family: 'latomedium' !important;
	    padding: 10px 10px !important; 
	    font-weight: bold !important;
	}
	.kt-badge--gainsboro, .kt-font-gainsboro{
		background-color: gainsboro;
	}

    .tab_padding:not(:last-child){

        padding: 0px 10px 0px 0px;
    }
    ul.menu-tab li.main_tab_name_click a{

        background-color: #5578eb;
        border-color: #5578eb;
        color: #ffffff;
    }
    ul.menu-tab li.tab_name_click a {
        
        background-color: #0abb87;
        border-color: #0abb87;
        color: #ffffff;
    }
    .btn-light{

        background: #f2f3f7;
        color: #959cb6;
        font-weight: 500;
    }
    .btn.active_list{

        background-color: #5578eb;
        border-color: #5578eb;
        color: #ffffff;
    }
    
    #kt_wrapper{
		padding-top: 3% !important;
	}
	.my-scroll{
      border:1px solid #e1e1e1;
      height: 670px;
      width:auto;
      overflow-y: auto;
    }
    .kt-aside--fixed .kt-wrapper {
        padding-left: 130px !important;
    }
    .btn-group {  
        white-space: nowrap;              
    }
    .btn-group .btn {  
        float: none;
        display: inline-block;
    }
    .btn + .dropdown-toggle { 
        margin-left: -4px;
    }
    .lead_font {
        font-size: 1rem;
        font-weight: 500;
        line-height: 1.5rem;
        -webkit-transition: color 0.3s ease;
        transition: color 0.3s ease;
        color: #000;
    }

    .custom-modal-style {
        border: 2px solid #343a40;
        box-shadow: 0 10px 30px rgba(0, 0, 0, 0.5);
        border-radius: 10px;
    }
    .lead_management_font {
        font-size: 1rem;
        font-weight: 500;
        line-height: 1.5rem;
        -webkit-transition: color 0.3s ease;
        transition: color 0.3s ease;
        color: #000;
    }
</style>
<?php $lead_tab_count = $this->session->userdata('lead_tab_count'); ?>  
<div class="kt-grid kt-grid--hor kt-grid--root">
	<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
			<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
				<!-- begin:: Content -->
				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">	
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__body">
                            <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                <div class="row">
                                    <div class="col-sm-12 piping_tab">
                                        <ul class="menu-tab kt-font-bolder">
                                            <li class="tab_padding main_tab_name_click" style="width: 15%;">
                                                <a>
                                                    <span>Piping</span>
                                                </a>
                                            </li>
                                            <?php if(!empty($lead_tab_count['pipe'])){ ?>
                                            <li class="tab_padding tab_name_click sub_tab_name_click " tab-name="PIPES" style="width:18%;">
                                                <a style="">
                                                    <span>Primary Pipe (<?php echo $lead_tab_count['pipe']; ?>)</span>
                                                </a>
                                            </li>
                                            <?php } if(!empty($lead_tab_count['shipyards'])){?>
                                            <li class="tab_padding tab_name_click sub_tab_name_click " tab-name="Shipyards" style="width:14%;">
                                                <a>
                                                    <span>Shipyard (<?php echo $lead_tab_count['shipyards']; ?>)</span>
                                                </a>
                                            </li>
                                            <?php } if(!empty($lead_tab_count['chemical_companies'])){?>
                                            <li class="tab_padding tab_name_click sub_tab_name_click" tab-name="Chemical Companies" style="width:14%;">
                                                <a>
                                                    <span>Chemical (<?php echo $lead_tab_count['chemical_companies']; ?>)</span>
                                                </a>
                                            </li>
                                            <?php } if(!empty($lead_tab_count['water_companies'])){?>
                                            <li class="tab_padding tab_name_click sub_tab_name_click " tab-name="Water Companies" style="width:13%;">
                                                <a>
                                                    <span>Water (<?php echo $lead_tab_count['water_companies']; ?>)</span>
                                                </a>
                                            </li>
                                            <?php } if(!empty($lead_tab_count['epc_companies'])){?>
                                            <li class="tab_padding tab_name_click sub_tab_name_click " tab-name="EPC Companies" style="width:12%;">
                                                <a>
                                                    <span>EPC (<?php echo $lead_tab_count['epc_companies']; ?>)</span>
                                                </a>
                                            </li>
                                            <?php } if(!empty($lead_tab_count['sugar_companies'])){?>
                                            <li class="tab_padding tab_name_click sub_tab_name_click" tab-name="sugar Companies" style="width:14%;">
                                                <a>
                                                    <span>Sugar (<?php echo $lead_tab_count['sugar_companies']; ?>)</span>
                                                </a>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <ul class="menu-tab kt-font-bolder">
                                            <?php if($this->session->userdata('lead_access')['lead_list_instrumentation_tab_access']){?>
                                            <li class="tab_padding main_tab_name_click sub_tab_name_click" tab-name="instrumentation" style="width:15%;">
                                                <a><span>Instrumentation</span></a>
                                            </li>
                                            <?php }else{ ?>
                                                <li class="tab_padding main_tab_name_click" tab-name="instrumentation" style="width:15%;">
                                                    <a><span>Instrumentation</span></a>
                                                </li>
                                            <?php } ?>
                                            <?php if(!empty($lead_tab_count['tube'])){ ?>
                                            <li class="tab_padding tab_name_click sub_tab_name_click " tab-name="TUBES" style="width:15%;">
                                                <a><span>Primary TF (<?php echo $lead_tab_count['tube']; ?>)</span></a>
                                            </li>
                                            <?php } if(!empty($lead_tab_count['process_control'])){?>
                                            <li class="tab_padding tab_name_click sub_tab_name_click " tab-name="PROCESS_CONTROL" style="width:15%;">
                                                <a><span>Primary PC (<?php echo $lead_tab_count['process_control']; ?>)</span></a>
                                            </li>
                                            <?php } if(!empty($lead_tab_count['distributors'])){?>
                                            <li class="tab_padding tab_name_click sub_tab_name_click" tab-name="distributors" style="width:15%;">
                                                <a><span>TF Dist (<?php echo $lead_tab_count['distributors']; ?>)</span></a>
                                            </li>
                                            <?php } if(!empty($lead_tab_count['heteregenous_tubes_india'])){?>
                                            <li class="tab_padding tab_name_click sub_tab_name_click " tab-name="Heteregenous Tubes India" style="width:15%;">
                                                <a><span>TF India (<?php echo $lead_tab_count['heteregenous_tubes_india']; ?>)</span></a>
                                            </li>
                                            <?php } if(!empty($lead_tab_count['pvf_companies'])){?>
                                            <li class="tab_padding tab_name_click sub_tab_name_click " tab-name="pvf companies" style="width:10%;">
                                                <a><span>PVF (<?php echo $lead_tab_count['pvf_companies']; ?>)</span></a>
                                            </li>
                                            <?php } if(!empty($lead_tab_count['hydraulic_fitting'])){ ?>
                                                <li class="tab_padding tab_name_click sub_tab_name_click " tab-name="Hydraulic fitting" style="width:15%;">
                                                <a><span>Hydraulic fitting (<?php echo $lead_tab_count['hydraulic_fitting']; ?>)</span></a>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <ul class="menu-tab kt-font-bolder">
                                            <li class="tab_padding main_tab_name_click" tab-name="piping_sub_tab" style="width:15%;">
                                                <a><span>Other</span></a>
                                            </li>
                                            <?php if(!empty($lead_tab_count['tubing'])){ ?>
                                            <li class="tab_padding tab_name_click sub_tab_name_click " tab-name="TUBING" style="width:20%;">
                                                <a><span>Primary Tube (<?php echo $lead_tab_count['tubing']; ?>)</span></a>
                                            </li>
                                            <?php } if(!empty($lead_tab_count['hammer_union'])){?>
                                            <li class="tab_padding tab_name_click sub_tab_name_click " tab-name="HAMMER UNION" style="width:20%;">
                                                <a><span>Primary HU (<?php echo $lead_tab_count['hammer_union']; ?>)</span></a>
                                            </li>
                                            <?php } if(!empty($lead_tab_count['forged_fittings'])){?>
                                            <li class="tab_padding tab_name_click sub_tab_name_click" tab-name="Forged Fittings" style="width:15%;">
                                                <a><span>Primary FF (<?php echo $lead_tab_count['forged_fittings']; ?>)</span></a>
                                            </li>
                                            <?php } if(!empty($lead_tab_count['miscellaneous_leads'])){?>
                                            <li class="tab_padding tab_name_click sub_tab_name_click " tab-name="Miscellaneous Leads" style="width:15%;">
                                                <a><span>Miscellaneous (<?php echo $lead_tab_count['miscellaneous_leads']; ?>)</span></a>
                                            </li>
                                            <?php } if(!empty($lead_tab_count['fasteners'])){?>
                                            <li class="tab_padding tab_name_click sub_tab_name_click " tab-name="Fasteners" style="width:15%;">
                                                <a><span>Fasteners (<?php echo $lead_tab_count['fasteners']; ?>)</span></a>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <ul class="menu-tab kt-font-bolder">
                                            <li class="tab_padding main_tab_name_click" tab-name="internal_sub_tab" style="width:15%;">
                                                <a><span>Internal Data</span></a>
                                            </li>
                                            <?php if(!empty($lead_tab_count['internal'])){ ?>
                                            <li class="tab_padding tab_name_click sub_tab_name_click " tab-name="internal" style="width:23%;">
                                                <a><span>Primary Internal Data (<?php echo $lead_tab_count['internal']; ?>)</span></a>
                                            </li>
                                            <?php } ?>
                                            <?php if($this->session->userdata('lead_access')['lead_list_pq_tab_access']){
                                                if(!empty($lead_tab_count['pq'])){ ?>
                                                    <li class="tab_padding tab_name_click sub_tab_name_click " tab-name="pq" style="width:23%;">
                                                        <a><span>Pre Qualification (<?php echo $lead_tab_count['pq']; ?>)</span></a>
                                                    </li>
                                                <?php }
                                            } ?>
                                            <li class="tab_padding" style="width: 5%; float: right;">
                                                <a href="javascript:void(0);" class="btn kt-subheader__btn-primary add_search_filter_form" button_value = "show">
                                                    <i class="fa fa-filter" style="float: none !Important;"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>		
                        </div>
                        <div class="kt-portlet__body search_filter" style="display: none;">
                            <?php 
                                $this->load->view(
                                                'lead_management/search_filter_form',
                                                array(
                                                    'search_filter_form_data' => 
                                                        array(
                                                            'sample_box' => array(),
                                                            'lead_type' => array(),
                                                            'lead_stage' => array(),
                                                            'client_id' => array(),
                                                            'search_lead_client_name' => '',
                                                            'member' => array(),
                                                            'country_id' => array(),
                                                            'region_id' => array(),
                                                            'assigned_to' => array(),
                                                            'last_contacted' => array(),
                                                            'last_purchased' => array(),
                                                            'connect_mode' => array(),
                                                            'document_file' => array(),
                                                            'pq_client_status' => array(),
                                                            'brand_list' => array()
                                                        )
                                                )
                                            );
                            ?>
                        </div>
                    </div>
                    <div class="kt-portlet kt-portlet--mobile">
						<div class="kt-portlet__body">
							<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
								<div class="row">
									<div class="col-sm-12">
										<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 1536px;">
                                            <thead id="table_head"></thead>
                                            <tbody id="table_body"></tbody>
										</table>
                                        <div id="table_loader" class="layer-white">
                                            <div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
                                        </div> 
									</div>
								</div>
                                <div class="row paggination">
                                    <?php 
                                        $this->load->view(
                                                        'lead_management/paggination',
                                                        array(
                                                            'paggination_data' => 
                                                                array(
                                                                    'limit' => 10,
                                                                    'offset' => 0,
                                                                    'total_rows' => 0
                                                                )
                                                        )
                                                    );
                                    ?>
                                </div>
							</div>
						</div>
					</div>
                </div>    
                <!-- start:: model -->
                <div class="modal fade" id="member_count_list_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Member Details</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                </button>
                                <a href="javascript:void(0);" class="btn btn-bold btn-label-brand btn-sm view_last_10_conversation" comp_mst_id = 0 title="View Last 10 conversation">Last 10 Conversation
                                </a>
                            </div>
                            <div class="modal-body">
                                <div class="kt-scroll" data-scroll="true" style="height: 600px">
                                    <form id="member_list_form"></form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="box_comment_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Comments</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                </button>
                            </div>
                            <div class="modal-body">
                                <form id="box_sample_comment">
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="modal fade" id="member-contact-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Follow Up</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="post" id="form_member_contact">
                                    <div class="row">
                                        <div class="col-md-4 form-group">
                                            <label for="contact_date">Connect Date</label>
                                            <input type="text" id="contact_date" name="contact_date" class="form-control" value="<?php echo date('d-m-Y'); ?>" readonly>
                                        </div>

                                        <div class="col-md-4 form-group">
                                            <label for="contact_date">Connect Mode</label>
                                            <select class="form-control validate[required]" name="connect_mode" id="connect_mode">
                                                <option value=""></option>
                                                <option value="whatsapp">Whatsapp</option>
                                                <option value="call">Call Connected</option>
                                                <option value="call_attempted">Call Attempted</option>
                                                <option value="linkedin">LinkedIn</option>
                                                <option value="email">Email</option>
                                            </select>
                                        </div>

                                        <div class="col-md-4 form-group">
                                            <label for="contact_date">Email Sent</label>
                                            <select class="form-control validate[required]" name="email_sent" id="email_sent">
                                                <option value=""></option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                        </div>

                                        <div class="col-md-12 form-group">
                                            <label for="contact_details">Comments</label>
                                            <textarea name="contact_details" class="form-control validate[maxSize[100],required]"></textarea>
                                        </div>
                                        <input type="hidden" name="comp_detail_id">
                                        <input type="hidden" name="comp_mst_id">
                                        <input type="hidden" name="member_name">
                                        <input type="hidden" name="country_name">
                                        <input type="hidden" name="lead_name">
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                            <div class="modal-footer">
                               
                                <button class="btn btn-success form_member_contact_save" type="reset">Save!</button>
                                <button class="btn btn-danger" type="button" data-dismiss="modal" aria-label="Close">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="member_contact_history" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl" role="document">
                        <div class="modal-content" style="height:800px;">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">chat History</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="kt-section">
                                    <div class="kt-section__content">
                                        <table class="table table-hover">
                                            <div id="member_contact_history_tbody_loader" class="layer-white">
                                                <div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
                                            </div> 
                                            <thead class="thead-light">
                                                <tr>
                                                    <th style="width:10%;">#</th>
                                                    <th style="width:10%;">Name</th>
                                                    <th style="width:10%;">Contacted On</th>
                                                    <th style="width:10%;">Contact Mode</th>
                                                    <th style="width:10%;">Email Sent</th>
                                                    <th>Comments</th>
                                                </tr>
                                            </thead>
                                            <tbody id="member_contact_history_tbody">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-danger" type="button" data-dismiss="modal" aria-label="Close">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="stats-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl" role="document">
                        <div class="modal-content">
                            <div class="modal-body" style="background: aliceblue;">
                                <div class="row">
                                    <div class="col-12">
                                        <h2 class="kt-font-info graph_company_name">h2. Heading 2</h2>
                                    </div>
                                    <!-- QUOTATION VS PROFORMA -->
                                    <!-- <div class="col-12">
                                        <div class="kt-portlet kt-portlet--height-fluid">
                                            <div class="kt-portlet__head">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title quotation_vs_proforma_title"></h3>
                                                </div>
                                            </div>
                                            <div class="kt-portlet__body quotation_vs_proforma_body" style="display:none;">
                                                <div class="kt-scroll" data-scroll="true" style="height: 400px">
                                                    <figure class="highcharts-figure">
                                                        <div id="quotation_vs_proforma_highchart"></div>
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->
                                    <!-- EXPORT STATS YEARLY -->
                                    <div class="col-6">
                                        <div class="kt-portlet kt-portlet--height-fluid">
                                            <div class="kt-portlet__head">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title export_stats_yearly_title"></h3>
                                                </div>
                                            </div>
                                            <div class="kt-portlet__body export_stats_yearly_body" style="display:none;">
                                                <div class="kt-scroll" data-scroll="true" style="height: 400px">
                                                    <figure class="highcharts-figure">
                                                        <div id="export_stats_yearly_container"></div>
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- EXPORT STATS YEARLY INTERNAL -->
                                    <div class="col-6">
                                        <div class="kt-portlet kt-portlet--height-fluid">
                                            <div class="kt-portlet__head">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title export_stats_yearly_internal_title"></h3>
                                                </div>
                                            </div>
                                            <div class="kt-portlet__body export_stats_yearly_internal_body" style="display:none;">
                                                <div class="kt-scroll" data-scroll="true" style="height: 400px">
                                                    <figure class="highcharts-figure">
                                                        <div id="export_stats_yearly_internal_container"></div>
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- IMPORT STATS -->
                                    <div class="col-6">
                                        <div class="kt-portlet kt-portlet--height-fluid">
                                            <div class="kt-portlet__head">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title import_stats_title"></h3>
                                                </div>
                                            </div>
                                            <div class="kt-portlet__body import_stats_body" style="display:none;">
                                                <div class="kt-scroll" data-scroll="true" style="height: 400px">
                                                    <figure class="highcharts-figure">
                                                        <div id="import_stats_container"></div>
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Production Status Wise Graph -->
                                    <div class="col-6 production_status_wise_div" style="display:none;">
                                        <div class="kt-portlet kt-portlet--height-fluid">
                                            <div class="kt-portlet__head">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title production_status_wise_title"></h3>
                                                </div>
                                            </div>
                                            <div class="kt-portlet__body">
                                                <div class="kt-scroll" data-scroll="true" style="height: 400px">
                                                    <figure class="highcharts-figure">
                                                        <div id="production_status_wise"></div>
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- EXPORTER DATA -->
                                    <div class="col-6 exporter_stats_div" style="display:none;">
                                        <div class="kt-portlet kt-portlet--height-fluid">
                                            <div class="kt-portlet__head">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title exporter_stats_title"></h3>
                                                </div>
                                            </div>
                                            <div class="kt-portlet__body">
                                                <div class="kt-scroll" data-scroll="true" style="height: 400px">
                                                    <figure class="highcharts-figure">
                                                        <div id="exporter_stats_container"></div>
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6" style="display:none;"></div>
                                    <!-- DECISION MAKER -->
                                    <div class="col-6">
                                        <div class="kt-portlet kt-portlet--height-fluid">
                                            <div class="kt-portlet__head">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title decision_maker_title">Decision Making Person</h3>
                                                </div>
                                            </div>
                                            <div class="kt-portlet__body decision_maker_body" style="display:none;">
                                                <div class="kt-scroll" data-scroll="true" style="height: 400px">
                                                    <figure class="highcharts-figure">
                                                        <div id="decision_maker_container"></div>
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- LEAD TABLE -->
                                    <div class="col-6">
                                        <div class="kt-portlet kt-portlet--height-fluid">
                                            <div class="kt-portlet__head">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title">Additional Information</h3>
                                                </div>
                                            </div>
                                            <div class="kt-portlet__body">
                                                <div class="kt-scroll" data-scroll="true" style="height: 400px">
                                                    <figure class="highcharts-figure">
                                                        <div id="leadtable"></div>
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="add_pq_and_lead_query" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-xl" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <form id="pq_and_lead_query_form">
                                    <div class="row">
                                        <!--Begin:: App Content-->
                                        <div class="kt-grid__item kt-grid__item--fluid kt-app__content" id="kt_chat_content" style="width:100%; padding: 0px 24px 0px 24px;">
                                            <div class="kt-chat">
                                                <div class="kt-portlet kt-portlet--head-lg kt-portlet--last">
                                                    <div class="kt-portlet__head">
                                                        <div class="kt-chat__head ">
                                                            <div class="kt-chat__left">

                                                                <!--begin:: Aside Mobile Toggle -->
                                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md kt-hidden-desktop" id="kt_chat_aside_mobile_toggle">
                                                                    <i class="flaticon2-open-text-book"></i>
                                                                </button>

                                                                <!--end:: Aside Mobile Toggle-->
                                                                <div class="dropdown dropdown-inline">
                                                                    <div class="kt-chat__label">
                                                                        <a href="#" class="kt-chat__title">PQ Query Informations</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="kt-chat__center">
                                                                <div class="kt-chat__label">
                                                                    <!-- <a href="#" class="kt-chat__title">Jason Muller</a>
                                                                    <span class="kt-chat__status">
                                                                        <span class="kt-badge kt-badge--dot kt-badge--success"></span> Active
                                                                    </span> -->
                                                                </div>
                                                                <div class="kt-chat__pic kt-hidden">
                                                                    <span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Jason Muller">
                                                                        <img src="assets/media/users/300_12.jpg" alt="image">
                                                                    </span>
                                                                    <span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Nick Bold">
                                                                        <img src="assets/media/users/300_11.jpg" alt="image">
                                                                    </span>
                                                                    <span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Milano Esco">
                                                                        <img src="assets/media/users/100_14.jpg" alt="image">
                                                                    </span>
                                                                    <span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Teresa Fox">
                                                                        <img src="assets/media/users/100_4.jpg" alt="image">
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="kt-chat__right">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="kt-portlet__body">
                                                        <div class="kt-scroll pq_and_lead_query_history" data-scroll="true" data-height="400">

                                                        </div>
                                                    </div>
                                                    <div class="kt-portlet__foot">
                                                        <div class="kt-chat__input">
                                                            <div class="kt-chat__editor">
                                                                <input type="text" class="form-control" name="query_type" value="pq_query" hidden />
                                                                <input type="text" class="form-control" name="query_reference_id" value="" hidden />
                                                                <input type="text" class="form-control" name="query_reference" value="" hidden />
                                                                <textarea style="height: 50px" placeholder="Type here..." name="query_text"></textarea>
                                                                <input type="text" class="form-control" name="query_creator_id" value="" hidden />
                                                                <input type="text" class="form-control" name="query_assigned_id" value="" hidden />
                                                                <input type="text" class="form-control" name="query_status" value="open" hidden />
                                                            </div>
                                                            <div class="kt-chat__toolbar">
                                                                <div class="kt_chat__tools">
                                                                    <div class="query_type">

                                                                    </div>
                                                                </div>
                                                                <div class="kt_chat__actions">
                                                                    <button type="button" class="btn btn-brand btn-md btn-upper btn-bold kt-chat__reply save_pq_and_lead_query">reply</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!--End:: App Content-->
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">

                            </div>
                        </div>
                    </div>
                </div>


                <div class="modal fade" id="document_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Upload Files:</h5>
                            </div>
                            <div class="modal-body row">
                                <div class="form-group form-group-last col-lg-12 row" style="padding: 10px 25px;">
                                    <label class="col-lg-5 col-form-label kt-font-bolder kt-font-lg">Documents :</label>
                                    <div class="col-lg-7">
                                        <div class="dropzone dropzone-multi" id="kt_dropzone_client_document">
                                            <div class="dropzone-panel">
                                                <a class="dropzone-select btn btn-label-brand btn-bold btn-sm">Attach files</a>
                                                <a class="dropzone-upload btn btn-label-brand btn-bold btn-sm">Upload All</a>
                                                <a class="dropzone-remove-all btn btn-label-brand btn-bold btn-sm">Remove All</a>
                                            </div>
                                            <div class="dropzone-items">
                                                <div class="dropzone-item" style="display:none">
                                                    <div class="dropzone-file">
                                                        <div class="dropzone-filename" title="some_image_file_name.jpg"><span data-dz-name>some_image_file_name.jpg</span> <strong>(<span  data-dz-size>340kb</span>)</strong></div>
                                                        <div class="dropzone-error" data-dz-errormessage></div>
                                                    </div>
                                                    <div class="dropzone-progress">
                                                        <div class="progress">
                                                            <div class="progress-bar kt-bg-brand" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress></div>
                                                        </div>
                                                    </div>
                                                    <div class="dropzone-toolbar">
                                                        <span class="dropzone-start"><i class="flaticon2-arrow"></i></span>
                                                        <span class="dropzone-cancel" data-dz-remove style="display: none;"><i class="flaticon2-cross"></i></span>
                                                        <span class="dropzone-delete" data-dz-remove><i class="flaticon2-cross"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <span class="form-text text-muted kt-font-bold">Max file size is 1MB and max number of files is 5.</span>
                                    </div>
                                </div>

                                <div class="col-lg-12" id="document_upload_history" style="padding: 10px 20px 10px 0px;"></div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary osdr_upload_close" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="pq_details_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">PQ Details</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="post" id="form_pq_details">

                                </form>
                            </div>
                            <div class="modal-footer">

                                <button class="btn btn-success save_pq_details" id="comp_mst_id" type="reset">Save!</button>
                                <button class="btn btn-danger" type="button" data-dismiss="modal" aria-label="Close">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="pq_comment_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content custom-modal-style">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">PQ Comments</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row lead_font" id="pq_comment_details">
                                    <label>Comment</label>
                                    <textarea class="form-control" id="pq_comment" placeholder="Type here..."></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">

                                <button class="btn btn-success save_pq_comment" id="pq_id" type="reset">Save</button>
                                <button class="btn btn-danger" type="button" data-dismiss="modal" aria-label="Close">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                 <div class="modal fade" id="special_comment_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl" role="document">
                        <div class="modal-content custom-modal-style">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel"> Comments</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="post" id="special_comment_details_form">

                                </form>
                            </div>
                            <div class="modal-footer">

                                <button class="btn btn-success save_special_comment" id="customer_id" type="reset">Save</button>
                                <button class="btn btn-danger" type="button" data-dismiss="modal" aria-label="Close">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!--end::model-->
            </div>
            <!--end::Content-->
        </div>
    </div>
</div>
