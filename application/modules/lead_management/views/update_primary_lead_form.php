<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12 col-xl-12 order-lg-1 order-xl-1">
                <div class="kt-portlet">

                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="kt-font-brand flaticon2-line-chart"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                UPDATE PRIMARY LEADS 
                            </h3>
                        </div> 
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-wrapper">
                                <div class="kt-portlet__head-actions">
                                    <button type="submit" class="btn btn-primary save_primary_form" comp_mst_id="<?php echo $customer_data['id'];?>" >Save lead</button>
                                </div>
                            </div>
                        </div>              
                    </div>
                    <div class="kt-portlet__body">
                        <form class="kt-form" id="add_primary_lead">
                            <div class="form-group row">
							    <div class="col-lg-3 form-group-sub">
								    <label class="form-control-label">Assign To:</label>
									<select class="form-control  lead_select_picker" name="assigned_to">
										<option value="">Select</option>
                                        <?php foreach ($assigned_to as $single_person) { ?>
                                            <option value="<?php echo $single_person['user_id']; ?>"
                                                <?php 
                                                echo ($customer_data['assigned_to'] == $single_person['user_id']) ? 'selected': '';
                                                ?>>
                                                <?php echo $single_person['name']; ?>
                                            </option>;
                                        <?php } ?>
                                    </select>
								</div>
								<div class="col-lg-3 form-group-sub">
								    <label class="form-control-label">Company</label>
                                    <input type="text" class="form-control"  value="<?php echo $customer_data['name'];?>" readonly>
                                </div>
                                <div class="col-lg-3 form-group-sub">
                                    <label class="form-control-label">Region</label>
                                    <!-- <input type="text" class="form-control"  value="<?php echo $region['name'];?>" readonly> -->
                                    <select class="form-control lead_select_picker" disabled="disabled">
                                        <?php foreach ($region_list as $single_region) { ?>
                                            <option value="<?php echo $single_region['id']; ?>"
                                            <?php 
                                                echo ($customer_data['region_id'] == $single_region['id']) ? 'selected': ''; ?>>
                                                <?php echo $single_region['name']; ?>
                                            </option>;
                                        <?php } ?>
                                    </select>
								</div>
                                <div class="col-lg-3 form-group-sub">
                                    <label class="form-control-label">Country</label>
                                    <!-- <input type="text" class="form-control"  value="<?php echo $country['name'];?>" readonly> -->
                                    <select class="form-control lead_select_picker" disabled="disabled">
                                        <?php foreach ($country_list as $single_country) { ?>
                                            <option value="<?php echo $single_country['id']; ?>"
                                            <?php 
                                                echo ($customer_data['country_id'] == $single_country['id']) ? 'selected': ''; ?>>
                                                <?php echo $single_country['name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
								</div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-3 form-group-sub">
                                    <label class="form-control-label">Website</label>
                                    <input type="text" class="form-control" name="website"  value="<?php echo  $customer_data['website']; ?>">
                                </div>
                                <div class="col-lg-3 form-group-sub">
                                    <label class="form-control-label">Source </label>
                                    <input type="text" class="form-control"  value="<?php echo $company_data['source'];?>" readonly>
								</div>
                                <div class="col-lg-3 form-group-sub">
                                    <label class="form-control-label">No of Employee</label>
                                    <select class="form-control lead_select_picker" name="no_of_employees">
									    <option value="">Select</option>
									    <option value="1-9" <?php echo ($customer_data["no_of_employees"] == '1-9') ? 'selected': '';?>>1-9</option>
                                        <option value="10-25" <?php echo ($customer_data["no_of_employees"] == '10-25') ? 'selected': '';?>>10-25</option>
                                        <option value="25-50" <?php echo ($customer_data["no_of_employees"] == '25-50') ? 'selected': '';?>>25-50</option>
                                        <option value="50-100" <?php echo ($customer_data["no_of_employees"] == '50-100') ? 'selected': '';?>>50-100</option>
                                        <option value="100-200" <?php echo ($customer_data["no_of_employees"] == '100-200') ? 'selected': '';?>>100-200</option>
                                        <option value="200-500" <?php echo ($customer_data["no_of_employees"] == '200-500') ? 'selected': '';?>>200-500</option>
                                        <option value="500-1000" <?php echo ($customer_data["no_of_employees"] == '500-1000') ? 'selected': '';?>>500-1000</option>
                                        <option value="1000+" <?php echo ($customer_data["no_of_employees"] == '1000+') ? 'selected': '';?>>1000+</option>
                                    </select>
								</div>
                                <div class="col-lg-3 form-group-sub">
                                    <label class="form-control-label">Last Contacted</label>
                                    <input type="text" class="form-control" value="<?php echo  date('d-m-Y', strtotime($member_last_contact['connected_on'])); ?>" readonly/>
                                </div>
                            </div>
                            <div class="form-group row">
								<div class="col-lg-3 form-group-sub">
								    <label class="form-control-label">Lead Type</label>
                                    <select class="form-control lead_select_picker lead_type" name="lead_type">
                                        <option value="">Select</option>
                                        <?php foreach ($lead_type as $lead_type_detail) { ?>
                                            <option value="<?php echo $lead_type_detail['lead_type_id']; ?>"
                                            <?php 
                                                echo ($customer_data['lead_type'] == $lead_type_detail['lead_type_id']) ? 'selected': ''; ?>>
                                                <?php echo $lead_type_detail['type_name']; ?>
                                            </option>;
                                        <?php } ?>
                                    </select>
                                </div>
                                <!-- <div class="col-lg-3 form-group-sub lead_industry" style="display:<?php echo ($customer_data['lead_type'] == 3) ? 'block': 'none';?>">
							        <label class="form-control-label">Lead Industry</label>
									<select class="form-control lead_select_picker" name="lead_industry">
                                        <option value="">Select</option>
                                        <?php foreach ($lead_industry as $lead_industry_detail) { ?>
                                            <option value="<?php echo $lead_industry_detail['lead_industry_id']; ?>" 
                                            <?php 
                                                echo ($customer_data['lead_industry'] == $lead_industry_detail['lead_industry_id']) ? 'selected': ''; ?>>
                                                <?php echo $lead_industry_detail['industry_name']; ?>
                                            </option>;
                                        <?php } ?>
                                    </select>
								</div> -->
                                <div class="col-lg-3 form-group-sub">
								    <label class="form-control-label">Lead Stage</label>
									<select class="form-control lead_select_picker lead_stage" name="lead_stage">
                                    <option value="">Select</option>
                                        <?php foreach ($lead_stage as $lead_stage_detail) { ?>
                                            <option value="<?php echo $lead_stage_detail['lead_stage_id']; ?>"
                                            <?php 
                                                echo ($customer_data['lead_stage'] == $lead_stage_detail['lead_stage_id']) ? 'selected': ''; ?>>
                                                <?php echo $lead_stage_detail['stage_name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
								</div>
                                <div class="col-lg-3 form-group-sub stage_reason" style="display: <?php echo($customer_data['lead_stage'] == 0) ? 'block': 'none';?>">
                                    <?php //if($customer_data['lead_stage'] == 0){ ?>
                                        
                                        <label class="form-control-label">Stage 0 -Reason</label>
                                        <select class="form-control lead_select_picker" name="stage_reason">
                                            <option value="">Select</option>
                                            <?php foreach ($stage_reasons as $stage_reasons_detail) { ?>
                                                <option value="<?php echo $stage_reasons_detail['lead_reason_id']; ?>" 
                                                <?php 
                                                    echo ($customer_data['stage_reason'] == $stage_reasons_detail['lead_reason_id']) ? 'selected': ''; ?>>
                                                <?php echo $stage_reasons_detail['reason']; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    <?php  //}?>									
                                </div>
                                <div class="col-lg-3 form-group-sub">
                                    <label class="form-control-label">Specific product to pitch</label>
									<input type="text" name="product_pitch" class="form-control" value="<?php echo $customer_data['product_pitch']; ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-3 form-group-sub">
                                    <label class="form-control-label">Purchase Factor - 1</label>
									<select class="form-control lead_select_picker" name="purchase_factor_1">
                                        <option value="">Select</option>
                                        <?php foreach ($purchase_factor as $purchase_factor_detail) { ?>
                                            <option value="<?php echo $purchase_factor_detail['factor_id']; ?>" 
                                                <?php 
                                                echo ($customer_data['purchase_factor_1'] == $purchase_factor_detail['factor_id']) ? 'selected': '';
                                                ?>>
                                                <?php echo $purchase_factor_detail['factor_value']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-lg-3 form-group-sub">
                                    <label class="form-control-label">Purchase Factor - 2</label>
                                    <select class="form-control lead_select_picker" name="purchase_factor_2">
                                        <option value="">Select</option>
                                        <?php foreach ($purchase_factor as $purchase_factor_detail) { ?>
                                            <option value="<?php echo $purchase_factor_detail['factor_id']; ?>" 
                                                <?php 
                                                echo ($customer_data['purchase_factor_2'] == $purchase_factor_detail['factor_id']) ? 'selected': '';
                                                ?>>
                                                <?php echo $purchase_factor_detail['factor_value']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-lg-3 form-group-sub">
                                    <label class="form-control-label">Purchase Factor - 3</label>
                                    <select class="form-control lead_select_picker" name="purchase_factor_3">
                                        <option value="">Select</option>
                                        <?php foreach ($purchase_factor as $purchase_factor_detail) { ?>
                                            <option value="<?php echo $purchase_factor_detail['factor_id']; ?>" 
                                                <?php 
                                                echo ($customer_data['purchase_factor_3'] == $purchase_factor_detail['factor_id']) ? 'selected': '';
                                                ?>>
                                                <?php echo $purchase_factor_detail['factor_value']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-lg-3 form-group-sub">
                                    <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="javascript:void(0)" class="btn-sm btn btn-label-primary btn-bold add_sample_query" data-toggle="modal" data-target="#add_query" comp_mst_id="<?php echo $customer_data['id'];?>" query_type="lead_sample_query" title="Sample">
                                        <i class="la la-comment"> Sample Request</i>
                                    </a>
                                </div>
                            </div>
                            <div class="form-group row">                                
                                <div class="col-lg-3 form-group-sub">
                                    <label class="form-control-label">Box</label>
                                    <select class="form-control lead_select_picker box_change" name="box">
                                        <option value="">Select</option>                                        
									    <option value="No" <?php echo ($customer_data['box'] == 'No') ? 'selected' : ''; ?>>No</option>
                                        <option value="Yes" <?php echo ($customer_data['box'] == 'Yes') ? 'selected' : ''; ?>>Yes</option>                                       
                                    </select>
                                </div>
                                <div class="col-lg-3 form-group-sub box_comment" style="display:<?php echo ($customer_data['box'] == 'Yes') ? 'block' : 'none';?>">
                                    <label class="form-control-label">Box Comments</label>
                                    <textarea name="box_comment" class="form-control"><?php echo $customer_data['box_comment']; ?></textarea>
                                </div>
                                <div class="col-lg-6 form-group-sub">
                                    <label class="form-control-label">Purchase Comments</label>
                                    <textarea name="purchase_comments" class="form-control"><?php echo $customer_data['purchase_comments']; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-3 form-group-sub">
                                    <label class="form-control-label">Sample</label>
									<select class="form-control lead_select_picker sample_change" name="sample">
                                        <option value="">Select</option>                                        
									    <option value="No" <?php echo ($customer_data['sample'] == 'No') ? 'selected' : ''; ?>>No</option>
                                        <option value="Yes" <?php echo ($customer_data['sample'] == 'Yes') ? 'selected' : ''; ?>>Yes</option>                                       
                                    </select>
                                </div>
                                <div class="col-lg-3 form-group-sub sample_comment" style="display:<?php echo ($customer_data['sample'] == 'Yes') ? 'block' : 'none';?>">
                                    <label class="form-control-label">Sample Comments</label>
                                    <textarea name="sample_comment" class="form-control"><?php echo $customer_data['sample_comment']; ?></textarea>
                                </div>
                                <div class="col-lg-6 form-group-sub">
                                    <label class="form-control-label">Sales Notes</label>
                                    <textarea name="sales_notes" class="form-control"><?php echo $customer_data['sales_notes']; ?></textarea> 
                                </div> 
                            </div>
                            <div class="form-group form-group-last row">
                                <div class="col-lg-3 form-group-sub">
                                    <label class="form-control-label">Margins: </label>
                                    <input type="text" name="margins" class="form-control" value="<?php echo $customer_data['margins']; ?>">
                                </div>
                                <div class="col-lg-5 form-group-sub row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Priority Range</label>
                                    <div class="col-lg-8 col-md-9 col-sm-12">
                                        <div class="kt-ion-range-slider">
                                            <input type="hidden" class="lead_priority" id="lead_priority_value" name="lead_priority" value="<?php echo $customer_data['lead_priority'];?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 form-group-sub row primary_priority_reason" style="display:<?php echo ($customer_data['lead_priority'] != 0) ? 'block' : 'none';?>">
                                   <label class="form-control-label">Priority Reason</label>
                                    <input type="text" class="form-control" id="priority_reason" name="priority_reason" value="<?php echo $customer_data['priority_reason'];?>">
                                    <small class="form-text text-danger" id="reason_error"></small>
                                </div>
                            </div>
                        </form>

                        <form id="primary_member_details">
                            <div class="form-group row">
                                <div class="col-lg-12" style="padding: 0px 0px 0px 0px !important;">
                                    <table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info">
                                        <thead>
                                            <tr role="row">
                                                <th colspan="12" style="text-align: left;">Member</th>     
                                                <th style="text-align: center;">
                                                    <a href="javascript:;" class="btn-sm btn btn-label-success btn-bold add_primary_member" next_count_number="<?php echo $next_count_number;?>">
                                                        <i class="la la-plus">Add</i>
                                                    </a>
                                                </th>
                                            </tr>       
                                            <tr role="row">
                                                <th style="width: 3%;">Sr</th>
                                                <th style="width: 10%;">Name</th>
												<th style="width: 6%;">Designation</th>
												<th style="width: 6%;">Email</th>
								    			<th style="width: 6%;">Mobile</th>
												<th style="width: 6%;">Whatsapp</th>
												<th style="width: 3%;">Skype</th>
												<th style="width: 6%;">Telephone</th>
												<th style="width: 6%;">MainBuyer</th>
												<th style="width: 6%;">Last Contact Date</th>
												<th style="width: 6%;">Mode</th>
												<th style="width: 3%;">Decision Maker (%)</th>
												<th style="width: 4%;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="add_primary_member_body">
                                            <?php if(!empty($member_detail)){ ?>
                                                <?php $count_no = 1;?>
                                                <?php foreach($member_detail as $member_detail_key => $single_member_detail){ ?>

                                                    <tr class="<?php echo $count_no;?>">

                                                        <td><?php echo ++$member_detail_key; ?><br>
                                                            <input type="text" class="form-control" name="comp_dtl_id_<?php echo $count_no;?>" value="<?php echo $single_member_detail['comp_dtl_id'];?>" hidden>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="member_name_<?php echo $count_no;?>"  value="<?php echo $single_member_detail['member_name'];?>">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="designation_<?php echo $count_no;?>" value="<?php echo $single_member_detail['designation'];?>">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="email_<?php echo $count_no;?>" value="<?php echo $single_member_detail['email'];?>">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="mobile_<?php echo $count_no;?>" value="<?php echo $single_member_detail['mobile'];?>">
                                                        </td>
                                                        <td>
                                                            <select class="form-control" name="is_whatsapp_<?php echo $count_no;?>">
                                                                <option value="No"<?php echo ($single_member_detail['is_whatsapp'] =='No') ? 'selected' : '';?>>No</option>
                                                                <option value="Yes"<?php echo ($single_member_detail['is_whatsapp'] =='Yes') ? 'selected' : '';?>>Yes</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="skype_<?php echo $count_no;?>" value="<?php echo $single_member_detail['skype'];?>">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="telephone_<?php echo $count_no;?>" value="<?php echo $single_member_detail['telephone'];?>">
                                                        </td>
                                                        <td>
                                                            <select class="form-control" name="main_buyer_<?php echo $count_no;?>">
                                                                <option value="No"<?php echo ($single_member_detail['main_buyer'] =='No') ? 'selected' : ''; ?>>No</option>
                                                                <option value="Yes"<?php echo ($single_member_detail['main_buyer'] =='Yes') ? 'selected' : ''; ?>>Yes</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" value="<?php echo $single_member_detail['connected_on'];?>" readonly>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" value="<?php echo $single_member_detail['connect_mode'];?>" readonly>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="decision_maker_<?php echo $count_no;?>" value="<?php echo $single_member_detail['decision_maker'];?>">
                                                        </td>
                                                        <td>
                                                        
                                                            <a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon btn-icon-md primary_member_followup_modal" data-toggle="modal" data-target="#primary_followup_model" comp_dtl_id="<?php echo $single_member_detail['comp_dtl_id'];?>" comp_mst_id="<?php echo $single_member_detail['comp_mst_id'];?>" country_id="<?php echo $customer_data['country_id'];?>" category_id="<?php echo $company_data['product_category_id'];?>" title="Follow Up">
                                                                <i class="la la-comment"></i>
                                                            </a>

                                                            <a href="javascript:;" data-repeater-delete="" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_member" comp_dtl_id="<?php echo $single_member_detail['comp_dtl_id']; ?>" comp_mst_id="<?php echo $single_member_detail['comp_mst_id']; ?>" count_no="<?php echo $count_no;?>">
                                                                <i class="la la-trash"></i>
                                                            </a>
                                                        </td>             
                                                    </tr>
                                                <?php $count_no++;}?>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                        
                        <form id="primary_other_member_details">
                            <div class="form-group row">
                                <div class="col-lg-12" style="padding: 0px 0px 0px 0px !important;">
                                    <table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info">
                                        <thead>
                                            <tr role="row">
                                                <th colspan="10" style="text-align: left;">Other Member</th>     
                                                <th style="text-align: center;">
                                                    <a href="javascript:;" class="btn-sm btn btn-label-success btn-bold add_primary_other_member" next_number="<?php echo $next_number;?>">
                                                        <i class="la la-plus">Add</i>
                                                    </a>
                                                </th>
                                            </tr>       
                                            <tr role="row">
                                                <th width="3%">Sr</th>
                                                <th width="15%">Name</th>
												<th width="15%">Designation</th>
												<th width="15%">Email</th>
												<th width="15%">Mobile</th>
												<th width="6%">Whatsapp</th>
												<th width="7%">Skype</th>
												<th width="7%">Telephone</th>
												<th width="15%">Last Contact Date</th>
			    								<th width="15%">Mode</th>
												<th width="8%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="add_primary_other_member_body">
                                            <?php if(!empty($other_member_detail)){ ?>
                                                <?php $next_count = 1;?>
                                                <?php foreach($other_member_detail  as $other_member_detail_key => $single_other_member_detail){ ?>

                                                    <tr class="<?php echo $next_count;?>">
                                                        <td><?php echo ++$other_member_detail_key;?><br>
                                                            <input type="text" class="form-control" name="comp_dtl_id_<?php echo $next_count;?>" value="<?php echo $single_other_member_detail['comp_dtl_id'];?>" hidden>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="member_name_<?php echo $next_count;?>" value="<?php echo $single_other_member_detail['member_name'];?>">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="designation_<?php echo $next_count;?>" value="<?php echo $single_other_member_detail['designation'];?>">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="email_<?php echo $next_count;?>" value="<?php echo $single_other_member_detail['email'];?>">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="mobile_<?php echo $next_count;?>" value="<?php echo $single_other_member_detail['mobile'];?>">
                                                        </td>
                                                        <td>
                                                            <select class="form-control" name="is_whatsapp_<?php echo $next_count;?>">
                                                                <option value="No" <?php echo ($single_other_member_detail['is_whatsapp'] =='No') ? 'selected' : ''; ?>>No</option>
                                                                <option value="Yes" <?php echo ($single_other_member_detail['is_whatsapp'] =='Yes') ? 'selected' : ''; ?>>Yes</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="skype_<?php echo $next_count;?>" value="<?php echo $single_other_member_detail['skype'];?>">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="telephone_<?php echo $next_count;?>" value="<?php echo $single_other_member_detail['telephone'];?>">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" value="<?php echo $single_other_member_detail['connected_on'];?>" readonly>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" value="<?php echo $single_other_member_detail['connect_mode'];?>" readonly>
                                                        </td>
                                                        <td>                                                    
                                                            <a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon btn-icon-md primary_other_member_followup_modal" data-toggle="modal"data-target="#primary_followup_model" comp_dtl_id="<?php echo $single_other_member_detail['comp_dtl_id'];?>" comp_mst_id="<?php echo $single_other_member_detail['comp_mst_id'];?>" country_id="<?php echo $customer_data['country_id'];?>" category_id="<?php echo $company_data['product_category_id'];?>" title="Follow Up">
                                                                <i class="la la-comment"></i>
                                                            </a>
                                                       
                                                            <a href="javascript:;" data-repeater-delete="" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_other_member" comp_dtl_id="<?php echo $single_other_member_detail['comp_dtl_id'];?>" comp_mst_id="<?php echo $single_other_member_detail['comp_mst_id'];?>" next_count="<?php echo $next_count;?>">
                                                                <i class="la la-trash"></i>
                                                            </a>

                                                        </td>             
                                                    </tr>
                                                <?php $next_count++; }?>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                    </div>
                                        
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="primary_followup_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
         	<div class="modal-content">
	            <div class="modal-header">
	               <h5 class="modal-title" id="exampleModalLabel">Follow Up</h5>
	               <button type="reset" class="close" data-dismiss="modal" aria-label="Close">
	               </button>
	            </div>
	            <div class="modal-body">
		            <form id="primary_followup_form">	            	

						
		            </form>
	            </div>
	            <div class="modal-footer">
	               	<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
	            	<!-- <button class="btn btn-success save_primary_followup" type="reset" id="comp_dtl_id" style="float: right;">Submit</button> -->
	            </div>
        	</div>
    	</div>
   	</div>

    <div class="modal fade" id="add_query" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <form id="sample_query_form">
                        <div class="row">
                            <!--Begin:: App Content-->
                            <div class="kt-grid__item kt-grid__item--fluid kt-app__content" id="kt_chat_content" style="width:100%; padding: 0px 24px 0px 24px;">
                                <div class="kt-chat">
                                    <div class="kt-portlet kt-portlet--head-lg kt-portlet--last">
                                        <div class="kt-portlet__head">
                                            <div class="kt-chat__head ">
                                                <div class="kt-chat__left">

                                                    <!--begin:: Aside Mobile Toggle -->
                                                    <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md kt-hidden-desktop" id="kt_chat_aside_mobile_toggle">
                                                        <i class="flaticon2-open-text-book"></i>
                                                    </button>

                                                    <!--end:: Aside Mobile Toggle-->
                                                    <div class="dropdown dropdown-inline">
                                                        <div class="kt-chat__label">
                                                            <a href="#" class="kt-chat__title">Query Informations</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="kt-chat__center">
                                                    <div class="kt-chat__label">
                                                        <!-- <a href="#" class="kt-chat__title">Jason Muller</a>
                                                        <span class="kt-chat__status">
                                                            <span class="kt-badge kt-badge--dot kt-badge--success"></span> Active
                                                        </span> -->
                                                    </div>
                                                    <div class="kt-chat__pic kt-hidden">
                                                        <span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Jason Muller">
                                                            <img src="assets/media/users/300_12.jpg" alt="image">
                                                        </span>
                                                        <span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Nick Bold">
                                                            <img src="assets/media/users/300_11.jpg" alt="image">
                                                        </span>
                                                        <span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Milano Esco">
                                                            <img src="assets/media/users/100_14.jpg" alt="image">
                                                        </span>
                                                        <span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Teresa Fox">
                                                            <img src="assets/media/users/100_4.jpg" alt="image">
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="kt-chat__right">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="kt-scroll lead_sample_query_history" data-scroll="true" data-height="400">

                                            </div>
                                        </div>
                                        <div class="kt-portlet__foot">
                                            <div class="kt-chat__input">
                                                <div class="kt-chat__editor">
                                                    <input type="text" class="form-control" name="query_type" value="" hidden />
                                                    <!-- <input type="text" class="form-control" name="query_type" value="sample_query" hidden />
                                                    <input type="text" class="form-control" name="sample_type" value="lead_sample_query" hidden /> -->
                                                    <input type="text" class="form-control" name="query_reference" value="" hidden />
                                                    <input type="text" class="form-control" name="query_reference_id" value="" hidden />
                                                    <textarea style="height: 50px" placeholder="Type here..." name="query_text" class="kt-font-bolder"></textarea>
                                                    <input type="text" class="form-control" name="query_creator_id" value="" hidden />
                                                    <input type="text" class="form-control" name="query_status" value="open" hidden />
                                                </div>
                                                <div class="kt-chat__toolbar">
                                                    <div class="kt_chat__tools">
                                                        <div class="query_type">
  
                                                        </div>
                                                    </div>
                                                    <div class="kt_chat__actions">
                                                        <button type="button" class="btn btn-brand btn-md btn-upper btn-bold kt-chat__reply save_sample_query">reply</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--End:: App Content-->
                        </div>
                    </form>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>

</div>
<!-- end:: Content -->