
<?php foreach($list_data as $list_data_key => $single_list_data_value){ ?>

    <tr role="row">
		<td class="sorting_1 kt-align-center" tabindex="0"><?php echo $list_data_key+1; ?></td>
		<td class="sorting_1 kt-align-center <?php echo $rank_colum_hidden; ?>" tabindex="0">
			<?php echo $single_list_data_value['rank']; ?>
			<?php if($single_list_data_value['lead_status']=='New'){ ?><br/>
			<span class="kt-align-left kt-font-bold">
				<span class="" style="width: auto; font-size: 14px;">
					<span class="kt-badge kt-badge--unified-info kt-badge--sm kt-badge--rounded kt-badge--bold kt-badge--inline lead_management_font">New</span>
				</span>
			</span>
			<?php }?>
		</td>
		<td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="OrderID">
			<span>
				<div class="kt-user-card-v2">
					<div class="kt-user-card-v2__pic">
						<img class="kt-widget3__img rounded-circle" src="assets/media/flags/<?php echo $single_list_data_value['flag_name'];?>" title="<?php echo $single_list_data_value['country_name']; ?>" style="width: 30px" alt="">
					</div>
					<div class="kt-user-card-v2__details">
						<span class="kt-user-card-v2__name">
                            <?php echo $single_list_data_value['customer_name']; ?>
                        </span>
                        <br>
						<a href="javascript:void(0)" class="kt-user-card-v2__email kt-link">
                            <?php echo $single_list_data_value['lead_name']; ?>
                        </a>
                        <br>
						<a href="javascript:void(0)" class="kt-user-card-v2__email kt-link">
                            <?php echo $single_list_data_value['last_purchased']; ?>
                        </a>
                        <br>
                        <span class="box_sample_comment_model" data-toggle="modal" data-target="#box_comment_modal" comp_mst_id="<?php echo $single_list_data_value['id']; ?>">
                            <span>
                                <abbr>
                                    <?php if(!empty($single_list_data_value['box'] == 'Yes')) { ?>
                                        <strong style="margin-left: 3px;">  BOX</strong>
                                    <?php } ?>			    
                                </abbr>
                            </span>
                            <span>
                                <abbr>
                                    <?php if(!empty($single_list_data_value['sample'] == 'Yes')) { ?>
                                        <strong style="margin-left: 3px;">  Sample</strong>
                                    <?php } ?>			    
                                </abbr>
                            </span>
                        </span>  
					</div>
				</div>
			</span>
		</td>
		<td class="lead_stage_td" style="padding: 5px 5px;">
			<div class="btn-group lead_stage_div" style="white-space: nowrap;">
				<?php 
					$static_stage_array =	array(
												1 => array('stage_value'=> '1', 'stage_color'=> '#bbb'),
												2 => array('stage_value'=> '2', 'stage_color'=> 'yellow'),
												3 => array('stage_value'=> '3', 'stage_color'=> 'orange'),
												4 => array('stage_value'=> '4', 'stage_color'=> 'blue'),
												5 => array('stage_value'=> '5', 'stage_color'=> 'green'),
												6 => array('stage_value'=> '6', 'stage_color'=> 'linear-gradient(to bottom, #33ccff 0%, #ff99cc 100%)'),
												0 => array('stage_value'=> '0', 'stage_color'=> 'red')
					);
				?>
                <button type="button " class="btn btn-secondary" data-toggle="dropdown" style="float: none; display: inline-block;padding: 5px 10px;">
					<div class="">
						<span class="selected_stage_color_<?php echo $single_list_data_value['id']; ?>" style="height: 25px; width: 25px; background: <?php echo $static_stage_array[$single_list_data_value['lead_stage']]['stage_color']; ?>; border-radius: 50%; display: inline-block;"></span>
						
						<span class="selected_stage_value_<?php echo $single_list_data_value['id']; ?>">&nbsp&nbsp<?php echo "Stage ",$static_stage_array[$single_list_data_value['lead_stage']]['stage_value']; ?></span>
					</div>
                </button>
                <ul class="lead_stage_ul dropdown-menu row" role="menu">
				<?php 
					foreach ($static_stage_array as $single_stage_details) {
				?>
					<li class="lead_stage_li" style="padding: 5px 20px;" onclick="update_lead_stage('<?php echo $single_stage_details['stage_value']; ?>', '<?php echo $single_list_data_value['id']; ?>', '<?php echo $single_stage_details['stage_color']; ?>');">
						<button type="button" class="btn btn-secondary" style="height:40px;width: 100%;">
							<div class="row">
								<div style="">
									<span class="caret" style="height: 25px; width: 25px; background: <?php echo $single_stage_details['stage_color']; ?>; border-radius: 50%; display: inline-block;"></span>
								</div>&nbsp&nbsp&nbsp<?php echo "Stage ",$single_stage_details['stage_value']; ?>
							</div>
						</button>
					</li>
				<?php	
					}
				?>
                </ul>
			</div>
			<br>
			<span style="font-weight: lighter;" class="last_contacted"><?php echo $single_list_data_value['last_contacted']; ?></span>
			<br>	
			<?php
				$member_class_name = '';
				if(!empty($single_list_data_value['no_of_employees'])) {

					$member_class_name = 'fa-users';
					if(in_array($single_list_data_value['no_of_employees'], array('1-9','10-25'))) {

						$member_class_name = 'fa-user';
					} else if (in_array($single_list_data_value['no_of_employees'], array('25-50', '50-100'))) {

						$member_class_name = 'fa-user-friends';
					}
				}
			?>
			<i class="fas <?php echo $member_class_name;?>"></i> <span style="font-size: 12px;"><?php echo $single_list_data_value['no_of_employees']; ?></span>
		</td>
		<td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="Ship City">
            <span>
				<div class="kt-user-card-v2">
					<div class="kt-user-card-v2__pic"></div>
					<div class="kt-user-card-v2__details">
						<span class="kt-user-card-v2__name">
                            <?php echo $single_list_data_value['member_name']; ?>
                        </span>
                        <br>
						<a href="javascript:void(0)" class="kt-user-card-v2__email kt-link">
                            <?php echo $single_list_data_value['designation']; ?>
                        </a>
					</div>
				</div>
			</span>
		</td>
		<td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="Ship City">

			<div class="row">
				<span class="col-xl-12">
					<div class="kt-user-card-v2">
						<div class="kt-user-card-v2__details">
							<span class="kt-user-card-v2__name"><i class="fas fa-envelope"> </i>&nbsp <?php echo $single_list_data_value['email']; ?></span>
						</div>
					</div>
				</span>
			</div>
			<div class="row">
				<span class="col-xl-6">
					<div class="kt-user-card-v2">
						<div class="kt-user-card-v2__details">
							<a href="javascript:void(0)" class="kt-user-card-v2__email kt-link"><i class="fas fa-phone"> &nbsp <?php echo $single_list_data_value['mobile']; ?></i></a>
						</div>
					</div>
				</span>
				<span class="col-xl-6 kt-align-right kt-font-bold">
					<div style = "text-align:right;" class="member_count_list_model" data-toggle="modal" data-target="#member_count_list_modal" comp_mst_id="<?php echo $single_list_data_value['id']; ?>" country_name="<?php echo $single_list_data_value['country_name']; ?>" >
						<span style="cursor: pointer; color: #9583ce; font-weight: bold;">
							<?php echo $single_list_data_value['member_count'];?>
						</span> /
						<span style="cursor: pointer; color: #ce8383; font-weight: bold;">
							<?php echo $single_list_data_value['non_member_count'];?>
						</span>
					</div>
				</span>
			</div>
			<div class="row">
				<span class="lead_font "style="font-size: 12px; color: red;">
                    <?php echo $single_list_data_value['last_special_comment'];?>
                </span>
			</div>
		</td>
        <?php 
        if($lead_name == 'distributors'){

            echo "<td>".$single_list_data_value['brand']."</td>";
        }
        ?>
        <td><?php echo $single_list_data_value['user_name']; ?></td>
		<td>
            <span style="font-size: 16px;">
                <?php echo $single_list_data_value['comments'];?>
            </span>           
            </br></br>
            <?php if($single_list_data_value['lead_stage'] == 0){?>
                <span style="font-size: 12px;">
                    <?php echo $single_list_data_value['stage_reason'];?>
                </span>
            <?php }?>
			<?php if($lead_name == 'pq' && !empty($single_list_data_value['last_pq_comment'])){?>
				</br></br>
                <span class="lead_font" style="font-size: 12px;">
                    PQ Comment:- <?php echo $single_list_data_value['last_pq_comment'];?>
                </span>
            <?php }?>
        </td>
		<td><?php echo $single_list_data_value['connect_mode']; ?>
			<?php if($lead_name == 'pq'){ ?>
				<br/><br/>
				<span class="kt-align-left kt-font-bold">
					<span class="<?php echo $single_list_data_value['pq_status_class'];?>" style="width: auto; padding: 10px; font-size: 12px;"><?php echo $single_list_data_value['pq_client_status'];?></span>
				</span>
			<?php } ?>
		</td>
		<td>
			<a href="<?php echo base_url('lead_management/update_primary_lead/'.$single_list_data_value['id']);?>" target="_blank" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
				<i class="la la-edit"></i>
			</a>
			<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md action_comment pull-right"  data-toggle="modal" data-target="#member_count_list_modal" title="add comment" comp_mst_id="<?php echo $single_list_data_value['id']; ?>" country_name="<?php echo $single_list_data_value['country_name']; ?>" >
				<i class="la la-comment"></i>
			</button>
			<?php if($this->session->userdata('lead_access')['lead_list_action_special_comment_access']){ ?>
			<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md special_comment pull-right"  data-toggle="modal" data-target="#special_comment_modal" title="Special comment" comp_mst_id="<?php echo $single_list_data_value['id']; ?>">
				<i class="fa fa-comment-medical"></i>
			</button>
			<?php }?>
			<?php if($this->session->userdata('lead_access')['lead_list_action_graph_access']){ ?>
				<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md graph_details pull-right" title="view Graph" company_name="<?php echo $single_list_data_value['customer_name'];?>" product_category="<?php echo $lead_name; ?>"  comp_mst_id="<?php echo $single_list_data_value['id']; ?>">
					<i class="la la-bar-chart"></i>
				</button>
			<?php }?>
			<?php if(!empty($search_engine_url_details['url'])){

				$name = trim($single_list_data_value['customer_name']);
				$lead = str_replace(array(' ', '-', '&', '.', '(', ')', '/'), array('', '', '', '', '', '', ''), strtoupper($name));
				?>

				<a href='<?php echo $search_engine_url_details['url'];?>":"<?php echo $lead;?>"%7D' class="btn btn-sm btn-clean btn-icon btn-icon-md" target="_blank"  title="URL">
						<i class="la la-bullhorn"></i>
				</a>
			<?php }?>
			<?php if($this->session->userdata('lead_access')['lead_list_action_pq_query_access']){ ?>
				<a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon btn-icon-md add_pq_and_lead_query" data-toggle="modal" data-target="#add_pq_and_lead_query" client_id="<?php echo $single_list_data_value['id'];?>" title="Query">
					<i class="fa fa-question"></i>
				</a>
			<?php }?>
			<?php if(in_array($lead_name, array('TUBES', 'PROCESS_CONTROL', 'distributors', 'Hydraulic fitting'))){?>

				<?php if(!empty($single_list_data_value['document_file'])){?>

					<button type="button" class="btn btn-sm btn-success btn-icon btn-icon-md add_document pull-right"  data-toggle="modal" data-target="#document_modal" title="Document" comp_mst_id="<?php echo $single_list_data_value['id']; ?>">
						<i class="la la-camera"></i>
					</button>
				<?php } else{ ?>

					<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md add_document pull-right"  data-toggle="modal" data-target="#document_modal" title="Add Document" comp_mst_id="<?php echo $single_list_data_value['id']; ?>">
						<i class="la la-camera"></i>
					</button>
				<?php }
			}?>
			<?php if($this->session->userdata('lead_access')['lead_list_action_pq_status_access']  && in_array($lead_name, array('pq'))){ ?>
				<div class="dropdown" >
					<a href="javascript:void(0);" class="btn btn-md btn-clean btn-icon btn-icon-md" data-toggle="dropdown">
						<i class="flaticon2-check-mark" title="Change status"></i>
					</a>
					<div class="dropdown-menu dropdown-menu-right" data-size="7" data-live-search="true">
						<?php if(!empty($single_list_data_value['pq_status_array'])){ ?>
							<?php foreach ($single_list_data_value['pq_status_array'] as $status_details) { ?>
								<a class="dropdown-item change_status <?php echo $status_details['class'];?>" value="<?php echo $status_details['value']; ?>" id="<?php echo $single_list_data_value['id'];?>" is_pq="<?php echo $single_list_data_value['is_pq'];?>">
									<?php echo $status_details['display_name']; ?>
								</a>
							<?php } ?>
						<?php } ?>
					</div>
				</div>
			<?php } ?>
			<?php if($this->session->userdata('lead_access')['lead_list_action_pq_details_access'] && in_array($lead_name, array('pq'))){ ?>

				<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md pq_details pull-right"  data-toggle="modal" data-target="#pq_details_modal" title="PQ Details" comp_mst_id="<?php echo $single_list_data_value['id'];?>">
					<i class="flaticon2-talk"></i>
				</button>
			<?php } ?>
		</td>
	</tr>
<?php } ?>
<script>
function update_lead_stage(stage_value, lead_mst_id, stage_color){

	if(stage_value > 0){

		$("span.selected_stage_value_"+lead_mst_id).html('').html("&nbsp&nbsp&nbspStage "+stage_value);
		$("span.selected_stage_color_"+lead_mst_id).css('background', stage_color);
		$.ajax({
			type: 'POST',
			data: {call_type: 'update_lead_stage', comp_mst_id: lead_mst_id, lead_stage: stage_value},
			url: '<?php echo base_url('lead_management/ajax_function'); ?>',
			success: function(res){
				toastr.success('Lead Stage Updated!');
			}
		});
	}
}
</script>