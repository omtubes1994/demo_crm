
<?php if(!empty($hetro_list)){ ?>

    <?php foreach($hetro_list as $hetro_list_key => $single_hetro_list_value){
        // echo "<pre>";print_r($single_hetro_list_value);echo"</pre><hr>";exit; ?>
       
        <tr role="row" class="<?php echo (($hetro_list_key+1)/2 == 00)?'even':'odd'?>">

            <td><?php echo $hetro_list_key+1; ?></td>
            <td class="kt-align">
				<img class="kt-widget3__img rounded-circle" src="assets/media/flags/<?php echo $single_hetro_list_value['flag_name'];?>" title="<?php echo $single_hetro_list_value['country_name'];?>" style="width: 30px" alt="">
                <strong style="margin-left: 5px; font-size: 13px;"> <?php echo $single_hetro_list_value['customer_name'];?> </strong><br/>
                <span>
				    <abbr>
                        <?php if(!empty($single_hetro_list_value['lead_name'])) { ?>
                                <?php echo $single_hetro_list_value['lead_name'];?>
                        
                        <?php } ?>			    
				    </abbr>
				</span>
                <span class="box_sample_comment_model" data-toggle="modal" data-target="#box_comment_modal" comp_mst_id="<?php echo $single_hetro_list_value['id']; ?>">
                    <span>
                        <abbr>
                            <?php if(!empty($single_hetro_list_value['box'] == 'Yes')) { ?>
                                <strong style="margin-left: 3px;">  BOX</strong>
                            <?php } ?>			    
                        </abbr>
                    </span>
                    <span>
                        <abbr>
                            <?php if(!empty($single_hetro_list_value['sample'] == 'Yes')) { ?>
                                <strong style="margin-left: 3px;">  Sample</strong>
                            <?php } ?>			    
                        </abbr>
                    </span>
                </span>
			</td>
            <td> 
                <select class="hetro_dropdown" name="lead_stage" comp_mst_id=<?php echo $single_hetro_list_value['id'];?>>
                        <option value="1"<?php echo ($single_hetro_list_value['lead_stage'] == '1') ? 'selected': '';?>> Stage 1</option>   

                        <option value="2"<?php echo ($single_hetro_list_value['lead_stage'] == '2') ? 'selected': '';?>> Stage 2</option>

                        <option value="3"<?php echo ($single_hetro_list_value['lead_stage'] == '3') ? 'selected': '';?>> Stage 3</option>
                        
                        <option value="4"<?php echo ($single_hetro_list_value['lead_stage'] == '4') ? 'selected': '';?>> Stage 4</option>
                        
                        <option value="5"<?php echo ($single_hetro_list_value['lead_stage'] == '5') ? 'selected': '';?>> Stage 5</option>
                        
                        <option value="6"<?php echo ($single_hetro_list_value['lead_stage'] == '6') ? 'selected': '';?>> Stage 6</option>
                        
                        <option value="0"<?php echo ($single_hetro_list_value['lead_stage'] == '0') ? 'selected': '';?> disabled> Stage 0</option>
                </select>
                <br/>
                <?php if(!empty($single_hetro_list_value['last_contacted'])){ ?>
                    <span>
                        <i style="font-size: 10px;"><?php echo $single_hetro_list_value['last_contacted'];?></i></br>
                    </span>
                <?php } ?>
                <?php if(!empty($single_hetro_list_value['no_of_employees'])){ ?>
                    <span>
                        <i class="fas fa-users" style="font-size: 12px;"></i>
                        <?php echo $single_hetro_list_value['no_of_employees'];?>
                    </span>
               <?php } ?>
            </td>
            <td> 
                <span style="font-size: 16px;">
                    <?php echo $single_hetro_list_value['member_name'];?>
                </span></br>         
                <span style="font-size: 13px;"><?php echo $single_hetro_list_value['designation'];?></span> 
			</td>
            <td>  
                <p> 
                    <?php if(!empty($single_hetro_list_value['email'])){ ?>
                        <span class="contact_span">
                            <span class="email">
                                <i class="fas fa-envelope"></i>
                                <?php echo $single_hetro_list_value['email'];?>
                            </span>
                        </span><br/>
                    <?php } ?>
                    <?php if(!empty($single_hetro_list_value['mobile'])){ ?>
                        <span class="contact_span">
                            <span class="mobile">
                                <i class="fas fa-phone"></i>
                                <?php echo $single_hetro_list_value['mobile'];?>
                            </span>
                        <span>
                    <?php } ?>
                </p>
            </td>
            <td>
                <div style = "text-align:left;" class="member_count_list_model" data-toggle="modal" data-target="#member_count_list_modal" comp_mst_id="<?php echo $single_hetro_list_value['id']; ?>">
                    <span style="cursor: pointer; color: #9583ce; font-weight: bold;">
                        <?php echo $single_hetro_list_value['member_count'];?>
                    </span> /
                    <span style="cursor: pointer; color: #ce8383; font-weight: bold;">
                        <?php echo $single_hetro_list_value['non_member_count'];?>
                    </span>
                </div>
			</td>
            <td>
                <?php echo $single_hetro_list_value['user_name']; ?>
            </td>            
            <td>
                <span style="font-size: 16px;">
                    <?php echo $single_hetro_list_value['comments'];?>
                </span>           
                </br></br>
                <?php if($single_hetro_list_value['lead_stage'] == 0){?>
                        
                    <span style="font-size: 12px;"><?php echo $single_hetro_list_value['stage_reason'];?>
                </span>
                <?php }?>
            </td>
            <td>
                <span style="font-size: 13px;">
                    <?php echo $single_hetro_list_value['connect_mode']; ?>
                </span>
            </td>
            <td>
                <a href="<?php echo base_url('lead_management/update_hetro_lead/'.$single_hetro_list_value['id']);?>" target="_blank" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">
						<i class="la la-info-circle"></i>
				</a>

                <!-- <a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon btn-icon-md create_task_model" data-toggle="modal" data-target="#task_modal" comp_mst_id="<?php echo $single_hetro_list_value['comp_mst_id']; ?>" assigned_to="<?php echo $single_hetro_list_value['assigned_to'];?>" title="Create Task">
                    <i class="la la-tasks"></i>
                </a> -->

                <a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon btn-icon-md hetro_update_rating" data-toggle="modal" data-target="#update_rating" comp_mst_id="<?php echo $single_hetro_list_value['id']; ?>"  lead_priority= <?php echo $single_hetro_list_value['lead_priority'];?> priority_reason="<?php echo $single_hetro_list_value['priority_reason'];?>" title="Update Rating">
                    <i class="la la-star"></i>
                </a>
                
            </td>
        <tr>
    <?php } ?>
<?php } ?>