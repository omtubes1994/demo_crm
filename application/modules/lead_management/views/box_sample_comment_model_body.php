<table class="table table-bordered" id="box_comments_table">
    <thead>
	    <tr>
		    <th style="width: 50% !important;">Box Comments</th>
			<th style="width: 50% !important;">Sample Box Comments</th>
		</tr>
	</thead>
	<tbody>
        <tr>
            <td><?php if(!empty($comment_detail['box_comment'])){                        
                    echo $comment_detail['box_comment']; 
                }else{ ?>
                    N/A
                <?php }?>
            </td>
            <td><?php if(!empty($comment_detail['sample_comment'])){                        
                    echo $comment_detail['sample_comment']; 
                }else{ ?>
                    N/A
                <?php }?>
            </td>
        </tr>
    </tbody>
</table>