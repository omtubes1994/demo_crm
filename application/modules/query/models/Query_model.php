<?php 
class Query_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	public function get_query_data($where_array,$limit,$offset){
	 	$this->db->select('
            SQL_CALC_FOUND_ROWS *
            ', FALSE);
        $this->db->where($where_array, null, false);
        $return_array['query_list'] = $this->db->get('query_mst', $limit, $offset)->result_array();
        //echo $this->db->last_query(),"<hr>";
       //echo "<pre>";print_r($return_array['query_list']);echo"</pre><hr>";exit();
        $return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows;")->row_array();
        return $return_array;
	}
	
	public function get_query_text($query_id){
	 	$this->db->select('query_id,query_text,entered_by');
	 	$this->db->where(array('query_id'=>$query_id));
		$res = $this->db->get('query_texts')->result_array();
		//echo "<pre>";print_r($res);echo"</pre><hr>";exit();
		return $res;
	}
	
	// public function get_quotation_no($quotation_id) {
	public function get_where_in_data($select, $where_in_array, $table_name) {

		return $this->db->select($select)->where_in($where_in_array['column_name'], $where_in_array['value'])->get($table_name)->result_array();
	}

	public function query_quotation_assign_details($quotation_id) {

		$this->db->select('quotation_mst.quotation_mst_id quotation_id, quotation_mst.quote_no, quotation_mst.proforma_no, quotation_mst.work_order_no, quotation_mst.assigned_to sales_person_id, rfq_mst.assigned_to procurement_person_id, production_process_information.handled_by, production_process_information.product_family');
		$this->db->join('rfq_mst', 'rfq_mst.rfq_mst_id = quotation_mst.rfq_id', 'left');
		$this->db->join('production_process_information', 'production_process_information.quotation_mst_id = quotation_mst.quotation_mst_id', 'left');
		$this->db->where(array('quotation_mst.quotation_mst_id'=>$quotation_id));
		return $this->db->get('quotation_mst')->row_array();
	}

	public function get_query_list_data($where_array, $limit, $offset, $user_id_array){
		
	 	$this->db->select('SQL_CALC_FOUND_ROWS *', FALSE);
        $this->db->where($where_array);
		$this->db->where("( query_creator_id IN ('".implode("', '", $user_id_array)."') OR query_assigned_id IN ('".implode("', '", $user_id_array)."') )", NULL, FALSE);
		$this->db->order_by('query_master.query_add_time', 'desc');
		$return_array['query_list'] = $this->db->get('query_master', $limit, $offset)->result_array();
        $return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows;")->row_array();
		return $return_array;
	}

	public function get_sub_query_list($query_type, $query_status, $user_id_array) {

		$this->db->select("sub_query_type, COUNT(*) AS count");
		if ($query_status === 'open') {

			$this->db->select("SUM(CASE WHEN query_status = 'open' THEN 1 ELSE 0 END) AS open,
							SUM(CASE WHEN reply_text != '' AND query_status = 'open' THEN 1 ELSE 0 END) AS done");
		} elseif ($query_status === 'closed') {

			$this->db->select("SUM(CASE WHEN query_status = 'closed' THEN 1 ELSE 0 END) AS closed");
		}
        $res = $this->db->where('status = "Active" AND query_type IN'.$query_type)
                ->where('query_status', $query_status)
                ->where_in('query_assigned_id', $user_id_array)
                ->group_by('sub_query_type')
                ->order_by('count', 'desc')
                ->get('query_master')
                ->result_array();
		// echo $this->query_model->db->last_query();
		return $res;
	}

	public function query_list($query_type, $query_status, $user_id_array) {
		
		$this->db->select("query_assigned_id, COUNT(*) AS count");
		if ($query_status === 'open') {

			$this->db->select("SUM(CASE WHEN query_status = 'open' THEN 1 ELSE 0 END) AS open,
							SUM(CASE WHEN reply_text != '' AND query_status = 'open' THEN 1 ELSE 0 END) AS done");
		} elseif ($query_status === 'closed') {

			$this->db->select("SUM(CASE WHEN query_status = 'closed' THEN 1 ELSE 0 END) AS closed");
		}
        $res = $this->db->where('status = "Active" AND query_type IN'.$query_type)
			->where('query_status', $query_status)
			->where_in('query_assigned_id', $user_id_array)
			->group_by('query_assigned_id')
			->order_by('count', 'desc')
			->get('query_master')
			->result_array();
		// echo $this->query_model->db->last_query();
		return $res;
	}
	
	public function user_id_under_this_role($role_id) {

		// echo $role_id; die;
		$this->db->select('user_id');
		if($role_id == 16) {

			$this->db->where_in('role', array('5', '16'));
		}else if($role_id == 5) {

			$this->db->where_in('user_id', $this->session->userdata('query_access')['query_sales_user_id']);
		}else if($role_id == 4) {

			$user_ids = array_merge(
				$this->session->userdata('query_access')['query_procurement_user_id'],
				$this->session->userdata('query_access')['query_production_user_id'],
				$this->session->userdata('query_access')['query_dataentry_user_id']
			);
			$user_ids = array_unique($user_ids);
			$this->db->where_in('user_id', $user_ids);

		}else if($role_id == 6) {

			if(in_array($this->session->userdata('user_id'), array(23))){

				$this->db->where_in('role', array('4', '6', '7', '8', '10', '11', '18', '21'));
			}else{

				$this->db->where_in('role', array('4', '6', '8', '10', '11', '21'));
			}

		}else if($role_id == 7) {
			
			$this->db->where_in('user_id', $this->session->userdata('query_access')['query_pq_user_id']);
		}else if($role_id == 8) {

			if(in_array($this->session->userdata('user_id'), array(180))){

				$user_ids = array_merge(
					$this->session->userdata('query_access')['query_procurement_user_id'],
					$this->session->userdata('query_access')['query_dataentry_user_id']
				);
				$user_ids = array_unique($user_ids);
				$this->db->where_in('user_id', $user_ids);
			}else{

				$this->db->where_in('user_id', $this->session->userdata('query_access')['query_procurement_user_id']);
			}
		}else if($role_id == 11) {

			$this->db->where_in('role', array('7', '10', '11'));
		}else if($role_id == 10) {

			$user_ids = array_merge(
				$this->session->userdata('query_access')['query_pq_user_id'],
				$this->session->userdata('query_access')['query_quality_user_id']
			);
			$user_ids = array_unique($user_ids);
			$this->db->where_in('user_id', $user_ids);

		}else if($role_id == 21) {

			$this->db->where('user_id', $this->session->userdata('user_id'));
		}else if($role_id == 24) {

			$this->db->where_in('role', array('7'));
		}else if($role_id == 18) {

			$user_ids = array_merge(
				$this->session->userdata('query_access')['query_procurement_user_id'],
				$this->session->userdata('query_access')['query_production_user_id'],
				$this->session->userdata('query_access')['query_dataentry_user_id']
			);
			$user_ids = array_unique($user_ids);
			$this->db->where_in('user_id', $user_ids);
		}
		$res = $this->db->get('users')->result_array();
		return array_column($res, 'user_id');
	}
	
	public function query_rfq_assign_details($rfq_mst_id){

		$this->db->select('*');
		$this->db->where(array('rfq_mst_id'=>$rfq_mst_id));
		return $this->db->get('rfq_mst')->row_array();
	}
	public function query_lead_assign_details($lead_id){

		$this->db->select('customer_mst.id, customer_mst.name, customer_mst.assigned_to, customer_data.product_category_id, product_category.product_name');
		$this->db->join('customer_data', 'customer_data.customer_mst_id = customer_mst.id', 'left');
		$this->db->join('product_category', 'product_category.id = customer_data.product_category_id', 'left');
		$this->db->where('customer_mst.deleted is null AND ((customer_data.product_category_id is not null AND customer_data.rank is not null) OR (customer_data.product_category_id is not null)) AND customer_mst.id ='.$lead_id);

		// echo "<pre>";print_r($this->db->last_query());echo"</pre><hr>";exit;
		return $this->db->get('customer_mst')->row_array();
	}
}
?>
