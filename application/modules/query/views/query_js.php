function ajax_call_function(data, callType, url = "<?php echo base_url('query/ajax_function'); ?>") {

    $.ajax({
        type: 'POST',
        data: data,
        url: url,
        dataType: 'JSON',
        success: function(res){
            if(res.status == 'successful') {
                switch(callType) {
                    case 'query_search_filter':
                    case 'change_query_tab':
                    case 'query_paggination':
                       $('tbody#query_listing').html('').html(res.table_body);
                       $('div#query_paggination').html('').html(res.table_paggination);
                        $('form#query_search_filter_form').html('').html(res.search_filter);
                       $('div#invoice_table_loader').hide();
                        dropdown_set_click_event();
                    break;
                    case'query_assigned_highchart':
                        query_assigned_highchart(res.query_highchart_category, res.query_highchart_data);
                        sub_query_highchart(res.sub_query_highchart_category, res.sub_query_highchart_data);
                    break;
                    case'delete_query':
                         toastr.success('Query is deleted successfully!');
                         setTimeout(function(){
                          location.reload();
                          }, 600)
                    break;
                    case'close_query':
                         toastr.success('Query is closed successfully!');
                         setTimeout(function(){
                          location.reload();
                          }, 600)
                    break
                    case'reopen_query':
                         toastr.success('Query is reopened successfully!');
                         setTimeout(function(){
                          location.reload();
                          }, 600)
                    break
                    case'send_sms':
                        toastr.success('Query  SMS is send successfully!');
                    break;
                    case 'get_production_query_history':

                        $('div#kt_chat_modal').modal('show');
                        $('em.header_1').html('').html(res.work_order_no);
                        $('span.header_2').html('').html('Work Order No');
                        $('div.div_chat_history').html(res.query_history_html);
                        $('textarea[name="query_text"]').val('');
                        autosize($('textarea[name="query_text"]'));
                        $('input[name="query_reference"]').val(res.work_order_no);
                        $('input[name="query_reference_id"]').val(res.quotation_id);
                        $('input[name="query_creator_id"]').val(res.creator_id);
                        $('input[name="query_assigned_id"]').val(res.query_assigned_id);
                        $('div.query_type').html(res.query_type_option_tag);
                        $('form#rfq_query_add_form').hide();
                        $('form#sales_query_add_form').hide();
                        $('form#production_query_add_form').show();
                        $('form#sample_query_add_form').hide();
                        $('form#pq_and_lead_query_add_form').hide();
                        $('form#osdr_query_add_form').hide();
                        $('.kt-selectpicker').selectpicker();
                        setTimeout(function(){

                            $('.kt-scroll').scrollTop($('.kt-scroll')[0].scrollHeight);
                            $('div.chat_loader').hide();
                        }, 300);                       
                    break;
                    case 'get_rfq_query_history':

                        $('div#kt_chat_modal').modal('show');
                        $('em.header_1').html('').html(res.query_reference);
                        $('span.header_2').html('').html('RFQ Quote No#');
                        $('div.div_chat_history').html(res.rfq_query_history);
                        $('textarea[name="query_text"]').val('');
                        autosize($('textarea[name="query_text"]'));
                        $('input[name="query_reference"]').val(res.query_reference);
                        $('input[name="query_reference_id"]').val(res.query_reference_id);
                        $('input[name="query_creator_id"]').val(res.query_creator_id);
                        $('input[name="query_assigned_id"]').val(res.query_assigned_id);
                        $('form#rfq_query_add_form').show();
                        $('form#sales_query_add_form').hide();
                        $('form#production_query_add_form').hide();
                        $('form#sample_query_add_form').hide();
                        $('form#pq_and_lead_query_add_form').hide();
                        $('form#osdr_query_add_form').hide();
                        setTimeout(function(){

                            $('.kt-scroll').scrollTop($('.kt-scroll')[0].scrollHeight);
                            $('div.chat_loader').hide();
                        }, 300);
                    break;
                    case 'save_production_query':
                        
                        ajax_call_function({call_type: 'get_production_query_history', production_query_quotation_id: $('form#production_query_add_form').serializeArray()[2]['value']},'get_production_query_history',"<?php echo base_url('query/ajax_function'); ?>");
                        toastr.clear();
                        toastr.success(res.message);
                    break;
                    case 'save_rfq_query':
                        
                        ajax_call_function({call_type: 'get_rfq_query_history', rfq_query_rfq_id: $('form#rfq_query_add_form').serializeArray()[2]['value']},'get_rfq_query_history');
                        toastr.clear();
                        toastr.success(res.message);
                    break;
                    case 'get_upload_history':

                        $('div#pdf_upload_history_mtc').html('').html(res.pdf_upload_history_mtc);
                        $('div#pdf_upload_history_drawing').html('').html(res.pdf_upload_history_drawing);
                        Listing_js.init();
                    break;
                    case 'get_sales_query_history':

                        $('div#kt_chat_modal').modal('show');
                        $('em.header_1').html('').html(res.quote_no);
                        $('span.header_2').html('').html('Quote No #');
                        $('div.div_chat_history').html(res.query_history_html);
                        $('textarea[name="query_text"]').val('');
                        autosize($('textarea[name="query_text"]'));
                        $('input[name="query_reference"]').val(res.quote_no);
                        $('input[name="query_reference_id"]').val(res.quotation_id);
                        $('input[name="query_creator_id"]').val(res.sales_person_id);
                        $('input[name="query_assigned_id"]').val(res.query_assigned_id);
                        $('div.query_type').html(res.query_type_option_tag);
                        $('form#rfq_query_add_form').hide();
                        $('form#sales_query_add_form').show();
                        $('form#production_query_add_form').hide();
                        $('form#sample_query_add_form').hide();
                        $('form#pq_and_lead_query_add_form').hide();
                        $('form#osdr_query_add_form').hide();
                        $('.kt-selectpicker').selectpicker();
                        setTimeout(function(){

                            $('.kt-scroll').scrollTop($('.kt-scroll')[0].scrollHeight);
                            $('div.chat_loader').hide();
                        }, 300);
                    break;
                    case 'get_sample_query_history':

                        $('div#kt_chat_modal').modal('show');
                        $('em.header_1').html('').html(res.reference);
                        $('div.div_chat_history').html(res.query_history_html);
                        $('textarea[name="query_text"]').val('');
                        autosize($('textarea[name="query_text"]'));
                        $('input[name="query_type"]').val(res.sample_type);
                        $('input[name="query_reference"]').val(res.reference);
                        $('input[name="query_reference_id"]').val(res.reference_id);
                        $('input[name="query_creator_id"]').val(res.sales_person_id);
                        $('div.query_type').html(res.query_type_option_tag);
                        $('form#rfq_query_add_form').hide();
                        $('form#sales_query_add_form').hide();
                        $('form#production_query_add_form').hide();
                        $('form#sample_query_add_form').show();
                        $('form#pq_and_lead_query_add_form').hide();
                        $('form#osdr_query_add_form').hide();
                        $('.kt-selectpicker').selectpicker();
                        setTimeout(function () {

                            $('.kt-scroll').scrollTop($('.kt-scroll')[0].scrollHeight);
                            $('div.chat_loader').hide();
                        }, 300);
                    break;
                    case 'get_pq_and_lead_query_history':
                        // console.log(res)
                        $('div#kt_chat_modal').modal('show');
                        $('em.header_1').html('').html(res.reference);
                        $('div.div_chat_history').html(res.query_history_html);
                        $('textarea[name="query_text"]').val('');
                        autosize($('textarea[name="query_text"]'));
                        $('input[name="query_type"]').val(res.query_type);
                        $('input[name="query_reference"]').val(res.reference);
                        $('input[name="query_reference_id"]').val(res.reference_id);
                        $('input[name="query_creator_id"]').val(res.sales_person_id);
                        $('input[name="query_assigned_id"]').val(res.query_assigned_id);
                        $('div.query_type').html(res.query_type_option_tag);
                        $('form#rfq_query_add_form').hide();
                        $('form#sales_query_add_form').hide();
                        $('form#production_query_add_form').hide();
                        $('form#sample_query_add_form').hide();
                        $('form#pq_and_lead_query_add_form').show();
                        $('form#osdr_query_add_form').hide();
                        $('.kt-selectpicker').selectpicker();
                        setTimeout(function () {

                            $('.kt-scroll').scrollTop($('.kt-scroll')[0].scrollHeight);
                            $('div.chat_loader').hide();
                        }, 300);
                    break;
                    case 'save_query_comment_form_details':
                        
                        toastr.clear();
                        toastr.success("Comment is Added Successfully!");
                    break;
                    case 'get_osdr_query_history':

                        $('div#kt_chat_modal').modal('show');
                        $('em.header_1').html('').html(res.work_order_no);
                        $('span.header_2').html('').html('Work Order No');
                        $('div.div_chat_history').html(res.query_history_html);
                        $('textarea[name="query_text"]').val('');
                        autosize($('textarea[name="query_text"]'));
                        $('input[name="query_reference"]').val(res.work_order_no);
                        $('input[name="query_reference_id"]').val(res.quotation_id);
                        $('input[name="query_creator_id"]').val(res.creator_id);
                        $('input[name="query_assigned_id"]').val(res.query_assigned_id);
                        $('div.query_type').html(res.query_type_option_tag);
                        $('form#rfq_query_add_form').hide();
                        $('form#sales_query_add_form').hide();
                        $('form#production_query_add_form').hide();
                        $('form#sample_query_add_form').hide();
                        $('form#pq_and_lead_query_add_form').hide();
                        $('form#osdr_query_add_form').show();
                        $('.kt-selectpicker').selectpicker();
                        setTimeout(function () {

                            $('.kt-scroll').scrollTop($('.kt-scroll')[0].scrollHeight);
                            $('div.chat_loader').hide();
                        }, 300);
                    break;
                    case 'save_osdr_query':

                        ajax_call_function({ call_type: 'get_osdr_query_history', osdr_query_quotation_id: $('form#osdr_query_add_form').serializeArray()[2]['value'] }, 'get_osdr_query_history', "<?php echo base_url('query/ajax_function'); ?>");
                        toastr.clear();
                        toastr.success(res.message);
                    break;
                    case 'get_osdr_document_history':

                        $('div#osdr_document_history').html('').html(res.production_osdr_document_history);
                        Listing_js.init();
                    break;
                    case 'change_pq_status':
                        ajax_call_function({ call_type: 'change_query_tab', main_tab_name: get_main_tab_name(), sub_tab_name: get_sub_tab_name() }, 'change_query_tab');
                        swal({
                            title: "PQ Status Changes",
                            icon: "success",
                        });
                    break;
                }
            }
        },
        beforeSend: function(response){
            switch(callType) {
                case 'query_search_filter':
                case 'change_query_tab':
                case 'query_paggination':
                    $('div#invoice_table_loader').show();
                break;
            }
        }
    });
};
function sweet_alert(title, icon = 'success') {

    swal({
            title: title,
            icon: icon,
        });
}
jQuery(document).ready(function() {

    dropdown_set_click_event();
    
    $('li.change_manage_tab').click(function(){
        var query_type =$(this).attr('query_type');
        var query_status =$(this).attr('query_status');
        if(!$(this).hasClass('active')){
            $('li.change_manage_tab').removeClass('active');
            $($(this)).addClass('active');
            ajax_call_function({call_type: 'get_management_tab_details',tab_name: $(this).attr('tab-name'), query_type: $(this).attr('query_type'), query_status: $(this).attr('query_status')}, 'get_management_tab_details');
        }
    });

    $('div#query_paggination').on('click', '.query_list_paggination_number', function(){

        ajax_call_function({
                            call_type: 'paggination_filter',
                            limit: $(this).attr('limit'),
                            offset: $(this).attr('offset'),
                            search_form_data: $('form#query_search_filter_form').serializeArray(),
                        }, 'paggination_filter');
    });
    $('div#query_paggination').on('change', 'select#set_limit', function(){

        ajax_call_function({
                            call_type: 'paggination_filter',
                            limit: $(this).val(),
                            offset: 0,
                            search_form_data: $('form#query_search_filter_form').serializeArray(),
                        }, 'paggination_filter');
    });
    $('div.query_search_filter_form_div').on('click', '.query_search_filter_form_submit', function(){

        ajax_call_function({
                            call_type: 'search_filter',
                            limit: $('input#query_list_paggination_limit').val(),
                            offset: $('input#query_list_paggination_offset').val(),
                            search_form_data: $('form#query_search_filter_form').serializeArray(),
                        }, 'search_filter'); 
    });
    $('div.query_search_filter_form_div').on('click', '.query_search_filter_form_reset', function(){

        ajax_call_function({
                            call_type: 'search_filter',
                            limit: $('input#query_list_paggination_limit').val(),
                            offset: $('input#query_list_paggination_offset').val(),
                            search_form_data: [],
                        }, 'search_filter'); 
    });

    $('tbody#query_body').on('click', 'i.get_history', function(){
        ajax_call_function({call_type:'get_history',query_id:$(this).attr('query-id')},'get_history');
    });

    $('button.reply_query').click(function(){

    	ajax_call_function({call_type:'reply_query',query_id:$('input#query_id').val(),entered_by: $('input#entered_by').val(),query_text:$('textarea#query_text').val()},'reply_query');
    });

    $('a.add_query_search_filter_form').click(function(){

    	$('div.query_search_filter_div').show();
    });

    $('tbody#query_body').on('click', 'i.close_query', function(){
        swal({
            title: "Are you sure?",
            icon: "warning",
            buttons:  ["Cancel", "close query"],
            dangerMode: true, 
        })
        .then((willDelete) => {
            if (willDelete) {
                ajax_call_function({call_type:'close_query',query_id:$(this).attr('query-id')},'close_query');
                swal({
                    title: "Query details is closed",
                    icon: "success",
                });
                location.reload();
            } else {
                swal({
                    title: "Query is not closed",
                    icon: "info",
                });
                location.reload();
            }
        });
    });
    KTBootstrapSelect.init();
    add_date_picker();
    ajax_call_function({call_type: 'query_assigned_highchart', main_tab_name: get_main_tab_name(), sub_tab_name: get_sub_tab_name()}, 'query_assigned_highchart');
    $('.kt-selectpicker').selectpicker();
    $('li.main_tab_name').click(function(){
        var current_main_tab_name = get_main_tab_name();
        if(current_main_tab_name == $(this).attr('tab-name')) {
            return false;
        }
        $('li.main_tab_name').removeClass('active_list');
        $(this).addClass('active_list');
        if($(this).attr('tab-name') == "sales_query"){
            document.getElementById("quote_type").textContent = "Quotation No Details";
        }
        else if($(this).attr('tab-name') == "proforma_query"){
           document.getElementById("quote_type").textContent = "Profoma #";
        }
        else if($(this).attr('tab-name') == "production_query"){
            document.getElementById("quote_type").textContent = "Work Order No Details";
        }
        else if($(this).attr('tab-name') == "rfq_query"){
           document.getElementById("quote_type").textContent = "RFQ No Details";
        }
        else if ($(this).attr('tab-name') == "sample_query"){
           document.getElementById("quote_type").textContent = "Sample Details";
        }
        else if ($(this).attr('tab-name') == "pq_query"){
            document.getElementById("quote_type").textContent = "PQ Details";
        }
        else if ($(this).attr('tab-name') == "osdr_query") {
            document.getElementById("quote_type").textContent = "Work Order No Details";
        }
        else if ($(this).attr('tab-name') == "lead_query") {
            document.getElementById("quote_type").textContent = "Lead Details";
        }
        ajax_call_function({call_type: 'change_query_tab', main_tab_name: $(this).attr('tab-name'), sub_tab_name: get_sub_tab_name(), limit: $('input#query_list_paggination_limit').val(), offset: $('input#query_list_paggination_offset').val(),form_data:$('form#query_search_filter_form').serializeArray()},'change_query_tab');
        ajax_call_function({call_type: 'query_assigned_highchart', main_tab_name: get_main_tab_name(), sub_tab_name: get_sub_tab_name()}, 'query_assigned_highchart');
        
    }); 
    $('li.sub_tab_name').click(function(){
        var current_sub_tab_name = get_sub_tab_name();
        if(current_sub_tab_name == $(this).attr('tab-name')) {
            return false;
        }
        $('li.sub_tab_name').removeClass('active_list');
        $(this).addClass('active_list');
        ajax_call_function({call_type: 'change_query_tab', main_tab_name: get_main_tab_name(), sub_tab_name: $(this).attr('tab-name'), limit: $('input#query_list_paggination_limit').val(), offset: $('input#query_list_paggination_offset').val(),form_data:$('form#query_search_filter_form').serializeArray()},'change_query_tab');
        ajax_call_function({call_type: 'query_assigned_highchart', main_tab_name: get_main_tab_name(), sub_tab_name: $(this).attr('tab-name')}, 'query_assigned_highchart');
    });
    $('div#query_paggination').on('click', 'li.query_list_paggination_number', function(){

        ajax_call_function({call_type: 'query_paggination', main_tab_name: get_main_tab_name(), sub_tab_name: get_sub_tab_name(), limit: $(this).attr('limit'), offset: $(this).attr('offset'), form_data:$('form#query_search_filter_form').serializeArray()},'query_paggination');

    });
    $('div#query_paggination').on('change', 'select#set_limit', function(){

        ajax_call_function({call_type: 'query_paggination', main_tab_name: get_main_tab_name(), sub_tab_name: get_sub_tab_name(), limit: $('select#set_limit').val(), offset: 0,form_data:$('form#query_search_filter_form').serializeArray()},'query_paggination');
    });
    $('a.add_query_search_filter_form').click(function(){
        
        $('div.query_listing_search_filter').show();
    });
    $('div.query_listing_search_filter').on('click', 'button.query_search_filter_form_submit', function(){

        ajax_call_function({call_type: 'query_search_filter', main_tab_name: get_main_tab_name(), sub_tab_name: get_sub_tab_name(), limit: $('input#query_list_paggination_limit').val(), offset: $('input#query_list_paggination_offset').val(),form_data:$('form#query_search_filter_form').serializeArray()},'query_search_filter');
    });
    $('div.query_listing_search_filter').on('click', 'button.query_search_filter_form_reset', function(){

        ajax_call_function({call_type: 'query_search_filter', main_tab_name: get_main_tab_name(), sub_tab_name: get_sub_tab_name(), limit: $('input#query_list_paggination_limit').val(), offset: $('input#query_list_paggination_offset').val(),form_data:[]},'query_search_filter');
    });

    $('tbody#query_listing').on('click','button.close_query',function(){
        swal({
              title: "Are you sure?",
              icon: "warning",
              buttons:  ["Cancel", "Close"],
              dangerMode: true, 
            })
        .then((willDelete) => {
            if (willDelete) {
                ajax_call_function({call_type: 'close_query',query_id:$(this).attr('query_id')},'close_query');
            } else {
                swal({
                    title: "Query is not closed",
                    icon: "info",
                });
            }
        });
    });
    $('tbody#query_listing').on('click','button.deletequery',function(){
        swal({
            title: "Are you sure?",
            icon: "warning",
            buttons:  ["Cancel", "Delete"],
            dangerMode: true, 
        })
        .then((willDelete) => {
            if (willDelete) {
                ajax_call_function({call_type: 'delete_query',query_id:$(this).attr('query_id')},'delete_query');
            } else {
                swal({
                    title: "Query is not deleted",
                    icon: "info",
                });
            }
        });
    });
    $('tbody#query_listing').on('click','button.open_query',function(){
        swal({
            title: "Are you sure?",
            icon: "warning",
            buttons:  ["Cancel", "Re Open"],
            dangerMode: true, 
        })
        .then((willDelete) => {
            if (willDelete) {
                ajax_call_function({call_type: 'reopen_query',query_id:$(this).attr('query_id')},'reopen_query');
            } else {
                swal({
                    title: "Query is not reopen",
                    icon: "info",
                });
            }
        });
    });
    $('tbody#query_listing').on('click','button.send_sms',function(){
        ajax_call_function({call_type: 'query_overdue_sms_alert',sender_id:$(this).attr('query_creator_id'),assigned_id:$(this).attr('assigned_id')},'send_sms',"<?php echo base_url('query/ajax_function'); ?>");
    });
    $("tbody#query_listing").on('click', '.sales_query', function(){
        
        ajax_call_function({call_type: 'get_sales_query_history', sales_query_quotation_id: $(this).attr('quote_id')},'get_sales_query_history',"<?php echo base_url('query/ajax_function'); ?>");
        $('div.chat_loader').show();
    });
    $('button.add_sales_query').click(function(){

        $.ajax({
            type: 'POST',
            data: {'call_type': 'save_sales_query', form_data: $('form#sales_query_add_form').serializeArray()},
            url: "<?php echo base_url('query/ajax_function'); ?>",
            dataType: 'JSON',
            success: function(res){
                toastr.clear();
                if(res.status == 'successful') {

                    toastr.success(res.message);
                    ajax_call_function({call_type: 'get_sales_query_history', sales_query_quotation_id: $('form#sales_query_add_form').serializeArray()[2]['value']},'get_sales_query_history',"<?php echo base_url('query/ajax_function'); ?>");
                }else{

                    toastr.error(res.message);
                }
            },
            beforeSend: function(response){
                toastr.clear();
                toastr.warning('Saving of the query has begun.');
            }
        });
    });
    $("tbody#query_listing").on('click', '.proforma_query', function(){
        $.ajax({
            type: "POST",
            data: {"call_type": "get_proforma_query_history", 'proforma_query_quotation_id':$(this).attr('quote_id')},
            url: "<?php echo site_url('query/ajax_function'); ?>",
            dataType: 'JSON',
            success: function(res){
                $('div#proforma_query_modal').modal('show');
                $('input[name="query_reference"]').val(res.proforma_no);
                $('input[name="query_reference_id"]').val(res.quotation_id);
                $('input[name="query_creator_id"]').val(res.creator_id);
                $('div.query_type').html(res.query_type_option_tag);
                $('textarea[name="query_text"]').val('');
                $('div.sales_query_history').html(res.query_history_html);
                $('.kt-selectpicker').selectpicker();
            }
        });
    });
    $('button.add_proforma_query').click(function(){

        $.ajax({
            type: 'POST',
            data: {'call_type': 'save_proforma_query', form_data: $('form#proforma_query_add_form').serializeArray()},
            url: "<?php echo base_url('query/ajax_function'); ?>",
            dataType: 'JSON',
            success: function(res){
                toastr.clear();
                if(res.status == 'successful') {
                    $('div#proforma_query_modal').modal('hide');
                    toastr.success(res.message);
                }else{

                    toastr.error(res.message);
                }
            },
            beforeSend: function(response){
                toastr.clear();
                toastr.warning('Saving of the query has begun.');
            }
        });
    });
    $("tbody#query_listing").on('click', '.production_query', function(){
       
        $('div.chat_loader').show();
        ajax_call_function({call_type: 'get_production_query_history', production_query_quotation_id: $(this).attr('quote_id')},'get_production_query_history',"<?php echo base_url('query/ajax_function'); ?>");

    });
    $('button.add_production_query').click(function(){

        ajax_call_function({call_type: 'save_production_query', form_data: $('form#production_query_add_form').serializeArray()},'save_production_query',"<?php echo base_url('query/ajax_function'); ?>");
    });

    $('button.add_rfq_query').click(function(){

        $('div.chat_loader').show();
        ajax_call_function({call_type: 'save_rfq_query', form_data: $('form#rfq_query_add_form').serializeArray()},'save_rfq_query',"<?php echo base_url('query/ajax_function'); ?>");
    });

    $("tbody#query_listing").on('click', '.rfq_query', function(){
        ajax_call_function({call_type: 'get_rfq_query_history', rfq_query_rfq_id: $(this).attr('quote_id')},'get_rfq_query_history');
    });

    $("tbody#query_listing").on('click', 'button.upload_pdf', function(){

        ajax_call_function({call_type: 'get_upload_history', query_number: $(this).attr('query_number')},'get_upload_history');
    });

    $("tbody#query_listing").on('click', 'a.get_comment', function(){

        $('button.save_comment').attr('query_master_id', $(this).attr('query_master_id'));
        $('div#query_comment').modal('show');
        autosize($('textarea[name="query_comment"]'));
    });
    $('button.save_comment').click(function(){

        toastr.clear();
        toastr.info("Comment is Getting Added");
        ajax_call_function({call_type: 'save_query_comment_form_details', query_master_id: $(this).attr('query_master_id'), comments: $('textarea[name="query_comment"]').val()},'save_query_comment_form_details');
    });
    $("tbody#query_listing").on('click', '.sample_query', function () {

        ajax_call_function({ call_type: 'get_sample_query_history', sample_id: $(this).attr('lead_id'), sample_type: $(this).attr('query_type') }, 'get_sample_query_history', "<?php echo base_url('query/ajax_function'); ?>");
        $('div.chat_loader').show();
    });
    $('button.add_sample_query').click(function () {

        $.ajax({
            type: 'POST',
            data: { 'call_type': 'save_sample_query', form_data: $('form#sample_query_add_form').serializeArray() },
            url: "<?php echo base_url('query/ajax_function'); ?>",
            dataType: 'JSON',
            success: function (res) {
                toastr.clear();
                if (res.status == 'successful') {

                    toastr.success(res.message);
                    ajax_call_function({ call_type: 'get_quotation_sample_query_history', quotation_id: $('form#sample_query_add_form').serializeArray()[3]['value'] }, 'get_quotation_sample_query_history', "<?php echo base_url('query/ajax_function'); ?>");

                } else {

                    toastr.error(res.message);
                }
            },
            beforeSend: function (response) {
                toastr.clear();
                toastr.warning('Saving of the query has begun.');
            }
        });
    });

    $("tbody#query_listing").on('click', '.pq_query', function () {

        ajax_call_function({ call_type: 'get_pq_and_lead_query_history', 'client_id': $(this).attr('lead_id'), query_type: $(this).attr('query_type')}, 'get_pq_and_lead_query_history', "<?php echo base_url('query/ajax_function'); ?>");
        $('div.chat_loader').show();
    });

    $("tbody#query_listing").on('click', '.lead_query', function () {

        ajax_call_function({ call_type: 'get_pq_and_lead_query_history', 'client_id': $(this).attr('lead_id'), query_type: $(this).attr('query_type') }, 'get_pq_and_lead_query_history', "<?php echo base_url('query/ajax_function'); ?>");
        $('div.chat_loader').show();
    });

    $('button.add_pq_and_lead_query').click(function () {

        $.ajax({
            type: 'POST',
            data: { 'call_type': 'save_pq_and_lead_query', form_data: $('form#pq_and_lead_query_add_form').serializeArray() },
            url: "<?php echo base_url('query/ajax_function'); ?>",
            dataType: 'JSON',
            success: function (res) {
                toastr.clear();
                if (res.status == 'successful') {
                    toastr.success(res.message);
                    ajax_call_function({ call_type: 'get_pq_and_lead_query_history', client_id: $('form#pq_and_lead_query_add_form').serializeArray()[2]['value'] }, 'get_pq_and_lead_query_history', "<?php echo base_url('query/ajax_function'); ?>");

                } else {
                    toastr.error(res.message);

                }
            },
            beforeSend: function (response) {
                toastr.clear();
                toastr.warning('Saving of the query has begun.');
            }
        });
    });

    $("tbody#query_listing").on('click', '.osdr_query', function () {

        $('div.chat_loader').show();
        ajax_call_function({ call_type: 'get_osdr_query_history', osdr_query_quotation_id: $(this).attr('quote_id') }, 'get_osdr_query_history', "<?php echo base_url('query/ajax_function'); ?>");
    });
    $('button.add_osdr_query').click(function () {

        ajax_call_function({ call_type: 'save_osdr_query', form_data: $('form#osdr_query_add_form').serializeArray() }, 'save_osdr_query', "<?php echo base_url('query/ajax_function'); ?>");
    });
    $("tbody#query_listing").on('click', 'button.osdr_document', function () {

        ajax_call_function({ call_type: 'get_osdr_document_history', work_order_no: $(this).attr('work_order_no') }, 'get_osdr_document_history', "<?php echo base_url('query/ajax_function'); ?>");
    });

    $("form#sales_query_add_form").on('change', 'select[name="sub_query_type"]', function () {
        var type = $(this).val();
        var assigned_id = $("input[name='query_assigned_id']").val();

        if (type == 'Sample MTC') {

            $("input[name='query_assigned_id']").val(302);
        } else if (type == 'Drawing') {

            $("input[name='query_assigned_id']").val(325);
        } else {

            $("input[name='query_assigned_id']").val(assigned_id);
        }
    });

    $("form#production_query_add_form").on('change', 'select[name="sub_query_type"]', function () {
        var type = $(this).val();
        var assigned_id = $("input[name='query_assigned_id']").val();

        if (type == 'Physical Inspection' || type == 'Material Readiness' || type == 'Payment' || type == 'Dispatch') {

            $("input[name='query_assigned_id']").val(assigned_id);
        } else if (type == 'MTC' || type == 'QC') {

            $("input[name='query_assigned_id']").val(297);
        } else {

            $("input[name='query_assigned_id']").val(assigned_id);
        }
    });

    $("form#pq_and_lead_query_add_form").on('change', 'select[name="sub_query_type"]', function () {
        var type = $(this).val();
        var assigned_id = $("input[name='query_assigned_id']").val();

        if (type == 'Registration') {

            $("input[name='query_assigned_id']").val(assigned_id);
            $("input[name='query_type']").val("pq_query");
        } else if (type == 'Other') {

            $("input[name='query_assigned_id']").val(2);
            $("input[name='query_type']").val("lead_query");
        }
    });

});


function add_date_picker() {

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }
    $('#kt_datepicker_3, #kt_datepicker_3_validate').datepicker({
        rtl: KTUtil.isRTL(),
        todayBtn: "linked",
        clearBtn: true,
        todayHighlight: true,
        templates: arrows
    });    
}


function search_filter(){
    ajax_call_function({
                        call_type: 'query_search_filter',
                        limit: $(this).attr('limit'),
                        offset: $(this).attr('offset'),
                        search_form_data: $('form#query_search_filter_form').serializeArray(),
                        invoice_sort_name: $('#column_name').val(),
                        invoice_sort_value: $('#column_value').val(),
                    }, 'search_filter');
}

function get_main_tab_name() {

    if($('li.sales_query').hasClass('active_list')) {

        $('span.sub_tab_count').hide();
        $('span.sub_tab_sales_count').show();
        return 'sales_query';
    } else if($('li.production_query').hasClass('active_list')) {

        $('span.sub_tab_count').hide();
        $('span.sub_tab_production_count').show();
        return 'production_query';
    } else if($('li.rfq_query').hasClass('active_list')) {

        $('span.sub_tab_count').hide();
        $('span.sub_tab_rfq_count').show();
        return 'rfq_query';
    } else if($('li.sample_query').hasClass('active_list')) {

        $('span.sub_tab_count').hide();
        $('span.sub_tab_sample_count').show();
        return 'sample_query';
    } else if($('li.pq_query').hasClass('active_list')) {

        $('span.sub_tab_count').hide();
        $('span.sub_tab_pqq_count').show();
        return 'pq_query';
    } else if ($('li.osdr_query').hasClass('active_list')) {

        $('span.sub_tab_count').hide();
        $('span.sub_tab_osdr_count').show();
        return 'osdr_query';
    } else if ($('li.lead_query').hasClass('active_list')) {

        $('span.sub_tab_count').hide();
        $('span.sub_tab_lead_count').show();
        return 'lead_query';
    }
    return '';
}

function get_sub_tab_name() {
    if($('li.open').hasClass('active_list')) {
        return 'open';
    } else if($('li.closed').hasClass('active_list')) {
        return 'closed';
    }
    return '';
}

function dropdown_set_click_event() {
    const dropdownItems = document.querySelectorAll("tbody#query_listing .dropdown-item");
    dropdownItems.forEach(item => {
        item.addEventListener("click", function (event) {
            event.preventDefault();
            const value = this.getAttribute("value");
            const id = this.getAttribute("id");
            console.log(id);
            const classList = this.classList;

            if (typeof id !== 'undefined' && id.length !== 0 && typeof value !== 'undefined' && value.length !== 0) {
                if (classList.contains('change_status')) {
                ajax_call_function({
                    call_type: 'change_pq_status',
                    id: id,
                    pq_status: value,
                    is_pq: this.getAttribute("is_pq"),
                    query_type: this.getAttribute("query_type"),
                }, 'change_pq_status');
                }
                else if (classList.contains('change_assigned_to')) {
                    ajax_call_function({
                        call_type: 'query_reassigned_to',
                        id: id,
                        assigned_to: value,
                        query_number: this.getAttribute("query_number"),
                        query_type: this.getAttribute("query_type"),
                    }, 'query_reassigned_to');
                }
            }
        });
    });
}

function query_assigned_highchart(category, highchart_data) {

    Highcharts.chart('query_assigned_highchart', {
        // Create the chart
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            // categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
            categories: category
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total Query'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: highchart_data
    });
}

//seema khade
function sub_query_highchart(category, highchart_data) {

    Highcharts.chart('sub_query_highchart', {
        // Create the chart
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            // categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
            categories: category
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total Query'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: highchart_data
    });
}

var Listing_js = function () {

    var mtc_pdf = function () {
        // set the dropzone container id
        var id = '#kt_dropzone_mtc_pdf';

        // set the preview element template
        var previewNode = $(id + " .dropzone-item");
        previewNode.id = "";
        var previewTemplate = previewNode.parent('.dropzone-items').html();
        previewNode.remove();

        var myDropzone4 = new Dropzone(id, { // Make the whole body a dropzone
            url: "<?php echo base_url('query/ajax_function');?>", // Set the url for your upload script location
            params: {'call_type': 'upload_mtc_drawing', 'pdf_type': 'MTC'},
            parallelUploads: 3,
            previewTemplate: previewTemplate,
            maxFilesize: 5, // Max filesize in MB
            autoQueue: false, // Make sure the files aren't queued until manually added
            previewsContainer: id + " .dropzone-items", // Define the container to display the previews
            clickable: id + " .dropzone-select" // Define the element that should be used as click trigger to select files.
        });

        myDropzone4.on("addedfile", function(file) {
            // Hookup the start button
            file.previewElement.querySelector(id + " .dropzone-start").onclick = function() { myDropzone4.enqueueFile(file); };
            $(document).find( id + ' .dropzone-item').css('display', '');
            $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'inline-block');
        });

        // Update the total progress bar
        myDropzone4.on("totaluploadprogress", function(progress) {
            $(this).find( id + " .progress-bar").css('width', progress + "%");
        });

        myDropzone4.on("sending", function(file) {
            // Show the total progress bar when upload starts
            $( id + " .progress-bar").css('opacity', '1');
            // And disable the start button
            file.previewElement.querySelector(id + " .dropzone-start").setAttribute("disabled", "disabled");
        });

        // Hide the total progress bar when nothing's uploading anymore
        myDropzone4.on("complete", function(progress) {
            var thisProgressBar = id + " .dz-complete";
            setTimeout(function(){
                $( thisProgressBar + " .progress-bar, " + thisProgressBar + " .progress, " + thisProgressBar + " .dropzone-start").css('opacity', '0');
            }, 300)
            toastr.success("MTC Pdf is Uploaded!!!.");
            ajax_call_function({call_type: 'get_upload_history'},'get_upload_history');
        });

        // Setup the buttons for all transfers
        document.querySelector( id + " .dropzone-upload").onclick = function() {
            myDropzone4.enqueueFiles(myDropzone4.getFilesWithStatus(Dropzone.ADDED));
        };

        // Setup the button for remove all files
        document.querySelector(id + " .dropzone-remove-all").onclick = function() {
            $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
            myDropzone4.removeAllFiles(true);
        };

        // On all files completed upload
        myDropzone4.on("queuecomplete", function(progress){
            $( id + " .dropzone-upload").css('display', 'none');
        });

        // On all files removed
        myDropzone4.on("removedfile", function(file){
            if(myDropzone4.files.length < 1){
                $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
            }
        });
    }
    var drawing_pdf = function () {
        // set the dropzone container id
        var id = '#kt_dropzone_drawing_pdf';

        // set the preview element template
        var previewNode = $(id + " .dropzone-item");
        previewNode.id = "";
        var previewTemplate = previewNode.parent('.dropzone-items').html();
        previewNode.remove();

        var myDropzone4 = new Dropzone(id, { // Make the whole body a dropzone
            url: "<?php echo base_url('query/ajax_function');?>", // Set the url for your upload script location
            params: {'call_type': 'upload_mtc_drawing', 'pdf_type': 'Drawing'},
            parallelUploads: 3,
            previewTemplate: previewTemplate,
            maxFilesize: 5, // Max filesize in MB
            autoQueue: false, // Make sure the files aren't queued until manually added
            previewsContainer: id + " .dropzone-items", // Define the container to display the previews
            clickable: id + " .dropzone-select" // Define the element that should be used as click trigger to select files.
        });

        myDropzone4.on("addedfile", function(file) {
            // Hookup the start button
            file.previewElement.querySelector(id + " .dropzone-start").onclick = function() { myDropzone4.enqueueFile(file); };
            $(document).find( id + ' .dropzone-item').css('display', '');
            $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'inline-block');
        });

        // Update the total progress bar
        myDropzone4.on("totaluploadprogress", function(progress) {
            $(this).find( id + " .progress-bar").css('width', progress + "%");
        });

        myDropzone4.on("sending", function(file) {
            // Show the total progress bar when upload starts
            $( id + " .progress-bar").css('opacity', '1');
            // And disable the start button
            file.previewElement.querySelector(id + " .dropzone-start").setAttribute("disabled", "disabled");
        });

        // Hide the total progress bar when nothing's uploading anymore
        myDropzone4.on("complete", function(progress) {
            var thisProgressBar = id + " .dz-complete";
            setTimeout(function(){
                $( thisProgressBar + " .progress-bar, " + thisProgressBar + " .progress, " + thisProgressBar + " .dropzone-start").css('opacity', '0');
            }, 300)
            toastr.success("Drawing is Uploaded!!!.");
            ajax_call_function({call_type: 'get_upload_history'},'get_upload_history');
        });

        // Setup the buttons for all transfers
        document.querySelector( id + " .dropzone-upload").onclick = function() {
            myDropzone4.enqueueFiles(myDropzone4.getFilesWithStatus(Dropzone.ADDED));
        };

        // Setup the button for remove all files
        document.querySelector(id + " .dropzone-remove-all").onclick = function() {
            $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
            myDropzone4.removeAllFiles(true);
        };

        // On all files completed upload
        myDropzone4.on("queuecomplete", function(progress){
            $( id + " .dropzone-upload").css('display', 'none');
        });

        // On all files removed
        myDropzone4.on("removedfile", function(file){
            if(myDropzone4.files.length < 1){
                $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
            }
        });
    }
    return {
        // public functions
        init: function() {
            mtc_pdf();
            drawing_pdf();
        }
    };
}();