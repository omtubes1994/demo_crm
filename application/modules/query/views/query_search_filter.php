
<!--begin::Form-->
<form id="query_search_filter_form" class="kt-form kt-form--label-right" style="display:;">
    <div class="kt-portlet__body query_search_filter_form_div">
        <div class="form-group row">
            <label class="col-form-label col-lg-1 col-sm-12">Quotation NO#</label>
            <div class="col-lg-2 col-md-9 col-sm-12">
               <select class="form-control kt-selectpicker" data-size="7" name="query_reference" data-live-search="true">
                    <option value="">Select</option>
                    <?php foreach($quote_no as  $single_details) {?>
                        <option value="<?php echo $single_details; ?>"><?php echo $single_details; ?></option>
                    <?php } ?>
                </select>
            </div>
            <?php if(in_array($this->session->userdata('role'), array(1, 16, 6, 11, 17))) {?>
                <label class="col-form-label col-lg-1 col-sm-12">Query Creater</label>
                <div class="col-lg-2 col-md-9 col-sm-12">
                    <select class="form-control kt-selectpicker" data-size="7" name="query_creator_id" data-live-search="true">
                        <option value="">Select</option>
                        <?php 
                        foreach(array_unique($query_creator_id) as $single_details) {
                            if($single_details > 0){

                                echo "<option value=".$single_details.">";
                                    echo $user_details[$single_details];
                                echo "</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
                <label class="col-form-label col-lg-1 col-sm-12">Query Assinged To </label>
                <div class="col-lg-2 col-md-9 col-sm-12">
                    <select class="form-control kt-selectpicker" data-size="7" name="query_assigned_id" data-live-search="true">
                        <option value="">Select</option>
                        <?php 
                        foreach(array_unique($query_assigned_id) as $single_details) {
                            if($single_details > 0){
                                
                                echo "<option value=".$single_details.">";
                                    echo $user_details[$single_details];
                                echo "</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
            <?php  } ?>
            <label class="col-form-label col-lg-1 col-sm-12"> Query raised Date</label>
            <div class="col-lg-2 col-md-9 col-sm-12">
                <div class="input-group date">
                    <input type="text" class="form-control" readonly value="<?php //echo $data['selected_date'];?>" name="add_time" id="kt_datepicker_3"/>
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <i class="la la-calendar"></i>
                        </span>
                    </div>
                </div>
            </div>            
        </div>
        <?php if($tab_name == 'sample_query'){?>
        <div class="form-group row">
            <label class="col-form-label col-lg-1 col-sm-12"> Sample Query Type</label>
            <div class="col-lg-2 col-md-9 col-sm-12">
                <select class="form-control kt-selectpicker" data-size="7" name="query_type" data-live-search="true">
                    <option value="">Select</option>
                    <option value="quotation_sample_query">Quotation</option>
                    <option value="lead_sample_query">Lead</option>
                </select>
            </div>
        </div>
        <?php }?>
        <div class="kt-separator kt-separator--md kt-separator--dashed"></div>
            <div class="row">
                <div class="col-lg-12 query_search_filter">
                    <button class="btn btn-primary btn-brand--icon query_search_filter_form_submit" type="button">
                        <span>
                            <i class="la la-search"></i>
                            <span>Search</span>
                        </span>
                    </button>
                    &nbsp;&nbsp;
                    <button class="btn btn-secondary btn-secondary--icon query_search_filter_form_reset" type="reset">
                        <span>
                            <i class="la la-close"></i>
                            <span>Reset</span>
                        </span>
                    </button>
                </div>
            </div>
    </div>
</form>

<!--end::Form-->