<style type="text/css">
	
	.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper {
		padding-top: 60px !important;
	}
</style>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

	<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="alert alert-light alert-elevate" role="alert">
			<div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
			<div class="alert-text">Search Filter</div>
		</div>
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon2-line-chart"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						Basic
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="kt-portlet__head-actions">
							<div class="dropdown dropdown-inline">
								<button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="la la-download"></i> Export
								</button>
								<div class="dropdown-menu dropdown-menu-right">
									<ul class="kt-nav">
										<li class="kt-nav__section kt-nav__section--first">
											<span class="kt-nav__section-text">Choose an option</span>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link">
												<i class="kt-nav__link-icon la la-print"></i>
												<span class="kt-nav__link-text">Print</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link">
												<i class="kt-nav__link-icon la la-copy"></i>
												<span class="kt-nav__link-text">Copy</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link">
												<i class="kt-nav__link-icon la la-file-excel-o"></i>
												<span class="kt-nav__link-text">Excel</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link">
												<i class="kt-nav__link-icon la la-file-text-o"></i>
												<span class="kt-nav__link-text">CSV</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link">
												<i class="kt-nav__link-icon la la-file-pdf-o"></i>
												<span class="kt-nav__link-text">PDF</span>
											</a>
										</li>
									</ul>
								</div>
							</div>
							&nbsp;
							<a href="#" class="btn btn-brand btn-elevate btn-icon-sm">
								<i class="la la-plus"></i>
								New Record
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet__body">

				<!--begin: Datatable -->
				<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
					<div class="row">
						<div class="col-sm-12">
							<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 1542px;">
								<thead>
									<tr role="row">
										<th class="sorting_disabled" style="width: 16%;" aria-label="Actions">Sales:Open</th>
										<th class="sorting_disabled" style="width: 16%;" aria-label="Actions">Sales:Closed</th>
										<th class="sorting_disabled" style="width: 16%;" aria-label="Actions">Proforma:Open</th>
										<th class="sorting_disabled" style="width: 16%;" aria-label="Actions">Proforma:Closed</th>
										<th class="sorting_disabled" style="width: 16%;" aria-label="Actions">Production:Open</th>
										<th class="sorting_disabled" style="width: 16%;" aria-label="Actions">Production:Closed</th>
										<th class="sorting_disabled" style="width: 16%;" aria-label="Actions">RFQ:Open</th>
										<th class="sorting_disabled" style="width: 16%;" aria-label="Actions">RFQ:Closed</th>
									</tr>
								</thead>
							</table>
						</div>
						<div class="col-sm-12">
							<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 1542px;">
								<thead>
									<tr role="row">
										<th class="sorting_desc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 67.25px;" aria-sort="descending" aria-label="Order ID: activate to sort column ascending">ID</th>
										<th class="sorting_desc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 67.25px;" aria-sort="descending" aria-label="Order ID: activate to sort column ascending">Query NO</th>
										<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 113.25px;" aria-label="Country: activate to sort column ascending">Quote#</th>
										<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 138.25px;" aria-label="Ship City: activate to sort column ascending">Creator</th>
										<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 178.25px;" aria-label="Ship Address: activate to sort column ascending">Date</th>
										<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 136.25px;" aria-label="Company Agent: activate to sort column ascending">Query</th>
										<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 212.25px;" aria-label="Company Name: activate to sort column ascending">Assigned To</th>
										<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 55.25px;" aria-label="Status: activate to sort column ascending">Status</th>
										<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 81.5px;" aria-label="Actions">Actions</th>
									</tr>
								</thead>
								<tbody>
									<tr role="row" class="odd">
										<td class="sorting_1">1</td>
										<td class="sorting_1">SQ1</td>
										<td>ssdwsdw</td>
										<td>sds</td>
										<td>3 Goodland Terrace</td>
										<td>Pavel Kringe</td>
										<td>Goldner-Lehner</td>
										<td><span class="kt-badge kt-badge--danger kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-danger">Open</span></td>
										<td nowrap="">
											<span class="dropdown">
												<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
													<i class="la la-ellipsis-h"></i>
												</a>
												<div class="dropdown-menu dropdown-menu-right">
													<a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>
													<a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a>
													<a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>
												</div>
					                        </span>
	                                        <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
	                                        	<i class="la la-edit"></i>
	                                        </a>
	                                    </td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-5">
							<div class="dataTables_info" id="kt_table_1_info" role="status" aria-live="polite">Showing 1 to 10 of 40 entries</div>
						</div>
						<div class="col-sm-12 col-md-7 dataTables_pager">
							<div class="dataTables_length" id="kt_table_1_length">
								<label>Display 
									<select name="kt_table_1_length" aria-controls="kt_table_1" class="custom-select custom-select-sm form-control form-control-sm">
										<option value="5">5</option>
										<option value="10">10</option>
										<option value="25">25</option>
										<option value="50">50</option>
									</select>
								</label>
							</div>
							<div class="dataTables_paginate paging_simple_numbers" id="kt_table_1_paginate">
								<ul class="pagination">
									<li class="paginate_button page-item previous disabled" id="kt_table_1_previous">
										<a href="#" aria-controls="kt_table_1" data-dt-idx="0" tabindex="0" class="page-link">
											<i class="la la-angle-left"></i>
										</a>
									</li>
									<li class="paginate_button page-item active">
										<a href="#" aria-controls="kt_table_1" data-dt-idx="1" tabindex="0" class="page-link">1</a>
									</li>
									<li class="paginate_button page-item ">
										<a href="#" aria-controls="kt_table_1" data-dt-idx="2" tabindex="0" class="page-link">2</a>
									</li>
									<li class="paginate_button page-item ">
										<a href="#" aria-controls="kt_table_1" data-dt-idx="3" tabindex="0" class="page-link">3</a>
									</li>
									<li class="paginate_button page-item ">
										<a href="#" aria-controls="kt_table_1" data-dt-idx="4" tabindex="0" class="page-link">4</a>
									</li>
									<li class="paginate_button page-item next" id="kt_table_1_next">
										<a href="#" aria-controls="kt_table_1" data-dt-idx="5" tabindex="0" class="page-link">
											<i class="la la-angle-right"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<!--end: Datatable -->
			</div>
		</div>
	</div>

	<!-- end:: Content -->
</div>

<!-- end:: Content -->
