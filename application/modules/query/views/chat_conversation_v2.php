<?php if(!empty($conversation_details)) { ?>

	<?php foreach ($conversation_details as $conversation_key => $single_conversation) { ?>

		<?php if($single_conversation['query_assigned_id'] == $this->session->userdata('user_id')){ ?>

			<div class="kt-chat__message kt-chat__message--success">
				<div class="kt-chat__user">
					<span class="kt-media kt-media--circle kt-media--sm" style="max-width:50px !important">
						<?php if(empty($people_information_details[$single_conversation['query_creator_id']])){ ?>
							<img src="assets/media/users/default.jpg" alt="image" style="max-width:50px !important;height: 50px;">
						<?php } else { ?>
							<img src="assets/hr_document/profile_pic/<?php echo $people_information_details[$single_conversation['query_creator_id']];?>" alt="image" style="max-width:50px !important;height: 50px;">
						<?php } ?>
					</span>
					<a href="javascript:void(0);" class="kt-chat__username"><?php echo $user_details[$single_conversation['query_creator_id']]; ?></span></a>
					<span class="kt-chat__datetime"><?php echo $single_conversation['tat']; ?></span>
				</div>
				<div class="kt-chat__text">
					<?php echo $single_conversation['query_text']; ?>
				</div>
			</div>
		<?php }else if($single_conversation['query_creator_id'] == $this->session->userdata('user_id')){ ?>

			<div class="kt-chat__message kt-chat__message--right kt-chat__message--brand">
				<div class="kt-chat__user">
					<span class="kt-chat__datetime"><?php echo $single_conversation['tat']; ?></span>
					<a href="#" class="kt-chat__username"><?php echo $user_details[$single_conversation['query_creator_id']]; ?></span></a>
					<span class="kt-media kt-media--circle kt-media--sm" style="max-width:50px !important;">
						<?php if(empty($people_information_details[$single_conversation['query_creator_id']])){ ?>
							<img src="assets/media/users/default.jpg" alt="image" style="max-width:50px !important;height: 50px;">
						<?php } else { ?>
							<img src="assets/hr_document/profile_pic/<?php echo $people_information_details[$single_conversation['query_creator_id']];?>" alt="image" style="max-width:50px !important;height: 50px;">
						<?php } ?>
					</span>
				</div>
				<div class="kt-chat__text"><?php echo $single_conversation['query_text']; ?></div>
			</div>
		<?php }else{ ?>

			<?php if($conversation_key % 2 == 0){ ?>
				<div class="kt-chat__message kt-chat__message--success">
					<div class="kt-chat__user">
						<span class="kt-media kt-media--circle kt-media--sm" style="max-width:50px !important">
							<?php if(empty($people_information_details[$single_conversation['query_creator_id']])){ ?>
								<img src="assets/media/users/default.jpg" alt="image" style="max-width:50px !important;height: 50px;">
							<?php } else { ?>
								<img src="assets/hr_document/profile_pic/<?php echo $people_information_details[$single_conversation['query_creator_id']];?>" alt="image" style="max-width:50px !important;height: 50px;">
							<?php } ?>
						</span>
						<a href="javascript:void(0);" class="kt-chat__username"><?php echo $user_details[$single_conversation['query_creator_id']]; ?></span></a>
						<span class="kt-chat__datetime"><?php echo $single_conversation['tat']; ?></span>
					</div>
					<div class="kt-chat__text">
						<?php echo $single_conversation['query_text']; ?>
					</div>
				</div>
			<?php }else{ ?>
				<div class="kt-chat__message kt-chat__message--right kt-chat__message--brand">
					<div class="kt-chat__user">
						<span class="kt-chat__datetime"><?php echo $single_conversation['tat']; ?></span>
						<a href="#" class="kt-chat__username"><?php echo $user_details[$single_conversation['query_creator_id']]; ?></span></a>
						<span class="kt-media kt-media--circle kt-media--sm" style="max-width:50px !important;">
							<?php if(empty($people_information_details[$single_conversation['query_creator_id']])){ ?>
								<img src="assets/media/users/default.jpg" alt="image" style="max-width:50px !important;height: 50px;">
							<?php } else { ?>
								<img src="assets/hr_document/profile_pic/<?php echo $people_information_details[$single_conversation['query_creator_id']];?>" alt="image" style="max-width:50px !important;height: 50px;">
							<?php } ?>
						</span>
					</div>
					<div class="kt-chat__text"><?php echo $single_conversation['query_text']; ?></div>
				</div>
			<?php } ?>
		<?php } ?>
	<?php } ?>
<?php } ?>
</div>