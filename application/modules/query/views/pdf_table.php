<table class="table table-striped" style="margin: 0% 0% 0% 0%;">
	<thead>
		<tr>
			<th scope="col" style="width:10%;">#</th>
			<th scope="col" style="width:30%;">Date</th>
			<th scope="col" style="width:50%;">Pdf Name</th>
			<th scope="col" style="width:10%;">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php if(!empty($pdf_details)) {?>
			<?php foreach($pdf_details as $single_pdf_details) {?>
                <!-- <tr>
					<th scope="row" colspan= "4" style="text-align: center;">
                        <?php echo $single_pdf_details['pdf_name']; ?>
                    </th>
				</tr> -->
                <?php foreach(explode(',', $single_pdf_details['value']) as $pdf_key => $pdf_name) {?>
				<tr>
					<th scope="row"><?php echo $pdf_key+1;?></th>
                    <td class="kt-font-bold">
                        <?php echo date('F d, h:i A', strtotime($single_pdf_details['add_time']));?>
                    </td>
					<td class="kt-font-bold"><?php echo $pdf_name; ?></td>
					<td class="kt-font-bold">
						<div class="row" style="padding: 0px 20px 0px 0px;">
							<div class="col-md-6">
		        				<a class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View <?php echo $single_pdf_details['pdf_name']; ?>" href="<?php echo base_url('assets/query_pdf/'.$pdf_name);?>" style="color: #212529;" target="_blank">
						    		<i class="la la-eye kt-font-bolder" style="color: #212529;"></i>
							    </a>
							</div>
						</div>
					</td>
				</tr>
                <?php } ?>
			<?php } ?>
		<?php } else {?>
		<tr>
			<th scope="row"></th>
			<td>NO DATA FOUND</td>
			<td></td>
		</tr>
		<?php } ?>
	</tbody>
</table>