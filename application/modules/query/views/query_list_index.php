<style type="text/css">
	.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper{
		padding-top: 4% !important;
	}
	.layer-white{
	    display: none;
	    position: fixed;
	    top: 0em !important;
	    left: 0em !important;
	    width: 100%;
	    height: 100%;
	    text-align: center;
	    vertical-align: middle;
	    background-color: rgba(255, 255, 255, 0.55);
	    opacity: 1;
	    line-height: 1;
	    -webkit-animation-fill-mode: both;
	    animation-fill-mode: both;
	    -webkit-animation-duration: 0.5s;
	    animation-duration: 0.5s;
	    -webkit-transition: background-color 0.5s linear;
	    transition: background-color 0.5s linear;
	    -webkit-user-select: none;
	    -moz-user-select: none;
	    -ms-user-select: none;
	    user-select: none;
	    will-change: opacity;
	    z-index: 9;
	}
	.div-loader{
	    position: absolute;
		top: 50%;
		left: 50%;
		margin: 0px;
		text-align: center;
		z-index: 1000;
		-webkit-transform: translateX(-50%) translateY(-50%);
		transform: translateX(-50%) translateY(-50%);
	}
	.kt-spinner:before {
	    width: 50px;
	    height: 50px;
	    margin-top: -10px;
	}
	tbody#query_listing tr:hover {
		background-color: gainsboro;
	}
	tbody#query_listing tr.lightgreen {
		background-color: #0abb871a;
	}
	tbody#query_listing tr:hover .first_div {
	    border-left: 0.25rem solid #5578eb !important;
	}
	tbody#query_listing td {
		font-style: normal;
		font-size: 15px;
	    border: 0.05rem solid gainsboro;
	}
	tbody#query_listing td.lightgreen {
        font-style: normal;
        font-size: 15px;
        border: 0.05rem solid lightgreen;
    }
	tbody#query_listing td span{
		/*width:200px !important;*/
		display: inline-flex;
	    width: 100%;
	}
	tbody#query_listing td span i{
		cursor: pointer;
	}
	tbody#query_listing td span abbr{
	    width: 100%;
	}    
	tbody#query_listing td span abbr em{
		display: block;
		color: #898989;
		margin: 3px 0 3px 0;
		font-style: normal;
		font-family: Helvetica Neue,Helvetica,Arial,sans-serif
	}
	tbody#query_listing td span abbr em i{
		font-weight: 500;
	    color: #898989;
	    margin: 3px 0 3px 0;
	    font-style: normal;
	    font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
	}
	thead#query_header tr th{
	    background: gainsboro;
	    color: #767676;
	    font-size: 14px;
	    font-family: 'latomedium';
	    padding: 10px 20px;
	    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
	}
	ul.menu-tab {
		margin: 0px;
	    padding: 0px;
	    border: 1px solid #E7E7E7;
	    /*border-radius: 50px;*/
	    overflow: hidden;
	    font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
	    font-size: 14px;
	    line-height: 20px;
	    color: #333;
	}
	ul.menu-tab li.main_tab_name{
		width: calc(100%/7) !important;
	    display: inline;
	    text-align: center;
	    float: left;
	}
	ul.menu-tab li.sub_tab_name{
		width: calc(100%/2) !important;
	    display: inline;
	    text-align: center;
	    float: left;
	}
	ul.menu-tab li.active_list{
	    border-bottom: 0.25rem solid #767676 !important;
	}
	ul.menu-tab li a{
		cursor: pointer;
	    display: inline-block;
	    outline: none;
	    text-align: center;
	    width: 100%;
	    background: #F5F5F5;
	    /*border-right: 2px solid #fff;*/
	    color: #767676;
	    font-size: 15px;
	    font-family: 'latomedium';
	    background-color: gainsboro;
	    padding: 3px 3px 12px 3px;
	}
	ul.menu-tab li a i{
		display: inline-block;
	    width: 35px;
	    height: 28px;
	    font-style: normal;
	    background-size: 100%;
	    position: relative;
	    top: 6px;
	}
	.form_name{
	    color: #767676 !important;
	    font-size: 14px !important;
	    font-family: 'latomedium' !important;
	    padding: 10px 10px !important; 
	    font-weight: bold !important;
	}
	.kt-badge--gainsboro, .kt-font-gainsboro{
		background-color: gainsboro;
	}
	.production_title_name{
		font-weight: 700 !important;
		font-size: 1.5rem;
		display: block;
		color: #898989;
		margin: 3px 0 3px 0;
		font-style: normal;
		font-family: Helvetica Neue,Helvetica,Arial,sans-serif
	}
	.production_title_value{
		font-weight: 500;
		font-size: 1.75rem;
	    color: #898989;
	    margin: 3px 0 3px 0;
	    font-style: normal;
	    font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
	}
</style>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col-xl-12">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-toolbar">
				        <div class="kt-portlet__head-wrapper">
				            <div class="kt-portlet__head-actions">
				                <a href="javascript:void(0);" class="btn btn-brand btn-elevate btn-icon-sm add_query_search_filter_form" action_value="show" style="background-color: #c6cced; border-color: #c6cced; color: #767676; font-weight: 800;">
				                    Add Search Filter
				                </a>
				            </div>
				        </div>
			    	</div>
				</div>
			</div>	
		</div>
	</div>
	<div class="row">
		<div class="col-xl-12">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__body query_listing_search_filter" style="display:none; padding: 1% 1% 0% 1%;">
		        	<?php $this->load->view('query/query_search_filter');?>
				</div>
			</div>
		</div>
	</div>
	<?php if(in_array($this->session->userdata('role'),array(1, 6, 7, 10, 11, 16, 17, 21))){?>
		<div class="row">
			<div class="col-xl-7">
				<!--Begin::Portlet-->
				<div class="kt-portlet kt-portlet--height-fluid">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">
								Query Graph
							</h3>
						</div>
					</div>
					<div class="kt-portlet__body">
						<!--Begin::Timeline 3 -->	
						<div class="kt-scroll" data-scroll="true" style="height: 400px">
							<div id="query_assigned_highchart"></div>
						</div>

						<!--End::Timeline 3 -->
					</div>
				</div>

				<!--End::Portlet-->
			</div>
			<div class="col-xl-5">
				<!--Begin::Portlet-->
				<div class="kt-portlet kt-portlet--height-fluid">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">
								Sub Query Graph
							</h3>
						</div>
					</div>
					<div class="kt-portlet__body">
						<!--Begin::Timeline 3 -->
						<div class="kt-scroll" data-scroll="true" style="height: 400px">
							<div id="sub_query_highchart"></div>
						</div>

						<!--End::Timeline 3 -->
					</div>
				</div>

				<!--End::Portlet-->
			</div>
		</div>
	<?php }?>
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__body" style="padding: 25px 25px 0px 25px;">
			<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
				<div class="row">
					<!-- <div class="col-sm-1"></div> -->
					<div class="col-sm-12">
						<ul class="menu-tab kt-font-bolder"
							style="">
							<li class="main_tab_name sales_query active_list " tab-name="sales_query">
								<a>
									<i class="custom_heading_icon active_issues_tabs_icon">
										<img src="assets/media/icons/svg/Tools/Angle Grinder.svg"/>
									</i>
									<span style="top: 6px;position: relative;">
										Quotation
									</span>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<span style="top: 6px;position: relative;">
										(<?php echo ( $count['sales_count']['open'] );?>)
									</span>
								</a>
							</li>
							<li class="main_tab_name sample_query"  tab-name="sample_query" style="padding: 0px 0px 0px 10px;">
								<a>
									<i class="custom_heading_icon active_issues_tabs_icon">
										<img src="assets/media/icons/svg/Tools/Tools.svg"/>
									</i>
									<span style="top: 6px;position: relative;">
										Sample
									</span>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<span style="top: 6px;position: relative;">
										(<?php echo ( $count['sample_count']['open']);?>)
									</span>
								</a>
							</li>
							<li class="main_tab_name proforma_query kt-hidden" tab-name="proforma_query" style="padding: 0px 0px 0px 10px;">
								<a>
									<i class="custom_heading_icon active_issues_tabs_icon">
									    <img src="assets/media/icons/svg/Tools/Tools.svg"/>
									</i>
									<span style="top: 6px;position: relative;">
										Profoma
									</span>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<span style="top: 6px;position: relative;">
										 (<?php echo $proforma_open_count;?>)
									</span>
								</a>
							</li>
							<li class="main_tab_name production_query"  tab-name="production_query" style="padding: 0px 0px 0px 10px;">
								<a>
									<i class="custom_heading_icon active_issues_tabs_icon">
									    <img src="assets/media/icons/svg/Tools/Tools.svg"/>
									</i>
									<span style="top: 6px;position: relative;">
									  Production
									</span>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<span style="top: 6px;position: relative;">
										(<?php echo ( $count['production_count']['open'] );?>)
									</span>
								</a>
							</li>
							<?php if(!in_array($this->session->userdata('user_id'), array(343))){?>
							<li class="main_tab_name osdr_query"  tab-name="osdr_query" style="padding: 0px 0px 0px 10px;">
								<a>
									<i class="custom_heading_icon active_issues_tabs_icon">
									    <img src="assets/media/icons/svg/Tools/Tools.svg"/>
									</i>
									<span style="top: 6px;position: relative;">
									  OSDR
									</span>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<span style="top: 6px;position: relative;">
										(<?php echo ( $count['osdr_count']['open'] );?>)
									</span>
								</a>
							</li>
							<?php }?>
							<li class="main_tab_name rfq_query"  tab-name="rfq_query" style="padding: 0px 0px 0px 10px;">
								<a>
									<i class="custom_heading_icon active_issues_tabs_icon">
									    <img src="assets/media/icons/svg/Tools/Tools.svg"/>
									</i>
									<span style="top: 6px;position: relative;">
									  RFQ
									</span>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<span style="top: 6px;position: relative;">
										(<?php echo ( $count['rfq_count']['open']);?>)
									</span>
								</a>
							</li>	
							<?php if(!in_array($this->session->userdata('role'), array(10, 11, 24))){?>
							<li class="main_tab_name pq_query"  tab-name="pq_query" style="padding: 0px 0px 0px 10px;">
								<a>
									<i class="custom_heading_icon active_issues_tabs_icon">
									    <img src="assets/media/icons/svg/Tools/Tools.svg"/>
									</i>
									<span style="top: 6px;position: relative;">
									  PQQ
									</span>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<span style="top: 6px;position: relative;">
										(<?php echo ( $count['pq_count']['open']);?>)
									</span>
								</a>
							</li>
							<?php }?>
							<li class="main_tab_name lead_query"  tab-name="lead_query" style="padding: 0px 0px 0px 10px;">
								<a>
									<i class="custom_heading_icon active_issues_tabs_icon">
									    <img src="assets/media/icons/svg/Tools/Tools.svg"/>
									</i>
									<span style="top: 6px;position: relative;">
									  Lead
									</span>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<span style="top: 6px;position: relative;">
										(<?php echo ( $count['lead_count']['open']);?>)
									</span>
								</a>
							</li>
						</ul>
					</div>
				   	<div class="col-sm-12">
						<ul class="menu-tab  kt-font-bolder tab_2"
							style="">
							<li class="sub_tab_name open active_list" tab-name="open">
								<a>
									<i class="custom_heading_icon active_issues_tabs_icon">
										<img src="assets/media/icons/svg/Tools/Angle Grinder.svg"/>
									</i>
									<span style="top: 6px;position: relative;">
										Open
									</span>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<span class="sub_tab_count sub_tab_sales_count kt-hidden" style="top: 6px;position: relative;">
										(<?php echo ( $count['sales_count']['open'] );?>)
									</span>
									<span class="sub_tab_count sub_tab_sample_count kt-hidden" style="top: 6px;position: relative;display:none;">
										(<?php echo ( $count['sample_count']['open'] );?>)
									</span>
									<span class="sub_tab_count sub_tab_production_count kt-hidden" style="top: 6px;position: relative; display:none;">
										(<?php echo ( $count['production_count']['open'] );?>)
									</span>
									<span class="sub_tab_count sub_tab_osdr_count kt-hidden" style="top: 6px;position: relative;display:none;">
										(<?php echo ( $count['osdr_count']['open'] );?>)
									</span>
									<span class="sub_tab_count sub_tab_rfq_count kt-hidden" style="top: 6px;position: relative;display:none;">
										(<?php echo ( $count['rfq_count']['open'] );?>)
									</span>
									<span class="sub_tab_count sub_tab_pq_count kt-hidden" style="top: 6px;position: relative;display:none;">
										(<?php echo ( $count['pq_count']['open'] );?>)
									</span>
								</a>
							</li>
							<li class="sub_tab_name closed" tab-name="closed" style="padding: 0px 0px 0px 10px;">
								<a>
									<i class="custom_heading_icon active_issues_tabs_icon">
									    <img src="assets/media/icons/svg/Tools/Tools.svg"/>
									</i>
									<span style="top: 6px;position: relative;">
										Close
									</span>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<span class="sub_tab_count sub_tab_sales_count kt-hidden" style="top: 6px;position: relative;">
										(<?php echo ( $count['sales_count']['closed'] );?>)
									</span>
									<span class="sub_tab_count sub_tab_sample_count kt-hidden" style="top: 6px;position: relative;display:none;">
										(<?php echo ( $count['sample_count']['closed'] );?>)
									</span>
									<span class="sub_tab_count sub_tab_production_count kt-hidden" style="top: 6px;position: relative;display:none;">
										(<?php echo ( $count['production_count']['closed'] );?>)
									</span>
									<span class="sub_tab_count sub_tab_osdr_count kt-hidden" style="top: 6px;position: relative;display:none;">
										(<?php echo ( $count['osdr_count']['closed'] );?>)
									</span>
									<span class="sub_tab_count sub_tab_rfq_count kt-hidden" style="top: 6px;position: relative;display:none;">
										(<?php echo ( $count['rfq_count']['closed'] );?>)
									</span>
									<span class="sub_tab_count sub_tab_pq_count kt-hidden" style="top: 6px;position: relative;display:none;">
										(<?php echo ( $count['pq_count']['closed'] );?>)
									</span>
								</a>
							</li>
						</ul>
					</div>
					<!-- <div class="col-sm-1"></div> -->
					<div class="col-sm-12">
						<div class="kt-scroll" data-scroll="true" data-scroll-x="true" style="height: auto;">
							<div style="width: 1
							500px;">
								<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 100%; border: 0.25rem solid gainsboro;">
									<thead id="query_header">
		          						<tr role="row">
		          							<th class="kt-align-center kt-font-bolder" style="width:5%">ID</th>
		          							<th class="kt-align-center kt-font-bolder" style="width:30%" id="quote_type">Quotation No Details</th>
											<th class="kt-align-left kt-font-bolder" style="width:20%">Query Creator Details</th>
											<th class="kt-align-left kt-font-bolder" style="width:20%">Query Assigned Details</th>
											<th class="kt-align-center kt-font-bolder" style="width:20%">Other Details</th>
											<th class="sorting_disabled kt-font-bolder kt-align-center" style="width:5%">Actions</th>
										</tr>
									</thead>
									<tbody id="query_listing">
										<?php $this->load->view('query/query_list_table_body'); ?>
									</tbody>
								</table>
							</div>
						</div>
						<div id="invoice_table_loader" class="layer-white">
							<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
						</div>
					</div>
				</div>
				<div id="query_paggination" style="padding: 25px 1px 1px 1px;">
          			<?php $this->load->view('query/query_paggination')?>
				</div>
			</div>
		</div>
	</div>
</div>

<!--begin::Modal-->
<div class="modal modal-stick-to-bottom fade" id="mtc_upload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Upload Files:</h5>
			</div>
			<div class="modal-body row">
				<?php if($this->session->userdata('query_access')['query_list_mtc_drawing_tab_access']){ ?>
				<div class="form-group form-group-last col-lg-6 row mtc_upload_div" style="padding: 10px 25px;">
					<label class="col-lg-3 col-form-label kt-font-bolder kt-font-lg">MTC PDF</label>
					<div class="col-lg-9">
						<div class="dropzone dropzone-multi" id="kt_dropzone_mtc_pdf">
							<div class="dropzone-panel">
								<a class="dropzone-select btn btn-label-brand btn-bold btn-sm">Attach files</a>
								<a class="dropzone-upload btn btn-label-brand btn-bold btn-sm">Upload All</a>
								<a class="dropzone-remove-all btn btn-label-brand btn-bold btn-sm">Remove All</a>
							</div>
							<div class="dropzone-items">
								<div class="dropzone-item" style="display:none">
									<div class="dropzone-file">
										<div class="dropzone-filename" title="some_image_file_name.jpg"><span data-dz-name>some_image_file_name.jpg</span> <strong>(<span  data-dz-size>340kb</span>)</strong></div>
										<div class="dropzone-error" data-dz-errormessage></div>
									</div>
									<div class="dropzone-progress">
										<div class="progress">
											<div class="progress-bar kt-bg-brand" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress></div>
										</div>
									</div>
									<div class="dropzone-toolbar">
										<span class="dropzone-start"><i class="flaticon2-arrow"></i></span>
										<span class="dropzone-cancel" data-dz-remove style="display: none;"><i class="flaticon2-cross"></i></span>
										<span class="dropzone-delete" data-dz-remove><i class="flaticon2-cross"></i></span>
									</div>
								</div>
							</div>
						</div>
						<span class="form-text text-muted kt-font-bold">Max file size is 1MB and max number of files is 5.</span>
					</div>
				</div>
				<div class="form-group form-group-last col-lg-6 row drawing_upload_div" style="padding: 10px 25px;">
					<label class="col-lg-3 col-form-label kt-font-bolder kt-font-lg">Drawing</label>
					<div class="col-lg-9">
						<div class="dropzone dropzone-multi" id="kt_dropzone_drawing_pdf">
							<div class="dropzone-panel">
								<a class="dropzone-select btn btn-label-brand btn-bold btn-sm">Attach files</a>
								<a class="dropzone-upload btn btn-label-brand btn-bold btn-sm">Upload All</a>
								<a class="dropzone-remove-all btn btn-label-brand btn-bold btn-sm">Remove All</a>
							</div>
							<div class="dropzone-items">
								<div class="dropzone-item" style="display:none">
									<div class="dropzone-file">
										<div class="dropzone-filename" title="some_image_file_name.jpg"><span data-dz-name>some_image_file_name.jpg</span> <strong>(<span  data-dz-size>340kb</span>)</strong></div>
										<div class="dropzone-error" data-dz-errormessage></div>
									</div>
									<div class="dropzone-progress">
										<div class="progress">
											<div class="progress-bar kt-bg-brand" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress></div>
										</div>
									</div>
									<div class="dropzone-toolbar">
										<span class="dropzone-start"><i class="flaticon2-arrow"></i></span>
										<span class="dropzone-cancel" data-dz-remove style="display: none;"><i class="flaticon2-cross"></i></span>
										<span class="dropzone-delete" data-dz-remove><i class="flaticon2-cross"></i></span>
									</div>
								</div>
							</div>
						</div>
						<span class="form-text text-muted kt-font-bold">Max file size is 1MB and max number of files is 5.</span>
					</div>
				</div>
				<?php } ?>
				
				<div class="col-lg-6" id="pdf_upload_history_mtc" style="padding: 10px 20px 10px 20px;"></div>
				<div class="col-lg-6" id="pdf_upload_history_drawing" style="padding: 10px 20px 10px 0px;"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger mtc_upload_close" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->

<!--Begin:: Chat-->
<div class="modal fade- modal-sticky-bottom-right" id="kt_chat_modal" role="dialog" data-backdrop="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="kt-chat">
				<div class="kt-portlet kt-portlet--last">
					<div class="kt-portlet__head">
						<div class="kt-chat__head ">
							<div class="kt-chat__left">
								<div class="kt-chat__label">
									<a href="javascript:void(0);" class="kt-chat__title">
										<em class= "header_1"></em>
									</a>
									<span class="kt-chat__status header_2"></span>
								</div>
							</div>
							<div class="kt-chat__right"></div>
						</div>
					</div>
					<div class="kt-portlet__body">
						<div class="kt-scroll kt-scroll--pull chat_scroll" data-height="410" data-mobile-height="225">
							<div class="layer-white chat_loader">
								<div class="kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info div-loader"></div>
							</div>
							<div class="kt-chat__messages kt-chat__messages--solid div_chat_history"></div>
						</div>
					</div>
					<div class="kt-portlet__foot">
						<form id="sales_query_add_form"  style="display:none;">
							<div class="kt-chat__input">
								<div class="kt-chat__editor">
									<input type="text" class="form-control" name="query_type" value="sales_query" hidden />
									<input type="text" class="form-control" name="query_reference" value="" hidden />
									<input type="text" class="form-control" name="query_reference_id" value="" hidden />
									<textarea class="kt-font-dark" style="height: 50px" placeholder="Type here..." name="query_text"></textarea>
									<input type="text" class="form-control" name="query_creator_id" value="" hidden />
									<input type="text" class="form-control" name="query_assigned_id" value="" hidden />
									<input type="text" class="form-control" name="query_status" value="open" hidden />
								</div>
								<div class="kt-chat__toolbar">
									<div class="kt_chat__tools">
										<div class="query_type"></div>
									</div>
									<div class="kt_chat__actions">
										<button type="button" class="btn btn-brand btn-md btn-upper btn-bold add_sales_query">reply</button>
										<button type="button" class="btn btn-brand btn-md btn-font-sm btn-upper btn-bold" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</form>
						<form id="production_query_add_form" style="display:none;">
							<div class="kt-chat__input">
								<div class="kt-chat__editor">
									<input type="text" class="form-control" name="query_type" value="production_query" hidden />
									<input type="text" class="form-control" name="query_reference" value="" hidden />
									<input type="text" class="form-control" name="query_reference_id" value="" hidden />
									<textarea class="kt-font-dark" style="height: 50px" placeholder="Type here..." name="query_text"></textarea>
									<input type="text" class="form-control" name="query_creator_id" value="" hidden />
									<input type="text" class="form-control" name="query_assigned_id" value="" hidden />
									<input type="text" class="form-control" name="query_status" value="open" hidden />
								</div>
								<div class="kt-chat__toolbar">
									<div class="kt_chat__tools">
										<div class="query_type"></div>
									</div>
									<div class="kt_chat__actions">
										<button type="button" class="btn btn-brand btn-md btn-upper btn-bold add_production_query">reply</button>
										<button type="button" class="btn btn-brand btn-md btn-font-sm btn-upper btn-bold" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</form>
						<form id="rfq_query_add_form" style="display:none;">
							<div class="kt-chat__input">
								<div class="kt-chat__editor">
									<input type="text" class="form-control" name="query_type" value="rfq_query" hidden />
									<input type="text" class="form-control" name="query_reference" value="" hidden />
									<input type="text" class="form-control" name="query_reference_id" value="" hidden />
									<textarea class="kt-font-dark" style="height: 50px" placeholder="Type here..." name="query_text"></textarea>
									<input type="text" class="form-control" name="query_creator_id" value="" hidden />
									<input type="text" class="form-control" name="query_assigned_id" value="" hidden />
									<input type="text" class="form-control" name="query_status" value="open" hidden />
								</div>
								<div class="kt-chat__toolbar">
									<div class="kt_chat__tools">
										<div class="query_type"></div>
									</div>
									<div class="kt_chat__actions">
										<button type="button" class="btn btn-brand btn-md btn-upper btn-bold add_rfq_query">reply</button>
										<button type="button" class="btn btn-brand btn-md btn-font-sm btn-upper btn-bold" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</form>
						<form id="sample_query_add_form" style="display:none;">
							<div class="kt-chat__input">
								<div class="kt-chat__editor">
									<input type="text" class="form-control" name="query_type" value="" hidden />
									<input type="text" class="form-control" name="query_reference" value="" hidden />
									<input type="text" class="form-control" name="query_reference_id" value="" hidden />
									<textarea class="kt-font-dark" style="height: 50px" placeholder="Type here..." name="query_text"></textarea>
									<input type="text" class="form-control" name="query_creator_id" value="" hidden />
									<input type="text" class="form-control" name="query_status" value="open" hidden />
								</div>
								<div class="kt-chat__toolbar">
									<div class="kt_chat__tools">
										<div class="query_type"></div>
									</div>
									<div class="kt_chat__actions">
										<button type="button" class="btn btn-brand btn-md btn-upper btn-bold add_sample_query">reply</button>
										<button type="button" class="btn btn-brand btn-md btn-font-sm btn-upper btn-bold" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</form>
						<form id="pq_and_lead_query_add_form" style="display:none;">
							<div class="kt-chat__input">
								<div class="kt-chat__editor">
									<input type="text" class="form-control" name="query_type" value="pq_query" hidden />
									<input type="text" class="form-control" name="query_reference_id" value="" hidden />
									<input type="text" class="form-control" name="query_reference" value="" hidden />
									<textarea class="kt-font-dark" style="height: 50px" placeholder="Type here..." name="query_text"></textarea>
									<input type="text" class="form-control" name="query_creator_id" value="" hidden />
									<input type="text" class="form-control" name="query_assigned_id" value="" hidden />
									<input type="text" class="form-control" name="query_status" value="open" hidden />
								</div>
								<div class="kt-chat__toolbar">
									<div class="kt_chat__tools">
										<div class="query_type"></div>
									</div>
									<div class="kt_chat__actions">
										<button type="button" class="btn btn-brand btn-md btn-upper btn-bold add_pq_and_lead_query">reply</button>
										<button type="button" class="btn btn-brand btn-md btn-font-sm btn-upper btn-bold" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</form>
						<form id="osdr_query_add_form" style="display:none;">
							<div class="kt-chat__input">
								<div class="kt-chat__editor">
									<input type="text" class="form-control" name="query_type" value="osdr_query" hidden />
									<input type="text" class="form-control" name="query_reference" value="" hidden />
									<input type="text" class="form-control" name="query_reference_id" value="" hidden />
									<textarea class="kt-font-dark" style="height: 50px" placeholder="Type here..." name="query_text"></textarea>
									<input type="text" class="form-control" name="query_creator_id" value="" hidden />
									<input type="text" class="form-control" name="query_assigned_id" value="" hidden />
									<input type="text" class="form-control" name="query_status" value="open" hidden />
								</div>
								<div class="kt-chat__toolbar">
									<div class="kt_chat__tools">
										<div class="query_type"></div>
									</div>
									<div class="kt_chat__actions">
										<button type="button" class="btn btn-brand btn-md btn-upper btn-bold add_osdr_query">reply</button>
										<button type="button" class="btn btn-brand btn-md btn-font-sm btn-upper btn-bold" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--ENd:: Chat-->


<!--begin::Modal-->
<div class="modal fade" id="query_comment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<div class="kt-chat">
				<div class="kt-portlet kt-portlet--last">
					<div class="kt-portlet__head">
						<div class="kt-chat__head ">
							<div class="kt-chat__left">
								<div class="kt-chat__label">
									<a href="javascript:void(0);" class="kt-chat__title">
										<em>Query Comment</em>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="kt-portlet__foot">
						<div class="kt-chat__input">
							<div class="kt-chat__editor">
								<textarea class="kt-font-dark" style="height: 50px" placeholder="Type here..." name="query_comment"></textarea>
							</div>
							<div class="kt-chat__toolbar">
								<div class="kt_chat__tools">
									<div class=""></div>
								</div>
								<div class="kt_chat__actions">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-primary save_comment" query_master_id = 0>Save changes</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->


<!--begin::Modal-->
<div class="modal fade" id="osdr_document" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">OSDR Document Files:</h5>
			</div>
			<div class="modal-body">
				<div id="osdr_document_history"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!--end::Modal-->