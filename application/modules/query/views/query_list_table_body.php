<?php if (!empty($query_list)){ ?>
	<?php foreach ($query_list as $key =>  $query_data){ ?>
		<?php if(in_array($query_data['modal_class_name'], array('sales_query', 'sample_query', 'production_query', 'rfq_query', 'pq_query', 'osdr_query', 'lead_query'))){?>

		<tr class="<?php echo $query_data['background_class']; ?>">
			<td class="kt-align-center <?php echo $query_data['background_class']; ?>">
				<span>
					<abbr>
						<em class="kt-font-bolder"><?php echo $key+1;?></em>
						<?php if($query_data['query_type'] == 'lead_sample_query'){?>
								<hr style="margin: 2px;">
								<em class="kt-font-bolder">L</em>
						<?php } else if($query_data['query_type'] == 'quotation_sample_query'){?>
								<hr style="margin: 2px;">
								<em class="kt-font-bolder">Q</em>
						<?php } ?>
					</abbr>
				</span>
			</td>
			<td class="kt-align-left <?php echo $query_data['background_class']; ?>">
				<span>
					<abbr>
						<em class="kt-font-bolder">Query No:
							<i><?php echo $query_data['query_number'];?></i>
						</em>
						<?php if(!in_array($query_data['query_type'], array('lead_sample_query','pq_query', 'lead_query'))){?>
						<em class="kt-font-bolder">
							<?php if($query_data['query_type'] == 'sales_query' || $query_data['query_type'] == 'quotation_sample_query'){

								echo "Quotation No #:";
							}else if($query_data['query_type'] == 'production_query' || $query_data['query_type'] == 'osdr_query'){

								echo "Work Order No #:";
							}else if($query_data['query_type'] == 'rfq_query'){

								echo "Rfq No #:";
							} ?>

							<i><?php echo $query_data['query_reference'];?></i>
						</em>
						<?php }?>
						<?php if($this->session->userdata('query_access')['query_list_client_name_access']){?>
							<em class="kt-font-bolder">Client Name :
								<i><?php echo $query_data['client_name'];?></i>
							</em>
							<?php if($query_data['query_type'] == 'lead_sample_query' || $query_data['query_type'] == 'pq_query' || $query_data['query_type'] == 'lead_query'){ ?>
								<em class="kt-font-bolder">Source :
								<i><?php echo $query_data['query_reference'];?></i>
							<?php }?>
						<?php } ?>
						<em class="kt-font-bolder"> Query Date:
							<i>
								<?php
									if(!empty($query_data['query_add_time'])) {
										echo $query_data['query_add_time'];
									} else {
										echo "Create Date Not Found";
									}
								?>
							</i>
						</em>
						<em class="kt-font-bolder"> Reply Date:
							<i>
								<?php echo $query_data['reply_add_time']; ?>
							</i>
						</em>
					</abbr>
				</span>
			</td>
			<td class="kt-align-left <?php echo $query_data['background_class']; ?>">
				<span>    
					<abbr>
						<em class="kt-font-bolder"> Creator Name : 
							<i class=""><?php echo $user_details[$query_data['query_creator_id']];?></i>
						</em>
						<?php if($query_data['modal_class_name'] == 'sample_query' ){ ?>
							<em class="kt-font-bolder"> Send to Sample :
								<i class=""><?php echo $query_data['query_text'];?></i>
							</em>
						<?php }else {?>
							<em class="kt-font-bolder"> Query Text :
								<i class=""><?php echo $query_data['query_text'];?></i>
							</em>
						<?php }?>
					</abbr>
				</span>
			</td>
			<td class="kt-align-left <?php echo $query_data['background_class']; ?>">
				<span>    
					<abbr>
						<em class="kt-font-bolder"> Assigned Name : 
							<i><?php echo $user_details[$query_data['query_assigned_id']];?></i>
						</em>
						<?php if($query_data['modal_class_name'] == 'sample_query' ){ ?>
							<em class="kt-font-bolder">Feedback for Sample:
								<i><?php echo $query_data['reply_text'];?></i>
							</em>
						<?php }else {?>
							<em class="kt-font-bolder"> Reply Text :
								<i><?php echo $query_data['reply_text'];?></i>
							</em>
						<?php }?>
						<?php if(!empty($query_data['grand_total'] && $query_data['modal_class_name'] != 'sample_query')){ ?>
							<em class="kt-font-bolder"> Value:
								<i><?php echo $query_data['grand_total'];?></i>
							</em>
						<?php } ?>
					</abbr>
				</span>
			</td>
			<!-- <td class="kt-align-center">
				<span>
					<abbr>
						<em class="kt-font-bolder"><?php echo $query_data['ageing'];?></em>
					</abbr>
				</span>
			</td> -->
			<td class="kt-align-left <?php echo $query_data['background_class']; ?>">
				<span>    
					<abbr>

							<?php if(!empty($query_data['rfq_subject'])){ ?>
								<em class="kt-font-bolder"> Subject : <i><?php echo $query_data['rfq_subject'];?></i></em>
							<?php } ?>
							<em class="kt-font-bolder"> TAT : <i class="kt-font-success kt-font-bolder"><?php echo $query_data['TAT'];?></i></em>
							<em class="kt-font-bolder"> Comment : <i><?php echo $query_data['comments'];?></i></em>
							<em class="kt-font-bolder"> Comment Creator : <i><?php echo $query_data['comment_creator'];?></i></em>
							<?php if(!empty($query_data['comment_add_time'])){ ?>
							<em class="kt-font-bolder"> Comment Date:<i><?php echo $query_data['comment_add_time'];?></i></em>
							<?php }?>
					</abbr>
				</span>
			</td>
			<td class="kt-align-center <?php echo $query_data['background_class']; ?>">
				<?php if(!empty($query_data["url"])){?>
				<a href="<?php echo $query_data["url"]; ?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View Quotation" target="_blank" style="color: #ACACAC;">
					<i class="la la-eye kt-font-bolder" style="color: #ACACAC;"></i>
				</a>
				<?php } ?>
				<?php if(in_array($query_data['modal_class_name'], array('sample_query', 'pq_query', 'lead_query'))){ ?>
					<button class="btn btn-sm btn-clean btn-icon btn-icon-md <?php echo $query_data['modal_class_name'];?>" title="Query_history" lead_id="<?php echo $query_data['query_reference_id'];?>" query_type="<?php echo $query_data['query_type'];?>" style="color: #ACACAC;">
						<i class="fa fa-question kt-font-bolder" style="color: #ACACAC;"></i>
					</button>
				<?php }else { ?>

					<button class="btn btn-sm btn-clean btn-icon btn-icon-md <?php echo $query_data['modal_class_name'];?>" title="Query_history" quote_id="<?php echo $query_data['query_reference_id'];?>" style="color: #ACACAC;">
						<i class="fa fa-question kt-font-bolder" style="color: #ACACAC;"></i>
					</button>
				<?php  }?>
				<?php if($query_data['show_delete']){?>
				<button class="btn btn-sm btn-clean btn-icon btn-icon-md deletequery" title="Delete" query_id="<?php echo $query_data['id'];?>" style="color: #ACACAC;">
					<i class="la la-trash kt-font-bolder" style="color: #ACACAC;"></i>
				</button>
				<?php } ?>
				<?php if($query_data['show_close']){?>
				<button class="btn btn-sm btn-clean btn-icon btn-icon-md close_query" title="close query" query_id="<?php echo $query_data['id'];?>" style="color: #ACACAC;">
					<i class="la la-close kt-font-bolder" style="color: #ACACAC;"></i>
				</button>
				<?php } ?>
				<?php if($query_data['show_reopen']){?>
					<button class="btn btn-sm btn-clean btn-icon btn-icon-md open_query" title="Reopen query" query_id="<?php echo $query_data['id'];?>" style="color: #ACACAC;">
						<i class="la la-rotate-left kt-font-bolder" style="color: #ACACAC;"></i>
					</button>
				<?php } ?>
				<?php if($query_data['TAT'] >=2){ ?><button class="btn btn-sm btn-clean btn-icon btn-icon-md send_sms" title="send SMS" query_creator_id="<?php echo $query_data['query_creator_id'];?>" assigned_id="<?php echo $query_data['query_assigned_id'];?>" style="color: #ACACAC;">
						<i class="la la-bell-o kt-font-bolder" style="color: #ACACAC;"></i>
					</button>
				<?php } ?>
				<?php if($query_data['show_work_order_sheet'] && $this->session->userdata('production_access')['production_list_work_order_sheet_view_access']){?>
				<a href="https://localhost/demo_crm/production/pdf/<?php echo $query_data['query_reference']; ?>" target="_blank" title="work_order_sheet">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--dark kt-svg-icon--md">
						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<rect x="0" y="0" width="24" height="24"></rect>
							<path d="M3,12 C3,12 5.45454545,6 12,6 C16.9090909,6 21,12 21,12 C21,12 16.9090909,18 12,18 C5.45454545,18 3,12 3,12 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
							<path d="M12,15 C10.3431458,15 9,13.6568542 9,12 C9,10.3431458 10.3431458,9 12,9 C13.6568542,9 15,10.3431458 15,12 C15,13.6568542 13.6568542,15 12,15 Z" fill="#000000" opacity="0.3"></path>
						</g>
					</svg>
				</a>
				<?php } ?>
				<?php if($query_data['show_mtc_drawing_upload']){ ?>

					<button type="button" class="btn btn-sm btn-icon btn-icon-md upload_pdf" title="Upload" data-toggle="modal" data-target="#mtc_upload" style="color: #ACACAC;" query_number="<?php echo $query_data['query_number']; ?>">
						<i class="fa fa-upload kt-font-bolder" style="color: #ACACAC;"></i>
					</button>
				<?php } ?>
				<?php if($query_data['show_mtc_drawing_download']){ ?>

					<button type="button" class="btn btn-sm btn-icon btn-icon-md btn-label-success upload_pdf" title="Download" data-toggle="modal" data-target="#mtc_upload" query_number="<?php echo $query_data['query_number']; ?>">
						<i class="fa fa-upload kt-font-bolder"></i>
					</button>
				<?php } ?>
				<?php if($this->session->userdata('query_access')['query_list_comment_tab_access']){ ?>
					<a class="btn btn-sm btn-clean btn-icon btn-icon-md get_comment" title="Query Comment" href="javascript:;" style="color: #ACACAC;" query_master_id="<?php echo $query_data['id']; ?>">
						<i class="fa fa-comment kt-font-bolder" style="color: #ACACAC;"></i>
					</a>
				<?php } ?>
				<?php if($this->session->userdata('query_access')['query_list_sample_query_reassigned_to_access'] && in_array($query_data['modal_class_name'], array('sample_query','lead_query'))){ ?>
					<div class="dropdown">
						<button class="btn btn-sm btn-icon btn-icon-md" data-toggle="dropdown" title="Change Status" style="color: #ACACAC;">
							<i class="fa fa-user-friends kt-font-bolder"></i>
						</button>
						<div class="dropdown-menu dropdown-menu-right sample_query_reassigned_to" data-size="7" data-live-search="true" style="max-height: 300px; overflow-y: auto;">
							<?php if(!empty($query_data['change_assigned_to_array'])){ ?>
								<?php foreach ($query_data['change_assigned_to_array'] as $status_details) { ?>
									<a class="dropdown-item change_assigned_to" value="<?php echo $status_details['value']; ?>" id="<?php echo $query_data['id'];?>" query_number="<?php echo $query_data['query_number'];?>" query_type="<?php echo $query_data['modal_class_name'];?>">
										<?php echo $status_details['display_name']; ?>
									</a>
								<?php } ?>
							<?php } ?>
						</div>
					</div>
				<?php }?>

				<?php if($this->session->userdata('quotation_access')['quotation_list_action_edit_access'] && $query_data['show_quotation_edit']){ ?>
					<a href="<?php echo base_url('quotations/add/'.$query_data['quotation_mst_id'].'/'.$query_data['rfq_id']);?>" target="_blank" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Make Quote" style="margin: 10px 0px 0px 0px; color: #ACACAC;">
						<i class="la la-edit kt-font-bolder" style="color: #ACACAC;"></i>
					</a>
				<?php } ?>
				<?php if($query_data['show_osdr_document']){ ?>

					<button type="button" class="btn btn-sm btn-icon btn-icon-md btn-label-danger osdr_document" title="OSDR Docuemnt" data-toggle="modal" data-target="#osdr_document" work_order_no="<?php echo $query_data['query_reference']; ?>">
						<i class="fa fa-exclamation kt-font-bolder"></i>
					</button>
				<?php } ?>
				<?php if($query_data['change_pq_status']){ ?>

					<div class="dropdown">
						<button class="btn btn-sm btn-icon btn-icon-md" data-toggle="dropdown" title="Change Status" style="<?php echo $query_data['pq_status_background']; ?>">
							<i class="fa fa-check kt-font-bolder"></i>
						</button>
						<div class="dropdown-menu dropdown-menu-right" data-size="7" data-live-search="true">
							<?php if(!empty($query_data['pq_status_array'])){ ?>
								<?php foreach ($query_data['pq_status_array'] as $status_details) { ?>
									<a class="dropdown-item change_status <?php echo $status_details['class'];?>" value="<?php echo $status_details['value']; ?>" id="<?php echo $query_data['query_reference_id'];?>" is_pq="<?php echo $query_data['is_pq'];?>" query_type="<?php echo $query_data['modal_class_name'];?>">
										<?php echo $status_details['display_name']; ?>
									</a>
								<?php } ?>
							<?php } ?>
						</div>
					</div>
				<?php } ?>
			</td>
		</tr>
		<?php }?>
	<?php } ?>
<?php  } ?>