<div class="kt-chat__messages">
<?php if(!empty($conversation_details)) { ?>
	<?php foreach ($conversation_details as $single_conversation) { 
		// echo "<pre>";print_r($single_conversation);echo"</pre><hr>";exit;
		?>

		<?php if($single_conversation['query_assigned_id'] == $this->session->userdata('user_id')){ ?>
			<div class="kt-chat__message">
				<div class="kt-chat__user">
					<span class="kt-media kt-media--circle kt-media--sm" style="max-width:50px !important">
						<?php if(empty($people_information_details[$single_conversation['query_creator_id']])){ ?>
							<img src="assets/media/users/default.jpg" alt="image" style="max-width:50px !important;height: 50px;">
						<?php } else { ?>
							<img src="assets/hr_document/profile_pic/<?php echo $people_information_details[$single_conversation['query_creator_id']];?>" alt="image" style="max-width:50px !important;height: 50px;">
						<?php } ?>
					</span>
					<a href="javascript:void(0);" class="kt-chat__username"><?php echo $user_details[$single_conversation['query_creator_id']]; ?></a>
					<span class="kt-chat__datetime"><?php echo $single_conversation['add_time']; ?></span>
				</div>
				<div class="kt-chat__text kt-bg-light-success">
					<?php echo $single_conversation['query_text']; ?>
				</div>
			</div>
		<?php }else if($single_conversation['query_creator_id'] == $this->session->userdata('user_id')){ ?>
			<div class="kt-chat__message kt-chat__message--right">
				<div class="kt-chat__user">
					<span class="kt-chat__datetime"><?php echo $single_conversation['add_time']; ?></span>
					<a href="javascript:void(0);" class="kt-chat__username"><?php echo $user_details[$single_conversation['query_creator_id']]; ?></a>
					<span class="kt-media kt-media--circle kt-media--sm" style="max-width:50px !important;">
						<?php if(empty($people_information_details[$single_conversation['query_creator_id']])){ ?>
							<img src="assets/media/users/default.jpg" alt="image" style="max-width:50px !important;height: 50px;">
						<?php } else { ?>
							<img src="assets/hr_document/profile_pic/<?php echo $people_information_details[$single_conversation['query_creator_id']];?>" alt="image" style="max-width:50px !important;height: 50px;">
						<?php } ?>
					</span>
				</div>
				<div class="kt-chat__text kt-bg-light-brand">
					<?php echo $single_conversation['query_text']; ?>
				</div>
			</div>
		<?php }else{ ?>
			<div class="kt-chat__message">
				<div class="kt-chat__user">
					<span class="kt-media kt-media--circle kt-media--sm" style="max-width:50px !important">
						<?php if(empty($people_information_details[$single_conversation['query_creator_id']])){ ?>
							<img src="assets/media/users/default.jpg" alt="image" style="max-width:50px !important;height: 50px;">
						<?php } else { ?>
							<img src="assets/hr_document/profile_pic/<?php echo $people_information_details[$single_conversation['query_creator_id']];?>" alt="image" style="max-width:50px !important;height: 50px;">
						<?php } ?>
					</span>
					<a href="javascript:void(0);" class="kt-chat__username"><?php echo $user_details[$single_conversation['query_creator_id']]; ?></a>
					<span class="kt-chat__datetime"><?php echo $single_conversation['add_time']; ?></span>
				</div>
				<div class="kt-chat__text kt-bg-light-success">
					<?php echo $single_conversation['query_text']; ?>
				</div>
			</div>
		<?php } ?>
	<?php } ?>
<?php } ?>
</div>