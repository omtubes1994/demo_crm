<!--begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        Query
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">
                            <a href="javascript:;" class="btn btn-brand btn-elevate btn-icon-sm add_query_search_filter_form">
                                <span id="form_action">Add Search Filter<span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="query_search_filter_div" style="display:none;">
                        <?php $this->load->view('query/query_search_filter');?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" data-sticky-container>
            <div class="col-lg-3 col-xl-2">
                <div class="kt-portlet sticky" data-sticky="true" data-margin-top="100px" data-sticky-for="1023" data-sticky-class="kt-sticky">
                    <div class="kt-portlet__body kt-portlet__body--fit">
                        <ul class="kt-nav kt-nav--bold kt-nav--md-space kt-nav--v3 kt-margin-t-20 kt-margin-b-20 nav nav-tabs" role="tablist">
                            <li class="kt-nav__separator"></li>
                            <li class="kt-nav__item change_manage_tab <?php echo ($this->session->userdata('query_tab_name')['query_type'] == 'sales' && $this->session->userdata('query_tab_name')['query_status'] == 'open') ? 'active':'';?>" query_type="sales" query_status="open">
                                <a class="kt-nav__link active" data-toggle="tab" href="#kt_profile_tab_personal_information" role="tab">
                                    <span class="kt-nav__link-icon"><i class="flaticon-avatar"></i></span>
                                    <span class="kt-nav__link-text">Sales : Open</span>
                                </a>
                            </li>
                            <li class="kt-nav__separator"></li>
                            <li class="kt-nav__item change_manage_tab <?php echo ($this->session->userdata('query_tab_name')['query_type'] == 'sales' && $this->session->userdata('query_tab_name')['query_status'] == 'closed') ? 'active':'';?>" query_type="sales" query_status="closed">
                                <a class="kt-nav__link" data-toggle="tab" href="#kt_profile_tab_account_information" role="tab">
                                    <span class="kt-nav__link-icon"><i class="flaticon-lock"></i></span>
                                    <span class="kt-nav__link-text">Sales : Closed</span>
                                </a>
                            </li>
                            <li class="kt-nav__separator"></li>
                            <li class="kt-nav__item change_manage_tab <?php echo ($this->session->userdata('query_tab_name')['query_type'] == 'proforma' && $this->session->userdata('query_tab_name')['query_status'] == 'open') ? 'active':'';?>" query_type="proforma" query_status="open">
                                <a class="kt-nav__link" data-toggle="tab" href="#kt_profile_change_password" role="tab">
                                    <span class="kt-nav__link-icon"><i class="flaticon-settings"></i></span>
                                    <span class="kt-nav__link-text">Proforma : Open</span>
                                </a>
                            </li>
                            <li class="kt-nav__separator"></li>
                            <li class="kt-nav__item change_manage_tab <?php echo ($this->session->userdata('query_tab_name')['query_type'] == 'proforma' && $this->session->userdata('query_tab_name')['query_status'] == 'closed') ? 'active':'';?>" query_type="proforma" query_status="closed">
                                <a class="kt-nav__link" data-toggle="tab" href="#kt_profile_email_settings" role="tab">
                                    <span class="kt-nav__link-icon"><i class="flaticon-multimedia-2"></i></span>
                                    <span class="kt-nav__link-text">Proforma : Closed</span>
                                </a>
                            </li>
                            <li class="kt-nav__separator"></li>
                            <li class="kt-nav__item change_manage_tab <?php echo ($this->session->userdata('query_tab_name')['query_type'] == 'rfq' && $this->session->userdata('query_tab_name')['query_status'] == 'open') ? 'active':'';?>" query_type="rfq" query_status="open" style="display:none;">
                                <a class="kt-nav__link" role="tab" data-toggle="kt-tooltip" title="" data-placement="right" data-original-title="This feature is coming soon!">
                                    <span class="kt-nav__link-icon"><i class="flaticon-coins"></i></span>
                                    <span class="kt-nav__link-text">RFQ : Open</span>
                                </a>
                            </li>
                            <li class="kt-nav__separator" style="display:none;"></li>
                            <li class="kt-nav__item change_manage_tab <?php echo ($this->session->userdata('query_tab_name')['query_type'] == 'rfq' && $this->session->userdata('query_tab_name')['query_status'] == 'closed') ? 'active':'';?>" query_type="rfq" query_status="closed"  style="display:none;">
                                <a class="kt-nav__link" role="tab" data-toggle="kt-tooltip" title="" data-placement="right" data-original-title="This feature is coming soon!">
                                    <span class="kt-nav__link-icon"><i class="flaticon-customer"></i></span>
                                    <span class="kt-nav__link-text">RFQ : Closed</span>
                                </a>
                            </li>
                            <li class="kt-nav__separator" style="display:none;"></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-xl-10">
                <div class="kt-portlet">
                    <div class="kt-portlet__body" style="padding: 5px 0px 2px 0px !important;">
                        <div class="kt-widget27">
                            <div class="kt-widget27__container kt-portlet__space-x" style="padding: 0px 0px 0px 0px !important; margin: 0px 0px 0px 0px !important;">
                                <div class="tab-content" style="padding: 0px 0px 0px 0px !important;">
                                    <div class="tab-pane active">
                                        <div class="kt-widget11">
                                            <div class="table-responsive">

                                                <!--begin::Table-->
                                                <table class="table">

                                                    <!--begin::Tbody-->
                                                    <tbody class="kt-align-center">
                                                        <tr>
                                                            <td style="width: 10%;">
                                                                <a href="javascript:void(0);" class="kt-widget11__title">No</a>
                                                            </td>
                                                            <td style="width: 15%;">
                                                                <a href="javascript:void(0);" class="kt-widget11__title">Quotation No#</a>
                                                            </td>
                                                            <td style="width: 15%;">
                                                                <a href="javascript:void(0);" class="kt-widget11__title">Creator Person</a>
                                                            </td>
                                                            <td style="width: 15%;">
                                                                <a href="javascript:void(0);" class="kt-widget11__title">Answer Person</a>
                                                            </td>
                                                            <td style="width: 15%;">
                                                                <a href="javascript:void(0);" class="kt-widget11__title">Query</a>
                                                            </td>
                                                            <td style="width: 15%;">
                                                                <a href="javascript:void(0);" class="kt-widget11__title">Created Date</a>
                                                            </td>
                                                            <td style="width: 15%;">
                                                                <a href="javascript:void(0);" class="kt-widget11__title">Action</a>
                                                            </td>
                                                        </tr>
                                                    </tbody>

                                                    <!--end::Tbody-->
                                                    <!--begin::Tbody-->
                                                    <tbody>
                                                    </tbody>

                                                    <!--end::Tbody-->
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                                    
                <div class="kt-portlet">
                    <div class="kt-portlet__body" style="padding: 5px 0px 2px 0px !important;">
                        <div class="kt-widget27">
                            <div class="kt-widget27__container kt-portlet__space-x" style="padding: 0px 0px 0px 0px !important; margin: 0px 0px 0px 0px !important;">
                                <div class="tab-content" style="padding: 0px 0px 0px 0px !important;">
                                    <div class="tab-pane active">
                                        <div class="kt-widget11">
                                            <div class="table-responsive">

                                                <!--begin::Table-->
                                                <table class="table">

                                                    <!--begin::Tbody-->
                                                    <tbody class="kt-align-center" id="query_body">
                                                        <?php $this->load->view('query/query_body_table');?>
                                                    </tbody>

                                                    <!--end::Tbody-->
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet">
                    <div class="kt-portlet__body" style="padding: 5px 0px 2px 0px !important;" id="query_paggination">
                    
                        <?php $this->load->view('query/query_paggination');?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- end:: Content-->

<!--begin::Modal-->
<div class="modal fade add_query" id="add_product" tabindex="-1" role="dialog"  aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Query</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="reply_to_query_form" name="reply_query_form">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="query_text">Query Details</label>
                            <textarea   class="form-control " type="textarea" id="query_text" name="query_text"></textarea>
                        </div>
                    </div> 
                    </br>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-6 align-self-center">
                            <div class="query_id">
                            </div>
                           <input type="hidden" id="entered_by" name="entered_by" value="<?php echo $this->session->userdata('user_id');?>">
                           <button class="btn btn-success reply_query"type="button">Reply Query</button>
                        </div>
                    </div>
                </form>
            </div>    
            <hr/>
            <h4>Query</h4>
            <div id="tab_history">
                <table class="table table-bordered table-stripped">
                    <tbody class="kt-datatable__body query_txt">

                    </tbody>
                </table>
            </div>    
        <div>
            <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
            </div>
        </div>
    </div>
</div>

<!--end::Modal-->