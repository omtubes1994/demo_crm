<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Query extends MX_Controller {
    public function __construct(){
        parent::__construct();
        if(!$this->session->has_userdata('user_id') && $this->router->fetch_method() != 'daily_followup'){
            redirect('login', 'refresh');
            exit;
        }else{
            $access_count = 0;
            if(!empty($this->session->userdata('main_module_access')) && in_array(14, array_column($this->session->userdata('main_module_access'), 'module_id'))) {

                foreach ($this->session->userdata('sub_module_access') as $key => $value) {
                    
                    if($value['module_id'] == 14 && $value['status'] == 'Active') {
                        $access_count++;
                    }
                }
            }
            if($access_count == 0){
                redirect($this->session->userdata('home/dashboard'));
                exit;
            }
        }
        error_reporting(0);
        $this->load->model('query/query_model');
        $this->load->model('common/common_model');
    }
   
    public function query_list_data(){

        $data = array();
        if(empty($this->session->userdata('query_tab_name'))) {

            $this->session->set_userdata(array('query_tab_name' => array('query_status'=>'open', 'query_type' => 'sales')));
        }
        $where_array = $this->create_query_where('');
        $data = $this->create_query_data($where_array);
        // echo "<pre>";print_r($data);echo"</pre><hr>";exit();
        $this->load->view('header', array('title' => 'Query List'));
        $this->load->view('sidebar', array('title' => 'Query List'));
        $this->load->view('query/query_list',$data);
        $this->load->view('footer');
    }

    public function query_management() {

        $data = array();
        // echo "<pre>";print_r($data);echo"</pre><hr>";exit();
        $this->load->view('header', array('title' => 'Query List'));
        $this->load->view('sidebar', array('title' => 'Query List'));
        $this->load->view('query/query_management_index',$data);
        $this->load->view('footer');
    }

    public function sendSms($mobile,$sms_txt, $dlt_id){ 

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.msg91.com/api/sendhttp.php?authkey=345359A1zGdmFe5f930cf3P1&mobiles=91".$mobile."&message=".$sms_txt."&sender=OMTUBE&route=4&DLT_TE_ID=".$dlt_id,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        
        if ($err) {
          // echo "cURL Error #:" . $err;
        } else {
          // echo $response,"<hr>";
        }
    }

    public function pending_query_alert() {

        $pending_query_details = $this->common_model->get_all_conditional_data_sales_db('count(*) as total_pending_query, query_assigned_id',array('status'=>'Active'), 'query_details', 'result_array', array(), array(), 'query_assigned_id');
        foreach ($pending_query_details as  $assigned_person_details) {
            $user_details = array();
            $user_details = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>1, 'user_id'=> $assigned_person_details['query_assigned_id']), 'users', 'row_array');
            if(!empty($user_details)) {

                $sms_txt = 'Dear '.$user_details['name'].', Good Morning. You have '.$assigned_person_details['total_pending_query'].' query pending. - Om Tubes';
                // $this->sendSms(9082159156, str_replace(' ', '%20', $sms_txt), '1307164551238202161');
                $this->sendSms($user_details['mobile'], str_replace(' ', '%20', $sms_txt), '1307164551238202161');
            }
        }
        echo "<pre>";print_r($pending_query_details);echo"</pre><hr>";exit;
    }

    public function send_query_sms($creater_id, $sender_id, $reference_no, $query_type='raised') {

        $query_creator_details = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>1, 'user_id'=> $creater_id), 'users', 'row_array');
        $query_sender_details = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>1, 'user_id'=> $sender_id), 'users', 'row_array');
        if(!empty($query_creator_details) && !empty($query_sender_details)) {
             //echo "<pre>";print_r($query_creator_details);echo"</pre><hr>";
             //echo "<pre>";print_r($query_sender_details);echo"</pre><hr>";exit;
            $dtl_id = '1307161789767950941';
            if($query_type === 'raised') {
    
                $sms_txt = 'Dear '.$query_creator_details['name'].', '.$query_sender_details['name'].' has raised a query against '.$reference_no;

            } elseif($query_type === 'answered') {
                $sms_txt = 'Dear '.$query_creator_details['name'].', '.$query_sender_details['name'].' has answered to your query against '.$reference_no;
                $dtl_id = '1307161789779790647';
            }
            // $this->sendSms('9082159156', str_replace(' ', '%20', $sms_txt), $dtl_id);
            $this->sendSms($query_creator_details['mobile'], str_replace(' ', '%20', $sms_txt), $dtl_id);
        }
        // $this->sendSms($query_creator_details['mobile'], str_replace(' ', '%20', $sms_txt), '1307161789767950941');
    }

    public function query_list(){

        $data = $this->prepare_data_for_query(array('status'=>'Active', 'query_type'=> 'sales_query', 'query_status'=> 'open'), 0, 0);
        $data['count'] = $this->get_query_total();

        
        // echo "<pre>";print_r($data);echo"</pre><hr>";exit;
        $this->load->view('header', array('title' => 'Query List'));
        $this->load->view('sidebar', array('title' => 'Query List'));
        $this->load->view('query/query_list_index', $data);
        $this->load->view('footer');
    }

    public function set_reply(){
        $data=$this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'), 'query_master');
        if(!empty($data)){
                foreach($data as $single_data){
                    if($single_data['query_status']=="open"){
                        echo "<pre>";print_r("id =".$single_data['id']);echo"</pre><hr>";
                       if(!empty($single_data['query_add_time'])){
                           $time1 = date_create($single_data['query_add_time']);
                           $time2 =date_create();
                           $interval=date_diff($time1,$time2);
                           $difference=$interval->format('%d ');
                          echo "<pre>";print_r($difference.'difference');echo"</pre><hr>";
                              if($difference >= 2)
                              {
                           
                                $this->common_model->update_data_sales_db('query_master', array('query_status'=>'closed'), array('id'=>$single_data['id']));
                              }
                        
                        }
                    }
                }
        }
        echo "<pre>";print_r("done");echo"</pre><hr>";   
    }

    public function ajax_function() {

        if($this->input->is_ajax_request()) {
            $call_type = $this->input->post('call_type');
            $response['status'] = 'successful'; 
            switch ($call_type) {
               
                case 'get_sales_query_history':
                    
                    $data = array();
                    $data['user_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array(), 'users'), 'name', 'user_id');
                    $quotation_assign_details = $this->query_model->query_quotation_assign_details($this->input->post('sales_query_quotation_id'));

                    $response['quotation_id'] = 0;
                    if(!empty($quotation_assign_details)){

                        $response['quotation_id'] = $quotation_assign_details['quotation_id'];
                        $response['quote_no'] = $quotation_assign_details['quote_no'];
                        $response['sales_person_id'] = $quotation_assign_details['sales_person_id'];
                        $response['query_type_option_tag'] = '';
                        if(in_array($this->session->userdata('role'),array(1, 5, 16, 17))){
                            $response['query_assigned_id'] = $quotation_assign_details['procurement_person_id'];
                            $response['query_type_option_tag'] = '
                            <select class="form-control kt-selectpicker" name="sub_query_type">
                                <optgroup label="Procurement" data-max-options="2">
                                    <option value="Specification Related" selected>Select Query Type</option>
                                    <option value="Weight Dimension Freight">Weight Dimension Freight ('.$data['user_details'][$quotation_assign_details['procurement_person_id']].')</option>
                                    <option value="Discount">Discount ('.$data['user_details'][$quotation_assign_details['procurement_person_id']].')</option>
                                    <option value="Specification Related">Specification Related ('.$data['user_details'][$quotation_assign_details['procurement_person_id']].')</option>
                                </optgroup>
                                <optgroup label="Technical" data-max-options="2">
                                    <option value="Sample MTC">Sample MTC ('.$data['user_details'][302].')</option>
                                    <option value="Drawing">Drawing ('.$data['user_details'][325].')</option>
                                </optgroup>
                            </select>';
                        } else if(in_array($this->session->userdata('role'),array(6, 8, 10, 11, 21))){
                            $response['query_assigned_id'] = $this->session->userdata('user_id');
                            $response['query_type_option_tag'] = '
                            <select class="form-control kt-selectpicker" name="sub_query_type" readonly>
                                <optgroup label="Procurement" data-max-options="2">
                                    <option value="" selected>'.$this->session->userdata('name').'</option>
                                </optgroup>
                            </select>';
                        }

                        $query_master_details = $this->common_model->get_dynamic_data_sales_db('id, query_number', array('status'=>'Active', 'query_type'=>'sales_query','query_reference_id'=>$quotation_assign_details['quotation_id']), 'query_master', 'row_array');
                        // echo "<pre>";print_r($query_master_details);echo"</pre><hr>";exit;

                        if(!empty($query_master_details)) {

                            $data['conversation_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'query_number'=>$query_master_details['query_number']), 'query_details');
                            if(!empty($data['conversation_details'])){

                                foreach ($data['conversation_details'] as $conversation_details_key => $single_chat) {

                                    $data['conversation_details'][$conversation_details_key]['tat'] = $this->creat_tat($single_chat['add_time']);
                                }
                            }
                            $data['people_information_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'), 'people_information'), 'profile_pic_file_path', 'user_id');
                        }
                        // echo "<pre>";print_r($data);echo"</pre><hr>";exit;
                        // $response['query_history_html'] = $this->load->view('query/chat_conversation', $data, true);
                        $response['query_history_html'] = $this->load->view('query/chat_conversation_v2', $data, true);
                    }
                    // echo "<pre>";print_r($quotation_assign_details);echo"</pre><hr>";die;
                break;
                case 'save_sales_query':

                    // echo "<pre>";print_r($this->input->post('form_data'));echo"</pre><hr>";exit;
                    $form_data = array_column($this->input->post('form_data'),'value','name');
                    // echo "<pre>";print_r($form_data);echo"</pre><hr>";exit;
                    $response['status'] = 'failed';
                    $response['message'] = "Form Data Not Found. Please Contact Developer";
                    if(!empty($form_data)){
                        if(empty($form_data['query_text'])) {

                            $response['message'] = "Please Enter Query!!!";
                        } else if($form_data['query_assigned_id'] == 'blank') {

                            $response['message'] = "Please Select Query Type!!!";
                        } else {

                            //checking record exist or not in query master
                            $query_master_details = $this->common_model->get_dynamic_data_sales_db('id, query_number', array('status'=>'Active', 'query_type'=>'sales_query','query_reference_id'=>$form_data['query_reference_id']), 'query_master', 'row_array');
                            if(empty($query_master_details)){

                                //create insert array for query master
                                $query_master_insert_data = $form_data;
                                $last_query_id = $this->common_model->get_dynamic_data_sales_db('last_quote_id', array('status'=> "Active", 'quote_name'=>'sales_query'), 'last_quote_created_number', 'row_array');
                                $query_master_insert_data['query_number'] = 'SQ'.(((int)$last_query_id['last_quote_id'])+1);
                                $query_master_insert_data['query_add_time'] = date("Y-m-d H:i:s");
                                $query_master_insert_data['add_time'] = date("Y-m-d H:i:s");
                                // echo "<pre>";print_r($query_master_insert_data);echo"</pre><hr>";die;
                                $query_master_id = $this->common_model->insert_data_sales_db('query_master',$query_master_insert_data);
                                if(!empty($query_master_id)){
                                    
                                    $this->common_model->update_data_sales_db('last_quote_created_number', array('last_quote_id'=>(((int)$last_query_id['last_quote_id'])+1)), array('status'=> "Active", 'quote_name'=>'sales_query'));
                                    //create insert array for query details
                                    $query_details_insert_data =  array();
                                    $query_details_insert_data['query_number'] = $query_master_insert_data['query_number'];
                                    $query_details_insert_data['query_text'] = $form_data['query_text'];
                                    $query_details_insert_data['add_time'] = date("Y-m-d H:i:s");
                                    if(in_array($this->session->userdata('role'),array(5,16,1))){

                                        $this->send_query_sms($form_data['query_assigned_id'], $form_data['query_creator_id'], 'Quotation No '.$form_data['query_reference']);
                                        $query_details_insert_data['query_creator_id'] = $form_data['query_creator_id'];
                                        $query_details_insert_data['query_assigned_id'] = $form_data['query_assigned_id'];
                                    } else if(in_array($this->session->userdata('role'),array(6, 8, 10, 11, 21))){

                                        $this->send_query_sms($form_data['query_creator_id'], $form_data['query_assigned_id'], 'Quotation No '.$form_data['query_reference']);
                                        $query_details_insert_data['query_creator_id'] = $form_data['query_assigned_id'];
                                        $query_details_insert_data['query_assigned_id'] = $form_data['query_creator_id'];
                                    }
                                    // echo "<pre>";print_r($query_details_insert_data);echo"</pre><hr>";exit;
                                    $this->common_model->insert_data_sales_db('query_details',$query_details_insert_data);
                                }
                                
                            }else{
                                //create insert array for query details
                                $query_details_insert_data =  array();
                                $query_details_insert_data['query_number'] = $query_master_details['query_number'];
                                $query_details_insert_data['query_text'] = $form_data['query_text'];
                                $query_details_insert_data['add_time'] = date("Y-m-d H:i:s");
                                if(in_array($this->session->userdata('role'),array(5,16,1))){
                                    $this->send_query_sms($form_data['query_assigned_id'], $form_data['query_creator_id'], 'Quotation No '.$form_data['query_reference']);
                                    $query_details_insert_data['query_creator_id'] = $form_data['query_creator_id'];
                                    $query_details_insert_data['query_assigned_id'] = $form_data['query_assigned_id'];
                                    $this->common_model->update_data_sales_db('query_master', array('query_text'=>$form_data['query_text'], 'query_assigned_id'=> $form_data['query_assigned_id'], 'query_creator_id'=> $form_data['query_creator_id'], 'sub_query_type'=> $form_data['sub_query_type'], 'query_add_time'=> date("Y-m-d H:i:s"), 'reply_text'=>'', 'reply_creator_id'=>0, 'reply_assigned_id'=>0, 'reply_add_time'=> NULL, 'query_status'=> 'open'), array('id'=> $query_master_details['id']));
                                } else if(in_array($this->session->userdata('role'),array(6, 8, 10, 11, 21))){
                                    $this->send_query_sms($form_data['query_creator_id'], $form_data['query_assigned_id'], 'Quotation No '.$form_data['query_reference'], 'answered');
                                    $query_details_insert_data['query_creator_id'] = $form_data['query_assigned_id'];
                                    $query_details_insert_data['query_assigned_id'] = $form_data['query_creator_id'];
                                    $this->common_model->update_data_sales_db('query_master', array('reply_text'=>$form_data['query_text'], 'reply_assigned_id'=> $form_data['query_assigned_id'], 'reply_creator_id'=> $form_data['query_creator_id'], 'reply_add_time'=> date("Y-m-d H:i:s"), 'query_status'=> 'open'), array('id'=> $query_master_details['id']));
                                }
                                // echo "<pre>";print_r($query_details_insert_data);echo"</pre><hr>";exit;
                                $this->common_model->insert_data_sales_db('query_details',$query_details_insert_data);
                            }
                            $response['status'] = 'successful';
                            $response['message'] = "Successfully added Sales Query.";
                        }
                    }
                break;
                case 'get_proforma_query_history':
                    
                    $data = array();
                    $quotation_assign_details = $this->query_model->query_quotation_assign_details($this->input->post('proforma_query_quotation_id'));
                    $response['quotation_id'] = 0;
                    if(!empty($quotation_assign_details)){

                        $response['quotation_id'] = $quotation_assign_details['quotation_id'];
                        $response['proforma_no'] = $quotation_assign_details['proforma_no'];
                        $response['query_type_option_tag'] = '<select class="form-control kt-selectpicker" name="query_assigned_id">';
                        $response['creator_id'] = $this->session->userdata('user_id');
                        $handle_by_array = array();
                        if(!empty($quotation_assign_details['handled_by'])) {

                            $handle_by_option_tag .= '<optgroup label="Handled By" data-max-options="2">';
                            foreach (explode(',', $quotation_assign_details['handled_by']) as $single_handler_name) {
                                
                                $user_details = $this->common_model->get_dynamic_data_sales_db('user_id, name', 'name like "%'.$single_handler_name.'%" and (role=8 or role=6) AND status = 1', 'users', 'row_array');
                                $handle_by_array[] = $user_details['user_id'];
                                $handle_by_option_tag .= '<option value="'.$user_details['user_id'].'">'.$user_details['name'].'</option>';
                            }
                            $handle_by_option_tag .= '</optgroup>';
                        }
                        if(in_array($this->session->userdata('role'),array(5,16,1))){

                            $response['query_type_option_tag'] .= '<optgroup label="QC Head" data-max-options="2"><option value="blank" selected>Select Assigned To</option><option value="33">Aftab Qureshi</option></optgroup>'.$handle_by_option_tag;
                            
                        } else if(in_array($this->session->userdata('role'),array(11))){ 

                            $user_details = $this->common_model->get_dynamic_data_sales_db('user_id, name', array('user_id'=>$quotation_assign_details['sales_person_id']), 'users', 'row_array');
                            $response['query_type_option_tag'] .= '<optgroup label="Sales Person" data-max-options="2"><option value="blank" selected>Select Assigned To</option><option value="'.$user_details['user_id'].'">'.$user_details['name'].'</option></optgroup>'.$handle_by_option_tag;
                            
                        }else if (in_array($this->session->userdata('user_id'), $handle_by_array)){

                            $user_details = $this->common_model->get_dynamic_data_sales_db('user_id, name', array('user_id'=>$quotation_assign_details['sales_person_id']), 'users', 'row_array');
                            $response['query_type_option_tag'] .= '<optgroup label="QC Head" data-max-options="2"><option value="blank" selected>Select Assigned To</option><option value="33">Aftab Qureshi</option></optgroup><optgroup label="Sales Person" data-max-options="2"><option value="'.$user_details['user_id'].'">'.$user_details['name'].'</option></optgroup>';

                        }
                        $response['query_type_option_tag'] .= '</select>';
                        $query_master_details = $this->common_model->get_dynamic_data_sales_db('id, query_number', array('status'=>'Active', 'query_type'=>'proforma_query','query_reference_id'=>$quotation_assign_details['quotation_id']), 'query_master', 'row_array');
                        if(!empty($query_master_details)) {

                            $data['conversation_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'query_number'=>$query_master_details['query_number']), 'query_details');
                            $data['user_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status'=>'1'), 'users'), 'name', 'user_id');
                            $data['people_information_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'), 'people_information'), 'profile_pic_file_path', 'user_id');
                        }
                        // echo "<pre>";print_r($data);echo"</pre><hr>";exit;
                        $response['query_history_html'] = $this->load->view('query/chat_conversation', $data, true);
                        // echo "<pre>";print_r($response);echo"</pre><hr>";exit;
                    }
                    // echo "<pre>";print_r($quotation_assign_details);echo"</pre><hr>";die;
                break;
                case 'save_proforma_query':

                    // echo "<pre>";print_r($this->input->post('form_data'));echo"</pre><hr>";exit;
                    $form_data = array_column($this->input->post('form_data'),'value','name');
                    // echo "<pre>";print_r($form_data);echo"</pre><hr>";exit;
                    $response['status'] = 'failed';
                    $response['message'] = "Form Data Not Found. Please Contact Developer";
                    if(!empty($form_data)){
                        if(empty($form_data['query_text'])) {

                            $response['message'] = "Please Enter Query!!!";
                        } else if($form_data['query_assigned_id'] == 'blank') {

                            $response['message'] = "Please Select Query Type!!!";
                        } else {

                            //checking record exist or not in query master
                            $query_master_details = $this->common_model->get_dynamic_data_sales_db('id, query_number', array('status'=>'Active', 'query_type'=>'proforma_query','query_reference_id'=>$form_data['query_reference_id']), 'query_master', 'row_array');
                            if(empty($query_master_details)){

                                //create insert array for query master
                                $query_master_insert_data = $form_data;
                                $last_query_id = $this->common_model->get_dynamic_data_sales_db('last_quote_id', array('status'=> "Active", 'quote_name'=>'proforma_query'), 'last_quote_created_number', 'row_array');
                                $query_master_insert_data['query_number'] = 'PQ'.(((int)$last_query_id['last_quote_id'])+1);
                                $query_master_insert_data['query_add_time'] = date("Y-m-d H:i:s");

                                // echo "<pre>";print_r($query_master_insert_data);echo"</pre><hr>";
                                $query_master_id = $this->common_model->insert_data_sales_db('query_master',$query_master_insert_data);
                                if(!empty($query_master_id)){
                                    $this->common_model->update_data_sales_db('last_quote_created_number', array('last_quote_id'=>(((int)$last_query_id['last_quote_id'])+1)), array('status'=> "Active", 'quote_name'=>'proforma_query'));
                                    //create insert array for query details
                                    $query_details_insert_data =  array();
                                    $query_details_insert_data['query_number'] = $query_master_insert_data['query_number'];
                                    $query_details_insert_data['query_text'] = $form_data['query_text'];
                                    $query_details_insert_data['query_creator_id'] = $form_data['query_creator_id'];
                                    $query_details_insert_data['query_assigned_id'] = $form_data['query_assigned_id'];
                                    // echo "<pre>";print_r($query_details_insert_data);echo"</pre><hr>";exit;
                                    $this->common_model->insert_data_sales_db('query_details',$query_details_insert_data);
                                }
                                
                            }else{
                                //create insert array for query details
                                $query_details_insert_data =  array();
                                $query_details_insert_data['query_number'] = $query_master_details['query_number'];
                                $query_details_insert_data['query_text'] = $form_data['query_text'];
                                $query_details_insert_data['query_creator_id'] = $form_data['query_creator_id'];
                                $query_details_insert_data['query_assigned_id'] = $form_data['query_assigned_id'];
                                
                                if(in_array($this->session->userdata('role'),array(6, 8, 10, 11))){
                                    if($this->session->userdata('user_id') == 59){

                                        $this->common_model->update_data_sales_db('query_master', array('query_text'=>$form_data['query_text'], 'query_assigned_id'=> $form_data['query_assigned_id'], 'query_add_time'=> date("Y-m-d hH:i:s")), array('id'=> $query_master_details['id']));
                                    }else{

                                        $this->common_model->update_data_sales_db('query_master', array('query_text'=>$form_data['query_text'], 'query_assigned_id'=> $form_data['query_assigned_id'], 'query_creator_id'=> $form_data['query_creator_id'], 'query_add_time'=> date("Y-m-d hH:i:s")), array('id'=> $query_master_details['id']));
                                    }
                                } else if(in_array($this->session->userdata('role'),array(5,16,1))){
                                    $this->common_model->update_data_sales_db('query_master', array('reply_text'=>$form_data['query_text'], 'reply_assigned_id'=> $form_data['query_assigned_id'], 'reply_creator_id'=> $form_data['query_creator_id'], 'reply_add_time'=> date("Y-m-d H:i:s")), array('id'=> $query_master_details['id']));
                                }
                                // echo "<pre>";print_r($query_details_insert_data);echo"</pre><hr>";exit;
                                $this->common_model->insert_data_sales_db('query_details',$query_details_insert_data);
                            }
                            $this->send_query_sms($form_data['query_assigned_id'], $form_data['query_creator_id'], 'Proforma No '.$form_data['query_reference'], 'answered');
                            $response['status'] = 'successful';
                            $response['message'] = "Successfully added Proforma Query.";
                        }
                    }
                break;
                case 'get_production_query_history':
                    
                    $data = array();
                    $data['user_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array(), 'users'), 'name', 'user_id');
                    $quotation_assign_details = $this->query_model->query_quotation_assign_details($this->input->post('production_query_quotation_id'));
                    // echo "<pre>";print_r($quotation_assign_details);echo"</pre><hr>";exit;
                    $response['quotation_id'] = 0;
                    if(!empty($quotation_assign_details)){

                        if(in_array($quotation_assign_details['product_family'], array('piping', 'tubing', 'fastener'))){
                            $data_entry_person = 328;
                        }else if(in_array($quotation_assign_details['product_family'], array('instrumentation', 'precision', 'industrial', 'valve'))){
                            $data_entry_person = 355;
                        }
                        $response['work_order_no'] = $quotation_assign_details['work_order_no'];
                        $response['quotation_id'] = $quotation_assign_details['quotation_id'];
                        $response['creator_id'] = $this->session->userdata('user_id');

                        if(in_array($this->session->userdata('role'),array(1, 5, 16))){
                            $response['query_assigned_id'] = $data_entry_person;
                            $response['query_type_option_tag'] = '
                            <select class="form-control kt-selectpicker" name="sub_query_type">
                                <optgroup label="Procurement" data-max-options="2">
                                    <option value="Specification Related" selected>Select Query Type</option>
                                    <option value="Physical Inspection">Physical Inspection ('.$data['user_details'][$data_entry_person].')</option>
                                    <option value="Material Readiness">Material Readiness ('.$data['user_details'][$data_entry_person].')</option>
                                    <option value="Payment">Payment ('.$data['user_details'][$data_entry_person].')</option>
                                    <option value="Dispatch">Dispatch ('.$data['user_details'][$data_entry_person].')</option>
                                </optgroup>
                                <optgroup label="QC" data-max-options="2">
                                    <option value="MTC">MTC ('.$data['user_details'][297].')</option>
                                </optgroup>
                            </select>';
                        }elseif(in_array($this->session->userdata('role'),array(4, 6, 8))){

                            $response['query_assigned_id'] = $quotation_assign_details['sales_person_id'];
                            $response['query_type_option_tag'] = '
                            <select class="form-control kt-selectpicker" name="sub_query_type">
                                <option value="Sales Specification">Sales ('.$data['user_details'][$quotation_assign_details['sales_person_id']].')</option>
                                <option value="QC Specification">QC ('.$data['user_details'][297].')</option>
                            </select>';

                        }elseif(in_array($this->session->userdata('role'),array(10, 11))){

                            $response['query_assigned_id'] = $quotation_assign_details['sales_person_id'];
                            $response['query_type_option_tag'] = '
                            <select class="form-control kt-selectpicker" name="sub_query_type">
                                <option value="Procurment Specification">Procurment ('.$data['user_details'][327].')</option>
                                <option value="Sales Specification">Sales ('.$data['user_details'][$quotation_assign_details['sales_person_id']].')</option>
                            </select>';
                        }

                        $query_master_details = $this->common_model->get_dynamic_data_sales_db('id, query_number', array('status'=>'Active', 'query_type'=>'production_query','query_reference_id'=>$quotation_assign_details['quotation_id']), 'query_master', 'row_array');
                        if(!empty($query_master_details)) {

                            $data['conversation_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'query_number'=>$query_master_details['query_number']), 'query_details');
                            if(!empty($data['conversation_details'])){

                                foreach ($data['conversation_details'] as $conversation_details_key => $single_chat) {
                                    
                                    $data['conversation_details'][$conversation_details_key]['tat'] = $this->creat_tat($single_chat['add_time']);
                                }
                            }
                            $data['user_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status'=>'1'), 'users'), 'name', 'user_id');
                            $data['people_information_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'), 'people_information'), 'profile_pic_file_path', 'user_id');
                        }


                        // echo "<pre>";print_r($data);echo"</pre><hr>";exit;
                        // $response['query_history_html'] = $this->load->view('query/chat_conversation', $data, true);
                        $response['query_history_html'] = $this->load->view('query/chat_conversation_v2', $data, true);
                        // echo "<pre>";print_r($response);echo"</pre><hr>";exit;
                    }
                    // echo "<pre>";print_r($quotation_assign_details);echo"</pre><hr>";die;
                break;
                case 'save_production_query':

                    // echo "<pre>";print_r($this->input->post('form_data'));echo"</pre><hr>";exit;
                    $form_data = array_column($this->input->post('form_data'),'value','name');
                    //echo "<pre>";print_r($form_data);echo"</pre><hr>";exit;
                    $response['status'] = 'failed';
                    $response['message'] = "Form Data Not Found. Please Contact Developer";
                    if(!empty($form_data)){
                        if(empty($form_data['query_text'])) {

                            $response['message'] = "Please Enter Query!!!";
                        } else if($form_data['query_assigned_id'] == 'blank') {

                            $response['message'] = "Please Select Assigned To !!!";
                        } else {

                            //checking record exist or not in query master
                            $query_master_details = $this->common_model->get_dynamic_data_sales_db('id, query_number, query_creator_id, query_assigned_id', array('status'=>'Active', 'query_type'=>'production_query','query_reference_id'=>$form_data['query_reference_id']), 'query_master', 'row_array');
                            if(empty($query_master_details)){

                                //create insert array for query master
                                $query_master_insert_data = $form_data;
                                $last_query_id = $this->common_model->get_dynamic_data_sales_db('last_quote_id', array('status'=> "Active", 'quote_name'=>'production_query'), 'last_quote_created_number', 'row_array');
                                $query_master_insert_data['query_number'] = 'PDQ'.(((int)$last_query_id['last_quote_id'])+1);
                                $query_master_insert_data['query_add_time'] = date("Y-m-d H:iD:s");
                                $query_master_insert_data['add_time'] = date("Y-m-d H:iD:s");

                                // echo "<pre>";print_r($query_master_insert_data);echo"</pre><hr>";
                                $query_master_id = $this->common_model->insert_data_sales_db('query_master',$query_master_insert_data);
                                if(!empty($query_master_id)){

                                    $this->common_model->update_data_sales_db('last_quote_created_number', array('last_quote_id'=>(((int)$last_query_id['last_quote_id'])+1)), array('status'=> "Active", 'quote_name'=>'production_query'));
                                    //create insert array for query details
                                    $query_details_insert_data =  array();
                                    $query_details_insert_data['query_number'] = $query_master_insert_data['query_number'];
                                    $query_details_insert_data['query_text'] = $form_data['query_text'];
                                    $query_details_insert_data['add_time'] = date("Y-m-d H:i:s");

                                    if(in_array($this->session->userdata('role'),array(1, 5, 16))){

                                        $this->send_query_sms($form_data['query_assigned_id'], $form_data['query_creator_id'], 'Work Order No '.$form_data['query_reference']);
                                        $query_details_insert_data['query_creator_id'] = $form_data['query_creator_id'];
                                        $query_details_insert_data['query_assigned_id'] = $form_data['query_assigned_id'];
                                    } else if(in_array($this->session->userdata('role'),array(4, 6, 8, 10, 11))){

                                        $this->send_query_sms($form_data['query_creator_id'], $form_data['query_assigned_id'], 'Work Order No '.$form_data['query_reference']);
                                        $query_details_insert_data['query_creator_id'] = $form_data['query_assigned_id'];
                                        $query_details_insert_data['query_assigned_id'] = $form_data['query_creator_id'];
                                    }

                                    $this->common_model->insert_data_sales_db('query_details',$query_details_insert_data);
                                }
                                
                            }else{
                                // echo "<pre>";print_r($form_data);echo"</pre><hr>";exit;
                                //create insert array for query details
                                $query_details_insert_data =  array();
                                $query_details_insert_data['query_number'] = $query_master_details['query_number'];
                                $query_details_insert_data['query_text'] = $form_data['query_text'];
                                $query_details_insert_data['add_time'] = date("Y-m-d H:i:s");

                                if(in_array($this->session->userdata('role'),array(5, 16, 1))){

                                    $this->send_query_sms($form_data['query_assigned_id'], $form_data['query_creator_id'], 'Work Order No '.$form_data['query_reference']);
                                    $query_details_insert_data['query_creator_id'] = $form_data['query_creator_id'];
                                    $query_details_insert_data['query_assigned_id'] = $form_data['query_assigned_id'];
                                    $this->common_model->update_data_sales_db('query_master', array('query_text'=>$form_data['query_text'], 'query_assigned_id'=> $form_data['query_assigned_id'], 'query_creator_id'=> $form_data['query_creator_id'], 'sub_query_type'=> $form_data['sub_query_type'], 'query_add_time'=> date("Y-m-d H:i:s"), 'reply_text'=>'', 'reply_creator_id'=>0, 'reply_assigned_id'=>0, 'reply_add_time'=> NULL, 'query_status'=> 'open'), array('id'=> $query_master_details['id']));

                                }else if(in_array($this->session->userdata('role'),array(4, 6, 8, 10, 11))){

                                    $this->send_query_sms($form_data['query_assigned_id'], $form_data['query_creator_id'], 'Work Order No '.$form_data['query_reference'], 'answered');
                                    $query_details_insert_data['query_creator_id'] = $form_data['query_creator_id'];
                                    $query_details_insert_data['query_assigned_id'] = $form_data['query_assigned_id'];
                                    $this->common_model->update_data_sales_db('query_master', array('reply_text'=>$form_data['query_text'], 'reply_creator_id'=> $form_data['query_creator_id'], 'reply_assigned_id'=> $form_data['query_assigned_id'],'reply_add_time'=> date("Y-m-d H:i:s"), 'query_status'=> 'open'), array('id'=> $query_master_details['id']));

                                }
                                // echo "<pre>";print_r($query_details_insert_data);echo"</pre><hr>";exit;
                                $this->common_model->insert_data_sales_db('query_details',$query_details_insert_data);
                                
                            }
                            $response['status'] = 'successful';
                            $response['message'] = "Successfully added Production Query.";
                        }
                    }
                break;
                case 'get_rfq_query_history':
                    $data = array();
                    $data['user_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status'=>'1'), 'users'), 'name', 'user_id');                   
                    $rfq_assign_details = $this->query_model->query_rfq_assign_details($this->input->post('rfq_query_rfq_id'));                

                    if(!empty($rfq_assign_details)){

                        if(in_array($this->session->userdata('role'), array(1, 5, 6, 8, 16, 17))){

                            $response['query_reference'] = $rfq_assign_details['rfq_no'];
                            $response['query_reference_id'] = $rfq_assign_details['rfq_mst_id'];
                            $response['query_creator_id'] = $rfq_assign_details['assigned_to'];
                            $response['query_assigned_id'] = $rfq_assign_details['rfq_sentby'];

                            $query_master_details = $this->common_model->get_dynamic_data_sales_db('id, query_number', array('status'=>'Active', 'query_type'=>'rfq_query','query_reference_id'=>$rfq_assign_details['rfq_mst_id']), 'query_master', 'row_array');  

                            if(!empty($query_master_details)) {

                                $data['conversation_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'query_number'=>$query_master_details['query_number']), 'query_details');
                                if(!empty($data['conversation_details'])){

                                    foreach ($data['conversation_details'] as $conversation_details_key => $single_chat) {
                                        
                                        $data['conversation_details'][$conversation_details_key]['tat'] = $this->creat_tat($single_chat['add_time']);
                                    }
                                }
                                $data['people_information_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'), 'people_information'), 'profile_pic_file_path', 'user_id');
                            }
                            //echo "<pre>";print_r($data);echo"</pre><hr>";exit;
                            // $response['rfq_query_history'] = $this->load->view('query/chat_conversation', $data, true);
                            $response['rfq_query_history'] = $this->load->view('query/chat_conversation_v2', $data, true);
                        }
                         
                    }
                //    echo "<pre>";print_r($rfq_assign_details);echo"</pre><hr>";die;
                break;
                case 'save_rfq_query':

                    // echo "<pre>";print_r($this->input->post('form_data'));echo"</pre><hr>";exit;
                    $form_data = array_column($this->input->post('form_data'),'value','name');
                    // echo "<pre>";print_r($form_data);echo"</pre><hr>";exit;
                    $response['status'] = 'failed';
                    $response['message'] = "Form Data Not Found. Please Contact Developer";
                    if(!empty($form_data)){
                        if(empty($form_data['query_text'])) {

                            $response['message'] = "Please Enter Query!!!";
                        } else if($form_data['query_assigned_id'] == 'blank') {

                            $response['message'] = "Please Select Query Type!!!";
                        } else {

                            //checking record exist or not in query master
                            $query_master_details = $this->common_model->get_dynamic_data_sales_db('id, query_number', array('status'=>'Active', 'query_type'=>'rfq_query','query_reference_id'=>$form_data['query_reference_id']), 'query_master', 'row_array');

                            if(empty($query_master_details)){

                                //create insert array for query master
                                $query_master_insert_data = $form_data;
                                $last_query_id = $this->common_model->get_dynamic_data_sales_db('last_quote_id', array('status'=> "Active", 'quote_name'=>'rfq_query'), 'last_quote_created_number', 'row_array');
                                $query_master_insert_data['query_number'] = 'RFQQ'.(((int)$last_query_id['last_quote_id'])+1);
                                $query_master_insert_data['query_add_time'] = date("Y-m-d H:i:s");
                                $query_master_insert_data['add_time'] = date("Y-m-d H:i:s");
                                $query_master_insert_data['sub_query_type'] = 'RFQ';


                                $query_master_id =  $this->common_model->insert_data_sales_db('query_master',$query_master_insert_data);
                                if(!empty($query_master_id)){

                                    $this->common_model->update_data_sales_db('last_quote_created_number', array('last_quote_id'=>(((int)$last_query_id['last_quote_id'])+1)), array('status'=> "Active", 'quote_name'=>'rfq_query'));
                                    //create insert array for query details
                                    $query_details_insert_data =  array();
                                    $query_details_insert_data['query_number'] = $query_master_insert_data['query_number'];
                                    $query_details_insert_data['query_text'] = $form_data['query_text'];
                                    $query_details_insert_data['add_time'] = date("Y-m-d H:i:s");

                                    if(in_array($this->session->userdata('role'),array(6, 8))){

                                        $this->send_query_sms($form_data['query_assigned_id'], $form_data['query_creator_id'], 'RFQ No '.$form_data['query_reference'],'raised');
                                        $query_details_insert_data['query_creator_id'] = $form_data['query_creator_id'];
                                        $query_details_insert_data['query_assigned_id'] = $form_data['query_assigned_id'];

                                    } else if(in_array($this->session->userdata('role'),array(1, 5, 16))){

                                        $this->send_query_sms($form_data['query_creator_id'], $form_data['query_assigned_id'], 'RFQ No '.$form_data['query_reference']);
                                        $query_details_insert_data['query_creator_id'] = $form_data['query_assigned_id'];
                                        $query_details_insert_data['query_assigned_id'] = $form_data['query_creator_id'];
                                    }
                                    // echo "<pre>";print_r($query_details_insert_data);echo"</pre><hr>";exit;
                                    $this->common_model->insert_data_sales_db('query_details',$query_details_insert_data);
                                }
                            } else {
                                
                                $query_details_insert_data =  array();
                                $query_details_insert_data['query_number'] = $query_master_details['query_number'];
                                $query_details_insert_data['query_text'] = $form_data['query_text'];
                                $query_details_insert_data['add_time'] = date("Y-m-d H:i:s");

                                if(in_array($this->session->userdata('role'),array(6, 8))){
                                    $this->send_query_sms($form_data['query_assigned_id'], $form_data['query_creator_id'], 'RFQ No '.$form_data['query_reference']);
                                    $query_details_insert_data['query_creator_id'] = $form_data['query_creator_id'];
                                    $query_details_insert_data['query_assigned_id'] = $form_data['query_assigned_id'];

                                    $this->common_model->update_data_sales_db('query_master', array('query_text'=>$form_data['query_text'], 'query_assigned_id'=> $form_data['query_assigned_id'], 'query_creator_id'=> $form_data['query_creator_id'], 'query_add_time'=> date("Y-m-d H:i:s"), 'reply_text'=>'', 'reply_creator_id'=>0, 'reply_assigned_id'=>0, 'reply_add_time'=> NULL, 'query_status'=> 'open'), array('id'=> $query_master_details['id']));

                                } else if(in_array($this->session->userdata('role'),array(1, 5, 16))){
                                    $this->send_query_sms($form_data['query_creator_id'], $form_data['query_assigned_id'], 'RFQ No '.$form_data['query_reference'], 'answered');
                                    $query_details_insert_data['query_creator_id'] = $form_data['query_assigned_id'];
                                    $query_details_insert_data['query_assigned_id'] = $form_data['query_creator_id'];

                                    $this->common_model->update_data_sales_db('query_master', array('reply_text'=>$form_data['query_text'], 'reply_assigned_id'=> $form_data['query_assigned_id'], 'reply_creator_id'=> $form_data['query_creator_id'], 'reply_add_time'=> date("Y-m-d H:i:s"), 'query_status'=> 'open'), array('id'=> $query_master_details['id']));
                                }
                                // echo "<pre>";print_r($query_details_insert_data);echo"</pre><hr>";exit;
                                $this->common_model->insert_data_sales_db('query_details',$query_details_insert_data);
                            }
                            $response['status'] = 'successful';
                            $response['message'] = "Successfully added RFQ Query.";
                        }
                    }
                break;
                case 'query_overdue_sms_alert':
                    
                    if(!empty($this->input->post('assigned_id')) && !empty($this->input->post('sender_id'))) {

                        $query_assigned_details = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>1, 'user_id'=> $this->input->post('assigned_id')), 'users', 'row_array');
                        $query_sender_details = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>1, 'user_id'=> $this->input->post('sender_id')), 'users', 'row_array');
                        $sms_txt = 'Hi '.$query_assigned_details['name'].', '.$query_sender_details['name'].' has pinged you to solve a query. - Om Tubes';
                        // $this->sendSms('9082159156', str_replace(' ', '%20', $sms_txt), '1307164551142407208');
                        $this->sendSms($query_assigned_details['mobile'], str_replace(' ', '%20', $sms_txt), '1307164551142407208');
                    }
                break;
                case 'change_query_tab':

                    $search_filter_data = $this->create_search_filter_data($this->input->post('form_data'), $this->input->post('main_tab_name'), $this->input->post('sub_tab_name'));

                    if($this->input->post('sub_tab_name') == 'closed'){

                        $data = $this->prepare_data_for_query($search_filter_data['where'], 10, 0);
                    }else{

                        $data = $this->prepare_data_for_query($search_filter_data['where'], 0, 0);
                    }
                    $data['tab_name'] = $this->input->post('main_tab_name');

                    // echo "<pre>";print_r($data);echo"</pre><hr>";exit;
                    $response['search_filter'] = $this->load->view('query/query_search_filter', $data, true);
                    $response['table_body'] = $this->load->view('query/query_list_table_body', $data, true);
                    $response['table_paggination'] = $this->load->view('query/query_paggination', $data, true);
                break;
                case 'query_paggination':
                case 'query_search_filter':

                    // echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
                    $search_filter_data = $this->create_search_filter_data($this->input->post('form_data'), $this->input->post('main_tab_name'), $this->input->post('sub_tab_name'));

                    $data = $this->prepare_data_for_query($search_filter_data['where'], $this->input->post('limit'), $this->input->post('offset'));
                    $data['tab_name'] = $this->input->post('main_tab_name');

                    // echo "<pre>";print_r($data);echo"</pre><hr>";exit;

                    $response['search_filter'] = $this->load->view('query/query_search_filter', $data, true);
                    $response['table_body'] = $this->load->view('query/query_list_table_body', $data, true);
                    $response['table_paggination'] = $this->load->view('query/query_paggination', $data, true);
                    // echo "<pre>";print_r($response;echo"</pre><hr>";exit;
                break;
                case 'query_assigned_highchart':

                    $response['query_highchart_data'][0] = array();
                    $response['query_highchart_data'][0]['name'] = 'OPEN';
                    $response['query_highchart_data'][0]['color'] = '#69a9f7';
                    $response['query_highchart_data'][1] = array();
                    $response['query_highchart_data'][1]['name'] = 'DONE';
                    $response['query_highchart_data'][1]['color'] = '#13c55a';
                    $response['query_highchart_data'][2] = array();
                    $response['query_highchart_data'][2]['name'] = 'CLOSED';
                    $response['query_highchart_data'][2]['color'] = '#d75c5c';

                    $response['sub_query_highchart_data'][0] = array();
                    $response['sub_query_highchart_data'][0]['name'] = 'OPEN';
                    $response['sub_query_highchart_data'][0]['color'] = '#69a9f7';
                    $response['sub_query_highchart_data'][1] = array();
                    $response['sub_query_highchart_data'][1]['name'] = 'DONE';
                    $response['sub_query_highchart_data'][1]['color'] = '#13c55a';
                    $response['sub_query_highchart_data'][2] = array();
                    $response['sub_query_highchart_data'][2]['name'] = 'CLOSED';
                    $response['sub_query_highchart_data'][2]['color'] = '#d75c5c';

                    $user=array_column($this->common_model->get_dynamic_data_sales_db('name, user_id', array('status'=>1), 'users'), 'name', 'user_id');
                    $query_list_details = $user_id_under_this_role = array();
                    if(in_array($this->session->userdata('role'), array(1, 4, 5, 6, 7, 8, 10, 11, 16, 17, 18, 21, 24))) {
                        $user_id_under_this_role = $this->query_model->user_id_under_this_role($this->session->userdata('role'));
                    }
                    if($this->input->post('main_tab_name') == 'sample_query'){
                        $main_tab_name = "('quotation_sample_query', 'lead_sample_query')";
                    }else{
                        $main_tab_name = "('".$this->input->post('main_tab_name')."')";
                    }
                    $query_list_details =$this->query_model->query_list($main_tab_name, $this->input->post('sub_tab_name'), $user_id_under_this_role);
                    foreach ($query_list_details as $query_details) {
                        if(!empty($user[$query_details['query_assigned_id']])){ 

                            if($query_details['done'] > 0){
                                $query_details['open'] = $query_details['open'] - $query_details['done'];
                            }
                            $response['query_highchart_category'][] = $user[$query_details['query_assigned_id']];
                            $response['query_highchart_data'][0]['data'][] = (int)$query_details['open'];
                            $response['query_highchart_data'][1]['data'][] = (int)$query_details['done'];
                            $response['query_highchart_data'][2]['data'][] = (int)$query_details['closed'];
                        }                        
                    }

                    $sub_query_list_details =$this->query_model->get_sub_query_list($main_tab_name, $this->input->post('sub_tab_name'), $user_id_under_this_role);
                    if(!empty($sub_query_list_details)){
                        foreach ($sub_query_list_details as $sub_query_detail) {

                            if($sub_query_detail['done'] > 0){
                                $sub_query_detail['open'] = $sub_query_detail['open'] - $sub_query_detail['done'];
                            }
                            $response['sub_query_highchart_category'][] = $sub_query_detail['sub_query_type'];
                            $response['sub_query_highchart_data'][0]['data'][] = (int)$sub_query_detail['open'];
                            $response['sub_query_highchart_data'][1]['data'][] = (int)$sub_query_detail['done'];
                            $response['sub_query_highchart_data'][2]['data'][] = (int)$sub_query_detail['closed'];
                        }
                    }
                break;
                case'close_query':
                    if(!empty($this->input->post('query_id'))){
                         $this->common_model->update_data_sales_db('query_master',array('query_status'=>'closed'), array('id'=>$this->input->post('query_id')));
                    }
                break;
                case 'delete_query':
                    if(!empty($this->input->post('query_id'))){
                             $this->common_model->update_data_sales_db('query_master',array('status'=>'Inactive'), array('id'=>$this->input->post('query_id')));
                        }
                break;
                case 'reopen_query':
                    if(!empty($this->input->post('query_id'))){
                             $this->common_model->update_data_sales_db('query_master',array('query_status'=>'open'), array('id'=>$this->input->post('query_id')));
                        }
                break;
                case 'upload_mtc_drawing':
                    
                    $response['msg'] = 'File is uploaded successfully!!!';
                    if(!empty($this->session->userdata('query_number'))){

                        $query_details = $this->common_model->get_dynamic_data_sales_db('id, query_reference',array('query_number'=>$this->session->userdata('query_number')), 'query_master', 'row_array');
                        if(!empty($query_details)){

                            $return_response = $this->upload_doc_config($this->input->post('pdf_type'), $query_details['query_reference']);
                        }
                    }
					$response['status'] = $return_response['status'];
					if($return_response['status'] == 'successful') {

						$data = $this->common_model->get_dynamic_data_sales_db('value',array('query_number'=>$this->session->userdata('query_number'), 'name'=> $this->input->post('pdf_type').'_pdf'), 'query_other_information', 'row_array');
                        $update_array = $return_response['file_name'];
						if(empty($data['value'])) {

                            $this->common_model->insert_data_sales_db('query_other_information', array('query_number'=>$this->session->userdata('query_number'), 'name'=> $this->input->post('pdf_type').'_pdf', 'value'=> $update_array, 'created_by'=>$this->session->userdata('user_id')));
                            
						}else{

                            $this->common_model->update_data_sales_db('query_other_information', array('value'=> $data['value'].','.$return_response['file_name']), array('query_number'=>$this->session->userdata('query_number'), 'name'=> $this->input->post('pdf_type').'_pdf', 'created_by'=>$this->session->userdata('user_id')));
                        }
                        
					}else {

						$response['msg'] = 'File is not uploaded.';
					}
                break;
                case 'get_upload_history':
				
					$query_number = (!empty($this->input->post('query_number'))) ? $this->input->post('query_number') : $this->session->userdata('query_number');			
					if(!empty($query_number)) {

						$data['pdf_details'] = $this->common_model->get_dynamic_data_sales_db('name as pdf_name, value, add_time',array('query_number'=>$query_number, 'name'=>'MTC_pdf'), 'query_other_information', 'result_array');
						$response['pdf_upload_history_mtc'] = $this->load->view('query/pdf_table', $data, true);

                        $data['pdf_details'] = $this->common_model->get_dynamic_data_sales_db('name as pdf_name, value, add_time',array('query_number'=>$query_number, 'name'=>'Drawing_pdf'), 'query_other_information', 'result_array');
						$response['pdf_upload_history_drawing'] = $this->load->view('query/pdf_table', $data, true);

						// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
						$this->session->set_userdata('query_number', $query_number);
					}
				break;
                case 'save_query_comment_form_details':
                    
                    // echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
                    $response['status'] = 'failed';
                    if(!empty($this->input->post('query_master_id'))){

                        $update_array['comments'] = $this->input->post('comments');
                        $update_array['comment_creator_id'] = $this->session->userdata('user_id');
                        $update_array['comment_add_time'] = date("Y-m-d H:i:s");
                        
                        $this->common_model->update_data_sales_db('query_master', $update_array, array('id'=> $this->input->post('query_master_id')));
                        $response['status'] = 'successful';
                    }
                break;
                case 'save_sample_query':

                    $form_data = array_column($this->input->post('form_data'),'value','name');
                    // echo "<pre>";print_r($form_data);echo"</pre><hr>";exit;
                    $response['status'] = 'failed';
                    $response['message'] = "Form Data Not Found. Please Contact Developer";
                    if(!empty($form_data)){
                        if(empty($form_data['query_text'])) {

                            $response['message'] = "Please Enter Query!!!";
                        } else if($form_data['query_assigned_id'] == 'blank') {

                            $response['message'] = "Please Select Query Type!!!";
                        } else {

                            //checking record exist or not in query master
                            $query_master_details = $this->common_model->get_dynamic_data_sales_db('id, query_number', array('status'=>'Active', 'query_type'=>$form_data['query_type'],'query_reference_id'=>$form_data['query_reference_id']), 'query_master', 'row_array');
                            if($form_data['query_type'] == "lead_sample_query"){

                                $client_details = $this->common_model->get_dynamic_data_sales_db('id, name', array('id'=>$form_data['query_reference_id']), 'customer_mst', 'row_array');
                            }
                            if(empty($query_master_details)){

                                //create insert array for query master
                                $query_master_insert_data = $form_data;
                                $last_query_id = $this->common_model->get_dynamic_data_sales_db('last_quote_id', array('status'=> "Active", 'quote_name'=>$form_data['query_type']), 'last_quote_created_number', 'row_array');

                                if($form_data['query_type'] == "quotation_sample_query"){

                                    $query_master_insert_data['query_number'] = 'QS'.(((int)$last_query_id['last_quote_id'])+1);
                                    $query_master_insert_data['sub_query_type'] = 'Quotation';
                                }
                                if($form_data['query_type'] == "lead_sample_query"){

                                    $query_master_insert_data['query_number'] = 'LS'.(((int)$last_query_id['last_quote_id'])+1);
                                    $query_master_insert_data['sub_query_type'] = 'Lead';
                                }
                                $query_master_insert_data['query_add_time'] = date("Y-m-d H:i:s");
                                $query_master_insert_data['add_time'] = date("Y-m-d H:i:s");
                                $query_master_id = $this->common_model->insert_data_sales_db('query_master',$query_master_insert_data);

                                //  echo "<pre>";print_r($query_master_id);echo"</pre><hr>";exit;
                                if(!empty($query_master_id)){

                                    $this->common_model->update_data_sales_db('last_quote_created_number', array('last_quote_id'=>(((int)$last_query_id['last_quote_id'])+1)), array('status'=> "Active", 'quote_name'=>$form_data['query_type']));
                                    //create insert array for query details
                                    $query_details_insert_data =  array();
                                    $query_details_insert_data['query_number'] = $query_master_insert_data['query_number'];
                                    $query_details_insert_data['query_text'] = $form_data['query_text'];
                                    $query_details_insert_data['add_time'] = date("Y-m-d H:i:s");
                                    if(in_array($this->session->userdata('role'),array(5,16,1))){

                                        if($form_data['query_type'] == "quotation_sample_query"){
                                            $this->send_query_sms($form_data['query_assigned_id'], $form_data['query_creator_id'], 'Sample Quotation No '.$form_data['query_reference']);
                                        }else{

                                            $this->send_query_sms($form_data['query_assigned_id'], $form_data['query_creator_id'], 'Sample Client Name '.$client_details['name']);
                                        }
                                        $query_details_insert_data['query_creator_id'] = $form_data['query_creator_id'];
                                        $query_details_insert_data['query_assigned_id'] = $form_data['query_assigned_id'];
                                    } else if(in_array($this->session->userdata('role'),array(4, 6, 8, 18))){

                                        if($form_data['query_type'] == "quotation_sample_query"){
                                            $this->send_query_sms($form_data['query_creator_id'], $form_data['query_assigned_id'], 'Sample Quotation No '.$form_data['query_reference']);
                                        }else{

                                            $this->send_query_sms($form_data['query_creator_id'], $form_data['query_assigned_id'], 'Sample Client Name '.$client_details['name']);
                                        }
                                        $query_details_insert_data['query_creator_id'] = $form_data['query_assigned_id'];
                                        $query_details_insert_data['query_assigned_id'] = $form_data['query_creator_id'];
                                    }
                                    // echo "<pre>";print_r($query_details_insert_data);echo"</pre><hr>";exit;
                                    $this->common_model->insert_data_sales_db('query_details',$query_details_insert_data);
                                }
                            }else{
                                //create insert array for query details
                                $query_details_insert_data =  array();
                                $query_details_insert_data['query_number'] = $query_master_details['query_number'];
                                $query_details_insert_data['query_text'] = $form_data['query_text'];
                                $query_details_insert_data['add_time'] = date("Y-m-d H:i:s");
                                if(in_array($this->session->userdata('role'),array(1, 5, 16))){
                                    if($form_data['query_type'] == "quotation_sample_query"){
                                        $this->send_query_sms($form_data['query_assigned_id'], $form_data['query_creator_id'], 'Sample Quotation No'.$form_data['query_reference']);
                                    }else{

                                        $this->send_query_sms($form_data['query_assigned_id'], $form_data['query_creator_id'], 'Sample Client Name '.$client_details['name']);
                                    }
                                    $query_details_insert_data['query_creator_id'] = $form_data['query_creator_id'];
                                    $query_details_insert_data['query_assigned_id'] = $form_data['query_assigned_id'];
                                    $this->common_model->update_data_sales_db('query_master', array('query_text'=>$form_data['query_text'], 'query_assigned_id'=> $form_data['query_assigned_id'], 'query_creator_id'=> $form_data['query_creator_id'], 'query_add_time'=> date("Y-m-d H:i:s"), 'reply_text'=>'', 'reply_creator_id'=>0, 'reply_assigned_id'=>0, 'reply_add_time'=> NULL, 'query_status'=> 'open'), array('id'=> $query_master_details['id']));
                                } else if(in_array($this->session->userdata('role'),array(4, 6, 8, 18))){
                                    if($form_data['query_type'] == "quotation_sample_query"){

                                        $this->send_query_sms($form_data['query_creator_id'], $form_data['query_assigned_id'], 'Sample Quotation No'.$form_data['query_reference'], 'answered');
                                    }else{

                                        $this->send_query_sms($form_data['query_creator_id'], $form_data['query_assigned_id'], 'Sample Client Name '.$client_details['name'], 'answered');
                                    }
                                    $query_details_insert_data['query_creator_id'] = $form_data['query_assigned_id'];
                                    $query_details_insert_data['query_assigned_id'] = $form_data['query_creator_id'];
                                    $this->common_model->update_data_sales_db('query_master', array('reply_text'=>$form_data['query_text'], 'reply_assigned_id'=> $form_data['query_creator_id'], 'reply_creator_id'=> $form_data['query_assigned_id'], 'reply_add_time'=> date("Y-m-d H:i:s"), 'query_status'=> 'open'), array('id'=> $query_master_details['id']));
                                }
                                // echo "<pre>";print_r($query_details_insert_data);echo"</pre><hr>";exit;
                                $this->common_model->insert_data_sales_db('query_details',$query_details_insert_data);
                            }
                            $response['status'] = 'successful';
                            $response['message'] = "Successfully added Sales Query.";
                        }
                    }
                break;
                case 'get_sample_query_history':

                    //  echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
                    $data = array();
                    $data['user_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array(), 'users'), 'name', 'user_id');

                    $response['reference_id'] = 0;
                    if($this->input->post('sample_type') == "quotation_sample_query"){

                        $sample_assign_details = $this->query_model->query_quotation_assign_details($this->input->post('sample_id'));

                        if(!empty($sample_assign_details)){

                            $response['reference_id'] = $sample_assign_details['quotation_id'];
                            $response['reference'] = $sample_assign_details['quote_no'];
                            $response['sales_person_id'] = $sample_assign_details['sales_person_id'];
                            $response['sample_type'] = $this->input->post('sample_type');
                            $response['query_type_option_tag'] = '';
                            if(in_array($this->session->userdata('role'),array(1, 5, 16, 17))){
                                $response['query_type_option_tag'] = '<select class="form-control kt-selectpicker" name="query_assigned_id"><optgroup label="" data-max-options="2"><option value="38">'.$data['user_details'][38].'</option></option></optgroup></select>';
                            } else if(in_array($this->session->userdata('role'),array(4, 6, 8, 18))){
                                $response['query_type_option_tag'] = '<select class="form-control kt-selectpicker" name="query_assigned_id" readonly><optgroup label="" data-max-options="2"><option value="'.$this->session->userdata('user_id').'" selected>'.$this->session->userdata('name').'</option></optgroup>';
                            }

                            $query_master_details = $this->common_model->get_dynamic_data_sales_db('id, query_number', array('status'=>'Active', 'query_type'=>$this->input->post('sample_type'),'query_reference_id'=>$sample_assign_details['quotation_id']), 'query_master', 'row_array');
                        }
                    }else if($this->input->post('sample_type') == "lead_sample_query"){

                        $sample_assign_details = $this->query_model->query_lead_assign_details($this->input->post('sample_id'));
                        //  echo "<pre>";print_r($sample_assign_details);echo"</pre><hr>";exit;

                        if(!empty($sample_assign_details)){

                            $response['reference_id'] = $sample_assign_details['id'];
                            $response['reference'] = $sample_assign_details['product_name'];
                            $response['sales_person_id'] = $sample_assign_details['assigned_to'];
                            $response['sample_type'] = $this->input->post('sample_type');
                            $response['query_type_option_tag'] = '';
                            if(in_array($this->session->userdata('role'),array(1, 5, 16, 17))){
                                $response['query_type_option_tag'] = '<select class="form-control kt-selectpicker" name="query_assigned_id"><optgroup label="Procurement" data-max-options="2"><option value="38">'.$data['user_details'][38].'</option></option></optgroup></select>';
                            } else if(in_array($this->session->userdata('role'),array(4, 6, 8, 18))){
                                $response['query_type_option_tag'] = '<select class="form-control kt-selectpicker" name="query_assigned_id" readonly><optgroup label="" data-max-options="2"><option value="'.$this->session->userdata('user_id').'" selected>'.$this->session->userdata('name').'</option></optgroup>';
                            }

                            $query_master_details = $this->common_model->get_dynamic_data_sales_db('id, query_number', array('status'=>'Active', 'query_type'=>$this->input->post('sample_type'),'query_reference_id'=>$sample_assign_details['id']), 'query_master', 'row_array');
                        }
                    }

                    if(!empty($query_master_details)) {

                        $data['conversation_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'query_number'=>$query_master_details['query_number']), 'query_details');
                        if(!empty($data['conversation_details'])){

                            foreach ($data['conversation_details'] as $conversation_details_key => $single_chat) {

                                $data['conversation_details'][$conversation_details_key]['tat'] = $this->creat_tat($single_chat['add_time']);
                            }
                        }
                        $data['people_information_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'), 'people_information'), 'profile_pic_file_path', 'user_id');
                    }

                    $response['query_history_html'] = $this->load->view('query/chat_conversation_v2', $data, true);
                    // echo "<pre>";print_r($response);echo"</pre><hr>";exit;
                break;
                case 'save_pq_and_lead_query':
                    // echo "<pre>";print_r($this->input->post('form_data'));echo"</pre><hr>";exit;
                    $form_data = array_column($this->input->post('form_data'),'value','name');
                    // echo "<pre>";print_r($form_data);echo"</pre><hr>";exit;
                    $response['status'] = 'failed';
                    $response['message'] = "Form Data Not Found. Please Contact Developer";
                    if(!empty($form_data)){
                        if(empty($form_data['query_text'])) {

                            $response['message'] = "Please Enter Query!!!";
                        } else if($form_data['query_assigned_id'] == 'blank') {

                            $response['message'] = "Please Select Query Type!!!";
                        } else {

                            //checking record exist or not in query master
                            $query_master_details = $this->common_model->get_dynamic_data_sales_db('id, query_number', array('status'=>'Active', 'query_type'=>$form_data['query_type'],'query_reference_id'=>$form_data['query_reference_id']), 'query_master', 'row_array');

                            $client_details = $this->common_model->get_dynamic_data_sales_db('id, name', array('status'=>'Active', 'id'=>$form_data['query_reference_id']), 'customer_mst', 'row_array');
                            if(empty($query_master_details)){

                                //create insert array for query master
                                $query_master_insert_data = $form_data;
                                $last_query_id = $this->common_model->get_dynamic_data_sales_db('last_quote_id', array('status'=> "Active", 'quote_name'=>$form_data['query_type']), 'last_quote_created_number', 'row_array');

                                if($form_data['query_type'] == 'pq_query'){

                                    $query_master_insert_data['query_number'] = 'PQQ'.(((int)$last_query_id['last_quote_id'])+1);
                                }else if($form_data['query_type'] == 'lead_query'){

                                    $query_master_insert_data['query_number'] = 'LDQ'.(((int)$last_query_id['last_quote_id'])+1);
                                }

                                $query_master_insert_data['query_add_time'] = date("Y-m-d H:i:s");
                                $query_master_insert_data['add_time'] = date("Y-m-d H:i:s");
                                $query_master_id = $this->common_model->insert_data_sales_db('query_master',$query_master_insert_data);

                                //  echo "<pre>";print_r($query_master_id);echo"</pre><hr>";exit;
                                if(!empty($query_master_id)){

                                    $this->common_model->update_data_sales_db('last_quote_created_number', array('last_quote_id'=>(((int)$last_query_id['last_quote_id'])+1)), array('status'=> "Active", 'quote_name'=>$form_data['query_type']));
                                    //create insert array for query details
                                    $query_details_insert_data =  array();
                                    $query_details_insert_data['query_number'] = $query_master_insert_data['query_number'];
                                    $query_details_insert_data['query_text'] = $form_data['query_text'];
                                    $query_details_insert_data['add_time'] = date("Y-m-d H:i:s");

                                    if($form_data['query_type'] == 'pq_query'){

                                    if(in_array($this->session->userdata('role'),array(1, 5, 16))){

                                        $this->send_query_sms($form_data['query_assigned_id'], $form_data['query_creator_id'], 'PQ Client Name '.$client_details['name']);
                                        $query_details_insert_data['query_creator_id'] = $form_data['query_creator_id'];
                                        $query_details_insert_data['query_assigned_id'] = $form_data['query_assigned_id'];
                                    } else if(in_array($this->session->userdata('role'),array(1, 7))){

                                        $this->send_query_sms($form_data['query_creator_id'], $form_data['query_assigned_id'], 'PQ Client Name '.$client_details['name']);
                                        $query_details_insert_data['query_creator_id'] = $form_data['query_assigned_id'];
                                        $query_details_insert_data['query_assigned_id'] = $form_data['query_creator_id'];
                                    }
                                    }else if($form_data['query_type'] == 'lead_query'){

                                        if(in_array($this->session->userdata('role'),array(5, 16))){

                                            $this->send_query_sms($form_data['query_assigned_id'], $form_data['query_creator_id'], 'Client Name '.$client_details['name']);
                                            $query_details_insert_data['query_creator_id'] = $form_data['query_creator_id'];
                                            $query_details_insert_data['query_assigned_id'] = $form_data['query_assigned_id'];
                                        } else if(in_array($this->session->userdata('role'),array(1, 6, 7, 8, 16, 10, 11, 14, 18))){

                                            $this->send_query_sms($form_data['query_creator_id'], $form_data['query_assigned_id'], 'Client Name '.$client_details['name']);
                                            $query_details_insert_data['query_creator_id'] = $form_data['query_assigned_id'];
                                            $query_details_insert_data['query_assigned_id'] = $form_data['query_creator_id'];
                                        }
                                    }
                                    // echo "<pre>";print_r($query_details_insert_data);echo"</pre><hr>";exit;
                                    $this->common_model->insert_data_sales_db('query_details',$query_details_insert_data);
                                }
                            }else{
                                //create insert array for query details
                                $query_details_insert_data =  array();
                                $query_details_insert_data['query_number'] = $query_master_details['query_number'];
                                $query_details_insert_data['query_text'] = $form_data['query_text'];
                                $query_details_insert_data['add_time'] = date("Y-m-d H:i:s");

                                if($form_data['query_type'] == 'pq_query'){

                                if(in_array($this->session->userdata('role'),array(1, 5, 16))){

                                    $this->send_query_sms($form_data['query_assigned_id'], $form_data['query_creator_id'], 'PQ Client Name '.$client_details['name']);
                                    $query_details_insert_data['query_creator_id'] = $form_data['query_creator_id'];
                                    $query_details_insert_data['query_assigned_id'] = $form_data['query_assigned_id'];

                                    $this->common_model->update_data_sales_db('query_master', array('query_text'=>$form_data['query_text'], 'query_assigned_id'=> $form_data['query_assigned_id'], 'query_creator_id'=> $form_data['query_creator_id'], 'sub_query_type'=> $form_data['sub_query_type'],'query_add_time'=> date("Y-m-d H:i:s"), 'reply_text'=>'', 'reply_creator_id'=>0, 'reply_assigned_id'=>0, 'reply_add_time'=> NULL, 'query_status'=> 'open'), array('id'=> $query_master_details['id']));

                                } else if(in_array($this->session->userdata('role'),array(1, 7))){

                                    $this->send_query_sms($form_data['query_creator_id'], $form_data['query_assigned_id'], 'PQ Client Name '.$client_details['name'], 'answered');
                                    $query_details_insert_data['query_creator_id'] = $form_data['query_assigned_id'];
                                    $query_details_insert_data['query_assigned_id'] = $form_data['query_creator_id'];

                                    $this->common_model->update_data_sales_db('query_master', array('reply_text'=>$form_data['query_text'], 'reply_assigned_id'=> $form_data['query_creator_id'], 'reply_creator_id'=> $form_data['query_assigned_id'], 'reply_add_time'=> date("Y-m-d H:i:s"), 'query_status'=> 'open'), array('id'=> $query_master_details['id']));
                                }
                                }else if($form_data['query_type'] == 'lead_query'){

                                    if(in_array($this->session->userdata('role'),array(5, 16))){

                                        $this->send_query_sms($form_data['query_assigned_id'], $form_data['query_creator_id'], 'Client Name '.$client_details['name']);
                                        $query_details_insert_data['query_creator_id'] = $form_data['query_creator_id'];
                                        $query_details_insert_data['query_assigned_id'] = $form_data['query_assigned_id'];

                                        $this->common_model->update_data_sales_db('query_master', array('query_text'=>$form_data['query_text'], 'query_assigned_id'=> $form_data['query_assigned_id'], 'query_creator_id'=> $form_data['query_creator_id'], 'sub_query_type'=> $form_data['sub_query_type'],'query_add_time'=> date("Y-m-d H:i:s"), 'reply_text'=>'', 'reply_creator_id'=>0, 'reply_assigned_id'=>0, 'reply_add_time'=> NULL, 'query_status'=> 'open'), array('id'=> $query_master_details['id']));

                                    }else if(in_array($this->session->userdata('role'),array(1, 6, 7, 8, 16, 10, 11, 14, 18))){

                                        $this->send_query_sms($form_data['query_creator_id'], $form_data['query_assigned_id'], 'Client Name '.$client_details['name'], 'answered');
                                        $query_details_insert_data['query_creator_id'] = $form_data['query_assigned_id'];
                                        $query_details_insert_data['query_assigned_id'] = $form_data['query_creator_id'];

                                        $this->common_model->update_data_sales_db('query_master', array('reply_text'=>$form_data['query_text'], 'reply_assigned_id'=> $form_data['query_creator_id'], 'reply_creator_id'=> $form_data['query_assigned_id'], 'reply_add_time'=> date("Y-m-d H:i:s"), 'query_status'=> 'open'), array('id'=> $query_master_details['id']));
                                    }
                                }

                                // echo "<pre>";print_r($query_details_insert_data);echo"</pre><hr>";exit;
                                $this->common_model->insert_data_sales_db('query_details',$query_details_insert_data);
                            }
                            $response['status'] = 'successful';
                            $response['message'] = "Successfully added PQ Query.";
                        }
                    }
                break;
                case 'get_pq_and_lead_query_history':
                    // echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
                    $data = array();
                    $data['user_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array(), 'users'), 'name', 'user_id');
                    $lead_assign_details = $this->query_model->query_lead_assign_details($this->input->post('client_id'));
                    // echo "<pre>";print_r($lead_assign_details);echo"</pre><hr>";exit;
                    $response['reference_id'] = 0;
                    if(!empty($lead_assign_details)){

                        $response['reference_id'] = $lead_assign_details['id'];
                        $response['reference'] = $lead_assign_details['product_name'];
                        $response['sales_person_id'] = $lead_assign_details['assigned_to'];
                        $response['query_type'] = $this->input->post('query_type');
                        $response['query_type_option_tag'] = '';

                        if($this->input->post('query_type') == 'pq_query'){
                        if(in_array($this->session->userdata('role'),array(1, 5, 16, 17))){
                            $response['query_assigned_id'] = 267;
                            $response['query_type_option_tag'] = '
                            <select class="form-control kt-selectpicker" name="sub_query_type">
                                <optgroup label="PQ" data-max-options="2">
                                    <option value="blank" selected>Select Query Type</option>
                                    <option value="Registration">Registration ('.$data['user_details'][267].')</option>
                                </optgroup>
                            </select>';

                        } else if(in_array($this->session->userdata('role'),array(1, 7))){

                            $response['query_assigned_id'] = $this->session->userdata('user_id');
                            $response['query_type_option_tag'] = '
                            <select class="form-control kt-selectpicker" name="sub_query_type" readonly>
                                <optgroup label="PQ" data-max-options="2">
                                    <option value="">'.$this->session->userdata('name').'</option>
                                </optgroup>
                            </select>';
                        }
                        }else if($this->input->post('query_type') == 'lead_query'){
                            if(in_array($this->session->userdata('role'),array(5, 16, 17))){
                                $response['query_assigned_id'] = 2;
                                $response['query_type_option_tag'] = '
                                <select class="form-control kt-selectpicker" name="sub_query_type">
                                    <optgroup label="Lead Query" data-max-options="2">
                                        <option value="blank" selected>Select Query Type</option>
                                        <option value="Other">Other ('.$data['user_details'][2].')</option>
                                    </optgroup>
                                </select>';
                            } else if(in_array($this->session->userdata('role'),array(1, 6, 7, 8, 16, 10, 11, 14, 18))){

                                $response['query_assigned_id'] = $this->session->userdata('user_id');
                                $response['query_type_option_tag'] = '
                                <select class="form-control kt-selectpicker" name="sub_query_type" readonly>
                                    <optgroup label="" data-max-options="2">
                                        <option value="">'.$this->session->userdata('name').'</option>
                                    </optgroup>
                                </select>';
                            }
                        }else{
                            $response['query_assigned_id'] = 267;
                            $response['query_type_option_tag'] = '
                            <select class="form-control kt-selectpicker" name="sub_query_type">
                                <optgroup label="PQ" data-max-options="2">
                                    <option value="blank" selected>Select Query Type</option>
                                    <option value="Registration">Registration ('.$data['user_details'][267].')</option>
                                </optgroup>
                                <optgroup label="Lead" data-max-options="2">
                                    <option value="Other">Other ('.$data['user_details'][2].')</option>
                                </optgroup>
                            </select>';
                        }

                        $query_master_details = $this->common_model->get_dynamic_data_sales_db('id, query_number', array('status'=>'Active', 'query_type'=>$this->input->post('query_type'),'query_reference_id'=>$lead_assign_details['id']), 'query_master', 'row_array');

                        if(!empty($query_master_details)) {

                            $data['conversation_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'query_number'=>$query_master_details['query_number']), 'query_details');
                            if(!empty($data['conversation_details'])){

                                foreach ($data['conversation_details'] as $conversation_details_key => $single_chat) {

                                    $data['e2'][$conversation_details_key]['tat'] = $this->creat_tat($single_chat['add_time']);
                                }
                            }
                            $data['people_information_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'), 'people_information'), 'profile_pic_file_path', 'user_id');
                        }
                    }

                    // echo "<pre>";print_r($data);echo"</pre><hr>";exit;
                    $response['query_history_html'] = $this->load->view('query/chat_conversation_v2', $data, true);

                break;
                case 'save_osdr_query':

                    // echo "<pre>";print_r($this->input->post('form_data'));echo"</pre><hr>";exit;
                    $form_data = array_column($this->input->post('form_data'),'value','name');
                    $response['status'] = 'failed';
                    $response['message'] = "Form Data Not Found. Please Contact Developer";
                    if(!empty($form_data)){
                        if(empty($form_data['query_text'])) {

                            $response['message'] = "Please Enter Query!!!";
                        } else if($form_data['query_assigned_id'] == 'blank') {

                            $response['message'] = "Please Select Assigned To !!!";
                        } else {

                            //checking record exist or not in query master
                            $query_master_details = $this->common_model->get_dynamic_data_sales_db('id, query_number, query_creator_id, query_assigned_id', array('status'=>'Active', 'query_type'=>'osdr_query','query_reference_id'=>$form_data['query_reference_id']), 'query_master', 'row_array');
                            if(empty($query_master_details)){

                                //create insert array for query master
                                $query_master_insert_data = $form_data;
                                $last_query_id = $this->common_model->get_dynamic_data_sales_db('last_quote_id', array('status'=> "Active", 'quote_name'=>'osdr_query'), 'last_quote_created_number', 'row_array');
                                $query_master_insert_data['query_number'] = 'OSDRQ'.(((int)$last_query_id['last_quote_id'])+1);
                                $query_master_insert_data['query_add_time'] = date("Y-m-d H:i:s");
                                $query_master_insert_data['add_time'] = date("Y-m-d H:i:s");
                                $query_master_insert_data['sub_query_type'] = 'OSDR';

                                $query_master_id = $this->common_model->insert_data_sales_db('query_master',$query_master_insert_data);
                                if(!empty($query_master_id)){

                                    $this->common_model->update_data_sales_db('last_quote_created_number', array('last_quote_id'=>(((int)$last_query_id['last_quote_id'])+1)), array('status'=> "Active", 'quote_name'=>'osdr_query'));
                                    //create insert array for query details
                                    $query_details_insert_data =  array();
                                    $query_details_insert_data['query_number'] = $query_master_insert_data['query_number'];
                                    $query_details_insert_data['query_text'] = $form_data['query_text'];
                                    $query_details_insert_data['add_time'] = date("Y-m-d H:i:s");

                                    if(in_array($this->session->userdata('role'),array(1, 5, 16))){

                                        $this->send_query_sms($form_data['query_assigned_id'], $form_data['query_creator_id'], 'Work Order No '.$form_data['query_reference']);
                                        $query_details_insert_data['query_creator_id'] = $form_data['query_creator_id'];
                                        $query_details_insert_data['query_assigned_id'] = $form_data['query_assigned_id'];
                                    }else if(in_array($this->session->userdata('role'),array(1, 6, 7, 8, 10, 11, 24))){

                                        $this->send_query_sms($form_data['query_creator_id'], $form_data['query_assigned_id'], 'Work Order No '.$form_data['query_reference']);
                                        $query_details_insert_data['query_creator_id'] = $form_data['query_assigned_id'];
                                        $query_details_insert_data['query_assigned_id'] = $form_data['query_creator_id'];
                                    }

                                    $this->common_model->insert_data_sales_db('query_details',$query_details_insert_data);
                                }
                            }else{
                                //create insert array for query details
                                $query_details_insert_data =  array();
                                $query_details_insert_data['query_number'] = $query_master_details['query_number'];
                                $query_details_insert_data['query_text'] = $form_data['query_text'];
                                $query_details_insert_data['add_time'] = date("Y-m-d H:i:s");

                                if(in_array($this->session->userdata('role'),array(1, 5, 16))){

                                    $this->send_query_sms($form_data['query_assigned_id'], $form_data['query_creator_id'], 'Work Order No '.$form_data['query_reference']);
                                    $query_details_insert_data['query_creator_id'] = $form_data['query_creator_id'];
                                    $query_details_insert_data['query_assigned_id'] = $form_data['query_assigned_id'];
                                    $this->common_model->update_data_sales_db('query_master', array('query_text'=>$form_data['query_text'], 'query_assigned_id'=> $form_data['query_assigned_id'], 'query_creator_id'=> $form_data['query_creator_id'], 'query_add_time'=> date("Y-m-d H:i:s"), 'reply_text'=>'', 'reply_creator_id'=>0, 'reply_assigned_id'=>0, 'reply_add_time'=> NULL, 'query_status'=> 'open'), array('id'=> $query_master_details['id']));

                                }else if(in_array($this->session->userdata('role'),array(1, 6, 7, 8, 10, 11, 24))){

                                    $this->send_query_sms($form_data['query_assigned_id'], $form_data['query_creator_id'], 'Work Order No '.$form_data['query_reference'], 'answered');
                                    $query_details_insert_data['query_creator_id'] = $form_data['query_creator_id'];
                                    $query_details_insert_data['query_assigned_id'] = $form_data['query_assigned_id'];
                                    $this->common_model->update_data_sales_db('query_master', array('reply_text'=>$form_data['query_text'], 'reply_creator_id'=> $form_data['query_creator_id'], 'reply_assigned_id'=> $form_data['query_assigned_id'],'reply_add_time'=> date("Y-m-d H:i:s"), 'query_status'=> 'open'), array('id'=> $query_master_details['id']));

                                }
                                $this->common_model->insert_data_sales_db('query_details',$query_details_insert_data);
                            }
                            $response['status'] = 'successful';
                            $response['message'] = "Successfully added OSDR Query.";
                        }
                    }
                    $this->session->unset_userdata('production_id_for_osdr_document_upload');
                break;
                case 'get_osdr_query_history':

                    $data = array();
                    $production_id = (!empty($this->input->post('production_id'))) ? $this->input->post('production_id') : $this->session->userdata('production_id_for_osdr_document_upload');

                    $data['user_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status'=>'1'), 'users'), 'name', 'user_id');
                    $quotation_assign_details = $this->query_model->query_quotation_assign_details($this->input->post('osdr_query_quotation_id'));
                    // echo "<pre>";print_r($quotation_assign_details);echo"</pre><hr>";exit;
                    $response['quotation_id'] = 0;
                    if(!empty($quotation_assign_details)){

                        $response['work_order_no'] = $quotation_assign_details['work_order_no'];
                        $response['quotation_id'] = $quotation_assign_details['quotation_id'];
                        $response['creator_id'] = $this->session->userdata('user_id');

                        if(in_array($this->session->userdata('role'),array(1, 5, 16))){

                            $response['query_type_option_tag'] = '<select class="form-control kt-selectpicker" name="query_assigned_id">';
                            $response['query_type_option_tag'] .= '<optgroup label="Pre-Qualification" data-max-options="1"><option value="267">Abhishek Thakare</option></optgroup>';

                        }elseif(in_array($this->session->userdata('role'),array(1, 7, 6, 8, 10, 11, 24))){

                            $response['query_assigned_id'] = $quotation_assign_details['sales_person_id'];
                            $response['query_type_option_tag'] = '<select class="form-control kt-selectpicker" name="query_creator_id">';
                            $response['query_type_option_tag'] .= '<optgroup label="Pre-Qualification/Quality" data-max-options="2"><option value="'.$this->session->userdata('user_id').'" selected>'.$this->session->userdata('name').'</option></optgroup>';
                        }

                        $query_master_details = $this->common_model->get_dynamic_data_sales_db('id, query_number', array('status'=>'Active', 'query_type'=>'osdr_query','query_reference_id'=>$quotation_assign_details['quotation_id']), 'query_master', 'row_array');
                        if(!empty($query_master_details)) {

                            $data['conversation_details'] = $this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active', 'query_number'=>$query_master_details['query_number']), 'query_details');
                            if(!empty($data['conversation_details'])){

                                foreach ($data['conversation_details'] as $conversation_details_key => $single_chat) {

                                    $data['conversation_details'][$conversation_details_key]['tat'] = $this->creat_tat($single_chat['add_time']);
                                }
                            }
                            $data['people_information_details'] = array_column($this->common_model->get_dynamic_data_sales_db('*', array('status'=>'Active'), 'people_information'), 'profile_pic_file_path', 'user_id');
                        }

                        $response['query_history_html'] = $this->load->view('query/chat_conversation_v2', $data, true);

                        if(!empty($production_id)) {

                            $documents_details = $this->common_model->get_dynamic_data_sales_db('id, osdr_document',array('id'=>$production_id), 'production_process_information', 'row_array');

                            if(!empty($documents_details)){

                                $response['production_osdr_document_history'] = $this->load->view('production/document_pdf_table', array('document_details'=> $documents_details), true);
                            }
                            $this->session->set_userdata('production_id_for_osdr_document_upload', $production_id);
                        }
                    }
                break;
                case 'get_osdr_document_history':

					if(!empty($this->input->post('work_order_no'))) {

						$documents_details = $this->common_model->get_dynamic_data_sales_db('id, osdr_document',array('work_order_no'=>$this->input->post('work_order_no')), 'production_process_information', 'row_array');

						if(!empty($documents_details)){

							$response['production_osdr_document_history'] = $this->load->view('production/document_pdf_table', array('document_details'=> $documents_details), true);
						}
					}
				break;

                case 'change_pq_status':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";die;
					if(!empty($this->input->post('id')) && !empty($this->input->post('pq_status'))){

                        if(empty($this->input->post('is_pq'))){
                            $update_data['is_pq'] = "Yes";
						}else{
                            $update_data['is_pq'] = $this->input->post('is_pq');
						}
						$update_data['pq_client_status'] = $this->input->post('pq_status');
						$update_data['modified_on'] = date('Y-m-d H:i:s');
						$update_data['modified_by'] = $this->session->userdata('user_id');

						$this->common_model->update_data_sales_db('customer_mst', $update_data, array('id'=> $this->input->post('id')));
					}
				break;
                case 'query_reassigned_to':
                    // echo "<pre>";print_r($this->input->post());echo"</pre><hr>";die;
                    if(!empty($this->input->post('id')) && !empty($this->input->post('assigned_to'))){

                        $update_data['query_assigned_id'] = $this->input->post('assigned_to');

                        $query_details = $this->common_model->get_all_conditional_data_sales_db('*',array('status'=>'Active', 'query_number'=>$this->input->post('query_number')), 'query_details', 'row_array', array(), array('column_name'=>'id', 'column_value'=>'desc'));

                        $this->common_model->update_data_sales_db('query_master', $update_data, array('id'=> $this->input->post('id'), 'query_number'=>$this->input->post('query_number')));

                        $this->common_model->update_data_sales_db('query_details', $update_data, array('id'=> $query_details('id'), 'query_number'=>$this->input->post('query_number')));
                    }
                break;
                default:
                    $response['status'] = 'failed';
                    $response['message'] = "call type not found";
                break;
            }
        } else{
            die('access is not allowed to this function');
        }
        echo json_encode($response);
    }

    private function upload_doc_config($pdf_type = 'MTC', $query_number) {

		// echo "<pre>";print_r($_FILES);echo"</pre><hr>";exit;
		$config['upload_path']          = './assets/query_pdf/';
        $config['allowed_types']        = 'pdf';//'jpeg|gif|jpg|png';
        $config['file_name']            = str_replace('/', '-', $query_number).'_'.$pdf_type;
        $config['overwrite']            = TRUE;

        $this->load->library('upload', $config);

		$data = array('status' => 'successful', 'msg' => '', 'file_name' => '');
        if ( ! $this->upload->do_upload('file'))
        {
			$error = array('error' => $this->upload->display_errors());
			$data['status'] = 'failed';
			$data['msg'] = $error['error'];
        }
        else
        {
        	$file_dtls = $this->upload->data();
			$data['file_name'] = $file_dtls['file_name'];
        }
        // echo "<pre>";print_r($data);echo"</pre><hr>";exit;
        return $data;
	}

    private function get_query_total(){

        $return_array = array('sales_count'=> array('open'=> 0, 'closed'=> 0),'production_count'=> array('open'=> 0, 'closed'=> 0),'rfq_count'=> array('open'=> 0, 'closed'=> 0), 'sample_count'=> array('open'=> 0, 'closed'=> 0), 'pqq_count'=> array('open'=> 0, 'closed'=> 0), 'osdr_count'=> array('open'=> 0, 'closed'=> 0), 'lead_count'=> array('open'=> 0, 'closed'=> 0));
        $user_id_under_this_role = $this->session->userdata('user_id');
        if(in_array($this->session->userdata('role'), array(1, 4, 5, 6, 7, 8, 10, 11, 16, 17, 18, 21, 24))) {

            $user_id_under_this_role = $this->query_model->user_id_under_this_role($this->session->userdata('role'));
        }
        $return_array['sales_count'] = $this->create_query_total($user_id_under_this_role, "'sales_query'");
        $return_array['production_count'] = $this->create_query_total($user_id_under_this_role, "'production_query'");
        $return_array['rfq_count'] = $this->create_query_total($user_id_under_this_role, "'rfq_query'");
        $return_array['pq_count'] = $this->create_query_total($user_id_under_this_role, "'pq_query'");
        $return_array['osdr_count'] = $this->create_query_total($user_id_under_this_role, "'osdr_query'");
        $return_array['lead_count'] = $this->create_query_total($user_id_under_this_role, "'lead_query'");

        $sample_type = "'lead_sample_query','quotation_sample_query'";
        $return_array['sample_count'] = $this->create_query_total($user_id_under_this_role, $sample_type);
        // echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;

        return $return_array;
    }

    private function create_query_total($user_id_under_this_role, $query_type){

        $return_array = array('open'=> 0, 'closed'=> 0);
        $data = $this->common_model->get_all_conditional_data_sales_db(
            "query_status, count(*) as total",
            "
            status = 'Active'
                AND 
            query_type IN ({$query_type})
                AND 
            ( 
                query_creator_id IN ('".implode("', '", $user_id_under_this_role)."')
                    OR
                query_assigned_id IN ('".implode("', '", $user_id_under_this_role)."')
            )
            ",
            'query_master',
            'result_array',
            array(),
            array(),
            'query_status'
        );
        if(!empty($data)){

            foreach ($data as $single_details) {
                
                $return_array[$single_details['query_status']] = $single_details['total']; 
            }
        }

        return $return_array;
    }

    private function create_query_where($search_form_data) {
        
        $return_array = array(
                            'query_status'=>"'".$this->session->userdata('query_tab_name')['query_status']."'",
                            'query_type'=>"'".$this->session->userdata('query_tab_name')['query_type']."'" ,
                        );
        if($this->session->userdata('role') == 5) {

            if(in_array($this->session->userdata('query_tab_name')['query_type'], array('sales', 'proforma'))) {

                $return_array['raised_by'] = $this->session->userdata('user_id');
            }else if (in_array($this->session->userdata('query_tab_name')['query_type'], array('purchase'))) {

                $return_array['query_recepient'] = $this->session->userdata('user_id');
            }
        }
        if(!empty($search_form_data)){
        // echo "<pre>";print_r($search_form_data);echo"</pre><hr>";exit();
            $query_for_id = $raised_by_id = $answer_by_id = array();
            foreach ($search_form_data as $form_data) {
                
                if(!empty($form_data['value'])) {
                    if($form_data['name'] == 'query_for_id') {
                        $query_for_id[] = $form_data['value'];
                    } else if($form_data['name'] == 'raised_by') {

                        $raised_by_id[] = $form_data['value'];
                    } else if($form_data['name'] == 'query_recepient') {

                        $answer_by_id[] = $form_data['value'];
                    } else if($form_data['name'] == 'raised_on') {
                        
                        $return_array['raised_on >= '] = "'".date('Y-m-d 00:00:00', strtotime($form_data['value']))."'";
                        $return_array['raised_on <= '] = "'".date('Y-m-d 23:59:59', strtotime($form_data['value']))."'";
                    }
                }
            }
            if(!empty($query_for_id)) {

                $return_array['query_for_id IN'] = "('".implode("', '", $query_for_id)."')";
            }
            if(!empty($raised_by_id)) {
                $return_array['raised_by IN'] = "('".implode("', '", $raised_by_id)."')";
            }
            if(!empty($answer_by_id)) {
                $return_array['query_recepient IN'] = "('".implode("', '", $answer_by_id)."')";
            }
        }
        
        return $return_array;   
    }

    private function create_query_data($where_array, $limit = 10, $offset = 0) {

        // echo "<pre>";print_r($where_array);echo"</pre><hr>";
        $return_data =  array();
        $return_data = $this->query_model->get_query_data($where_array,$limit,$offset);
        $users=array_column($this->common_model->get_dynamic_data_sales_db('user_id,name',array('status'=>1),'users'),'name','user_id');
        foreach ($return_data['query_list'] as $key => $value) {

            $return_data['query_list'][$key] = $this->common_model->get_dynamic_data_sales_db('quote_no',array('quotation_mst_id'=>$value['query_for_id']),'quotation_mst', 'row_array');
            $return_data['query_list'][$key]['query_id']=$value['query_id'];
            $return_data['query_list'][$key]['query_recepient']=$users[$value['query_recepient']];
            $return_data['query_list'][$key]['raised_by']=$users[$value['raised_by']];
            $return_data['query_list'][$key]['query_for_id']=$value['query_for_id'];
            $return_data['query_list'][$key]['query_status']=$value['query_status'];
            $query_text = $this->common_model->get_dynamic_data_sales_db('query_text',array('query_id'=>$value['query_id']),'query_texts');
            $return_data['query_list'][$key]['query_text'] = $query_text[count($query_text)-1]['query_text']; 
            $return_data['query_list'][$key]['raised_on']=date('d F,Y', strtotime($value['raised_on']));
        }
        //echo "<pre>";print_r($return_data);echo"</pre><hr>";exit();
        // getting query details
        $all_query_details = $this->common_model->get_dynamic_data_sales_db('*',array('query_status'=>$this->session->userdata('query_tab_name')['query_status'],'query_type'=>$this->session->userdata('query_tab_name')['query_type']),'query_mst');
        $return_data['search_filter_data']['quotation_no'] = array();
        $return_data['search_filter_data']['selected_quotation_no'] = array();
        $return_data['search_filter_data']['raised_by'] = array();
        $return_data['search_filter_data']['selected_raised_by'] = array();
        $return_data['search_filter_data']['answer_by'] = array();
        $return_data['search_filter_data']['selected_answer_by'] = array();
        $return_data['search_filter_data']['selected_date'] = '';
        if(!empty($all_query_details)) {

            $return_data['search_filter_data']['quotation_no'] = $this->query_model->get_where_in_data(
                                                            'quotation_mst_id, quote_no',
                                                            array(
                                                                    'column_name' => 'quotation_mst_id',
                                                                    'value' => array_column($all_query_details, 'query_for_id')
                                                                ),
                                                            'quotation_mst'
                                                        );
            $return_data['search_filter_data']['raised_by'] = $this->query_model->get_where_in_data(
                                                            'user_id, name',
                                                            array(
                                                                    'column_name' => 'user_id',
                                                                    'value' => array_column($all_query_details, 'raised_by')
                                                                ),
                                                            'users'
                                                        );
            $return_data['search_filter_data']['answer_by'] = $this->query_model->get_where_in_data(
                                                            'user_id, name',
                                                            array(
                                                                    'column_name' => 'user_id',
                                                                    'value' => array_column($all_query_details, 'query_recepient')
                                                                ),
                                                            'users'
                                                        );
        }
        if(!empty($where_array['query_for_id IN'])) {

            $return_data['search_filter_data']['selected_quotation_no'] = explode("', '", str_replace("('", '', str_replace("')", '', $where_array['query_for_id IN'])));
        }
        if(!empty($where_array['raised_by IN'])) {
            $return_data['search_filter_data']['selected_raised_by'] = explode("', '", str_replace("('", '', str_replace("')", '', $where_array['raised_by IN'])));
        }
            // echo "<pre>";print_r($return_data);echo"</pre><hr>";exit();
        if(!empty($where_array['query_recepient IN'])) {
            
            $return_data['search_filter_data']['selected_answer_by'] = explode("', '", str_replace("('", '', str_replace("')", '', $where_array['query_recepient IN'])));
        }
        if(!empty($where_array['raised_on >= '])) {
            
            $return_data['search_filter_data']['selected_date'] = date('d F,Y', strtotime(str_replace("'", "", str_replace("00", "", str_replace(":", "", str_replace(" ", "", $where_array['raised_on >= ']))))));
        }
        // echo "<pre>";print_r($return_data);echo"</pre><hr>";exit();
        $return_data['paggination_data']['limit'] = $limit;
        $return_data['paggination_data']['offset'] = $offset;
        return $return_data;
    }

    private function prepare_data_for_query($where_array, $limit, $offset){
        
        $data = array();
        $user_id_under_this_role = $this->session->userdata('user_id');
        if(in_array($this->session->userdata('role'), array(1, 4, 5, 6, 7, 8, 10, 11, 16, 17, 18, 21, 24))) {

            $user_id_under_this_role = $this->query_model->user_id_under_this_role($this->session->userdata('role'));
            $data = $this->query_model->get_query_list_data($where_array, $limit, $offset, $user_id_under_this_role);

            $user_list = array_column($this->common_model->get_dynamic_data_sales_db('user_id, name', array('status'=>1), 'users'), 'name', 'user_id');
            $user_list['']='';
            $user_list[0]='';
        }
        // echo "<pre>";print_r($data);echo"</pre><hr>";exit;
        foreach($data['query_list'] as $key=>$single_data) {

            $data['query_list'][$key]['comment_creator'] =  $user_list[$single_data['comment_creator_id']];
            $data['query_list'][$key]['TAT'] =  $data['query_list'][$key]['rfq_subject'] = $data['query_list'][$key]['background_class'] = '';
            $data['query_list'][$key]['show_delete'] = $data['query_list'][$key]['show_close'] =
            $data['query_list'][$key]['show_reopen'] = $data['query_list'][$key]['show_work_order_sheet'] =
            $data['query_list'][$key]['show_mtc_drawing_upload'] = $data['query_list'][$key]['show_mtc_drawing_download'] = $data['query_list'][$key]['show_osdr_document'] =
            $data['query_list'][$key]['change_pq_status'] = false;
                                        
            if($this->session->userdata('role') == 1){

                $data['query_list'][$key]['show_delete'] = true;
            }
            if(in_array($single_data['query_creator_id'], $this->session->userdata('query_access')['query_creator_id'])) {
                
                if($single_data['query_status'] ==  'closed') {

                    $data['query_list'][$key]['show_reopen'] = true;
                }
                if(!empty($single_data['reply_add_time'])) {

                    $data['query_list'][$key]['show_close'] = true;
                }
            }
            $data['query_list'][$key]['query_add_time']=date('F j, Y g:i A', strtotime($single_data['query_add_time']));
            if(!empty($single_data['comment_add_time'])) {
                $data['query_list'][$key]['comment_add_time']=date('F j, Y g:i A', strtotime($single_data['comment_add_time']));
            }
            if(!empty($single_data['reply_add_time'])) {

                $data['query_list'][$key]['reply_add_time']=date('F j, Y g:i A', strtotime($single_data['reply_add_time']));
                $data['query_list'][$key]['TAT']=$this->get_tat($single_data['query_add_time'],$single_data['reply_add_time']);
                if($data['query_list'][$key]['TAT'] != ''){

                    $data['query_list'][$key]['background_class'] = "lightgreen";
                }
            }
            if($single_data['query_type'] == 'quotation_sample_query' || $single_data['query_type'] == 'lead_sample_query'){

                $data['query_list'][$key]['modal_class_name']= 'sample_query';

                $data['query_list'][$key]['change_assigned_to_array'] = array(
                    'Aisha S'   =>	array('value'=> '38',    'display_name'=> 'Aisha S'),
                    'Ankit Y'   =>	array('value'=> '190',   'display_name'=> 'Ankit Y'),
                );
            }else{

                $data['query_list'][$key]['modal_class_name']= $single_data['query_type'];
            }
            $data['query_list'][$key]['url']= $this->get_query_pdf_url($single_data['query_type'], $single_data['query_reference_id']);
            if($single_data['query_type'] == 'production_query' || $single_data['query_type'] == 'osdr_query'){

                $res = $this->common_model->get_dynamic_data_sales_db('id', array('work_order_no'=> $data['query_list'][$key]['query_reference']), 'work_order_sheet', 'row_array');
                if(!empty($res)){

                    if(!empty($res['approved_by'])){
                        $data['query_list'][$key]['show_work_order_sheet'] = true;
                    }
                }

                if($single_data['query_type'] == 'osdr_query'){

                    $production_details = $this->common_model->get_dynamic_data_sales_db('id, osdr_document', array('work_order_no'=> $data['query_list'][$key]['query_reference'], 'status'=>'Active'), 'production_process_information', 'row_array');

                    if(!empty($production_details['osdr_document'])){

                        $data['query_list'][$key]['show_osdr_document'] = true;
                    }
                }
            }else if($single_data['query_type'] == 'sales_query' || $single_data['query_type'] == 'quotation_sample_query'){

                if($this->session->userdata('query_access')['query_list_mtc_drawing_tab_access']){

                    $mtc_drawing_details = $this->common_model->get_dynamic_data_sales_db('*',array('query_number'=>$single_data['query_number'], 'status'=>'Active'), 'query_other_information', 'result_array');

                    if(!empty($mtc_drawing_details)){

                        $data['query_list'][$key]['show_mtc_drawing_download'] = true;
                    }else{

                        $data['query_list'][$key]['show_mtc_drawing_upload'] = true;
                    }
                }
            }else if($single_data['query_type'] == 'rfq_query'){

                $data['query_list'][$key]['rfq_subject'] = "Test";
                $data['query_list'][$key]['rfq_subject'] = $this->common_model->get_all_conditional_data_sales_db('rfq_subject', array('rfq_mst_id'=> $data['query_list'][$key]['query_reference_id']), 'rfq_mst', 'row_array')['rfq_subject'];
            }else if($single_data['query_type'] == 'pq_query'){

                $pq_details = $this->common_model->get_dynamic_data_sales_db('*',array('id'=>$single_data['query_reference_id'], 'status'=>'Active'), 'customer_mst', 'row_array');
                if($pq_details['is_pq'] == 'Yes'){

                    $data['query_list'][$key]['pq_status_background'] = "color: #0abb87;";
                }else{
                    $data['query_list'][$key]['pq_status_background'] = "color: #ACACAC;";
                }

                $pending_class = $approved_class = '';
                if($pq_details['pq_client_status'] == 'Approved'){

                    $approved_class = 'active';
                }else if($pq_details['pq_client_status'] == 'Pending'){

                    $pending_class = 'active';
                }
                $data['query_list'][$key]['change_pq_status'] = true;
                $data['query_list'][$key]['pq_status'] = $pq_details['is_pq'];
                $data['query_list'][$key]['pq_status_array'] = array(
                'Pending'   =>	array('value'=> 'Pending',    'display_name'=> 'Pending',    'class'=> $pending_class),
                'Approved'  =>	array('value'=> 'Approved',   'display_name'=> 'Approved',   'class'=> $approved_class),
                );
            }else if($single_data['query_type'] == 'lead_query'){

                $user_details = $this->common_model->get_dynamic_data_sales_db_null_false('user_id, name', "status = 1 and (role in (6, 7, 16, 11, 14, 18) OR user_id in (38, 120, 189, 302))", 'users');

                foreach ($user_details as $user) {
                    $data['query_list'][$key]['change_assigned_to_array'][$user['name']] = [
                        'value' => $user['user_id'],
                        'display_name' => $user['name']
                    ];
                }
            }
            $quotation_edit_details = $this->get_quotation_edit_details($single_data['query_type'], $single_data['query_reference_id']);
            $data['query_list'][$key]['show_quotation_edit'] = $quotation_edit_details['show_quotation_edit'];
            $data['query_list'][$key]['quotation_mst_id'] = $quotation_edit_details['quotation_mst_id'];
            $data['query_list'][$key]['rfq_id'] = $quotation_edit_details['rfq_id'];
            $data['query_list'][$key]['grand_total'] = $quotation_edit_details['grand_total'];
            $client_detail = $this->get_client_id($single_data['query_reference_id'], $single_data['query_type']);
            if(!empty($client_detail)){

                foreach ($client_detail as $client_key => $value) {
              
                    $data['query_list'][$key]['client_name'] = $value['name'];
                }
            }
        }

        $data['user_details'] = array_column($this->common_model->get_dynamic_data_sales_db('name, user_id', array(), 'users'), 'name', 'user_id');
        $data['user_details'][0] = '';
        //search filter data
        $query_master_all_data = $this->query_model->get_query_list_data($where_array, 0, 0, $user_id_under_this_role)['query_list'];
        foreach ($query_master_all_data as $single_query_details) {
            // echo "<pre>";print_r($single_query_details);echo"</pre><hr>";exit;    
            $data['quote_no'][]=$single_query_details['query_reference'];
            $data['query_creator_id'][]=$single_query_details['query_creator_id'];
            $data['query_assigned_id'][]=$single_query_details['query_assigned_id'];;
        }
        $data['paggination_data']['limit'] =$limit;
        $data['paggination_data']['offset'] =$offset;
        // echo "<pre>";print_r($data);echo"</pre><hr>";exit;
        return $data;
    }

    private function get_quotation_edit_details($query_type = '', $query_reference_id = ''){

        $currency_details = $this->common_model->get_dynamic_data_sales_db('*', array('status' => 'Active'), 'currency');
        $currency_icon = $currency_value = array();
        $currency_icon[''] = $currency_value[''] = '';
		$currency_icon[0] = $currency_value[0] = '';
        
        foreach ($currency_details as $single_currency_details) {

            $currency_icon[$single_currency_details['currency_id']] = $single_currency_details['decimal_number'];
            $currency_value[$single_currency_details['currency_id']] = $single_currency_details['currency_rate'];
        }
		
        $return_array = array(
            'show_quotation_edit' => false,
            'quotation_mst_id' => '',
            'rfq_id' => '',
            'grand_total' => '',
        );
        if(empty($query_type) && empty($query_reference_id)){

            return $return_array;
        }
        
        if(in_array($query_type, array('sales_query', 'production_query', 'quotation_sample_query'))){

            $quotation_mst_details = $this->common_model->get_dynamic_data_sales_db('quotation_mst_id, rfq_id, grand_total, currency', array('quotation_mst_id'=> $query_reference_id), 'quotation_mst', 'row_array');
            if(!empty($quotation_mst_details)){

                // echo "<pre>";print_r($quotation_mst_details);echo"</pre><hr>";exit;

                $return_array = array(
                    'show_quotation_edit' => true,
                    'quotation_mst_id' => $quotation_mst_details['quotation_mst_id'],
                    'rfq_id' => $quotation_mst_details['rfq_id'],
                    'grand_total' => $currency_icon[$quotation_mst_details['currency']].number_format($quotation_mst_details['grand_total'],2,'.',','),
                );
                if(($quotation_mst_details['grand_total'] * ($currency_value[$quotation_mst_details['currency']] / $currency_value[1])) >= 50000){

                    $return_array['grand_total'] = '<span class="kt-font-bolder kt-badge kt-badge--unified-warning kt-badge--sm kt-badge--rounded kt-badge--bold" style="width: auto; padding: 10px;font-size: 1.4rem;">'.$currency_icon[$quotation_mst_details['currency']].number_format($quotation_mst_details['grand_total'],2,'.',',').'</span>';
                }
            }
        }elseif($query_type == 'rfq_query'){

            $quotation_mst_details = $this->common_model->get_dynamic_data_sales_db('quotation_mst_id, rfq_id', array('rfq_id'=> $query_reference_id), 'quotation_mst', 'row_array');
            if(!empty($quotation_mst_details)){

                $return_array = array(
                    'show_quotation_edit' => true,
                    'quotation_mst_id' => $quotation_mst_details['quotation_mst_id'],
                    'rfq_id' => $quotation_mst_details['rfq_id'],
                );
            }
        }

        return $return_array;
    }

    private function get_query_pdf_url($query_type, $query_reference_id){

        $return_url = '';
        switch ($query_type) {

            case 'production_query':
            case 'osdr_query':

                $quotation_details = $this->common_model->get_dynamic_data_sales_db("client_type", array('quotation_mst_id'=> $query_reference_id), 'quotation_mst', 'row_array');
                if(!empty($quotation_details) && $this->session->userdata('quotation_access')['quotation_list_action_view_proforma_pdf_access']){

                    $return_url = "pdf_management/proforma_pdf/".$query_reference_id;
                }
            break;

            case 'sales_query':
            case 'rfq_query':
            case 'quotation_sample_query':

                $where_array = array('quotation_mst_id'=> $query_reference_id);
                if($query_type == 'rfq_query'){

                    $where_array = array('rfq_id'=> $query_reference_id);
                }
                $quotation_details = $this->common_model->get_dynamic_data_sales_db("quotation_mst_id, client_type", $where_array, 'quotation_mst', 'row_array');
                if(!empty($quotation_details)){

                    $return_url = "pdf_management/quotation_pdf/".$quotation_details['quotation_mst_id'];
                }
            break;
        }

        return $return_url;
    }

    private function get_client_id($id, $query_type){

		$return = '';

        if($query_type == 'sales_query' || $query_type == 'production_query' || $query_type == 'quotation_sample_query' || $query_type == 'osdr_query'){

            $quotation_details = $this->common_model->get_all_conditional_data_sales_db('client_id', array('quotation_mst_id'=> $id), 'quotation_mst', 'row_array');

            if(!empty($quotation_details)){

                $return = $this->common_model->get_dynamic_data_sales_db('name', array('id'=>$quotation_details['client_id']), 'customer_mst');
            }
        }else if($query_type == 'rfq_query'){

            $rfq_details = $this->common_model->get_all_conditional_data_sales_db('rfq_company', array('rfq_mst_id'=> $id), 'rfq_mst', 'row_array');
         
            if(!empty($rfq_details)){
             
                $return = $this->common_model->get_dynamic_data_sales_db('name', array('id'=>$rfq_details['rfq_company']), 'customer_mst');
            }
        }else if($query_type == 'lead_sample_query' || $query_type == 'pq_query' || $query_type == 'lead_query'){

            $return = $this->common_model->get_dynamic_data_sales_db('name', array('id'=>$id), 'customer_mst');
        }
        // echo "<pre>";print_r($return);echo"<pre>";exit;
		return $return;
	}

    public function get_tat($query_add_time,$reply_add_time){
        

        $diff='';
        if($query_add_time != '' && $reply_add_time != '') {

            $time1 =date_create($query_add_time);
            $time2 = date_create($reply_add_time);
            // echo $time1, "<hr>", $time2;die;
            $interval=date_diff($time1,$time2);
            $difference['day']=$interval->format('%d');
            $difference['hours']=$interval->format('%h H');
            $difference['minutes']=$interval->format('%i m');
            if($difference['day'] !=='0' ){
            
                $diff=$difference['day']." day";
           
            }else if($difference['day'] =='0' && $difference['hours'] !=='0 H' ||  $difference['minutes'] !=='0 m' ){
            
                $diff="Less than 1 day";
            
            }
        }
        return $diff;
    }

    private function creat_tat($date_time){

		$return_tat = '';
		// Get the timestamp of the chat message
		$chat_time = strtotime(str_replace("Z", "", $date_time)); // Replace with your chat message timestamp

		// Create DateTime objects for the chat message and current time
		$chat_date = new DateTime("@$chat_time");
		$current_date = new DateTime();
		// Get the difference between the two dates
		$date_diff = $current_date->diff($chat_date);

		// Output the result
		if ($date_diff->y > 0) {

			$return_tat = $date_diff->y . " year(s)";
		}else if ($date_diff->m > 0) {

			$return_tat = $date_diff->m . " month(s)";
		}else if ($date_diff->d > 0) {

			$return_tat = $date_diff->d . " day(s)";
		}else if ($date_diff->h > 0) {

			$return_tat = $date_diff->h . " hour(s)";
		}else if ($date_diff->i > 0) {

			$return_tat = $date_diff->i . " minute(s)";
		}else {

			$return_tat = $date_diff->s . " second(s)";
		}
		
		return $return_tat." ago";
	}

    private function create_search_filter_data($search_filter_form_data, $main_tab_name, $sub_tab_name){

        $return_array['where'] = '';

        if($main_tab_name == 'sample_query'){

            $return_array['where'] = "status='Active' AND query_type IN ('quotation_sample_query', 'lead_sample_query') AND query_status='".$sub_tab_name."'";
        }else{

            $return_array['where'] = "status='Active' AND query_type IN ('".$main_tab_name."') AND query_status='".$sub_tab_name."'";
        }

        if(!empty($search_filter_form_data)){

			foreach ($search_filter_form_data as $form_data) {

				if(!empty($form_data['value'])){

					switch ($form_data['name']) {

						case 'query_reference':

							$return_array['where'] .= " AND query_reference = '".$form_data['value']."'";
						break;
						case 'query_creator_id':

                            $return_array['where'] .= " AND query_creator_id = '".$form_data['value']."'";
						break;
						case 'query_assigned_id':

							$return_array['where'] .= " AND query_assigned_id = '".$form_data['value']."'";
						break;
						case 'query_type':

							$return_array['where'] .= " AND query_type = '".$form_data['value']."'";
						break;
						case 'add_time':

							$return_array['where'] .= " AND add_time >= '".date('Y-m-d 00:00:00', strtotime($form_data['value']))."' AND add_time <= '".date('Y-m-d 23:59:59', strtotime($form_data['value']))."'"; 
						break;
						default:

						break;
					}
				}
			}
		}
		// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		return $return_array;
    }
}
?>