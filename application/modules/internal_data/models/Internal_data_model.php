<?php 
Class Internal_data_model extends CI_Model{

	function __construct(){
		parent::__construct();
		$CI = &get_instance();
		$this->db2 = $CI->load->database('marketing', true);
	}

	public function get_all_conditional_data($select = '*', $where, $table_name, $return_type = 'result_array', $join_array = array(), $order_by_array = array(), $group_by_array = array(),$limit = 0, $offset = 0) {

		$this->db->select($select);
		$this->db->where($where);
		if(!empty($group_by_array)) {
			$this->db->group_by($group_by_array);
		}
		if(!empty($order_by_array)) {
			$this->db->order_by($order_by_array['column_name'], $order_by_array['column_value']);
		}
		return $this->db->get($table_name, $limit, $offset)->$return_type();
	}

	// public function insert_data($table, $data){
	// 	$this->db->insert($table, $data);
	// 	return $this->db->insert_id();
	// }

	// public function update_data($table, $data, $where){
	// 	return $this->db->update($table, $data, $where);
	// }

	public function delete_Data($table, $where){
		$this->db->delete($table, $where);
	}

	public function update_data($table_name, $update_data, $where_array, $update_type = 'single_update'){
		
		if($update_type == 'single_update') {

			return $this->db->update($table_name, $update_data, $where_array);
		} else if ($update_type == 'batch') {

			$this->db->update_batch($table_name, $update_data, $where_array);

			// echo "<pre>";print_r($this->db->last_query());echo"</pre><hr>";
		}
	}

	public function update_data_sales_db($table, $data, $where){
		$this->db2->where($where, null, false);
		$this->db2->limit(1);
		return $this->db2->update($table, $data);
	}

	public function get_dynamic_data($select = '*', $where, $table_name, $return_type = 'result_array', $limit = 0, $offset = 0) {

		$this->db->select($select);
		$this->db->where($where);
		return $this->db->get($table_name, $limit, $offset)->$return_type();
	}

	// public function insert_data_batch($table_name, $insert_data) {

	// 	$this->db->insert_batch($table_name, $insert_data);
	// }

	public function insert_data($table_name, $insert_data, $insert_type = 'single_insert'){

		if($insert_type == 'single_insert') {

			$this->db->insert($table_name, $insert_data);
			return $this->db->insert_id();
		} else if ($insert_type == 'batch') {

			$this->db->insert_batch($table_name, $insert_data);
		}
	}

	public function update_data_batch($table_name, $update_data, $where_string) {

		$this->db->update_batch($table_name, $update_data, $where_string);	
	}

	// invoice code


	public function get_country_data($select = '*', $where, $table_name, $return_type = 'result_array', $join_array = array(), $order_by_array = array(), $group_by_array = array(),$limit = 0, $offset = 0) {

		$this->db->select($select);
		$this->db->where($where);
		if(!empty($join_array)) {
			// $this->db->join('customer_data','customer_data.cod_mst_id = country_mst.id','LEFT');
			$this->db->join('customer_mst','customer_mst.country_id = country_mst.id','LEFT');
			$this->db->join('customer_mst','customer_mst.id = customer_data.customer_mst_id','LEFT');
		}
		if(!empty($order_by_array)) {

			$this->db->order_by('customer_data.rank asc');
			// $this->db->order_by($order_by_array['column_name'], $order_by_array['column_value']);
		}
		return $this->db->get($table_name, $limit, $offset)->$return_type();
	}

	// public function get_data($select = '*', $where, $table_name, $return_type = 'result_array', $join_array = array(), $order_by_array = array(), $group_by_array = array(),$limit = 0, $offset = 0) {

		// 	$this->db->select($select);
		// 	$this->db->where($where);
		// 	if(!empty($join_array)) {
		// 		$this->db->join('lead_management_connect','lead_management_connect.details_id = lead_management_details.details_id','LEFT');
		// 	}
		// 	if(!empty($order_by_array)) {
		// 		$this->db->order_by($order_by_array['column_name'], $order_by_array['column_value']);
		// 	}
		// 	return $this->db->get($table_name, $limit, $offset)->$return_type();
	// }

	public function get_data($select = '*', $where, $table_name, $return_type = 'result_array', $join_array = array(), $order_by_array = array(), $group_by_array = array(),$limit = 0, $offset = 0) {

		$this->db->select($select);
		$this->db->where($where);
		if(!empty($join_array)) {
			$this->db->join('customer_connect','customer_connect.comp_detail_id = customer_dtl.comp_dtl_id','LEFT');
			
		}
		if(!empty($order_by_array)) {
			$this->db->order_by($order_by_array['column_name'], $order_by_array['column_value']);
		}
		return $this->db->get($table_name, $limit, $offset)->$return_type();
	}

	public function get_dynamic_data_sales_db($select = '*', $where, $table_name, $return_type = 'result_array', $limit = 0, $offset = 0) {

		$this->db2->select($select);
		$this->db2->where($where, null, false);
		return $this->db2->get($table_name, $limit, $offset)->$return_type();
	}

	public function get_all_conditional_data_sales_db($select_string = '*', $where_array, $table_name, $return_type = 'result_array', $join_array = array(), $order_by_array = array(), $group_by_array = array(), $limit = 0, $offset = 0) {

		$this->db->select($select_string);
		$this->db->where($where_array);
		if(!empty($group_by_array)) {
			$this->db->group_by($group_by_array);
		}
		if(!empty($order_by_array)) {
			$this->db->order_by($order_by_array['column_name'], $order_by_array['column_value']);
		}
		return $this->db->get($table_name, $limit, $offset)->$return_type();
	}

	public function get_sales_person($select) {

		$this->db->select($select);
		$this->db->where('role=5 OR role=16', null, false);
		return $this->db->get('users')->result_array();
	}

	// public function get_marketing_db_data($select, $where_array, $table_name, $limit = 0, $offset = 0, $return_type = 'result_array', $order_by_array = array(), $group_by_array = array()) {

		// 	$this->db2->select($select);
		// 	$this->db2->where($where_array);
		// 	if(!empty($order_by_array)) {
		// 		$this->db2->order_by($order_by_array['column_name'], $order_by_array['column_value']);
		// 	}
		// 	if(!empty($group_by_array)) {
		// 		$this->db2->group_by($group_by_array);		
		// 	}
		// 	return $this->db2->get($table_name, $limit, $offset)->$return_type();
	// }

	public function get_marketing_db_data($select, $where_array, $table_name, $limit = 0, $offset = 0, $return_type = 'result_array', $order_by_array = array(), $group_by_array = array()) {

		$this->db->select($select);
		$this->db->where($where_array);
		if(!empty($order_by_array)) {
			$this->db->order_by($order_by_array['column_name'], $order_by_array['column_value']);
		}
		if(!empty($group_by_array)) {
			$this->db->group_by($group_by_array);		
		}
		return $this->db->get($table_name, $limit, $offset)->$return_type();
	}


	// public function get_competitor_rank_invoice($where_array, $order_by_array, $limit, $offset) {

		// 	$return_array = array();
		// 	$this->db->select('SQL_CALC_FOUND_ROWS *', FALSE);
		// 	if(!empty($where_array)) {
		// 		$this->db->where($where_array, NULL, FALSE);
		// 	}
		// 	if(!empty($order_by_array)) {
		// 		$this->db->order_by($order_by_array['column_name'], $order_by_array['column_value']);
		// 	}
		// 	$return_array['competitor_ranks_list'] = $this->db->get('lead_management', $limit, $offset)->result_array();
		// 	// $return_array['competitor_ranks_list'] = $this->db->get('lead_management_bkpS', $limit, $offset)->result_array();
		// 	// echo $this->db->last_query(),"<hr>";
		// 	// echo "<pre>";print_r($return_array);echo"</pre><hr>";exit;
		// 	$return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows;")->row_array();
		// 	return $return_array;
	// }

	public function get_competitor_rank_invoice($where_array, $order_by_array, $limit, $offset) {
		$return_array = array();
		$this->db->select('SQL_CALC_FOUND_ROWS customer_mst.id, customer_mst.name customer_name, customer_mst.region_id, customer_mst.country_id, customer_mst.assigned_to, customer_mst.website, customer_mst.no_of_employees, customer_mst.lead_type, customer_mst.lead_stage, customer_mst.stage_reason, customer_mst.lead_priority, customer_mst.priority_reason, customer_mst.deleted, customer_mst.delete_reason, customer_mst.status,		
		customer_data.source, customer_data.rank, customer_data.last_purchased,
		country_mst.name country_name, country_flags.flag_name', FALSE);
		$this->db->join('customer_data', 'customer_data.customer_mst_id = customer_mst.id', 'left');
		$this->db->join('country_mst', 'country_mst.id = customer_mst.country_id', 'left');
		// $this->db->join('country_mst', 'country_mst.id = customer_data.cod_mst_id', 'left');
		$this->db->join('country_flags', 'country_flags.country = country_mst.name', 'left');
		if(!empty($where_array)) {
			$this->db->where($where_array, NULL, FALSE);
		}
		if(!empty($order_by_array)) {
			$this->db->order_by($order_by_array['column_name'], $order_by_array['column_value']);
		}
		$return_array['competitor_ranks_list'] = $this->db->get('customer_mst', $limit, $offset)->result_array();
		$return_array['paggination_data'] = $this->db->query("select FOUND_ROWS() total_rows;")->row_array();
		return $return_array;
	}

	// new code end

	public function get_invoice_details($where_array, $order_by_array, $limit, $offset) {
		
		$return_array = array();
		$this->db2->select('SQL_CALC_FOUND_ROWS *', FALSE);
		if(!empty($where_array)) {
			$this->db2->where($where_array, NULL, FALSE);
		}
		if(!empty($order_by_array)) {
			// foreach ($order_by_array as $order_by_array_value) {
				
				$this->db2->order_by($order_by_array['column_name'], $order_by_array['column_value']);
			// }
		}
		$return_array['invoice_list'] = $this->db2->get('lead_mst', $limit, $offset)->result_array();
		$return_array['paggination_data'] = $this->db2->query("select FOUND_ROWS() total_rows;")->row_array();
		return $return_array;
	}

	public function get_all_data($select, $where_array, $table_name, $result_type = 'result_array') {

		$this->db->select($select);
		$this->db->where($where_array);
		return $this->db->get($table_name)->$result_type();		
	}
}