<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Internal_data extends MX_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('user_id')){
			redirect('login', 'refresh');
			exit;
		}else{
			$access_count = 0;
			if(!empty($this->session->userdata('main_module_access')) && in_array(18, array_column($this->session->userdata('main_module_access'), 'module_id'))) {

				foreach ($this->session->userdata('sub_module_access') as $key => $value) {
					
					if($value['module_id'] == 18 && $value['status'] == 'Active') {
						$access_count++;
					}
				}
			}
			if($access_count == 0){
				redirect($this->session->userdata('home/dashboard'));
				exit;
			}
		}
		error_reporting(E_ALL);
		$this->load->model('internal_data_model');
	}

	public function index(){
		redirect('internal_data/competitor_ranks_invoice');
	}

	public function create_invoice_leads_html() {

		// $data = $this->internal_data_model->getPrimaryListData();
		$data = array();
		$this->load->view('header', array('title' => 'Invoice Leads'));
		$this->load->view('sidebar', array('title' => 'Invoice Leads'));
		$this->load->view('internal_data/html_for_invoice', $data);
		$this->load->view('footer');
	}

	public function competitor_ranks_invoice_html() {

		$data = array();
		$this->load->view('header', array('title' => 'Invoice Leads'));
		$this->load->view('sidebar', array('title' => 'Invoice Leads'));
		$this->load->view('internal_data/competitor_ranks_invoice_html', $data);
		$this->load->view('footer');	
	}

	public function competitor_ranks_invoice() {

		$where_array = array();
		$where_array['customer_data.source'] = "'internal'";
		if($this->session->userdata('role') == 5) {
			$where_array['assigned_to'] = "'".$this->session->userdata('user_id')."'";
		}
		
		$data = $this->create_competitor_ranks_invoice_data($where_array, array('column_name' => 'customer_data.rank', 'column_value' => 'asc'), 10, 00);

		$data['search_form']['customer_name'] = $data['search_form']['country_name'] =  '';
		$data['search_form']['lead_type_id'] = $data['search_form']['lead_stage_id'] =  '';
		$data['search_form']['user_id'] =  $data['search_form']['region_id'] =  '';
		$data['sorting'] = $this->create_sorting_reponse(array('column_name'=>'customer_data.rank', 'column_value' => 'asc'));
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => ''));
		$this->load->view('sidebar', array('title' => ''));
		$this->load->view('internal_data/competitor_ranks_invoice', $data);
		$this->load->view('footer');	
	}

	public function update_competitor_rank_invoice() {
		
		$data['next_count_number'] = 1;
		$data['next_number'] = 1;
		$customer_id = $this->uri->segment('3',0);
		if(empty($customer_id)) {
			redirect('internal_data/competitor_ranks_invoice');
		}
		else{
			$data = array();
			$data = $this->create_competitor_ranks_invoice_update_data($customer_id);
		}
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		$this->load->view('header', array('title' => 'Invoice Leads'));
		$this->load->view('sidebar', array('title' => 'Invoice Leads'));
		$this->load->view('internal_data/update_competitor_ranks_invoice', $data);
		$this->load->view('footer');	
	}

	// public function invoice_leads() {

	// 	// $data = $this->internal_data_model->getPrimaryListData();
	// 	$data = array();
	// 	$data = $this->create_invoice_details(array('deleted is'=> 'NULL', 'imp_id >' => 0, 'data_category' => "'pipes'"), array('column_name' => 'RANK', 'column_value' => 'ASC'), 5, 00);

	// 	// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
	// 	$this->load->view('header', array('title' => 'Invoice Leads'));
	// 	$this->load->view('sidebar', array('title' => 'Invoice Leads'));
	// 	$this->load->view('internal_data/invoice_lead_listing', $data);
	// 	$this->load->view('footer');
	// }
	
	// public function find_assign_count() {

	// 	$user_id = $this->uri->segment('3',0);
	// 	// changing deleted user assign quotation
	// 	$data['quotation'] = count($this->internal_data_model->get_dynamic_data('*',array('assigned_to' => $user_id), 'quotation_mst'));
	// 	// changing deleted user assign procurement
	// 	$data['pq_client'] = count($this->internal_data_model->get_dynamic_data('*',array('assigned_to' => $user_id), 'pq_client'));
	// 	// changing deleted user assign rfq
	// 	$data['rfq'] = count($this->internal_data_model->get_dynamic_data('*',array('assigned_to' => $user_id), 'rfq_mst'));
	// 	// changing deleted user assign mtc
	// 	$data['mtc'] = count($this->internal_data_model->get_dynamic_data('*',array('assigned_to' => $user_id), 'mtc_mst'));

	// 	$lead_source_array['hetro_leads'] = array( 
	// 											array('source_name' => 'chemical companies', 'sub_module_id' => 14),
	// 											array('source_name' => 'distributors', 'sub_module_id' => 20),
	// 											array('source_name' => 'epc companies', 'sub_module_id' => 16),
	// 											array('source_name' => 'heteregenous tubes india', 'sub_module_id' => 21),
	// 											array('source_name' => 'pvf companies', 'sub_module_id' => 22),
	// 											array('source_name' => 'shipyards', 'sub_module_id' => 13),
	// 											array('source_name' => 'sugar companies', 'sub_module_id' => 17),
	// 											array('source_name' => 'water companies', 'sub_module_id' => 15)
	// 										);
	// 	$lead_source_array['primary_leads'] = array(
	// 											array('source_name' => 'pipes', 'sub_module_id' => 12),
	// 											array('source_name' => 'tubes', 'sub_module_id' => 18),
	// 											array('source_name' => 'process control', 'sub_module_id' => 19),
	// 											array('source_name' => 'tubing', 'sub_module_id' => 23),
	// 											array('source_name' => 'hammer union', 'sub_module_id' => 24)
	// 										);
	// 	foreach ($lead_source_array['hetro_leads'] as $value) {

	// 		$data[$value['source_name']] = count($this->internal_data_model->get_dynamic_data('*',array('source' => $value['source_name'], 'assigned_to' => $user_id), 'clients'));
	// 	}
	// 	foreach ($lead_source_array['primary_leads'] as $value) {

	// 		$data[$value['source_name']] = count($this->internal_data_model->get_dynamic_data_sales_db('*',array('deleted is' => 'Null', 'imp_id >' => 0, 'assigned_to' => $user_id, 'data_category' => "'".$value['source_name']."'"), 'lead_mst'));
	// 	}
	// 	echo "<pre>";print_r($data);echo"</pre><hr>";exit;
	// }

	public function ajax_function_for_invoice() {
		if($this->input->is_ajax_request()){
			$call_type = $this->input->post('call_type');
			$response['status'] = 'successful';	
			switch ($call_type) {

				// case 'advance_search_for_invoice':
						
				// 	$where_array = array('deleted is'=> 'NULL', 'imp_id >' => 0, 'data_category' => "'pipes'");
				// 	if(!empty($this->input->post('company_name'))) {
				// 		$where_array['IMPORTER_NAME LIKE'] = "'%".$this->input->post('company_name')."%'";
				// 	}
				// 	$data = $this->create_invoice_details($where_array, array('column_name' => 'RANK', 'column_value' => 'ASC'), 5, 00);
				// 	// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
				// 	$response['html_body'] = $this->load->view('internal_data/invoice_table_body', $data, TRUE);
				// break;

				// case 'reset_advance_search_for_invoice':
					
				// 	$where_array = array('deleted is'=> 'NULL', 'imp_id >' => 0, 'data_category' => "'pipes'");
				// 	$data = $this->create_invoice_details($where_array, array('column_name' => 'RANK', 'column_value' => 'ASC'), 5, 00);
				// 	$response['html_body'] = $this->load->view('internal_data/invoice_table_body', $data, TRUE);
				// break;	
					
				// case 'update_competitor_rank_invoice_form_data':
					
				// 	$client_id = $this->input->post('client_id');
				// 	$lead_id = $this->input->post('lead_id');
				// 	// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";
				// 	// exit;

				// 	$update_lead_management_data = $update_member_array = $insert_member_array = array();
				// 	foreach ($this->input->post('invoice_form_data') as $lead_form_data) {
						
				// 		$update_lead_management_data[$lead_form_data['name']] = $lead_form_data['value'];
				// 	}
				// 	// echo "<pre>";print_r($update_lead_management_data);echo"</pre><hr>";die;
				// 	// create main member update ya insert data
				// 	$main_member_form_data = array_column($this->input->post('invoice_main_member_form_data'), 'value', 'name');
				// 	for ($i=0; $i < (count($main_member_form_data))/10; $i++) { 
						
				// 		if(!empty($main_member_form_data['main_member_array['.$i.'][details_id]'])) {

				// 			$update_member_array[] = array(
				// 											'details_id' => $main_member_form_data['main_member_array['.$i.'][details_id]'],
				// 											'member_name' => $main_member_form_data['main_member_array['.$i.'][name]'],
				// 											'designation' => $main_member_form_data['main_member_array['.$i.'][designation]'],
				// 											'email' => $main_member_form_data['main_member_array['.$i.'][email]'],
				// 											'mobile' => $main_member_form_data['main_member_array['.$i.'][mobile]'],
				// 											'is_whatsapp' => $main_member_form_data['main_member_array['.$i.'][is_whatsapp]'],
				// 											'skype' => $main_member_form_data['main_member_array['.$i.'][skype]'],
				// 											'telephone' => $main_member_form_data['main_member_array['.$i.'][telephone]'],
				// 											'main_member' => $main_member_form_data['main_member_array['.$i.'][main_member]'],
				// 											'other_member' => 'No',
				// 											'decision_maker' => $main_member_form_data['main_member_array['.$i.'][decision_maker]']
				// 										);

				// 		} else {
				// 			if( $main_member_form_data['main_member_array['.$i.'][name]'] != '' || $main_member_form_data['main_member_array['.$i.'][email]'] != '' || $main_member_form_data['main_member_array['.$i.'][mobile]'] != '' || $main_member_form_data['main_member_array['.$i.'][is_whatsapp]'] != '' || $main_member_form_data['main_member_array['.$i.'][skype]'] != '') {

				// 				$insert_member_array[] = array(
				// 										'lead_id' => $lead_id,
				// 										'member_name' => $main_member_form_data['main_member_array['.$i.'][name]'],
				// 										'designation' => $main_member_form_data['main_member_array['.$i.'][designation]'],
				// 										'email' => $main_member_form_data['main_member_array['.$i.'][email]'],
				// 										'mobile' => $main_member_form_data['main_member_array['.$i.'][mobile]'],
				// 										'is_whatsapp' => $main_member_form_data['main_member_array['.$i.'][is_whatsapp]'],
				// 										'skype' => $main_member_form_data['main_member_array['.$i.'][skype]'],
				// 										'telephone' => $main_member_form_data['main_member_array['.$i.'][telephone]'],	
				// 										'main_member' => $main_member_form_data['main_member_array['.$i.'][main_member]'],
				// 										'other_member' => 'No',
				// 										'decision_maker' => $main_member_form_data['main_member_array['.$i.'][decision_maker]']
				// 										);
				// 			}
				// 		}
				// 	}
				// 	$other_member_form_data = array_column($this->input->post('invoice_other_member_form_data'), 'value', 'name');
				// 	// echo "<pre>";print_r($member_form_data);echo"</pre><hr>";exit;
				// 	for ($i=0; $i < (count($other_member_form_data))/10; $i++) { 
						
				// 		if(!empty($other_member_form_data['other_member_array['.$i.'][details_id]'])) {

				// 			$update_member_array[] = array(
				// 											'details_id' => $other_member_form_data['other_member_array['.$i.'][details_id]'],
				// 											'member_name' => $other_member_form_data['other_member_array['.$i.'][name]'],
				// 											'designation' => $other_member_form_data['other_member_array['.$i.'][designation]'],
				// 											'email' => $other_member_form_data['other_member_array['.$i.'][email]'],
				// 											'mobile' => $other_member_form_data['other_member_array['.$i.'][mobile]'],
				// 											'is_whatsapp' => $other_member_form_data['other_member_array['.$i.'][is_whatsapp]'],
				// 											'skype' => $other_member_form_data['other_member_array['.$i.'][skype]'],
				// 											'telephone' => $other_member_form_data['other_member_array['.$i.'][telephone]'],
				// 											'main_member' => 'No',
				// 											'other_member' => 'Yes',
				// 										);

				// 		} else {
				// 			if( $other_member_form_data['other_member_array['.$i.'][name]'] != '' || $other_member_form_data['other_member_array['.$i.'][email]'] != '' || $other_member_form_data['other_member_array['.$i.'][mobile]'] != '' || $other_member_form_data['other_member_array['.$i.'][is_whatsapp]'] != '' || $other_member_form_data['other_member_array['.$i.'][skype]'] != '') {

				// 				$insert_member_array[] = array(
				// 										'lead_id' => $lead_id,
				// 										'member_name' => $other_member_form_data['other_member_array['.$i.'][name]'],
				// 										'designation' => $other_member_form_data['other_member_array['.$i.'][designation]'],
				// 										'email' => $other_member_form_data['other_member_array['.$i.'][email]'],
				// 										'mobile' => $other_member_form_data['other_member_array['.$i.'][mobile]'],
				// 										'is_whatsapp' => $other_member_form_data['other_member_array['.$i.'][is_whatsapp]'],
				// 										'skype' => $other_member_form_data['other_member_array['.$i.'][skype]'],
				// 										'telephone' => $other_member_form_data['other_member_array['.$i.'][telephone]'],
				// 										'main_member' => 'No',
				// 										'other_member' => 'Yes'
				// 										);
				// 			}
				// 		}
				// 	}
				// 	// echo "<pre>";print_r($update_lead_management_data);echo"</pre><hr>";
				// 	// echo "<pre>";print_r($update_member_array);echo"</pre><hr>";
				// 	// echo "<pre>";print_r($insert_member_array);echo"</pre><hr>";
				// 	// exit;
				// 	if(!empty($update_lead_management_data)) {

				// 		$this->internal_data_model->update_data('lead_management', $update_lead_management_data, array('lead_id'=>$lead_id));
				// 	}
				// 	// check already inserted member and get its count
				// 	$lead_management_invoice_member_data = $this->internal_data_model->get_dynamic_data('*', array('lead_id'=>$lead_id, 'delete_status' => 'Active'), 'lead_management_details');
				// 	if(count($lead_management_invoice_member_data) > 0){
				// 		foreach ($lead_management_invoice_member_data as $lead_management_invoice_member_data_value) {
					
				// 			// checking whether record present in db is present in post request or not
				// 			// if not present then deleting it from db
				// 			if(!in_array($lead_management_invoice_member_data_value['details_id'], array_column($update_member_array, 'details_id'))) {
				// 				$this->internal_data_model->update_data('lead_management_details', array('delete_status'=>'Inactive'), array('details_id'=>$lead_management_invoice_member_data_value['details_id']));
				// 			}
				// 		}	
				// 		if(!empty($update_member_array)) {
				// 			$this->internal_data_model->update_data_batch('lead_management_details', $update_member_array, 'details_id');
				// 		}
				// 	}
				// 	if(!empty($insert_member_array)) {
				// 		foreach ($insert_member_array as $insert_member_array_value) {
							
				// 			$this->internal_data_model->insert_data('lead_management_details', $insert_member_array_value);
				// 		}
				// 		// echo "<pre>";print_r($insert_member_array);echo"</pre><hr>";exit;
				// 	}
				// break;	

				case 'update_competitor_rank_invoice_form_data':
		
					$update_array = array();
					$update_array = array_column($this->input->post('invoice_form_data'), 'value', 'name');
					$update_member_array = array_column($this->input->post('invoice_main_member_form_data'), 'value', 'name');
					$update_other_member_array = array_column($this->input->post('invoice_other_member_form_data'), 'value', 'name');

					$update_array['modified_on'] = date('Y-m-d H:i:s');
					$update_array['modified_by'] = $this->session->userdata('user_id');
					$comp_mst_id = $this->input->post('customer_id');
					
					$this->internal_data_model->update_data('customer_mst', $update_array, array('id'=>$comp_mst_id));
					$this->internal_data_model->delete_Data('customer_dtl', array('comp_mst_id'=>$comp_mst_id));
					
					//Update member data
					for($i=1; $i <= (count($this->input->post('invoice_main_member_form_data'))/10); $i++){
						
						$member_array[] = array(
							
							'comp_mst_id' => $comp_mst_id,
							'comp_dtl_id' => $update_member_array['comp_dtl_id_'.$i],
							'member_name'=>$update_member_array['member_name_'.$i],
							'designation'=>$update_member_array['designation_'.$i],
							'email'=>$update_member_array['email_'.$i],
							'mobile'=>$update_member_array['mobile_'.$i],
							'is_whatsapp'=>$update_member_array['is_whatsapp_'.$i],
							'skype'=>$update_member_array['skype_'.$i],
							'telephone'=>$update_member_array['telephone_'.$i],
							'main_buyer'=>$update_member_array['main_buyer_'.$i],
							'decision_maker'=>$update_member_array['decision_maker_'.$i],
							'other_member' => $update_member_array['other_member'.$i]='No',
						);
						
					}
					if (!empty($member_array)) {
						$this->internal_data_model->insert_data('customer_dtl', $member_array, 'batch');
					}

					// update other member detail
					for ($i=1; $i <= (count($this->input->post('invoice_other_member_form_data'))/8); $i++) {
						
						$other_member_array[] = array(
							
							'comp_mst_id' => $comp_mst_id,
							'comp_dtl_id' => $update_other_member_array['comp_dtl_id_'.$i],
							'member_name' => $update_other_member_array['member_name_'.$i],
							'designation' => $update_other_member_array['designation_'.$i],
							'email' => $update_other_member_array['email_'.$i],
							'mobile' => $update_other_member_array['mobile_'.$i],
							'is_whatsapp' => $update_other_member_array['is_whatsapp_'.$i],
							'skype' => $update_other_member_array['skype_'.$i],
							'telephone' => $update_other_member_array['telephone_'.$i],
							'other_member' => $update_other_member_array['other_member'.$i]='Yes',
						);						
					}
					if (!empty($other_member_array)) {
						$this->internal_data_model->insert_data('customer_dtl', $other_member_array, 'batch');
					}
				break;

				case 'competitor_rank_invoice_advance_searh_form_submit':
					
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					$where_array = array();
					$where_array['customer_data.source'] = "'internal'";
					$order_by_array = array('column_name' => 'customer_data.rank', 'column_value' => 'asc');
					$limit = 10;
					$offset = 00;
					// search by filter
					if($this->session->userdata('role') == 5) {
						$where_array['assigned_to'] = "'".$this->session->userdata('user_id')."'";
					}
					if(!empty($this->input->post('competitor_search_company_name'))) {
						$where_array['customer_mst.name LIKE'] = "'%".$this->input->post('competitor_search_company_name')."%'";
					}
					if(!empty($this->input->post('competitor_search_country_name'))) {
						$where_array['country_mst.name'] = "'".$this->input->post('competitor_search_country_name')."'";
					}
					if(!empty($this->input->post('competitor_search_region_id'))) {

						$all_country_with_same_region =  $this->internal_data_model->get_dynamic_data('*', array('status'=>'Active', 'region_id'=>$this->input->post('competitor_search_region_id')), 'country_mst');
						$where_array['country_mst.name IN'] = '("'.implode('", "', array_unique(array_column($all_country_with_same_region, 'name'))).'")';
					}
					$handling_blank_filled_searched[] = array('field_name' => 'competitor_search_lead_type', 'column_name' => 'customer_mst.lead_type', 'blank_column_name' => 'customer_mst.lead_type is', 'blank_column_value' => 'NULL OR customer_mst.lead_type = 0');

					$handling_blank_filled_searched[] = array('field_name' => 'competitor_search_lead_stage', 'column_name' => 'customer_mst.lead_stage', 'blank_column_name' => 'customer_mst.lead_stage is', 'blank_column_value' => 'NULL');
					
					$handling_blank_filled_searched[] = array('field_name' => 'competitor_search_assigned_person', 'column_name' => 'customer_mst.assigned_to', 'blank_column_name' => 'customer_mst.assigned_to', 'blank_column_value' => 0);

					foreach ($handling_blank_filled_searched as $single_value) {
						
						if($this->input->post($single_value['field_name']) != '') {
							if($this->input->post($single_value['field_name']) == 'blank') {

								$where_array['('.$single_value['blank_column_name']] = $single_value['blank_column_value'].')';
							} else {

								$where_array['('.$single_value['column_name']] = $this->input->post($single_value['field_name']).')';
							}
						}	
					}
					// echo "<pre>";print_r($where_array);echo"</pre><hr>";exit;
					// end
					// order by 
					if(!empty($this->input->post('competitor_sort_name')) && !empty($this->input->post('competitor_sort_value'))) {
						$order_by_array['column_name'] = $this->input->post('competitor_sort_name');
						$order_by_array['column_value'] = $this->input->post('competitor_sort_value');
					}
					// end
					// limit and offser for paggination
					if(!empty($this->input->post('limit'))) {
						$limit = $this->input->post('limit');
					}
					if(!empty($this->input->post('offset'))) {
						$offset = $this->input->post('offset');
					}
					$data = $this->create_competitor_ranks_invoice_data($where_array, $order_by_array, $limit, $offset);
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
					$data['search_form']['customer_name'] = $this->input->post('competitor_search_company_name');
					$data['search_form']['lead_type_id'] = $this->input->post('competitor_search_lead_type');
					$data['search_form']['lead_stage_id'] = $this->input->post('competitor_search_lead_stage');
					$data['search_form']['country_name'] = $this->input->post('competitor_search_country_name');
					$data['search_form']['user_id'] = $this->input->post('competitor_search_assigned_person');
					$data['search_form']['region_id'] = $this->input->post('competitor_search_region_id');
					// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
					$data['sorting'] = $this->create_sorting_reponse($order_by_array);	
					$response['html_head'] = $this->load->view('internal_data/competitor_rank_head', $data, true);
					$response['html_body'] = $this->load->view('internal_data/competitor_rank_body', $data, true);								
					$response['internal_paggination'] = $this->load->view('internal_data/competitor_rank_paggination', $data, true);
					
					// echo "<pre>";print_r($response);echo"</pre><hr>";exit;
				break;	

				// case 'add_follow_up_history':
					
				// 	// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";
				// 	$insert_data['lead_id'] = $this->input->post('lead_id');
				// 	foreach ($this->input->post('add_follow_up_history_form_data') as $form_data) {
						
				// 		$insert_data[$form_data['name']] = $form_data['value'];
				// 		if($form_data['name'] == 'contact_date') {
				// 			$insert_data['contact_date'] = date('Y-m-d',strtotime($form_data['value']));
				// 		}
				// 	}
				// 	// echo "<pre>";print_r($insert_data);echo"</pre><hr>";
				// 	if(!empty($insert_data)) {

				// 		$this->internal_data_model->insert_data('lead_management_connect', $insert_data);
				// 		$insert_array_for_daily_report['user_id'] = $this->session->userdata('user_id');
				// 		$insert_array_for_daily_report['client_id'] = $insert_data['lead_id'];
				// 		$lead_details = $this->common_model->get_dynamic_data_sales_db('IMPORTER_NAME, COUNTRY_OF_DESTINATION', array('lead_id' => $insert_data['lead_id']), 'lead_management','row_array');
				// 		$insert_array_for_daily_report['client_name'] = '';
				// 		$insert_array_for_daily_report['country'] = '';
				// 		$insert_array_for_daily_report['lead_name'] = 'internal data';
				// 		if(!empty($lead_details)){

				// 			$insert_array_for_daily_report['client_name'] = $lead_details['IMPORTER_NAME'];
				// 			$insert_array_for_daily_report['country'] = $lead_details['COUNTRY_OF_DESTINATION'];
				// 		}
				// 		$insert_array_for_daily_report['member_id'] = $insert_data['details_id'];
				// 		$member_details = $this->common_model->get_dynamic_data_sales_db('member_name', array('details_id' => $insert_data['details_id'], 'lead_id' => $insert_data['lead_id']), 'lead_management_details','row_array');
				// 		$insert_array_for_daily_report['member_name'] = '';
				// 		if(!empty($member_details)){

				// 			$insert_array_for_daily_report['member_name'] = $member_details['member_name'];
				// 		}
				// 		$insert_array_for_daily_report['contact_date'] = date('Y-m-d', strtotime($insert_data['contact_date'])).' '.date('h:i:s');
				// 		$insert_array_for_daily_report['contact_mode'] = $insert_data['contact_mode'];
				// 		$insert_array_for_daily_report['comments'] = $insert_data['comments'];
				// 		$insert_array_for_daily_report['email_sent'] = $insert_data['email_status'];
						
				// 		// echo "<pre>";print_r($insert_array_for_daily_report);echo"</pre><hr>";exit;
				// 		$this->common_model->insert_data_sales_db('daily_work_sales_on_lead_data', $insert_array_for_daily_report);
				// 	}
				// break;

				// case 'add_task':
					
				// 	// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
				// 	$insert_data = array(
				// 		'lead_id' => $this->input->post('lead_id'),
				// 		'member_id' => $this->input->post('details_id'),
				// 		'lead_source' => 'invoice',
				// 		'task_detail' => $this->input->post('task_detail'),
				// 		'deadline' => date('Y-m-d H:i:s', strtotime($this->input->post('deadline'))),
				// 		'created_by' => $this->session->userdata('user_id'),
				// 		'assigned_to' => $this->session->userdata('user_id'),
				// 		'status' => 'Open',
				// 		'entered_on' => date('Y-m-d H:i:s')
				// 	);
				// 	$this->internal_data_model->insert_data('tasks', $insert_data);
				// break;

				// case 'get_member_list_modal_body':
						
				// 	// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;	
				// 	$customer_id = $this->input->post('customer_id');	
					
				// 	$member_data = $this->internal_data_model->get_data('*', array('comp_mst_id'=> $customer_id, 'status' => 'Active'), 'customer_dtl');
				// 	$data = array();
				// 	$data['main_buyer'] = array();	
				// 	$data['other_member'] = array();	
				// 	foreach ($member_data as $member_data_value) {
						
				// 		if($member_data_value['other_member'] == 'No') {
							
				// 			$data['main_buyer'][] = $member_data_value;
						
				// 		} else if ($member_data_value['main_buyer'] == 'No') {
							
				// 			$data['other_member'][] = $member_data_value;
				// 		}
				// 	}
				// 	$response['member_html_body'] = $this->load->view('internal_data/member_modal_body', $data, true);					
				// break;
				
				case 'get_member_body':

					// echo "<pre>"; print_r($this->input->post()); "</pre>";exit;
					if(!empty($this->input->post('next_count_number'))){
						
						$data['next_count_number'] = $this->input->post('next_count_number');
						$data['connected_on'] = '-';
						$data['connect_mode'] = '-';
						$response['member_body_detail']	= $this->load->view('internal_data/add_member_body', $data, true);
						// echo "<pre>"; print_r($response); "</pre>";exit;
					}
				break;

				case 'get_other_member_body':
					// echo "<pre>"; print_r($this->input->post()); "</pre>";exit;
					if(!empty($this->input->post('next_number'))){
						
						$data['next_number'] = $this->input->post('next_number');
						$data['connected_on'] = '-';
						$data['connect_mode'] = '-';
						// echo "<pre>"; print_r($data); "</pre>";exit;
						$response['other_member_body_detail']	= $this->load->view('internal_data/add_other_member_body', $data, true);
					}
				break;

				case 'delete_member':
				case 'delete_other_member':	
					// echo "<pre>";print_r($this->input->post('comp_dtl_id'));echo"</pre><hr>";exit;
					if(!empty($this->input->post('comp_dtl_id'))){

						$this->internal_data_model->delete_Data('customer_dtl', array('comp_mst_id' => $this->input->post('comp_mst_id'), 'comp_dtl_id'=> $this->input->post('comp_dtl_id')));
					}
				break;

				case 'member_followup_modal':
				case 'other_member_followup_modal':
					
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					$response['follow_up_html'] = "";
					$response['comp_dtl_id'] = $this->input->post('comp_dtl_id');
					if(!empty($this->input->post('comp_dtl_id'))){
							
						$data['connected_on'] = date('d-m-Y');
						$data['comp_mst_id'] = $this->input->post('comp_mst_id');
							
						//getting follow up history
						$data['follow_up_details'] = $this->internal_data_model->get_all_conditional_data_sales_db('*',array('comp_detail_id'=>$this->input->post('comp_dtl_id'), 'comp_mst_id'=>$this->input->post('comp_mst_id')),'customer_connect','result_array',array(), array('column_name'=>'connected_on', 'column_value'=>'desc'), array(), 0,0);

						$user_list = array_column($this->internal_data_model->get_all_data('user_id, name', array(), 'users'), 'name', 'user_id');
						$user_list['']='';
						$user_list[0]='';

						foreach ($data['follow_up_details'] as $follow_up_details_key => $single_follow_up) {

							$data['follow_up_details'][$follow_up_details_key]['user_name'] = $user_list[$single_follow_up['entered_by']]; 
						}
												
						// echo "<pre>";print_r($data);echo"</pre><hr>";
						$response['follow_up_html'] = $this->load->view('internal_data/follow_up_model_body', $data, true);
						$response['comp_dtl_id'] = $this->input->post('comp_dtl_id');
					}
				break;

				case 'save_followup':
					// echo "<pre>";print_r($this->input->post());echo"</pre><hr>";exit;
					if(!empty($this->input->post('comp_dtl_id'))){

						$form_data = array_column($this->input->post('add_followup_data_form'), 'value', 'name');
						$comp_dtl_id = $this->input->post('comp_dtl_id');
						
						$insert_array = array(
												'comp_detail_id' => $comp_dtl_id,
												'comp_mst_id' => $form_data['comp_mst_id'],
												'connect_mode' => $form_data['connect_mode'],
												'email_sent' => $form_data['email_sent'],
												'comments' => $form_data['comments'],
												'connected_on' => $form_data['connected_on'],
												'entered_on' => date('Y-m-d H:i:s'),
												'entered_by' => $this->session->userdata('user_id'),
						); 
						// echo "<pre>";print_r($insert_array);echo"</pre><hr>";
						if(!empty($insert_array)){

							$this->internal_data_model->insert_data('customer_connect', $insert_array);
						}
					}
				break;

				default:
					$response['status'] = 'failed';	
					$response['message'] = "call type not found";
				break;
			}
			echo json_encode($response);

		} else{
			die('access is not allowed to this function');
		}
	}

	private function create_sorting_reponse($sort_array) {

		$sorting_static_array = array('rank', 'customer_name', 'lead_stage', 'assigned_to');
		$sorting = array();
		foreach ($sorting_static_array as $sorting_static_value) {
			
			$sorting[strtolower($sorting_static_value)]['class_name'] = 'sorting';
			$sorting[strtolower($sorting_static_value)]['value'] = 'asc';
			if($sort_array['column_name'] == $sorting_static_value) {
				if($sort_array['column_value'] == 'asc') {

					$sorting[strtolower($sorting_static_value)]['class_name'] = 'sorting_asc';
					$sorting[strtolower($sorting_static_value)]['value'] = 'desc';
				} else {
					$sorting[strtolower($sorting_static_value)]['class_name'] = 'sorting_desc';
				}			
			}
		}
		return $sorting;
	}

	private function create_competitor_ranks_invoice_update_data($customer_id) { 
		// getting lead management data
		if(!empty($customer_id)){

			$data['customer_data'] = $this->internal_data_model->get_all_data('*', array('id'=>$customer_id), 'customer_mst', 'row_array');

			if(!empty($data['customer_data'])){
				
				$data['customer_details'] = $this->internal_data_model->get_all_data('*', array('customer_mst_id'=>$customer_id, 'source'=>'internal'), 'customer_data', 'row_array');
				
				$data['last_contacted_date'] = $this->internal_data_model->get_all_conditional_data_sales_db('*', array('comp_mst_id'=>$customer_id), 'customer_connect', 'row_array', array(), array('column_name'=>'connected_on', 'column_value'=>'desc'), array(),0,0);	
			}

			$data['main_member_data'] = $this->get_member_detail($customer_id);
			$data['other_member_data'] = $this->get_other_member_detail($customer_id);
			// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		
			$data['next_count_number'] = count($data['main_member_data'])+1;
			$data['next_number'] = count($data['other_member_data'])+1;
		}

		$data['sales_person'] = $this->internal_data_model->get_sales_person('name, user_id');
		$data['lead_type'] = $this->internal_data_model->get_dynamic_data('*', array(), 'lead_type');
		$data['lead_stages'] = $this->internal_data_model->get_dynamic_data('*', array(), 'lead_stages');
		$data['lead_stage_reasons'] = $this->internal_data_model->get_dynamic_data('*', array('status'=> 'Active'), 'lead_stage_reasons');

		
		// $data['follow_up_details'] = $this->internal_data_model->get_dynamic_data('comp_detail_id, connected_on, connect_mode, email_sent, comments', array('comp_mst_id'=> $customer_id), 'customer_connect');
		$data['region'] = $this->internal_data_model->get_dynamic_data('*', array(), 'region_mst');
		$data['country'] = $this->internal_data_model->get_dynamic_data('*', array(), 'country_mst');
		return $data;		
	}

	private function create_competitor_ranks_invoice_data($where_array, $order_by_array, $limit, $offset) {

		// echo "<pre>";print_r($where_array);echo"</pre><hr>";exit;
		$data = array();
		$competitor_data = $this->internal_data_model->get_competitor_rank_invoice($where_array, $order_by_array, $limit, $offset);
		// echo "<pre>";print_r($competitor_data);echo"</pre><hr>";exit;
		$lead_type_table_data = $this->internal_data_model->get_dynamic_data('*', array(), 'lead_type');
		$lead_type_data = array_column($lead_type_table_data, 'type_name', 'lead_type_id');
		$sales_person_table_data = $this->internal_data_model->get_sales_person('name, user_id');
		$sales_person_data = array_column($sales_person_table_data, 'name', 'user_id');
		
		foreach ($competitor_data['competitor_ranks_list'] as $competitor_data_key => $competitor_data_value) {
			// echo "<pre>";print_r($competitor_data_value);echo"</pre><hr>";exit;
			
			$data['competitor_ranks_list'][$competitor_data_key]['id'] = $competitor_data_value['id'];
			// $data['competitor_ranks_list'][$competitor_data_key]['client_id'] = $competitor_data_value['client_id'];
			$data['competitor_ranks_list'][$competitor_data_key]['rank'] = $competitor_data_value['rank'];
			$data['competitor_ranks_list'][$competitor_data_key]['customer_name'] = $competitor_data_value['customer_name'];
			$data['competitor_ranks_list'][$competitor_data_key]['country_name'] = $competitor_data_value['country_name'];
			$data['competitor_ranks_list'][$competitor_data_key]['last_buying_date'] = date('F, Y', strtotime($competitor_data_value['last_purchased']));
			$data['competitor_ranks_list'][$competitor_data_key]['country'] = $competitor_data_value['country_id'];
			$data['competitor_ranks_list'][$competitor_data_key]['region'] = $competitor_data_value['region_id'];
			$data['competitor_ranks_list'][$competitor_data_key]['website'] = $competitor_data_value['website'];
			$data['competitor_ranks_list'][$competitor_data_key]['no_of_employees'] = $competitor_data_value['no_of_employees'];
			$data['competitor_ranks_list'][$competitor_data_key]['country_flag_img_name'] = $competitor_data_value['flag_name'];

			$data['competitor_ranks_list'][$competitor_data_key]['lead_type'] = '';
			if(!empty($competitor_data_value['lead_type'])) {
				$data['competitor_ranks_list'][$competitor_data_key]['lead_type'] = $lead_type_data[$competitor_data_value['lead_type']];
			}
			$data['competitor_ranks_list'][$competitor_data_key]['lead_stage'] = $competitor_data_value['lead_stage'];
			$data['competitor_ranks_list'][$competitor_data_key]['sales_person_name'] = '';
			if(!empty($competitor_data_value['assigned_to']) && $sales_person_data[$competitor_data_value['assigned_to']]) {
				$data['competitor_ranks_list'][$competitor_data_key]['sales_person_name'] = ucfirst(strtolower($sales_person_data[$competitor_data_value['assigned_to']]));
			}
			
			// getting last contacted date, comment and contact mode
			$last_contacted_details = $this->internal_data_model->get_all_conditional_data_sales_db('connected_on, connect_mode, comments', array('comp_mst_id'=>$competitor_data_value['id']),'customer_connect','row_array', array(),array('column_name'=>'connected_on', 'column_value'=>'desc'),array(),0,0);

			$last_contacted_data = $this->change_response_for_last_contacted($last_contacted_details);

			$data['competitor_ranks_list'][$competitor_data_key]['connected_on'] = $last_contacted_data['last_contacted'];
			$data['competitor_ranks_list'][$competitor_data_key]['comments'] = $last_contacted_data['comments'];
			$data['competitor_ranks_list'][$competitor_data_key]['connect_mode'] = $last_contacted_data['connect_mode'];

			// getting member count
			$main_member_count = $this->internal_data_model->get_dynamic_data('*', array('comp_mst_id' => $competitor_data_value['id'], 'main_buyer' => 'Yes', 'other_member' => 'No', 'status' => 'Active'), 'customer_dtl');

			$other_member_count = $this->internal_data_model->get_dynamic_data('*', array('comp_mst_id' => $competitor_data_value['id'], 'main_buyer' => 'No', 'other_member' => 'Yes', 'status' => 'Active'), 'customer_dtl');

			$get_main_member_details = $this->internal_data_model->get_data('*', array('comp_mst_id' => $competitor_data_value['id'], 'main_buyer' => 'Yes', 'other_member' => 'No', 'status' => 'Active'), 'customer_dtl', 'row_array', array(),array('column_name' => 'entered_on', 'column_value' => 'Desc'));
			
			$data['competitor_ranks_list'][$competitor_data_key]['member_count'] = count($main_member_count);
			$data['competitor_ranks_list'][$competitor_data_key]['non_member_count'] = count($other_member_count);
			$data['competitor_ranks_list'][$competitor_data_key]['comp_dtl_id'] = '';
			$data['competitor_ranks_list'][$competitor_data_key]['member_name'] = '';
			$data['competitor_ranks_list'][$competitor_data_key]['email'] = '';
			$data['competitor_ranks_list'][$competitor_data_key]['designation'] = '';
			$data['competitor_ranks_list'][$competitor_data_key]['mobile'] = '';
			if(!empty($get_main_member_details)) {
				$data['competitor_ranks_list'][$competitor_data_key]['comp_dtl_id'] = $get_main_member_details['comp_dtl_id'];
				$data['competitor_ranks_list'][$competitor_data_key]['member_name'] = $get_main_member_details['member_name'];
				$data['competitor_ranks_list'][$competitor_data_key]['email'] = $get_main_member_details['email'];
				$data['competitor_ranks_list'][$competitor_data_key]['designation'] = $get_main_member_details['designation'];
				$data['competitor_ranks_list'][$competitor_data_key]['mobile'] = $get_main_member_details['mobile'];	
			}						
		}

		$data['paggination_data'] = $competitor_data['paggination_data'];
		$data['paggination_data']['limit'] = $limit;
		$data['paggination_data']['offset'] = $offset;

		$data['country_list'] = array();
		$data['country_list'] = $this->internal_data_model->get_country_data('name', array('name !=' => ''), 'country_mst', 'result_array', array(), array(), array('name'));

		$data['region'] = $this->internal_data_model->get_dynamic_data('*', array(), 'region_mst');

		$data['lead_type'] = $this->internal_data_model->get_dynamic_data('*', array(), 'lead_type');
		array_unshift($data['lead_type'], array('lead_type_id' => 'blank', 'type_name'=> 'Blank Lead Type'));
		
		$data['stage_details'] = $this->internal_data_model->get_dynamic_data('*', array(), 'lead_stages');
		array_unshift($data['stage_details'], array('lead_stage_id' => 'blank', 'stage_name'=> 'Blank Lead Stage'));
		
		$data['sales_person'] = $sales_person_table_data;
		array_unshift($data['sales_person'], array('name'=> 'Blank User Name', 'user_id' => 'blank'));
		
		$search_engine= $this->internal_data_model->get_dynamic_data('*', array('user_id'=>$this->session->userdata('user_id')), 'search_engine_primary_data');
		$data['uri']=$this->internal_data_model->get_dynamic_data('*', array('access_type'=>$search_engine[0]['access_type'],'lead_name'=>'internal_data'), 'search_engine_access_url');
        
		// echo "<pre>";print_r($data);echo"</pre><hr>";exit;
		return $data;	
	}

	private function create_invoice_details($where_array, $order_by_array, $limit, $offset) {

		$data = array();
		$invoice_data = $this->internal_data_model->get_invoice_details($where_array, $order_by_array, $limit, $offset);
		// echo "<pre>";print_r($invoice_data);echo"</pre><hr>";exit;
		$lead_type_table_data = $this->internal_data_model->get_dynamic_data('*', array(), 'lead_type');
		$lead_type_data = array_column($lead_type_table_data, 'type_name', 'lead_type_id');
		$sales_person_table_data = $this->internal_data_model->get_dynamic_data('*', array('role'=>5), 'users');
		$sales_person_data = array_column($sales_person_table_data, 'name', 'user_id');

		foreach ($invoice_data['invoice_list'] as $invoice_data_key => $invoice_data_value) {
			
			$data['invoice_list'][$invoice_data_key]['data_category'] = strtolower($invoice_data_value['data_category']);
			$data['invoice_list'][$invoice_data_key]['company_name'] = $invoice_data_value['IMPORTER_NAME'];
			$data['invoice_list'][$invoice_data_key]['last_buying_date'] = date('F, Y', strtotime($invoice_data_value['LAST_PURCHASED']));
			$data['invoice_list'][$invoice_data_key]['rank'] = $invoice_data_value['RANK'];
			$data['invoice_list'][$invoice_data_key]['no_of_employees'] = $invoice_data_value['no_of_employees'];
			$data['invoice_list'][$invoice_data_key]['sales_person_name'] = ucfirst(strtolower($sales_person_data[$invoice_data_value['assigned_to']]));

			// getting lead type value
			$data['invoice_list'][$invoice_data_key]['lead_type'] = '';
			if(!empty($invoice_data_value['lead_type'])){
				$data['invoice_list'][$invoice_data_key]['lead_type'] = $lead_type_data[$invoice_data_value['lead_type']];
			}
			// getting last contacted date, comment and contact mode
			$last_contacted = $this->internal_data_model->get_marketing_db_data('connected_on, connect_mode, comments', array('lead_id' => $invoice_data_value['lead_mst_id']), 'lead_connects', 0,  0, 'row_array', array('column_name' => 'connected_on', 'column_value' => 'desc'));
			$last_contacted_data = $this->change_response_for_last_contacted($last_contacted);			
			$data['invoice_list'][$invoice_data_key]['last_contacted'] = $last_contacted_data['last_contacted'];
			$data['invoice_list'][$invoice_data_key]['comments'] = $last_contacted_data['comments'];
			$data['invoice_list'][$invoice_data_key]['connect_mode'] = $last_contacted_data['connect_mode'];

			// getting member count
			$members = $this->internal_data_model->get_marketing_db_data('*', array('lead_mst_id' => $invoice_data_value['lead_mst_id']), 'lead_detail');
			$member_data = $this->change_response_for_member($members);			
			$data['invoice_list'][$invoice_data_key]['member_count'] = $member_data['member_count'];
			$data['invoice_list'][$invoice_data_key]['non_member_count'] = $member_data['non_member_count'];
			$data['invoice_list'][$invoice_data_key]['lead_dtl_id'] = $member_data['lead_dtl_id'];
			$data['invoice_list'][$invoice_data_key]['member_name'] = $member_data['member_name'];
			$data['invoice_list'][$invoice_data_key]['email'] = $member_data['email'];
			$data['invoice_list'][$invoice_data_key]['designation'] = $member_data['designation'];
			$data['invoice_list'][$invoice_data_key]['mobile'] = $member_data['mobile'];

			//getting country flag
			$country_details = $this->internal_data_model->get_marketing_db_data('*', array('country' => $invoice_data_value['COUNTRY_OF_DESTINATION']), 'country_flags', 0, 0, 'row_array');
			$data['invoice_list'][$invoice_data_key]['country_flag_img_name'] = $country_details['flag_name'];
			$data['invoice_list'][$invoice_data_key]['country_name'] = $invoice_data_value['COUNTRY_OF_DESTINATION'];

		}
		$data['stage_details'] = $this->internal_data_model->get_dynamic_data('*', array(), 'lead_stages');
		// $data['country_details'] = $this->internal_data_model->get_dynamic_data_sales_db("IMPORTER_NAME", $where_array, 'lead_mst'); 
		$data['paggination_data'] = $invoice_data['paggination_data'];
		$data['paggination_data']['limit'] = $limit;
		$data['paggination_data']['offset'] = $offset;
		// echo "<pre>";print_r($data);echo"</pre><hr>";die('debug');
		return $data;	
	}

	private function change_response_for_last_contacted($data) {
					
		$return_array = array('last_contacted' => '', 'comments' => '', 'connect_mode' => '');
		if(!empty($data)){

			$date1 = date_create($data['connected_on']);
			$date2 = date_create(date('Y-m-d'));
			$diff_obj = date_diff($date1, $date2);
			$diff = $diff_obj->format("%a");
			if($diff < 8){
		 		$return_array['last_contacted'] = $diff.' days ago';
			}else if($diff < 30){
				$weeks = round($diff / 7);
				$return_array['last_contacted'] = $weeks.' weeks ago';
			}else if($diff < 365){
				$months = round($diff / 30);
				$return_array['last_contacted'] = $months.' months ago';
			}else if($diff > 365){
				$years = round($diff / 365);
				$return_array['last_contacted'] = $years.' years ago';
			}
			$return_array['comments'] = $data['comments'];
			$return_array['connect_mode'] = $data['connect_mode'];
		}
		return $return_array;
	}

	private function change_response_for_member($members) {

		$return_array = array('member_count' => 00, 'non_member_count' => 00, 'lead_dtl_id' => '', 'member_name' => '', 'email' => '', 'designation' => '', 'mobile' => '');
		if(!empty($members)){
			foreach ($members as $mem) {
				if(strtolower($mem['main_buyer']) == 'yes' && $return_array['lead_dtl_id'] == null){
					$return_array['lead_dtl_id'] = $mem['lead_dtl_id'];
					$return_array['member_name'] = $mem['member_name'];
					$return_array['email'] = $mem['email'];
					$return_array['designation'] = $mem['designation'];
					$return_array['mobile'] = $mem['mobile'];
				}else{
					if(strtolower($mem['other_member']) == 'y'){
						$return_array['non_member_count']++;
					}else{
						$return_array['member_count']++;
					}
				}
			}
		}
		return $return_array;
	}

	private function get_member_detail($customer_id){
		
		$member_data= $this->internal_data_model->get_all_conditional_data_sales_db('*', array('comp_mst_id'=> $customer_id, 'other_member'=>'No', 'main_buyer'=> 'Yes', 'status'=>'Active'), 'customer_dtl','result_array', array(),array('column_name'=>'comp_dtl_id', 'column_value'=>'asc'),array(),0,0);
		
		if(!empty($member_data)){
				
			foreach($member_data as $member_key => $single_member_detail){	
	
				$member_lead_connect = $this->internal_data_model->get_all_conditional_data_sales_db('*', array('comp_detail_id'=>$single_member_detail['comp_dtl_id']), 'customer_connect','row_array', array(),array('column_name'=>'connected_on', 'column_value'=>'desc'),array(),0,0);
					
				if(!empty($member_lead_connect)){
						
					$member_data[$member_key]['connected_on'] = date('d-m-Y', strtotime($member_lead_connect['connected_on']));
						
					$member_data[$member_key]['connect_mode'] = $member_lead_connect['connect_mode'];				
				}else{
						
					$member_data[$member_key]['connected_on'] = '-';
					$member_data[$member_key]['connect_mode'] = '-';
				}					
			}
		}			
		// echo "<pre>";print_r($member_data);echo"</pre><hr>";exit;
		return $member_data;
	}

	private function get_other_member_detail($customer_id){

		$other_member= $this->internal_data_model->get_all_conditional_data_sales_db('*', array('comp_mst_id'=> $customer_id, 'other_member'=>'Yes', 'main_buyer'=> 'No', 'status'=>'Active'), 'customer_dtl','result_array', array(),array('column_name'=>'comp_dtl_id', 'column_value'=>'asc'),array(),0,0);
		
		if(!empty($other_member)){
			
			foreach($other_member as $other_member_key => $other_member_detail){	
					// echo "<pre>";print_r($other_member_detail);echo"</pre><hr>";exit;

					$other_member_lead_connect = $this->internal_data_model->get_all_conditional_data_sales_db('*', array('comp_mst_id'=> $customer_id, 'comp_detail_id'=>$other_member_detail['comp_dtl_id']), 'customer_connect','row_array', array(),array('column_name'=>'connected_on', 'column_value'=>'desc'),array(),0,0);
					// echo "<pre>";print_r($other_member_lead_connect);echo"</pre><hr>";exit;

					if(!empty($other_member_lead_connect)){

						$other_member[$other_member_key]['connected_on'] = date('d-m-y', strtotime($other_member_lead_connect['connected_on']));

						$other_member[$other_member_key]['connect_mode'] = $other_member_lead_connect['connect_mode'];
					}else{

						$other_member[$other_member_key]['connected_on'] = '-';	
						$other_member[$other_member_key]['connect_mode'] = '-';
					}
					
				}
			}
		// echo "<pre>";print_r($other_member);echo"</pre><hr>";exit;
		return $other_member;
	}


}