<style type="text/css">
	#kt_wrapper{
		padding-top: 3% !important;
	}
	.my-scroll{
      border:1px solid #e1e1e1;
      height: 670px;
      width:auto;
      overflow-y: auto;
    }
    .kt-aside--fixed .kt-wrapper {
        padding-left: 130px !important;
    }
</style>
<div class="kt-grid kt-grid--hor kt-grid--root">
	<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

			
			<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

				<!-- begin:: Content -->
				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
					
					<div class="kt-portlet kt-portlet--mobile">
						<div class="kt-portlet__head kt-portlet__head--lg">
							<div class="kt-portlet__head-label">
								<span class="kt-portlet__head-icon">
									<i class="kt-font-brand flaticon2-line-chart"></i>
								</span>
								<h3 class="kt-portlet__head-title">
									PRIMARY LEADS - INTERNAL TOP LEADS
								</h3>
							</div>
						</div>
						<div class="kt-portlet__body">

							<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
								<div class="row">
									<div class="col-sm-12">
										<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 1536px;">
											<thead id="competitor_rank_head">
						            			<?php $this->load->view('internal_data/competitor_rank_head');?>
											</thead>
											<tbody id="competitor_rank_body">
						            			<?php $this->load->view('internal_data/competitor_rank_body');?>
											</tbody>
										</table>
										<div id="kt_table_1_processing" class="dataTables_processing card" style="display: none; top: 10% !important;">Processing...</div>
									</div>
								</div>
								<div class="row" id="competitor_rank_paggination">
			            			<?php $this->load->view('internal_data/competitor_rank_paggination');?>
								</div>
							</div>
						</div>

					</div>

				</div>

				<!-- end:: Content -->
			</div>

		</div>
	</div>
</div>
<div class="modal fade" id="kt_modal_4_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Total Import : <span class="kt-font-info kt-font-xl kt-font-bold" id="total_amount"></span></h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<h5 class="modal-title" id="exampleModalLabel">Client Name : <span class="kt-font-info kt-font-xl kt-font-bold" id="client_name"></span></h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<div class="col-md-12">
					<div id="graph_invoice"></div>
				</div>
				<div class="col-md-12">
					<div id="graph_invoice_column"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Send message</button>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="invoice_member_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Member Details</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form id="invoice_member_body">
				
				</form>
			</div>
		</div>
	</div>
</div> 