<tr>
	<?php
		$charge_value = ''; 
		if(!empty($this->session->userdata('order_details')['different_charges'])) {
			$charge_value = implode(' , ', array_column($this->session->userdata('order_details')['different_charges'], 'charges_name'));
			// echo "<pre>";print_r($charge_value);echo"</pre><hr>";exit;
		}	 
	?>
	<td class="kt-font-danger kt-font-lg"><?php echo $charge_value;?></td>
	<td class="kt-font-success kt-font-xl kt-font-boldest purchase_order_gross_amount">
		<?php echo $this->session->userdata('order_details')['net_total'];?>
	</td>
	<td class="kt-font-success kt-font-xl kt-font-boldest purchase_order_gross_amount">
		<?php echo $this->session->userdata('order_details')['gross_total'];?>
	</td>
</tr>