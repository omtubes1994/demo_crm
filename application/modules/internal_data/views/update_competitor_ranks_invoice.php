<style type="text/css">
    .kt-aside--fixed .kt-wrapper {
        padding-left: 130px !important;
    }
    .kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper {
        padding-top: 25px !important;
    }
</style>
<div class="kt-grid kt-grid--hor kt-grid--root">
	<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
			<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
				<!-- begin:: Content -->
				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
					<div class="row">
						<div class="col">
							<div class="alert alert-light alert-elevate fade show" role="alert">
								<div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
								<div class="alert-text">
									Update Competitor Rank Invoice.
								</div>
							</div>
						</div>
					</div>

					<!--begin::Portlet-->
					<div class="kt-portlet">
						<div class="kt-portlet__body">
							<div class="kt-form__section kt-form__section--first">
								<!--begin::Form-->
								<form class="kt-form" id="update_lead_management_invoice_form">
									<div class="form-group row">
										<?php if($this->session->userdata('role') == 1){ ?>
										<label class="col-lg-2 col-form-label">Assigned To:</label>
										<div class="col-lg-2 form-group-sub">
											<select class="form-control" name="assigned_to">
												<option value="">Select</option>

												<?php foreach($sales_person as $sales_person_name) {?>
													<option value="<?php echo $sales_person_name['user_id'];?>" <?php echo ($sales_person_name['user_id'] == $customer_data['assigned_to']) ? 'selected':'' ?> ><?php echo ucfirst(strtolower($sales_person_name['name']));?></option>
												<?php } ?>
											</select>
										</div>
										<?php }?>
										<label class="col-lg-2 col-form-label">Company Name:</label>
										<div class="col-lg-2">
											<input type="text" class="form-control" name="name" value="<?php echo $customer_data['name'];?>">
										</div>
										<label class="col-lg-2 col-form-label">Country Name:</label>
										<div class="col-lg-2 form-group-sub">
											<select class="form-control" name="country_id" disabled="disabled">
												<option value="">Select</option>
												<?php 
													foreach($country as $country_name) {?>
														<option value="<?php echo $country_name['id'];?>" <?php echo ($country_name['id'] == $customer_data['country_id']) ? 'selected':'' ?> ><?php echo $country_name['name'];?>
														</option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-lg-2 col-form-label">Region:</label>
										<div class="col-lg-2 form-group-sub">
											<select class="form-control" name="region_id">
												<option value="">Select</option>
												<?php foreach($region as $region_name) {?>
													<option value="<?php echo $region_name['id'];?>" <?php echo ($region_name['id'] == $customer_data['region_id']) ? 'selected':'' ?> ><?php echo $region_name['name'];?></option>
												<?php } ?>
											</select>
										</div>
										<label class="col-lg-2 col-form-label">Website:</label>
										<div class="col-lg-2">
											<input type="text" class="form-control" name="website" value="<?php echo $customer_data['website'];?>">
										</div>
										<label class="col-lg-2 col-form-label">No Of Employees:</label>
										<div class="col-lg-2 form-group-sub">
											<select class="form-control" name="no_of_employees">
												<option value="">Select</option>
												<option value="1-9" <?php echo ($customer_data['no_of_employees'] == '1-9') ? 'selected':'' ?>>1-9</option>
												<option value="10-25" <?php echo ($customer_data['no_of_employees'] == '10-25') ? 'selected':'' ?>>10-2</option>
												<option value="25-50" <?php echo ($customer_data['no_of_employees'] == '25-50') ? 'selected':'' ?>>25-50</option>
												<option value="50-100" <?php echo ($customer_data['no_of_employees'] == '50-100') ? 'selected':'' ?>>50-100</option>
												<option value="100-200" <?php echo ($customer_data['no_of_employees'] == '100-200') ? 'selected':'' ?>>100-200</option>
												<option value="200-500" <?php echo ($customer_data['no_of_employees'] == '200-500') ? 'selected':'' ?>>200-500</option>
												<option value="500-1000" <?php echo ($customer_data['no_of_employees'] == '500-1000') ? 'selected':'' ?>>500-1000</option>
												<option value="1000+" <?php echo ($customer_data['no_of_employees'] == '1000+') ? 'selected':'' ?>>1000+</option>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-lg-2 col-form-label">Last Contact Date:</label>
										<div class="col-lg-2">
											<input type="text" class="form-control" value="<?php echo date('d-m-Y', strtotime($last_contacted_date['connected_on'])); ?>" readonly/>
										</div>
										<label class="col-lg-2 col-form-label">Lead Type:</label>
										<div class="col-lg-2 form-group-sub">
											<select class="form-control" name="lead_type">
												<option value="">Select</option>
												<?php foreach($lead_type as $lead_type_single) {?>
													<option value="<?php echo $lead_type_single['lead_type_id'];?>" <?php echo ($lead_type_single['lead_type_id'] == $customer_data['lead_type']) ? 'selected':'' ?> ><?php echo $lead_type_single['type_name'];?></option>
												<?php } ?>
											</select>
										</div>
										<label class="col-lg-2 col-form-label">Lead Stage:</label>
										<div class="col-lg-2 form-group-sub">
											<select class="form-control lead_stage" name="lead_stage">
												<option value="">Select</option>
												<?php foreach($lead_stages as $lead_stages_single) {?>
													<option value="<?php echo $lead_stages_single['lead_stage_id'];?>" <?php echo ($lead_stages_single['lead_stage_id'] == $customer_data['lead_stage']) ? 'selected':'' ?> ><?php echo $lead_stages_single['stage_name'];?></option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 col-form-label">Stage 0 -Reason: </label>
										<div class="col-lg-6 form-group-sub stage_reason" style="display: <?php echo($customer_data['lead_stage'] == 0) ? 'block': 'none';?>">
											<?php //if($customer_data['lead_stage'] == 0){ ?>
											<select class="form-control lead_stage" name="stage_reason">
												<option value="">Select</option>
												<?php foreach ($lead_stage_reasons as $stage_reasons_detail) { ?>
													<option value="<?php echo $stage_reasons_detail['lead_reason_id']; ?>" 
													<?php 
														echo ($customer_data['stage_reason'] == $stage_reasons_detail['lead_reason_id']) ? 'selected': ''; ?>>
													<?php echo $stage_reasons_detail['reason']; ?>
												</option>
												<?php } ?>
											</select>
											<?php //} ?>
										</div>
									</div>
								</form>
								<!--end::Form-->
								
								<!--begin::Form-->
								<form id="update_lead_management_invoice_main_member_form">
									<div class="form-group row">
										<div class="col-lg-12" style="padding: 0px 0px 0px 0px !important;">
											<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info">
												<thead>
													<tr role="row">
														<th colspan="12" style="text-align: center;">Member</th>     
														<th style="text-align: center;">
															<a href="javascript:;" class="btn-sm btn btn-label-success btn-bold add_member" next_count_number="<?php echo $next_count_number;?>">
																<i class="la la-plus">Add</i>
															</a>
														</th>
													</tr>       
													<tr role="row">
														<th style="width: 3%;">Sr</th>
														<th style="width: 10%;">Name</th>
														<th style="width: 6%;">Designation</th>
														<th style="width: 6%;">Email</th>
														<th style="width: 6%;">Mobile</th>
														<th style="width: 6%;">Whatsapp</th>
														<th style="width: 3%;">Skype</th>
														<th style="width: 6%;">Telephone</th>
														<th style="width: 6%;">MainBuyer</th>
														<th style="width: 6%;">Last Contact Date</th>
														<th style="width: 6%;">Mode</th>
														<th style="width: 3%;">Decision Maker (%)</th>
														<th style="width: 4%;">Action</th>
													</tr>
												</thead>
												<tbody id= "add_member_body">
													<?php if(!empty($main_member_data)){ ?>
														<?php $count_no = 1; ?>
														<?php foreach($main_member_data as $member_detail_key => $single_member_detail){ ?>

															<tr class="<?php echo $count_no;?>">
																<td><?php echo ++$member_detail_key;?><br>
																	<input type="text" class="form-control" name="comp_dtl_id_<?php echo $count_no;?>" value="<?php echo $single_member_detail['comp_dtl_id'];?>" hidden>
																</td>
																<td>
																	<input type="text" class="form-control" name="member_name_<?php echo $count_no;?>"  value="<?php echo $single_member_detail['member_name'];?>">
																</td>
																<td>
																	<input type="text" class="form-control" name="designation_<?php echo $count_no;?>" value="<?php echo $single_member_detail['designation'];?>">
																</td>
																<td>
																	<input type="text" class="form-control" name="email_sent_<?php echo $count_no;?>" value="<?php echo $single_member_detail['email'];?>">
																</td>
																<td>
																	<input type="text" class="form-control" name="mobile_<?php echo $count_no;?>" value="<?php echo $single_member_detail['mobile'];?>">
																</td>
																<td>
																	<select class="form-control" name="is_whatsapp_<?php echo $count_no;?>">
																		<option value="No"<?php echo ($single_member_detail['is_whatsapp'] =='No') ? 'selected' : '';?>>No</option>
																		<option value="Yes"<?php echo ($single_member_detail['is_whatsapp'] =='Yes') ? 'selected' : '';?>>Yes</option>
																	</select>
																</td>
																<td>
																	<input type="text" class="form-control" name="skype_<?php echo $count_no;?>" value="<?php echo $single_member_detail['skype'];?>">
																</td>
																<td>
																	<input type="text" class="form-control" name="telephone_<?php echo $count_no;?>" value="<?php echo $single_member_detail['telephone'];?>">
																</td>
																<td>
																	<select class="form-control" name="main_buyer_<?php echo $count_no;?>">
																		<option value="No"<?php echo ($single_member_detail['main_buyer'] =='No') ? 'selected' : ''; ?>>No</option>
																		<option value="Yes"<?php echo ($single_member_detail['main_buyer'] =='Yes') ? 'selected' : ''; ?>>Yes</option>
																	</select>
																</td>
																<td>
																	<input type="text" class="form-control" value="<?php echo $single_member_detail['connected_on'];?>" readonly>
																</td>
																<td>
																	<input type="text" class="form-control" value="<?php echo $single_member_detail['connect_mode'];?>" readonly>
																</td>
																<td>
																	<input type="text" class="form-control" name="decision_maker_<?php echo $count_no;?>" value="<?php echo $single_member_detail['decision_maker'];?>">
																</td>
																<td>
																	<a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon btn-icon-md member_followup_modal" data-toggle="modal" data-target="#followup_model" comp_dtl_id="<?php echo $single_member_detail['comp_dtl_id'];?>" comp_mst_id="<?php echo $single_member_detail['comp_mst_id'];?>" title="Follow Up">
																		<i class="la la-comment"></i>
																	</a>
															
																	<a href="javascript:;" data-repeater-delete="" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_member" comp_dtl_id="<?php echo $single_member_detail['comp_dtl_id']; ?>" comp_mst_id="<?php echo $single_member_detail['comp_mst_id']; ?>" count_no="<?php echo $count_no;?>">
																		<i class="la la-trash"></i>
																	</a>
																</td>             
															</tr>
														<?php $count_no++; }?>
													<?php }?>
												</tbody>
											</table>
										</div>
									</div>
								</form>	
								<!--end::Form-->
								
								<!--begin::Form-->	
								<form id="update_lead_management_invoice_other_member_form">
									<div class="form-group row">
										<div class="col-lg-12" style="padding: 0px 0px 0px 0px !important;">
											<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info">
												<thead>
													<tr role="row">
														<th colspan="10" style="text-align: left;">Other Member</th>     
														<th style="text-align: center;">
															<a href="javascript:;" class="btn-sm btn btn-label-success btn-bold add_other_member" next_number="<?php echo $next_number;?>">
																<i class="la la-plus">Add</i>
															</a>
														</th>
													</tr>       
													<tr role="row">
														<th width="3%">Sr</th>
														<th width="15%">Name</th>
														<th width="15%">Designation</th>
														<th width="15%">Email</th>
														<th width="15%">Mobile</th>
														<th width="6%">Whatsapp</th>
														<th width="7%">Skype</th>
														<th width="7%">Telephone</th>
														<th width="15%">Last Contact Date</th>
														<th width="15%">Mode</th>
														<th width="8%">Action</th>
													</tr>
												</thead>
												<tbody id = "add_other_member_body">
													<?php if(!empty($other_member_data)){ ?>
														<?php $next_count = 1;?>
														<?php foreach($other_member_data  as $other_member_detail_key => $single_other_member_detail){ ?>

															<tr class="<?php echo $next_count;?>">
																<td><?php echo ++$other_member_detail_key;?><br>
																	<input type="text" class="form-control" name="comp_dtl_id_<?php echo $next_count;?>" value="<?php echo $single_other_member_detail['comp_dtl_id'];?>" hidden></td>
																<td>
																	<input type="text" class="form-control" name="member_name_<?php echo $next_count;?>" value="<?php echo $single_other_member_detail['member_name'];?>">
																</td>
																<td>
																	<input type="text" class="form-control" name="designation_<?php echo $next_count;?>" value="<?php echo $single_other_member_detail['designation'];?>">
																</td>
																<td>
																	<input type="text" class="form-control" name="email_sent_<?php echo $next_count;?>" value="<?php echo $single_other_member_detail['email'];?>">
																</td>
																<td>
																	<input type="text" class="form-control" name="mobile_<?php echo $next_count;?>" value="<?php echo $single_other_member_detail['mobile'];?>">
																</td>
																<td>
																	<select class="form-control" name="is_whatsapp_<?php echo $next_count;?>">
																		<option value="No" <?php echo ($single_other_member_detail['is_whatsapp'] =='No') ? 'selected' : ''; ?>>No</option>
																		<option value="Yes" <?php echo ($single_other_member_detail['is_whatsapp'] =='Yes') ? 'selected' : ''; ?>>Yes</option>
																	</select>
																</td>
																<td>
																	<input type="text" class="form-control" name="skype_<?php echo $next_count;?>" value="<?php echo $single_other_member_detail['skype'];?>">
																</td>
																<td>
																	<input type="text" class="form-control" name="telephone_<?php echo $next_count;?>" value="<?php echo $single_other_member_detail['telephone'];?>">
																</td>
																<td>
																	<input type="text" class="form-control" value="<?php echo $single_other_member_detail['connected_on'];?>" readonly>
																</td>
																<td>
																	<input type="text" class="form-control" value="<?php echo $single_other_member_detail['connect_mode'];?>" readonly>
																</td>
																<td>                                                    
																	<a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon btn-icon-md other_member_followup_modal" data-toggle="modal"data-target="#followup_model" comp_dtl_id="<?php echo $single_other_member_detail['comp_dtl_id'];?>" comp_mst_id="<?php echo $single_other_member_detail['comp_mst_id'];?>" title="Follow Up">
																		<i class="la la-comment"></i>
																	</a>
															
																	<a href="javascript:;" data-repeater-delete="" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_other_member" comp_dtl_id="<?php echo $single_other_member_detail['comp_dtl_id'];?>" comp_mst_id="<?php echo $single_other_member_detail['comp_mst_id'];?>" next_count="<?php echo $next_count;?>">
																		<i class="la la-trash"></i>
																	</a>

																</td>             
															</tr>
														<?php $next_count++; }?>                                                
													<?php }?>
												</tbody>
											</table>
										</div>
									</div>
								</form>
								<!--end::Form-->	
							</div>	
						</div>
						<div class="kt-portlet__foot">
							<div class="kt-form__actions">
								<div class="row">
									<div class="col-lg-2"></div>
									<div class="col-lg-2">
										
										<button type="reset" class="btn btn-success update_competitor_rank_invoice" customer_id="<?php echo $customer_data['id'];?>">Submit</button>
										<button type="reset" class="btn btn-secondary">Cancel</button>
									</div>
									
								</div>
							</div>
						</div>
					</div>
					<!--end::Portlet-->

				</div>
				<!-- end:: Content -->
			</div>
		</div>
	</div>
</div>


<!--begin::Client form-->
<!-- <div class="modal fade" id="invoice_follow_up" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Follow Up</h5>
				<button type="reset" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
                <form id="add_follow_up_form">
               		
               	</form>               	
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
			</div>
		</div>
	</div>
</div> -->

<div class="modal fade" id="followup_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">Follow Up</h5>
			<button type="reset" class="close" data-dismiss="modal" aria-label="Close">
			</button>
			</div>
			<div class="modal-body">
				<form id="followup_form">	            	

					
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
				<!-- <button class="btn btn-success save_primary_followup" type="reset" id="comp_dtl_id" style="float: right;">Submit</button> -->
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="taskModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<form id="add_task_form_data">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">New Task</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
               			<div class="col-12 align-self-center form-group">
               				<textarea class="form-control validate[required]" placeholder="Task Details" id="task_detail" name="task_detail"></textarea>
               			</div>
               			<div class="col-12 align-self-center form-group">
               				<input type="text" class="form-control validate[required]" id="deadline" name="deadline" placeholder="Task Deadline">
               			</div>
               		</div>
				</div>
				<div class="modal-footer">
					<button type="reset" class="btn btn-success add_task">Save Task</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>