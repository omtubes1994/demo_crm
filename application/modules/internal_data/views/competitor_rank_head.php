<?php 
	// if assign to is removed
	$add_on_width = 0;
	$display_name = 'block'; 
	if($this->session->userdata('role') == 5) {
		$add_on_width = 3;
		$display_name = 'none'; 
	}
?>

<tr role="row">
	<th 
		class="<?php echo $sorting['rank']['class_name'];?> sorting_search" 
		sorting_name="rank"
		sorting_value="<?php echo $sorting['rank']['value'];?>"
		style="width: 7%;">
	Rank</th>
	<th
		class="<?php echo $sorting['customer_name']['class_name'];?> sorting_search"
		sorting_name="customer_name"
		sorting_value="<?php echo $sorting['customer_name']['value'];?>"
		style="width: 13%;">
	Company Details</th>
	<th 
		class="<?php echo $sorting['lead_stage']['class_name'];?> sorting_search"
		sorting_name="lead_stage"
		sorting_value="<?php echo $sorting['lead_stage']['value'];?>"
		style="width: 10%;">
	Lead Stage</th>
	<th class="sorting_disabled" style="width: <?php echo $add_on_width+10;?>%;">Name</th>
	<th class="sorting_disabled" style="width: <?php echo $add_on_width+13;?>%;">Contact</th>
	<th 
		class="<?php echo $sorting['assigned_to']['class_name'];?> sorting_search"
		sorting_name="assigned_to"
		sorting_value="<?php echo $sorting['assigned_to']['value'];?>"
		style="width: 10%; display: <?php echo $display_name; ?>;">
	Assigned To</th>
	<th class="sorting" style="width: <?php echo $add_on_width+12;?>%;">Comments</th>
	<th class="sorting" style="width: 8%;">Connect Mode</th>
	<th class="sorting_disabled" style="width: 7%;">Actions</th>
</tr>

<tr class="filter">
	<th></th>
	<th>
		<input type="text" class="form-control form-control-sm form-filter kt-input" placeholder="Search Company Name" id="competitor_search_company_name" value="<?php echo (!empty($search_form['customer_name']))? $search_form['customer_name']:''?>">
	</th>
	<th>
		<select class="form-control form-control-sm form-filter kt-input" id="competitor_search_lead_type">
			<option value="">Select Lead Type</option>
			<?php foreach($lead_type as $lead_type_single) { ?>
				<option value="<?php echo $lead_type_single['lead_type_id']; ?>" <?php echo ($lead_type_single['lead_type_id'] == $search_form['lead_type_id']) ?'selected':''; ?> ><?php echo $lead_type_single['type_name']; ?></option>
			<?php } ?>	
		</select>
	</th>
	<th>
		<select class="form-control form-control-sm form-filter kt-input" id="competitor_search_lead_stage">
			<option value="">Select Lead Stage</option>
			<?php foreach($stage_details as $stage_details_single) { ?>
				<option value="<?php echo $stage_details_single['lead_stage_id']; ?>" <?php echo ($stage_details_single['lead_stage_id'] == $search_form['lead_stage_id']) ?'selected':''; ?> ><?php echo $stage_details_single['stage_name']; ?></option>
			<?php } ?>
		</select>
	</th>
	<th>
		<select class="form-control form-control-sm form-filter kt-input" id="competitor_search_country_name">
			<option value="">Select Country Name</option>
			<?php foreach($country_list as $country_list_single) { ?>
				<option value="<?php echo $country_list_single['name']; ?>" <?php echo ($country_list_single['name'] == $search_form['country_name']) ?'selected':''; ?> ><?php echo ucfirst(strtolower($country_list_single['name'])); ?></option>
			<?php } ?>	
		</select>
		<br>
		<select class="form-control form-control-sm form-filter kt-input" id="competitor_search_region_id">
			<option value="">Select Region Name</option>
			<?php foreach($region as $region_single) { ?>
				<option value="<?php echo $region_single['id']; ?>" <?php echo ($region_single['id'] == $search_form['region_id']) ?'selected':''; ?> ><?php echo ucfirst(strtolower($region_single['name'])); ?></option>
			<?php } ?>	
		</select>
	</th>
	<th style="display: <?php echo $display_name; ?>;">
		<select class="form-control form-control-sm form-filter kt-input" id="competitor_search_assigned_person">
			<option value="">Select Assigned To</option>
			<?php foreach($sales_person as $sales_person_single) { ?>
				<option value="<?php echo $sales_person_single['user_id']; ?>" <?php echo ($sales_person_single['user_id'] == $search_form['user_id']) ?'selected':''; ?> ><?php echo $sales_person_single['name']; ?></option>
			<?php } ?>
		</select>
	</th>
	<th></th>
	<th>
		<select class="form-control form-control-sm form-filter kt-input"id="competitor_search_connect_mode">
			<option value="">Select Connect Mode</option>
			<option value="Call">Call</option>
			<option value="WhatsApp">Whatsapp</option>
			<option value="Email">Email</option>
			<option value="Linkedin">LinkedIn</option>
		</select>
	</th>
	<th>
		<button class="btn btn-brand kt-btn btn-sm kt-btn--icon competitor_rank_invoice_advance_searh_form_submit" type="reset">
			<span>
				<i class="la la-search"></i>
				<span>Search</span>
			</span>
		</button>
		<button class="btn btn-secondary kt-btn btn-sm kt-btn--icon competitor_rank_invoice_advance_searh_form_reset" type="reset">
			<span>
				<i class="la la-close"></i>
				<span>Reset</span>
			</span>
		</button>
	</th>
</tr>