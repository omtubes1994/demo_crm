// invoice js
$('div.get_invoice_advance_search_form').click(function(){
	$('div#invoice_advance_search_form').show();
});
$('button.close_invoice_advance_search_form').click(function(){
	$('div#invoice_advance_search_form').hide();
});
 // $("#update_competitor_rank_invoice_form").submit(function(e){
 // 	alert('came in');
 //        e.preventDefault();
 //    });

$('form#update_lead_management_invoice_form').on('change', 'select.lead_stage', function () {

	$('div.stage_reason').hide();
	var val = $(this).val();
	if (val == 0) {
		$('div.stage_reason').show();
	}
}); 

$('div#competitor_rank_paggination').on('click', 'li.competitor_rank_paggination_number', function(){

	sorting_options = get_sorting_name();
	common_call($(this).attr('limit'), $(this).attr('offset'), sorting_options.competitor_sort_name, sorting_options.competitor_sort_value);

});
$('div#competitor_rank_paggination').on('change', 'select#set_limit', function(){

	sorting_options = get_sorting_name();
	common_call($('select#set_limit').val(), 0, sorting_options.competitor_sort_name, sorting_options.competitor_sort_value);

});

$('thead#competitor_rank_head').on('click', 'th.sorting_search', function(){

	common_call($('input#internal_data_limit').val(), $('input#internal_data_offset').val(), $(this).attr('sorting_name'), $(this).attr('sorting_value'));

});

$('thead#competitor_rank_head').on('click', 'button.competitor_rank_invoice_advance_searh_form_submit', function(){

	sorting_options = get_sorting_name();
	common_call($('input#internal_data_limit').val(), $('input#internal_data_offset').val(), sorting_options.competitor_sort_name, sorting_options.competitor_sort_value);

});
function common_call(limit, offset, sort_name, sort_value){

	ajax_call_function_for_invoice(
				{
				call_type: 'competitor_rank_invoice_advance_searh_form_submit',
				competitor_search_company_name: $('input#competitor_search_company_name').val(),
				competitor_search_lead_type: $('select#competitor_search_lead_type').val(),
				competitor_search_lead_stage: $('select#competitor_search_lead_stage').val(),
				competitor_search_country_name: $('select#competitor_search_country_name').val(),
				competitor_search_assigned_person: $('select#competitor_search_assigned_person').val(),
				competitor_search_region_id: $('select#competitor_search_region_id').val(),
				limit: limit,
				offset: offset,
				competitor_sort_name: sort_name,
				competitor_sort_value: sort_value
				}, 'competitor_ranks_list');

}

function get_sorting_name(){

	return_array = [];
	if($('.sorting_asc.sorting_search').attr('sorting_name') != ''){
		
		return_array.competitor_sort_name = $('.sorting_asc.sorting_search').attr('sorting_name');
		return_array.competitor_sort_value = 'asc';

	}else if($('.sorting_desc.sorting_search').attr('sorting_name') != ''){

		return_array.competitor_sort_name = $('.sorting_desc.sorting_search').attr('sorting_name');
		return_array.competitor_sort_value = 'desc';

	}

	return return_array;
}

$('thead#competitor_rank_head').on('click', 'button.competitor_rank_invoice_advance_searh_form_reset', function(){
	ajax_call_function_for_invoice({call_type: 'competitor_rank_invoice_advance_searh_form_submit'}, 'competitor_ranks_list');
});
$('button.add_task').click(function(){
	ajax_call_function_for_invoice({ call_type: 'add_task', lead_id: $('input#comp_detail_id').val(), details_id: $('input#comp_detail_id').val(), task_detail: $('textarea#task_detail').val(), deadline: $('input#deadline').val()}, 'add_follow_up_history');
});
// $('button.add_follow_up_history').click(function(){
// 	ajax_call_function_for_invoice({ call_type: 'add_follow_up_history', add_follow_up_history_form_data: $('form#add_follow_up_history_form').serializeArray(), comp_detail_id: $('input#comp_detail_id').val()}, 'add_follow_up_history');
// });
// $('a.invoice_get_follow_up_modal').click(function(){
// 	$("#add_follow_up_history_form").trigger("reset");
// 	$('input#comp_detail_id').val($(this).attr('comp_detail_id'));
// 	$('table#invoice_connect_history tbody tr').hide();
// 	// var val= $(this).attr('comp_detail_id');
// 	// alert(val);
// 	$('table#invoice_connect_history tbody tr[comp_detail_id=' + $(this).attr('comp_detail_id')+']').show();
//     //  alert(val);
// });

$('a.invoice_get_follow_up_modal').click(function () {

	ajax_call_function_for_invoice({
		call_type: 'member_followup_modal',
		comp_dtl_id: $(this).attr('comp_dtl_id'),
		comp_mst_id: $(this).attr('comp_mst_id'),
	}, 'member_followup_modal');
});

$('div#invoice_follow_up').on('click', 'button.save_followup', function () {

	ajax_call_function_for_invoice({
		call_type: 'save_followup',
		comp_dtl_id: $(this).val(),
		add_followup_data_form: $('form#add_follow_up_form').serializeArray(),
	}, 'save_followup');
});

$('span.mem_count').click(function(){
	ajax_call_function_for_invoice({ call_type: 'get_member_list_modal_body', customer_id: $(this).attr('customer_id')}, 'get_member_list_modal_body');
});

$('button.update_competitor_rank_invoice').click(function(){
	var customer_id= $(this).attr('customer_id');
	ajax_call_function_for_invoice({

			call_type: 'update_competitor_rank_invoice_form_data',
			invoice_form_data: $('form#update_lead_management_invoice_form').serializeArray(),
			invoice_main_member_form_data: $('form#update_lead_management_invoice_main_member_form').serializeArray(),
			invoice_other_member_form_data: $('form#update_lead_management_invoice_other_member_form').serializeArray(),
			customer_id: customer_id,			
		}, 'form_update');
});

$('button.search_invoice_advance_search_form').click(function(){
	ajax_call_function_for_invoice({call_type: 'advance_search_for_invoice', company_name: $('input#advance_search_company_name').val()}, 'invoice_list');
});
$('button.reset_invoice_advance_search_form').click(function(){
	ajax_call_function_for_invoice({call_type: 'reset_advance_search_for_invoice'}, 'invoice_list');
});


$('a.add_member').click(function () {

	var next_count_number = $(this).attr('next_count_number');
	// alert(next_count_number);
	ajax_call_function_for_invoice({
		call_type: 'get_member_body',
		next_count_number: next_count_number,
	}, 'get_member_body');
	$(this).attr('next_count_number', ++next_count_number);
});

$('a.add_other_member').click(function () {

	var next_number = $(this).attr('next_number');
	ajax_call_function_for_invoice({
		call_type: 'get_other_member_body',
		next_number: next_number,
	}, 'get_other_member_body');
	$(this).attr('next_number', ++next_number);
});

$('tbody#add_member_body').on('click', 'a.delete_member_details', function () {
	$(this).closest('tr').remove();
});

$('tbody#add_other_member_body').on('click', 'a.delete_other_member_details', function () {
	$(this).closest('tr').remove();
});

$('tbody#add_member_body').on('click', 'a.delete_member', function () {
	// $('a.delete_member').click( function () {
	swal({
		title: "Are you sure?",
		text: "Once deleted, you will not be able to recover Member Details",
		icon: "warning",
		buttons: ["Cancel", "Delete"],
		dangerMode: true,
	})
		.then((willDelete) => {
			if (willDelete) {
				ajax_call_function_for_invoice({
					call_type: 'delete_member',
					comp_dtl_id: $(this).attr('comp_dtl_id'),
					comp_mst_id: $(this).attr('comp_mst_id'),
				}, 'delete_member');
				swal({
					title: "Member Deleted",
					icon: "success",
				});
			} else {
				swal({
					title: "Member Not Delete",
					icon: "info",
				});
			}
		});
});

$('tbody#add_other_member_body').on('click', 'a.delete_other_member', function () {
	swal({
		title: "Are you sure?",
		text: "Once deleted, you will not be able to recover Member Details",
		icon: "warning",
		buttons: ["Cancel", "Delete"],
		dangerMode: true,
	})
		.then((willDelete) => {
			if (willDelete) {
				ajax_call_function_for_invoice({
					call_type: 'delete_other_member',
					comp_dtl_id: $(this).attr('comp_dtl_id'),
					comp_mst_id: $(this).attr('comp_mst_id'),
				}, 'delete_other_member');
				swal({
					title: "Member Deleted",
					icon: "success",
				});
			} else {
				swal({
					title: "Member Not Delete",
					icon: "info",
				});
			}
		});
});

$('tbody#add_member_body').on('click', 'a.member_followup_modal', function () {

	ajax_call_function_for_invoice({
		call_type: 'member_followup_modal',
		comp_dtl_id: $(this).attr('comp_dtl_id'),
		comp_mst_id: $(this).attr('comp_mst_id'),
	}, 'member_followup_modal');
});

$('tbody#add_other_member_body').on('click', 'a.other_member_followup_modal', function () {

	ajax_call_function_for_invoice({
		call_type: 'other_member_followup_modal',
		comp_dtl_id: $(this).attr('comp_dtl_id'),
		comp_mst_id: $(this).attr('comp_mst_id'),
	}, 'other_member_followup_modal');
});

$('div#followup_model').on('click', 'button.save_followup', function () {

	ajax_call_function_for_invoice({
		call_type: 'save_followup',
		comp_dtl_id: $(this).val(),
		add_followup_data_form: $('form#followup_form').serializeArray()
	}, 'save_followup');
});


function ajax_call_function_for_invoice(data, call_type, url="<?php echo base_url('internal_data/ajax_function_for_invoice');?>") {

	$.ajax({
		type: 'POST',
		data: data,
		url: url,
		dataType: 'JSON',
		success: function(res){
			if(res.status == 'successful') {

				if(call_type == 'invoice_list') {
					$('tbody#invoice_table_list').html('').html(res.html_body);
				}
				if(call_type == 'form_update') {
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"newestOnTop": false,
						"progressBar": true,
						"positionClass": "toast-top-right",
						"preventDuplicates": false,
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					};
					toastr.success("Competitor rank data update", "Data update");
				}
				if(call_type == 'competitor_ranks_list') {
					$('thead#competitor_rank_head').html('').html(res.html_head);
					$('tbody#competitor_rank_body').html('').html(res.html_body);
					$('div#competitor_rank_paggination').html('').html(res.internal_paggination);
				}
				if(call_type == 'graph_view') {
					create_highcharts(res.graph_data);
					$('div#kt_modal_4_2').modal('show');
					$('tbody#competitor_rank_body button.'+data.lead_id).hide();
					$('a.'+data.lead_id).show();
					$('#total_amount').html(res.total_amount_html);
					$('#client_name').html(res.client_name_html);
				}
				if(call_type == 'add_follow_up_history') {
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"newestOnTop": false,
						"progressBar": true,
						"positionClass": "toast-top-right",
						"preventDuplicates": false,
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					};
					toastr.success("Follow Up is Done", "Member Contacted details added");
					$('div#invoice_follow_up').slideUp();   
					$('div#invoice_follow_up').modal('hide');
					location.reload();

				}
				if(call_type == 'get_member_list_modal_body') {
					$('form#invoice_member_body').html('').html(res.member_html_body);
				}

				if (call_type == 'member_followup_modal') {
					$('form#add_follow_up_form').html('').html(res.follow_up_html);
					$('button[id=comp_dtl_id]').val(res.comp_dtl_id);
				}

				if (call_type == 'get_member_body') {
					$('tbody#add_member_body').append(res.member_body_detail);
				}

				if (call_type == 'get_other_member_body') {
					$('tbody#add_other_member_body').append(res.other_member_body_detail);
				}

				if (call_type == 'member_followup_modal') {
					
					$('form#followup_form').html('').html(res.follow_up_html);
					$('button[id=comp_dtl_id]').val(res.comp_dtl_id);
				}

				if (call_type == 'other_member_followup_modal') {
					
					$('form#followup_form').html('').html(res.follow_up_html);
					$('button[id=comp_dtl_id]').val(res.comp_dtl_id);
				}
			}
			$('div#kt_table_1_processing').hide();
		},
		beforeSend: function(response){
			if(call_type == 'graph_view') {

				$('tbody#competitor_rank_body button.'+data.lead_id).show();
			}
			$('div#kt_table_1_processing').show();
		}
	});
};

$('tbody#competitor_rank_body').on('click', 'a.get_graph_for_invoice', function(){
	$(this).hide();
	// get graph data
	ajax_call_function_for_invoice({ call_type: 'get_data_for_graph', company_name: $(this).attr('company-name'), customer_id: $(this).attr('customer-id') }, 'graph_view');
	
});

function set_reset_spinner(obj, set_unset_flag = true) {

	if(set_unset_flag) {

		$(obj).addClass('kt-spinner');
    	$(obj).addClass('kt-spinner--right');
    	$(obj).addClass('kt-spinner--sm');
    	$(obj).addClass('kt-spinner--light');	
	} else{

		$(obj).removeClass('kt-spinner');
    	$(obj).removeClass('kt-spinner--right');
    	$(obj).removeClass('kt-spinner--sm');
    	$(obj).removeClass('kt-spinner--light');
	}
}

function create_series_data(series_data, series_type){
	var return_json = [];
	if(series_type == 'column_main_series') {
		for(var i=0; i< series_data.length; i++) {
			return_json[i] = {
	            name: series_data[i].name,
	            y: series_data[i].y,
	            drilldown: series_data[i].drilldown
			}
		}	
	}
	if(series_type == 'pie_main_series') {
		console.log(series_data);
		// return_json = [
		// 	{
	 //            name: '13/32" PTFE HOSE HSN CODE',
	 //            y: 4.820478724317995
		// 	},
		// 	{
	 //            name: '179-04-PTFE HOSE R14 -04 DN 005 3/16" HSN CODE',
	 //            y: 4.820478724317995
		// 	},	
		// 	{
	 //            name: '5/16" ID PTFE PLAIN HOSE -R14 HSN CODE',
	 //            y: 9.325192463613133
		// 	},
		// 	{
	 //            name: 'BARBED 3/8” CRIMP-ON TUBE STUB SS304 HSN CODE',
	 //            y: 4.077111492747282
		// 	},
		// ] 
		for(var i=0; i< series_data.length; i++) {
			return_json[i] = {
	            name: series_data[i].name,
	            y: series_data[i].value
			}
		}	
	} 
	return return_json;
}
function create_highcharts(data) {
	console.log(data);
	// return false;
	var main_series_json = create_series_data(data.main_series, 'column_main_series');
	var pie_main_series_json = create_series_data(data.pie_chart_series, 'pie_main_series');
	var drilldown_series_json = [];
	// var drilldown_series_json = create_series_data(data.drilldown_series, 'column_drilldown_series');
	console.log(main_series_json);
	console.log(pie_main_series_json);
	Highcharts.chart('graph_invoice', {
	    chart: {
	        plotBackgroundColor: null,
	        plotBorderWidth: null,
	        plotShadow: false,
	        type: 'pie'
	    },
	    title: {
	        text: 'Top Product'
	    },
	    tooltip: {
	        // pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	        pointFormat: '{series.name}: <b>{point.y:.1f}</b>'
	    },
	    accessibility: {
	        point: {
	            valueSuffix: '%'
	        }
	    },
	    plotOptions: {
	        pie: {
	            allowPointSelect: true,
	            cursor: 'pointer',
	            dataLabels: {
	                enabled: true,
	                format: '<b>{point.name}</b>: {point.y:.1f}'
	            }
	        }
	    },
	    series: [{
	        name: 'Brands',
	        colorByPoint: true,
	        data: pie_main_series_json
	        // data: [{
	        //     name: 'Chrome',
	        //     y: 61.41,
	        // }, {
	        //     name: 'Internet Explorer',
	        //     y: 11.84
	        // }, {
	        //     name: 'Firefox',
	        //     y: 10.85
	        // }, {
	        //     name: 'Edge',
	        //     y: 4.67
	        // }, {
	        //     name: 'Safari',
	        //     y: 4.18
	        // }, {
	        //     name: 'Sogou Explorer',
	        //     y: 1.64
	        // }, {
	        //     name: 'Opera',
	        //     y: 1.6
	        // }, {
	        //     name: 'QQ',
	        //     y: 1.2
	        // }, {
	        //     name: 'Other',
	        //     y: 2.61
	        // }]
	    }]
	});

	// Create the chart
	Highcharts.chart('graph_invoice_column', {
		chart: {
		    type: 'column'
		},
		title: {
		    text: 'Import History'
		},
		subtitle: {
		    text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
		},
		accessibility: {
		    announceNewData: {
		        enabled: true
		    }
		},
		xAxis: {
		    type: 'category'
		},
		yAxis: {
		    title: {
		        text: 'Export Wise import values'
		    }

		},
		legend: {
		    enabled: false
		},
		plotOptions: {
		    series: {
		        borderWidth: 0,
		        dataLabels: {
		            enabled: true,
		            format: '{point.y:.1f}'
		        }
		    }
		},

		tooltip: {
		    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
		    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
		},

		series: [
		    {
		        name: "Browsers",
		        colorByPoint: true,
		        data: main_series_json
		    }
		],
		drilldown: {
			series: drilldown_series_json
		    // series: [
		    //     {
		    //         name: "Chrome",
		    //         id: "Chrome",	
		    //         data: [
		    //             [
		    //                 "v65.0",
		    //                 0.1
		    //             ],
		    //             [
		    //                 "v64.0",
		    //                 1.3
		    //             ],
		    //             [
		    //                 "v63.0",
		    //                 53.02
		    //             ],
		    //             [
		    //                 "v62.0",
		    //                 1.4
		    //             ],
		    //             [
		    //                 "v61.0",
		    //                 0.88
		    //             ],
		    //             [
		    //                 "v60.0",
		    //                 0.56
		    //             ],
		    //             [
		    //                 "v59.0",
		    //                 0.45
		    //             ],
		    //             [
		    //                 "v58.0",
		    //                 0.49
		    //             ],
		    //             [
		    //                 "v57.0",
		    //                 0.32
		    //             ],
		    //             [
		    //                 "v56.0",
		    //                 0.29
		    //             ],
		    //             [
		    //                 "v55.0",
		    //                 0.79
		    //             ],
		    //             [
		    //                 "v54.0",
		    //                 0.18
		    //             ],
		    //             [
		    //                 "v51.0",
		    //                 0.13
		    //             ],
		    //             [
		    //                 "v49.0",
		    //                 2.16
		    //             ],
		    //             [
		    //                 "v48.0",
		    //                 0.13
		    //             ],
		    //             [
		    //                 "v47.0",
		    //                 0.11
		    //             ],
		    //             [
		    //                 "v43.0",
		    //                 0.17
		    //             ],
		    //             [
		    //                 "v29.0",
		    //                 0.26
		    //             ]
		    //         ]
		    //     },
		    //     {
		    //         name: "Firefox",
		    //         id: "Firefox",
		    //         data: [
		    //             [
		    //                 "v58.0",
		    //                 1.02
		    //             ],
		    //             [
		    //                 "v57.0",
		    //                 7.36
		    //             ],
		    //             [
		    //                 "v56.0",
		    //                 0.35
		    //             ],
		    //             [
		    //                 "v55.0",
		    //                 0.11
		    //             ],
		    //             [
		    //                 "v54.0",
		    //                 0.1
		    //             ],
		    //             [
		    //                 "v52.0",
		    //                 0.95
		    //             ],
		    //             [
		    //                 "v51.0",
		    //                 0.15
		    //             ],
		    //             [
		    //                 "v50.0",
		    //                 0.1
		    //             ],
		    //             [
		    //                 "v48.0",
		    //                 0.31
		    //             ],
		    //             [
		    //                 "v47.0",
		    //                 0.12
		    //             ]
		    //         ]
		    //     },
		    //     {
		    //         name: "Internet Explorer",
		    //         id: "Internet Explorer",
		    //         data: [
		    //             [
		    //                 "v11.0",
		    //                 6.2
		    //             ],
		    //             [
		    //                 "v10.0",
		    //                 0.29
		    //             ],
		    //             [
		    //                 "v9.0",
		    //                 0.27
		    //             ],
		    //             [
		    //                 "v8.0",
		    //                 0.47
		    //             ]
		    //         ]
		    //     },
		    //     {
		    //         name: "Safari",
		    //         id: "Safari",
		    //         data: [
		    //             [
		    //                 "v11.0",
		    //                 3.39
		    //             ],
		    //             [
		    //                 "v10.1",
		    //                 0.96
		    //             ],
		    //             [
		    //                 "v10.0",
		    //                 0.36
		    //             ],
		    //             [
		    //                 "v9.1",
		    //                 0.54
		    //             ],
		    //             [
		    //                 "v9.0",
		    //                 0.13
		    //             ],
		    //             [
		    //                 "v5.1",
		    //                 0.2
		    //             ]
		    //         ]
		    //     },
		    //     {
		    //         name: "Edge",
		    //         id: "Edge",
		    //         data: [
		    //             [
		    //                 "v16",
		    //                 2.6
		    //             ],
		    //             [
		    //                 "v15",
		    //                 0.92
		    //             ],
		    //             [
		    //                 "v14",
		    //                 0.4
		    //             ],
		    //             [
		    //                 "v13",
		    //                 0.1
		    //             ]
		    //         ]
		    //     },
		    //     {
		    //         name: "Opera",
		    //         id: "Opera",
		    //         data: [
		    //             [
		    //                 "v50.0",
		    //                 0.96
		    //             ],
		    //             [
		    //                 "v49.0",
		    //                 0.82
		    //             ],
		    //             [
		    //                 "v12.1",
		    //                 0.14
		    //             ]
		    //         ]
		    //     }
		    // ]
		}
	});	
}
// Class definition
var KTFormRepeater = function() {

    // Private functions
    var demo1 = function() {
        $('#kt_repeater_1').repeater({
            initEmpty: false,
           
            defaultValues: {
                'text-input': 'foo',
            },
             
            show: function () {
                $(this).slideDown();
            },

            hide: function (deleteElement) {

                swal({
				  title: "Are you sure?",
				  text: "Once deleted, you will not be able to recover this Member Details!",
				  icon: "warning",
				  buttons:  ["Cancel", "Delete"],
				  dangerMode: true,	
				})
				.then((willDelete) => {
				  	if (willDelete) {
                		$(this).slideUp(deleteElement);                 
				    	swal({
				    		title: "Member details is deleted",
				      		icon: "success",
				    	});
			  		} else {
					    swal({
				    		title: "Member is not deleted",
				      		icon: "info",
				    	});
				  	}
				});
            }   
        });
    }
    var demo2 = function() {
        $('#kt_repeater_2').repeater({
            initEmpty: false,
           
            defaultValues: {
                'text-input': 'foo',
            },
             
            show: function () {
                $(this).slideDown();
            },

            hide: function (deleteElement) {  

            	swal({
				  title: "Are you sure?",
				  text: "Once deleted, you will not be able to recover this Member Details!",
				  icon: "warning",
				  buttons:  ["Cancel", "Delete"],
				  dangerMode: true,	
				})
				.then((willDelete) => {
				  	if (willDelete) {
                		$(this).slideUp(deleteElement);                 
				    	swal({
				    		title: "Member details is deleted",
				      		icon: "success",
				    	});
			  		} else {
					    swal({
				    		title: "Member is not deleted",
				      		icon: "info",
				    	});
				  	}
				});             
            }   
        });
    }

    return {
        // public functions
        init: function() {
            demo1();
            demo2();
        }
    };
}();

jQuery(document).ready(function() {
    KTFormRepeater.init();
	    
});
