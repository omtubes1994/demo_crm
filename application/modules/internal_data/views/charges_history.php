<thead>
   <tr>
   	<th>Charge Name</th>
   	<th>Charge Added Or Deducted</th>
   	<th>Charge Value</th>
   	<th>Net Total: <?php echo $this->session->userdata('order_details')['net_total']; ?></th>
   </tr>
</thead>
<tbody>
   <?php if(!empty($this->session->userdata('order_details')['different_charges'])) {?>
      <?php foreach($this->session->userdata('order_details')['different_charges'] as $charges_details) {?>
         <tr>
         	<td><?php echo $charges_details['charges_name'];?></td>
         	<td><?php echo $charges_details['add_minus_charges_value'];?></td>
         	<td><?php echo $charges_details['charge_value'];?></td>
         	<td><?php echo $charges_details['gross_total'];?></td>
         </tr>
      <?php }?>
   <?php }?>
   <tr>
   	<td class="kt-font-success kt-font-xl kt-font-boldest" colspan="3" style="text-align: center;">GROSS TOTAL</td>
   	<td class="kt-font-success kt-font-xl kt-font-boldest" style="text-align: center;">
         <?php  echo $this->session->userdata('order_details')['gross_total']; ?>  
      </td>
   </tr>	
</tbody>