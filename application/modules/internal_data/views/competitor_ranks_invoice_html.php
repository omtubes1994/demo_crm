	<style type="text/css">
	#kt_wrapper{
		padding-top: 3% !important;
	}
	.my-scroll{
      border:1px solid #e1e1e1;
      height: 670px;
      width:auto;
      overflow-y: auto;
    }
    .kt-aside--fixed .kt-wrapper {
        padding-left: 130px !important;
    }
</style>
<div class="kt-grid kt-grid--hor kt-grid--root">
	<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

			
			<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

				<!-- begin:: Content -->
				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
					
					<div class="kt-portlet kt-portlet--mobile">
						<div class="kt-portlet__head kt-portlet__head--lg">
							<div class="kt-portlet__head-label">
								<span class="kt-portlet__head-icon">
									<i class="kt-font-brand flaticon2-line-chart"></i>
								</span>
								<h3 class="kt-portlet__head-title">
									Individual Column Search
								</h3>
							</div>
							<div class="kt-portlet__head-toolbar">
								<div class="kt-portlet__head-wrapper">
									<div class="kt-portlet__head-actions">
										<div class="dropdown dropdown-inline">
											<button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<i class="la la-download"></i> Export
											</button>
											<div class="dropdown-menu dropdown-menu-right">
												<ul class="kt-nav">
													<li class="kt-nav__section kt-nav__section--first">
														<span class="kt-nav__section-text">Choose an option</span>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon la la-print"></i>
															<span class="kt-nav__link-text">Print</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon la la-copy"></i>
															<span class="kt-nav__link-text">Copy</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon la la-file-excel-o"></i>
															<span class="kt-nav__link-text">Excel</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon la la-file-text-o"></i>
															<span class="kt-nav__link-text">CSV</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon la la-file-pdf-o"></i>
															<span class="kt-nav__link-text">PDF</span>
														</a>
													</li>
												</ul>
											</div>
										</div>
										&nbsp;
										<a href="#" class="btn btn-brand btn-elevate btn-icon-sm">
											<i class="la la-plus"></i>
											New Record
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="kt-portlet__body">

							<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
								<div class="row">
									<div class="col-sm-12">
										<table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 1536px;">
											<thead>
												<tr role="row">
													<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 40px;" aria-sort="ascending" aria-label="Record ID: activate to sort column descending">Rank</th>
													<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 270px;" aria-label="Order ID: activate to sort column ascending">Company Details</th>
													<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 150px;" aria-label="Country: activate to sort column ascending">Lead Stage</th>
													<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 230px;" aria-label="Country: activate to sort column ascending">Name</th>
													<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 150px;" aria-label="Country: activate to sort column ascending">Contact</th>
													<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 150px;" aria-label="Country: activate to sort column ascending">Assigned To</th>
													<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 300px;" aria-label="Country: activate to sort column ascending">Comments</th>
													<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 120px;" aria-label="Country: activate to sort column ascending">Connect Mode</th>
													<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 130px;" aria-label="Actions">Actions</th>
												</tr>
												<tr class="filter">
													<th></th>
													<th>
														<input type="text" class="form-control form-control-sm form-filter kt-input" placeholder="Search Company Name"value="">
													</th>
													<th>
														<select class="form-control form-control-sm form-filter kt-input" title="Select" name="competitor_search_country_name" id="competitor_search_country_name">
															<option value="">Search LEad Type</option>
														</select>
													</th>
													<th>
														<select class="form-control form-control-sm form-filter kt-input" title="Select" name="competitor_search_country_name" id="competitor_search_country_name">
															<option value="">Search Lead Stage</option>
														</select>
													</th>
													<th>
														<select class="form-control form-control-sm form-filter kt-input" title="Select" name="competitor_search_country_name" id="competitor_search_country_name">
															<option value="">Search Country</option>
														</select>
													</th>
													<th>
														<select class="form-control form-control-sm form-filter kt-input" title="Select" name="competitor_search_country_name" id="competitor_search_country_name">
															<option value="">Search Assigned TO</option>
														</select>
													</th>
													<th></th>
													<th>
														<select class="form-control form-control-sm form-filter kt-input" title="Select" name="competitor_search_country_name" id="competitor_search_country_name">
															<option value="">Search Connect Mode</option>
														</select>
													</th>
													<th>
														<button class="btn btn-brand kt-btn btn-sm kt-btn--icon competitor_rank_invoice_advance_searh_form_submit" type="reset">
															<span>
																<i class="la la-search"></i>
																<span>Search</span>
															</span>
														</button>
														<button class="btn btn-secondary kt-btn btn-sm kt-btn--icon competitor_rank_invoice_advance_searh_form_reset" type="reset">
															<span>
																<i class="la la-close"></i>
																<span>Reset</span>
															</span>
														</button>
													</th>
												</tr>
											</thead>
											<tfoot style="display: none;">
												<tr><th rowspan="1" colspan="1">Record ID</th><th rowspan="1" colspan="1">Order ID</th><th rowspan="1" colspan="1">Country</th><th rowspan="1" colspan="1">Ship City</th><th rowspan="1" colspan="1">Company Agent</th><th rowspan="1" colspan="1">Ship Date</th><th rowspan="1" colspan="1">Status</th><th rowspan="1" colspan="1">Type</th><th rowspan="1" colspan="1">Actions</th>
												</tr>
											</tfoot>
											<tbody>
												<tr role="row" class="odd">
													<td class="sorting_1" tabindex="0">1</td>
													<td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="OrderID">
														<span style="width: 181px;">
															<div class="kt-user-card-v2">
																<div class="kt-user-card-v2__pic">
																	<img src="https://democrm.omtubes.com/assets/media/flags/226-united-states.svg" alt="photo" class="img img-responsive rounded-circle" style="width: 30px">
																</div>
																<div class="kt-user-card-v2__details">
																	<span class="kt-user-card-v2__name">Viking Wholesale Supply</span>
																	<a href="#" class="kt-user-card-v2__email kt-link">United States</a><br>
																	<a href="#" class="kt-user-card-v2__email kt-link">END USER</a><br>
																	<a href="#" class="kt-user-card-v2__email kt-link">January, 2021</a>
																</div>
															</div>
														</span>
													</td>
													<td>
														<select class="form-control form-control-sm form-filter kt-input">
															<option value="">Search Country Name</option>
															<option value=""></option>
															<option value="1" id="grey">Stage 1</option>
															<option value="2" id="yellow">Stage 2</option>
															<option value="3" id="orange">Stage 3</option>
															<option value="4" id="blue">Stage 4</option>
															<option value="5" selected="selected" id="green" data-select2-id="green">Stage 5</option>
															<option value="6" id="multi">Stage 6</option>
															<option value="7" id="red" disabled="disabled">Stage 0</option></select>
														</select><br>
														<span style="font-weight: lighter;" class="last_contacted">6 months ago</span><br>
														<i class="fas fa-user-friends"></i> <span style="font-size: 12px;">50-100</span>
														<i class="fas fa-user"></i><span style="font-size: 12px;">10-25</span>
														<i class="fas fa-users"></i> <span style="font-size: 12px;">1000+</span>
													</td>

													<td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="Ship City">
														<span style="width: 181px;">
															<div class="kt-user-card-v2">
																<div class="kt-user-card-v2__details">
																	<span class="kt-user-card-v2__name">Anand Avinash Wangikar</span>
																	<a href="#" class="kt-user-card-v2__email kt-link">Staff Engineer</a>
																</div>
															</div>
														</span>
													</td>
													<td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="Ship City">
														<span style="width: 181px;">
															<div class="kt-user-card-v2">
																<div class="kt-user-card-v2__details">
																	<span class="kt-user-card-v2__name">Loay.Bagido@tronox.com</span>
																	<a href="#" class="kt-user-card-v2__email kt-link">+966 14 328 3541</a>
																</div>
															</div>
														</span>
														<span style="cursor: pointer; color: #9583ce; font-weight: bold; padding-left: 130px;" lead_id="1226" member_type="mem" class="mem_count">9</span> / <span style="cursor: pointer; color: #ce8383; font-weight: bold;" lead_id="1226" member_type="nonmem" class="mem_count">0</span>
													</td>
													<td>Parth Brid</td>
													<td>Sorry for that yes I was but I am recovering Thank you for the below asking and prayers</td>
													<td>email</td>
													<td>
														<span class="dropdown">
															<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
																<i class="la la-ellipsis-h"></i>
															</a>
															<div class="dropdown-menu dropdown-menu-right">
																<a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>
																<a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a>
																<a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>
															</div>
														</span>
														<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
															<i class="la la-edit"></i>
														</a>
													</td>
												</tr>
											</tbody>
										</table>
										<div id="kt_table_1_processing" class="dataTables_processing card" style="display: none;">Processing...</div>
									</div>
								</div>
								<div class="row" id="competitor_rank_paggination">
									<div class="col-sm-12 col-md-5">
										<div class="dataTables_info" id="kt_table_1_info" role="status" aria-live="polite">Showing 1 to 10 of 350 entries</div>
									</div>
									<div class="col-sm-12 col-md-7 dataTables_pager">
										<div class="dataTables_length" id="kt_table_1_length">
											<label>Display <select name="kt_table_1_length" aria-controls="kt_table_1" class="custom-select custom-select-sm form-control form-control-sm"><option value="5">5</option><option value="10">10</option><option value="25">25</option><option value="50">50</option></select></label>
										</div>
										<div class="dataTables_paginate paging_simple_numbers" id="kt_table_1_paginate">
											<ul class="pagination">
												<li class="paginate_button page-item previous disabled" id="kt_table_1_previous">
													<a href="#" aria-controls="kt_table_1" data-dt-idx="0" tabindex="0" class="page-link">
														<i class="la la-angle-left"></i>
													</a>
												</li>
												<li class="paginate_button page-item active competitor_rank_paggination_number">
													<a href="javascript:;" aria-controls="kt_table_1" data-dt-idx="1" tabindex="0" class="page-link">1</a>
												</li>
												<li class="paginate_button page-item ">
													<a href="#" aria-controls="kt_table_1" data-dt-idx="2" tabindex="0" class="page-link">2</a>
												</li>
												<li class="paginate_button page-item "><a href="#" aria-controls="kt_table_1" data-dt-idx="3" tabindex="0" class="page-link">3</a></li><li class="paginate_button page-item "><a href="#" aria-controls="kt_table_1" data-dt-idx="4" tabindex="0" class="page-link">4</a></li><li class="paginate_button page-item "><a href="#" aria-controls="kt_table_1" data-dt-idx="5" tabindex="0" class="page-link">5</a></li><li class="paginate_button page-item disabled" id="kt_table_1_ellipsis"><a href="#" aria-controls="kt_table_1" data-dt-idx="6" tabindex="0" class="page-link">…</a></li><li class="paginate_button page-item "><a href="#" aria-controls="kt_table_1" data-dt-idx="7" tabindex="0" class="page-link">35</a></li><li class="paginate_button page-item next" id="kt_table_1_next"><a href="#" aria-controls="kt_table_1" data-dt-idx="8" tabindex="0" class="page-link"><i class="la la-angle-right"></i></a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>

				</div>

				<!-- end:: Content -->
			</div>

			<!-- begin:: Footer -->
			<div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
				<div class="kt-container  kt-container--fluid ">
					<div class="kt-footer__copyright">
						2020&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Keenthemes</a>
					</div>
					<div class="kt-footer__menu">
						<a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">About</a>
						<a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Team</a>
						<a href="http://keenthemes.com/metronic" target="_blank" class="kt-footer__menu-link kt-link">Contact</a>
					</div>
				</div>
			</div>

			<!-- end:: Footer -->
		</div>
	</div>
</div>