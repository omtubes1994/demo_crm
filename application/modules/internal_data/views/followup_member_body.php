<div class="row">
    <input type="text" class="form-control" name="comp_mst_id" value="<?php echo $comp_mst_id;  ?>" hidden/>
	<div class="col-md-4 form-group">
    	<label for="contact_date">Connect Date</label>
		<input type="text" name="connected_on" class="form-control validate hasdatepicker" value="<?php echo date('d-m-Y'); ?>">
    </div>
	<div class="col-md-4 form-group">
    	<label for="contact_mode">Connect Mode</label>
        <select class="form-control validate[required]" name="connect_mode">
        	<option value=""></option>
            <option value="WhatsApp">Whatsapp</option>
            <option value="Call">Call</option>
            <option value="Linkedin">LinkedIn</option>
            <option value="Email">Email</option>
        </select>
    </div>

    <div class="col-md-4 form-group">
    	<label for="email_status">Email Sent</label>
		<select class="form-control validate[required]" name="email_sent">
        	<option value=""></option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
</div>
<div class="row">
    <div class="col-md-12 form-group">
    	<label for="comments">Comments</label>
        <textarea name="comments" class="form-control validate[maxSize[100],required]"></textarea>
    </div>
</div>
<div class="row">
	<div class="col-md-6 align-self-center">
        <button class="btn btn-success save_followup" id="comp_dtl_id" type="reset">Submit</button>
    	<input type="hidden" id="comp_dtl_id" name="comp_detail_id">
        <button type="reset" class="btn btn-primary" data-toggle="modal" data-backdrop="static" data-target="#taskModal" id="createTask">Create Task</button>
    </div>
</div>
	
<hr/>
<h4>Connect History</h4>
<div>
   	<table class="table table-bordered" id="invoice_connect_history">
       	<thead>
        	<tr>
				<th>Contacted On</th>
                <th>Contact Mode</th>
                <th>Email Sent</th>
                <th>Comments</th>
            </tr>
        </thead>
        <tbody>
           	<?php if(!empty($follow_up_details)) { ?>
               	<?php foreach($follow_up_details as $follow_up_single_details) {?>
					<!-- <tr>			 -->
               		<tr comp_detail_id="<?php echo $follow_up_single_details['comp_detail_id']; ?>">
               		<td><?php echo date('d F, Y', strtotime($follow_up_single_details['connected_on']));?></td>
               		<td><?php echo $follow_up_single_details['connect_mode'];?></td>
               		<td><?php echo $follow_up_single_details['email_sent'];?></td>
               		<td><?php echo $follow_up_single_details['comments'];?></td>
              		</tr>	
              	<?php }?>
            <?php }?>
        </tbody>
    </table>
</div>
