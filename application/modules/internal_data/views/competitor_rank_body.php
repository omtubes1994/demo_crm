<?php if(!empty($competitor_ranks_list)) {
	foreach ($competitor_ranks_list as $competitor_ranks_list_key => $competitor_ranks_list_value) {

		// echo "<pre>";print_r($competitor_ranks_list_value);echo"</pre><hr>";exit;	
?>			
	<tr role="row" class="<?php echo (($competitor_ranks_list_key+1)/2 == 00)?'even':'odd'?>">
		<td class="sorting_1" tabindex="0"><?php echo $competitor_ranks_list_value['rank']; ?></td>
		<td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="OrderID">
			<span style="width: 181px;">
				<div class="kt-user-card-v2">
					<div class="kt-user-card-v2__pic">
						<img src="<?php echo base_url('/assets/media/flags/'.$competitor_ranks_list_value['country_flag_img_name']);?>" alt="photo" class="img img-responsive rounded-circle" style="width: 30px">
					</div>
					<div class="kt-user-card-v2__details">
						<span class="kt-user-card-v2__name"><?php echo $competitor_ranks_list_value['customer_name']; ?></span>
						<a href="#" class="kt-user-card-v2__email kt-link"><?php echo $competitor_ranks_list_value['country_name']; ?></a><br>
						<a href="#" class="kt-user-card-v2__email kt-link"><?php echo $competitor_ranks_list_value['lead_type']; ?></a><br>
						<a href="#" class="kt-user-card-v2__email kt-link"><?php echo $competitor_ranks_list_value['last_buying_date']; ?></a>
					</div>
				</div>
			</span>
		</td>
		<td>
			<select class="form-control form-control-sm form-filter kt-input">
				<option value="">Select Stage Name</option>
				<?php foreach($stage_details as $stage_details_single) {?>
					<option value="<?php echo $stage_details_single['lead_stage_id'];?>" <?php echo ($competitor_ranks_list_value['lead_stage'] == $stage_details_single['lead_stage_id']) ? 'selected': '';?>><?php echo $stage_details_single['stage_name'];?></option>
				<?php }?>
			</select>
			<br>
			<span style="font-weight: lighter;" class="last_contacted"><?php echo $competitor_ranks_list_value['connected_on']; ?></span>
			<br>	
			<?php
				$member_class_name = '';
				if(!empty($competitor_ranks_list_value['no_of_employees'])) {

					$member_class_name = 'fa-users';
					if(in_array($competitor_ranks_list_value['no_of_employees'], array('1-9','10-25'))) {

						$member_class_name = 'fa-user';
					} else if (in_array($competitor_ranks_list_value['no_of_employees'], array('25-50', '50-100'))) {

						$member_class_name = 'fa-user-friends';
					}
				}
			?>
			<i class="fas <?php echo $member_class_name;?>"></i> <span style="font-size: 12px;"><?php echo $competitor_ranks_list_value['no_of_employees']; ?></span>
		</td>

		<td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="Ship City">
			<span style="width: 181px;">
				<div class="kt-user-card-v2">
					<div class="kt-user-card-v2__details">
						<span class="kt-user-card-v2__name"><?php echo $competitor_ranks_list_value['member_name']; ?></span>
						<a href="#" class="kt-user-card-v2__email kt-link"><?php echo $competitor_ranks_list_value['designation']; ?></a>
					</div>
				</div>
			</span>
		</td>
		<td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="Ship City">
			<span style="width: 181px;">
				<div class="kt-user-card-v2">
					<div class="kt-user-card-v2__details">
						<span class="kt-user-card-v2__name"><?php echo $competitor_ranks_list_value['email']; ?></span>
						<a href="#" class="kt-user-card-v2__email kt-link"><?php echo $competitor_ranks_list_value['mobile']; ?></a>
					</div>
				</div>
			</span>
			<span data-toggle="modal" data-target="#invoice_member_modal" customer_id="<?php echo $competitor_ranks_list_value['id']; ?>" style="cursor: pointer; color: #9583ce; font-weight: bold; padding-left: 80%;"class="mem_count">
				<?php echo $competitor_ranks_list_value['member_count']; ?>					
			</span>
			/
			<span data-toggle="modal" data-target="#invoice_member_modal" customer_id="<?php echo $competitor_ranks_list_value['id']; ?>" style="cursor: pointer; color: #ce8383; font-weight: bold;"class="mem_count">
				<?php echo $competitor_ranks_list_value['non_member_count']; ?>
			</span>
		</td>
		<?php if($this->session->userdata('role') != 5) { ?>
			<td><?php echo $competitor_ranks_list_value['sales_person_name']; ?></td>
		<?php } ?>
		<td><?php echo $competitor_ranks_list_value['comments']; ?></td>
		<td><?php echo $competitor_ranks_list_value['connect_mode']; ?></td>
		<td>
			<a href="<?php echo base_url('internal_data/update_competitor_rank_invoice/'.$competitor_ranks_list_value['id']);?>" target="_blank" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
				<i class="la la-edit"></i>
			</a>
			<button class="btn btn-outline-info btn-icon kt-spinner kt-spinner--center kt-spinner--sm kt-spinner--success <?php echo $competitor_ranks_list_value['id'];?>" style="display: none;"></button>
			<!-- <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md get_graph_for_invoice  <?php echo $competitor_ranks_list_value['id'];?>" company-name="<?php echo $competitor_ranks_list_value['customer_name'];?>" customer-id="<?php echo $competitor_ranks_list_value['id'];?>"title="View">
				<i class="la la-bar-chart"></i>
			</a> -->
			<?php if(!empty($uri)){?>
			<a href="<?php echo $uri[0]['url']."%22:%22".strtoupper(str_replace(array(',','.','>','<','&','(',')',' ','-','@','/','!'), '',$competitor_ranks_list_value['customer_name'])).'%22%7D'?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" target="_blank"><i class="la la-bullhorn"></i></a>
		    <?php } ?>
		</td>
	</tr>
<?php }	} ?>