

<table class="table table-bordered" id="invoice_main_member">
	<thead>
		<tr>
			<th colspan="3" style="text-align:center">Buyers</th>
		</tr>
		<tr>
			<th>Name</th>
			<th>Email</th>
			<th>Whatsapp</th>
		</tr>
	</thead>
    <tbody>
        <?php if(!empty($main_buyer)){ ?>
            <?php foreach($main_buyer as $single_main_buyer){ ?>
                <tr>
                    <td><?php echo $single_main_buyer['member_name'];?></td>
                    <td><?php echo $single_main_buyer['designation'];?></td>
                    <td><?php echo $single_main_buyer['email_sent'];?></td>
                </tr>
            <?php }?>
        <?php }?>
    </tbody>
</table>
<table class="table table-bordered" id="invoice_other_member">
	<thead>
		<tr>
			<th colspan="3" style="text-align:center">Other Member</th>
		</tr>
		<tr>
			<th>Name</th>
			<th>Email</th>
			<th>Whatsapp</th>
		</tr>
	</thead>
    <tbody>
        <?php if(!empty($other_member)){ ?>
            <?php foreach($other_member as $single_other_buyer){ ?>
                <tr>
                    <td><?php echo $single_other_buyer['member_name'];?></td>
                    <td><?php echo $single_other_buyer['designation'];?></td>
                    <td><?php echo $single_other_buyer['email_sent'];?></td>
                </tr>
            <?php }?>
        <?php }?>
    </tbody>
</table>