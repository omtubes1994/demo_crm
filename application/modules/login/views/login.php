<!DOCTYPE html>
<html lang="en">

	<!-- begin::Head -->
	<head>
		<base href="<?php echo site_url(); ?>">
		<meta charset="utf-8" />
		<title></title>
		<meta name="description" content="Login page example">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!--begin::Fonts -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

		<!--end::Fonts -->

		<!--begin::Page Custom Styles(used by this page) -->
		<link href="assets/css/pages/login/login-3.css" rel="stylesheet" type="text/css" />

		<!--end::Page Custom Styles -->

		<!--begin::Global Theme Styles(used by all pages) -->
		<link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />

		<!--end::Global Theme Styles -->

		<!--begin::Layout Skins(used by all pages) -->
		<link href="assets/css/skins/header/base/light.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/skins/brand/dark.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/skins/aside/dark.css" rel="stylesheet" type="text/css" />

		<!--end::Layout Skins -->
		<link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

		<!-- begin:: Page -->
		<div class="kt-grid kt-grid--ver kt-grid--root">
			<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v3 kt-login--signin" id="kt_login">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url(assets/media/bg/bg-3.jpg);">
					<div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
						<div class="kt-login__container">
							<div class="kt-login__logo">
								<a href="#">
									<img src="assets/media/logos/logo.png" width="150" height="50">
								</a>
							</div>
							<div class="kt-login__signin">
								<form id="om_tubes_login_form" class="kt-form">
									<div class="kt-login__head">
										<h3 class="kt-login__title">Login dashboard</h3>
									</div>
									<div class="input-group">
										<input class="form-control" type="text" placeholder="Username / Email" name="email" autocomplete="off">
									</div>
									<div class="input-group">
										<input class="form-control" type="password" placeholder="Password" name="password">
									</div>
									<div class="input-group show_otp_div" style="display:none;">
										<input class="form-control" type="text" placeholder="Enter verification code" name="verification_code">
									</div>
									<div class="row kt-login__extra" style="display:none;">
										<div class="col kt-align-right">
											<a href="javascript:;" id="kt_login_forgot" class="kt-login__link">Forget Password ?</a>
										</div>
									</div>
								</form>
								<div class="kt-section__content show_otp_div" style="display:none;">
									
									<div class="kt-space-10"></div>
									<label class="kt-font-bolder">OTP can be resent in 1 minute</label>
									<div class="kt-space-10"></div>
									<div class="progress">
										<div class="progress-bar progress-bar-striped progress-bar-animated add_otp_progress_bar" role="progressbar" aria-valuenow="11" aria-valuemin="0" aria-valuemax="100" style="width: 11%; background-color: #454889;"></div>
									</div>
									<div class="kt-space-10"></div>
									
								</div>
								<div class="row kt-login__extra" style="margin-top: 30px;margin-bottom: 15px;color: #74788d;font-size: 1rem;">
									<div class="col resend_otp_div" style="display:none;">
										<button class="btn btn-brand btn-elevate kt-login__btn-primary send_otp" style="background-color: #454889;border-color: #454889;">Resend OTP</button>
									</div>
									<div class="col kt-align-right">
										<button class="btn btn-brand btn-elevate kt-login__btn-primary check_login" style="background-color: #454889;border-color: #454889;">Sign In</button>
									</div>
								</div>
							</div>
							<div class="kt-login__forgot">
								<div class="kt-login__head">
									<h3 class="kt-login__title">Forgotten Password ?</h3>
									<div class="kt-login__desc">Enter your email to reset your password:</div>
								</div>
								<form class="kt-form" action="">
									<div class="input-group">
										<input class="form-control" type="text" placeholder="Email" name="email" id="kt_email" autocomplete="off">
									</div>
									<div class="kt-login__actions">
										<button id="kt_login_forgot_submit" class="btn btn-brand btn-elevate kt-login__btn-primary">Request</button>&nbsp;&nbsp;
										<button id="kt_login_forgot_cancel" class="btn btn-light btn-elevate kt-login__btn-secondary">Cancel</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- end:: Page -->

		<!-- begin::Global Config(global config for global JS sciprts) -->
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": [
							"#c5cbe3",
							"#a1a8c3",
							"#3d4465",
							"#3e4466"
						],
						"shape": [
							"#f0f3ff",
							"#d9dffa",
							"#afb4d4",
							"#646c9a"
						]
					}
				}
			};
		</script>

		<!-- end::Global Config -->

		<!--begin::Global Theme Bundle(used by all pages) -->
		<script src="assets/plugins/global/plugins.bundle.js" type="text/javascript"></script>
		<script src="assets/js/scripts.bundle.js" type="text/javascript"></script>

		<!--end::Global Theme Bundle -->

		<!--begin::Page Scripts(used by this page) -->
		<script src="assets/js/pages/custom/login/login-general.js" type="text/javascript"></script>
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

		<!--end::Page Scripts -->
		<script type="text/javascript">
			jQuery(document).ready(function() {
				$('.check_login, .send_otp').click(function(){
	    			$('.resend_otp_div').hide();
					$.ajax({
						type: 'POST',
						data: {call_type:'check_login', form_data: $('form#om_tubes_login_form').serializeArray()},
						url: "<?php echo base_url('login/ajax_function');?>",
						dataType: 'JSON',
						success: function(res){
							if(res.status == 'successful') {
								set_reset_spinner($('button.check_login'), false);
						    	toastr.success(res.message);
						    	if(res.message == 'OTP is sent on to your number valid for only 1 minute' || res.message == 'OTP is sent on to your email valid for only 1 minute' || res.message == 'OTP is sent on to your whatsapp valid for only 1 minute') {
									
						    		$('.show_otp_div').show();
						    		$('div.add_otp_progress_bar').attr('aria-valuenow',40);
									$('div.add_otp_progress_bar').css('width','40%');
									set_otp_progress();
							    	setTimeout(function(){

						    			$('.resend_otp_div').show();
							    	}, 60000);
						    	} else if(res.message == 'OTP is matched' || res.message == 'Welcome') {
							    	setTimeout(function(){
										if(res.url != '') {

											location.href = res.url;
										}
							    	}, 1000);
						    	}
							}
						},
						beforeSend: function(res){
							
							set_reset_spinner($('button.check_login'));
						}
					});
				});
			});
			function set_otp_progress() {
				setTimeout(add_progress, 1000);
			}
			function add_progress() {

				var progress_value = Number($('div.add_otp_progress_bar').attr('aria-valuenow'));
				progress_value = Math.ceil(progress_value+1);
				$('div.add_otp_progress_bar').attr('aria-valuenow',progress_value);
				$('div.add_otp_progress_bar').css('width',progress_value+'%');
				if(progress_value <= 100) {

					$('div.add_otp_progress_bar').html(100-progress_value+' seconds left');
					set_otp_progress();
				}
			}
			function set_reset_spinner(obj, set_unset_flag = true) {

	    		if(set_unset_flag) {

		    		$(obj).addClass('kt-spinner');
			    	$(obj).addClass('kt-spinner--right');
			    	$(obj).addClass('kt-spinner--sm');
			    	$(obj).addClass('kt-spinner--light');	
	    		} else{

	    			$(obj).removeClass('kt-spinner');
			    	$(obj).removeClass('kt-spinner--right');
			    	$(obj).removeClass('kt-spinner--sm');
			    	$(obj).removeClass('kt-spinner--light');
	    		}
		    }
		    toastr.options = {
			    "closeButton": true,
			    "debug": false,
			    "newestOnTop": true,
			    "progressBar": true,
			    "positionClass": "toast-top-right",
			    "preventDuplicates": true,
			    "onclick": null,
			    "showDuration": "300",
			    "hideDuration": "1000",
			    "timeOut": "5000",
			    "extendedTimeOut": "1000",
			    "showEasing": "swing",
			    "hideEasing": "linear",
			    "showMethod": "fadeIn",
			    "hideMethod": "fadeOut"
			};
		</script>
	</body>

	<!-- end::Body -->
</html>