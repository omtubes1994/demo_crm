<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('login_model');
		$this->load->model('quotations/quotation_model');
		$this->load->model('common/common_model');
		error_reporting(0);
	}

	public function index_bckp(){
		if($this->session->has_userdata('user_id') && FALSE){
			// $this->session->sess_destroy();
			redirect('home/dashboard');
		}else{
			if(!empty($this->input->post())){
				$post_fields = $this->security->xss_clean($this->input->post());
				// $userdata = $this->login_model->checkLogin($post_fields);
				// if($userdata === false){
				// 	redirect('login', 'refresh');
				// }else if(is_array($userdata)){
				// 	$this->session->set_userdata($userdata);
					
				// 		 redirect('home/dashboard');
						 
						 
				// 		 $id = $this->session->set_userdata($userdata);
				// 		 $oldip = $this->login_model->getipDetails($id);
				// 		 $ip = $this->input->ip_address();
				// 		 date_default_timezone_set('Asia/Karachi');
				// 		 $now = date('Y-m-d H:i:s');
				// 		 $this->login_model->insertIp( $id, $ip, $now);
						 
						 
					
				
				// }
				$user_details = $this->login_model->checkLogin($post_fields);
				if($user_details){
						
					$otp = $this->otp_generator();
					if($this->common_model->insert_data_sales_db('login_verfication',array('user_id'=>$user_details['user_id'],'login_type'=>'sms','confirm_password'=>$otp))){
						$this->send_otp($otp);	
					}
				}else{
					redirect('login', 'refresh');
				}
			}else{
				$this->load->view('login');
			}
		}
	}

	public function index(){
		if(!empty($this->session->userdata('user_id'))){
			redirect('home/dashboard');
		}else{
			$this->load->view('login');
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('login', 'refresh');
	}

	public function changePassword(){
		if(!empty($this->input->post())){
			$res = $this->login_model->updatePassword($this->input->post());
			if($res == 0){
				$this->session->set_flashdata('failed', 'Current password is incorrect');
			}else if($res == 1){
				$this->session->set_flashdata('success', 'Password updated successfully!');
			}
			redirect('login/changePassword');
		}else{
			if(!$this->session->has_userdata('user_id')){
				redirect('login');
			}else{
				$this->load->view('header', array('title' => 'Change Password'));
				$this->load->view('sidebar', array('title' => 'Change Password'));
				$this->load->view('password_change');
				$this->load->view('footer');
			}
		}
	}

	public function otherLogin(){
		if(!empty($this->input->post())){
			$current_user = $this->input->post('current_user');
			$next_user = $this->input->post('next_user');
			$verify = $this->login_model->verify_sub_user($current_user, $next_user);
			if($verify == true){
				$this->session->unset_userdata('user_id');
				$this->session->unset_userdata('name');
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('email');
				$this->session->unset_userdata('role');
				$this->session->unset_userdata('module');
				$this->session->unset_userdata('email');
				$this->session->unset_userdata('last_ip');
				$this->session->unset_userdata('last_login');
				$this->session->unset_userdata('current_ip');
				$this->session->unset_userdata('current_login');
				$this->session->unset_userdata('sub_users');
				//$this->session->sess_destroy();
				$userdata = $this->login_model->otherLogin($next_user);
				if($userdata === false){
					redirect('login', 'refresh');
				}else if(is_array($userdata)){
					$this->session->set_userdata($userdata);
					redirect('home/dashboard');
				}	
			}else{
				redirect('login');
			}
		}else{
			redirect('login');
		}
	}

	public function send_otp($mobile_number = '9082159156', $otp = 90) {

		// echo $mobile_number, $otp,"</hr>";
		// die('came in');
		$sms_txt = 'Your OTP to login to OmTubes CRM is '.$otp;
		$this->sendSms($mobile_number, str_replace(' ', '%20', $sms_txt), '1307162123022934456');			
		// $this->sendSms('9082159156', str_replace(' ', '%20', $sms_txt), '1307162123022934456');			
	}

	public function sendSms($mobile,$sms_txt, $dlt_id){ 

		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.msg91.com/api/sendhttp.php?authkey=345359A1zGdmFe5f930cf3P1&mobiles=91".$mobile."&message=".$sms_txt."&sender=OMTUBE&route=4&DLT_TE_ID=".$dlt_id,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_SSL_VERIFYHOST => 0,
		  CURLOPT_SSL_VERIFYPEER => 0,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);
		
		if ($err) {
		  // echo "cURL Error #:" . $err;
		} else {
		  // echo $response,"<hr>";
		}
	}

	public function ajax_function() {

		if($this->input->is_ajax_request()){
			$call_type = $this->input->post('call_type');
			$response['status'] = 'successful';	
			switch ($call_type) {

				case 'check_login':

					$post_fields = $this->security->xss_clean($this->input->post());
					$form_data = array_column($post_fields['form_data'],'value','name');
					$response['message'] = 'User name or password is empty';
					if(!empty($form_data['email']) && !empty($form_data['password'])){

						$response['message'] = 'User name or password is incorrect';
						$response['url'] = '';
						$user_details = $this->login_model->checkLogin($form_data);
						if(!empty($user_details)) {

							$response = $this->_manage_user_login($form_data, $user_details);
							$response['status'] = 'successful';
						}
					}
				break;
			}
			echo json_encode($response);

		} else{
			die('access is not allowed to this function');
		}
	}

	private function _manage_user_login($form_data, $user_details){

		$response['message'] = 'User name or password is incorrect';
		$response['url'] = '';
		// check user id is excluded or not
		$check_id_excluded_or_not = $this->common_model->get_all_conditional_data_sales_db('login_type',array('status'=>'Active', 'user_id'=>$user_details['user_id']), 'login_verification_ignore', 'row_array', array(), array(), 'login_type');
		// echo "<pre>";print_r($check_id_excluded_or_not);echo"</pre><hr>";die('debug');
		if(empty($check_id_excluded_or_not)) {

			//check OTP already sent or not in last 10 minutes
			$check_otp_sent_or_not = $this->login_model->check_otp_exist_or_not('status = "Active" AND user_id ='.$user_details['user_id'].' AND add_time >= date_sub(now(), INTERVAL 10 MINUTE)');
			if(empty($check_otp_sent_or_not)) {

				$response['message'] = 'OTP is sent on to your number valid for only 1 minute';
				$this->send_otp_via_sms_or_email($user_details, 'sms');
			} else if(count($check_otp_sent_or_not) < 4){

				if(empty($form_data['verification_code'])) {

					if(count($check_otp_sent_or_not) < 2) {
						
						$response['message'] = $this->send_otp_via_sms_or_email($user_details, 'sms');

					} else if(count($check_otp_sent_or_not) < 4) {
					
						$response['message'] = $this->send_otp_via_sms_or_email($user_details, 'email');

					}

				} else {

					$response['message'] = 'OTP is invalid and ';
					$otp_result = $this->login_model->check_otp_exist_or_not('status = "Active" AND user_id ='.$user_details['user_id'].' AND add_time >= date_sub(now(), INTERVAL 1 MINUTE) AND confirm_password = "'.$form_data['verification_code'].'"');
					if(!empty($otp_result)){

						$response = $this->_otp_match_set_session_data($user_details);

					} else {
						if(count($check_otp_sent_or_not) < 2) {

							$response['message'] .= $this->send_otp_via_sms_or_email($user_details, 'sms');

						} else if(count($check_otp_sent_or_not) < 4) {
						
							$response['message'] .= $this->send_otp_via_sms_or_email($user_details, 'email');

						}

					}

				}

			} else {
			
				$response['message'] = 'OTP limit is over try login after 5 minutes';

			}
		} else {

			if($check_id_excluded_or_not['login_type'] == 'email'){
				
				$response = $this->_manage_otp_via_email_or_sms($form_data, $user_details, 'email');
				
			}else if($check_id_excluded_or_not['login_type'] == 'sms'){
				
				$response = $this->_manage_otp_via_email_or_sms($form_data, $user_details, 'sms');
				
			}else if($check_id_excluded_or_not['login_type'] == 'whatsapp'){
				
				$response = $this->_manage_otp_via_email_or_sms($form_data, $user_details, 'whatsapp');
				
			}else{
				
				$response = $this->_otp_match_set_session_data($user_details);
			}
		}
		return $response;
	}

	private function _manage_otp_via_email_or_sms($form_data, $user_details, $login_type){

		$response['message'] = 'User name or password is incorrect';
		$response['url'] = '';
		if(empty($form_data['verification_code'])) {

			$response['message'] = $this->send_otp_via_sms_or_email($user_details, $login_type);

		}else{

			$response['message'] = 'OTP is invalid and ';
			$otp_result = $this->login_model->check_otp_exist_or_not('status = "Active" AND user_id ='.$user_details['user_id'].' AND add_time >= date_sub(now(), INTERVAL 1 MINUTE) AND confirm_password = "'.$form_data['verification_code'].'"');
			if(!empty($otp_result)){

				$response = $this->_otp_match_set_session_data($user_details);

			} else {
			
				$response['message'] .= $this->send_otp_via_sms_or_email($user_details, $login_type);
			}
		}

		return $response;
	}

	private function _otp_match_set_session_data($user_details){

		$this->session->set_userdata($this->login_model->get_user_details($user_details));
		$this->session->set_userdata(array('global_user_details'=> array('user_name'=> $user_details['username'], 'password'=> $user_details['password'])));
		$response['message'] = 'OTP is matched';
		$response['url'] = base_url('home/dashboard');

		return $response;
	}
	
	private function send_otp_via_sms_or_email($user_details, $otp_type) {

		// echo "<pre>";print_r($user_details);echo"</pre><hr>";exit;
		$return_message = '';
		$otp = $this->otp_generator();
		if($otp_type == 'sms') {

			$return_message = 'OTP is sent on to your number valid for only 1 minute';
			$check_otp_expired = $this->login_model->check_otp_exist_or_not('status = "Active" AND user_id ='.$user_details['user_id'].' AND add_time >= date_sub(now(), INTERVAL 1 MINUTE) AND login_type = "sms"');
			if(empty($check_otp_expired)){

				if($this->common_model->insert_data_sales_db('login_verfication',array('user_id'=>$user_details['user_id'],'login_type'=>'sms','confirm_password'=>$otp))){

						$this->send_otp($user_details['mobile'], $otp);

				}

			} else {

				$return_message = 'OTP is already sent on to your number';

			}

		} else if ($otp_type == 'email') {

			$return_message = 'OTP is sent on to your email valid for only 1 minute';
			$check_otp_expired = $this->login_model->check_otp_exist_or_not('status = "Active" AND user_id ='.$user_details['user_id'].' AND add_time >= date_sub(now(), INTERVAL 1 MINUTE) AND login_type = "email"');
			if(empty($check_otp_expired)){

				if($this->common_model->insert_data_sales_db('login_verfication',array('user_id'=>$user_details['user_id'],'login_type'=>'email','confirm_password'=>$otp))){

					$this->sendMail($user_details['email'], $otp);

				}

			} else {

				$return_message = 'OTP is already sent on to your email';

			}
			
		} else if ($otp_type == 'whatsapp') {

			$return_message = 'OTP is sent on to your whatsapp valid for only 1 minute';
			$check_otp_expired = $this->login_model->check_otp_exist_or_not('status = "Active" AND user_id ='.$user_details['user_id'].' AND add_time >= date_sub(now(), INTERVAL 1 MINUTE) AND login_type = "whatsapp"');
			if(empty($check_otp_expired)){

				if($this->common_model->insert_data_sales_db('login_verfication',array('user_id'=>$user_details['user_id'],'login_type'=>'whatsapp','confirm_password'=>$otp))){

					$this->send_otp_whatsapp($user_details['mobile'], $otp);

				}

			} else {

				$return_message = 'OTP is already sent on to your whatsapp';

			}
			
		}
		return $return_message;

	}

	private function otp_generator(){
		$id = '';
		$possible_chars = '0123456789';
		for($i=1; $i<=5; $i++)
		{
			$id .= $possible_chars[rand(0,(strlen($possible_chars)-1))];
		}

		return $id;
	}

	public function send_otp_email() {

		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'crm@omtubes.com',
			'smtp_pass' => 'cqecjzodmvknusqh',
			'smtp_crypto' => 'ssl',
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'smtp_timeout' => '4', //in seconds
			'wordwrap' => TRUE
		);
        $message = 'Your OTP to login into OmTubes CRM is 88888';
        $res = $this->load->library('email', $config);
      	$this->email->set_newline("\r\n");
      	$this->email->from('crm@omtubes.com', 'OTP Verification'); // change it to yours
      	$this->email->to('abhishekdesai48@gmail.com');// change it to yours
      	$this->email->subject('Om tubes OTP Verification');
      	$this->email->message($message);
      	$response = $this->email->send();
      	echo "<pre>";var_dump($response);echo"</pre><hr>";die('debug');	
	}
	
	private function sendMail($email_id, $otp){

		// $config = array(
		//     'protocol' => 'mail', // 'mail', 'sendmail', or 'smtp'
		//     'smtp_host' => 'mail.gmail.com', 
		//     'smtp_port' => 465,
		//     'smtp_user' => 'otp.omtubes@gmail.com', // change it to yours
		//   	'smtp_pass' => 'Omtubes@123', // change it to yours
		//     'mailtype' => 'html', //plaintext 'text' mails or 'html'
		//     'smtp_timeout' => '4', //in seconds
		//     'charset' => 'iso-8859-1',
		//     'wordwrap' => TRUE
		// );
		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'crm@omtubes.com',
			'smtp_pass' => 'cqecjzodmvknusqh',
			// 'smtp_pass' => 'qnyurhojswuvamkt',
			// 'smtp_pass' => 'ofohsjgipyyzsced',
			// 'smtp_pass' => 'gnhejaqmiolzbrki',
			'smtp_crypto' => 'ssl',
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'smtp_timeout' => '4', //in seconds
			'wordwrap' => TRUE
		);
        $message = 'Your OTP to login into OmTubes CRM is '.$otp;
        $res = $this->load->library('email', $config);
      	$this->email->set_newline("\r\n");
      	$this->email->from('crm@omtubes.com', 'OTP Verification'); // change it to yours
      	$this->email->to($email_id);// change it to yours
      	$this->email->subject('Om tubes OTP Verification');
      	$this->email->message($message);
      	$response = $this->email->send();
      	// echo "<pre>";var_dump($response);echo"</pre><hr>";die('debug');
      	// echo $response;
	}

	public function test_whatsapp_otp(){

		$this->send_otp_whatsapp(9082159156, 83738);
	}
	private function send_otp_whatsapp($mobile_number, $otp){

		// URL and data
        $url = 'https://live-mt-server.wati.io/200110/api/v1/sendTemplateMessage?whatsappNumber=%2B91'.$mobile_number;
        $data = json_encode(array(
            'broadcast_name' => 'crm6',
            'template_name' => 'crm6',
            'parameters' => array(
                array(
                    'name' => 'your_number',
                    'value' => $otp
                )
            )
        ));

        // Headers
        $headers = array(
            'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0NzVhNGE4OC03ZDQ5LTRjMmItOGM0ZS01N2RlYzNkMGZiZWMiLCJ1bmlxdWVfbmFtZSI6ImNybUBvbXR1YmVzLmNvbSIsIm5hbWVpZCI6ImNybUBvbXR1YmVzLmNvbSIsImVtYWlsIjoiY3JtQG9tdHViZXMuY29tIiwiYXV0aF90aW1lIjoiMTAvMDQvMjAyMyAxMjoyNTowNyIsImRiX25hbWUiOiJtdC1wcm9kLVRlbmFudHMiLCJ0ZW5hbnRfaWQiOiIyMDAxMTAiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJBRE1JTklTVFJBVE9SIiwiZXhwIjoyNTM0MDIzMDA4MDAsImlzcyI6IkNsYXJlX0FJIiwiYXVkIjoiQ2xhcmVfQUkifQ.cFanSeCHT9pYMdMtdjERT-qu3waG7KGE6TMc1N03l1s',
            'Content-Type: application/json'
        );

        // cURL initialization
        $ch = curl_init($url);

        // Set cURL options
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // Execute cURL and get the response
        $response = curl_exec($ch);

        // Check for cURL errors
        if (curl_errno($ch)) {
            echo 'cURL Error: ' . curl_error($ch);
        }

        // Close cURL
        curl_close($ch);

        // Output the response
        // echo $response;
	}
}
