<?php 
class Login_model extends CI_Model{

	function __construct(){
		parent::__construct();
		$CI = &get_instance();
		$this->db2 = $CI->load->database('marketing', true);
		error_reporting(0);
	}

	function checkLogin($input){
		
		// $this->db->select('u.user_id, name, username, email, role, m.module_controller, u.sub_users');
		// $this->db->join('user_to_module um', 'u.user_id = um.user_id', 'left');
		// $this->db->join('modules m', 'm.module_id = um.module_id', 'inner');
		// $this->db->group_start();
		// $this->db->where('username', $input['email']);
		// $this->db->or_where('email', $input['email']);
		// $this->db->group_end();
		// $this->db->where('password', md5($input['password']));
		// $res = $this->db->get('users u')->result_array();

		$this->db->select('*');
		$this->db->group_start();
		$this->db->where('username', $input['email']);
		$this->db->or_where('email', $input['email']);
		$this->db->group_end();
		$this->db->where('password', md5($input['password']));
		$this->db->where('status', '1');
		$user_details = $this->db->get('users')->row_array();
		if(empty($user_details)) {
			return false;	
		}
		return $user_details;
	}

	// public function get_user_details($user_details) {

	// 	$this->db->update('users', array('last_login' => date('Y-m-d H:i:s')), array('user_id' => $user_details['user_id']));
	// 	$this->db->order_by('log_id', 'desc');
	// 	$this->db->limit(1);
	// 	$ip_details = $this->db->get('ip_logs', array('user_id' => $user_details['user_id']))->row_array();
		
	// 	$ret = array();
	// 	$ret['user_id'] = $user_details['user_id'];
	// 	$ret['name'] = $user_details['name'];
	// 	$ret['username'] = $user_details['username'];
	// 	$ret['email'] = $user_details['email'];
	// 	$ret['role'] = $user_details['role'];
	// 	$sub_users = $user_details['sub_users'];
	// 	if($sub_users != '' && $sub_users != null){
	// 		$this->db->select('user_id, name');
	// 		$this->db->where('user_id in ('.$sub_users.')');
	// 		$ret['sub_users'] = $this->db->get('users')->result_array();
	// 	}
	// 	if(!empty($ip_details)){
	// 		$ret['last_ip'] = $ip_details['ip_address'];
	// 		$ret['last_login'] = $ip_details['login_time'];
	// 	}else{
	// 		$ret['last_ip'] = '';
	// 		$ret['last_login'] = '';
	// 	}
	// 	$ret['current_ip'] = $_SERVER['REMOTE_ADDR'];
	// 	$ret['current_login'] = date('Y-m-d H:i:s');
	// 	$modules_controller_array = $this->db->select('module_controller')->where(array('status' => 'Active'))->get('modules')->result_array();
	// 	$this->db->where(array('status'=>'Active'));
	// 	$this->db->order_by('module_order', 'ASC');
	// 	$ret['main_module'] = $this->db->get('modules')->result_array();
	// 	// $ret['main_module'] = $this->db->get_where('modules', array('status'=>'Active'))->result_array();
	// 	$ret['main_module_access'] = $this->db->get_where('user_to_module', array('user_id'=>$ret['user_id'], 'status' => 'Active'))->result_array();
	// 	$sub_module = $this->db->get_where('sub_module', array('status'=>'Active'))->result_array();
	// 	foreach ($sub_module as $key => $value) {
	// 		$ret['sub_module'][$value['module_id']][] = array('sub_module_id' => $value['sub_module_id'], 'sub_module_name' => $value['sub_module_name'], 'url' => $value['url']);
	// 	}
	// 	$ret['sub_module_access'] = $this->db->get_where('user_to_sub_module', array('user_id'=>$ret['user_id'], 'status' => 'Active'))->result_array();
	// 	$ret['active_module_id'] = 1;
	// 	$ret['active_sub_module_id'] = 1;

	// 	//check lead for user loged in
	// 	if($ret['role'] == 5) {
	// 		$lead_source_array['hetro_leads'] = array( 
	// 												array('source_name' => 'chemical companies', 'sub_module_id' => 14),
	// 												array('source_name' => 'distributors', 'sub_module_id' => 20),
	// 												array('source_name' => 'epc companies', 'sub_module_id' => 16),
	// 												array('source_name' => 'heteregenous tubes india', 'sub_module_id' => 21),
	// 												array('source_name' => 'pvf companies', 'sub_module_id' => 22),
	// 												array('source_name' => 'shipyards', 'sub_module_id' => 13),
	// 												array('source_name' => 'sugar companies', 'sub_module_id' => 17),
	// 												array('source_name' => 'water companies', 'sub_module_id' => 15),
	// 												array('source_name' => 'forged fittings', 'sub_module_id' => 55),
	// 												array('source_name' => 'Miscellaneous Leads', 'sub_module_id' => 62)
	// 											);
	// 		$lead_source_array['primary_leads'] = array(
	// 												array('source_name' => 'pipes', 'sub_module_id' => 12),
	// 												array('source_name' => 'tubes', 'sub_module_id' => 18),
	// 												array('source_name' => 'process control', 'sub_module_id' => 19),
	// 												array('source_name' => 'tubing', 'sub_module_id' => 23),
	// 												array('source_name' => 'hammer union', 'sub_module_id' => 24)
	// 											);
	// 		$lead_count = array();
	// 		foreach ($lead_source_array['hetro_leads'] as $value) {

	// 			$res = $this->db->get_where('clients', array('source' => $value['source_name'], 'assigned_to' => $ret['user_id']))->result_array();
	// 			$lead_count[$value['sub_module_id']] = count($res);
	// 		}
	// 		foreach ($lead_source_array['primary_leads'] as $value) {

	// 			$this->db2->select('count(*) as count');
	// 			$this->db2->where(array('deleted is' => 'Null', 'imp_id >' => 0, 'assigned_to' => $ret['user_id'], 'data_category' => "'".$value['source_name']."'"), NULL, FALSE);
	// 			$res = $this->db2->get('lead_mst')->row_array();
	// 			$lead_count[$value['sub_module_id']] = $res['count'];
	// 		}
	// 		$ret['lead_count'] = $lead_count;
	// 	}else if ($ret['role'] == 1 || $ret['role'] == 2 || $ret['role'] == 16 || $ret['role'] == 17) {

	// 		$ret['lead_count'][14] = 1;
	// 		$ret['lead_count'][20] = 1;
	// 		$ret['lead_count'][16] = 1;
	// 		$ret['lead_count'][21] = 1;
	// 		$ret['lead_count'][22] = 1;
	// 		$ret['lead_count'][13] = 1;
	// 		$ret['lead_count'][17] = 1;
	// 		$ret['lead_count'][15] = 1;
	// 		$ret['lead_count'][55] = 1;
	// 		$ret['lead_count'][12] = 1;
	// 		$ret['lead_count'][18] = 1;
	// 		$ret['lead_count'][19] = 1;
	// 		$ret['lead_count'][23] = 1;
	// 		$ret['lead_count'][24] = 1;
	// 		$ret['lead_count'][62] = 1;
	// 	}
	// 	$data = array(
	// 		'user_id'      =>  $user_details['user_id'],
	// 		'ip_address'   =>  $_SERVER['REMOTE_ADDR'],
	// 		'login_time '  =>  date('Y-m-d H:i:s')
	// 	);
	// 	$this->db->insert('ip_logs', $data);
	// 	//insert new ip
	// 	return $ret;
	// }

	public function get_user_details($user_details) {

		$this->db->update('users', array('last_login' => date('Y-m-d H:i:s')), array('user_id' => $user_details['user_id']));
		$this->db->order_by('log_id', 'desc');
		$this->db->limit(1);
		$ip_details = $this->db->get('ip_logs', array('user_id' => $user_details['user_id']))->row_array();
		
		$ret = array();
		$ret['user_id'] = $user_details['user_id'];
		$ret['name'] = $user_details['name'];
		$ret['username'] = $user_details['username'];
		$ret['email'] = $user_details['email'];
		$ret['role'] = $user_details['role'];
		$sub_users = $user_details['sub_users'];
		if($sub_users != '' && $sub_users != null){
			$this->db->select('user_id, name');
			$this->db->where('user_id in ('.$sub_users.')');
			$ret['sub_users'] = $this->db->get('users')->result_array();
		}
		if(!empty($ip_details)){
			$ret['last_ip'] = $ip_details['ip_address'];
			$ret['last_login'] = $ip_details['login_time'];
		}else{
			$ret['last_ip'] = '';
			$ret['last_login'] = '';
		}
		$ret['current_ip'] = $_SERVER['REMOTE_ADDR'];
		$ret['current_login'] = date('Y-m-d H:i:s');
		$modules_controller_array = $this->db->select('module_controller')->where(array('status' => 'Active'))->get('modules')->result_array();
		$this->db->where(array('status'=>'Active'));
		$this->db->order_by('module_order', 'ASC');
		$ret['main_module'] = $this->db->get('modules')->result_array();
		// $ret['main_module'] = $this->db->get_where('modules', array('status'=>'Active'))->result_array();
		$ret['main_module_access'] = $this->db->get_where('user_to_module', array('user_id'=>$ret['user_id'], 'status' => 'Active'))->result_array();
		$sub_module = $this->db->get_where('sub_module', array('status'=>'Active'))->result_array();
		foreach ($sub_module as $key => $value) {
			$ret['sub_module'][$value['module_id']][] = array('sub_module_id' => $value['sub_module_id'], 'sub_module_name' => $value['sub_module_name'], 'url' => $value['url']);
		}
		$ret['sub_module_access'] = $this->db->get_where('user_to_sub_module', array('user_id'=>$ret['user_id'], 'status' => 'Active'))->result_array();
		$ret['active_module_id'] = 1;
		$ret['active_sub_module_id'] = 1;
		$ret['lead_tab_count'] = array();
		//check lead for user loged in
		if($ret['role'] == 5) {
			$lead_source_array['hetro_leads'] = array( 
													array('source_name' => 'chemical companies', 'sub_module_id' => 14),
													array('source_name' => 'distributors', 'sub_module_id' => 20),
													array('source_name' => 'epc companies', 'sub_module_id' => 16),
													array('source_name' => 'heteregenous tubes india', 'sub_module_id' => 21),
													array('source_name' => 'pvf companies', 'sub_module_id' => 22),
													array('source_name' => 'shipyards', 'sub_module_id' => 13),
													array('source_name' => 'sugar companies', 'sub_module_id' => 17),
													array('source_name' => 'water companies', 'sub_module_id' => 15),
													array('source_name' => 'forged fittings', 'sub_module_id' => 55),
													array('source_name' => 'Miscellaneous Leads', 'sub_module_id' => 62)
												);
			$lead_source_array['primary_leads'] = array(
													array('source_name' => 'pipes', 'sub_module_id' => 12),
													array('source_name' => 'tubes', 'sub_module_id' => 18),
													array('source_name' => 'process control', 'sub_module_id' => 19),
													array('source_name' => 'tubing', 'sub_module_id' => 23),
													array('source_name' => 'hammer union', 'sub_module_id' => 24)
												);
			$lead_source_array['new_hetro_leads'] = array( 
													array('source_name' => 'chemical companies', 'sub_module_id' => 76),
													array('source_name' => 'distributors', 'sub_module_id' => 83),
													array('source_name' => 'epc companies', 'sub_module_id' => 77),
													array('source_name' => 'heteregenous tubes india', 'sub_module_id' => 81),
													array('source_name' => 'pvf companies', 'sub_module_id' => 82),
													array('source_name' => 'shipyards', 'sub_module_id' => 74),
													array('source_name' => 'sugar companies', 'sub_module_id' => 78),
													array('source_name' => 'water companies', 'sub_module_id' => 75),
													array('source_name' => 'forged fittings', 'sub_module_id' => 86),
													array('source_name' => 'Miscellaneous Leads', 'sub_module_id' => 87)
												);
			$lead_source_array['new_primary_leads'] = array(
													array('source_name' => 'pipes', 'sub_module_id' => 73),
													array('source_name' => 'tubes', 'sub_module_id' => 79),
													array('source_name' => 'process_control', 'sub_module_id' => 80),
													array('source_name' => 'tubing', 'sub_module_id' => 84),
													array('source_name' => 'hammer union', 'sub_module_id' => 85)
												);
			$lead_count = array();
			// foreach ($lead_source_array['hetro_leads'] as $value) {

			// 	$res = $this->db->get_where('clients', array('source' => $value['source_name'], 'assigned_to' => $ret['user_id']))->result_array();
			// 	$lead_count[$value['sub_module_id']] = count($res);
			// }
			// foreach ($lead_source_array['primary_leads'] as $value) {

			// 	$this->db2->select('count(*) as count');
			// 	$this->db2->where(array('deleted is' => 'Null', 'imp_id >' => 0, 'assigned_to' => $ret['user_id'], 'data_category' => "'".$value['source_name']."'"), NULL, FALSE);
			// 	$res = $this->db2->get('lead_mst')->row_array();
			// 	$lead_count[$value['sub_module_id']] = $res['count'];
			// }
			foreach ($lead_source_array['new_hetro_leads'] as $value) {

				$this->db->select('count(*) as count, product_category.product_name');
				$this->db->join('customer_data','customer_data.customer_mst_id = customer_mst.id');
				$this->db->join('product_category', 'product_category.id = customer_data.product_category_id');
				$res = $this->db->get_where('customer_mst', array('product_category.product_name' => $value['source_name'], 'assigned_to' => $ret['user_id']))->result_array();
				$lead_count[$value['sub_module_id']] = count($res);
			}
			foreach ($lead_source_array['new_primary_leads'] as $value) {

				$this->db->select('count(*) as count, product_category.product_name');
				$this->db->join('customer_data','customer_data.customer_mst_id = customer_mst.id');
				$this->db->join('product_category', 'product_category.id = customer_data.product_category_id');
				$this->db->where(array('customer_mst.deleted is' => 'Null', 'customer_mst.id >' => 0, 'customer_mst.assigned_to' => $ret['user_id'], 'product_category.product_name' => "'".$value['source_name']."'"), NULL, FALSE);
				$res = $this->db->get('customer_mst')->row_array();
				$lead_count[$value['sub_module_id']] = $res['count'];
			}
			$ret['lead_count'] = $lead_count;
		}else if ($ret['role'] == 1 || $ret['role'] == 2 || $ret['role'] == 16 || $ret['role'] == 17) {
		
			$ret['lead_count'][14] = 1;
			$ret['lead_count'][20] = 1;
			$ret['lead_count'][16] = 1;
			$ret['lead_count'][21] = 1;
			$ret['lead_count'][22] = 1;
			$ret['lead_count'][13] = 1;
			$ret['lead_count'][17] = 1;
			$ret['lead_count'][15] = 1;
			$ret['lead_count'][55] = 1;
			$ret['lead_count'][12] = 1;
			$ret['lead_count'][18] = 1;
			$ret['lead_count'][19] = 1;
			$ret['lead_count'][23] = 1;
			$ret['lead_count'][24] = 1;
			$ret['lead_count'][62] = 1;

			// new lead count
			$ret['lead_count'][76] = 1;
			$ret['lead_count'][83] = 1;
			$ret['lead_count'][77] = 1;
			$ret['lead_count'][81] = 1;
			$ret['lead_count'][82] = 1;
			$ret['lead_count'][74] = 1;
			$ret['lead_count'][78] = 1;
			$ret['lead_count'][75] = 1;
			$ret['lead_count'][86] = 1;
			$ret['lead_count'][87] = 1;
			$ret['lead_count'][73] = 1;
			$ret['lead_count'][79] = 1;
			$ret['lead_count'][80] = 1;
			$ret['lead_count'][84] = 1;
			$ret['lead_count'][85] = 1;
		}

		### QUOTATION ###
			$ret['quotation_access']['quotation_sales_user_id'] = array();
			$ret['quotation_access']['quotation_procurement_user_id'] = array();
			$ret['quotation_access']['quotation_sales_user_id'][] = $ret['user_id'];
			$ret['quotation_access']['quotation_procurement_user_id'][] = $ret['user_id'];
			$ret['quotation_access']['quotation_form_margin_and_unit_price_access'] = false;
			$ret['quotation_access']['quotation_pdf_client_name_access'] = false;
			$ret['quotation_access']['quotation_pdf_line_item_amount_access'] = false;
			$ret['quotation_access']['quotation_pdf_total_amount_access'] = false;
			$ret['quotation_access']['quotation_pdf_before_pulish_line_item_amount_access'] = false;
			$ret['quotation_access']['quotation_pdf_before_publish_total_amount_access'] = false;
			$ret['quotation_access']['quotation_form_stage_access'] = false;
			$ret['quotation_access']['quotation_list_client_name_access'] = false;
			$ret['quotation_access']['quotation_list_value_access'] = false;
			$ret['quotation_access']['quotation_list_avg_margin_access'] = false;
			$ret['quotation_access']['quotation_list_action_view_quotation_details_access'] = false;
			$ret['quotation_access']['quotation_list_action_view_pdf_access'] = false;
			$ret['quotation_access']['quotation_list_action_edit_access'] = false;
			$ret['quotation_access']['quotation_list_action_delete_access'] = false;
			$ret['quotation_access']['quotation_list_action_follow_up_access'] = false;
			$ret['quotation_access']['quotation_list_action_query_access'] = false;
			$ret['quotation_access']['quotation_list_action_graph_access'] = false;
			$ret['quotation_access']['quotation_list_action_rating_access'] = false;
			$ret['quotation_access']['quotation_list_action_view_proforma_pdf_access'] = false;
			$ret['quotation_access']['quotation_list_action_purchase_order_add_access'] = false;
			$ret['quotation_access']['quotation_list_action_purchase_order_view_access'] = false;
			$ret['quotation_access']['quotation_add_client_name_access'] = false;
			$ret['quotation_access']['quotation_add_member_name_access'] = false;
			$ret['quotation_access']['quotation_pdf_banner_for_saudi_access'] = false;
			$ret['quotation_access']['quotation_pdf_banner_for_qatar_access'] = false;
			$ret['quotation_access']['quotation_list_tab_access'] = array();
			$ret['quotation_access']['quotation_form_add_new_product_access'] = false;
			$ret['quotation_access']['quotation_form_add_new_unit_access'] = false;

		### LEAD ###
			$ret['lead_access']['lead_sales_user_id'] = array();
			$ret['lead_access']['lead_country_id'] = array();
			$ret['lead_access']['lead_sales_user_id'][] = $ret['user_id'];
			$ret['lead_access']['lead_list_instrumentation_tab_access'] = false;
			$ret['lead_access']['lead_list_pq_tab_access'] = false;
			$ret['lead_access']['lead_list_action_pq_status_access'] = false;
			$ret['lead_access']['lead_list_action_pq_details_access'] = false;
			$ret['lead_access']['lead_list_action_graph_expoter_name_access'] = false;
			$ret['lead_access']['lead_list_action_graph_access'] = false;
			$ret['lead_access']['lead_list_action_pq_query_access'] = false;
			$ret['lead_access']['lead_list_action_special_comment_access'] = false;

		
		### PQ ###
			$ret['pq_access']['pq_sales_user_id'] = array();
			$ret['pq_access']['pq_sales_user_id'][] = $ret['user_id'];
		
		### RFQ ###
			$ret['rfq_access']['rfq_sales_user_id'] = array();
			$ret['rfq_access']['rfq_procurement_user_id'] = array();
			$ret['rfq_access']['rfq_sales_user_id'][] = $ret['user_id'];
			$ret['rfq_access']['rfq_procurement_user_id'][] = $ret['user_id'];
			$ret['rfq_access']['rfq_priority'] = false;
			$ret['rfq_access']['rfq_list_client_name_access'] = false;
			$ret['rfq_access']['rfq_add_client_name_access'] = false;
			$ret['rfq_access']['rfq_list_action_edit_access'] = false;
			$ret['rfq_access']['rfq_list_action_is_new_access'] = false;
			$ret['rfq_access']['rfq_list_action_delete_access'] = false;
			$ret['rfq_access']['rfq_list_action_query_access'] = false;
			$ret['rfq_access']['rfq_list_action_chat_conversation_access'] = false;
			$ret['rfq_access']['rfq_list_action_graph_access'] = false;
			$ret['rfq_access']['rfq_list_action_rfq_and_technical_document_access'] = false;
			$ret['rfq_access']['rfq_list_member_name_access'] = false;
			$ret['rfq_access']['rfq_list_value_and_country_access'] = false;
			$ret['rfq_access']['rfq_list_client_type_access'] = false;
			$ret['rfq_access']['rfq_list_rfq_type_access'] = false;
			$ret['rfq_access']['rfq_add_member_name_access'] = false;
			$ret['rfq_access']['rfq_document_delete_access'] = array();
			$ret['rfq_access']['rfq_product_family_access'] = array();

		### QUERY ###
			$ret['query_access']['query_sales_user_id'] = array();
			$ret['query_access']['query_procurement_user_id'] = array();
			$ret['query_access']['query_sales_user_id'][] = $ret['user_id'];
			$ret['query_access']['query_procurement_user_id'][] = $ret['user_id'];
			$ret['query_access']['query_list_client_name_access'] = false;
			$ret['query_access']['query_list_comment_tab_access'] = false;
			$ret['query_access']['query_creator_id'][] = $ret['user_id'];
			$ret['query_access']['query_list_mtc_drawing_tab_access'] = false;
			$ret['query_access']['query_quality_user_id'] = array();
			$ret['query_access']['query_quality_user_id'][] = $ret['user_id'];
			$ret['query_access']['query_dataentry_user_id'] = array();
			$ret['query_access']['query_dataentry_user_id'][] = $ret['user_id'];
			$ret['query_access']['query_production_user_id'] = array();
			$ret['query_access']['query_production_user_id'][] = $ret['user_id'];
			$ret['query_access']['query_pq_user_id'] = array();
			$ret['query_access']['query_pq_user_id'][] = $ret['user_id'];
			$ret['query_access']['query_list_sample_query_reassigned_to_access'] = false;
		
		### PRODUCTION ###

			$ret['production_access']['production_sales_user_id'] = array();
			$ret['production_access']['production_sales_user_id'][] = $ret['user_id'];
			$ret['production_access']['production_procurement_user_id'] = array();
			$ret['production_access']['production_procurement_user_id'][] = $ret['user_id'];
			$ret['production_access']['production_list_tab_access'] = array();
			// $ret['production_access']['production_yta_tab_access'] = false;
			$ret['production_access']['production_listing_filter_client_name'] = false;
			$ret['production_access']['production_status_wise_count'] = false;
			$ret['production_access']['production_list_delete'] = false;
			// $ret['production_access']['production_list_action_tab_access_procurment_user'] = false;
			$ret['production_access']['production_mobile_client_type'] = false;
			$ret['production_access']['production_list_action_edit_access'] = false;
			$ret['production_access']['production_list_action_query_access'] = false;
			$ret['production_access']['production_list_action_mtc_view_access'] = false;
			$ret['production_access']['production_list_action_technical_document_file_and_path_access'] = false;
			$ret['production_access']['production_list_action_get_task_module_document_access'] = false;
			$ret['production_access']['production_list_action_show_in_details_access'] = false;
			$ret['production_access']['production_list_action_rating_access'] = false;
			$ret['production_access']['production_list_search_filter_access'] = false;
			$ret['production_access']['production_list_work_order_sheet_add_access'] = false;
			$ret['production_access']['production_list_work_order_sheet_edit_access'] = false;
			$ret['production_access']['production_list_work_order_sheet_view_access'] = false;
			$ret['production_access']['production_list_charges_and_grand_total_access'] = false;
			$ret['production_access']['production_product_family_access'] = array();
			$ret['production_access']['production_list_action_rfq_edit_access'] = false;
			$ret['production_access']['production_list_action_quotation_edit_access'] = false;
			$ret['production_access']['production_form_data_view_access'] = false;
			$ret['production_access']['production_technical_document_delete_access'] = false;


			
		### HR ###
			$ret['hr_access']['hr_user_id'] = array();
			$ret['hr_access']['hr_user_id'][] = $ret['user_id'];
			$ret['hr_access']['reporting_to_user_id'] = array();
			$ret['hr_access']['reporting_to_user_id'][] = $ret['user_id'];
			$ret['hr_access']['mrf_list_action_stage_and_clone_access'] = false;
			$ret['hr_access']['mrf_list_action_delete_access'] = false;
			$ret['hr_access']['candidate_list_action_tab_access'] = false;
			$ret['hr_access']['candidate_list_action_change_mrf_access'] = false;
			$ret['hr_access']['mrf_form_assigned_to_access'] = false;


		### Quality ###
			$ret['quality_access']['client_name_access'] = false;
			$ret['quality_access']['quality_mtc_and_marking_upload_access'] = false;
			$ret['quality_access']['quality_product_family_access'] = array();



		### Invoice ###
			$ret['invoice_access']['sales_user_id'] = array();
			$ret['invoice_access']['sales_user_id'][] = $ret['user_id'];

		### Task ###
			$ret['task_access']['task_list_type_access'] = array();
			$ret['task_access']['task_list_action_marking_view_access'] = false;
			$ret['task_access']['task_list_action_mtc_view_access'] = false;
			$ret['task_access']['task_list_client_name_access'] = false;
			$ret['task_access']['task_list_rfq_and_quote_no_access'] = false;
			$ret['task_access']['task_list_action_status_access'] = false;


		if(in_array($ret['role'], array(1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 13, 14, 16, 17, 18, 21, 24))){
			
			$manager_details = $this->db->get_where('manager_access_details', array('user_id'=>$ret['user_id']))->row_array();
			

			if(!empty($manager_details)){

				foreach (json_decode($manager_details['quotation'], true) as $key => $single_details) {
					
					switch ($single_details['name']) {
						case 'quotation_sales_user_id':
							
							foreach (explode(", ", $single_details['ids']) as $single_id) {

								$ret['quotation_access']['quotation_sales_user_id'][] = $single_id;
							}
						break;
						case 'quotation_procurement_user_id':
							
							foreach (explode(", ", $single_details['ids']) as $single_id) {

								$ret['quotation_access']['quotation_procurement_user_id'][] = $single_id;
							}
						break;
						case 'quotation_form_margin_and_unit_price_access':
						case 'quotation_pdf_client_name_access':
						case 'quotation_pdf_line_item_amount_access':
						case 'quotation_pdf_total_amount_access':
						case 'quotation_pdf_before_pulish_line_item_amount_access':
						case 'quotation_pdf_before_publish_total_amount_access':
						case 'quotation_form_stage_access':
						case 'quotation_list_client_name_access':
						case 'quotation_list_value_access':
						case 'quotation_list_avg_margin_access':
						case 'quotation_list_action_view_quotation_details_access':
						case 'quotation_list_action_view_pdf_access':
						case 'quotation_list_action_edit_access':
						case 'quotation_list_action_delete_access':
						case 'quotation_list_action_follow_up_access':
						case 'quotation_list_action_query_access':
						case 'quotation_list_action_graph_access':
						case 'quotation_list_action_rating_access':
						case 'quotation_list_action_view_proforma_pdf_access':
						case 'quotation_list_action_purchase_order_add_access':
						case 'quotation_list_action_purchase_order_view_access':
						case 'quotation_add_client_name_access':
						case 'quotation_add_member_name_access':
						case 'quotation_pdf_banner_for_saudi_access':
						case 'quotation_pdf_banner_for_qatar_access':
						case 'quotation_form_add_new_product_access':
						case 'quotation_form_add_new_unit_access':

							$ret['quotation_access'][$single_details['name']] = (boolean)$single_details['access'];
						break;
						case 'quotation_list_tab_access':

							foreach (explode(", ", $single_details['list_tab']) as $single_list) {

								$ret['quotation_access']['quotation_list_tab_access'][] = $single_list;
							}
						break;
						default:
						break;
					}
				}
				foreach (json_decode($manager_details['lead'], true) as $key => $single_details) {
					
					switch ($single_details['name']) {

						case 'lead_sales_user_id':
							
							foreach (explode(", ", $single_details['ids']) as $single_id) {

								$ret['lead_access']['lead_sales_user_id'][] = $single_id;
							}
						break;
						case 'lead_country_id':
							
							foreach (explode(", ", $single_details['ids']) as $single_id) {

								$ret['lead_access']['lead_country_id'][] = $single_id;
							}
						break;
						case 'lead_list_instrumentation_tab_access':
						case 'lead_list_pq_tab_access':
						case 'lead_list_action_pq_status_access':
						case 'lead_list_action_pq_details_access':
						case 'lead_list_action_graph_expoter_name_access':
						case 'lead_list_action_graph_access':
						case 'lead_list_action_pq_query_access':
						case 'lead_list_action_special_comment_access':

							$ret['lead_access'][$single_details['name']] = (boolean)$single_details['access'];
						break;
						default:
						break;
					}
				}
				foreach (json_decode($manager_details['pq'], true) as $key => $single_details) {
					
					switch ($single_details['name']) {
						case 'pq_sales_user_id':
							
							foreach (explode(", ", $single_details['ids']) as $single_id) {

								$ret['pq_access']['sales_user_id'][] = $single_id;
							}
						break;
						default:
						break;
					}
				}
				foreach (json_decode($manager_details['rfq'], true) as $key => $single_details) {
					
					switch ($single_details['name']) {
						case 'rfq_sales_user_id':
							
							foreach (explode(", ", $single_details['ids']) as $single_id) {

								$ret['rfq_access']['rfq_sales_user_id'][] = $single_id;
							}
						break;
						case 'rfq_procurement_user_id':
							
							foreach (explode(", ", $single_details['ids']) as $single_id) {

								$ret['rfq_access']['rfq_procurement_user_id'][] = $single_id;
							}
						break;
						case 'rfq_priority':
						case 'rfq_list_client_name_access':
						case 'rfq_add_client_name_access':
						case 'rfq_list_action_edit_access':
						case 'rfq_list_action_is_new_access':
						case 'rfq_list_action_delete_access':
						case 'rfq_list_action_query_access':
						case 'rfq_list_action_chat_conversation_access':
						case 'rfq_list_action_graph_access':
						case 'rfq_list_action_rfq_and_technical_document_access':
						case 'rfq_list_member_name_access':
						case 'rfq_list_value_and_country_access':
						case 'rfq_list_client_type_access':
						case 'rfq_list_rfq_type_access':
						case 'rfq_add_member_name_access':

							$ret['rfq_access'][$single_details['name']] = (boolean)$single_details['access'];
						break;
						case 'rfq_document_delete_access':

							foreach (explode(", ", $single_details['list_tab']) as $single_list) {

								$ret['rfq_access']['rfq_document_delete_access'][] = $single_list;
							}
						break;
						case 'rfq_product_family_access':

							foreach (explode(", ", $single_details['list_tab']) as $single_list) {

								$ret['rfq_access']['rfq_product_family_access'][] = $single_list;
							}
						break;
						default:
						break;
					}
				}
				foreach (json_decode($manager_details['query'], true) as $key => $single_details) {
					
					switch ($single_details['name']) {
						case 'query_sales_user_id':
							
							foreach (explode(", ", $single_details['ids']) as $single_id) {

								$ret['query_access']['query_sales_user_id'][] = $single_id;
							}
						break;
						case 'query_procurement_user_id':
							
							foreach (explode(", ", $single_details['ids']) as $single_id) {

								$ret['query_access']['query_procurement_user_id'][] = $single_id;
							}
						break;
						case 'query_list_client_name_access':
						case 'query_list_comment_tab_access':
						case 'query_list_mtc_drawing_tab_access':
						case 'query_list_sample_query_reassigned_to_access':

							$ret['query_access'][$single_details['name']] = (boolean)$single_details['access'];
						break;
						case 'query_creator_id':

							foreach (explode(", ", $single_details['ids']) as $single_id) {

								$ret['query_access']['query_creator_id'][] = $single_id;
							}
						break;
						case 'query_quality_user_id':

							foreach (explode(", ", $single_details['ids']) as $single_id) {

								$ret['query_access']['query_quality_user_id'][] = $single_id;
							}
						break;
						case 'query_dataentry_user_id':

							foreach (explode(", ", $single_details['ids']) as $single_id) {

								$ret['query_access']['query_dataentry_user_id'][] = $single_id;
							}
						break;
						case 'query_production_user_id':

							foreach (explode(", ", $single_details['ids']) as $single_id) {

								$ret['query_access']['query_production_user_id'][] = $single_id;
							}
						break;
						case 'query_pq_user_id':

							foreach (explode(", ", $single_details['ids']) as $single_id) {

								$ret['query_access']['query_pq_user_id'][] = $single_id;
							}
						break;
						default:
						break;
					}
				}
				foreach (json_decode($manager_details['production'], true) as $key => $single_details) {
					
					switch ($single_details['name']) {
						case 'production_sales_user_id':
							
							foreach (explode(", ", $single_details['ids']) as $single_id) {

								$ret['production_access']['production_sales_user_id'][] = $single_id;
							}
						break;
						
						case 'production_procurement_user_id':
							
							foreach (explode(", ", $single_details['ids']) as $single_id) {

								$ret['production_access']['production_procurement_user_id'][] = $single_id;
							}
						break;
						case 'production_list_tab_access':
							
							foreach (explode(", ", $single_details['list_tab']) as $single_tab) {

								$ret['production_access']['production_list_tab_access'][] = $single_tab;
							}
						break;
						// case 'yta_tab_access':
						case 'production_listing_filter_client_name':
						case 'production_status_wise_count':
						case 'production_list_delete':
						// case 'production_list_action_tab_access_procurment_user':
						case 'production_mobile_client_type':
						case 'production_list_action_edit_access':
						case 'production_list_action_query_access':
						case 'production_list_action_mtc_view_access':
						case 'production_list_action_technical_document_file_and_path_access':
						case 'production_list_action_get_task_module_document_access':
						case 'production_list_action_show_in_details_access':
						case 'production_list_action_rating_access':
						case 'production_list_search_filter_access':
						case 'production_list_work_order_sheet_add_access':
						case 'production_list_work_order_sheet_edit_access':
						case 'production_list_work_order_sheet_view_access':
						case 'production_list_charges_and_grand_total_access':
						case 'production_list_action_rfq_edit_access':
						case 'production_list_action_quotation_edit_access':
						case 'production_form_data_view_access':
						case 'production_technical_document_delete_access':
							
							$ret['production_access'][$single_details['name']] = (boolean)$single_details['access'];
						break;
						case 'production_product_family_access':

							foreach (explode(", ", $single_details['list_tab']) as $single_list) {

								$ret['production_access']['production_product_family_access'][] = $single_list;
							}
						break;
						default:
						break;
					}
				}
				foreach (json_decode($manager_details['hr'], true) as $key => $single_details) {

					switch ($single_details['name']) {
						case 'hr_user_id':

							foreach (explode(", ", $single_details['ids']) as $single_id) {

								$ret['hr_access']['hr_user_id'][] = $single_id;
							}
						break;
						case 'reporting_to_user_id':

							foreach (explode(", ", $single_details['ids']) as $single_id) {

								$ret['hr_access']['reporting_to_user_id'][] = $single_id;
							}
						break;
						case 'mrf_list_action_stage_and_clone_access':
						case 'mrf_list_action_delete_access':
						case 'candidate_list_action_change_mrf_access':
						case 'candidate_list_action_tab_access':
						case 'mrf_form_assigned_to_access':

							$ret['hr_access'][$single_details['name']] = (boolean)$single_details['access'];
						break;
						default:
						break;
					}
				}
				foreach (json_decode($manager_details['quality'], true) as $key => $single_details) {

					switch ($single_details['name']) {
						case 'client_name_access':
						case 'quality_mtc_and_marking_upload_access':

							$ret['quality_access'][$single_details['name']] = $single_details['access'];

						break;
						case 'quality_product_family_access':

							foreach (explode(", ", $single_details['list_tab']) as $single_list) {

								$ret['quality_access']['quality_product_family_access'][] = $single_list;
							}
						break;
						default:
						break;
					}
				}
				foreach (json_decode($manager_details['invoice'], true) as $key => $single_details) {

					switch ($single_details['name']) {
						case 'sales_user_id':

							foreach (explode(", ", $single_details['ids']) as $single_id) {

								$ret['invoice_access']['sales_user_id'][] = $single_id;
							}
						break;
						default:
						break;
					}
				}
				// echo "<pre>";print_r($manager_details['task']);echo"</pre><hr>";

				// echo "<pre>";print_r(json_decode($manager_details['task'], true));echo"</pre><hr>";die;
				foreach (json_decode($manager_details['task'], true) as $key => $single_details) {
					
					switch ($single_details['name']) {
						case 'task_list_type_access':

							foreach (explode(", ", $single_details['list_tab']) as $single_id) {

								$ret['task_access']['task_list_type_access'][] = $single_id;
							}
						break;
						case 'task_list_action_marking_view_access':
						case 'task_list_action_mtc_view_access':
						case 'task_list_client_name_access':
						case 'task_list_rfq_and_quote_no_access':
						case 'task_list_action_status_access':

							$ret['task_access'][$single_details['name']] = (boolean)$single_details['access'];
						break;
						default:
						break;
					}
				}
				// echo "<pre>";print_r($ret['production_access']);echo"</pre><hr>";die('debug');
			}
			if(in_array($ret['role'], array(5))){

				$org_chart_manager_details = $this->db->get_where('user_organization_chart', array('reporting_manager_id'=>$ret['user_id']))->result_array();
				// echo "<pre>";print_r($org_chart_manager_details);echo"</pre><hr>";die;

				if(!empty($org_chart_manager_details)){

					foreach ($org_chart_manager_details as $single_details) {
					
						$ret['quotation_access']['quotation_sales_user_id'][] = $single_details['user_id'];
						$ret['lead_access']['lead_sales_user_id'][] = $single_details['user_id'];
						$ret['pq_access']['pq_sales_user_id'][] = $single_details['user_id'];
						$ret['rfq_access']['rfq_sales_user_id'][] = $single_details['user_id'];
						$ret['query_access']['query_sales_user_id'][] = $single_details['user_id'];
						$ret['production_access']['production_sales_user_id'][] = $single_details['user_id'];
					}
					// echo "<pre>";print_r($ret['production_access']);echo"</pre><hr>";die('debug');
				}
			}
			// echo "<pre>";print_r($ret);echo"</pre><hr>";die('debug');
			$sales_where = "";
			if($ret['role'] == 5){

				if(!empty($ret['lead_access']['lead_country_id'])){

					$sales_where .= " AND customer_mst.country_id IN (".implode(', ', $ret['lead_access']['lead_country_id']).")";
				}
				$sales_where .= " AND customer_mst.assigned_to IN (".implode(', ', $ret['lead_access']['lead_sales_user_id']).")";
				// echo $sales_where;die('debug');
			}
			if(in_array($ret['role'], array(1, 2, 3, 5, 16, 17))){
			foreach (array('tube'=>1, 'pipe'=>2, 'hammer_union'=>5, 'tubing'=>6, 'process_control'=>7) as $lead_name => $product_id) {
				
				$ret['lead_tab_count'][$lead_name] = count($this->db->query("SELECT customer_mst.id FROM `customer_mst` INNER JOIN`customer_data`ON `customer_data`.`customer_mst_id` = `customer_mst`.`id` WHERE customer_mst.deleted is null AND customer_data.rank is not null AND customer_data.product_category_id = {$product_id} {$sales_where}")->result_array());
			}
			foreach (array('chemical_companies'=>8,
			'epc_companies'=>9, 'distributors'=>10,	'shipyards'=>11, 'water_companies'=>12, 'forged_fittings'=>13, 'heteregenous_tubes_india'=>14, 'sugar_companies'=>15, 'pvf_companies'=>16, 'miscellaneous_leads'=>17, 'hydraulic_fitting'=>18, 'fasteners'=>19) as $lead_name => $product_id) {
				
				$ret['lead_tab_count'][$lead_name] = count($this->db->query("SELECT customer_mst.id FROM `customer_mst` INNER JOIN `customer_data` ON `customer_data`.`customer_mst_id` = `customer_mst`.`id` WHERE customer_mst.deleted is null AND customer_data.product_category_id = {$product_id} {$sales_where}")->result_array());
				// $ret['lead_tab_count'][$lead_name] = 1111;
			}
			$ret['lead_tab_count']['internal'] = count($this->db->query("SELECT customer_mst.id FROM `customer_mst` INNER JOIN `customer_data` ON `customer_data`.`customer_mst_id` = `customer_mst`.`id` WHERE customer_data.source = 'internal' {$sales_where}")->result_array());
			// $ret['lead_tab_count']['internal'] = 1111;
			}
			if(in_array($ret['role'], array(1, 2, 7, 16, 17))){

			$ret['lead_tab_count']['pq'] = count($this->db->query("SELECT id FROM `customer_mst` WHERE deleted is null AND is_pq = 'Yes' AND status = 'Active' {$sales_where}")->result_array());
			// $ret['lead_tab_count']['pq'] = 1111;
			}


			// echo "<pre>";print_r($ret['lead_tab_count']);echo"</pre><hr>";exit;
		}
		$data = array(
			'user_id'      =>  $user_details['user_id'],
			'ip_address'   =>  $_SERVER['REMOTE_ADDR'],
			'login_time '  =>  date('Y-m-d H:i:s')
		);
		$this->db->insert('ip_logs', $data);
		//insert new ip
		return $ret;
	}

//fetch old ip from db and insert ip
/*
	public function insertIp($id, $ip, $now)
    {
        $data = array(
            'user_id'      =>  $id,
            'ip_address'   =>  $ip,
            'login_time '  =>  $now
        );
        $this->db->insert('ip_logs', $data);
        $insert_id = $this->db->insert_id(); 
    }
    
    public function getipDetails($id)
    {
        $query = $this->db->query("SELECT * FROM ip WHERE user_id = '$id' ORDER BY login_time DESC");
        $result = $query->result_array();
        return $result;
    }    
	
*/
	function updatePassword($arr){
		$this->db->select('password');
		$res = $this->db->get_where('users', array('user_id' => $this->session->userdata('user_id')))->row_array();

		if($res['password'] == md5($arr['current_password'])){
			$this->db->update('users', array('password' => md5($arr['new_password'])), array('user_id' => $this->session->userdata('user_id')));
			return 1;
		}else{
			return 0;
		}
	}
	function verify_sub_user($current_id, $next_id){
		$row = $this->db->get_where('users', array('user_id' => $current_id))->row_array();
		$sub_users = explode(',', $row['sub_users']);
		if(in_array($next_id, $sub_users)){
			return true;
		}else{
			return false;
		}
	}

	function otherLogin($next_id){
		$this->db->select('u.user_id, name, username, email, role, m.module_controller, u.sub_users');
		$this->db->join('user_to_module um', 'u.user_id = um.user_id', 'left');
		$this->db->join('modules m', 'm.module_id = um.module_id', 'inner');
		$this->db->where('u.user_id', $next_id);
		$res = $this->db->get('users u')->result_array();

		if(!empty($res)){
			$this->db->update('users', array('last_login' => date('Y-m-d H:i:s')), array('user_id' => $res[0]['user_id']));
			
			$this->db->order_by('log_id', 'desc');
			$this->db->limit(1);
			$ip_details = $this->db->get('ip_logs', array('user_id' => $res[0]['user_id']))->row_array();
			
			$ret = array();
			foreach ($res as $key => $value) {
				$ret['user_id'] = $value['user_id'];
				$ret['name'] = $value['name'];
				$ret['username'] = $value['username'];
				$ret['email'] = $value['email'];
				$ret['role'] = $value['role'];
				$ret['module'][] = $value['module_controller'];
				if(!empty($ip_details)){
					$ret['last_ip'] = $ip_details['ip_address'];
					$ret['last_login'] = $ip_details['login_time'];
				}else{
					$ret['last_ip'] = '';
					$ret['last_login'] = '';
				}
				$ret['current_ip'] = $_SERVER['REMOTE_ADDR'];
				$ret['current_login'] = date('Y-m-d H:i:s');
				$sub_users = $value['sub_users'];
			}

			if($sub_users != '' && $sub_users != null){
				$this->db->select('user_id, name');
				$this->db->where('user_id in ('.$sub_users.')');
				$ret['sub_users'] = $this->db->get('users')->result_array();
			}

			if($ret['role'] == 5){
				$this->db->select('source');
				$this->db->distinct();
				$sales_res = $this->db->get_where('hetro_leads', array('assigned_to' => $ret['user_id']))->result_array();
				$ret['sales_access'] = array();
				if(!empty($sales_res)){
					foreach ($sales_res as $key => $value) {
						$ret['sales_access'][] = $value['source'];
					}
				}

				$this->db2->select('data_category');
				$this->db2->distinct();
				$sales_res = $this->db2->get_where('lead_mst', array('assigned_to' => $ret['user_id']))->result_array();
				if(!empty($sales_res)){
					foreach ($sales_res as $key => $value) {
						$ret['sales_access'][] = $value['data_category'];
					}
				}
			}
			
			$data = array(
				'user_id'      =>  $res[0]['user_id'],
				'ip_address'   =>  $_SERVER['REMOTE_ADDR'],
				'login_time '  =>  date('Y-m-d H:i:s')
			);
			$this->db->insert('ip_logs', $data);
			//insert new ip
			return $ret;
		}else{
			return false;
		}
	}

	public function check_otp_exist_or_not($where_string) {

		$this->db->where($where_string, null, false);
		$result = $this->db->get('login_verfication')->result_array();
		return $result;

	}
}
?>