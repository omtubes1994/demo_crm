<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_hooks extends MX_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('common/common_model');
    }

    public function set_graph_data() {
        
        $data['graph_data_access']=array_column($this->common_model->get_all_conditional_data_sales_db('*',array('user_id'=>$this->session->userdata('user_id'),'status'=>'Active'),'users_to_graph'),'graph_information_id');
        $this->session->set_userdata(array('graph_data_access' =>$data['graph_data_access']));

    }

    public function set_alert_data() {

        // getting alert receiver notification data
        $alert_data = $this->common_model->get_all_conditional_data_sales_db('*',array('status'=>'Active', 'alert_showed_user_id_status'=> 'True', 'alert_showed_user_id'=>$this->session->userdata('user_id')),'user_alert');
        $user_name = array_column($this->common_model->get_all_conditional_data_sales_db('*',array(),'users'), 'name', 'user_id');
        $data['alert_data'] = array();
        foreach ($alert_data as $single_alert_details) {
            
            $data['alert_data']['alert_received'][$single_alert_details['module_name']][] = array(
                'id' => $single_alert_details['id'],
                'alert_showed_user_name' => $user_name[$single_alert_details['alert_showed_user_id']],
                'alert_given_user_name' => $user_name[$single_alert_details['alert_given_user_id']],
                'date' => date('M, j', strtotime($single_alert_details['add_time'])),
                'subject' => $single_alert_details['alert_subject']
            );
            
        }

        // getting alert sender notification data
        $alert_data = $this->common_model->get_all_conditional_data_sales_db('*',array('status'=>'Active', 'alert_given_user_id_status'=> 'True', 'alert_viewed'=> 'True','alert_given_user_id'=>$this->session->userdata('user_id')),'user_alert');
        $user_name = array_column($this->common_model->get_all_conditional_data_sales_db('*',array(),'users'), 'name', 'user_id');
        foreach ($alert_data as $single_alert_details) {
            
            $data['alert_data']['alert_given'][$single_alert_details['module_name']][] = array(
                'id' => $single_alert_details['id'],
                'alert_showed_user_name' => $user_name[$single_alert_details['alert_showed_user_id']],
                'alert_given_user_name' => $user_name[$single_alert_details['alert_given_user_id']],
                'date' => date('M, j', strtotime($single_alert_details['add_time'])),
                'subject' => $single_alert_details['alert_subject']
            );
            
        }
        $this->session->set_userdata(array('alert_data' =>$data['alert_data']));        
    }
}
?>